package com.alitalia.aem.ws.mmb.staticdataservice;


import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.security.KeyStore;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;
import javax.xml.namespace.QName;
import javax.xml.ws.Binding;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.common.exception.WSClientException;
import com.alitalia.aem.ws.common.logginghandler.SOAPLoggingHandler;
import com.alitalia.aem.ws.common.messagehandler.SOAPMessageHandler;
import com.alitalia.aem.ws.common.utils.WSClientConstants;
import com.alitalia.aem.ws.mmb.staticdataservice.wsdl.IStaticDataService;
import com.alitalia.aem.ws.mmb.staticdataservice.wsdl.StaticDataService;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd1.StaticDataRequest;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.ArrayOfAirportCheckin;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.ArrayOfAmericanStates;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.ArrayOfCountry;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.ArrayOfDictionaryItem;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.ArrayOfFrequentFlyerCarrier;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd3.ArrayOfMeal;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd3.ArrayOfPaymentTypeItem;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd3.ArrayOfPhonePrefix;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd6.StaticDataResponse;

@Service(value = MmbStaticDataServiceClient.class)
@Component(immediate = true, metatype = false)
@Properties({
	@Property(name = "service.vendor", value = "Reply")
})
public class MmbStaticDataServiceClient {

	private static final String[] OUTBOUND_ELEMENTS_CONTAINING_TYPE_DECLARATION = {"anyType"};

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Property(description = "URL of the 'Static Data Web Service WSDL' used to invoke the Web Service.")
	private static final String WS_ENDPOINT_PROPERTY = "ws.endpoint";

	@Property(description = "Flag used to invoke Web Service in HTTPS.")
	private static final String WS_HTTPS_MODE_PROPERTY = "ws.https_mode";

	@Property(description = "Flag used to load identity store.")
	private static final String WS_LOAD_IDENTITY_STORE_PROPERTY = "ws.load_identity_store";

	@Property(description = "Flag used to enable hostname verifier.")
	private static final String WS_HOSTNAME_VERIFIER_PROPERTY = "ws.hostname_verifier";

	@Property(description = "Property used to set identity store path.")
	private static final String WS_IDENTITY_STORE_PATH_PROPERTY = "ws.identity_store_path";

	@Property(description = "Property used to set identity store password.")
	private static final String WS_IDENTITY_STORE_PASSWORD_PROPERTY = "ws.identity_store_password";

	@Property(description = "Property used to set identity key password.")
	private static final String WS_IDENTITY_KEY_PASSWORD_PROPERTY = "ws.identity_key_password";

	@Property(description = "Property used to set trust store path.")
	private static final String WS_TRUST_STORE_PATH_PROPERTY = "ws.trust_store_path";

	@Property(description = "Property used to set trust store password.")
	private static final String WS_TRUST_STORE_PASSWORD_PROPERTY = "ws.trust_store_password";

	@Property(description = "Property used to set connection timout")
	private static final String WS_CONNECTION_TIMEOUT_PROPERTY = "ws.connection_timeout";

	@Property(description = "Property used to set request timout")
	private static final String WS_REQUEST_TIMEOUT_PROPERTY = "ws.request_timeout";

	@Property(description = "Property used to set request timout")
	private static final String WS_SSL_PROTOCL = "ws.ssl_protocol";

	@Property(description = "Property used to print the request")
	private static final String WS_PRINT_REQUET = "ws.print_request";

	@Property(description = "Property used to print the response")
	private static final String WS_PRINT_RESPONSE = "ws.print_response";

	private String serviceEndpoint;
	private String serviceQNameNamespace;
	private String serviceQNameLocalPart;
	private Integer connectionTimeout;
	private Integer requestTimeout;
	private String identityStorePath;
	private String identityStorePassword;
	private String identityKeyPassword;
	private String trustStorePath;
	private String trustStorePassword;
	private String sslProtocol;
	private boolean hostnameVerifier;
	private boolean loadIdentityStore;
	private boolean httpsMode;
	private boolean printRequest;
	private boolean printResponse;

	private StaticDataService staticDataService;
	private IStaticDataService iStaticDataService;

	private boolean initRequired = true;

	public MmbStaticDataServiceClient() throws WSClientException {
		this.serviceQNameLocalPart = "StaticDataService";
		this.serviceQNameNamespace = "http://tempuri.org/"; 		
		setDefault();
	}

	public MmbStaticDataServiceClient(String serviceEndpoint) throws WSClientException {
		this.serviceQNameLocalPart = "StaticDataService";
		this.serviceQNameNamespace = "http://tempuri.org/"; 		
		setDefault();

		this.serviceEndpoint = serviceEndpoint;

		try {
			init();
			this.initRequired = false;
		} catch (Exception e) {
			this.initRequired = true;
			logger.error("Error initializing MmbStaticDataServiceClient", e);
		}
	}

	public MmbStaticDataServiceClient(String serviceEndpoint, Integer connectionTimeout, Integer requestTimeout) throws WSClientException {
		this.serviceQNameLocalPart = "StaticDataService";
		this.serviceQNameNamespace = "http://tempuri.org/"; 		
		setDefault();

		this.serviceEndpoint = serviceEndpoint;
		this.connectionTimeout = connectionTimeout;
		this.requestTimeout = requestTimeout;

		try {
			init();
			this.initRequired = false;
		} catch (Exception e) {
			this.initRequired = true;
			logger.error("Error initializing MmbStaticDataServiceClient", e);
		}
	}

	public MmbStaticDataServiceClient(String serviceEndpoint, Integer connectionTimeout, Integer requestTimeout, Boolean httpsMode,
			Boolean hostnameVerifier, Boolean loadIdentityStore, String identityStorePath, String identityStorePassword,
			String identityKeyPassword, String trustStorePath, String trustStorePassword, String sslProtocol) throws WSClientException {
		this.serviceEndpoint = serviceEndpoint;
		this.connectionTimeout = connectionTimeout;
		this.requestTimeout = requestTimeout;
		this.httpsMode = httpsMode;
		this.hostnameVerifier = hostnameVerifier;
		this.loadIdentityStore = loadIdentityStore;
		this.identityStorePath = identityStorePath;
		this.identityStorePassword = identityStorePassword;
		this.identityKeyPassword = identityKeyPassword;
		this.trustStorePath = trustStorePath;
		this.trustStorePassword = trustStorePassword;
		this.sslProtocol = sslProtocol;

		this.serviceQNameLocalPart = "StaticDataService";
		this.serviceQNameNamespace = "http://tempuri.org/"; 

		try {
			init();
			this.initRequired = false;
		} catch (Exception e) {
			this.initRequired = true;
			logger.error("Error initializing MmbStaticDataServiceClient", e);
		}
	}

	private void init() throws WSClientException {
		FileInputStream fisTruststore = null;
		FileInputStream fisIdentitystore = null;
		try {
			URL classURL = StaticDataService.class.getClassLoader().getResource(this.serviceEndpoint);
			QName serviceQName = new QName(serviceQNameNamespace, serviceQNameLocalPart);

			staticDataService = new StaticDataService(classURL, serviceQName);
			iStaticDataService = staticDataService.getEndPoint8();

			BindingProvider bindingProviderNetwork = (BindingProvider) iStaticDataService;
			Binding bindingNetwork = bindingProviderNetwork.getBinding();

			@SuppressWarnings("rawtypes")
			List<Handler> handlerListNetwork = bindingNetwork.getHandlerChain();
			SOAPLoggingHandler soapLoggingHandlerNetwork = new SOAPLoggingHandler(this.printRequest, this.printResponse, true);
			SOAPMessageHandler msgHandler = new SOAPMessageHandler(true);
			msgHandler.setElementsContainingTypeDeclaration(OUTBOUND_ELEMENTS_CONTAINING_TYPE_DECLARATION);
			handlerListNetwork.add(msgHandler);
			handlerListNetwork.add(soapLoggingHandlerNetwork);

			bindingNetwork.setHandlerChain(handlerListNetwork);

			logger.debug("Https mode [" + httpsMode + "]");

			if (httpsMode) {
				logger.debug("Loaded identity store path [" + identityStorePath + "], trust store path [" + trustStorePath + "], " 
						+ "hostname verifier [" + hostnameVerifier + "] and loading identity store flag [" + loadIdentityStore + "]");

				SSLContext sslContext = SSLContext.getInstance(sslProtocol);

				TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
				KeyStore trustKeystore = KeyStore.getInstance(KeyStore.getDefaultType());
				fisTruststore = new FileInputStream(trustStorePath);
				trustKeystore.load(fisTruststore, trustStorePassword.toCharArray());
				tmf.init(trustKeystore);

				if (loadIdentityStore) {
					KeyStore identityKeystore = KeyStore.getInstance(WSClientConstants.keystoreTypeDefault);
					fisIdentitystore = new FileInputStream(identityStorePath);
					identityKeystore.load(fisIdentitystore, identityStorePassword.toCharArray());

					KeyManagerFactory kmf = KeyManagerFactory.getInstance(WSClientConstants.keymanagerFactoryAlgorithmDefault);
					kmf.init(identityKeystore, identityKeyPassword.toCharArray());
					sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

					logger.debug("Loaded identity and trust store");
				} else {
					sslContext.init(null, tmf.getTrustManagers(), null);
					logger.debug("Loaded trust store only");
				}

				SSLContext.setDefault(sslContext);
				bindingProviderNetwork.getRequestContext().put(WSClientConstants.JAXWS_SSL_SOCKET_FACTORY_PROPERTY,
						sslContext.getSocketFactory());

				if(!hostnameVerifier) {
					bindingProviderNetwork.getRequestContext().put(WSClientConstants.JAXWS_HOSTNAME_VERIFIER_PROPERTY, new HostnameVerifier() {

						@Override
						public boolean verify(String arg0, SSLSession arg1) {					
							return true;
						}

					});				
				}
			}

			((BindingProvider) iStaticDataService).getRequestContext().put(
					WSClientConstants.BINDING_PROVIDER_CONNECT_TIMEOUT_PROPERTY, connectionTimeout);
			((BindingProvider) iStaticDataService).getRequestContext().put(
					WSClientConstants.BINDING_PROVIDER_REQUEST_TIMEOUT_PROPERTY, requestTimeout);

			logger.debug("Set up request timeout [" +
					((BindingProvider) iStaticDataService).getRequestContext()
					.get(WSClientConstants.BINDING_PROVIDER_REQUEST_TIMEOUT_PROPERTY) + "] and " + "connection timeout [" +
					((BindingProvider) iStaticDataService).getRequestContext()
					.get("WSClientConstants.BINDING_PROVIDER_CONNECT_TIMEOUT_PROPERTY") + "]");
		} catch (Exception e) {
			logger.error("Error to initialize StaticDataServiceClient", e);
			throw new WSClientException("Error to initialize StaticDataServiceClient", e);
		} finally {
			if (fisIdentitystore != null) {
				try {
					fisIdentitystore.close();
				} catch (IOException e) {
					logger.error("Error to close input stream", e);
				}
			}
			
			if (fisTruststore != null) {
				try {
					fisTruststore.close();
				} catch (IOException e) {
					logger.error("Error to close input stream", e);
				}
			}
		}
	}

	@Activate
	protected void activate(final Map<String, Object> config) {
		String serviceEndpoint = null;
		boolean httpsMode;
		boolean hostnameVerifier;
		boolean loadIdentityStore;
		boolean printRequest;
		boolean printResponse;
		String identityStorePath = null;
		String identityStorePassword = null;
		String identityKeyPassword = null;
		String trustStorePath = null;
		String trustStorePassword = null;
		Integer connectionTimeout = null;
		Integer requestTimeout = null;
		String sslProtocol = null;

		try {			
			httpsMode = Boolean.valueOf(String.valueOf(config.get(WS_HTTPS_MODE_PROPERTY)));
			this.httpsMode = httpsMode;

			hostnameVerifier = Boolean.valueOf(String.valueOf(config.get(WS_HOSTNAME_VERIFIER_PROPERTY)));
			this.hostnameVerifier = hostnameVerifier;

			loadIdentityStore = Boolean.valueOf(String.valueOf(config.get(WS_LOAD_IDENTITY_STORE_PROPERTY)));
			this.loadIdentityStore = loadIdentityStore;

			identityStorePath = String.valueOf(config.get(WS_IDENTITY_STORE_PATH_PROPERTY));
			this.identityStorePath = identityStorePath;

			identityStorePassword = String.valueOf(config.get(WS_IDENTITY_STORE_PASSWORD_PROPERTY));
			this.identityStorePassword = identityStorePassword;

			identityKeyPassword = String.valueOf(config.get(WS_IDENTITY_KEY_PASSWORD_PROPERTY));
			this.identityKeyPassword = identityKeyPassword;

			trustStorePath = String.valueOf(config.get(WS_TRUST_STORE_PATH_PROPERTY));
			this.trustStorePath = trustStorePath;

			trustStorePassword = String.valueOf(config.get(WS_TRUST_STORE_PASSWORD_PROPERTY));
			this.trustStorePassword = trustStorePassword;

			connectionTimeout = Integer.valueOf(String.valueOf(config.get(WS_CONNECTION_TIMEOUT_PROPERTY)));
			this.connectionTimeout = connectionTimeout;	

			requestTimeout = Integer.valueOf(String.valueOf(config.get(WS_REQUEST_TIMEOUT_PROPERTY)));
			this.requestTimeout = requestTimeout;	

			sslProtocol = String.valueOf(config.get(WS_SSL_PROTOCL));
			this.sslProtocol = sslProtocol;

			printRequest = Boolean.valueOf(String.valueOf(config.get(WS_PRINT_REQUET)));
			this.printRequest = printRequest;	

			printResponse = Boolean.valueOf(String.valueOf(config.get(WS_PRINT_RESPONSE)));
			this.printResponse = printResponse;	
		} catch (Exception e) {
			logger.error("Error to read custom configuration to activate StaticDataServiceClient - default configuration used", e);
			setDefault();
		}

		try {			
			serviceEndpoint = String.valueOf(config.get(WS_ENDPOINT_PROPERTY));
			this.serviceEndpoint = serviceEndpoint;

			logger.info("Activated StaticDataServiceClient: "
					+ "WS_ENDPOINT_PROPERTY=[" + this.serviceEndpoint + "], "
					+ "WS_HTTPS_MODE_PROPERTY=[" + this.httpsMode + "], "
					+ "WS_HOSTNAME_VERIFIER_PROPERTY=[" + this.hostnameVerifier +"], "
					+ "WS_LOAD_IDENTITY_STORE_PROPERTY=[" + this.loadIdentityStore + "], "
					+ "WS_IDENTITY_STORE_PATH_PROPERTY=[" + this.identityStorePath + "], "
					+ "WS_TRUST_STORE_PATH_PROPERTY=[" + this.trustStorePath + "], "
					+ "WS_CONNECTION_TIMEOUT_PROPERTY=[" + this.connectionTimeout + "], "
					+ "WS_REQUEST_TIMEOUT_PROPERTY=[" + this.requestTimeout + "], "
					+ "WS_REQUEST_TIMEOUT_PROPERTY=[" + this.requestTimeout + "], "
					+ "WS_SSL_PROTOCOL=[" + this.sslProtocol + "], "
					+ "WS_PRINT_RESPONSE=[" + this.printRequest + "], "
					+ "WS_PRINT_REQUET=[" + this.printResponse + "]");
		} catch (Exception e) {
			logger.error("Error to read WS_ENDPOINT_PROPERTY to activate StaticDataServiceClient", e);
			throw new WSClientException("Error to initialize StaticDataServiceClient", e);
		}

		try {
			init();
			this.initRequired = false;
		} catch (Exception e) {
			this.initRequired = true;
			logger.error("Error initializing MmbStaticDataServiceClient", e);
		}
	}

	@Modified
	protected void modified(final Map<String, Object> config) {
		String serviceEndpoint = null;
		boolean httpsMode;
		boolean hostnameVerifier;
		boolean loadIdentityStore;
		boolean printRequest;
		boolean printResponse;
		String identityStorePath = null;
		String identityStorePassword = null;
		String identityKeyPassword = null;
		String trustStorePath = null;
		String trustStorePassword = null;
		Integer connectionTimeout = null;
		Integer requestTimeout = null;
		String sslProtocol = null;

		try {			
			httpsMode = Boolean.valueOf(String.valueOf(config.get(WS_HTTPS_MODE_PROPERTY)));
			this.httpsMode = httpsMode;

			hostnameVerifier = Boolean.valueOf(String.valueOf(config.get(WS_HOSTNAME_VERIFIER_PROPERTY)));
			this.hostnameVerifier = hostnameVerifier;

			loadIdentityStore = Boolean.valueOf(String.valueOf(config.get(WS_LOAD_IDENTITY_STORE_PROPERTY)));
			this.loadIdentityStore = loadIdentityStore;

			identityStorePath = String.valueOf(config.get(WS_IDENTITY_STORE_PATH_PROPERTY));
			this.identityStorePath = identityStorePath;

			identityStorePassword = String.valueOf(config.get(WS_IDENTITY_STORE_PASSWORD_PROPERTY));
			this.identityStorePassword = identityStorePassword;

			identityKeyPassword = String.valueOf(config.get(WS_IDENTITY_KEY_PASSWORD_PROPERTY));
			this.identityKeyPassword = identityKeyPassword;

			trustStorePath = String.valueOf(config.get(WS_TRUST_STORE_PATH_PROPERTY));
			this.trustStorePath = trustStorePath;

			trustStorePassword = String.valueOf(config.get(WS_TRUST_STORE_PASSWORD_PROPERTY));
			this.trustStorePassword = trustStorePassword;

			connectionTimeout = Integer.valueOf(String.valueOf(config.get(WS_CONNECTION_TIMEOUT_PROPERTY)));
			this.connectionTimeout = connectionTimeout;	

			requestTimeout = Integer.valueOf(String.valueOf(config.get(WS_REQUEST_TIMEOUT_PROPERTY)));
			this.requestTimeout = requestTimeout;	

			sslProtocol = String.valueOf(config.get(WS_SSL_PROTOCL));
			this.sslProtocol = sslProtocol;	

			printRequest = Boolean.valueOf(String.valueOf(config.get(WS_PRINT_REQUET)));
			this.printRequest = printRequest;	

			printResponse = Boolean.valueOf(String.valueOf(config.get(WS_PRINT_RESPONSE)));
			this.printResponse = printResponse;	
		} catch (Exception e) {
			logger.error("Error to read custom configuration to modify StaticDataServiceClient - default configuration used", e);
			setDefault();
		}

		try {			
			serviceEndpoint = String.valueOf(config.get(WS_ENDPOINT_PROPERTY));
			this.serviceEndpoint = serviceEndpoint;

			logger.info("Modified StaticDataServiceClient: "
					+ "WS_ENDPOINT_PROPERTY=[" + this.serviceEndpoint + "], "
					+ "WS_HTTPS_MODE_PROPERTY=[" + this.httpsMode + "], "
					+ "WS_HOSTNAME_VERIFIER_PROPERTY=[" + this.hostnameVerifier +"], "
					+ "WS_LOAD_IDENTITY_STORE_PROPERTY=[" + this.loadIdentityStore + "], "
					+ "WS_IDENTITY_STORE_PATH_PROPERTY=[" + this.identityStorePath + "], "
					+ "WS_TRUST_STORE_PATH_PROPERTY=[" + this.trustStorePath + "], "
					+ "WS_CONNECTION_TIMEOUT_PROPERTY=[" + this.connectionTimeout + "], "
					+ "WS_REQUEST_TIMEOUT_PROPERTY=[" + this.requestTimeout + "], "
					+ "WS_REQUEST_TIMEOUT_PROPERTY=[" + this.requestTimeout + "], "
					+ "WS_SSL_PROTOCOL=[" + this.sslProtocol + "], "
					+ "WS_PRINT_RESPONSE=[" + this.printRequest + "], "
					+ "WS_PRINT_REQUET=[" + this.printResponse + "]");
		} catch (Exception e) {
			logger.error("Error to read WS_ENDPOINT_PROPERTY to modify StaticDataServiceClient", e);
			throw new WSClientException("Error to initialize StaticDataServiceClient", e);
		}

		this.staticDataService = null;

		try {
			init();
			this.initRequired = false;
		} catch (Exception e) {
			this.initRequired = true;
			logger.error("Error initializing MmbStaticDataServiceClient", e);
		}
	}

	private void setDefault() {
		this.httpsMode = WSClientConstants.httpsModeDefault;
		this.hostnameVerifier = WSClientConstants.hostnameVerifierDefault;
		this.loadIdentityStore = WSClientConstants.loadIdentityStoreDefault;
		this.connectionTimeout = WSClientConstants.connectionTimeoutDefault;
		this.requestTimeout = WSClientConstants.requestTimeoutDefault;
		this.sslProtocol = WSClientConstants.sslProtocolDefault;
		this.printRequest = WSClientConstants.printRequest;
		this.printResponse = WSClientConstants.printResponse;
	}

	public Boolean deleteItemCache(StaticDataRequest request) {
		if (this.initRequired) {
			init();
			this.initRequired = false;
		}
		
		try {
			logger.info("Calling WS-MMBStaticDataService Operation-deleteItemCache...");
			long before = System.currentTimeMillis();
			Boolean response = iStaticDataService.deleteItemCache(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MMBStaticDataService Operation-deleteItemCache...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MMBStaticDataService Operation-deleteItemCache", e);
		}
	}

	public StaticDataResponse getData(StaticDataRequest request) {
		if (this.initRequired) {
			init();
			this.initRequired = false;
		}
		
		try {
			logger.info("Calling WS-MMBStaticDataService Operation-getData...");
			long before = System.currentTimeMillis();
			StaticDataResponse response = iStaticDataService.getData(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MMBStaticDataService Operation-getData...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MMBStaticDataService Operation-getData", e);
		}
	}

	public ArrayOfMeal loadMeal(ArrayOfDictionaryItem parameters) {
		if (this.initRequired) {
			init();
			this.initRequired = false;
		}
		
		try {
			logger.info("Calling WS-MMBStaticDataService Operation-loadMeal...");
			long before = System.currentTimeMillis();
			ArrayOfMeal response = iStaticDataService.loadMeal(parameters);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MMBStaticDataService Operation-loadMeal...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MMBStaticDataService Operation-loadMeal", e);
		}
	}

	public ArrayOfAirportCheckin loadAirportCheckin(ArrayOfDictionaryItem parameters) {
		if (this.initRequired) {
			init();
			this.initRequired = false;
		}
		
		try {
			logger.info("Calling WS-MMBStaticDataService Operation-loadAirportCheckin...");
			long before = System.currentTimeMillis();
			ArrayOfAirportCheckin response = iStaticDataService.loadAirportCheckin(parameters);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MMBStaticDataService Operation-loadAirportCheckin...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MMBStaticDataService Operation-loadAirportCheckin", e);
		}
	}

	public ArrayOfCountry loadCountry(ArrayOfDictionaryItem parameters) {
		if (this.initRequired) {
			init();
			this.initRequired = false;
		}
		
		try {
			logger.info("Calling WS-MMBStaticDataService Operation-loadCountry...");
			long before = System.currentTimeMillis();
			ArrayOfCountry response = iStaticDataService.loadCountry(parameters);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MMBStaticDataService Operation-loadCountry...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MMBStaticDataService Operation-loadCountry", e);
		}
	}

	public ArrayOfFrequentFlyerCarrier loadFrequentFlyersList(ArrayOfDictionaryItem parameters) {
		if (this.initRequired) {
			init();
			this.initRequired = false;
		}
		
		try {
			logger.info("Calling WS-MMBStaticDataService Operation-loadFrequentFlyersList...");
			long before = System.currentTimeMillis();
			ArrayOfFrequentFlyerCarrier response = iStaticDataService.loadFrequentFlyersList(parameters);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MMBStaticDataService Operation-loadFrequentFlyersList...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MMBStaticDataService Operation-loadFrequentFlyersList", e);
		}
	}

	public ArrayOfAmericanStates loadAmericanStates() {
		if (this.initRequired) {
			init();
			this.initRequired = false;
		}
		
		try {
			logger.info("Calling WS-MMBStaticDataService Operation-loadAmericanStates...");
			long before = System.currentTimeMillis();
			ArrayOfAmericanStates response = iStaticDataService.loadAmericanStates();
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MMBStaticDataService Operation-loadAmericanStates...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MMBStaticDataService Operation-loadAmericanStates", e);
		}
	}

	public ArrayOfPhonePrefix loadPhonePrefix(ArrayOfDictionaryItem parameters) {
		if (this.initRequired) {
			init();
			this.initRequired = false;
		}
		
		try {
			logger.info("Calling WS-MMBStaticDataService Operation-loadPhonePrefix...");
			long before = System.currentTimeMillis();
			ArrayOfPhonePrefix response = iStaticDataService.loadPhonePrefix(parameters);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MMBStaticDataService Operation-loadPhonePrefix...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MMBStaticDataService Operation-loadPhonePrefix", e);
		}
	}

	public ArrayOfPaymentTypeItem loadPaymentTypeItem(ArrayOfDictionaryItem parameters)  {
		if (this.initRequired) {
			init();
			this.initRequired = false;
		}
		
		try {
			logger.info("Calling WS-MMBStaticDataService Operation-loadPaymentTypeItem...");
			long before = System.currentTimeMillis();
			ArrayOfPaymentTypeItem response = iStaticDataService.loadPaymentTypeItem(parameters);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MMBStaticDataService Operation-loadPaymentTypeItem...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MMBStaticDataService Operation-loadPaymentTypeItem", e);
		}
	}

	public Integer getConnectionTimeout() {
		return connectionTimeout;
	}

	public Integer getRequestTimeout() {
		return requestTimeout;
	}

	public String getIdentityStorePath() {
		return identityStorePath;
	}

	public String getIdentityStorePassword() {
		return identityStorePassword;
	}

	public String getTrustStorePath() {
		return trustStorePath;
	}

	public String getTrustStorePassword() {
		return trustStorePassword;
	}

	public boolean getHostnameVerifier() {
		return hostnameVerifier;
	}

	public boolean getLoadIdentityStore() {
		return loadIdentityStore;
	}

	public boolean getHttpsMode() {
		return httpsMode;
	}

	public String getSslProtocol() {
		return sslProtocol;
	}

	public String getIdentityKeyPassword() {
		return identityKeyPassword;
	}

	public boolean isPrintRequest() {
		return printRequest;
	}

	public boolean isPrintResponse() {
		return printResponse;
	}	

}
