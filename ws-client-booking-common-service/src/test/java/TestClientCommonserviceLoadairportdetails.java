import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.booking.commonservice.CommonServiceClient;

public class TestClientCommonserviceLoadairportdetails {
	
	private CommonServiceClient commonServiceClient;

	private static final Logger log = LoggerFactory.getLogger(TestClientCommonserviceLoadairportdetails.class);
	
	public TestClientCommonserviceLoadairportdetails() throws Exception {
		try {
			commonServiceClient = new CommonServiceClient("http://romcs0944.user.alitalia.local:9000/WcfBooking/Alitalia.Portal.WebFrontend.LCWP.Booking.Services.Lib.CommonService.svc");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void execute() {
		try {
			/*
//			DatatypeFactory dtf = DatatypeFactory.newInstance();
//			com.alitalia.aem.ws.booking.commonservice.xsd.ObjectFactory factory = new com.alitalia.aem.ws.booking.commonservice.xsd.ObjectFactory();
			com.alitalia.aem.ws.booking.commonservice.xsd1.ObjectFactory factory1 = new com.alitalia.aem.ws.booking.commonservice.xsd1.ObjectFactory();
//			com.alitalia.aem.ws.booking.commonservice.xsd2.ObjectFactory factory2 = new com.alitalia.aem.ws.booking.commonservice.xsd2.ObjectFactory();
//			com.alitalia.aem.ws.booking.commonservice.xsd3.ObjectFactory factory3 = new com.alitalia.aem.ws.booking.commonservice.xsd3.ObjectFactory();
//			com.alitalia.aem.ws.booking.commonservice.xsd4.ObjectFactory factory4 = new com.alitalia.aem.ws.booking.commonservice.xsd4.ObjectFactory();
//			com.alitalia.aem.ws.booking.commonservice.xsd5.ObjectFactory factory5 = new com.alitalia.aem.ws.booking.commonservice.xsd5.ObjectFactory();
//			com.alitalia.aem.ws.booking.commonservice.xsd6.ObjectFactory factory6 = new com.alitalia.aem.ws.booking.commonservice.xsd6.ObjectFactory();
//			com.alitalia.aem.ws.booking.commonservice.xsd7.ObjectFactory factory7 = new com.alitalia.aem.ws.booking.commonservice.xsd7.ObjectFactory();

			AirportDetailRequest request = factory1.createAirportDetailRequest();
			request.setAirportCode(factory1.createAirportDetailRequestAirportCode("FCO"));

			AirportDetailResponse response = commonServiceClient.loadAirportDetails(request);
	*/
			log.info("call ok");
		} catch (Exception e) {
			log.info("error");
			e.printStackTrace();
		}
	}
	
	public static void main(String arg []) throws Exception {
		log.info("calling..");
		TestClientCommonserviceLoadairportdetails client = new TestClientCommonserviceLoadairportdetails();
		client.execute();
		log.info("call completed");
	} 

	

}
