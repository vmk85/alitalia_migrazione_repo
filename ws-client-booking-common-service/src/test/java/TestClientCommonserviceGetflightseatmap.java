import java.math.BigDecimal;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.booking.commonservice.CommonServiceClient;
import com.alitalia.aem.ws.booking.commonservice.xsd1.SeatMapRequest;
import com.alitalia.aem.ws.booking.commonservice.xsd1.SeatMapResponse;
import com.alitalia.aem.ws.booking.commonservice.xsd2.Airport;
import com.alitalia.aem.ws.booking.commonservice.xsd2.AreaValue;
import com.alitalia.aem.ws.booking.commonservice.xsd2.BaggageAllowance;
import com.alitalia.aem.ws.booking.commonservice.xsd2.Brand;
import com.alitalia.aem.ws.booking.commonservice.xsd2.BrandPenalties;
import com.alitalia.aem.ws.booking.commonservice.xsd2.BrandPenalty;
import com.alitalia.aem.ws.booking.commonservice.xsd2.DirectFlight;
import com.alitalia.aem.ws.booking.commonservice.xsd2.FlightDetails;
import com.alitalia.aem.ws.booking.commonservice.xsd2.FlightType;
import com.alitalia.aem.ws.booking.commonservice.xsd3.ArrayOfanyType;
import com.alitalia.aem.ws.booking.commonservice.xsd3.ArrayOfstring;
import com.alitalia.aem.ws.booking.commonservice.xsd4.Cabin;

public class TestClientCommonserviceGetflightseatmap {
	
	private CommonServiceClient commonServiceClient;

	private static final Logger log = LoggerFactory.getLogger(TestClientCommonserviceGetflightseatmap.class);
	
	public TestClientCommonserviceGetflightseatmap() throws Exception {
		try {
			commonServiceClient = new CommonServiceClient("http://romcs0944.user.alitalia.local:9000/WcfBooking/Alitalia.Portal.WebFrontend.LCWP.Booking.Services.Lib.CommonService.svc");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void execute() {
		try {
			DatatypeFactory dtf = DatatypeFactory.newInstance();
//			com.alitalia.aem.ws.booking.commonservice.xsd.ObjectFactory factory = new com.alitalia.aem.ws.booking.commonservice.xsd.ObjectFactory();
			com.alitalia.aem.ws.booking.commonservice.xsd1.ObjectFactory factory1 = new com.alitalia.aem.ws.booking.commonservice.xsd1.ObjectFactory();
			com.alitalia.aem.ws.booking.commonservice.xsd2.ObjectFactory factory2 = new com.alitalia.aem.ws.booking.commonservice.xsd2.ObjectFactory();
			com.alitalia.aem.ws.booking.commonservice.xsd3.ObjectFactory factory3 = new com.alitalia.aem.ws.booking.commonservice.xsd3.ObjectFactory();
//			com.alitalia.aem.ws.booking.commonservice.xsd4.ObjectFactory factory4 = new com.alitalia.aem.ws.booking.commonservice.xsd4.ObjectFactory();
//			com.alitalia.aem.ws.booking.commonservice.xsd5.ObjectFactory factory5 = new com.alitalia.aem.ws.booking.commonservice.xsd5.ObjectFactory();
//			com.alitalia.aem.ws.booking.commonservice.xsd6.ObjectFactory factory6 = new com.alitalia.aem.ws.booking.commonservice.xsd6.ObjectFactory();
//			com.alitalia.aem.ws.booking.commonservice.xsd7.ObjectFactory factory7 = new com.alitalia.aem.ws.booking.commonservice.xsd7.ObjectFactory();
			
			ArrayOfstring compartimentalClass = factory3.createArrayOfstring();
			compartimentalClass.getString().add("Q");

			Airport fromAirport = factory2.createAirport();
			fromAirport.setAZDeserves(true);
			fromAirport.setArea(AreaValue.DOM);
			fromAirport.setCity(factory2.createAirportCity("Roma"));
			fromAirport.setCityCode(factory2.createAirportCityCode("ROM"));
			fromAirport.setCityUrl(factory2.createAirportCityUrl(null));
			fromAirport.setCode(factory2.createAirportCode("FCO"));
			fromAirport.setCountry(factory2.createAirportCountry("Italia"));
			fromAirport.setCountryCode(factory2.createAirportCountryCode("IT"));
			fromAirport.setDescription(factory2.createAirportDescription("Roma, Fiumicino, Italia"));
			fromAirport.setDetails(factory2.createAirportDetails(null));
			fromAirport.setEligibility(true);
			fromAirport.setEtktAvlblFrom(factory2.createAirportEtktAvlblFrom(null));
			fromAirport.setHiddenInFirstDeparture(false);
			fromAirport.setIdMsg(1954);
			fromAirport.setName(factory2.createAirportName("Fiumicino"));
			fromAirport.setState(factory2.createAirportState(null));
			fromAirport.setStateCode(factory2.createAirportStateCode(""));
			fromAirport.setUnavlblFrom(factory2.createAirportUnavlblFrom(null));
			fromAirport.setUnavlblUntil(factory2.createAirportUnavlblUntil(null));

			Airport toAirport = factory2.createAirport();
			toAirport.setAZDeserves(true);
			toAirport.setArea(AreaValue.DOM);
			toAirport.setCity(factory2.createAirportCity("Palermo"));
			toAirport.setCityCode(factory2.createAirportCityCode("PMO"));
			toAirport.setCityUrl(factory2.createAirportCityUrl(null));
			toAirport.setCode(factory2.createAirportCode("PMO"));
			toAirport.setCountry(factory2.createAirportCountry("Italia"));
			toAirport.setCountryCode(factory2.createAirportCountryCode("IT"));
			toAirport.setDescription(factory2.createAirportDescription("Palermo, Palermo, Italia"));
			toAirport.setDetails(factory2.createAirportDetails(null));
			toAirport.setEligibility(true);
			toAirport.setEtktAvlblFrom(factory2.createAirportEtktAvlblFrom(null));
			toAirport.setHiddenInFirstDeparture(false);
			toAirport.setIdMsg(0);
			toAirport.setName(factory2.createAirportName("Palermo"));
			toAirport.setState(factory2.createAirportState(null));
			toAirport.setStateCode(factory2.createAirportStateCode(""));
			toAirport.setUnavlblFrom(factory2.createAirportUnavlblFrom(null));
			toAirport.setUnavlblUntil(factory2.createAirportUnavlblUntil(null));

			XMLGregorianCalendar departureDate = dtf.newXMLGregorianCalendar(2015, 07, 15, 00, 00, 00, 0000, DatatypeConstants.FIELD_UNDEFINED);
			XMLGregorianCalendar arrivalDate = dtf.newXMLGregorianCalendar(2015, 07, 21, 00, 00, 00, 0000, DatatypeConstants.FIELD_UNDEFINED);
			Duration duration = dtf.newDuration("PT1H10M");

			BrandPenalty penaltyChangeAfterDep = factory2.createBrandPenalty();
			penaltyChangeAfterDep.setMaxPrice(factory2.createBrandPenaltyMaxPrice(new BigDecimal(50)));
			penaltyChangeAfterDep.setMaxPriceCurrency(factory2.createBrandPenaltyMaxPriceCurrency("EUR"));
			penaltyChangeAfterDep.setMinPrice(factory2.createBrandPenaltyMinPrice(new BigDecimal(50)));
			penaltyChangeAfterDep.setMinPriceCurrency(factory2.createBrandPenaltyMinPriceCurrency("EUR"));
			penaltyChangeAfterDep.setPermitted(true);

			BrandPenalty penaltyChangeBeforeDep = factory2.createBrandPenalty();
			penaltyChangeBeforeDep.setMaxPrice(factory2.createBrandPenaltyMaxPrice(new BigDecimal(50)));
			penaltyChangeBeforeDep.setMaxPriceCurrency(factory2.createBrandPenaltyMaxPriceCurrency("EUR"));
			penaltyChangeBeforeDep.setMinPrice(factory2.createBrandPenaltyMinPrice(new BigDecimal(50)));
			penaltyChangeBeforeDep.setMinPriceCurrency(factory2.createBrandPenaltyMinPriceCurrency("EUR"));
			penaltyChangeBeforeDep.setPermitted(true);

			BrandPenalty penaltyRefundAfterDep = factory2.createBrandPenalty();
			penaltyRefundAfterDep.setMaxPrice(factory2.createBrandPenaltyMaxPrice(new BigDecimal(0)));
			penaltyRefundAfterDep.setMaxPriceCurrency(factory2.createBrandPenaltyMaxPriceCurrency(""));
			penaltyRefundAfterDep.setMinPrice(factory2.createBrandPenaltyMinPrice(new BigDecimal(0)));
			penaltyRefundAfterDep.setMinPriceCurrency(factory2.createBrandPenaltyMinPriceCurrency(""));
			penaltyRefundAfterDep.setPermitted(false);

			BrandPenalty penaltyRefundBeforeDep = factory2.createBrandPenalty();
			penaltyRefundBeforeDep.setMaxPrice(factory2.createBrandPenaltyMaxPrice(new BigDecimal(0)));
			penaltyRefundBeforeDep.setMaxPriceCurrency(factory2.createBrandPenaltyMaxPriceCurrency(""));
			penaltyRefundBeforeDep.setMinPrice(factory2.createBrandPenaltyMinPrice(new BigDecimal(0)));
			penaltyRefundBeforeDep.setMinPriceCurrency(factory2.createBrandPenaltyMinPriceCurrency(""));
			penaltyRefundBeforeDep.setPermitted(false);

			BrandPenalties brandPenalties = factory2.createBrandPenalties();
			brandPenalties.setChangeAfterDepature(factory2.createBrandPenaltiesChangeAfterDepature(penaltyChangeAfterDep));
			brandPenalties.setChangeBeforeDepature(factory2.createBrandPenaltiesChangeBeforeDepature(penaltyChangeBeforeDep));
			brandPenalties.setRefundAfterDepature(factory2.createBrandPenaltiesRefundAfterDepature(penaltyRefundAfterDep));
			brandPenalties.setRefundBeforeDepature(factory2.createBrandPenaltiesRefundBeforeDepature(penaltyRefundBeforeDep));

			ArrayOfstring brandNotes = factory3.createArrayOfstring();
			brandNotes.getString().add("NONREFUNDABLE-FARE");
			brandNotes.getString().add("PENALTY-FARE");

			Brand brand1 = factory2.createBrand();
			brand1.setProperties(factory2.createABoomBoxGenericInfoProperties(null));
			brand1.setSolutionId(factory2.createABoomBoxItemInfoSolutionId("Xdp8yxqK8TAUF6mh8UQaCS001"));
			brand1.setRefreshSolutionId(factory2.createABoomBoxRefreshInfoRefreshSolutionId(""));
			brand1.setBaggageAllowanceList(factory2.createBrandBaggageAllowanceList(null));
			brand1.setBestFare(true);
			brand1.setCode(factory2.createBrandCode("EconomyEasy"));
			brand1.setCompartimentalClass(factory2.createBrandCompartimentalClass(null));
			brand1.setCurrency(factory2.createBrandCurrency("EUR"));
			brand1.setEnabled(true);
			brand1.setGrossFare(new BigDecimal(396.23));
			brand1.setIndex(0);
			brand1.setNetFare(new BigDecimal(222.92));
			brand1.setNotes(factory2.createBrandNotes(brandNotes));
			brand1.setPenalties(factory2.createBrandPenalties(brandPenalties));
			brand1.setRealPrice(new BigDecimal(396.23));
			brand1.setSeatsAvailable(0);
			brand1.setSelected(true);
			brand1.setAdvancePurchaseDays(0);

			ArrayOfanyType brandsList = factory3.createArrayOfanyType();
			brandsList.getAnyType().add(brand1);

			BaggageAllowance baggageAllowance = factory2.createBaggageAllowance();
			baggageAllowance.setCode(factory2.createBaggageAllowanceCode("OGO"));
			baggageAllowance.setCount(1);
			ArrayOfanyType baggageAllowanceList  = factory3.createArrayOfanyType();
			baggageAllowanceList.getAnyType().add(baggageAllowance);

			String carrier = "AZ";
			String flightNumber= "1779";

			FlightDetails flightDetails = factory2.createFlightDetails();
			flightDetails.setAeromobile(factory2.createFlightDetailsAeromobile("32S"));
			flightDetails.setArrivalTerminal(factory2.createFlightDetailsArrivalTerminal(null));
			flightDetails.setDepartureTerminal(factory2.createFlightDetailsDepartureTerminal("1"));
			flightDetails.setDistance(factory2.createFlightDetailsDistance("254"));
			flightDetails.setOtherInformation(factory2.createFlightDetailsOtherInformation(null));

			DirectFlight directFlight = factory2.createDirectFlight();
			directFlight.setProperties(factory2.createABoomBoxGenericInfoProperties(null));
			directFlight.setBrands(factory2.createAFlightBaseBrands(brandsList));
			directFlight.setDuration(duration);
			directFlight.setType(FlightType.DIRECT);
			directFlight.setArrivalDate(arrivalDate);
			directFlight.setBaggageAllowanceList(factory2.createDirectFlightBaggageAllowanceList(baggageAllowanceList));
			directFlight.setCabin(Cabin.ECONOMY);
			directFlight.setCarrier(factory2.createDirectFlightCarrier(carrier ));
			directFlight.setCarrierForLogo(factory2.createDirectFlightCarrierForLogo(carrier));
			directFlight.setCompartimentalClass(factory2.createDirectFlightCompartimentalClass(compartimentalClass));
			directFlight.setDepartureDate(departureDate);
			directFlight.setDetails(factory2.createDirectFlightDetails(flightDetails));
			directFlight.setEnabledSeatMap(factory2.createDirectFlightEnabledSeatMap(null));
			directFlight.setFlightNumber(factory2.createDirectFlightFlightNumber(flightNumber));
			directFlight.setFrom(factory2.createDirectFlightFrom(fromAirport));
			directFlight.setMarriageGroup(factory2.createDirectFlightMarriageGroup("0"));
			directFlight.setMileage(new Long(0));
			directFlight.setOperationalCarrier(factory2.createDirectFlightOperationalCarrier(""));
			directFlight.setOperationalFlightNumber(factory2.createDirectFlightOperationalFlightNumber(""));
			directFlight.setOperationalFlightText(factory2.createDirectFlightOperationalFlightText(null));
			directFlight.setStopOver(false);
			directFlight.setTo(factory2.createDirectFlightTo(toAirport));
			directFlight.setUrlConditions(factory2.createDirectFlightUrlConditions(null));

			JAXBElement<DirectFlight> flightInfo = factory1.createSeatMapRequestFlightInfo(directFlight);

			SeatMapRequest request = factory1.createSeatMapRequest(); 
			request.setCompartimentalClass(factory1.createSeatMapRequestCompartimentalClass(compartimentalClass));
			request.setFlightInfo(flightInfo);

			@SuppressWarnings("unused")
			SeatMapResponse response = commonServiceClient.getFlightSeatMap(request);
	
			log.info("call ok");
		} catch (Exception e) {
			log.info("error");
			e.printStackTrace();
		}
	}
	
	public static void main(String arg []) throws Exception {
		log.info("calling..");
		TestClientCommonserviceGetflightseatmap client = new TestClientCommonserviceGetflightseatmap();
		client.execute();
		log.info("call completed");
	} 

	

}
