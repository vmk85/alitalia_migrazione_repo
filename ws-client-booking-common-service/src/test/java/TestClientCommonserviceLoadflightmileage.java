import java.math.BigDecimal;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.booking.commonservice.CommonServiceClient;
import com.alitalia.aem.ws.booking.commonservice.xsd1.FlightMileageRequest;
import com.alitalia.aem.ws.booking.commonservice.xsd1.FlightMileageResponse;
import com.alitalia.aem.ws.booking.commonservice.xsd2.Airport;
import com.alitalia.aem.ws.booking.commonservice.xsd2.AirportDetails;
import com.alitalia.aem.ws.booking.commonservice.xsd2.AreaValue;
import com.alitalia.aem.ws.booking.commonservice.xsd2.BaggageAllowance;
import com.alitalia.aem.ws.booking.commonservice.xsd2.Brand;
import com.alitalia.aem.ws.booking.commonservice.xsd2.BrandPenalties;
import com.alitalia.aem.ws.booking.commonservice.xsd2.BrandPenalty;
import com.alitalia.aem.ws.booking.commonservice.xsd2.DirectFlight;
import com.alitalia.aem.ws.booking.commonservice.xsd2.FlightDetails;
import com.alitalia.aem.ws.booking.commonservice.xsd2.FlightType;
import com.alitalia.aem.ws.booking.commonservice.xsd3.ArrayOfanyType;
import com.alitalia.aem.ws.booking.commonservice.xsd3.ArrayOfstring;
import com.alitalia.aem.ws.booking.commonservice.xsd4.Cabin;

public class TestClientCommonserviceLoadflightmileage {
	
	private CommonServiceClient commonServiceClient;

	private static final Logger log = LoggerFactory.getLogger(TestClientCommonserviceLoadflightmileage.class);
	
	public TestClientCommonserviceLoadflightmileage() throws Exception {
		try {
			commonServiceClient = new CommonServiceClient("http://romcs0944.user.alitalia.local:9000/WcfBooking/Alitalia.Portal.WebFrontend.LCWP.Booking.Services.Lib.CommonService.svc");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void execute() {
		try {
			DatatypeFactory dtf = DatatypeFactory.newInstance();
//			com.alitalia.aem.ws.booking.commonservice.xsd.ObjectFactory factory = new com.alitalia.aem.ws.booking.commonservice.xsd.ObjectFactory();
			com.alitalia.aem.ws.booking.commonservice.xsd1.ObjectFactory factory1 = new com.alitalia.aem.ws.booking.commonservice.xsd1.ObjectFactory();
			com.alitalia.aem.ws.booking.commonservice.xsd2.ObjectFactory factory2 = new com.alitalia.aem.ws.booking.commonservice.xsd2.ObjectFactory();
			com.alitalia.aem.ws.booking.commonservice.xsd3.ObjectFactory factory3 = new com.alitalia.aem.ws.booking.commonservice.xsd3.ObjectFactory();
//			com.alitalia.aem.ws.booking.commonservice.xsd4.ObjectFactory factory4 = new com.alitalia.aem.ws.booking.commonservice.xsd4.ObjectFactory();
//			com.alitalia.aem.ws.booking.commonservice.xsd5.ObjectFactory factory5 = new com.alitalia.aem.ws.booking.commonservice.xsd5.ObjectFactory();
//			com.alitalia.aem.ws.booking.commonservice.xsd6.ObjectFactory factory6 = new com.alitalia.aem.ws.booking.commonservice.xsd6.ObjectFactory();
//			com.alitalia.aem.ws.booking.commonservice.xsd7.ObjectFactory factory7 = new com.alitalia.aem.ws.booking.commonservice.xsd7.ObjectFactory();
			
			ArrayOfstring compartimentalClass = factory3.createArrayOfstring();
			compartimentalClass.getString().add("Q");

			AirportDetails airportFromDetails = factory2.createAirportDetails();
			airportFromDetails.setAZTerminal(factory2.createAirportDetailsAZTerminal("1,3"));
			airportFromDetails.setCheckInTimeDomestic(35);
			airportFromDetails.setCheckInTimeIntercontinental(60);
			airportFromDetails.setCheckInTimeInternational(45);
			airportFromDetails.setCityDistance(new Double(35.5));
			airportFromDetails.setCityTransferTime(0);
			airportFromDetails.setGMT(120);
			airportFromDetails.setNotesCode(1954);

			AirportDetails airportToDetails = factory2.createAirportDetails();
			airportToDetails.setAZTerminal(factory2.createAirportDetailsAZTerminal(""));
			airportToDetails.setCheckInTimeDomestic(25);
			airportToDetails.setCheckInTimeIntercontinental(0);
			airportToDetails.setCheckInTimeInternational(0);
			airportToDetails.setCityDistance(new Double(32));
			airportToDetails.setCityTransferTime(0);
			airportToDetails.setGMT(120);
			airportToDetails.setNotesCode(0);

			Airport fromAirport = factory2.createAirport();
			fromAirport.setAZDeserves(true);
			fromAirport.setArea(AreaValue.DOM);
			fromAirport.setCity(factory2.createAirportCity("Roma"));
			fromAirport.setCityCode(factory2.createAirportCityCode("ROM"));
			fromAirport.setCityUrl(factory2.createAirportCityUrl(null));
			fromAirport.setCode(factory2.createAirportCode("FCO"));
			fromAirport.setCountry(factory2.createAirportCountry("Italia"));
			fromAirport.setCountryCode(factory2.createAirportCountryCode("IT"));
			fromAirport.setDescription(factory2.createAirportDescription("Roma, Fiumicino, Italia"));
			fromAirport.setDetails(factory2.createAirportDetails(airportFromDetails));
			fromAirport.setEligibility(true);
			fromAirport.setEtktAvlblFrom(factory2.createAirportEtktAvlblFrom(null));
			fromAirport.setHiddenInFirstDeparture(false);
			fromAirport.setIdMsg(1954);
			fromAirport.setName(factory2.createAirportName("Fiumicino"));
			fromAirport.setState(factory2.createAirportState(null));
			fromAirport.setStateCode(factory2.createAirportStateCode(""));
			fromAirport.setUnavlblFrom(factory2.createAirportUnavlblFrom(null));
			fromAirport.setUnavlblUntil(factory2.createAirportUnavlblUntil(null));

			Airport toAirport = factory2.createAirport();
			toAirport.setAZDeserves(true);
			toAirport.setArea(AreaValue.DOM);
			toAirport.setCity(factory2.createAirportCity("Palermo"));
			toAirport.setCityCode(factory2.createAirportCityCode("PMO"));
			toAirport.setCityUrl(factory2.createAirportCityUrl(null));
			toAirport.setCode(factory2.createAirportCode("PMO"));
			toAirport.setCountry(factory2.createAirportCountry("Italia"));
			toAirport.setCountryCode(factory2.createAirportCountryCode("IT"));
			toAirport.setDescription(factory2.createAirportDescription("Palermo, Palermo, Italia"));
			toAirport.setDetails(factory2.createAirportDetails(airportToDetails));
			toAirport.setEligibility(true);
			toAirport.setEtktAvlblFrom(factory2.createAirportEtktAvlblFrom(null));
			toAirport.setHiddenInFirstDeparture(false);
			toAirport.setIdMsg(0);
			toAirport.setName(factory2.createAirportName("Palermo"));
			toAirport.setState(factory2.createAirportState(null));
			toAirport.setStateCode(factory2.createAirportStateCode(""));
			toAirport.setUnavlblFrom(factory2.createAirportUnavlblFrom(null));
			toAirport.setUnavlblUntil(factory2.createAirportUnavlblUntil(null));

			XMLGregorianCalendar departureDate = dtf.newXMLGregorianCalendar(2015, 07, 15, 00, 00, 00, 0000, DatatypeConstants.FIELD_UNDEFINED);
			XMLGregorianCalendar arrivalDate = dtf.newXMLGregorianCalendar(2015, 07, 21, 00, 00, 00, 0000, DatatypeConstants.FIELD_UNDEFINED);
			Duration duration = dtf.newDuration("PT1H10M");

			BrandPenalty penaltyChangeAfterDep = factory2.createBrandPenalty();
			penaltyChangeAfterDep.setMaxPrice(factory2.createBrandPenaltyMaxPrice(null));
			penaltyChangeAfterDep.setMaxPriceCurrency(factory2.createBrandPenaltyMaxPriceCurrency(null));
			penaltyChangeAfterDep.setMinPrice(factory2.createBrandPenaltyMinPrice(null));
			penaltyChangeAfterDep.setMinPriceCurrency(factory2.createBrandPenaltyMinPriceCurrency(null));
			penaltyChangeAfterDep.setPermitted(false);

			BrandPenalty penaltyChangeBeforeDep = factory2.createBrandPenalty();
			penaltyChangeBeforeDep.setMaxPrice(factory2.createBrandPenaltyMaxPrice(null));
			penaltyChangeBeforeDep.setMaxPriceCurrency(factory2.createBrandPenaltyMaxPriceCurrency(null));
			penaltyChangeBeforeDep.setMinPrice(factory2.createBrandPenaltyMinPrice(null));
			penaltyChangeBeforeDep.setMinPriceCurrency(factory2.createBrandPenaltyMinPriceCurrency(null));
			penaltyChangeBeforeDep.setPermitted(false);

			BrandPenalty penaltyRefundAfterDep = factory2.createBrandPenalty();
			penaltyRefundAfterDep.setMaxPrice(factory2.createBrandPenaltyMaxPrice(null));
			penaltyRefundAfterDep.setMaxPriceCurrency(factory2.createBrandPenaltyMaxPriceCurrency(null));
			penaltyRefundAfterDep.setMinPrice(factory2.createBrandPenaltyMinPrice(null));
			penaltyRefundAfterDep.setMinPriceCurrency(factory2.createBrandPenaltyMinPriceCurrency(null));
			penaltyRefundAfterDep.setPermitted(false);

			BrandPenalty penaltyRefundBeforeDep = factory2.createBrandPenalty();
			penaltyRefundBeforeDep.setMaxPrice(factory2.createBrandPenaltyMaxPrice(null));
			penaltyRefundBeforeDep.setMaxPriceCurrency(factory2.createBrandPenaltyMaxPriceCurrency(null));
			penaltyRefundBeforeDep.setMinPrice(factory2.createBrandPenaltyMinPrice(null));
			penaltyRefundBeforeDep.setMinPriceCurrency(factory2.createBrandPenaltyMinPriceCurrency(null));
			penaltyRefundBeforeDep.setPermitted(false);

			BrandPenalties brandPenalties = factory2.createBrandPenalties();
			brandPenalties.setChangeAfterDepature(factory2.createBrandPenaltiesChangeAfterDepature(penaltyChangeAfterDep));
			brandPenalties.setChangeBeforeDepature(factory2.createBrandPenaltiesChangeBeforeDepature(penaltyChangeBeforeDep));
			brandPenalties.setRefundAfterDepature(factory2.createBrandPenaltiesRefundAfterDepature(penaltyRefundAfterDep));
			brandPenalties.setRefundBeforeDepature(factory2.createBrandPenaltiesRefundBeforeDepature(penaltyRefundBeforeDep));

//			ArrayOfstring brandNotes = factory3.createArrayOfstring();
//			brandNotes.getString().add("NONREFUNDABLE-FARE");
//			brandNotes.getString().add("PENALTY-FARE");

			BaggageAllowance baggageAllowance = factory2.createBaggageAllowance();
			baggageAllowance.setCode(factory2.createBaggageAllowanceCode("0GO"));
			baggageAllowance.setCount(1);
			ArrayOfanyType baggageAllowanceList  = factory3.createArrayOfanyType();
			baggageAllowanceList.getAnyType().add(baggageAllowance);

			Brand brand1 = factory2.createBrand();
			brand1.setProperties(factory2.createABoomBoxGenericInfoProperties(null));
			brand1.setSolutionId(factory2.createABoomBoxItemInfoSolutionId("Esp7LJxtIXQPC3Vn90s92S001"));
			brand1.setRefreshSolutionId(factory2.createABoomBoxRefreshInfoRefreshSolutionId(""));
			brand1.setBaggageAllowanceList(factory2.createBrandBaggageAllowanceList(baggageAllowanceList));
			brand1.setBestFare(true);
			brand1.setCode(factory2.createBrandCode("EconomyPromo"));
			brand1.setCompartimentalClass(factory2.createBrandCompartimentalClass("L"));
			brand1.setCurrency(factory2.createBrandCurrency("EUR"));
			brand1.setEnabled(true);
			brand1.setGrossFare(new BigDecimal(396.23));
			brand1.setIndex(0);
			brand1.setNetFare(new BigDecimal(222.92));
			brand1.setNotes(factory2.createBrandNotes(null));
			brand1.setPenalties(factory2.createBrandPenalties(brandPenalties));
			brand1.setRealPrice(new BigDecimal(0));
			brand1.setSeatsAvailable(7);
			brand1.setSelected(true);
			brand1.setAdvancePurchaseDays(14);

			ArrayOfanyType brandsList = factory3.createArrayOfanyType();
			brandsList.getAnyType().add(brand1);

			String carrier = "AZ";
			String flightNumber= "1779";

			FlightDetails flightDetails = factory2.createFlightDetails();
			flightDetails.setAeromobile(factory2.createFlightDetailsAeromobile("32S"));
			flightDetails.setArrivalTerminal(factory2.createFlightDetailsArrivalTerminal(null));
			flightDetails.setDepartureTerminal(factory2.createFlightDetailsDepartureTerminal("1"));
			flightDetails.setDistance(factory2.createFlightDetailsDistance("254"));
			flightDetails.setOtherInformation(factory2.createFlightDetailsOtherInformation(null));
			
			DirectFlight directFlight = factory2.createDirectFlight();
			directFlight.setProperties(factory2.createABoomBoxGenericInfoProperties(null));
			directFlight.setBrands(factory2.createAFlightBaseBrands(brandsList));
			directFlight.setDuration(duration);
			directFlight.setType(FlightType.DIRECT);
			directFlight.setArrivalDate(arrivalDate);
			directFlight.setBaggageAllowanceList(factory2.createDirectFlightBaggageAllowanceList(null));
			directFlight.setCabin(Cabin.ECONOMY);
			directFlight.setCarrier(factory2.createDirectFlightCarrier(carrier));
			directFlight.setCarrierForLogo(factory2.createDirectFlightCarrierForLogo(null));
			directFlight.setCompartimentalClass(factory2.createDirectFlightCompartimentalClass(compartimentalClass));
			directFlight.setDepartureDate(departureDate);
			directFlight.setDetails(factory2.createDirectFlightDetails(flightDetails));
			directFlight.setDuration(duration);
			directFlight.setEnabledSeatMap(factory2.createDirectFlightEnabledSeatMap(null));
			directFlight.setFlightNumber(factory2.createDirectFlightFlightNumber(flightNumber));
			directFlight.setFrom(factory2.createDirectFlightFrom(fromAirport));
			directFlight.setMarriageGroup(factory2.createDirectFlightMarriageGroup(null));
			directFlight.setMileage(new Long(-1));
			directFlight.setOperationalCarrier(factory2.createDirectFlightOperationalCarrier(""));
			directFlight.setOperationalFlightNumber(factory2.createDirectFlightOperationalFlightNumber(""));
			directFlight.setOperationalFlightText(factory2.createDirectFlightOperationalFlightText(null));
			directFlight.setStopOver(false);
			directFlight.setTo(factory2.createDirectFlightTo(toAirport));
			directFlight.setUrlConditions(factory2.createDirectFlightUrlConditions(null));

			JAXBElement<DirectFlight> flightInfo = factory1.createFlightMileageRequestFlight(directFlight);

			FlightMileageRequest request = factory1.createFlightMileageRequest();
			request.setFlight(flightInfo);

			@SuppressWarnings("unused")
			FlightMileageResponse loadFlightMileage = commonServiceClient.loadFlightMileage(request);
	
			log.info("call ok");
		} catch (Exception e) {
			log.info("error");
			e.printStackTrace();
		}
	}
	
	public static void main(String arg []) throws Exception {
		log.info("calling..");
		TestClientCommonserviceLoadflightmileage client = new TestClientCommonserviceLoadflightmileage();
		client.execute();
		log.info("call completed");
	} 

	

}
