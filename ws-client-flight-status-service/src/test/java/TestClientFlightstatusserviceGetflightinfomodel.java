import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.booking.flightstatusservice.FlightStatusServiceClient;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetFlightInfoModelRequest;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetFlightInfoModelResponse;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.ArrivalAirport;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.DepartureAirport;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.FlightInfoModel;

public class TestClientFlightstatusserviceGetflightinfomodel {
	
	private FlightStatusServiceClient flightStatusServiceClient;

	private static final Logger log = LoggerFactory.getLogger(TestClientFlightstatusserviceGetflightinfomodel.class);
	
	public TestClientFlightstatusserviceGetflightinfomodel() throws Exception {
		try {
			flightStatusServiceClient = new FlightStatusServiceClient("http://romcs0944.user.alitalia.local:9000/WcfFlightStatus/Alitalia.Portal.WebFrontend.LCWP.FlightStatus.Services.Lib.FlightStatusService.svc");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void execute() {
		try {
			DatatypeFactory dtf = DatatypeFactory.newInstance();
//			com.alitalia.aem.ws.booking.flightstatusservice.xsd.ObjectFactory factory = new com.alitalia.aem.ws.booking.flightstatusservice.xsd.ObjectFactory();
//			com.alitalia.aem.ws.booking.flightstatusservice.xsd1.ObjectFactory factory1 = new com.alitalia.aem.ws.booking.flightstatusservice.xsd1.ObjectFactory();
			com.alitalia.aem.ws.booking.flightstatusservice.xsd2.ObjectFactory factory2 = new com.alitalia.aem.ws.booking.flightstatusservice.xsd2.ObjectFactory();
			com.alitalia.aem.ws.booking.flightstatusservice.xsd3.ObjectFactory factory3 = new com.alitalia.aem.ws.booking.flightstatusservice.xsd3.ObjectFactory();
			com.alitalia.aem.ws.booking.flightstatusservice.xsd4.ObjectFactory factory4 = new com.alitalia.aem.ws.booking.flightstatusservice.xsd4.ObjectFactory();

			XMLGregorianCalendar flightDate = dtf.newXMLGregorianCalendar(2015, 7, 8, 00, 00, 00, 00, DatatypeConstants.FIELD_UNDEFINED);
			DepartureAirport departureAirport = factory3.createDepartureAirport();
			departureAirport.setLocationCode(factory4.createAirportLocationCode("FCO"));
			ArrivalAirport arrivalAirport = factory3.createArrivalAirport();
			arrivalAirport.setLocationCode(factory4.createAirportLocationCode("PMO"));

			FlightInfoModel flightInfoModel = factory3.createFlightInfoModel();
			flightInfoModel.setDepartureAirport(factory3.createFlightInfoModelDepartureAirport(departureAirport));
			flightInfoModel.setDepartureDateTime(flightDate);
			flightInfoModel.setArrivalAirport(factory3.createFlightInfoModelArrivalAirport(arrivalAirport));
			flightInfoModel.setArrivalDateTime(flightDate);
			flightInfoModel.setFlightNumber(factory3.createFlightInfoModelFlightNumber("1779"));
			flightInfoModel.setVector(factory3.createFlightInfoModelVector("AZ"));

			GetFlightInfoModelRequest request = factory2.createGetFlightInfoModelRequest();
			request.setFlightInfo(factory2.createGetFlightInfoModelRequestFlightInfo(flightInfoModel));

			@SuppressWarnings("unused")
			GetFlightInfoModelResponse result = flightStatusServiceClient.getFlightInfoModel(request);
	
			log.info("call ok");
		} catch (Exception e) {
			log.info("error");
			e.printStackTrace();
		}
	}
	
	public static void main(String arg []) throws Exception {
		log.info("calling..");
		TestClientFlightstatusserviceGetflightinfomodel client = new TestClientFlightstatusserviceGetflightinfomodel();
		client.execute();
		log.info("call completed");
	} 

	

}
