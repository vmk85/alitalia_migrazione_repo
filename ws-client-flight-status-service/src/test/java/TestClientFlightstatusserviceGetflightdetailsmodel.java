import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.booking.flightstatusservice.FlightStatusServiceClient;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetFlightDetailsModelRequest;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetFlightDetailsModelResponse;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.FlightDetailsModel;

public class TestClientFlightstatusserviceGetflightdetailsmodel {
	
	private FlightStatusServiceClient flightStatusServiceClient;

	private static final Logger log = LoggerFactory.getLogger(TestClientFlightstatusserviceGetflightdetailsmodel.class);
	
	public TestClientFlightstatusserviceGetflightdetailsmodel() throws Exception {
		try {
			flightStatusServiceClient = new FlightStatusServiceClient("http://romcs0944.user.alitalia.local:9000/WcfFlightStatus/Alitalia.Portal.WebFrontend.LCWP.FlightStatus.Services.Lib.FlightStatusService.svc");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void execute() {
		try {
			DatatypeFactory dtf = DatatypeFactory.newInstance();
			new com.alitalia.aem.ws.booking.flightstatusservice.xsd.ObjectFactory();
			new com.alitalia.aem.ws.booking.flightstatusservice.xsd1.ObjectFactory();
			com.alitalia.aem.ws.booking.flightstatusservice.xsd2.ObjectFactory factory2 = new com.alitalia.aem.ws.booking.flightstatusservice.xsd2.ObjectFactory();
			com.alitalia.aem.ws.booking.flightstatusservice.xsd3.ObjectFactory factory3 = new com.alitalia.aem.ws.booking.flightstatusservice.xsd3.ObjectFactory();
			new com.alitalia.aem.ws.booking.flightstatusservice.xsd4.ObjectFactory();

			XMLGregorianCalendar flightDate = dtf.newXMLGregorianCalendar(2015, 7, 8, 00, 00, 00, 00, DatatypeConstants.FIELD_UNDEFINED);
			FlightDetailsModel flightDetailsModel = factory3.createFlightDetailsModel();
			flightDetailsModel.setFlightDate(flightDate);
			flightDetailsModel.setFlightNumber(factory3.createFlightDetailsModelFlightNumber("1779"));
			flightDetailsModel.setVector(factory3.createFlightDetailsModelVector("AZ"));

			GetFlightDetailsModelRequest request = factory2.createGetFlightDetailsModelRequest();
			request.setFlightDetails(factory2.createGetFlightDetailsModelRequestFlightDetails(flightDetailsModel));

			@SuppressWarnings("unused")
			GetFlightDetailsModelResponse flightDetailsModel2 = flightStatusServiceClient.getFlightDetailsModel(request);
	
			log.info("call ok");
		} catch (Exception e) {
			log.info("error");
			e.printStackTrace();
		}
	}
	
	public static void main(String arg []) throws Exception {
		log.info("calling..");
		TestClientFlightstatusserviceGetflightdetailsmodel client = new TestClientFlightstatusserviceGetflightdetailsmodel();
		client.execute();
		log.info("call completed");
	} 

	

}
