package com.alitalia.aem.ws.booking.flightstatusservice;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.security.KeyStore;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;
import javax.xml.namespace.QName;
import javax.xml.ws.Binding;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.booking.flightstatusservice.wsdl.FlightStatusService;
import com.alitalia.aem.ws.booking.flightstatusservice.wsdl.IFlightStatusService;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetAirportInfoModelRequest;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetAirportInfoModelResponse;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetFlightDetailsModelRequest;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetFlightDetailsModelResponse;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetFlightInfoModelRequest;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetFlightInfoModelResponse;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetIteneraryListModelFullRequest;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetIteneraryListModelFullResponse;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetItineraryListModelRequest;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetItineraryListModelResponse;
import com.alitalia.aem.ws.common.exception.WSClientException;
import com.alitalia.aem.ws.common.logginghandler.SOAPLoggingHandler;
import com.alitalia.aem.ws.common.messagehandler.SOAPMessageHandler;
import com.alitalia.aem.ws.common.utils.WSClientConstants;

@Service(value = FlightStatusServiceClient.class)
@Component(immediate = true, metatype = true)
@Properties({
	@Property(name = "service.vendor", value = "Reply")
})
public class FlightStatusServiceClient {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Property(description = "URL of the 'Static Data Web Service WSDL' used to invoke the Web Service.")
	private static final String WS_ENDPOINT_PROPERTY = "ws.endpoint";

	@Property(description = "Flag used to invoke Web Service in HTTPS.")
	private static final String WS_HTTPS_MODE_PROPERTY = "ws.https_mode";

	@Property(description = "Flag used to load identity store.")
	private static final String WS_LOAD_IDENTITY_STORE_PROPERTY = "ws.load_identity_store";

	@Property(description = "Flag used to enable hostname verifier.")
	private static final String WS_HOSTNAME_VERIFIER_PROPERTY = "ws.hostname_verifier";

	@Property(description = "Property used to set identity store path.")
	private static final String WS_IDENTITY_STORE_PATH_PROPERTY = "ws.identity_store_path";

	@Property(description = "Property used to set identity store password.")
	private static final String WS_IDENTITY_STORE_PASSWORD_PROPERTY = "ws.identity_store_password";
	
	@Property(description = "Property used to set identity key password.")
	private static final String WS_IDENTITY_KEY_PASSWORD_PROPERTY = "ws.identity_key_password";

	@Property(description = "Property used to set trust store path.")
	private static final String WS_TRUST_STORE_PATH_PROPERTY = "ws.trust_store_path";

	@Property(description = "Property used to set trust store password.")
	private static final String WS_TRUST_STORE_PASSWORD_PROPERTY = "ws.trust_store_password";

	@Property(description = "Property used to set connection timout")
	private static final String WS_CONNECTION_TIMEOUT_PROPERTY = "ws.connection_timeout";

	@Property(description = "Property used to set request timout")
	private static final String WS_REQUEST_TIMEOUT_PROPERTY = "ws.request_timeout";

	@Property(description = "Property used to set request timout")
	private static final String WS_SSL_PROTOCL = "ws.ssl_protocol";
	
	@Property(description = "Property used to print the request")
	private static final String WS_PRINT_REQUET = "ws.print_request";
	
	@Property(description = "Property used to print the response")
	private static final String WS_PRINT_RESPONSE = "ws.print_response";
	
	private String serviceEndpoint;
	private String serviceQNameNamespace;
	private String serviceQNameLocalPart;
	private Integer connectionTimeout;
	private Integer requestTimeout;
	private String identityStorePath;
	private String identityStorePassword;
	private String identityKeyPassword;
	private String trustStorePath;
	private String trustStorePassword;
	private String sslProtocol;
	private boolean hostnameVerifier;
	private boolean loadIdentityStore;
	private boolean httpsMode;
	private boolean printRequest;
	private boolean printResponse;
	private boolean initializationRequired = false;

	private FlightStatusService flightStatusService;
	private IFlightStatusService iFlightStatusService;
	
	public FlightStatusServiceClient() throws WSClientException {
		this.serviceQNameLocalPart = "FlightStatusService";
		this.serviceQNameNamespace = "http://tempuri.org/"; 		
		setDefault();
	}

	public FlightStatusServiceClient(String serviceEndpoint) throws WSClientException {
		this.serviceQNameLocalPart = "FlightStatusService";
		this.serviceQNameNamespace = "http://tempuri.org/"; 		
		setDefault();

		this.serviceEndpoint = serviceEndpoint;
		
		try {
			init();
		} catch(WSClientException e){
		  initializationRequired = true;
		}
	}

	public FlightStatusServiceClient(String serviceEndpoint, Integer connectionTimeout, Integer requestTimeout) throws WSClientException {
		this.serviceQNameLocalPart = "FlightStatusService";
		this.serviceQNameNamespace = "http://tempuri.org/"; 	
		setDefault();

		this.serviceEndpoint = serviceEndpoint;
		this.connectionTimeout = connectionTimeout;
		this.requestTimeout = requestTimeout;
		
		try {
			init();
		} catch(WSClientException e){
		  initializationRequired = true;
		}
	}

	public FlightStatusServiceClient(String serviceEndpoint, Integer connectionTimeout, Integer requestTimeout, Boolean httpsMode,
			Boolean hostnameVerifier, Boolean loadIdentityStore, String identityStorePath, String identityStorePassword,
			String identityKeyPassword, String trustStorePath, String trustStorePassword, String sslProtocol) throws WSClientException {
		this.serviceEndpoint = serviceEndpoint;
		this.connectionTimeout = connectionTimeout;
		this.requestTimeout = requestTimeout;
		this.httpsMode = httpsMode;
		this.hostnameVerifier = hostnameVerifier;
		this.loadIdentityStore = loadIdentityStore;
		this.identityStorePath = identityStorePath;
		this.identityStorePassword = identityStorePassword;
		this.identityKeyPassword = identityKeyPassword;
		this.trustStorePath = trustStorePath;
		this.trustStorePassword = trustStorePassword;
		this.sslProtocol = sslProtocol;

		this.serviceQNameLocalPart = "FlightStatusService";
		this.serviceQNameNamespace = "http://tempuri.org/"; 	
		
		try {
			init();
		} catch(WSClientException e){
		  initializationRequired = true;
		}
	}

	private void init() {
		FileInputStream fisTruststore = null;
		FileInputStream fisIdentitystore = null;
		try {
			URL classURL = FlightStatusServiceClient.class.getClassLoader().getResource(this.serviceEndpoint);
			QName serviceQName = new QName(serviceQNameNamespace, serviceQNameLocalPart);

			flightStatusService = new FlightStatusService(classURL, serviceQName);
			iFlightStatusService = flightStatusService.getBasicHttpEndPoin1();

			BindingProvider bindingProviderNetwork = (BindingProvider) iFlightStatusService;
			Binding bindingNetwork = bindingProviderNetwork.getBinding();

			@SuppressWarnings("rawtypes")
			List<Handler> handlerListNetwork = bindingNetwork.getHandlerChain();
			SOAPLoggingHandler soapLoggingHandlerNetwork = new SOAPLoggingHandler(this.printRequest, this.printResponse, true);
			SOAPMessageHandler soapMessageHandler = new SOAPMessageHandler(true);
			
			handlerListNetwork.add(soapLoggingHandlerNetwork);
			handlerListNetwork.add(soapMessageHandler);
			
			bindingNetwork.setHandlerChain(handlerListNetwork);

			logger.debug("Https mode [" + httpsMode + "]");

			if (httpsMode) {
				logger.debug("Loaded identity store path [" + identityStorePath + "], trust store path [" + trustStorePath + "], " 
					+ "hostname verifier [" + hostnameVerifier + "] and loading identity store flag [" + loadIdentityStore + "]");

				SSLContext sslContext = SSLContext.getInstance(sslProtocol);

				TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
				KeyStore trustKeystore = KeyStore.getInstance(KeyStore.getDefaultType());
				fisTruststore = new FileInputStream(trustStorePath);
				trustKeystore.load(fisTruststore, trustStorePassword.toCharArray());
				tmf.init(trustKeystore);

				if (loadIdentityStore) {
					KeyStore identityKeystore = KeyStore.getInstance(WSClientConstants.keystoreTypeDefault);
					fisIdentitystore = new FileInputStream(identityStorePath);
					identityKeystore.load(fisIdentitystore, identityStorePassword.toCharArray());

					KeyManagerFactory kmf = KeyManagerFactory.getInstance(WSClientConstants.keymanagerFactoryAlgorithmDefault);
					kmf.init(identityKeystore, identityKeyPassword.toCharArray());
					sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

					logger.debug("Loaded identity and trust store");
				} else {
					sslContext.init(null, tmf.getTrustManagers(), null);
					logger.debug("Loaded trust store only");
				}

				SSLContext.setDefault(sslContext);
				bindingProviderNetwork.getRequestContext().put(WSClientConstants.JAXWS_SSL_SOCKET_FACTORY_PROPERTY,
						sslContext.getSocketFactory());

				if(!hostnameVerifier) {
					bindingProviderNetwork.getRequestContext().put(WSClientConstants.JAXWS_HOSTNAME_VERIFIER_PROPERTY, new HostnameVerifier() {

						@Override
						public boolean verify(String arg0, SSLSession arg1) {					
							return true;
						}

					});				
				}
			}

			((BindingProvider) iFlightStatusService).getRequestContext().put(
					WSClientConstants.BINDING_PROVIDER_CONNECT_TIMEOUT_PROPERTY, connectionTimeout);
			((BindingProvider) iFlightStatusService).getRequestContext().put(
					WSClientConstants.BINDING_PROVIDER_REQUEST_TIMEOUT_PROPERTY, requestTimeout);

			logger.debug("Set up request timeout [" +
					((BindingProvider) iFlightStatusService).getRequestContext()
					.get(WSClientConstants.BINDING_PROVIDER_REQUEST_TIMEOUT_PROPERTY) + "] and " + "connection timeout [" +
					((BindingProvider) iFlightStatusService).getRequestContext()
					.get(WSClientConstants.BINDING_PROVIDER_CONNECT_TIMEOUT_PROPERTY) + "]");
		} catch (Exception e) {
			logger.error("Non-blocking error: FlightStatusServiceClient Inizialization Failed.", e);
			throw new WSClientException("Error to initialize FlightStatusServiceClient", e);
		} finally {
			if (fisIdentitystore != null) {
				try {
					fisIdentitystore.close();
				} catch (IOException e) {
					logger.error("Error to close input stream", e);
				}
			}
			
			if (fisTruststore != null) {
				try {
					fisTruststore.close();
				} catch (IOException e) {
					logger.error("Error to close input stream", e);
				}
			}
		}
	}


	@Activate
	protected void activate(final Map<String, Object> config) {
		String serviceEndpoint = null;
		boolean httpsMode;
		boolean hostnameVerifier;
		boolean loadIdentityStore;
		boolean printRequest;
		boolean printResponse;
		String identityStorePath = null;
		String identityStorePassword = null;
		String identityKeyPassword = null;
		String trustStorePath = null;
		String trustStorePassword = null;
		Integer connectionTimeout = null;
		Integer requestTimeout = null;
		String sslProtocol = null;

		try {			
			httpsMode = Boolean.valueOf(String.valueOf(config.get(WS_HTTPS_MODE_PROPERTY)));
			this.httpsMode = httpsMode;
			
			hostnameVerifier = Boolean.valueOf(String.valueOf(config.get(WS_HOSTNAME_VERIFIER_PROPERTY)));
			this.hostnameVerifier = hostnameVerifier;
			
			loadIdentityStore = Boolean.valueOf(String.valueOf(config.get(WS_LOAD_IDENTITY_STORE_PROPERTY)));
			this.loadIdentityStore = loadIdentityStore;
			
			identityStorePath = String.valueOf(config.get(WS_IDENTITY_STORE_PATH_PROPERTY));
			this.identityStorePath = identityStorePath;
			
			identityStorePassword = String.valueOf(config.get(WS_IDENTITY_STORE_PASSWORD_PROPERTY));
			this.identityStorePassword = identityStorePassword;
			
			identityKeyPassword = String.valueOf(config.get(WS_IDENTITY_KEY_PASSWORD_PROPERTY));
			this.identityKeyPassword = identityKeyPassword;
			
			trustStorePath = String.valueOf(config.get(WS_TRUST_STORE_PATH_PROPERTY));
			this.trustStorePath = trustStorePath;
			
			trustStorePassword = String.valueOf(config.get(WS_TRUST_STORE_PASSWORD_PROPERTY));
			this.trustStorePassword = trustStorePassword;
			
			connectionTimeout = Integer.valueOf(String.valueOf(config.get(WS_CONNECTION_TIMEOUT_PROPERTY)));
			this.connectionTimeout = connectionTimeout;	
			
			requestTimeout = Integer.valueOf(String.valueOf(config.get(WS_REQUEST_TIMEOUT_PROPERTY)));
			this.requestTimeout = requestTimeout;	
			
			sslProtocol = String.valueOf(config.get(WS_SSL_PROTOCL));
			this.sslProtocol = sslProtocol;	
			
			printRequest = Boolean.valueOf(String.valueOf(config.get(WS_PRINT_REQUET)));
			this.printRequest = printRequest;	
			
			printResponse = Boolean.valueOf(String.valueOf(config.get(WS_PRINT_RESPONSE)));
			this.printResponse = printResponse;			
		} catch (Exception e) {
			logger.error("Error to read custom configuration to activate FlightStatusServiceClient - default configuration used", e);
			setDefault();
		}
		
		try {			
			serviceEndpoint = String.valueOf(config.get(WS_ENDPOINT_PROPERTY));
			this.serviceEndpoint = serviceEndpoint;
			
			logger.info("Activated FlightStatusServiceClient: "
					+ "WS_ENDPOINT_PROPERTY=[" + this.serviceEndpoint + "], "
					+ "WS_HTTPS_MODE_PROPERTY=[" + this.httpsMode + "], "
					+ "WS_HOSTNAME_VERIFIER_PROPERTY=[" + this.hostnameVerifier +"], "
					+ "WS_LOAD_IDENTITY_STORE_PROPERTY=[" + this.loadIdentityStore + "], "
					+ "WS_IDENTITY_STORE_PATH_PROPERTY=[" + this.identityStorePath + "], "
					+ "WS_TRUST_STORE_PATH_PROPERTY=[" + this.trustStorePath + "], "
					+ "WS_CONNECTION_TIMEOUT_PROPERTY=[" + this.connectionTimeout + "], "
					+ "WS_REQUEST_TIMEOUT_PROPERTY=[" + this.requestTimeout + "], "
					+ "WS_SSL_PROTOCOL=[" + this.sslProtocol + "], "
					+ "WS_PRINT_RESPONSE=[" + this.printRequest + "], "
					+ "WS_PRINT_REQUET=[" + this.printResponse + "]");
		} catch (Exception e) {
			logger.error("Error to read WS_ENDPOINT_PROPERTY to activate FlightStatusServiceClient", e);
			throw new WSClientException("Error to initialize FlightStatusServiceClient", e);
		}
		
		try {
			init();
		} catch(WSClientException e){
		  initializationRequired = true;
		}
	}

	@Modified
	protected void modified(final Map<String, Object> config) {
		String serviceEndpoint = null;
		boolean httpsMode;
		boolean hostnameVerifier;
		boolean loadIdentityStore;
		boolean printRequest;
		boolean printResponse;
		String identityStorePath = null;
		String identityStorePassword = null;
		String identityKeyPassword = null;
		String trustStorePath = null;
		String trustStorePassword = null;
		Integer connectionTimeout = null;
		Integer requestTimeout = null;
		String sslProtocol = null;

		try {			
			httpsMode = Boolean.valueOf(String.valueOf(config.get(WS_HTTPS_MODE_PROPERTY)));
			this.httpsMode = httpsMode;
			
			hostnameVerifier = Boolean.valueOf(String.valueOf(config.get(WS_HOSTNAME_VERIFIER_PROPERTY)));
			this.hostnameVerifier = hostnameVerifier;
			
			loadIdentityStore = Boolean.valueOf(String.valueOf(config.get(WS_LOAD_IDENTITY_STORE_PROPERTY)));
			this.loadIdentityStore = loadIdentityStore;
			
			identityStorePath = String.valueOf(config.get(WS_IDENTITY_STORE_PATH_PROPERTY));
			this.identityStorePath = identityStorePath;
			
			identityStorePassword = String.valueOf(config.get(WS_IDENTITY_STORE_PASSWORD_PROPERTY));
			this.identityStorePassword = identityStorePassword;
			
			identityKeyPassword = String.valueOf(config.get(WS_IDENTITY_KEY_PASSWORD_PROPERTY));
			this.identityKeyPassword = identityKeyPassword;
			
			trustStorePath = String.valueOf(config.get(WS_TRUST_STORE_PATH_PROPERTY));
			this.trustStorePath = trustStorePath;
			
			trustStorePassword = String.valueOf(config.get(WS_TRUST_STORE_PASSWORD_PROPERTY));
			this.trustStorePassword = trustStorePassword;
			
			connectionTimeout = Integer.valueOf(String.valueOf(config.get(WS_CONNECTION_TIMEOUT_PROPERTY)));
			this.connectionTimeout = connectionTimeout;	
			
			requestTimeout = Integer.valueOf(String.valueOf(config.get(WS_REQUEST_TIMEOUT_PROPERTY)));
			this.requestTimeout = requestTimeout;	
			
			sslProtocol = String.valueOf(config.get(WS_SSL_PROTOCL));
			this.sslProtocol = sslProtocol;	
			
			printRequest = Boolean.valueOf(String.valueOf(config.get(WS_PRINT_REQUET)));
			this.printRequest = printRequest;	
			
			printResponse = Boolean.valueOf(String.valueOf(config.get(WS_PRINT_RESPONSE)));
			this.printResponse = printResponse;			
		} catch (Exception e) {
			logger.error("Error to read custom configuration to modify FlightStatusServiceClient - default configuration used", e);
			setDefault();
		}
		
		try {			
			serviceEndpoint = String.valueOf(config.get(WS_ENDPOINT_PROPERTY));
			this.serviceEndpoint = serviceEndpoint;
			
			logger.info("Modified FlightStatusServiceClient: "
					+ "WS_ENDPOINT_PROPERTY=[" + this.serviceEndpoint + "], "
					+ "WS_HTTPS_MODE_PROPERTY=[" + this.httpsMode + "], "
					+ "WS_HOSTNAME_VERIFIER_PROPERTY=[" + this.hostnameVerifier +"], "
					+ "WS_LOAD_IDENTITY_STORE_PROPERTY=[" + this.loadIdentityStore + "], "
					+ "WS_IDENTITY_STORE_PATH_PROPERTY=[" + this.identityStorePath + "], "
					+ "WS_TRUST_STORE_PATH_PROPERTY=[" + this.trustStorePath + "], "
					+ "WS_CONNECTION_TIMEOUT_PROPERTY=[" + this.connectionTimeout + "], "
					+ "WS_REQUEST_TIMEOUT_PROPERTY=[" + this.requestTimeout + "], "
					+ "WS_SSL_PROTOCOL=[" + this.sslProtocol + "], "
					+ "WS_PRINT_RESPONSE=[" + this.printRequest + "], "
					+ "WS_PRINT_REQUET=[" + this.printResponse + "]");
		} catch (Exception e) {
			logger.error("Error to read WS_ENDPOINT_PROPERTY to modify FlightStatusServiceClient", e);
			throw new WSClientException("Error to initialize FlightStatusServiceClient", e);
		}

		flightStatusService = null;
		try {
			init();
		} catch(WSClientException e){
		  initializationRequired = true;
		}
	}
	
	private void setDefault() {
		this.httpsMode = WSClientConstants.httpsModeDefault;
		this.hostnameVerifier = WSClientConstants.hostnameVerifierDefault;
		this.loadIdentityStore = WSClientConstants.loadIdentityStoreDefault;
		this.connectionTimeout = WSClientConstants.connectionTimeoutDefault;
		this.requestTimeout = WSClientConstants.requestTimeoutDefault;
		this.sslProtocol = WSClientConstants.sslProtocolDefault;
		this.printRequest = WSClientConstants.printRequest;
		this.printResponse = WSClientConstants.printResponse;
	}

	
	public GetFlightDetailsModelResponse getFlightDetailsModel(GetFlightDetailsModelRequest request) {
		if (initializationRequired){
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-FlightStatusService Operation-getFlightDetailsModel...");
			long before = System.currentTimeMillis();
			GetFlightDetailsModelResponse response = iFlightStatusService.getFlightDetailsModel(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-FlightStatusService Operation-getFlightDetailsModel...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-FlightStatusService Operation-getFlightDetailsModel", e);
		}
	}
	
	public GetFlightInfoModelResponse getFlightInfoModel(GetFlightInfoModelRequest request) {
		if (initializationRequired){
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-FlightStatusService Operation-getFlightInfoModel...");
			long before = System.currentTimeMillis();
			GetFlightInfoModelResponse response = iFlightStatusService.getFlightInfoModel(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-FlightStatusService Operation-getFlightInfoModel...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-FlightStatusService Operation-getFlightInfoModel", e);
		}
	}

	public GetItineraryListModelResponse getItineraryListModel(GetItineraryListModelRequest request) {
		if (initializationRequired){
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-FlightStatusService Operation-getItineraryListModel...");
			long before = System.currentTimeMillis();
			GetItineraryListModelResponse response = iFlightStatusService.getItineraryListModel(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-FlightStatusService Operation-getItineraryListModel...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-FlightStatusService Operation-getItineraryListModel", e);
		}
	}

	public GetIteneraryListModelFullResponse getIteneraryListModelFull(GetIteneraryListModelFullRequest request) {
		if (initializationRequired){
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-FlightStatusService Operation-getIteneraryListModelFull...");
			long before = System.currentTimeMillis();
			GetIteneraryListModelFullResponse response = iFlightStatusService.getIteneraryListModelFull(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-FlightStatusService Operation-getIteneraryListModelFull...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-FlightStatusService Operation-getIteneraryListModelFull", e);
		}
	}	

	public GetAirportInfoModelResponse getAirportInfoModel(GetAirportInfoModelRequest request) {
		if (initializationRequired){
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-FlightStatusService Operation-getAirportInfoModel...");
			long before = System.currentTimeMillis();
			GetAirportInfoModelResponse response = iFlightStatusService.getAirportInfoModel(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-FlightStatusService Operation-getAirportInfoModel...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-FlightStatusService Operation-getAirportInfoModel", e);
		}
	}

	public Integer getConnectionTimeout() {
		return connectionTimeout;
	}

	public Integer getRequestTimeout() {
		return requestTimeout;
	}

	public String getIdentityStorePath() {
		return identityStorePath;
	}

	public String getIdentityStorePassword() {
		return identityStorePassword;
	}

	public String getTrustStorePath() {
		return trustStorePath;
	}

	public String getTrustStorePassword() {
		return trustStorePassword;
	}

	public boolean getHostnameVerifier() {
		return hostnameVerifier;
	}

	public boolean getLoadIdentityStore() {
		return loadIdentityStore;
	}

	public boolean getHttpsMode() {
		return httpsMode;
	}

	public String getSslProtocol() {
		return sslProtocol;
	}
	
	public String getIdentityKeyPassword() {
		return identityKeyPassword;
	}
	
	public boolean isPrintRequest() {
		return printRequest;
	}

	public boolean isPrintResponse() {
		return printResponse;
	}	
	
}