package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.CrossSellingsResponse;
import com.alitalia.aem.common.messages.home.MmbCrossSellingsRequest;
import com.alitalia.aem.common.messages.home.SapInvoiceRequest;
import com.alitalia.aem.common.messages.home.SapInvoiceResponse;

public interface MmbTicketService {

	SapInvoiceResponse callSAPService(SapInvoiceRequest request);
	CrossSellingsResponse getCrossSellings(MmbCrossSellingsRequest request);

}