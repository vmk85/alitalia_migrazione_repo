package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.CarnetBuyCarnetRequest;
import com.alitalia.aem.common.messages.home.CarnetBuyCarnetResponse;
import com.alitalia.aem.common.messages.home.CarnetSendEmailRequest;
import com.alitalia.aem.common.messages.home.CarnetSendEmailResponse;


public interface CarnetPaymentService {

	CarnetBuyCarnetResponse buyCarnet(CarnetBuyCarnetRequest request);
	CarnetSendEmailResponse sendEmail(CarnetSendEmailRequest request);

}
