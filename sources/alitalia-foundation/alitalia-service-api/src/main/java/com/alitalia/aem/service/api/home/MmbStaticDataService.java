package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.RetrieveCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCountriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveCreditCardCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCreditCardCountriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveFrequentFlayerRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAirportCheckinRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAirportCheckinResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbAmericanStatesRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAmericanStatesResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbFrequentFlyerCarriersResponse;
import com.alitalia.aem.common.messages.home.RetrieveMealsRequest;
import com.alitalia.aem.common.messages.home.RetrieveMealsResponse;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemRequest;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemResponse;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixRequest;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixResponse;
import com.alitalia.aem.common.messages.home.RetrieveProvincesRequest;
import com.alitalia.aem.common.messages.home.RetrieveProvincesResponse;

public interface MmbStaticDataService {
	
	RetrieveProvincesResponse retrieveProvinces(RetrieveProvincesRequest request);
	RetrieveCreditCardCountriesResponse retrieveCreditCardCountries(RetrieveCreditCardCountriesRequest request);
	RetrieveCountriesResponse retrieveCountries(RetrieveCountriesRequest request);
	RetrievePhonePrefixResponse retrievePhonePrefix(RetrievePhonePrefixRequest request);
	RetrievePaymentTypeItemResponse retrievePaymentTypeItem(RetrievePaymentTypeItemRequest request);
	RetrieveMealsResponse retrieveMeals(RetrieveMealsRequest request);
	RetrieveMmbFrequentFlyerCarriersResponse retrieveFrequentFlyers(RetrieveFrequentFlayerRequest request);
	RetrieveMmbAirportCheckinResponse retrieveAirportCheckIn(RetrieveMmbAirportCheckinRequest request);
	RetrieveMmbAmericanStatesResponse retrieveAmericanStates(RetrieveMmbAmericanStatesRequest request);

}