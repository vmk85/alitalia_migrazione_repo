package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.common.messages.home.RetrieveCouncilsRequest;
import com.alitalia.aem.common.messages.home.RetrieveCouncilsResponse;
import com.alitalia.aem.common.messages.home.RetrieveCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCountriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveCreditCardCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCreditCardCountriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveFrequentFlayerRequest;
import com.alitalia.aem.common.messages.home.RetrieveFrequentFlyersResponse;
import com.alitalia.aem.common.messages.home.RetrieveMealsRequest;
import com.alitalia.aem.common.messages.home.RetrieveMealsResponse;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemRequest;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemResponse;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixRequest;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixResponse;
import com.alitalia.aem.common.messages.home.RetrieveProvincesRequest;
import com.alitalia.aem.common.messages.home.RetrieveProvincesResponse;
import com.alitalia.aem.common.messages.home.RetrieveSeatTypeRequest;
import com.alitalia.aem.common.messages.home.RetrieveSeatTypeResponse;
import com.alitalia.aem.common.messages.home.RetrieveTicketOfficeRequest;
import com.alitalia.aem.common.messages.home.RetrieveTicketOfficeResponse;
//import com.alitalia.aem.common.messages.home.RetrieveFrequentFlyersResponse;
//import com.alitalia.aem.common.messages.home.RetrieveFrequentFlayerRequest;
import com.alitalia.aem.common.messages.home.SearchFlightsRequest;
import com.alitalia.aem.common.messages.home.SearchFlightsResponse;

public interface StaticDataService {
	
	RetrieveAirportsResponse retrieveAirports(RetrieveAirportsRequest request);
	SearchFlightsResponse searchFlights(SearchFlightsRequest request); 
	RetrieveCountriesResponse retrieveCountries(RetrieveCountriesRequest request);
	RetrieveCreditCardCountriesResponse retrieveCreditCardCountries(RetrieveCreditCardCountriesRequest request);
	RetrieveFrequentFlyersResponse retrieveFrequentFlyers(RetrieveFrequentFlayerRequest request);
	RetrieveMealsResponse retrieveMeals(RetrieveMealsRequest request);
	RetrievePaymentTypeItemResponse retrievePaymentTypeItem(RetrievePaymentTypeItemRequest request);
	RetrievePhonePrefixResponse retrievePhonePrefix(RetrievePhonePrefixRequest request);
	RetrieveSeatTypeResponse retrieveSeatTypes(RetrieveSeatTypeRequest request);
	RetrieveTicketOfficeResponse retrieveTicketsOffice(RetrieveTicketOfficeRequest request);
	RetrieveCouncilsResponse retrieveCouncils(RetrieveCouncilsRequest request);
	RetrieveProvincesResponse retrieveProvinces(RetrieveProvincesRequest request);
}