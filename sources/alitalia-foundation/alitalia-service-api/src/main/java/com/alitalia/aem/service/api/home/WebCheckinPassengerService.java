package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.CanAddPassengerCheckinRequest;
import com.alitalia.aem.common.messages.home.CanAddPassengerCheckinResponse;
import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesRequest;
import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesResponse;
import com.alitalia.aem.common.messages.home.PassengerListCheckinRequest;
import com.alitalia.aem.common.messages.home.PassengerListCheckinResponse;
import com.alitalia.aem.common.messages.home.WebCheckinNewPassengerRequest;
import com.alitalia.aem.common.messages.home.WebCheckinNewPassengerResponse;


public interface WebCheckinPassengerService {

	PassengerListCheckinResponse getPassengerList(PassengerListCheckinRequest request);
	CanAddPassengerCheckinResponse canAddPassenger(CanAddPassengerCheckinRequest request);
	WebCheckinNewPassengerResponse getNewPassenger(WebCheckinNewPassengerRequest request);
	MmbCheckFrequentFlyerCodesResponse checkFFCode(MmbCheckFrequentFlyerCodesRequest request);
}
