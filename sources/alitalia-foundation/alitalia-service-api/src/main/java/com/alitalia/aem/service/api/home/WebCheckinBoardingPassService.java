package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.BoardingPassCheckinRequest;
import com.alitalia.aem.common.messages.home.BoardingPassCheckinResponse;
import com.alitalia.aem.common.messages.home.SendBoardingPassCheckinRequest;
import com.alitalia.aem.common.messages.home.SendBoardingPassCheckinResponse;
import com.alitalia.aem.common.messages.home.SendReminderCheckinRequest;
import com.alitalia.aem.common.messages.home.SendReminderCheckinResponse;


public interface WebCheckinBoardingPassService {

	BoardingPassCheckinResponse getBoardingPass(BoardingPassCheckinRequest request);
	SendBoardingPassCheckinResponse sendBoardingPass(SendBoardingPassCheckinRequest request);
	SendReminderCheckinResponse sendReminder(SendReminderCheckinRequest request);

}
