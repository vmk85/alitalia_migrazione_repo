package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.MmbAncillaryAddToCartRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryAddToCartResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutEndRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutEndResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutInitRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutInitResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutStatusRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutStatusResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryRemoveFromCartRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryRemoveFromCartResponse;
import com.alitalia.aem.common.messages.home.MmbAncillarySendEmailRequest;
import com.alitalia.aem.common.messages.home.MmbAncillarySendEmailResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryUpdateCartRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryUpdateCartResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCartRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCartResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCatalogRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCatalogResponse;

public interface MmbAncillaryService {

	RetrieveMmbAncillaryCatalogResponse getCatalog(RetrieveMmbAncillaryCatalogRequest request);
	RetrieveMmbAncillaryCartResponse getCart(RetrieveMmbAncillaryCartRequest request);
	MmbAncillaryAddToCartResponse addToCart(MmbAncillaryAddToCartRequest request);
	MmbAncillaryUpdateCartResponse updateCart(MmbAncillaryUpdateCartRequest request);
	MmbAncillaryRemoveFromCartResponse removeFromCart(MmbAncillaryRemoveFromCartRequest request);
	MmbAncillaryCheckOutInitResponse checkOutInit(MmbAncillaryCheckOutInitRequest request);
	MmbAncillaryCheckOutEndResponse checkOutEnd(MmbAncillaryCheckOutEndRequest request);
	MmbAncillaryCheckOutStatusResponse getCheckOutStatus(MmbAncillaryCheckOutStatusRequest request);
	MmbAncillarySendEmailResponse sendEmail(MmbAncillarySendEmailRequest request);

}
