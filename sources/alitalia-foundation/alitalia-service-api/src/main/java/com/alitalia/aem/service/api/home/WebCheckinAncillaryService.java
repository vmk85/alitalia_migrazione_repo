package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutInitRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutInitResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutStatusRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutStatusResponse;
import com.alitalia.aem.common.messages.home.WebCheckinAddToCartRequest;
import com.alitalia.aem.common.messages.home.WebCheckinAncillaryCartResponse;
import com.alitalia.aem.common.messages.home.WebCheckinAncillaryResponse;
import com.alitalia.aem.common.messages.home.WebCheckinCancelCartRequest;
import com.alitalia.aem.common.messages.home.WebCheckinCancelCartResponse;
import com.alitalia.aem.common.messages.home.WebCheckinGetCartRequest;
import com.alitalia.aem.common.messages.home.WebCheckinGetCatalogRequest;
import com.alitalia.aem.common.messages.home.WebCheckinRemoveFromCartRequest;
import com.alitalia.aem.common.messages.home.WebCheckinUpdateCartRequest;

public interface WebCheckinAncillaryService {

	WebCheckinAncillaryCartResponse getCatalog(WebCheckinGetCatalogRequest request);
	WebCheckinAncillaryResponse addToCart(WebCheckinAddToCartRequest request);
	WebCheckinAncillaryResponse updateCart(WebCheckinUpdateCartRequest request);
	WebCheckinAncillaryResponse removeFromCart(WebCheckinRemoveFromCartRequest request);
	WebCheckinAncillaryCartResponse getCart(WebCheckinGetCartRequest request);
	MmbAncillaryCheckOutInitResponse checkOutInit(MmbAncillaryCheckOutInitRequest request);
	MmbAncillaryCheckOutStatusResponse getCheckOutStatus(MmbAncillaryCheckOutStatusRequest request);
	WebCheckinCancelCartResponse cancelCart(WebCheckinCancelCartRequest request);
}
