package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.RetrieveStateListRequest;
import com.alitalia.aem.common.messages.home.RetrieveStateListResponse;

public interface StaticDataMillemigliaService {
	
	RetrieveStateListResponse retrieveStatesList(RetrieveStateListRequest request);
}