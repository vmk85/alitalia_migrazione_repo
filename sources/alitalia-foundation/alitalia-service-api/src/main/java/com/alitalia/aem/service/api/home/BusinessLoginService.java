package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.AgencyInsertConsentRequest;
import com.alitalia.aem.common.messages.home.AgencyInsertConsentResponse;
import com.alitalia.aem.common.messages.home.AgencyLoginRequest;
import com.alitalia.aem.common.messages.home.AgencyLoginResponse;
import com.alitalia.aem.common.messages.home.AgencyPasswordChangeRequest;
import com.alitalia.aem.common.messages.home.AgencyPasswordChangeResponse;
import com.alitalia.aem.common.messages.home.AgencyRetrieveDataRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrieveDataResponse;
import com.alitalia.aem.common.messages.home.AgencyRetrievePasswordByIDRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrievePasswordByVATRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrievePasswordResponse;
import com.alitalia.aem.common.messages.home.AgencySendMailRequest;
import com.alitalia.aem.common.messages.home.AgencySendMailResponse;
import com.alitalia.aem.common.messages.home.AgencySubscriptionRequest;
import com.alitalia.aem.common.messages.home.AgencySubscriptionResponse;
import com.alitalia.aem.common.messages.home.AgencyUpdateDataRequest;
import com.alitalia.aem.common.messages.home.AgencyUpdateDataResponse;

public interface BusinessLoginService {

	AgencyLoginResponse agencyLogin(AgencyLoginRequest request);
	AgencyPasswordChangeResponse changePassword(AgencyPasswordChangeRequest request);
	AgencyRetrievePasswordResponse retrievePasswordByID(AgencyRetrievePasswordByIDRequest request);
	AgencyRetrievePasswordResponse retrievePasswordByVAT(AgencyRetrievePasswordByVATRequest request);
	AgencyRetrieveDataResponse retrieveAgencyData(AgencyRetrieveDataRequest request);
	AgencyUpdateDataResponse updateAgencyData(AgencyUpdateDataRequest request);
	AgencySubscriptionResponse subscribeAgency(AgencySubscriptionRequest request);
	AgencySendMailResponse sendMail(AgencySendMailRequest request);
	AgencyInsertConsentResponse insertConsent(AgencyInsertConsentRequest request);
}
