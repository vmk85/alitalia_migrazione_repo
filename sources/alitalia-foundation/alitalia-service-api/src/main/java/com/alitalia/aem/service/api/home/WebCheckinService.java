package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.WebCheckinRequest;
import com.alitalia.aem.common.messages.home.WebCheckinResponse;
import com.alitalia.aem.common.messages.home.WebCheckinUndoCheckinRequest;
import com.alitalia.aem.common.messages.home.WebCheckinUndoCheckinResponse;
import com.alitalia.aem.common.messages.home.WebCheckinUndoManyCheckinRequest;
import com.alitalia.aem.common.messages.home.WebCheckinUndoManyCheckinResponse;

public interface WebCheckinService {

	WebCheckinResponse checkIn(WebCheckinRequest request);
	WebCheckinUndoCheckinResponse undoCheckin(WebCheckinUndoCheckinRequest request);
	WebCheckinUndoManyCheckinResponse undoManyCheckin(WebCheckinUndoManyCheckinRequest request);
	
}
