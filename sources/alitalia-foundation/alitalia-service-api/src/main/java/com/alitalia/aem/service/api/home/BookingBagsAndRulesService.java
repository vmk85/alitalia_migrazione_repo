package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.BagsAndRuleRequest;
import com.alitalia.aem.common.messages.home.BagsAndRuleResponse;


public interface BookingBagsAndRulesService {

	BagsAndRuleResponse execute(BagsAndRuleRequest request);
}