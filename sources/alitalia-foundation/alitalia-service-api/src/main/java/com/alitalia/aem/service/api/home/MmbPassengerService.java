package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesRequest;
import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesResponse;

public interface MmbPassengerService {

	MmbCheckFrequentFlyerCodesResponse checkFrequentFlyerCodes(MmbCheckFrequentFlyerCodesRequest request);

}
