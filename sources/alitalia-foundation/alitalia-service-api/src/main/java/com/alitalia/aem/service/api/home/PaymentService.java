package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.AuthorizePaymentRequest;
import com.alitalia.aem.common.messages.home.AuthorizePaymentResponse;
import com.alitalia.aem.common.messages.home.CheckPaymentRequest;
import com.alitalia.aem.common.messages.home.CheckPaymentResponse;
import com.alitalia.aem.common.messages.home.InitializePaymentRequest;
import com.alitalia.aem.common.messages.home.InitializePaymentResponse;
import com.alitalia.aem.common.messages.home.RetrieveTicketRequest;
import com.alitalia.aem.common.messages.home.RetrieveTicketResponse;

public interface PaymentService {
	
	public abstract InitializePaymentResponse initializePayment(InitializePaymentRequest request);
	public abstract AuthorizePaymentResponse authorizePayment(AuthorizePaymentRequest request);
	public abstract CheckPaymentResponse checkPayment(CheckPaymentRequest request);
	public abstract RetrieveTicketResponse retrieveTicket(RetrieveTicketRequest request);
}
