package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.CarnetGetListRequest;
import com.alitalia.aem.common.messages.home.CarnetGetListResponse;
import com.alitalia.aem.common.messages.home.CarnetLoginRequest;
import com.alitalia.aem.common.messages.home.CarnetLoginResponse;
import com.alitalia.aem.common.messages.home.CarnetRecoveryCodeRequest;
import com.alitalia.aem.common.messages.home.CarnetRecoveryCodeResponse;
import com.alitalia.aem.common.messages.home.CarnetUpdateRequest;
import com.alitalia.aem.common.messages.home.CarnetUpdateResponse;


public interface CarnetService {

	CarnetGetListResponse getCarnetList(CarnetGetListRequest request);
	CarnetLoginResponse loginCarnet(CarnetLoginRequest request);
	CarnetRecoveryCodeResponse recoveryCarnetCode(CarnetRecoveryCodeRequest request);
	CarnetUpdateResponse updateCarnet(CarnetUpdateRequest request);

}
