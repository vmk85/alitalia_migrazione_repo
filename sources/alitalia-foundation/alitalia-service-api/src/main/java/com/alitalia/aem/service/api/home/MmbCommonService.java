package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.RetrieveMmbFlightSeatMapRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbFlightSeatMapResponse;

public interface MmbCommonService {

	RetrieveMmbFlightSeatMapResponse getMmbFlightSeatsMap(RetrieveMmbFlightSeatMapRequest request);

}
