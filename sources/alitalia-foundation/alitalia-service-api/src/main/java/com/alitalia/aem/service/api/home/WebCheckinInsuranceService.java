package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.CheckinInsuranceResponse;
import com.alitalia.aem.common.messages.home.WebCheckinGetInsuranceRequest;
import com.alitalia.aem.common.messages.home.WebCheckinPayInsuranceRequest;

public interface WebCheckinInsuranceService {

	CheckinInsuranceResponse getInsuranceInfo(WebCheckinGetInsuranceRequest request);
	CheckinInsuranceResponse payInsurance(WebCheckinPayInsuranceRequest request);
}
