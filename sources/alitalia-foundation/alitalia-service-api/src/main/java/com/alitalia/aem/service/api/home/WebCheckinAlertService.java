package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.WebCheckinAlertRequest;
import com.alitalia.aem.common.messages.home.WebCheckinAlertResponse;

public interface WebCheckinAlertService {

	WebCheckinAlertResponse submitUser(WebCheckinAlertRequest request);
	
}
