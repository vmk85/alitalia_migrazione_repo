package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.RetrieveGeoAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoAirportsResponse;
import com.alitalia.aem.common.messages.home.RetrieveGeoCitiesRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoCitiesResponse;
import com.alitalia.aem.common.messages.home.RetrieveGeoCountryRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoCountryResponse;
import com.alitalia.aem.common.messages.home.RetrieveGeoOffersRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoOffersResponse;
import com.alitalia.aem.common.messages.home.RetrieveGeoNearestAirportByCoordsRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoNearestAirportByCoordsResponse;

public interface GeoSpatialService {

	RetrieveGeoOffersResponse retrieveGeoOffers(RetrieveGeoOffersRequest request);
	RetrieveGeoCitiesResponse retrieveGeoCities(RetrieveGeoCitiesRequest request);
	RetrieveGeoOffersResponse retrieveGeoOffersByTarget(RetrieveGeoOffersRequest request);
	RetrieveGeoCitiesResponse retrieveGeoCitiesByTarget(RetrieveGeoCitiesRequest request);
	RetrieveGeoAirportsResponse retrieveGeoAirports(RetrieveGeoAirportsRequest request);
	RetrieveGeoCountryResponse retrieveGeoCountry(RetrieveGeoCountryRequest request);
	RetrieveGeoNearestAirportByCoordsResponse retrieveGeoNearestAirportByCoords(RetrieveGeoNearestAirportByCoordsRequest request);
	
}