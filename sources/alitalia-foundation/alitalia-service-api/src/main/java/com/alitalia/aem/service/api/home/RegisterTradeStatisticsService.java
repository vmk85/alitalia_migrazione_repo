package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.RegisterStatisticsResponse;
import com.alitalia.aem.common.messages.home.TradeRegisterStatisticRequest;

public interface RegisterTradeStatisticsService {

	RegisterStatisticsResponse registerStatistics(TradeRegisterStatisticRequest request);
}