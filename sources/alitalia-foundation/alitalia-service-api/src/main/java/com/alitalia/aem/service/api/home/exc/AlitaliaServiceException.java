package com.alitalia.aem.service.api.home.exc;

public class AlitaliaServiceException extends RuntimeException {

	private static final long serialVersionUID = 2378302295487918165L;

	public AlitaliaServiceException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AlitaliaServiceException(String arg0, Throwable arg1, boolean arg2,
			boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

	public AlitaliaServiceException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public AlitaliaServiceException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public AlitaliaServiceException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}
	
	

}
