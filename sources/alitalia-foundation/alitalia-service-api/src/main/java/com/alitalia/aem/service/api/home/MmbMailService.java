package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.MmbSendEmailRequest;
import com.alitalia.aem.common.messages.home.MmbSendEmailResponse;

public interface MmbMailService {

	MmbSendEmailResponse sendMail(MmbSendEmailRequest request);

}
