package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.RetrievePnrInformationCheckinResponse;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationRequest;
import com.alitalia.aem.common.messages.home.ValidateMmUserCheckinRequest;
import com.alitalia.aem.common.messages.home.ValidateMmUserCheckinResponse;

public interface WebCheckinSearchService {

	RetrievePnrInformationCheckinResponse retrievePnrInformation(RetriveMmbPnrInformationRequest request);
	ValidateMmUserCheckinResponse validateUser(ValidateMmUserCheckinRequest request);

}
