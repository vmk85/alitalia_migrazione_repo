package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.BaseResponse;
import com.alitalia.aem.common.messages.home.ChangeCabinCheckinRequest;
import com.alitalia.aem.common.messages.home.ChangeCabinCheckinResponse;
import com.alitalia.aem.common.messages.home.ComfortSeatOrderCheckinRequest;
import com.alitalia.aem.common.messages.home.ComfortSeatOrderUpdateCheckinRequest;
import com.alitalia.aem.common.messages.home.ComfortSeatPriceCheckinRequest;
import com.alitalia.aem.common.messages.home.ComfortSeatPriceCheckinResponse;
import com.alitalia.aem.common.messages.home.ComfortSeatPurchaseCheckinRequest;
import com.alitalia.aem.common.messages.home.ComfortSeatPurchaseCheckinResponse;
import com.alitalia.aem.common.messages.home.GenericServiceResponse;
import com.alitalia.aem.common.messages.home.UpgradePolicyCheckinRequest;
import com.alitalia.aem.common.messages.home.UpgradePolicyCheckinResponse;


public interface WebCheckinComfortSeatService {

	 ComfortSeatPurchaseCheckinResponse selectComfortSeatPurchase(ComfortSeatPurchaseCheckinRequest request);
	 ComfortSeatPriceCheckinResponse getComfortSeatPrice(ComfortSeatPriceCheckinRequest request);
	 GenericServiceResponse existsComfortSeatPurchaseIntoBO(ComfortSeatOrderCheckinRequest request);
	 BaseResponse updateComfortSeatPurchaseIntoBO(ComfortSeatOrderUpdateCheckinRequest request);
	 ChangeCabinCheckinResponse changeCabin(ChangeCabinCheckinRequest request);
	 UpgradePolicyCheckinResponse getUpgradePolicy(UpgradePolicyCheckinRequest request);

}
