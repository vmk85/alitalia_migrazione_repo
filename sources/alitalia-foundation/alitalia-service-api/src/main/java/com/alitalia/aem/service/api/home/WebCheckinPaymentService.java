package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.GenericServiceResponse;
import com.alitalia.aem.common.messages.home.PaymentCheckinRequest;
import com.alitalia.aem.common.messages.home.PaymentCheckinResponse;
import com.alitalia.aem.common.messages.home.SendReceiptCheckinRequest;


public interface WebCheckinPaymentService {

	PaymentCheckinResponse executePayment(PaymentCheckinRequest request);
	GenericServiceResponse sendEmailReceipt(SendReceiptCheckinRequest request);

}
