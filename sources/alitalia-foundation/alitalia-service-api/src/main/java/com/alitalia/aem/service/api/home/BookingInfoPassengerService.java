package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.InfoPassengerCheckFrequentFlyersCodeRequest;
import com.alitalia.aem.common.messages.home.InfoPassengerCheckFrequentFlyersCodeResponse;
import com.alitalia.aem.common.messages.home.InfoPassengerCreatePNRRequest;
import com.alitalia.aem.common.messages.home.InfoPassengerCreatePNRResponse;
import com.alitalia.aem.common.messages.home.InfoPassengerSubmitPassengerRequest;
import com.alitalia.aem.common.messages.home.InfoPassengerSubmitPassengerResponse;

public interface BookingInfoPassengerService {

	InfoPassengerCreatePNRResponse executeCreatePNR(InfoPassengerCreatePNRRequest request);
	InfoPassengerCheckFrequentFlyersCodeResponse executeCheckFrequentFlyersCode(InfoPassengerCheckFrequentFlyersCodeRequest request);
	InfoPassengerSubmitPassengerResponse submitPassengerDetails(InfoPassengerSubmitPassengerRequest request);
}