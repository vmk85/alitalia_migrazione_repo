package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionDisplayFareTotalData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionDisplayFareTotal;

@Component(immediate=true, metatype=false)
@Service(value=RTDSDisplayFareTotalToRTDSDisplayFareTotalData.class)
public class RTDSDisplayFareTotalToRTDSDisplayFareTotalData implements
		Converter<ResultTicketingDetailSolutionDisplayFareTotal, ResultTicketingDetailSolutionDisplayFareTotalData> {

	@Override
	public ResultTicketingDetailSolutionDisplayFareTotalData convert(ResultTicketingDetailSolutionDisplayFareTotal source) {
		ResultTicketingDetailSolutionDisplayFareTotalData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionDisplayFareTotalData();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
