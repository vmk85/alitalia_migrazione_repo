package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.FFCardData;
import com.alitalia.aem.common.data.home.enumerations.FFCardStatusEnum;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.searchservice.xsd3.FFCard;

@Component(immediate=true, metatype=false)
@Service(value=FFCardToFFCardData.class)
public class FFCardToFFCardData 
		implements Converter<FFCard, FFCardData> {
	
	
	@Override
	public FFCardData convert(FFCard source) {
		
		FFCardData destination = null;
		
		if (source != null) {
			
			destination = new FFCardData();
			
			if (source.getCode() != null) {
				destination.setCode(
							source.getCode().getValue());
			}
			
			destination.setEndDate(
					XsdConvertUtils.parseCalendar(
							source.getEndDate()));
			
			if (source.getOther() != null) {
				destination.setOther(
							source.getOther().getValue());
			}
			
			destination.setStartDate(
					XsdConvertUtils.parseCalendar(
							source.getStartDate()));
			
			if (source.getStatus() != null) {
				destination.setStatus(
						FFCardStatusEnum.fromValue(
								source.getStatus().value()));
			}
			
			if (source.getText() != null) {
				destination.setText(
						source.getText().getValue());
			}
			
			if (source.getVIPCode() != null) {
				destination.setVipCode(
						source.getVIPCode().getValue());
			}
			
		}
		
		return destination;
	}

}
