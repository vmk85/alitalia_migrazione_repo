package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.BaggageAllowanceData;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.BrandPenaltiesData;
import com.alitalia.aem.ws.booking.ticketservice.xsd2.ArrayOfanyType;
import com.alitalia.aem.ws.booking.ticketservice.xsd2.ArrayOfstring;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.BaggageAllowance;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.Brand;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ObjectFactory;

@Component(immediate = true, metatype = false)
@Service(value = BrandDataToBrand.class)
public class BrandDataToBrand implements Converter<BrandData, Brand> {

	@Reference
	private BaggageAllowanceDataToBaggageAllowance baggageAllowanceDataConverter;

	@Reference
	private BrandPenaltiesDataToBrandPenalties brandPenaltiesDataConverter;

	@Override
	public Brand convert(BrandData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		com.alitalia.aem.ws.booking.ticketservice.xsd2.ObjectFactory objectFactory2 = new com.alitalia.aem.ws.booking.ticketservice.xsd2.ObjectFactory();
		Brand destination = objectFactory.createBrand();

		destination.setAdvancePurchaseDays(source.getAdvancePurchaseDays());
		destination.setBestFare(source.isBestFare());
		destination.setCode(objectFactory.createBrandCode(source.getCode()));
		destination.setCompartimentalClass(objectFactory.createBrandCompartimentalClass(source.getCompartimentalClass()));
		destination.setCurrency(objectFactory.createBrandCurrency(source.getCurrency()));
		destination.setEnabled(source.isEnabled());
		destination.setGrossFare(source.getGrossFare());
		destination.setIndex(source.getIndex());
		destination.setNetFare(source.getNetFare());
		destination.setRealPrice(source.getRealPrice());
		destination.setRefreshSolutionId(objectFactory.createABoomBoxRefreshInfoRefreshSolutionId(source
				.getRefreshSolutionId()));
		destination.setSeatsAvailable(source.getSeatsAvailable());
		destination.setSelected(source.isSelected());

		List<BaggageAllowanceData> sourceBaggageAllowance = source.getBaggageAllowanceList();
		ArrayOfanyType baggageAllowanceList = objectFactory2.createArrayOfanyType();
		if (sourceBaggageAllowance != null) {
			for (BaggageAllowanceData baggageAllowanceData : sourceBaggageAllowance) {
				BaggageAllowance baggageAllowance = baggageAllowanceDataConverter.convert(baggageAllowanceData);
				baggageAllowanceList.getAnyType().add(baggageAllowance);
			}
		}
		destination.setBaggageAllowanceList(objectFactory.createBrandBaggageAllowanceList(baggageAllowanceList));

		List<String> sourceNotes = source.getNotes();
		ArrayOfstring notes = objectFactory2.createArrayOfstring();
		if (sourceNotes != null) {
			for (String noteSource : sourceNotes)
				notes.getString().add(noteSource);
		}
		destination.setNotes(objectFactory.createBrandNotes(notes));

		BrandPenaltiesData penalties = source.getPenalties();
		if (penalties == null)
			destination.setPenalties(null);
		else
			destination.setPenalties(objectFactory.createBrandPenalties(brandPenaltiesDataConverter.convert(penalties)));

		destination.setProperties(null);

		return destination;
	}
	
}