package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingReissueInfoData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetailsSolutionPricingReissueInfo;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingReissueInfoDataToPricingReissueInfo.class)
public class RBDSPricingReissueInfoDataToPricingReissueInfo implements
		Converter<ResultBookingDetailsSolutionPricingReissueInfoData, ResultBookingDetailsSolutionPricingReissueInfo> {

	@Override
	public ResultBookingDetailsSolutionPricingReissueInfo convert(
			ResultBookingDetailsSolutionPricingReissueInfoData source) {
		ResultBookingDetailsSolutionPricingReissueInfo destination = null;
		if(source != null){
			ObjectFactory objf = new ObjectFactory();
			destination = objf.createResultBookingDetailsSolutionPricingReissueInfo();
			destination.setReissueActionField(source.getReissueActionField());
		}
		return destination;
	}

}
