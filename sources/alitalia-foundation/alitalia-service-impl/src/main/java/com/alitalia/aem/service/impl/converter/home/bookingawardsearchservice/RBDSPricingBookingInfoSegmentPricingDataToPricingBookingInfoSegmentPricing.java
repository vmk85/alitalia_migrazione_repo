package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatchData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ArrayOfresultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatch;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ArrayOfresultBookingDetailsSolutionPricingBookingInfoSegmentPricingFare;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricing;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingBookingInfoSegmentPricingDataToPricingBookingInfoSegmentPricing.class)
public class RBDSPricingBookingInfoSegmentPricingDataToPricingBookingInfoSegmentPricing
		implements Converter<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData, ResultBookingDetailsSolutionPricingBookingInfoSegmentPricing> {

	@Reference
	RBDSPricingBookingInfoSegmentPricingBaggageMatchDataToPricingBookingInfoSegmentPricingBaggageMatch rbdsPricingBookingInfoSegmentPricingBaggageMatchConverter;
	
	@Reference
	RBDSPricingBookingInfoSegmentPricingFareDataToPricingBookingInfoSegmentPricingFare rbdsPricingBookingInfoSegmentPricingFareConverter;
	
	@Override
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentPricing convert(
			ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData source) {
		ResultBookingDetailsSolutionPricingBookingInfoSegmentPricing destination = null;
		
		if(source != null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionPricingBookingInfoSegmentPricing();
			
			ArrayOfresultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatch arraydest_1 = objfact.createArrayOfresultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatch();
			if(source.getExtField()!=null)
			for(ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatchData s : source.getExtField()){
				arraydest_1.getResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatch().add(rbdsPricingBookingInfoSegmentPricingBaggageMatchConverter.convert(s));
			}
			destination.setExtField(arraydest_1);
			
			ArrayOfresultBookingDetailsSolutionPricingBookingInfoSegmentPricingFare arraydest_2 = objfact.createArrayOfresultBookingDetailsSolutionPricingBookingInfoSegmentPricingFare();
			if(source.getFareField()!=null)
			for(ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData s : source.getFareField()){
				arraydest_2.getResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFare().add(rbdsPricingBookingInfoSegmentPricingFareConverter.convert(s));
			}
			destination.setFareField(arraydest_2);			
		}
		return destination;
	}

}
