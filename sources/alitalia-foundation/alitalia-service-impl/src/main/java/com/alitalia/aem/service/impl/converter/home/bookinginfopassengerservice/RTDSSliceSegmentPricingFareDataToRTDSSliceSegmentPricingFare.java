package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentPricingFareData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionSliceSegmentPricingFare;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentPricingFareDataToRTDSSliceSegmentPricingFare.class)
public class RTDSSliceSegmentPricingFareDataToRTDSSliceSegmentPricingFare implements
		Converter<ResultTicketingDetailSolutionSliceSegmentPricingFareData, 
					ResultTicketingDetailSolutionSliceSegmentPricingFare> {

	@Override
	public ResultTicketingDetailSolutionSliceSegmentPricingFare convert(
			ResultTicketingDetailSolutionSliceSegmentPricingFareData source) {
		ResultTicketingDetailSolutionSliceSegmentPricingFare destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionSliceSegmentPricingFare();

			destination.setCarrierField(source.getCarrierField());
			destination.setCity1Field(source.getCity1Field());
			destination.setCity2Field(source.getCity2Field());
			destination.setExtendedFareCodeField(source.getExtendedFareCodeField());
		}

		return destination;
	}

}
