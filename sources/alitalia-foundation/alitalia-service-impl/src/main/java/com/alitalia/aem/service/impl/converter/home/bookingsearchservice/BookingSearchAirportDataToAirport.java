package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import java.util.Calendar;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.searchservice.xsd3.Airport;
import com.alitalia.aem.ws.booking.searchservice.xsd3.AreaValue;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=BookingSearchAirportDataToAirport.class)
public class BookingSearchAirportDataToAirport implements Converter<AirportData, Airport> {

	private Logger logger = LoggerFactory.getLogger(SearchDestinationDataToSearchDestination.class);

	@Override
	public Airport convert(AirportData source) {
		ObjectFactory factory = new ObjectFactory();
		Airport destination = factory.createAirport();

		destination.setAZDeserves(source.getAzDeserves());

		AreaValueEnum area = source.getArea();
		if (area != null)
			destination.setArea(AreaValue.valueOf(area.value()));

		destination.setCity(factory.createAirportCity(source.getCity()));
		destination.setCityCode(factory.createAirportCityCode(source.getCityCode()));
		destination.setCityUrl(factory.createAirportCityUrl(source.getCityUrl()));
		destination.setCode(factory.createAirportCode(source.getCode()));
		destination.setCountry(factory.createAirportCountry(source.getCountry()));
		destination.setCountryCode(factory.createAirportCountryCode(source.getCountryCode()));
		destination.setDescription(factory.createAirportDescription(source.getDescription()));
//		destination.setDetails(factory.createAirportDetails(null));
		destination.setEligibility(source.getEligibility());

		Calendar etktAvlblFrom = source.getEtktAvlblFrom();
		if (etktAvlblFrom != null) 
			destination.setEtktAvlblFrom(factory.createAirportEtktAvlblFrom(XsdConvertUtils.toXMLGregorianCalendar(etktAvlblFrom)));

		destination.setHiddenInFirstDeparture(source.getHiddenInFirstDeparture());
		destination.setIdMsg(source.getIdMsg());
		destination.setName(factory.createAirportName(source.getName()));
		destination.setState(factory.createAirportState(source.getState()));
		destination.setStateCode(factory.createAirportStateCode(source.getStateCode()));

		Calendar unavlblFrom = source.getUnavlblFrom();
		if (unavlblFrom != null) 
			destination.setUnavlblFrom(factory.createAirportUnavlblFrom(XsdConvertUtils.toXMLGregorianCalendar(unavlblFrom)));

		Calendar unavlblUntil = source.getUnavlblUntil();
		if (unavlblUntil != null) 
			destination.setUnavlblUntil(factory.createAirportUnavlblUntil(XsdConvertUtils.toXMLGregorianCalendar(unavlblUntil)));

		return destination;
	}

}
