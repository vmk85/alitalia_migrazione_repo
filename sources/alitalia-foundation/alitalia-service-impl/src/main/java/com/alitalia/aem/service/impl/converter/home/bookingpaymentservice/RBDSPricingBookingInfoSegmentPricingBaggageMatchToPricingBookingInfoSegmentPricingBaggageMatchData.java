//2
package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatchData;
import com.alitalia.aem.ws.booking.paymentservice.xsd11.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatch;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingBookingInfoSegmentPricingBaggageMatchToPricingBookingInfoSegmentPricingBaggageMatchData.class)
public class RBDSPricingBookingInfoSegmentPricingBaggageMatchToPricingBookingInfoSegmentPricingBaggageMatchData
		implements Converter<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatch, ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatchData> {

	@Override
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatchData convert(
			ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatch source) {
		ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatchData destination = null;
		
		if(source != null){
			destination =new ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatchData();
			destination.setCommercialNameField(source.getCommercialNameField());
			destination.setCountField(source.getCountField());
			destination.setCountFieldSpecified(source.isCountFieldSpecified());
			destination.setSubcodeField(source.getSubcodeField());
		}
		return destination;
	}

}
