package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.MmbAncillaryErrorData;
import com.alitalia.aem.common.messages.home.WebCheckinAncillaryCartResponse;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.GetCatalogResponse;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.Cart;


@Component(immediate=true, metatype=false)
@Service(value=GetCatalogResponseToWebCheckinAncillaryCartResponse.class)
public class GetCatalogResponseToWebCheckinAncillaryCartResponse implements
		Converter<GetCatalogResponse, WebCheckinAncillaryCartResponse> {

	@Reference
	private CartToCheckinAncillaryCartData cartConverter;

	@Reference
	private ErrorToMmbAncillaryErrorData errorConverter;

	@Override
	public WebCheckinAncillaryCartResponse convert(GetCatalogResponse source) {
		WebCheckinAncillaryCartResponse destination = null;

		if (source != null) {
			destination = new WebCheckinAncillaryCartResponse();

			JAXBElement<String> sourceMachineName = source.getMachineName();
			if (sourceMachineName != null)
				destination.setMachine(sourceMachineName.getValue());

			JAXBElement<Cart> sourceCart = source.getCart();
			if (sourceCart != null)
				destination.setCart(cartConverter.convert(sourceCart.getValue()));

			if (source.getErrors() != null &&
					source.getErrors().getValue() != null &&
					source.getErrors().getValue().getAnyType() != null &&
					!source.getErrors().getValue().getAnyType().isEmpty()) {
				List<Object> sourceErrors = source.getErrors().getValue().getAnyType();
				List<MmbAncillaryErrorData> errors = new ArrayList<MmbAncillaryErrorData>();
				for (Object sourceError : sourceErrors) {
					errors.add(errorConverter.convert(sourceError));
				}
				destination.setErrors(errors);
			}
		}

		return destination;
	}

}
