package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareAdjustedPriceData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionPricingFareAdjustedPrice;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareAdjustedPriceToRTDSPricingFareAdjustedPriceData.class)
public class RTDSPricingFareAdjustedPriceToRTDSPricingFareAdjustedPriceData implements
		Converter<ResultTicketingDetailSolutionPricingFareAdjustedPrice, 
					ResultTicketingDetailSolutionPricingFareAdjustedPriceData> {

	@Override
	public ResultTicketingDetailSolutionPricingFareAdjustedPriceData convert(
			ResultTicketingDetailSolutionPricingFareAdjustedPrice source) {
		ResultTicketingDetailSolutionPricingFareAdjustedPriceData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingFareAdjustedPriceData();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
