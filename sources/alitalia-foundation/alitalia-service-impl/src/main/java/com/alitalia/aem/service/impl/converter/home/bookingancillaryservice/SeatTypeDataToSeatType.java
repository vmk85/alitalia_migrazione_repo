package com.alitalia.aem.service.impl.converter.home.bookingancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.SeatTypeData;
import com.alitalia.aem.ws.booking.ancillaryservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.booking.ancillaryservice.xsd2.SeatType;

@Component(immediate=true, metatype=false)
@Service(value=SeatTypeDataToSeatType.class)
public class SeatTypeDataToSeatType 
		implements Converter<SeatTypeData, SeatType> {
	
	@Override
	public SeatType convert(SeatTypeData source) {
		
		SeatType destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			destination = objectFactory.createSeatType();
			
			destination.setCode(
					objectFactory.createSeatTypeCode(
							source.getCode()));
			
			destination.setDescription(
					objectFactory.createSeatTypeDescription(
							source.getDescription()));
			
		}
		
		return destination;
	}

}
