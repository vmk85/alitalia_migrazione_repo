package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingExtData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingExtEndorsementData;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ArrayOfresultBookingDetailsSolutionPricingExtEndorsement;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ResultBookingDetailsSolutionPricingExt;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingExtDataToPricingExt.class)
public class RBDSPricingExtDataToPricingExt implements Converter<ResultBookingDetailsSolutionPricingExtData, ResultBookingDetailsSolutionPricingExt> {

	@Reference
	RBDSPricingExtEndorsementDataToPricingExtEndorsement endorsementFieldConverter;
	
	@Reference
	RBDSPricingExtPaxDataToPricingExtPax paxFieldConverter;
	
	@Override
	public ResultBookingDetailsSolutionPricingExt convert(
			ResultBookingDetailsSolutionPricingExtData source) {
		ResultBookingDetailsSolutionPricingExt destination = null;
		if(source != null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionPricingExt();
			ArrayOfresultBookingDetailsSolutionPricingExtEndorsement array = objfact.createArrayOfresultBookingDetailsSolutionPricingExtEndorsement();
			if(source.getEndorsementField()!=null)
			for(ResultBookingDetailsSolutionPricingExtEndorsementData s : source.getEndorsementField()){
				array.getResultBookingDetailsSolutionPricingExtEndorsement().add(endorsementFieldConverter.convert(s));
			}
			
			destination.setEndorsementField(array);
			destination.setPaxField(paxFieldConverter.convert(source.getPaxField()));
		}
		return destination;
	}

}
