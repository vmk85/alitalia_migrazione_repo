package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.InfoPassengerCreatePNRRequest;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd1.CreatePNRRequest;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd1.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=InfoPassengerCreatePNRRequestToCreatePNRRequest.class)
public class InfoPassengerCreatePNRRequestToCreatePNRRequest 
		implements Converter<InfoPassengerCreatePNRRequest, CreatePNRRequest> {

	@Reference
	private RoutesDataToRoutes routesDataToRoutesConverter;
	
	@Override
	public CreatePNRRequest convert(InfoPassengerCreatePNRRequest source) {
		
		CreatePNRRequest destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
	
			destination = objectFactory.createCreatePNRRequest();
			
			destination.setCUG(
					objectFactory.createCreatePNRRequestCUG(
							source.getCug()));
			
			destination.setCarnetCode(
					objectFactory.createCreatePNRRequestCarnetCode(
							source.getCarnetCode()));
			
			destination.setMarketCode(
					objectFactory.createCreatePNRRequestMarketCode(
							source.getMarketCode()));
			
			destination.setPrenotation(
					objectFactory.createCreatePNRRequestPrenotation(
							routesDataToRoutesConverter.convert(
									source.getPrenotation())));
			
			destination.setCookie(objectFactory.createCreatePNRRequestCookie(source.getCookie()));
			destination.setExecution(objectFactory.createCreatePNRRequestExecution(source.getExecution()));
			destination.setSabreGateWayAuthToken(objectFactory.createCreatePNRRequestSabreGateWayAuthToken(source.getSabreGateWayAuthToken()));
			
		}
		
		return destination;
	}

}
