package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ArcoQueueData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ArcoQueue;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=ArcoQueueDataToArcoQueue.class)
public class ArcoQueueDataToArcoQueue 
		implements Converter<ArcoQueueData, ArcoQueue> {
	
	@Override
	public ArcoQueue convert(ArcoQueueData source) {
		
		ArcoQueue destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			destination = objectFactory.createArcoQueue();
			
			destination.setPseudoCityCode(
					objectFactory.createArcoQueuePseudoCityCode(
						source.getPseudoCityCode()));
			
			destination.setQueueNumber(
					source.getQueueNumber());
			
		}
		
		return destination;
	}

}
