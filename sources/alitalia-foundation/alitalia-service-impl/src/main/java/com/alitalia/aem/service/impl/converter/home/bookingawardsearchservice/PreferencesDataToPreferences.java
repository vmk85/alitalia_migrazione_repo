package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PreferencesData;
import com.alitalia.aem.common.data.home.SeatPreferencesData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.ObjectFactory;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd6.ArrayOfanyType;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.Preferences;

@Component(immediate=true, metatype=false)
@Service(value=PreferencesDataToPreferences.class)
public class PreferencesDataToPreferences 
		implements Converter<PreferencesData, Preferences> {
	
	@Reference
	private SeatPreferencesDataToSeatPreference seatPreferencesDataToSeatPreferencesConverter;
	
	@Reference
	private MealDataToMeal mealDataToMealConverter;
	
	@Reference
	private SeatTypeDataToSeatType seatTypeDataToSeatTypeConverter;
	
	@Override
	public Preferences convert(PreferencesData source) {
		
		Preferences destination = null;
		
		if (source != null) {
			
			ObjectFactory factory3 = new ObjectFactory();
			com.alitalia.aem.ws.bookingaward.searchservice.xsd6.ObjectFactory factory6 = 
					new com.alitalia.aem.ws.bookingaward.searchservice.xsd6.ObjectFactory();
			
			destination = factory3.createPreferences();
			
			ArrayOfanyType seatPreferences = factory6.createArrayOfanyType();
			if (source.getSeatPreferences() != null) {
				for (SeatPreferencesData seatPreferencesData : source.getSeatPreferences()) {
					seatPreferences.getAnyType().add(
							seatPreferencesDataToSeatPreferencesConverter.convert(
									seatPreferencesData));
				}
			}
			destination.setSeatPreferences(
					factory3.createPreferencesSeatPreferences(
							seatPreferences));
			
			destination.setMealType(
					factory3.createPreferencesMealType(
							mealDataToMealConverter.convert(
									source.getMealType())));
			
			destination.setSeatType(
					factory3.createPreferencesSeatType(
							seatTypeDataToSeatTypeConverter.convert(
									source.getSeatType())));
			
		}
		
		return destination;
	}

}
