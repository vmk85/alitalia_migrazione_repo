package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.messages.home.InfoPassengerCheckFrequentFlyersCodeRequest;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd1.CheckFrequentFlyersCodeRequest;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ArrayOfAdultPassenger;

@Component(immediate=true, metatype=false)
@Service(value=CheckFrequentFlyersCodeRequestConverter.class)
public class CheckFrequentFlyersCodeRequestConverter 
		implements Converter<InfoPassengerCheckFrequentFlyersCodeRequest, CheckFrequentFlyersCodeRequest> {

	@Reference
	private AdultPassengerDataToAdultPassenger adultPassengerDataToAdultPassengerConverter;
	
	@Override
	public CheckFrequentFlyersCodeRequest convert(InfoPassengerCheckFrequentFlyersCodeRequest source) {
		
		CheckFrequentFlyersCodeRequest destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ObjectFactory objectFactoryXs2 =
					new com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ObjectFactory();
	
			destination = objectFactory.createCheckFrequentFlyersCodeRequest();
			
			if (source.getAdultPassengers() != null) {
				ArrayOfAdultPassenger arrayOfAdultPassenger = objectFactoryXs2.createArrayOfAdultPassenger();
				for (AdultPassengerData adultPassengerData : source.getAdultPassengers()) {
					arrayOfAdultPassenger.getAdultPassenger().add(
							adultPassengerDataToAdultPassengerConverter.convert(
									adultPassengerData));
				}
				destination.setAdultPassengers(
						objectFactory.createCheckFrequentFlyersCodeRequestAdultPassengers(
								arrayOfAdultPassenger));
			}
			
		}
		
		return destination;
	}

}
