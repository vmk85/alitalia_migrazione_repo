package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareBookingInfoSegmentData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingFareBookingInfoSegment;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareBookingInfoSegmentToRTDSPricingFareBookingInfoSegmentData.class)
public class RTDSPricingFareBookingInfoSegmentToRTDSPricingFareBookingInfoSegmentData implements
		Converter<ResultTicketingDetailSolutionPricingFareBookingInfoSegment, 
					ResultTicketingDetailSolutionPricingFareBookingInfoSegmentData> {

	@Reference
	private RTDSPricingFareBookingInfoSegmentFlightToRTDSPricingFareBookingInfoSegmentFlightData flightFieldConverter;

	@Override
	public ResultTicketingDetailSolutionPricingFareBookingInfoSegmentData convert(
			ResultTicketingDetailSolutionPricingFareBookingInfoSegment source) {
		ResultTicketingDetailSolutionPricingFareBookingInfoSegmentData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingFareBookingInfoSegmentData();

			destination.setDepartureField(source.getDepartureField());
			destination.setDestinationField(source.getDestinationField());
			destination.setHashField(source.getHashField());
			destination.setOriginField(source.getOriginField());

			destination.setFlightField(
					flightFieldConverter.convert(source.getFlightField()));
		}

		return destination;
	}

}
