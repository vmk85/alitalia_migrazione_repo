package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingTaxSalePriceData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingTaxSalePrice;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingTaxSalePriceToRTDPricingTaxSalePriceData.class)
public class RTDSPricingTaxSalePriceToRTDPricingTaxSalePriceData implements
		Converter<ResultTicketingDetailSolutionPricingTaxSalePrice, 
					ResultTicketingDetailSolutionPricingTaxSalePriceData> {

	@Override
	public ResultTicketingDetailSolutionPricingTaxSalePriceData convert(
			ResultTicketingDetailSolutionPricingTaxSalePrice source) {
		ResultTicketingDetailSolutionPricingTaxSalePriceData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingTaxSalePriceData();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
