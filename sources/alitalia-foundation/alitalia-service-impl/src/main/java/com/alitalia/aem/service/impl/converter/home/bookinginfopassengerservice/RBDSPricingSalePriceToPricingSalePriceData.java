package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingSalePriceData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ResultBookingDetailsSolutionPricingSalePrice;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingSalePriceToPricingSalePriceData.class)
public class RBDSPricingSalePriceToPricingSalePriceData implements
		Converter<ResultBookingDetailsSolutionPricingSalePrice, ResultBookingDetailsSolutionPricingSalePriceData> {

	@Override
	public ResultBookingDetailsSolutionPricingSalePriceData convert(
			ResultBookingDetailsSolutionPricingSalePrice source) {
		ResultBookingDetailsSolutionPricingSalePriceData destination = null;
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingSalePriceData();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}

}
