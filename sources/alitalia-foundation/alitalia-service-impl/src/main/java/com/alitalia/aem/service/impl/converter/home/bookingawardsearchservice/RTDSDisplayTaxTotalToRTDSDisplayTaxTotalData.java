package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionDisplayTaxTotalData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionDisplayTaxTotal;

@Component(immediate=true, metatype=false)
@Service(value=RTDSDisplayTaxTotalToRTDSDisplayTaxTotalData.class)
public class RTDSDisplayTaxTotalToRTDSDisplayTaxTotalData implements
		Converter<ResultTicketingDetailSolutionDisplayTaxTotal, ResultTicketingDetailSolutionDisplayTaxTotalData> {

	@Override
	public ResultTicketingDetailSolutionDisplayTaxTotalData convert(ResultTicketingDetailSolutionDisplayTaxTotal source) {
		ResultTicketingDetailSolutionDisplayTaxTotalData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionDisplayTaxTotalData();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
