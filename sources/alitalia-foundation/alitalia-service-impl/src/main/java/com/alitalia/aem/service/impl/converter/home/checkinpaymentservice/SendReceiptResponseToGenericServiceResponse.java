package com.alitalia.aem.service.impl.converter.home.checkinpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.GenericServiceResponse;


@Component(immediate=true, metatype=false)
@Service(value=SendReceiptResponseToGenericServiceResponse.class)
public class SendReceiptResponseToGenericServiceResponse implements Converter<Boolean, GenericServiceResponse> {


	@Override
	public GenericServiceResponse convert(Boolean source) {
		GenericServiceResponse destination = null;

		if (source != null) {
			destination = new GenericServiceResponse();
			
			destination.setGenericResult(source);
		}

		return destination;
	}

}
