package com.alitalia.aem.service.impl.converter.home.checkincomfortseatservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.ComfortSeatOrderCheckinRequest;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.ComfortSeatOrderRequest;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.Resource;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.SearchReason;


@Component(immediate=true, metatype=false)
@Service(value=ComfortSeatOrderCheckinRequestToComfortSeatOrderRequest.class)
public class ComfortSeatOrderCheckinRequestToComfortSeatOrderRequest implements Converter<ComfortSeatOrderCheckinRequest, ComfortSeatOrderRequest> {

	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;
	

	@Override
	public ComfortSeatOrderRequest convert(ComfortSeatOrderCheckinRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		ComfortSeatOrderRequest destination = null;

		if (source != null) {
			destination = objectFactory.createComfortSeatOrderRequest();
			
			destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			destination.setX003CResourceX003EKBackingField(Resource.WEB);
				
			destination.setFlightDepartureDate(XsdConvertUtils.toXMLGregorianCalendar(source.getFlightDepartureDate()));			
			destination.setFlightVectorAndNumber(objectFactory.createComfortSeatOrderRequestFlightVectorAndNumber(source.getFlightVectorAndNumber()));
			if(source.getReason() != null){
				destination.setReason(SearchReason.fromValue(source.getReason().value()));
			}
			destination.setTicketNumber(objectFactory.createComfortSeatOrderRequestTicketNumber(source.getTicketNumber()));			
		}

		return destination;
	}


}
