package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceExtWarningData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionSliceExtWarning;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceExtWarningToRTDSSliceExtWarningData.class)
public class RTDSSliceExtWarningToRTDSSliceExtWarningData implements
		Converter<ResultTicketingDetailSolutionSliceExtWarning, ResultTicketingDetailSolutionSliceExtWarningData> {

	@Override
	public ResultTicketingDetailSolutionSliceExtWarningData convert(ResultTicketingDetailSolutionSliceExtWarning source) {
		ResultTicketingDetailSolutionSliceExtWarningData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionSliceExtWarningData();

			destination.setChangeOfAirportField(source.isChangeOfAirportField());
			destination.setChangeOfAirportFieldSpecified(source.isChangeOfAirportFieldSpecified());
			destination.setChangeOfTerminalField(source.isChangeOfTerminalField());
			destination.setChangeOfTerminalFieldSpecified(source.isChangeOfTerminalFieldSpecified());
			destination.setLongLayoverField(source.isLongLayoverField());
			destination.setLongLayoverFieldSpecified(source.isLongLayoverFieldSpecified());
			destination.setNonPreferredCabinField(source.isNonPreferredCabinField());
			destination.setNonPreferredCabinFieldSpecified(source.isNonPreferredCabinFieldSpecified());
			destination.setOvernightField(source.isOvernightField());
			destination.setOvernightFieldSpecified(source.isOvernightFieldSpecified());
			destination.setRiskyConnectionField(source.isRiskyConnectionField());
			destination.setRiskyConnectionFieldSpecified(source.isRiskyConnectionFieldSpecified());
			destination.setTightConnectionField(source.isTightConnectionField());
			destination.setTightConnectionFieldSpecified(source.isTightConnectionFieldSpecified());
		}

		return destination;
	}

}
