package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ApplicantPassengerData;
import com.alitalia.aem.common.data.home.ContactData;
import com.alitalia.aem.common.data.home.TicketInfoData;
import com.alitalia.aem.common.data.home.enumerations.ARTaxInfoTypesEnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ApplicantPassenger;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.Contact;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.InfoBase;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.Preferences;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.TicketInfo;

@Component(immediate=true, metatype=false)
@Service(value=ApplicantPassengerToApplicantPassengerData.class)
public class ApplicantPassengerToApplicantPassengerData 
		implements Converter<ApplicantPassenger, ApplicantPassengerData> {
	
	@Reference
	private InfoBaseToPassengerInfoBaseData infoBaseToPassengerBaseInfoDataConverter;
	
	@Reference
	private PreferencesToPreferencesData preferencesToPreferencesDataConverter;
	
	@Reference
	private FrequentFlyerTypeToFrequentFlyerTypeData frequentFlyerTypeToFrequentFlyerTypeDataConverter;
	
	@Reference
	private TicketInfoToTicketInfoData ticketInfoToTicketInfoDataConverter;
	
	@Reference
	private ContactToContactData contactToContactDataConverter;
	
	@Override
	public ApplicantPassengerData convert(ApplicantPassenger source) {
		
		ApplicantPassengerData destination = null;
		
		if (source != null) {
			
			destination = new ApplicantPassengerData();
			
			// ApplicantPassenger data
			
			if (source.getARTaxInfoType() != null) {
				destination.setArTaxInfoType(
							ARTaxInfoTypesEnum.fromValue(
									source.getARTaxInfoType().value()));
			}
			
			if (source.getBlueBizCode() != null) {
				destination.setBlueBizCode(
						source.getBlueBizCode().getValue());
			}
			
			if (source.getSkyBonusCode() != null) {
				destination.setSkyBonusCode(
							source.getSkyBonusCode().getValue());
			}
			
			if (source.getSubscribeToNewsletter() != null) {
				destination.setSubscribeToNewsletter(
							source.getSubscribeToNewsletter().getValue());
			}
			
			List<ContactData> contacts = new ArrayList<ContactData>();
			if (source.getContact() != null && source.getContact().getValue() != null) {
				for (Object contact : source.getContact().getValue().getAnyType()) {
					contacts.add(
							contactToContactDataConverter.convert(
									(Contact) contact));
				}
			}
			destination.setContact(contacts);
			
			if (source.getCountry() != null) {
				destination.setCountry(
						source.getCountry().getValue());
			}
			
			destination.setCouponPrice(
					source.getCouponPrice());
			
			if (source.getCUIT() != null) {
				destination.setCuit(
						source.getCUIT().getValue());
			}
			
			if (source.getEmail() != null) {
				destination.setEmail(
						source.getEmail().getValue());
			}
			
			destination.setExtraCharge(
					source.getExtraCharge());
			
			destination.setFee(
					source.getFee());
			
			// AdultPassenger data
			
			if (source.getFrequentFlyerCode() != null) {
				destination.setFrequentFlyerCode(
						source.getFrequentFlyerCode().getValue());
			}
			
			if (source.getFrequentFlyerTier() != null) {
				destination.setFrequentFlyerTier(
						source.getFrequentFlyerTier().getValue());
			}
			
			if (source.getFrequentFlyerType() != null) {
				destination.setFrequentFlyerType(
						frequentFlyerTypeToFrequentFlyerTypeDataConverter.convert(
								source.getFrequentFlyerType().getValue()));
			}
			
			// ARegularPassenger data
			
			if (source.getPreferences() != null) {
				destination.setPreferences(
						preferencesToPreferencesDataConverter.convert(
								(Preferences) source.getPreferences().getValue()));
			}
			
			// APassengerBase data
			
			if (source.getType() != null) {
				destination.setType(
						PassengerTypeEnum.fromValue(source.getType().value()));
			}
			
			if (source.getLastName() != null) {
				destination.setLastName(
						source.getLastName().getValue());
			}
			
			if (source.getName() != null) {
				destination.setName(
						source.getName().getValue());
			}
			
			destination.setCouponPrice(
					source.getCouponPrice());
			
			destination.setExtraCharge(
					source.getExtraCharge());
			
			destination.setFee(
					source.getFee());
			
			destination.setGrossFare(
					source.getGrossFare());
			
			destination.setNetFare(
					source.getNetFare());
			
			if (source.getInfo() != null) {
				destination.setInfo(
						infoBaseToPassengerBaseInfoDataConverter.convert(
								(InfoBase) source.getInfo().getValue()));
			}
			
			List<TicketInfoData> tickets = null;
			if (source.getTickets() != null && source.getTickets().getValue() != null) {
				tickets = new ArrayList<TicketInfoData>();
				for (Object ticketInfo : source.getTickets().getValue().getAnyType()) {
					tickets.add(
							ticketInfoToTicketInfoDataConverter.convert(
									(TicketInfo) ticketInfo));
				}
			}
			destination.setTickets(
					tickets);
		
		}
		
		return destination;
	}

}
