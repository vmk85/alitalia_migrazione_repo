package com.alitalia.aem.service.impl.converter.home.checkinextrabaggageservice;

import java.util.Calendar;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbFlightStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbLegTypeEnum;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.checkin.extrabaggageservice.xsd2.ArrayOfCompartimentalClass;
import com.alitalia.aem.ws.checkin.extrabaggageservice.xsd2.CompartimentalClass;
import com.alitalia.aem.ws.checkin.extrabaggageservice.xsd2.FlightStatus;
import com.alitalia.aem.ws.checkin.extrabaggageservice.xsd2.LegType;
import com.alitalia.aem.ws.checkin.extrabaggageservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.checkin.extrabaggageservice.xsd2.Flight;


@Component(immediate=true, metatype=false)
@Service(value=CheckinFlightDataToFlight.class)
public class CheckinFlightDataToFlight implements Converter<CheckinFlightData, Flight> {
	
	@Reference
	private MmbDeepLinkDataToDeepLink mmbDeepLinkDataConverter;
	
	@Reference
	private CheckinAirportToAirport checkinAirportDataConverter;

	@Override
	public Flight convert(CheckinFlightData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		Flight destination = null;

		if (source != null) {
			destination = objectFactory.createFlight();
			destination.setX003CIdX003EKBackingField(source.getId());
			
			Calendar sourceArrivalDate = (Calendar) source.getArrivalDateTime().clone();
			sourceArrivalDate.set(Calendar.HOUR, 0);
			sourceArrivalDate.set(Calendar.AM_PM, Calendar.AM);
			sourceArrivalDate.set(Calendar.MINUTE, 0);
			sourceArrivalDate.set(Calendar.SECOND, 0);
			sourceArrivalDate.set(Calendar.MILLISECOND, 0);
			destination.setX003CArrivalDateX003EKBackingField(XsdConvertUtils.toXMLGregorianCalendar(sourceArrivalDate));
			
			Calendar sourceArrivalTime = (Calendar) source.getArrivalDateTime().clone();
			sourceArrivalTime.set(Calendar.YEAR, 1);
			sourceArrivalTime.set(Calendar.MONTH, 0);
			sourceArrivalTime.set(Calendar.DAY_OF_MONTH, 1);
			destination.setX003CArrivalTimeX003EKBackingField(XsdConvertUtils.toXMLGregorianCalendar(sourceArrivalTime));
			
			List<MmbCompartimentalClassEnum> sourceCabins = source.getCabins();
			if(sourceCabins != null){
				ArrayOfCompartimentalClass sourceCabinsList = objectFactory.createArrayOfCompartimentalClass();
				for(MmbCompartimentalClassEnum sourceCabinItem : sourceCabins){
					sourceCabinsList.getCompartimentalClass().add(CompartimentalClass.fromValue(sourceCabinItem.value()));
				}
				destination.setX003CCabinsX003EKBackingField(sourceCabinsList);
			}
			
			destination.setX003CCarrierX003EKBackingField(source.getCarrier());
			destination.setX003CComfortSeatFareX003EKBackingField(source.getComfortSeatFare());
			destination.setX003CComfortSeatPaidX003EKBackingField(source.getComfortSeatPaid());
			
			Calendar sourceDepartureDate = (Calendar) source.getDepartureDateTime().clone();
			sourceDepartureDate.set(Calendar.HOUR, 0);
			sourceDepartureDate.set(Calendar.AM_PM, Calendar.AM);
			sourceDepartureDate.set(Calendar.MINUTE, 0);
			sourceDepartureDate.set(Calendar.SECOND, 0);
			sourceDepartureDate.set(Calendar.MILLISECOND, 0);
			destination.setX003CDepartureDateX003EKBackingField(XsdConvertUtils.toXMLGregorianCalendar(sourceDepartureDate));
			
			Calendar sourceDepartureTime = (Calendar) source.getDepartureDateTime().clone();
			sourceDepartureTime.set(Calendar.YEAR, 1);
			sourceDepartureTime.set(Calendar.MONTH, 1);
			sourceDepartureTime.set(Calendar.DAY_OF_MONTH, 1);
			destination.setX003CDepartureTimeX003EKBackingField(XsdConvertUtils.toXMLGregorianCalendar(sourceDepartureTime));
			
			destination.setX003CEticketClassX003EKBackingField(source.getEticketClass());
			destination.setX003CEticketX003EKBackingField(source.getEticket());
			destination.setX003CFlightNumberX003EKBackingField(source.getFlightNumber());
			destination.setX003CHasComfortSeatX003EKBackingField(source.getHasComfortSeat());
			destination.setX003CIndexX003EKBackingField(source.getIndex());
			MmbLegTypeEnum sourceLegType = source.getLegType();
			if (sourceLegType != null)
				destination.setX003CLegTypeX003EKBackingField(LegType.fromValue(sourceLegType.value()));
			
			destination.setX003COperatingCarrierX003EKBackingField(source.getOperatingCarrier());
			destination.setX003COperatingFlightNumberX003EKBackingField(source.getOperatingFlightNumber());
			destination.setX003CPnrX003EKBackingField(source.getPnr());
			destination.setX003CReservationRPHX003EKBackingField(source.getRph());
			destination.setX003CRouteIdX003EKBackingField(source.getRouteId());
			MmbCompartimentalClassEnum sourceSeatClass = source.getSeatClass();
			if (sourceSeatClass != null)
				destination.setX003CSeatClassX003EKBackingField(CompartimentalClass.fromValue(sourceSeatClass.value()));
			
			MmbFlightStatusEnum sourceStatus = source.getStatus();
			if (sourceStatus != null)
				destination.setX003CStatusX003EKBackingField(FlightStatus.fromValue(sourceStatus.value()));
			
			destination.setX003CDeepLinkCodeX003EKBackingField(mmbDeepLinkDataConverter.convert(source.getDeepLinkCode()));
			
			destination.setX003CFromX003EKBackingField(checkinAirportDataConverter.convert(source.getFrom()));
			
			destination.setX003CToX003EKBackingField(checkinAirportDataConverter.convert(source.getTo()));
			
		}
		return destination;
	}

}
