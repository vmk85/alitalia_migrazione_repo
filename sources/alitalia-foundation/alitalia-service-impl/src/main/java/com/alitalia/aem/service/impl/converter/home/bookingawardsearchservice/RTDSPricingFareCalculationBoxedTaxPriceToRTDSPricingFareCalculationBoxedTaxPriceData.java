package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxPriceData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxPrice;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareCalculationBoxedTaxPriceToRTDSPricingFareCalculationBoxedTaxPriceData.class)
public class RTDSPricingFareCalculationBoxedTaxPriceToRTDSPricingFareCalculationBoxedTaxPriceData implements
		Converter<ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxPrice, 
					ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxPriceData> {

	@Override
	public ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxPriceData convert(
			ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxPrice source) {
		ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxPriceData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxPriceData();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
