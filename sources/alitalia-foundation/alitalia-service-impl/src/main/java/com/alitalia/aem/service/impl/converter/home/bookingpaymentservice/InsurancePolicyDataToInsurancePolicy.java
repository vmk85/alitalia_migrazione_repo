package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.InsurancePolicyData;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.InsurancePolicy;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=InsurancePolicyDataToInsurancePolicy.class)
public class InsurancePolicyDataToInsurancePolicy implements Converter<InsurancePolicyData, InsurancePolicy> {
	
	@Override
	public InsurancePolicy convert(InsurancePolicyData source) {
		
		InsurancePolicy destination = null;
		
		if (source != null) {
		
			ObjectFactory objectFactory = new ObjectFactory();
			
			destination = objectFactory.createInsurancePolicy();
			
			destination.setBuy(
					source.getBuy());
			
			destination.setCardType(
					objectFactory.createInsurancePolicyCardType(
							source.getCardType()));
			
			destination.setCurrency(
					objectFactory.createInsurancePolicyCurrency(
							source.getCurrency()));
			
			destination.setDiscount(
					source.getDiscount());
			
			destination.setDiscountedPrice(
					source.getDiscountedPrice());
			
			destination.setErrorDescription(
					objectFactory.createInsurancePolicyErrorDescription(
							source.getCurrency()));
			
			destination.setPolicyNumber(
					objectFactory.createInsurancePolicyPolicyNumber(
							source.getPolicyNumber()));
			
			destination.setTotalInsuranceCost(
					source.getTotalInsuranceCost());
			
			destination.setTypeOfTrip(
					objectFactory.createInsurancePolicyTypeOfTrip(
							source.getTypeOfTrip()));
		
		}
		
		return destination;
	}

}
