package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareBookingInfoData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareEndorsementData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareSurchargeData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingFare;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingFareBookingInfo;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingFareEndorsement;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingFareSurcharge;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareToRTDSPricingFareData.class)
public class RTDSPricingFareToRTDSPricingFareData implements
		Converter<ResultTicketingDetailSolutionPricingFare, ResultTicketingDetailSolutionPricingFareData> {

	@Reference
	private RTDSPricingFareAdjustedPriceToRTDSPricingFareAdjustedPriceData adjustedPriceFieldConverter;

	@Reference
	private RTDSPricingFareBasePriceToRTDSPricingFareBasePriceData basePriceFieldConverter;

	@Reference
	private RTDSPricingFareDisplayBasePriceToRTDSPricingFareDisplayBasePriceData displayBasePriceFieldConverter;

	@Reference
	private RTDSPricingFareSaleAdjustedPriceToRTDSPricingFareSaleAdjustedPriceData saleAdjustedPriceFieldConverter;

	@Reference
	private RTDSPricingFareSaleBasePriceToRTDSPricingFareSaleBasePriceData saleBasePriceFieldConverter;

	@Reference
	private RTDSPricingFareBookingInfoToRTDSPricingFareBookingInfoData bookingInfoFieldConverter;

	@Reference
	private RTDSPricingFareEndorsementToRTDSPricingFareEndorsementData endorsementFieldConverter;

	@Reference
	private RTDSPricingFareSurchargeToRTDSPricingFareSurchargeData surchargeFieldConverter;

	@Override
	public ResultTicketingDetailSolutionPricingFareData convert(
			ResultTicketingDetailSolutionPricingFare source) {
		ResultTicketingDetailSolutionPricingFareData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingFareData();

			destination.setCarrierField(source.getCarrierField());
			destination.setCity1Field(source.getCity1Field());
			destination.setCity2Field(source.getCity2Field());
			destination.setDestinationCityField(source.getDestinationCityField());
			destination.setExtendedFareCodeField(source.getExtendedFareCodeField());
			destination.setGlobalIndicatorField(source.getGlobalIndicatorField());
			destination.setOriginCityField(source.getOriginCityField());
			destination.setRkeyField(source.getRkeyField());
			destination.setTagField(source.getTagField());
			destination.setTicketDesignatorField(source.getTicketDesignatorField());

			destination.setAdjustedPriceField(
					adjustedPriceFieldConverter.convert(source.getAdjustedPriceField()));

			destination.setBasePriceField(
					basePriceFieldConverter.convert(source.getBasePriceField()));

			List<ResultTicketingDetailSolutionPricingFareBookingInfoData> bookingInfoField = null;
			if (source.getBookingInfoField() != null &&
					source.getBookingInfoField().getResultTicketingDetailSolutionPricingFareBookingInfo() != null &&
					!source.getBookingInfoField().getResultTicketingDetailSolutionPricingFareBookingInfo().isEmpty()) {
				bookingInfoField = new ArrayList<ResultTicketingDetailSolutionPricingFareBookingInfoData>();
				for(ResultTicketingDetailSolutionPricingFareBookingInfo sourceBookingInfoFieldElem : 
						source.getBookingInfoField().getResultTicketingDetailSolutionPricingFareBookingInfo())
					bookingInfoField.add(bookingInfoFieldConverter.convert(sourceBookingInfoFieldElem));
			}
			destination.setBookingInfoField(bookingInfoField);

			destination.setDisplayBasePriceField(
					displayBasePriceFieldConverter.convert(source.getDisplayBasePriceField()));

			List<ResultTicketingDetailSolutionPricingFareEndorsementData> endorsementField = null;
			if (source.getEndorsementField() != null &&
					source.getEndorsementField().getResultTicketingDetailSolutionPricingFareEndorsement() != null &&
					!source.getEndorsementField().getResultTicketingDetailSolutionPricingFareEndorsement().isEmpty()) {
				endorsementField = new ArrayList<ResultTicketingDetailSolutionPricingFareEndorsementData>();
				for(ResultTicketingDetailSolutionPricingFareEndorsement sourceEndorsementFieldElem : 
						source.getEndorsementField().getResultTicketingDetailSolutionPricingFareEndorsement())
					endorsementField.add(endorsementFieldConverter.convert(sourceEndorsementFieldElem));
			}
			destination.setEndorsementField(endorsementField);

			List<String> ptcField = null;
			if (source.getPtcField() != null &&
					source.getPtcField().getString() != null &&
					!source.getPtcField().getString().isEmpty()) {
				ptcField = new ArrayList<String>();
				for (String sourcePtcFieldElem : source.getPtcField().getString())
					ptcField.add(sourcePtcFieldElem);
			}
			destination.setPtcField(ptcField);

			destination.setSaleAdjustedPriceField(
					saleAdjustedPriceFieldConverter.convert(source.getSaleAdjustedPriceField()));

			destination.setSaleBasePriceField(
					saleBasePriceFieldConverter.convert(source.getSaleBasePriceField()));

			List<ResultTicketingDetailSolutionPricingFareSurchargeData> surchargeField = null;
			if (source.getSurchargeField() != null &&
					source.getSurchargeField().getResultTicketingDetailSolutionPricingFareSurcharge() != null &&
					!source.getSurchargeField().getResultTicketingDetailSolutionPricingFareSurcharge().isEmpty()) {
				surchargeField = new ArrayList<ResultTicketingDetailSolutionPricingFareSurchargeData>();
				for(ResultTicketingDetailSolutionPricingFareSurcharge sourceSurchargeFieldElem : 
						source.getSurchargeField().getResultTicketingDetailSolutionPricingFareSurcharge())
					surchargeField.add(surchargeFieldConverter.convert(sourceSurchargeFieldElem));
			}
			destination.setSurchargeField(surchargeField);
		}

		return destination;
	}

}
