/**
 * 
 */
package com.alitalia.aem.service.impl.converter.home.carnetpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.CarnetBuyCarnetRequest;
import com.alitalia.aem.ws.carnet.paymentservice.xsd1.BuyCarnetRequest;
import com.alitalia.aem.ws.carnet.paymentservice.xsd1.ObjectFactory;


@Component(immediate=true, metatype=false)
@Service(value=CarnetBuyCarnetRequestToBuyCarnetRequest.class)
public class CarnetBuyCarnetRequestToBuyCarnetRequest implements Converter<CarnetBuyCarnetRequest, BuyCarnetRequest> {
	
	@Reference
	private CarnetPrenotationDataToPrenotation carnetPrenotationConverter;

	@Override
	public BuyCarnetRequest convert(CarnetBuyCarnetRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		BuyCarnetRequest destination = null;

		if (source != null) {
			
			destination = objectFactory.createBuyCarnetRequest();
			destination.setLanguageCode(objectFactory.createBuyCarnetRequestLanguageCode(source.getLanguageCode()));
			destination.setMarketCode(objectFactory.createBuyCarnetRequestMarketCode(source.getMarketCode()));
			destination.setPrenotation(objectFactory.createBuyCarnetRequestPrenotation(carnetPrenotationConverter.convert(source.getPrenotation())));

		}
		return destination;
	}
}
