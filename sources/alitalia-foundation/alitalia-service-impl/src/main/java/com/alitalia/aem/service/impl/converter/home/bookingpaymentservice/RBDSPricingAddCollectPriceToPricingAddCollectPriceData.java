//2
package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingAddCollectPriceData;
import com.alitalia.aem.ws.booking.paymentservice.xsd11.ResultBookingDetailsSolutionPricingAddCollectPrice;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingAddCollectPriceToPricingAddCollectPriceData.class)
public class RBDSPricingAddCollectPriceToPricingAddCollectPriceData implements
		Converter<ResultBookingDetailsSolutionPricingAddCollectPrice, ResultBookingDetailsSolutionPricingAddCollectPriceData> {

	@Override
	public ResultBookingDetailsSolutionPricingAddCollectPriceData convert(
			ResultBookingDetailsSolutionPricingAddCollectPrice source) {
		ResultBookingDetailsSolutionPricingAddCollectPriceData destination = null;
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingAddCollectPriceData();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}
	
}
