package com.alitalia.aem.service.impl.converter.home.checkinpassengerservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.messages.home.WebCheckinNewPassengerRequest;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.NewPassengerRequest;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.Resource;
import com.alitalia.aem.ws.checkin.passengerservice.xsd2.ArrayOfPassenger;


@Component(immediate=true, metatype=false)
@Service(value=WebCheckinNewPassengerRequestToNewPassengerRequest.class)
public class WebCheckinNewPassengerRequestToNewPassengerRequest implements Converter<WebCheckinNewPassengerRequest, NewPassengerRequest> {

	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;
	
	@Reference
	private CheckinRouteDataToRoute checkinRouteDataConverter;
	
	@Reference
	private CheckinPassengerDataToPassenger checkinPassengerConverter;

	@Override
	public NewPassengerRequest convert(WebCheckinNewPassengerRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		NewPassengerRequest destination = null;

		if (source != null) {
			
			destination = objectFactory.createNewPassengerRequest();
			destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			destination.setX003CResourceX003EKBackingField(Resource.WEB);
			
			com.alitalia.aem.ws.checkin.passengerservice.xsd2.ObjectFactory objectFactory2 = new com.alitalia.aem.ws.checkin.passengerservice.xsd2.ObjectFactory();
			JAXBElement<ArrayOfPassenger> passengersList = objectFactory.createNewPassengerRequestPassengers(objectFactory2.createArrayOfPassenger());
			if (source.getPassengers()!=null){
				for (CheckinPassengerData passengerData : source.getPassengers()) {
					passengersList.getValue().getPassenger().add(checkinPassengerConverter.convert(passengerData));
				}
			}
			destination.setPassengers(passengersList);
			destination.setGroupPnr(objectFactory.createNewPassengerRequestGroupPnr(source.getGroupPnr()));
			destination.setSelectedRoute(objectFactory.createNewPassengerRequestSelectedRoute(checkinRouteDataConverter.convert(source.getSelectedRoute())));
			destination.setEticket(objectFactory.createNewPassengerRequestEticket(source.getEticket()));
			destination.setName(objectFactory.createNewPassengerRequestName(source.getName()));
			destination.setLastName(objectFactory.createNewPassengerRequestLastName(source.getLastName()));
		}
		return destination;
	}
}
