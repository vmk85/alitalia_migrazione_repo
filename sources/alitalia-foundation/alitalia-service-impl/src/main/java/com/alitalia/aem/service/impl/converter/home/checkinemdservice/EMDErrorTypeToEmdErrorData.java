package com.alitalia.aem.service.impl.converter.home.checkinemdservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.EmdErrorData;
import com.alitalia.aem.ws.checkin.emdservice.xsd2.EMDErrorType;


@Component(immediate=true, metatype=false)
@Service(value=EMDErrorTypeToEmdErrorData.class)
public class EMDErrorTypeToEmdErrorData implements
		Converter<EMDErrorType, EmdErrorData> {


	@Override
	public EmdErrorData convert(EMDErrorType source) {
		EmdErrorData destination = null;

		if (source != null) {
			
			destination = new EmdErrorData();	
			destination.setCode(source.getX003CCodeX003EKBackingField());
			destination.setDocURL(source.getX003CDocURLX003EKBackingField());
			destination.setNodeList(source.getX003CNodeListX003EKBackingField());
			destination.setRecordId(source.getX003CRecordIDX003EKBackingField());
			destination.setStatus(source.getX003CStatusX003EKBackingField());
			destination.setTag(source.getX003CTagX003EKBackingField());
			destination.setType(source.getX003CTypeX003EKBackingField());
			destination.setValue(source.getX003CValueX003EKBackingField());

		}
		return destination;
	}

}
