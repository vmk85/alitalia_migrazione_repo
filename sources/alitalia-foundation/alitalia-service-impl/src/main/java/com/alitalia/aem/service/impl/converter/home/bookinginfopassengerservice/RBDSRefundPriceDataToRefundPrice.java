package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionRefundPriceData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ResultBookingDetailsSolutionRefundPrice;

@Component(immediate=true, metatype=false)
@Service(value=RBDSRefundPriceDataToRefundPrice.class)
public class RBDSRefundPriceDataToRefundPrice implements Converter<ResultBookingDetailsSolutionRefundPriceData, ResultBookingDetailsSolutionRefundPrice> {

	@Override
	public ResultBookingDetailsSolutionRefundPrice convert(ResultBookingDetailsSolutionRefundPriceData source) {
		ResultBookingDetailsSolutionRefundPrice destination = null;
		if(source!=null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionRefundPrice();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}
	
}
