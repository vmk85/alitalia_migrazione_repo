package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.data.home.AirportDetailsData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.Airport;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.AreaValue;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.ObjectFactory;

@Component(immediate = true, metatype = false)
@Service(value = AirportDataToAirport.class)
public class AirportDataToAirport implements Converter<AirportData, Airport> {

	@Reference
	private AirportDetailsDataToAirportDetails airportDetailsDataConverter;

	@Override
	public Airport convert(AirportData source) {
		ObjectFactory factory3 = new ObjectFactory();
		Airport airport = factory3.createAirport();

		airport.setArea(AreaValue.fromValue(source.getArea().value()));
		airport.setAZDeserves(source.getAzDeserves());
		airport.setCity(factory3.createAirportCity(source.getCity()));
		airport.setCityCode(factory3.createAirportCityCode(source.getCityCode()));
		airport.setCityUrl(factory3.createAirportCityUrl(source.getCityUrl()));
		airport.setCode(factory3.createAirportCode(source.getCode()));
		airport.setCountry(factory3.createAirportCountry(source.getCountry()));
		airport.setCountryCode(factory3.createAirportCountryCode(source.getCountryCode()));
		airport.setDescription(factory3.createAirportDescription(source.getDescription()));
		airport.setEligibility(source.getEligibility());
		airport.setEtktAvlblFrom(factory3.createAirportEtktAvlblFrom(XsdConvertUtils.toXMLGregorianCalendar(source.getEtktAvlblFrom())));
		airport.setHiddenInFirstDeparture(source.getHiddenInFirstDeparture());
		airport.setIdMsg(source.getIdMsg());
		airport.setName(factory3.createAirportName(source.getName()));
		airport.setState(factory3.createAirportState(source.getState()));
		airport.setStateCode(factory3.createAirportStateCode(source.getStateCode()));
		airport.setUnavlblFrom(factory3.createAirportUnavlblFrom(XsdConvertUtils.toXMLGregorianCalendar(source.getUnavlblFrom())));
		airport.setUnavlblUntil(factory3.createAirportUnavlblUntil(XsdConvertUtils.toXMLGregorianCalendar(source.getUnavlblUntil())));

		AirportDetailsData sourceDetails = source.getDetails();
		if (sourceDetails != null)
			airport.setDetails(factory3.createAirportDetails(airportDetailsDataConverter.convert(sourceDetails)));
		else
			airport.setDetails(factory3.createAirportDetails(null));
		
		return airport;
	}
}
