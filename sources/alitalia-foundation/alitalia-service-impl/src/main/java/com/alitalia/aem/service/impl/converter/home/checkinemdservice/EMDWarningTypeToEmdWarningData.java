package com.alitalia.aem.service.impl.converter.home.checkinemdservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.EmdWarningData;
import com.alitalia.aem.ws.checkin.emdservice.xsd2.EMDWarningType;


@Component(immediate=true, metatype=false)
@Service(value=EMDWarningTypeToEmdWarningData.class)
public class EMDWarningTypeToEmdWarningData implements
		Converter<EMDWarningType, EmdWarningData> {


	@Override
	public EmdWarningData convert(EMDWarningType source) {
		EmdWarningData destination = null;

		if (source != null) {
			
			destination = new EmdWarningData();	
			destination.setCode(source.getX003CCodeX003EKBackingField());
			destination.setDocURL(source.getX003CDocURLX003EKBackingField());
			destination.setRecordId(source.getX003CRecordIDX003EKBackingField());
			destination.setStatus(source.getX003CStatusX003EKBackingField());
			destination.setTag(source.getX003CTagX003EKBackingField());
			destination.setType(source.getX003CTypeX003EKBackingField());
			destination.setRph(source.getX003CRPHX003EKBackingField());
			destination.setShortText(source.getX003CShortTextX003EKBackingField());

		}
		return destination;
	}

}
