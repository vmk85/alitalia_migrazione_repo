package com.alitalia.aem.service.impl.converter.home.checkincomfortseatservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinUpgradeClassPolicyData;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd2.UpgradeClassPolicy;



@Component(immediate=true, metatype=false)
@Service(value=UpgradeClassPolicyToCheckinUpgradeClassPolicyData.class)
public class UpgradeClassPolicyToCheckinUpgradeClassPolicyData implements Converter<UpgradeClassPolicy, CheckinUpgradeClassPolicyData> {

	@Override
	public CheckinUpgradeClassPolicyData convert(UpgradeClassPolicy source) {
		CheckinUpgradeClassPolicyData destination = null;

		if (source != null) {
			
			destination = new CheckinUpgradeClassPolicyData();
			
			destination.setId(source.getX003CIdX003EKBackingField());
			
			if(source.getX003CCompartimentalClassX003EKBackingField() != null){
				destination.setCompartimentalClass(MmbCompartimentalClassEnum.fromValue(source.getX003CCompartimentalClassX003EKBackingField().value()));
			}
			
			destination.setCurrency(source.getX003CCurrencyX003EKBackingField());
			destination.setPrice(source.getX003CPriceX003EKBackingField());
			
		}
		
		return destination;
	}

}
