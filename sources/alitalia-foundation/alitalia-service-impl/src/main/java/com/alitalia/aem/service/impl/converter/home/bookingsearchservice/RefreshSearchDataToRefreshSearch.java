package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.PassengerBase;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.PropertiesData;
import com.alitalia.aem.common.data.home.RefreshSearchData;
import com.alitalia.aem.ws.booking.searchservice.xsd2.RefreshSearch;
import com.alitalia.aem.ws.booking.searchservice.xsd3.APassengerBase;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ArrayOfAPassengerBase;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ArrayOfDictionaryItem;
import com.alitalia.aem.ws.booking.searchservice.xsd3.GatewayType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.RefreshType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.RouteType;

@Component(immediate=true, metatype=false)
@Service(value=RefreshSearchDataToRefreshSearch.class)
public class RefreshSearchDataToRefreshSearch implements Converter<RefreshSearchData, RefreshSearch> {

	@Reference
	private PassengerBaseDataToAPassengerBase passengerBaseDataConverter;

	@Reference
	private PropertiesDataToArrayOfDictionaryItem propertiesDataConverter;

	@Override
	public RefreshSearch convert(RefreshSearchData source) {
		com.alitalia.aem.ws.booking.searchservice.xsd2.ObjectFactory factory2 = 
				new com.alitalia.aem.ws.booking.searchservice.xsd2.ObjectFactory();
		com.alitalia.aem.ws.booking.searchservice.xsd3.ObjectFactory factory3 = 
				new com.alitalia.aem.ws.booking.searchservice.xsd3.ObjectFactory();
		
		RefreshSearch destination = factory2.createRefreshSearch();

		destination.setCurrency(factory2.createRefreshSearchCurrency(source.getCurrency()));
		destination.setFarmId(factory3.createABoomBoxInfoFarmId(source.getFarmId()));
		destination.setId(factory3.createABoomBoxInfoId(source.getId()));
		destination.setOriginalFare(source.getOriginalFare());
		destination.setOriginalPrice(source.getOriginalPrice());
		destination.setRefreshType(RefreshType.REFRESH_SEARCH);
		destination.setSessionId(factory3.createABoomBoxInfoSessionId(source.getSessionId()));
		destination.setSliceType(RouteType.fromValue(source.getSliceType().value()));
		destination.setSolutionId(factory2.createGateWayBrandSearchSolutionId(source.getSolutionId()));
		destination.setSolutionSet(factory3.createABoomBoxInfoSolutionSet(source.getSolutionSet()));
		destination.setType(GatewayType.GATEWAY_SEARCH);

		ArrayOfAPassengerBase passengers = null;
		if (source.getPassengers() != null) {
			passengers = factory3.createArrayOfAPassengerBase();
			for (PassengerBase sourcePassengerBase : source.getPassengers()) {
				APassengerBase aPassengerBase = passengerBaseDataConverter.convert((PassengerBaseData) sourcePassengerBase);
				passengers.getAPassengerBase().add(aPassengerBase);
			}
		}
		destination.setPassengers(factory2.createGateWayBrandSearchPassengers(passengers));

		PropertiesData sourceProperties = source.getProperties();
		if (sourceProperties != null) {
			ArrayOfDictionaryItem properties = propertiesDataConverter.convert(sourceProperties);
			destination.setProperties(factory3.createABoomBoxGenericInfoProperties(properties));
		}
		else
			destination.setProperties(factory3.createABoomBoxGenericInfoProperties(null));

		return destination;
	}

}
