package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.CashAndMiles;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.ECoupon;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.InsurancePolicy;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.PaymentOfanyTypeanyType;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.Route;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.Routes;

@Component(immediate=true, metatype=false)
@Service(value=RoutesToRoutesData.class)
public class RoutesToRoutesData implements Converter<Routes, RoutesData> {

	private static final Logger logger = LoggerFactory.getLogger(RoutesToRoutesData.class);

	@Reference
	private BillingToBillingData billingToBillingDataConverter;
	
	@Reference
	private CashAndMilesToCashAndMilesData cashAndMilesToCashAndMilesDataConverter;
	
	@Reference
	private PaymentToPaymentData paymentToPaymentDataConverter;
	
	@Reference
	private MmCustomerToMmCustomerData mmCustomerToMmCustomerDataConverter;
	
	@Reference
	private ECouponToECouponData eCouponToECouponDataConverter;
	
	@Reference
	private InsurancePolicyToInsurancePolicyData insurancePolicyToInsurancePolicyDataConverter;
	
	@Reference
	private PassengerBaseToAPassengerBaseData passengerBaseToAPassengerBaseDataConverter;
	
	@Reference
	private ArrayOfDictionaryItemToPropertiesData arrayOfDictionaryItemToPropertiesDataConverter;
	
	@Reference
	private RouteToRouteData routeConverter;
	
	@Reference
	private BookingAwardSearchServiceJAXBContextFactory jaxbContextFactory;

	@SuppressWarnings("unchecked")
	@Override
	public RoutesData convert(Routes source) {
		
		RoutesData destination = null;
		
		if (source != null) {
			
			destination = new RoutesData();
		
			destination.setBilling(
					billingToBillingDataConverter.convert(
							source.getBilling().getValue()));
			
			if (source.getCabin() != null) {
				destination.setCabin(
						CabinEnum.fromValue(
								source.getCabin().value()));
			}
			
			CashAndMiles cashAndMiles = null;
			if ( source.getCashAndMiles() != null && source.getCashAndMiles().getValue() != null ) {
				if(source.getCashAndMiles().getValue() instanceof  CashAndMiles) {
					cashAndMiles = (CashAndMiles) source.getCashAndMiles().getValue();
				} else {
					try {
						Unmarshaller unmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
						JAXBElement<CashAndMiles> element = (JAXBElement<CashAndMiles>) 
								unmarshaller.unmarshal((Node) source.getCashAndMiles().getValue());
						cashAndMiles = element.getValue();
					} catch (JAXBException e) {
						logger.error("Impossibile convertire oggetto CashAndMiles", e);
					}
					
				}
			}
			destination.setCashAndMiles(cashAndMilesToCashAndMilesDataConverter.convert(cashAndMiles));
			
			ECoupon eCoupon = null;
			if (source.getCoupon() != null && source.getCoupon().getValue() != null) {
				try {
					if (source.getCoupon().getValue() instanceof ECoupon) {
						eCoupon = (ECoupon) source.getCoupon().getValue();
					} else {
						Unmarshaller unmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
						JAXBElement<ECoupon> element = (JAXBElement<ECoupon>) 
								unmarshaller.unmarshal((Node) source.getCoupon().getValue());
						eCoupon = element.getValue();
					}
				} catch (JAXBException e) {
					logger.error("Error creating JAXBContext/Unmarshaller for ECoupon class: {}", e);
				}
			}
			destination.setCoupon(eCouponToECouponDataConverter.convert(eCoupon));
			
			if (source.getProperties() != null) {
				destination.setProperties(
						arrayOfDictionaryItemToPropertiesDataConverter.convert(
								source.getProperties().getValue()));
			}
			
			if (source.getFarmId() != null) {
				destination.setFarmId(
						source.getFarmId().getValue());
			}
			
			if (source.getId() != null) {
				destination.setId(
						source.getId().getValue());
			}
			
			if (source.getSessionId() != null) {
				destination.setSessionId(
						source.getSessionId().getValue());
			}
			
			if (source.getSolutionSet() != null) {
				destination.setSolutionSet(
						source.getSolutionSet().getValue());
			}
			
			if (source.getInsurance() != null && source.getInsurance().getValue() != null) {
				Object insurancePolicyObject = source.getInsurance().getValue();
				if (insurancePolicyObject instanceof InsurancePolicy) {
				destination.setInsurance(insurancePolicyToInsurancePolicyDataConverter.convert(
								(InsurancePolicy) insurancePolicyObject));
				}
				else {
					InsurancePolicy insurancePolicy = null;
					try {
						Unmarshaller insuranceUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
						insurancePolicy = (InsurancePolicy)  
								((JAXBElement<InsurancePolicy>) insuranceUnmarshaller.unmarshal((Node) insurancePolicyObject)).getValue();
					} catch (JAXBException e) {
						logger.error("Error creating JAXBContext/Unmarshaller for InsurancePolicy class: {}", e);
					}
					destination.setInsurance(insurancePolicyToInsurancePolicyDataConverter.convert(insurancePolicy));
				}
			}
			
			if (source.getMilleMigliaCustomer() != null) {
				destination.setMilleMigliaCustomer(
						mmCustomerToMmCustomerDataConverter.convert(
								source.getMilleMigliaCustomer().getValue()));
			}
						
			if (source.getMseType() != null) {
				destination.setMseType(
						source.getMseType().getValue());
			}
			
			if (source.getNationalInsuranceNumber() != null) {
				destination.setNationalInsuranceNumber(
						source.getNationalInsuranceNumber().getValue());
			}
			
			List<PassengerBaseData> passengers = new ArrayList<PassengerBaseData>();
			if (source.getPassengers() != null && source.getPassengers().getValue() != null) {
				for (Object passenger : source.getPassengers().getValue().getAnyType()) {
					passengers.add(passengerBaseToAPassengerBaseDataConverter.convert(passenger));
				}
			}	
			destination.setPassengers(
					passengers);
			
			
			if (source.getPayment() != null) {
				destination.setPayment(
						paymentToPaymentDataConverter.convert(
								(PaymentOfanyTypeanyType) source.getPayment().getValue()));
			}
			
			
			if (source.getPNR() != null) {
				destination.setPnr(
						source.getPNR().getValue());
			}
			
			List<RouteData> routesList = new ArrayList<RouteData>();
			if (source.getRoutesList() != null && source.getRoutesList().getValue() != null) {
				try {
					Unmarshaller routeUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
					for (Object routeObject : source.getRoutesList().getValue().getAnyType()) {
						Route route = null;
						if (routeObject instanceof Route)
							route = (Route) routeObject;
						else {
							JAXBElement<Route> routeElement = (JAXBElement<Route>) routeUnmarshaller.unmarshal((Node) routeObject);
							route = routeElement.getValue();
						}
						routesList.add(routeConverter.convert(route));
					}
				} catch (JAXBException e) {
					logger.error("Error creating JAXBContext/Unmarshaller for Route class: {}", e);
				}
			}
			destination.setRoutesList(
					routesList);
			
			destination.setSliceCount(
					source.getSliceCount());
			
		}
		
		return destination;
	}

}
