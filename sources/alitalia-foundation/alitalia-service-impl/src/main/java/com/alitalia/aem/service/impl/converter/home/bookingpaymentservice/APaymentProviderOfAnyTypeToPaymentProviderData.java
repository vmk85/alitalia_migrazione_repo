package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PaymentProviderBancaIntesaData;
import com.alitalia.aem.common.data.home.PaymentProviderBancoPostaData;
import com.alitalia.aem.common.data.home.PaymentProviderCreditCardData;
import com.alitalia.aem.common.data.home.PaymentProviderData;
import com.alitalia.aem.common.data.home.PaymentProviderFindomesticData;
import com.alitalia.aem.common.data.home.PaymentProviderGlobalCollectData;
import com.alitalia.aem.common.data.home.PaymentProviderInstalmentsBrazilData;
import com.alitalia.aem.common.data.home.PaymentProviderLottomaticaData;
import com.alitalia.aem.common.data.home.PaymentProviderMasterPassData;
import com.alitalia.aem.common.data.home.PaymentProviderPayAtTOData;
import com.alitalia.aem.common.data.home.PaymentProviderPayLaterData;
import com.alitalia.aem.common.data.home.PaymentProviderPayPalData;
import com.alitalia.aem.common.data.home.PaymentProviderPosteIDData;
import com.alitalia.aem.common.data.home.PaymentProviderSTSData;
import com.alitalia.aem.common.data.home.PaymentProviderZeroPaymentData;
import com.alitalia.aem.common.data.home.enumerations.CreditCardTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.GlobalCollectPaymentTypeEnum;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.AComunicationBase;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.APaymentProviderOfanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.AUserInfoBase;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.BancaIntesaOfanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.BancoPostaOfanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.Contact;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.CreditCardOfanyTypeanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.FindomesticOfanyTypeanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.GlobalCollectOfanyTypeanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.InstalmentsBrazilOfanyTypeanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.LottomaticaOfanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.MasterPassOfanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.PayAtTOOfanyTypeanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.PayLaterOfanyTypeanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.PayPalOfanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.PosteIDOfanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.STSOfanyTypeanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ZeroPaymentProviderOfanyTypeanyType;

@Component(immediate=true, metatype=false)
@Service(value=APaymentProviderOfAnyTypeToPaymentProviderData.class)
public class APaymentProviderOfAnyTypeToPaymentProviderData 
		implements Converter<APaymentProviderOfanyType, PaymentProviderData> {
	
	@Reference
	private AComunicationBaseToPaymentComunicationBaseData comunicationOfAnyTypeToPaymentComunicationConverter;
	
	@Reference
	private AUserInfoToUserInfoBaseData aUserInfoToUserInfoBaseDataConverter;
	
	@Reference
	private ContactToContactData contactToContactDataConverter;
	
	@Override
	public PaymentProviderData convert(APaymentProviderOfanyType source) {
		
		PaymentProviderData destination = null;
		
		if (source != null) {
			
			if (source instanceof BancaIntesaOfanyType) {
				destination = new PaymentProviderBancaIntesaData();
				
			} else if (source instanceof BancoPostaOfanyType) {
				destination = new PaymentProviderBancoPostaData();
				
			} else if (source instanceof ZeroPaymentProviderOfanyTypeanyType) {
				destination = new PaymentProviderZeroPaymentData();
				
			} else if (source instanceof PosteIDOfanyType) {
				destination = new PaymentProviderPosteIDData();
				
			} else if (source instanceof CreditCardOfanyTypeanyType) {
				CreditCardOfanyTypeanyType typedSource = (CreditCardOfanyTypeanyType) source;
				PaymentProviderCreditCardData typedDestination = new PaymentProviderCreditCardData();
				destination = typedDestination;
				
				if (typedSource.getCreditCardNumber() != null) {
					typedDestination.setCreditCardNumber(
							typedSource.getCreditCardNumber().getValue());
				}
				
				if (typedSource.getCVV() != null) {
					typedDestination.setCvv(
							typedSource.getCVV().getValue());
				}
				
				typedDestination.setExpiryMonth(
						typedSource.getExpiryMonth());
				
				typedDestination.setExpiryYear(
						typedSource.getExpiryYear());
				
				if (typedSource.getToken() != null) {
					typedDestination.setToken(
							typedSource.getToken().getValue());
				}
				
				if (typedSource.getType() != null) {
					typedDestination.setType(
							CreditCardTypeEnum.fromValue(typedSource.getType().value()));
				}
				
				if (typedSource.getUserInfo() != null) {
					typedDestination.setUserInfo(
							aUserInfoToUserInfoBaseDataConverter.convert(
									(AUserInfoBase) typedSource.getUserInfo().getValue()));
				}
				
			} else if (source instanceof FindomesticOfanyTypeanyType) {
				FindomesticOfanyTypeanyType typedSource = (FindomesticOfanyTypeanyType) source;
				PaymentProviderFindomesticData typedDestination = new PaymentProviderFindomesticData();
				destination = typedDestination;	
				
				if (typedSource.getUserInfo() != null) {
					typedDestination.setUserInfo(
							aUserInfoToUserInfoBaseDataConverter.convert(
									(AUserInfoBase) typedSource.getUserInfo().getValue()));
				}
				
				
			} else if (source instanceof GlobalCollectOfanyTypeanyType) {
				GlobalCollectOfanyTypeanyType typedSource = (GlobalCollectOfanyTypeanyType) source;
				PaymentProviderGlobalCollectData typedDestination = new PaymentProviderGlobalCollectData();
				destination = typedDestination;	
				
				if (typedSource.getUserInfo() != null) {
					typedDestination.setUserInfo(
							aUserInfoToUserInfoBaseDataConverter.convert(
									(AUserInfoBase) typedSource.getUserInfo().getValue()));
				}
				
				if (typedSource.getType() != null) {
					typedDestination.setType(
							GlobalCollectPaymentTypeEnum.fromValue(typedSource.getType().value()));
				}
				
				typedDestination.setInstallementsNumber(
						typedSource.getInstallementsNumber());
				
			} else if (source instanceof InstalmentsBrazilOfanyTypeanyType) {
				InstalmentsBrazilOfanyTypeanyType typedSource = (InstalmentsBrazilOfanyTypeanyType) source;
				PaymentProviderInstalmentsBrazilData typedDestination = new PaymentProviderInstalmentsBrazilData();
				destination = typedDestination;	
				
				List<String> emailForMail = new ArrayList<String>();
				if (typedSource.getEmailForMail() != null && typedSource.getEmailForMail().getValue() != null) {
					for (String email : typedSource.getEmailForMail().getValue().getString()) {
						emailForMail.add(email);
					}
				}
				typedDestination.setEmailForMail(
						emailForMail);
				
				if (typedSource.getEmailForPnr() != null) {
					typedDestination.setEmailForPnr(
							typedSource.getEmailForPnr().getValue());
				}
				
				if (typedSource.getOtherContact() != null) {
					typedDestination.setOtherContact(
							contactToContactDataConverter.convert(
									(Contact) typedSource.getOtherContact().getValue()));
				}
				
				if (typedSource.getType() != null) {
					typedDestination.setType(
							CreditCardTypeEnum.fromValue(typedSource.getType().value()));
				}
				
			} else if (source instanceof LottomaticaOfanyType) {
				PaymentProviderLottomaticaData typedDestination = new PaymentProviderLottomaticaData();
				destination = typedDestination;	
				
			} else if (source instanceof MasterPassOfanyType) {
				PaymentProviderMasterPassData typedDestination = new PaymentProviderMasterPassData();
				destination = typedDestination;	
				
			} else if (source instanceof PayAtTOOfanyTypeanyType) {
				PayAtTOOfanyTypeanyType typedSource = (PayAtTOOfanyTypeanyType) source;
				PaymentProviderPayAtTOData typedDestination = new PaymentProviderPayAtTOData();
				destination = typedDestination;	
				
				List<String> emailForMail = new ArrayList<String>();
				if (typedSource.getEmailForMail() != null && typedSource.getEmailForMail().getValue() != null) {
					for (String email : typedSource.getEmailForMail().getValue().getString()) {
						emailForMail.add(email);
					}
				}
				typedDestination.setEmailForMail(
						emailForMail);
				
				if (typedSource.getEmailForPnr() != null) {
					typedDestination.setEmailForPnr(
							typedSource.getEmailForPnr().getValue());
				}
				
				if (typedSource.getOtherContact() != null) {
					typedDestination.setOtherContact(
							contactToContactDataConverter.convert(
									(Contact) typedSource.getOtherContact().getValue()));
				}
				
			} else if (source instanceof PayLaterOfanyTypeanyType) {
				PayLaterOfanyTypeanyType typedSource = (PayLaterOfanyTypeanyType) source;
				PaymentProviderPayLaterData typedDestination = new PaymentProviderPayLaterData();
				destination = typedDestination;	
				
				List<String> emailForMail = new ArrayList<String>();
				if (typedSource.getEmailForMail() != null && typedSource.getEmailForMail().getValue() != null) {
					for (String email : typedSource.getEmailForMail().getValue().getString()) {
						emailForMail.add(email);
					}
				}
				typedDestination.setEmailForMail(
						emailForMail);
				
				if (typedSource.getEmailForPnr() != null) {
					typedDestination.setEmailForPnr(
							typedSource.getEmailForPnr().getValue());
				}
				
				if (typedSource.getOtherContact() != null) {
					typedDestination.setOtherContact(
							contactToContactDataConverter.convert(
									(Contact) typedSource.getOtherContact().getValue()));
				}
				
			} else if (source instanceof PayPalOfanyType) {
				PaymentProviderPayPalData typedDestination = new PaymentProviderPayPalData();
				destination = typedDestination;	
				
			} else if (source instanceof STSOfanyTypeanyType) {
				STSOfanyTypeanyType typedSource = (STSOfanyTypeanyType) source;
				PaymentProviderSTSData typedDestination = new PaymentProviderSTSData();
				destination = typedDestination;
				
				if (typedSource.getClientInfoRef() != null) {
					typedDestination.setClientInfoRef(
							typedSource.getClientInfoRef().getValue());
				}
				
				if (typedSource.getClientIP() != null) {
					typedDestination.setClientIP(
							typedSource.getClientIP().getValue());
				}
				
				if (typedSource.getClientUserAccount() != null) {
					typedDestination.setClientUserAccount(
							typedSource.getClientUserAccount().getValue());
				}
				
				if (typedSource.getFailurePageUrl() != null) {
					typedDestination.setFailurePageUrl(
							typedSource.getFailurePageUrl().getValue());
				}
				
				if (typedSource.getMerchantShopRef() != null) {
					typedDestination.setMerchantShopRef(
							typedSource.getMerchantShopRef().getValue());
				}
				
				if (typedSource.getStsClientID() != null) {
					typedDestination.setStsClientID(
							typedSource.getStsClientID().getValue());
				}
				
				if (typedSource.getStsMerchantID() != null) {
					typedDestination.setStsMerchantID(
							typedSource.getStsMerchantID().getValue());
				}
				
				if (typedSource.getStsPayType() != null) {
					typedDestination.setStsPayType(
							typedSource.getStsPayType().getValue());
				}
				
				if (typedSource.getSuccessPageUrl() != null) {
					typedDestination.setSuccessPageUrl(
							typedSource.getSuccessPageUrl().getValue());
				}
				
				if (typedSource.getStsAdvanceDate() != null)
					typedDestination.setStsAdvanceDate(typedSource.getStsAdvanceDate().toGregorianCalendar());
				
				if (typedSource.getStsBalanceDate() != null)
					typedDestination.setStsBalanceDate(typedSource.getStsBalanceDate().toGregorianCalendar());
				
				if (typedSource.getStsDepartureDate() != null)
					typedDestination.setStsDepartureDate(typedSource.getStsDepartureDate().toGregorianCalendar());
				
			} else {
				destination = new PaymentProviderData();
				
				
			}
			
			if (source.getComunication() != null) {
				destination.setComunication(
						comunicationOfAnyTypeToPaymentComunicationConverter.convert(
								(AComunicationBase) source.getComunication().getValue()));
			}
		
		}
				
		return destination;
	}

}
