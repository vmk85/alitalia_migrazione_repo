package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.data.home.TicketInfoData;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.AdultPassenger;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.InfoBase;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.Preferences;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.TicketInfo;

@Component(immediate=true, metatype=false)
@Service(value=AdultPassengerToAdultPassengerData.class)
public class AdultPassengerToAdultPassengerData 
		implements Converter<AdultPassenger, AdultPassengerData> {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Reference
	private InfoBaseToPassengerInfoBaseData infoBaseToPassengerBaseInfoDataConverter;
	
	@Reference
	private PreferencesToPreferencesData preferencesToPreferencesDataConverter;
	
	@Reference
	private FrequentFlyerTypeToFrequentFlyerTypeData frequentFlyerTypeToFrequentFlyerTypeDataConverter;
	
	@Reference
	private TicketInfoToTicketInfoData ticketInfoToTicketInfoDataConverter;

	@Reference
	private BookingInfoPassengerServiceJAXBContextFactory jaxbContextFactory;
	
	@Override
	public AdultPassengerData convert(AdultPassenger source) {
		
		AdultPassengerData destination = null;
		
		if (source != null) {
			
			destination = new AdultPassengerData();
			
			// AdultPassenger data
			
			if (source.getFrequentFlyerCode() != null) {
				destination.setFrequentFlyerCode(
						source.getFrequentFlyerCode().getValue());
			}
			
			if (source.getFrequentFlyerTier() != null) {
				destination.setFrequentFlyerTier(
						source.getFrequentFlyerTier().getValue());
			}
			
			if (source.getFrequentFlyerType() != null) {
				destination.setFrequentFlyerType(
						frequentFlyerTypeToFrequentFlyerTypeDataConverter.convert(
								source.getFrequentFlyerType().getValue()));
			}
			
			// ARegularPassenger data
			
			if (source.getPreferences() != null
					&& source.getPreferences().getValue() != null) {
				Object uncastedValue = source.getPreferences().getValue();
				Preferences castedValue = null;
				if (uncastedValue instanceof Preferences) {
					castedValue = (Preferences) source.getPreferences().getValue();
				} else if (uncastedValue instanceof Node) {
					try {
						Unmarshaller unmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
						@SuppressWarnings("unchecked")
						JAXBElement<Preferences> jaxbElement = (JAXBElement<Preferences>) 
								unmarshaller.unmarshal((Node) uncastedValue);
						castedValue = jaxbElement.getValue();
					} catch (JAXBException e) {
						logger.error("Error while creating JAXBContext/Unmarshller for Preferences: {}", e);
					}
				}
				destination.setPreferences(
						preferencesToPreferencesDataConverter.convert(
								castedValue));
			}
			
			// APassengerBase data
			if (source.getAwardPrice() != null) {
				destination.setAwardPrice(source.getAwardPrice());
			}
			
			if (source.getCCFee() != null) {
				destination.setCcFee(source.getCCFee());
			}

			if (source.getType() != null) {
				destination.setType(
						PassengerTypeEnum.fromValue(source.getType().value()));
			}
			
			if (source.getLastName() != null) {
				destination.setLastName(
						source.getLastName().getValue());
			}
			
			if (source.getName() != null) {
				destination.setName(
						source.getName().getValue());
			}
			
			destination.setCouponPrice(
					source.getCouponPrice());
			
			destination.setExtraCharge(
					source.getExtraCharge());
			
			destination.setFee(
					source.getFee());
			
			destination.setGrossFare(
					source.getGrossFare());
			
			destination.setNetFare(
					source.getNetFare());
			
			if (source.getInfo() != null
					&& source.getInfo().getValue() != null) {
				Object uncastedValue = source.getInfo().getValue();
				InfoBase castedValue = null;
				if (uncastedValue instanceof InfoBase) {
					castedValue = (InfoBase) source.getInfo().getValue();
				} else if (uncastedValue instanceof Node) {
					try {
						Unmarshaller unmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
						@SuppressWarnings("unchecked")
						JAXBElement<InfoBase> jaxbElement = (JAXBElement<InfoBase>) 
								unmarshaller.unmarshal((Node) uncastedValue);
						castedValue = jaxbElement.getValue();
					} catch (JAXBException e) {
						logger.error("Error while creating JAXBContext/Unmarshller for InfoBase: {}", e);
					}
				}
				destination.setInfo(
						infoBaseToPassengerBaseInfoDataConverter.convert(
								castedValue));
			}
			
			List<TicketInfoData> tickets = null;
			if (source.getTickets() != null && source.getTickets().getValue() != null) {
				tickets = new ArrayList<TicketInfoData>();
				for (Object ticketInfo : source.getTickets().getValue().getAnyType()) {
					tickets.add(
							ticketInfoToTicketInfoDataConverter.convert(
									(TicketInfo) ticketInfo));
				}
			}
			destination.setTickets(
					tickets);
		
		}
		
		return destination;
	}

}
