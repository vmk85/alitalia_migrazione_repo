package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryCouponData;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.Coupons;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd3.CompartimentalClass;


@Component(immediate=true, metatype=false)
@Service(value=CouponsToCheckinAncillaryCouponData.class)
public class CouponsToCheckinAncillaryCouponData implements Converter<Coupons, CheckinAncillaryCouponData> {

	@Override
	public CheckinAncillaryCouponData convert(Coupons source) {
		CheckinAncillaryCouponData destination = null;

		if (source != null) {
			destination = new CheckinAncillaryCouponData();

			destination.setBaggageAllowance(source.getX003CBaggageAllowanceX003EKBackingField());
			destination.setFareClass(source.getX003CFareClassX003EKBackingField());
			destination.setFlightId(source.getX003CFlightIdX003EKBackingField());
			destination.setFlightNumber(source.getX003CFlightNumberX003EKBackingField());
			destination.setInboundCouponNumber(source.getX003CInboundCouponNumberX003EKBackingField());
			destination.setNumber(source.getX003CNumberX003EKBackingField());
			destination.setPassengerNumber(source.getX003CPassengerNumberX003EKBackingField());
			destination.setIsChild(source.isX003CIsChildX003EKBackingField());
			destination.setIsInfant(source.isX003CIsInfantX003EKBackingField());
			destination.setIsInfantAccompanist(source.isX003CIsInfantAccompanistX003EKBackingField());
			destination.setIsInterline(source.isX003CIsInterlineX003EKBackingField());
			destination.setIsSeatAssignPayment(source.isX003CIsSeatAssignPaymentX003EKBackingField());
			destination.setIsSeatAssignDisabled(source.isX003CIsSeatAssignDisabledX003EKBackingField());
			destination.setIsSpecialPassenger(source.isX003CIsSpecialPassengerX003EKBackingField());
			destination.setSeatNumber(source.getX003CSeatNumberX003EKBackingField());
			List<MmbCompartimentalClassEnum> compClasses = new ArrayList<>();
			if (source.getX003CUpgradeCompartimentalClassesX003EKBackingField()!=null &&
					source.getX003CUpgradeCompartimentalClassesX003EKBackingField().getCompartimentalClass()!=null){
				for (CompartimentalClass compClass : source.getX003CUpgradeCompartimentalClassesX003EKBackingField().getCompartimentalClass()){
					compClasses.add(MmbCompartimentalClassEnum.fromValue(compClass.value()));
				}
			}
			destination.setUpgradeCompartimentalClasses(compClasses);
		}

		return destination;
	}

}
