package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.data.home.TicketInfoData;
import com.alitalia.aem.ws.booking.ticketservice.xsd2.ArrayOfanyType;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.AdultPassenger;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ObjectFactory;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.PassengerType;

@Component(immediate = true, metatype = false)
@Service(value = AdultPassengerDataToAdultPassenger.class)
public class AdultPassengerDataToAdultPassenger implements Converter<AdultPassengerData, AdultPassenger> {

	@Reference
	private PassengerInfoBaseDataToInfoBase infoBaseDataToInfoBaseConverter;

	@Reference
	private PreferencesDataToPreferences preferencesDataToPreferencesConverter;

	@Reference
	private FrequentFlyerTypeDataToFrequentFlyerType frequentFlyerTypeDataToFrequentFlyerTypeConverter;

	@Reference
	private TicketInfoDataToTicketInfo ticketInfoDataToTicketInfoConverter;

	@Override
	public AdultPassenger convert(AdultPassengerData source) {

		AdultPassenger destination = null;
		if (source != null) {
			ObjectFactory factory5 = new ObjectFactory();
			com.alitalia.aem.ws.booking.ticketservice.xsd2.ObjectFactory factory2 = new com.alitalia.aem.ws.booking.ticketservice.xsd2.ObjectFactory();

			destination = factory5.createAdultPassenger();

			// AdultPassenger data

			destination.setFrequentFlyerCode(factory5.createAdultPassengerFrequentFlyerCode(source
					.getFrequentFlyerCode()));

			destination.setFrequentFlyerTier(factory5.createAdultPassengerFrequentFlyerTier(source
					.getFrequentFlyerTier()));

			destination.setFrequentFlyerType(factory5
					.createAdultPassengerFrequentFlyerType(frequentFlyerTypeDataToFrequentFlyerTypeConverter
							.convert(source.getFrequentFlyerType())));

			// ARegularPassenger data
			destination.setPreferences(factory5
					.createARegularPassengerPreferences(preferencesDataToPreferencesConverter.convert(source
							.getPreferences())));

			// APassengerBase data
			destination.setLastName(factory5.createAPassengerBaseLastName(source.getLastName()));

			destination.setName(factory5.createAPassengerBaseName(source.getName()));

			destination.setCouponPrice(source.getCouponPrice());

			destination.setExtraCharge(source.getExtraCharge());

			destination.setFee(source.getFee());

			destination.setGrossFare(source.getGrossFare());

			destination.setNetFare(source.getNetFare());

			destination.setInfo(factory5.createAPassengerBaseInfo(infoBaseDataToInfoBaseConverter.convert(source
					.getInfo())));

			List<TicketInfoData> sourceTickets = source.getTickets();
			if (sourceTickets != null) {
				ArrayOfanyType tickets = factory2.createArrayOfanyType();
				for (TicketInfoData ticketInfoData : sourceTickets) {
					tickets.getAnyType().add(ticketInfoDataToTicketInfoConverter.convert(ticketInfoData));
				}
				destination.setTickets(factory5.createAPassengerBaseTickets(tickets));
			}
			destination.setType(PassengerType.fromValue(source.getType().value()));
		}

		return destination;
	}

}
