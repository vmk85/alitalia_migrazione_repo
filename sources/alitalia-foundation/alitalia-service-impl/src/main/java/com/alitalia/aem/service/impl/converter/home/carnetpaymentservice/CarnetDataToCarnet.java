package com.alitalia.aem.service.impl.converter.home.carnetpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.carnet.CarnetData;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.Carnet;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.ObjectFactory;



@Component(immediate=true, metatype=false)
@Service(value=CarnetDataToCarnet.class)
public class CarnetDataToCarnet implements Converter<CarnetData, Carnet> {

	@Override
	public Carnet convert(CarnetData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		Carnet destination = null;

		if (source != null) {
			destination = objectFactory.createCarnet();
			destination.setChangeDescription(objectFactory.createCarnetChangeDescription(source.getChangeDescription()));
			destination.setCode(objectFactory.createCarnetCode(source.getCode()));
			destination.setCodeEMD(objectFactory.createCarnetCodeEMD(source.getCodeEMD()));
			destination.setCurrency(objectFactory.createCarnetCurrency(source.getCurrency()));
			destination.setIncrementPrice(source.getIncrementPrice());
			destination.setPrenotationClass(objectFactory.createCarnetPrenotationClass(source.getPrenotationClass()));
			destination.setPrice(source.getPrice());
			destination.setQuantity(source.getQuantity());
			destination.setRedemption(objectFactory.createCarnetRedemption(source.getRedemption()));
			destination.setValidity(source.getValidity());
		}
		
		return destination;
	}
}