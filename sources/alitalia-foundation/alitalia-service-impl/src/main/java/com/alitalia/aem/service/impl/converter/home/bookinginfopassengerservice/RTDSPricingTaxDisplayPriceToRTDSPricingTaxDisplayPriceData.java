package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingTaxDisplayPriceData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionPricingTaxDisplayPrice;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingTaxDisplayPriceToRTDSPricingTaxDisplayPriceData.class)
public class RTDSPricingTaxDisplayPriceToRTDSPricingTaxDisplayPriceData implements
		Converter<ResultTicketingDetailSolutionPricingTaxDisplayPrice, 
					ResultTicketingDetailSolutionPricingTaxDisplayPriceData> {

	@Override
	public ResultTicketingDetailSolutionPricingTaxDisplayPriceData convert(
			ResultTicketingDetailSolutionPricingTaxDisplayPrice source) {
		ResultTicketingDetailSolutionPricingTaxDisplayPriceData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingTaxDisplayPriceData();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
