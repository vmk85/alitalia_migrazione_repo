package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.CashAndMilesData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.CashAndMiles;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=CashAndMilesDataToCashAndMiles.class)
public class CashAndMilesDataToCashAndMiles 
		implements Converter<CashAndMilesData, CashAndMiles> {
	
	@Reference
	private MmCustomerDataToMmCustomer mmCustomerDataToMmCustomerConverter;
	
	@Override
	public CashAndMiles convert(CashAndMilesData source) {
		
		CashAndMiles destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			destination = objectFactory.createCashAndMiles();
			
			destination.setDiscountAmount(
					source.getDiscountAmount());
			
			destination.setMMCustomer(
					objectFactory.createCashAndMilesMMCustomer(
							mmCustomerDataToMmCustomerConverter.convert(
									source.getMmCustomer())));
			
			destination.setPaymentDate(
					XsdConvertUtils.toXMLGregorianCalendar(
							source.getPaymentDate()));
			
			destination.setUsedMileage(
					source.getUsedMileage());
			
		}
		
		return destination;
	}

}
