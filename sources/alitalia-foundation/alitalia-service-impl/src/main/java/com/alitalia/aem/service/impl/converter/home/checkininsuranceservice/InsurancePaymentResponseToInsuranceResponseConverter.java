package com.alitalia.aem.service.impl.converter.home.checkininsuranceservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.CheckinInsuranceResponse;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd1.InsurancePaymentResponse;


@Component(immediate=true, metatype=false)
@Service(value=InsurancePaymentResponseToInsuranceResponseConverter.class)
public class InsurancePaymentResponseToInsuranceResponseConverter implements Converter<InsurancePaymentResponse, CheckinInsuranceResponse> {

	@Reference
	private InsurancePolicyToInsurancePolicyData checkinInsurancePolicyConverter;
	
	@Override
	public CheckinInsuranceResponse convert(InsurancePaymentResponse source) {
		
		CheckinInsuranceResponse response = new CheckinInsuranceResponse();
		response.setPolicy(checkinInsurancePolicyConverter.convert(source.getInsurancePolicy().getValue()));
		return response;			
	}
}