package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.CarnetSendNotificationEmailRequest;
import com.alitalia.aem.ws.booking.ticketservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.booking.ticketservice.xsd1.SendCarnetNotificationEmailRequest;

@Component(immediate=true, metatype= false)
@Service(value=CarnetSendNotificationEmailRequestToSendCarnetNotificationEmailRequest.class)
public class CarnetSendNotificationEmailRequestToSendCarnetNotificationEmailRequest implements Converter<CarnetSendNotificationEmailRequest, SendCarnetNotificationEmailRequest> {

	@Reference
	private RoutesDataToRoutes routesDataConverter;
	
	@Reference
	private InfoCarnetDataToInfoCarnet infoCarnetDataConverter;

	@Override
	public SendCarnetNotificationEmailRequest convert(CarnetSendNotificationEmailRequest source) {
		ObjectFactory objectFactory1 = new ObjectFactory();

		SendCarnetNotificationEmailRequest destination = objectFactory1.createSendCarnetNotificationEmailRequest();

		destination.setLanguageCode(objectFactory1.createSendCarnetNotificationEmailRequestLanguageCode(source.getLanguageCode()));
		destination.setMarketCode(objectFactory1.createSendCarnetNotificationEmailRequestMarketCode(source.getMarketCode()));
		destination.setNotificationMailHtml(objectFactory1.createSendCarnetNotificationEmailRequestNotificationMailHtml(source.getNotificationMailHtml()));
		destination.setPrenotation(objectFactory1.createSendCarnetNotificationEmailRequestPrenotation(routesDataConverter.convert(source.getPrenotation())));
		destination.setCarnetInfo(objectFactory1.createSendCarnetNotificationEmailRequestCarnetInfo(infoCarnetDataConverter.convert(source.getCarnetInfo())));

		return destination;
	}
}