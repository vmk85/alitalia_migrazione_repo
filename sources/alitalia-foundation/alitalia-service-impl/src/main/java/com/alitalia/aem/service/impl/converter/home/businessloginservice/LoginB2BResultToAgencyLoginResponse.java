package com.alitalia.aem.service.impl.converter.home.businessloginservice;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AlitaliaTradeAgencyData;
import com.alitalia.aem.common.messages.home.AgencyLoginResponse;
import com.alitalia.aem.ws.b2bv2.xsd.LoginB2BResponse.LoginB2BResult;
import com.alitalia.aem.ws.b2bv2.xsd1.AlitaliaTrade;

@Component
@Service(value=LoginB2BResultToAgencyLoginResponse.class)
public class LoginB2BResultToAgencyLoginResponse implements Converter<LoginB2BResult, AgencyLoginResponse> {

	private static final Logger logger = LoggerFactory.getLogger(LoginB2BResultToAgencyLoginResponse.class);

	@Reference
	private AlitaliaTradeToAlitaliaTradeAgencyData alitaliaTradeConverter;
	
	@Reference
	private BusinessLoginServiceJAXBContextFactory jaxbContextFactory;

	@Override
	public AgencyLoginResponse convert(LoginB2BResult source) {
		AlitaliaTradeAgencyData agencyData = null;
		Boolean loginSuccessful = false;
		Boolean firstAccess = null;
		Boolean passwordExpired = null;
		Boolean accountLocked = null;
		Boolean approvalRequired = null;

		AlitaliaTrade unmarshalledObj = null;
		if (source.getAny() instanceof AlitaliaTrade) {
			unmarshalledObj = (AlitaliaTrade) source.getAny();
		}
		else {
			try {
				Unmarshaller loginUnmashaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
				unmarshalledObj = null;
				Node responseElement = (Node) source.getAny();
				unmarshalledObj = (AlitaliaTrade) loginUnmashaller.unmarshal(responseElement);
			} catch (JAXBException e) {
				logger.error("Error instantiating JAXBContext/Unmarshaller: {}", e);
			}
		}

		if (unmarshalledObj != null) {
			agencyData = alitaliaTradeConverter.convert(unmarshalledObj);
			loginSuccessful = true;
			firstAccess = agencyData.isNuovoUtente();
			passwordExpired = agencyData.isPasswordExpired() || agencyData.isNuovoUtente();
			accountLocked = agencyData.isAccountLocked();
			approvalRequired = agencyData.isCondizioniAccettate() == null ? null : !agencyData.isCondizioniAccettate();
		}

		AgencyLoginResponse destination = 
				new AgencyLoginResponse(agencyData, loginSuccessful, firstAccess, passwordExpired, accountLocked, approvalRequired);

		return destination;
	}

}
