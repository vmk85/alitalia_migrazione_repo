package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.PassengerBase;
import com.alitalia.aem.common.data.home.FullTextSearchData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.PropertiesData;
import com.alitalia.aem.common.data.home.RefreshSearchData;
import com.alitalia.aem.ws.booking.searchservice.xsd2.FullTextRulesSearch;
import com.alitalia.aem.ws.booking.searchservice.xsd2.RefreshSearch;
import com.alitalia.aem.ws.booking.searchservice.xsd3.APassengerBase;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ArrayOfAPassengerBase;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ArrayOfDictionaryItem;
import com.alitalia.aem.ws.booking.searchservice.xsd3.GatewayType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.RefreshType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.RouteType;

@Component(immediate=true, metatype=false)
@Service(value=FullTextRulesSearchDataToFullTextRulesSearch.class)
public class FullTextRulesSearchDataToFullTextRulesSearch implements Converter<FullTextSearchData, FullTextRulesSearch> {

	@Reference
	private PassengerBaseDataToAPassengerBase passengerBaseDataConverter;

	@Reference
	private PropertiesDataToArrayOfDictionaryItem propertiesDataConverter;

	@Override
	public FullTextRulesSearch convert(FullTextSearchData source) {
		com.alitalia.aem.ws.booking.searchservice.xsd2.ObjectFactory factory2 = 
				new com.alitalia.aem.ws.booking.searchservice.xsd2.ObjectFactory();
		com.alitalia.aem.ws.booking.searchservice.xsd3.ObjectFactory factory3 = 
				new com.alitalia.aem.ws.booking.searchservice.xsd3.ObjectFactory();
		
		FullTextRulesSearch destination = factory2.createFullTextRulesSearch();

		destination.setFarmId(factory3.createABoomBoxInfoFarmId(source.getFarmId()));
		destination.setId(factory3.createABoomBoxInfoId(source.getId()));
		destination.setSessionId(factory3.createABoomBoxInfoSessionId(source.getSessionId()));
		destination.setSolutionId(factory2.createGateWayBrandSearchSolutionId(source.getSolutionId()));
		destination.setSolutionSet(factory3.createABoomBoxInfoSolutionSet(source.getSolutionSet()));
		destination.setType(GatewayType.FULL_TEXT_RULES_SEARCH);

		ArrayOfAPassengerBase passengers = null;
		if (source.getPassengers() != null) {
			passengers = factory3.createArrayOfAPassengerBase();
			for (PassengerBase sourcePassengerBase : source.getPassengers()) {
				APassengerBase aPassengerBase = passengerBaseDataConverter.convert((PassengerBaseData) sourcePassengerBase);
				passengers.getAPassengerBase().add(aPassengerBase);
			}
		}
		destination.setPassengers(factory2.createGateWayBrandSearchPassengers(passengers));

		PropertiesData sourceProperties = source.getProperties();
		if (sourceProperties != null) {
			ArrayOfDictionaryItem properties = propertiesDataConverter.convert(sourceProperties);
			destination.setProperties(factory3.createABoomBoxGenericInfoProperties(properties));
		}
		else
			destination.setProperties(factory3.createABoomBoxGenericInfoProperties(null));

		return destination;
	}

}
