package com.alitalia.aem.service.impl.converter.home.carnetpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.UserInfoBaseData;
import com.alitalia.aem.common.data.home.UserInfoCompleteData;
import com.alitalia.aem.common.data.home.UserInfoStandardData;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.ObjectFactory;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.AUserInfoBase;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.UserInfoComplete;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.UserInfoStandard;


@Component(immediate=true, metatype=false)
@Service(value=UserInfoBaseToAUserInfo.class)
public class UserInfoBaseToAUserInfo 
		implements Converter<UserInfoBaseData, AUserInfoBase> {
	
	@Override
	public AUserInfoBase convert(UserInfoBaseData source) {
		
		AUserInfoBase destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			if (source instanceof UserInfoCompleteData) {
				UserInfoCompleteData typedSource = (UserInfoCompleteData) source;
				UserInfoComplete typedDestination =  new UserInfoComplete();
				destination = typedDestination;
				
				typedDestination.setAddress(objectFactory.createUserInfoCompleteAddress(typedSource.getAdress()));
				
				typedDestination.setCAP(
						objectFactory.createUserInfoCompleteCAP(
								typedSource.getCap()));
				
				typedDestination.setCity(
						objectFactory.createUserInfoCompleteCity(
								typedSource.getCity()));
				
				typedDestination.setCountry(
						objectFactory.createUserInfoCompleteCountry(
								typedSource.getCountry()));
				
			} else if (source instanceof UserInfoStandardData) {
				UserInfoStandard typedDestination =  new UserInfoStandard();
				destination = typedDestination;
				
			} else {
				destination = new AUserInfoBase();
				
			}
			
			destination.setLastname(
					objectFactory.createAUserInfoBaseLastname(
							source.getLastname()));

			destination.setName(
					objectFactory.createAUserInfoBaseName(
							source.getName()));
			
		}
		
		return destination;
	}

}
