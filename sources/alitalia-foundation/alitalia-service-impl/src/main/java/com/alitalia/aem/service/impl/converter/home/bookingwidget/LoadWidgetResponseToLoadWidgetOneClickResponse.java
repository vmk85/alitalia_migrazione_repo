package com.alitalia.aem.service.impl.converter.home.bookingwidget;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMCreditCardData;
import com.alitalia.aem.common.messages.home.LoadWidgetOneClickResponse;
import com.alitalia.aem.ws.booking.widgetservice.xsd1.LoadOneClickResponse;
import com.alitalia.aem.ws.booking.widgetservice.xsd8.ArrayOfCreditCard;

@Component(immediate=true, metatype=false)
@Service(value=LoadWidgetResponseToLoadWidgetOneClickResponse.class)
public class LoadWidgetResponseToLoadWidgetOneClickResponse implements Converter<LoadOneClickResponse, LoadWidgetOneClickResponse> {

	@Reference
	private CreditCardsToMMCreditCardsData creditCardsConverter;

	@Override
	public LoadWidgetOneClickResponse convert(LoadOneClickResponse source) {
		
		LoadWidgetOneClickResponse response = new LoadWidgetOneClickResponse();

		if (source != null && source.getCreditCards() != null) {

			ArrayOfCreditCard creditCards = source.getCreditCards().getValue();
			List<MMCreditCardData> creditCardsDTO = creditCardsConverter.convert(creditCards);
			response.setCreditCards(creditCardsDTO);

		}

		return response;
	}
}
