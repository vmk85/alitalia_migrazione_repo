package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentPricingFareData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionSliceSegmentPricingFare;

@Component(immediate=true, metatype=false)
@Service(value=ResultTicketingDetailSolutionSliceSegmentPricingFareToResultTicketingDetailSolutionSliceSegmentPricingFareData.class)
public class ResultTicketingDetailSolutionSliceSegmentPricingFareToResultTicketingDetailSolutionSliceSegmentPricingFareData implements
		Converter<ResultTicketingDetailSolutionSliceSegmentPricingFare, ResultTicketingDetailSolutionSliceSegmentPricingFareData> {

	@Override
	public ResultTicketingDetailSolutionSliceSegmentPricingFareData convert(
			ResultTicketingDetailSolutionSliceSegmentPricingFare source) {
		ResultTicketingDetailSolutionSliceSegmentPricingFareData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionSliceSegmentPricingFareData();

			destination.setCarrierField(source.getCarrierField());
			destination.setCity1Field(source.getCity1Field());
			destination.setCity2Field(source.getCity2Field());
			destination.setExtendedFareCodeField(source.getExtendedFareCodeField());
		}

		return destination;
	}

}
