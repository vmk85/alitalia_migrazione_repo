package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareCalculationData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareRestrictionData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingNoteData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingPenaltyPriceData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingRefundPriceData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingReissueInfoData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingTaxData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ArrayOfresultBookingDetailsSolutionPricingBookingInfo;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ArrayOfresultBookingDetailsSolutionPricingFare;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ArrayOfresultBookingDetailsSolutionPricingFareCalculation;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ArrayOfresultBookingDetailsSolutionPricingFareRestriction;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ArrayOfresultBookingDetailsSolutionPricingNote;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ArrayOfresultBookingDetailsSolutionPricingPenaltyPrice;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ArrayOfresultBookingDetailsSolutionPricingRefundPrice;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ArrayOfresultBookingDetailsSolutionPricingReissueInfo;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ArrayOfresultBookingDetailsSolutionPricingTax;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ResultBookingDetailsSolutionPricing;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingDataToPricing.class)
public class RBDSPricingDataToPricing implements Converter<ResultBookingDetailsSolutionPricingData, ResultBookingDetailsSolutionPricing> {

	@Reference
	private RBDSPricingAddCollectPriceDataToPricingAddCollectPrice rbdsPricingAddCollectPriceConverter;
	
	@Reference
	private RBDSPricingBookingInfoDataToPricingBookingInfo rbdsPricingBookingInfoConverter;
	 
	@Reference
	private RBDSPricingCocFareDifferenceDataToPricingCocFareDifference rbdsPricingCocFareDifferenceConverter;
	
	@Reference
	private RBDSPricingCocFareTotalDataToPricingCocFareTotal cocFareTotalFieldConverter;
	
	@Reference
	private RBDSPricingExtDataToPricingExt extFieldConverter;
	
	@Reference
	private RBDSPricingFareCalculationDataToPricingFareCalculationConverter rbdsPricingFareCalculationConverter;
	
	@Reference
	private RBDSPricingFareDataToPricingFare rbdsPricingFareFieldConverter;
	
	@Reference
	private RBDSPricingFareRestrictionDataToPricingFareRestriction rbdsPricingFareRestrictionConvert;
	
	@Reference
	private RBDSPricingNoteDataToPricingNote noteFieldConverter;
	
	@Reference
	private RBDSPricingPenaltyPriceDataToPricingPenaltyPrice penaltyPriceFieldConverter;
	
	@Reference
	private RBDSPricingPreviousCocFareTotalDataToPricingPreviousCocFareTotal previousCocFareTotalFieldConverter;
	
	@Reference
	private RBDSPricingRefundPriceDataToPricingRefundPrice refundPriceConverter; 
	 
	@Reference
	private RBDSPricingReissueInfoDataToPricingReissueInfo pricingReissueInfoConverter;
	
	@Reference
	private RBDSPricingReissueTaxTotalDifferenceDataToPricingReissueTaxTotalDifference reissueTaxTotalDifferenceFieldConverter;
	
	@Reference
	private RBDSPricingSaleFareTotalDataToPricingSaleFareTotal saleFareTotalFieldConverter;
	
	@Reference
	private RBDSPricingSalePriceDataToPricingSalePrice salePriceFieldConverter;
	
	@Reference
	private RBDSPricingSaleTaxTotalDataToPricingSaleTaxTotal saleTaxTotalFieldConverter;
	
	@Reference
	private RBDSPricingTaxDataToPricingTax taxFieldConverter;
	
	@Override
	public ResultBookingDetailsSolutionPricing convert(ResultBookingDetailsSolutionPricingData source) {
		ResultBookingDetailsSolutionPricing destination = null;
		
		if(source!=null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionPricing();
			
		    destination.setAddCollectPriceField(rbdsPricingAddCollectPriceConverter.convert(source.getAddCollectPriceField()));
		    ArrayOfresultBookingDetailsSolutionPricingBookingInfo arrayOfRBDSPricingBookingInfo = objfact.createArrayOfresultBookingDetailsSolutionPricingBookingInfo();
		    if(source.getBookingInfoField()!=null)
		    for(ResultBookingDetailsSolutionPricingBookingInfoData s : source.getBookingInfoField()){
		    	arrayOfRBDSPricingBookingInfo.getResultBookingDetailsSolutionPricingBookingInfo().add(rbdsPricingBookingInfoConverter.convert(s));
		    }
		    destination.setBookingInfoField(arrayOfRBDSPricingBookingInfo);
		    destination.setCocFareDifferenceField(rbdsPricingCocFareDifferenceConverter.convert(source.getCocFareDifferenceField()));
		    destination.setCocFareTotalField(cocFareTotalFieldConverter.convert(source.getCocFareTotalField()));
		    destination.setExtField(extFieldConverter.convert(source.getExtField()));
		    
		    ArrayOfresultBookingDetailsSolutionPricingFareCalculation fareCalculationField = objfact.createArrayOfresultBookingDetailsSolutionPricingFareCalculation();
		    if(source.getFareCalculationField()!=null)
		    for(ResultBookingDetailsSolutionPricingFareCalculationData s : source.getFareCalculationField())
		    	 fareCalculationField.getResultBookingDetailsSolutionPricingFareCalculation().add(rbdsPricingFareCalculationConverter.convert(s));
		    destination.setFareCalculationField(fareCalculationField);
		    
		    ArrayOfresultBookingDetailsSolutionPricingFare fareField = objfact.createArrayOfresultBookingDetailsSolutionPricingFare();
		    if(source.getFareField()!=null)
		    for(ResultBookingDetailsSolutionPricingFareData s : source.getFareField())
		    	fareField.getResultBookingDetailsSolutionPricingFare().add(rbdsPricingFareFieldConverter.convert(s));
		    destination.setFareField(fareField);
		    
		    ArrayOfresultBookingDetailsSolutionPricingFareRestriction fareRestrictionField = objfact.createArrayOfresultBookingDetailsSolutionPricingFareRestriction();
		    if(source.getFareRestrictionField()!=null)
		    for(ResultBookingDetailsSolutionPricingFareRestrictionData s : source.getFareRestrictionField())
		    	fareRestrictionField.getResultBookingDetailsSolutionPricingFareRestriction().add(rbdsPricingFareRestrictionConvert.convert(s));
		    destination.setFareRestrictionField(fareRestrictionField);
		    
		    ArrayOfresultBookingDetailsSolutionPricingNote noteField = objfact.createArrayOfresultBookingDetailsSolutionPricingNote();
		    if(source.getNoteField()!=null)
		    for(ResultBookingDetailsSolutionPricingNoteData s : source.getNoteField())
		    	noteField.getResultBookingDetailsSolutionPricingNote().add(noteFieldConverter.convert(s));
		    destination.setNoteField(noteField);
		    destination.setPaxCountField(source.getPaxCountField());
		    
		    ArrayOfresultBookingDetailsSolutionPricingPenaltyPrice penaltyPriceField = objfact.createArrayOfresultBookingDetailsSolutionPricingPenaltyPrice();
		    if(source.getPenaltyPriceField()!=null)
		    for(ResultBookingDetailsSolutionPricingPenaltyPriceData s : source.getPenaltyPriceField())
		    	penaltyPriceField.getResultBookingDetailsSolutionPricingPenaltyPrice().add(penaltyPriceFieldConverter.convert(s));
		    destination.setPenaltyPriceField(penaltyPriceField);
		    
		    destination.setPreviousCocFareTotalField(previousCocFareTotalFieldConverter.convert(source.getPreviousCocFareTotalField()));
		    
		    ArrayOfresultBookingDetailsSolutionPricingRefundPrice refundPriceField = objfact.createArrayOfresultBookingDetailsSolutionPricingRefundPrice();
		    if(source.getRefundPriceField()!=null)
		    for(ResultBookingDetailsSolutionPricingRefundPriceData s : source.getRefundPriceField())
		    	refundPriceField.getResultBookingDetailsSolutionPricingRefundPrice().add(refundPriceConverter.convert(s));
		    destination.setRefundPriceField(refundPriceField);
		  
		    ArrayOfresultBookingDetailsSolutionPricingReissueInfo reissueInfoField = objfact.createArrayOfresultBookingDetailsSolutionPricingReissueInfo();
		    if(source.getReissueInfoField()!=null)
		    for(ResultBookingDetailsSolutionPricingReissueInfoData s : source.getReissueInfoField())
		    	reissueInfoField.getResultBookingDetailsSolutionPricingReissueInfo().add(pricingReissueInfoConverter.convert(s));
		    destination.setReissueInfoField(reissueInfoField);
		    
		    
		    destination.setReissueTaxTotalDifferenceField(reissueTaxTotalDifferenceFieldConverter.convert(source.getReissueTaxTotalDifferenceField()));
		    destination.setSaleFareTotalField(saleFareTotalFieldConverter.convert(source.getSaleFareTotalField()));
		    
		    destination.setSalePriceField(salePriceFieldConverter.convert(source.getSalePriceField()));
		    destination.setSaleTaxTotalField(saleTaxTotalFieldConverter.convert(source.getSaleTaxTotalField()));
		    
		    ArrayOfresultBookingDetailsSolutionPricingTax taxField = objfact.createArrayOfresultBookingDetailsSolutionPricingTax();
		    if(source.getTaxField()!=null)
		    for(ResultBookingDetailsSolutionPricingTaxData s : source.getTaxField())
		    	taxField.getResultBookingDetailsSolutionPricingTax().add(taxFieldConverter.convert(s));
		    destination.setTaxField(taxField);
			
			
		}
		return destination;
		
	}

	
}
