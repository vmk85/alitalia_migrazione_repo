package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentLegDistanceData;
import com.alitalia.aem.common.data.home.enumerations.DistanceUnitsEnum;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.DistanceUnits;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionSliceSegmentLegDistance;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentLegDistanceDataToRTDSliceSegmentLegDistance.class)
public class RTDSSliceSegmentLegDistanceDataToRTDSliceSegmentLegDistance implements
		Converter<ResultTicketingDetailSolutionSliceSegmentLegDistanceData, 
					ResultTicketingDetailSolutionSliceSegmentLegDistance> {

	@Override
	public ResultTicketingDetailSolutionSliceSegmentLegDistance convert(
			ResultTicketingDetailSolutionSliceSegmentLegDistanceData source) {
		ResultTicketingDetailSolutionSliceSegmentLegDistance destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionSliceSegmentLegDistance();

			DistanceUnitsEnum sourceUnitsField = source.getUnitsField();
			if (sourceUnitsField != null)
				destination.setUnitsField(DistanceUnits.fromValue(sourceUnitsField.value()));

			destination.setValueField(source.getValueField());
		}

		return destination;
	}

}
