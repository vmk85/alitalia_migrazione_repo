package com.alitalia.aem.service.impl.converter.home.bookingancillaryservice;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ApplicantPassengerData;
import com.alitalia.aem.common.data.home.ContactData;
import com.alitalia.aem.common.data.home.TicketInfoData;
import com.alitalia.aem.ws.booking.ancillaryservice.xsd2.ARTaxInfoTypes;
import com.alitalia.aem.ws.booking.ancillaryservice.xsd2.ApplicantPassenger;
import com.alitalia.aem.ws.booking.ancillaryservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.booking.ancillaryservice.xsd2.PassengerType;
import com.alitalia.aem.ws.booking.ancillaryservice.xsd3.ArrayOfanyType;

@Component(immediate=true, metatype=false)
@Service(value=ApplicantPassengerDataToApplicantPassenger.class)
public class ApplicantPassengerDataToApplicantPassenger 
		implements Converter<ApplicantPassengerData, ApplicantPassenger> {
	
	@Reference
	private PassengerInfoBaseDataToInfoBase infoBaseDataToInfoBaseConverter;
	
	@Reference
	private PreferencesDataToPreferences preferencesDataToPreferencesConverter;
	
	@Reference
	private FrequentFlyerTypeDataToFrequentFlyerType frequentFlyerTypeDataToFrequentFlyerTypeConverter;
	
	@Reference
	private TicketInfoDataToTicketInfo ticketInfoDataToTicketInfoConverter;
	
	@Reference
	private ContactDataToContact contactDataToContactConverter;
	
	@Override
	public ApplicantPassenger convert(ApplicantPassengerData source) {
		
		ApplicantPassenger destination = null;
		
		if (source != null) {
			
			ObjectFactory factory2 = new ObjectFactory();
			com.alitalia.aem.ws.booking.ancillaryservice.xsd3.ObjectFactory factory3 = 
					new com.alitalia.aem.ws.booking.ancillaryservice.xsd3.ObjectFactory();
			
			destination = factory2.createApplicantPassenger();
			
			// ApplicantPassenger data
			
			if (source.getArTaxInfoType() != null) {
				destination.setARTaxInfoType(ARTaxInfoTypes.fromValue(source.getArTaxInfoType().value()));
			}
			
			destination.setBlueBizCode(
					factory2.createApplicantPassengerBlueBizCode(
							source.getBlueBizCode()));
			
			List<ContactData> sourceContacts = source.getContact();
			ArrayOfanyType contacts = factory3.createArrayOfanyType();
			if (sourceContacts != null) {
				for (ContactData contact : sourceContacts) {
					contacts.getAnyType().add(
							contactDataToContactConverter.convert(contact));
				}
			}
			destination.setContact(factory2.createApplicantPassengerContact(contacts));

			destination.setCountry(factory2.createApplicantPassengerCountry(source.getCountry()));
			
			destination.setCouponPrice(source.getCouponPrice());
			
			destination.setCUIT(factory2.createApplicantPassengerCUIT(source.getCuit()));
			
			destination.setEmail(factory2.createApplicantPassengerEmail(source.getEmail()));
			
			destination.setExtraCharge(source.getExtraCharge());
			
			destination.setFee(source.getFee());
			
			// AdultPassenger data
			
			destination.setFrequentFlyerCode(factory2.createAdultPassengerFrequentFlyerCode(source.getFrequentFlyerCode()));
			
			destination.setFrequentFlyerTier(factory2.createAdultPassengerFrequentFlyerTier(source.getFrequentFlyerTier()));
			
			destination.setFrequentFlyerType(factory2.createAdultPassengerFrequentFlyerType(
							frequentFlyerTypeDataToFrequentFlyerTypeConverter.convert(source.getFrequentFlyerType())));
			
			// ARegularPassenger data
			
			destination.setPreferences(factory2.createARegularPassengerPreferences(
							preferencesDataToPreferencesConverter.convert(source.getPreferences())));
			
			// APassengerBase data
			
			destination.setLastName(factory2.createAPassengerBaseLastName(source.getLastName()));
			
			destination.setName(factory2.createAPassengerBaseName(source.getName()));
			
			destination.setCouponPrice(source.getCouponPrice());
			
			destination.setExtraCharge(source.getExtraCharge());
			
			destination.setFee(source.getFee());
			
			destination.setGrossFare(source.getGrossFare());
			
			destination.setNetFare(source.getNetFare());
			
			destination.setInfo(factory2.createAPassengerBaseInfo(
							infoBaseDataToInfoBaseConverter.convert(source.getInfo())));
			
			List<TicketInfoData> sourceTickets = source.getTickets();
			if (sourceTickets != null && !source.getTickets().isEmpty()) {
				ArrayOfanyType tickets = factory3.createArrayOfanyType();
				for (TicketInfoData ticketInfoData : sourceTickets) {
					tickets.getAnyType().add(ticketInfoDataToTicketInfoConverter.convert(ticketInfoData));
				}
				destination.setTickets(factory2.createAPassengerBaseTickets(tickets));
			}
			else
				destination.setTickets(factory2.createAPassengerBaseTickets(null));

			destination.setType(PassengerType.fromValue(source.getType().value()));
			destination.setSkyBonusCode(factory2.createApplicantPassengerSkyBonusCode(source.getSkyBonusCode()));
			destination.setSubscribeToNewsletter(factory2.createApplicantPassengerSubscribeToNewsletter(source.isSubscribeToNewsletter()));
		}
		
		return destination;
	}

}
