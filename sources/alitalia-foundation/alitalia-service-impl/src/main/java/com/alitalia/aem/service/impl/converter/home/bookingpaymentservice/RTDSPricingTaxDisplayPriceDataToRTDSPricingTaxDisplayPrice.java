package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingTaxDisplayPriceData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingTaxDisplayPrice;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingTaxDisplayPriceDataToRTDSPricingTaxDisplayPrice.class)
public class RTDSPricingTaxDisplayPriceDataToRTDSPricingTaxDisplayPrice implements
		Converter<ResultTicketingDetailSolutionPricingTaxDisplayPriceData, 
					ResultTicketingDetailSolutionPricingTaxDisplayPrice> {

	@Override
	public ResultTicketingDetailSolutionPricingTaxDisplayPrice convert(
			ResultTicketingDetailSolutionPricingTaxDisplayPriceData source) {
		ResultTicketingDetailSolutionPricingTaxDisplayPrice destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingTaxDisplayPrice();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
