package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentLegOperationalFlightData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionSliceSegmentLegOperationalFlight;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentLegOperationalFlightDataToRTDSSliceSegmentLegOperationalFlight.class)
public class RTDSSliceSegmentLegOperationalFlightDataToRTDSSliceSegmentLegOperationalFlight implements
		Converter<ResultTicketingDetailSolutionSliceSegmentLegOperationalFlightData, 
					ResultTicketingDetailSolutionSliceSegmentLegOperationalFlight> {

	@Override
	public ResultTicketingDetailSolutionSliceSegmentLegOperationalFlight convert(
			ResultTicketingDetailSolutionSliceSegmentLegOperationalFlightData source) {
		ResultTicketingDetailSolutionSliceSegmentLegOperationalFlight destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionSliceSegmentLegOperationalFlight();

			destination.setCarrierField(source.getCarrierField());
			destination.setNumberField(source.getNumberField());
			destination.setNumberFieldSpecified(source.isNumberFieldSpecified());
		}

		return destination;
	}

}
