package com.alitalia.aem.service.impl.converter.home.checkinboardingpassservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.SendBoardingPassCheckinResponse;


@Component(immediate=true, metatype=false)
@Service(value=SendBoardingPassResponseToSendBoardingPassCheckinResponse.class)
public class SendBoardingPassResponseToSendBoardingPassCheckinResponse implements Converter<Boolean, SendBoardingPassCheckinResponse> {


	@Override
	public SendBoardingPassCheckinResponse convert(Boolean source) {
		
		SendBoardingPassCheckinResponse destination = null;

		if (source != null) {
			
			destination = new SendBoardingPassCheckinResponse();
			
			destination.setSendBoardingPassResult(source);		
		}

		return destination;
	}
}
