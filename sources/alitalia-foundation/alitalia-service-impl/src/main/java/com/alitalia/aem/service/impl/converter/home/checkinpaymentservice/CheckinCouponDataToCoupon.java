package com.alitalia.aem.service.impl.converter.home.checkinpaymentservice;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinCouponData;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbGdsTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbSeatStatusEnum;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.checkin.paymentservice.xsd2.ArrayOfCompartimentalClass;
import com.alitalia.aem.ws.checkin.paymentservice.xsd2.CompartimentalClass;
import com.alitalia.aem.ws.checkin.paymentservice.xsd2.Coupon;
import com.alitalia.aem.ws.checkin.paymentservice.xsd2.GdsType;
import com.alitalia.aem.ws.checkin.paymentservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.checkin.paymentservice.xsd2.SeatStatus;


@Component(immediate=true, metatype=false)
@Service(value=CheckinCouponDataToCoupon.class)
public class CheckinCouponDataToCoupon implements Converter<CheckinCouponData, Coupon> {

	@Reference
	private CheckinFlightDataToFlight checkinFlightDataConverter;

	@Override
	public Coupon convert(CheckinCouponData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		Coupon destination = null;

		if (source != null) {
			destination = objectFactory.createCoupon();

			// Il campo Barcode dovrebbe contenere l'immagine del codice a barre ma non e' usato
			destination.setX003CBarcodeX003EKBackingField(source.getBarcode());
			destination.setX003CBoardingPassColorX003EKBackingField(source.getBoardingPassColor());
			if(source.getBoardingPassLoaded() != null){
				destination.setX003CBoardingPassLoadedX003EKBackingField(source.getBoardingPassLoaded());
			}
			destination.setX003CBoardingTimeX003EKBackingField(
					XsdConvertUtils.toXMLGregorianCalendar(source.getBoardingTime()));
			destination.setX003CBoardingZoneX003EKBackingField(source.getBoardingZone());
			if(source.getChangeSeatEnabled() != null){
				destination.setX003CChangeSeatEnabledX003EKBackingField(source.getChangeSeatEnabled());
			}
			if(source.getChangeSeatPayment() != null){
				destination.setX003CChangeSeatPaymentX003EKBackingField(source.getChangeSeatPayment());
			}
			destination.setX003CEticketX003EKBackingField(source.getEticket());
			destination.setX003CFlightIdX003EKBackingField(source.getFlightId());
			destination.setX003CGateX003EKBackingField(source.getGate());

			MmbGdsTypeEnum sourceCouponType = source.getGdsCouponType();
			if (sourceCouponType != null)
				destination.setX003CGdsCouponTypeX003EKBackingField(GdsType.fromValue(sourceCouponType.value()));

			destination.setX003CIdX003EKBackingField(source.getId());
			if (source.getComfortSeatPaid() != null)
				destination.setX003CIsComfortSeatPaidX003EKBackingField(source.getComfortSeatPaid());
			if (source.getInfantCoupon()!=null)
				destination.setX003CIsInfantCouponX003EKBackingField(source.getInfantCoupon());
			if (source.getSkyPriority()!=null)
				destination.setX003CIsSkyPriorityX003EKBackingField(source.getSkyPriority());
			destination.setX003COldSeatX003EKBackingField(source.getOldSeat());
			if(source.getOldSeatClass() != null){
				destination.setX003COldSeatClassX003EKBackingField(CompartimentalClass.fromValue(source.getOldSeatClass().value()));
			}
			destination.setX003COrderNumberX003EKBackingField(source.getOrderNumber());
			destination.setX003CPassengerIdX003EKBackingField(source.getPassengerId());
			destination.setX003CPreviousSeatX003EKBackingField(source.getPreviousSeat());
			destination.setX003CSeatClassNameX003EKBackingField(source.getSeatClassName());

			MmbCompartimentalClassEnum sourceSeatClass = source.getSeatClass();
			if (sourceSeatClass != null)
				destination.setX003CSeatClassX003EKBackingField(CompartimentalClass.fromValue(sourceSeatClass.value()));

			destination.setX003CSeatX003EKBackingField(source.getSeat());
			destination.setX003CSequenceNumberX003EKBackingField(source.getSequenceNumber());
			destination.setX003CServiceCooperatinDetailsX003EKBackingField(source.getServiceCooperationDetails());
			destination.setX003CServiceOperatingDetailsX003EKBackingField(source.getServiceOperatingDetails());
			destination.setX003CSocialCodeX003EKBackingField(source.getSocialCode());
			destination.setX003CSpecialFareX003EKBackingField(source.getSpecialFare());
			destination.setX003CSSRCodeX003EKBackingField(source.getSsrCode());

			MmbSeatStatusEnum sourceStatus = source.getStatus();
			if (sourceStatus != null)
				destination.setX003CStatusX003EKBackingField(SeatStatus.fromValue(sourceStatus.value()));

			destination.setX003CTerminalX003EKBackingField(source.getTerminal());
			
			List<MmbCompartimentalClassEnum> sourceUpgradeEnabledClasses = source.getUpgradeEnabledClasses();
			if(sourceUpgradeEnabledClasses != null){
				ArrayOfCompartimentalClass upgradeEnabledClassList = objectFactory.createArrayOfCompartimentalClass();
				for(MmbCompartimentalClassEnum sourceUpgradeEnabledClassItem : sourceUpgradeEnabledClasses){
					upgradeEnabledClassList.getCompartimentalClass().add(CompartimentalClass.fromValue(sourceUpgradeEnabledClassItem.value()));
				}
				destination.setX003CUpgradeEnabledClassesX003EKBackingField(upgradeEnabledClassList);
			}
			
			if(source.getUpgradeEnabled() != null){
				destination.setX003CUpgradeEnabledX003EKBackingField(source.getUpgradeEnabled());
			}
			
			if(source.getUpgradedAlready() != null){
				destination.setX003CUpgradedAlreadyX003EKBackingField(source.getUpgradedAlready());
			}

			destination.setX003CFlightX003EKBackingField(checkinFlightDataConverter.convert(source.getFlight()));
		}

		return destination;
	}

}
