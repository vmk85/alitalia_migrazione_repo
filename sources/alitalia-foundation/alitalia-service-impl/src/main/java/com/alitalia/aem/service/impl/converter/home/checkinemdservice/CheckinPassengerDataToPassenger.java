package com.alitalia.aem.service.impl.converter.home.checkinemdservice;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinCouponData;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.enumerations.MmbAQQTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbLinkTypeEnum;
import com.alitalia.aem.ws.checkin.emdservice.xsd2.AQQType;
import com.alitalia.aem.ws.checkin.emdservice.xsd2.ArrayOfCoupon;
import com.alitalia.aem.ws.checkin.emdservice.xsd2.CompartimentalClass;
import com.alitalia.aem.ws.checkin.emdservice.xsd2.FrequentFlyerCategory;
import com.alitalia.aem.ws.checkin.emdservice.xsd2.LinkType;
import com.alitalia.aem.ws.checkin.emdservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.checkin.emdservice.xsd2.Passenger;
import com.alitalia.aem.ws.checkin.emdservice.xsd2.PassengerStatus;
import com.alitalia.aem.ws.checkin.emdservice.xsd2.PassengerType;


@Component(immediate=true, metatype=false)
@Service(value=CheckinPassengerDataToPassenger.class)
public class CheckinPassengerDataToPassenger implements Converter<CheckinPassengerData, Passenger> {

	@Reference
	private CheckinApisInfoDataToApisData checkinApisInfoDataConverter;

	@Reference
	private CheckinFrequentFlyerCarrierDataToFrequentFlyerCarrier checkinFrequentFlyerCarrierDataConverter;

	@Reference
	private CheckinBaggageOrderDataToBaggageOrder checkinBaggageOrderDataConverter;

	@Reference
	private CheckinCouponDataToCoupon checkinCouponDataConverter;


	@Override
	public Passenger convert(CheckinPassengerData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		Passenger destination = null;

		if (source != null) {
			destination = objectFactory.createPassenger();

			destination.setX003CIdX003EKBackingField(source.getId());

			destination.setX003CApisDataX003EKBackingField(
					checkinApisInfoDataConverter.convert(source.getApisData()));

			MmbAQQTypeEnum sourceAuthPermssion = source.getAuthorityPermission();
			if (sourceAuthPermssion != null)
				destination.setX003CAuthorityPermissionX003EKBackingField(AQQType.fromValue(sourceAuthPermssion.value()));

			destination.setX003CBaggageAllowanceQuantityX003EKBackingField(source.getBaggageAllowanceQuantity());
			destination.setX003CBaggageAllowanceX003EKBackingField(source.getBaggageAllowance());
			destination.setX003CBoardingPassRetrieveEmailX003EKBackingField(source.getBoardingPassRetrieveEmail());
			destination.setX003CBoardingPassRetrieveSmsX003EKBackingField(source.getBoardingPassRetrieveSms());

			if (source.getBoardingPassViaEmail() != null)
				destination.setX003CBoardingPassViaEmailX003EKBackingField(source.getBoardingPassViaEmail());

			if (source.getBoardingPassViaSms() != null)
				destination.setX003CBoardingPassViaSmsX003EKBackingField(source.getBoardingPassViaSms());

			if (source.getComfortSeatFree() != null)
				destination.setX003CComfortSeatFreeX003EKBackingField(source.getComfortSeatFree());

			if (source.getCorporate() != null)
				destination.setX003CCorporateX003EKBackingField(source.getCorporate());

			List<CheckinCouponData> sourceCouponsList = source.getCoupons();
			if (sourceCouponsList != null) {
				ArrayOfCoupon couponsList = objectFactory.createArrayOfCoupon();
				for (CheckinCouponData couponData : sourceCouponsList) {
					couponsList.getCoupon().add(checkinCouponDataConverter.convert(couponData));
				}
				destination.setX003CCouponsX003EKBackingField(couponsList);
			}

			destination.setX003CEmailX003EKBackingField(source.getEmail());
			destination.setX003CEticketClassX003EKBackingField(source.getEticketClass());
			destination.setX003CEticketX003EKBackingField(source.getEticket());

			destination.setX003CFrequentFlyerCarrierX003EKBackingField(
					checkinFrequentFlyerCarrierDataConverter.convert(source.getFrequentFlyerCarrier()));

			destination.setX003CFrequentFlyerCodeX003EKBackingField(source.getFrequentFlyerCode());
			if (source.getFrequentFlyerType() != null)
				destination.setX003CFrequentFlyerTypeX003EKBackingField(FrequentFlyerCategory.fromValue(source.getFrequentFlyerType().value()));

			if (source.getIsGroupPnr() != null)
				destination.setX003CIsGroupPnrX003EKBackingField(source.getIsGroupPnr());

			if (source.getSpecialFare() != null)
				destination.setX003CIsSpecialFareX003EKBackingField(source.getSpecialFare());

			destination.setX003CLastNameX003EKBackingField(source.getLastName());

			MmbLinkTypeEnum sourceLinkType = source.getLinkType();
			if (sourceLinkType != null)
				destination.setX003CLinkTypeX003EKBackingField(LinkType.fromValue(sourceLinkType.value()));

			destination.setX003CNameX003EKBackingField(source.getName());
			if (source.getOldSeatClass()!=null)
				destination.setX003COldSeatClassX003EKBackingField(CompartimentalClass.fromValue(source.getOldSeatClass().value()));

			destination.setX003COrderX003EKBackingField(
					checkinBaggageOrderDataConverter.convert(source.getBaggageOrder()));

			destination.setX003CPaymentRetrieveEmailX003EKBackingField(source.getPaymentRetrieveEmail());
			if (source.getPaymentViaEmail() != null)
				destination.setX003CPaymentViaEmailX003EKBackingField(source.getPaymentViaEmail());
			destination.setX003CPnrX003EKBackingField(source.getPnr());
			destination.setX003CReservationRPHX003EKBackingField(source.getReservationRPH());
			destination.setX003CRouteIdX003EKBackingField(source.getRouteId());

			if (source.getSeatClass() != null)
				destination.setX003CSeatClassX003EKBackingField(CompartimentalClass.fromValue(source.getSeatClass().value()));

			if (source.getSelected() != null)
				destination.setX003CSelectedX003EKBackingField(source.getSelected());
			destination.setX003CStatusNoteX003EKBackingField(source.getStatusNote());

			if (source.getStatus() != null)
				destination.setX003CStatusX003EKBackingField(PassengerStatus.fromValue(source.getStatus().value()));

			if (source.getType() != null)
				destination.setX003CTypeX003EKBackingField(PassengerType.fromValue(source.getType().value()));

		}
		return destination;
	}

}
