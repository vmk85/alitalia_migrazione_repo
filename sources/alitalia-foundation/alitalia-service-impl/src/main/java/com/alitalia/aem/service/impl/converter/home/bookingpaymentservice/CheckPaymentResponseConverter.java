package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.CheckPaymentResponse;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ProcessInfo;

@Component(metatype=false, immediate=true)
@Service(value=CheckPaymentResponseConverter.class)
public class CheckPaymentResponseConverter implements Converter<com.alitalia.aem.ws.booking.paymentservice.xsd1.CheckPaymentResponse, CheckPaymentResponse> {

	Logger logger = LoggerFactory.getLogger(CheckPaymentResponseConverter.class);
	
	@Reference
	ProcessInfoToPaymentProcessInfoData paymentConverter;
	
	@Reference
	private BookingPaymentServiceJAXBContextFactory jaxbContextFactory;
	
	@Override
	public CheckPaymentResponse convert(com.alitalia.aem.ws.booking.paymentservice.xsd1.CheckPaymentResponse source) {
		CheckPaymentResponse response = new CheckPaymentResponse();
		
		ProcessInfo castedinfoProcess = null;
		try{
			Unmarshaller infoprocessUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			JAXBElement<ProcessInfo> unmarshalledObj = null;
			unmarshalledObj = (JAXBElement<ProcessInfo>) infoprocessUnmarshaller.unmarshal((Node) source.getProcess().getValue());
			castedinfoProcess = unmarshalledObj.getValue();
		}catch(Exception e){
			logger.error("Impossibile effettuare il cast dell'oggetto process di tipo InfoProcess" , e);
		}
		response.setProcess(paymentConverter.convert(castedinfoProcess));
		return response;
		
	}
	
}
