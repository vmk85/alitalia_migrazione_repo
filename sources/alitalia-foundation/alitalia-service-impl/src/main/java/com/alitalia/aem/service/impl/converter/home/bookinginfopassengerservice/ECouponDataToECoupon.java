package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ECouponData;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ArrayOfPassengerType;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ECoupon;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.PassengerType;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd7.DiscountType;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd7.FareType;

@Component(immediate=true, metatype=false)
@Service(value=ECouponDataToECoupon.class)
public class ECouponDataToECoupon 
		implements Converter<ECouponData, ECoupon> {
	
	@Override
	public ECoupon convert(ECouponData source) {
		
		ECoupon destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			destination = objectFactory.createECoupon();
			
			destination.setAmount(
					source.getAmount());
			
			destination.setAmountBus(
					objectFactory.createECouponAmountBus(
							source.getAmountBus()));
			
			destination.setAmountEco(
					objectFactory.createECouponAmountEco(
							source.getAmountEco()));
			
			destination.setAmountEcoPlus(
					objectFactory.createECouponAmountEcoPlus(
							source.getAmountEcoPlus()));
			
			destination.setAnyPaxMAXNumPax(
					objectFactory.createECouponAnyPaxMAXNumPax(
							source.getAnyPaxMAXNumPax()));
			
			destination.setAnyPaxMinNumPax(
					objectFactory.createECouponAnyPaxMinNumPax(
							source.getAnyPaxMinNumPax()));
			
			destination.setCode(
					objectFactory.createECouponCode(
							source.getCode()));
			
			destination.setDatBlkFrm(
					XsdConvertUtils.toXMLGregorianCalendar(
							source.getDatBlkFrm()));
			
			destination.setDatBlkFrmDue(
					XsdConvertUtils.toXMLGregorianCalendar(
							source.getDatBlkFrmDue()));
			
			destination.setDatBlkFrmTre(
					XsdConvertUtils.toXMLGregorianCalendar(
							source.getDatBlkFrmTre()));
			
			destination.setDatBlkTo(
					XsdConvertUtils.toXMLGregorianCalendar(
							source.getDatBlkTo()));
			
			destination.setDatBlkToDue(
					XsdConvertUtils.toXMLGregorianCalendar(
							source.getDatBlkToDue()));

			destination.setDatBlkToTre(
					XsdConvertUtils.toXMLGregorianCalendar(
							source.getDatBlkToTre()));
			
			destination.setDatBlkFrm(
					XsdConvertUtils.toXMLGregorianCalendar(
							source.getDatBlkFrm()));
			
			destination.setDatBlkTo(
					XsdConvertUtils.toXMLGregorianCalendar(
							source.getDatBlkTo()));
			
			destination.setDatVolFrm(
					XsdConvertUtils.toXMLGregorianCalendar(
							source.getDatVolFrm()));
			
			destination.setDatVolTo(
					XsdConvertUtils.toXMLGregorianCalendar(
							source.getDatVolTo()));
			
			destination.setErrorCode(
					source.getErrorCode());
			
			destination.setErrorDescription(
					objectFactory.createECouponErrorDescription(
							source.getErrorDescription()));
			
			destination.setFamilyEcoupon(
					objectFactory.createECouponFamilyEcoupon(
							source.getFamilyEcoupon()));
			
			if (source.getFare() != null) {
				destination.setFare(
						FareType.fromValue(source.getFare().value()));
			}
			
			destination.setIsSingleUse(
					source.isSingleUse());
			
			destination.setIsValid(
					source.isValid());
			
			destination.setMinAmount(
					objectFactory.createECouponMinAmount(
							source.getMinAmount()));
			
			ArrayOfPassengerType paxTypes = objectFactory.createArrayOfPassengerType();
			if (source.getPaxTypes() != null) {
				for (PassengerTypeEnum paxType : source.getPaxTypes()) {
					paxTypes.getPassengerType().add(
							PassengerType.fromValue(
									paxType.value()));
				}		
			}
			destination.setPaxTypes(
					objectFactory.createECouponPaxTypes(
						paxTypes));
			
			if (source.getType() != null) {
				destination.setType(
						DiscountType.fromValue(source.getType().value()));
			}

			destination.setWeekSalesDay(
					objectFactory.createECouponWeekSalesDay(
							source.getWeekSalesDay()));
			
			destination.setWeekTravellDay(
					objectFactory.createECouponWeekTravellDay(
							source.getWeekSalesDay()));
			
		}
		
		return destination;
	}

}
