package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ArrayOfresultBookingDetailsSolutionPricingBookingInfoSegmentPricing;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ResultBookingDetailsSolutionPricingBookingInfoSegment;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingBookingInfoSegmentDataToPricingBookingInfoSegment.class)
public class RBDSPricingBookingInfoSegmentDataToPricingBookingInfoSegment
		implements Converter<ResultBookingDetailsSolutionPricingBookingInfoSegmentData, ResultBookingDetailsSolutionPricingBookingInfoSegment> {

	@Reference
	private RBDSPricingBookingInfoSegmentDepartureDataToPricingBookingInfoSegmentDepartureConverter rbdsPricingBookingInfoSegmentDepartureConverter;

	@Reference
	private RBDSPricingBookingInfoSegmentFlightConverterDataToPricingBookingInfoSegmentFlightConverter rbdsPricingBookingInfoSegmentFlightConverter;
	
	@Reference
	private RBDSPricingBookingInfoSegmentPricingDataToPricingBookingInfoSegmentPricing rbdsPricingBookingInfoSegmentPricingConverter;
	
	@Override
	public ResultBookingDetailsSolutionPricingBookingInfoSegment convert(
			ResultBookingDetailsSolutionPricingBookingInfoSegmentData source) {
		ResultBookingDetailsSolutionPricingBookingInfoSegment destination = null;
		
		if(source != null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionPricingBookingInfoSegment();
			destination.setDepartureField(rbdsPricingBookingInfoSegmentDepartureConverter.convert(source.getDepartureField()));
			destination.setDestinationField(source.getDestinationField());
			destination.setFlightField(rbdsPricingBookingInfoSegmentFlightConverter.convert(source.getFlightField()));
			destination.setOriginField(source.getOriginField());
			ArrayOfresultBookingDetailsSolutionPricingBookingInfoSegmentPricing array = objfact.createArrayOfresultBookingDetailsSolutionPricingBookingInfoSegmentPricing();
			if(source.getPricingField()!=null)
			for(ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData s : source.getPricingField()){
				array.getResultBookingDetailsSolutionPricingBookingInfoSegmentPricing().add(rbdsPricingBookingInfoSegmentPricingConverter.convert(s));
			}
			destination.setPricingField(array);
			
			
			
		}
		
		return destination;
	}

}
