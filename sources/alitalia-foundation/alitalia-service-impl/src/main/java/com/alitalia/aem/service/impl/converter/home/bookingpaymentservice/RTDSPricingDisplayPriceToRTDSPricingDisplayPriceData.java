package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingDisplayPriceData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingDisplayPrice;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingDisplayPriceToRTDSPricingDisplayPriceData.class)
public class RTDSPricingDisplayPriceToRTDSPricingDisplayPriceData implements
		Converter<ResultTicketingDetailSolutionPricingDisplayPrice,
					ResultTicketingDetailSolutionPricingDisplayPriceData> {

	@Override
	public ResultTicketingDetailSolutionPricingDisplayPriceData convert(
			ResultTicketingDetailSolutionPricingDisplayPrice source) {
		ResultTicketingDetailSolutionPricingDisplayPriceData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingDisplayPriceData();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
