package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentLegConnectionData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionSliceSegmentLegConnection;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentLegConnectionToRTDSSliceSegmentLegConnectionData.class)
public class RTDSSliceSegmentLegConnectionToRTDSSliceSegmentLegConnectionData implements
		Converter<ResultTicketingDetailSolutionSliceSegmentLegConnection, ResultTicketingDetailSolutionSliceSegmentLegConnectionData> {

	@Override
	public ResultTicketingDetailSolutionSliceSegmentLegConnectionData convert(ResultTicketingDetailSolutionSliceSegmentLegConnection source) {
		ResultTicketingDetailSolutionSliceSegmentLegConnectionData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionSliceSegmentLegConnectionData();

			destination.setDurationField(source.getDurationField());
		}

		return destination;
	}

}
