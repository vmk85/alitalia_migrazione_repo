package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesData;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ArrayOfresultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenalty;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ArrayOfresultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenalty;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenalties;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesDataToRBDSBInfoSegmentPricing.class)
public class RBDSPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesDataToRBDSBInfoSegmentPricing
		implements Converter<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesData, ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenalties> {

	@Reference
	RBDSPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyDataToRBDSPBISPFRSPenaltiesCancellationAndRefundPenalty penaltiesCancellationAndRefundPenaltyConverter;
	
	@Reference
	RBDSPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyDataToRBDSPFRSPenaltiesChangePenalty penaltiesChangePenaltyFieldConverter;
	
	@Override
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenalties convert(
			ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesData source) {
		ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenalties destination = null;
		
		if(source != null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenalties();
			ArrayOfresultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenalty array_1 = objfact.createArrayOfresultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenalty();
			if(source.getCancellationAndRefundPenaltyField()!=null)
			for(ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData s : source.getCancellationAndRefundPenaltyField())
				array_1.getResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenalty().add(penaltiesCancellationAndRefundPenaltyConverter.convert(s));
			
			ArrayOfresultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenalty array_2 = objfact.createArrayOfresultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenalty();
			if(source.getChangePenaltyField()!=null)
			for(ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData s : source.getChangePenaltyField())
				array_2.getResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenalty().add(penaltiesChangePenaltyFieldConverter.convert(s));
			
			destination.setCancellationAndRefundPenaltyField(array_1);
			destination.setChangePenaltyField(array_2);
			destination.setNonrefundableField(source.isNonrefundableField());
			destination.setNonrefundableFieldSpecified(source.isNonrefundableFieldSpecified());
		}
		
		return destination;
	}

}
