package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingExtEndorsementData;
import com.alitalia.aem.ws.booking.paymentservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd11.ResultBookingDetailsSolutionPricingExtEndorsement;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingExtEndorsementDataToPricingExtEndorsement.class)
public class RBDSPricingExtEndorsementDataToPricingExtEndorsement implements
		Converter<ResultBookingDetailsSolutionPricingExtEndorsementData, ResultBookingDetailsSolutionPricingExtEndorsement> {

	@Override
	public ResultBookingDetailsSolutionPricingExtEndorsement convert(
			ResultBookingDetailsSolutionPricingExtEndorsementData source) {
		ResultBookingDetailsSolutionPricingExtEndorsement destination = null;
		
		if(source!=null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionPricingExtEndorsement();
			destination.setBoxesField(source.getBoxesField());
			destination.setValueField(source.getValueField());
		}
		
		return destination;
	}
	
}
