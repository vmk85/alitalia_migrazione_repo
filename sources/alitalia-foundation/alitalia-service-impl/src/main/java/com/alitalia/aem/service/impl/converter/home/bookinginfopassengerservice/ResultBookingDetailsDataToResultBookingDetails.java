package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ArrayOfresultBookingDetailsSolution;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ResultBookingDetails;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ObjectFactory;;

@Component(immediate=true, metatype=false)
@Service(value=ResultBookingDetailsDataToResultBookingDetails.class)
public class ResultBookingDetailsDataToResultBookingDetails 
		implements Converter<ResultBookingDetailsData, ResultBookingDetails> {
	
	@Reference
	private ResultBookingDetailsSolutionDataToResultBookingDetailsSolution resultBookingDetailsSolutionConverter;
	
	@Override
	public ResultBookingDetails convert(ResultBookingDetailsData source) {
		
		ResultBookingDetails destination = null;
		if (source != null) {
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetails();

			ArrayOfresultBookingDetailsSolution array = objfact.createArrayOfresultBookingDetailsSolution();
			if(source.getSolutionField()!=null)
			for(ResultBookingDetailsSolutionData s : source.getSolutionField()){
				array.getResultBookingDetailsSolution().add(resultBookingDetailsSolutionConverter.convert(s));
			}
			destination.setSolutionField(array);
		}
		return destination;
	}
}
