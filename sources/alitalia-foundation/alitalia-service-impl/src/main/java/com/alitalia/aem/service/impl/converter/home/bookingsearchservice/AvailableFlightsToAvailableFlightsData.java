package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AvailableFlightsData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.common.data.home.TabData;
import com.alitalia.aem.common.data.home.TaxData;
import com.alitalia.aem.ws.booking.searchservice.xsd15.AvailableFlights;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ArrayOfRoutes;
import com.alitalia.aem.ws.booking.searchservice.xsd3.Route;
import com.alitalia.aem.ws.booking.searchservice.xsd3.Routes;
import com.alitalia.aem.ws.booking.searchservice.xsd3.Tab;
import com.alitalia.aem.ws.booking.searchservice.xsd4.FullTextRules;
import com.alitalia.aem.ws.booking.searchservice.xsd4.Tax;
import com.alitalia.aem.ws.booking.searchservice.xsd5.ArrayOfanyType;

@Component(immediate=true, metatype=false)
@Service(value=AvailableFlightsToAvailableFlightsData.class)
public class AvailableFlightsToAvailableFlightsData implements Converter<AvailableFlights, AvailableFlightsData> {

	private static final Logger logger = LoggerFactory.getLogger(AvailableFlightsToAvailableFlightsData.class);

	@Reference
	private RouteToRouteData routeConverter;

	@Reference
	private RoutesToRoutesData routesConverter;

	@Reference
	private TabToTabData tabConverter;

	@Reference
	private TaxToTaxData taxConverter;

	@Reference
	private ArrayOfDictionaryItemToPropertiesData arrayOfDictionaryItemToPropertiesDataConverter;

	@Reference
	private BookingSearchServiceJAXBContextFactory jaxbContextFactory;

	@SuppressWarnings("unchecked")
	@Override
	public AvailableFlightsData convert(AvailableFlights source) {
		AvailableFlightsData destination = new AvailableFlightsData();

		JAXBElement<String> farmId = source.getFarmId();
		if (farmId != null)
			destination.setFarmId(farmId.getValue());

		JAXBElement<ArrayOfanyType> sourceFullTextRules = source.getFullTextRules();
		ArrayList<String> fullTextRules = null;
		if (sourceFullTextRules != null &&
				sourceFullTextRules.getValue() != null &&
				sourceFullTextRules.getValue().getAnyType() != null &&
				!sourceFullTextRules.getValue().getAnyType().isEmpty()) {
			fullTextRules = new ArrayList<String>();
			try {
				Unmarshaller fullTextRuleUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
				for (Object sourcefullTextRule : sourceFullTextRules.getValue().getAnyType()) {
					if (sourcefullTextRule instanceof FullTextRules)
						fullTextRules.add(((FullTextRules) sourcefullTextRule).getX003CRulesX003EKBackingField());
					else {
						FullTextRules castedObject = (FullTextRules) fullTextRuleUnmarshaller.unmarshal((Node) sourcefullTextRule);
						fullTextRules.add(castedObject.getX003CRulesX003EKBackingField());
					}
				}
			} catch (JAXBException e) {
				logger.error("Error while creating JAXBConext/Unmarshller for FullTextRules: {}", e);
			}
		}
		destination.setFullTextRules(fullTextRules);

		JAXBElement<String> id = source.getId();
		if (id != null)
			destination.setId(id.getValue());

		destination.setRefreshSolutionId(null);
		destination.setSolutionId(null);

		JAXBElement<String> solutionSet = source.getSolutionSet();
		if (solutionSet != null)
			destination.setSolutionSet(solutionSet.getValue());

		JAXBElement<String> sessionId = source.getSessionId();
		if (sessionId != null)
			destination.setSessionId(sessionId.getValue());

		if (source.getProperties() != null) {
			destination.setProperties(
					arrayOfDictionaryItemToPropertiesDataConverter.convert(source.getProperties().getValue()));
		}

		JAXBElement<ArrayOfanyType> sourceRoutes = source.getRoutes();
		if (sourceRoutes != null) {
			try {
				Unmarshaller routeUnmashaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
				JAXBElement<Route> unmarshalledObj = null;

				List<Object> routesFromSource = sourceRoutes.getValue().getAnyType();
				ArrayList<RouteData> routes = new ArrayList<RouteData>();
				for (Object routeFromSource : routesFromSource) {
					unmarshalledObj = (JAXBElement<Route>) routeUnmashaller.unmarshal((Node) routeFromSource);
					Route castedRoute = (Route) unmarshalledObj.getValue();
					RouteData routeData = routeConverter.convert(castedRoute);
					routes.add(routeData);
				}
				destination.setRoutes(routes);
			} catch (JAXBException e) {
				logger.error("Error instantiating JAXBContext/Unmarshaller: {}", e);
			}
		}

		JAXBElement<ArrayOfRoutes> sourceRoutesList = source.getRoutesList();
		if (sourceRoutesList != null) {
			List<Routes> routesListFromSource = sourceRoutesList.getValue().getRoutes();
			ArrayList<RoutesData> routesList = new ArrayList<RoutesData>();
			for (Object routesFromSource : routesListFromSource) {
				Routes castedRoute = (Routes) routesFromSource;
				RoutesData routeData = routesConverter.convert(castedRoute);
				routesList.add(routeData);
			}
			destination.setRoutesList(routesList);
		}

		JAXBElement<ArrayOfanyType> sourceTabs = source.getTabs();
		ArrayList<TabData> tabs = new ArrayList<TabData>();
		if (sourceTabs != null) {
			try {
				Unmarshaller tabUnmashaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
				JAXBElement<Tab> unmarshalledObj = null;

				for (Object sourceTab : sourceTabs.getValue().getAnyType()) {
					unmarshalledObj = (JAXBElement<Tab>) tabUnmashaller.unmarshal((Node) sourceTab);
					Tab castedTab = unmarshalledObj.getValue();
					TabData tabData = tabConverter.convert(castedTab);
					tabs.add(tabData);
				}
			} catch (JAXBException e) {
				logger.error("Error instantiating JAXBContext/Unmarshaller: {}", e);
			}
		}
		destination.setTabs(tabs);

		JAXBElement<ArrayOfanyType> sourceTaxes = source.getTaxes();
		ArrayList<TaxData> taxes = new ArrayList<TaxData>();
		if (sourceTaxes != null) {
			for (Object sourceTax : sourceTaxes.getValue().getAnyType()) {
				Tax castedTax = (Tax) sourceTax;
				TaxData taxData = taxConverter.convert(castedTax);
				taxes.add(taxData);
			}
		}
		destination.setTaxes(taxes);

		return destination;
	}

}
