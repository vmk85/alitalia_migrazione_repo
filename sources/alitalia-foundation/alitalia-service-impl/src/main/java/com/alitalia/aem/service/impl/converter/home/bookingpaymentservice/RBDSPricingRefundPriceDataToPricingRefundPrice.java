package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingRefundPriceData;
import com.alitalia.aem.ws.booking.paymentservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd11.ResultBookingDetailsSolutionPricingRefundPrice;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingRefundPriceDataToPricingRefundPrice.class)
public class RBDSPricingRefundPriceDataToPricingRefundPrice implements Converter<ResultBookingDetailsSolutionPricingRefundPriceData, ResultBookingDetailsSolutionPricingRefundPrice> {

	@Override
	public ResultBookingDetailsSolutionPricingRefundPrice convert(
			ResultBookingDetailsSolutionPricingRefundPriceData source) {
		ResultBookingDetailsSolutionPricingRefundPrice destination = null;
		if(source != null){
			ObjectFactory objf = new ObjectFactory();
			destination = objf.createResultBookingDetailsSolutionPricingRefundPrice();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}

}
