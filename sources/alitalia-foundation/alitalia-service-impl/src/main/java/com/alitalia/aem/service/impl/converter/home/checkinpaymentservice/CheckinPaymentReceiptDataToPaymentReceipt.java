package com.alitalia.aem.service.impl.converter.home.checkinpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinPaymentReceiptData;
import com.alitalia.aem.ws.checkin.paymentservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.checkin.paymentservice.xsd2.PaymentReceipt;


@Component(immediate=true, metatype=false)
@Service(value=CheckinPaymentReceiptDataToPaymentReceipt.class)
public class CheckinPaymentReceiptDataToPaymentReceipt implements Converter<CheckinPaymentReceiptData, PaymentReceipt> {
	
	@Reference
	private CheckinPassengerDataToPassenger checkinPassengerDataConverter;
	
	@Reference
	private CheckinRouteDataToRoute checkinRouteDataConverter;

	@Override
	public PaymentReceipt convert(CheckinPaymentReceiptData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		PaymentReceipt destination = null;

		if (source != null) {
			destination = objectFactory.createPaymentReceipt();
			destination.setX003CIdX003EKBackingField(source.getId());
			
			destination.setX003CLocaleX003EKBackingField(source.getLocale());
			destination.setX003CPassengerX003EKBackingField(checkinPassengerDataConverter.convert(source.getPassenger()));
			destination.setX003CRouteX003EKBackingField(checkinRouteDataConverter.convert(source.getRoute()));
			
		}
		return destination;
	}

}
