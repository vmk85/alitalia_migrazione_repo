//2
package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingReissueTaxTotalDifferenceData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetailsSolutionPricingReissueTaxTotalDifference;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingReissueTaxTotalDifferenceToPricingReissueTaxTotalDifferenceData.class)
public class RBDSPricingReissueTaxTotalDifferenceToPricingReissueTaxTotalDifferenceData
		implements Converter<ResultBookingDetailsSolutionPricingReissueTaxTotalDifference, ResultBookingDetailsSolutionPricingReissueTaxTotalDifferenceData> {

	@Override
	public ResultBookingDetailsSolutionPricingReissueTaxTotalDifferenceData convert(
			ResultBookingDetailsSolutionPricingReissueTaxTotalDifference source) {
		ResultBookingDetailsSolutionPricingReissueTaxTotalDifferenceData destination = null;
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingReissueTaxTotalDifferenceData();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}

	public ResultBookingDetailsSolutionPricingReissueTaxTotalDifferenceData convert(
			com.alitalia.aem.ws.bookingaward.searchservice.xsd13.ResultBookingDetailsSolutionPricingReissueTaxTotalDifference source) {
		ResultBookingDetailsSolutionPricingReissueTaxTotalDifferenceData destination = null;
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingReissueTaxTotalDifferenceData();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}
}
