package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.booking.searchservice.xsd3.AdultPassenger;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ApplicantPassenger;
import com.alitalia.aem.ws.booking.searchservice.xsd3.CashAndMiles;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ChildPassenger;
import com.alitalia.aem.ws.booking.searchservice.xsd3.InfantPassenger;
import com.alitalia.aem.ws.booking.searchservice.xsd3.InfoBase;
import com.alitalia.aem.ws.booking.searchservice.xsd3.InsurancePolicy;
import com.alitalia.aem.ws.booking.searchservice.xsd3.Route;
import com.alitalia.aem.ws.booking.searchservice.xsd3.Routes;
import com.alitalia.aem.ws.booking.searchservice.xsd3.Tab;
import com.alitalia.aem.ws.booking.searchservice.xsd3.TicketInfo;
import com.alitalia.aem.ws.booking.searchservice.xsd4.FullTextRules;

@Component(immediate=true, metatype=false)
@Service(value=BookingSearchServiceJAXBContextFactory.class)
public class BookingSearchServiceJAXBContextFactory {

	private static final Logger logger = LoggerFactory.getLogger(BookingSearchServiceJAXBContextFactory.class);

	private JAXBContext jaxbContext = null;

	public synchronized JAXBContext getJaxbContext() throws JAXBException {

		if (jaxbContext == null) {
			jaxbContext = JAXBContext.newInstance(FullTextRules.class, 
								Route.class,
								ApplicantPassenger.class,
								AdultPassenger.class,
								ChildPassenger.class,
								InfantPassenger.class,
								com.alitalia.aem.ws.booking.searchservice.xsd12.ResultBookingDetails.class,
								com.alitalia.aem.ws.booking.searchservice.xsd14.ResultBookingDetails.class,
								CashAndMiles.class,
								InsurancePolicy.class,
								InfoBase.class,
								TicketInfo.class,
								Routes.class,
								Tab.class
								);
		}

		return jaxbContext;
	}

	@Activate
	private void activate() {
		try {
			jaxbContext = JAXBContext.newInstance(FullTextRules.class, 
					Route.class,
					ApplicantPassenger.class,
					AdultPassenger.class,
					ChildPassenger.class,
					InfantPassenger.class,
					com.alitalia.aem.ws.booking.searchservice.xsd12.ResultBookingDetails.class,
					com.alitalia.aem.ws.booking.searchservice.xsd14.ResultBookingDetails.class,
					CashAndMiles.class,
					InsurancePolicy.class,
					InfoBase.class,
					TicketInfo.class,
					Routes.class,
					Tab.class
					);
		} catch (JAXBException e) {
			logger.warn("BookingSearchServiceJAXBContextFactory - cannot instantiate JAXBContext: {}", e.getMessage());
		}
	}
}
