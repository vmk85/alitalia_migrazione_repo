package com.alitalia.aem.service.impl.converter.home.checkinpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinAirport;
import com.alitalia.aem.ws.checkin.paymentservice.xsd2.Airport;
import com.alitalia.aem.ws.checkin.paymentservice.xsd2.ObjectFactory;


@Component(immediate=true, metatype=false)
@Service(value=CheckinAirportToAirport.class)
public class CheckinAirportToAirport implements Converter<CheckinAirport, Airport> {
	
	@Override
	public Airport convert(CheckinAirport source) {
		ObjectFactory objectFactory = new ObjectFactory();
		Airport destination = null;

		if (source != null) {
			destination = objectFactory.createAirport();
			destination.setX003CIdX003EKBackingField(source.getId());
			destination.setX003CAirportNameX003EKBackingField(source.getAirportName());
			destination.setX003CCityNameX003EKBackingField(source.getCity());
			destination.setX003CCodeX003EKBackingField(source.getCode());
			destination.setX003CCountryCodeX003EKBackingField(source.getCountryCode());
			destination.setX003CNameX003EKBackingField(source.getName());
			
		}
		return destination;
	}

}
