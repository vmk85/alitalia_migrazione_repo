package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentBookingInfoData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentLegData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentPricingFareData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ArrayOfresultTicketingDetailSolutionSliceSegmentBookingInfo;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ArrayOfresultTicketingDetailSolutionSliceSegmentLeg;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ArrayOfresultTicketingDetailSolutionSliceSegmentPricingFare;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionSliceSegment;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentDataToRTDSSliceSegment.class)
public class RTDSSliceSegmentDataToRTDSSliceSegment implements
		Converter<ResultTicketingDetailSolutionSliceSegmentData, 
					ResultTicketingDetailSolutionSliceSegment> {

	@Reference
	private RTDSSliceSegmentBookingInfoDataToRTDSSliceSegmentBookingInfo rtdsBookingInfoFieldConverter;

	@Reference
	private RTDSSliceSegmentConnectionDataToRTDSSliceSegmentConnection rtdsConnectionFieldConverter;

	@Reference
	private RTDSSliceSegmentFlightDataToRTDSSliceSegmentFlight rtdsFlightFieldConverter;

	@Reference
	private RTDSSliceSegmentLegDataToRTSSliceSegmentLeg rtdsLegFieldConverter;

	@Reference
	private RTDSSliceSegmentPricingFareDataToRTDSSliceSegmentPricingFare rtdsPricingFieldConverter;

	@Override
	public ResultTicketingDetailSolutionSliceSegment convert(
			ResultTicketingDetailSolutionSliceSegmentData source) {
		ResultTicketingDetailSolutionSliceSegment destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionSliceSegment();

			ArrayOfresultTicketingDetailSolutionSliceSegmentBookingInfo bookingInfoField = null;
			if (source.getBookingInfoField() != null &&
					!source.getBookingInfoField().isEmpty()) {
				bookingInfoField = objectFactory.createArrayOfresultTicketingDetailSolutionSliceSegmentBookingInfo();
				for(ResultTicketingDetailSolutionSliceSegmentBookingInfoData sourceBookingInfoFieldElem : source.getBookingInfoField())
					bookingInfoField.getResultTicketingDetailSolutionSliceSegmentBookingInfo().add(
							rtdsBookingInfoFieldConverter.convert(sourceBookingInfoFieldElem));
			}
			destination.setBookingInfoField(bookingInfoField);

			destination.setConnectionField(rtdsConnectionFieldConverter.convert(source.getConnectionField()));

			destination.setDepartureField(source.getDepartureField());
			destination.setDestinationField(source.getDestinationField());

			destination.setFlightField(rtdsFlightFieldConverter.convert(source.getFlightField()));

			destination.setHashField(source.getHashField());

			ArrayOfresultTicketingDetailSolutionSliceSegmentLeg legField = null;
			if (source.getLegField() != null &&
					!source.getLegField().isEmpty()) {
				legField = objectFactory.createArrayOfresultTicketingDetailSolutionSliceSegmentLeg();
				for(ResultTicketingDetailSolutionSliceSegmentLegData sourceLegFieldElem : source.getLegField())
					legField.getResultTicketingDetailSolutionSliceSegmentLeg().add(
							rtdsLegFieldConverter.convert(sourceLegFieldElem));
			}
			destination.setLegField(legField);

			destination.setOriginField(source.getOriginField());

			ArrayOfresultTicketingDetailSolutionSliceSegmentPricingFare pricingField = null;
			if (source.getPricingField() != null &&
					!source.getPricingField().isEmpty()) {
				pricingField = objectFactory.createArrayOfresultTicketingDetailSolutionSliceSegmentPricingFare();
				for(ResultTicketingDetailSolutionSliceSegmentPricingFareData sourcePricingFieldElem : source.getPricingField())
					pricingField.getResultTicketingDetailSolutionSliceSegmentPricingFare().add(
							rtdsPricingFieldConverter.convert(sourcePricingFieldElem));
			}
			destination.setPricingField(pricingField);
		}

		return destination;
	}

}
