
package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ConnectingFlight;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.DirectFlight;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.Route;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.RouteType;
import com.alitalia.aem.ws.booking.paymentservice.xsd5.ArrayOfanyType;

@Component(immediate=true, metatype=false)
@Service(value=RouteDataToRoute.class)
public class RouteDataToRoute implements Converter<RouteData, Route> {

	@Reference
	private DirectFlightDataToDirectFlight directFlightDataConverter;

	@Reference
	private ConnectingFlightDataToConnectingFlight connectingFlightDataConverter;

	@Override
	public Route convert(RouteData source) {
		ObjectFactory factory3 = new ObjectFactory();
		com.alitalia.aem.ws.booking.paymentservice.xsd5.ObjectFactory factory4 =
				new com.alitalia.aem.ws.booking.paymentservice.xsd5.ObjectFactory();
		Route destination = factory3.createRoute();

		destination.setIndex(source.getIndex());
		destination.setType(RouteType.fromValue(source.getType().value()));

		ArrayOfanyType flights = factory4.createArrayOfanyType();
		List<FlightData> sourceFlights = source.getFlights();
		for (Object flight : sourceFlights) {
			if (flight instanceof DirectFlightData)
			{
				DirectFlightData castedDirectFlightData = (DirectFlightData) flight;
				DirectFlight directFlight = directFlightDataConverter.convert(castedDirectFlightData);
				flights.getAnyType().add(directFlight);
			}
			else 
			{
				ConnectingFlightData castedConnectingFlightData = (ConnectingFlightData) flight;
				ConnectingFlight connectingFlight = connectingFlightDataConverter.convert(castedConnectingFlightData);
				flights.getAnyType().add(connectingFlight);
			}
		}
		destination.setFlights(factory3.createRouteFlights(flights));
		
		return destination;
	}

}
