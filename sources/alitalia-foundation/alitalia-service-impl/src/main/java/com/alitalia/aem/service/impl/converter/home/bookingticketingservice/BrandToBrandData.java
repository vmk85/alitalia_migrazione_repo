package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.BaggageAllowanceData;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.ws.booking.ticketservice.xsd2.ArrayOfanyType;
import com.alitalia.aem.ws.booking.ticketservice.xsd2.ArrayOfstring;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.BaggageAllowance;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.Brand;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.BrandPenalties;

@Component(immediate = true, metatype = false)
@Service(value = BrandToBrandData.class)
public class BrandToBrandData implements Converter<Brand, BrandData> {

	@Reference
	private BaggageAllowanceToBaggageAllowanceData baggageAllowanceConverter;

	@Reference
	private BrandPenaltiesToBrandPenaltiesData brandPenaltiesConverter;

	@Override
	public BrandData convert(Brand source) {
		BrandData destination = new BrandData();

		destination.setAdvancePurchaseDays(source.getAdvancePurchaseDays());
		destination.setBestFare(source.isBestFare());
		destination.setCode(source.getCode().getValue());
		destination.setCompartimentalClass(source.getCompartimentalClass().getValue());
		destination.setCurrency(source.getCurrency().getValue());
		destination.setEnabled(source.isEnabled());
		destination.setGrossFare(source.getGrossFare());
		destination.setId(null);
		destination.setIndex(source.getIndex());
		destination.setNetFare(source.getNetFare());
		destination.setRealPrice(source.getRealPrice());
		destination.setRefreshSolutionId(source.getRefreshSolutionId().getValue());
		destination.setSeatsAvailable(source.getSeatsAvailable());
		destination.setSelected(source.isSelected());
		destination.setSolutionId(source.getSolutionId().getValue());
		destination.setSolutionSet(null);

		ArrayList<BaggageAllowanceData> baggageAllowance = new ArrayList<BaggageAllowanceData>();
		ArrayOfanyType sourceBaggageAllowance = source.getBaggageAllowanceList().getValue();
		if (sourceBaggageAllowance != null) {
			List<Object> sourceBaggageAllow = sourceBaggageAllowance.getAnyType();
			for (Object baggageAllow : sourceBaggageAllow) {
				BaggageAllowance castedBaggageAllowance = (BaggageAllowance) baggageAllow;
				BaggageAllowanceData allowanceData = baggageAllowanceConverter.convert(castedBaggageAllowance);
				baggageAllowance.add(allowanceData);
			}
		}
		destination.setBaggageAllowanceList(baggageAllowance);

		ArrayList<String> notes = new ArrayList<String>();
		ArrayOfstring sourceNotes = source.getNotes().getValue();
		if (sourceNotes != null) {
			for (String noteSource : sourceNotes.getString())
				notes.add(noteSource);
		}
		destination.setNotes(notes);

		BrandPenalties penalties = (BrandPenalties) source.getPenalties().getValue();
		if (penalties == null)
			destination.setPenalties(null);
		else
			destination.setPenalties(brandPenaltiesConverter.convert(penalties));

		destination.setProperties(null);

		return destination;
	}
}