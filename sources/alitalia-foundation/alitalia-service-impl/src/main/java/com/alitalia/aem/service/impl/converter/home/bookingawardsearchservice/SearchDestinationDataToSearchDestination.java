package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import java.util.Calendar;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.SearchDestinationData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.ObjectFactory;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.RouteType;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.SearchDestination;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd5.TimeType;

@Component(immediate=true, metatype=false)
@Service(value=SearchDestinationDataToSearchDestination.class)
public class SearchDestinationDataToSearchDestination implements Converter<SearchDestinationData, SearchDestination> {

	private Logger logger = LoggerFactory.getLogger(SearchDestinationDataToSearchDestination.class);

	@Reference
	private BookingSearchAirportDataToAirport airportConverter;
	
	@Override
	public SearchDestination convert(SearchDestinationData source) {
		ObjectFactory objectFactory =  new ObjectFactory();
		SearchDestination destination = objectFactory.createSearchDestination();

		Calendar departureDate = source.getDepartureDate();
		if (departureDate != null)
			destination.setDepartureDate(XsdConvertUtils.toXMLGregorianCalendar(departureDate));

		destination.setFrom(objectFactory.createASearchDestinationFrom(airportConverter.convert(source.getFromAirport())));
		destination.setTo(objectFactory.createSearchDestinationTo(airportConverter.convert(source.getToAirport())));
		destination.setTimeType(TimeType.fromValue(source.getTimeType().value()));
		destination.setType(RouteType.fromValue(source.getRouteType().value()));

		return destination;
	}

}
