package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryCouponData;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryEticketData;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ArrayOfCoupons;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ETicket;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ObjectFactory;


@Component(immediate=true, metatype=false)
@Service(value=CheckinAncillaryETicketDataToETicket.class)
public class CheckinAncillaryETicketDataToETicket implements Converter<CheckinAncillaryEticketData, ETicket> {

	@Reference
	private CheckinAncillaryCouponDataToCoupons mmbAncillaryCouponDataConverter;

	@Override
	public ETicket convert(CheckinAncillaryEticketData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		ETicket destination = null;

		if (source != null) {
			destination = objectFactory.createETicket();

			destination.setX003CNumberX003EKBackingField(source.getNumber());
			destination.setX003CPnrX003EKBackingField(source.getPnr());

			ArrayOfCoupons coupons = objectFactory.createArrayOfCoupons();
			for(CheckinAncillaryCouponData sourceCoupon : source.getCoupons()) {
				coupons.getCoupons().add(mmbAncillaryCouponDataConverter.convert(sourceCoupon));
			}
			destination.setX003CCouponsX003EKBackingField(coupons);
		}

		return destination;
	}

}
