package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.CrossSellingsRequest;
import com.alitalia.aem.ws.booking.ticketservice.xsd1.GetCrossSellingsRequest;
import com.alitalia.aem.ws.booking.ticketservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.Routes;

@Component(immediate = true, metatype = false)
@Service(value = CrossSellingRequestToGetCrossSellingRequest.class)
public class CrossSellingRequestToGetCrossSellingRequest implements
		Converter<CrossSellingsRequest, GetCrossSellingsRequest> {

	@Reference
	private RoutesDataToRoutes routesDataToRoutesConverter;

	@Override
	public GetCrossSellingsRequest convert(CrossSellingsRequest source) {
		GetCrossSellingsRequest destination = null;

		if (source != null) {
			ObjectFactory objectFactory1 = new ObjectFactory();

			destination = new GetCrossSellingsRequest();
			destination.setIpAddress(objectFactory1.createGetCrossSellingsRequestIpAddress(source.getIpAddress()));
			destination.setLanguageCode(objectFactory1.createGetCrossSellingsRequestLanguageCode(source
					.getLanguageCode()));
			destination.setMarketCode(objectFactory1.createGetCrossSellingsRequestMarketCode(source.getMarketCode()));
			destination.setUserAgent(objectFactory1.createGetCrossSellingsRequestUserAgent(source.getUserAgent()));

			JAXBElement<Routes> value = null;
			if (source.getPrenotation() != null) {
				Routes routes = routesDataToRoutesConverter.convert(source.getPrenotation());
				value = objectFactory1.createGetCrossSellingsRequestPrenotation(routes);
			}
			destination.setPrenotation(value);
		}

		return destination;
	}

}
