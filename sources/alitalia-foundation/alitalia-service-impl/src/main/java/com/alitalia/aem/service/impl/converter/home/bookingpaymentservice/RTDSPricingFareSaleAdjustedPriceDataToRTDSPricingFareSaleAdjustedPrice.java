package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareSaleAdjustedPriceData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingFareSaleAdjustedPrice;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareSaleAdjustedPriceDataToRTDSPricingFareSaleAdjustedPrice.class)
public class RTDSPricingFareSaleAdjustedPriceDataToRTDSPricingFareSaleAdjustedPrice implements
		Converter<ResultTicketingDetailSolutionPricingFareSaleAdjustedPriceData, 
					ResultTicketingDetailSolutionPricingFareSaleAdjustedPrice> {

	@Override
	public ResultTicketingDetailSolutionPricingFareSaleAdjustedPrice convert(
			ResultTicketingDetailSolutionPricingFareSaleAdjustedPriceData source) {
		ResultTicketingDetailSolutionPricingFareSaleAdjustedPrice destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingFareSaleAdjustedPrice();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
