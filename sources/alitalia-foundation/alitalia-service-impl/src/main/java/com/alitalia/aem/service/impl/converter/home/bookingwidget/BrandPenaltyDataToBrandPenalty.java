package com.alitalia.aem.service.impl.converter.home.bookingwidget;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.BrandPenaltyData;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.BrandPenalty;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=BrandPenaltyDataToBrandPenalty.class)
public class BrandPenaltyDataToBrandPenalty implements Converter<BrandPenaltyData, BrandPenalty> {

	@Override
	public BrandPenalty convert(BrandPenaltyData source) {
		ObjectFactory factory5 = new ObjectFactory(); 
		BrandPenalty destination = factory5.createBrandPenalty();

		destination.setMaxPrice(factory5.createBrandPenaltyMaxPrice(source.getMaxPrice()));
		destination.setMaxPriceCurrency(factory5.createBrandPenaltyMaxPriceCurrency(source.getMaxPriceCurrency()));
		destination.setMinPrice(factory5.createBrandPenaltyMinPrice(source.getMinPrice()));
		destination.setMinPriceCurrency(factory5.createBrandPenaltyMinPriceCurrency(source.getMinPriceCurrency()));
		destination.setPermitted(source.isPermitted());

		return destination;
	}

}
