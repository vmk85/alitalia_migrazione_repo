package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.ws.booking.searchservice.xsd3.AdultPassenger;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ApplicantPassenger;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ChildPassenger;
import com.alitalia.aem.ws.booking.searchservice.xsd3.InfantPassenger;

@Component(immediate=true, metatype=false)
@Service(value=PassengerBaseToAPassengerBaseData.class)
public class PassengerBaseToAPassengerBaseData implements Converter<Object, PassengerBaseData> {

	private static final Logger logger = LoggerFactory.getLogger(PassengerBaseToAPassengerBaseData.class);
	private XPath xpath = null;
	private XPathExpression expr = null;

	@Reference
	private ApplicantPassengerToApplicantPassengerData applicantPassengerConverter;
	
	@Reference
	private AdultPassengerToAdultPassengerData adultPassengerConverter;
	
	@Reference
	private ChildPassengerToChildPassengerData childPassengerConverter;
	
	@Reference
	private InfantPassengerToInfantPassengerData infantPassengerConverter;

	@Reference
	private BookingSearchServiceJAXBContextFactory jaxbContextFactory;

	@SuppressWarnings("unchecked")
	@Override
	public PassengerBaseData convert(Object source) {
		
		PassengerBaseData destination = null;
		
		if (source != null) {
			if (source instanceof ApplicantPassenger) {
				destination = applicantPassengerConverter.convert((ApplicantPassenger) source);
			} else if (source instanceof AdultPassenger) {
				destination = adultPassengerConverter.convert((AdultPassenger) source);
			} else if (source instanceof ChildPassenger) {
				destination = childPassengerConverter.convert((ChildPassenger) source);
			} else if (source instanceof InfantPassenger) {
				destination = infantPassengerConverter.convert((InfantPassenger) source);
			} else {
				try {
					if (xpath == null)
						xpath = XPathFactory.newInstance().newXPath();

					if (expr == null)
						expr = xpath.compile("/*[local-name()='anyType']/@*[local-name()='type']");
	
					String passengerInstanceType = (String) expr.evaluate((Node) source, XPathConstants.STRING);
	
					if ("ApplicantPassenger".equals(passengerInstanceType)) {
						Unmarshaller applicantUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
						ApplicantPassenger castedSource = (ApplicantPassenger) 
								((JAXBElement<ApplicantPassenger>) applicantUnmarshaller.unmarshal((Node) source)).getValue();
						destination = applicantPassengerConverter.convert(castedSource);
					
					} else if ("AdultPassenger".equals(passengerInstanceType)) {
						Unmarshaller adultUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
						AdultPassenger castedSource = (AdultPassenger) 
								((JAXBElement<AdultPassenger>) adultUnmarshaller.unmarshal((Node) source)).getValue();
						destination = adultPassengerConverter.convert(castedSource);
					
					} else if ("ChildPassenger".equals(passengerInstanceType)) {
						Unmarshaller childUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
						ChildPassenger castedSource = (ChildPassenger) 
								((JAXBElement<ChildPassenger>) childUnmarshaller.unmarshal((Node) source)).getValue();
						destination = childPassengerConverter.convert(castedSource);
					
					} else if ("InfantPassenger".equals(passengerInstanceType)) {
						Unmarshaller infantUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
						InfantPassenger castedSource = (InfantPassenger) 
								((JAXBElement<InfantPassenger>) infantUnmarshaller.unmarshal((Node) source)).getValue();
						destination = infantPassengerConverter.convert(castedSource);
					
					}
	
				} catch (JAXBException e) {
					logger.error("Error creating JAXBContext/Unmarshaller for a Passenger class: {}", e);
				} catch (XPathExpressionException e) {
					logger.error("Error extracting type attribute value: {}", e);
				}
			}
			
		}
		
		return destination;
	}

}
