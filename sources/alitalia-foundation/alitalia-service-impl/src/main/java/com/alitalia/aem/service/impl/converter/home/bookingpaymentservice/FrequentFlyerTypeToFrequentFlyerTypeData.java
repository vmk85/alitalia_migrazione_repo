package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.FrequentFlyerTypeData;
import com.alitalia.aem.ws.booking.paymentservice.xsd6.FrequentFlyerType;

@Component(immediate=true, metatype=false)
@Service(value=FrequentFlyerTypeToFrequentFlyerTypeData.class)
public class FrequentFlyerTypeToFrequentFlyerTypeData 
		implements Converter<FrequentFlyerType, FrequentFlyerTypeData> {
	
	@Override
	public FrequentFlyerTypeData convert(FrequentFlyerType source) {
		
		FrequentFlyerTypeData destination = null;
		
		if (source != null) {
			
			destination = new FrequentFlyerTypeData();
			
			destination.setCode(
					source.getX003CCodeX003EKBackingField());
			
			destination.setDescription(
					source.getX003CDescriptionX003EKBackingField());
			
			destination.setLenght(
					source.getX003CLenghtX003EKBackingField());
			
			destination.setPreFillChar(
					source.getX003CPreFillCharX003EKBackingField());
			
			destination.setRegularExpressionValidation(
					source.getX003CRegularExpressionValidationX003EKBackingField());
			
		}
		
		return destination;
	}

}
