package com.alitalia.aem.service.impl.converter.home.checkinemdservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinReasonForIssuance;
import com.alitalia.aem.ws.checkin.emdservice.xsd2.ReasonForIssuanceType;


@Component(immediate=true, metatype=false)
@Service(value=ReasonForIssuanceTypeToCheckinReasonForIssuance.class)
public class ReasonForIssuanceTypeToCheckinReasonForIssuance implements Converter<ReasonForIssuanceType, CheckinReasonForIssuance> {
	
	@Override
	public CheckinReasonForIssuance convert(ReasonForIssuanceType source) {
		CheckinReasonForIssuance destination = null;

		if (source != null) {
			destination = new CheckinReasonForIssuance();
			destination.setReasonCode(source.getX003CReasonCodeX003EKBackingField());
			destination.setReasonDescription(source.getX003CReasonDescriptionX003EKBackingField());
			destination.setReasonSubCode(source.getX003CReasonSubCodeX003EKBackingField());
		}

		return destination;
	}

}
