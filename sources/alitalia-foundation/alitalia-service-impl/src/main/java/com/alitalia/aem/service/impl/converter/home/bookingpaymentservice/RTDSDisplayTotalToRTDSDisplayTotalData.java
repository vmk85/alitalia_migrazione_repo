package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionDisplayTotalData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionDisplayTotal;

@Component(immediate=true, metatype=false)
@Service(value=RTDSDisplayTotalToRTDSDisplayTotalData.class)
public class RTDSDisplayTotalToRTDSDisplayTotalData implements
		Converter<ResultTicketingDetailSolutionDisplayTotal, ResultTicketingDetailSolutionDisplayTotalData> {

	@Override
	public ResultTicketingDetailSolutionDisplayTotalData convert(ResultTicketingDetailSolutionDisplayTotal source) {
		ResultTicketingDetailSolutionDisplayTotalData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionDisplayTotalData();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
