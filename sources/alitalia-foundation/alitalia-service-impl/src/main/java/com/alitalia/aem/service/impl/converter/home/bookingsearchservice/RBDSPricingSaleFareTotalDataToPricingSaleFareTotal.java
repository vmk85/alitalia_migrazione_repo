package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingSaleFareTotalData;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ResultBookingDetailsSolutionPricingSaleFareTotal;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingSaleFareTotalDataToPricingSaleFareTotal.class)
public class RBDSPricingSaleFareTotalDataToPricingSaleFareTotal implements
		Converter<ResultBookingDetailsSolutionPricingSaleFareTotalData, ResultBookingDetailsSolutionPricingSaleFareTotal> {

	@Override
	public ResultBookingDetailsSolutionPricingSaleFareTotal convert(
			ResultBookingDetailsSolutionPricingSaleFareTotalData source) {
		ResultBookingDetailsSolutionPricingSaleFareTotal destination = null;
		if(source != null){
			ObjectFactory objf = new ObjectFactory();
			destination = objf.createResultBookingDetailsSolutionPricingSaleFareTotal();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}

}
