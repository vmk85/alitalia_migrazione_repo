package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareCalculationData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionPricingFareCalculation;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionPricingFareCalculationBoxedTax;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareCalculationToRTDSPricingFareCalculationData.class)
public class RTDSPricingFareCalculationToRTDSPricingFareCalculationData implements
		Converter<ResultTicketingDetailSolutionPricingFareCalculation, 
						ResultTicketingDetailSolutionPricingFareCalculationData> {

	@Reference
	private RTDSPricingFareCalculationBoxedTaxToRTDSPricingFareCalculationBoxedTaxData boxedTaxFieldConverter;

	@Override
	public ResultTicketingDetailSolutionPricingFareCalculationData convert(
			ResultTicketingDetailSolutionPricingFareCalculation source) {
		ResultTicketingDetailSolutionPricingFareCalculationData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingFareCalculationData();

			List<ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxData> boxedTaxField = null;
			if (source.getBoxedTaxField() != null &&
					source.getBoxedTaxField().getResultTicketingDetailSolutionPricingFareCalculationBoxedTax() != null &&
					!source.getBoxedTaxField().getResultTicketingDetailSolutionPricingFareCalculationBoxedTax().isEmpty()) {
				boxedTaxField = new ArrayList<ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxData>();
				for(ResultTicketingDetailSolutionPricingFareCalculationBoxedTax sourceBoxedTaxFieldElem : 
							source.getBoxedTaxField().getResultTicketingDetailSolutionPricingFareCalculationBoxedTax())
					boxedTaxField.add(boxedTaxFieldConverter.convert(sourceBoxedTaxFieldElem));
			}
			destination.setBoxedTaxField(boxedTaxField);

			List<String> lineField = null;
			if (source.getLineField() != null &&
					source.getLineField().getString() != null &&
					!source.getLineField().getString().isEmpty()) {
				lineField = new ArrayList<String>();
				for(String sourceLineFieldElem : source.getLineField().getString())
					lineField.add(sourceLineFieldElem);
			}
			destination.setLineField(lineField);
		}

		return destination;
	}

}
