package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.CashAndMilesData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.CashAndMiles;

@Component(immediate=true, metatype=false)
@Service(value=CashAndMilesToCashAndMilesData.class)
public class CashAndMilesToCashAndMilesData 
		implements Converter<CashAndMiles, CashAndMilesData> {
	
	@Reference
	private MmCustomerToMmCustomerData mmCustomerToMmCustomerDataConverter;
	
	@Override
	public CashAndMilesData convert(CashAndMiles source) {
		
		CashAndMilesData destination = null;
		
		if (source != null) {
			
			destination = new CashAndMilesData();
			
			destination.setDiscountAmount(
					source.getDiscountAmount());
			
			destination.setMmCustomer(
					mmCustomerToMmCustomerDataConverter.convert(
							source.getMMCustomer().getValue()));
			
			destination.setPaymentDate(
					XsdConvertUtils.parseCalendar(
							source.getPaymentDate()));
			
			destination.setUsedMileage(
					source.getUsedMileage());
			
		}
		
		return destination;
	}

}
