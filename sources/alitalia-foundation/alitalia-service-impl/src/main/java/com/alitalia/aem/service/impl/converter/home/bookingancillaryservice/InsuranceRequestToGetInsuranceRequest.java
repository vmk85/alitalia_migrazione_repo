package com.alitalia.aem.service.impl.converter.home.bookingancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.PassengerBase;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.messages.home.InsuranceRequest;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.ancillaryservice.xsd1.GetInsuranceRequest;
import com.alitalia.aem.ws.booking.ancillaryservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.booking.ancillaryservice.xsd2.APassengerBase;
import com.alitalia.aem.ws.booking.ancillaryservice.xsd2.AreaValue;
import com.alitalia.aem.ws.booking.ancillaryservice.xsd2.ArrayOfAPassengerBase;
import com.alitalia.aem.ws.booking.ancillaryservice.xsd2.RouteType;

@Component(immediate=true, metatype=false)
@Service(value=InsuranceRequestToGetInsuranceRequest.class)
public class InsuranceRequestToGetInsuranceRequest implements Converter<InsuranceRequest, GetInsuranceRequest> {

	@Reference
	private PassengerBaseDataToAPassengerBase passengerBaseDataConveter;

	@Override
	public GetInsuranceRequest convert(InsuranceRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		com.alitalia.aem.ws.booking.ancillaryservice.xsd2.ObjectFactory objectFactory2 = 
				new com.alitalia.aem.ws.booking.ancillaryservice.xsd2.ObjectFactory();
		GetInsuranceRequest destination = objectFactory.createGetInsuranceRequest();

		AreaValueEnum sourceArea = source.getArea();
		if (sourceArea != null)
			destination.setArea(AreaValue.fromValue(sourceArea.value()));
		else
			destination.setArea(null);
		destination.setArrivalAirport(objectFactory.createGetInsuranceRequestArrivalAirport(source.getArrivalAirport()));
		destination.setArrivalDate(XsdConvertUtils.toXMLGregorianCalendar(source.getArrivalDate()));
		destination.setCountryCode(objectFactory.createGetInsuranceRequestCountryCode(source.getCountryCode()));
		destination.setDepartureAirport(objectFactory.createGetInsuranceRequestDepartureAirport(source.getDepartureAirport()));
		destination.setDepartureDate(XsdConvertUtils.toXMLGregorianCalendar(source.getDepartureDate()));
		destination.setFlgUsaCa(source.isFlgUsaCa());
		destination.setLanguageCode(objectFactory.createGetInsuranceRequestLanguageCode(source.getLanguageCode()));
		destination.setRouteType(RouteType.fromValue(source.getRouteType().value()));

		
		ArrayOfAPassengerBase passengers = objectFactory2.createArrayOfAPassengerBase();
		for(PassengerBase sourcePassenger: source.getPassengers()) {
			APassengerBase passengerBase = passengerBaseDataConveter.convert((PassengerBaseData) sourcePassenger);
			passengers.getAPassengerBase().add(passengerBase);
		}
		destination.setPassengers(objectFactory.createGetInsuranceRequestPassengers(passengers));

		return destination;
	}

}
