package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.RefreshSearchData;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.ws.booking.searchservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.booking.searchservice.xsd1.SearchRequest;
import com.alitalia.aem.ws.booking.searchservice.xsd2.RefreshSearch;
import com.alitalia.aem.ws.booking.searchservice.xsd2.SearchResponseType;

@Component(immediate=true, metatype=false)
@Service(value=SearchFlightSolutionRequestToRefreshSearchRequest.class)
public class SearchFlightSolutionRequestToRefreshSearchRequest implements Converter<SearchFlightSolutionRequest, SearchRequest> {

	@Reference
	private RefreshSearchDataToRefreshSearch refreshSearchDataConverter;

	@Override
	public SearchRequest convert(SearchFlightSolutionRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		SearchRequest destination = objectFactory.createSearchRequest();
		RefreshSearchData refreshSearchData = (RefreshSearchData) source.getFilter();
		RefreshSearch filter = refreshSearchDataConverter.convert(refreshSearchData);

		destination.setFilter(objectFactory.createSearchRequestFilter(filter));
		destination.setLanguageCode(objectFactory.createSearchRequestLanguageCode(source.getLanguageCode()));
		destination.setMarketCode(objectFactory.createSearchRequestMarketCode(source.getMarket()));
		destination.setResponseType(SearchResponseType.fromValue(source.getResponseType().value()));
		
		destination.setCookie(objectFactory.createSearchRequestCookie(source.getCookie()));
		destination.setExecution(objectFactory.createSearchRequestExecution(source.getExecution()));
		destination.setSabreGateWayAuthToken(objectFactory.createSearchRequestSabreGateWayAuthToken(source.getSabreGateWayAuthToken()));

		return destination;
	}

}
