package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionPricingFareCalculationBoxedTax;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareCalculationBoxedTaxToRTDSPricingFareCalculationBoxedTaxData.class)
public class RTDSPricingFareCalculationBoxedTaxToRTDSPricingFareCalculationBoxedTaxData implements
		Converter<ResultTicketingDetailSolutionPricingFareCalculationBoxedTax,
						ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxData> {

	@Reference
	private RTDSPricingFareCalculationBoxedTaxPriceToRTDSPricingFareCalculationBoxedTaxPriceData priceFieldConverter;

	@Override
	public ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxData convert(
			ResultTicketingDetailSolutionPricingFareCalculationBoxedTax source) {
		ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxData();

			destination.setCountryField(source.getCountryField());
			destination.setTaxCodeField(source.getTaxCodeField());
			destination.setTaxSubcodeField(source.getTaxSubcodeField());

			destination.setPriceField(
					priceFieldConverter.convert(source.getPriceField()));
		}

		return destination;
	}

}
