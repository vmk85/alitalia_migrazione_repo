package com.alitalia.aem.service.impl.converter.home.checkinextrabaggageservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.ExtraBaggageOrderCheckinResponse;
import com.alitalia.aem.ws.checkin.extrabaggageservice.xsd1.ExtraBaggageOrderResponse;


@Component(immediate=true, metatype=false)
@Service(value=ExtraBaggageOrderResponseToExtraBaggageOrderCheckinResponse.class)
public class ExtraBaggageOrderResponseToExtraBaggageOrderCheckinResponse implements Converter<ExtraBaggageOrderResponse, ExtraBaggageOrderCheckinResponse> {

	@Reference
	private BaggageOrderToMmbBaggageOrderData baggageOrderConverter;
	
	@Override
	public ExtraBaggageOrderCheckinResponse convert(ExtraBaggageOrderResponse source) {
		
		ExtraBaggageOrderCheckinResponse destination = null;

		if (source != null) {
			
			destination = new ExtraBaggageOrderCheckinResponse();
			
			if(source.getOrder() != null){
				destination.setOrder(baggageOrderConverter.convert(source.getOrder().getValue()));
			}
		}

		return destination;
	}
}
