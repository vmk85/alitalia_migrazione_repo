package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AwardSearchData;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd1.SearchRequest;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd2.AwardCalendarSearch;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd2.SearchResponseType;

@Component(immediate=true, metatype=false)
@Service(value=SearchFlightSolutionRequestToAwardCalendarSearchRequest.class)
public class SearchFlightSolutionRequestToAwardCalendarSearchRequest implements Converter<SearchFlightSolutionRequest, SearchRequest> {

	@Reference
	private AwardSearchDataToAwardCalendarSearch awardCalendarSearchDataConverter;

	@Override
	public SearchRequest convert(SearchFlightSolutionRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		SearchRequest destination = objectFactory.createSearchRequest();
		AwardSearchData awardSearchData = (AwardSearchData) source.getFilter();
		AwardCalendarSearch filter = awardCalendarSearchDataConverter.convert(awardSearchData);

		destination.setFilter(objectFactory.createSearchRequestFilter(filter));
		destination.setLanguageCode(objectFactory.createSearchRequestLanguageCode(source.getLanguageCode()));
		destination.setMarketCode(objectFactory.createSearchRequestMarketCode(source.getMarket()));
		destination.setResponseType(SearchResponseType.fromValue(source.getResponseType().value()));

		return destination;
	}

}
