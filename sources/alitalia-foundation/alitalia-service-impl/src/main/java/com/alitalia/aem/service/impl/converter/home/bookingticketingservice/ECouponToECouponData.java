package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ECouponData;
import com.alitalia.aem.common.data.home.enumerations.DiscountTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.FareTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ECoupon;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.PassengerType;

@Component(immediate = true, metatype = false)
@Service(value = ECouponToECouponData.class)
public class ECouponToECouponData implements Converter<ECoupon, ECouponData> {

	@Override
	public ECouponData convert(ECoupon source) {
		ECouponData destination = null;

		if (source != null) {
			destination = new ECouponData();

			destination.setAmount(source.getAmount());
			if (source.getCode() != null) {
				destination.setCode(source.getCode().getValue());
			}

			destination.setErrorCode(source.getErrorCode());

			if (source.getErrorDescription() != null) {
				destination.setErrorDescription(source.getErrorDescription().getValue());
			}

			if (source.getFamilyEcoupon() != null) {
				destination.setFamilyEcoupon(source.getFamilyEcoupon().getValue());
			}

			if (source.getFare() != null) {
				destination.setFare(FareTypeEnum.fromValue(source.getFare().value()));
			}

			destination.setSingleUse(source.isIsSingleUse());

			destination.setValid(source.isIsValid());

			List<PassengerTypeEnum> paxTypes = new ArrayList<PassengerTypeEnum>();
			if (source.getPaxTypes() != null && source.getPaxTypes().getValue() != null) {
				for (PassengerType paxType : source.getPaxTypes().getValue().getPassengerType()) {
					paxTypes.add(PassengerTypeEnum.fromValue(paxType.value()));
				}
			}
			destination.setPaxTypes(paxTypes);

			if (source.getType() != null) {
				destination.setType(DiscountTypeEnum.fromValue(source.getType().value()));
			}
		}

		return destination;
	}

}
