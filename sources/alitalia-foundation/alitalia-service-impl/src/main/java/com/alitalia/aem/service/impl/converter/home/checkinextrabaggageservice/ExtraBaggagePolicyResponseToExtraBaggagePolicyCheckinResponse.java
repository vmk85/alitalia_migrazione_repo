package com.alitalia.aem.service.impl.converter.home.checkinextrabaggageservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.ExtraBaggagePolicyCheckinResponse;
import com.alitalia.aem.ws.checkin.extrabaggageservice.xsd1.ExtraBaggagePolicyResponse;


@Component(immediate=true, metatype=false)
@Service(value=ExtraBaggagePolicyResponseToExtraBaggagePolicyCheckinResponse.class)
public class ExtraBaggagePolicyResponseToExtraBaggagePolicyCheckinResponse implements Converter<ExtraBaggagePolicyResponse, ExtraBaggagePolicyCheckinResponse> {

	@Reference
	private BaggagePolicyToCheckinBaggagePolicyData baggagePolicyConverter;
	
	@Override
	public ExtraBaggagePolicyCheckinResponse convert(ExtraBaggagePolicyResponse source) {
		
		ExtraBaggagePolicyCheckinResponse destination = null;

		if (source != null) {
			
			destination = new ExtraBaggagePolicyCheckinResponse();
			
			if(source.getPolicy() != null){
				destination.setPolicy(baggagePolicyConverter.convert(source.getPolicy().getValue()));
			}
		}

		return destination;
	}
}
