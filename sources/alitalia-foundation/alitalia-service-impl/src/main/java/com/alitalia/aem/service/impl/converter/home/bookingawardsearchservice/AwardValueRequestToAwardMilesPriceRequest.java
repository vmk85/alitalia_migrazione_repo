package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.common.messages.home.AwardValueRequest;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd1.AwardMilesPriceRequest;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd6.ArrayOfanyType;

@Component(immediate=true, metatype=false)
@Service(value=AwardValueRequestToAwardMilesPriceRequest.class)
public class AwardValueRequestToAwardMilesPriceRequest implements Converter<AwardValueRequest, AwardMilesPriceRequest> {

	@Reference
	private RouteDataToRoute routeDataconverter;

	@Override
	public AwardMilesPriceRequest convert(AwardValueRequest source) {
		AwardMilesPriceRequest destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createAwardMilesPriceRequest();

			destination.setCustomerNumber(objectFactory.createAwardMilesPriceRequestCustomerNumber(source.getMmCustomerNumber()));
			destination.setCustomerSurname(objectFactory.createAwardMilesPriceRequestCustomerSurname(source.getMmCustomerSurname()));
			destination.setPaxNumber(source.getPassengersNumber());

			ArrayOfanyType flights = null;
			if (source.getRoutes() != null && !source.getRoutes().isEmpty()) {

				com.alitalia.aem.ws.bookingaward.searchservice.xsd6.ObjectFactory objectFactory6 =
						new com.alitalia.aem.ws.bookingaward.searchservice.xsd6.ObjectFactory();

				flights = objectFactory6.createArrayOfanyType();

				for(RouteData sourceRoute : source.getRoutes())
					flights.getAnyType().add(routeDataconverter.convert(sourceRoute));
			}
			destination.setFlights(objectFactory.createAwardMilesPriceRequestFlights(flights));
		}

		return destination;
	}

}
