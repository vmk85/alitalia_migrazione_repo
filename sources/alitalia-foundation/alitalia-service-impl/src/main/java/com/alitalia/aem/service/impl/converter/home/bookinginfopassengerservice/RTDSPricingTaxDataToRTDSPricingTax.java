package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingTaxData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionPricingTax;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingTaxDataToRTDSPricingTax.class)
public class RTDSPricingTaxDataToRTDSPricingTax implements
		Converter<ResultTicketingDetailSolutionPricingTaxData, ResultTicketingDetailSolutionPricingTax> {

	@Reference
	private RTDSPricingTaxDisplayPriceDataToRTDSPricingTaxDisplayPrice rtdsDisplayPriceFieldConverter;

	@Reference
	private RTDSPricingTaxSalePriceDataToRTDSPricingTaxSalePrice rtdsSalePriceFieldConverter;

	@Override
	public ResultTicketingDetailSolutionPricingTax convert(
			ResultTicketingDetailSolutionPricingTaxData source) {
		ResultTicketingDetailSolutionPricingTax destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingTax();

			destination.setCodeField(source.getCodeField());

			destination.setDisplayPriceField(rtdsDisplayPriceFieldConverter.convert(source.getDisplayPriceField()));

			destination.setIdField(source.getIdField());

			destination.setSalePriceField(rtdsSalePriceFieldConverter.convert(source.getSalePriceField()));

			destination.setStatusField(source.getStatusField());
		}

		return destination;
	}

}
