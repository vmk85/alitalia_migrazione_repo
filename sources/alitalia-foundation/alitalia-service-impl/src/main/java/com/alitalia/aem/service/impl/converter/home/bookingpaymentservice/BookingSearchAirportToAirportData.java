package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.Airport;

@Component(immediate=true, metatype=false)
@Service(value=BookingSearchAirportToAirportData.class)
public class BookingSearchAirportToAirportData implements Converter<Airport, AirportData> {

	@Override
	public AirportData convert(Airport source) {
		AirportData destination = new AirportData();
		
//		JAXBElement<String> jaxbAirportCode = source.getCode();
//		JAXBElement<String> jaxbAirportName = source.getName();
//		JAXBElement<String> jaxbAirportCountry = source.getCountry();
//		JAXBElement<String> jaxbAirportCountryCode = source.getCountryCode();
		XMLGregorianCalendar cal = null;

		destination.setArea(AreaValueEnum.fromValue(source.getArea().value()));
		destination.setAzDeserves(source.isAZDeserves());
		destination.setCity(source.getCity().getValue());
		destination.setCityCode(source.getCityCode().getValue());
		destination.setCityUrl(source.getCityUrl().getValue());
		destination.setCode(source.getCode().getValue());
		destination.setCountry(source.getCountry().getValue());
		destination.setCountryCode(source.getCountryCode().getValue());
		destination.setDescription(source.getDescription().getValue());
//		destination.setDetails();
		destination.setEligibility(source.isEligibility());

		cal = source.getEtktAvlblFrom().getValue();
		if (cal != null)
			destination.setEtktAvlblFrom(XsdConvertUtils.parseCalendar(cal));
		else
			destination.setEtktAvlblFrom(null);

		destination.setHiddenInFirstDeparture(source.isHiddenInFirstDeparture());
		destination.setIdMsg(source.getIdMsg());
		destination.setName(source.getName().getValue());
		destination.setState(source.getState().getValue());
		destination.setStateCode(source.getStateCode().getValue());

		cal = source.getUnavlblFrom().getValue();
		if (cal != null)
			destination.setUnavlblFrom(XsdConvertUtils.parseCalendar(cal));
		else
			destination.setUnavlblFrom(null);

		cal = source.getUnavlblUntil().getValue();
		if (cal != null)
			destination.setUnavlblUntil(XsdConvertUtils.parseCalendar(cal));
		else
			destination.setUnavlblUntil(null);
		
		return destination;
	}
}