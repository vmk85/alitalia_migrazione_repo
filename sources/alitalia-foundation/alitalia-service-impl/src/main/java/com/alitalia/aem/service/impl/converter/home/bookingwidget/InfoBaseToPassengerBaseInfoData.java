package com.alitalia.aem.service.impl.converter.home.bookingwidget;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PassengerBaseInfoData;
import com.alitalia.aem.common.data.home.enumerations.GenderTypeEnum;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.InfoBase;

@Component(immediate=true, metatype=false)
@Service(value=InfoBaseToPassengerBaseInfoData.class)
public class InfoBaseToPassengerBaseInfoData implements Converter<InfoBase, PassengerBaseInfoData> {

	@Override
	public PassengerBaseInfoData convert(InfoBase source) {
		PassengerBaseInfoData destination = new PassengerBaseInfoData();

		destination.setBirthDate(XsdConvertUtils.parseCalendar(source.getBirthDate()));
		destination.setGender(GenderTypeEnum.fromValue(source.getGender().value()));

		return destination;
	}

}
