package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.MmbAncillaryErrorData;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutInitResponse;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.CheckOutInitResponse;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.InitPaymentData;


@Component(immediate=true, metatype=false)
@Service(value=CheckOutInitResponseToMmbAncillaryCheckOutInitResponse.class)
public class CheckOutInitResponseToMmbAncillaryCheckOutInitResponse implements Converter<CheckOutInitResponse, MmbAncillaryCheckOutInitResponse> {

	@Reference
	private InitPaymentDataToMmbAncillaryInitPaymentData initPaymentDataConverter;

	@Reference
	private ErrorToMmbAncillaryErrorData errorConverter;

	@Override
	public MmbAncillaryCheckOutInitResponse convert(CheckOutInitResponse source) {
		MmbAncillaryCheckOutInitResponse destination = null;

		if (source != null) {
			destination = new MmbAncillaryCheckOutInitResponse();

			JAXBElement<String> sourceMachineName = source.getMachineName();
			if (sourceMachineName != null)
				destination.setMachine(sourceMachineName.getValue());

			if (source.getErrors() != null &&
					source.getErrors().getValue() != null &&
					source.getErrors().getValue().getAnyType() != null &&
					!source.getErrors().getValue().getAnyType().isEmpty()) {
				List<Object> sourceErrors = source.getErrors().getValue().getAnyType();
				List<MmbAncillaryErrorData> errors = new ArrayList<MmbAncillaryErrorData>();
				for (Object sourceError : sourceErrors) {
					errors.add(errorConverter.convert(sourceError));
				}
				destination.setErrors(errors);
			}

			JAXBElement<InitPaymentData> sourcePaymentData = source.getPaymentData();
			if (sourcePaymentData != null)
				destination.setInitPayment(initPaymentDataConverter.convert(sourcePaymentData.getValue()));
		}

		return destination;
	}

}
