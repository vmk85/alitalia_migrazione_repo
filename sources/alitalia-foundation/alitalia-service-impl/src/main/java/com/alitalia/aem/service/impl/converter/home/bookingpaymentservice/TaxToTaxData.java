package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.TaxData;
import com.alitalia.aem.ws.booking.paymentservice.xsd4.Tax;

@Component(immediate=true, metatype=false)
@Service(value=TaxToTaxData.class)
public class TaxToTaxData implements Converter<Tax, TaxData> {

	@Override
	public TaxData convert(Tax source) {
		TaxData destination = new TaxData();

		destination.setAmount(source.getX003CAmountX003EKBackingField());
		destination.setCode(source.getX003CCodeX003EKBackingField());
		destination.setCurrency(source.getX003CCurrencyX003EKBackingField());
		destination.setId(source.getX003CIdX003EKBackingField());
		destination.setName(source.getX003CNameX003EKBackingField());

		return destination;
	}

}
