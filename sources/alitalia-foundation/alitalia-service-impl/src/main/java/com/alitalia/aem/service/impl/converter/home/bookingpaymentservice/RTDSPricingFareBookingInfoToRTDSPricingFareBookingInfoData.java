package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareBookingInfoData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingFareBookingInfo;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareBookingInfoToRTDSPricingFareBookingInfoData.class)
public class RTDSPricingFareBookingInfoToRTDSPricingFareBookingInfoData implements
		Converter<ResultTicketingDetailSolutionPricingFareBookingInfo, 
					ResultTicketingDetailSolutionPricingFareBookingInfoData> {

	@Reference
	private RTDSPricingFareBookingInfoSegmentToRTDSPricingFareBookingInfoSegmentData segmentFieldConverter;

	@Override
	public ResultTicketingDetailSolutionPricingFareBookingInfoData convert(
			ResultTicketingDetailSolutionPricingFareBookingInfo source) {
		ResultTicketingDetailSolutionPricingFareBookingInfoData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingFareBookingInfoData();

			destination.setBookingCodeField(source.getBookingCodeField());

			destination.setSegmentField(
					segmentFieldConverter.convert(source.getSegmentField()));
		}

		return destination;
	}

}
