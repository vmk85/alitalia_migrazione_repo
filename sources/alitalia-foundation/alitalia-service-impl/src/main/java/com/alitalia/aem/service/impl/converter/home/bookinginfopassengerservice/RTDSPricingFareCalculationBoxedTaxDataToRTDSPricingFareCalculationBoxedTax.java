package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionPricingFareCalculationBoxedTax;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareCalculationBoxedTaxDataToRTDSPricingFareCalculationBoxedTax.class)
public class RTDSPricingFareCalculationBoxedTaxDataToRTDSPricingFareCalculationBoxedTax implements
		Converter<ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxData, 
					ResultTicketingDetailSolutionPricingFareCalculationBoxedTax> {

	@Reference
	private RTDSPricingFareCalculationBoxedTaxPriceDataToRTDSPricingFareCalculationBoxedTaxPrice priceFieldConverter;

	@Override
	public ResultTicketingDetailSolutionPricingFareCalculationBoxedTax convert(
			ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxData source) {
		ResultTicketingDetailSolutionPricingFareCalculationBoxedTax destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingFareCalculationBoxedTax();

			destination.setCountryField(source.getCountryField());
			destination.setPriceField(priceFieldConverter.convert(source.getPriceField()));
			destination.setTaxCodeField(source.getTaxCodeField());
			destination.setTaxSubcodeField(source.getTaxSubcodeField());
		}

		return destination;
	}

}
