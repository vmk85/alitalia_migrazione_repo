package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareSaleAdjustedPriceData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionPricingFareSaleAdjustedPrice;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareSaleAdjustedPriceToRTDSPricingFareSaleAdjustedPriceData.class)
public class RTDSPricingFareSaleAdjustedPriceToRTDSPricingFareSaleAdjustedPriceData implements
		Converter<ResultTicketingDetailSolutionPricingFareSaleAdjustedPrice, 
					ResultTicketingDetailSolutionPricingFareSaleAdjustedPriceData> {

	@Override
	public ResultTicketingDetailSolutionPricingFareSaleAdjustedPriceData convert(
			ResultTicketingDetailSolutionPricingFareSaleAdjustedPrice source) {
		ResultTicketingDetailSolutionPricingFareSaleAdjustedPriceData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingFareSaleAdjustedPriceData();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
