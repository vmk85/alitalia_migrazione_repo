package com.alitalia.aem.service.impl.converter.home.checkinpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinBaggageInfoData;
import com.alitalia.aem.common.data.home.checkin.CheckinComfortSeatInfoData;
import com.alitalia.aem.common.data.home.checkin.CheckinUpgradeCabinInfoData;
import com.alitalia.aem.common.messages.home.PaymentCheckinRequest;
import com.alitalia.aem.ws.checkin.paymentservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.paymentservice.xsd1.PaymentRequest;
import com.alitalia.aem.ws.checkin.paymentservice.xsd1.Resource;


@Component(immediate=true, metatype=false)
@Service(value=PaymentCheckinRequestToPaymentRequest.class)
public class PaymentCheckinRequestToPaymentRequest implements Converter<PaymentCheckinRequest, PaymentRequest> {
	
	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;
	
	@Reference
	private CheckinBaggageInfoDataToBaggageInfo checkinBaggageInfoDataConverter;
	
	@Reference
	private CheckinComfortSeatInfoDataToComfortSeatInfo checkinComfortSeatInfoDataConverter;
	
	@Reference
	private CheckinUpgradeCabinInfoDataToUpgradeCabinInfo checkinUpgradeCabinInfoDataConverter;
	
	@Override
	public PaymentRequest convert(PaymentCheckinRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();		
		PaymentRequest destination = objectFactory.createPaymentRequest();
		
		destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
		destination.setX003CResourceX003EKBackingField(Resource.WEB);
		
		if(source.getTInfo() instanceof CheckinBaggageInfoData){
			destination.setTInfo(objectFactory.createPaymentRequestTInfo(checkinBaggageInfoDataConverter.convert((CheckinBaggageInfoData)source.getTInfo())));
		}
		else if(source.getTInfo() instanceof CheckinComfortSeatInfoData){
			destination.setTInfo(objectFactory.createPaymentRequestTInfo(checkinComfortSeatInfoDataConverter.convert((CheckinComfortSeatInfoData)source.getTInfo())));
		}
		else if(source.getTInfo() instanceof CheckinUpgradeCabinInfoData){
			destination.setTInfo(objectFactory.createPaymentRequestTInfo(checkinUpgradeCabinInfoDataConverter.convert((CheckinUpgradeCabinInfoData)source.getTInfo())));
		}

		return destination;
	}
}