package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailPagesData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailPages;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd5.ArrayOfanyType;

@Component(immediate=true, metatype=false)
@Service(value=ResultTicketingDetailPagesDataToResultTicketingDetailPages.class)
public class ResultTicketingDetailPagesDataToResultTicketingDetailPages implements
		Converter<ResultTicketingDetailPagesData, ResultTicketingDetailPages> {

	@Override
	public ResultTicketingDetailPages convert(ResultTicketingDetailPagesData source) {
		ResultTicketingDetailPages destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			com.alitalia.aem.ws.booking.infopassengerservice.xsd5.ObjectFactory objectFactory5 =
					new com.alitalia.aem.ws.booking.infopassengerservice.xsd5.ObjectFactory();
			destination = objectFactory.createResultTicketingDetailPages();

			destination.setCountField(source.getCountField());
			destination.setCurrentField(source.getCurrentField());

			ArrayOfanyType pageField = null;
			if (source.getPageField() != null &&
					!source.getPageField().isEmpty()) {
				pageField = objectFactory5.createArrayOfanyType();
				for(Object sourcePageFieldElem : source.getPageField())
					pageField.getAnyType().add(sourcePageFieldElem);
			}
			destination.setPageField(pageField);
		}

		return destination;
	}

}
