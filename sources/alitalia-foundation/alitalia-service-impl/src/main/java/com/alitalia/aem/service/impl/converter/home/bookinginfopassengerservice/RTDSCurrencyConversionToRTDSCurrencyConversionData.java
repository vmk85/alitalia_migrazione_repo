package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionCurrencyConversionData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionCurrencyConversion;

@Component(immediate=true, metatype=false)
@Service(value=RTDSCurrencyConversionToRTDSCurrencyConversionData.class)
public class RTDSCurrencyConversionToRTDSCurrencyConversionData
		implements Converter<ResultTicketingDetailSolutionCurrencyConversion, ResultTicketingDetailSolutionCurrencyConversionData> {

	@Override
	public ResultTicketingDetailSolutionCurrencyConversionData convert(ResultTicketingDetailSolutionCurrencyConversion source) {
		ResultTicketingDetailSolutionCurrencyConversionData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionCurrencyConversionData();

			destination.setApplicationField(source.getApplicationField());
			destination.setFromField(source.getFromField());
			destination.setRateField(source.getRateField());
			destination.setRateTableField(source.getRateTableField());
			destination.setToField(source.getToField());
		}

		return destination;
	}

}
