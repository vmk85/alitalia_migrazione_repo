package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingData;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ResultBookingDetailsSolution;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ResultBookingDetailsSolutionPricing;

@Component(immediate=true, metatype=false)
@Service(value=ResultBookingDetailsSolutionToResultBookingDetailsSolutionData.class)
public class ResultBookingDetailsSolutionToResultBookingDetailsSolutionData
		implements Converter<ResultBookingDetailsSolution, ResultBookingDetailsSolutionData> {

	@Reference
	private RBDSAddCollectPriceToAddCollectPriceData rbdsAddCollectPriceConverter;
	
	@Reference
	private RBDSRefundPriceToRefundPriceData rbdsRefundPriceDataConverter;

	@Reference
	private RBDSPricingToPricingData rbdsPricingConverter;
	
	@Override
	public ResultBookingDetailsSolutionData convert(ResultBookingDetailsSolution source) {
	   
		ResultBookingDetailsSolutionData destination = null;
		
		
		if(source != null){
			destination = new ResultBookingDetailsSolutionData();
			destination.setAddCollectPriceField(rbdsAddCollectPriceConverter.convert(source.getAddCollectPriceField()));
			List<ResultBookingDetailsSolutionPricingData> pricingField = new ArrayList<ResultBookingDetailsSolutionPricingData>();
			if(source.getPricingField()!=null)
			for(ResultBookingDetailsSolutionPricing s : source.getPricingField().getResultBookingDetailsSolutionPricing()){
				pricingField.add(rbdsPricingConverter.convert(s));
			}
				
			destination.setPricingField(pricingField);
			destination.setRefundPriceField(rbdsRefundPriceDataConverter.convert(source.getRefundPriceField()));
			destination.setSaleTaxTotalField(source.getSaleTaxTotalField());
			destination.setSaleTotalField(source.getSaleTotalField());
		    destination.setTimeField(source.getTimeField());
		}
	    return destination;
	}

	public ResultBookingDetailsSolutionData convert(com.alitalia.aem.ws.booking.searchservice.xsd14.ResultBookingDetailsSolution source) {
		   
		ResultBookingDetailsSolutionData destination = null;
		
		
		if(source != null){
			destination = new ResultBookingDetailsSolutionData();
			destination.setAddCollectPriceField(rbdsAddCollectPriceConverter.convert(source.getAddCollectPriceField()));
			List<ResultBookingDetailsSolutionPricingData> pricingField = new ArrayList<ResultBookingDetailsSolutionPricingData>();
			if(source.getPricingField()!=null)
			for(com.alitalia.aem.ws.booking.searchservice.xsd14.ResultBookingDetailsSolutionPricing s : source.getPricingField().getResultBookingDetailsSolutionPricing()){
				pricingField.add(rbdsPricingConverter.convert(s));
			}
				
			destination.setPricingField(pricingField);
			destination.setRefundPriceField(rbdsRefundPriceDataConverter.convert(source.getRefundPriceField()));
			destination.setSaleTaxTotalField(source.getSaleTaxTotalField());
			destination.setSaleTotalField(source.getSaleTotalField());
		    destination.setTimeField(source.getTimeField());
		}
	    return destination;
	}

}
