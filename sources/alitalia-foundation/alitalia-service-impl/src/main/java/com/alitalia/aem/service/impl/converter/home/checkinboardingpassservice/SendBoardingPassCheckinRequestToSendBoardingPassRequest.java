package com.alitalia.aem.service.impl.converter.home.checkinboardingpassservice;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.messages.home.SendBoardingPassCheckinRequest;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd1.Resource;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd1.SendBoardingPassRequest;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd2.ArrayOfPassenger;


@Component(immediate=true, metatype=false)
@Service(value=SendBoardingPassCheckinRequestToSendBoardingPassRequest.class)
public class SendBoardingPassCheckinRequestToSendBoardingPassRequest implements Converter<SendBoardingPassCheckinRequest, SendBoardingPassRequest> {

	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;
	
	@Reference
	private CheckinPassengerDataToPassenger checkinPassengerDataConverter;
	

	@Override
	public SendBoardingPassRequest convert(SendBoardingPassCheckinRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		SendBoardingPassRequest destination = null;

		if (source != null) {
			destination = objectFactory.createSendBoardingPassRequest();
			
			destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			destination.setX003CResourceX003EKBackingField(Resource.WEB);
			
			destination.setAttachment(objectFactory.createSendBoardingPassRequestAttachment(source.getAttachment()));
			destination.setAttachmentName(objectFactory.createSendBoardingPassRequestAttachmentName(source.getAttachmentName()));
			destination.setEmailBody(objectFactory.createSendBoardingPassRequestEmailBody(source.getEmailBody()));
			destination.setEmailSubject(objectFactory.createSendBoardingPassRequestEmailSubject(source.getEmailSubject()));
			destination.setSmsBodyText(objectFactory.createSendBoardingPassRequestSmsBodyText(source.getSmsBodyText()));
			
			List<CheckinPassengerData> sourcePassengerList = source.getPassengers();
			if(sourcePassengerList != null){
				ArrayOfPassenger passengersList = new com.alitalia.aem.ws.checkin.boardingpassservice.xsd2.ObjectFactory().createArrayOfPassenger();
				for(CheckinPassengerData sourcePassengerData : sourcePassengerList){
					passengersList.getPassenger().add(checkinPassengerDataConverter.convert(sourcePassengerData));
				}
				destination.setPassengers(objectFactory.createBoardingPassRequestPassengers(passengersList));
			}

			
		}

		return destination;
	}


}
