package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import java.util.Calendar;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MetaSearchDestinationData;
import com.alitalia.aem.common.data.home.SearchDestinationData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.searchservice.xsd3.MetaSearchDestination;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ObjectFactory;
import com.alitalia.aem.ws.booking.searchservice.xsd3.RouteType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.SearchDestination;
import com.alitalia.aem.ws.booking.searchservice.xsd4.Cabin;
import com.alitalia.aem.ws.booking.searchservice.xsd4.TimeType;

@Component(immediate=true, metatype=false)
@Service(value=SearchDestinationDataToSearchDestination.class)
public class SearchDestinationDataToSearchDestination implements Converter<SearchDestinationData, SearchDestination> {

	@Reference
	private BookingSearchAirportDataToAirport airportConverter;
	
	@Override
	public SearchDestination convert(SearchDestinationData source) {
		ObjectFactory objectFactory =  new ObjectFactory();
		SearchDestination destination = objectFactory.createSearchDestination();
		
		if (source instanceof MetaSearchDestinationData){
			destination = objectFactory.createMetaSearchDestination();
			((MetaSearchDestination) destination).setSlices(((MetaSearchDestinationData) source).getSlices());
			((MetaSearchDestination) destination).setCode(objectFactory.createMetaSearchDestinationCode(((MetaSearchDestinationData) source).getCode()));
			((MetaSearchDestination) destination).setFlight(((MetaSearchDestinationData) source).getFlight());
			((MetaSearchDestination) destination).setArrivalDate(XsdConvertUtils.toXMLGregorianCalendar(((MetaSearchDestinationData) source).getArrivalDate()));
			((MetaSearchDestination) destination).setBookingClass(objectFactory.createMetaSearchDestinationBookingClass(((MetaSearchDestinationData) source).getBookingClass()));
			if (((MetaSearchDestinationData) source).getCabinClass() != null)
				((MetaSearchDestination) destination).setCabinClass(Cabin.fromValue(((MetaSearchDestinationData) source).getCabinClass().value()));
		}

		Calendar departureDate = source.getDepartureDate();
		if (departureDate != null)
			destination.setDepartureDate(XsdConvertUtils.toXMLGregorianCalendar(departureDate));

		destination.setSelectedOffer(source.getSelectedOffer());
		destination.setFrom(objectFactory.createASearchDestinationFrom(airportConverter.convert(source.getFromAirport())));
		destination.setTo(objectFactory.createSearchDestinationTo(airportConverter.convert(source.getToAirport())));
		destination.setTimeType(TimeType.fromValue(source.getTimeType().value()));
		destination.setType(RouteType.fromValue(source.getRouteType().value()));

		return destination;
	}

}
