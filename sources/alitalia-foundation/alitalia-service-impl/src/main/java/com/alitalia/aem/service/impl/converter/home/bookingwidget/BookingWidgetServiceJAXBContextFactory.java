package com.alitalia.aem.service.impl.converter.home.bookingwidget;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.booking.widgetservice.xsd5.AdultPassenger;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.ApplicantPassenger;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.CashAndMiles;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.ChildPassenger;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.ECoupon;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.InfantPassenger;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.InfoBase;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.TicketInfo;

@Component(immediate=true, metatype=false)
@Service(value=BookingWidgetServiceJAXBContextFactory.class)
public class BookingWidgetServiceJAXBContextFactory {

	private static final Logger logger = LoggerFactory.getLogger(BookingWidgetServiceJAXBContextFactory.class);

	private JAXBContext jaxbContext = null;

	public synchronized JAXBContext getJaxbContext() throws JAXBException {

		if (jaxbContext == null) {
			jaxbContext = JAXBContext.newInstance(ApplicantPassenger.class,
								AdultPassenger.class,
								ChildPassenger.class,
								InfantPassenger.class,
								com.alitalia.aem.ws.booking.widgetservice.xsd11.ResultBookingDetails.class,
								com.alitalia.aem.ws.booking.widgetservice.xsd12.ResultBookingDetails.class,
								CashAndMiles.class,
								ECoupon.class,
								InfoBase.class,
								TicketInfo.class
								);
		}

		return jaxbContext;
	}

	@Activate
	private void activate() {
		try {
			jaxbContext = JAXBContext.newInstance(ApplicantPassenger.class,
					AdultPassenger.class,
					ChildPassenger.class,
					InfantPassenger.class,
					com.alitalia.aem.ws.booking.widgetservice.xsd11.ResultBookingDetails.class,
					com.alitalia.aem.ws.booking.widgetservice.xsd12.ResultBookingDetails.class,
					CashAndMiles.class,
					InfoBase.class,
					TicketInfo.class
					);
		} catch (JAXBException e) {
			logger.warn("BookingSearchServiceJAXBContextFactory - cannot instantiate JAXBContext: {}", e.getMessage());
		}
	}
}
