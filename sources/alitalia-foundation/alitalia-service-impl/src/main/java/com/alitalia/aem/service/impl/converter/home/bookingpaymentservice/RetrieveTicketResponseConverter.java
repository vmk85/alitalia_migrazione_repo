package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.PaymentProcessInfoData;
import com.alitalia.aem.common.messages.home.RetrieveTicketResponse;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ProcessInfo;

@Component(metatype=false, immediate=true)
@Service(value=RetrieveTicketResponseConverter.class)
public class RetrieveTicketResponseConverter implements Converter<com.alitalia.aem.ws.booking.paymentservice.xsd1.RetrieveTicketResponse, RetrieveTicketResponse> {

	private static final Logger logger = LoggerFactory.getLogger(RetrieveTicketResponseConverter.class);

	@Reference
	private PassengerBaseToAPassengerBaseData passengerConverter;
	
	@Reference
	private ProcessInfoToPaymentProcessInfoData processConverter;
	
	@Reference
	private BookingPaymentServiceJAXBContextFactory jaxbContextFactory;
	
	@Override
	public RetrieveTicketResponse convert(com.alitalia.aem.ws.booking.paymentservice.xsd1.RetrieveTicketResponse source) {
		RetrieveTicketResponse response = new RetrieveTicketResponse();
		List<PassengerBaseData> passengers = new ArrayList<PassengerBaseData>();
		for(Object passenger : source.getPassengers().getValue().getAnyType())
			passengers.add(passengerConverter.convert(passenger));
		response.setPassenger(passengers);
		JAXBElement<Object> sourceProcess = source.getProcess();
		PaymentProcessInfoData paymentProcessInfoData = null;
		if (sourceProcess != null && sourceProcess.getValue() != null) {
			try {
				Unmarshaller unmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
				@SuppressWarnings("unchecked")
				JAXBElement<ProcessInfo> unmarshalledObject = 
						(JAXBElement<ProcessInfo>) unmarshaller.unmarshal((Node) sourceProcess.getValue());
				paymentProcessInfoData = processConverter.convert((ProcessInfo) unmarshalledObject.getValue());
			} catch (JAXBException e) {
				logger.error("Error creating JAXBContext/Unmarshaller for a Passenger class: {}", e);
			}
		}
		response.setProcess(paymentProcessInfoData);
		response.setCookie(source.getCookie().getValue());
		response.setExecute(source.getExecution().getValue());
		response.setSabreGateWayAuthToken(source.getSabreGateWayAuthToken().getValue());
		
		return response;
	}

}
