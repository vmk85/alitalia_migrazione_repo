package com.alitalia.aem.service.impl.converter.home.checkincomfortseatservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.ComfortSeatPriceCheckinRequest;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.ComfortSeatPriceRequest;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.Resource;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.SearchReason;


@Component(immediate=true, metatype=false)
@Service(value=ComfortSeatPriceCheckinRequestToComfortSeatPriceRequest.class)
public class ComfortSeatPriceCheckinRequestToComfortSeatPriceRequest implements Converter<ComfortSeatPriceCheckinRequest, ComfortSeatPriceRequest> {

	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;
	

	@Override
	public ComfortSeatPriceRequest convert(ComfortSeatPriceCheckinRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		ComfortSeatPriceRequest destination = null;

		if (source != null) {
			destination = objectFactory.createComfortSeatPriceRequest();
			
			destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			destination.setX003CResourceX003EKBackingField(Resource.WEB);
				
			destination.setBoardApt(objectFactory.createComfortSeatPriceRequestBoardApt(source.getBoardApt()));
			destination.setCurrency(objectFactory.createComfortSeatPriceRequestCurrency(source.getCurrency()));
			destination.setLandApt(objectFactory.createComfortSeatPriceRequestLandApt(source.getLandApt()));
			destination.setMarket(objectFactory.createComfortSeatPriceRequestMarket(source.getMarket()));		
			if(source.getReason() != null){
				destination.setReason(SearchReason.fromValue(source.getReason().value()));
			}
		}

		return destination;
	}


}
