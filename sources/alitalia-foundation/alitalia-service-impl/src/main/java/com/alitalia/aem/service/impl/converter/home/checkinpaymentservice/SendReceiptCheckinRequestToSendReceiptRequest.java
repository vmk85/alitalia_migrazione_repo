package com.alitalia.aem.service.impl.converter.home.checkinpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.SendReceiptCheckinRequest;
import com.alitalia.aem.ws.checkin.paymentservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.paymentservice.xsd1.Resource;
import com.alitalia.aem.ws.checkin.paymentservice.xsd1.SendReceiptRequest;


@Component(immediate=true, metatype=false)
@Service(value=SendReceiptCheckinRequestToSendReceiptRequest.class)
public class SendReceiptCheckinRequestToSendReceiptRequest implements Converter<SendReceiptCheckinRequest, SendReceiptRequest> {
	
	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;
	
	
	@Override
	public SendReceiptRequest convert(SendReceiptCheckinRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();		
		SendReceiptRequest destination = objectFactory.createSendReceiptRequest();
		
		destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
		destination.setX003CResourceX003EKBackingField(Resource.WEB);
		
		destination.setAttachment(objectFactory.createSendReceiptRequestAttachment(source.getAttachment()));
		destination.setAttachmentName(objectFactory.createSendReceiptRequestAttachmentName(source.getAttachmentName()));
		destination.setEmailAddress(objectFactory.createSendReceiptRequestEmailAddress(source.getEmailAddress()));
		destination.setEmailBody(objectFactory.createSendReceiptRequestEmailBody(source.getEmailBody()));
		destination.setEmailSubject(objectFactory.createSendReceiptRequestEmailSubject(source.getEmailSubject()));	

		return destination;
	}
}