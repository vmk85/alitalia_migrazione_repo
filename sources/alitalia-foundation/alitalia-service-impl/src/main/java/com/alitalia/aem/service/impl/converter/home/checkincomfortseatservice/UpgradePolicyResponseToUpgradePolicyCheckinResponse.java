package com.alitalia.aem.service.impl.converter.home.checkincomfortseatservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinUpgradeClassPolicyData;
import com.alitalia.aem.common.messages.home.UpgradePolicyCheckinResponse;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.UpgradePolicyResponse;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd2.UpgradeClassPolicy;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd2.UpgradePolicy;


@Component(immediate=true, metatype=false)
@Service(value=UpgradePolicyResponseToUpgradePolicyCheckinResponse.class)
public class UpgradePolicyResponseToUpgradePolicyCheckinResponse implements Converter<UpgradePolicyResponse, UpgradePolicyCheckinResponse> {


	@Reference
	private UpgradeClassPolicyToCheckinUpgradeClassPolicyData upgradeClassPolicyConverter;
	
	@Override
	public UpgradePolicyCheckinResponse convert(UpgradePolicyResponse source) {
		UpgradePolicyCheckinResponse destination = null;

		if (source != null) {
			destination = new UpgradePolicyCheckinResponse();

			JAXBElement<UpgradePolicy> sourcePolicy = source.getPolicy();
			if(sourcePolicy != null &&
					sourcePolicy.getValue() != null &&
					sourcePolicy.getValue().getX003CClassPoliciesX003EKBackingField() != null &&
					sourcePolicy.getValue().getX003CClassPoliciesX003EKBackingField().getUpgradeClassPolicy() != null &&
					!sourcePolicy.getValue().getX003CClassPoliciesX003EKBackingField().getUpgradeClassPolicy().isEmpty()){
				List<CheckinUpgradeClassPolicyData> policyList = new ArrayList<CheckinUpgradeClassPolicyData>();
				for(UpgradeClassPolicy sourcePolicyItem : sourcePolicy.getValue().getX003CClassPoliciesX003EKBackingField().getUpgradeClassPolicy()){
					policyList.add(upgradeClassPolicyConverter.convert(sourcePolicyItem));
				}
				destination.setPolicy(policyList);
			}
			
		}

		return destination;
	}

}
