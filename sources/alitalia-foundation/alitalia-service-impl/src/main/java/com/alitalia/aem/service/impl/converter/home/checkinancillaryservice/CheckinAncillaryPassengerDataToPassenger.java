package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryEticketData;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryPassengerData;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ArrayOfETicket;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.Passenger;


@Component(immediate=true, metatype=false)
@Service(value=CheckinAncillaryPassengerDataToPassenger.class)
public class CheckinAncillaryPassengerDataToPassenger implements Converter<CheckinAncillaryPassengerData, Passenger> {

	@Reference
	private CheckinAncillaryETicketDataToETicket checkinAncillaryETicketDataConverter;

	@Reference
	private MmbAncillaryFrequentFlyerDataToFrequentFlyer mmbAncillaryFrequentFlyerDataConverter;
	
	@Override
	public Passenger convert(CheckinAncillaryPassengerData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		Passenger destination = null;

		if (source != null) {
			destination = objectFactory.createPassenger();

			destination.setX003CIdX003EKBackingField(source.getId());
			destination.setX003CLastNameX003EKBackingField(source.getLastName());
			destination.setX003CNameX003EKBackingField(source.getName());
			destination.setX003CReferenceNumberX003EKBackingField(source.getReferenceNumber());

			ArrayOfETicket tickets = objectFactory.createArrayOfETicket();
			for (CheckinAncillaryEticketData sourceETicket : source.getEtickets()) {
				tickets.getETicket().add(checkinAncillaryETicketDataConverter.convert(sourceETicket));
			}
			destination.setX003CETicketsX003EKBackingField(tickets);

			destination.setX003CFrequentFlyerInfoX003EKBackingField(
					mmbAncillaryFrequentFlyerDataConverter.convert(source.getFrequentFlyerInfo()));
		}

		return destination;
	}

}
