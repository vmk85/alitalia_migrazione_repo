package com.alitalia.aem.service.impl.converter.home.checkinextrabaggageservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.ExtraBaggageOrderCheckinRequest;
import com.alitalia.aem.common.messages.home.ExtraBaggagePolicyCheckinRequest;
import com.alitalia.aem.ws.checkin.extrabaggageservice.xsd1.ExtraBaggageOrderRequest;
import com.alitalia.aem.ws.checkin.extrabaggageservice.xsd1.ExtraBaggagePolicyRequest;
import com.alitalia.aem.ws.checkin.extrabaggageservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.extrabaggageservice.xsd1.Resource;


@Component(immediate=true, metatype=false)
@Service(value=ExtraBaggagePolicyCheckinRequestToExtraBaggagePolicyRequest.class)
public class ExtraBaggagePolicyCheckinRequestToExtraBaggagePolicyRequest implements Converter<ExtraBaggagePolicyCheckinRequest, ExtraBaggagePolicyRequest> {

	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;

	@Override
	public ExtraBaggagePolicyRequest convert(ExtraBaggagePolicyCheckinRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		ExtraBaggagePolicyRequest destination = null;

		if (source != null) {
			destination = objectFactory.createExtraBaggagePolicyRequest();
			
			destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			destination.setX003CResourceX003EKBackingField(Resource.WEB);
	
			destination.setAnySpecialPassenger(source.getAnySpecialPassenger());
			destination.setBagaAllowance(source.getBagaAllowance());
			destination.setCodAptFrom(objectFactory.createExtraBaggagePolicyRequestCodAptFrom(source.getCodAptFrom()));
			destination.setCodAptTo(objectFactory.createExtraBaggagePolicyRequestCodAptTo(source.getCodAptTo()));
			destination.setMarketCode(objectFactory.createExtraBaggagePolicyRequestMarketCode(source.getMarketCode()));
		}

		return destination;
	}


}
