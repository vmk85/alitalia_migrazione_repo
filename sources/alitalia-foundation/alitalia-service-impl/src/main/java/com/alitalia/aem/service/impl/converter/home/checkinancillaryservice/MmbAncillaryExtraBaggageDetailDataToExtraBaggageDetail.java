package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryExtraBaggageDetailData;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ExtraBaggageDetail;


@Component(immediate=true, metatype=false)
@Service(value=MmbAncillaryExtraBaggageDetailDataToExtraBaggageDetail.class)
public class MmbAncillaryExtraBaggageDetailDataToExtraBaggageDetail implements
		Converter<MmbAncillaryExtraBaggageDetailData, ExtraBaggageDetail> {

	@Override
	public ExtraBaggageDetail convert(MmbAncillaryExtraBaggageDetailData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		ExtraBaggageDetail destination = null;

		if (source != null) {
			destination = objectFactory.createExtraBaggageDetail();

			destination.setX003CBaggageTypeX003EKBackingField(source.getBaggageType());
		}

		return destination;
	}

}
