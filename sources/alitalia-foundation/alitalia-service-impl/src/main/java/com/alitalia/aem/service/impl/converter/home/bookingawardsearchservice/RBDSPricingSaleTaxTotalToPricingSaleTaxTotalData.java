//2
package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingSaleTaxTotalData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetailsSolutionPricingSaleTaxTotal;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingSaleTaxTotalToPricingSaleTaxTotalData.class)
public class RBDSPricingSaleTaxTotalToPricingSaleTaxTotalData implements
		Converter<ResultBookingDetailsSolutionPricingSaleTaxTotal, ResultBookingDetailsSolutionPricingSaleTaxTotalData> {

	@Override
	public ResultBookingDetailsSolutionPricingSaleTaxTotalData convert(
			ResultBookingDetailsSolutionPricingSaleTaxTotal source) {
		ResultBookingDetailsSolutionPricingSaleTaxTotalData destination = null;
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingSaleTaxTotalData();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}

	public ResultBookingDetailsSolutionPricingSaleTaxTotalData convert(
			com.alitalia.aem.ws.bookingaward.searchservice.xsd13.ResultBookingDetailsSolutionPricingSaleTaxTotal source) {
		ResultBookingDetailsSolutionPricingSaleTaxTotalData destination = null;
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingSaleTaxTotalData();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}
}
