package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowanceData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowanceData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareCalculationData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingPassengerData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingTaxData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionPricing;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowance;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowance;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionPricingFare;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionPricingFareCalculation;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionPricingPassenger;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionPricingTax;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingToRTDSPricingData.class)
public class RTDSPricingToRTDSPricingData implements
		Converter<ResultTicketingDetailSolutionPricing, ResultTicketingDetailSolutionPricingData> {

	@Reference
	private RTDSPricingCarryOnBaggageAllowanceFreeBaggageAllowanceToRTDSPricingCarryOnBaggageAllowanceFreeBaggageAllowanceData carryOnBaggageAllowanceFieldConverter;

	@Reference
	private RTDSPricingCheckedBaggageAllowanceFreeBaggageAllowanceToRTDSPricingCheckedBaggageAllowanceFreeBaggageAllowanceData checkedBaggageAllowanceFieldConverter;

	@Reference
	private RTDSPricingDisplayFareTotalToRTDSPricingDisplayFareTotalData displayFareTotalFieldConverter;

	@Reference
	private RTDSPricingDisplayPriceToRTDSPricingDisplayPriceData displayPriceFieldConverter;

	@Reference
	private RTDSPricingDisplayTaxTotalToRTDSPricingDisplayTaxTotalData displayTaxTotalFieldConverter;

	@Reference
	private RTDSPricingExtToRTDSPricingExtData extFieldConverter;

	@Reference
	private RTDSPricingSaleFareTotalToRTDSPricingSaleFareTotalData saleFareTotalFieldConverter;

	@Reference
	private RTDSPricingSalePriceToRTDSPricingSalePriceData salePriceFieldConverter;

	@Reference
	private RTDSPricingSaleTaxTotalToRTDSPricingSaleTaxTotalData saleTaxTotalFieldConverter;

	@Reference
	private RTDSPricingFareCalculationToRTDSPricingFareCalculationData fareCalculationFieldConverter;

	@Reference
	private RTDSPricingFareToRTDSPricingFareData fareFieldConverter;

	@Reference
	private RTDSPricingPassengerToRTDSPricingPassengerData passengerFieldConverter;

	@Reference
	private RTDSPricingTaxToRTDSPricingTaxData taxFieldConverter;

	@Override
	public ResultTicketingDetailSolutionPricingData convert(ResultTicketingDetailSolutionPricing source) {
		ResultTicketingDetailSolutionPricingData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingData();

			destination.setPtcField(source.getPtcField());

			List<ResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowanceData> carryOnBaggageAllowanceField = null;
			if (source.getCarryOnBaggageAllowanceField() != null &&
					source.getCarryOnBaggageAllowanceField().getResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowance() != null &&
					!source.getCarryOnBaggageAllowanceField().getResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowance().isEmpty()) {
				carryOnBaggageAllowanceField = new ArrayList<ResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowanceData>();
				for(ResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowance sourceCarryOnBaggageAllowanceFieldElem : 
						source.getCarryOnBaggageAllowanceField().getResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowance())
					carryOnBaggageAllowanceField.add(carryOnBaggageAllowanceFieldConverter.convert(sourceCarryOnBaggageAllowanceFieldElem));
			}
			destination.setCarryOnBaggageAllowanceField(carryOnBaggageAllowanceField);

			List<ResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowanceData> checkedBaggageAllowanceField = null;
			if (source.getCheckedBaggageAllowanceField() != null &&
					source.getCheckedBaggageAllowanceField().getResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowance() != null &&
					!source.getCheckedBaggageAllowanceField().getResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowance().isEmpty()) {
				checkedBaggageAllowanceField = new ArrayList<ResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowanceData>();
				for(ResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowance sourceCheckedBaggageAllowanceFieldElem : 
						source.getCheckedBaggageAllowanceField().getResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowance())
					checkedBaggageAllowanceField.add(checkedBaggageAllowanceFieldConverter.convert(sourceCheckedBaggageAllowanceFieldElem));
				
			}
			destination.setCheckedBaggageAllowanceField(checkedBaggageAllowanceField);

			destination.setDisplayFareTotalField(
					displayFareTotalFieldConverter.convert(source.getDisplayFareTotalField()));

			destination.setDisplayPriceField(
					displayPriceFieldConverter.convert(source.getDisplayPriceField()));

			destination.setDisplayTaxTotalField(
					displayTaxTotalFieldConverter.convert(source.getDisplayTaxTotalField()));

			destination.setExtField(
					extFieldConverter.convert(source.getExtField()));

			List<ResultTicketingDetailSolutionPricingFareCalculationData> fareCalculationField = null;
			if (source.getFareCalculationField() != null &&
					source.getFareCalculationField().getResultTicketingDetailSolutionPricingFareCalculation() != null &&
					!source.getFareCalculationField().getResultTicketingDetailSolutionPricingFareCalculation().isEmpty()) {
				fareCalculationField = new ArrayList<ResultTicketingDetailSolutionPricingFareCalculationData>();
				for(ResultTicketingDetailSolutionPricingFareCalculation sourceFareCalculationFieldElem : 
						source.getFareCalculationField().getResultTicketingDetailSolutionPricingFareCalculation())
					fareCalculationField.add(fareCalculationFieldConverter.convert(sourceFareCalculationFieldElem));
			}
			destination.setFareCalculationField(fareCalculationField);

			List<ResultTicketingDetailSolutionPricingFareData> fareField = null;
			if (source.getFareField() != null &&
					source.getFareField().getResultTicketingDetailSolutionPricingFare() != null &&
					!source.getFareField().getResultTicketingDetailSolutionPricingFare().isEmpty()) {
				fareField = new ArrayList<ResultTicketingDetailSolutionPricingFareData>();
				for (ResultTicketingDetailSolutionPricingFare sourceFareFieldElem : 
						source.getFareField().getResultTicketingDetailSolutionPricingFare())
					fareField.add(fareFieldConverter.convert(sourceFareFieldElem));
			}
			destination.setFareField(fareField);

			List<ResultTicketingDetailSolutionPricingPassengerData> passengerField = null;
			if (source.getPassengerField() != null &&
					source.getPassengerField().getResultTicketingDetailSolutionPricingPassenger() != null &&
					!source.getPassengerField().getResultTicketingDetailSolutionPricingPassenger().isEmpty()) {
				passengerField = new ArrayList<ResultTicketingDetailSolutionPricingPassengerData>();
				for(ResultTicketingDetailSolutionPricingPassenger sourcePassengerFieldElem : 
						source.getPassengerField().getResultTicketingDetailSolutionPricingPassenger())
					passengerField.add(passengerFieldConverter.convert(sourcePassengerFieldElem));
			}
			destination.setPassengerField(passengerField);

			destination.setSaleFareTotalField(
					saleFareTotalFieldConverter.convert(source.getSaleFareTotalField()));

			destination.setSalePriceField(
					salePriceFieldConverter.convert(source.getSalePriceField()));

			destination.setSaleTaxTotalField(
					saleTaxTotalFieldConverter.convert(source.getSaleTaxTotalField()));

			List<ResultTicketingDetailSolutionPricingTaxData> taxField = null;
			if (source.getTaxField() != null &&
					source.getTaxField().getResultTicketingDetailSolutionPricingTax() != null &&
					!source.getTaxField().getResultTicketingDetailSolutionPricingTax().isEmpty()) {
				taxField = new ArrayList<ResultTicketingDetailSolutionPricingTaxData>();
				for(ResultTicketingDetailSolutionPricingTax sourceTaxFieldElem : 
						source.getTaxField().getResultTicketingDetailSolutionPricingTax())
					taxField.add(taxFieldConverter.convert(sourceTaxFieldElem));
			}
			destination.setTaxField(taxField);
		}

		return destination;
	}

}
