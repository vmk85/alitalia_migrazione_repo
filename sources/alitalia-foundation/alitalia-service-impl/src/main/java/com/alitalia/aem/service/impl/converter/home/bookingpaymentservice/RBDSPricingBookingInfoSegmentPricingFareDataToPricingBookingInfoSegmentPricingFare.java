package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData;
import com.alitalia.aem.ws.booking.paymentservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd11.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFare;
import com.alitalia.aem.ws.booking.paymentservice.xsd11.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummary;


@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingBookingInfoSegmentPricingFareDataToPricingBookingInfoSegmentPricingFare.class)
public class RBDSPricingBookingInfoSegmentPricingFareDataToPricingBookingInfoSegmentPricingFare
		implements Converter<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData, ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFare> {

	@Reference
	RBDSPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesDataToRBDSBInfoSegmentPricing convertPenalties;
	
	@Override
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFare convert(
			ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData source) {
		ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFare destination = null;
		
		if(source != null){
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFare();
			ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummary ruleSummaryField =
							objectFactory.createResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummary();
				ruleSummaryField.setPenaltiesField(convertPenalties.convert(source.getRuleSummaryField().getPenaltiesField()));
			destination.setRuleSummaryField(ruleSummaryField);
		}
		return destination;
	}

}
