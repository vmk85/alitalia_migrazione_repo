package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSaleTotalData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionSaleTotal;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSaleTotalDataToRTDSSaleTotal.class)
public class RTDSSaleTotalDataToRTDSSaleTotal implements
		Converter<ResultTicketingDetailSolutionSaleTotalData, 
					ResultTicketingDetailSolutionSaleTotal> {

	@Override
	public ResultTicketingDetailSolutionSaleTotal convert(ResultTicketingDetailSolutionSaleTotalData source) {
		ResultTicketingDetailSolutionSaleTotal destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionSaleTotal();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
