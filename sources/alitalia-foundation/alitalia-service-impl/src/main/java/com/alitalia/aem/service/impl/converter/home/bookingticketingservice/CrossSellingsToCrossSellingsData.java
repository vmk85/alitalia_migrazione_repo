package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.CodeDescriptionData;
import com.alitalia.aem.common.data.home.CrossSellingBaseData;
import com.alitalia.aem.common.data.home.CrossSellingsData;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.CodeDescription;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.CrossSellingBase;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.CrossSellings;

@Component(immediate = true, metatype = false)
@Service(value = CrossSellingsToCrossSellingsData.class)
public class CrossSellingsToCrossSellingsData implements Converter<CrossSellings, CrossSellingsData> {

	@Reference
	private CodeDescriptionToCodeDescriptionData codeDescriptionToCodeDescriptionDataConverter;
	
	@Reference
	private CrossSellingBaseToCrossSellingBaseData crossSellingBaseToCrossSellingBaseDataConverter;

	@Override
	public CrossSellingsData convert(CrossSellings source) {
		CrossSellingsData destination = null;

		if (source != null) {
			destination = new CrossSellingsData();

			List<CodeDescriptionData> crossSellingInformationDatas = new ArrayList<CodeDescriptionData>();
			if (source.getCrossSellingInformations() != null && 
					source.getCrossSellingInformations().getValue() != null) {
				for (Object object : source.getCrossSellingInformations().getValue().getAnyType()) {
					crossSellingInformationDatas.add(codeDescriptionToCodeDescriptionDataConverter.convert((CodeDescription)object));
				}
			}
			destination.setCrossSellingInformations(crossSellingInformationDatas);
			
			List<CrossSellingBaseData> items = new ArrayList<CrossSellingBaseData>();
			if (source.getItems() != null && 
					source.getItems().getValue() != null) {
				for (Object object : source.getItems().getValue().getAnyType()) {
					items.add(crossSellingBaseToCrossSellingBaseDataConverter.convert((CrossSellingBase)object));
				}
			}
			destination.setItems(items);
		}

		return destination;
	}

}
