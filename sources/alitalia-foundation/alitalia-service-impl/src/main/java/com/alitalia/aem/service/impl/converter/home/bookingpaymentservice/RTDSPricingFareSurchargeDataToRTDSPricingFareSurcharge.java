package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareSurchargeData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingFareSurcharge;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareSurchargeDataToRTDSPricingFareSurcharge.class)
public class RTDSPricingFareSurchargeDataToRTDSPricingFareSurcharge implements
		Converter<ResultTicketingDetailSolutionPricingFareSurchargeData, 
			ResultTicketingDetailSolutionPricingFareSurcharge> {

	@Reference
	private RTDSPricingFareSurchargePriceDataToRTDSPricingFareSurchargePrice rtdsPriceFieldConverter;

	@Override
	public ResultTicketingDetailSolutionPricingFareSurcharge convert(
			ResultTicketingDetailSolutionPricingFareSurchargeData source) {
		ResultTicketingDetailSolutionPricingFareSurcharge destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingFareSurcharge();

			destination.setCodeField(source.getCodeField());

			destination.setPriceField(rtdsPriceFieldConverter.convert(source.getPriceField()));
		}

		return destination;
	}

}
