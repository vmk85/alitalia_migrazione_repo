package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingDisplayTaxTotalData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionPricingDisplayTaxTotal;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingDisplayTaxTotalToRTDSPricingDisplayTaxTotalData.class)
public class RTDSPricingDisplayTaxTotalToRTDSPricingDisplayTaxTotalData implements
		Converter<ResultTicketingDetailSolutionPricingDisplayTaxTotal, 
					ResultTicketingDetailSolutionPricingDisplayTaxTotalData> {

	@Override
	public ResultTicketingDetailSolutionPricingDisplayTaxTotalData convert(
			ResultTicketingDetailSolutionPricingDisplayTaxTotal source) {
		ResultTicketingDetailSolutionPricingDisplayTaxTotalData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingDisplayTaxTotalData();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
