package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.BillingData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd3.Billing;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd3.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=BillingDataToBilling.class)
public class BillingDataToBilling 
		implements Converter<BillingData, Billing> {
	
	@Override
	public Billing convert(BillingData source) {
		
		Billing destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			destination = objectFactory.createBilling();
			
			destination.setX003CBillX003EKBackingField(
					source.isBill());
			
			destination.setX003CCapX003EKBackingField(
					source.getCap());
			
			destination.setX003CCfIntX003EKBackingField(
					source.getCfInt());
			
			destination.setX003CCfPaxX003EKBackingField(
					source.getCfPax());
			
			destination.setX003CCFPIVAX003EKBackingField(
					source.getCFPIVA());
			
			destination.setX003CEmailIntX003EKBackingField(
					source.getEmailInt());
			
			destination.setX003CEnteEmitX003EKBackingField(
					source.getEnteEmit());
			
			destination.setX003CEnteRichX003EKBackingField(
					source.getEnteRich());
			
			destination.setX003CFormaSocietariaX003EKBackingField(
					source.getFormaSocietaria());
			
			destination.setX003CIndSpedX003EKBackingField(
					source.getIndSped());
			
			destination.setX003CIntFatturaX003EKBackingField(
					source.getIntFattura());
			
			destination.setX003CLocSpedX003EKBackingField(
					source.getLocSped());
			
			destination.setX003CNameX003EKBackingField(
					source.getName());
			
			destination.setX003CNomeX003EKBackingField(
					source.getNome());
			
			destination.setX003CPaeseX003EKBackingField(
					source.getPaese());
			
			destination.setX003CPivaIntX003EKBackingField(
					source.getPivaInt());
			
			destination.setX003CProvX003EKBackingField(
					source.getProv());
			
			destination.setX003CSelectedCountryX003EKBackingField(
					source.getSelectedCountry());
			
			destination.setX003CSocietaX003EKBackingField(
					source.getSocieta());
			
			destination.setX003CSurnameXYX003EKBackingField(
					source.getSurname());
			
			destination.setX003CTipoRichiestaX003EKBackingField(
					source.getTipoRichiesta());
			
		}
		
		return destination;
	}

}
