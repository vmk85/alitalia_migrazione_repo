package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingExtData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingExt;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingExtDataToRTDSPricingExt.class)
public class RTDSPricingExtDataToRTDSPricingExt implements
		Converter<ResultTicketingDetailSolutionPricingExtData, 
					ResultTicketingDetailSolutionPricingExt> {

	@Override
	public ResultTicketingDetailSolutionPricingExt convert(
			ResultTicketingDetailSolutionPricingExtData source) {
		ResultTicketingDetailSolutionPricingExt destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingExt();

			destination.setPrivateFareField(
					source.isPrivateFareField()!=null && source.isPrivateFareField());

			destination.setPrivateFareFieldSpecified(
					source.isPrivateFareFieldSpecified() != null &&
						source.isPrivateFareFieldSpecified());
		}

		return destination;
	}

}
