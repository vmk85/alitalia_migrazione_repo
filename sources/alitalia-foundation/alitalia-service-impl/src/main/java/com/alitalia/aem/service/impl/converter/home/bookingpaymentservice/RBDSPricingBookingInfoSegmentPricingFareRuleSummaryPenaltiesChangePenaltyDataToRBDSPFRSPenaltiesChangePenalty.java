package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData;
import com.alitalia.aem.ws.booking.paymentservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd11.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenalty;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyDataToRBDSPFRSPenaltiesChangePenalty.class)
public class RBDSPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyDataToRBDSPFRSPenaltiesChangePenalty
		implements Converter<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData, ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenalty> {

	@Override
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenalty convert(
			ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData source) {
		ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenalty destination = null;
		
		if(source!=null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenalty();
			destination.setApplicationField(source.getApplicationField());
			destination.setMaxPriceField(source.getMaxPriceField());
			destination.setMinPriceField(source.getMinPriceField());
		}
		return destination;
	}

}
