package com.alitalia.aem.service.impl.converter.home.checkinboardingpassservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinCouponData;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.enumerations.CheckinPassengerStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.CheckinPassengerTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.FrequentFlyerCategoryEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAQQTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbLinkTypeEnum;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd2.AQQType;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd2.ArrayOfCoupon;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd2.CompartimentalClass;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd2.Coupon;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd2.FrequentFlyerCategory;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd2.LinkType;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd2.Passenger;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd2.PassengerStatus;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd2.PassengerType;


@Component(immediate=true, metatype=false)
@Service(value=PassengerToCheckinPassengerData.class)
public class PassengerToCheckinPassengerData implements Converter<Passenger, CheckinPassengerData> {


	@Reference
	private ApisDataToCheckinApisInfoData checkinApisDataConverter;

	@Reference
	private BaggageOrderToMmbBaggageOrderData baggageOrderConverter;

	@Reference
	private FrequentFlyerCarrierToMmbFrequentFlyerCarrierData frequentFlyerCarrierConverter;

	@Reference
	private CouponToCheckinCouponData couponConverter;

	@Override
	public CheckinPassengerData convert(Passenger source) {
		CheckinPassengerData destination = null;

		if (source != null) {
			destination = new CheckinPassengerData();

			AQQType sourceAQQType = source.getX003CAuthorityPermissionX003EKBackingField();
			if (sourceAQQType != null)
				destination.setAuthorityPermission(MmbAQQTypeEnum.fromValue(sourceAQQType.value()));

			destination.setBaggageAllowance(source.getX003CBaggageAllowanceX003EKBackingField());
			destination.setBaggageAllowanceQuantity(source.getX003CBaggageAllowanceQuantityX003EKBackingField());
			destination.setBoardingPassRetrieveEmail(source.getX003CBoardingPassRetrieveEmailX003EKBackingField());
			destination.setBoardingPassRetrieveSms(source.getX003CBoardingPassRetrieveSmsX003EKBackingField());
			destination.setBoardingPassViaEmail(source.isX003CBoardingPassViaEmailX003EKBackingField());
			destination.setBoardingPassViaSms(source.isX003CBoardingPassViaSmsX003EKBackingField());
			destination.setComfortSeatFree(source.isX003CComfortSeatFreeX003EKBackingField());
			destination.setCorporate(source.isX003CCorporateX003EKBackingField());
			destination.setEmail(source.getX003CEmailX003EKBackingField());
			destination.setEticket(source.getX003CEticketX003EKBackingField());
			destination.setEticketClass(source.getX003CEticketClassX003EKBackingField());
			destination.setFrequentFlyerCode(source.getX003CFrequentFlyerCodeX003EKBackingField());
			destination.setIsGroupPnr(source.isX003CIsGroupPnrX003EKBackingField());
			destination.setId(source.getX003CIdX003EKBackingField());
			destination.setLastName(source.getX003CLastNameX003EKBackingField());

			LinkType sourceLinkType = source.getX003CLinkTypeX003EKBackingField();
			if (sourceLinkType != null)
				destination.setLinkType(MmbLinkTypeEnum.fromValue(sourceLinkType.value()));

			destination.setName(source.getX003CNameX003EKBackingField());
			
			CompartimentalClass sourceOldSeatClass = source.getX003COldSeatClassX003EKBackingField();
			if (sourceOldSeatClass != null){
				destination.setOldSeatClass(MmbCompartimentalClassEnum.fromValue(sourceOldSeatClass.value()));
			}
			destination.setPaymentRetrieveEmail(source.getX003CPaymentRetrieveEmailX003EKBackingField());
			destination.setPaymentViaEmail(source.isX003CPaymentViaEmailX003EKBackingField());
			destination.setPnr(source.getX003CPnrX003EKBackingField());
			destination.setReservationRPH(source.getX003CReservationRPHX003EKBackingField());
			destination.setRouteId(source.getX003CRouteIdX003EKBackingField());
			destination.setSpecialFare(source.isX003CIsSpecialFareX003EKBackingField());

			CompartimentalClass sourceSeatClass = source.getX003CSeatClassX003EKBackingField();
			if (sourceSeatClass != null)
				destination.setSeatClass(MmbCompartimentalClassEnum.fromValue(sourceSeatClass.value()));

			destination.setSelected(source.isX003CSelectedX003EKBackingField());

			PassengerStatus sourceStatus = source.getX003CStatusX003EKBackingField();
			if (sourceStatus != null)
				destination.setStatus(CheckinPassengerStatusEnum.fromValue(sourceStatus.value()));

			destination.setStatusNote(source.getX003CStatusNoteX003EKBackingField());

			PassengerType sourcePassengerType = source.getX003CTypeX003EKBackingField();
			if (sourcePassengerType != null)
				destination.setType(CheckinPassengerTypeEnum.fromValue(sourcePassengerType.value()));

			destination.setApisData(checkinApisDataConverter.convert(source.getX003CApisDataX003EKBackingField()));

			destination.setBaggageOrder(baggageOrderConverter.convert(
					source.getX003COrderX003EKBackingField()));

			destination.setFrequentFlyerCarrier(frequentFlyerCarrierConverter.convert(
					source.getX003CFrequentFlyerCarrierX003EKBackingField()));
			
			FrequentFlyerCategory sourceFrequentFlyerType = source.getX003CFrequentFlyerTypeX003EKBackingField();
			if (sourceFrequentFlyerType != null)
				destination.setFrequentFlyerType(FrequentFlyerCategoryEnum.fromValue(sourceFrequentFlyerType.value()));


			List<CheckinCouponData> couponsList = null;
			ArrayOfCoupon sourceCouponsList = source.getX003CCouponsX003EKBackingField();
			if (sourceCouponsList != null &&
					sourceCouponsList.getCoupon() != null &&
					!sourceCouponsList.getCoupon().isEmpty()) {
				couponsList = new ArrayList<CheckinCouponData>();
				for (Coupon sourceCoupon : sourceCouponsList.getCoupon()) {
					couponsList.add(couponConverter.convert(sourceCoupon));
				}
				destination.setCoupons(couponsList);
			}
		}

		return destination;
	}

}
