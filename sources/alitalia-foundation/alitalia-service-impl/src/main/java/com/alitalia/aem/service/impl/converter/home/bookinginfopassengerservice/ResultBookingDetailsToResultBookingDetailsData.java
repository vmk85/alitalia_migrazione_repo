package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ResultBookingDetails;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ResultBookingDetailsSolution;

@Component(immediate=true, metatype=false)
@Service(value=ResultBookingDetailsToResultBookingDetailsData.class)
public class ResultBookingDetailsToResultBookingDetailsData 
		implements Converter<ResultBookingDetails, ResultBookingDetailsData> {
	
	@Reference
	private ResultBookingDetailsSolutionToResultBookingDetailsSolutionData resultBookingDetailsSolutionConverter;
	
	@Override
	public ResultBookingDetailsData convert(ResultBookingDetails source) {
		
		ResultBookingDetailsData destination = null;
		if (source != null) {
			
			destination = new ResultBookingDetailsData();
			List<ResultBookingDetailsSolutionData> solutionField = new ArrayList<ResultBookingDetailsSolutionData>();
			if(source.getSolutionField()!=null)
			for(ResultBookingDetailsSolution s : source.getSolutionField().getResultBookingDetailsSolution()){
				solutionField.add(resultBookingDetailsSolutionConverter.convert(s));
			}
		destination.setSolutionField(solutionField);
	}
	return destination;
}
}
