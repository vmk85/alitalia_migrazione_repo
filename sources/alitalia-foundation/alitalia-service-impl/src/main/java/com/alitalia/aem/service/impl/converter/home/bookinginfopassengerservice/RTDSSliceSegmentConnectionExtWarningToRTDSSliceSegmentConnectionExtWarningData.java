package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentConnectionExtWarningData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionSliceSegmentConnectionExtWarning;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentConnectionExtWarningToRTDSSliceSegmentConnectionExtWarningData.class)
public class RTDSSliceSegmentConnectionExtWarningToRTDSSliceSegmentConnectionExtWarningData implements
		Converter<ResultTicketingDetailSolutionSliceSegmentConnectionExtWarning, ResultTicketingDetailSolutionSliceSegmentConnectionExtWarningData> {

	@Override
	public ResultTicketingDetailSolutionSliceSegmentConnectionExtWarningData convert(ResultTicketingDetailSolutionSliceSegmentConnectionExtWarning source) {
		ResultTicketingDetailSolutionSliceSegmentConnectionExtWarningData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionSliceSegmentConnectionExtWarningData();

			destination.setLongLayoverField(source.isLongLayoverField());
			destination.setLongLayoverFieldSpecified(source.isLongLayoverFieldSpecified());
			destination.setRiskyConnectionField(source.isRiskyConnectionField());
			destination.setRiskyConnectionFieldSpecified(source.isRiskyConnectionFieldSpecified());
			destination.setTightConnectionField(source.isTightConnectionField());
			destination.setTightConnectionFieldSpecified(source.isTightConnectionFieldSpecified());
		}

		return destination;
	}

}
