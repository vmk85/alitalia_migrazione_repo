package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.WebCheckinRemoveFromCartRequest;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.RemoveFromCartRequest;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.Resource;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd5.ArrayOfint;


@Component(immediate=true, metatype=false)
@Service(value=WebCheckinRemoveFromCartRequestToRemoveFromCartRequest.class)
public class WebCheckinRemoveFromCartRequestToRemoveFromCartRequest implements
		Converter<WebCheckinRemoveFromCartRequest, RemoveFromCartRequest> {

	@Reference
	private MmbRequestBaseInfoDataToRequestBaseInfo mmbRequestBaseInfoDataConverter;

	@Override
	public RemoveFromCartRequest convert(WebCheckinRemoveFromCartRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		com.alitalia.aem.ws.checkin.ancillaryservice.xsd5.ObjectFactory objectFactory5 =
				new com.alitalia.aem.ws.checkin.ancillaryservice.xsd5.ObjectFactory();
		RemoveFromCartRequest destination = null;

		if (source != null) {
			destination = objectFactory.createRemoveFromCartRequest();

			destination.setCartId(source.getCartId());
			destination.setClient(objectFactory.createRemoveFromCartRequestClient(source.getClient()));
			destination.setMachineName(objectFactory.createAddToCartRequestMachineName(source.getMachineName()));
			destination.setSessionId(objectFactory.createAddToCartRequestSessionId(source.getSessionId()));

			destination.setX003CInfoRequestX003EKBackingField(mmbRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			destination.setX003CResourceX003EKBackingField(Resource.WEB);
			
			ArrayOfint ancillaryIds = objectFactory5.createArrayOfint();
			for(Integer sourceAncillaryId : source.getAncillaryIDs())
				ancillaryIds.getInt().add(sourceAncillaryId);
			destination.setAncillaryIDs(objectFactory.createRemoveFromCartRequestAncillaryIDs(ancillaryIds));
		}

		return destination;
	}

}
