package com.alitalia.aem.service.impl.converter.home.checkinpassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.WebCheckinNewPassengerResponse;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.NewPassengerResponse;

@Component(immediate=true, metatype=false)
@Service(value=NewPassengerResponseToWebCheckinNewPassengerResponse.class)
public class NewPassengerResponseToWebCheckinNewPassengerResponse implements
		Converter<NewPassengerResponse, WebCheckinNewPassengerResponse> {

	@Reference
	private PassengerToCheckinPassengerData passengerConverter;

	@Override
	public WebCheckinNewPassengerResponse convert(NewPassengerResponse source) {
		WebCheckinNewPassengerResponse destination = null;

		if (source != null) {
			destination = new WebCheckinNewPassengerResponse();
			destination.setPassenger(passengerConverter.convert(source.getPassenger().getValue()));
		}
		return destination;
	}

}
