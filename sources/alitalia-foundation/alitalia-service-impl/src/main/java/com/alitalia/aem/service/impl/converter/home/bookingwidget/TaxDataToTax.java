package com.alitalia.aem.service.impl.converter.home.bookingwidget;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.TaxData;
import com.alitalia.aem.ws.booking.widgetservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.booking.widgetservice.xsd2.Tax;

@Component(immediate=true, metatype=false)
@Service(value=TaxDataToTax.class)
public class TaxDataToTax implements Converter<TaxData, Tax> {

	@Override
	public Tax convert(TaxData source) {
		ObjectFactory factory2 = new ObjectFactory();
		Tax destination = factory2.createTax();

		destination.setX003CAmountX003EKBackingField(source.getAmount());
		destination.setX003CCodeX003EKBackingField(source.getCode());
		destination.setX003CCurrencyX003EKBackingField(source.getCurrency());
		destination.setX003CIdX003EKBackingField(source.getId());
		destination.setX003CNameX003EKBackingField(source.getName());

		return destination;
	}

}
