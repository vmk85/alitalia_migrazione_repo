package com.alitalia.aem.service.impl.converter.home.carnetservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.CarnetUpdateResponse;
import com.alitalia.aem.ws.carnet.carnetservice.xsd1.UpdateCarnetResponse;
import com.alitalia.aem.ws.carnet.carnetservice.xsd5.InfoCarnet;

@Component(immediate=true, metatype=false)
@Service(value=UpdateCarnetResponseToCarnetUpdateResponse.class)
public class UpdateCarnetResponseToCarnetUpdateResponse implements Converter<UpdateCarnetResponse, CarnetUpdateResponse> {

	@Reference
	private InfoCarnetToCarnetInfoCarnet infoCarnetConverter;
	
	@Reference
	private CarnetServiceJAXBContextFactory jaxbContextFactory;
	
	private Logger logger = LoggerFactory.getLogger(UpdateCarnetResponseToCarnetUpdateResponse.class);
	
	@Override
	public CarnetUpdateResponse convert(UpdateCarnetResponse source) {
		CarnetUpdateResponse destination = null;

		if (source != null) {
			destination = new CarnetUpdateResponse();
			if (source.getInfoCarnet() != null){
				if (source.getInfoCarnet().getValue() instanceof InfoCarnet){
					destination.setInfoCarnet(infoCarnetConverter.convert((InfoCarnet) source.getInfoCarnet().getValue()));
				} else {
					try {
						Unmarshaller carnetUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
						@SuppressWarnings("unchecked")
						JAXBElement<InfoCarnet> unmarshalledObj = (JAXBElement<InfoCarnet>) carnetUnmarshaller.unmarshal((Node) source.getInfoCarnet().getValue());
						InfoCarnet infoCarn = (InfoCarnet) unmarshalledObj.getValue();
						destination.setInfoCarnet(infoCarnetConverter.convert(infoCarn));
					} catch (JAXBException e) {
						logger.error("Error while Unmarshalling InfoCarnet: ", e);
					}
				}
			}
		}

		return destination;
	}

}
