//2
package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingReissueTaxTotalDifferenceData;
import com.alitalia.aem.ws.booking.paymentservice.xsd11.ResultBookingDetailsSolutionPricingReissueTaxTotalDifference;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingReissueTaxTotalDifferenceToPricingReissueTaxTotalDifferenceData.class)
public class RBDSPricingReissueTaxTotalDifferenceToPricingReissueTaxTotalDifferenceData
		implements Converter<ResultBookingDetailsSolutionPricingReissueTaxTotalDifference, ResultBookingDetailsSolutionPricingReissueTaxTotalDifferenceData> {

	@Override
	public ResultBookingDetailsSolutionPricingReissueTaxTotalDifferenceData convert(
			ResultBookingDetailsSolutionPricingReissueTaxTotalDifference source) {
		ResultBookingDetailsSolutionPricingReissueTaxTotalDifferenceData destination = null;
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingReissueTaxTotalDifferenceData();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}

}
