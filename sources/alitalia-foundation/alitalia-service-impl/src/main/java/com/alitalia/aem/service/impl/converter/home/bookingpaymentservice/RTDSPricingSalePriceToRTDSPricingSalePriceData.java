package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingSalePriceData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingSalePrice;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingSalePriceToRTDSPricingSalePriceData.class)
public class RTDSPricingSalePriceToRTDSPricingSalePriceData implements
		Converter<ResultTicketingDetailSolutionPricingSalePrice,
						ResultTicketingDetailSolutionPricingSalePriceData> {

	@Override
	public ResultTicketingDetailSolutionPricingSalePriceData convert(
			ResultTicketingDetailSolutionPricingSalePrice source) {
		ResultTicketingDetailSolutionPricingSalePriceData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingSalePriceData();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
