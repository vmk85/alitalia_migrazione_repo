package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingExtEndorsementData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ResultBookingDetailsSolutionPricingExtEndorsement;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingExtEndorsementToPricingExtEndorsementData.class)
public class RBDSPricingExtEndorsementToPricingExtEndorsementData implements
		Converter<ResultBookingDetailsSolutionPricingExtEndorsement, ResultBookingDetailsSolutionPricingExtEndorsementData> {

	@Override
	public ResultBookingDetailsSolutionPricingExtEndorsementData convert(
			ResultBookingDetailsSolutionPricingExtEndorsement source) {
		ResultBookingDetailsSolutionPricingExtEndorsementData destination = null;
		
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingExtEndorsementData();
			destination.setBoxesField(source.getBoxesField());
			destination.setValueField(source.getValueField());
		}
		
		return destination;
	}
	
}
