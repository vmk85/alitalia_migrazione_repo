package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.AdultPassenger;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ApplicantPassenger;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.CashAndMiles;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ChildPassenger;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.InfantPassenger;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.InfoBase;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.InsurancePolicy;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.Preferences;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.Route;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.Routes;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.TicketInfo;

@Component(immediate=true, metatype=false)
@Service(value=BookingInfoPassengerServiceJAXBContextFactory.class)
public class BookingInfoPassengerServiceJAXBContextFactory {

	private static final Logger logger = LoggerFactory.getLogger(BookingInfoPassengerServiceJAXBContextFactory.class);

	private JAXBContext jaxbContext = null;

	public synchronized JAXBContext getJaxbContext() throws JAXBException {

		if (jaxbContext == null) {
			jaxbContext = JAXBContext.newInstance(Route.class,
								ApplicantPassenger.class,
								AdultPassenger.class,
								ChildPassenger.class,
								InfantPassenger.class,
								com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ResultBookingDetails.class,
								com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetail.class,
								CashAndMiles.class,
								InsurancePolicy.class,
								InfoBase.class,
								TicketInfo.class,
								Preferences.class,
								Routes.class
								);
		}

		return jaxbContext;
	}

	@Activate
	private void activate() {
		try {
			jaxbContext = JAXBContext.newInstance(Route.class,
					ApplicantPassenger.class,
					AdultPassenger.class,
					ChildPassenger.class,
					InfantPassenger.class,
					com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ResultBookingDetails.class,
					com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetail.class,
					CashAndMiles.class,
					InsurancePolicy.class,
					InfoBase.class,
					TicketInfo.class,
					Preferences.class,
					Routes.class
					);
		} catch (JAXBException e) {
			logger.warn("BookingInfoPassengerServiceJAXBContextFactory - cannot instantiate JAXBContext: {}", e.getMessage());
		}
	}
}
