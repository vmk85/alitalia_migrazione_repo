package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingDisplayTaxTotalData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingDisplayTaxTotal;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingDisplayTaxTotalDataToRTDSPricingDisplayTaxTotal.class)
public class RTDSPricingDisplayTaxTotalDataToRTDSPricingDisplayTaxTotal implements
		Converter<ResultTicketingDetailSolutionPricingDisplayTaxTotalData, 
					ResultTicketingDetailSolutionPricingDisplayTaxTotal> {

	@Override
	public ResultTicketingDetailSolutionPricingDisplayTaxTotal convert(
			ResultTicketingDetailSolutionPricingDisplayTaxTotalData source) {
		ResultTicketingDetailSolutionPricingDisplayTaxTotal destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingDisplayTaxTotal();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
