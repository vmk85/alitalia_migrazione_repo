package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.Duration;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.enumerations.FlightTypeEnum;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ArrayOfDictionaryItem;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.Brand;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ConnectingFlight;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.DirectFlight;

@Component(immediate=true, metatype=false)
@Service(value=ConnectingFlightToConnectingFlightData.class)
public class ConnectingFlightToConnectingFlightData implements Converter<ConnectingFlight, ConnectingFlightData> {

	@Reference
	private BrandToBrandData brandConverter;

	@Reference
	private DirectFlightToDirectFlightData directFlightConverter;

	@Reference
	private ArrayOfDictionaryItemToPropertiesData arrayOfDictionaryItem; 

	@Override
	public ConnectingFlightData convert(ConnectingFlight source) {
		ConnectingFlightData destination = new ConnectingFlightData();

		Duration duration = source.getDuration();
		if (duration != null) {
			destination.setDurationHour(duration.getDays() * 24 + duration.getHours());
			destination.setDurationMinutes(duration.getMinutes());
		}
		destination.setFlightType(FlightTypeEnum.fromValue(source.getType().value()));

		ArrayList<FlightData> flights = new ArrayList<FlightData>();
		List<Object> sourceFlights = source.getFlights().getValue().getAnyType();
		for (Object flight : sourceFlights) {
			if (flight instanceof DirectFlight)
			{
				DirectFlight castedDirectFlight = (DirectFlight) flight;
				DirectFlightData directFlightData = directFlightConverter.convert(castedDirectFlight);
				flights.add(directFlightData);
			}
			else 
			{
				ConnectingFlight castedConnectingFlight = (ConnectingFlight) flight;
				ConnectingFlightData connectingFlightData = this.convert(castedConnectingFlight);
				flights.add(connectingFlightData);
			}
		}
		destination.setFlights(flights);

		ArrayList<BrandData> brands = new ArrayList<BrandData>();
		List<Object> brandsSource = source.getBrands().getValue().getAnyType();
		for (Object brand : brandsSource) {
			Brand castedBrand = (Brand) brand;
			BrandData brandData = brandConverter.convert(castedBrand);
			brands.add(brandData);
		}
		destination.setBrands(brands);

		JAXBElement<ArrayOfDictionaryItem> sourceProperties = source.getProperties();
		if (sourceProperties != null && sourceProperties.getValue() != null)
			destination.setProperties(arrayOfDictionaryItem.convert(sourceProperties.getValue()));
		else
			destination.setProperties(null);

		return destination;
	}

}
