package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.messages.home.InfoPassengerSubmitPassengerRequest;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd1.PassengerSubmitRequest;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd5.ArrayOfanyType;

@Component(immediate=true, metatype=false)
@Service(value=InfoPassengerSubmitPassengerRequestToPassengerSubmitRequest.class)
public class InfoPassengerSubmitPassengerRequestToPassengerSubmitRequest 
		implements Converter<InfoPassengerSubmitPassengerRequest, PassengerSubmitRequest> {
	
	@Reference
	private PassengerBaseDataToAPassengerBase passengerBaseDataToAPassengerBaseConverter;
	
	@Override
	public PassengerSubmitRequest convert(InfoPassengerSubmitPassengerRequest source) {
		
		PassengerSubmitRequest destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
	
			destination = objectFactory.createPassengerSubmitRequest();
			com.alitalia.aem.ws.booking.infopassengerservice.xsd5.ObjectFactory xsd5ObjectFactory = 
					new com.alitalia.aem.ws.booking.infopassengerservice.xsd5.ObjectFactory();
			
			ArrayOfanyType passengers = xsd5ObjectFactory.createArrayOfanyType();
			if (source.getPassengers() != null) {
				for (PassengerBaseData passenger : source.getPassengers()) {
					passengers.getAnyType().add(
							passengerBaseDataToAPassengerBaseConverter.convert(
									passenger));
				}
			}
			destination.setCUG(objectFactory.createPassengerSubmitRequestCUG(source.getCug()));
			destination.setPassengers(objectFactory.createPassengerSubmitRequestPassengers(passengers));
			destination.setCookie(objectFactory.createPassengerSubmitRequestCookie(source.getCookie()));
			destination.setExecution(objectFactory.createPassengerSubmitRequestExecution(source.getExecution()));
			destination.setSabreGateWayAuthToken(objectFactory.createPassengerSubmitRequestSabreGateWayAuthToken(source.getSabreGateWayAuthToken()));
			
		}
		
		return destination;
	}

}
