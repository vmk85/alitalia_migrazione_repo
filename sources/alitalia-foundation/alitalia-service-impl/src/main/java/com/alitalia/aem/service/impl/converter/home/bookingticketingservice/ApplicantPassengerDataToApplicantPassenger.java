package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ApplicantPassengerData;
import com.alitalia.aem.common.data.home.ContactData;
import com.alitalia.aem.common.data.home.TicketInfoData;
import com.alitalia.aem.ws.booking.ticketservice.xsd2.ArrayOfanyType;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ARTaxInfoTypes;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ApplicantPassenger;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ObjectFactory;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.PassengerType;

@Component(immediate = true, metatype = false)
@Service(value = ApplicantPassengerDataToApplicantPassenger.class)
public class ApplicantPassengerDataToApplicantPassenger implements
		Converter<ApplicantPassengerData, ApplicantPassenger> {

	@Reference
	private PassengerInfoBaseDataToInfoBase infoBaseDataToInfoBaseConverter;

	@Reference
	private PreferencesDataToPreferences preferencesDataToPreferencesConverter;

	@Reference
	private FrequentFlyerTypeDataToFrequentFlyerType frequentFlyerTypeDataToFrequentFlyerTypeConverter;

	@Reference
	private TicketInfoDataToTicketInfo ticketInfoDataToTicketInfoConverter;

	@Reference
	private ContactDataToContact contactDataToContactConverter;

	@Override
	public ApplicantPassenger convert(ApplicantPassengerData source) {
		ApplicantPassenger destination = null;

		if (source != null) {
			ObjectFactory factory5 = new ObjectFactory();
			com.alitalia.aem.ws.booking.ticketservice.xsd2.ObjectFactory factory2 = new com.alitalia.aem.ws.booking.ticketservice.xsd2.ObjectFactory();

			destination = factory5.createApplicantPassenger();

			// ApplicantPassenger data

			if (source.getArTaxInfoType() != null) {
				destination.setARTaxInfoType(ARTaxInfoTypes.fromValue(source.getArTaxInfoType().value()));
			}

			destination.setBlueBizCode(factory5.createApplicantPassengerBlueBizCode(source.getBlueBizCode()));

			List<ContactData> sourceContacts = source.getContact();
			ArrayOfanyType contacts = factory2.createArrayOfanyType();
			if (sourceContacts != null) {
				for (ContactData contact : sourceContacts) {
					contacts.getAnyType().add(contactDataToContactConverter.convert(contact));
				}
			}
			
			destination.setContact(factory5.createApplicantPassengerContact(contacts));
			destination.setCountry(factory5.createApplicantPassengerCountry(source.getCountry()));
			destination.setCouponPrice(source.getCouponPrice());
			destination.setCUIT(factory5.createApplicantPassengerCUIT(source.getCuit()));
			destination.setEmail(factory5.createApplicantPassengerEmail(source.getEmail()));
			destination.setExtraCharge(source.getExtraCharge());
			destination.setFee(source.getFee());

			// AdultPassenger data
			destination.setFrequentFlyerCode(factory5.createAdultPassengerFrequentFlyerCode(source
					.getFrequentFlyerCode()));
			destination.setFrequentFlyerTier(factory5.createAdultPassengerFrequentFlyerTier(source
					.getFrequentFlyerTier()));
			destination.setFrequentFlyerType(factory5
					.createAdultPassengerFrequentFlyerType(frequentFlyerTypeDataToFrequentFlyerTypeConverter
							.convert(source.getFrequentFlyerType())));

			// ARegularPassenger data
			destination.setPreferences(factory5
					.createARegularPassengerPreferences(preferencesDataToPreferencesConverter.convert(source
							.getPreferences())));

			// APassengerBase data
			destination.setLastName(factory5.createAPassengerBaseLastName(source.getLastName()));
			destination.setName(factory5.createAPassengerBaseName(source.getName()));
			destination.setCouponPrice(source.getCouponPrice());
			destination.setExtraCharge(source.getExtraCharge());
			destination.setFee(source.getFee());
			destination.setGrossFare(source.getGrossFare());
			destination.setNetFare(source.getNetFare());
			destination.setInfo(factory5.createAPassengerBaseInfo(infoBaseDataToInfoBaseConverter.convert(source
					.getInfo())));

			List<TicketInfoData> sourceTickets = source.getTickets();
			if (sourceTickets != null) {
				ArrayOfanyType tickets = factory2.createArrayOfanyType();
				for (TicketInfoData ticketInfoData : sourceTickets) {
					tickets.getAnyType().add(ticketInfoDataToTicketInfoConverter.convert(ticketInfoData));
				}
				destination.setTickets(factory5.createAPassengerBaseTickets(tickets));
			} else
				destination.setTickets(factory5.createAPassengerBaseTickets(null));

			destination.setType(PassengerType.fromValue(source.getType().value()));
			destination.setSkyBonusCode(factory5.createApplicantPassengerSkyBonusCode(source.getSkyBonusCode()));
			destination.setSubscribeToNewsletter(factory5.createApplicantPassengerSubscribeToNewsletter(source
					.isSubscribeToNewsletter()));
		}

		return destination;
	}

}
