package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentFlightData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionSliceSegmentFlight;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentFlightToRTDSSliceSegmentFlightData.class)
public class RTDSSliceSegmentFlightToRTDSSliceSegmentFlightData implements
		Converter<ResultTicketingDetailSolutionSliceSegmentFlight, ResultTicketingDetailSolutionSliceSegmentFlightData> {

	@Override
	public ResultTicketingDetailSolutionSliceSegmentFlightData convert(ResultTicketingDetailSolutionSliceSegmentFlight source) {
		ResultTicketingDetailSolutionSliceSegmentFlightData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionSliceSegmentFlightData();

			destination.setCarrierField(source.getCarrierField());
			destination.setNumberField(source.getNumberField());
			destination.setNumberFieldSpecified(source.isNumberFieldSpecified());
		}

		return destination;
	}

}
