package com.alitalia.aem.service.impl.converter.home.checkininsuranceservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinPaymentTransactionInfoData;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd2.PaymentTransactionInfo;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd2.PaymentTransactionInfoResponseType;



@Component(immediate=true, metatype=false)
@Service(value=CheckinPaymentTransactionInfoDataToPaymentTransactionInfo.class)
public class CheckinPaymentTransactionInfoDataToPaymentTransactionInfo implements Converter<CheckinPaymentTransactionInfoData, PaymentTransactionInfo> {
	

	@Override
	public PaymentTransactionInfo convert(CheckinPaymentTransactionInfoData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		PaymentTransactionInfo destination = null;

		if (source != null) {
			destination = objectFactory.createPaymentTransactionInfo();
			
			destination.setX003CIdX003EKBackingField(source.getId());
			
			destination.setX003CAcceptHeaderX003EKBackingField(source.getAcceptHeader());
			destination.setX003CAuthorizationIDX003EKBackingField(source.getAuthorizationID());
			if(source.getErrorCode() != null){
				destination.setX003CErrorCodeX003EKBackingField(source.getErrorCode());
			}
			destination.setX003CErrorDescriptionX003EKBackingField(source.getErrorDescription());
			destination.setX003CMarketCodeX003EKBackingField(source.getMarketCode());
			destination.setX003COpaqueParametersX003EKBackingField(source.getOpaqueParameters());
			destination.setX003COrderCodeX003EKBackingField(source.getOrderCode());
			destination.setX003COrderDescriptionX003EKBackingField(source.getOrderDescription());
			destination.setX003CRequestDataX003EKBackingField(source.getRequestData());
			destination.setX003CSessionIdX003EKBackingField(source.getSessionId());
			destination.setX003CSiteCodeX003EKBackingField(source.getSiteCode());
			destination.setX003CThreeDSecureIssueURLX003EKBackingField(source.getThreeDSecureIssueURL());
			if(source.getType() != null){
				destination.setX003CTypeX003EKBackingField(PaymentTransactionInfoResponseType.fromValue(source.getType().value()));
			}
			destination.setX003CUserAgentHeaderX003EKBackingField(source.getUserAgentHeader());
			destination.setX003CUserHostAddressX003EKBackingField(source.getUserHostAddress());
			destination.setX003CVbVPaRequestX003EKBackingField(source.getVbvPaRequest());
			destination.setX003CVbVPaResponseX003EKBackingField(source.getVbvPaResponse());
			
		}
		return destination;
	}

}
