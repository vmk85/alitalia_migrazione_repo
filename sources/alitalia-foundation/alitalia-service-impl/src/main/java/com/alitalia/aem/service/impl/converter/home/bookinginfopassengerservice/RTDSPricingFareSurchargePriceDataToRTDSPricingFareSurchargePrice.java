package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareSurchargePriceData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionPricingFareSurchargePrice;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareSurchargePriceDataToRTDSPricingFareSurchargePrice.class)
public class RTDSPricingFareSurchargePriceDataToRTDSPricingFareSurchargePrice implements
		Converter<ResultTicketingDetailSolutionPricingFareSurchargePriceData, 
					ResultTicketingDetailSolutionPricingFareSurchargePrice> {

	@Override
	public ResultTicketingDetailSolutionPricingFareSurchargePrice convert(
			ResultTicketingDetailSolutionPricingFareSurchargePriceData source) {
		ResultTicketingDetailSolutionPricingFareSurchargePrice destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingFareSurchargePrice();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
