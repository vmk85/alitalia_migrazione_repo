package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentFlightData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionSliceSegmentFlight;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentFlightDataToRTDSSliceSegmentFlight.class)
public class RTDSSliceSegmentFlightDataToRTDSSliceSegmentFlight implements
		Converter<ResultTicketingDetailSolutionSliceSegmentFlightData, 
					ResultTicketingDetailSolutionSliceSegmentFlight> {

	@Override
	public ResultTicketingDetailSolutionSliceSegmentFlight convert(
			ResultTicketingDetailSolutionSliceSegmentFlightData source) {
		ResultTicketingDetailSolutionSliceSegmentFlight destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionSliceSegmentFlight();

			destination.setCarrierField(source.getCarrierField());
			destination.setNumberField(source.getNumberField());
			destination.setNumberFieldSpecified(source.isNumberFieldSpecified());
		}

		return destination;
	}

}
