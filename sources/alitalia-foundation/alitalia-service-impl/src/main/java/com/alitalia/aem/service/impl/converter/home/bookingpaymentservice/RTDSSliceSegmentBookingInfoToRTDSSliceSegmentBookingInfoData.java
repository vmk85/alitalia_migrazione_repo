package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentBookingInfoData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionSliceSegmentBookingInfo;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentBookingInfoToRTDSSliceSegmentBookingInfoData.class)
public class RTDSSliceSegmentBookingInfoToRTDSSliceSegmentBookingInfoData implements
		Converter<ResultTicketingDetailSolutionSliceSegmentBookingInfo, ResultTicketingDetailSolutionSliceSegmentBookingInfoData> {

	@Override
	public ResultTicketingDetailSolutionSliceSegmentBookingInfoData convert(ResultTicketingDetailSolutionSliceSegmentBookingInfo source) {
		ResultTicketingDetailSolutionSliceSegmentBookingInfoData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionSliceSegmentBookingInfoData();

			destination.setBookingCodeCountField(source.getBookingCodeCountField());
			destination.setBookingCodeField(source.getBookingCodeField());
			destination.setCabinField(source.getCabinField());
			destination.setMarriedSegmentIndexField(source.getMarriedSegmentIndexField());
			destination.setMarriedSegmentIndexFieldSpecified(source.isMarriedSegmentIndexFieldSpecified());
		}

		return destination;
	}

}
