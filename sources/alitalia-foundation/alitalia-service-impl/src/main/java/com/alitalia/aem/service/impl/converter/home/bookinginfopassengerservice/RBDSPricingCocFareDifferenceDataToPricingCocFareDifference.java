package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingCocFareDifferenceData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ResultBookingDetailsSolutionPricingCocFareDifference;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingCocFareDifferenceDataToPricingCocFareDifference.class)
public class RBDSPricingCocFareDifferenceDataToPricingCocFareDifference
		implements Converter<ResultBookingDetailsSolutionPricingCocFareDifferenceData, ResultBookingDetailsSolutionPricingCocFareDifference> {

	@Override
	public ResultBookingDetailsSolutionPricingCocFareDifference convert(
			ResultBookingDetailsSolutionPricingCocFareDifferenceData source) {
		ResultBookingDetailsSolutionPricingCocFareDifference destination = null;
		
		if(source != null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionPricingCocFareDifference();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		
		return destination;
	}

}
