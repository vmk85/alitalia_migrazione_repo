package com.alitalia.aem.service.impl.converter.home.carnetpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.CarnetSendEmailResponse;
import com.alitalia.aem.ws.carnet.paymentservice.xsd1.SendEmailResponse;

@Component(immediate=true, metatype=false)
@Service(value=SendEmailResponseToCarnetSendEmailResponse.class)
public class SendEmailResponseToCarnetSendEmailResponse implements Converter<SendEmailResponse, CarnetSendEmailResponse> {

	@Override
	public CarnetSendEmailResponse convert(SendEmailResponse source) {
		CarnetSendEmailResponse destination = null;

		if (source != null) {
			destination = new CarnetSendEmailResponse();
			destination.setHaveSendEmail(source.isHaveSendEmail());
		}

		return destination;
	}

}
