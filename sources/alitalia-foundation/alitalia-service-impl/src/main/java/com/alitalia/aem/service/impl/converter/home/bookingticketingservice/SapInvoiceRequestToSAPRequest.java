package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.TicketData;
import com.alitalia.aem.common.messages.home.SapInvoiceRequest;
import com.alitalia.aem.ws.booking.ticketservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.booking.ticketservice.xsd1.SAPRequest;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ArrayOfticket;

@Component(immediate=true, metatype= false)
@Service(value=SapInvoiceRequestToSAPRequest.class)
public class SapInvoiceRequestToSAPRequest implements Converter<SapInvoiceRequest, SAPRequest> {

	@Reference
	private TicketDataToTicket ticketDataConverter;

	@Override
	public SAPRequest convert(SapInvoiceRequest source) {
		ObjectFactory objectFactory1 = new ObjectFactory();
		com.alitalia.aem.ws.booking.ticketservice.xsd5.ObjectFactory objectFactory5 =
				new com.alitalia.aem.ws.booking.ticketservice.xsd5.ObjectFactory();

		SAPRequest destination = objectFactory1.createSAPRequest();

		destination.setCap(objectFactory1.createSAPRequestCap(source.getCap()));
		destination.setCfInt(objectFactory1.createSAPRequestCfInt(source.getPartitaIva()));
		destination.setCfPax(objectFactory1.createSAPRequestCfPax(source.getCodiceFiscale()));
		destination.setEmailInt(objectFactory1.createSAPRequestEmailInt(source.getEmailIntestatario()));

		String enteEmittente = source.getEnteEmittente();
		if (enteEmittente != null)
			destination.setEnteEmit(objectFactory1.createSAPRequestEnteEmit(enteEmittente));
		else
			destination.setEnteEmit(objectFactory1.createSAPRequestEnteEmit(""));

		String enteRichiedente = source.getEnteRichiedente();
		if (enteRichiedente != null)
			destination.setEnteRich(objectFactory1.createSAPRequestEnteRich(enteRichiedente));
		else
			destination.setEnteRich(objectFactory1.createSAPRequestEnteRich(""));

		destination.setIndSped(objectFactory1.createSAPRequestIndSped(source.getIndirizzoSpedizione()));
		destination.setIntFattura(objectFactory1.createSAPRequestIntFattura(source.getIntestatarioFattura()));
		destination.setLocSped(objectFactory1.createSAPRequestLocSped(source.getLocalitaSpedizione()));
		destination.setNome(objectFactory1.createSAPRequestNome(source.getNome()));
		destination.setPaese(objectFactory1.createSAPRequestPaese(source.getPaese()));
		
		/* 
		 * Questo campo va settato a null perchè la Partita IVA è memorizzata in cfInt 
		 * */
		destination.setPivaInt(objectFactory1.createSAPRequestPivaInt(null));

		destination.setProv(objectFactory1.createSAPRequestProv(source.getProvincia()));

		String societa = source.getSocieta();
		if (societa != null)
			destination.setSocieta(objectFactory1.createSAPRequestSocieta(societa));
		else
			destination.setSocieta(objectFactory1.createSAPRequestSocieta(""));

		ArrayOfticket tickets = null;
		if (source.getBiglietti() != null && !source.getBiglietti().isEmpty()) {
			tickets = objectFactory5.createArrayOfticket();
			for (TicketData sourceTicketData : source.getBiglietti()) {
				tickets.getTicket().add(ticketDataConverter.convert(sourceTicketData));
			}
		}
		destination.setTickets(objectFactory1.createSAPRequestTickets(tickets));

		String tipoRichiesta = source.getTipoRichiesta();
		if (tipoRichiesta != null)
			destination.setTipoRichiesta(objectFactory1.createSAPRequestTipoRichiesta(tipoRichiesta));
		else
			destination.setTipoRichiesta(objectFactory1.createSAPRequestTipoRichiesta(""));

		return destination;
	}

}
