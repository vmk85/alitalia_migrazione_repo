package com.alitalia.aem.service.impl.converter.home.businessloginservice;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AlitaliaTradeAgencyData;
import com.alitalia.aem.common.messages.home.AgencyRetrievePasswordResponse;
import com.alitalia.aem.ws.b2bv2.xsd.RetrievePasswordByPartitaIvaResponse.RetrievePasswordByPartitaIvaResult;
import com.alitalia.aem.ws.b2bv2.xsd.RetrievePasswordResponse.RetrievePasswordResult;
import com.alitalia.aem.ws.b2bv2.xsd1.AlitaliaTrade;

@Component
@Service(value=RetrievePasswordResultToAgencyRetrievePasswordResponse.class)
public class RetrievePasswordResultToAgencyRetrievePasswordResponse implements Converter<RetrievePasswordResult, AgencyRetrievePasswordResponse> {

	private static final Logger logger = LoggerFactory.getLogger(RetrievePasswordResultToAgencyRetrievePasswordResponse.class);

	@Reference
	private AlitaliaTradeToAlitaliaTradeAgencyData alitaliaTradeConverter;
	
	@Reference
	private BusinessLoginServiceJAXBContextFactory jaxbContextFactory;

	@Override
	public AgencyRetrievePasswordResponse convert(RetrievePasswordResult source) {
		AlitaliaTradeAgencyData agencyData = null;
		Boolean retrieveSuccessful = false;
		Boolean passwordExpired = null;
		Boolean accountLocked = null;

		AlitaliaTrade unmarshalledObj = null;
		if (source.getAny() instanceof AlitaliaTrade) {
			unmarshalledObj = (AlitaliaTrade) source.getAny();
		}
		else {
			try {
				Unmarshaller loginUnmashaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
				unmarshalledObj = null;
				Node responseElement = (Node) source.getAny();
				unmarshalledObj = (AlitaliaTrade) loginUnmashaller.unmarshal(responseElement);
			} catch (JAXBException e) {
				logger.error("Error instantiating JAXBContext/Unmarshaller: {}", e);
			}
		}

		if (unmarshalledObj != null) {
			agencyData = alitaliaTradeConverter.convert(unmarshalledObj);
			retrieveSuccessful = true;
			passwordExpired = agencyData.isPasswordExpired();
			accountLocked = agencyData.isAccountLocked();
		}

		AgencyRetrievePasswordResponse destination = new AgencyRetrievePasswordResponse(agencyData, retrieveSuccessful, passwordExpired, accountLocked);

		return destination;
	}

	public AgencyRetrievePasswordResponse convert(RetrievePasswordByPartitaIvaResult source) {
		AlitaliaTradeAgencyData agencyData = null;
		Boolean retrieveSuccessful = false;
		Boolean passwordExpired = null;
		Boolean accountLocked = null;
		
		AlitaliaTrade unmarshalledObj = null;
		if (source.getAny() instanceof AlitaliaTrade) {
			unmarshalledObj = (AlitaliaTrade) source.getAny();
		}
		else {
			try {
				Unmarshaller loginUnmashaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
				unmarshalledObj = null;
				Node responseElement = (Node) source.getAny();
				unmarshalledObj = (AlitaliaTrade) loginUnmashaller.unmarshal(responseElement);
			} catch (JAXBException e) {
				logger.error("Error instantiating JAXBContext/Unmarshaller: {}", e);
			}
		}

		if (unmarshalledObj != null) {
			agencyData = alitaliaTradeConverter.convert(unmarshalledObj);
			retrieveSuccessful = true;
			passwordExpired = agencyData.isPasswordExpired();
			accountLocked = agencyData.isAccountLocked();
		}

		AgencyRetrievePasswordResponse destination = new AgencyRetrievePasswordResponse(agencyData, retrieveSuccessful, passwordExpired, accountLocked);

		return destination;
	}

}
