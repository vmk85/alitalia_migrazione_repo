package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.TicketInfoData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.ObjectFactory;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.TicketInfo;

@Component(immediate=true, metatype=false)
@Service(value=TicketInfoDataToTicketInfo.class)
public class TicketInfoDataToTicketInfo 
		implements Converter<TicketInfoData, TicketInfo> {
	
	@Override
	public TicketInfo convert(TicketInfoData source) {
		
		TicketInfo destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			destination = objectFactory.createTicketInfo();
			
			destination.setSegmentId(
					objectFactory.createTicketInfoSegmentId(
							source.getSegmentId()));
			
			destination.setTicketNumber(
					objectFactory.createTicketInfoTicketNumber(
							source.getTicketNumber()));
			
		}
		
		return destination;
	}

}
