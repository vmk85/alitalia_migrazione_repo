package com.alitalia.aem.service.impl.converter.home.checkinalertservice;

import javax.xml.ws.Holder;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.WebCheckinAlertRequest;
import com.alitalia.aem.ws.checkin.alertservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.checkin.alertservice.xsd2.User;

@Component(immediate=true, metatype=false)
@Service(value=WebCheckinAlertRequestToAlertRequest.class)
public class WebCheckinAlertRequestToAlertRequest implements
		Converter<WebCheckinAlertRequest, Holder<User>> {

	@Override
	public Holder<User> convert(WebCheckinAlertRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		User user = null;
		
		if (source != null) {
			user = objectFactory.createUser();
			user.setEMAIL(source.getUser().getEmail());
			user.setENABLED(source.getUser().isEnabled());
			user.setMMCODE(source.getUser().getMmcode());
			user.setNAME(source.getUser().getName());
			user.setNUMBER(source.getUser().getNumber());
			user.setRESULT(source.getUser().getResult());
			user.setSURNAME(source.getUser().getSurname());	
		}
		
		return new Holder<User>(user);
	}
}