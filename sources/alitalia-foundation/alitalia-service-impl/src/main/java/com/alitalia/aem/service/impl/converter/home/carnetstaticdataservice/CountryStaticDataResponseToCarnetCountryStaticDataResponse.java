package com.alitalia.aem.service.impl.converter.home.carnetstaticdataservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.messages.home.CarnetCountryStaticDataResponse;
import com.alitalia.aem.ws.carnet.staticdataservice.xsd1.CountryStaticDataResponse;
import com.alitalia.aem.ws.carnet.staticdataservice.xsd2.Country;

@Component(immediate=true, metatype=false)
@Service(value=CountryStaticDataResponseToCarnetCountryStaticDataResponse.class)
public class CountryStaticDataResponseToCarnetCountryStaticDataResponse implements Converter<CountryStaticDataResponse, CarnetCountryStaticDataResponse> {

	@Reference
	private CountryToCountryData countryDataConverter;
	
	@Override
	public CarnetCountryStaticDataResponse convert(CountryStaticDataResponse source) {
		CarnetCountryStaticDataResponse destination = null;

		if (source != null) {
			destination = new CarnetCountryStaticDataResponse();
			List<CountryData> countryData = null;
			if (source.getEntities() != null && source.getEntities().getValue() != null
				&& source.getEntities().getValue().getCountry()!=null && source.getEntities().getValue().getCountry().size() > 0) {
				countryData = new ArrayList<CountryData>();
				for (Country country : source.getEntities().getValue().getCountry()){
					countryData.add(countryDataConverter.convert(country));
				}
				destination.setCountries(countryData);
			}
		}
		return destination;
	}

}
