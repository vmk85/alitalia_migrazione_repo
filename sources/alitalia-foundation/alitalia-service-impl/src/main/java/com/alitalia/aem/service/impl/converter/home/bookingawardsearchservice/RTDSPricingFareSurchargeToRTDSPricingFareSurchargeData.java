package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareSurchargeData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionPricingFareSurcharge;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareSurchargeToRTDSPricingFareSurchargeData.class)
public class RTDSPricingFareSurchargeToRTDSPricingFareSurchargeData implements
		Converter<ResultTicketingDetailSolutionPricingFareSurcharge, 
					ResultTicketingDetailSolutionPricingFareSurchargeData> {

	@Reference
	private RTDSPricingFareSurchargePriceToRTDSPricingFareSurchargePriceData priceFieldConverter;

	@Override
	public ResultTicketingDetailSolutionPricingFareSurchargeData convert(
			ResultTicketingDetailSolutionPricingFareSurcharge source) {
		ResultTicketingDetailSolutionPricingFareSurchargeData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingFareSurchargeData();

			destination.setCodeField(source.getCodeField());

			destination.setPriceField(
					priceFieldConverter.convert(source.getPriceField()));
		}

		return destination;
	}

}
