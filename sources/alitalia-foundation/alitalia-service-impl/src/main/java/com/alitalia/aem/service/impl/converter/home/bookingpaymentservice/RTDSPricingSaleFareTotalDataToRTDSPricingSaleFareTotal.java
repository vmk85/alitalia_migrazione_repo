package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingSaleFareTotalData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingSaleFareTotal;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingSaleFareTotalDataToRTDSPricingSaleFareTotal.class)
public class RTDSPricingSaleFareTotalDataToRTDSPricingSaleFareTotal implements
		Converter<ResultTicketingDetailSolutionPricingSaleFareTotalData, 
					ResultTicketingDetailSolutionPricingSaleFareTotal> {

	@Override
	public ResultTicketingDetailSolutionPricingSaleFareTotal convert(
			ResultTicketingDetailSolutionPricingSaleFareTotalData source) {
		ResultTicketingDetailSolutionPricingSaleFareTotal destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingSaleFareTotal();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
