package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.FlightTabData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.searchservice.xsd3.FlightTab;

@Component(immediate=true, metatype=false)
@Service(value=FlightTabToFlightTabData.class)
public class FlightTabToFlightTabData implements Converter<FlightTab, FlightTabData> {

	@Override
	public FlightTabData convert(FlightTab source) {
		FlightTabData destination = new FlightTabData();

		JAXBElement<String> currency = source.getCurrency();
		if (currency != null)
			destination.setCurrency(currency.getValue());
		destination.setPrice(source.getPrice());

		XMLGregorianCalendar sourceDate = source.getDate();
		if (sourceDate != null)
			destination.setDate(XsdConvertUtils.parseCalendar(sourceDate));

		return destination;
	}

}
