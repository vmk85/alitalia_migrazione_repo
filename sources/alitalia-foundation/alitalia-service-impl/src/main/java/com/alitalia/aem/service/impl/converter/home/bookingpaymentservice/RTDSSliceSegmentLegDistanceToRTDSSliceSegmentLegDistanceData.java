package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentLegDistanceData;
import com.alitalia.aem.common.data.home.enumerations.DistanceUnitsEnum;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.DistanceUnits;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionSliceSegmentLegDistance;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentLegDistanceToRTDSSliceSegmentLegDistanceData.class)
public class RTDSSliceSegmentLegDistanceToRTDSSliceSegmentLegDistanceData implements
		Converter<ResultTicketingDetailSolutionSliceSegmentLegDistance, ResultTicketingDetailSolutionSliceSegmentLegDistanceData> {

	@Override
	public ResultTicketingDetailSolutionSliceSegmentLegDistanceData convert(ResultTicketingDetailSolutionSliceSegmentLegDistance source) {
		ResultTicketingDetailSolutionSliceSegmentLegDistanceData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionSliceSegmentLegDistanceData();

			DistanceUnits sourceUnitsField = source.getUnitsField();
			if (sourceUnitsField != null)
				destination.setUnitsField(DistanceUnitsEnum.fromValue(sourceUnitsField.value()));

			destination.setValueField(source.getValueField());
		}

		return destination;
	}

}
