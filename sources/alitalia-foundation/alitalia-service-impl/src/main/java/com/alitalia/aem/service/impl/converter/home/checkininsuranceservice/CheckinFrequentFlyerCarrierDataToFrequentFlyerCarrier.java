package com.alitalia.aem.service.impl.converter.home.checkininsuranceservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.MmbFrequentFlyerCarrierData;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd2.FrequentFlyerCarrier;



@Component(immediate=true, metatype=false)
@Service(value=CheckinFrequentFlyerCarrierDataToFrequentFlyerCarrier.class)
public class CheckinFrequentFlyerCarrierDataToFrequentFlyerCarrier implements Converter<MmbFrequentFlyerCarrierData, FrequentFlyerCarrier> {

	@Override
	public FrequentFlyerCarrier convert(MmbFrequentFlyerCarrierData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		FrequentFlyerCarrier destination = null;

		if (source != null) {
			destination = objectFactory.createFrequentFlyerCarrier();

			destination.setX003CCodeX003EKBackingField(source.getCode());
			destination.setX003CDescriptionX003EKBackingField(source.getDescription());
			destination.setX003CIdX003EKBackingField(source.getId());
			if(source.getLenght() != null){
				destination.setX003CLenghtX003EKBackingField(source.getLenght());
			}
			destination.setX003CPreFillCharX003EKBackingField(source.getPreFillChar());
			destination.setX003CRegularExpressionValidationX003EKBackingField(source.getRegularExpressionValidation());
		}

		return destination;
	}

}
