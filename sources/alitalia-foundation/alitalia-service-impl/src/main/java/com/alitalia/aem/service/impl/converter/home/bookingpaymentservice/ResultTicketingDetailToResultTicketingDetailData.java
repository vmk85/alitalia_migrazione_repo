package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetail;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolution;

@Component(immediate=true, metatype=false)
@Service(value=ResultTicketingDetailToResultTicketingDetailData.class)
public class ResultTicketingDetailToResultTicketingDetailData implements
		Converter<ResultTicketingDetail, ResultTicketingDetailData> {

	@Reference
	private ResultTicketingDetailPagesToResultTicketingDetailPagesData resultTicketingDetailPagesConverter;

	@Reference
	private ResultTicketingDetailSolutionToResultTicketingDetailSolutionData resultTicketingDetailSolutionConverter;

	@Override
	public ResultTicketingDetailData convert(ResultTicketingDetail source) {
		ResultTicketingDetailData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailData();

			destination.setPagesField(resultTicketingDetailPagesConverter.convert(source.getPagesField()));

			List<ResultTicketingDetailSolutionData> solutionField = null;
			if (source.getSolutionField() != null &&
					source.getSolutionField().getResultTicketingDetailSolution() != null &&
					!source.getSolutionField().getResultTicketingDetailSolution().isEmpty()) {
				solutionField = new ArrayList<ResultTicketingDetailSolutionData>();
				for(ResultTicketingDetailSolution sourceResultTicketingDetailSolutionElement : source.getSolutionField().getResultTicketingDetailSolution()) {
					solutionField.add(resultTicketingDetailSolutionConverter.convert(sourceResultTicketingDetailSolutionElement));
				}
			}
			destination.setSolutionField(solutionField);
		}

		return destination;
	}

}
