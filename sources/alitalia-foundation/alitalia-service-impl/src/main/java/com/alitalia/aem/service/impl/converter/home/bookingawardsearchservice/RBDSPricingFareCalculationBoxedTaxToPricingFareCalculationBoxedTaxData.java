//2
package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetailsSolutionPricingFareCalculationBoxedTax;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingFareCalculationBoxedTaxToPricingFareCalculationBoxedTaxData.class)
public class RBDSPricingFareCalculationBoxedTaxToPricingFareCalculationBoxedTaxData
		implements Converter<ResultBookingDetailsSolutionPricingFareCalculationBoxedTax, ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData> {

	@Override
	public ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData convert(
			ResultBookingDetailsSolutionPricingFareCalculationBoxedTax source) {
		ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData destination = null;
		
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData();
			destination.setPriceField(source.getPriceField());
			destination.setTaxCodeField(source.getTaxCodeField());
		}
		return destination;
	}
	
	public ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData convert(
			com.alitalia.aem.ws.bookingaward.searchservice.xsd13.ResultBookingDetailsSolutionPricingFareCalculationBoxedTax source) {
		ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData destination = null;
		
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData();
			destination.setPriceField(source.getPriceField());
			destination.setTaxCodeField(source.getTaxCodeField());
		}
		return destination;
	}

}
