package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingSaleTaxTotalData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingSaleTaxTotal;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingSaleTaxTotalDataToRTDSPricingSaleTaxTotal.class)
public class RTDSPricingSaleTaxTotalDataToRTDSPricingSaleTaxTotal implements
		Converter<ResultTicketingDetailSolutionPricingSaleTaxTotalData, 
					ResultTicketingDetailSolutionPricingSaleTaxTotal> {

	@Override
	public ResultTicketingDetailSolutionPricingSaleTaxTotal convert(
			ResultTicketingDetailSolutionPricingSaleTaxTotalData source) {
		ResultTicketingDetailSolutionPricingSaleTaxTotal destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingSaleTaxTotal();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
