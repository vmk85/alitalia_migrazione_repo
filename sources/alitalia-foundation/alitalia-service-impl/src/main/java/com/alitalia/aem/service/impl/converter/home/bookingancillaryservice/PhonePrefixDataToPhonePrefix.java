package com.alitalia.aem.service.impl.converter.home.bookingancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PhonePrefixData;
import com.alitalia.aem.ws.booking.ancillaryservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.booking.ancillaryservice.xsd2.PhonePrefix;

@Component(immediate=true, metatype=false)
@Service(value=PhonePrefixDataToPhonePrefix.class)
public class PhonePrefixDataToPhonePrefix 
		implements Converter<PhonePrefixData, PhonePrefix> {
	
	@Override
	public PhonePrefix convert(PhonePrefixData source) {
		
		PhonePrefix destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			destination = objectFactory.createPhonePrefix();
			
			destination.setCode(
					objectFactory.createPhonePrefixCode(
							source.getCode()));
			
			destination.setDescription(
					objectFactory.createPhonePrefixDescription(
							source.getDescription()));
			
			destination.setPrefix(
					objectFactory.createPhonePrefixPrefix(
							source.getPrefix()));
			
		}
		
		return destination;
	}

}
