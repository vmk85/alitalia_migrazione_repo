package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.ws.booking.ticketservice.xsd2.ArrayOfanyType;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ConnectingFlight;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.DirectFlight;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ObjectFactory;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.Route;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.RouteType;

@Component(immediate = true, metatype = false)
@Service(value = RouteDataToRoute.class)
public class RouteDataToRoute implements Converter<RouteData, Route> {

	@Reference
	private DirectFlightDataToDirectFlight directFlightDataConverter;

	@Reference
	private ConnectingFlightDataToConnectingFlight connectingFlightDataConverter;

	@Override
	public Route convert(RouteData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		com.alitalia.aem.ws.booking.ticketservice.xsd2.ObjectFactory objectFactory2 = new com.alitalia.aem.ws.booking.ticketservice.xsd2.ObjectFactory();
		Route destination = objectFactory.createRoute();

		destination.setIndex(source.getIndex());
		destination.setType(RouteType.fromValue(source.getType().value()));

		ArrayOfanyType flights = objectFactory2.createArrayOfanyType();
		List<FlightData> sourceFlights = source.getFlights();
		for (Object flight : sourceFlights) {
			if (flight instanceof DirectFlightData) {
				DirectFlightData castedDirectFlightData = (DirectFlightData) flight;
				DirectFlight directFlight = directFlightDataConverter.convert(castedDirectFlightData);
				flights.getAnyType().add(directFlight);
			} else {
				ConnectingFlightData castedConnectingFlightData = (ConnectingFlightData) flight;
				ConnectingFlight connectingFlight = connectingFlightDataConverter.convert(castedConnectingFlightData);
				flights.getAnyType().add(connectingFlight);
			}
		}
		destination.setFlights(objectFactory.createRouteFlights(flights));

		return destination;
	}

}
