package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingDisplayPriceData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingDisplayPrice;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingDisplayPriceDataToRTDSPricingDisplayPrice.class)
public class RTDSPricingDisplayPriceDataToRTDSPricingDisplayPrice implements
		Converter<ResultTicketingDetailSolutionPricingDisplayPriceData,
					ResultTicketingDetailSolutionPricingDisplayPrice> {

	@Override
	public ResultTicketingDetailSolutionPricingDisplayPrice convert(
			ResultTicketingDetailSolutionPricingDisplayPriceData source) {
		ResultTicketingDetailSolutionPricingDisplayPrice destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingDisplayPrice();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
