package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.ws.booking.paymentservice.xsd1.CheckPaymentRequest;
import com.alitalia.aem.ws.booking.paymentservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.PaymentType;



@Service(value=CheckPaymentRequestConverter.class)
@Component(immediate=true, metatype=false)
public class CheckPaymentRequestConverter implements Converter<com.alitalia.aem.common.messages.home.CheckPaymentRequest , CheckPaymentRequest> {

	@Reference
	PaymentProcessInfoDataToProcessInfo processConverter;
	
	
	
	@Override
	public CheckPaymentRequest convert(com.alitalia.aem.common.messages.home.CheckPaymentRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		CheckPaymentRequest request = objectFactory.createCheckPaymentRequest();
		request.setProcess(objectFactory.createCheckPaymentRequestProcess(processConverter.convert(source.getProcess())));
		PaymentType type = null;
		
		if(source.getType()!=null)
			type = PaymentType.fromValue(source.getType().value());
		request.setType(type);
		return request;
	}

	

}
