package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.WebCheckinCancelCartRequest;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.CancelCartRequest;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.Resource;



@Component(immediate=true, metatype=false)
@Service(value=WebCheckinCancelCartRequestToCancelCartRequest.class)
public class WebCheckinCancelCartRequestToCancelCartRequest implements Converter<WebCheckinCancelCartRequest, CancelCartRequest> {

	@Reference
	private MmbRequestBaseInfoDataToRequestBaseInfo mmbRequestBaseInfoDataConverter;

	@Override
	public CancelCartRequest convert(WebCheckinCancelCartRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		CancelCartRequest destination = null;

		if (source != null) {
			destination = objectFactory.createCancelCartRequest();

			destination.setCartId(source.getCartId());
			destination.setClient(objectFactory.createGetCartRequestClient(source.getClient()));
			destination.setMachineName(objectFactory.createAddToCartRequestMachineName(source.getMachineName()));
			destination.setSessionId(objectFactory.createAddToCartRequestSessionId(source.getSessionId()));

			destination.setX003CResourceX003EKBackingField(Resource.WEB);
			destination.setX003CInfoRequestX003EKBackingField(
					mmbRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
		}

		return destination;
	}

}
