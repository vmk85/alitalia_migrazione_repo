package com.alitalia.aem.service.impl.converter.home.carnetpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.carnet.CarnetPaymentData;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.ObjectFactory;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.PaymentOfanyTypeanyType;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.PaymentType;


@Component(immediate=true, metatype=false)
@Service(value=CarnetPaymentDataToPayment.class)
public class CarnetPaymentDataToPayment 
		implements Converter<CarnetPaymentData, PaymentOfanyTypeanyType> {
	
	@Reference
	private CarnetProviderDataToAPaymentProviderOfAnyType carnetProviderDataConverter;
	
	@Reference
	private CarnetProcessInfoDataToProcessInfo processInfoConverter;
	
	
	@Override
	public PaymentOfanyTypeanyType convert(CarnetPaymentData source) {
		
		PaymentOfanyTypeanyType destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			destination = objectFactory.createPaymentOfanyTypeanyType();
			
			destination.setEnabledNewsLetter(source.getEnabledNewsLetter());
			
			destination.setCurrency(
					objectFactory.createPaymentOfanyTypeanyTypeCurrency(
							source.getCurrency()));
			
			destination.setDescription(
					objectFactory.createPaymentOfanyTypeanyTypeDescription(
							source.getDescription()));
			
			destination.setEmail(
					objectFactory.createPaymentOfanyTypeanyTypeEmail(
							source.getEmail()));
			
			destination.setGrossAmount(source.getGrossAmount());

			destination.setNetAmount(
					source.getNetAmount());
			
			if (source.getProcess() != null) {
				destination.setProcess(
						objectFactory.createPaymentOfanyTypeanyTypeProcess(
								processInfoConverter.convert(
										source.getProcess())));
			}
			
			if (source.getProvider() != null) {
				destination.setProvider(
						objectFactory.createPaymentOfanyTypeanyTypeProvider(
								carnetProviderDataConverter.convert(
										source.getProvider())));
			}
			
			if (source.getXsltMail() != null) {
				destination.setXsltMail(
						objectFactory.createPaymentOfanyTypeanyTypeXsltMail(
								source.getXsltMail()));
			}
			
			PaymentType type = null;
			if(source.getType()!=null)
				type = PaymentType.fromValue(source.getType().value());
			destination.setType(type);
			
		}
		
		return destination;
	}

}
