package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ECouponData;
import com.alitalia.aem.common.data.home.enumerations.DiscountTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.FareTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.ECoupon;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.PassengerType;

@Component(immediate=true, metatype=false)
@Service(value=ECouponToECouponData.class)
public class ECouponToECouponData 
		implements Converter<ECoupon, ECouponData> {
	
	@Override
	public ECouponData convert(ECoupon source) {
		
		ECouponData destination = null;
		
		if (source != null) {
			
			destination = new ECouponData();
			
			destination.setAmount(
					source.getAmount());
			
			destination.setAmountBus(
					source.getAmountBus().getValue());
			
			if (source.getAmountBus() != null) {
				destination.setAmountBus(
						source.getAmountBus().getValue());
			}
			
			if (source.getAmountEco() != null) {
				destination.setAmountEco(
						source.getAmountEco().getValue());
			}
			
			if (source.getAmountEcoPlus() != null) {
				destination.setAmountEcoPlus(
						source.getAmountEcoPlus().getValue());
			}
			
			if (source.getCode() != null) {
				destination.setCode(
						source.getCode().getValue());
			}
			
			destination.setDatBlkFrm(
					XsdConvertUtils.parseCalendar(
							source.getDatBlkFrm()));
						
			destination.setDatBlkTo(
					XsdConvertUtils.parseCalendar(
							source.getDatBlkTo()));
						
			destination.setDatBlkFrm(
					XsdConvertUtils.parseCalendar(
							source.getDatBlkFrm()));
			
			destination.setDatBlkTo(
					XsdConvertUtils.parseCalendar(
							source.getDatBlkTo()));
			
			destination.setDatVolFrm(
					XsdConvertUtils.parseCalendar(
							source.getDatVolFrm()));
			
			destination.setDatVolTo(
					XsdConvertUtils.parseCalendar(
							source.getDatVolTo()));
			
			destination.setErrorCode(
					source.getErrorCode());
			
			if (source.getErrorDescription() != null) {
				destination.setErrorDescription(
						source.getErrorDescription().getValue());
			}
			
			if (source.getFamilyEcoupon() != null) {
				destination.setFamilyEcoupon(
						source.getFamilyEcoupon().getValue());
			}
			
			if (source.getFare() != null) {
				destination.setFare(
						FareTypeEnum.fromValue(source.getFare().value()));
			}
			
			destination.setSingleUse(
					source.isIsSingleUse());
			
			destination.setValid(
					source.isIsValid());
			
			List<PassengerTypeEnum> paxTypes = new ArrayList<PassengerTypeEnum>();
			if (source.getPaxTypes() != null && source.getPaxTypes().getValue() != null) {
				for (PassengerType paxType : source.getPaxTypes().getValue().getPassengerType()) {
					paxTypes.add(
							PassengerTypeEnum.fromValue(
									paxType.value()));
				}
			}
			destination.setPaxTypes(paxTypes);
			
			if (source.getType() != null) {
				destination.setType(
						DiscountTypeEnum.fromValue(source.getType().value()));
			}

		}
		
		return destination;
	}

}
