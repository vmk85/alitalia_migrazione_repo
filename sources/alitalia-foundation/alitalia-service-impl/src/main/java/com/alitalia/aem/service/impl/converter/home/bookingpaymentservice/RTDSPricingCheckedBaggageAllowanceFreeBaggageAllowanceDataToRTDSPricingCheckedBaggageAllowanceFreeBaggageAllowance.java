package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowanceData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowance;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingCheckedBaggageAllowanceFreeBaggageAllowanceDataToRTDSPricingCheckedBaggageAllowanceFreeBaggageAllowance.class)
public class RTDSPricingCheckedBaggageAllowanceFreeBaggageAllowanceDataToRTDSPricingCheckedBaggageAllowanceFreeBaggageAllowance implements
		Converter<ResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowanceData, 
					ResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowance> {

	@Override
	public ResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowance convert(
			ResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowanceData source) {
		ResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowance destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowance();

			destination.setBagDescriptorField(null);

			destination.setKilosField(source.getKilosField());
			destination.setKilosFieldSpecified(source.isKilosFieldSpecified());
			destination.setKilosPerPieceField(source.getKilosPerPieceField());
			destination.setKilosPerPieceFieldSpecified(source.isKilosPerPieceFieldSpecified());
			destination.setPiecesField(source.getPiecesField());
			destination.setPiecesFieldSpecified(source.isPiecesFieldSpecified());
			destination.setPoundsField(source.getPoundsField());
			destination.setPoundsFieldSpecified(source.isPoundsFieldSpecified());
		}

		return destination;
	}

}
