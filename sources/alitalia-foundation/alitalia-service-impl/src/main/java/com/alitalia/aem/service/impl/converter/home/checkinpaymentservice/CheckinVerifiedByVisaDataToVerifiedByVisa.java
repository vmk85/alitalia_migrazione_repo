package com.alitalia.aem.service.impl.converter.home.checkinpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinVerifiedByVisaData;
import com.alitalia.aem.ws.checkin.paymentservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.checkin.paymentservice.xsd2.VerifiedByVisa;


@Component(immediate=true, metatype=false)
@Service(value=CheckinVerifiedByVisaDataToVerifiedByVisa.class)
public class CheckinVerifiedByVisaDataToVerifiedByVisa implements Converter<CheckinVerifiedByVisaData, VerifiedByVisa> {
	

	@Override
	public VerifiedByVisa convert(CheckinVerifiedByVisaData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		VerifiedByVisa destination = null;

		if (source != null) {
			destination = objectFactory.createVerifiedByVisa();
			
			destination.setX003CIdX003EKBackingField(source.getId());
			destination.setX003CThreeDSecureOpaqueParams64EncodedX003EKBackingField(source.getThreeDSecureOpaqueParams64Encoded());
			
		}
		return destination;
	}

}
