package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentBookingInfoData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionSliceSegmentBookingInfo;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentBookingInfoDataToRTDSSliceSegmentBookingInfo.class)
public class RTDSSliceSegmentBookingInfoDataToRTDSSliceSegmentBookingInfo implements
		Converter<ResultTicketingDetailSolutionSliceSegmentBookingInfoData, 
					ResultTicketingDetailSolutionSliceSegmentBookingInfo> {

	@Override
	public ResultTicketingDetailSolutionSliceSegmentBookingInfo convert(
			ResultTicketingDetailSolutionSliceSegmentBookingInfoData source) {
		ResultTicketingDetailSolutionSliceSegmentBookingInfo destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionSliceSegmentBookingInfo();

			destination.setBookingCodeCountField(source.getBookingCodeCountField());
			destination.setBookingCodeField(source.getBookingCodeField());
			destination.setCabinField(source.getCabinField());
			destination.setMarriedSegmentIndexField(source.getMarriedSegmentIndexField());
			destination.setMarriedSegmentIndexFieldSpecified(source.isMarriedSegmentIndexFieldSpecified());
		}

		return destination;
	}

}
