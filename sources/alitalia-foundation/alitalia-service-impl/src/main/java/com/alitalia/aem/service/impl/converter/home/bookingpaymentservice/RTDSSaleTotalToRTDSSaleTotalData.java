package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSaleTotalData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionSaleTotal;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSaleTotalToRTDSSaleTotalData.class)
public class RTDSSaleTotalToRTDSSaleTotalData implements
		Converter<ResultTicketingDetailSolutionSaleTotal, ResultTicketingDetailSolutionSaleTotalData> {

	@Override
	public ResultTicketingDetailSolutionSaleTotalData convert(ResultTicketingDetailSolutionSaleTotal source) {
		ResultTicketingDetailSolutionSaleTotalData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionSaleTotalData();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
