package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentConnectionExtData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionSliceSegmentConnectionExt;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentConnectionExtDataToRTDSSliceSegmentConnectionExt.class)
public class RTDSSliceSegmentConnectionExtDataToRTDSSliceSegmentConnectionExt implements
		Converter<ResultTicketingDetailSolutionSliceSegmentConnectionExtData, 
					ResultTicketingDetailSolutionSliceSegmentConnectionExt> {

	@Reference
	private RTDSSliceSegmentConnectionExtWarningDataToRTDSSliceSegmentConnectionExtWarning rtdsWarningFieldConverter;

	@Override
	public ResultTicketingDetailSolutionSliceSegmentConnectionExt convert(
			ResultTicketingDetailSolutionSliceSegmentConnectionExtData source) {
		ResultTicketingDetailSolutionSliceSegmentConnectionExt destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionSliceSegmentConnectionExt();

			destination.setWarningField(rtdsWarningFieldConverter.convert(source.getWarningField()));
		}

		return destination;
	}

}
