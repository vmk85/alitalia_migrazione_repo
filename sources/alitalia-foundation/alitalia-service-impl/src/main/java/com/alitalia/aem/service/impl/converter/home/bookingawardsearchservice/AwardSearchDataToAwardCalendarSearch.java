package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AwardSearchData;
import com.alitalia.aem.common.data.home.PassengerNumbersData;
import com.alitalia.aem.common.data.home.SearchDestinationData;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.CabinTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.ResidencyTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.SearchTypeEnum;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd2.AwardCalendarSearch;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.PassengerNumbers;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.SearchDestination;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd5.Cabin;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd5.CabinType;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd5.ResidencyType;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd5.SearchType;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd6.ArrayOfanyType;

@Component(immediate=true, metatype=false)
@Service(value=AwardSearchDataToAwardCalendarSearch.class)
public class AwardSearchDataToAwardCalendarSearch implements Converter<AwardSearchData, AwardCalendarSearch> {

	@Reference
	private SearchDestinationDataToSearchDestination destinationConverter;

	@Reference
	private PassengerNumbersDataToPassengerNumbers psnNumbersConverter;

	@Override
	public AwardCalendarSearch convert(AwardSearchData source) {
		com.alitalia.aem.ws.bookingaward.searchservice.xsd2.ObjectFactory factory2 = new com.alitalia.aem.ws.bookingaward.searchservice.xsd2.ObjectFactory();
		com.alitalia.aem.ws.bookingaward.searchservice.xsd3.ObjectFactory factory3 = new com.alitalia.aem.ws.bookingaward.searchservice.xsd3.ObjectFactory();
		com.alitalia.aem.ws.bookingaward.searchservice.xsd6.ObjectFactory factory6 = new com.alitalia.aem.ws.bookingaward.searchservice.xsd6.ObjectFactory();

		ArrayOfanyType destinations = factory6.createArrayOfanyType();
		for (SearchDestinationData sdData : source.getDestinations()) {
			SearchDestination searchDestination = destinationConverter.convert(sdData);
			destinations.getAnyType().add(searchDestination);
		}

		ArrayOfanyType passengerNumbers = factory6.createArrayOfanyType();
		for (PassengerNumbersData psnNumberData: source.getPassengerNumbers()) {
			PassengerNumbers numbers = psnNumbersConverter.convert(psnNumberData);
			passengerNumbers.getAnyType().add(numbers);
		}

		AwardCalendarSearch destination = factory2.createAwardCalendarSearch();

		destination.setProperties(factory3.createABoomBoxGenericInfoProperties(null));
		destination.setFarmId(factory3.createABoomBoxInfoFarmId(source.getFarmId()));
		destination.setId(factory3.createABoomBoxInfoId(source.getId()));
		destination.setSessionId(factory3.createABoomBoxInfoSessionId(source.getSolutionId()));
		destination.setSolutionSet(factory3.createABoomBoxInfoSolutionSet(source.getSolutionSet()));
		destination.setCUG(factory3.createASearchCUG(source.getCug()));
		destination.setDestinations(factory3.createASearchDestinations(destinations));
		destination.setInnerSearch(factory3.createASearchInnerSearch(null));
		destination.setMarket(factory3.createASearchMarket(source.getMarket()));
		destination.setOnlyDirectFlight(source.isOnlyDirectFlight());
		destination.setPassengerNumbers(factory3.createASearchPassengerNumbers(passengerNumbers));

		CabinEnum searchCabin = source.getSearchCabin();
		if (searchCabin != null)
			destination.setSearchCabin(Cabin.fromValue(searchCabin.value()));

		CabinTypeEnum searchCabinType = source.getSearchCabinType();
		if (searchCabinType != null)
			destination.setSearchCabinType(CabinType.fromValue(searchCabinType.value()));

		SearchTypeEnum type = source.getType();
		if (type != null)
			destination.setType(SearchType.fromValue(type.value()));

		ResidencyTypeEnum sourceResidency = source.getResidency();
		if (sourceResidency != null)
			destination.setResidency(ResidencyType.fromValue(sourceResidency.value()));
		else
			destination.setResidency(ResidencyType.NONE);

		return destination;
	}

}
