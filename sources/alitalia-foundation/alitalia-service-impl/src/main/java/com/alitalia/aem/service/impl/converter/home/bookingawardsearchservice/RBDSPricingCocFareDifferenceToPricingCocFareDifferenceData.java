//2
package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingCocFareDifferenceData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetailsSolutionPricingCocFareDifference;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingCocFareDifferenceToPricingCocFareDifferenceData.class)
public class RBDSPricingCocFareDifferenceToPricingCocFareDifferenceData
		implements Converter<ResultBookingDetailsSolutionPricingCocFareDifference, ResultBookingDetailsSolutionPricingCocFareDifferenceData> {

	@Override
	public ResultBookingDetailsSolutionPricingCocFareDifferenceData convert(
			ResultBookingDetailsSolutionPricingCocFareDifference source) {
		ResultBookingDetailsSolutionPricingCocFareDifferenceData destination = null;
		
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingCocFareDifferenceData();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		
		return destination;
	}
	
	public ResultBookingDetailsSolutionPricingCocFareDifferenceData convert(
			com.alitalia.aem.ws.bookingaward.searchservice.xsd13.ResultBookingDetailsSolutionPricingCocFareDifference source) {
		ResultBookingDetailsSolutionPricingCocFareDifferenceData destination = null;
		
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingCocFareDifferenceData();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		
		return destination;
	}

}
