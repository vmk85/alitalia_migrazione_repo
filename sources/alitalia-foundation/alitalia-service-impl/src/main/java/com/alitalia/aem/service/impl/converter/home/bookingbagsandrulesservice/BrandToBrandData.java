package com.alitalia.aem.service.impl.converter.home.bookingbagsandrulesservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.BaggageAllowanceData;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd2.ArrayOfanyType;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd2.ArrayOfstring;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd4.ArrayOfDictionaryItem;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd4.BaggageAllowance;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd4.Brand;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd4.BrandPenalties;


@Component(immediate=true, metatype=false)
@Service(value=BrandToBrandData.class)
public class BrandToBrandData implements Converter<Brand, BrandData> {

	@Reference
	private BaggageAllowanceToBaggageAllowanceData baggageAllowanceConverter; 

	@Reference
	private BrandPenaltiesToBrandPenaltiesData brandPenaltiesConverter;

	@Reference
	private ArrayOfDictionaryItemToPropertiesData arrayOfDictionaryItemConverter;

	@Override
	public BrandData convert(Brand source) {
		BrandData destination = new BrandData();

		destination.setAdvancePurchaseDays(source.getAdvancePurchaseDays());
		destination.setBestFare(source.isBestFare());
		destination.setCode(source.getCode().getValue());
		destination.setCompartimentalClass(source.getCompartimentalClass().getValue());
		destination.setCurrency(source.getCurrency().getValue());
		destination.setEnabled(source.isEnabled());
		destination.setGrossFare(source.getGrossFare());
		destination.setId(null);
		destination.setIndex(source.getIndex());
		destination.setNetFare(source.getNetFare());
		destination.setRealPrice(source.getRealPrice());
		destination.setRefreshSolutionId(source.getRefreshSolutionId().getValue());
		destination.setSeatsAvailable(source.getSeatsAvailable());
		destination.setSelected(source.isSelected());
		destination.setSolutionId(source.getSolutionId().getValue());
		destination.setSolutionSet(null);
		destination.setTridionName(source.getTridionName().getValue());

		ArrayList<BaggageAllowanceData> baggageAllowance = new ArrayList<BaggageAllowanceData>();
		ArrayOfanyType sourceBaggageAllowance = source.getBaggageAllowanceList().getValue();
		if (sourceBaggageAllowance != null) {
			List<Object> sourceBaggageAllow = sourceBaggageAllowance.getAnyType();
			for (Object baggageAllow : sourceBaggageAllow) {
				BaggageAllowance castedBaggageAllowance = (BaggageAllowance) baggageAllow;
				BaggageAllowanceData allowanceData = baggageAllowanceConverter.convert(castedBaggageAllowance);
				baggageAllowance.add(allowanceData);
			}
		}
		destination.setBaggageAllowanceList(baggageAllowance);

		ArrayList<String> notes = new ArrayList<String>();
		ArrayOfstring sourceNotes = source.getNotes().getValue();
		if (sourceNotes != null) {
			for (String noteSource : sourceNotes.getString() ) 
				notes.add(noteSource);
		}
		destination.setNotes(notes);

		BrandPenalties penalties = (BrandPenalties) source.getPenalties().getValue();
		if (penalties == null)
			destination.setPenalties(null);
		else
			destination.setPenalties(brandPenaltiesConverter.convert(penalties));

		JAXBElement<ArrayOfDictionaryItem> sourceProperties = source.getProperties();
		if (sourceProperties != null && sourceProperties.getValue() != null)
			destination.setProperties(arrayOfDictionaryItemConverter.convert(sourceProperties.getValue()));
		else
			destination.setProperties(null);

		return destination;
	}
}