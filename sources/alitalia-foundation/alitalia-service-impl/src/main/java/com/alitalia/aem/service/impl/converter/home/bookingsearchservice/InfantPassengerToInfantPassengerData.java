package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.InfantPassengerData;
import com.alitalia.aem.common.data.home.TicketInfoData;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.ws.booking.searchservice.xsd3.InfantPassenger;
import com.alitalia.aem.ws.booking.searchservice.xsd3.InfoBase;
import com.alitalia.aem.ws.booking.searchservice.xsd3.TicketInfo;

@Component(immediate=true, metatype=false)
@Service(value=InfantPassengerToInfantPassengerData.class)
public class InfantPassengerToInfantPassengerData 
		implements Converter<InfantPassenger, InfantPassengerData> {
	
	@Reference
	private InfoBaseToPassengerInfoBaseData infoBaseToPassengerBaseInfoDataConverter;
	
	@Reference
	private PreferencesToPreferencesData preferencesToPreferencesDataConverter;
	
	@Reference
	private TicketInfoToTicketInfoData ticketInfoToTicketInfoDataConverter;
	
	@Override
	public InfantPassengerData convert(InfantPassenger source) {
		
		InfantPassengerData destination = null;
		
		if (source != null) {
			
			destination = new InfantPassengerData();
			
			// InfantPassenger data
			
			destination.setAdultRefent(
					source.getAdultReferent());
			
			// APassengerBase data
			
			if (source.getType() != null) {
				destination.setType(
						PassengerTypeEnum.fromValue(source.getType().value()));
			}
			
			if (source.getLastName() != null) {
				destination.setLastName(
						source.getLastName().getValue());
			}
			
			if (source.getName() != null) {
				destination.setName(
						source.getName().getValue());
			}
			
			destination.setCouponPrice(
					source.getCouponPrice());
			
			destination.setExtraCharge(
					source.getExtraCharge());
			
			destination.setFee(
					source.getFee());
			
			destination.setCcFee(
					source.getCCFee());
			
			destination.setGrossFare(
					source.getGrossFare());
			
			destination.setNetFare(
					source.getNetFare());
			
			if (source.getInfo() != null) {
				destination.setInfo(
						infoBaseToPassengerBaseInfoDataConverter.convert(
								(InfoBase) source.getInfo().getValue()));
			}
			
			List<TicketInfoData> tickets = null;
			if (source.getTickets() != null && source.getTickets().getValue() != null) {
				tickets = new ArrayList<TicketInfoData>();
				for (Object ticketInfo : source.getTickets().getValue().getAnyType()) {
					tickets.add(
							ticketInfoToTicketInfoDataConverter.convert(
									(TicketInfo) ticketInfo));
				}
			}
			destination.setTickets(
					tickets);
		
		}
		
		return destination;
	}

}
