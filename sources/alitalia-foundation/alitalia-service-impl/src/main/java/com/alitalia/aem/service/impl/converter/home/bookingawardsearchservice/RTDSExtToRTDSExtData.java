package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionExtData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionExt;

@Component(immediate=true, metatype=false)
@Service(value=RTDSExtToRTDSExtData.class)
public class RTDSExtToRTDSExtData implements
		Converter<ResultTicketingDetailSolutionExt, ResultTicketingDetailSolutionExtData> {

	@Override
	public ResultTicketingDetailSolutionExtData convert(ResultTicketingDetailSolutionExt source) {
		ResultTicketingDetailSolutionExtData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionExtData();

			destination.setPrivateFareField(source.isPrivateFareField());
			destination.setPrivateFareFieldSpecified(source.isPrivateFareFieldSpecified());
		}

		return destination;
	}

}
