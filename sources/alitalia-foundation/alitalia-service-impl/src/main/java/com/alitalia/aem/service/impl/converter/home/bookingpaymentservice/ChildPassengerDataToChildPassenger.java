package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ChildPassengerData;
import com.alitalia.aem.common.data.home.TicketInfoData;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ChildPassenger;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.PassengerType;
import com.alitalia.aem.ws.booking.paymentservice.xsd5.ArrayOfanyType;

@Component(immediate=true, metatype=false)
@Service(value=ChildPassengerDataToChildPassenger.class)
public class ChildPassengerDataToChildPassenger 
		implements Converter<ChildPassengerData, ChildPassenger> {
	
	@Reference
	private PassengerInfoBaseDataToInfoBase infoBaseDataToInfoBaseConverter;
	
	@Reference
	private PreferencesDataToPreferences preferencesDataToPreferencesConverter;
	
	@Reference
	private TicketInfoDataToTicketInfo ticketInfoDataToTicketInfoConverter;
	
	@Override
	public ChildPassenger convert(ChildPassengerData source) {
		
		ChildPassenger destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			com.alitalia.aem.ws.booking.paymentservice.xsd5.ObjectFactory xsd5ObjectFactory = 
					new com.alitalia.aem.ws.booking.paymentservice.xsd5.ObjectFactory();
			
			destination = objectFactory.createChildPassenger();
			
			// ARegularPassenger data
			
			destination.setPreferences(
					objectFactory.createARegularPassengerPreferences(
							preferencesDataToPreferencesConverter.convert(
									source.getPreferences())));
			
			// APassengerBase data
			
			if (source.getType() != null) {
				destination.setType(
						PassengerType.fromValue(source.getType().value()));
			}
			
			destination.setLastName(
					objectFactory.createAPassengerBaseLastName(
							source.getLastName()));
			
			destination.setName(
					objectFactory.createAPassengerBaseName(
							source.getName()));
			
			destination.setCouponPrice(
					source.getCouponPrice());
			
			destination.setExtraCharge(
					source.getExtraCharge());
			
			destination.setFee(
					source.getFee());
			
			destination.setGrossFare(
					source.getGrossFare());
			
			destination.setNetFare(
					source.getNetFare());
			
			destination.setInfo(
					objectFactory.createAPassengerBaseInfo(
							infoBaseDataToInfoBaseConverter.convert(
									source.getInfo())));
			
			ArrayOfanyType tickets = xsd5ObjectFactory.createArrayOfanyType();
			if (source.getTickets() != null && !source.getTickets().isEmpty()) {
				for (TicketInfoData ticketInfoData : source.getTickets()) {
					tickets.getAnyType().add(
							ticketInfoDataToTicketInfoConverter.convert(
								ticketInfoData));
				}
			}
			destination.setTickets(
					objectFactory.createAPassengerBaseTickets(
							tickets));
		
		}
		
		return destination;
	}

}
