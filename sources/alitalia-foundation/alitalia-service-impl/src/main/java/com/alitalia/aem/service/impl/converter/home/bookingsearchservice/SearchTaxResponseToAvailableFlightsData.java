package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AvailableFlightsData;
import com.alitalia.aem.ws.booking.searchservice.xsd1.SearchResponse;
import com.alitalia.aem.ws.booking.searchservice.xsd15.AvailableFlights;
import com.alitalia.aem.ws.booking.searchservice.xsd2.ModelResponse;

@Component(immediate=true, metatype=false)
@Service(value=SearchTaxResponseToAvailableFlightsData.class)
public class SearchTaxResponseToAvailableFlightsData implements Converter<SearchResponse, AvailableFlightsData> {

	private static final Logger logger = LoggerFactory.getLogger(SearchTaxResponseToAvailableFlightsData.class);

	@Reference
	private AvailableFlightsToAvailableFlightsData availableFlightsConverter;

	@Override
	public AvailableFlightsData convert(SearchResponse source) {
		AvailableFlightsData destination = null;

		logger.debug("Converting from SearchTaxResponse to AvailableFlightsData class.");
		ModelResponse result = (ModelResponse) source.getResult().getValue();
		AvailableFlights flights = (AvailableFlights) result.getModelTaxes().getValue();
		
		//if (flights != null){
			destination = availableFlightsConverter.convert(flights);
		//}
		return destination;
	}

}
