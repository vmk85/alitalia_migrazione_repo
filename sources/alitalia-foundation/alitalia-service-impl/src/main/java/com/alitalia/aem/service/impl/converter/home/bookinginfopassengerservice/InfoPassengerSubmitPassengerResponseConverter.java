package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.messages.home.InfoPassengerSubmitPassengerResponse;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd1.PassengerSubmitResponse;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.AdultPassenger;

@Component(immediate=true, metatype=false)
@Service(value=InfoPassengerSubmitPassengerResponseConverter.class)
public class InfoPassengerSubmitPassengerResponseConverter 
		implements Converter<PassengerSubmitResponse, InfoPassengerSubmitPassengerResponse> {

	@Reference
	private AdultPassengerToAdultPassengerData adultPassengerToAdultPassengerDataConverter;
	
	@Override
	public InfoPassengerSubmitPassengerResponse convert(PassengerSubmitResponse source) {
		
		InfoPassengerSubmitPassengerResponse destination = null;
		
		if (source != null) {
	
			destination = new InfoPassengerSubmitPassengerResponse();
			
			if (source.getAdultPassengersWithWrongFrequentFlyer() != null && 
					source.getAdultPassengersWithWrongFrequentFlyer().getValue() != null &&
					source.getAdultPassengersWithWrongFrequentFlyer().getValue().getAdultPassenger() != null) {
				List<AdultPassengerData> adultPassengersWithWrongFrequentFlyer = new ArrayList<>();
				for (AdultPassenger adultPassenger : 
						source.getAdultPassengersWithWrongFrequentFlyer().getValue().getAdultPassenger()) {
					adultPassengersWithWrongFrequentFlyer.add(
							adultPassengerToAdultPassengerDataConverter.convert(
									adultPassenger));
				}
				destination.setAdultPassengersWithWrongFrequentFlyer(
						adultPassengersWithWrongFrequentFlyer);
			}
			destination.setCookie(source.getCookie().getValue());
			destination.setExecute(source.getExecution().getValue());
			destination.setSabreGateWayAuthToken(source.getSabreGateWayAuthToken().getValue());
			
		}
		
		return destination;
	}

}
