package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentLegData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentLegScheduledServicesData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionSliceSegmentLeg;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionSliceSegmentLegScheduledServices;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentLegToRTDSSliceSegmentLegData.class)
public class RTDSSliceSegmentLegToRTDSSliceSegmentLegData implements
		Converter<ResultTicketingDetailSolutionSliceSegmentLeg, ResultTicketingDetailSolutionSliceSegmentLegData> {

	@Reference
	private RTDSSliceSegmentLegAircraftToRTDSSliceSegmentLegAircraftData aircraftFieldConverter;

	@Reference
	private RTDSSliceSegmentLegConnectionToRTDSSliceSegmentLegConnectionData connectionFieldConverter;

	@Reference
	private RTDSSliceSegmentLegDistanceToRTDSSliceSegmentLegDistanceData distanceFieldConverter;

	@Reference
	private RTDSSliceSegmentLegExtToRTDSSliceSegmentLegExtData extFieldConverter;

	@Reference
	private RTDSSliceSegmentLegOperationalDisclosureToRTDSSliceSegmentLegOperationalDisclosureData operationalDisclosureFieldConverter;

	@Reference
	private RTDSSliceSegmentLegOperationalFlightToRTDSSliceSegmentLegOperationalFlightData operationalFlightFieldConverter;

	@Reference
	private RTDSSliceSegmentLegScheduledServicesToRTDSSliceSegmentLegScheduledServicesData scheduledServicesFieldConverter;

	@Override
	public ResultTicketingDetailSolutionSliceSegmentLegData convert(ResultTicketingDetailSolutionSliceSegmentLeg source) {
		ResultTicketingDetailSolutionSliceSegmentLegData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionSliceSegmentLegData();

			destination.setArrivalField(source.getArrivalField());
			destination.setDepartureField(source.getDepartureField());
			destination.setDestinationField(source.getDestinationField());
			destination.setDestinationTerminalField(source.getDestinationTerminalField());
			destination.setDurationField(source.getDurationField());
			destination.setDurationFieldSpecified(source.isDurationFieldSpecified());
			destination.setOriginField(source.getOriginField());
			destination.setOriginTerminalField(source.getOriginTerminalField());

			destination.setAircraftField(aircraftFieldConverter.convert(source.getAircraftField()));

			destination.setConnectionField(connectionFieldConverter.convert(source.getConnectionField()));

			destination.setDistanceField(distanceFieldConverter.convert(source.getDistanceField()));

			destination.setExtField(extFieldConverter.convert(source.getExtField()));

			destination.setOperationalDisclosureField(
					operationalDisclosureFieldConverter.convert(source.getOperationalDisclosureField()));

			destination.setOperationalFlightField(
					operationalFlightFieldConverter.convert(source.getOperationalFlightField()));

			List<ResultTicketingDetailSolutionSliceSegmentLegScheduledServicesData> scheduledServicesField = null;
			if (source.getScheduledServicesField() != null &&
					source.getScheduledServicesField().getResultTicketingDetailSolutionSliceSegmentLegScheduledServices() != null &&
					!source.getScheduledServicesField().getResultTicketingDetailSolutionSliceSegmentLegScheduledServices().isEmpty()) {
				scheduledServicesField = new ArrayList<ResultTicketingDetailSolutionSliceSegmentLegScheduledServicesData>();
				for(ResultTicketingDetailSolutionSliceSegmentLegScheduledServices sourceScheduledServicesFieldElem : 
						source.getScheduledServicesField().getResultTicketingDetailSolutionSliceSegmentLegScheduledServices())
					scheduledServicesField.add(scheduledServicesFieldConverter.convert(sourceScheduledServicesFieldElem));
			}
			destination.setScheduledServicesField(scheduledServicesField);
		}

		return destination;
	}

}
