//2
package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingExtPaxData;
import com.alitalia.aem.ws.booking.paymentservice.xsd11.ResultBookingDetailsSolutionPricingExtPax;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingExtPaxToPricingExtPaxData.class)
public class RBDSPricingExtPaxToPricingExtPaxData implements Converter<ResultBookingDetailsSolutionPricingExtPax, ResultBookingDetailsSolutionPricingExtPaxData> {

	@Override
	public ResultBookingDetailsSolutionPricingExtPaxData convert(
			ResultBookingDetailsSolutionPricingExtPax source) {
		ResultBookingDetailsSolutionPricingExtPaxData destination = null;
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingExtPaxData();
			destination.setAdultsField(source.getAdultsField());
			destination.setAdultsFieldSpecified(source.isAdultsFieldSpecified());
			destination.setChildrenField(source.getChildrenField());
			destination.setChildrenFieldSpecified(source.isChildrenFieldSpecified());
			destination.setInfantsInLapField(source.getInfantsInLapField());
			destination.setInfantsInLapFieldSpecified(source.isInfantsInLapFieldSpecified());
//			destination.setMilitaryField(source.getMilitaryField());
//			destination.setMilitaryFieldSpecified(source.isMilitaryFieldSpecified());
			destination.setYouthField(source.getYouthField());
			destination.setYouthFieldSpecified(source.isYouthFieldSpecified());
		}
		return destination;
	}

}
