package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PhonePrefixData;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.PhonePrefix;

@Component(immediate = true, metatype = false)
@Service(value = PhonePrefixToPhonePrefixData.class)
public class PhonePrefixToPhonePrefixData implements Converter<PhonePrefix, PhonePrefixData> {

	@Override
	public PhonePrefixData convert(PhonePrefix source) {
		PhonePrefixData destination = null;

		if (source != null) {
			destination = new PhonePrefixData();

			if (source.getCode() != null) {
				destination.setCode(source.getCode().getValue());
			}

			if (source.getDescription() != null) {
				destination.setDescription(source.getDescription().getValue());
			}

			if (source.getPrefix() != null) {
				destination.setPrefix(source.getPrefix().getValue());
			}
		}

		return destination;
	}

}
