package com.alitalia.aem.service.impl.converter.home.carnetpaymentservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.CarnetBuyCarnetResponse;
import com.alitalia.aem.service.impl.converter.home.carnetservice.LoginCarnetResponseToCarnetLoginResponse;
import com.alitalia.aem.service.impl.converter.home.checkinpaymentservice.PaymentTransactionInfoToCheckinPaymentTransactionInfoData;
import com.alitalia.aem.ws.carnet.paymentservice.xsd1.BuyCarnetResponse;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.InfoCarnet;

@Component(immediate=true, metatype=false)
@Service(value=BuyCarnetResponseToCarnetBuyCarnetResponse.class)
public class BuyCarnetResponseToCarnetBuyCarnetResponse implements Converter<BuyCarnetResponse, CarnetBuyCarnetResponse> {
	
	@Reference
	private PaymentTransactionInfoToCheckinPaymentTransactionInfoData paymentTransactionInfoConverter;

	@Reference
	private InfoCarnetToCarnetInfoCarnet infoCarnetResponseConverter;
	
	@Reference
	private CarnetPaymentServiceJAXBContextFactory jaxbContextFactory;
	
	private Logger logger = LoggerFactory.getLogger(LoginCarnetResponseToCarnetLoginResponse.class);

	@Override
	public CarnetBuyCarnetResponse convert(BuyCarnetResponse source) {
		CarnetBuyCarnetResponse destination = null;

		if (source != null) {
			destination = new CarnetBuyCarnetResponse();
			
			if(source.getInfoCarnet() != null) {
				if (source.getInfoCarnet().getValue() instanceof InfoCarnet){
					destination.setInfoCarnet(infoCarnetResponseConverter.convert((InfoCarnet) source.getInfoCarnet().getValue()));
				} else {
					try {
						Unmarshaller carnetUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
						@SuppressWarnings("unchecked")
						JAXBElement<InfoCarnet> unmarshalledObj = (JAXBElement<InfoCarnet>) carnetUnmarshaller.unmarshal((Node) source.getInfoCarnet().getValue());
						InfoCarnet infoCarn = (InfoCarnet) unmarshalledObj.getValue();
						destination.setInfoCarnet(infoCarnetResponseConverter.convert(infoCarn));
					} catch (JAXBException e) {
						logger.error("Error while Unmarshalling InfoCarnet: ", e);
					}
				}
			}
		}
		return destination;
	}

}
