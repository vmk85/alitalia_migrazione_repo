package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillarySeatDetailData;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.SeatDetail;


@Component(immediate=true, metatype=false)
@Service(value=SeatDetailToMmbAncillarySeatDetailData.class)
public class SeatDetailToMmbAncillarySeatDetailData implements 
			Converter<SeatDetail, MmbAncillarySeatDetailData> {

	@Override
	public MmbAncillarySeatDetailData convert(SeatDetail source) {
		MmbAncillarySeatDetailData destination = null;

		if (source != null) {
			destination = new MmbAncillarySeatDetailData();

			destination.setComfort(source.isX003CIsComfortX003EKBackingField());
			destination.setPayment(source.isX003CIsPaymentX003EKBackingField());
			destination.setSeat(source.getX003CSeatX003EKBackingField());
		}

		return destination;
	}
}