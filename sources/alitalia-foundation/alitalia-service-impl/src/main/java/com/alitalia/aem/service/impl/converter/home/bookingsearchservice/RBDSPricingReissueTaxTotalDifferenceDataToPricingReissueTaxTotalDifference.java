package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingReissueTaxTotalDifferenceData;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ResultBookingDetailsSolutionPricingReissueTaxTotalDifference;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingReissueTaxTotalDifferenceDataToPricingReissueTaxTotalDifference.class)
public class RBDSPricingReissueTaxTotalDifferenceDataToPricingReissueTaxTotalDifference
		implements Converter<ResultBookingDetailsSolutionPricingReissueTaxTotalDifferenceData, ResultBookingDetailsSolutionPricingReissueTaxTotalDifference> {

	@Override
	public ResultBookingDetailsSolutionPricingReissueTaxTotalDifference convert(
			ResultBookingDetailsSolutionPricingReissueTaxTotalDifferenceData source) {
		ResultBookingDetailsSolutionPricingReissueTaxTotalDifference destination = null;
		if(source!=null){
			ObjectFactory objf = new ObjectFactory();
			destination = objf.createResultBookingDetailsSolutionPricingReissueTaxTotalDifference();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}

}
