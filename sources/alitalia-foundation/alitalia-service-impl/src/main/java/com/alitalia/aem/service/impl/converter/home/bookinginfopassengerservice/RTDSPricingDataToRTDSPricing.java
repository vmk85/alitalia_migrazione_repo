package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowanceData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowanceData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareCalculationData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingPassengerData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingTaxData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ArrayOfresultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowance;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ArrayOfresultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowance;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ArrayOfresultTicketingDetailSolutionPricingFare;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ArrayOfresultTicketingDetailSolutionPricingFareCalculation;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ArrayOfresultTicketingDetailSolutionPricingPassenger;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ArrayOfresultTicketingDetailSolutionPricingTax;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionPricing;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingDataToRTDSPricing.class)
public class RTDSPricingDataToRTDSPricing implements
		Converter<ResultTicketingDetailSolutionPricingData, ResultTicketingDetailSolutionPricing> {

	@Reference
	private RTDSPricingCarryOnBaggageAllowanceFreeBaggageAllowanceDataToRTDSPricingCarryOnBaggageAllowanceFreeBaggageAllowance rtdsCarryOnBaggageAllowanceFieldConverter;

	@Reference
	private RTDSPricingCheckedBaggageAllowanceFreeBaggageAllowanceDataToRTDSPricingCheckedBaggageAllowanceFreeBaggageAllowance rtdsCheckedBaggageAllowanceFieldConverter;

	@Reference
	private RTDSPricingDisplayFareTotalDataToRTDSPricingDisplayFareTotal rtdsPricingDisplayFareTotalDataConverter;

	@Reference
	private RTDSPricingDisplayPriceDataToRTDSPricingDisplayPrice rtdsPricingDisplayPriceDataConverter;

	@Reference
	private RTDSPricingDisplayTaxTotalDataToRTDSPricingDisplayTaxTotal rtdsPricingDisplayTaxTotalDataConverter;

	@Reference
	private RTDSPricingFareCalculationDataToRTDSPricingFareCalculation rtdsFareCalculationFieldConverter;

	@Reference
	private RTDSPricingFareDataToRTDSPricingFare rtdsFareFieldConverter;

	@Reference
	private RTDSPricingExtDataToRTDSPricingExt rtdsPricingExtDataConverter;

	@Reference
	private RTDSPricingPassengerDataToRTDSPricingPassenger rtdsPassengerFieldConverter;

	@Reference
	private RTDSPricingSaleFareTotalDataToRTDSPricingSaleFareTotal rtdsPricingSaleFareTotalDataConverter;

	@Reference
	private RTDSPricingSalePriceDataToRTDSPricingSalePrice rtdsPricingSalePriceDataConverter;

	@Reference
	private RTDSPricingSaleTaxTotalDataToRTDSPricingSaleTaxTotal rtdsPricingSaleTaxTotalDataConverter;

	@Reference
	private RTDSPricingTaxDataToRTDSPricingTax rtdsTaxFieldConverter;

	@Override
	public ResultTicketingDetailSolutionPricing convert(ResultTicketingDetailSolutionPricingData source) {
		ResultTicketingDetailSolutionPricing destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricing();

			ArrayOfresultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowance carryOnBaggageAllowanceField = null;
			if (source.getCarryOnBaggageAllowanceField() != null &&
					!source.getCarryOnBaggageAllowanceField().isEmpty()){
				carryOnBaggageAllowanceField = objectFactory.createArrayOfresultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowance();
				for(ResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowanceData sourceCarryOnBaggageAllowanceFieldElem : 
						source.getCarryOnBaggageAllowanceField())
					carryOnBaggageAllowanceField.getResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowance().add(
							rtdsCarryOnBaggageAllowanceFieldConverter.convert(sourceCarryOnBaggageAllowanceFieldElem));
			}
			destination.setCarryOnBaggageAllowanceField(carryOnBaggageAllowanceField);

			ArrayOfresultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowance checkedBaggageAllowanceField = null;
			if (source.getCheckedBaggageAllowanceField() != null &&
					!source.getCheckedBaggageAllowanceField().isEmpty()) {
				checkedBaggageAllowanceField = objectFactory.createArrayOfresultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowance();
				for(ResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowanceData sourceCheckedBaggageAllowanceFieldElem : 
						source.getCheckedBaggageAllowanceField())
					checkedBaggageAllowanceField.getResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowance().add(
							rtdsCheckedBaggageAllowanceFieldConverter.convert(sourceCheckedBaggageAllowanceFieldElem));
			}
			destination.setCheckedBaggageAllowanceField(checkedBaggageAllowanceField);

			destination.setDisplayFareTotalField(
					rtdsPricingDisplayFareTotalDataConverter.convert(source.getDisplayFareTotalField()));

			destination.setDisplayPriceField(
					rtdsPricingDisplayPriceDataConverter.convert(source.getDisplayPriceField()));

			destination.setDisplayTaxTotalField(
					rtdsPricingDisplayTaxTotalDataConverter.convert(source.getDisplayTaxTotalField()));

			destination.setExtField(rtdsPricingExtDataConverter.convert(source.getExtField()));

			ArrayOfresultTicketingDetailSolutionPricingFareCalculation fareCalculationField = null;
			if (source.getFareCalculationField() != null &&
					!source.getFareCalculationField().isEmpty()) {
				fareCalculationField = objectFactory.createArrayOfresultTicketingDetailSolutionPricingFareCalculation();
				for(ResultTicketingDetailSolutionPricingFareCalculationData sourceFareCalculationFieldElem : source.getFareCalculationField())
					fareCalculationField.getResultTicketingDetailSolutionPricingFareCalculation().add(
							rtdsFareCalculationFieldConverter.convert(sourceFareCalculationFieldElem));
			}
			destination.setFareCalculationField(fareCalculationField);

			ArrayOfresultTicketingDetailSolutionPricingFare fareField = null;
			if (source.getFareField() != null &&
					!source.getFareField().isEmpty()) {
				fareField = objectFactory.createArrayOfresultTicketingDetailSolutionPricingFare();
				for(ResultTicketingDetailSolutionPricingFareData sourceFareFieldElem : source.getFareField())
					fareField.getResultTicketingDetailSolutionPricingFare().add(
							rtdsFareFieldConverter.convert(sourceFareFieldElem));
			}
			destination.setFareField(fareField);

			ArrayOfresultTicketingDetailSolutionPricingPassenger passengerField = null;
			if (source.getPassengerField() != null &&
					!source.getPassengerField().isEmpty()) {
				passengerField = objectFactory.createArrayOfresultTicketingDetailSolutionPricingPassenger();
				for (ResultTicketingDetailSolutionPricingPassengerData sourcePassengerFieldElem : source.getPassengerField())
					passengerField.getResultTicketingDetailSolutionPricingPassenger().add(
							rtdsPassengerFieldConverter.convert(sourcePassengerFieldElem));
			}
			destination.setPassengerField(passengerField);

			destination.setPtcField(source.getPtcField());

			destination.setSaleFareTotalField(
					rtdsPricingSaleFareTotalDataConverter.convert(source.getSaleFareTotalField()));

			destination.setSalePriceField(
					rtdsPricingSalePriceDataConverter.convert(source.getSalePriceField()));

			destination.setSaleTaxTotalField(
					rtdsPricingSaleTaxTotalDataConverter.convert(source.getSaleTaxTotalField()));

			ArrayOfresultTicketingDetailSolutionPricingTax taxField = null;
			if (source.getTaxField() != null &&
					!source.getTaxField().isEmpty()) {
				taxField = objectFactory.createArrayOfresultTicketingDetailSolutionPricingTax();
				for(ResultTicketingDetailSolutionPricingTaxData sourceTaxFieldElem : source.getTaxField())
					taxField.getResultTicketingDetailSolutionPricingTax().add(
							rtdsTaxFieldConverter.convert(sourceTaxFieldElem));
			}
			destination.setTaxField(taxField);
		}

		return destination;
	}

}
