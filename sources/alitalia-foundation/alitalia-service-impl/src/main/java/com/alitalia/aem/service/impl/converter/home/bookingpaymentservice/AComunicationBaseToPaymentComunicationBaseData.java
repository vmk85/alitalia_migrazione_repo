package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ArcoQueueData;
import com.alitalia.aem.common.data.home.PaymentComunicationBancaIntesaData;
import com.alitalia.aem.common.data.home.PaymentComunicationBancoPostaData;
import com.alitalia.aem.common.data.home.PaymentComunicationBaseData;
import com.alitalia.aem.common.data.home.PaymentComunicationCreditCardData;
import com.alitalia.aem.common.data.home.PaymentComunicationFindomesticData;
import com.alitalia.aem.common.data.home.PaymentComunicationGlobalCollectData;
import com.alitalia.aem.common.data.home.PaymentComunicationInfoData;
import com.alitalia.aem.common.data.home.PaymentComunicationLottomaticaData;
import com.alitalia.aem.common.data.home.PaymentComunicationPayLaterData;
import com.alitalia.aem.common.data.home.PaymentComunicationPayPalData;
import com.alitalia.aem.common.data.home.PaymentComunicationPosteIDData;
import com.alitalia.aem.common.data.home.PaymentComunicationSTSData;
import com.alitalia.aem.common.data.home.PaymentComunicationUnicreditData;
import com.alitalia.aem.common.data.home.enumerations.BancoPostaTypeEnum;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.AComunicationBase;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.AComunicationInfo;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ArcoQueue;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ComunicationBancaIntesa;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ComunicationBancoPosta;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ComunicationCreditCard;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ComunicationFindomestic;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ComunicationGlobalCollect;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ComunicationLottomatica;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ComunicationMasterPass;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ComunicationPayLater;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ComunicationPayPal;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ComunicationPosteID;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ComunicationSTS;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ComunicationUnicredit;
import com.alitalia.aem.ws.booking.paymentservice.xsd9.KeyValuePairOfstringstring;

@Component(immediate=true, metatype=false)
@Service(value=AComunicationBaseToPaymentComunicationBaseData.class)
public class AComunicationBaseToPaymentComunicationBaseData 
		implements Converter<AComunicationBase, PaymentComunicationBaseData> {
	
	@Reference
	private ArcoQueueToArcoQueueData arcoQueueToArcoQueueDataConverter;
	
	@Override
	public PaymentComunicationBaseData convert(AComunicationBase source) {
		
		PaymentComunicationBaseData destination = null;
		
		if (source != null) {
			
			if (source instanceof ComunicationBancaIntesa) {
				ComunicationBancaIntesa typedSource = (ComunicationBancaIntesa) source;
				PaymentComunicationBancaIntesaData typedDestination = new PaymentComunicationBancaIntesaData();
				destination = typedDestination;
				
				if (typedSource.getErrorUrl() != null) {
					typedDestination.setErrorUrl(
							typedSource.getErrorUrl().getValue());
				}
				
				if (typedSource.getCancelUrl() != null) {
					typedDestination.setCancelUrl(
							typedSource.getCancelUrl().getValue());
				}
				
				if (typedSource.getShopperId() != null) {
					typedDestination.setShopperId(
							typedSource.getShopperId().getValue());
				}
				
				Map<String, String> requestData = new HashMap<String, String>();
				if (typedSource.getRequestData() != null && typedSource.getRequestData().getValue() != null) {
					for (KeyValuePairOfstringstring entry : 
							typedSource.getRequestData().getValue().getKeyValuePairOfstringstring()) {
						requestData.put(entry.getKey(), entry.getValue());
					}
				}
				typedDestination.setRequestData(requestData);
				
			} else if (source instanceof ComunicationBancoPosta) {
				ComunicationBancoPosta typedSource = (ComunicationBancoPosta) source;
				PaymentComunicationBancoPostaData typedDestination = new PaymentComunicationBancoPostaData();
				destination = typedDestination;
				
				if (typedSource.getErrorUrl() != null) {
					typedDestination.setErrorUrl(
							typedSource.getErrorUrl().getValue());
				}
				
				if (typedSource.getType() != null) {
					typedDestination.setType(
							BancoPostaTypeEnum.fromValue(typedSource.getType().value()));
				}
				
			} else if (source instanceof ComunicationPosteID) {
				ComunicationPosteID typedSource = (ComunicationPosteID) source;
				PaymentComunicationPosteIDData typedDestination = new PaymentComunicationPosteIDData();
				destination = typedDestination;
				
				if (typedSource.getErrorUrl() != null) {
					typedDestination.setErrorUrl(
							typedSource.getErrorUrl().getValue());
				}
				
			} else if (source instanceof ComunicationCreditCard) {
				ComunicationCreditCard typedSource = (ComunicationCreditCard) source;
				PaymentComunicationCreditCardData typedDestination = new PaymentComunicationCreditCardData();
				destination = typedDestination;
				
				if (typedSource.getErrorUrl() != null) {
					typedDestination.setErrorUrl(
							typedSource.getErrorUrl().getValue());
				}
				
				if (typedSource.getAcceptHeader() != null) {
					typedDestination.setAcceptHeader(
							typedSource.getAcceptHeader().getValue());
				}
				
				if (typedSource.getIPAddress() != null) {
					typedDestination.setIpAddress(
							typedSource.getIPAddress().getValue());
				}
				
				if (typedSource.getUserAgent() != null) {
					typedDestination.setUserAgent(
							typedSource.getUserAgent().getValue());
				}
				
			} else if (source instanceof ComunicationFindomestic) {
				ComunicationFindomestic typedSource = (ComunicationFindomestic) source;
				PaymentComunicationFindomesticData typedDestination = new PaymentComunicationFindomesticData();
				destination = typedDestination;
				
				if (typedSource.getUrlCallBack() != null) {
					typedDestination.setUrlCallBack(
							typedSource.getUrlCallBack().getValue());
				}
				
				if (typedSource.getUrlRedirect() != null) {
					typedDestination.setUrlRedirect(
							typedSource.getUrlRedirect().getValue());
				}
				
				Map<String, String> requestData = new HashMap<String, String>();
				if (typedSource.getRequestData() != null && typedSource.getRequestData().getValue() != null) {
					for (KeyValuePairOfstringstring entry : 
							typedSource.getRequestData().getValue().getKeyValuePairOfstringstring()) {
						requestData.put(entry.getKey(), entry.getValue());
					}
				}
				typedDestination.setRequestData(requestData);
				
			} else if (source instanceof ComunicationGlobalCollect) {
				ComunicationGlobalCollect typedSource = (ComunicationGlobalCollect) source;
				PaymentComunicationGlobalCollectData typedDestination = new PaymentComunicationGlobalCollectData();
				destination = typedDestination;
				
				if (typedSource.getCurrency() != null) {
					typedDestination.setCurrency(
							typedSource.getCurrency().getValue());
				}
				
				if (typedSource.getCustomerId() != null) {
					typedDestination.setCustomerId(
							typedSource.getCustomerId().getValue());
				}
				
				if (typedSource.getErrorUrl() != null) {
					typedDestination.setErrorUrl(
							typedSource.getErrorUrl().getValue());
				}
				
				if (typedSource.getIPAddress() != null) {
					typedDestination.setIpAddress(
							typedSource.getIPAddress().getValue());
				}
				
				typedDestination.setIsPost(
						typedSource.isIsPost());
				
				if (typedSource.getOrderId() != null) {
					typedDestination.setOrderId(
							typedSource.getOrderId().getValue());
				}
				
				Map<String, String> requestData = new HashMap<String, String>();
				if (typedSource.getRequestData() != null && typedSource.getRequestData().getValue() != null) {
					for (KeyValuePairOfstringstring entry : 
							typedSource.getRequestData().getValue().getKeyValuePairOfstringstring()) {
						requestData.put(entry.getKey(), entry.getValue());
					}
				}
				typedDestination.setRequestData(requestData);
				
			} else if (source instanceof ComunicationLottomatica) {
				PaymentComunicationLottomaticaData typedDestination = new PaymentComunicationLottomaticaData();
				destination = typedDestination; 
				
			} else if (source instanceof ComunicationMasterPass) {
				PaymentComunicationLottomaticaData typedDestination = new PaymentComunicationLottomaticaData();
				destination = typedDestination;	
				
			} else if (source instanceof ComunicationPayLater) {
				ComunicationPayLater typedSource = (ComunicationPayLater) source;
				PaymentComunicationPayLaterData typedDestination = new PaymentComunicationPayLaterData();
				destination = typedDestination;	
				
				List<String> osis = new ArrayList<String>();
				if (typedSource.getOsis() != null && typedSource.getOsis().getValue() != null) {
					for (String osisItem : typedSource.getOsis().getValue().getString()) {
						osis.add(osisItem);
					}
				}
				typedDestination.setOsis(osis);
				
				List<String> remarks = new ArrayList<String>();
				if (typedSource.getRemarks() != null && typedSource.getRemarks().getValue() != null) {
					for (String remark : typedSource.getRemarks().getValue().getString()) {
						remarks.add(remark);
					}
				}
				typedDestination.setRemarks(remarks);
				
				List<ArcoQueueData> queues = new ArrayList<ArcoQueueData>();
				if (typedSource.getQueues() != null && typedSource.getQueues().getValue() != null) {
					for (Object queue : typedSource.getQueues().getValue().getAnyType()) {
						queues.add(
								arcoQueueToArcoQueueDataConverter.convert(
										(ArcoQueue) queue));
					}
				}
				typedDestination.setQueues(queues);
				
			} else if (source instanceof ComunicationPayPal) {
				ComunicationPayPal typedSource = (ComunicationPayPal) source;
				PaymentComunicationPayPalData typedDestination = new PaymentComunicationPayPalData();
				destination = typedDestination;	
				
				if (typedSource.getCancelUrl() != null) {
					typedDestination.setCancelUrl(
							typedSource.getCancelUrl().getValue());
				}
				
				typedDestination.setShippingEnabled(
						typedSource.isShippingEnabled());
				
			} else if (source instanceof ComunicationPosteID) {
				ComunicationPosteID typedSource = (ComunicationPosteID) source;
				PaymentComunicationPosteIDData typedDestination = new PaymentComunicationPosteIDData();
				destination = typedDestination;	
				
				if (typedSource.getErrorUrl() != null) {
					typedDestination.setErrorUrl(
							typedSource.getErrorUrl().getValue());
				}
				
			} else if (source instanceof ComunicationUnicredit) {
				ComunicationUnicredit typedSource = (ComunicationUnicredit) source;
				PaymentComunicationUnicreditData typedDestination = new PaymentComunicationUnicreditData();
				destination = typedDestination;	
				
				if (typedSource.getErrorUrl() != null) {
					typedDestination.setErrorUrl(
							typedSource.getErrorUrl().getValue());
				}
				
			} else if (source instanceof AComunicationInfo) {
				AComunicationInfo typedSource = (AComunicationInfo) source;
				PaymentComunicationInfoData typedDestination = new PaymentComunicationInfoData();
				destination = typedDestination;
				
				if (typedSource.getErrorUrl() != null) {
					typedDestination.setErrorUrl(
							typedSource.getErrorUrl().getValue());
				}
				
			} else if (source instanceof ComunicationSTS) {
				ComunicationSTS typedSource = (ComunicationSTS) source;
				PaymentComunicationSTSData typedDestination = new PaymentComunicationSTSData();
				destination = typedDestination;	
				
				if (typedSource.getAcceptHeader() != null) {
					typedDestination.setAcceptHeader(
							typedSource.getAcceptHeader().getValue());
				}
				
				if (typedSource.getIPAddress() != null){
					typedDestination.setIpAddress(
							typedSource.getIPAddress().getValue());
				}
				
				if (typedSource.getLanguageCode() != null){
					typedDestination.setLanguageCode(
							typedSource.getLanguageCode().getValue());
				}
				
				typedDestination.setShippingEnabled(typedSource.isShippingEnabled());
				
			} else {
				destination = new PaymentComunicationBaseData();
				
			}
			
			if (source.getDescription() != null) {
				destination.setDescription(
					source.getDescription().getValue());
			}
			
			if (source.getLanguageCode() != null) {
				destination.setLanguageCode(
					source.getLanguageCode().getValue());
			}
			
			if (source.getReturnUrl() != null) {
				destination.setReturnUrl(
					source.getReturnUrl().getValue());
			}
			
		}
				
		return destination;
	}

}
