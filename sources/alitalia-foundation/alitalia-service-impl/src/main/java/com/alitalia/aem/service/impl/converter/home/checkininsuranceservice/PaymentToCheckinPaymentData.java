package com.alitalia.aem.service.impl.converter.home.checkininsuranceservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinPaymentData;
import com.alitalia.aem.common.data.home.enumerations.CheckinAncillaryServiceEnum;
import com.alitalia.aem.common.data.home.enumerations.CheckinSaleChannelEnum;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd2.Payment;


@Component(immediate=true, metatype=false)
@Service(value=PaymentToCheckinPaymentData.class)
public class PaymentToCheckinPaymentData 
		implements Converter<Payment, CheckinPaymentData> {
	
	@Reference
	private CreditCardToCheckinCreditCardData creditCardDataConverter;

	@Reference
	private VerifiedByVisaToCheckinVerifiedByVisaData verifiedByVisaConverter;
	
	@Reference
	private PaymentTransactionInfoToCheckinPaymentTransactionInfoData paymentTransactionInfoDataConverter;
	
	@Override
	public CheckinPaymentData convert(Payment source) {
		
		CheckinPaymentData destination = null;
		
		if (source != null) {
			
			destination = new CheckinPaymentData();
			
			destination.setAmount(source.getX003CAmountX003EKBackingField());
			destination.setApplicationSpecificOrderID(source.getX003CApplicationSpecificOrderIDX003EKBackingField());
			destination.setAuthUserFrequentFlyerCode(source.getX003CAuthUserFrequentFlyerCodeX003EKBackingField());
			destination.setCartID(source.getX003CCartIDX003EKBackingField());
			destination.setCreditCardData(creditCardDataConverter.convert(source.getX003CCreditCardDataX003EKBackingField()));
			destination.setCurrency(source.getX003CCurrencyX003EKBackingField());
			destination.setDescription(source.getX003CDescriptionX003EKBackingField());
			destination.setEmail(source.getX003CEmailX003EKBackingField());
			destination.setId(source.getX003CIdX003EKBackingField());
			destination.setIdAuthEvent(source.getX003CIDAuthEventX003EKBackingField());
			destination.setMethodId(source.getX003CMethodIdX003EKBackingField());
			destination.setOrderCode(source.getX003COrderCodeX003EKBackingField());
			destination.setPersistenceId(source.getX003CPersistenceIdX003EKBackingField());
			if (source.getX003CSaleChannelX003EKBackingField() != null)
				destination.setSaleChannel(CheckinSaleChannelEnum.fromValue(source.getX003CSaleChannelX003EKBackingField().value()));
			if (source.getX003CSoldGoodX003EKBackingField() != null)
				destination.setSoldGood(CheckinAncillaryServiceEnum.fromValue(source.getX003CSoldGoodX003EKBackingField().value()));
			destination.setVbvParam(source.getX003CVBVParamX003EKBackingField());
			destination.setVerifiedByVisa(verifiedByVisaConverter.convert(source.getX003CVerifiedByVisaX003EKBackingField()));
			destination.setPaymentAttemptCount(source.getX003CPaymentAttemptCountX003EKBackingField());
			if (source.getX003CTransactionDateX003EKBackingField() != null)
				destination.setTransactionDate(source.getX003CTransactionDateX003EKBackingField().toGregorianCalendar());
			destination.setTransactionInfo(paymentTransactionInfoDataConverter.convert(source.getX003CTransactionInfoX003EKBackingField()));
		}
		
		return destination;
	}

}
