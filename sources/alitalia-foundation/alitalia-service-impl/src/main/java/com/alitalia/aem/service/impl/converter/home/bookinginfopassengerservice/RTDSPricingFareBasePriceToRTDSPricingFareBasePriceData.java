package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareBasePriceData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionPricingFareBasePrice;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareBasePriceToRTDSPricingFareBasePriceData.class)
public class RTDSPricingFareBasePriceToRTDSPricingFareBasePriceData implements
		Converter<ResultTicketingDetailSolutionPricingFareBasePrice, 
					ResultTicketingDetailSolutionPricingFareBasePriceData> {

	@Override
	public ResultTicketingDetailSolutionPricingFareBasePriceData convert(
			ResultTicketingDetailSolutionPricingFareBasePrice source) {
		ResultTicketingDetailSolutionPricingFareBasePriceData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingFareBasePriceData();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
