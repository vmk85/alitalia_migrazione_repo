package com.alitalia.aem.service.impl.converter.home.bookingwidget;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.UpdateWidgetCashMilesRequest;
import com.alitalia.aem.ws.booking.widgetservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.booking.widgetservice.xsd1.OperationType;
import com.alitalia.aem.ws.booking.widgetservice.xsd1.UpdateCashAndMilesRequest;
import com.alitalia.aem.ws.booking.widgetservice.xsd2.WidgetType;

@Component(immediate=true, metatype=false)
@Service(value=UpdateWidgetCashMilesRequestToUpdateCashAndMilesRequest.class)
public class UpdateWidgetCashMilesRequestToUpdateCashAndMilesRequest implements
		Converter<UpdateWidgetCashMilesRequest, UpdateCashAndMilesRequest> {

	@Reference
	private CashAndMilesDataToCashAndMiles cashAndMilesDataConverter;

	@Override
	public UpdateCashAndMilesRequest convert(UpdateWidgetCashMilesRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		UpdateCashAndMilesRequest destination = objectFactory.createUpdateCashAndMilesRequest();

		destination.setPNR(objectFactory.createUpdateCashAndMilesRequestPNR(source.getPnr()));
		destination.setOperationType(OperationType.fromValue(source.getOperationType().value()));
		destination.setSite(objectFactory.createUpdateWidgetRequestSite(source.getSite()));
		destination.setType(WidgetType.CASH_AND_MILES);

		if (source.getCashMiles() != null)
			destination.setCashAndMiles(
					objectFactory.createUpdateCashAndMilesRequestCashAndMiles(
							cashAndMilesDataConverter.convert(source.getCashMiles())));
		else
			destination.setCashAndMiles(null);

		return destination;
	}

}
