package com.alitalia.aem.service.impl.converter.home.checkincomfortseatservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.enumerations.LegTypeEnum;
import com.alitalia.aem.common.messages.home.ComfortSeatPriceCheckinResponse;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.ComfortSeatPriceResponse;


@Component(immediate=true, metatype=false)
@Service(value=ComfortSeatPriceResponseToComfortSeatPriceCheckinResponse.class)
public class ComfortSeatPriceResponseToComfortSeatPriceCheckinResponse implements Converter<ComfortSeatPriceResponse, ComfortSeatPriceCheckinResponse> {


	@Override
	public ComfortSeatPriceCheckinResponse convert(ComfortSeatPriceResponse source) {
		ComfortSeatPriceCheckinResponse destination = null;

		if (source != null) {
			destination = new ComfortSeatPriceCheckinResponse();

			destination.setIsComfortSeatFlight(source.isIsComfortSeatFlight());
			if(source.getLegType() != null){
				destination.setLegType(LegTypeEnum.fromValue(source.getLegType().value()));
			}
			destination.setPrice(source.getPrice());
		}

		return destination;
	}

}
