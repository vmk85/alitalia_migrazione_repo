package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.SeatPreferencesData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.ObjectFactory;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.SeatPreference;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd5.Cabin;

@Component(immediate=true, metatype=false)
@Service(value=SeatPreferencesDataToSeatPreference.class)
public class SeatPreferencesDataToSeatPreference 
		implements Converter<SeatPreferencesData, SeatPreference> {
	
	@Override
	public SeatPreference convert(SeatPreferencesData source) {
		
		SeatPreference destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			destination = objectFactory.createSeatPreference();
			
			if (source.getCabinSeat() != null) {
				destination.setCabinSeat(
						Cabin.valueOf(
								source.getCabinSeat().value()));
			}
			
			destination.setFlightCarrier(
					objectFactory.createSeatPreferenceFlightCarrier(
							source.getFlightCarrier()));
			
			destination.setFlightNumber(
					objectFactory.createSeatPreferenceFlightNumber(
							source.getFlightNumber()));
			
			destination.setNumber(
					objectFactory.createSeatPreferenceNumber(
							source.getNumber()));
			
			destination.setRow(
					objectFactory.createSeatPreferenceRow(
							source.getRow()));
			
		}
		
		return destination;
	}

}
