package com.alitalia.aem.service.impl.converter.home.carnetpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.carnet.CarnetProcessInfoData;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.ObjectFactory;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.PaymentStep;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.ProcessInfo;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.ResultType;


@Component(immediate=true, metatype=false)
@Service(value=CarnetProcessInfoDataToProcessInfo.class)
public class CarnetProcessInfoDataToProcessInfo 
		implements Converter<CarnetProcessInfoData, ProcessInfo> {
	
	@Override
	public ProcessInfo convert(CarnetProcessInfoData source) {
		
		ProcessInfo destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			destination = objectFactory.createProcessInfo();
			
			destination.setMarketCode(
					objectFactory.createProcessInfoMarketCode(
							source.getMarketCode()));
			
			destination.setPaymentAttemps(
					source.getPaymentAttemps());
			
			destination.setPointOfSale(
					objectFactory.createProcessInfoPointOfSale(
							source.getPointOfSale()));
			
			destination.setRedirectUrl(
					objectFactory.createProcessInfoRedirectUrl(
							source.getRedirectUrl()));
			

			
			destination.setSessionId(
					objectFactory.createProcessInfoSessionId(
							source.getSessionId()));
			
			destination.setShopId(
					objectFactory.createProcessInfoShopId(
							source.getShopId()));
			
			destination.setSiteCode(
					objectFactory.createProcessInfoSiteCode(
							source.getSiteCode()));
			
			if (source.getStep() != null) {
				destination.setStep(
						PaymentStep.fromValue(source.getStep().value()));
			}
			
			destination.setTokenId(
					objectFactory.createProcessInfoTokenId(
							source.getTokenId()));
			
			destination.setTransactionId(
					objectFactory.createProcessInfoTransactionId(
							source.getTransactionId()));
			
			if (source.getResult() != null) {
				destination.setResult(
						ResultType.fromValue(source.getResult().value()));
			}
		}
		
		return destination;
	}

}
