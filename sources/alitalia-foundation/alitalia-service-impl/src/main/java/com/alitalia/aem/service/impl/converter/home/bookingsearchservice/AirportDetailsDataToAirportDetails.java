package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AirportDetailsData;
import com.alitalia.aem.ws.booking.searchservice.xsd3.AirportDetails;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=AirportDetailsDataToAirportDetails.class)
public class AirportDetailsDataToAirportDetails implements Converter<AirportDetailsData, AirportDetails> {

	@Override
	public AirportDetails convert(AirportDetailsData source) {
		ObjectFactory factory = new ObjectFactory();
		AirportDetails destination = factory.createAirportDetails();

		destination.setAZTerminal(factory.createAirportDetailsAZTerminal(source.getAzTerminal()));
		destination.setCheckInTimeDomestic(source.getCheckInTimeDomestic());
		destination.setCheckInTimeIntercontinental(source.getCheckInTimeIntercontinental());
		destination.setCheckInTimeInternational(source.getCheckInTimeInternational());
		destination.setCityDistance(source.getCityDistance());
		destination.setCityTransferTime(source.getCityTransferTime());
		destination.setGMT(source.getFusoOrarioGMT());
		destination.setNotesCode(source.getNotesCode());

		return destination;
	}

}
