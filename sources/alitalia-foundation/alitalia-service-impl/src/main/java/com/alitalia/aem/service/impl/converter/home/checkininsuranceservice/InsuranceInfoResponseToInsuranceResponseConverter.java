package com.alitalia.aem.service.impl.converter.home.checkininsuranceservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.CheckinInsuranceResponse;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd1.InsuranceInfoResponse;


@Component(immediate=true, metatype=false)
@Service(value=InsuranceInfoResponseToInsuranceResponseConverter.class)
public class InsuranceInfoResponseToInsuranceResponseConverter implements Converter<InsuranceInfoResponse, CheckinInsuranceResponse> {

	@Reference
	private InsurancePolicyToInsurancePolicyData checkinInsurancePolicyConverter;
	
	@Override
	public CheckinInsuranceResponse convert(InsuranceInfoResponse source) {
		
		CheckinInsuranceResponse response = new CheckinInsuranceResponse();
		response.setPolicy(checkinInsurancePolicyConverter.convert(source.getInsurancePolicy().getValue()));
		return response;			
	}
}