package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.booking.paymentservice.xsd2.AdultPassenger;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ApplicantPassenger;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.CashAndMiles;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ChildPassenger;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ECoupon;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.InfantPassenger;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.InsurancePolicy;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.PaymentOfanyTypeanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ProcessInfo;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.Route;

@Component(immediate=true, metatype=false)
@Service(value=BookingPaymentServiceJAXBContextFactory.class)
public class BookingPaymentServiceJAXBContextFactory {

	private static final Logger logger = LoggerFactory.getLogger(BookingPaymentServiceJAXBContextFactory.class);

	private JAXBContext jaxbContext = null;

	public synchronized JAXBContext getJaxbContext() throws JAXBException {

		if (jaxbContext == null) {
			jaxbContext = JAXBContext.newInstance(PaymentOfanyTypeanyType.class,
													InsurancePolicy.class,
													ProcessInfo.class,
													ApplicantPassenger.class,
													AdultPassenger.class,
													ChildPassenger.class,
													InfantPassenger.class,
													CashAndMiles.class,
													ECoupon.class,
													Route.class,
													com.alitalia.aem.ws.booking.paymentservice.xsd11.ResultBookingDetails.class,
													com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetail.class
								);
		}

		return jaxbContext;
	}

	@Activate
	private void activate() {
		try {
			jaxbContext = JAXBContext.newInstance(PaymentOfanyTypeanyType.class,
													InsurancePolicy.class,
													ProcessInfo.class,
													ApplicantPassenger.class,
													AdultPassenger.class,
													ChildPassenger.class,
													InfantPassenger.class,
													CashAndMiles.class,
													ECoupon.class,
													Route.class,
													com.alitalia.aem.ws.booking.paymentservice.xsd11.ResultBookingDetails.class,
													com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetail.class
					);
		} catch (JAXBException e) {
			logger.warn("BookingPaymentServiceJAXBContextFactory - cannot instantiate JAXBContext: {}", e.getMessage());
		}
	}
}
