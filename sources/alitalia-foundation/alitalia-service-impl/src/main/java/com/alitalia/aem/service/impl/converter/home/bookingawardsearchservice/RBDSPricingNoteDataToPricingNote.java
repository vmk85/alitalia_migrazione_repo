package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingNoteData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetailsSolutionPricingNote;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingNoteDataToPricingNote.class)
public class RBDSPricingNoteDataToPricingNote implements Converter<ResultBookingDetailsSolutionPricingNoteData, ResultBookingDetailsSolutionPricingNote> {

	@Override
	public ResultBookingDetailsSolutionPricingNote convert(
			ResultBookingDetailsSolutionPricingNoteData source) {
		ResultBookingDetailsSolutionPricingNote destination = null;
		if(source != null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionPricingNote();
			destination.setIdField(source.getIdField());
			destination.setNameField(source.getNameField());
			destination.setValueField(source.getValueField());
		}
		return destination;
	}

}
