package com.alitalia.aem.service.impl.converter.home.checkinextrabaggageservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinBaggagePolicyData;
import com.alitalia.aem.ws.checkin.extrabaggageservice.xsd2.BaggagePolicy;


@Component(immediate=true, metatype=false)
@Service(value=BaggagePolicyToCheckinBaggagePolicyData.class)
public class BaggagePolicyToCheckinBaggagePolicyData implements Converter<BaggagePolicy, CheckinBaggagePolicyData> {

	@Override
	public CheckinBaggagePolicyData convert(BaggagePolicy source) {
		CheckinBaggagePolicyData destination = null;

		if (source != null) {
			destination = new CheckinBaggagePolicyData();

			destination.setId(source.getX003CIdX003EKBackingField());
			destination.setAirportFareForFirstBaggage(source.getX003CAirportFareForFirstBaggageX003EKBackingField());
			destination.setAirportFareForSpecialPassenger(source.getX003CAirportFareForSpecialPassengerX003EKBackingField());
			destination.setAirportFare(source.getX003CAirportFareX003EKBackingField());
			destination.setAllowanceQuantity(source.getX003CAllowanceQuantityX003EKBackingField());
			destination.setAllowanceWeight(source.getX003CAllowanceWeightX003EKBackingField());
			destination.setCurrency(source.getX003CCurrencyX003EKBackingField());
			destination.setEnableForFirstBaggage(source.isX003CEnableForFirstBaggageX003EKBackingField());
			destination.setExtraBaggageMaxQuantity(source.getX003CExtraBaggageMaxQuantityX003EKBackingField());
			destination.setExtraBaggageWeight(source.getX003CExtraBaggageWeightX003EKBackingField());
			destination.setWebFareForFirstBaggage(source.getX003CWebFareForFirstBaggageX003EKBackingField());
			destination.setWebFareForSpecialPassenger(source.getX003CWebFareForSpecialPassengerX003EKBackingField());
			destination.setWebFare(source.getX003CWebFareX003EKBackingField());		
		}

		return destination;
	}

}
