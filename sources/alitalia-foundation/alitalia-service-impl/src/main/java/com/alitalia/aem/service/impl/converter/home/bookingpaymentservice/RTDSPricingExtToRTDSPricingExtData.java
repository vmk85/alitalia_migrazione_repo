package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingExtData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingExt;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingExtToRTDSPricingExtData.class)
public class RTDSPricingExtToRTDSPricingExtData implements
		Converter<ResultTicketingDetailSolutionPricingExt, ResultTicketingDetailSolutionPricingExtData> {

	@Override
	public ResultTicketingDetailSolutionPricingExtData convert(
			ResultTicketingDetailSolutionPricingExt source) {
		ResultTicketingDetailSolutionPricingExtData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingExtData();

			destination.setPrivateFareField(source.isPrivateFareField());
			destination.setPrivateFareFieldSpecified(source.isPrivateFareFieldSpecified());
		}

		return destination;
	}

}
