package com.alitalia.aem.service.impl.converter.home.bookingwidget;


public enum MMCreditCardEnum {
	
	AmericanExpress("AX"),
	MasterCard("MC"),
	Visa("VS"),
	Diners("DC");
    
    private final String value;

    MMCreditCardEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MMCreditCardEnum fromValue(String v) {
        for (MMCreditCardEnum c: MMCreditCardEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
