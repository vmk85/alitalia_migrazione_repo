package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MealData;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.Meal;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=MealDataToMeal.class)
public class MealDataToMeal 
		implements Converter<MealData, Meal> {
	
	@Override
	public Meal convert(MealData source) {
		
		Meal destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			destination = objectFactory.createMeal();
			
			destination.setCode(
					objectFactory.createMealCode(
							source.getCode()));
			
			destination.setDescription(
					objectFactory.createMealDescription(
							source.getDescription()));
			
		}
		
		return destination;
	}

}
