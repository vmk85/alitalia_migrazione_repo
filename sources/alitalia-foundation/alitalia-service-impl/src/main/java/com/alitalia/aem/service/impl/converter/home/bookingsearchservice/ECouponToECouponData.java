package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ECouponData;
import com.alitalia.aem.common.data.home.enumerations.DiscountTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.FareTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ECoupon;
import com.alitalia.aem.ws.booking.searchservice.xsd3.PassengerType;

@Component(immediate=true, metatype=false)
@Service(value=ECouponToECouponData.class)
public class ECouponToECouponData 
		implements Converter<ECoupon, ECouponData> {
	
	@Override
	public ECouponData convert(ECoupon source) {
		
		ECouponData destination = null;
		
		if (source != null) {
			
			destination = new ECouponData();
			
			destination.setAmount(
					source.getAmount());
			
			destination.setAmountBus(
					source.getAmountBus().getValue());
			
			if (source.getAmountBus() != null) {
				destination.setAmountBus(
						source.getAmountBus().getValue());
			}
			
			if (source.getAmountEco() != null) {
				destination.setAmountEco(
						source.getAmountEco().getValue());
			}
			
			if (source.getAmountEcoPlus() != null) {
				destination.setAmountEcoPlus(
						source.getAmountEcoPlus().getValue());
			}
			
			if (source.getAnyPaxMAXNumPax() != null) {
				destination.setAnyPaxMAXNumPax(
						source.getAnyPaxMAXNumPax().getValue());
			}
			
			if (source.getAnyPaxMinNumPax() != null) {
				destination.setAnyPaxMinNumPax(
						source.getAnyPaxMinNumPax().getValue());
			}
			
			if (source.getCode() != null) {
				destination.setCode(
						source.getCode().getValue());
			}
			
			destination.setDatBlkFrm(
					XsdConvertUtils.parseCalendar(
							source.getDatBlkFrm()));
			
			destination.setDatBlkFrmDue(
					XsdConvertUtils.parseCalendar(
							source.getDatBlkFrmDue()));
			
			destination.setDatBlkFrmTre(
					XsdConvertUtils.parseCalendar(
							source.getDatBlkFrmTre()));
			
			destination.setDatBlkTo(
					XsdConvertUtils.parseCalendar(
							source.getDatBlkTo()));
			
			destination.setDatBlkToDue(
					XsdConvertUtils.parseCalendar(
							source.getDatBlkToDue()));

			destination.setDatBlkToTre(
					XsdConvertUtils.parseCalendar(
							source.getDatBlkToTre()));
			
			destination.setDatBlkFrm(
					XsdConvertUtils.parseCalendar(
							source.getDatBlkFrm()));
			
			destination.setDatBlkTo(
					XsdConvertUtils.parseCalendar(
							source.getDatBlkTo()));
			
			destination.setDatVolFrm(
					XsdConvertUtils.parseCalendar(
							source.getDatVolFrm()));
			
			destination.setDatVolTo(
					XsdConvertUtils.parseCalendar(
							source.getDatVolTo()));
			
			destination.setErrorCode(
					source.getErrorCode());
			
			if (source.getErrorDescription() != null) {
				destination.setErrorDescription(
						source.getErrorDescription().getValue());
			}
			
			if (source.getFamilyEcoupon() != null) {
				destination.setFamilyEcoupon(
						source.getFamilyEcoupon().getValue());
			}
			
			if (source.getFare() != null) {
				destination.setFare(
						FareTypeEnum.fromValue(source.getFare().value()));
			}
			
			destination.setSingleUse(
					source.isIsSingleUse());
			
			destination.setValid(
					source.isIsValid());
			
			if (source.getMinAmount() != null) {
				destination.setMinAmount(
						source.getMinAmount().getValue());
			}
			
			List<PassengerTypeEnum> paxTypes = new ArrayList<PassengerTypeEnum>();
			if (source.getPaxTypes() != null && source.getPaxTypes().getValue() != null) {
				for (PassengerType paxType : source.getPaxTypes().getValue().getPassengerType()) {
					paxTypes.add(
							PassengerTypeEnum.fromValue(
									paxType.value()));
				}
			}
			destination.setPaxTypes(paxTypes);
			
			if (source.getType() != null) {
				destination.setType(
						DiscountTypeEnum.fromValue(source.getType().value()));
			}

			if (source.getWeekSalesDay() != null) {
				destination.setWeekSalesDay(
						source.getWeekSalesDay().getValue());
			}
			
			if (source.getWeekTravellDay() != null) {
				destination.setWeekTravellDay(
						source.getWeekSalesDay().getValue());
			}
			
		}
		
		return destination;
	}

}
