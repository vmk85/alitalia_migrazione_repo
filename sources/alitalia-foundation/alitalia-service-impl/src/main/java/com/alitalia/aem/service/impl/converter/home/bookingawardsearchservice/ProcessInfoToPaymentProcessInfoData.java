package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PaymentProcessInfoData;
import com.alitalia.aem.common.data.home.enumerations.PaymentStepEnum;
import com.alitalia.aem.common.data.home.enumerations.ResultTypeEnum;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.ProcessInfo;

@Component(immediate=true, metatype=false)
@Service(value=ProcessInfoToPaymentProcessInfoData.class)
public class ProcessInfoToPaymentProcessInfoData 
		implements Converter<ProcessInfo, PaymentProcessInfoData> {
	
	@Override
	public PaymentProcessInfoData convert(ProcessInfo source) {
		
		PaymentProcessInfoData destination = null;
		
		if (source != null) {
			
			destination = new PaymentProcessInfoData();
			
			if (source.getMarketCode() != null) {
				destination.setMarketCode(
						source.getMarketCode().getValue());
			}
			
			if (source.getPaRes() != null) {
				destination.setPaRes(
						source.getPaRes().getValue());
			}
			
			destination.setPaymentAttemps(
					source.getPaymentAttemps());
			
			if (source.getPointOfSale() != null) {
				destination.setPointOfSale(
						source.getPointOfSale().getValue());
			}
			
			if (source.getRedirectUrl() != null) {
				destination.setRedirectUrl(
						source.getRedirectUrl().getValue());
			}
			
			if (source.getResult() != null) {
				destination.setResult(
						ResultTypeEnum.fromValue(source.getResult().value()));
			}
			
			if (source.getSessionId() != null) {
				destination.setSessionId(
						source.getSessionId().getValue());
			}
			
			if (source.getShopId() != null) {
				destination.setShopId(
						source.getShopId().getValue());
			}
			
			if (source.getSiteCode() != null) {
				destination.setSiteCode(
						source.getSiteCode().getValue());
			}
			
			if (source.getStep() != null) {
				destination.setStep(
						PaymentStepEnum.fromValue(source.getStep().value()));
			}
			
			if (source.getTokenId() != null) {
				destination.setTokenId(
						source.getTokenId().getValue());
			}
			
			if (source.getTransactionId() != null) {
				destination.setTransactionId(
						source.getTransactionId().getValue());
			}
			
		}
		
		return destination;
	}

}
