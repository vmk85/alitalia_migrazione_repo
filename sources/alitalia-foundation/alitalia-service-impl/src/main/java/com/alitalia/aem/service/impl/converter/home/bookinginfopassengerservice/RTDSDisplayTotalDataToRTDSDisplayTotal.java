package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionDisplayTotalData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionDisplayTotal;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=RTDSDisplayTotalDataToRTDSDisplayTotal.class)
public class RTDSDisplayTotalDataToRTDSDisplayTotal implements
		Converter<ResultTicketingDetailSolutionDisplayTotalData, 
					ResultTicketingDetailSolutionDisplayTotal> {

	@Override
	public ResultTicketingDetailSolutionDisplayTotal convert(ResultTicketingDetailSolutionDisplayTotalData source) {
		ResultTicketingDetailSolutionDisplayTotal destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionDisplayTotal();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
