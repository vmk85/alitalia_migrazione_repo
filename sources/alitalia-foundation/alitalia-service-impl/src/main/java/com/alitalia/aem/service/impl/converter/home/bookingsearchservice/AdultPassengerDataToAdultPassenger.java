package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.data.home.TicketInfoData;
import com.alitalia.aem.ws.booking.searchservice.xsd3.AdultPassenger;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ObjectFactory;
import com.alitalia.aem.ws.booking.searchservice.xsd3.PassengerType;
import com.alitalia.aem.ws.booking.searchservice.xsd5.ArrayOfanyType;


@Component(immediate=true, metatype=false)
@Service(value=AdultPassengerDataToAdultPassenger.class)
public class AdultPassengerDataToAdultPassenger 
		implements Converter<AdultPassengerData, AdultPassenger> {
	
	@Reference
	private PassengerInfoBaseDataToInfoBase infoBaseDataToInfoBaseConverter;
	
	@Reference
	private PreferencesDataToPreferences preferencesDataToPreferencesConverter;
	
	@Reference
	private FrequentFlyerTypeDataToFrequentFlyerType frequentFlyerTypeDataToFrequentFlyerTypeConverter;
	
	@Reference
	private TicketInfoDataToTicketInfo ticketInfoDataToTicketInfoConverter;
	
	@Override
	public AdultPassenger convert(AdultPassengerData source) {
		
		AdultPassenger destination = null;
		
		if (source != null) {
			
			ObjectFactory factory3 = new ObjectFactory();
			com.alitalia.aem.ws.booking.searchservice.xsd5.ObjectFactory factory5 = 
					new com.alitalia.aem.ws.booking.searchservice.xsd5.ObjectFactory();
			
			destination = factory3.createAdultPassenger();
			
			// AdultPassenger data
			
			destination.setFrequentFlyerCode(
					factory3.createAdultPassengerFrequentFlyerCode(
							source.getFrequentFlyerCode()));
			
			destination.setFrequentFlyerTier(
					factory3.createAdultPassengerFrequentFlyerTier(
							source.getFrequentFlyerTier()));
			
			destination.setFrequentFlyerType(
					factory3.createAdultPassengerFrequentFlyerType(
							frequentFlyerTypeDataToFrequentFlyerTypeConverter.convert(
								source.getFrequentFlyerType())));
			
			// ARegularPassenger data
			
			destination.setPreferences(
					factory3.createARegularPassengerPreferences(
							preferencesDataToPreferencesConverter.convert(
									source.getPreferences())));
			
			// APassengerBase data
			
			destination.setLastName(
					factory3.createAPassengerBaseLastName(
							source.getLastName()));
			
			destination.setName(
					factory3.createAPassengerBaseName(
							source.getName()));
			
			destination.setCouponPrice(
					source.getCouponPrice());
			
			destination.setExtraCharge(
					source.getExtraCharge());
			
			destination.setCCFee(
					source.getCcFee());
			
			destination.setFee(
					source.getFee());
			
			destination.setGrossFare(
					source.getGrossFare());
			
			destination.setNetFare(
					source.getNetFare());
			
			destination.setInfo(
					factory3.createAPassengerBaseInfo(
							infoBaseDataToInfoBaseConverter.convert(
									source.getInfo())));
			
			List<TicketInfoData> sourceTickets = source.getTickets();
			if (sourceTickets != null && !source.getTickets().isEmpty()) {
				ArrayOfanyType tickets = factory5.createArrayOfanyType();
				for (TicketInfoData ticketInfoData : sourceTickets) {
					tickets.getAnyType().add(ticketInfoDataToTicketInfoConverter.convert(ticketInfoData));
				}
				destination.setTickets(factory3.createAPassengerBaseTickets(tickets));
			}
			destination.setType(PassengerType.fromValue(source.getType().value()));
		
		}
		
		return destination;
	}

}
