package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.PassengerBase;
import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.data.home.ApplicantPassengerData;
import com.alitalia.aem.common.data.home.ChildPassengerData;
import com.alitalia.aem.common.data.home.InfantPassengerData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.ws.booking.searchservice.xsd1.ExtraChargeResponse;
import com.alitalia.aem.ws.booking.searchservice.xsd3.APassengerBase;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ArrayOfAPassengerBase;

@Component(immediate=true, metatype=false)
@Service(value=ExtraChargeResponseToPassengerList.class)
public class ExtraChargeResponseToPassengerList implements Converter<ExtraChargeResponse, List<PassengerBase>> {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Reference
	APassengerBaseToPassengerBaseData aPassengerBaseConverter;

	@Override
	public List<PassengerBase> convert(ExtraChargeResponse source) {
		List<PassengerBase> destination = new ArrayList<PassengerBase>();
		int adultCount = 0;

		if (source.getPassengerList() != null) {
			ArrayOfAPassengerBase sourcePassengerList = source.getPassengerList().getValue();
			for (APassengerBase sourcePassengereElement : sourcePassengerList.getAPassengerBase()) {
				PassengerBaseData passengerBase = aPassengerBaseConverter.convert(sourcePassengereElement);
				switch (passengerBase.getType()) {
				case ADULT:
					if (adultCount > 0) {
						AdultPassengerData castedAdultPassengerData = new AdultPassengerData(passengerBase);
						destination.add(castedAdultPassengerData);
					} else {
						ApplicantPassengerData castedApplicantPassengerData = new ApplicantPassengerData(passengerBase);
						destination.add(castedApplicantPassengerData);
					}
					adultCount++;
					break;
				case CHILD:
					ChildPassengerData castedChildPassengerData = new ChildPassengerData(passengerBase);
					destination.add(castedChildPassengerData);
					break;
				case INFANT:
					InfantPassengerData castedInfantPassengerData = new InfantPassengerData(passengerBase);
					destination.add(castedInfantPassengerData);
					break;
				case YOUTH:
					if (adultCount > 0) {
						AdultPassengerData castedAdultPassengerData = new AdultPassengerData(passengerBase);
						destination.add(castedAdultPassengerData);
					} else {
						ApplicantPassengerData castedApplicantPassengerData = new ApplicantPassengerData(passengerBase);
						destination.add(castedApplicantPassengerData);
					}
					adultCount++;
				}
			}
		}

		return destination;
	}

}
