package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoFareData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ResultBookingDetailsSolutionPricingBookingInfoFare;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingBookingInfoFareDataToPricingBookingInfoFare.class)
public class RBDSPricingBookingInfoFareDataToPricingBookingInfoFare implements
		Converter<ResultBookingDetailsSolutionPricingBookingInfoFareData, ResultBookingDetailsSolutionPricingBookingInfoFare> {

	@Override
	public ResultBookingDetailsSolutionPricingBookingInfoFare convert(
			ResultBookingDetailsSolutionPricingBookingInfoFareData source) {
		ResultBookingDetailsSolutionPricingBookingInfoFare destination = null;
		
		if(source!=null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionPricingBookingInfoFare();
			destination.setExtendedFareCodeField(source.getExtendedFareCodeField());
			destination.setTicketDesignatorField(source.getTicketDesignatorField());
		}
		return destination;
		
	}

}
