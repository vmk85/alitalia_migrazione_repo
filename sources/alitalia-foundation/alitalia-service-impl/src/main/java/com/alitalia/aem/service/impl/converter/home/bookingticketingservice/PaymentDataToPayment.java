package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PaymentData;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ObjectFactory;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.PaymentOfanyTypeanyType;

@Component(immediate = true, metatype = false)
@Service(value = PaymentDataToPayment.class)
public class PaymentDataToPayment implements Converter<PaymentData, PaymentOfanyTypeanyType> {

	@Reference
	PaymentProcessInfoDataToProcessInfo paymentProcessInfoDataToProcessInfoConverter;

	@Reference
	PaymentProviderDataToAPaymentProviderOfAnyType paymentProviderDataToAPaymentProviderOfAnyTypeConverter;

	@Override
	public PaymentOfanyTypeanyType convert(PaymentData source) {
		PaymentOfanyTypeanyType destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();

			destination = objectFactory.createPaymentOfanyTypeanyType();

			destination.setCurrency(objectFactory.createPaymentOfanyTypeanyTypeCurrency(source.getCurrency()));
			destination.setDescription(objectFactory.createPaymentOfanyTypeanyTypeDescription(source.getDescription()));
			destination.setEmail(objectFactory.createPaymentOfanyTypeanyTypeEmail(source.getEmail()));
			destination.setGrossAmount(source.getGrossAmount());
			destination.setMailSubject(objectFactory.createPaymentOfanyTypeanyTypeMailSubject(source.getMailSubject()));
			destination.setNetAmount(source.getNetAmount());

			if (source.getProcess() != null) {
				destination.setProcess(objectFactory
						.createPaymentOfanyTypeanyTypeProcess(paymentProcessInfoDataToProcessInfoConverter
								.convert(source.getProcess())));
			}

			if (source.getProvider() != null) {
				destination.setProvider(objectFactory
						.createPaymentOfanyTypeanyTypeProvider(paymentProviderDataToAPaymentProviderOfAnyTypeConverter
								.convert(source.getProvider())));
			}

			if (source.getXsltMail() != null) {
				destination.setXsltMail(objectFactory.createPaymentOfanyTypeanyTypeXsltMail(source.getXsltMail()));
			}
		}

		return destination;
	}

}
