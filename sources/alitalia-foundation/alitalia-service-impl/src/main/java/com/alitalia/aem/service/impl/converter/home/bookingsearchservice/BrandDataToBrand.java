package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.BaggageAllowanceData;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.BrandPenaltiesData;
import com.alitalia.aem.common.data.home.PropertiesData;
import com.alitalia.aem.ws.booking.searchservice.xsd3.BaggageAllowance;
import com.alitalia.aem.ws.booking.searchservice.xsd3.Brand;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ObjectFactory;
import com.alitalia.aem.ws.booking.searchservice.xsd5.ArrayOfanyType;
import com.alitalia.aem.ws.booking.searchservice.xsd5.ArrayOfstring;

@Component(immediate=true, metatype=false)
@Service(value=BrandDataToBrand.class)
public class BrandDataToBrand implements Converter<BrandData, Brand> {

	@Reference
	private BaggageAllowanceDataToBaggageAllowance baggageAllowanceDataConverter; 

	@Reference
	private BrandPenaltiesDataToBrandPenalties brandPenaltiesDataConverter;

	@Reference
	private PropertiesDataToArrayOfDictionaryItem propertiesDataConverter;
	
	@Override
	public Brand convert(BrandData source) {
		ObjectFactory factory3 = new ObjectFactory();
		com.alitalia.aem.ws.booking.searchservice.xsd5.ObjectFactory factory5 =
				new com.alitalia.aem.ws.booking.searchservice.xsd5.ObjectFactory();
		Brand destination = factory3.createBrand();

		destination.setAdvancePurchaseDays(source.getAdvancePurchaseDays());
		destination.setBestFare(source.isBestFare());
		destination.setCode(factory3.createBrandCode(source.getCode()));
		destination.setCompartimentalClass(factory3.createBrandCompartimentalClass(source.getCompartimentalClass()));
		destination.setCurrency(factory3.createBrandCurrency(source.getCurrency()));
		destination.setEnabled(source.isEnabled());
		destination.setGrossFare(source.getGrossFare());
		destination.setIndex(source.getIndex());
		destination.setNetFare(source.getNetFare());
		destination.setRealPrice(source.getRealPrice());
		destination.setRefreshSolutionId(factory3.createABoomBoxRefreshInfoRefreshSolutionId(source.getRefreshSolutionId()));
		destination.setSeatsAvailable(source.getSeatsAvailable());
		destination.setSelected(source.isSelected());
		destination.setSolutionId(factory3.createABoomBoxItemInfoSolutionId(source.getSolutionId()));
		destination.setTridionName(factory3.createBrandTridionName(source.getTridionName()));

		List<BaggageAllowanceData> sourceBaggageAllowance = source.getBaggageAllowanceList();
		ArrayOfanyType baggageAllowanceList = null;
		if (sourceBaggageAllowance != null && !sourceBaggageAllowance.isEmpty()) {
			baggageAllowanceList = factory5.createArrayOfanyType();
			for (BaggageAllowanceData baggageAllowanceData : sourceBaggageAllowance) {
				BaggageAllowance baggageAllowance = baggageAllowanceDataConverter.convert(baggageAllowanceData);
				baggageAllowanceList.getAnyType().add(baggageAllowance);
			}
		}
		destination.setBaggageAllowanceList(factory3.createBrandBaggageAllowanceList(baggageAllowanceList));

		List<String> sourceNotes = source.getNotes();
		ArrayOfstring notes = null;
		if (sourceNotes != null && !sourceNotes.isEmpty()) {
			notes = factory5.createArrayOfstring();
			for (String noteSource : sourceNotes ) 
				notes.getString().add(noteSource);
		}
		destination.setNotes(factory3.createBrandNotes(notes));

		BrandPenaltiesData penalties = source.getPenalties();
		if (penalties == null)
			destination.setPenalties(null);
		else
			destination.setPenalties(factory3.createBrandPenalties(brandPenaltiesDataConverter.convert(penalties)));

		PropertiesData sourceProperties = source.getProperties();
		if (sourceProperties != null)
			destination.setProperties(factory3.createABoomBoxGenericInfoProperties(
				propertiesDataConverter.convert(sourceProperties)));
		else
			destination.setProperties(factory3.createABoomBoxGenericInfoProperties(null));

		return destination;
	}
}