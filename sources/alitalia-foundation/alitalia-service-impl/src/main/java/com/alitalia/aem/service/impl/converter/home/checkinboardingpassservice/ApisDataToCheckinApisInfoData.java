package com.alitalia.aem.service.impl.converter.home.checkinboardingpassservice;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinApisInfoData;
import com.alitalia.aem.common.data.home.enumerations.DocumentTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PassportTypeEnum;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd2.ApisData;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd2.DocumentType;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd2.PassportType;


@Component(immediate=true, metatype=false)
@Service(value=ApisDataToCheckinApisInfoData.class)
public class ApisDataToCheckinApisInfoData implements Converter<ApisData, CheckinApisInfoData> {

	@Override
	public CheckinApisInfoData convert(ApisData source) {
		CheckinApisInfoData destination = null;

		if (source != null) {
			destination = new CheckinApisInfoData();

			XMLGregorianCalendar sourceBirthDate = source.getX003CBirthDateX003EKBackingField();
			if (sourceBirthDate != null)
				destination.setBirthDate(sourceBirthDate.toGregorianCalendar());

			destination.setDestinationAddress(source.getX003CDestinationAddressX003EKBackingField());
			destination.setDestinationCity(source.getX003CDestinationCityX003EKBackingField());
			destination.setDestinationState(source.getX003CDestinationStateX003EKBackingField());
			destination.setDestinationZipCode(source.getX003CDestinationZipCodeX003EKBackingField());

			XMLGregorianCalendar sourceExtraDocumentExpirationDate = source.getX003CExtraDocumentExpirationDateX003EKBackingField();
			if (sourceExtraDocumentExpirationDate != null)
				destination.setExtraDocumentExpirationDate(sourceExtraDocumentExpirationDate.toGregorianCalendar());

			destination.setExtraDocumentIssuingCountry(source.getX003CExtraDocumentIssuingCountryX003EKBackingField());
			destination.setExtraDocumentIssuingState(source.getX003CExtraDocumentIssuingStateX003EKBackingField());
			destination.setExtraDocumentNumber(source.getX003CExtraDocumentNumberX003EKBackingField());

			DocumentType sourceExtraDocumentType = source.getX003CExtraDocumentTypeX003EKBackingField();
			if (sourceExtraDocumentType != null)
				destination.setExtraDocumentType(DocumentTypeEnum.fromValue(sourceExtraDocumentType.value()));
			else
				destination.setExtraDocumentType(DocumentTypeEnum.NONE);

			destination.setGender(source.getX003CGenderX003EKBackingField());
			destination.setLastname(source.getX003CLastnameX003EKBackingField());
			destination.setName(source.getX003CNameX003EKBackingField());
			destination.setNationality(source.getX003CNationalityX003EKBackingField());

			XMLGregorianCalendar sourcePassportExpirationDate = source.getX003CPassportExpirationDateX003EKBackingField();
			if (sourcePassportExpirationDate != null)
				destination.setPassportExpirationDate(sourcePassportExpirationDate.toGregorianCalendar());

			destination.setPassportNumber(source.getX003CPassportNumberX003EKBackingField());
			destination.setResidenceCountry(source.getX003CResidenceCountryX003EKBackingField());
			destination.setSecondName(source.getX003CSecondNameX003EKBackingField());
			
			PassportType sourcePassportType = source.getX003CPassportTypeX003EKBackingField();
			if (sourcePassportType != null)
				destination.setPassportType(PassportTypeEnum.fromValue(sourcePassportType.value()));
		}

		return destination;
	}

}
