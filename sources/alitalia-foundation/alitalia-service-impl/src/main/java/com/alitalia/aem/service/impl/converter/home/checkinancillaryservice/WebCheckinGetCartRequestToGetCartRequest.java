package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.WebCheckinGetCartRequest;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.GetCartRequest;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.Resource;



@Component(immediate=true, metatype=false)
@Service(value=WebCheckinGetCartRequestToGetCartRequest.class)
public class WebCheckinGetCartRequestToGetCartRequest implements Converter<WebCheckinGetCartRequest, GetCartRequest> {

	@Reference
	private MmbRequestBaseInfoDataToRequestBaseInfo mmbRequestBaseInfoDataConverter;

	@Override
	public GetCartRequest convert(WebCheckinGetCartRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		GetCartRequest destination = null;

		if (source != null) {
			destination = objectFactory.createGetCartRequest();

			destination.setCartId(source.getCartId());
			destination.setClient(objectFactory.createGetCartRequestClient(source.getClient()));
			destination.setMachineName(objectFactory.createAddToCartRequestMachineName(source.getMachineName()));
			destination.setSessionId(objectFactory.createAddToCartRequestSessionId(source.getSessionId()));

			destination.setX003CResourceX003EKBackingField(Resource.WEB);
			destination.setX003CInfoRequestX003EKBackingField(
					mmbRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
		}

		return destination;
	}

}
