package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PassengerApisInfoData;
import com.alitalia.aem.common.data.home.PassengerApisSecureFlightInfoData;
import com.alitalia.aem.common.data.home.PassengerBaseInfoData;
import com.alitalia.aem.common.data.home.PassengerSecureFlightInfoData;
import com.alitalia.aem.common.data.home.enumerations.GenderTypeEnum;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ApisInfo;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ApisSecureFlightInfo;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.InfoBase;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.SecureFlightInfo;

@Component(immediate=true, metatype=false)
@Service(value=InfoBaseToPassengerInfoBaseData.class)
public class InfoBaseToPassengerInfoBaseData 
		implements Converter<InfoBase, PassengerBaseInfoData> {
	
	@Override
	public PassengerBaseInfoData convert(InfoBase source) {
		
		PassengerBaseInfoData destination = null;
			
		if (source != null) {
			
			if (source instanceof ApisSecureFlightInfo) {
				
				destination = new PassengerApisSecureFlightInfoData();
				
				if (((ApisSecureFlightInfo) source).getPassportNumber() != null) {
					((PassengerApisSecureFlightInfoData) destination).setPassportNumber(
							((ApisSecureFlightInfo) source).getPassportNumber().getValue());
				}
				
				if (((ApisSecureFlightInfo) source).getNationality() != null) {
					((PassengerApisSecureFlightInfoData) destination).setNationality(
							((ApisSecureFlightInfo) source).getNationality().getValue());
				}
						
				if (((ApisSecureFlightInfo) source).getSecondName() != null) {
					((PassengerApisSecureFlightInfoData) destination).setSecondName(
							((ApisSecureFlightInfo) source).getSecondName().getValue());
				}
				
			} else if (source instanceof ApisInfo) {
				
				destination = new PassengerApisInfoData();
				
				if (((ApisInfo) source).getPassportNumber() != null) {
					((PassengerApisInfoData) destination).setPassportNumber(
							((ApisInfo) source).getPassportNumber().getValue());
				}
				
				if (((ApisInfo) source).getNationality() != null) {
					((PassengerApisInfoData) destination).setNationality(
							((ApisInfo) source).getNationality().getValue());
				}
				
			} else if (source instanceof SecureFlightInfo) {
				
				destination = new PassengerSecureFlightInfoData();
				
				if (((SecureFlightInfo) source).getSecondName() != null) {
					((PassengerSecureFlightInfoData) destination).setSecondName(
							((SecureFlightInfo) source).getSecondName().getValue());
				}
				
			} else {
				
				destination = new PassengerBaseInfoData();
				
			}
			
			destination.setBirthDate(
					XsdConvertUtils.parseCalendar(
							source.getBirthDate()));
			
			if (source.getGender() != null) {
				destination.setGender(
						GenderTypeEnum.fromValue(
								source.getGender().value()));
			}
		
		}
		
		return destination;
	}

}
