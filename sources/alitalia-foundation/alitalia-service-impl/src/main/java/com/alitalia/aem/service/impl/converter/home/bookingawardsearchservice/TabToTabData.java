package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.FlightTabData;
import com.alitalia.aem.common.data.home.TabData;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.FlightTab;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.Tab;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd6.ArrayOfanyType;

@Component(immediate=true, metatype=false)
@Service(value=TabToTabData.class)
public class TabToTabData implements Converter<Tab, TabData> {

	@Reference
	private FlightTabToFlightTabData flightTabConverter;

	@Override
	public TabData convert(Tab source) {
		TabData destination = new TabData();

		JAXBElement<ArrayOfanyType> sourceFlightTabList = source.getFlightTabs();
		if (sourceFlightTabList != null)
		{
			for(Object sourceFlightTab : sourceFlightTabList.getValue().getAnyType()) {
				FlightTab castedFlightTab = (FlightTab) sourceFlightTab;
				FlightTabData flightTabData = flightTabConverter.convert(castedFlightTab);
				destination.addFlightTab(flightTabData);
			}
		}
		destination.setType(RouteTypeEnum.fromValue(source.getType().value()));

		return destination;
	}

}
