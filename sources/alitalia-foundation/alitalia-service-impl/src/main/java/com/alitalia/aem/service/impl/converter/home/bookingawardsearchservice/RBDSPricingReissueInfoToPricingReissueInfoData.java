//2
package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingReissueInfoData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetailsSolutionPricingReissueInfo;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingReissueInfoToPricingReissueInfoData.class)
public class RBDSPricingReissueInfoToPricingReissueInfoData implements
		Converter<ResultBookingDetailsSolutionPricingReissueInfo, ResultBookingDetailsSolutionPricingReissueInfoData> {

	@Override
	public ResultBookingDetailsSolutionPricingReissueInfoData convert(
			ResultBookingDetailsSolutionPricingReissueInfo source) {
		ResultBookingDetailsSolutionPricingReissueInfoData destination = null;
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingReissueInfoData();
			destination.setReissueActionField(source.getReissueActionField());
		}
		return destination;
	}

	public ResultBookingDetailsSolutionPricingReissueInfoData convert(
			com.alitalia.aem.ws.bookingaward.searchservice.xsd13.ResultBookingDetailsSolutionPricingReissueInfo source) {
		ResultBookingDetailsSolutionPricingReissueInfoData destination = null;
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingReissueInfoData();
			destination.setReissueActionField(source.getReissueActionField());
		}
		return destination;
	}
}
