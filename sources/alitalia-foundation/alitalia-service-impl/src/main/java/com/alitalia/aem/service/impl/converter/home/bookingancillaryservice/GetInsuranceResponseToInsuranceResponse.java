package com.alitalia.aem.service.impl.converter.home.bookingancillaryservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.InsuranceResponse;
import com.alitalia.aem.ws.booking.ancillaryservice.xsd1.GetInsuranceResponse;
import com.alitalia.aem.ws.booking.ancillaryservice.xsd2.InsurancePolicy;

@Component(immediate=true, metatype=false)
@Service(value=GetInsuranceResponseToInsuranceResponse.class)
public class GetInsuranceResponseToInsuranceResponse implements Converter<GetInsuranceResponse, InsuranceResponse> {

	@Reference
	private InsurancePolicyToInsurancePolicyData insurancePolicyConverter;

	@Override
	public InsuranceResponse convert(GetInsuranceResponse source) {
		InsuranceResponse destination = new InsuranceResponse();

		JAXBElement<InsurancePolicy> insurancePolicy = source.getInsurancePolicy();
		if (insurancePolicy != null && insurancePolicy.getValue() != null)
			destination.setPolicy(insurancePolicyConverter.convert(insurancePolicy.getValue()));
		else
			destination.setPolicy(null);
		
		return destination;
	}

}
