package com.alitalia.aem.service.impl.converter.home.bookingwidget;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.TaxData;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.messages.home.LoadWidgetECouponRequest;
import com.alitalia.aem.ws.booking.widgetservice.xsd1.LoadECouponRequest;
import com.alitalia.aem.ws.booking.widgetservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.booking.widgetservice.xsd2.ArrayOfCabin;
import com.alitalia.aem.ws.booking.widgetservice.xsd2.ArrayOfTax;
import com.alitalia.aem.ws.booking.widgetservice.xsd2.Cabin;
import com.alitalia.aem.ws.booking.widgetservice.xsd2.WidgetType;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.ArrayOfAFlightBase;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.ArrayOfAPassengerBase;

@Component(immediate = true, metatype = false)
@Service(value = LoadWidgetECouponRequestToLoadECouponRequest.class)
public class LoadWidgetECouponRequestToLoadECouponRequest implements Converter<LoadWidgetECouponRequest, LoadECouponRequest> {

	@Reference
	private FlightDataToAFlightBase flightDataToAFlightBase;

	@Reference
	private PassengerBaseDataToAPassengerBase passengerDataToAPassengerBase;

	@Reference
	private TaxDataToTax taxDataToTax;

	@Override
	public LoadECouponRequest convert(LoadWidgetECouponRequest source) {

		ObjectFactory objectFactory = new ObjectFactory();

		com.alitalia.aem.ws.booking.widgetservice.xsd2.ObjectFactory xsd2ObjectFactory = 
				new com.alitalia.aem.ws.booking.widgetservice.xsd2.ObjectFactory();

		com.alitalia.aem.ws.booking.widgetservice.xsd5.ObjectFactory xsd5ObjectFactory = 
				new com.alitalia.aem.ws.booking.widgetservice.xsd5.ObjectFactory();

		LoadECouponRequest destination = objectFactory.createLoadECouponRequest();

		destination.setType(WidgetType.E_COUPON);

		destination.setSite(
				objectFactory.createLoadWidgetRequestSite(
						source.getSite()));

		
		ArrayOfCabin cabinClasses = xsd2ObjectFactory.createArrayOfCabin();
		for (CabinEnum cabin : source.getCabinClassList()) {
			cabinClasses.getCabin().add(
					Cabin.fromValue(cabin.value()));
		}
		destination.setCabinClass(
				objectFactory.createLoadECouponRequestCabinClass(
						cabinClasses));
		
		destination.setECouponCode(
				objectFactory.createLoadECouponRequestECouponCode(
						source.geteCouponCode()));

		destination.setIsRoundTrip(
				source.getIsRoundTrip());

		ArrayOfAFlightBase itineraries = xsd5ObjectFactory.createArrayOfAFlightBase();
		for (FlightData itinerary : source.getItineraries()) {
			itineraries.getAFlightBase().add(
					flightDataToAFlightBase.convert(
							itinerary));
		}
		destination.setItineraries(
				objectFactory.createLoadECouponRequestItineraries(
						itineraries));

		ArrayOfAPassengerBase passengers = xsd5ObjectFactory.createArrayOfAPassengerBase();
		for (Object passengerData : source.getPassengers()) {
			passengers.getAPassengerBase().add(
					passengerDataToAPassengerBase.convert((PassengerBaseData) passengerData));
		}
		destination.setPassengers(objectFactory.createLoadECouponRequestPassengers(passengers));
		
		destination.setSabreGateWayAuthToken(objectFactory.createLoadECouponRequestSabreGateWayAuthToken(source.getSabreGateWayAuthToken()));

		ArrayOfTax taxes = xsd2ObjectFactory.createArrayOfTax();
		if(source.getTaxes() != null){
			for (TaxData taxData : source.getTaxes()) {
				taxes.getTax().add(taxDataToTax.convert(taxData));
			}
		}
		destination.setTaxes(objectFactory.createLoadECouponRequestTaxes(taxes));
		
		destination.setCookie(objectFactory.createLoadECouponRequestCookie(source.getCookie()));
		destination.setExecution(objectFactory.createLoadECouponRequestExecution(source.getExecution()));

		return destination;
	}
}