package com.alitalia.aem.service.impl.converter.home.bookingbagsandrulesservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionAddCollectPriceData;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetailsSolutionAddCollectPrice;

@Component(immediate=true, metatype=false)
@Service(value=RBDSAddCollectPriceToAddCollectPriceData.class)
public class RBDSAddCollectPriceToAddCollectPriceData implements Converter<ResultBookingDetailsSolutionAddCollectPrice, ResultBookingDetailsSolutionAddCollectPriceData> {

	@Override
	public ResultBookingDetailsSolutionAddCollectPriceData convert(ResultBookingDetailsSolutionAddCollectPrice source) {
		ResultBookingDetailsSolutionAddCollectPriceData destination = null;
		
		if(source != null){
			destination = new ResultBookingDetailsSolutionAddCollectPriceData();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		
		return destination;
	}
	
	public ResultBookingDetailsSolutionAddCollectPriceData convert(com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetailsSolutionAddCollectPrice source) {
		ResultBookingDetailsSolutionAddCollectPriceData destination = null;
		
		if(source != null){
			destination = new ResultBookingDetailsSolutionAddCollectPriceData();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		
		return destination;
	}
	
}
