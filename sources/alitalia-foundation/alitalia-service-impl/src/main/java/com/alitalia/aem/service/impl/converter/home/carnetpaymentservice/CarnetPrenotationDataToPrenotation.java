package com.alitalia.aem.service.impl.converter.home.carnetpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.carnet.CarnetPrenotationData;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.ObjectFactory;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.Prenotation;



@Component(immediate=true, metatype=false)
@Service(value=CarnetPrenotationDataToPrenotation.class)
public class CarnetPrenotationDataToPrenotation implements Converter<CarnetPrenotationData, Prenotation> {
	
	@Reference
	private CarnetBuyerDataToBuyer buyerDataConverter;
	
	@Reference
	private CarnetDataToCarnet carnetDataConverter;
	
	@Reference
	private CarnetComunicationDataToAComunicationBase carnetComunicationDataConverter;
	
	@Reference
	private CarnetPaymentDataToPayment carnetPaymentConverter;


	@Override
	public Prenotation convert(CarnetPrenotationData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		Prenotation destination = null;

		if (source != null) {
			destination = objectFactory.createPrenotation();

			destination.setCarnetCode(objectFactory.createPrenotationCarnetCode(source.getCarnetCode()));
			destination.setPassword(objectFactory.createPrenotationPassword(source.getPassword()));
			
			destination.setBuyer(objectFactory.createPrenotationBuyer(buyerDataConverter.convert(source.getBuyer())));
			destination.setCarnet(objectFactory.createPrenotationCarnet(carnetDataConverter.convert(source.getCarnet())));

			destination.setPayment(objectFactory.createPrenotationPayment(carnetPaymentConverter.convert(source.getPayment())));
			

		}
		return destination;
	}

}
