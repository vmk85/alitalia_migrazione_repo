package com.alitalia.aem.service.impl.converter.home.checkinpassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.PassengerListCheckinRequest;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.PassengerListRequest;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.Resource;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.SearchReason;

@Component(immediate=true, metatype=false)
@Service(value=PassengerListCheckinRequestToPassengerListRequest.class)
public class PassengerListCheckinRequestToPassengerListRequest implements Converter<PassengerListCheckinRequest, PassengerListRequest> {

	@Reference
	private CheckinRouteDataToRoute checkinRouteDataConverter;
	
	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;

	@Override
	public PassengerListRequest convert(PassengerListCheckinRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		PassengerListRequest destination = null;

		if (source != null) {
			destination = objectFactory.createPassengerListRequest();
			
			destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			destination.setX003CResourceX003EKBackingField(Resource.WEB);
			
			destination.setGetFullUndoList(source.getIsFullUndoList());
			destination.setLastName(objectFactory.createPassengerListRequestLastName(source.getLastName()));
			destination.setName(objectFactory.createPassengerListRequestName(source.getName()));
			destination.setPnr(objectFactory.createPassengerListRequestPnr(source.getPnr()));
			if (source.getReason() != null)
				destination.setReason(SearchReason.fromValue((source.getReason().value())));
			destination.setSelectedRoute(objectFactory.createPassengerListRequestSelectedRoute(checkinRouteDataConverter.convert(source.getSelectedRoute())));
		}

		return destination;
	}


}
