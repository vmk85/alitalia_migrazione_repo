package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareRestrictionData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetailsSolutionPricingFareRestriction;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingFareRestrictionDataToPricingFareRestriction.class)
public class RBDSPricingFareRestrictionDataToPricingFareRestriction implements
		Converter<ResultBookingDetailsSolutionPricingFareRestrictionData, ResultBookingDetailsSolutionPricingFareRestriction> {

	@Reference
	RBDSPricingFareRestrictionEarliestReturnDataToRBDSPFREarliestReturn earliestReturnFieldConverter;
	
	@Reference
	RBDSPricingFareRestrictionLatestReturnDataToRBDSPFRestrictionLatestReturn latestReturnFieldConverter;
	
	@Override
	public ResultBookingDetailsSolutionPricingFareRestriction convert(
			ResultBookingDetailsSolutionPricingFareRestrictionData source) {
		ResultBookingDetailsSolutionPricingFareRestriction destination = null;
		
		if(source!=null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionPricingFareRestriction();
			destination.setEarliestReturnField(earliestReturnFieldConverter.convert(source.getEarliestReturnField()));
			destination.setLatestReturnField(latestReturnFieldConverter.convert(source.getLatestReturnField()));
		}
		return destination;
	}
		
}
