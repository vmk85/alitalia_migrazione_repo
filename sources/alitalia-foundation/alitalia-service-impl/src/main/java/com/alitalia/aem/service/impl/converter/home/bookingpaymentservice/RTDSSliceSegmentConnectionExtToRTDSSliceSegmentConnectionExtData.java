package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentConnectionExtData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionSliceSegmentConnectionExt;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentConnectionExtToRTDSSliceSegmentConnectionExtData.class)
public class RTDSSliceSegmentConnectionExtToRTDSSliceSegmentConnectionExtData implements
		Converter<ResultTicketingDetailSolutionSliceSegmentConnectionExt, ResultTicketingDetailSolutionSliceSegmentConnectionExtData> {

	@Reference
	private RTDSSliceSegmentConnectionExtWarningToRTDSSliceSegmentConnectionExtWarningData warningFieldConverter;

	@Override
	public ResultTicketingDetailSolutionSliceSegmentConnectionExtData convert(ResultTicketingDetailSolutionSliceSegmentConnectionExt source) {
		ResultTicketingDetailSolutionSliceSegmentConnectionExtData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionSliceSegmentConnectionExtData();

			destination.setWarningField(warningFieldConverter.convert(source.getWarningField()));;
		}

		return destination;
	}

}
