package com.alitalia.aem.service.impl.converter.home.bookingbagsandrulesservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingNoteData;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetailsSolutionPricingNote;


@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingNoteToPricingNoteData.class)
public class RBDSPricingNoteToPricingNoteData implements Converter<ResultBookingDetailsSolutionPricingNote, ResultBookingDetailsSolutionPricingNoteData> {

	@Override
	public ResultBookingDetailsSolutionPricingNoteData convert(
			ResultBookingDetailsSolutionPricingNote source) {
		ResultBookingDetailsSolutionPricingNoteData destination = null;
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingNoteData();
			destination.setIdField(source.getIdField());
			destination.setNameField(source.getNameField());
			destination.setValueField(source.getValueField());
		}
		return destination;
	}
	
	public ResultBookingDetailsSolutionPricingNoteData convert(
			com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetailsSolutionPricingNote source) {
		ResultBookingDetailsSolutionPricingNoteData destination = null;
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingNoteData();
			destination.setIdField(source.getIdField());
			destination.setNameField(source.getNameField());
			destination.setValueField(source.getValueField());
		}
		return destination;
	}

}
