package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionAddCollectPriceData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetailsSolutionAddCollectPrice;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=RBDSAddCollectPriceDataToAddCollectPrice.class)
public class RBDSAddCollectPriceDataToAddCollectPrice implements Converter<ResultBookingDetailsSolutionAddCollectPriceData, ResultBookingDetailsSolutionAddCollectPrice> {

	@Override
	public ResultBookingDetailsSolutionAddCollectPrice convert(ResultBookingDetailsSolutionAddCollectPriceData source) {
		ResultBookingDetailsSolutionAddCollectPrice destination = null;
		
		if(source != null){
			ObjectFactory objFact = new ObjectFactory();
			destination = objFact.createResultBookingDetailsSolutionAddCollectPrice();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		
		return destination;
	}
	
}
