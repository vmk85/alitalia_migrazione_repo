package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.MmbAncillaryDetail;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryTypeEnum;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryQuotationData;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.Ancillaries;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.AncillaryStatus;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.AncillaryType;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ExtraBaggageDetail;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.MealDetail;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.Quotations;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.SeatDetail;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.UpgradeDetail;


@Component(immediate=true, metatype=false)
@Service(value=AncillariesToMmbAncillaryData.class)
public class AncillariesToMmbAncillaryData implements Converter<Ancillaries, MmbAncillaryData> {

	@Reference
	private ExtraBaggageDetailToMmbAncillaryExtraBaggageDetailData extraBaggageDetailConverter;

	@Reference
	private MealDetailToMmbAncillaryMealDetailData mealDetailConverter;

	@Reference
	private SeatDetailToMmbAncillarySeatDetailData seatDetailConverter;

	@Reference
	private QuotationsToMmbAncillaryQuotationData quotationsConverter;
	
	@Reference
	private UpgradeDetailToCheckinAncillaryUpgradeDetailData upgradeDetailConverter;

	@Override
	public MmbAncillaryData convert(Ancillaries source) {
		MmbAncillaryData destination = null;

		if (source != null) {
			destination = new MmbAncillaryData();

			destination.setAmount(source.getX003CAmountX003EKBackingField());
			AncillaryStatus sourceAncillaryStatus = source.getX003CAncillaryStatusX003EKBackingField();
			if (sourceAncillaryStatus != null)
				destination.setAncillaryStatus(MmbAncillaryStatusEnum.fromValue(sourceAncillaryStatus.value()));

			AncillaryType sourceAncillaryType = source.getX003CAncillaryTypeX003EKBackingField();
			if (sourceAncillaryType != null) {
				destination.setAncillaryType(MmbAncillaryTypeEnum.fromValue(sourceAncillaryType.value()));

				MmbAncillaryDetail destinationAncillaryDetail = null;
				if (source.getX003CAncillaryDetailX003EKBackingField() != null) {
					switch (sourceAncillaryType) {
					case EXTRA_BAGGAGE:
						destinationAncillaryDetail = extraBaggageDetailConverter.convert(
								(ExtraBaggageDetail) source.getX003CAncillaryDetailX003EKBackingField());
						break;

					case MEAL:
						destinationAncillaryDetail = mealDetailConverter.convert(
								(MealDetail) source.getX003CAncillaryDetailX003EKBackingField());
						break;

					case SEAT:
						destinationAncillaryDetail = seatDetailConverter.convert(
								(SeatDetail) source.getX003CAncillaryDetailX003EKBackingField());
						break;
						
					case UPGRADE:
						destinationAncillaryDetail = upgradeDetailConverter.convert(
								(UpgradeDetail) source.getX003CAncillaryDetailX003EKBackingField());
						break;

					default:
						break;
					}
				}
				destination.setAncillaryDetail(destinationAncillaryDetail);
			}

			destination.setCurrency(source.getX003CCurrencyX003EKBackingField());
			destination.setEmd(source.getX003CEMDX003EKBackingField());
			destination.setId(source.getX003CIdX003EKBackingField());
			destination.setQuantity(source.getX003CQuantityX003EKBackingField());
			destination.setReasonCode(source.getX003CReasonCodeX003EKBackingField());
			destination.setReasonSubcode(source.getX003CReasonSubcodeX003EKBackingField());
			
			List<Integer> incompatibleAncillaries = null;
			if (source.getX003CIncompatibleAncillariesX003EKBackingField() != null &&
					source.getX003CIncompatibleAncillariesX003EKBackingField().getInt() != null &&
					!source.getX003CIncompatibleAncillariesX003EKBackingField().getInt().isEmpty()) {
				incompatibleAncillaries = new ArrayList<>();
				for(Integer incompAncillary : source.getX003CIncompatibleAncillariesX003EKBackingField().getInt()) {
					incompatibleAncillaries.add(incompAncillary);
				}
			}
			destination.setIncompatibleAncillaries(incompatibleAncillaries);
			
			List<Integer> mandatoryAncillaries = null;
			if (source.getX003CMandatoryAncillariesX003EKBackingField() != null &&
					source.getX003CMandatoryAncillariesX003EKBackingField().getInt() != null &&
					!source.getX003CMandatoryAncillariesX003EKBackingField().getInt().isEmpty()) {
				mandatoryAncillaries = new ArrayList<>();
				for(Integer mandAncillary : source.getX003CMandatoryAncillariesX003EKBackingField().getInt()) {
					mandatoryAncillaries.add(mandAncillary);
				}
			}
			destination.setMandatoryAncillaries(mandatoryAncillaries);

			List<String> couponNumbers = null;
			if (source.getX003CCouponNumbersX003EKBackingField() != null &&
					source.getX003CCouponNumbersX003EKBackingField().getString() != null &&
					!source.getX003CCouponNumbersX003EKBackingField().getString().isEmpty()) {
				couponNumbers = new ArrayList<String>();
				for(String sourceCouponNumber : source.getX003CCouponNumbersX003EKBackingField().getString()) {
					couponNumbers.add(sourceCouponNumber);
				}
			}
			destination.setCouponNumbers(couponNumbers);

			List<MmbAncillaryQuotationData> quotations = null;
			if (source.getX003CQuotationsX003EKBackingField() != null &&
					source.getX003CQuotationsX003EKBackingField().getQuotations() != null &&
					!source.getX003CQuotationsX003EKBackingField().getQuotations().isEmpty()) {
				quotations = new ArrayList<MmbAncillaryQuotationData>();
				List<Quotations> sourceQuotations = source.getX003CQuotationsX003EKBackingField().getQuotations();
				for (Quotations sourceQuotation : sourceQuotations) {
					quotations.add(quotationsConverter.convert(sourceQuotation));
				}
			}
			destination.setQuotations(quotations);
		}

		return destination;
	}

}
