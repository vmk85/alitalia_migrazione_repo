package com.alitalia.aem.service.impl.converter.home.carnetservice;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.carnet.CarnetInfoCarnet;
import com.alitalia.aem.ws.carnet.carnetservice.xsd5.InfoCarnet;


@Component(immediate=true, metatype=false)
@Service(value=InfoCarnetToCarnetInfoCarnet.class)
public class InfoCarnetToCarnetInfoCarnet implements Converter<InfoCarnet, CarnetInfoCarnet> {

	@Override
	public CarnetInfoCarnet convert(InfoCarnet source) {
		CarnetInfoCarnet destination = null;

		if (source != null) {
			
				destination = new CarnetInfoCarnet();
				if (source.getBuyerEmail() != null)
					destination.setBuyerEmail(source.getBuyerEmail().getValue());
				if (source.getBuyerLastname() != null)
					destination.setBuyerLastname(source.getBuyerLastname().getValue());
				if (source.getBuyerName() != null)
					destination.setBuyerName(source.getBuyerName().getValue());
				if (source.getCarnetCode() != null)
					destination.setCarnetCode(source.getCarnetCode().getValue());
				XMLGregorianCalendar creationDate = source.getCreationDate();
				if (creationDate != null)
					destination.setCreationDate(creationDate.toGregorianCalendar());
				if (source.getECouponCode() != null)
					destination.seteCouponCode(source.getECouponCode().getValue());
				if (source.getEMDCode() != null)
					destination.setEmdCode(source.getEMDCode().getValue());
				XMLGregorianCalendar expireDate = source.getExpiryDate();
				if (expireDate != null)
					destination.setExpiryDate(expireDate.toGregorianCalendar());
				destination.setId(source.getId());
				if (source.getPassword() != null)
					destination.setPassword(source.getPassword().getValue());
				if (source.getPurchasedCarnetCode() != null)
					destination.setPurchasedCarnetCode(source.getPurchasedCarnetCode().getValue());
				destination.setResidualFare(source.getResidualFare());
				destination.setResidualRoutes(source.getResidualRoutes());
				destination.setTotalFare(source.getTotalFare());
				destination.setTotalRoutes(source.getTotalRoutes());
				destination.setUsedFare(source.getUsedFare());
				destination.setUsedRoutes(source.getUsedRoutes());
		}
		return destination;
	}
}