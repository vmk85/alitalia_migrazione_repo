package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.PassengerBase;
import com.alitalia.aem.common.data.home.AwardBookingSearchData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.PropertiesData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.common.data.home.enumerations.GatewayTypeEnum;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.APassengerBase;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.ArrayOfAPassengerBase;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.ArrayOfDictionaryItem;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd2.GateWayAwardSearch;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.GatewayType;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd6.ArrayOfanyType;

@Component(immediate=true, metatype=false)
@Service(value=AwardBookingSearchDataToGateWayAwardSearch.class)
public class AwardBookingSearchDataToGateWayAwardSearch implements Converter<AwardBookingSearchData, GateWayAwardSearch> {

	@Reference
	private PassengerBaseDataToAPassengerBase passengerBaseDataConverter;

	@Reference
	private PropertiesDataToArrayOfDictionaryItem propertiesDataConverter; 

	@Reference
	private RouteDataToRoute routeDataConverter;

	@Override
	public GateWayAwardSearch convert(AwardBookingSearchData source) {
		ObjectFactory objectFactory2 = new ObjectFactory();
		com.alitalia.aem.ws.bookingaward.searchservice.xsd3.ObjectFactory objectFactory3 = 
				new com.alitalia.aem.ws.bookingaward.searchservice.xsd3.ObjectFactory();
		com.alitalia.aem.ws.bookingaward.searchservice.xsd6.ObjectFactory objectFactory6 = 
				new com.alitalia.aem.ws.bookingaward.searchservice.xsd6.ObjectFactory();
		GateWayAwardSearch destination = null;

		if (source != null) {
			destination = objectFactory2.createGateWayAwardSearch();

			destination.setSolutionId(objectFactory2.createAwardFlightDetailSearchSolutionId(source.getBookingSolutionId()));
			destination.setFarmId(objectFactory3.createABoomBoxInfoFarmId(source.getFarmId()));
			destination.setId(objectFactory3.createABoomBoxInfoId(source.getId()));
			destination.setIsMultiSlice(source.isMultiSlice());
			destination.setIsNeutral(source.isNeutral());
			destination.setSessionId(objectFactory3.createABoomBoxInfoSessionId(source.getSessionId()));
			destination.setBookingSolutionId(objectFactory2.createGateWayAwardSearchBookingSolutionId(source.getSolutionId()));
			destination.setSolutionSet(objectFactory3.createABoomBoxInfoSolutionSet(source.getSolutionSet()));

			GatewayTypeEnum sourceType = source.getType();
			if (sourceType != null)
				destination.setType(GatewayType.fromValue(sourceType.value()));

			ArrayOfAPassengerBase passengers = new ArrayOfAPassengerBase();
			for (PassengerBase sourcePassengerBase : source.getPassengers()) {
				APassengerBase aPassengerBase = passengerBaseDataConverter.convert((PassengerBaseData) sourcePassengerBase);
				passengers.getAPassengerBase().add(aPassengerBase);
			}
			destination.setPassengers(objectFactory2.createGateWayBrandSearchPassengers(passengers));

			PropertiesData sourceProperties = source.getProperties();
			if (sourceProperties != null) {
				ArrayOfDictionaryItem properties = propertiesDataConverter.convert(sourceProperties);
				destination.setProperties(objectFactory3.createABoomBoxGenericInfoProperties(properties));
			}
			else
				destination.setProperties(objectFactory3.createABoomBoxGenericInfoProperties(null));

			if (source.getSelectedFlights() != null) {
				ArrayOfanyType selectedFlights = objectFactory6.createArrayOfanyType();
				for( RouteData sourceRouteData : source.getSelectedFlights()) {
					selectedFlights.getAnyType().add(routeDataConverter.convert(sourceRouteData));
				}
				destination.setSelectedFlights(objectFactory2.createGateWayAwardSearchSelectedFlights(selectedFlights));
			} else {
				destination.setSelectedFlights(objectFactory2.createGateWayAwardSearchSelectedFlights(null));
			}
		}

		return destination;
	}

}
