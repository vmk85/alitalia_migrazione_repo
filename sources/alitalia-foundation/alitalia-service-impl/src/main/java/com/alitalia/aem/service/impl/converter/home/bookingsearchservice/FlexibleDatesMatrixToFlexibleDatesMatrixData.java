package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.FlexibleDatesMatrixData;
import com.alitalia.aem.ws.booking.searchservice.xsd11.ArrayOfresultFlexibleDatesMatrixColumn;
import com.alitalia.aem.ws.booking.searchservice.xsd11.ArrayOfresultFlexibleDatesMatrixRow;
import com.alitalia.aem.ws.booking.searchservice.xsd11.ResultFlexibleDatesMatrixColumn;
import com.alitalia.aem.ws.booking.searchservice.xsd11.ResultFlexibleDatesMatrixRow;
import com.alitalia.aem.ws.booking.searchservice.xsd11.ResultFlexibleDatesMatrixRowCell;
import com.alitalia.aem.ws.booking.searchservice.xsd11.ResultFlexibleDatesMatrixRowCellSolution;
import com.alitalia.aem.ws.booking.searchservice.xsd2.FlexibleDatesMatrix;

@Component(immediate=true, metatype=false)
@Service(value=FlexibleDatesMatrixToFlexibleDatesMatrixData.class)
public class FlexibleDatesMatrixToFlexibleDatesMatrixData implements Converter<FlexibleDatesMatrix, List<FlexibleDatesMatrixData>> {

	private static final Logger logger = LoggerFactory.getLogger(FlexibleDatesMatrixToFlexibleDatesMatrixData.class);

	@Override
	public List<FlexibleDatesMatrixData> convert(FlexibleDatesMatrix source) {
		List<FlexibleDatesMatrixData> destination = new ArrayList<FlexibleDatesMatrixData>();

		logger.info("Unmarshalling FlexibleDatesMatrix: ArrayOfresultFlexibleDatesMatrixColumn and ArrayOfresultFlexibleDatesMatrixRow");

		ArrayOfresultFlexibleDatesMatrixColumn flexibleDatesMatrixColumn = null;
		ArrayOfresultFlexibleDatesMatrixRow flexibleDatesMatrixRow = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		GregorianCalendar cal = null;
		try {
			flexibleDatesMatrixColumn = (ArrayOfresultFlexibleDatesMatrixColumn) source.getColumn().getValue();//unmarshalledColumnObj.getValue();

			flexibleDatesMatrixRow = (ArrayOfresultFlexibleDatesMatrixRow) source.getRow().getValue();//unmarshalledRowObj.getValue();

			List<ResultFlexibleDatesMatrixColumn> resultFlexibleDatesMatrixColumn = flexibleDatesMatrixColumn.getResultFlexibleDatesMatrixColumn();
			List<ResultFlexibleDatesMatrixRow> resultFlexibleDatesMatrixRow = flexibleDatesMatrixRow.getResultFlexibleDatesMatrixRow();

			logger.info("Remapping FlexibleDatesMatrix response in List<FlexibleDatesMatrixData>");

			if (resultFlexibleDatesMatrixColumn != null && resultFlexibleDatesMatrixRow!=null){
				FlexibleDatesMatrixData flexibleDatesMatrixData = null;
				for(ResultFlexibleDatesMatrixRow row : resultFlexibleDatesMatrixRow){
					if (row.getCellField() != null){
						for (int i = 0; i < resultFlexibleDatesMatrixColumn.size(); i++){
							flexibleDatesMatrixData = new FlexibleDatesMatrixData();

							String dataAndata = row.getDayField();
							logger.debug("Data Andata: {}", dataAndata);
							cal = new GregorianCalendar();
							cal.setTime(sdf.parse(dataAndata));
							flexibleDatesMatrixData.setDepartureDateTime(cal);

							String dataRitorno = resultFlexibleDatesMatrixColumn.get(i).getDayField();
							logger.debug("Data Ritorno: {}", dataRitorno);
							cal = new GregorianCalendar();
							cal.setTime(sdf.parse(dataRitorno));
							flexibleDatesMatrixData.setReturnDateTime(cal);
							
							ResultFlexibleDatesMatrixRowCell cell = row.getCellField().getResultFlexibleDatesMatrixRowCell().get(i);
							
							if (cell != null) {
								ResultFlexibleDatesMatrixRowCellSolution solutionField = cell.getSolutionField();
								if (solutionField != null) {
									flexibleDatesMatrixData.setAmount(solutionField.getSaleTotalField().getAmountField());
									flexibleDatesMatrixData.setCurrency(solutionField.getSaleTotalField().getCurrencyField());
									flexibleDatesMatrixData.setIdField(solutionField.getIdField());
								}
	
								destination.add(flexibleDatesMatrixData);
							}
						}
					}

				}
			}
		} catch (ParseException e1) {
			logger.error("Errore durante il parsing delle data di andata/ritorno relative al servizio matrice 7x7", e1);
		} catch (Exception e2) {
			logger.error("Errore durante la conversione della response del servizio matrice 7x7", e2);
		}
		return destination;
	}

}
