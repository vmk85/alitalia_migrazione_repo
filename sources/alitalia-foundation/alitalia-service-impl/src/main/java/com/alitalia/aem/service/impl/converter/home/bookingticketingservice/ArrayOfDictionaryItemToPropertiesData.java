package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PropertiesData;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ArrayOfDictionaryItem;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.DictionaryItem;

@Component(immediate = true, metatype = false)
@Service(value = ArrayOfDictionaryItemToPropertiesData.class)
public class ArrayOfDictionaryItemToPropertiesData implements Converter<ArrayOfDictionaryItem, PropertiesData> {

	@Override
	public PropertiesData convert(ArrayOfDictionaryItem source) {
		PropertiesData destination = null;

		if (source != null) {
			destination = new PropertiesData();

			for (DictionaryItem item : source.getDictionaryItem()) {
				if (item.getKey() != null && item.getValue() != null) {
					destination.put(item.getKey().getValue(), item.getValue().getValue());
				}
			}
		}
		
		return destination;
	}

}
