package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentLegScheduledServicesData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionSliceSegmentLegScheduledServices;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentLegScheduledServicesToRTDSSliceSegmentLegScheduledServicesData.class)
public class RTDSSliceSegmentLegScheduledServicesToRTDSSliceSegmentLegScheduledServicesData implements
		Converter<ResultTicketingDetailSolutionSliceSegmentLegScheduledServices, ResultTicketingDetailSolutionSliceSegmentLegScheduledServicesData> {

	@Override
	public ResultTicketingDetailSolutionSliceSegmentLegScheduledServicesData convert(
			ResultTicketingDetailSolutionSliceSegmentLegScheduledServices source) {
		ResultTicketingDetailSolutionSliceSegmentLegScheduledServicesData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionSliceSegmentLegScheduledServicesData();

			destination.setCabinField(source.getCabinField());

			List<String> mealField = null;
			if (source.getMealField() != null &&
					source.getMealField().getString() != null &&
					!source.getMealField().getString().isEmpty()) {
				mealField = new ArrayList<String>();
				for(String sourceMealFieldElem : source.getMealField().getString())
					mealField.add(sourceMealFieldElem);
			}
			destination.setMealField(mealField);
		}

		return destination;
	}

}
