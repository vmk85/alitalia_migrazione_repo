package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.UserInfoBaseData;
import com.alitalia.aem.common.data.home.UserInfoCompleteData;
import com.alitalia.aem.common.data.home.UserInfoFullData;
import com.alitalia.aem.common.data.home.UserInfoStandardData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.AUserInfoBase;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.UserInfoComplete;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.UserInfoFull;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.UserInfoStandard;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd4.GenderType;

@Component(immediate=true, metatype=false)
@Service(value=UserInfoBaseToAUserInfo.class)
public class UserInfoBaseToAUserInfo 
		implements Converter<UserInfoBaseData, AUserInfoBase> {
	
	@Override
	public AUserInfoBase convert(UserInfoBaseData source) {
		
		AUserInfoBase destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			if (source instanceof UserInfoFullData) {
				UserInfoFullData typedSource = (UserInfoFullData) source;
				UserInfoFull typedDestination =  new UserInfoFull();
				destination = typedDestination;
				
				// UserInfoCompleteData
				
				typedDestination.setAdress(
						objectFactory.createUserInfoCompleteAdress(
								typedSource.getAdress()));
				
				typedDestination.setCAP(
						objectFactory.createUserInfoCompleteCAP(
								typedSource.getCap()));
				
				typedDestination.setCity(
						objectFactory.createUserInfoCompleteCity(
								typedSource.getCity()));
				
				typedDestination.setCountry(
						objectFactory.createUserInfoCompleteCountry(
								typedSource.getCountry()));
				
				typedDestination.setState(
						objectFactory.createUserInfoCompleteState(
								typedSource.getState()));
				
				// UserInfoFullData
				
				if (typedSource.getGender() != null) {
					typedDestination.setGender(
							GenderType.fromValue(typedSource.getGender().value()));
				}
				
				typedDestination.setBirthDate(
						XsdConvertUtils.toXMLGregorianCalendar(
								typedSource.getBirthDate()));
				
				typedDestination.setHouseNumber(
						objectFactory.createUserInfoFullHouseNumber(
								typedSource.getHouseNumber()));
				
				typedDestination.setTitle(
						objectFactory.createUserInfoFullTitle(
								typedSource.getTitle()));
				
				typedDestination.setVatNumber(
						objectFactory.createUserInfoFullVatNumber(
								typedSource.getVatNumber()));
				
			} else if (source instanceof UserInfoCompleteData) {
				UserInfoCompleteData typedSource = (UserInfoCompleteData) source;
				UserInfoComplete typedDestination =  new UserInfoComplete();
				destination = typedDestination;
				
				typedDestination.setAdress(
						objectFactory.createUserInfoCompleteAdress(
								typedSource.getAdress()));
				
				typedDestination.setCAP(
						objectFactory.createUserInfoCompleteCAP(
								typedSource.getCap()));
				
				typedDestination.setCity(
						objectFactory.createUserInfoCompleteCity(
								typedSource.getCity()));
				
				typedDestination.setCountry(
						objectFactory.createUserInfoCompleteCountry(
								typedSource.getCountry()));
				
				typedDestination.setState(
						objectFactory.createUserInfoCompleteState(
								typedSource.getState()));
				
			} else if (source instanceof UserInfoStandardData) {
				UserInfoStandard typedDestination =  new UserInfoStandard();
				destination = typedDestination;
				
			} else {
				destination = new AUserInfoBase();
				
			}
			
			destination.setLastname(
					objectFactory.createAUserInfoBaseLastname(
							source.getLastname()));

			destination.setName(
					objectFactory.createAUserInfoBaseName(
							source.getName()));
			
		}
		
		return destination;
	}

}
