package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.BrandPenaltiesData;
import com.alitalia.aem.common.data.home.BrandPenaltyData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.BrandPenalties;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=BrandPenaltiesDataToBrandPenalties.class)
public class BrandPenaltiesDataToBrandPenalties implements Converter<BrandPenaltiesData, BrandPenalties> {

	@Reference
	private BrandPenaltyDataToBrandPenalty brandPenaltyDataConveter;

	@Override
	public BrandPenalties convert(BrandPenaltiesData source) {
		ObjectFactory factory3 = new ObjectFactory();
		BrandPenalties destination = factory3.createBrandPenalties();
		BrandPenaltyData penalty = null;

		penalty = source.getChangeAfterDepature();
		if (penalty != null) {
			destination.setChangeAfterDepature(
					factory3.createBrandPenaltiesChangeAfterDepature(brandPenaltyDataConveter.convert(penalty)));
		}
		else
			destination.setChangeAfterDepature(null);

		penalty = source.getChangeBeforeDepature();
		if (penalty != null) {
			destination.setChangeBeforeDepature(
					factory3.createBrandPenaltiesChangeBeforeDepature(brandPenaltyDataConveter.convert(penalty)));
		}
		else
			destination.setChangeBeforeDepature(null);

		penalty = source.getRefundAfterDepature();
		if (penalty != null) {
			destination.setRefundAfterDepature(
					factory3.createBrandPenaltiesRefundAfterDepature(brandPenaltyDataConveter.convert(penalty)));
		}
		else
			destination.setRefundAfterDepature(null);

		penalty = source.getRefundBeforeDepature();
		if (penalty != null) {
			destination.setRefundBeforeDepature(
					factory3.createBrandPenaltiesRefundBeforeDepature(brandPenaltyDataConveter.convert(penalty)));
		}
		else
			destination.setRefundBeforeDepature(null);

		return destination;
	}

}
