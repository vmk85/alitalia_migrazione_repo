package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ArrayOfresultBookingDetailsSolutionPricing;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ResultBookingDetailsSolution;

@Component(immediate=true, metatype=false)
@Service(value=ResultBookingDetailsSolutionDataToResultBookingDetailsSolution.class)
public class ResultBookingDetailsSolutionDataToResultBookingDetailsSolution
		implements Converter<ResultBookingDetailsSolutionData, ResultBookingDetailsSolution> {

	@Reference
	private RBDSAddCollectPriceDataToAddCollectPrice rbdsAddCollectPriceConverter;
	
	@Reference
	private RBDSRefundPriceDataToRefundPrice rbdsRefundPriceDataConverter;

	@Reference
	private RBDSPricingDataToPricing rbdsPricingConverter;
	
	@Override
	public ResultBookingDetailsSolution convert(ResultBookingDetailsSolutionData source) {
	   
		ResultBookingDetailsSolution destination = null;
		
		
		if(source != null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolution();
			destination.setAddCollectPriceField(rbdsAddCollectPriceConverter.convert(source.getAddCollectPriceField()));
			ArrayOfresultBookingDetailsSolutionPricing pricingField = objfact.createArrayOfresultBookingDetailsSolutionPricing();
			if(source.getPricingField()!=null)
			for(ResultBookingDetailsSolutionPricingData s : source.getPricingField()){
				pricingField.getResultBookingDetailsSolutionPricing().add(rbdsPricingConverter.convert(s));
			}
				
			destination.setPricingField(pricingField);
			destination.setRefundPriceField(rbdsRefundPriceDataConverter.convert(source.getRefundPriceField()));
			destination.setSaleTaxTotalField(source.getSaleTaxTotalField());
			destination.setSaleTotalField(source.getSaleTotalField());
		    destination.setTimeField(source.getTimeField());
		}
	    return destination;
	}

}
