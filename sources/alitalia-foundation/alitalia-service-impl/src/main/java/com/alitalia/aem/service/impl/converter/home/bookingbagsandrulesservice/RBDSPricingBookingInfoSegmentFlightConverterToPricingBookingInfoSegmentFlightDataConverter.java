package com.alitalia.aem.service.impl.converter.home.bookingbagsandrulesservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentFlightData;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetailsSolutionPricingBookingInfoSegmentFlight;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingBookingInfoSegmentFlightConverterToPricingBookingInfoSegmentFlightDataConverter.class)
public class RBDSPricingBookingInfoSegmentFlightConverterToPricingBookingInfoSegmentFlightDataConverter
		implements Converter<ResultBookingDetailsSolutionPricingBookingInfoSegmentFlight, ResultBookingDetailsSolutionPricingBookingInfoSegmentFlightData> {

	@Override
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentFlightData convert(
			ResultBookingDetailsSolutionPricingBookingInfoSegmentFlight source) {
		
		ResultBookingDetailsSolutionPricingBookingInfoSegmentFlightData destination = null;
		
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingBookingInfoSegmentFlightData();
			destination.setCarrierField(source.getCarrierField());
			destination.setNumberField(source.getNumberField());
			destination.setNumberFieldSpecified(source.isNumberFieldSpecified());
		}
		
		return destination;
		
	}
	
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentFlightData convert(
			com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetailsSolutionPricingBookingInfoSegmentFlight source) {
		
		ResultBookingDetailsSolutionPricingBookingInfoSegmentFlightData destination = null;
		
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingBookingInfoSegmentFlightData();
			destination.setCarrierField(source.getCarrierField());
			destination.setNumberField(source.getNumberField());
			destination.setNumberFieldSpecified(source.isNumberFieldSpecified());
		}
		
		return destination;
		
	}
	
}
