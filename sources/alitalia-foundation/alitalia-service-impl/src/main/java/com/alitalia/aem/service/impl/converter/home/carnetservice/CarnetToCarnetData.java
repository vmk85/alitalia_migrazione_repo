package com.alitalia.aem.service.impl.converter.home.carnetservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.carnet.CarnetData;
import com.alitalia.aem.ws.carnet.carnetservice.xsd5.Carnet;


@Component(immediate=true, metatype=false)
@Service(value=CarnetToCarnetData.class)
public class CarnetToCarnetData implements Converter<Carnet, CarnetData> {

	@Override
	public CarnetData convert(Carnet source) {
		CarnetData destination = null;

		if (source != null) {
			destination = new CarnetData();
			if (source.getChangeDescription() != null)
				destination.setChangeDescription(source.getChangeDescription().getValue());
			if (source.getCode() != null)
				destination.setCode(source.getCode().getValue());
			if (source.getCodeEMD() != null)
				destination.setCodeEMD(source.getCodeEMD().getValue());
			if (source.getCurrency() != null)
				destination.setCurrency(source.getCurrency().getValue());
			destination.setIncrementPrice(source.getIncrementPrice());
			if (source.getPrenotationClass() != null)
				destination.setPrenotationClass(source.getPrenotationClass().getValue());
			destination.setPrice(source.getPrice());
			destination.setQuantity(source.getQuantity());
			if (source.getRedemption() != null)
				destination.setRedemption(source.getRedemption().getValue());
			destination.setValidity(source.getValidity());
		}
		
		return destination;
	}
}