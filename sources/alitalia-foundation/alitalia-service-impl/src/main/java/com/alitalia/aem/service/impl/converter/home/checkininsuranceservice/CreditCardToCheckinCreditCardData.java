package com.alitalia.aem.service.impl.converter.home.checkininsuranceservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinCreditCardData;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd2.CreditCardData;


@Component(immediate=true, metatype=false)
@Service(value=CreditCardToCheckinCreditCardData.class)
public class CreditCardToCheckinCreditCardData implements Converter<CreditCardData, CheckinCreditCardData> {
	
	@Reference
	private CreditCardHolderDataToCheckinCreditCardHolderData checkinCreditCardHolderDataConverter;
	
	@Override
	public CheckinCreditCardData convert(CreditCardData source) {
		CheckinCreditCardData destination = null;

		if (source != null) {
			destination = new CheckinCreditCardData();
			
			destination.setCreditCardHolderData(checkinCreditCardHolderDataConverter.convert(source.getX003CCreditCardHolderDataX003EKBackingField()));
			destination.setCvv(source.getX003CCVVX003EKBackingField());
			destination.setExpireMonth(source.getX003CExpireMonthX003EKBackingField());
			destination.setExpireYear(source.getX003CExpireYearX003EKBackingField());
			destination.setId(source.getX003CIdX003EKBackingField());
			destination.setIs3DSecure(source.isX003CIs3DSecureX003EKBackingField());
			destination.setNumber(source.getX003CNumberX003EKBackingField());
			destination.setThreeDSecureOpaqueParams64Encoded(source.getX003CThreeDSecureOpaqueParams64EncodedX003EKBackingField());
			destination.setType(source.getX003CTypeX003EKBackingField());
			
		}
		return destination;
	}
}
