package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareDisplayBasePriceData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionPricingFareDisplayBasePrice;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareDisplayBasePriceDataToRTDSPricingFareDisplayBasePrice.class)
public class RTDSPricingFareDisplayBasePriceDataToRTDSPricingFareDisplayBasePrice implements
		Converter<ResultTicketingDetailSolutionPricingFareDisplayBasePriceData, 
					ResultTicketingDetailSolutionPricingFareDisplayBasePrice> {

	@Override
	public ResultTicketingDetailSolutionPricingFareDisplayBasePrice convert(
			ResultTicketingDetailSolutionPricingFareDisplayBasePriceData source) {
		ResultTicketingDetailSolutionPricingFareDisplayBasePrice destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingFareDisplayBasePrice();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
