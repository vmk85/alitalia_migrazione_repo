package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingPenaltyPriceData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ResultBookingDetailsSolutionPricingPenaltyPrice;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingPenaltyPriceDataToPricingPenaltyPrice.class)
public class RBDSPricingPenaltyPriceDataToPricingPenaltyPrice implements
		Converter<ResultBookingDetailsSolutionPricingPenaltyPriceData, ResultBookingDetailsSolutionPricingPenaltyPrice> {

	@Override
	public ResultBookingDetailsSolutionPricingPenaltyPrice convert(
			ResultBookingDetailsSolutionPricingPenaltyPriceData source) {
		ResultBookingDetailsSolutionPricingPenaltyPrice destination = null;
		if(source!=null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionPricingPenaltyPrice();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}

}
