package com.alitalia.aem.service.impl.converter.home.bookingbagsandrulesservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareCalculationData;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetailsSolutionPricingFareCalculation;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetailsSolutionPricingFareCalculationBoxedTax;


@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingFareCalculationToPricingFareCalculationDataConverter.class)
public class RBDSPricingFareCalculationToPricingFareCalculationDataConverter
		implements Converter<ResultBookingDetailsSolutionPricingFareCalculation, ResultBookingDetailsSolutionPricingFareCalculationData> {

	@Reference
	private RBDSPricingFareCalculationBoxedTaxToPricingFareCalculationBoxedTaxData pricingFareCalculationBoxedTaxConverter;
	
	
	@Override
	public ResultBookingDetailsSolutionPricingFareCalculationData convert(
			ResultBookingDetailsSolutionPricingFareCalculation source) {
		ResultBookingDetailsSolutionPricingFareCalculationData destination = null;
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingFareCalculationData();
			
			
			List<ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData> boxedTaxField = new ArrayList<ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData>();
			if(source.getBoxedTaxField()!=null)
			for(ResultBookingDetailsSolutionPricingFareCalculationBoxedTax s : source.getBoxedTaxField().getResultBookingDetailsSolutionPricingFareCalculationBoxedTax()){
				boxedTaxField.add(pricingFareCalculationBoxedTaxConverter.convert(s));
			}
			destination.setBoxedTaxField(boxedTaxField);
			
			List<String> lineField = new ArrayList<String>();
			if(source.getLineField()!=null)
			for(String s : source.getLineField().getString())
				lineField.add(s);
			destination.setLineField(lineField);
		}
		return destination;
	}
	
	public ResultBookingDetailsSolutionPricingFareCalculationData convert(
			com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetailsSolutionPricingFareCalculation source) {
		ResultBookingDetailsSolutionPricingFareCalculationData destination = null;
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingFareCalculationData();

			List<ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData> boxedTaxField = new ArrayList<ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData>();
			if(source.getBoxedTaxField()!=null)
			for(com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetailsSolutionPricingFareCalculationBoxedTax s : source.getBoxedTaxField().getResultBookingDetailsSolutionPricingFareCalculationBoxedTax()){
				boxedTaxField.add(pricingFareCalculationBoxedTaxConverter.convert(s));
			}
			destination.setBoxedTaxField(boxedTaxField);
			
			List<String> lineField = new ArrayList<String>();
			if(source.getLineField()!=null)
			for(String s : source.getLineField().getString())
				lineField.add(s);
			destination.setLineField(lineField);
		}
		return destination;
	}

}
