package com.alitalia.aem.service.impl.converter.home.bookingbagsandrulesservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoFareData;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetailsSolutionPricingBookingInfoFare;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingBookingInfoFareToPricingBookingInfoFareData.class)
public class RBDSPricingBookingInfoFareToPricingBookingInfoFareData implements
		Converter<ResultBookingDetailsSolutionPricingBookingInfoFare, ResultBookingDetailsSolutionPricingBookingInfoFareData> {

	@Override
	public ResultBookingDetailsSolutionPricingBookingInfoFareData convert(
			ResultBookingDetailsSolutionPricingBookingInfoFare source) {
		ResultBookingDetailsSolutionPricingBookingInfoFareData destination = null;
		
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingBookingInfoFareData();
			destination.setExtendedFareCodeField(source.getExtendedFareCodeField());
			destination.setTicketDesignatorField(source.getTicketDesignatorField());
		}
		return destination;
		
	}
	
	public ResultBookingDetailsSolutionPricingBookingInfoFareData convert(
			com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetailsSolutionPricingBookingInfoFare source) {
		ResultBookingDetailsSolutionPricingBookingInfoFareData destination = null;
		
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingBookingInfoFareData();
			destination.setExtendedFareCodeField(source.getExtendedFareCodeField());
			destination.setTicketDesignatorField(source.getTicketDesignatorField());
		}
		return destination;
		
	}

}
