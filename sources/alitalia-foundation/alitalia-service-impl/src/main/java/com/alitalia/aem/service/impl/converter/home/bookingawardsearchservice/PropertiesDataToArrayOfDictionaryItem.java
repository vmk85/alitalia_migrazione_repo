package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PropertiesData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.ArrayOfDictionaryItem;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.DictionaryItem;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=PropertiesDataToArrayOfDictionaryItem.class)
public class PropertiesDataToArrayOfDictionaryItem implements Converter<PropertiesData, ArrayOfDictionaryItem> {

	@Override
	public ArrayOfDictionaryItem convert(PropertiesData source) {
		
		ArrayOfDictionaryItem destination = null;
		
		if (source != null) {
			
			ObjectFactory factory = new ObjectFactory();
			
			destination = factory.createArrayOfDictionaryItem();
	
			for (String key : source.keySet()) {
				DictionaryItem dictionaryItem = factory.createDictionaryItem();
				
				dictionaryItem.setKey(
						factory.createDictionaryItemKey(
								key));
				
				dictionaryItem.setValue(
						factory.createDictionaryItemValue(
								source.getElement(key)));
				
				destination.getDictionaryItem().add(dictionaryItem);
			}
			
		}

		return destination;
	}

}
