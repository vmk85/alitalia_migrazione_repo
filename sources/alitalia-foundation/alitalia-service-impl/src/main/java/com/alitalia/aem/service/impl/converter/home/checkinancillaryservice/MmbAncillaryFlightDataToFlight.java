package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.enumerations.MmbFlightTypeEnum;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryFlightData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.Flight;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.FlightType;


@Component(immediate=true, metatype=false)
@Service(value=MmbAncillaryFlightDataToFlight.class)
public class MmbAncillaryFlightDataToFlight implements Converter<MmbAncillaryFlightData, Flight> {

	@Override
	public Flight convert(MmbAncillaryFlightData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		Flight destination = null;

		if (source != null) {
			destination = objectFactory.createFlight();

			destination.setX003CCarrierX003EKBackingField(source.getCarrier());
			destination.setX003CDepartureDateX003EKBackingField(
					XsdConvertUtils.toXMLGregorianCalendar(source.getDepartureDate()));
			destination.setX003CDestinationX003EKBackingField(source.getDestination());

			MmbFlightTypeEnum sourceFlightType = source.getFlightType();
			if (sourceFlightType != null)
				destination.setX003CFlightTypeX003EKBackingField(FlightType.fromValue(sourceFlightType.value()));

			destination.setX003CIdX003EKBackingField(source.getId());
			destination.setX003CNumberX003EKBackingField(source.getNumber());
			destination.setX003COperationalFlightCarrierX003EKBackingField(source.getOperationalFlightCarrier());
			destination.setX003COperationalFlightNumberX003EKBackingField(source.getOperationalFlightNumber());
			destination.setX003COriginX003EKBackingField(source.getOrigin());
			destination.setX003CReferenceNumberX003EKBackingField(source.getReferenceNumber());
		}

		return destination;
	}

}
