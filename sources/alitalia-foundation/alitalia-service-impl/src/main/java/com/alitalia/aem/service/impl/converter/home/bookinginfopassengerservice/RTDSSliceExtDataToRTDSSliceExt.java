package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceExtData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionSliceExt;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceExtDataToRTDSSliceExt.class)
public class RTDSSliceExtDataToRTDSSliceExt implements
		Converter<ResultTicketingDetailSolutionSliceExtData, ResultTicketingDetailSolutionSliceExt> {

	@Reference
	private RTDSSliceExtWarningDataToRTSSliceExtWarning rtdsWarningFieldConvertert;

	@Override
	public ResultTicketingDetailSolutionSliceExt convert(
			ResultTicketingDetailSolutionSliceExtData source) {
		ResultTicketingDetailSolutionSliceExt destination = null;

		if (source !=null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionSliceExt();

			destination.setWarningField(rtdsWarningFieldConvertert.convert(source.getWarningField()));
		}

		return destination;
	}

}
