package com.alitalia.aem.service.impl.converter.home.checkinboardingpassservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.messages.home.BoardingPassCheckinResponse;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd1.BoardingPassResponse;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd2.ArrayOfPassenger;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd2.Passenger;


@Component(immediate=true, metatype=false)
@Service(value=BoardingPassResponseToBoardingPassCheckinResponse.class)
public class BoardingPassResponseToBoardingPassCheckinResponse implements Converter<BoardingPassResponse, BoardingPassCheckinResponse> {

	@Reference
	private PassengerToCheckinPassengerData passengerConverter;
	
	@Reference
	private RouteToCheckinRouteData routeConverter;

	@Override
	public BoardingPassCheckinResponse convert(BoardingPassResponse source) {
		
		BoardingPassCheckinResponse destination = null;

		if (source != null) {
			
			destination = new BoardingPassCheckinResponse();
			
			JAXBElement<ArrayOfPassenger> sourcePassengers = source.getPassengers();
			if(sourcePassengers != null &&
					sourcePassengers.getValue() != null &&
					sourcePassengers.getValue().getPassenger() != null &&
					!sourcePassengers.getValue().getPassenger().isEmpty()){
				List<CheckinPassengerData> passengersList = new ArrayList<CheckinPassengerData>();
				for(Passenger sourcePassenger : sourcePassengers.getValue().getPassenger()){
					passengersList.add(passengerConverter.convert(sourcePassenger));
				}
				destination.setPassengers(passengersList);
			}
			
			if(source.getSelectedRoute() != null){
				destination.setSelectedRoute(routeConverter.convert(source.getSelectedRoute().getValue()));
			}
			
		}

		return destination;
	}


}
