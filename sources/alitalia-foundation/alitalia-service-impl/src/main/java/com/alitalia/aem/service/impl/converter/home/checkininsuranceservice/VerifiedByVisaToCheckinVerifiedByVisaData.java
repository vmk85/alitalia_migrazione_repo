package com.alitalia.aem.service.impl.converter.home.checkininsuranceservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinVerifiedByVisaData;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd2.VerifiedByVisa;


@Component(immediate=true, metatype=false)
@Service(value=VerifiedByVisaToCheckinVerifiedByVisaData.class)
public class VerifiedByVisaToCheckinVerifiedByVisaData implements Converter<VerifiedByVisa, CheckinVerifiedByVisaData> {
	

	@Override
	public CheckinVerifiedByVisaData convert(VerifiedByVisa source) {
		CheckinVerifiedByVisaData destination = null;

		if (source != null) {
			destination = new CheckinVerifiedByVisaData();
			destination.setId(source.getX003CIdX003EKBackingField());
			destination.setThreeDSecureOpaqueParams64Encoded(source.getX003CThreeDSecureOpaqueParams64EncodedX003EKBackingField());
		}
		return destination;
	}

}
