package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingPassengerData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingPassenger;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingPassengerToRTDSPricingPassengerData.class)
public class RTDSPricingPassengerToRTDSPricingPassengerData implements
		Converter<ResultTicketingDetailSolutionPricingPassenger, 
					ResultTicketingDetailSolutionPricingPassengerData> {

	@Override
	public ResultTicketingDetailSolutionPricingPassengerData convert(
			ResultTicketingDetailSolutionPricingPassenger source) {
		ResultTicketingDetailSolutionPricingPassengerData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingPassengerData();

			destination.setIdField(source.getIdField());

			List<String> ptcField = null;
			if (source.getPtcField() != null &&
					source.getPtcField().getString() != null &&
					!source.getPtcField().getString().isEmpty()) {
				ptcField = new ArrayList<String>();
				for(String sourcePtcFieldElem : source.getPtcField().getString())
					ptcField.add(sourcePtcFieldElem);
			}
			destination.setPtcField(ptcField);
		}

		return destination;
	}

}
