package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentLegConnectionData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionSliceSegmentLegConnection;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentLegConnectionDataToRTDSliceSegmentLegConnection.class)
public class RTDSSliceSegmentLegConnectionDataToRTDSliceSegmentLegConnection implements
		Converter<ResultTicketingDetailSolutionSliceSegmentLegConnectionData, 
					ResultTicketingDetailSolutionSliceSegmentLegConnection> {

	@Override
	public ResultTicketingDetailSolutionSliceSegmentLegConnection convert(
			ResultTicketingDetailSolutionSliceSegmentLegConnectionData source) {
		ResultTicketingDetailSolutionSliceSegmentLegConnection destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionSliceSegmentLegConnection();

			destination.setDurationField(source.getDurationField());
		}

		return destination;
	}

}
