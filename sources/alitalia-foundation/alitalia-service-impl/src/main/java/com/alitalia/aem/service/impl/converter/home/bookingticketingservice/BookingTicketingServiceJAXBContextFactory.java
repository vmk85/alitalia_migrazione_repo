package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.booking.ticketservice.xsd5.AdultPassenger;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ApplicantPassenger;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.CashAndMiles;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ChildPassenger;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.InfantPassenger;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.InsurancePolicy;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.Route;


@Component(immediate=true, metatype=false)
@Service(value=BookingTicketingServiceJAXBContextFactory.class)
public class BookingTicketingServiceJAXBContextFactory {

	private static final Logger logger = LoggerFactory.getLogger(BookingTicketingServiceJAXBContextFactory.class);

	private JAXBContext jaxbContext = null;

	public synchronized JAXBContext getJaxbContext() throws JAXBException {

		if (jaxbContext == null) {
			jaxbContext = JAXBContext.newInstance(ApplicantPassenger.class,
					AdultPassenger.class,
					ChildPassenger.class,
					InfantPassenger.class,
					CashAndMiles.class,
					InsurancePolicy.class,
					Route.class);
		}

		return jaxbContext;
	}

	@Activate
	private void activate() {
		try {
			jaxbContext = JAXBContext.newInstance(ApplicantPassenger.class,
					AdultPassenger.class,
					ChildPassenger.class,
					InfantPassenger.class,
					CashAndMiles.class,
					InsurancePolicy.class,
					Route.class);
		} catch (JAXBException e) {
			logger.warn("BookingSearchServiceJAXBContextFactory - cannot instantiate JAXBContext: {}", e.getMessage());
		}
	}
}
