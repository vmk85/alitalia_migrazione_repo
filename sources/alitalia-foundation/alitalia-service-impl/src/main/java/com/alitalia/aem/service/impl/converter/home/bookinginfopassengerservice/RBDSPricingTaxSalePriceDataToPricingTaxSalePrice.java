package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingTaxSalePriceData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ResultBookingDetailsSolutionPricingTaxSalePrice;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingTaxSalePriceDataToPricingTaxSalePrice.class)
public class RBDSPricingTaxSalePriceDataToPricingTaxSalePrice implements
		Converter<ResultBookingDetailsSolutionPricingTaxSalePriceData, ResultBookingDetailsSolutionPricingTaxSalePrice> {

	@Override
	public ResultBookingDetailsSolutionPricingTaxSalePrice convert(
			ResultBookingDetailsSolutionPricingTaxSalePriceData source) {
		ResultBookingDetailsSolutionPricingTaxSalePrice destination = null;
		if(source!=null){
			ObjectFactory objf = new ObjectFactory();
			destination = objf.createResultBookingDetailsSolutionPricingTaxSalePrice();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}

}
