package com.alitalia.aem.service.impl.converter.home.bookingwidget;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.UpdateWidgetCashMilesResponse;
import com.alitalia.aem.ws.booking.widgetservice.xsd1.UpdateWidgetResponse;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.CashAndMiles;

@Component(immediate=true, metatype=false)
@Service(value=UpdateWidgetResponseToUpdateWidgetCashMilesResponse.class)
public class UpdateWidgetResponseToUpdateWidgetCashMilesResponse implements Converter<UpdateWidgetResponse, UpdateWidgetCashMilesResponse> {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Reference
	private CashAndMilesToCashAndMilesData cashAndMilesConverter;

	@Reference
	private BookingWidgetServiceJAXBContextFactory jaxbContextFactory;

	@SuppressWarnings("unchecked")
	@Override
	public UpdateWidgetCashMilesResponse convert(UpdateWidgetResponse source) {
		UpdateWidgetCashMilesResponse destination = new UpdateWidgetCashMilesResponse();

		destination.setSucceded(source.isIsSucceded());
		if (source.getWidget() != null && 
				source.getWidget().getValue() != null) {
			CashAndMiles castedWidget = null;
			Object widget = source.getWidget().getValue();

			if (widget instanceof CashAndMiles) {
				castedWidget = (CashAndMiles) widget;
			} else {
				try {
					Unmarshaller widgetUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
					castedWidget = (CashAndMiles) 
							((JAXBElement<CashAndMiles>) widgetUnmarshaller.unmarshal((Node) widget)).getValue();
				} catch (JAXBException e) {
					logger.error("Error while creating JAXBConext/Unmarshller for CashAndMiles: {}", e);
				}
			}

			destination.setCashMiles(cashAndMilesConverter.convert(castedWidget));
		}
		else
			destination.setCashMiles(null);

		return destination;
	}

}
