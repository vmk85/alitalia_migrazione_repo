package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.InfoPassengerCreatePNRResponse;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd1.CreatePNRResponse;

@Component(immediate=true, metatype=false)
@Service(value=CreatePNRResponseToInfoPassengerCreatePNRResponse.class)
public class CreatePNRResponseToInfoPassengerCreatePNRResponse 
		implements Converter<CreatePNRResponse, InfoPassengerCreatePNRResponse> {

	@Reference
	private RoutesToRoutesData routesToRoutesDataConverter;
	
	@Override
	public InfoPassengerCreatePNRResponse convert(CreatePNRResponse source) {

		InfoPassengerCreatePNRResponse destination = null;
		
		if (source != null) {
		
			destination = new InfoPassengerCreatePNRResponse();
			
			destination.setPrenotation(
					routesToRoutesDataConverter.convert(
							source.getPrenotation().getValue()));
			
			destination.setCookie(source.getCookie().getValue());
			destination.setExecute(source.getExecution().getValue());
			destination.setSabreGateWayAuthToken(source.getSabreGateWayAuthToken().getValue());
			
		}
		
		return destination;
	}

}
