package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.BrandPenaltyData;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.BrandPenalty;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=BrandPenaltyDataToBrandPenalty.class)
public class BrandPenaltyDataToBrandPenalty implements Converter<BrandPenaltyData, BrandPenalty> {

	@Override
	public BrandPenalty convert(BrandPenaltyData source) {
		ObjectFactory objectFactory = new ObjectFactory(); 
		BrandPenalty destination = objectFactory.createBrandPenalty();

		destination.setMaxPrice(objectFactory.createBrandPenaltyMaxPrice(source.getMaxPrice()));
		destination.setMaxPriceCurrency(objectFactory.createBrandPenaltyMaxPriceCurrency(source.getMaxPriceCurrency()));
		destination.setMinPrice(objectFactory.createBrandPenaltyMinPrice(source.getMinPrice()));
		destination.setMinPriceCurrency(objectFactory.createBrandPenaltyMinPriceCurrency(source.getMinPriceCurrency()));
		destination.setPermitted(source.isPermitted());

		return destination;
	}

}
