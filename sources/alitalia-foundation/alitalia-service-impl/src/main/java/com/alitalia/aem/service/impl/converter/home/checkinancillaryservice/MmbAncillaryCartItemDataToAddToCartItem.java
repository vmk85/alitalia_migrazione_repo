package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.MmbAncillaryDetail;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryUpgradeDetailData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryCartItemData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryExtraBaggageDetailData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryMealDetailData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillarySeatDetailData;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.AddToCartItem;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ObjectFactory;


@Component(immediate=true, metatype=false)
@Service(value=MmbAncillaryCartItemDataToAddToCartItem.class)
public class MmbAncillaryCartItemDataToAddToCartItem implements Converter<MmbAncillaryCartItemData, AddToCartItem> {

	@Reference
	private MmbAncillaryExtraBaggageDetailDataToExtraBaggageDetail mmbAncillaryExtraBaggageDetailDataConverter;

	@Reference
	private MmbAncillarySeatDetailDataToSeatDetail mmbAncillarySeatDetailDataConverter;

	@Reference
	private MmbAncillaryMealDetailDataToMealDetail mmbAncillaryMealDetailDataConverter;
	
	@Reference
	private CheckinAncillaryUpgradeDetailDataToUpgradeDetail ancillaryUpgradeDetailDataConverter;

	@Override
	public AddToCartItem convert(MmbAncillaryCartItemData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		AddToCartItem destination = null;

		if (source != null) {
			destination = objectFactory.createAddToCartItem();

			destination.setX003CAncillaryIdX003EKBackingField(source.getAncillaryId());
			destination.setX003CQuantityX003EKBackingField(source.getQuantity());

			Object ancillaryDetail = null;
			MmbAncillaryDetail sourceAncillaryDetail = source.getAncillaryDetail();
			if (sourceAncillaryDetail != null) {
				if (sourceAncillaryDetail instanceof MmbAncillaryExtraBaggageDetailData) {
					ancillaryDetail = mmbAncillaryExtraBaggageDetailDataConverter.convert(
							(MmbAncillaryExtraBaggageDetailData) sourceAncillaryDetail);
				} else if (sourceAncillaryDetail instanceof MmbAncillarySeatDetailData) {
					ancillaryDetail = mmbAncillarySeatDetailDataConverter.convert(
							(MmbAncillarySeatDetailData) sourceAncillaryDetail);
				} else if (sourceAncillaryDetail instanceof MmbAncillaryMealDetailData) {
					ancillaryDetail = mmbAncillaryMealDetailDataConverter.convert(
							(MmbAncillaryMealDetailData) sourceAncillaryDetail);
				} else if (sourceAncillaryDetail instanceof CheckinAncillaryUpgradeDetailData) {
					ancillaryDetail = ancillaryUpgradeDetailDataConverter.convert(
							(CheckinAncillaryUpgradeDetailData) sourceAncillaryDetail);
				}
			}
			destination.setX003CAncillaryDetailX003EKBackingField(ancillaryDetail);
		}
		

		return destination;
	}

}
