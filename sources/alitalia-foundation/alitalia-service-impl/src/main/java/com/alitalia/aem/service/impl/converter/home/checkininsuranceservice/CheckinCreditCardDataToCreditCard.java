package com.alitalia.aem.service.impl.converter.home.checkininsuranceservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinCreditCardData;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd2.CreditCardData;



@Component(immediate=true, metatype=false)
@Service(value=CheckinCreditCardDataToCreditCard.class)
public class CheckinCreditCardDataToCreditCard implements Converter<CheckinCreditCardData, CreditCardData> {	

	@Reference
	private CheckinCreditCardHolderDataToCreditCardHolderData checkinCreditCardHolderDataConverter;
	
	@Override
	public CreditCardData convert(CheckinCreditCardData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		CreditCardData destination = null;

		if (source != null) {
			destination = objectFactory.createCreditCardData();
			if (source.getId() != null){
				destination.setX003CIdX003EKBackingField(source.getId());
			}
			
			destination.setX003CCVVX003EKBackingField(source.getCvv());
			destination.setX003CCreditCardHolderDataX003EKBackingField(checkinCreditCardHolderDataConverter.convert(source.getCreditCardHolderData()));
			if(source.getExpireMonth() != null){
				destination.setX003CExpireMonthX003EKBackingField(source.getExpireMonth());
			}
			if(source.getExpireYear() != null){
				destination.setX003CExpireYearX003EKBackingField(source.getExpireYear());
			}
			if(source.getIs3DSecure() != null){
				destination.setX003CIs3DSecureX003EKBackingField(source.getIs3DSecure());
			}
			destination.setX003CNumberX003EKBackingField(source.getNumber());
			destination.setX003CThreeDSecureOpaqueParams64EncodedX003EKBackingField(source.getThreeDSecureOpaqueParams64Encoded());
			destination.setX003CTypeX003EKBackingField(source.getType());
			
		}
		return destination;
	}

}
