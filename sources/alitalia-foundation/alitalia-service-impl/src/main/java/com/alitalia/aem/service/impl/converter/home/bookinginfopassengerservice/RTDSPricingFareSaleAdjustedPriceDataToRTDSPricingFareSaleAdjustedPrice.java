package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareSaleAdjustedPriceData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionPricingFareSaleAdjustedPrice;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareSaleAdjustedPriceDataToRTDSPricingFareSaleAdjustedPrice.class)
public class RTDSPricingFareSaleAdjustedPriceDataToRTDSPricingFareSaleAdjustedPrice implements
		Converter<ResultTicketingDetailSolutionPricingFareSaleAdjustedPriceData, 
					ResultTicketingDetailSolutionPricingFareSaleAdjustedPrice> {

	@Override
	public ResultTicketingDetailSolutionPricingFareSaleAdjustedPrice convert(
			ResultTicketingDetailSolutionPricingFareSaleAdjustedPriceData source) {
		ResultTicketingDetailSolutionPricingFareSaleAdjustedPrice destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingFareSaleAdjustedPrice();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
