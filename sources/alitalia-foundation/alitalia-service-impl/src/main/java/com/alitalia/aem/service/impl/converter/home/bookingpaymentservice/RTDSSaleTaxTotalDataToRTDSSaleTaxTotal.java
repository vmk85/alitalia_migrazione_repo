package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSaleTaxTotalData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionSaleTaxTotal;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSaleTaxTotalDataToRTDSSaleTaxTotal.class)
public class RTDSSaleTaxTotalDataToRTDSSaleTaxTotal implements
		Converter<ResultTicketingDetailSolutionSaleTaxTotalData, 
					ResultTicketingDetailSolutionSaleTaxTotal> {

	@Override
	public ResultTicketingDetailSolutionSaleTaxTotal convert(ResultTicketingDetailSolutionSaleTaxTotalData source) {
		ResultTicketingDetailSolutionSaleTaxTotal destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionSaleTaxTotal();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
