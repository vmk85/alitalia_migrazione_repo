package com.alitalia.aem.service.impl.converter.home.bookingbagsandrulesservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingPenaltyPriceData;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetailsSolutionPricingPenaltyPrice;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingPenaltyPriceToPricingPenaltyPriceData.class)
public class RBDSPricingPenaltyPriceToPricingPenaltyPriceData implements
		Converter<ResultBookingDetailsSolutionPricingPenaltyPrice, ResultBookingDetailsSolutionPricingPenaltyPriceData> {

	@Override
	public ResultBookingDetailsSolutionPricingPenaltyPriceData convert(
			ResultBookingDetailsSolutionPricingPenaltyPrice source) {
		ResultBookingDetailsSolutionPricingPenaltyPriceData destination = null;
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingPenaltyPriceData();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}
	
	public ResultBookingDetailsSolutionPricingPenaltyPriceData convert(
			com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetailsSolutionPricingPenaltyPrice source) {
		ResultBookingDetailsSolutionPricingPenaltyPriceData destination = null;
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingPenaltyPriceData();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}

}
