package com.alitalia.aem.service.impl.converter.home.checkincomfortseatservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.UpgradePolicyCheckinRequest;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.Resource;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.UpgradePolicyRequest;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd2.CompartimentalClass;


@Component(immediate=true, metatype=false)
@Service(value=UpgradePolicyCheckinRequestToUpgradePolicyRequest.class)
public class UpgradePolicyCheckinRequestToUpgradePolicyRequest implements Converter<UpgradePolicyCheckinRequest, UpgradePolicyRequest> {

	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;
	
	@Reference
	private CheckinFlightDataToFlight checkinFlightDataConverter;
	

	@Override
	public UpgradePolicyRequest convert(UpgradePolicyCheckinRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		UpgradePolicyRequest destination = null;

		if (source != null) {
			destination = objectFactory.createUpgradePolicyRequest();
			
			destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			destination.setX003CResourceX003EKBackingField(Resource.WEB);
				
			if(source.getCompartClass() != null){
				destination.setCompartClass(CompartimentalClass.fromValue(source.getCompartClass().value()));
			}
			destination.setFlight(objectFactory.createChangeCabinRequestFlight(checkinFlightDataConverter.convert(source.getFlight())));
			destination.setMarketCode(objectFactory.createUpgradePolicyRequestMarketCode(source.getMarketCode()));

		}

		return destination;
	}


}
