package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AvailableFlightsData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd1.SearchResponse;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd14.AvailableFlights;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd2.ModelResponse;

@Component(immediate=true, metatype=false)
@Service(value=SearchResponseToAvailableFlightsData.class)
public class SearchResponseToAvailableFlightsData implements Converter<SearchResponse, AvailableFlightsData> {

	private static final Logger logger = LoggerFactory.getLogger(SearchResponseToAvailableFlightsData.class);

	@Reference
	private AvailableFlightsToAvailableFlightsData availableFlightsConverter;

	@Override
	public AvailableFlightsData convert(SearchResponse source) {
		AvailableFlightsData destination;

		logger.debug("Converting from SearchResponse to AvailableFlightsData class.");
		ModelResponse result = (ModelResponse) source.getResult().getValue();
		AvailableFlights flights = (AvailableFlights) result.getModel().getValue();
		destination = availableFlightsConverter.convert(flights);

		return destination;
	}

}
