package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.MmbAncillaryErrorData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryBusinessErrorData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryDataAccessErrorData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryServiceClientErrorData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryServiceErrorData;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.BusinessError;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.DataAccessError;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ServiceClientError;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ServiceError;


@Component(immediate=true, metatype=false)
@Service(value=ErrorToMmbAncillaryErrorData.class)
public class ErrorToMmbAncillaryErrorData implements Converter<Object, MmbAncillaryErrorData> {

	@Override
	public MmbAncillaryErrorData convert(Object source) {
		MmbAncillaryErrorData destination = null;

		if (source != null) {
			if (source instanceof BusinessError) {
				destination = new MmbAncillaryBusinessErrorData();
				destination.setCode(((BusinessError) source).getX003CCodeX003EKBackingField());
				destination.setDescription(((BusinessError) source).getX003CDescriptionX003EKBackingField());
			} else if (source instanceof DataAccessError) {
				destination = new MmbAncillaryDataAccessErrorData();
				destination.setCode(((DataAccessError) source).getX003CCodeX003EKBackingField());
				destination.setDescription(((DataAccessError) source).getX003CDescriptionX003EKBackingField());
			} else if (source instanceof ServiceClientError) {
				destination = new MmbAncillaryServiceClientErrorData();
				destination.setCode(((ServiceClientError) source).getX003CCodeX003EKBackingField());
				destination.setDescription(((ServiceClientError) source).getX003CDescriptionX003EKBackingField());
			} else if (source instanceof ServiceError) {
				destination = new MmbAncillaryServiceErrorData();
				destination.setCode(((ServiceError) source).getX003CCodeX003EKBackingField());
				destination.setDescription(((ServiceError) source).getX003CDescriptionX003EKBackingField());
			}
		}

		return destination;
	}

}
