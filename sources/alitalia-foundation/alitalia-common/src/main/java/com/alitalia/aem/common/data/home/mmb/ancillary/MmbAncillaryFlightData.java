package com.alitalia.aem.common.data.home.mmb.ancillary;

import java.util.Calendar;

import com.alitalia.aem.common.data.home.enumerations.MmbFlightTypeEnum;

public class MmbAncillaryFlightData {

	private String carrier;
	private Calendar departureDate;
	private String destination;
	private MmbFlightTypeEnum flightType;
	private Integer id;
	private String number;
	private String operationalFlightCarrier;
	private String operationalFlightNumber;
	private String origin;
	private Integer referenceNumber;

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public Calendar getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Calendar departureDate) {
		this.departureDate = departureDate;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public MmbFlightTypeEnum getFlightType() {
		return flightType;
	}

	public void setFlightType(MmbFlightTypeEnum flightType) {
		this.flightType = flightType;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getOperationalFlightCarrier() {
		return operationalFlightCarrier;
	}

	public void setOperationalFlightCarrier(String operationalFlightCarrier) {
		this.operationalFlightCarrier = operationalFlightCarrier;
	}

	public String getOperationalFlightNumber() {
		return operationalFlightNumber;
	}

	public void setOperationalFlightNumber(String operationalFlightNumber) {
		this.operationalFlightNumber = operationalFlightNumber;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public Integer getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(Integer referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((carrier == null) ? 0 : carrier.hashCode());
		result = prime * result
				+ ((departureDate == null) ? 0 : departureDate.hashCode());
		result = prime * result
				+ ((destination == null) ? 0 : destination.hashCode());
		result = prime * result
				+ ((flightType == null) ? 0 : flightType.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime
				* result
				+ ((operationalFlightCarrier == null) ? 0
						: operationalFlightCarrier.hashCode());
		result = prime
				* result
				+ ((operationalFlightNumber == null) ? 0
						: operationalFlightNumber.hashCode());
		result = prime * result + ((origin == null) ? 0 : origin.hashCode());
		result = prime * result
				+ ((referenceNumber == null) ? 0 : referenceNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbAncillaryFlightData other = (MmbAncillaryFlightData) obj;
		if (carrier == null) {
			if (other.carrier != null)
				return false;
		} else if (!carrier.equals(other.carrier))
			return false;
		if (departureDate == null) {
			if (other.departureDate != null)
				return false;
		} else if (!departureDate.equals(other.departureDate))
			return false;
		if (destination == null) {
			if (other.destination != null)
				return false;
		} else if (!destination.equals(other.destination))
			return false;
		if (flightType != other.flightType)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (operationalFlightCarrier == null) {
			if (other.operationalFlightCarrier != null)
				return false;
		} else if (!operationalFlightCarrier
				.equals(other.operationalFlightCarrier))
			return false;
		if (operationalFlightNumber == null) {
			if (other.operationalFlightNumber != null)
				return false;
		} else if (!operationalFlightNumber
				.equals(other.operationalFlightNumber))
			return false;
		if (origin == null) {
			if (other.origin != null)
				return false;
		} else if (!origin.equals(other.origin))
			return false;
		if (referenceNumber == null) {
			if (other.referenceNumber != null)
				return false;
		} else if (!referenceNumber.equals(other.referenceNumber))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbAncillaryFlightData [carrier=" + carrier
				+ ", departureDate=" + departureDate + ", destination="
				+ destination + ", flightType=" + flightType + ", id=" + id
				+ ", number=" + number + ", operationalFlightCarrier="
				+ operationalFlightCarrier + ", operationalFlightNumber="
				+ operationalFlightNumber + ", origin=" + origin
				+ ", referenceNumber=" + referenceNumber + "]";
	}

}
