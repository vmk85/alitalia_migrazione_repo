package com.alitalia.aem.common.data.home;

import java.util.List;

public class ResultBookingDetailsSolutionPricingFareData {
	
	private String carrierField;
	private String destinationCityField;
	private String extendedFareCodeField;
	private String originCityField;
	private List<String> ptcField;
	private String tagField;
	private String ticketDesignatorField;
	
	public String getCarrierField() {
		return carrierField;
	}
	
	public void setCarrierField(String carrierField) {
		this.carrierField = carrierField;
	}
	
	public String getDestinationCityField() {
		return destinationCityField;
	}
	
	public void setDestinationCityField(String destinationCityField) {
		this.destinationCityField = destinationCityField;
	}
	
	public String getExtendedFareCodeField() {
		return extendedFareCodeField;
	}
	
	public void setExtendedFareCodeField(String extendedFareCodeField) {
		this.extendedFareCodeField = extendedFareCodeField;
	}
	
	public String getOriginCityField() {
		return originCityField;
	}
	
	public void setOriginCityField(String originCityField) {
		this.originCityField = originCityField;
	}
	
	public List<String> getPtcField() {
		return ptcField;
	}
	
	public void setPtcField(List<String> ptcField) {
		this.ptcField = ptcField;
	}
	
	public String getTagField() {
		return tagField;
	}
	
	public void setTagField(String tagField) {
		this.tagField = tagField;
	}
	
	public String getTicketDesignatorField() {
		return ticketDesignatorField;
	}
	
	public void setTicketDesignatorField(String ticketDesignatorField) {
		this.ticketDesignatorField = ticketDesignatorField;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((carrierField == null) ? 0 : carrierField.hashCode());
		result = prime
				* result
				+ ((destinationCityField == null) ? 0 : destinationCityField
						.hashCode());
		result = prime
				* result
				+ ((extendedFareCodeField == null) ? 0 : extendedFareCodeField
						.hashCode());
		result = prime * result
				+ ((originCityField == null) ? 0 : originCityField.hashCode());
		result = prime * result
				+ ((ptcField == null) ? 0 : ptcField.hashCode());
		result = prime * result
				+ ((tagField == null) ? 0 : tagField.hashCode());
		result = prime
				* result
				+ ((ticketDesignatorField == null) ? 0 : ticketDesignatorField
						.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingDetailsSolutionPricingFareData other = (ResultBookingDetailsSolutionPricingFareData) obj;
		if (carrierField == null) {
			if (other.carrierField != null)
				return false;
		} else if (!carrierField.equals(other.carrierField))
			return false;
		if (destinationCityField == null) {
			if (other.destinationCityField != null)
				return false;
		} else if (!destinationCityField.equals(other.destinationCityField))
			return false;
		if (extendedFareCodeField == null) {
			if (other.extendedFareCodeField != null)
				return false;
		} else if (!extendedFareCodeField.equals(other.extendedFareCodeField))
			return false;
		if (originCityField == null) {
			if (other.originCityField != null)
				return false;
		} else if (!originCityField.equals(other.originCityField))
			return false;
		if (ptcField == null) {
			if (other.ptcField != null)
				return false;
		} else if (!ptcField.equals(other.ptcField))
			return false;
		if (tagField == null) {
			if (other.tagField != null)
				return false;
		} else if (!tagField.equals(other.tagField))
			return false;
		if (ticketDesignatorField == null) {
			if (other.ticketDesignatorField != null)
				return false;
		} else if (!ticketDesignatorField.equals(other.ticketDesignatorField))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingDetailsSolutionPricingFareData [carrierField="
				+ carrierField + ", destinationCityField="
				+ destinationCityField + ", extendedFareCodeField="
				+ extendedFareCodeField + ", originCityField="
				+ originCityField + ", ptcField=" + ptcField + ", tagField="
				+ tagField + ", ticketDesignatorField=" + ticketDesignatorField
				+ "]";
	}
	
}
