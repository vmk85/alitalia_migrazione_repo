package com.alitalia.aem.common.data.home;

import java.util.List;

import com.alitalia.aem.common.data.home.enumerations.PaymentStepEnum;
import com.alitalia.aem.common.data.home.enumerations.ResultTypeEnum;

public class PaymentProcessInfoData {
	
    private List<FraudNetParamData> fraudNetParams;
    private String mac;
	private String marketCode;
	private String paRes;
	private Integer paymentAttemps;
	private String paymentRef;
	private String pointOfSale;
	private String redirectUrl;
	private ResultTypeEnum result;
	private String sessionId;
	private String shopId;
	private String siteCode;
	private PaymentStepEnum step;
	private String tokenId;
	private String transactionId;
	private String merchantId;
	private String responseCode;
	private String resultCode;
	private String supplierId;
	
	public String getMac() {
		return mac;
	}
	public void setMac(String mac) {
		this.mac = mac;
	}
	public String getMarketCode() {
		return marketCode;
	}
	public void setMarketCode(String marketCode) {
		this.marketCode = marketCode;
	}
	public String getPaRes() {
		return paRes;
	}
	public void setPaRes(String paRes) {
		this.paRes = paRes;
	}
	public Integer getPaymentAttemps() {
		return paymentAttemps;
	}
	public void setPaymentAttemps(Integer paymentAttemps) {
		this.paymentAttemps = paymentAttemps;
	}
	public String getPointOfSale() {
		return pointOfSale;
	}
	public void setPointOfSale(String pointOfSale) {
		this.pointOfSale = pointOfSale;
	}
	public String getRedirectUrl() {
		return redirectUrl;
	}
	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
	public ResultTypeEnum getResult() {
		return result;
	}
	public void setResult(ResultTypeEnum result) {
		this.result = result;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getShopId() {
		return shopId;
	}
	public void setShopId(String shopId) {
		this.shopId = shopId;
	}
	public String getSiteCode() {
		return siteCode;
	}
	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}
	public PaymentStepEnum getStep() {
		return step;
	}
	public void setStep(PaymentStepEnum step) {
		this.step = step;
	}
	public String getTokenId() {
		return tokenId;
	}
	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	public List<FraudNetParamData> getFraudNetParams() {
		return fraudNetParams;
	}
	public void setFraudNetParams(List<FraudNetParamData> fraudNetParams) {
		this.fraudNetParams = fraudNetParams;
	}
	public String getPaymentRef() {
		return paymentRef;
	}
	public void setPaymentRef(String paymentRef) {
		this.paymentRef = paymentRef;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((fraudNetParams == null) ? 0 : fraudNetParams.hashCode());
		result = prime * result + ((mac == null) ? 0 : mac.hashCode());
		result = prime * result
				+ ((marketCode == null) ? 0 : marketCode.hashCode());
		result = prime * result
				+ ((merchantId == null) ? 0 : merchantId.hashCode());
		result = prime * result + ((paRes == null) ? 0 : paRes.hashCode());
		result = prime * result
				+ ((paymentAttemps == null) ? 0 : paymentAttemps.hashCode());
		result = prime * result
				+ ((paymentRef == null) ? 0 : paymentRef.hashCode());
		result = prime * result
				+ ((pointOfSale == null) ? 0 : pointOfSale.hashCode());
		result = prime * result
				+ ((redirectUrl == null) ? 0 : redirectUrl.hashCode());
		result = prime * result
				+ ((responseCode == null) ? 0 : responseCode.hashCode());
		result = prime * result
				+ ((this.result == null) ? 0 : this.result.hashCode());
		result = prime * result
				+ ((resultCode == null) ? 0 : resultCode.hashCode());
		result = prime * result
				+ ((sessionId == null) ? 0 : sessionId.hashCode());
		result = prime * result + ((shopId == null) ? 0 : shopId.hashCode());
		result = prime * result
				+ ((siteCode == null) ? 0 : siteCode.hashCode());
		result = prime * result + ((step == null) ? 0 : step.hashCode());
		result = prime * result
				+ ((supplierId == null) ? 0 : supplierId.hashCode());
		result = prime * result + ((tokenId == null) ? 0 : tokenId.hashCode());
		result = prime * result
				+ ((transactionId == null) ? 0 : transactionId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentProcessInfoData other = (PaymentProcessInfoData) obj;
		if (fraudNetParams == null) {
			if (other.fraudNetParams != null)
				return false;
		} else if (!fraudNetParams.equals(other.fraudNetParams))
			return false;
		if (mac == null) {
			if (other.mac != null)
				return false;
		} else if (!mac.equals(other.mac))
			return false;
		if (marketCode == null) {
			if (other.marketCode != null)
				return false;
		} else if (!marketCode.equals(other.marketCode))
			return false;
		if (merchantId == null) {
			if (other.merchantId != null)
				return false;
		} else if (!merchantId.equals(other.merchantId))
			return false;
		if (paRes == null) {
			if (other.paRes != null)
				return false;
		} else if (!paRes.equals(other.paRes))
			return false;
		if (paymentAttemps == null) {
			if (other.paymentAttemps != null)
				return false;
		} else if (!paymentAttemps.equals(other.paymentAttemps))
			return false;
		if (paymentRef == null) {
			if (other.paymentRef != null)
				return false;
		} else if (!paymentRef.equals(other.paymentRef))
			return false;
		if (pointOfSale == null) {
			if (other.pointOfSale != null)
				return false;
		} else if (!pointOfSale.equals(other.pointOfSale))
			return false;
		if (redirectUrl == null) {
			if (other.redirectUrl != null)
				return false;
		} else if (!redirectUrl.equals(other.redirectUrl))
			return false;
		if (responseCode == null) {
			if (other.responseCode != null)
				return false;
		} else if (!responseCode.equals(other.responseCode))
			return false;
		if (result != other.result)
			return false;
		if (resultCode == null) {
			if (other.resultCode != null)
				return false;
		} else if (!resultCode.equals(other.resultCode))
			return false;
		if (sessionId == null) {
			if (other.sessionId != null)
				return false;
		} else if (!sessionId.equals(other.sessionId))
			return false;
		if (shopId == null) {
			if (other.shopId != null)
				return false;
		} else if (!shopId.equals(other.shopId))
			return false;
		if (siteCode == null) {
			if (other.siteCode != null)
				return false;
		} else if (!siteCode.equals(other.siteCode))
			return false;
		if (step != other.step)
			return false;
		if (supplierId == null) {
			if (other.supplierId != null)
				return false;
		} else if (!supplierId.equals(other.supplierId))
			return false;
		if (tokenId == null) {
			if (other.tokenId != null)
				return false;
		} else if (!tokenId.equals(other.tokenId))
			return false;
		if (transactionId == null) {
			if (other.transactionId != null)
				return false;
		} else if (!transactionId.equals(other.transactionId))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "PaymentProcessInfoData [fraudNetParams=" + fraudNetParams
				+ ", mac=" + mac + ", marketCode=" + marketCode + ", paRes="
				+ paRes + ", paymentAttemps=" + paymentAttemps
				+ ", paymentRef=" + paymentRef + ", pointOfSale=" + pointOfSale
				+ ", redirectUrl=" + redirectUrl + ", result=" + result
				+ ", sessionId=" + sessionId + ", shopId=" + shopId
				+ ", siteCode=" + siteCode + ", step=" + step + ", tokenId="
				+ tokenId + ", transactionId=" + transactionId
				+ ", merchantId=" + merchantId + ", responseCode="
				+ responseCode + ", resultCode=" + resultCode + ", supplierId="
				+ supplierId + ", getClass()=" + getClass() + ", toString()="
				+ super.toString() + "]";
	}
}