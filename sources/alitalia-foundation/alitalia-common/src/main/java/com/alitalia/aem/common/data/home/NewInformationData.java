package com.alitalia.aem.common.data.home;

import com.alitalia.aem.common.data.home.enumerations.WorkPositionTypesEnum;

public class NewInformationData {

	private String pin;
	private boolean smsAuthorization;
	private boolean smsAuthorizationUpdated;
	private WorkPositionTypesEnum workPosition;
	private boolean workPositionUpdated;
	
	public String getPin() {
		return pin;
	}
	
	public void setPin(String pin) {
		this.pin = pin;
	}
	
	public boolean isSmsAuthorization() {
		return smsAuthorization;
	}
	
	public void setSmsAuthorization(boolean smsAuthorization) {
		this.smsAuthorization = smsAuthorization;
	}
	
	public boolean isSmsAuthorizationUpdated() {
		return smsAuthorizationUpdated;
	}
	
	public void setSmsAuthorizationUpdated(boolean smsAuthorizationUpdated) {
		this.smsAuthorizationUpdated = smsAuthorizationUpdated;
	}
	
	public WorkPositionTypesEnum getWorkPosition() {
		return workPosition;
	}
	
	public void setWorkPosition(WorkPositionTypesEnum workPosition) {
		this.workPosition = workPosition;
	}
	
	public boolean isWorkPositionUpdated() {
		return workPositionUpdated;
	}
	
	public void setWorkPositionUpdated(boolean workPositionUpdated) {
		this.workPositionUpdated = workPositionUpdated;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pin == null) ? 0 : pin.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewInformationData other = (NewInformationData) obj;
		if (pin == null) {
			if (other.pin != null)
				return false;
		} else if (!pin.equals(other.pin))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NewsInformationData [pin=" + pin + ", smsAuthorization="
				+ smsAuthorization + ", smsAuthorizationUpdated="
				+ smsAuthorizationUpdated + ", workPosition=" + workPosition
				+ ", workPositionUpdated=" + workPositionUpdated + "]";
	}
}