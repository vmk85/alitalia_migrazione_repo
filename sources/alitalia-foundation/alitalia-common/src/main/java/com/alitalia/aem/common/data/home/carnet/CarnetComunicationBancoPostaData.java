package com.alitalia.aem.common.data.home.carnet;

import com.alitalia.aem.common.data.home.enumerations.BancoPostaTypeEnum;

public class CarnetComunicationBancoPostaData extends CarnetComunicationData {

	private String errorUrl;
    private BancoPostaTypeEnum type;
    
	public String getErrorUrl() {
		return errorUrl;
	}
	public void setErrorUrl(String errorUrl) {
		this.errorUrl = errorUrl;
	}
	public BancoPostaTypeEnum getType() {
		return type;
	}
	public void setType(BancoPostaTypeEnum type) {
		this.type = type;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((errorUrl == null) ? 0 : errorUrl.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarnetComunicationBancoPostaData other = (CarnetComunicationBancoPostaData) obj;
		if (errorUrl == null) {
			if (other.errorUrl != null)
				return false;
		} else if (!errorUrl.equals(other.errorUrl))
			return false;
		if (type != other.type)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CarnetComunicationBancoPostaData [errorUrl=" + errorUrl
				+ ", type=" + type + ", getDescription()=" + getDescription()
				+ ", getLanguageCode()=" + getLanguageCode()
				+ ", getReturnUrl()=" + getReturnUrl() + "]";
	}
}