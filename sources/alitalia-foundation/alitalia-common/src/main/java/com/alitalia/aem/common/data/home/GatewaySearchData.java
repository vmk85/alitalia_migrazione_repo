package com.alitalia.aem.common.data.home;

import java.util.List;

import com.alitalia.aem.common.data.PassengerBase;
import com.alitalia.aem.common.data.home.enumerations.GatewayTypeEnum;
import com.alitalia.aem.common.messages.SearchExecuteRequestFilter;

public abstract class GatewaySearchData extends BoomBoxData implements SearchExecuteRequestFilter {

	private List<PassengerBase> passengers;
	private GatewayTypeEnum type;

	public GatewaySearchData() {
		super();
	}

	public List<PassengerBase> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<PassengerBase> passengers) {
		this.passengers = passengers;
	}

	public GatewayTypeEnum getType() {
		return type;
	}

	public void setType(GatewayTypeEnum type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "GatewaySearchData [passengers=" + passengers + ", type=" + type
				+ ", getId()=" + getId() + ", getSessionId()=" + getSessionId()
				+ ", getSolutionSet()=" + getSolutionSet()
				+ ", getSolutionId()=" + getSolutionId()
				+ ", getRefreshSolutionId()=" + getRefreshSolutionId()
				+ ", getFarmId()=" + getFarmId() + ", getProperties()="
				+ getProperties() + "]";
	}

}