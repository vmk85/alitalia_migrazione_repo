package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.messages.BaseResponse;

public class WebCheckinUndoCheckinResponse extends BaseResponse {

	private List<String> messages;
	
	public List<String> getMessages() {
		return messages;
	}
	public void setMessages(List<String> messages) {
		this.messages = messages;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((messages == null) ? 0 : messages.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebCheckinUndoCheckinResponse other = (WebCheckinUndoCheckinResponse) obj;
		if (messages == null) {
			if (other.messages != null)
				return false;
		} else if (!messages.equals(other.messages))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "WebCheckinUndoCheckinResponse [messages=" + messages
				+ ", toString()=" + super.toString() + "]";
	}
}
