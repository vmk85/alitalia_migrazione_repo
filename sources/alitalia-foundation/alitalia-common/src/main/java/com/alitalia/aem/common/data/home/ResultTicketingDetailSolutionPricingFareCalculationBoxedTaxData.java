package com.alitalia.aem.common.data.home;

public class ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxData {

    private String countryField;
    private ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxPriceData priceField;
    private String taxCodeField;
    private String taxSubcodeField;

    public String getCountryField() {
        return countryField;
    }

    public void setCountryField(String value) {
        this.countryField = value;
    }

    public ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxPriceData getPriceField() {
        return priceField;
    }

    public void setPriceField(ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxPriceData value) {
        this.priceField = value;
    }

    public String getTaxCodeField() {
        return taxCodeField;
    }

    public void setTaxCodeField(String value) {
        this.taxCodeField = value;
    }

    public String getTaxSubcodeField() {
        return taxSubcodeField;
    }

    public void setTaxSubcodeField(String value) {
        this.taxSubcodeField = value;
    }

}
