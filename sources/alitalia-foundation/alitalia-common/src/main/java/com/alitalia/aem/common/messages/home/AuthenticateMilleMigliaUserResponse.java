package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.MmCustomerData;
import com.alitalia.aem.common.messages.BaseResponse;

public class AuthenticateMilleMigliaUserResponse extends BaseResponse {

	private int errorCode;
	private MmCustomerData customer;
	
	public int getErrorCode() {
		return errorCode;
	}
	
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	
	public MmCustomerData getCustomer() {
		return customer;
	}
	
	public void setCustomer(MmCustomerData customer) {
		this.customer = customer;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + errorCode;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthenticateMilleMigliaUserResponse other = (AuthenticateMilleMigliaUserResponse) obj;
		if (errorCode != other.errorCode)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AuthenticateMilleMigliaUserResponse [errorCode=" + errorCode + "]";
	}
}