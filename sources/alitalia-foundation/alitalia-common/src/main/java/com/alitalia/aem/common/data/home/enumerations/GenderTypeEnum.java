package com.alitalia.aem.common.data.home.enumerations;

public enum GenderTypeEnum {

    UNKNOWN("Unknown"),
    MALE("Male"),
    FEMALE("Female");
    private final String value;

    GenderTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GenderTypeEnum fromValue(String v) {
        for (GenderTypeEnum c: GenderTypeEnum.values()) {
            if (c.value.equalsIgnoreCase(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
