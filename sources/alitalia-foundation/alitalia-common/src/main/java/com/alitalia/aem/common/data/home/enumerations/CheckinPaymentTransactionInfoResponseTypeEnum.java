package com.alitalia.aem.common.data.home.enumerations;


public enum CheckinPaymentTransactionInfoResponseTypeEnum {

	AUTHORIZED("Authorized"),
	THREE_D_SECURE_NEEDED("ThreeDSecureNeeded"),
	FAILED("Failed"),
	AUTHORIZATION_DENIED("AuthorizationDenied"),
	PROVIDER_ERROR("ProviderError"),
	AUTHORIZATION_DATA_ERROR("AuthorizationDataError");
    
    private final String value;

    CheckinPaymentTransactionInfoResponseTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CheckinPaymentTransactionInfoResponseTypeEnum fromValue(String v) {
        for (CheckinPaymentTransactionInfoResponseTypeEnum c: CheckinPaymentTransactionInfoResponseTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
