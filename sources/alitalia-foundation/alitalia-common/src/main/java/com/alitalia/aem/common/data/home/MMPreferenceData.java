package com.alitalia.aem.common.data.home;

import java.util.Calendar;

public class MMPreferenceData {

	private String additionalText;
	private String description;
	private String idCategory;
	private String sequenceNumber;
	private String shortNameCategory;
	private String shortNamePreference;

	private Calendar endDate;
	private Calendar startDate;

	private Integer idPreference;

	public String getAdditionalText() {
		return additionalText;
	}

	public void setAdditionalText(String additionalText) {
		this.additionalText = additionalText;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(String idCategory) {
		this.idCategory = idCategory;
	}

	public String getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getShortNameCategory() {
		return shortNameCategory;
	}

	public void setShortNameCategory(String shortNameCategory) {
		this.shortNameCategory = shortNameCategory;
	}

	public String getShortNamePreference() {
		return shortNamePreference;
	}

	public void setShortNamePreference(String shortNamePreference) {
		this.shortNamePreference = shortNamePreference;
	}

	public Calendar getEndDate() {
		return endDate;
	}

	public void setEndDate(Calendar endDate) {
		this.endDate = endDate;
	}

	public Calendar getStartDate() {
		return startDate;
	}

	public void setStartDate(Calendar startDate) {
		this.startDate = startDate;
	}

	public Integer getIdPreference() {
		return idPreference;
	}

	public void setIdPreference(Integer idPreference) {
		this.idPreference = idPreference;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((additionalText == null) ? 0 : additionalText.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result
				+ ((idCategory == null) ? 0 : idCategory.hashCode());
		result = prime * result
				+ ((idPreference == null) ? 0 : idPreference.hashCode());
		result = prime * result
				+ ((sequenceNumber == null) ? 0 : sequenceNumber.hashCode());
		result = prime
				* result
				+ ((shortNameCategory == null) ? 0 : shortNameCategory
						.hashCode());
		result = prime
				* result
				+ ((shortNamePreference == null) ? 0 : shortNamePreference
						.hashCode());
		result = prime * result
				+ ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MMPreferenceData other = (MMPreferenceData) obj;
		if (additionalText == null) {
			if (other.additionalText != null)
				return false;
		} else if (!additionalText.equals(other.additionalText))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (idCategory == null) {
			if (other.idCategory != null)
				return false;
		} else if (!idCategory.equals(other.idCategory))
			return false;
		if (idPreference == null) {
			if (other.idPreference != null)
				return false;
		} else if (!idPreference.equals(other.idPreference))
			return false;
		if (sequenceNumber == null) {
			if (other.sequenceNumber != null)
				return false;
		} else if (!sequenceNumber.equals(other.sequenceNumber))
			return false;
		if (shortNameCategory == null) {
			if (other.shortNameCategory != null)
				return false;
		} else if (!shortNameCategory.equals(other.shortNameCategory))
			return false;
		if (shortNamePreference == null) {
			if (other.shortNamePreference != null)
				return false;
		} else if (!shortNamePreference.equals(other.shortNamePreference))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MMPreferenceData [additionalText=" + additionalText
				+ ", description=" + description + ", idCategory=" + idCategory
				+ ", sequenceNumber=" + sequenceNumber + ", shortNameCategory="
				+ shortNameCategory + ", shortNamePreference="
				+ shortNamePreference + ", endDate=" + endDate + ", startDate="
				+ startDate + ", idPreference=" + idPreference + "]";
	}

}
