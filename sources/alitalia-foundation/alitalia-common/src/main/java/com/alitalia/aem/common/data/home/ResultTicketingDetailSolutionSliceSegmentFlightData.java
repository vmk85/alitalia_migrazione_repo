package com.alitalia.aem.common.data.home;

public class ResultTicketingDetailSolutionSliceSegmentFlightData {

    private String carrierField;
    private Integer numberField;
    private Boolean numberFieldSpecified;

    public String getCarrierField() {
        return carrierField;
    }

    public void setCarrierField(String value) {
        this.carrierField = value;
    }

    public Integer getNumberField() {
        return numberField;
    }

    public void setNumberField(int value) {
        this.numberField = value;
    }

    public Boolean isNumberFieldSpecified() {
        return numberFieldSpecified;
    }

    public void setNumberFieldSpecified(Boolean value) {
        this.numberFieldSpecified = value;
    }

}
