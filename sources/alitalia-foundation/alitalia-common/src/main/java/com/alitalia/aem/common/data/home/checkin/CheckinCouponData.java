package com.alitalia.aem.common.data.home.checkin;

import java.util.Calendar;
import java.util.List;

import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbGdsTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbSeatStatusEnum;

public class CheckinCouponData{
	
	private Integer id;
	private byte[] barcode;
	private String boardingPassColor;
	private Boolean boardingPassLoaded;
	private Calendar boardingTime;
	private String boardingZone;
	private Boolean changeSeatEnabled;
	private Boolean changeSeatPayment;
	private String eticket;
	private Integer flightId;
	private CheckinFlightData flight;
	private String gate;
	private MmbGdsTypeEnum gdsCouponType;
	private Boolean comfortSeatPaid;
	private Boolean infantCoupon;
	private Boolean skyPriority;
	private String oldSeat;
	private MmbCompartimentalClassEnum oldSeatClass;
	private String orderNumber;
	private Integer passengerId;
	private String previousSeat;
	private String ssrCode;
	private String seatClassName;
	private MmbCompartimentalClassEnum seatClass;
	private String seat;
	private String sequenceNumber;
	private String serviceCooperationDetails;
	private String serviceOperatingDetails;
	private String socialCode;
	private String specialFare;
	private MmbSeatStatusEnum status;
	private String terminal;
	private List<MmbCompartimentalClassEnum> upgradeEnabledClasses;
	private Boolean upgradeEnabled;
	private Boolean upgradedAlready;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public byte[] getBarcode() {
		return barcode;
	}
	public void setBarcode(byte[] barcode) {
		this.barcode = barcode;
	}
	public String getBoardingPassColor() {
		return boardingPassColor;
	}
	public void setBoardingPassColor(String boardingPassColor) {
		this.boardingPassColor = boardingPassColor;
	}
	public Boolean getBoardingPassLoaded() {
		return boardingPassLoaded;
	}
	public void setBoardingPassLoaded(Boolean boardingPassLoaded) {
		this.boardingPassLoaded = boardingPassLoaded;
	}
	public Calendar getBoardingTime() {
		return boardingTime;
	}
	public void setBoardingTime(Calendar boardingTime) {
		this.boardingTime = boardingTime;
	}
	public String getBoardingZone() {
		return boardingZone;
	}
	public void setBoardingZone(String boardingZone) {
		this.boardingZone = boardingZone;
	}
	public Boolean getChangeSeatEnabled() {
		return changeSeatEnabled;
	}
	public void setChangeSeatEnabled(Boolean changeSeatEnabled) {
		this.changeSeatEnabled = changeSeatEnabled;
	}
	public Boolean getChangeSeatPayment() {
		return changeSeatPayment;
	}
	public void setChangeSeatPayment(Boolean changeSeatPayment) {
		this.changeSeatPayment = changeSeatPayment;
	}
	public String getEticket() {
		return eticket;
	}
	public void setEticket(String eticket) {
		this.eticket = eticket;
	}
	public Integer getFlightId() {
		return flightId;
	}
	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}
	public CheckinFlightData getFlight() {
		return flight;
	}
	public void setFlight(CheckinFlightData flight) {
		this.flight = flight;
	}
	public String getGate() {
		return gate;
	}
	public void setGate(String gate) {
		this.gate = gate;
	}
	public MmbGdsTypeEnum getGdsCouponType() {
		return gdsCouponType;
	}
	public void setGdsCouponType(MmbGdsTypeEnum gdsCouponType) {
		this.gdsCouponType = gdsCouponType;
	}
	public Boolean getComfortSeatPaid() {
		return comfortSeatPaid;
	}
	public void setComfortSeatPaid(Boolean comfortSeatPaid) {
		this.comfortSeatPaid = comfortSeatPaid;
	}
	public Boolean getInfantCoupon() {
		return infantCoupon;
	}
	public void setInfantCoupon(Boolean infantCoupon) {
		this.infantCoupon = infantCoupon;
	}
	public Boolean getSkyPriority() {
		return skyPriority;
	}
	public void setSkyPriority(Boolean skyPriority) {
		this.skyPriority = skyPriority;
	}
	public String getOldSeat() {
		return oldSeat;
	}
	public void setOldSeat(String oldSeat) {
		this.oldSeat = oldSeat;
	}
	public MmbCompartimentalClassEnum getOldSeatClass() {
		return oldSeatClass;
	}
	public void setOldSeatClass(MmbCompartimentalClassEnum oldSeatClass) {
		this.oldSeatClass = oldSeatClass;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public Integer getPassengerId() {
		return passengerId;
	}
	public void setPassengerId(Integer passengerId) {
		this.passengerId = passengerId;
	}
	public String getPreviousSeat() {
		return previousSeat;
	}
	public void setPreviousSeat(String previousSeat) {
		this.previousSeat = previousSeat;
	}
	public String getSsrCode() {
		return ssrCode;
	}
	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}
	public String getSeatClassName() {
		return seatClassName;
	}
	public void setSeatClassName(String seatClassName) {
		this.seatClassName = seatClassName;
	}
	public MmbCompartimentalClassEnum getSeatClass() {
		return seatClass;
	}
	public void setSeatClass(MmbCompartimentalClassEnum seatClass) {
		this.seatClass = seatClass;
	}
	public String getSeat() {
		return seat;
	}
	public void setSeat(String seat) {
		this.seat = seat;
	}
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	public String getServiceCooperationDetails() {
		return serviceCooperationDetails;
	}
	public void setServiceCooperationDetails(String serviceCooperationDetails) {
		this.serviceCooperationDetails = serviceCooperationDetails;
	}
	public String getServiceOperatingDetails() {
		return serviceOperatingDetails;
	}
	public void setServiceOperatingDetails(String serviceOperatingDetails) {
		this.serviceOperatingDetails = serviceOperatingDetails;
	}
	public String getSocialCode() {
		return socialCode;
	}
	public void setSocialCode(String socialCode) {
		this.socialCode = socialCode;
	}
	public String getSpecialFare() {
		return specialFare;
	}
	public void setSpecialFare(String specialFare) {
		this.specialFare = specialFare;
	}
	public MmbSeatStatusEnum getStatus() {
		return status;
	}
	public void setStatus(MmbSeatStatusEnum status) {
		this.status = status;
	}
	public String getTerminal() {
		return terminal;
	}
	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}
	public List<MmbCompartimentalClassEnum> getUpgradeEnabledClasses() {
		return upgradeEnabledClasses;
	}
	public void setUpgradeEnabledClasses(
			List<MmbCompartimentalClassEnum> upgradeEnabledClasses) {
		this.upgradeEnabledClasses = upgradeEnabledClasses;
	}
	public Boolean getUpgradeEnabled() {
		return upgradeEnabled;
	}
	public void setUpgradeEnabled(Boolean upgradeEnabled) {
		this.upgradeEnabled = upgradeEnabled;
	}
	public Boolean getUpgradedAlready() {
		return upgradedAlready;
	}
	public void setUpgradedAlready(Boolean upgradedAlready) {
		this.upgradedAlready = upgradedAlready;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((barcode == null) ? 0 : barcode.hashCode());
		result = prime
				* result
				+ ((boardingPassColor == null) ? 0 : boardingPassColor
						.hashCode());
		result = prime
				* result
				+ ((boardingPassLoaded == null) ? 0 : boardingPassLoaded
						.hashCode());
		result = prime * result
				+ ((boardingTime == null) ? 0 : boardingTime.hashCode());
		result = prime * result
				+ ((boardingZone == null) ? 0 : boardingZone.hashCode());
		result = prime
				* result
				+ ((changeSeatEnabled == null) ? 0 : changeSeatEnabled
						.hashCode());
		result = prime
				* result
				+ ((changeSeatPayment == null) ? 0 : changeSeatPayment
						.hashCode());
		result = prime * result
				+ ((comfortSeatPaid == null) ? 0 : comfortSeatPaid.hashCode());
		result = prime * result + ((eticket == null) ? 0 : eticket.hashCode());
		result = prime * result + ((flight == null) ? 0 : flight.hashCode());
		result = prime * result
				+ ((flightId == null) ? 0 : flightId.hashCode());
		result = prime * result + ((gate == null) ? 0 : gate.hashCode());
		result = prime * result
				+ ((gdsCouponType == null) ? 0 : gdsCouponType.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((infantCoupon == null) ? 0 : infantCoupon.hashCode());
		result = prime * result + ((oldSeat == null) ? 0 : oldSeat.hashCode());
		result = prime * result
				+ ((oldSeatClass == null) ? 0 : oldSeatClass.hashCode());
		result = prime * result
				+ ((orderNumber == null) ? 0 : orderNumber.hashCode());
		result = prime * result
				+ ((passengerId == null) ? 0 : passengerId.hashCode());
		result = prime * result
				+ ((previousSeat == null) ? 0 : previousSeat.hashCode());
		result = prime * result + ((seat == null) ? 0 : seat.hashCode());
		result = prime * result
				+ ((seatClass == null) ? 0 : seatClass.hashCode());
		result = prime * result
				+ ((seatClassName == null) ? 0 : seatClassName.hashCode());
		result = prime * result
				+ ((sequenceNumber == null) ? 0 : sequenceNumber.hashCode());
		result = prime
				* result
				+ ((serviceCooperationDetails == null) ? 0
						: serviceCooperationDetails.hashCode());
		result = prime
				* result
				+ ((serviceOperatingDetails == null) ? 0
						: serviceOperatingDetails.hashCode());
		result = prime * result
				+ ((skyPriority == null) ? 0 : skyPriority.hashCode());
		result = prime * result
				+ ((socialCode == null) ? 0 : socialCode.hashCode());
		result = prime * result
				+ ((specialFare == null) ? 0 : specialFare.hashCode());
		result = prime * result + ((ssrCode == null) ? 0 : ssrCode.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result
				+ ((terminal == null) ? 0 : terminal.hashCode());
		result = prime * result
				+ ((upgradeEnabled == null) ? 0 : upgradeEnabled.hashCode());
		result = prime
				* result
				+ ((upgradeEnabledClasses == null) ? 0 : upgradeEnabledClasses
						.hashCode());
		result = prime * result
				+ ((upgradedAlready == null) ? 0 : upgradedAlready.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinCouponData other = (CheckinCouponData) obj;
		if (barcode == null) {
			if (other.barcode != null)
				return false;
		} else if (!barcode.equals(other.barcode))
			return false;
		if (boardingPassColor == null) {
			if (other.boardingPassColor != null)
				return false;
		} else if (!boardingPassColor.equals(other.boardingPassColor))
			return false;
		if (boardingPassLoaded == null) {
			if (other.boardingPassLoaded != null)
				return false;
		} else if (!boardingPassLoaded.equals(other.boardingPassLoaded))
			return false;
		if (boardingTime == null) {
			if (other.boardingTime != null)
				return false;
		} else if (!boardingTime.equals(other.boardingTime))
			return false;
		if (boardingZone == null) {
			if (other.boardingZone != null)
				return false;
		} else if (!boardingZone.equals(other.boardingZone))
			return false;
		if (changeSeatEnabled == null) {
			if (other.changeSeatEnabled != null)
				return false;
		} else if (!changeSeatEnabled.equals(other.changeSeatEnabled))
			return false;
		if (changeSeatPayment == null) {
			if (other.changeSeatPayment != null)
				return false;
		} else if (!changeSeatPayment.equals(other.changeSeatPayment))
			return false;
		if (comfortSeatPaid == null) {
			if (other.comfortSeatPaid != null)
				return false;
		} else if (!comfortSeatPaid.equals(other.comfortSeatPaid))
			return false;
		if (eticket == null) {
			if (other.eticket != null)
				return false;
		} else if (!eticket.equals(other.eticket))
			return false;
		if (flight == null) {
			if (other.flight != null)
				return false;
		} else if (!flight.equals(other.flight))
			return false;
		if (flightId == null) {
			if (other.flightId != null)
				return false;
		} else if (!flightId.equals(other.flightId))
			return false;
		if (gate == null) {
			if (other.gate != null)
				return false;
		} else if (!gate.equals(other.gate))
			return false;
		if (gdsCouponType != other.gdsCouponType)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (infantCoupon == null) {
			if (other.infantCoupon != null)
				return false;
		} else if (!infantCoupon.equals(other.infantCoupon))
			return false;
		if (oldSeat == null) {
			if (other.oldSeat != null)
				return false;
		} else if (!oldSeat.equals(other.oldSeat))
			return false;
		if (oldSeatClass != other.oldSeatClass)
			return false;
		if (orderNumber == null) {
			if (other.orderNumber != null)
				return false;
		} else if (!orderNumber.equals(other.orderNumber))
			return false;
		if (passengerId == null) {
			if (other.passengerId != null)
				return false;
		} else if (!passengerId.equals(other.passengerId))
			return false;
		if (previousSeat == null) {
			if (other.previousSeat != null)
				return false;
		} else if (!previousSeat.equals(other.previousSeat))
			return false;
		if (seat == null) {
			if (other.seat != null)
				return false;
		} else if (!seat.equals(other.seat))
			return false;
		if (seatClass != other.seatClass)
			return false;
		if (seatClassName == null) {
			if (other.seatClassName != null)
				return false;
		} else if (!seatClassName.equals(other.seatClassName))
			return false;
		if (sequenceNumber == null) {
			if (other.sequenceNumber != null)
				return false;
		} else if (!sequenceNumber.equals(other.sequenceNumber))
			return false;
		if (serviceCooperationDetails == null) {
			if (other.serviceCooperationDetails != null)
				return false;
		} else if (!serviceCooperationDetails
				.equals(other.serviceCooperationDetails))
			return false;
		if (serviceOperatingDetails == null) {
			if (other.serviceOperatingDetails != null)
				return false;
		} else if (!serviceOperatingDetails
				.equals(other.serviceOperatingDetails))
			return false;
		if (skyPriority == null) {
			if (other.skyPriority != null)
				return false;
		} else if (!skyPriority.equals(other.skyPriority))
			return false;
		if (socialCode == null) {
			if (other.socialCode != null)
				return false;
		} else if (!socialCode.equals(other.socialCode))
			return false;
		if (specialFare == null) {
			if (other.specialFare != null)
				return false;
		} else if (!specialFare.equals(other.specialFare))
			return false;
		if (ssrCode == null) {
			if (other.ssrCode != null)
				return false;
		} else if (!ssrCode.equals(other.ssrCode))
			return false;
		if (status != other.status)
			return false;
		if (terminal == null) {
			if (other.terminal != null)
				return false;
		} else if (!terminal.equals(other.terminal))
			return false;
		if (upgradeEnabled == null) {
			if (other.upgradeEnabled != null)
				return false;
		} else if (!upgradeEnabled.equals(other.upgradeEnabled))
			return false;
		if (upgradeEnabledClasses == null) {
			if (other.upgradeEnabledClasses != null)
				return false;
		} else if (!upgradeEnabledClasses.equals(other.upgradeEnabledClasses))
			return false;
		if (upgradedAlready == null) {
			if (other.upgradedAlready != null)
				return false;
		} else if (!upgradedAlready.equals(other.upgradedAlready))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CheckinCouponData [id=" + id + ", barcode=" + barcode
				+ ", boardingPassColor=" + boardingPassColor
				+ ", boardingPassLoaded=" + boardingPassLoaded
				+ ", boardingTime=" + boardingTime + ", boardingZone="
				+ boardingZone + ", changeSeatEnabled=" + changeSeatEnabled
				+ ", changeSeatPayment=" + changeSeatPayment + ", eticket="
				+ eticket + ", flightId=" + flightId + ", flight=" + flight
				+ ", gate=" + gate + ", gdsCouponType=" + gdsCouponType
				+ ", comfortSeatPaid=" + comfortSeatPaid + ", infantCoupon="
				+ infantCoupon + ", skyPriority=" + skyPriority + ", oldSeat="
				+ oldSeat + ", oldSeatClass=" + oldSeatClass + ", orderNumber="
				+ orderNumber + ", passengerId=" + passengerId
				+ ", previousSeat=" + previousSeat + ", ssrCode=" + ssrCode
				+ ", seatClassName=" + seatClassName + ", seatClass="
				+ seatClass + ", seat=" + seat + ", sequenceNumber="
				+ sequenceNumber + ", serviceCooperationDetails="
				+ serviceCooperationDetails + ", serviceOperatingDetails="
				+ serviceOperatingDetails + ", socialCode=" + socialCode
				+ ", specialFare=" + specialFare + ", status=" + status
				+ ", terminal=" + terminal + ", upgradeEnabledClasses="
				+ upgradeEnabledClasses + ", upgradeEnabled=" + upgradeEnabled
				+ ", upgradedAlready=" + upgradedAlready + ", toString()="
				+ super.toString() + "]";
	}

}
