package com.alitalia.aem.common.data.home.mmb.ancillary;

import com.alitalia.aem.common.data.home.enumerations.CabinEnum;

public class MmbAncillaryCouponData {

	private Integer baggageAllowance;
	private CabinEnum cabin;
	private String fareClass;
	private Integer flightId;
	private Integer flightNumber;
	private String inboundCouponNumber;
	private Boolean child;
	private Boolean infantAccompanist;
	private Boolean infant;
	private Boolean specialPassenger;
	private String number;
	private Integer passengerNumber;

	public Integer getBaggageAllowance() {
		return baggageAllowance;
	}

	public void setBaggageAllowance(Integer baggageAllowance) {
		this.baggageAllowance = baggageAllowance;
	}

	public CabinEnum getCabin() {
		return cabin;
	}

	public void setCabin(CabinEnum cabin) {
		this.cabin = cabin;
	}

	public String getFareClass() {
		return fareClass;
	}

	public void setFareClass(String fareClass) {
		this.fareClass = fareClass;
	}

	public Integer getFlightId() {
		return flightId;
	}

	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	public Integer getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(Integer flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getInboundCouponNumber() {
		return inboundCouponNumber;
	}

	public void setInboundCouponNumber(String inboundCouponNumber) {
		this.inboundCouponNumber = inboundCouponNumber;
	}

	public Boolean isChild() {
		return child;
	}

	public void setChild(Boolean isChild) {
		this.child = isChild;
	}

	public Boolean isInfantAccompanist() {
		return infantAccompanist;
	}

	public void setInfantAccompanist(Boolean isInfantAccompanist) {
		this.infantAccompanist = isInfantAccompanist;
	}

	public Boolean isInfant() {
		return infant;
	}

	public void setInfant(Boolean isInfant) {
		this.infant = isInfant;
	}

	public Boolean isSpecialPassenger() {
		return specialPassenger;
	}

	public void setSpecialPassenger(Boolean isSpecialPassenger) {
		this.specialPassenger = isSpecialPassenger;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Integer getPassengerNumber() {
		return passengerNumber;
	}

	public void setPassengerNumber(Integer passengerNumber) {
		this.passengerNumber = passengerNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cabin == null) ? 0 : cabin.hashCode());
		result = prime * result
				+ ((fareClass == null) ? 0 : fareClass.hashCode());
		result = prime * result
				+ ((flightId == null) ? 0 : flightId.hashCode());
		result = prime * result
				+ ((flightNumber == null) ? 0 : flightNumber.hashCode());
		result = prime
				* result
				+ ((inboundCouponNumber == null) ? 0 : inboundCouponNumber
						.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result
				+ ((passengerNumber == null) ? 0 : passengerNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbAncillaryCouponData other = (MmbAncillaryCouponData) obj;
		if (cabin != other.cabin)
			return false;
		if (fareClass == null) {
			if (other.fareClass != null)
				return false;
		} else if (!fareClass.equals(other.fareClass))
			return false;
		if (flightId == null) {
			if (other.flightId != null)
				return false;
		} else if (!flightId.equals(other.flightId))
			return false;
		if (flightNumber == null) {
			if (other.flightNumber != null)
				return false;
		} else if (!flightNumber.equals(other.flightNumber))
			return false;
		if (inboundCouponNumber == null) {
			if (other.inboundCouponNumber != null)
				return false;
		} else if (!inboundCouponNumber.equals(other.inboundCouponNumber))
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (passengerNumber == null) {
			if (other.passengerNumber != null)
				return false;
		} else if (!passengerNumber.equals(other.passengerNumber))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbAncillaryCouponData [baggageAllowance=" + baggageAllowance
				+ ", cabin=" + cabin + ", fareClass=" + fareClass
				+ ", flightId=" + flightId + ", flightNumber=" + flightNumber
				+ ", inboundCouponNumber=" + inboundCouponNumber + ", isChild="
				+ child + ", isInfantAccompanist=" + infantAccompanist
				+ ", isInfant=" + infant + ", isSpecialPassenger="
				+ specialPassenger + ", number=" + number
				+ ", passengerNumber=" + passengerNumber + "]";
	}

}
