package com.alitalia.aem.common.data.home;

import com.alitalia.aem.common.data.home.enumerations.CreditCardTypeEnum;

public class PaymentProviderInstalmentsBrazilData extends PaymentProviderPayLaterData {
	
	private CreditCardTypeEnum type;
	private int installementsNumber;

	public CreditCardTypeEnum getType() {
		return type;
	}

	public void setType(CreditCardTypeEnum type) {
		this.type = type;
	}

	public int getInstallementsNumber() {
		return installementsNumber;
	}

	public void setInstallementsNumber(int installementsNumber) {
		this.installementsNumber = installementsNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + installementsNumber;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentProviderInstalmentsBrazilData other = (PaymentProviderInstalmentsBrazilData) obj;
		if (installementsNumber != other.installementsNumber)
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PaymentProviderInstalmentsBrazilData [type=" + type
				+ ", installementsNumber=" + installementsNumber
				+ ", getType()=" + getType() + ", getInstallementsNumber()="
				+ getInstallementsNumber() + ", hashCode()=" + hashCode()
				+ ", getEmailForMail()=" + getEmailForMail()
				+ ", getEmailForPnr()=" + getEmailForPnr()
				+ ", getOtherContact()=" + getOtherContact() + ", toString()="
				+ super.toString() + ", getComunication()=" + getComunication()
				+ ", getClass()=" + getClass() + "]";
	}
}
