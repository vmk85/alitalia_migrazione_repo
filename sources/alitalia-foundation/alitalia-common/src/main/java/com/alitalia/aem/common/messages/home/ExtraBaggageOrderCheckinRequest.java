package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.common.messages.MmbBaseRequest;


public class ExtraBaggageOrderCheckinRequest extends MmbBaseRequest {

	private String eticket;
	private CheckinFlightData flight;
	
	public String getEticket() {
		return eticket;
	}
	public void setEticket(String eticket) {
		this.eticket = eticket;
	}
	public CheckinFlightData getFlight() {
		return flight;
	}
	public void setFlight(CheckinFlightData flight) {
		this.flight = flight;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((eticket == null) ? 0 : eticket.hashCode());
		result = prime * result + ((flight == null) ? 0 : flight.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExtraBaggageOrderCheckinRequest other = (ExtraBaggageOrderCheckinRequest) obj;
		if (eticket == null) {
			if (other.eticket != null)
				return false;
		} else if (!eticket.equals(other.eticket))
			return false;
		if (flight == null) {
			if (other.flight != null)
				return false;
		} else if (!flight.equals(other.flight))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ExtraBaggageOrderCheckinRequest [eticket=" + eticket
				+ ", flight=" + flight + ", toString()=" + super.toString()
				+ "]";
	}
	
}
