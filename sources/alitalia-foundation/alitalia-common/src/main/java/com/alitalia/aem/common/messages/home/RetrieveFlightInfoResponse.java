package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.FlightInfoData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetrieveFlightInfoResponse extends BaseResponse {

	private FlightInfoData flightInfoData;

	public FlightInfoData getFlightInfoData() {
		return flightInfoData;
	}

	public void setFlightInfoData(FlightInfoData flightInfoData) {
		this.flightInfoData = flightInfoData;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((flightInfoData == null) ? 0 : flightInfoData.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveFlightInfoResponse other = (RetrieveFlightInfoResponse) obj;
		if (flightInfoData == null) {
			if (other.flightInfoData != null)
				return false;
		} else if (!flightInfoData.equals(other.flightInfoData))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveFlightInfoResponse [flightInfoData=" + flightInfoData
				+ "]";
	}
}