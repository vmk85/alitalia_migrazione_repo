package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.messages.MmbBaseRequest;

public class MmbAncillarySendEmailRequest extends MmbBaseRequest {

	private String sessionId;
	private String machineName;
	private Long cartId;
    private List<String> recipients;
    private String paymentType;
    private String pnr;

    public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Long getCartId() {
		return cartId;
	}

    public void setCartId(Long cartId) {
		this.cartId = cartId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getMachineName() {
		return machineName;
	}

	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}

	public List<String> getRecipients() {
		return recipients;
	}

	public void setRecipients(List<String> recipients) {
		this.recipients = recipients;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((cartId == null) ? 0 : cartId.hashCode());
		result = prime * result
				+ ((machineName == null) ? 0 : machineName.hashCode());
		result = prime * result
				+ ((paymentType == null) ? 0 : paymentType.hashCode());
		result = prime * result
				+ ((recipients == null) ? 0 : recipients.hashCode());
		result = prime * result
				+ ((sessionId == null) ? 0 : sessionId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbAncillarySendEmailRequest other = (MmbAncillarySendEmailRequest) obj;
		if (cartId == null) {
			if (other.cartId != null)
				return false;
		} else if (!cartId.equals(other.cartId))
			return false;
		if (machineName == null) {
			if (other.machineName != null)
				return false;
		} else if (!machineName.equals(other.machineName))
			return false;
		if (paymentType == null) {
			if (other.paymentType != null)
				return false;
		} else if (!paymentType.equals(other.paymentType))
			return false;
		if (recipients == null) {
			if (other.recipients != null)
				return false;
		} else if (!recipients.equals(other.recipients))
			return false;
		if (sessionId == null) {
			if (other.sessionId != null)
				return false;
		} else if (!sessionId.equals(other.sessionId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbAncillarySendEmailRequest [sessionId=" + sessionId
				+ ", machineName=" + machineName + ", cartId=" + cartId
				+ ", recipients=" + recipients + ", paymentType=" + paymentType
				+ ", getBaseInfo()=" + getBaseInfo() + ", getClient()="
				+ getClient() + ", getTid()=" + getTid() + ", getSid()="
				+ getSid() + "]";
	}
	
}