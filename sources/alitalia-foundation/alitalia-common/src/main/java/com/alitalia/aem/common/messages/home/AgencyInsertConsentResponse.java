package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class AgencyInsertConsentResponse extends BaseResponse {

	private boolean result;

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (this.result ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgencyInsertConsentResponse other = (AgencyInsertConsentResponse) obj;
		if (result != other.result)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AgencyInsertConsentResponse [result=" + result + "]";
	}
}