package com.alitalia.aem.common.messages.home;

import java.util.ArrayList;
import java.util.List;

import com.alitalia.aem.common.data.home.TicketData;
import com.alitalia.aem.common.messages.BaseRequest;

public class SapInvoiceRequest extends BaseRequest {

    private String cap;
    private String partitaIva;
    private String codiceFiscale;
    private String emailIntestatario;
    private String enteEmittente;
    private String enteRichiedente;
    private String indirizzoSpedizione;
    private String intestatarioFattura;
    private String localitaSpedizione;
    private String nome;
    private String paese;
    private String provincia;
    private String societa;
    private List<TicketData> biglietti;
    private String tipoRichiesta;

    public SapInvoiceRequest() {
		super();
	}

    public SapInvoiceRequest(SapInvoiceRequest clone) {
		super();
		this.cap = clone.getCap();
		this.partitaIva = clone.getPartitaIva();
		this.codiceFiscale = clone.getCodiceFiscale();
		this.emailIntestatario = clone.getEmailIntestatario();
		this.enteEmittente = clone.getEnteEmittente();
		this.enteRichiedente = clone.getEnteRichiedente();
		this.indirizzoSpedizione = clone.getIndirizzoSpedizione();
		this.intestatarioFattura = clone.getIntestatarioFattura();
		this.localitaSpedizione = clone.getLocalitaSpedizione();
		this.nome = clone.getNome();
		this.paese = clone.getPaese();
		this.provincia = clone.getProvincia();
		this.societa = clone.getSocieta();

		if (clone.getBiglietti() != null) {

			this.biglietti = new ArrayList<TicketData>();
			for (TicketData cloneTicketData : clone.getBiglietti()) {
				this.biglietti.add(new TicketData(cloneTicketData));
			}
		} else
			this.biglietti = null;

		this.tipoRichiesta = clone.getTipoRichiesta();
	}

	public SapInvoiceRequest(String cap, String partitaIva,
			String codiceFiscale, String emailIntestatario,
			String enteEmittente, String enteRichiedente,
			String indirizzoSpedizione, String intestatarioFattura,
			String localitaSpedizione, String nome, String paese,
			String provincia, String societa, List<TicketData> biglietti,
			String tipoRichiesta) {
		super();
		this.cap = cap;
		this.partitaIva = partitaIva;
		this.codiceFiscale = codiceFiscale;
		this.emailIntestatario = emailIntestatario;
		this.enteEmittente = enteEmittente;
		this.enteRichiedente = enteRichiedente;
		this.indirizzoSpedizione = indirizzoSpedizione;
		this.intestatarioFattura = intestatarioFattura;
		this.localitaSpedizione = localitaSpedizione;
		this.nome = nome;
		this.paese = paese;
		this.provincia = provincia;
		this.societa = societa;
		if (biglietti != null) {

			this.biglietti = new ArrayList<TicketData>();
			for (TicketData cloneTicketData : biglietti) {
				this.biglietti.add(new TicketData(cloneTicketData));
			}
		} else
			this.biglietti = null;

		this.tipoRichiesta = tipoRichiesta;
	}

	public String getCap() {
		return cap;
	}

	public void setCap(String cap) {
		this.cap = cap;
	}

	public String getPartitaIva() {
		return partitaIva;
	}

	public void setPartitaIva(String partitaIva) {
		this.partitaIva = partitaIva;
	}

	public String getCodiceFiscale() {
		return codiceFiscale;
	}

	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}

	public String getEmailIntestatario() {
		return emailIntestatario;
	}

	public void setEmailIntestatario(String emailIntestatario) {
		this.emailIntestatario = emailIntestatario;
	}

	public String getEnteEmittente() {
		return enteEmittente;
	}

	public void setEnteEmittente(String enteEmittente) {
		this.enteEmittente = enteEmittente;
	}

	public String getEnteRichiedente() {
		return enteRichiedente;
	}

	public void setEnteRichiedente(String enteRichiedente) {
		this.enteRichiedente = enteRichiedente;
	}

	public String getIndirizzoSpedizione() {
		return indirizzoSpedizione;
	}

	public void setIndirizzoSpedizione(String indirizzoSpedizione) {
		this.indirizzoSpedizione = indirizzoSpedizione;
	}

	public String getIntestatarioFattura() {
		return intestatarioFattura;
	}

	public void setIntestatarioFattura(String intestatarioFattura) {
		this.intestatarioFattura = intestatarioFattura;
	}

	public String getLocalitaSpedizione() {
		return localitaSpedizione;
	}

	public void setLocalitaSpedizione(String localitaSpedizione) {
		this.localitaSpedizione = localitaSpedizione;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getPaese() {
		return paese;
	}

	public void setPaese(String paese) {
		this.paese = paese;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getSocieta() {
		return societa;
	}

	public void setSocieta(String societa) {
		this.societa = societa;
	}

	public List<TicketData> getBiglietti() {
		return biglietti;
	}

	public void setBiglietti(List<TicketData> biglietti) {
		this.biglietti = biglietti;
	}

	public String getTipoRichiesta() {
		return tipoRichiesta;
	}

	public void setTipoRichiesta(String tipoRichiesta) {
		this.tipoRichiesta = tipoRichiesta;
	}

	@Override
	public String toString() {
		return "SapInvoiceRequest [cap=" + cap + ", partitaIva=" + partitaIva
				+ ", codiceFiscale=" + codiceFiscale + ", emailIntestatario="
				+ emailIntestatario + ", enteEmittente=" + enteEmittente
				+ ", enteRichiedente=" + enteRichiedente
				+ ", indirizzoSpedizione=" + indirizzoSpedizione
				+ ", intestatarioFattura=" + intestatarioFattura
				+ ", localitaSpedizione=" + localitaSpedizione + ", nome="
				+ nome + ", paese=" + paese + ", provincia=" + provincia
				+ ", societa=" + societa + ", biglietti=" + biglietti
				+ ", tipoRichiesta=" + tipoRichiesta + ", getTid()=" + getTid()
				+ ", getSid()=" + getSid() + "]";
	}

}
