package com.alitalia.aem.common.messages.home;

import java.util.Calendar;

import com.alitalia.aem.common.messages.MmbBaseRequest;

public class GetSeatDisponibilityCheckinRequest extends MmbBaseRequest {

	private Calendar departureDate;
	private String flightCarrier;
	private String flightNumber;
	private String from;
	private String to;
	
	public Calendar getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(Calendar departureDate) {
		this.departureDate = departureDate;
	}
	public String getFlightCarrier() {
		return flightCarrier;
	}
	public void setFlightCarrier(String flightCarrier) {
		this.flightCarrier = flightCarrier;
	}
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((departureDate == null) ? 0 : departureDate.hashCode());
		result = prime * result
				+ ((flightCarrier == null) ? 0 : flightCarrier.hashCode());
		result = prime * result
				+ ((flightNumber == null) ? 0 : flightNumber.hashCode());
		result = prime * result + ((from == null) ? 0 : from.hashCode());
		result = prime * result + ((to == null) ? 0 : to.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		GetSeatDisponibilityCheckinRequest other = (GetSeatDisponibilityCheckinRequest) obj;
		if (departureDate == null) {
			if (other.departureDate != null)
				return false;
		} else if (!departureDate.equals(other.departureDate))
			return false;
		if (flightCarrier == null) {
			if (other.flightCarrier != null)
				return false;
		} else if (!flightCarrier.equals(other.flightCarrier))
			return false;
		if (flightNumber == null) {
			if (other.flightNumber != null)
				return false;
		} else if (!flightNumber.equals(other.flightNumber))
			return false;
		if (from == null) {
			if (other.from != null)
				return false;
		} else if (!from.equals(other.from))
			return false;
		if (to == null) {
			if (other.to != null)
				return false;
		} else if (!to.equals(other.to))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "GetSeatDisponibilityCheckinRequest [departureDate="
				+ departureDate + ", flightCarrier=" + flightCarrier
				+ ", flightNumber=" + flightNumber + ", from=" + from + ", to="
				+ to + ", toString()=" + super.toString() + "]";
	}

}
