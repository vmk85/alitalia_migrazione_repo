
package com.alitalia.aem.common.data.home;

import com.alitalia.aem.common.data.home.enumerations.BancoPostaTypeEnum;

public class PaymentComunicationBancoPostaData extends PaymentComunicationInfoData {

    private BancoPostaTypeEnum type;

	public BancoPostaTypeEnum getType() {
		return type;
	}

	public void setType(BancoPostaTypeEnum type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentComunicationBancoPostaData other = (PaymentComunicationBancoPostaData) obj;
		if (type != other.type)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PaymentComunicationBancoPostaData [type=" + type + "]";
	}

}
