package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.carnet.CarnetData;
import com.alitalia.aem.common.messages.BaseResponse;

public class CarnetGetListResponse extends BaseResponse {

	private List<CarnetData> carnets;

	public List<CarnetData> getCarnets() {
		return carnets;
	}

	public void setCarnets(List<CarnetData> carnets) {
		this.carnets = carnets;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((carnets == null) ? 0 : carnets.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarnetGetListResponse other = (CarnetGetListResponse) obj;
		if (carnets == null) {
			if (other.carnets != null)
				return false;
		} else if (!carnets.equals(other.carnets))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "GetCarnetListResponse [carnets=" + carnets + "]";
	}
}