package com.alitalia.aem.common.data.home.enumerations;

public enum FareTypeEnum {

    FARE("Fare"),
    FARE_YR("FareYR"),
    FARE_YR_TAX("FareYRTax"),
    FARE_YR_TAX_OD("FareYRTaxOD");
    
    private final String value;

    FareTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FareTypeEnum fromValue(String v) {
        for (FareTypeEnum c: FareTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
