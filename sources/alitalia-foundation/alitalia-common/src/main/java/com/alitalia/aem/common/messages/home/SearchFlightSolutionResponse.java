package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.AvailableFlightsData;
import com.alitalia.aem.common.data.PassengerBase;
import com.alitalia.aem.common.messages.BaseResponse;
/**
 * 
 * Holds results from flight solution search operations
 *  
 */
public class SearchFlightSolutionResponse extends BaseResponse {

	/**
	 * Holds possible flight solutions that satisfy input search criteria
	 * 
	 * @see AvailableFlightsData
	 */
	private AvailableFlightsData availableFlights;
	
	/**
	 * Holds extracharges (if applicable) information per passegner type
	 */
	private List<PassengerBase> extraChargePassengerList;
	
	/**
	 * Indicates if a route contains the bus carrier
	 */
	private boolean busCarrier;
	
	private Boolean youthSolutionsFound;
	private Boolean youthSolutionsFoundOtherDates;
	
	private boolean territorialContinuitySolutionsFound;
	private boolean territorialContinuitySolutionsFoundOtherDates;
	
	public AvailableFlightsData getAvailableFlights() {
		return availableFlights;
	}

	public void setAvailableFlights(AvailableFlightsData availableFlights) {
		this.availableFlights = availableFlights;
	}

	public List<PassengerBase> getExtraChargePassengerList() {
		return extraChargePassengerList;
	}

	public void setExtraChargePassengerList(
			List<PassengerBase> extraChargePassengerList) {
		this.extraChargePassengerList = extraChargePassengerList;
	}
	
	public void setBusCarrier(boolean busCarrier) {
		this.busCarrier = busCarrier;
	}
	
	public boolean isBusCarrier() {
		return busCarrier;
	}
	
	public Boolean getYouthSolutionsFound() {
		return youthSolutionsFound;
	}

	public void setYouthSolutionsFound(Boolean youthSolutionsFound) {
		this.youthSolutionsFound = youthSolutionsFound;
	}

	public Boolean getYouthSolutionsFoundOtherDates() {
		return youthSolutionsFoundOtherDates;
	}

	public void setYouthSolutionsFoundOtherDates(
			Boolean youthSolutionsFoundOtherDates) {
		this.youthSolutionsFoundOtherDates = youthSolutionsFoundOtherDates;
	}

	public boolean isTerritorialContinuitySolutionsFound() {
		return territorialContinuitySolutionsFound;
	}

	public void setTerritorialContinuitySolutionsFound(
			boolean territorialContinuitySolutionsFound) {
		this.territorialContinuitySolutionsFound = territorialContinuitySolutionsFound;
	}

	public boolean isTerritorialContinuitySolutionsFoundOtherDates() {
		return territorialContinuitySolutionsFoundOtherDates;
	}

	public void setTerritorialContinuitySolutionsFoundOtherDates(
			boolean territorialContinuitySolutionsFoundOtherDates) {
		this.territorialContinuitySolutionsFoundOtherDates = territorialContinuitySolutionsFoundOtherDates;
	}
	
	@Override
	public String toString() {
		return "SearchFlightSolutionResponse [availableFlights="
				+ availableFlights + ", extraChargePassengerList="
				+ extraChargePassengerList + ", busCarrier=" + busCarrier
				+ ", youthSolutionsFound=" + youthSolutionsFound
				+ ", youthSolutionsFoundOtherDates="
				+ youthSolutionsFoundOtherDates
				+ ", territorialContinuitySolutionsFound="
				+ territorialContinuitySolutionsFound
				+ ", territorialContinuitySolutionsFoundOtherDates="
				+ territorialContinuitySolutionsFoundOtherDates + "]";
	}

}
