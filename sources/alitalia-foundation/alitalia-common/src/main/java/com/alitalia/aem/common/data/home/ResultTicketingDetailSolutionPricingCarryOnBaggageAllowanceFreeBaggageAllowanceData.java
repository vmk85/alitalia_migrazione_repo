package com.alitalia.aem.common.data.home;

import java.util.List;

public class ResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowanceData {

    private List<Object> bagDescriptorField;
    private Integer kilosField;
    private Boolean kilosFieldSpecified;
    private Integer kilosPerPieceField;
    private Boolean kilosPerPieceFieldSpecified;
    private Integer piecesField;
    private Boolean piecesFieldSpecified;
    private Integer poundsField;
    private Boolean poundsFieldSpecified;

    public List<Object> getBagDescriptorField() {
        return bagDescriptorField;
    }

    public void setBagDescriptorField(List<Object> value) {
        this.bagDescriptorField = value;
    }

    public Integer getKilosField() {
        return kilosField;
    }

    public void setKilosField(Integer value) {
        this.kilosField = value;
    }

    public Boolean isKilosFieldSpecified() {
        return kilosFieldSpecified;
    }

    public void setKilosFieldSpecified(Boolean value) {
        this.kilosFieldSpecified = value;
    }

    public Integer getKilosPerPieceField() {
        return kilosPerPieceField;
    }

    public void setKilosPerPieceField(Integer value) {
        this.kilosPerPieceField = value;
    }

    public Boolean isKilosPerPieceFieldSpecified() {
        return kilosPerPieceFieldSpecified;
    }

    public void setKilosPerPieceFieldSpecified(Boolean value) {
        this.kilosPerPieceFieldSpecified = value;
    }

    public Integer getPiecesField() {
        return piecesField;
    }

    public void setPiecesField(Integer value) {
        this.piecesField = value;
    }

    public Boolean isPiecesFieldSpecified() {
        return piecesFieldSpecified;
    }

    public void setPiecesFieldSpecified(Boolean value) {
        this.piecesFieldSpecified = value;
    }

    public Integer getPoundsField() {
        return poundsField;
    }

    public void setPoundsField(Integer value) {
        this.poundsField = value;
    }

    public Boolean isPoundsFieldSpecified() {
        return poundsFieldSpecified;
    }

    public void setPoundsFieldSpecified(Boolean value) {
        this.poundsFieldSpecified = value;
    }

}
