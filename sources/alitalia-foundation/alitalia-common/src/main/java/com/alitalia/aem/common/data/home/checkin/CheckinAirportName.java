package com.alitalia.aem.common.data.home.checkin;


public class CheckinAirportName {

	private String countryCode;
	private String displayName;
	private String fqan;
	private int hiddenInFirstDeparture;
	private String iataCode;

	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getFqan() {
		return fqan;
	}
	public void setFqan(String fqan) {
		this.fqan = fqan;
	}
	public int getHiddenInFirstDeparture() {
		return hiddenInFirstDeparture;
	}
	public void setHiddenInFirstDeparture(int hiddenInFirstDeparture) {
		this.hiddenInFirstDeparture = hiddenInFirstDeparture;
	}
	public String getIataCode() {
		return iataCode;
	}
	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((countryCode == null) ? 0 : countryCode.hashCode());
		result = prime * result
				+ ((displayName == null) ? 0 : displayName.hashCode());
		result = prime * result + ((fqan == null) ? 0 : fqan.hashCode());
		result = prime * result + hiddenInFirstDeparture;
		result = prime * result
				+ ((iataCode == null) ? 0 : iataCode.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinAirportName other = (CheckinAirportName) obj;
		if (countryCode == null) {
			if (other.countryCode != null)
				return false;
		} else if (!countryCode.equals(other.countryCode))
			return false;
		if (displayName == null) {
			if (other.displayName != null)
				return false;
		} else if (!displayName.equals(other.displayName))
			return false;
		if (fqan == null) {
			if (other.fqan != null)
				return false;
		} else if (!fqan.equals(other.fqan))
			return false;
		if (hiddenInFirstDeparture != other.hiddenInFirstDeparture)
			return false;
		if (iataCode == null) {
			if (other.iataCode != null)
				return false;
		} else if (!iataCode.equals(other.iataCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CheckinAirportName [countryCode=" + countryCode
				+ ", displayName=" + displayName + ", fqan=" + fqan
				+ ", hiddenInFirstDeparture=" + hiddenInFirstDeparture
				+ ", iataCode=" + iataCode + "]";
	}
}
