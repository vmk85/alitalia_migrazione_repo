package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.SocialAccountData;
import com.alitalia.aem.common.messages.BaseResponse;

public class SocialAccountsResponse extends BaseResponse {

	private List<SocialAccountData> socialAccounts;

	public List<SocialAccountData> getSocialAccounts() {
		return socialAccounts;
	}

	public void setSocialAccounts(List<SocialAccountData> socialAccounts) {
		this.socialAccounts = socialAccounts;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((socialAccounts == null) ? 0 : socialAccounts.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SocialAccountsResponse other = (SocialAccountsResponse) obj;
		if (socialAccounts == null) {
			if (other.socialAccounts != null)
				return false;
		} else if (!socialAccounts.equals(other.socialAccounts))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SocialAccountResponse [socialAccounts=" + socialAccounts + "]";
	}
}