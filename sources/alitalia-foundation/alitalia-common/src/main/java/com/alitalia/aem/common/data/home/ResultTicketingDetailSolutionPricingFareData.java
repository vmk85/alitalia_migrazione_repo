package com.alitalia.aem.common.data.home;

import java.util.List;

public class ResultTicketingDetailSolutionPricingFareData {

    private ResultTicketingDetailSolutionPricingFareAdjustedPriceData adjustedPriceField;
    private ResultTicketingDetailSolutionPricingFareBasePriceData basePriceField;
    private List<ResultTicketingDetailSolutionPricingFareBookingInfoData> bookingInfoField;
    private String carrierField;
    private String city1Field;
    private String city2Field;
    private String destinationCityField;
    private ResultTicketingDetailSolutionPricingFareDisplayBasePriceData displayBasePriceField;
    private List<ResultTicketingDetailSolutionPricingFareEndorsementData> endorsementField;
    private String extendedFareCodeField;
    private String globalIndicatorField;
    private String originCityField;
    private List<String> ptcField;
    private String rkeyField;
    private ResultTicketingDetailSolutionPricingFareSaleAdjustedPriceData saleAdjustedPriceField;
    private ResultTicketingDetailSolutionPricingFareSaleBasePriceData saleBasePriceField;
    private List<ResultTicketingDetailSolutionPricingFareSurchargeData> surchargeField;
    private String tagField;
    private String ticketDesignatorField;

    public ResultTicketingDetailSolutionPricingFareAdjustedPriceData getAdjustedPriceField() {
        return adjustedPriceField;
    }

    public void setAdjustedPriceField(ResultTicketingDetailSolutionPricingFareAdjustedPriceData value) {
        this.adjustedPriceField = value;
    }

    public ResultTicketingDetailSolutionPricingFareBasePriceData getBasePriceField() {
        return basePriceField;
    }

    public void setBasePriceField(ResultTicketingDetailSolutionPricingFareBasePriceData value) {
        this.basePriceField = value;
    }

    public List<ResultTicketingDetailSolutionPricingFareBookingInfoData> getBookingInfoField() {
        return bookingInfoField;
    }

    public void setBookingInfoField(List<ResultTicketingDetailSolutionPricingFareBookingInfoData> value) {
        this.bookingInfoField = value;
    }

    public String getCarrierField() {
        return carrierField;
    }

    public void setCarrierField(String value) {
        this.carrierField = value;
    }

    public String getCity1Field() {
        return city1Field;
    }

    public void setCity1Field(String value) {
        this.city1Field = value;
    }

    public String getCity2Field() {
        return city2Field;
    }

    public void setCity2Field(String value) {
        this.city2Field = value;
    }

    public String getDestinationCityField() {
        return destinationCityField;
    }

    public void setDestinationCityField(String value) {
        this.destinationCityField = value;
    }

    public ResultTicketingDetailSolutionPricingFareDisplayBasePriceData getDisplayBasePriceField() {
        return displayBasePriceField;
    }

    public void setDisplayBasePriceField(ResultTicketingDetailSolutionPricingFareDisplayBasePriceData value) {
        this.displayBasePriceField = value;
    }

    public List<ResultTicketingDetailSolutionPricingFareEndorsementData> getEndorsementField() {
        return endorsementField;
    }

    public void setEndorsementField(List<ResultTicketingDetailSolutionPricingFareEndorsementData> value) {
        this.endorsementField = value;
    }

    public String getExtendedFareCodeField() {
        return extendedFareCodeField;
    }

    public void setExtendedFareCodeField(String value) {
        this.extendedFareCodeField = value;
    }

    public String getGlobalIndicatorField() {
        return globalIndicatorField;
    }

    public void setGlobalIndicatorField(String value) {
        this.globalIndicatorField = value;
    }

    public String getOriginCityField() {
        return originCityField;
    }

    public void setOriginCityField(String value) {
        this.originCityField = value;
    }

    public List<String> getPtcField() {
        return ptcField;
    }

    public void setPtcField(List<String> value) {
        this.ptcField = value;
    }

    public String getRkeyField() {
        return rkeyField;
    }

    public void setRkeyField(String value) {
        this.rkeyField = value;
    }

    public ResultTicketingDetailSolutionPricingFareSaleAdjustedPriceData getSaleAdjustedPriceField() {
        return saleAdjustedPriceField;
    }

    public void setSaleAdjustedPriceField(ResultTicketingDetailSolutionPricingFareSaleAdjustedPriceData value) {
        this.saleAdjustedPriceField = value;
    }

    public ResultTicketingDetailSolutionPricingFareSaleBasePriceData getSaleBasePriceField() {
        return saleBasePriceField;
    }

    public void setSaleBasePriceField(ResultTicketingDetailSolutionPricingFareSaleBasePriceData value) {
        this.saleBasePriceField = value;
    }

    public List<ResultTicketingDetailSolutionPricingFareSurchargeData> getSurchargeField() {
        return surchargeField;
    }

    public void setSurchargeField(List<ResultTicketingDetailSolutionPricingFareSurchargeData> value) {
        this.surchargeField = value;
    }

    public String getTagField() {
        return tagField;
    }

    public void setTagField(String value) {
        this.tagField = value;
    }

    public String getTicketDesignatorField() {
        return ticketDesignatorField;
    }

    public void setTicketDesignatorField(String value) {
        this.ticketDesignatorField = value;
    }

}
