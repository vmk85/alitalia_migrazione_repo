package com.alitalia.aem.common.data.home.checkin;

import com.alitalia.aem.common.data.home.InsurancePolicyData;

public class CheckinInsurancePolicyData extends InsurancePolicyData{
	
	private CheckinPaymentData paymentData;

	public CheckinInsurancePolicyData() {
		super();
	}

	public CheckinPaymentData getPaymentData() {
		return paymentData;
	}

	public void setPaymentData(CheckinPaymentData paymentData) {
		this.paymentData = paymentData;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((paymentData == null) ? 0 : paymentData.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinInsurancePolicyData other = (CheckinInsurancePolicyData) obj;
		if (paymentData == null) {
			if (other.paymentData != null)
				return false;
		} else if (!paymentData.equals(other.paymentData))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CheckinInsurancePolicyData [paymentData=" + paymentData
				+ ", buy=" + buy + ", cardType=" + cardType + ", currency="
				+ currency + ", discount=" + discount + ", discountedPrice="
				+ discountedPrice + ", errorDescription=" + errorDescription
				+ ", policyNumber=" + policyNumber + ", totalInsuranceCost="
				+ totalInsuranceCost + ", typeOfTrip=" + typeOfTrip + "]";
	}
}