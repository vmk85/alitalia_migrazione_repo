package com.alitalia.aem.common.data.home;

public class ResultBookingDetailsSolutionPricingFareRestrictionEarliestReturnData {
	
	private String dateField;
	private String timeField;
	
	public String getDateField() {
		return dateField;
	}
	
	public void setDateField(String dateField) {
		this.dateField = dateField;
	}
	
	public String getTimeField() {
		return timeField;
	}
	
	public void setTimeField(String timeField) {
		this.timeField = timeField;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dateField == null) ? 0 : dateField.hashCode());
		result = prime * result
				+ ((timeField == null) ? 0 : timeField.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingDetailsSolutionPricingFareRestrictionEarliestReturnData other = (ResultBookingDetailsSolutionPricingFareRestrictionEarliestReturnData) obj;
		if (dateField == null) {
			if (other.dateField != null)
				return false;
		} else if (!dateField.equals(other.dateField))
			return false;
		if (timeField == null) {
			if (other.timeField != null)
				return false;
		} else if (!timeField.equals(other.timeField))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingDetailsSolutionPricingFareRestrictionEarliestReturnData [dateField="
				+ dateField + ", timeField=" + timeField + "]";
	}
	
}
