package com.alitalia.aem.common.data.home;

public class FlySegmentData {

	private String boardPoint;
	private String boardPointDepTime;
	private Integer daysFromStart;
	private Integer milesFromStart;
	private String offPoint;
	private String offPointArrTime;
	private String timeFromStart;
	
	public String getBoardPoint() {
		return boardPoint;
	}
	
	public void setBoardPoint(String boardPoint) {
		this.boardPoint = boardPoint;
	}
	
	public String getBoardPointDepTime() {
		return boardPointDepTime;
	}
	
	public void setBoardPointDepTime(String boardPointDepTime) {
		this.boardPointDepTime = boardPointDepTime;
	}
	
	public Integer getDaysFromStart() {
		return daysFromStart;
	}
	
	public void setDaysFromStart(Integer daysFromStart) {
		this.daysFromStart = daysFromStart;
	}
	
	public Integer getMilesFromStart() {
		return milesFromStart;
	}
	
	public void setMilesFromStart(Integer milesFromStart) {
		this.milesFromStart = milesFromStart;
	}
	
	public String getOffPoint() {
		return offPoint;
	}
	
	public void setOffPoint(String offPoint) {
		this.offPoint = offPoint;
	}
	
	public String getOffPointArrTime() {
		return offPointArrTime;
	}
	
	public void setOffPointArrTime(String offPointArrTime) {
		this.offPointArrTime = offPointArrTime;
	}
	
	public String getTimeFromStart() {
		return timeFromStart;
	}
	
	public void setTimeFromStart(String timeFromStart) {
		this.timeFromStart = timeFromStart;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((boardPoint == null) ? 0 : boardPoint.hashCode());
		result = prime
				* result
				+ ((boardPointDepTime == null) ? 0 : boardPointDepTime
						.hashCode());
		result = prime * result
				+ ((daysFromStart == null) ? 0 : daysFromStart.hashCode());
		result = prime * result
				+ ((milesFromStart == null) ? 0 : milesFromStart.hashCode());
		result = prime * result
				+ ((offPoint == null) ? 0 : offPoint.hashCode());
		result = prime * result
				+ ((offPointArrTime == null) ? 0 : offPointArrTime.hashCode());
		result = prime * result
				+ ((timeFromStart == null) ? 0 : timeFromStart.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FlySegmentData other = (FlySegmentData) obj;
		if (boardPoint == null) {
			if (other.boardPoint != null)
				return false;
		} else if (!boardPoint.equals(other.boardPoint))
			return false;
		if (boardPointDepTime == null) {
			if (other.boardPointDepTime != null)
				return false;
		} else if (!boardPointDepTime.equals(other.boardPointDepTime))
			return false;
		if (daysFromStart == null) {
			if (other.daysFromStart != null)
				return false;
		} else if (!daysFromStart.equals(other.daysFromStart))
			return false;
		if (milesFromStart == null) {
			if (other.milesFromStart != null)
				return false;
		} else if (!milesFromStart.equals(other.milesFromStart))
			return false;
		if (offPoint == null) {
			if (other.offPoint != null)
				return false;
		} else if (!offPoint.equals(other.offPoint))
			return false;
		if (offPointArrTime == null) {
			if (other.offPointArrTime != null)
				return false;
		} else if (!offPointArrTime.equals(other.offPointArrTime))
			return false;
		if (timeFromStart == null) {
			if (other.timeFromStart != null)
				return false;
		} else if (!timeFromStart.equals(other.timeFromStart))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "FlySegmentData [boardPoint=" + boardPoint
				+ ", boardPointDepTime=" + boardPointDepTime
				+ ", daysFromStart=" + daysFromStart + ", milesFromStart="
				+ milesFromStart + ", offPoint=" + offPoint
				+ ", offPointArrTime=" + offPointArrTime + ", timeFromStart="
				+ timeFromStart + "]";
	}
}