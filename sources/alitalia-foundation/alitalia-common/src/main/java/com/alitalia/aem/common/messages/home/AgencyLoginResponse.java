package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.AlitaliaTradeAgencyData;
import com.alitalia.aem.common.messages.BaseResponse;

public class AgencyLoginResponse extends BaseResponse {

	private AlitaliaTradeAgencyData agencyData;
	private Boolean loginSuccessful;
	private Boolean firstAccess;
	private Boolean passwordExpired;
	private Boolean accountLocked;
	private Boolean approvalRequired;

	public AgencyLoginResponse() {
		super();
		this.loginSuccessful = false;
	}

	public AgencyLoginResponse(AlitaliaTradeAgencyData agencyData, Boolean loginSuccessful,
			Boolean firstAccess, Boolean passwordExpired, Boolean accountLocked, Boolean approvalRequired) {
		super();
		this.agencyData = agencyData;
		this.firstAccess = firstAccess;
		this.loginSuccessful = loginSuccessful;
		this.passwordExpired = passwordExpired;
		this.accountLocked = accountLocked;
		this.approvalRequired = approvalRequired;
	}

	public AgencyLoginResponse(String tid, String sid,
			AlitaliaTradeAgencyData agencyData, Boolean loginSuccessful, Boolean firstAccess,
			Boolean passwordExpired, Boolean accountLocked, Boolean approvalRequired) {
		super(tid, sid);
		this.agencyData = agencyData;
		this.firstAccess = firstAccess;
		this.loginSuccessful = loginSuccessful;
		this.passwordExpired = passwordExpired;
		this.accountLocked = accountLocked;
		this.approvalRequired = approvalRequired;
	}

	public AlitaliaTradeAgencyData getAgencyData() {
		return agencyData;
	}

	public void setAgencyData(AlitaliaTradeAgencyData agencyData) {
		this.agencyData = agencyData;
	}

	public Boolean isFirstAccess() {
		return firstAccess;
	}

	public void setFirstAccess(Boolean firstAccess) {
		this.firstAccess = firstAccess;
	}

	public Boolean isLoginSuccessful() {
		return loginSuccessful;
	}

	public void setLoginSuccessful(Boolean loginSuccessful) {
		this.loginSuccessful = loginSuccessful;
	}

	public Boolean isPasswordExpired() {
		return passwordExpired;
	}

	public void setPasswordExpired(Boolean passwordExpired) {
		this.passwordExpired = passwordExpired;
	}

	public Boolean isAccountLocked() {
		return accountLocked;
	}

	public void setAccountLocked(Boolean accountLocked) {
		this.accountLocked = accountLocked;
	}

	public Boolean isApprovalRequired() {
		return approvalRequired;
	}

	public void setApprovalRequired(Boolean approvalRequired) {
		this.approvalRequired = approvalRequired;
	}

	@Override
	public String toString() {
		return "AgencyLoginResponse [agencyData=" + agencyData
				+ ", loginSuccessful=" + loginSuccessful + ", firstAccess="
				+ firstAccess + ", passwordExpired=" + passwordExpired
				+ ", accountLocked=" + accountLocked + ", approvalRequired="
				+ approvalRequired + "]";
	}
}
