package com.alitalia.aem.common.data.home;

public class ResultBookingDetailsSolutionPricingBookingInfoData {
	
	private String bookingCodeCountField;
	private String bookingCodeField;
	private String cabinField;
	private ResultBookingDetailsSolutionPricingBookingInfoFareData fareField;
	private int marriedSegmentIndexField;
	private boolean marriedSegmentIndexFieldSpecified;
	private ResultBookingDetailsSolutionPricingBookingInfoSegmentData segmentField;
	
	public String getBookingCodeCountField() {
		return bookingCodeCountField;
	}
	
	public void setBookingCodeCountField(String bookingCodeCountField) {
		this.bookingCodeCountField = bookingCodeCountField;
	}
	
	public String getBookingCodeField() {
		return bookingCodeField;
	}
	
	public void setBookingCodeField(String bookingCodeField) {
		this.bookingCodeField = bookingCodeField;
	}
	
	public String getCabinField() {
		return cabinField;
	}
	
	public void setCabinField(String cabinField) {
		this.cabinField = cabinField;
	}
	
	public ResultBookingDetailsSolutionPricingBookingInfoFareData getFareField() {
		return fareField;
	}
	
	public void setFareField(
			ResultBookingDetailsSolutionPricingBookingInfoFareData fareField) {
		this.fareField = fareField;
	}
	
	public int getMarriedSegmentIndexField() {
		return marriedSegmentIndexField;
	}
	
	public void setMarriedSegmentIndexField(int marriedSegmentIndexField) {
		this.marriedSegmentIndexField = marriedSegmentIndexField;
	}
	
	public boolean isMarriedSegmentIndexFieldSpecified() {
		return marriedSegmentIndexFieldSpecified;
	}
	
	public void setMarriedSegmentIndexFieldSpecified(
			boolean marriedSegmentIndexFieldSpecified) {
		this.marriedSegmentIndexFieldSpecified = marriedSegmentIndexFieldSpecified;
	}
	
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentData getSegmentField() {
		return segmentField;
	}
	
	public void setSegmentField(
			ResultBookingDetailsSolutionPricingBookingInfoSegmentData segmentField) {
		this.segmentField = segmentField;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((bookingCodeCountField == null) ? 0 : bookingCodeCountField
						.hashCode());
		result = prime
				* result
				+ ((bookingCodeField == null) ? 0 : bookingCodeField.hashCode());
		result = prime * result
				+ ((cabinField == null) ? 0 : cabinField.hashCode());
		result = prime * result
				+ ((fareField == null) ? 0 : fareField.hashCode());
		result = prime * result + marriedSegmentIndexField;
		result = prime * result
				+ (marriedSegmentIndexFieldSpecified ? 1231 : 1237);
		result = prime * result
				+ ((segmentField == null) ? 0 : segmentField.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingDetailsSolutionPricingBookingInfoData other = (ResultBookingDetailsSolutionPricingBookingInfoData) obj;
		if (bookingCodeCountField == null) {
			if (other.bookingCodeCountField != null)
				return false;
		} else if (!bookingCodeCountField.equals(other.bookingCodeCountField))
			return false;
		if (bookingCodeField == null) {
			if (other.bookingCodeField != null)
				return false;
		} else if (!bookingCodeField.equals(other.bookingCodeField))
			return false;
		if (cabinField == null) {
			if (other.cabinField != null)
				return false;
		} else if (!cabinField.equals(other.cabinField))
			return false;
		if (fareField == null) {
			if (other.fareField != null)
				return false;
		} else if (!fareField.equals(other.fareField))
			return false;
		if (marriedSegmentIndexField != other.marriedSegmentIndexField)
			return false;
		if (marriedSegmentIndexFieldSpecified != other.marriedSegmentIndexFieldSpecified)
			return false;
		if (segmentField == null) {
			if (other.segmentField != null)
				return false;
		} else if (!segmentField.equals(other.segmentField))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingDetailsSolutionPricingBookingInfoData [bookingCodeCountField="
				+ bookingCodeCountField
				+ ", bookingCodeField="
				+ bookingCodeField
				+ ", cabinField="
				+ cabinField
				+ ", fareField="
				+ fareField
				+ ", marriedSegmentIndexField="
				+ marriedSegmentIndexField
				+ ", marriedSegmentIndexFieldSpecified="
				+ marriedSegmentIndexFieldSpecified
				+ ", segmentField="
				+ segmentField + "]";
	}
	
}
