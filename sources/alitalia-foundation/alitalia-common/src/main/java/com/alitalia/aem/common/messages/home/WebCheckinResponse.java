package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.messages.BaseResponse;

public class WebCheckinResponse extends BaseResponse {

	private List<String> messages;
	private List<CheckinPassengerData> passengers;
	
	public List<String> getMessages() {
		return messages;
	}
	public void setMessages(List<String> messages) {
		this.messages = messages;
	}
	public List<CheckinPassengerData> getPassengers() {
		return passengers;
	}
	public void setPassengers(List<CheckinPassengerData> passengers) {
		this.passengers = passengers;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((messages == null) ? 0 : messages.hashCode());
		result = prime * result
				+ ((passengers == null) ? 0 : passengers.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebCheckinResponse other = (WebCheckinResponse) obj;
		if (messages == null) {
			if (other.messages != null)
				return false;
		} else if (!messages.equals(other.messages))
			return false;
		if (passengers == null) {
			if (other.passengers != null)
				return false;
		} else if (!passengers.equals(other.passengers))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "WebCheckinResponse [messages=" + messages + ", passengers="
				+ passengers + ", getTid()=" + getTid() + ", getSid()="
				+ getSid() + "]";
	}
}
