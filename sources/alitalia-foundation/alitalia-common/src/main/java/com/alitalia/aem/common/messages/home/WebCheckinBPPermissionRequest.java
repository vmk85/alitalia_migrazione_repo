package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.messages.MmbBaseRequest;

public class WebCheckinBPPermissionRequest extends MmbBaseRequest {

	private List<String> aptCodes;

	public WebCheckinBPPermissionRequest(String tid, String sid) {
		super(tid, sid);
	}

	public WebCheckinBPPermissionRequest() {
		super();
	}

	public List<String> getAptCodes() {
		return aptCodes;
	}

	public void setAptCodes(List<String> aptCodes) {
		this.aptCodes = aptCodes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((aptCodes == null) ? 0 : aptCodes.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebCheckinBPPermissionRequest other = (WebCheckinBPPermissionRequest) obj;
		if (aptCodes == null) {
			if (other.aptCodes != null)
				return false;
		} else if (!aptCodes.equals(other.aptCodes))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WebCheckinBPPermissionRequest [aptCodes=" + aptCodes
				+ ", toString()=" + super.toString() + "]";
	}
}
