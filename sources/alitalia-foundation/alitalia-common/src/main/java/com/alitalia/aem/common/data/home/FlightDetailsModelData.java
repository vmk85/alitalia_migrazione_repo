package com.alitalia.aem.common.data.home;

import java.util.Calendar;
import java.util.List;

public class FlightDetailsModelData {

	private String airCraft;
	private Calendar flightDate;
	private String flightNumber;
	private List<FlySegmentData> flySegments;
	private String vector;
	
	public String getAirCraft() {
		return airCraft;
	}
	
	public void setAirCraft(String airCraft) {
		this.airCraft = airCraft;
	}
	
	public Calendar getFlightDate() {
		return flightDate;
	}
	
	public void setFlightDate(Calendar flightDate) {
		this.flightDate = flightDate;
	}
	
	public String getFlightNumber() {
		return flightNumber;
	}
	
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	
	public List<FlySegmentData> getFlySegments() {
		return flySegments;
	}
	
	public void setFlySegments(List<FlySegmentData> flySegments) {
		this.flySegments = flySegments;
	}
	
	public String getVector() {
		return vector;
	}
	
	public void setVector(String vector) {
		this.vector = vector;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((airCraft == null) ? 0 : airCraft.hashCode());
		result = prime * result
				+ ((flightDate == null) ? 0 : flightDate.hashCode());
		result = prime * result
				+ ((flightNumber == null) ? 0 : flightNumber.hashCode());
		result = prime * result
				+ ((flySegments == null) ? 0 : flySegments.hashCode());
		result = prime * result + ((vector == null) ? 0 : vector.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FlightDetailsModelData other = (FlightDetailsModelData) obj;
		if (airCraft == null) {
			if (other.airCraft != null)
				return false;
		} else if (!airCraft.equals(other.airCraft))
			return false;
		if (flightDate == null) {
			if (other.flightDate != null)
				return false;
		} else if (!flightDate.equals(other.flightDate))
			return false;
		if (flightNumber == null) {
			if (other.flightNumber != null)
				return false;
		} else if (!flightNumber.equals(other.flightNumber))
			return false;
		if (flySegments == null) {
			if (other.flySegments != null)
				return false;
		} else if (!flySegments.equals(other.flySegments))
			return false;
		if (vector == null) {
			if (other.vector != null)
				return false;
		} else if (!vector.equals(other.vector))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "FlightDetailsModelData [airCraft=" + airCraft + ", flightDate="
				+ flightDate + ", flightNumber=" + flightNumber
				+ ", flySegments=" + flySegments + ", vector=" + vector + "]";
	}
}