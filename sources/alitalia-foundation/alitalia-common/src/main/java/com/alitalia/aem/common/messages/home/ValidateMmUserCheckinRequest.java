package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.MmbBaseRequest;

public class ValidateMmUserCheckinRequest extends MmbBaseRequest {

	private String mmCode;
	private String mmPin;

	public ValidateMmUserCheckinRequest(String tid, String sid) {
		super(tid, sid);
	}

	public ValidateMmUserCheckinRequest() {
		super();
	}

	public String getMmCode() {
		return mmCode;
	}

	public void setMmCode(String mmCode) {
		this.mmCode = mmCode;
	}

	public String getMmPin() {
		return mmPin;
	}

	public void setMmPin(String mmPin) {
		this.mmPin = mmPin;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((mmCode == null) ? 0 : mmCode.hashCode());
		result = prime * result + ((mmPin == null) ? 0 : mmPin.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValidateMmUserCheckinRequest other = (ValidateMmUserCheckinRequest) obj;
		if (mmCode == null) {
			if (other.mmCode != null)
				return false;
		} else if (!mmCode.equals(other.mmCode))
			return false;
		if (mmPin == null) {
			if (other.mmPin != null)
				return false;
		} else if (!mmPin.equals(other.mmPin))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ValidateMmUserRequest [mmCode=" + mmCode + ", mmPin=" + mmPin
				+ ", getMmCode()=" + getMmCode() + ", getMmPin()=" + getMmPin()
				+ ", hashCode()=" + hashCode() + ", getBaseInfo()="
				+ getBaseInfo() + ", getClient()=" + getClient()
				+ ", toString()=" + super.toString() + ", getTid()=" + getTid()
				+ ", getSid()=" + getSid() + ", getClass()=" + getClass() + "]";
	}
}