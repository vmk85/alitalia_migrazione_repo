package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.BaseResponse;

public class ProfileResponse extends BaseResponse {

	private MMCustomerProfileData customerProfile;

	public MMCustomerProfileData getCustomerProfile() {
		return customerProfile;
	}

	public void setCustomerProfile(MMCustomerProfileData customerProfile) {
		this.customerProfile = customerProfile;
	}
	
}
