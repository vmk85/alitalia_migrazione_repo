package com.alitalia.aem.common.data.home;

import com.alitalia.aem.common.data.home.enumerations.DeliveryNotificationOptionsEnum;
import com.alitalia.aem.common.data.home.enumerations.MailPriorityEnum;

public class EmailMessage {

	private String From;
	private String To;
	private String Cc;
	private String Bcc;
	private String Subject;
	private String MessageText;
	private DeliveryNotificationOptionsEnum DeliveryNotification;
	private MailPriorityEnum Priority;
	private Boolean IsBodyHtml;
	private String UNCAllegato;
	
	public String getFrom() {
		return From;
	}
	
	public void setFrom(String from) {
		From = from;
	}
	
	public String getTo() {
		return To;
	}
	
	public void setTo(String to) {
		To = to;
	}
	
	public String getCc() {
		return Cc;
	}
	
	public void setCc(String cc) {
		Cc = cc;
	}
	
	public String getBcc() {
		return Bcc;
	}
	
	public void setBcc(String bcc) {
		Bcc = bcc;
	}
	
	public String getSubject() {
		return Subject;
	}
	
	public void setSubject(String subject) {
		Subject = subject;
	}
	
	public String getMessageText() {
		return MessageText;
	}
	
	public void setMessageText(String messageText) {
		MessageText = messageText;
	}
	
	public DeliveryNotificationOptionsEnum getDeliveryNotification() {
		return DeliveryNotification;
	}
	
	public void setDeliveryNotification(
			DeliveryNotificationOptionsEnum deliveryNotification) {
		DeliveryNotification = deliveryNotification;
	}
	
	public MailPriorityEnum getPriority() {
		return Priority;
	}
	
	public void setPriority(MailPriorityEnum priority) {
		Priority = priority;
	}
	
	public Boolean isBodyHtml() {
		return IsBodyHtml;
	}
	
	public void setIsBodyHtml(Boolean isBodyHtml) {
		IsBodyHtml = isBodyHtml;
	}
	
	public String getUNCAllegato() {
		return UNCAllegato;
	}
	
	public void setUNCAllegato(String uNCAllegato) {
		UNCAllegato = uNCAllegato;
	}

	@Override
	public String toString() {
		return "EmailMessage [From=" + From + ", To=" + To + ", Cc=" + Cc
				+ ", Bcc=" + Bcc + ", Subject=" + Subject + ", MessageText="
				+ MessageText + ", DeliveryNotification="
				+ DeliveryNotification + ", Priority=" + Priority
				+ ", IsBodyHtml=" + IsBodyHtml + ", UNCAllegato=" + UNCAllegato
				+ "]";
	}

}
