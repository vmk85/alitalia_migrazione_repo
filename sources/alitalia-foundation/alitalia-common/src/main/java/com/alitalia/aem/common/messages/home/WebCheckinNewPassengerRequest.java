package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.checkin.CheckinRouteData;
import com.alitalia.aem.common.messages.MmbBaseRequest;

public class WebCheckinNewPassengerRequest extends MmbBaseRequest {

    private String eticket;
    private String groupPnr;
    private String lastName;
    private String name;
	private List<CheckinPassengerData> passengers;
	private CheckinRouteData selectedRoute;
    
    public WebCheckinNewPassengerRequest(String tid, String sid){
    	super(tid, sid);
    }

	public String getEticket() {
		return eticket;
	}

	public void setEticket(String eticket) {
		this.eticket = eticket;
	}

	public String getGroupPnr() {
		return groupPnr;
	}

	public void setGroupPnr(String groupPnr) {
		this.groupPnr = groupPnr;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<CheckinPassengerData> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<CheckinPassengerData> passengers) {
		this.passengers = passengers;
	}

	public CheckinRouteData getSelectedRoute() {
		return selectedRoute;
	}

	public void setSelectedRoute(CheckinRouteData selectedRoute) {
		this.selectedRoute = selectedRoute;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((eticket == null) ? 0 : eticket.hashCode());
		result = prime * result
				+ ((groupPnr == null) ? 0 : groupPnr.hashCode());
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((passengers == null) ? 0 : passengers.hashCode());
		result = prime * result
				+ ((selectedRoute == null) ? 0 : selectedRoute.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebCheckinNewPassengerRequest other = (WebCheckinNewPassengerRequest) obj;
		if (eticket == null) {
			if (other.eticket != null)
				return false;
		} else if (!eticket.equals(other.eticket))
			return false;
		if (groupPnr == null) {
			if (other.groupPnr != null)
				return false;
		} else if (!groupPnr.equals(other.groupPnr))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (passengers == null) {
			if (other.passengers != null)
				return false;
		} else if (!passengers.equals(other.passengers))
			return false;
		if (selectedRoute == null) {
			if (other.selectedRoute != null)
				return false;
		} else if (!selectedRoute.equals(other.selectedRoute))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WebCheckinNewPassengerRequest [eticket=" + eticket
				+ ", groupPnr=" + groupPnr + ", lastName=" + lastName
				+ ", name=" + name + ", passengers=" + passengers
				+ ", selectedRoute=" + selectedRoute + "]";
	}
}