package com.alitalia.aem.common.data.home;

import java.util.ArrayList;
import java.util.Calendar;

import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.CabinTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.ResidencyTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.SearchTypeEnum;
import com.alitalia.aem.common.messages.SearchExecuteRequestFilter;

public class BrandSearchData extends BoomBoxData implements SearchExecuteRequestFilter {

	private String cug;
	private ArrayList<SearchDestinationData> destinations;
	private SearchExecuteRequestFilter innerSearch;
	private String market;
	private boolean onlyDirectFlight;
	private ArrayList<PassengerNumbersData> passengerNumbers;
	private CabinEnum searchCabin;
	private CabinTypeEnum searchCabinType;
	private SearchTypeEnum type;
	private ResidencyTypeEnum residency;
	private boolean isBandoPeriod;
	private Calendar startOutboundDate;
	private Calendar startReturnDate;

	public BrandSearchData() {
		super();
		this.type = SearchTypeEnum.BRAND_SEARCH;
	}

	public BrandSearchData(BrandSearchData clone) {
		super();
		ArrayList<SearchDestinationData> copyDestinationsList = new ArrayList<SearchDestinationData>();
		for (SearchDestinationData searchElement : clone.getDestinations()) {
			SearchDestinationData copyDestination = new SearchDestinationData(searchElement);
			copyDestinationsList.add(copyDestination);
		}
		this.setDestinations(copyDestinationsList);

		ArrayList<PassengerNumbersData> copyPassengerNumbersList = new ArrayList<PassengerNumbersData>();
		for (PassengerNumbersData pnElement : clone.getPassengerNumbers()) {
			PassengerNumbersData copyPassengerNumber = new PassengerNumbersData(pnElement);
			copyPassengerNumbersList.add(copyPassengerNumber);
		}
		this.setPassengerNumbers(copyPassengerNumbersList);

		this.cug = clone.getCug();
		this.setFarmId(clone.getFarmId());
		this.setId(clone.getId());
		this.innerSearch = clone.getInnerSearch();
		this.market = clone.getMarket();
		this.onlyDirectFlight = clone.isOnlyDirectFlight();
		this.setProperties(clone.getProperties());
		this.setRefreshSolutionId(clone.getRefreshSolutionId());
		this.searchCabin = clone.getSearchCabin();
		this.searchCabinType = clone.getSearchCabinType();
		this.setSolutionId(clone.getSolutionId());
		this.setSolutionSet(clone.getSolutionSet());
		this.type = clone.type;
		this.residency = clone.residency;
		this.startOutboundDate = clone.startOutboundDate;
		this.startReturnDate = clone.startReturnDate;
	}

	public String getCug() {
		return cug;
	}

	public void setCug(String cug) {
		this.cug = cug;
	}

	public ArrayList<SearchDestinationData> getDestinations() {
		return destinations;
	}

	public void setDestinations(ArrayList<SearchDestinationData> destinations) {
		this.destinations = destinations;
	}

	public SearchExecuteRequestFilter getInnerSearch() {
		return innerSearch;
	}

	public void setInnerSearch(SearchExecuteRequestFilter innerSearch) {
		this.innerSearch = innerSearch;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public boolean isOnlyDirectFlight() {
		return onlyDirectFlight;
	}

	public void setOnlyDirectFlight(boolean onlyDirectFlight) {
		this.onlyDirectFlight = onlyDirectFlight;
	}

	public ArrayList<PassengerNumbersData> getPassengerNumbers() {
		return passengerNumbers;
	}

	public void setPassengerNumbers(ArrayList<PassengerNumbersData> passengerNumbers) {
		this.passengerNumbers = passengerNumbers;
	}

	public CabinEnum getSearchCabin() {
		return searchCabin;
	}

	public void setSearchCabin(CabinEnum searchCabin) {
		this.searchCabin = searchCabin;
	}

	public CabinTypeEnum getSearchCabinType() {
		return searchCabinType;
	}

	public void setSearchCabinType(CabinTypeEnum searchCabinType) {
		this.searchCabinType = searchCabinType;
	}

	public SearchTypeEnum getType() {
		return type;
	}

	public void setType(SearchTypeEnum type) {
		this.type = type;
	}

	public ResidencyTypeEnum getResidency() {
		return residency;
	}

	public void setResidency(ResidencyTypeEnum residency) {
		this.residency = residency;
	}

	public Calendar getStartOutboundDate() {
		return startOutboundDate;
	}

	public void setStartOutboundDate(Calendar startOutboundDate) {
		this.startOutboundDate = startOutboundDate;
	}

	public Calendar getStartReturnDate() {
		return startReturnDate;
	}

	public void setStartReturnDate(Calendar startReturnDate) {
		this.startReturnDate = startReturnDate;
	}

	public void addPassengerNumbers(PassengerNumbersData passengerNumbersData) {
		if (this.passengerNumbers == null) {
			this.passengerNumbers = new ArrayList<PassengerNumbersData>();
		}
		this.passengerNumbers.add(passengerNumbersData);
	}

	public void addDestination(SearchDestinationData destination) {
		if (this.destinations == null) {
			this.destinations = new ArrayList<SearchDestinationData>();
		}
		this.destinations.add(destination);
	}

	public boolean isBandoPeriod() {
		return isBandoPeriod;
	}

	public void setBandoPeriod(boolean isBandoPeriod) {
		this.isBandoPeriod = isBandoPeriod;
	}

	@Override
	public String toString() {
		return "BrandSearchData [cug=" + cug + ", destinations=" + destinations
				+ ", innerSearch=" + innerSearch + ", market=" + market
				+ ", onlyDirectFlight=" + onlyDirectFlight
				+ ", passengerNumbers=" + passengerNumbers + ", searchCabin="
				+ searchCabin + ", searchCabinType=" + searchCabinType
				+ ", type=" + type + ", residency=" + residency
				+ ", isBandoPeriod=" + isBandoPeriod + ", startOutboundDate="
				+ startOutboundDate + ", startReturnDate=" + startReturnDate
				+ "]";
	}
}