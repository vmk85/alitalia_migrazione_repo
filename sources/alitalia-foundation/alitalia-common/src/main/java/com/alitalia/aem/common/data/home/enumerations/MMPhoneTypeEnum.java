package com.alitalia.aem.common.data.home.enumerations;

public enum MMPhoneTypeEnum {

	UNKNOWN("Unknown"),
	BUSINESS_FAX("BusinessFax"),
	BUSINESS_PHONE("BusinessPhone"),
	EMAIL("EMail"),
	HOME_FAX("HomeFax"),
	HOME_PHONE("HomePhone"),
	MOBILE_BUSINESS("MobileBusiness"),
	MOBILE_HOME("MobileHome"),
	STATEMENT_PHONE("StatementPhone"),
    TEMPORARY_PHONE("TemporaryPhone");

	private final String value;

	private MMPhoneTypeEnum(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	public static MMPhoneTypeEnum fromValue(String v) {
		for (MMPhoneTypeEnum c: MMPhoneTypeEnum.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}
