package com.alitalia.aem.common.data.home;

import java.util.List;

public class ResultBookingPriceSolutionSliceSegmentPricingBaggageAllowanceOptionFreeBaggageAllowanceData {
	
	private List<ResultBookingPriceSolutionSliceSegmentPricingBaggageAllowanceOptionFreeBaggageAllowanceBagDescriptorData> bagDescriptorField;
	private int kilosField;
	private boolean kilosFieldSpecified;
	private int kilosPerPieceField;
	private boolean kilosPerPieceFieldSpecified;
	private int piecesField;
	private boolean piecesFieldSpecified;
	private int poundsField;
	private boolean poundsFieldSpecified;
	
	public List<ResultBookingPriceSolutionSliceSegmentPricingBaggageAllowanceOptionFreeBaggageAllowanceBagDescriptorData> getBagDescriptorField() {
		return bagDescriptorField;
	}
	
	public void setBagDescriptorField(
			List<ResultBookingPriceSolutionSliceSegmentPricingBaggageAllowanceOptionFreeBaggageAllowanceBagDescriptorData> bagDescriptorField) {
		this.bagDescriptorField = bagDescriptorField;
	}
	
	public int getKilosField() {
		return kilosField;
	}
	
	public void setKilosField(int kilosField) {
		this.kilosField = kilosField;
	}
	
	public boolean isKilosFieldSpecified() {
		return kilosFieldSpecified;
	}
	
	public void setKilosFieldSpecified(boolean kilosFieldSpecified) {
		this.kilosFieldSpecified = kilosFieldSpecified;
	}
	
	public int getKilosPerPieceField() {
		return kilosPerPieceField;
	}
	
	public void setKilosPerPieceField(int kilosPerPieceField) {
		this.kilosPerPieceField = kilosPerPieceField;
	}
	
	public boolean isKilosPerPieceFieldSpecified() {
		return kilosPerPieceFieldSpecified;
	}
	
	public void setKilosPerPieceFieldSpecified(
			boolean kilosPerPieceFieldSpecified) {
		this.kilosPerPieceFieldSpecified = kilosPerPieceFieldSpecified;
	}
	
	public int getPiecesField() {
		return piecesField;
	}
	
	public void setPiecesField(int piecesField) {
		this.piecesField = piecesField;
	}
	
	public boolean isPiecesFieldSpecified() {
		return piecesFieldSpecified;
	}
	
	public void setPiecesFieldSpecified(boolean piecesFieldSpecified) {
		this.piecesFieldSpecified = piecesFieldSpecified;
	}
	
	public int getPoundsField() {
		return poundsField;
	}
	
	public void setPoundsField(int poundsField) {
		this.poundsField = poundsField;
	}
	
	public boolean isPoundsFieldSpecified() {
		return poundsFieldSpecified;
	}
	
	public void setPoundsFieldSpecified(boolean poundsFieldSpecified) {
		this.poundsFieldSpecified = poundsFieldSpecified;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((bagDescriptorField == null) ? 0 : bagDescriptorField
						.hashCode());
		result = prime * result + kilosField;
		result = prime * result + (kilosFieldSpecified ? 1231 : 1237);
		result = prime * result + kilosPerPieceField;
		result = prime * result + (kilosPerPieceFieldSpecified ? 1231 : 1237);
		result = prime * result + piecesField;
		result = prime * result + (piecesFieldSpecified ? 1231 : 1237);
		result = prime * result + poundsField;
		result = prime * result + (poundsFieldSpecified ? 1231 : 1237);
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingPriceSolutionSliceSegmentPricingBaggageAllowanceOptionFreeBaggageAllowanceData other = (ResultBookingPriceSolutionSliceSegmentPricingBaggageAllowanceOptionFreeBaggageAllowanceData) obj;
		if (bagDescriptorField == null) {
			if (other.bagDescriptorField != null)
				return false;
		} else if (!bagDescriptorField.equals(other.bagDescriptorField))
			return false;
		if (kilosField != other.kilosField)
			return false;
		if (kilosFieldSpecified != other.kilosFieldSpecified)
			return false;
		if (kilosPerPieceField != other.kilosPerPieceField)
			return false;
		if (kilosPerPieceFieldSpecified != other.kilosPerPieceFieldSpecified)
			return false;
		if (piecesField != other.piecesField)
			return false;
		if (piecesFieldSpecified != other.piecesFieldSpecified)
			return false;
		if (poundsField != other.poundsField)
			return false;
		if (poundsFieldSpecified != other.poundsFieldSpecified)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingPriceSolutionSliceSegmentPricingBaggageAllowanceOptionFreeBaggageAllowanceData [bagDescriptorField="
				+ bagDescriptorField
				+ ", kilosField="
				+ kilosField
				+ ", kilosFieldSpecified="
				+ kilosFieldSpecified
				+ ", kilosPerPieceField="
				+ kilosPerPieceField
				+ ", kilosPerPieceFieldSpecified="
				+ kilosPerPieceFieldSpecified
				+ ", piecesField="
				+ piecesField
				+ ", piecesFieldSpecified="
				+ piecesFieldSpecified
				+ ", poundsField="
				+ poundsField
				+ ", poundsFieldSpecified="
				+ poundsFieldSpecified + "]";
	}
	
}
