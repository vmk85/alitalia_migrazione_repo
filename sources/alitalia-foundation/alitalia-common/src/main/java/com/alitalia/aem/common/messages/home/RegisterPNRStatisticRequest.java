package com.alitalia.aem.common.messages.home;


public class RegisterPNRStatisticRequest extends RegisterStatisticsRequest {

	private String pnr;
	private String travelInfo;
	private boolean isAward;
	
	public RegisterPNRStatisticRequest(String tid, String sid){
		super(tid,sid);
	}
	
	public String getPnr() {
		return pnr;
	}
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	public String getTravelInfo() {
		return travelInfo;
	}
	public void setTravelInfo(String travelInfo) {
		this.travelInfo = travelInfo;
	}
	public boolean isAward() {
		return isAward;
	}
	public void setAward(boolean isAward) {
		this.isAward = isAward;
	}
	
	@Override
	public String toString() {
		return "RegisterPNRStatisticRequest [pnr=" + pnr + ", travelInfo="
				+ travelInfo + ", isAward=" + isAward + ", getPnr()="
				+ getPnr() + ", getTravelInfo()=" + getTravelInfo()
				+ ", isAward()=" + isAward() + ", getClientIP()="
				+ getClientIP() + ", getErrorDescr()=" + getErrorDescr()
				+ ", getSessionId()=" + getSessionId() + ", getSiteCode()="
				+ getSiteCode() + ", isSuccess()=" + isSuccess()
				+ ", getType()=" + getType() + ", getUserId()=" + getUserId()
				+ ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + ", getTid()=" + getTid() + ", getSid()="
				+ getSid() + ", getClass()=" + getClass() + "]";
	}
}