package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.common.messages.BaseResponse;

public class InfoPassengerCreatePNRResponse extends BaseResponse {

	private RoutesData prenotation;

	public InfoPassengerCreatePNRResponse() {
		super();
	}
	
	public InfoPassengerCreatePNRResponse(RoutesData prenotation) {
		super();
		this.prenotation = prenotation;
	}
	
	public InfoPassengerCreatePNRResponse(String tid, String sid) {
		super(tid, sid);
	}


	public InfoPassengerCreatePNRResponse(String tid, String sid, RoutesData prenotation) {
		super(tid, sid);
		this.prenotation = prenotation;
	}

	public RoutesData getPrenotation() {
		return prenotation;
	}

	public void setPrenotation(RoutesData prenotation) {
		this.prenotation = prenotation;
	}

	@Override
	public String toString() {
		return "CreatePNRResponse [prenotation=" + prenotation + "]";
	}
	
}
