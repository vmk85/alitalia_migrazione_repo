package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.InfoCarnetData;
import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.common.messages.BaseRequest;

public class InitializePaymentRequest extends BaseRequest {
	
	    private InfoCarnetData carnetInfo;
	    private String ipAddress;
	    private RoutesData prenotation;
	    private String userAgent;
		
	    public InfoCarnetData getCarnetInfo() {
			return carnetInfo;
		}
	    
		public void setCarnetInfo(InfoCarnetData carnetInfo) {
			this.carnetInfo = carnetInfo;
		}
		
		public String getIpAddress() {
			return ipAddress;
		}
		
		public void setIpAddress(String ipAddress) {
			this.ipAddress = ipAddress;
		}
		
		public RoutesData getPrenotation() {
			return prenotation;
		}
		
		public void setPrenotation(RoutesData prenotation) {
			this.prenotation = prenotation;
		}
		
		public String getUserAgent() {
			return userAgent;
		}
		
		public void setUserAgent(String userAgent) {
			this.userAgent = userAgent;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result
					+ ((carnetInfo == null) ? 0 : carnetInfo.hashCode());
			result = prime * result
					+ ((ipAddress == null) ? 0 : ipAddress.hashCode());
			result = prime * result
					+ ((prenotation == null) ? 0 : prenotation.hashCode());
			result = prime * result
					+ ((userAgent == null) ? 0 : userAgent.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			InitializePaymentRequest other = (InitializePaymentRequest) obj;
			if (carnetInfo == null) {
				if (other.carnetInfo != null)
					return false;
			} else if (!carnetInfo.equals(other.carnetInfo))
				return false;
			if (ipAddress == null) {
				if (other.ipAddress != null)
					return false;
			} else if (!ipAddress.equals(other.ipAddress))
				return false;
			if (prenotation == null) {
				if (other.prenotation != null)
					return false;
			} else if (!prenotation.equals(other.prenotation))
				return false;
			if (userAgent == null) {
				if (other.userAgent != null)
					return false;
			} else if (!userAgent.equals(other.userAgent))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "InitializePaymentRequest [carnetInfo=" + carnetInfo
					+ ", ipAddress=" + ipAddress + ", prenotation="
					+ prenotation + ", userAgent=" + userAgent + "]";
		}
	    
}
