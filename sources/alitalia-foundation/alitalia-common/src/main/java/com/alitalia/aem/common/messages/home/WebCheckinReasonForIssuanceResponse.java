package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.checkin.CheckinReasonForIssuance;
import com.alitalia.aem.common.messages.BaseResponse;

public class WebCheckinReasonForIssuanceResponse extends BaseResponse {


	private CheckinReasonForIssuance reasonForIssuance;

	public CheckinReasonForIssuance getCheckinReasonForIssuance() {
		return reasonForIssuance;
	}

	public void setCheckinReasonForIssuance(
			CheckinReasonForIssuance checkinReasonForIssuance) {
		this.reasonForIssuance = checkinReasonForIssuance;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((reasonForIssuance == null) ? 0
						: reasonForIssuance.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebCheckinReasonForIssuanceResponse other = (WebCheckinReasonForIssuanceResponse) obj;
		if (reasonForIssuance == null) {
			if (other.reasonForIssuance != null)
				return false;
		} else if (!reasonForIssuance
				.equals(other.reasonForIssuance))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WebCheckinReasonForIssuanceResponse [checkinReasonForIssuance="
				+ reasonForIssuance + "]";
	}
}