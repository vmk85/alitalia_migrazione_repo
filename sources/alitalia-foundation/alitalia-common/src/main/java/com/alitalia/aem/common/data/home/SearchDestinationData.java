package com.alitalia.aem.common.data.home;

import java.util.Calendar;

import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.TimeTypeEnum;

public class SearchDestinationData {

	private AirportData fromAirport;
	private AirportData toAirport;
	private RouteTypeEnum routeType;
	private TimeTypeEnum timeType;
	private Calendar departureDate;
	private String selectedOffer;

	public SearchDestinationData() {
		super();
	}

	public SearchDestinationData(SearchDestinationData clone) {
		this.departureDate = clone.departureDate;
		this.routeType = clone.routeType;
		this.timeType = clone.timeType;
		this.fromAirport = new AirportData(clone.fromAirport);
		this.toAirport = new AirportData(clone.toAirport);
	}

	public AirportData getFromAirport() {
		return fromAirport;
	}

	public void setFromAirport(AirportData fromAirport) {
		this.fromAirport = fromAirport;
	}

	public AirportData getToAirport() {
		return toAirport;
	}

	public void setToAirport(AirportData toAirport) {
		this.toAirport = toAirport;
	}

	public RouteTypeEnum getRouteType() {
		return routeType;
	}

	public void setRouteType(RouteTypeEnum routeType) {
		this.routeType = routeType;
	}

	public TimeTypeEnum getTimeType() {
		return timeType;
	}

	public void setTimeType(TimeTypeEnum timeType) {
		this.timeType = timeType;
	}

	public Calendar getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Calendar departureDate) {
		this.departureDate = departureDate;
	}
	
	public String getSelectedOffer() {
		return selectedOffer;
	}

	public void setSelectedOffer(String selectedOffer) {
		this.selectedOffer = selectedOffer;
	}

	
}
