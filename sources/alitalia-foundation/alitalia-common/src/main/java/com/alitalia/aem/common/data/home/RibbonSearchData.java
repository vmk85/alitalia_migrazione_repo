package com.alitalia.aem.common.data.home;

import java.util.ArrayList;
import java.util.Calendar;

import com.alitalia.aem.common.data.home.enumerations.SearchTypeEnum;
import com.alitalia.aem.common.messages.SearchExecuteRequestFilter;

public class RibbonSearchData extends BrandSearchData implements SearchExecuteRequestFilter {

	public RibbonSearchData() {
		super();
	}

	public RibbonSearchData(BrandSearchData brandSearchData, Calendar startOutboundDate, Calendar startReturnDate) {
		super();
		ArrayList<SearchDestinationData> copyDestinationsList = new ArrayList<SearchDestinationData>();
		for (SearchDestinationData searchElement : brandSearchData.getDestinations()) {
			SearchDestinationData copyDestination = new SearchDestinationData(searchElement);
			copyDestinationsList.add(copyDestination);
		}
		this.setDestinations(copyDestinationsList);

		ArrayList<PassengerNumbersData> copyPassengerNumbersList = new ArrayList<PassengerNumbersData>();
		for (PassengerNumbersData pnElement : brandSearchData.getPassengerNumbers()) {
			PassengerNumbersData copyPassengerNumber = new PassengerNumbersData(pnElement);
			copyPassengerNumbersList.add(copyPassengerNumber);
		}
		this.setPassengerNumbers(copyPassengerNumbersList);

		this.setCug(brandSearchData.getCug());
		this.setFarmId(brandSearchData.getFarmId());
		this.setId(brandSearchData.getId());
		SearchExecuteRequestFilter innerSearch = brandSearchData.getInnerSearch();
		if (innerSearch != null)
			this.setInnerSearch(innerSearch);
		this.setMarket(brandSearchData.getMarket());
		this.setOnlyDirectFlight(brandSearchData.isOnlyDirectFlight());
		this.setPassengerNumbers(brandSearchData.getPassengerNumbers());
		this.setProperties(brandSearchData.getProperties());
		this.setRefreshSolutionId(brandSearchData.getRefreshSolutionId());
		this.setSearchCabin(brandSearchData.getSearchCabin());
		this.setSearchCabinType(brandSearchData.getSearchCabinType());
		this.setSolutionId(brandSearchData.getSolutionId());
		this.setSolutionSet(brandSearchData.getSolutionSet());
		this.setType(SearchTypeEnum.RIBBON_SEARCH);
		this.setResidency(brandSearchData.getResidency());
		this.setStartOutboundDate(startOutboundDate);
		this.setStartReturnDate(startReturnDate);
	}

	@Override
	public String toString() {
		return "RibbonSearchData ["+ super.toString() + "]";
	}
}
