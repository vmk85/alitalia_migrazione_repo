package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.checkin.CheckinRouteData;
import com.alitalia.aem.common.messages.MmbBaseRequest;


public class BoardingPassCheckinRequest extends MmbBaseRequest {

	private List<CheckinPassengerData> passengers;
	private CheckinRouteData selectedRoute;
	
	public List<CheckinPassengerData> getPassengers() {
		return passengers;
	}
	public void setPassengers(List<CheckinPassengerData> passengers) {
		this.passengers = passengers;
	}
	public CheckinRouteData getSelectedRoute() {
		return selectedRoute;
	}
	public void setSelectedRoute(CheckinRouteData selectedRoute) {
		this.selectedRoute = selectedRoute;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((passengers == null) ? 0 : passengers.hashCode());
		result = prime * result
				+ ((selectedRoute == null) ? 0 : selectedRoute.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		BoardingPassCheckinRequest other = (BoardingPassCheckinRequest) obj;
		if (passengers == null) {
			if (other.passengers != null)
				return false;
		} else if (!passengers.equals(other.passengers))
			return false;
		if (selectedRoute == null) {
			if (other.selectedRoute != null)
				return false;
		} else if (!selectedRoute.equals(other.selectedRoute))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "BoardingPassCheckinRequest [passengers=" + passengers
				+ ", selectedRoute=" + selectedRoute + ", toString()="
				+ super.toString() + "]";
	}
	
}
