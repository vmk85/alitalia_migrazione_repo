package com.alitalia.aem.common.data.home.mmb.ancillary;

import java.math.BigDecimal;
import java.util.List;

import com.alitalia.aem.common.data.MmbAncillaryDetail;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryTypeEnum;

public class MmbAncillaryData {

	private BigDecimal amount;
	private MmbAncillaryDetail ancillaryDetail;
	private MmbAncillaryStatusEnum ancillaryStatus;
	private MmbAncillaryTypeEnum ancillaryType;
	private List<String> couponNumbers;
	private String currency;
	private String emd;
	private Integer id;
	private List<Integer> incompatibleAncillaries;
	private List<Integer> mandatoryAncillaries;
	private Integer quantity;
	private List<MmbAncillaryQuotationData> quotations;
	private String reasonCode;
	private String reasonSubcode;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public MmbAncillaryDetail getAncillaryDetail() {
		return ancillaryDetail;
	}

	public void setAncillaryDetail(MmbAncillaryDetail ancillaryDetail) {
		this.ancillaryDetail = ancillaryDetail;
	}

	public MmbAncillaryStatusEnum getAncillaryStatus() {
		return ancillaryStatus;
	}

	public void setAncillaryStatus(MmbAncillaryStatusEnum ancillaryStatus) {
		this.ancillaryStatus = ancillaryStatus;
	}

	public MmbAncillaryTypeEnum getAncillaryType() {
		return ancillaryType;
	}

	public void setAncillaryType(MmbAncillaryTypeEnum ancillaryType) {
		this.ancillaryType = ancillaryType;
	}

	public List<String> getCouponNumbers() {
		return couponNumbers;
	}

	public void setCouponNumbers(List<String> couponNumbers) {
		this.couponNumbers = couponNumbers;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getEmd() {
		return emd;
	}

	public void setEmd(String emd) {
		this.emd = emd;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<Integer> getIncompatibleAncillaries() {
		return incompatibleAncillaries;
	}

	public void setIncompatibleAncillaries(List<Integer> incompatibleAncillaries) {
		this.incompatibleAncillaries = incompatibleAncillaries;
	}

	public List<Integer> getMandatoryAncillaries() {
		return mandatoryAncillaries;
	}

	public void setMandatoryAncillaries(List<Integer> mandatoryAncillaries) {
		this.mandatoryAncillaries = mandatoryAncillaries;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public List<MmbAncillaryQuotationData> getQuotations() {
		return quotations;
	}

	public void setQuotations(List<MmbAncillaryQuotationData> quotations) {
		this.quotations = quotations;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getReasonSubcode() {
		return reasonSubcode;
	}

	public void setReasonSubcode(String reasonSubcode) {
		this.reasonSubcode = reasonSubcode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbAncillaryData other = (MmbAncillaryData) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbAncillaryData [amount=" + amount + ", ancillaryDetail="
				+ ancillaryDetail + ", ancillaryStatus=" + ancillaryStatus
				+ ", ancillaryType=" + ancillaryType + ", couponNumbers="
				+ couponNumbers + ", currency=" + currency + ", emd=" + emd
				+ ", id=" + id + ", incompatibleAncillaries="
				+ incompatibleAncillaries + ", mandatoryAncillaries="
				+ mandatoryAncillaries + ", quantity=" + quantity
				+ ", quotations=" + quotations + ", reasonCode=" + reasonCode
				+ ", reasonSubcode=" + reasonSubcode + "]";
	}

}
