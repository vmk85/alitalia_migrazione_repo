package com.alitalia.aem.common.data.home;

import java.util.Calendar;

public class FlightInfoData {

    private DestinationData arrivalAirport;    
    private Calendar arrivalDateTime;
    private DestinationDetailsData arrivalDetails;
    private Calendar arrivalGapTime;
    
    private DestinationData departureAirport;
    private Calendar departureDateTime;
    private DestinationDetailsData departureDetails;
    private Calendar departureGapTime;
    
    private String errorDescription;
    private String flightNumber;
    private String rescheduled;
    private Integer sequenceNumber;
    private String vector;
	
    public DestinationData getArrivalAirport() {
		return arrivalAirport;
	}
    
	public void setArrivalAirport(DestinationData arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}
	
	public Calendar getArrivalDateTime() {
		return arrivalDateTime;
	}
	
	public void setArrivalDateTime(Calendar arrivalDateTime) {
		this.arrivalDateTime = arrivalDateTime;
	}
	
	public DestinationDetailsData getArrivalDetails() {
		return arrivalDetails;
	}
	
	public void setArrivalDetails(DestinationDetailsData arrivalDetails) {
		this.arrivalDetails = arrivalDetails;
	}
	
	public Calendar getArrivalGapTime() {
		return arrivalGapTime;
	}
	
	public void setArrivalGapTime(Calendar arrivalGapTime) {
		this.arrivalGapTime = arrivalGapTime;
	}
	
	public DestinationData getDepartureAirport() {
		return departureAirport;
	}
	
	public void setDepartureAirport(DestinationData departureAirport) {
		this.departureAirport = departureAirport;
	}
	
	public Calendar getDepartureDateTime() {
		return departureDateTime;
	}
	
	public void setDepartureDateTime(Calendar departureDateTime) {
		this.departureDateTime = departureDateTime;
	}
	
	public DestinationDetailsData getDepartureDetails() {
		return departureDetails;
	}
	
	public void setDepartureDetails(DestinationDetailsData departureDetails) {
		this.departureDetails = departureDetails;
	}
	
	public Calendar getDepartureGapTime() {
		return departureGapTime;
	}
	
	public void setDepartureGapTime(Calendar departureGapTime) {
		this.departureGapTime = departureGapTime;
	}
	
	public String getErrorDescription() {
		return errorDescription;
	}
	
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	
	public String getFlightNumber() {
		return flightNumber;
	}
	
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	
	public String getRescheduled() {
		return rescheduled;
	}
	
	public void setRescheduled(String rescheduled) {
		this.rescheduled = rescheduled;
	}
	
	public Integer getSequenceNumber() {
		return sequenceNumber;
	}
	
	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	
	public String getVector() {
		return vector;
	}
	
	public void setVector(String vector) {
		this.vector = vector;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((arrivalAirport == null) ? 0 : arrivalAirport.hashCode());
		result = prime * result
				+ ((arrivalDateTime == null) ? 0 : arrivalDateTime.hashCode());
		result = prime * result
				+ ((arrivalDetails == null) ? 0 : arrivalDetails.hashCode());
		result = prime * result
				+ ((arrivalGapTime == null) ? 0 : arrivalGapTime.hashCode());
		result = prime
				* result
				+ ((departureAirport == null) ? 0 : departureAirport.hashCode());
		result = prime
				* result
				+ ((departureDateTime == null) ? 0 : departureDateTime
						.hashCode());
		result = prime
				* result
				+ ((departureDetails == null) ? 0 : departureDetails.hashCode());
		result = prime
				* result
				+ ((departureGapTime == null) ? 0 : departureGapTime.hashCode());
		result = prime
				* result
				+ ((errorDescription == null) ? 0 : errorDescription.hashCode());
		result = prime * result
				+ ((flightNumber == null) ? 0 : flightNumber.hashCode());
		result = prime * result
				+ ((rescheduled == null) ? 0 : rescheduled.hashCode());
		result = prime * result
				+ ((sequenceNumber == null) ? 0 : sequenceNumber.hashCode());
		result = prime * result + ((vector == null) ? 0 : vector.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FlightInfoData other = (FlightInfoData) obj;
		if (arrivalAirport == null) {
			if (other.arrivalAirport != null)
				return false;
		} else if (!arrivalAirport.equals(other.arrivalAirport))
			return false;
		if (arrivalDateTime == null) {
			if (other.arrivalDateTime != null)
				return false;
		} else if (!arrivalDateTime.equals(other.arrivalDateTime))
			return false;
		if (arrivalDetails == null) {
			if (other.arrivalDetails != null)
				return false;
		} else if (!arrivalDetails.equals(other.arrivalDetails))
			return false;
		if (arrivalGapTime == null) {
			if (other.arrivalGapTime != null)
				return false;
		} else if (!arrivalGapTime.equals(other.arrivalGapTime))
			return false;
		if (departureAirport == null) {
			if (other.departureAirport != null)
				return false;
		} else if (!departureAirport.equals(other.departureAirport))
			return false;
		if (departureDateTime == null) {
			if (other.departureDateTime != null)
				return false;
		} else if (!departureDateTime.equals(other.departureDateTime))
			return false;
		if (departureDetails == null) {
			if (other.departureDetails != null)
				return false;
		} else if (!departureDetails.equals(other.departureDetails))
			return false;
		if (departureGapTime == null) {
			if (other.departureGapTime != null)
				return false;
		} else if (!departureGapTime.equals(other.departureGapTime))
			return false;
		if (errorDescription == null) {
			if (other.errorDescription != null)
				return false;
		} else if (!errorDescription.equals(other.errorDescription))
			return false;
		if (flightNumber == null) {
			if (other.flightNumber != null)
				return false;
		} else if (!flightNumber.equals(other.flightNumber))
			return false;
		if (rescheduled == null) {
			if (other.rescheduled != null)
				return false;
		} else if (!rescheduled.equals(other.rescheduled))
			return false;
		if (sequenceNumber == null) {
			if (other.sequenceNumber != null)
				return false;
		} else if (!sequenceNumber.equals(other.sequenceNumber))
			return false;
		if (vector == null) {
			if (other.vector != null)
				return false;
		} else if (!vector.equals(other.vector))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FlightInfoData [arrivalAirport=" + arrivalAirport
				+ ", arrivalDateTime=" + arrivalDateTime + ", arrivalDetails="
				+ arrivalDetails + ", arrivalGapTime=" + arrivalGapTime
				+ ", departureAirport=" + departureAirport
				+ ", departureDateTime=" + departureDateTime
				+ ", departureDetails=" + departureDetails
				+ ", departureGapTime=" + departureGapTime
				+ ", errorDescription=" + errorDescription + ", flightNumber="
				+ flightNumber + ", rescheduled=" + rescheduled
				+ ", sequenceNumber=" + sequenceNumber + ", vector=" + vector
				+ "]";
	}
}