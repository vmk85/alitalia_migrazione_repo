package com.alitalia.aem.common.data.home;

public class ResultTicketingDetailSolutionSliceSegmentConnectionData {

    private Integer durationField;
    private ResultTicketingDetailSolutionSliceSegmentConnectionExtData extField;

    public Integer getDurationField() {
        return durationField;
    }

    public void setDurationField(int value) {
        this.durationField = value;
    }

    public ResultTicketingDetailSolutionSliceSegmentConnectionExtData getExtField() {
        return extField;
    }

    public void setExtField(ResultTicketingDetailSolutionSliceSegmentConnectionExtData value) {
        this.extField = value;
    }

}
