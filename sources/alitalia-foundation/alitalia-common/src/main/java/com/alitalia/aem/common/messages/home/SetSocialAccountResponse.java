package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class SetSocialAccountResponse extends BaseResponse {

	protected Boolean succeeded;
	private String ssoToken;

	public Boolean isSucceeded() {
		return succeeded;
	}

	public void setSucceeded(Boolean succeeded) {
		this.succeeded = succeeded;
	}

	public String getSsoToken() {
		return ssoToken;
	}

	public void setSsoToken(String ssoToken) {
		this.ssoToken = ssoToken;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ssoToken == null) ? 0 : ssoToken.hashCode());
		result = prime * result + ((succeeded == null) ? 0 : succeeded.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SetSocialAccountResponse other = (SetSocialAccountResponse) obj;
		if (ssoToken == null) {
			if (other.ssoToken != null)
				return false;
		} else if (!ssoToken.equals(other.ssoToken))
			return false;
		if (succeeded == null) {
			if (other.succeeded != null)
				return false;
		} else if (!succeeded.equals(other.succeeded))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SetSocialAccountResponse [succeeded=" + succeeded + ", ssoToken=" + ssoToken + "]";
	}
	
}
