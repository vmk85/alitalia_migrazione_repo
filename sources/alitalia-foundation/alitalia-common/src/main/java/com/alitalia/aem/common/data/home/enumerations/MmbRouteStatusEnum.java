package com.alitalia.aem.common.data.home.enumerations;

public enum MmbRouteStatusEnum {

	INVALID("Invalid"),
	REGULAR("Regular"),
	ALREADY_CHECKED_IN("AlreadyCheckedIn"),
	AVAILABLE("Available"),
	SPECIAL_TIME_RESTRICTION("SpecialTimeRestriction"),
	WEB_CHECKIN_UNAVAILABLE("WebCheckinUnavailable")
	;
    private final String value;

    MmbRouteStatusEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MmbRouteStatusEnum fromValue(String v) {
        for (MmbRouteStatusEnum c: MmbRouteStatusEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
