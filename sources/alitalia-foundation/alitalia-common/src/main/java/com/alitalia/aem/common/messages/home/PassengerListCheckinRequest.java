package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.checkin.CheckinRouteData;
import com.alitalia.aem.common.data.home.enumerations.CheckinSearchReasonEnum;
import com.alitalia.aem.common.messages.MmbBaseRequest;

public class PassengerListCheckinRequest extends MmbBaseRequest {

	private Boolean isFullUndoList;
    private String lastName;
    private String name;
    private String pnr;
    private CheckinSearchReasonEnum reason;
    private CheckinRouteData selectedRoute;
    
    public Boolean getIsFullUndoList() {
		return isFullUndoList;
	}

	public void setIsFullUndoList(Boolean isFullUndoList) {
		this.isFullUndoList = isFullUndoList;
	}

	public String getLastName() {
		return lastName;
	}

    public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getPnr() {
		return pnr;
	}
	
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public CheckinSearchReasonEnum getReason() {
		return reason;
	}

	public void setReason(CheckinSearchReasonEnum reason) {
		this.reason = reason;
	}

	public CheckinRouteData getSelectedRoute() {
		return selectedRoute;
	}

	public void setSelectedRoute(CheckinRouteData selectedRoute) {
		this.selectedRoute = selectedRoute;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((isFullUndoList == null) ? 0 : isFullUndoList.hashCode());
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((pnr == null) ? 0 : pnr.hashCode());
		result = prime * result + ((reason == null) ? 0 : reason.hashCode());
		result = prime * result
				+ ((selectedRoute == null) ? 0 : selectedRoute.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PassengerListCheckinRequest other = (PassengerListCheckinRequest) obj;
		if (isFullUndoList == null) {
			if (other.isFullUndoList != null)
				return false;
		} else if (!isFullUndoList.equals(other.isFullUndoList))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (pnr == null) {
			if (other.pnr != null)
				return false;
		} else if (!pnr.equals(other.pnr))
			return false;
		if (reason != other.reason)
			return false;
		if (selectedRoute == null) {
			if (other.selectedRoute != null)
				return false;
		} else if (!selectedRoute.equals(other.selectedRoute))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PassengerListCheckinRequest [isFullUndoList=" + isFullUndoList
				+ ", lastName=" + lastName + ", name=" + name + ", pnr=" + pnr
				+ ", reason=" + reason + ", selectedRoute=" + selectedRoute
				+ ", toString()=" + super.toString() + "]";
	}

}
