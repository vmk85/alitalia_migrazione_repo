package com.alitalia.aem.common.data.home;

import java.util.List;

public class AwardBookingSearchData extends BookingSearchData {

	private List<RouteData> selectedFlights;

	public List<RouteData> getSelectedFlights() {
		return selectedFlights;
	}

	public void setSelectedFlights(List<RouteData> selectedFlights) {
		this.selectedFlights = selectedFlights;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((selectedFlights == null) ? 0 : selectedFlights.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AwardBookingSearchData other = (AwardBookingSearchData) obj;
		if (selectedFlights == null) {
			if (other.selectedFlights != null)
				return false;
		} else if (!selectedFlights.equals(other.selectedFlights))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AwardBookingSearchData [selectedFlights=" + selectedFlights
				+ ", getBookingSolutionId()=" + getBookingSolutionId()
				+ ", isMultiSlice()=" + isMultiSlice() + ", isNeutral()="
				+ isNeutral() + ", getPassengers()=" + getPassengers()
				+ ", getType()=" + getType() + ", getId()=" + getId()
				+ ", getSessionId()=" + getSessionId() + ", getSolutionSet()="
				+ getSolutionSet() + ", getSolutionId()=" + getSolutionId()
				+ ", getRefreshSolutionId()=" + getRefreshSolutionId()
				+ ", getFarmId()=" + getFarmId() + ", getProperties()="
				+ getProperties() + "]";
	}

}
