package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.checkin.CheckinUserData;
import com.alitalia.aem.common.messages.MmbBaseRequest;

public class WebCheckinAlertRequest extends MmbBaseRequest {
	
	private CheckinUserData user;
	
	public WebCheckinAlertRequest(String tid, String sid) {
		super(tid, sid);
	}

	public CheckinUserData getUser() {
		return user;
	}

	public void setUser(CheckinUserData user) {
		this.user = user;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebCheckinAlertRequest other = (WebCheckinAlertRequest) obj;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WebCheckinAlertRequest [user=" + user + "]";
	}
}