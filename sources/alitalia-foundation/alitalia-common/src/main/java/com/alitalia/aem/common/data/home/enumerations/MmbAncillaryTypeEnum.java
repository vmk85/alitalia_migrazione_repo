package com.alitalia.aem.common.data.home.enumerations;

public enum MmbAncillaryTypeEnum {

	SEAT("Seat"),
	EXTRA_BAGGAGE("ExtraBaggage"),
	MEAL("Meal"),
	VIP_LOUNGE("VipLounge"),
	FAST_TRACK("FastTrack"),
	INSURANCE("Insurance"),
	UPGRADE("Upgrade");

	private final String value;

    MmbAncillaryTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MmbAncillaryTypeEnum fromValue(String v) {
        for (MmbAncillaryTypeEnum c: MmbAncillaryTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
