package com.alitalia.aem.common.data.home.enumerations;

public enum MMContractStatusEnum {

	REGISTERED("Registered"),
    DELETE("Delete");

	private final String value;

	private MMContractStatusEnum(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	public static MMContractStatusEnum fromValue(String v) {
		for (MMContractStatusEnum c: MMContractStatusEnum.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
	
}
