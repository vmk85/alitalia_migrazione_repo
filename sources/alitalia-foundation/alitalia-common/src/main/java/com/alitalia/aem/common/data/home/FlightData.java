package com.alitalia.aem.common.data.home;

import java.util.Calendar;
import java.util.Comparator;
import java.util.List;

import com.alitalia.aem.common.data.home.enumerations.FlightTypeEnum;

public abstract class FlightData {

	protected PropertiesData properties;
	protected List<BrandData> brands;
	protected FlightTypeEnum flightType;
	protected Integer durationHour;
	protected Integer durationMinutes;

	public static final Comparator<FlightData> COMPARE_BY_DURATION_AND_DATE = new Comparator<FlightData>() {
		/*
		 * Comparazione per ordinamento voli
		 * I voli mostrati in matrice rispetteranno dei criteri di ordinamento. Il primo ordinamento avviene per
		 * + durata complessiva
		 * + nel caso in cui la durata di un volo differisce dalla durata del volo precedente 
		 *   per un numero di minuti inferiore a 20, verrà mostrato per primo il volo che parte 
		 *   prima (orario di partenza minore)
		 * 
		 * Quest'ultimo punto deve tenere conto del fatto che i voli possono essere Diretti o Indiretti.
		 * Solo i voli Diretti hanno una data di partenza, per cui si verifica l'istanza dell'oggetto
		 * e nel caso di volo Indiratto si prende la data di partenza del primo volo che lo costituisce - 
		 * si presuppone che un volo Indiretto sia composto solo di voli Diretti
		 */
		@Override
		public int compare(FlightData one, FlightData other) {
			int compareResult = 0;
			int oneMinutesDuration = one.durationHour * 60 + one.durationMinutes;
			int otherMinutesDuration = other.durationHour * 60 + other.durationMinutes;
			boolean compareAlsoDate = (Math.abs(oneMinutesDuration - otherMinutesDuration) <= 20);
			
			if (compareAlsoDate) {
				Calendar oneDepartureDate = null;
				Calendar otherDepartureDate = null;
				if (one instanceof DirectFlightData)
					oneDepartureDate = ((DirectFlightData) one).getDepartureDate();
				else
					oneDepartureDate = ((DirectFlightData) ((ConnectingFlightData) one).getFlights().get(0)).getDepartureDate();
	
				if (other instanceof DirectFlightData)
					otherDepartureDate = ((DirectFlightData) other).getDepartureDate();
				else
					otherDepartureDate = ((DirectFlightData) ((ConnectingFlightData) other).getFlights().get(0)).getDepartureDate();
				
				compareResult = oneDepartureDate.compareTo(otherDepartureDate);
			}
			else
			{
				if (oneMinutesDuration < otherMinutesDuration)
					compareResult = -1;
				else if (oneMinutesDuration > otherMinutesDuration)
					compareResult = 1;
				else
					compareResult = 0;
			}

			return compareResult;
		}
	};

	public PropertiesData getProperties() {
		return properties;
	}

	public void setProperties(PropertiesData properties) {
		this.properties = properties;
	}

	public List<BrandData> getBrands() {
		return brands;
	}

	public void setBrands(List<BrandData> brands) {
		this.brands = brands;
	}

	public void addBrand(BrandData brand) {
		this.brands.add(brand);
	}

	public void removeBrand(BrandData brand) {
		this.brands.remove(brand);
	}

	public FlightTypeEnum getFlightType() {
		return flightType;
	}

	public void setFlightType(FlightTypeEnum flightType) {
		this.flightType = flightType;
	}

	public Integer getDurationHour() {
		return durationHour;
	}

	public void setDurationHour(Integer duration) {
		this.durationHour = duration;
	}

	public Integer getDurationMinutes() {
		return durationMinutes;
	}

	public void setDurationMinutes(Integer durationMinutes) {
		this.durationMinutes = durationMinutes;
	}

	@Override
	public String toString() {
		return "FlightData [properties=" + properties + ", brands=" + brands
				+ ", flightType=" + flightType + ", durationHour="
				+ durationHour + ", durationMinutes=" + durationMinutes + "]";
	}

}
