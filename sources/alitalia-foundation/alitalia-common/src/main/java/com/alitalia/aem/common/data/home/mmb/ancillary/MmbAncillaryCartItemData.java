package com.alitalia.aem.common.data.home.mmb.ancillary;

import com.alitalia.aem.common.data.MmbAncillaryDetail;

public class MmbAncillaryCartItemData {

	private MmbAncillaryDetail ancillaryDetail;
	private Integer ancillaryId;
	private Integer quantity;

	public MmbAncillaryDetail getAncillaryDetail() {
		return ancillaryDetail;
	}

	public void setAncillaryDetail(MmbAncillaryDetail ancillaryDetail) {
		this.ancillaryDetail = ancillaryDetail;
	}

	public Integer getAncillaryId() {
		return ancillaryId;
	}

	public void setAncillaryId(Integer ancillaryId) {
		this.ancillaryId = ancillaryId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((ancillaryId == null) ? 0 : ancillaryId.hashCode());
		result = prime * result
				+ ((quantity == null) ? 0 : quantity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbAncillaryCartItemData other = (MmbAncillaryCartItemData) obj;
		if (ancillaryId == null) {
			if (other.ancillaryId != null)
				return false;
		} else if (!ancillaryId.equals(other.ancillaryId))
			return false;
		if (quantity == null) {
			if (other.quantity != null)
				return false;
		} else if (!quantity.equals(other.quantity))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbAncillaryOperationData [ancillaryDetail=" + ancillaryDetail
				+ ", ancillaryId=" + ancillaryId + ", quantity=" + quantity
				+ "]";
	}

}
