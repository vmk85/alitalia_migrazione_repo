package com.alitalia.aem.common.data.home;

import java.util.List;

public class ResultBookingPriceSolutionData {
	
	private String saleTotalField;
	private List<ResultBookingPriceSolutionSliceData> sliceField;
	
	public String getSaleTotalField() {
		return saleTotalField;
	}
	
	public void setSaleTotalField(String saleTotalField) {
		this.saleTotalField = saleTotalField;
	}
	
	public List<ResultBookingPriceSolutionSliceData> getSliceField() {
		return sliceField;
	}
	
	public void setSliceField(
			List<ResultBookingPriceSolutionSliceData> sliceField) {
		this.sliceField = sliceField;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((saleTotalField == null) ? 0 : saleTotalField.hashCode());
		result = prime * result
				+ ((sliceField == null) ? 0 : sliceField.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingPriceSolutionData other = (ResultBookingPriceSolutionData) obj;
		if (saleTotalField == null) {
			if (other.saleTotalField != null)
				return false;
		} else if (!saleTotalField.equals(other.saleTotalField))
			return false;
		if (sliceField == null) {
			if (other.sliceField != null)
				return false;
		} else if (!sliceField.equals(other.sliceField))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingPriceSolutionData [saleTotalField="
				+ saleTotalField + ", sliceField=" + sliceField + "]";
	}
	
}
