package com.alitalia.aem.common.data.home;

import java.math.BigDecimal;
import java.util.List;

public class BrandData extends BoomBoxData {

	// AwardPrice viene usato solo da Award Booking
	private BigDecimal awardPrice;
	private Integer advancePurchaseDays;
	private List<BaggageAllowanceData> baggageAllowanceList;
	private Boolean bestFare;
	private String code;
	private String compartimentalClass;
	private String currency;
	private Boolean enabled;
	private BigDecimal grossFare;
	private Integer index;
	private BigDecimal netFare;
	private List<String> notes;
	private BrandPenaltiesData penalties;
	private BigDecimal realPrice;
	private Integer seatsAvailable;
	private Boolean selected;
	private String tridionName;

	public BigDecimal getAwardPrice() {
		return awardPrice;
	}

	public void setAwardPrice(BigDecimal awardPrice) {
		this.awardPrice = awardPrice;
	}

	public Integer getAdvancePurchaseDays() {
		return advancePurchaseDays;
	}

	public void setAdvancePurchaseDays(Integer advancePurchaseDays) {
		this.advancePurchaseDays = advancePurchaseDays;
	}

	public List<BaggageAllowanceData> getBaggageAllowanceList() {
		return baggageAllowanceList;
	}

	public void setBaggageAllowanceList(List<BaggageAllowanceData> baggageAllowanceList) {
		this.baggageAllowanceList = baggageAllowanceList;
	}

	public Boolean isBestFare() {
		return bestFare;
	}

	public void setBestFare(Boolean bestFare) {
		this.bestFare = bestFare;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCompartimentalClass() {
		return compartimentalClass;
	}

	public void setCompartimentalClass(String compartimentalClass) {
		this.compartimentalClass = compartimentalClass;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public BigDecimal getGrossFare() {
		return grossFare;
	}

	public void setGrossFare(BigDecimal grossFare) {
		this.grossFare = grossFare;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public BigDecimal getNetFare() {
		return netFare;
	}

	public void setNetFare(BigDecimal netFare) {
		this.netFare = netFare;
	}

	public List<String> getNotes() {
		return notes;
	}

	public void setNotes(List<String> notes) {
		this.notes = notes;
	}

	public BrandPenaltiesData getPenalties() {
		return penalties;
	}

	public void setPenalties(BrandPenaltiesData penalties) {
		this.penalties = penalties;
	}

	public BigDecimal getRealPrice() {
		return realPrice;
	}

	public void setRealPrice(BigDecimal realPrice) {
		this.realPrice = realPrice;
	}

	public Integer getSeatsAvailable() {
		return seatsAvailable;
	}

	public void setSeatsAvailable(Integer seatsAvailable) {
		this.seatsAvailable = seatsAvailable;
	}

	public Boolean isSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public String getTridionName() {
		return tridionName;
	}

	public void setTridionName(String tridionName) {
		this.tridionName = tridionName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		BrandData other = (BrandData) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BrandData [awardPrice=" + awardPrice + ", advancePurchaseDays="
				+ advancePurchaseDays + ", baggageAllowanceList="
				+ baggageAllowanceList + ", bestFare=" + bestFare + ", code="
				+ code + ", compartimentalClass=" + compartimentalClass
				+ ", currency=" + currency + ", enabled=" + enabled
				+ ", grossFare=" + grossFare + ", index=" + index
				+ ", netFare=" + netFare + ", notes=" + notes + ", penalties="
				+ penalties + ", realPrice=" + realPrice + ", seatsAvailable="
				+ seatsAvailable + ", selected=" + selected + ", tridionName="
				+ tridionName + ", getId()=" + getId() + ", getSessionId()="
				+ getSessionId() + ", getSolutionSet()=" + getSolutionSet()
				+ ", getSolutionId()=" + getSolutionId()
				+ ", getRefreshSolutionId()=" + getRefreshSolutionId()
				+ ", getFarmId()=" + getFarmId() + ", getProperties()="
				+ getProperties() + "]";
	}

}
