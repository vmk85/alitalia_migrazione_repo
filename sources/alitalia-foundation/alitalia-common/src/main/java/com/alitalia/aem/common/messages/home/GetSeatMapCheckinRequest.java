package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.messages.MmbBaseRequest;

public class GetSeatMapCheckinRequest extends MmbBaseRequest {

	private MmbCompartimentalClassEnum compartimentalClass;
	private String eticket;
	private CheckinFlightData flight;
	private String market;
	
	public MmbCompartimentalClassEnum getCompartimentalClass() {
		return compartimentalClass;
	}
	public void setCompartimentalClass(
			MmbCompartimentalClassEnum compartimentalClass) {
		this.compartimentalClass = compartimentalClass;
	}
	public String getEticket() {
		return eticket;
	}
	public void setEticket(String eticket) {
		this.eticket = eticket;
	}
	public CheckinFlightData getFlight() {
		return flight;
	}
	public void setFlight(CheckinFlightData flight) {
		this.flight = flight;
	}
	public String getMarket() {
		return market;
	}
	public void setMarket(String market) {
		this.market = market;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((compartimentalClass == null) ? 0 : compartimentalClass
						.hashCode());
		result = prime * result + ((eticket == null) ? 0 : eticket.hashCode());
		result = prime * result + ((flight == null) ? 0 : flight.hashCode());
		result = prime * result + ((market == null) ? 0 : market.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		GetSeatMapCheckinRequest other = (GetSeatMapCheckinRequest) obj;
		if (compartimentalClass != other.compartimentalClass)
			return false;
		if (eticket == null) {
			if (other.eticket != null)
				return false;
		} else if (!eticket.equals(other.eticket))
			return false;
		if (flight == null) {
			if (other.flight != null)
				return false;
		} else if (!flight.equals(other.flight))
			return false;
		if (market == null) {
			if (other.market != null)
				return false;
		} else if (!market.equals(other.market))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "GetSeatMapCheckinRequest [compartimentalClass="
				+ compartimentalClass + ", eticket=" + eticket + ", flight="
				+ flight + ", market=" + market + ", toString()="
				+ super.toString() + "]";
	}

}
