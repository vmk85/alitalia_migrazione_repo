package com.alitalia.aem.common.data.home;

import java.util.ArrayList;
import java.util.List;

import com.alitalia.aem.common.data.home.enumerations.BookingTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.messages.home.CashAndMilesData;

public class RoutesData extends BoomBoxData {

    private BillingData billing;
    private CabinEnum cabin;
    private CashAndMilesData cashAndMiles;
    private ECouponData coupon;
    private InsurancePolicyData insurance;
    private BookingTypeEnum bookingType;
    private MmCustomerData milleMigliaCustomer;
    private String mseType;
    private String nationalInsuranceNumber;
    private String pnr;
    private List<PassengerBaseData> passengers;
    private PaymentData payment;
    private List<RouteData> routesList;
    private Integer sliceCount;
    private String tradeAgencyCode;
	
    public BillingData getBilling() {
		return billing;
	}
	
    public void setBilling(BillingData billing) {
		this.billing = billing;
	}
	
    public CabinEnum getCabin() {
		return cabin;
	}
	
    public void setCabin(CabinEnum cabin) {
		this.cabin = cabin;
	}
	
    public CashAndMilesData getCashAndMiles() {
		return cashAndMiles;
	}
	
    public void setCashAndMiles(CashAndMilesData cashAndMiles) {
		this.cashAndMiles = cashAndMiles;
	}
	
    public ECouponData getCoupon() {
		return coupon;
	}
	
    public void setCoupon(ECouponData coupon) {
		this.coupon = coupon;
	}
	
    public InsurancePolicyData getInsurance() {
		return insurance;
	}
	
    public void setInsurance(InsurancePolicyData insurance) {
		this.insurance = insurance;
	}
	
    public BookingTypeEnum getBookingType() {
		return bookingType;
	}

	public void setBookingType(BookingTypeEnum bookingType) {
		this.bookingType = bookingType;
	}

	public MmCustomerData getMilleMigliaCustomer() {
		return milleMigliaCustomer;
	}
	
    public void setMilleMigliaCustomer(MmCustomerData milleMigliaCustomer) {
		this.milleMigliaCustomer = milleMigliaCustomer;
	}
	
    public String getMseType() {
		return mseType;
	}
	
    public void setMseType(String mseType) {
		this.mseType = mseType;
	}
	
    public String getNationalInsuranceNumber() {
		return nationalInsuranceNumber;
	}
	
    public void setNationalInsuranceNumber(String nationalInsuranceNumber) {
		this.nationalInsuranceNumber = nationalInsuranceNumber;
	}
	
    public String getPnr() {
		return pnr;
	}
	
    public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	
    public List<PassengerBaseData> getPassengers() {
		return passengers;
	}
	
    public void setPassengers(List<PassengerBaseData> passengers) {
		this.passengers = passengers;
	}
	
    public PaymentData getPayment() {
		return payment;
	}
	
    public void setPayment(PaymentData payment) {
		this.payment = payment;
	}
	
    public List<RouteData> getRoutesList() {
		return routesList;
	}
	
    public void setRoutesList(List<RouteData> routesList) {
		this.routesList = routesList;
	}
	
    public Integer getSliceCount() {
		return sliceCount;
	}
	
    public void setSliceCount(Integer sliceCount) {
		this.sliceCount = sliceCount;
	}

    public void addRoutesListElement(RouteData element) {
    	if (this.routesList == null)
    		this.routesList = new ArrayList<RouteData>();

    	this.routesList.add(element);
    }

    public String getTradeAgencyCode() {
		return tradeAgencyCode;
	}

	public void setTradeAgencyCode(String tradeAgencyCode) {
		this.tradeAgencyCode = tradeAgencyCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((billing == null) ? 0 : billing.hashCode());
		result = prime * result
				+ ((bookingType == null) ? 0 : bookingType.hashCode());
		result = prime * result + ((cabin == null) ? 0 : cabin.hashCode());
		result = prime * result
				+ ((cashAndMiles == null) ? 0 : cashAndMiles.hashCode());
		result = prime * result + ((coupon == null) ? 0 : coupon.hashCode());
		result = prime * result
				+ ((insurance == null) ? 0 : insurance.hashCode());
		result = prime
				* result
				+ ((milleMigliaCustomer == null) ? 0 : milleMigliaCustomer
						.hashCode());
		result = prime * result + ((mseType == null) ? 0 : mseType.hashCode());
		result = prime
				* result
				+ ((nationalInsuranceNumber == null) ? 0
						: nationalInsuranceNumber.hashCode());
		result = prime * result
				+ ((passengers == null) ? 0 : passengers.hashCode());
		result = prime * result + ((payment == null) ? 0 : payment.hashCode());
		result = prime * result + ((pnr == null) ? 0 : pnr.hashCode());
		result = prime * result
				+ ((routesList == null) ? 0 : routesList.hashCode());
		result = prime * result
				+ ((sliceCount == null) ? 0 : sliceCount.hashCode());
		result = prime * result
				+ ((tradeAgencyCode == null) ? 0 : tradeAgencyCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RoutesData other = (RoutesData) obj;
		if (billing == null) {
			if (other.billing != null)
				return false;
		} else if (!billing.equals(other.billing))
			return false;
		if (bookingType != other.bookingType)
			return false;
		if (cabin != other.cabin)
			return false;
		if (cashAndMiles == null) {
			if (other.cashAndMiles != null)
				return false;
		} else if (!cashAndMiles.equals(other.cashAndMiles))
			return false;
		if (coupon == null) {
			if (other.coupon != null)
				return false;
		} else if (!coupon.equals(other.coupon))
			return false;
		if (insurance == null) {
			if (other.insurance != null)
				return false;
		} else if (!insurance.equals(other.insurance))
			return false;
		if (milleMigliaCustomer == null) {
			if (other.milleMigliaCustomer != null)
				return false;
		} else if (!milleMigliaCustomer.equals(other.milleMigliaCustomer))
			return false;
		if (mseType == null) {
			if (other.mseType != null)
				return false;
		} else if (!mseType.equals(other.mseType))
			return false;
		if (nationalInsuranceNumber == null) {
			if (other.nationalInsuranceNumber != null)
				return false;
		} else if (!nationalInsuranceNumber
				.equals(other.nationalInsuranceNumber))
			return false;
		if (passengers == null) {
			if (other.passengers != null)
				return false;
		} else if (!passengers.equals(other.passengers))
			return false;
		if (payment == null) {
			if (other.payment != null)
				return false;
		} else if (!payment.equals(other.payment))
			return false;
		if (pnr == null) {
			if (other.pnr != null)
				return false;
		} else if (!pnr.equals(other.pnr))
			return false;
		if (routesList == null) {
			if (other.routesList != null)
				return false;
		} else if (!routesList.equals(other.routesList))
			return false;
		if (sliceCount == null) {
			if (other.sliceCount != null)
				return false;
		} else if (!sliceCount.equals(other.sliceCount))
			return false;
		if (tradeAgencyCode == null) {
			if (other.tradeAgencyCode != null)
				return false;
		} else if (!tradeAgencyCode.equals(other.tradeAgencyCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RoutesData [billing=" + billing + ", cabin=" + cabin
				+ ", cashAndMiles=" + cashAndMiles + ", coupon=" + coupon
				+ ", insurance=" + insurance + ", bookingType=" + bookingType
				+ ", milleMigliaCustomer=" + milleMigliaCustomer + ", mseType="
				+ mseType + ", nationalInsuranceNumber="
				+ nationalInsuranceNumber + ", pnr=" + pnr + ", passengers="
				+ passengers + ", payment=" + payment + ", routesList="
				+ routesList + ", sliceCount=" + sliceCount
				+ ", tradeAgencyCode=" + tradeAgencyCode + ", getId()="
				+ getId() + ", getSessionId()=" + getSessionId()
				+ ", getSolutionSet()=" + getSolutionSet()
				+ ", getSolutionId()=" + getSolutionId()
				+ ", getRefreshSolutionId()=" + getRefreshSolutionId()
				+ ", getFarmId()=" + getFarmId() + ", getProperties()="
				+ getProperties() + "]";
	}
    
}
