package com.alitalia.aem.common.data.home.checkin;

public class CheckinPaymentReceiptData {
	
	private Integer id;
	private String Locale;
	private CheckinPassengerData passenger;
	private CheckinRouteData route;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getLocale() {
		return Locale;
	}
	public void setLocale(String locale) {
		Locale = locale;
	}
	public CheckinPassengerData getPassenger() {
		return passenger;
	}
	public void setPassenger(CheckinPassengerData passenger) {
		this.passenger = passenger;
	}
	public CheckinRouteData getRoute() {
		return route;
	}
	public void setRoute(CheckinRouteData route) {
		this.route = route;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Locale == null) ? 0 : Locale.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((passenger == null) ? 0 : passenger.hashCode());
		result = prime * result + ((route == null) ? 0 : route.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinPaymentReceiptData other = (CheckinPaymentReceiptData) obj;
		if (Locale == null) {
			if (other.Locale != null)
				return false;
		} else if (!Locale.equals(other.Locale))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (passenger == null) {
			if (other.passenger != null)
				return false;
		} else if (!passenger.equals(other.passenger))
			return false;
		if (route == null) {
			if (other.route != null)
				return false;
		} else if (!route.equals(other.route))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CheckinPaymentReceiptData [id=" + id + ", Locale=" + Locale
				+ ", passenger=" + passenger + ", route=" + route + "]";
	}

}
