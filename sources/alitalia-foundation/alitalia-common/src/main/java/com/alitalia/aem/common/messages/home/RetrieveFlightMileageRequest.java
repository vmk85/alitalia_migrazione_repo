package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.messages.BaseRequest;

public class RetrieveFlightMileageRequest extends BaseRequest {

	public RetrieveFlightMileageRequest(String tid, String sid) {
		super(tid, sid);
	}
	
	private DirectFlightData directFlight;

	public DirectFlightData getDirectFlight() {
		return directFlight;
	}

	public void setDirectFlight(DirectFlightData directFlight) {
		this.directFlight = directFlight;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((directFlight == null) ? 0 : directFlight.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveFlightMileageRequest other = (RetrieveFlightMileageRequest) obj;
		if (directFlight == null) {
			if (other.directFlight != null)
				return false;
		} else if (!directFlight.equals(other.directFlight))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveFlightMileageRequest [directFlight=" + directFlight + "]";
	}
}