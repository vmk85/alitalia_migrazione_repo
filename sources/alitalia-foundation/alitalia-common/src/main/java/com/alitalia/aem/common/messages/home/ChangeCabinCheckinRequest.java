package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.messages.MmbBaseRequest;

public class ChangeCabinCheckinRequest extends MmbBaseRequest {

	private MmbCompartimentalClassEnum cabin;
	private CheckinFlightData flight;
	private CheckinPassengerData passenger;
	private String seat;
	
	public MmbCompartimentalClassEnum getCabin() {
		return cabin;
	}
	public void setCabin(MmbCompartimentalClassEnum cabin) {
		this.cabin = cabin;
	}
	public CheckinFlightData getFlight() {
		return flight;
	}
	public void setFlight(CheckinFlightData flight) {
		this.flight = flight;
	}
	public CheckinPassengerData getPassenger() {
		return passenger;
	}
	public void setPassenger(CheckinPassengerData passenger) {
		this.passenger = passenger;
	}
	public String getSeat() {
		return seat;
	}
	public void setSeat(String seat) {
		this.seat = seat;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((cabin == null) ? 0 : cabin.hashCode());
		result = prime * result + ((flight == null) ? 0 : flight.hashCode());
		result = prime * result
				+ ((passenger == null) ? 0 : passenger.hashCode());
		result = prime * result + ((seat == null) ? 0 : seat.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChangeCabinCheckinRequest other = (ChangeCabinCheckinRequest) obj;
		if (cabin != other.cabin)
			return false;
		if (flight == null) {
			if (other.flight != null)
				return false;
		} else if (!flight.equals(other.flight))
			return false;
		if (passenger == null) {
			if (other.passenger != null)
				return false;
		} else if (!passenger.equals(other.passenger))
			return false;
		if (seat == null) {
			if (other.seat != null)
				return false;
		} else if (!seat.equals(other.seat))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ChangeCabinCheckinRequest [cabin=" + cabin + ", flight="
				+ flight + ", passenger=" + passenger + ", seat=" + seat
				+ ", toString()=" + super.toString() + "]";
	}

}
