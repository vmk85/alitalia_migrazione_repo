package com.alitalia.aem.common.data.home;

public class ResultTicketingDetailSolutionExtData {

    private Boolean privateFareField;
    private Boolean privateFareFieldSpecified;

    public Boolean isPrivateFareField() {
        return privateFareField;
    }

    public void setPrivateFareField(Boolean value) {
        this.privateFareField = value;
    }

    public Boolean isPrivateFareFieldSpecified() {
        return privateFareFieldSpecified;
    }

    public void setPrivateFareFieldSpecified(Boolean value) {
        this.privateFareFieldSpecified = value;
    }

}
