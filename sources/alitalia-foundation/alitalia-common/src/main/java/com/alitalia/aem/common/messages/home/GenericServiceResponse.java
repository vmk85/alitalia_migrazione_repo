package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class GenericServiceResponse extends BaseResponse{

	private Boolean genericResult;
	
	public GenericServiceResponse(){}
	
	public GenericServiceResponse(String tid, String sid) {
		super(tid, sid);
	}

	public Boolean getGenericResult() {
		return genericResult;
	}

	public void setGenericResult(Boolean genericResult) {
		this.genericResult = genericResult;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((genericResult == null) ? 0 : genericResult.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		GenericServiceResponse other = (GenericServiceResponse) obj;
		if (genericResult == null) {
			if (other.genericResult != null)
				return false;
		} else if (!genericResult.equals(other.genericResult))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "GenericServiceResponse [genericResult=" + genericResult
				+ ", toString()=" + super.toString() + "]";
	}

}