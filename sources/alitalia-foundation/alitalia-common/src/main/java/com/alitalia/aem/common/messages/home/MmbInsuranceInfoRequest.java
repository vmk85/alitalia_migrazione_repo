package com.alitalia.aem.common.messages.home;

import java.util.Calendar;
import java.util.List;

import com.alitalia.aem.common.data.PassengerBase;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.messages.BaseRequest;

public class MmbInsuranceInfoRequest extends BaseRequest {

	private AreaValueEnum area;
	private String arrivalAirport;
	private Calendar arrivalDate;
	private String countryCode;
	private String departureAirport;
	private Calendar departureDate;
	private String languageCode;
	private Integer passengersCount;
	private String pnr;
	private RouteTypeEnum routeType;
	private boolean flgUsaCa;
	
	public AreaValueEnum getArea() {
		return area;
	}
	
	public void setArea(AreaValueEnum area) {
		this.area = area;
	}
	
	public String getArrivalAirport() {
		return arrivalAirport;
	}
	
	public void setArrivalAirport(String arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}
	
	public Calendar getArrivalDate() {
		return arrivalDate;
	}
	
	public void setArrivalDate(Calendar arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	
	public String getCountryCode() {
		return countryCode;
	}
	
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
	public String getDepartureAirport() {
		return departureAirport;
	}
	
	public void setDepartureAirport(String departureAirport) {
		this.departureAirport = departureAirport;
	}
	
	public Calendar getDepartureDate() {
		return departureDate;
	}
	
	public void setDepartureDate(Calendar departureDate) {
		this.departureDate = departureDate;
	}
	
	public String getLanguageCode() {
		return languageCode;
	}
	
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	
	public Integer getPassengersCount() {
		return passengersCount;
	}
	
	public void setPassengersCount(Integer passengers) {
		this.passengersCount = passengers;
	}
	
	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public RouteTypeEnum getRouteType() {
		return routeType;
	}
	
	public void setRouteType(RouteTypeEnum routeType) {
		this.routeType = routeType;
	}
	
	public boolean isFlgUsaCa() {
		return flgUsaCa;
	}
	
	public void setFlgUsaCa(boolean flgUsaCa) {
		this.flgUsaCa = flgUsaCa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((area == null) ? 0 : area.hashCode());
		result = prime * result
				+ ((arrivalAirport == null) ? 0 : arrivalAirport.hashCode());
		result = prime * result
				+ ((arrivalDate == null) ? 0 : arrivalDate.hashCode());
		result = prime * result
				+ ((countryCode == null) ? 0 : countryCode.hashCode());
		result = prime
				* result
				+ ((departureAirport == null) ? 0 : departureAirport.hashCode());
		result = prime * result
				+ ((departureDate == null) ? 0 : departureDate.hashCode());
		result = prime * result + (flgUsaCa ? 1231 : 1237);
		result = prime * result
				+ ((languageCode == null) ? 0 : languageCode.hashCode());
		result = prime * result
				+ ((passengersCount == null) ? 0 : passengersCount.hashCode());
		result = prime * result + ((pnr == null) ? 0 : pnr.hashCode());
		result = prime * result
				+ ((routeType == null) ? 0 : routeType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbInsuranceInfoRequest other = (MmbInsuranceInfoRequest) obj;
		if (area != other.area)
			return false;
		if (arrivalAirport == null) {
			if (other.arrivalAirport != null)
				return false;
		} else if (!arrivalAirport.equals(other.arrivalAirport))
			return false;
		if (arrivalDate == null) {
			if (other.arrivalDate != null)
				return false;
		} else if (!arrivalDate.equals(other.arrivalDate))
			return false;
		if (countryCode == null) {
			if (other.countryCode != null)
				return false;
		} else if (!countryCode.equals(other.countryCode))
			return false;
		if (departureAirport == null) {
			if (other.departureAirport != null)
				return false;
		} else if (!departureAirport.equals(other.departureAirport))
			return false;
		if (departureDate == null) {
			if (other.departureDate != null)
				return false;
		} else if (!departureDate.equals(other.departureDate))
			return false;
		if (flgUsaCa != other.flgUsaCa)
			return false;
		if (languageCode == null) {
			if (other.languageCode != null)
				return false;
		} else if (!languageCode.equals(other.languageCode))
			return false;
		if (passengersCount == null) {
			if (other.passengersCount != null)
				return false;
		} else if (!passengersCount.equals(other.passengersCount))
			return false;
		if (pnr == null) {
			if (other.pnr != null)
				return false;
		} else if (!pnr.equals(other.pnr))
			return false;
		if (routeType != other.routeType)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbInsuranceInfoRequest [area=" + area + ", arrivalAirport="
				+ arrivalAirport + ", arrivalDate=" + arrivalDate
				+ ", countryCode=" + countryCode + ", departureAirport="
				+ departureAirport + ", departureDate=" + departureDate
				+ ", languageCode=" + languageCode + ", passengersCount="
				+ passengersCount + ", pnr=" + pnr + ", routeType=" + routeType
				+ ", flgUsaCa=" + flgUsaCa + ", getTid()=" + getTid()
				+ ", getSid()=" + getSid() + "]";
	}

}