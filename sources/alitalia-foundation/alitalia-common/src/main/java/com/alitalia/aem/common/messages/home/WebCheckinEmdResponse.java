package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.checkin.EmdErrorData;
import com.alitalia.aem.common.data.home.checkin.EmdItemData;
import com.alitalia.aem.common.data.home.checkin.EmdWarningData;
import com.alitalia.aem.common.messages.BaseResponse;

public class WebCheckinEmdResponse extends BaseResponse {

	private List<EmdErrorData> errors;
    private List<EmdItemData> items;
    private Boolean success;
    private List<EmdWarningData> warnings;
	public List<EmdErrorData> getErrors() {
		return errors;
	}
	public void setErrors(List<EmdErrorData> errors) {
		this.errors = errors;
	}
	public List<EmdItemData> getItems() {
		return items;
	}
	public void setItems(List<EmdItemData> items) {
		this.items = items;
	}
	public Boolean getSuccess() {
		return success;
	}
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	public List<EmdWarningData> getWarnings() {
		return warnings;
	}
	public void setWarnings(List<EmdWarningData> warnings) {
		this.warnings = warnings;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((errors == null) ? 0 : errors.hashCode());
		result = prime * result + ((items == null) ? 0 : items.hashCode());
		result = prime * result + ((success == null) ? 0 : success.hashCode());
		result = prime * result
				+ ((warnings == null) ? 0 : warnings.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebCheckinEmdResponse other = (WebCheckinEmdResponse) obj;
		if (errors == null) {
			if (other.errors != null)
				return false;
		} else if (!errors.equals(other.errors))
			return false;
		if (items == null) {
			if (other.items != null)
				return false;
		} else if (!items.equals(other.items))
			return false;
		if (success == null) {
			if (other.success != null)
				return false;
		} else if (!success.equals(other.success))
			return false;
		if (warnings == null) {
			if (other.warnings != null)
				return false;
		} else if (!warnings.equals(other.warnings))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "WebCheckinEmdResponse [errors=" + errors + ", items=" + items
				+ ", success=" + success + ", warnings=" + warnings + "]";
	}
}