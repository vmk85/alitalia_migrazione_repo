package com.alitalia.aem.common.data.home.enumerations;

public enum MmbPassengerStatusEnum {

    INVALID("Invalid"),
    REGULAR("Regular"),
    ALREADY_CHECKED_IN("AlreadyCheckedIn"),
    NOT_CHECKED_IN_PREVIOUS_FLIGHT("NotCheckedInPreviousFlight"),
    INFANT("Infant"),
    INFANT_ACCOMPANIST("InfantAccompanist"),
    OTHER_ACCOMPANIST("OtherAccompanist"),
    SPECIAL_PASSENGER("SpecialPassenger"),
    CHECKED_IN("CheckedIn"),
    NOT_CHECKED_IN("NotCheckedIn");
    private final String value;

    MmbPassengerStatusEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MmbPassengerStatusEnum fromValue(String v) {
        for (MmbPassengerStatusEnum c: MmbPassengerStatusEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
