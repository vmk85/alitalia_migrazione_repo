package com.alitalia.aem.common.data.home;

public class ResultTicketingDetailSolutionSliceSegmentPricingFareData {

    private String carrierField;
    private String city1Field;
    private String city2Field;
    private String extendedFareCodeField;

    public String getCarrierField() {
        return carrierField;
    }

    public void setCarrierField(String value) {
        this.carrierField = value;
    }

    public String getCity1Field() {
        return city1Field;
    }

    public void setCity1Field(String value) {
        this.city1Field = value;
    }

    public String getCity2Field() {
        return city2Field;
    }

    public void setCity2Field(String value) {
        this.city2Field = value;
    }

    public String getExtendedFareCodeField() {
        return extendedFareCodeField;
    }

    public void setExtendedFareCodeField(String value) {
        this.extendedFareCodeField = value;
    }

}
