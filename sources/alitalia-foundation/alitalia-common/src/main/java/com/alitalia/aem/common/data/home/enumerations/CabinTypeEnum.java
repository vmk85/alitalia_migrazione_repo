package com.alitalia.aem.common.data.home.enumerations;

public enum CabinTypeEnum {

    PERMITTED("Permitted"),
    PREFERRED("Preferred");
    private final String value;

    CabinTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CabinTypeEnum fromValue(String v) {
        for (CabinTypeEnum c: CabinTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
