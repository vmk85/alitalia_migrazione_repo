package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.MmbBaseRequest;


public class ExtraBaggagePolicyCheckinRequest extends MmbBaseRequest {

	private Boolean anySpecialPassenger;
	private Integer bagaAllowance;
	private String codAptFrom;
	private String codAptTo;
	private String marketCode;
	
	public Boolean getAnySpecialPassenger() {
		return anySpecialPassenger;
	}
	public void setAnySpecialPassenger(Boolean anySpecialPassenger) {
		this.anySpecialPassenger = anySpecialPassenger;
	}
	public Integer getBagaAllowance() {
		return bagaAllowance;
	}
	public void setBagaAllowance(Integer bagaAllowance) {
		this.bagaAllowance = bagaAllowance;
	}
	public String getCodAptFrom() {
		return codAptFrom;
	}
	public void setCodAptFrom(String codAptFrom) {
		this.codAptFrom = codAptFrom;
	}
	public String getCodAptTo() {
		return codAptTo;
	}
	public void setCodAptTo(String codAptTo) {
		this.codAptTo = codAptTo;
	}
	public String getMarketCode() {
		return marketCode;
	}
	public void setMarketCode(String marketCode) {
		this.marketCode = marketCode;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((anySpecialPassenger == null) ? 0 : anySpecialPassenger
						.hashCode());
		result = prime * result
				+ ((bagaAllowance == null) ? 0 : bagaAllowance.hashCode());
		result = prime * result
				+ ((codAptFrom == null) ? 0 : codAptFrom.hashCode());
		result = prime * result
				+ ((codAptTo == null) ? 0 : codAptTo.hashCode());
		result = prime * result
				+ ((marketCode == null) ? 0 : marketCode.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExtraBaggagePolicyCheckinRequest other = (ExtraBaggagePolicyCheckinRequest) obj;
		if (anySpecialPassenger == null) {
			if (other.anySpecialPassenger != null)
				return false;
		} else if (!anySpecialPassenger.equals(other.anySpecialPassenger))
			return false;
		if (bagaAllowance == null) {
			if (other.bagaAllowance != null)
				return false;
		} else if (!bagaAllowance.equals(other.bagaAllowance))
			return false;
		if (codAptFrom == null) {
			if (other.codAptFrom != null)
				return false;
		} else if (!codAptFrom.equals(other.codAptFrom))
			return false;
		if (codAptTo == null) {
			if (other.codAptTo != null)
				return false;
		} else if (!codAptTo.equals(other.codAptTo))
			return false;
		if (marketCode == null) {
			if (other.marketCode != null)
				return false;
		} else if (!marketCode.equals(other.marketCode))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ExtraBaggagePolicyCheckinRequest [anySpecialPassenger="
				+ anySpecialPassenger + ", bagaAllowance=" + bagaAllowance
				+ ", codAptFrom=" + codAptFrom + ", codAptTo=" + codAptTo
				+ ", marketCode=" + marketCode + ", toString()="
				+ super.toString() + "]";
	}
	
}
