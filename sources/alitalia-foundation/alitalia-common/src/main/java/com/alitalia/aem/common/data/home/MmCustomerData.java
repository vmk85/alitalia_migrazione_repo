package com.alitalia.aem.common.data.home;

import java.util.Calendar;
import java.util.List;

import com.alitalia.aem.common.data.home.enumerations.GenderTypeEnum;

public class MmCustomerData {

    private Calendar birthDate;
    private List<FFCardData> cards;
    private String code;
    private List<ContactData> customerPhones;
    private String email;
    private GenderTypeEnum gender;
    private Boolean ignore;
    private String lastName;
    private MealData mealPreference;
    private Integer milesBalance;
    private Integer milesEarned;
    private Integer milesQualify;
    private String name;
    private String pin;
    private SeatTypeData seatPreference;
    private String tierCode;
    private String title;
	
    public Calendar getBirthDate() {
		return birthDate;
	}
	
    public void setBirthDate(Calendar birthDate) {
		this.birthDate = birthDate;
	}
	
    public List<FFCardData> getCards() {
		return cards;
	}
	
    public void setCards(List<FFCardData> cards) {
		this.cards = cards;
	}
	
    public String getCode() {
		return code;
	}
	
    public void setCode(String code) {
		this.code = code;
	}
	
    public List<ContactData> getCustomerPhones() {
		return customerPhones;
	}
	
    public void setCustomerPhones(List<ContactData> customerPhones) {
		this.customerPhones = customerPhones;
	}
	
    public String getEmail() {
		return email;
	}
	
    public void setEmail(String email) {
		this.email = email;
	}
	
    public GenderTypeEnum getGender() {
		return gender;
	}
	
    public void setGender(GenderTypeEnum gender) {
		this.gender = gender;
	}
	
    public Boolean getIgnore() {
		return ignore;
	}
	
    public void setIgnore(Boolean ignore) {
		this.ignore = ignore;
	}
	
    public String getLastName() {
		return lastName;
	}
	
    public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
    public MealData getMealPreference() {
		return mealPreference;
	}
	
    public void setMealPreference(MealData mealPreference) {
		this.mealPreference = mealPreference;
	}
	
    public Integer getMilesBalance() {
		return milesBalance;
	}
	
    public void setMilesBalance(Integer milesBalance) {
		this.milesBalance = milesBalance;
	}
	
    public Integer getMilesEarned() {
		return milesEarned;
	}
	
    public void setMilesEarned(Integer milesEarned) {
		this.milesEarned = milesEarned;
	}
	
    public Integer getMilesQualify() {
		return milesQualify;
	}
	
    public void setMilesQualify(Integer milesQualify) {
		this.milesQualify = milesQualify;
	}
	
    public String getName() {
		return name;
	}
	
    public void setName(String name) {
		this.name = name;
	}
	
    public String getPin() {
		return pin;
	}
	
    public void setPin(String pin) {
		this.pin = pin;
	}
	
    public SeatTypeData getSeatPreference() {
		return seatPreference;
	}
	
    public void setSeatPreference(SeatTypeData seatPreference) {
		this.seatPreference = seatPreference;
	}
	
    public String getTierCode() {
		return tierCode;
	}
	
    public void setTierCode(String tierCode) {
		this.tierCode = tierCode;
	}
	
    public String getTitle() {
		return title;
	}
	
    public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "MmCustomerData [birthDate=" + birthDate + ", cards=" + cards
				+ ", code=" + code + ", customerPhones=" + customerPhones
				+ ", email=" + email + ", gender=" + gender + ", ignore="
				+ ignore + ", lastName=" + lastName + ", mealPreference="
				+ mealPreference + ", milesBalance=" + milesBalance
				+ ", milesEarned=" + milesEarned + ", milesQualify="
				+ milesQualify + ", name=" + name + ", pin=" + pin
				+ ", seatPreference=" + seatPreference + ", tierCode="
				+ tierCode + ", title=" + title + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((birthDate == null) ? 0 : birthDate.hashCode());
		result = prime * result + ((cards == null) ? 0 : cards.hashCode());
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result
				+ ((customerPhones == null) ? 0 : customerPhones.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + ((ignore == null) ? 0 : ignore.hashCode());
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result
				+ ((mealPreference == null) ? 0 : mealPreference.hashCode());
		result = prime * result
				+ ((milesBalance == null) ? 0 : milesBalance.hashCode());
		result = prime * result
				+ ((milesEarned == null) ? 0 : milesEarned.hashCode());
		result = prime * result
				+ ((milesQualify == null) ? 0 : milesQualify.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((pin == null) ? 0 : pin.hashCode());
		result = prime * result
				+ ((seatPreference == null) ? 0 : seatPreference.hashCode());
		result = prime * result
				+ ((tierCode == null) ? 0 : tierCode.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmCustomerData other = (MmCustomerData) obj;
		if (birthDate == null) {
			if (other.birthDate != null)
				return false;
		} else if (!birthDate.equals(other.birthDate))
			return false;
		if (cards == null) {
			if (other.cards != null)
				return false;
		} else if (!cards.equals(other.cards))
			return false;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (customerPhones == null) {
			if (other.customerPhones != null)
				return false;
		} else if (!customerPhones.equals(other.customerPhones))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (gender != other.gender)
			return false;
		if (ignore == null) {
			if (other.ignore != null)
				return false;
		} else if (!ignore.equals(other.ignore))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (mealPreference == null) {
			if (other.mealPreference != null)
				return false;
		} else if (!mealPreference.equals(other.mealPreference))
			return false;
		if (milesBalance == null) {
			if (other.milesBalance != null)
				return false;
		} else if (!milesBalance.equals(other.milesBalance))
			return false;
		if (milesEarned == null) {
			if (other.milesEarned != null)
				return false;
		} else if (!milesEarned.equals(other.milesEarned))
			return false;
		if (milesQualify == null) {
			if (other.milesQualify != null)
				return false;
		} else if (!milesQualify.equals(other.milesQualify))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (pin == null) {
			if (other.pin != null)
				return false;
		} else if (!pin.equals(other.pin))
			return false;
		if (seatPreference == null) {
			if (other.seatPreference != null)
				return false;
		} else if (!seatPreference.equals(other.seatPreference))
			return false;
		if (tierCode == null) {
			if (other.tierCode != null)
				return false;
		} else if (!tierCode.equals(other.tierCode))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
	
	
}