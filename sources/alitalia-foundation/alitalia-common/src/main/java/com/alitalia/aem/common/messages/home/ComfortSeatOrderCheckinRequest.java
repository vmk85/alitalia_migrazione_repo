package com.alitalia.aem.common.messages.home;

import java.util.Calendar;

import com.alitalia.aem.common.data.home.enumerations.CheckinSearchReasonEnum;
import com.alitalia.aem.common.messages.MmbBaseRequest;

public class ComfortSeatOrderCheckinRequest extends MmbBaseRequest {

    private Calendar flightDepartureDate;
    private String flightVectorAndNumber;
    private CheckinSearchReasonEnum reason;
    private String ticketNumber;
    
	public Calendar getFlightDepartureDate() {
		return flightDepartureDate;
	}
	public void setFlightDepartureDate(Calendar flightDepartureDate) {
		this.flightDepartureDate = flightDepartureDate;
	}
	public String getFlightVectorAndNumber() {
		return flightVectorAndNumber;
	}
	public void setFlightVectorAndNumber(String flightVectorAndNumber) {
		this.flightVectorAndNumber = flightVectorAndNumber;
	}
	public CheckinSearchReasonEnum getReason() {
		return reason;
	}
	public void setReason(CheckinSearchReasonEnum reason) {
		this.reason = reason;
	}
	public String getTicketNumber() {
		return ticketNumber;
	}
	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((flightDepartureDate == null) ? 0 : flightDepartureDate
						.hashCode());
		result = prime
				* result
				+ ((flightVectorAndNumber == null) ? 0 : flightVectorAndNumber
						.hashCode());
		result = prime * result + ((reason == null) ? 0 : reason.hashCode());
		result = prime * result
				+ ((ticketNumber == null) ? 0 : ticketNumber.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComfortSeatOrderCheckinRequest other = (ComfortSeatOrderCheckinRequest) obj;
		if (flightDepartureDate == null) {
			if (other.flightDepartureDate != null)
				return false;
		} else if (!flightDepartureDate.equals(other.flightDepartureDate))
			return false;
		if (flightVectorAndNumber == null) {
			if (other.flightVectorAndNumber != null)
				return false;
		} else if (!flightVectorAndNumber.equals(other.flightVectorAndNumber))
			return false;
		if (reason != other.reason)
			return false;
		if (ticketNumber == null) {
			if (other.ticketNumber != null)
				return false;
		} else if (!ticketNumber.equals(other.ticketNumber))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ComfortSeatOrderCheckinRequest [flightDepartureDate="
				+ flightDepartureDate + ", flightVectorAndNumber="
				+ flightVectorAndNumber + ", reason=" + reason
				+ ", ticketNumber=" + ticketNumber + ", toString()="
				+ super.toString() + "]";
	}

}
