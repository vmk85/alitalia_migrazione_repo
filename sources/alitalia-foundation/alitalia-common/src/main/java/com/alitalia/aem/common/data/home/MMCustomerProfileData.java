package com.alitalia.aem.common.data.home;

import java.util.Calendar;
import java.util.List;

import com.alitalia.aem.common.data.home.enumerations.MMAddressTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMGenderTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMMaritalStatusTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMRegistrationTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMSystemChannelTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMTierCodeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMTitleTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMWorkPositionTypeEnum;

public class MMCustomerProfileData {

	private Calendar birthDate;
	private Calendar passportDate;
	private Calendar tierDate;

	private Boolean contractAgree;

	private String contratcType;
	private String customerName;
	private String customerNickName;
	private String customerNumber;
	private String customerPinCode;
	private String customerSurname;
	private String defaultAddressCountry;
	private String defaultAddressMunicipalityName;
	private String defaultAddressPostalCode;
	private String defaultAddressStateCode;
	private String defaultAddressStreetFreeText;
	private String email;
	private String fiscalCode;
	private String language;
	private String mailingType;
	private String market;
	private String professionalName;
	private String statusCode;
	private String userIdRequest;
	private String webPageRequest;

	private Long pointsEarnedPartial;
	private Long pointsEarnedTotal;
	private Long pointsEarnedTotalQualified;
	private Long pointsRemainingPartial;
	private Long pointsRemainingTotal;
	private Long pointsSpentPartial;
	private Long pointsSpentTotal;
	private Long qualifyingSegment;

	private Boolean smsAuthorization;
	private Boolean profiling;

	private MMTitleTypeEnum customerTitle;
	private MMWorkPositionTypeEnum customerWorkPosition;
	private MMAddressTypeEnum defaultAddressType;
	private MMGenderTypeEnum gender;
	private MMMaritalStatusTypeEnum maritalStatus;
	private MMRegistrationTypeEnum registrationRequired;
	private MMSystemChannelTypeEnum systemChannel;
	private MMTierCodeEnum tierCode;
	private MMCreditCardData storedCreditCard;

	private List<MMTelephoneData> telephones;
	private List<MMActivityData> activities;
	private List<MMAddressData> addresses;
	private List<MMContractData> contracts;
	private List<MMCreditCardData> creditCards;
	private List<MMFlightData> flight;
	private List<MMLifestyleData> lifestyles;
	private List<MMMessageData> messages;
	private List<MMPreferenceData> preferences;
	private List<MMProgramData> programs;

	public Calendar getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Calendar birthDate) {
		this.birthDate = birthDate;
	}

	public Calendar getPassportDate() {
		return passportDate;
	}

	public void setPassportDate(Calendar passportDate) {
		this.passportDate = passportDate;
	}

	public Calendar getTierDate() {
		return tierDate;
	}

	public void setTierDate(Calendar tierDate) {
		this.tierDate = tierDate;
	}

	public Boolean getContractAgree() {
		return contractAgree;
	}

	public void setContractAgree(Boolean contractAgree) {
		this.contractAgree = contractAgree;
	}

	public String getContratcType() {
		return contratcType;
	}

	public void setContratcType(String contratcType) {
		this.contratcType = contratcType;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerNickName() {
		return customerNickName;
	}

	public void setCustomerNickName(String customerNickName) {
		this.customerNickName = customerNickName;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getCustomerPinCode() {
		return customerPinCode;
	}

	public void setCustomerPinCode(String customerPinCode) {
		this.customerPinCode = customerPinCode;
	}

	public String getCustomerSurname() {
		return customerSurname;
	}

	public void setCustomerSurname(String customerSurname) {
		this.customerSurname = customerSurname;
	}

	public String getDefaultAddressCountry() {
		return defaultAddressCountry;
	}

	public void setDefaultAddressCountry(String defaultAddressCountry) {
		this.defaultAddressCountry = defaultAddressCountry;
	}

	public String getDefaultAddressMunicipalityName() {
		return defaultAddressMunicipalityName;
	}

	public void setDefaultAddressMunicipalityName(
			String defaultAddressMunicipalityName) {
		this.defaultAddressMunicipalityName = defaultAddressMunicipalityName;
	}

	public String getDefaultAddressPostalCode() {
		return defaultAddressPostalCode;
	}

	public void setDefaultAddressPostalCode(String defaultAddressPostalCode) {
		this.defaultAddressPostalCode = defaultAddressPostalCode;
	}

	public String getDefaultAddressStateCode() {
		return defaultAddressStateCode;
	}

	public void setDefaultAddressStateCode(String defaultAddressStateCode) {
		this.defaultAddressStateCode = defaultAddressStateCode;
	}

	public String getDefaultAddressStreetFreeText() {
		return defaultAddressStreetFreeText;
	}

	public void setDefaultAddressStreetFreeText(
			String defaultAddressStreetFreeText) {
		this.defaultAddressStreetFreeText = defaultAddressStreetFreeText;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFiscalCode() {
		return fiscalCode;
	}

	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getMailingType() {
		return mailingType;
	}

	public void setMailingType(String mailingType) {
		this.mailingType = mailingType;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public String getProfessionalName() {
		return professionalName;
	}

	public void setProfessionalName(String professionalName) {
		this.professionalName = professionalName;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getUserIdRequest() {
		return userIdRequest;
	}

	public void setUserIdRequest(String userIdRequest) {
		this.userIdRequest = userIdRequest;
	}

	public String getWebPageRequest() {
		return webPageRequest;
	}

	public void setWebPageRequest(String webPageRequest) {
		this.webPageRequest = webPageRequest;
	}

	public Long getPointsEarnedPartial() {
		return pointsEarnedPartial;
	}

	public void setPointsEarnedPartial(Long pointsEarnedPartial) {
		this.pointsEarnedPartial = pointsEarnedPartial;
	}

	public Long getPointsEarnedTotal() {
		return pointsEarnedTotal;
	}

	public void setPointsEarnedTotal(Long pointsEarnedTotal) {
		this.pointsEarnedTotal = pointsEarnedTotal;
	}

	public Long getPointsEarnedTotalQualified() {
		return pointsEarnedTotalQualified;
	}

	public void setPointsEarnedTotalQualified(Long pointsEarnedTotalQualified) {
		this.pointsEarnedTotalQualified = pointsEarnedTotalQualified;
	}

	public Long getPointsRemainingPartial() {
		return pointsRemainingPartial;
	}

	public void setPointsRemainingPartial(Long pointsRemainingPartial) {
		this.pointsRemainingPartial = pointsRemainingPartial;
	}

	public Long getPointsRemainingTotal() {
		return pointsRemainingTotal;
	}

	public void setPointsRemainingTotal(Long pointsRemainingTotal) {
		this.pointsRemainingTotal = pointsRemainingTotal;
	}

	public Long getPointsSpentPartial() {
		return pointsSpentPartial;
	}

	public void setPointsSpentPartial(Long pointsSpentPartial) {
		this.pointsSpentPartial = pointsSpentPartial;
	}

	public Long getPointsSpentTotal() {
		return pointsSpentTotal;
	}

	public void setPointsSpentTotal(Long pointsSpentTotal) {
		this.pointsSpentTotal = pointsSpentTotal;
	}

	public Long getQualifyingSegment() {
		return qualifyingSegment;
	}

	public void setQualifyingSegment(Long qualifyingSegment) {
		this.qualifyingSegment = qualifyingSegment;
	}

	public Boolean getSmsAuthorization() {
		return smsAuthorization;
	}

	public void setSmsAuthorization(Boolean smsAuthorization) {
		this.smsAuthorization = smsAuthorization;
	}

	public Boolean getProfiling() {
		return profiling;
	}

	public void setProfiling(Boolean profiling) {
		this.profiling = profiling;
	}

	public MMTitleTypeEnum getCustomerTitle() {
		return customerTitle;
	}

	public void setCustomerTitle(MMTitleTypeEnum customerTitle) {
		this.customerTitle = customerTitle;
	}

	public MMWorkPositionTypeEnum getCustomerWorkPosition() {
		return customerWorkPosition;
	}

	public void setCustomerWorkPosition(
			MMWorkPositionTypeEnum customerWorkPosition) {
		this.customerWorkPosition = customerWorkPosition;
	}

	public MMAddressTypeEnum getDefaultAddressType() {
		return defaultAddressType;
	}

	public void setDefaultAddressType(MMAddressTypeEnum defaultAddressType) {
		this.defaultAddressType = defaultAddressType;
	}

	public MMGenderTypeEnum getGender() {
		return gender;
	}

	public void setGender(MMGenderTypeEnum gender) {
		this.gender = gender;
	}

	public MMMaritalStatusTypeEnum getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(MMMaritalStatusTypeEnum maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public MMRegistrationTypeEnum getRegistrationRequired() {
		return registrationRequired;
	}

	public void setRegistrationRequired(
			MMRegistrationTypeEnum registrationRequired) {
		this.registrationRequired = registrationRequired;
	}

	public MMSystemChannelTypeEnum getSystemChannel() {
		return systemChannel;
	}

	public void setSystemChannel(MMSystemChannelTypeEnum systemChannel) {
		this.systemChannel = systemChannel;
	}

	public MMTierCodeEnum getTierCode() {
		return tierCode;
	}

	public void setTierCode(MMTierCodeEnum tierCode) {
		this.tierCode = tierCode;
	}

	public List<MMTelephoneData> getTelephones() {
		return telephones;
	}

	public void setTelephones(List<MMTelephoneData> telephones) {
		this.telephones = telephones;
	}

	public List<MMActivityData> getActivities() {
		return activities;
	}

	public void setActivities(List<MMActivityData> activities) {
		this.activities = activities;
	}

	public List<MMAddressData> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<MMAddressData> addresses) {
		this.addresses = addresses;
	}

	public List<MMContractData> getContracts() {
		return contracts;
	}

	public void setContracts(List<MMContractData> contracts) {
		this.contracts = contracts;
	}

	public List<MMCreditCardData> getCreditCards() {
		return creditCards;
	}

	public void setCreditCards(List<MMCreditCardData> creditCards) {
		this.creditCards = creditCards;
	}

	public List<MMFlightData> getFlight() {
		return flight;
	}

	public void setFlight(List<MMFlightData> flight) {
		this.flight = flight;
	}

	public List<MMLifestyleData> getLifestyles() {
		return lifestyles;
	}

	public void setLifestyles(List<MMLifestyleData> lifestyles) {
		this.lifestyles = lifestyles;
	}

	public List<MMMessageData> getMessages() {
		return messages;
	}

	public void setMessages(List<MMMessageData> messages) {
		this.messages = messages;
	}

	public List<MMPreferenceData> getPreferences() {
		return preferences;
	}

	public void setPreferences(List<MMPreferenceData> preferences) {
		this.preferences = preferences;
	}

	public List<MMProgramData> getPrograms() {
		return programs;
	}

	public void setPrograms(List<MMProgramData> programs) {
		this.programs = programs;
	}

	public MMCreditCardData getStoredCreditCard() {
		return storedCreditCard;
	}

	public void setStoredCreditCard(MMCreditCardData storedCreditCard) {
		this.storedCreditCard = storedCreditCard;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((activities == null) ? 0 : activities.hashCode());
		result = prime * result
				+ ((addresses == null) ? 0 : addresses.hashCode());
		result = prime * result
				+ ((birthDate == null) ? 0 : birthDate.hashCode());
		result = prime * result
				+ ((contractAgree == null) ? 0 : contractAgree.hashCode());
		result = prime * result
				+ ((contracts == null) ? 0 : contracts.hashCode());
		result = prime * result
				+ ((contratcType == null) ? 0 : contratcType.hashCode());
		result = prime * result
				+ ((creditCards == null) ? 0 : creditCards.hashCode());
		result = prime * result
				+ ((customerName == null) ? 0 : customerName.hashCode());
		result = prime
				* result
				+ ((customerNickName == null) ? 0 : customerNickName.hashCode());
		result = prime * result
				+ ((customerNumber == null) ? 0 : customerNumber.hashCode());
		result = prime * result
				+ ((customerPinCode == null) ? 0 : customerPinCode.hashCode());
		result = prime * result
				+ ((customerSurname == null) ? 0 : customerSurname.hashCode());
		result = prime * result
				+ ((customerTitle == null) ? 0 : customerTitle.hashCode());
		result = prime
				* result
				+ ((customerWorkPosition == null) ? 0 : customerWorkPosition
						.hashCode());
		result = prime
				* result
				+ ((defaultAddressCountry == null) ? 0 : defaultAddressCountry
						.hashCode());
		result = prime
				* result
				+ ((defaultAddressMunicipalityName == null) ? 0
						: defaultAddressMunicipalityName.hashCode());
		result = prime
				* result
				+ ((defaultAddressPostalCode == null) ? 0
						: defaultAddressPostalCode.hashCode());
		result = prime
				* result
				+ ((defaultAddressStateCode == null) ? 0
						: defaultAddressStateCode.hashCode());
		result = prime
				* result
				+ ((defaultAddressStreetFreeText == null) ? 0
						: defaultAddressStreetFreeText.hashCode());
		result = prime
				* result
				+ ((defaultAddressType == null) ? 0 : defaultAddressType
						.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result
				+ ((fiscalCode == null) ? 0 : fiscalCode.hashCode());
		result = prime * result + ((flight == null) ? 0 : flight.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result
				+ ((language == null) ? 0 : language.hashCode());
		result = prime * result
				+ ((lifestyles == null) ? 0 : lifestyles.hashCode());
		result = prime * result
				+ ((mailingType == null) ? 0 : mailingType.hashCode());
		result = prime * result
				+ ((maritalStatus == null) ? 0 : maritalStatus.hashCode());
		result = prime * result + ((market == null) ? 0 : market.hashCode());
		result = prime * result
				+ ((messages == null) ? 0 : messages.hashCode());
		result = prime * result
				+ ((passportDate == null) ? 0 : passportDate.hashCode());
		result = prime
				* result
				+ ((pointsEarnedPartial == null) ? 0 : pointsEarnedPartial
						.hashCode());
		result = prime
				* result
				+ ((pointsEarnedTotal == null) ? 0 : pointsEarnedTotal
						.hashCode());
		result = prime
				* result
				+ ((pointsEarnedTotalQualified == null) ? 0
						: pointsEarnedTotalQualified.hashCode());
		result = prime
				* result
				+ ((pointsRemainingPartial == null) ? 0
						: pointsRemainingPartial.hashCode());
		result = prime
				* result
				+ ((pointsRemainingTotal == null) ? 0 : pointsRemainingTotal
						.hashCode());
		result = prime
				* result
				+ ((pointsSpentPartial == null) ? 0 : pointsSpentPartial
						.hashCode());
		result = prime
				* result
				+ ((pointsSpentTotal == null) ? 0 : pointsSpentTotal.hashCode());
		result = prime * result
				+ ((preferences == null) ? 0 : preferences.hashCode());
		result = prime
				* result
				+ ((professionalName == null) ? 0 : professionalName.hashCode());
		result = prime * result
				+ ((profiling == null) ? 0 : profiling.hashCode());
		result = prime * result
				+ ((programs == null) ? 0 : programs.hashCode());
		result = prime
				* result
				+ ((qualifyingSegment == null) ? 0 : qualifyingSegment
						.hashCode());
		result = prime
				* result
				+ ((registrationRequired == null) ? 0 : registrationRequired
						.hashCode());
		result = prime
				* result
				+ ((smsAuthorization == null) ? 0 : smsAuthorization.hashCode());
		result = prime * result
				+ ((statusCode == null) ? 0 : statusCode.hashCode());
		result = prime
				* result
				+ ((storedCreditCard == null) ? 0 : storedCreditCard.hashCode());
		result = prime * result
				+ ((systemChannel == null) ? 0 : systemChannel.hashCode());
		result = prime * result
				+ ((telephones == null) ? 0 : telephones.hashCode());
		result = prime * result
				+ ((tierCode == null) ? 0 : tierCode.hashCode());
		result = prime * result
				+ ((tierDate == null) ? 0 : tierDate.hashCode());
		result = prime * result
				+ ((userIdRequest == null) ? 0 : userIdRequest.hashCode());
		result = prime * result
				+ ((webPageRequest == null) ? 0 : webPageRequest.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MMCustomerProfileData other = (MMCustomerProfileData) obj;
		if (activities == null) {
			if (other.activities != null)
				return false;
		} else if (!activities.equals(other.activities))
			return false;
		if (addresses == null) {
			if (other.addresses != null)
				return false;
		} else if (!addresses.equals(other.addresses))
			return false;
		if (birthDate == null) {
			if (other.birthDate != null)
				return false;
		} else if (!birthDate.equals(other.birthDate))
			return false;
		if (contractAgree == null) {
			if (other.contractAgree != null)
				return false;
		} else if (!contractAgree.equals(other.contractAgree))
			return false;
		if (contracts == null) {
			if (other.contracts != null)
				return false;
		} else if (!contracts.equals(other.contracts))
			return false;
		if (contratcType == null) {
			if (other.contratcType != null)
				return false;
		} else if (!contratcType.equals(other.contratcType))
			return false;
		if (creditCards == null) {
			if (other.creditCards != null)
				return false;
		} else if (!creditCards.equals(other.creditCards))
			return false;
		if (customerName == null) {
			if (other.customerName != null)
				return false;
		} else if (!customerName.equals(other.customerName))
			return false;
		if (customerNickName == null) {
			if (other.customerNickName != null)
				return false;
		} else if (!customerNickName.equals(other.customerNickName))
			return false;
		if (customerNumber == null) {
			if (other.customerNumber != null)
				return false;
		} else if (!customerNumber.equals(other.customerNumber))
			return false;
		if (customerPinCode == null) {
			if (other.customerPinCode != null)
				return false;
		} else if (!customerPinCode.equals(other.customerPinCode))
			return false;
		if (customerSurname == null) {
			if (other.customerSurname != null)
				return false;
		} else if (!customerSurname.equals(other.customerSurname))
			return false;
		if (customerTitle != other.customerTitle)
			return false;
		if (customerWorkPosition != other.customerWorkPosition)
			return false;
		if (defaultAddressCountry == null) {
			if (other.defaultAddressCountry != null)
				return false;
		} else if (!defaultAddressCountry.equals(other.defaultAddressCountry))
			return false;
		if (defaultAddressMunicipalityName == null) {
			if (other.defaultAddressMunicipalityName != null)
				return false;
		} else if (!defaultAddressMunicipalityName
				.equals(other.defaultAddressMunicipalityName))
			return false;
		if (defaultAddressPostalCode == null) {
			if (other.defaultAddressPostalCode != null)
				return false;
		} else if (!defaultAddressPostalCode
				.equals(other.defaultAddressPostalCode))
			return false;
		if (defaultAddressStateCode == null) {
			if (other.defaultAddressStateCode != null)
				return false;
		} else if (!defaultAddressStateCode
				.equals(other.defaultAddressStateCode))
			return false;
		if (defaultAddressStreetFreeText == null) {
			if (other.defaultAddressStreetFreeText != null)
				return false;
		} else if (!defaultAddressStreetFreeText
				.equals(other.defaultAddressStreetFreeText))
			return false;
		if (defaultAddressType != other.defaultAddressType)
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (fiscalCode == null) {
			if (other.fiscalCode != null)
				return false;
		} else if (!fiscalCode.equals(other.fiscalCode))
			return false;
		if (flight == null) {
			if (other.flight != null)
				return false;
		} else if (!flight.equals(other.flight))
			return false;
		if (gender != other.gender)
			return false;
		if (language == null) {
			if (other.language != null)
				return false;
		} else if (!language.equals(other.language))
			return false;
		if (lifestyles == null) {
			if (other.lifestyles != null)
				return false;
		} else if (!lifestyles.equals(other.lifestyles))
			return false;
		if (mailingType == null) {
			if (other.mailingType != null)
				return false;
		} else if (!mailingType.equals(other.mailingType))
			return false;
		if (maritalStatus != other.maritalStatus)
			return false;
		if (market == null) {
			if (other.market != null)
				return false;
		} else if (!market.equals(other.market))
			return false;
		if (messages == null) {
			if (other.messages != null)
				return false;
		} else if (!messages.equals(other.messages))
			return false;
		if (passportDate == null) {
			if (other.passportDate != null)
				return false;
		} else if (!passportDate.equals(other.passportDate))
			return false;
		if (pointsEarnedPartial == null) {
			if (other.pointsEarnedPartial != null)
				return false;
		} else if (!pointsEarnedPartial.equals(other.pointsEarnedPartial))
			return false;
		if (pointsEarnedTotal == null) {
			if (other.pointsEarnedTotal != null)
				return false;
		} else if (!pointsEarnedTotal.equals(other.pointsEarnedTotal))
			return false;
		if (pointsEarnedTotalQualified == null) {
			if (other.pointsEarnedTotalQualified != null)
				return false;
		} else if (!pointsEarnedTotalQualified
				.equals(other.pointsEarnedTotalQualified))
			return false;
		if (pointsRemainingPartial == null) {
			if (other.pointsRemainingPartial != null)
				return false;
		} else if (!pointsRemainingPartial.equals(other.pointsRemainingPartial))
			return false;
		if (pointsRemainingTotal == null) {
			if (other.pointsRemainingTotal != null)
				return false;
		} else if (!pointsRemainingTotal.equals(other.pointsRemainingTotal))
			return false;
		if (pointsSpentPartial == null) {
			if (other.pointsSpentPartial != null)
				return false;
		} else if (!pointsSpentPartial.equals(other.pointsSpentPartial))
			return false;
		if (pointsSpentTotal == null) {
			if (other.pointsSpentTotal != null)
				return false;
		} else if (!pointsSpentTotal.equals(other.pointsSpentTotal))
			return false;
		if (preferences == null) {
			if (other.preferences != null)
				return false;
		} else if (!preferences.equals(other.preferences))
			return false;
		if (professionalName == null) {
			if (other.professionalName != null)
				return false;
		} else if (!professionalName.equals(other.professionalName))
			return false;
		if (profiling == null) {
			if (other.profiling != null)
				return false;
		} else if (!profiling.equals(other.profiling))
			return false;
		if (programs == null) {
			if (other.programs != null)
				return false;
		} else if (!programs.equals(other.programs))
			return false;
		if (qualifyingSegment == null) {
			if (other.qualifyingSegment != null)
				return false;
		} else if (!qualifyingSegment.equals(other.qualifyingSegment))
			return false;
		if (registrationRequired != other.registrationRequired)
			return false;
		if (smsAuthorization == null) {
			if (other.smsAuthorization != null)
				return false;
		} else if (!smsAuthorization.equals(other.smsAuthorization))
			return false;
		if (statusCode == null) {
			if (other.statusCode != null)
				return false;
		} else if (!statusCode.equals(other.statusCode))
			return false;
		if (storedCreditCard == null) {
			if (other.storedCreditCard != null)
				return false;
		} else if (!storedCreditCard.equals(other.storedCreditCard))
			return false;
		if (systemChannel != other.systemChannel)
			return false;
		if (telephones == null) {
			if (other.telephones != null)
				return false;
		} else if (!telephones.equals(other.telephones))
			return false;
		if (tierCode != other.tierCode)
			return false;
		if (tierDate == null) {
			if (other.tierDate != null)
				return false;
		} else if (!tierDate.equals(other.tierDate))
			return false;
		if (userIdRequest == null) {
			if (other.userIdRequest != null)
				return false;
		} else if (!userIdRequest.equals(other.userIdRequest))
			return false;
		if (webPageRequest == null) {
			if (other.webPageRequest != null)
				return false;
		} else if (!webPageRequest.equals(other.webPageRequest))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MMCustomerProfileData [birthDate=" + birthDate
				+ ", passportDate=" + passportDate + ", tierDate=" + tierDate
				+ ", contractAgree=" + contractAgree + ", contratcType="
				+ contratcType + ", customerName=" + customerName
				+ ", customerNickName=" + customerNickName
				+ ", customerNumber=" + customerNumber + ", customerPinCode="
				+ customerPinCode + ", customerSurname=" + customerSurname
				+ ", defaultAddressCountry=" + defaultAddressCountry
				+ ", defaultAddressMunicipalityName="
				+ defaultAddressMunicipalityName
				+ ", defaultAddressPostalCode=" + defaultAddressPostalCode
				+ ", defaultAddressStateCode=" + defaultAddressStateCode
				+ ", defaultAddressStreetFreeText="
				+ defaultAddressStreetFreeText + ", email=" + email
				+ ", fiscalCode=" + fiscalCode + ", language=" + language
				+ ", mailingType=" + mailingType + ", market=" + market
				+ ", professionalName=" + professionalName + ", statusCode="
				+ statusCode + ", userIdRequest=" + userIdRequest
				+ ", webPageRequest=" + webPageRequest
				+ ", pointsEarnedPartial=" + pointsEarnedPartial
				+ ", pointsEarnedTotal=" + pointsEarnedTotal
				+ ", pointsEarnedTotalQualified=" + pointsEarnedTotalQualified
				+ ", pointsRemainingPartial=" + pointsRemainingPartial
				+ ", pointsRemainingTotal=" + pointsRemainingTotal
				+ ", pointsSpentPartial=" + pointsSpentPartial
				+ ", pointsSpentTotal=" + pointsSpentTotal
				+ ", qualifyingSegment=" + qualifyingSegment
				+ ", smsAuthorization=" + smsAuthorization + ", profiling="
				+ profiling + ", customerTitle=" + customerTitle
				+ ", customerWorkPosition=" + customerWorkPosition
				+ ", defaultAddressType=" + defaultAddressType + ", gender="
				+ gender + ", maritalStatus=" + maritalStatus
				+ ", registrationRequired=" + registrationRequired
				+ ", systemChannel=" + systemChannel + ", tierCode=" + tierCode
				+ ", storedCreditCard=" + storedCreditCard + ", telephones="
				+ telephones + ", activities=" + activities + ", addresses="
				+ addresses + ", contracts=" + contracts + ", creditCards="
				+ creditCards + ", flight=" + flight + ", lifestyles="
				+ lifestyles + ", messages=" + messages + ", preferences="
				+ preferences + ", programs=" + programs + ", getBirthDate()="
				+ getBirthDate() + ", getPassportDate()=" + getPassportDate()
				+ ", getTierDate()=" + getTierDate() + ", getContractAgree()="
				+ getContractAgree() + ", getContratcType()="
				+ getContratcType() + ", getCustomerName()="
				+ getCustomerName() + ", getCustomerNickName()="
				+ getCustomerNickName() + ", getCustomerNumber()="
				+ getCustomerNumber() + ", getCustomerPinCode()="
				+ getCustomerPinCode() + ", getCustomerSurname()="
				+ getCustomerSurname() + ", getDefaultAddressCountry()="
				+ getDefaultAddressCountry()
				+ ", getDefaultAddressMunicipalityName()="
				+ getDefaultAddressMunicipalityName()
				+ ", getDefaultAddressPostalCode()="
				+ getDefaultAddressPostalCode()
				+ ", getDefaultAddressStateCode()="
				+ getDefaultAddressStateCode()
				+ ", getDefaultAddressStreetFreeText()="
				+ getDefaultAddressStreetFreeText() + ", getEmail()="
				+ getEmail() + ", getFiscalCode()=" + getFiscalCode()
				+ ", getLanguage()=" + getLanguage() + ", getMailingType()="
				+ getMailingType() + ", getMarket()=" + getMarket()
				+ ", getProfessionalName()=" + getProfessionalName()
				+ ", getStatusCode()=" + getStatusCode()
				+ ", getUserIdRequest()=" + getUserIdRequest()
				+ ", getWebPageRequest()=" + getWebPageRequest()
				+ ", getPointsEarnedPartial()=" + getPointsEarnedPartial()
				+ ", getPointsEarnedTotal()=" + getPointsEarnedTotal()
				+ ", getPointsEarnedTotalQualified()="
				+ getPointsEarnedTotalQualified()
				+ ", getPointsRemainingPartial()="
				+ getPointsRemainingPartial() + ", getPointsRemainingTotal()="
				+ getPointsRemainingTotal() + ", getPointsSpentPartial()="
				+ getPointsSpentPartial() + ", getPointsSpentTotal()="
				+ getPointsSpentTotal() + ", getQualifyingSegment()="
				+ getQualifyingSegment() + ", getSmsAuthorization()="
				+ getSmsAuthorization() + ", getProfiling()=" + getProfiling()
				+ ", getCustomerTitle()=" + getCustomerTitle()
				+ ", getCustomerWorkPosition()=" + getCustomerWorkPosition()
				+ ", getDefaultAddressType()=" + getDefaultAddressType()
				+ ", getGender()=" + getGender() + ", getMaritalStatus()="
				+ getMaritalStatus() + ", getRegistrationRequired()="
				+ getRegistrationRequired() + ", getSystemChannel()="
				+ getSystemChannel() + ", getTierCode()=" + getTierCode()
				+ ", getTelephones()=" + getTelephones() + ", getActivities()="
				+ getActivities() + ", getAddresses()=" + getAddresses()
				+ ", getContracts()=" + getContracts() + ", getCreditCards()="
				+ getCreditCards() + ", getFlight()=" + getFlight()
				+ ", getLifestyles()=" + getLifestyles() + ", getMessages()="
				+ getMessages() + ", getPreferences()=" + getPreferences()
				+ ", getPrograms()=" + getPrograms()
				+ ", getStoredCreditCard()=" + getStoredCreditCard()
				+ ", hashCode()=" + hashCode() + ", getClass()=" + getClass()
				+ ", toString()=" + super.toString() + "]";
	}
}