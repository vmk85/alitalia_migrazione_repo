package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class AgencySendMailResponse extends BaseResponse {

	private boolean sendMailSuccessful;

	public boolean isSendMailSuccessful() {
		return sendMailSuccessful;
	}

	public void setSendMailSuccessful(boolean sendMailSuccessful) {
		this.sendMailSuccessful = sendMailSuccessful;
	}

	@Override
	public String toString() {
		return "SendMailResponse [sendMailSuccessful=" + sendMailSuccessful
				+ ", getTid()=" + getTid() + ", getSid()=" + getSid() + "]";
	}
}
