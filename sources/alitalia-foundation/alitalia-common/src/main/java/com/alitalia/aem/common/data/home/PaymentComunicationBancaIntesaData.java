package com.alitalia.aem.common.data.home;

import java.util.Map;

public class PaymentComunicationBancaIntesaData extends PaymentComunicationInfoData {
	
	private String cancelUrl;
	private Map<String, String> requestData;
	private String shopperId;
	
	public String getCancelUrl() {
		return cancelUrl;
	}
	
	public void setCancelUrl(String cancelUrl) {
		this.cancelUrl = cancelUrl;
	}
	
	public Map<String, String> getRequestData() {
		return requestData;
	}
	
	public void setRequestData(Map<String, String> requestData) {
		this.requestData = requestData;
	}
	
	public String getShopperId() {
		return shopperId;
	}
	
	public void setShopperId(String shopperId) {
		this.shopperId = shopperId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((cancelUrl == null) ? 0 : cancelUrl.hashCode());
		result = prime * result
				+ ((requestData == null) ? 0 : requestData.hashCode());
		result = prime * result
				+ ((shopperId == null) ? 0 : shopperId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentComunicationBancaIntesaData other = (PaymentComunicationBancaIntesaData) obj;
		if (cancelUrl == null) {
			if (other.cancelUrl != null)
				return false;
		} else if (!cancelUrl.equals(other.cancelUrl))
			return false;
		if (requestData == null) {
			if (other.requestData != null)
				return false;
		} else if (!requestData.equals(other.requestData))
			return false;
		if (shopperId == null) {
			if (other.shopperId != null)
				return false;
		} else if (!shopperId.equals(other.shopperId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PaymentComunicationBancaIntesaData [cancelUrl=" + cancelUrl
				+ ", requestData=" + requestData + ", shopperId=" + shopperId
				+ "]";
	}
	
}
