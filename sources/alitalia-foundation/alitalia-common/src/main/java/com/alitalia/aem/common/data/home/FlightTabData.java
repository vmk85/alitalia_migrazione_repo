package com.alitalia.aem.common.data.home;

import java.math.BigDecimal;
import java.util.Calendar;


public class FlightTabData {

    private String currency;
    private Calendar date;
    private BigDecimal price;
	
    public String getCurrency() {
		return currency;
	}
	
    public void setCurrency(String currency) {
		this.currency = currency;
	}
	
    public Calendar getDate() {
		return date;
	}
	
    public void setDate(Calendar date) {
		this.date = date;
	}
	
    public BigDecimal getPrice() {
		return price;
	}
	
    public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "FlightTabData [currency=" + currency + ", date=" + date
				+ ", price=" + price + "]";
	}

}
