package com.alitalia.aem.common.data.home;

import java.util.Calendar;

public class MMMessageData {

	private Boolean alreadyRead;

	private String description;
	private String text;
	private String title;

	private Integer sequenceNumber;
	private Integer messageNumber;

	private Calendar endValidatDate;
	private Calendar readDate;
	private Calendar startValidatDate;

	public Boolean getAlreadyRead() {
		return alreadyRead;
	}

	public void setAlreadyRead(Boolean alreadyRead) {
		this.alreadyRead = alreadyRead;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public Integer getMessageNumber() {
		return messageNumber;
	}

	public void setMessageNumber(Integer messageNumber) {
		this.messageNumber = messageNumber;
	}

	public Calendar getEndValidatDate() {
		return endValidatDate;
	}

	public void setEndValidatDate(Calendar endValidatDate) {
		this.endValidatDate = endValidatDate;
	}

	public Calendar getReadDate() {
		return readDate;
	}

	public void setReadDate(Calendar readDate) {
		this.readDate = readDate;
	}

	public Calendar getStartValidatDate() {
		return startValidatDate;
	}

	public void setStartValidatDate(Calendar startValidatDate) {
		this.startValidatDate = startValidatDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((alreadyRead == null) ? 0 : alreadyRead.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result
				+ ((endValidatDate == null) ? 0 : endValidatDate.hashCode());
		result = prime * result
				+ ((messageNumber == null) ? 0 : messageNumber.hashCode());
		result = prime * result
				+ ((readDate == null) ? 0 : readDate.hashCode());
		result = prime * result
				+ ((sequenceNumber == null) ? 0 : sequenceNumber.hashCode());
		result = prime
				* result
				+ ((startValidatDate == null) ? 0 : startValidatDate.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MMMessageData other = (MMMessageData) obj;
		if (alreadyRead == null) {
			if (other.alreadyRead != null)
				return false;
		} else if (!alreadyRead.equals(other.alreadyRead))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (endValidatDate == null) {
			if (other.endValidatDate != null)
				return false;
		} else if (!endValidatDate.equals(other.endValidatDate))
			return false;
		if (messageNumber == null) {
			if (other.messageNumber != null)
				return false;
		} else if (!messageNumber.equals(other.messageNumber))
			return false;
		if (readDate == null) {
			if (other.readDate != null)
				return false;
		} else if (!readDate.equals(other.readDate))
			return false;
		if (sequenceNumber == null) {
			if (other.sequenceNumber != null)
				return false;
		} else if (!sequenceNumber.equals(other.sequenceNumber))
			return false;
		if (startValidatDate == null) {
			if (other.startValidatDate != null)
				return false;
		} else if (!startValidatDate.equals(other.startValidatDate))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MMMessageData [alreadyRead=" + alreadyRead + ", description="
				+ description + ", text=" + text + ", title=" + title
				+ ", sequenceNumber=" + sequenceNumber + ", messageNumber="
				+ messageNumber + ", endValidatDate=" + endValidatDate
				+ ", readDate=" + readDate + ", startValidatDate="
				+ startValidatDate + "]";
	}

}
