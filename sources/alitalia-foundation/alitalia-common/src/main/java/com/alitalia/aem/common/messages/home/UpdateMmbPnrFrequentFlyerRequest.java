package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.MmbBaseRequest;

public class UpdateMmbPnrFrequentFlyerRequest extends MmbBaseRequest {

    private String pnr;
    private String lastName;
    private String name;
    private String frequentFlyerCarrier;
    private String frequentFlyerCode;
    private String travelerRefNumber;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

    public String getLastName() {
		return lastName;
	}

    public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFrequentFlyerCarrier() {
		return frequentFlyerCarrier;
	}

	public void setFrequentFlyerCarrier(String frequentFlyerCarrier) {
		this.frequentFlyerCarrier = frequentFlyerCarrier;
	}

	public String getFrequentFlyerCode() {
		return frequentFlyerCode;
	}

	public void setFrequentFlyerCode(String frequentFlyerCode) {
		this.frequentFlyerCode = frequentFlyerCode;
	}

	public String getTravelerRefNumber() {
		return travelerRefNumber;
	}

	public void setTravelerRefNumber(String travelerRefNumber) {
		this.travelerRefNumber = travelerRefNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((pnr == null) ? 0 : pnr.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		UpdateMmbPnrFrequentFlyerRequest other = (UpdateMmbPnrFrequentFlyerRequest) obj;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (pnr == null) {
			if (other.pnr != null)
				return false;
		} else if (!pnr.equals(other.pnr))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UpdateMmbPnrFrequentFlyerRequest [pnr=" + pnr + ", lastName="
				+ lastName + ", name=" + name + ", frequentFlyerCarrier="
				+ frequentFlyerCarrier + ", frequentFlyerCode="
				+ frequentFlyerCode + ", travelerRefNumber="
				+ travelerRefNumber + ", getBaseInfo()=" + getBaseInfo()
				+ ", getClient()=" + getClient() + "]";
	}

}
