package com.alitalia.aem.common.data.home.mmb.ancillary;

import com.alitalia.aem.common.data.MmbAncillaryDetail;

public class MmbAncillarySeatDetailData implements MmbAncillaryDetail {

	private Boolean comfort;
	private Boolean payment;
	private String seat;

	public Boolean isComfort() {
		return comfort;
	}

	public void setComfort(Boolean comfort) {
		this.comfort = comfort;
	}
	
	public Boolean isPayment() {
		return payment;
	}

	public void setPayment(Boolean payment) {
		this.payment = payment;
	}

	public String getSeat() {
		return seat;
	}

	public void setSeat(String seat) {
		this.seat = seat;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((comfort == null) ? 0 : comfort.hashCode());
		result = prime * result + ((seat == null) ? 0 : seat.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbAncillarySeatDetailData other = (MmbAncillarySeatDetailData) obj;
		if (comfort == null) {
			if (other.comfort != null)
				return false;
		} else if (!comfort.equals(other.comfort))
			return false;
		if (seat == null) {
			if (other.seat != null)
				return false;
		} else if (!seat.equals(other.seat))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbSeatDetailData [comfort=" + comfort + ", seat=" + seat + "]";
	}

}
