package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetrieveProvincesResponse extends BaseResponse{

	private List<CountryData> provinces;
	
	public RetrieveProvincesResponse(){}
	
	public RetrieveProvincesResponse(String tid, String sid) {
		super(tid, sid);
	}

	public List<CountryData> getProvinces() {
		return provinces;
	}

	public void setProvinces(List<CountryData> provinces) {
		this.provinces = provinces;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((provinces == null) ? 0 : provinces.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveProvincesResponse other = (RetrieveProvincesResponse) obj;
		if (provinces == null) {
			if (other.provinces != null)
				return false;
		} else if (!provinces.equals(other.provinces))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveProvincesResponse [provinces=" + provinces + "]";
	}
}