package com.alitalia.aem.common.data.home;

public class FullTextSearchData extends GatewaySearchData {

	public FullTextSearchData() {
		super();
	}

	@Override
	public String toString() {
		return "FullTextSearchData [getPassengers()=" + getPassengers()
				+ ", getType()=" + getType() + ", getId()=" + getId()
				+ ", getSessionId()=" + getSessionId() + ", getSolutionSet()="
				+ getSolutionSet() + ", getSolutionId()=" + getSolutionId()
				+ ", getRefreshSolutionId()=" + getRefreshSolutionId()
				+ ", getFarmId()=" + getFarmId() + ", getProperties()="
				+ getProperties() + "]";
	}

}
