package com.alitalia.aem.common.data.home.enumerations;

public enum ContactTypeEnum {

	M,
    H,
    A,
    B;

    public String value() {
        return name();
    }

    public static ContactTypeEnum fromValue(String v) {
        return valueOf(v);
    }
}