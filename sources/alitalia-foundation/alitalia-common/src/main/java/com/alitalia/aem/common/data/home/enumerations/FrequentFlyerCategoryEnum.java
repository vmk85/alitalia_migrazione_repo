package com.alitalia.aem.common.data.home.enumerations;

public enum FrequentFlyerCategoryEnum {
	
	NONE("None"),
    BSC("BSC"),
    CFA("CFA"),
    CFP("CFP"),
    ULI("ULI");
    private final String value;

    FrequentFlyerCategoryEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FrequentFlyerCategoryEnum fromValue(String v) {
        for (FrequentFlyerCategoryEnum c: FrequentFlyerCategoryEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
