package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.AirportDetailsData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetrieveAirportDetailsResponse extends BaseResponse {

	private AirportDetailsData airportDetails;

	public AirportDetailsData getAirportDetails() {
		return airportDetails;
	}

	public void setAirportDetails(AirportDetailsData airportDetails) {
		this.airportDetails = airportDetails;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((airportDetails == null) ? 0 : airportDetails.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveAirportDetailsResponse other = (RetrieveAirportDetailsResponse) obj;
		if (airportDetails == null) {
			if (other.airportDetails != null)
				return false;
		} else if (!airportDetails.equals(other.airportDetails))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveAirportDetailResponse [airportDetails="	+ airportDetails + "]";
	}
}