package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.FrequentFlyerTypeData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetrieveFrequentFlyersResponse extends BaseResponse{

	private List<FrequentFlyerTypeData> frequentFlyers;
	
	public RetrieveFrequentFlyersResponse(){}
	
	public RetrieveFrequentFlyersResponse(String tid, String sid) {
		super(tid, sid);
	}

	public List<FrequentFlyerTypeData> getFrequentFlyers() {
		return frequentFlyers;
	}

	public void setFrequentFlyers(List<FrequentFlyerTypeData> frequentFlyers) {
		this.frequentFlyers = frequentFlyers;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((frequentFlyers == null) ? 0 : frequentFlyers.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveFrequentFlyersResponse other = (RetrieveFrequentFlyersResponse) obj;
		if (frequentFlyers == null) {
			if (other.frequentFlyers != null)
				return false;
		} else if (!frequentFlyers.equals(other.frequentFlyers))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveFrequentFlyersResponse [frequentFlyers="
				+ frequentFlyers + "]";
	}

	
}