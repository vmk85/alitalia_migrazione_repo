package com.alitalia.aem.common.data.home.checkin;


public class CheckinCountryData implements Comparable<CheckinCountryData>{
	
	private String countryCode;
	private String countryISOCode;
	private String countryName;
	private Integer id;

	public CheckinCountryData() {
		super();
	}

	public CheckinCountryData(String countryCode, String countryISOCode,
			String countryName, Integer id) {
		super();
		this.countryCode = countryCode;
		this.countryISOCode = countryISOCode;
		this.countryName = countryName;
		this.id = id;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryISOCode() {
		return countryISOCode;
	}

	public void setCountryISOCode(String countryISOCode) {
		this.countryISOCode = countryISOCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((countryCode == null) ? 0 : countryCode.hashCode());
		result = prime * result
				+ ((countryISOCode == null) ? 0 : countryISOCode.hashCode());
		result = prime * result
				+ ((countryName == null) ? 0 : countryName.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinCountryData other = (CheckinCountryData) obj;
		if (countryCode == null) {
			if (other.countryCode != null)
				return false;
		} else if (!countryCode.equals(other.countryCode))
			return false;
		if (countryISOCode == null) {
			if (other.countryISOCode != null)
				return false;
		} else if (!countryISOCode.equals(other.countryISOCode))
			return false;
		if (countryName == null) {
			if (other.countryName != null)
				return false;
		} else if (!countryName.equals(other.countryName))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CheckinCountryData [countryCode=" + countryCode
				+ ", countryISOCode=" + countryISOCode + ", countryName="
				+ countryName + ", id=" + id + ", toString()="
				+ super.toString() + "]";
	}

	@Override
	public int compareTo(CheckinCountryData o) {
		/*Used to sort countries alphabetically*/
		return this.countryName.compareTo(o.countryName);
	}
	
	public CheckinCountryData clone(){
		CheckinCountryData newObj = new CheckinCountryData();
		newObj.countryCode = this.countryCode;
		newObj.countryISOCode = this.countryISOCode;
		newObj.countryName = this.countryName;
		return newObj; 
	}

}