package com.alitalia.aem.common.data.home.checkin;

import com.alitalia.aem.common.data.home.enumerations.CheckinAvailabilitySeatEnum;
import com.alitalia.aem.common.data.home.enumerations.CheckinTypeSeatEnum;

public class CheckinSeatData {
	
	private CheckinAvailabilitySeatEnum availability;
	private Boolean isComfortSeat;
	private Boolean isUpgradeSeat;
	private CheckinTypeSeatEnum type;
	
	public CheckinAvailabilitySeatEnum getAvailability() {
		return availability;
	}
	public void setAvailability(CheckinAvailabilitySeatEnum availability) {
		this.availability = availability;
	}
	public Boolean getIsComfortSeat() {
		return isComfortSeat;
	}
	public void setIsComfortSeat(Boolean isComfortSeat) {
		this.isComfortSeat = isComfortSeat;
	}
	public Boolean getIsUpgradeSeat() {
		return isUpgradeSeat;
	}
	public void setIsUpgradeSeat(Boolean isUpgradeSeat) {
		this.isUpgradeSeat = isUpgradeSeat;
	}
	public CheckinTypeSeatEnum getType() {
		return type;
	}
	public void setType(CheckinTypeSeatEnum type) {
		this.type = type;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((availability == null) ? 0 : availability.hashCode());
		result = prime * result
				+ ((isComfortSeat == null) ? 0 : isComfortSeat.hashCode());
		result = prime * result
				+ ((isUpgradeSeat == null) ? 0 : isUpgradeSeat.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinSeatData other = (CheckinSeatData) obj;
		if (availability != other.availability)
			return false;
		if (isComfortSeat == null) {
			if (other.isComfortSeat != null)
				return false;
		} else if (!isComfortSeat.equals(other.isComfortSeat))
			return false;
		if (isUpgradeSeat == null) {
			if (other.isUpgradeSeat != null)
				return false;
		} else if (!isUpgradeSeat.equals(other.isUpgradeSeat))
			return false;
		if (type != other.type)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CheckinSeatData [availability=" + availability
				+ ", isComfortSeat=" + isComfortSeat + ", isUpgradeSeat="
				+ isUpgradeSeat + ", type=" + type + "]";
	}

}
