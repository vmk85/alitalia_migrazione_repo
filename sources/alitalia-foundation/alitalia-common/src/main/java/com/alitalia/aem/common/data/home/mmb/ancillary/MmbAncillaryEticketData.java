package com.alitalia.aem.common.data.home.mmb.ancillary;

import java.util.List;

public class MmbAncillaryEticketData {

	private String number;
	private String pnr;
	private List<MmbAncillaryCouponData> coupons;

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public List<MmbAncillaryCouponData> getCoupons() {
		return coupons;
	}

	public void setCoupons(List<MmbAncillaryCouponData> coupons) {
		this.coupons = coupons;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((pnr == null) ? 0 : pnr.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbAncillaryEticketData other = (MmbAncillaryEticketData) obj;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (pnr == null) {
			if (other.pnr != null)
				return false;
		} else if (!pnr.equals(other.pnr))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbAncillaryETicketData [number=" + number + ", pnr=" + pnr
				+ ", coupons=" + coupons + "]";
	}

}
