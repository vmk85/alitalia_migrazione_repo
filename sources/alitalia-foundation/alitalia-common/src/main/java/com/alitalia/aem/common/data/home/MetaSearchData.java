package com.alitalia.aem.common.data.home;

import java.util.List;

public class MetaSearchData extends BrandSearchData {
	
	private String mseType;
    private List<PassengerBaseData> passengers;
    
	public String getMseType() {
		return mseType;
	}
	public void setMseType(String mseType) {
		this.mseType = mseType;
	}
	public List<PassengerBaseData> getPassengers() {
		return passengers;
	}
	public void setPassengers(List<PassengerBaseData> passengers) {
		this.passengers = passengers;
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((mseType == null) ? 0 : mseType.hashCode());
		result = prime * result
				+ ((passengers == null) ? 0 : passengers.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MetaSearchData other = (MetaSearchData) obj;
		if (mseType == null) {
			if (other.mseType != null)
				return false;
		} else if (!mseType.equals(other.mseType))
			return false;
		if (passengers == null) {
			if (other.passengers != null)
				return false;
		} else if (!passengers.equals(other.passengers))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "MetaSearchData [mseType=" + mseType + ", passengers="
				+ passengers + ", getMseType()="
				+ getMseType() + ", getPassengers()=" + getPassengers()
				+ ", getCug()=" + getCug() + ", getDestinations()=" + getDestinations()
				+ ", getInnerSearch()=" + getInnerSearch() + ", getMarket()="
				+ getMarket() + ", isOnlyDirectFlight()="
				+ isOnlyDirectFlight() + ", getPassengerNumbers()="
				+ getPassengerNumbers() + ", getSearchCabin()="
				+ getSearchCabin() + ", getSearchCabinType()="
				+ getSearchCabinType() + ", getType()=" + getType()
				+ ", getResidency()=" + getResidency()
				+ ", getStartOutboundDate()=" + getStartOutboundDate()
				+ ", getStartReturnDate()=" + getStartReturnDate()
				+ ", toString()=" + super.toString() + ", getId()=" + getId()
				+ ", getSessionId()=" + getSessionId() + ", getSolutionSet()="
				+ getSolutionSet() + ", getSolutionId()=" + getSolutionId()
				+ ", getRefreshSolutionId()=" + getRefreshSolutionId()
				+ ", getFarmId()=" + getFarmId() + ", getProperties()="
				+ getProperties() + ", hashCode()=" + hashCode()
				+ ", getClass()=" + getClass() + "]";
	}
}
