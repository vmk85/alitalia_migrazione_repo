package com.alitalia.aem.common.data.home.enumerations;

public enum MmbLegTypeEnum {

    NONE("None"),
    DOM("DOM"),
    INZ("INZ"),
    INC("INC");
    private final String value;

    MmbLegTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MmbLegTypeEnum fromValue(String v) {
        for (MmbLegTypeEnum c: MmbLegTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
