package com.alitalia.aem.common.data.home;

import java.util.Calendar;

import javax.xml.datatype.Duration;

public class FlightItinerayData {

	private FlightDetailsModelData flightDetails;
    private String arrival;    
    private Calendar arrivalDate;
    private Duration arrivalTime;
    
    private String departure;
    private Calendar departureDate;
    private Duration departureTime;
    
    private Duration flightLength;
    
    private String flightNumber;
    private String vector;
	
    public FlightDetailsModelData getFlightDetails() {
		return flightDetails;
	}
    
	public void setFlightDetails(FlightDetailsModelData flightDetails) {
		this.flightDetails = flightDetails;
	}
	
	public String getArrival() {
		return arrival;
	}
	
	public void setArrival(String arrival) {
		this.arrival = arrival;
	}
	
	public Calendar getArrivalDate() {
		return arrivalDate;
	}
	
	public void setArrivalDate(Calendar arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	
	public Duration getArrivalTime() {
		return arrivalTime;
	}
	
	public void setArrivalTime(Duration arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	
	public String getDeparture() {
		return departure;
	}
	
	public void setDeparture(String departure) {
		this.departure = departure;
	}
	
	public Calendar getDepartureDate() {
		return departureDate;
	}
	
	public void setDepartureDate(Calendar departureDate) {
		this.departureDate = departureDate;
	}
	
	public Duration getDepartureTime() {
		return departureTime;
	}
	
	public void setDepartureTime(Duration departureTime) {
		this.departureTime = departureTime;
	}
	
	public Duration getFlightLength() {
		return flightLength;
	}
	
	public void setFlightLength(Duration flightLength) {
		this.flightLength = flightLength;
	}
	
	public String getFlightNumber() {
		return flightNumber;
	}
	
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	
	public String getVector() {
		return vector;
	}
	
	public void setVector(String vector) {
		this.vector = vector;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((arrival == null) ? 0 : arrival.hashCode());
		result = prime * result
				+ ((arrivalDate == null) ? 0 : arrivalDate.hashCode());
		result = prime * result
				+ ((arrivalTime == null) ? 0 : arrivalTime.hashCode());
		result = prime * result
				+ ((departure == null) ? 0 : departure.hashCode());
		result = prime * result
				+ ((departureDate == null) ? 0 : departureDate.hashCode());
		result = prime * result
				+ ((departureTime == null) ? 0 : departureTime.hashCode());
		result = prime * result
				+ ((flightDetails == null) ? 0 : flightDetails.hashCode());
		result = prime * result
				+ ((flightLength == null) ? 0 : flightLength.hashCode());
		result = prime * result
				+ ((flightNumber == null) ? 0 : flightNumber.hashCode());
		result = prime * result + ((vector == null) ? 0 : vector.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FlightItinerayData other = (FlightItinerayData) obj;
		if (arrival == null) {
			if (other.arrival != null)
				return false;
		} else if (!arrival.equals(other.arrival))
			return false;
		if (arrivalDate == null) {
			if (other.arrivalDate != null)
				return false;
		} else if (!arrivalDate.equals(other.arrivalDate))
			return false;
		if (arrivalTime == null) {
			if (other.arrivalTime != null)
				return false;
		} else if (!arrivalTime.equals(other.arrivalTime))
			return false;
		if (departure == null) {
			if (other.departure != null)
				return false;
		} else if (!departure.equals(other.departure))
			return false;
		if (departureDate == null) {
			if (other.departureDate != null)
				return false;
		} else if (!departureDate.equals(other.departureDate))
			return false;
		if (departureTime == null) {
			if (other.departureTime != null)
				return false;
		} else if (!departureTime.equals(other.departureTime))
			return false;
		if (flightDetails == null) {
			if (other.flightDetails != null)
				return false;
		} else if (!flightDetails.equals(other.flightDetails))
			return false;
		if (flightLength == null) {
			if (other.flightLength != null)
				return false;
		} else if (!flightLength.equals(other.flightLength))
			return false;
		if (flightNumber == null) {
			if (other.flightNumber != null)
				return false;
		} else if (!flightNumber.equals(other.flightNumber))
			return false;
		if (vector == null) {
			if (other.vector != null)
				return false;
		} else if (!vector.equals(other.vector))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FlightItinerayData [flightDetails=" + flightDetails
				+ ", arrival=" + arrival + ", arrivalDate=" + arrivalDate
				+ ", arrivalTime=" + arrivalTime + ", departure=" + departure
				+ ", departureDate=" + departureDate + ", departureTime="
				+ departureTime + ", flightLength=" + flightLength
				+ ", flightNumber=" + flightNumber + ", vector=" + vector + "]";
	}
}