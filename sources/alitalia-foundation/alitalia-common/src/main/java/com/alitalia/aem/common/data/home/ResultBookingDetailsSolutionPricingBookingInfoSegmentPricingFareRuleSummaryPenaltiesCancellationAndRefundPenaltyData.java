package com.alitalia.aem.common.data.home;

public class ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData {
	
	private String applicationField;
	private String maxPriceField;
	private String minPriceField;
	
	public String getApplicationField() {
		return applicationField;
	}
	
	public void setApplicationField(String applicationField) {
		this.applicationField = applicationField;
	}
	
	public String getMaxPriceField() {
		return maxPriceField;
	}
	
	public void setMaxPriceField(String maxPriceField) {
		this.maxPriceField = maxPriceField;
	}
	
	public String getMinPriceField() {
		return minPriceField;
	}
	
	public void setMinPriceField(String minPriceField) {
		this.minPriceField = minPriceField;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((applicationField == null) ? 0 : applicationField.hashCode());
		result = prime * result
				+ ((maxPriceField == null) ? 0 : maxPriceField.hashCode());
		result = prime * result
				+ ((minPriceField == null) ? 0 : minPriceField.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData other = (ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData) obj;
		if (applicationField == null) {
			if (other.applicationField != null)
				return false;
		} else if (!applicationField.equals(other.applicationField))
			return false;
		if (maxPriceField == null) {
			if (other.maxPriceField != null)
				return false;
		} else if (!maxPriceField.equals(other.maxPriceField))
			return false;
		if (minPriceField == null) {
			if (other.minPriceField != null)
				return false;
		} else if (!minPriceField.equals(other.minPriceField))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData [applicationField="
				+ applicationField
				+ ", maxPriceField="
				+ maxPriceField
				+ ", minPriceField=" + minPriceField + "]";
	}
	
}
