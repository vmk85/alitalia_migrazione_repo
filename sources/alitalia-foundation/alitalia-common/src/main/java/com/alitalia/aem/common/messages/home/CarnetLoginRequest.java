package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class CarnetLoginRequest extends BaseRequest {

    private String carnetCode;
    private String password;

	public CarnetLoginRequest(String tid, String sid) {
		super(tid, sid);
	}

	public CarnetLoginRequest() {
		super();
	}

	public String getCarnetCode() {
		return carnetCode;
	}

	public void setCarnetCode(String carnetCode) {
		this.carnetCode = carnetCode;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((carnetCode == null) ? 0 : carnetCode.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarnetLoginRequest other = (CarnetLoginRequest) obj;
		if (carnetCode == null) {
			if (other.carnetCode != null)
				return false;
		} else if (!carnetCode.equals(other.carnetCode))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CarnetLoginRequest [carnetCode=" + carnetCode + ", password="
				+ password + "]";
	}
}