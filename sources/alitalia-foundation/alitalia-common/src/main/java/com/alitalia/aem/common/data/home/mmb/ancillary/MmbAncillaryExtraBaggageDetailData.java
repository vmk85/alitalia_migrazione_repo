package com.alitalia.aem.common.data.home.mmb.ancillary;

import com.alitalia.aem.common.data.MmbAncillaryDetail;

public class MmbAncillaryExtraBaggageDetailData implements MmbAncillaryDetail {

	private Integer baggageType;

	public Integer getBaggageType() {
		return baggageType;
	}

	public void setBaggageType(Integer baggageType) {
		this.baggageType = baggageType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((baggageType == null) ? 0 : baggageType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbAncillaryExtraBaggageDetailData other = (MmbAncillaryExtraBaggageDetailData) obj;
		if (baggageType == null) {
			if (other.baggageType != null)
				return false;
		} else if (!baggageType.equals(other.baggageType))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbExtraBaggageDetailData [baggageType=" + baggageType + "]";
	}

}
