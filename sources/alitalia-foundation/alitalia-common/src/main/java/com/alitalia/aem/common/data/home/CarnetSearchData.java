package com.alitalia.aem.common.data.home;


public class CarnetSearchData extends BrandSearchData {
	
	private Integer carnetQuantity;
	
	public CarnetSearchData() {
	}
	
	public CarnetSearchData(CarnetSearchData clone) {
		super(clone);
		this.carnetQuantity = clone.carnetQuantity;
	}
    
	public Integer getCarnetQuantity() {
		return carnetQuantity;
	}

	public void setCarnetQuantity(Integer carnetQuantity) {
		this.carnetQuantity = carnetQuantity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + carnetQuantity;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarnetSearchData other = (CarnetSearchData) obj;
		if (carnetQuantity != other.carnetQuantity)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CarnetSearchData [carnetQuantity=" + carnetQuantity + ", getCug()="
				+ getCug() + ", getDestinations()=" + getDestinations()
				+ ", getInnerSearch()=" + getInnerSearch() + ", getMarket()="
				+ getMarket() + ", isOnlyDirectFlight()="
				+ isOnlyDirectFlight() + ", getPassengerNumbers()="
				+ getPassengerNumbers() + ", getSearchCabin()="
				+ getSearchCabin() + ", getSearchCabinType()="
				+ getSearchCabinType() + ", getType()=" + getType()
				+ ", getResidency()=" + getResidency() + ", toString()="
				+ super.toString() + ", getId()=" + getId()
				+ ", getSessionId()=" + getSessionId() + ", getSolutionSet()="
				+ getSolutionSet() + ", getSolutionId()=" + getSolutionId()
				+ ", getRefreshSolutionId()=" + getRefreshSolutionId()
				+ ", getFarmId()=" + getFarmId() + ", getProperties()="
				+ getProperties() + ", hashCode()=" + hashCode()
				+ ", getClass()=" + getClass() + "]";
	}
}