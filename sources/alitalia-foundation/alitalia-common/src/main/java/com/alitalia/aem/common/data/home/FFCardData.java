package com.alitalia.aem.common.data.home;

import java.util.Calendar;

import com.alitalia.aem.common.data.home.enumerations.FFCardStatusEnum;

public class FFCardData {

    private String code;
    private Calendar startDate;
    private Calendar endDate;
    private String other;
    private FFCardStatusEnum status;
    private String text;
    private String vipCode;
	
    public String getCode() {
		return code;
	}
	
    public void setCode(String code) {
		this.code = code;
	}
	
    public Calendar getEndDate() {
		return endDate;
	}
	
    public void setEndDate(Calendar endDate) {
		this.endDate = endDate;
	}
	
    public String getOther() {
		return other;
	}
	
    public void setOther(String other) {
		this.other = other;
	}
	
    public Calendar getStartDate() {
		return startDate;
	}
	
    public void setStartDate(Calendar startDate) {
		this.startDate = startDate;
	}
	
    public FFCardStatusEnum getStatus() {
		return status;
	}
	
    public void setStatus(FFCardStatusEnum status) {
		this.status = status;
	}
	
    public String getText() {
		return text;
	}
	
    public void setText(String text) {
		this.text = text;
	}
	
    public String getVipCode() {
		return vipCode;
	}
	
    public void setVipCode(String vipCode) {
		this.vipCode = vipCode;
	}

	@Override
	public String toString() {
		return "FFCardData [code=" + code + ", endDate=" + endDate + ", other="
				+ other + ", startDate=" + startDate + ", status=" + status
				+ ", text=" + text + ", vipCode=" + vipCode + "]";
	}
}
