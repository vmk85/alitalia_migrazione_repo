package com.alitalia.aem.common.data.home;

public class ResultBookingDetailsSolutionPricingBookingInfoSegmentFlightData {
	
	private String carrierField;
	private int numberField;
	private boolean numberFieldSpecified;
	
	public String getCarrierField() {
		return carrierField;
	}
	
	public void setCarrierField(String carrierField) {
		this.carrierField = carrierField;
	}
	
	public int getNumberField() {
		return numberField;
	}
	
	public void setNumberField(int numberField) {
		this.numberField = numberField;
	}
	
	public boolean isNumberFieldSpecified() {
		return numberFieldSpecified;
	}
	
	public void setNumberFieldSpecified(boolean numberFieldSpecified) {
		this.numberFieldSpecified = numberFieldSpecified;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((carrierField == null) ? 0 : carrierField.hashCode());
		result = prime * result + numberField;
		result = prime * result + (numberFieldSpecified ? 1231 : 1237);
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingDetailsSolutionPricingBookingInfoSegmentFlightData other = (ResultBookingDetailsSolutionPricingBookingInfoSegmentFlightData) obj;
		if (carrierField == null) {
			if (other.carrierField != null)
				return false;
		} else if (!carrierField.equals(other.carrierField))
			return false;
		if (numberField != other.numberField)
			return false;
		if (numberFieldSpecified != other.numberFieldSpecified)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingDetailsSolutionPricingBookingInfoSegmentFlightData [carrierField="
				+ carrierField
				+ ", numberField="
				+ numberField
				+ ", numberFieldSpecified=" + numberFieldSpecified + "]";
	}
	
}
