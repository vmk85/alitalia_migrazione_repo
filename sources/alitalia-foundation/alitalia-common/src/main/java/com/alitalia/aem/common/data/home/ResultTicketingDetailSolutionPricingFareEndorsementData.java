package com.alitalia.aem.common.data.home;

public class ResultTicketingDetailSolutionPricingFareEndorsementData {

    protected String boxesField;

    public String getBoxesField() {
        return boxesField;
    }

    public void setBoxesField(String value) {
        this.boxesField = value;
    }

}
