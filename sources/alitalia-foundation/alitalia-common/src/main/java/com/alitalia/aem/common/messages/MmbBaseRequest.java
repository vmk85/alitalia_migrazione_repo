package com.alitalia.aem.common.messages;

import com.alitalia.aem.common.data.home.mmb.MmbRequestBaseInfoData;

public class MmbBaseRequest extends BaseRequest {

	private MmbRequestBaseInfoData baseInfo;
	private String client;

	public MmbBaseRequest() {
		super();
	}

	public MmbBaseRequest(String tid, String sid) {
		super(tid, sid);
	}

	public MmbBaseRequest(MmbRequestBaseInfoData baseInfo, String client) {
		super();
		this.baseInfo = baseInfo;
		this.client = client;
	}

	public MmbBaseRequest(MmbRequestBaseInfoData baseInfo, String client, String tid, String sid) {
		super(tid, sid);
		this.baseInfo = baseInfo;
		this.client = client;
	}

	public MmbRequestBaseInfoData getBaseInfo() {
		return baseInfo;
	}

	public void setBaseInfo(MmbRequestBaseInfoData baseInfo) {
		this.baseInfo = baseInfo;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	@Override
	public String toString() {
		return "MmbBaseRequest [baseInfo=" + baseInfo + ", client=" + client
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((baseInfo == null) ? 0 : baseInfo.hashCode());
		result = prime * result + ((client == null) ? 0 : client.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbBaseRequest other = (MmbBaseRequest) obj;
		if (baseInfo == null) {
			if (other.baseInfo != null)
				return false;
		} else if (!baseInfo.equals(other.baseInfo))
			return false;
		if (client == null) {
			if (other.client != null)
				return false;
		} else if (!client.equals(other.client))
			return false;
		return true;
	}

}