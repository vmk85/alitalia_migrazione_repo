package com.alitalia.aem.common.data.home.mmb;

import com.alitalia.aem.common.data.home.FrequentFlyerTypeData;

public class MmbFrequentFlyerTypeData extends FrequentFlyerTypeData {

	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbFrequentFlyerTypeData other = (MmbFrequentFlyerTypeData) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbFrequentFlyerTypeData [id=" + id + ", getCode()="
				+ getCode() + ", getDescription()=" + getDescription()
				+ ", getLenght()=" + getLenght() + ", getPreFillChar()="
				+ getPreFillChar() + ", getRegularExpressionValidation()="
				+ getRegularExpressionValidation() + "]";
	}


}
