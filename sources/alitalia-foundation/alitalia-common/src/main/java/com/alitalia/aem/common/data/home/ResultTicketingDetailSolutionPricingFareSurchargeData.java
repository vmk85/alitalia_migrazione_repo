package com.alitalia.aem.common.data.home;

public class ResultTicketingDetailSolutionPricingFareSurchargeData {

    private String codeField;
    private ResultTicketingDetailSolutionPricingFareSurchargePriceData priceField;

    public String getCodeField() {
        return codeField;
    }

    public void setCodeField(String value) {
        this.codeField = value;
    }

    public ResultTicketingDetailSolutionPricingFareSurchargePriceData getPriceField() {
        return priceField;
    }

    public void setPriceField(ResultTicketingDetailSolutionPricingFareSurchargePriceData value) {
        this.priceField = value;
    }

}
