package com.alitalia.aem.common.data.home;

import java.util.HashMap;

public class PropertiesData extends HashMap<String, Object> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7871756492243532994L;

	public Object getElement(String key) {
		return this.get(key);
	}

	public void addElement(String key, Object value) {
		this.put(key, value);
	}
}
