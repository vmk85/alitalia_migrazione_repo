package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.common.messages.BaseRequest;

public class AuthorizePaymentRequest extends BaseRequest {
	private RoutesData prenotation;
	private Boolean flgUsaCa;

	public RoutesData getPrenotation() {
		return prenotation;
	}

	public void setPrenotation(RoutesData prenotation) {
		this.prenotation = prenotation;
	}

	public Boolean getFlgUsaCa() {
		return flgUsaCa;
	}

	public void setFlgUsaCa(Boolean flgUsaCa) {
		this.flgUsaCa = flgUsaCa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((flgUsaCa == null) ? 0 : flgUsaCa.hashCode());
		result = prime * result
				+ ((prenotation == null) ? 0 : prenotation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthorizePaymentRequest other = (AuthorizePaymentRequest) obj;
		if (flgUsaCa == null) {
			if (other.flgUsaCa != null)
				return false;
		} else if (!flgUsaCa.equals(other.flgUsaCa))
			return false;
		if (prenotation == null) {
			if (other.prenotation != null)
				return false;
		} else if (!prenotation.equals(other.prenotation))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AuthorizePaymentRequest [prenotation=" + prenotation
				+ ", flgUsaCa=" + flgUsaCa + "]";
	}

}
