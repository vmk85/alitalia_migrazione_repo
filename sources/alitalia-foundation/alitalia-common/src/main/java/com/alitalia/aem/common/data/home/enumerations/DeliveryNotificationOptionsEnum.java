package com.alitalia.aem.common.data.home.enumerations;

public enum DeliveryNotificationOptionsEnum {

	NONE("None"),
	ONSUCCESS("OnSuccess"),
	ONFAILURE("OnFailure"),
	DELAY("Delay"),
	NEVER("Never");
    private final String value;

    DeliveryNotificationOptionsEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DeliveryNotificationOptionsEnum fromValue(String v) {
        for (DeliveryNotificationOptionsEnum c: DeliveryNotificationOptionsEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
