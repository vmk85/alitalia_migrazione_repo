package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.enumerations.CheckinSearchReasonEnum;
import com.alitalia.aem.common.messages.MmbBaseRequest;

public class ComfortSeatPriceCheckinRequest extends MmbBaseRequest {

    private String boardApt;
    private String currency;
    private String landApt;
    private String market;
    private CheckinSearchReasonEnum reason;
    
	public String getBoardApt() {
		return boardApt;
	}
	public void setBoardApt(String boardApt) {
		this.boardApt = boardApt;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getLandApt() {
		return landApt;
	}
	public void setLandApt(String landApt) {
		this.landApt = landApt;
	}
	public String getMarket() {
		return market;
	}
	public void setMarket(String market) {
		this.market = market;
	}
	public CheckinSearchReasonEnum getReason() {
		return reason;
	}
	public void setReason(CheckinSearchReasonEnum reason) {
		this.reason = reason;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((boardApt == null) ? 0 : boardApt.hashCode());
		result = prime * result
				+ ((currency == null) ? 0 : currency.hashCode());
		result = prime * result + ((landApt == null) ? 0 : landApt.hashCode());
		result = prime * result + ((market == null) ? 0 : market.hashCode());
		result = prime * result + ((reason == null) ? 0 : reason.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComfortSeatPriceCheckinRequest other = (ComfortSeatPriceCheckinRequest) obj;
		if (boardApt == null) {
			if (other.boardApt != null)
				return false;
		} else if (!boardApt.equals(other.boardApt))
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (landApt == null) {
			if (other.landApt != null)
				return false;
		} else if (!landApt.equals(other.landApt))
			return false;
		if (market == null) {
			if (other.market != null)
				return false;
		} else if (!market.equals(other.market))
			return false;
		if (reason != other.reason)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ComfortSeatPriceCheckinRequest [boardApt=" + boardApt
				+ ", currency=" + currency + ", landApt=" + landApt
				+ ", market=" + market + ", reason=" + reason + ", toString()="
				+ super.toString() + "]";
	}

}
