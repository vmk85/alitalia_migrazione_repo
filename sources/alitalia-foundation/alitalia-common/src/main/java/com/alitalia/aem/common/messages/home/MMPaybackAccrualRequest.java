package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.MMMemberIdentificationType;
import com.alitalia.aem.common.data.home.MMTicketNumberType;
import com.alitalia.aem.common.messages.BaseRequest;

public class MMPaybackAccrualRequest extends BaseRequest {

	private String memberVerificationFirstName;
	private String memberVerificationLastName;
    private MMMemberIdentificationType paybackCardNumber;
    private MMTicketNumberType ticketNumber;
    private String username;
    
	public String getMemberVerificationFirstName() {
		return memberVerificationFirstName;
	}
	public void setMemberVerificationFirstName(String memberVerificationFirstName) {
		this.memberVerificationFirstName = memberVerificationFirstName;
	}
	public String getMemberVerificationLastName() {
		return memberVerificationLastName;
	}
	public void setMemberVerificationLastName(String memberVerificationLastName) {
		this.memberVerificationLastName = memberVerificationLastName;
	}
	public MMMemberIdentificationType getPaybackCardNumber() {
		return paybackCardNumber;
	}
	public void setPaybackCardNumber(MMMemberIdentificationType paybackCardNumber) {
		this.paybackCardNumber = paybackCardNumber;
	}
	public MMTicketNumberType getTicketNumber() {
		return ticketNumber;
	}
	public void setTicketNumber(MMTicketNumberType ticketNumber) {
		this.ticketNumber = ticketNumber;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((memberVerificationFirstName == null) ? 0
						: memberVerificationFirstName.hashCode());
		result = prime
				* result
				+ ((memberVerificationLastName == null) ? 0
						: memberVerificationLastName.hashCode());
		result = prime
				* result
				+ ((paybackCardNumber == null) ? 0 : paybackCardNumber
						.hashCode());
		result = prime * result
				+ ((ticketNumber == null) ? 0 : ticketNumber.hashCode());
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MMPaybackAccrualRequest other = (MMPaybackAccrualRequest) obj;
		if (memberVerificationFirstName == null) {
			if (other.memberVerificationFirstName != null)
				return false;
		} else if (!memberVerificationFirstName
				.equals(other.memberVerificationFirstName))
			return false;
		if (memberVerificationLastName == null) {
			if (other.memberVerificationLastName != null)
				return false;
		} else if (!memberVerificationLastName
				.equals(other.memberVerificationLastName))
			return false;
		if (paybackCardNumber == null) {
			if (other.paybackCardNumber != null)
				return false;
		} else if (!paybackCardNumber.equals(other.paybackCardNumber))
			return false;
		if (ticketNumber == null) {
			if (other.ticketNumber != null)
				return false;
		} else if (!ticketNumber.equals(other.ticketNumber))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "MMPaybackAccrualRequest [memberVerificationFirstName="
				+ memberVerificationFirstName + ", memberVerificationLastName="
				+ memberVerificationLastName + ", paybackCardNumber="
				+ paybackCardNumber + ", ticketNumber=" + ticketNumber
				+ ", username=" + username + "]";
	}
}