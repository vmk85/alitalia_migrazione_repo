package com.alitalia.aem.common.messages.home;

import java.util.Calendar;

import com.alitalia.aem.common.messages.BaseRequest;
import com.alitalia.aem.common.messages.SearchExecuteRequestFilter;
import com.alitalia.aem.common.messages.home.enumeration.SearchExecuteResponseType;

public class SearchFlightSolutionRequest extends BaseRequest {

	private SearchExecuteRequestFilter filter;
	private String languageCode;
	private String market;
	private String marketExtraCharge;
	private SearchExecuteResponseType responseType;
	private Calendar ribbonOutboundStartDate;
	private Calendar ribbonReturnStartDate;
	private String fromSearchElement;
	private String toSearchElement;
	private String cug;

	public SearchFlightSolutionRequest(String tid, String sid) {
		super(tid, sid);
	}

	public SearchExecuteRequestFilter getFilter() {
		return filter;
	}

	public void setFilter(SearchExecuteRequestFilter filter) {
		this.filter = filter;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}
	
	public String getMarketExtraCharge() {
		return marketExtraCharge;		
	}
	
	public void setMarketExtraCharge(String marketExtraCharge) {
		this.marketExtraCharge = marketExtraCharge;		
	}

	public SearchExecuteResponseType getResponseType() {
		return responseType;
	}

	public void setResponseType(SearchExecuteResponseType responseType) {
		this.responseType = responseType;
	}

	public Calendar getRibbonOutboundStartDate() {
		return ribbonOutboundStartDate;
	}

	public void setRibbonOutboundStartDate(Calendar ribbonOutboundStartDate) {
		this.ribbonOutboundStartDate = ribbonOutboundStartDate;
	}

	public Calendar getRibbonReturnStartDate() {
		return ribbonReturnStartDate;
	}

	public void setRibbonReturnStartDate(Calendar ribbonReturnStartDate) {
		this.ribbonReturnStartDate = ribbonReturnStartDate;
	}
	
	public String getFromSearchElement() {
		return fromSearchElement;
	}

	public void setFromSearchElement(String fromSearchElement) {
		this.fromSearchElement = fromSearchElement;
	}

	public String getToSearchElement() {
		return toSearchElement;
	}

	public void setToSearchElement(String toSearchElement) {
		this.toSearchElement = toSearchElement;
	}
	
	public String getCug() {
		return cug;
	}

	public void setCug(String cug) {
		this.cug = cug;
	}
	

	@Override
	public String toString() {
		return "SearchFlightSolutionRequest [filter=" + filter
				+ ", languageCode=" + languageCode + ", market=" + market
				+ ", responseType=" + responseType
				+ ", ribbonOutboundStartDate=" + ribbonOutboundStartDate
				+ ", ribbonReturnStartDate=" + ribbonReturnStartDate
				+ ", getTid()=" + getTid() + ", getSid()=" + getSid() + "]";
	}

	
}
