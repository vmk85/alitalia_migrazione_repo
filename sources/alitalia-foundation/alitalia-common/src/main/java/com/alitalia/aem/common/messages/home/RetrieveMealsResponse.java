package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.MealData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetrieveMealsResponse extends BaseResponse{

	private List<MealData> meals;
	
	public RetrieveMealsResponse(){}

	public List<MealData> getMeals() {
		return meals;
	}

	public void setMeals(List<MealData> meals) {
		this.meals = meals;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((meals == null) ? 0 : meals.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveMealsResponse other = (RetrieveMealsResponse) obj;
		if (meals == null) {
			if (other.meals != null)
				return false;
		} else if (!meals.equals(other.meals))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveMealsResponse [meals=" + meals + "]";
	}
	
	
}