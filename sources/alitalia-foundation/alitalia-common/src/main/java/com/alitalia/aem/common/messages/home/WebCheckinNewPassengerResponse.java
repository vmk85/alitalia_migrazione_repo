package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.messages.BaseResponse;

public class WebCheckinNewPassengerResponse extends BaseResponse{
	
	private CheckinPassengerData passenger;

	public CheckinPassengerData getPassenger() {
		return passenger;
	}

	public void setPassenger(CheckinPassengerData passenger) {
		this.passenger = passenger;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((passenger == null) ? 0 : passenger.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebCheckinNewPassengerResponse other = (WebCheckinNewPassengerResponse) obj;
		if (passenger == null) {
			if (other.passenger != null)
				return false;
		} else if (!passenger.equals(other.passenger))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WebCheckinNewPassengerResponse [passenger=" + passenger + "]";
	}
}