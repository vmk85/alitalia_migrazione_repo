package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;
import com.alitalia.aem.common.messages.home.enumeration.CashMilesOperationType;

public class UpdateWidgetCashMilesRequest extends BaseRequest {
	
	private String site;
	private CashAndMilesData cashMiles;
	private CashMilesOperationType operationType;
	private String pnr;

	public UpdateWidgetCashMilesRequest(String tid, String sid) {
		super(tid, sid);
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public CashAndMilesData getCashMiles() {
		return cashMiles;
	}

	public void setCashMiles(CashAndMilesData cashMiles) {
		this.cashMiles = cashMiles;
	}

	public CashMilesOperationType getOperationType() {
		return operationType;
	}

	public void setOperationType(CashMilesOperationType operationType) {
		this.operationType = operationType;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((cashMiles == null) ? 0 : cashMiles.hashCode());
		result = prime * result
				+ ((operationType == null) ? 0 : operationType.hashCode());
		result = prime * result + ((pnr == null) ? 0 : pnr.hashCode());
		result = prime * result + ((site == null) ? 0 : site.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		UpdateWidgetCashMilesRequest other = (UpdateWidgetCashMilesRequest) obj;
		if (cashMiles == null) {
			if (other.cashMiles != null)
				return false;
		} else if (!cashMiles.equals(other.cashMiles))
			return false;
		if (operationType != other.operationType)
			return false;
		if (pnr == null) {
			if (other.pnr != null)
				return false;
		} else if (!pnr.equals(other.pnr))
			return false;
		if (site == null) {
			if (other.site != null)
				return false;
		} else if (!site.equals(other.site))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UpdateWidgetCashMilesRequest [site=" + site + ", cashMiles="
				+ cashMiles + ", operationType=" + operationType + ", pnr="
				+ pnr + ", getTid()=" + getTid() + ", getSid()=" + getSid()
				+ "]";
	}

}
