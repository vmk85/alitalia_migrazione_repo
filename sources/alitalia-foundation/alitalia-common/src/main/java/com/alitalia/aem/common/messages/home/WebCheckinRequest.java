package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.checkin.CheckinRouteData;
import com.alitalia.aem.common.messages.MmbBaseRequest;

public class WebCheckinRequest extends MmbBaseRequest {

	private List<CheckinPassengerData> passengersList;
	private String pnr;
	private CheckinRouteData selectedRoute;

	public WebCheckinRequest(String tid, String sid) {
		super(tid, sid);
	}

	public WebCheckinRequest() {
		super();
	}

	public List<CheckinPassengerData> getPassengersList() {
		return passengersList;
	}

	public void setPassengersList(List<CheckinPassengerData> passengersList) {
		this.passengersList = passengersList;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public CheckinRouteData getSelectedRoute() {
		return selectedRoute;
	}

	public void setSelectedRoute(CheckinRouteData selectedRoute) {
		this.selectedRoute = selectedRoute;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((passengersList == null) ? 0 : passengersList.hashCode());
		result = prime * result + ((pnr == null) ? 0 : pnr.hashCode());
		result = prime * result
				+ ((selectedRoute == null) ? 0 : selectedRoute.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebCheckinRequest other = (WebCheckinRequest) obj;
		if (passengersList == null) {
			if (other.passengersList != null)
				return false;
		} else if (!passengersList.equals(other.passengersList))
			return false;
		if (pnr == null) {
			if (other.pnr != null)
				return false;
		} else if (!pnr.equals(other.pnr))
			return false;
		if (selectedRoute == null) {
			if (other.selectedRoute != null)
				return false;
		} else if (!selectedRoute.equals(other.selectedRoute))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WebCheckinRequest [passengersList=" + passengersList + ", pnr="
				+ pnr + ", selectedRoute=" + selectedRoute
				+ ", getPassengersList()=" + getPassengersList()
				+ ", getPnr()=" + getPnr() + ", getSelectedRoute()="
				+ getSelectedRoute() + ", hashCode()=" + hashCode() + "]";
	}
}
