package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class CarnetSendNotificationEmailResponse extends BaseResponse {

	private Boolean haveSendEmail;

	public Boolean getHaveSendEmail() {
		return haveSendEmail;
	}

	public void setHaveSendEmail(Boolean haveSendEmail) {
		this.haveSendEmail = haveSendEmail;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((haveSendEmail == null) ? 0 : haveSendEmail.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarnetSendNotificationEmailResponse other = (CarnetSendNotificationEmailResponse) obj;
		if (haveSendEmail == null) {
			if (other.haveSendEmail != null)
				return false;
		} else if (!haveSendEmail.equals(other.haveSendEmail))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CarnetSendNotificationEmailResponse [haveSendEmail=" + haveSendEmail + "]";
	}
}
