package com.alitalia.aem.common.data.home;

public class JumpUpSearchData extends BrandSearchData {

	private Integer plusMinusDays;
	private Boolean flexSearchSet;
	
	public Integer getPlusMinusDays() {
		return plusMinusDays;
	}

	public void setPlusMinusDays(Integer plusMinusDays) {
		this.plusMinusDays = plusMinusDays;
	}

	public Boolean getFlexSearchSet() {
		return flexSearchSet;
	}

	public void setFlexSearchSet(Boolean flexSearchSet) {
		this.flexSearchSet = flexSearchSet;
	}
	
	@Override
	public String toString() {
		return "YouthSearchData [plusMinusDays=" + plusMinusDays + ", flexSearchSet=" + flexSearchSet + ", getCug()="
				+ getCug() + ", getDestinations()=" + getDestinations() + ", getInnerSearch()=" + getInnerSearch()
				+ ", getMarket()=" + getMarket() + ", isOnlyDirectFlight()=" + isOnlyDirectFlight()
				+ ", getPassengerNumbers()=" + getPassengerNumbers() + ", getSearchCabin()=" + getSearchCabin()
				+ ", getSearchCabinType()=" + getSearchCabinType() + ", getType()=" + getType() + ", getResidency()="
				+ getResidency() + ", toString()=" + super.toString() + ", getId()=" + getId() + ", getSessionId()="
				+ getSessionId() + ", getSolutionSet()=" + getSolutionSet() + ", getSolutionId()=" + getSolutionId()
				+ ", getRefreshSolutionId()=" + getRefreshSolutionId() + ", getFarmId()=" + getFarmId()
				+ ", getProperties()=" + getProperties() + ", hashCode()=" + hashCode() + ", getClass()=" + getClass()
				+ "]";
	}
	
}
