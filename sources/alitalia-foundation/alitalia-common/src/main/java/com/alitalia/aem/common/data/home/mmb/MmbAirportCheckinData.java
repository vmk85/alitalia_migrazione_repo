package com.alitalia.aem.common.data.home.mmb;

public class MmbAirportCheckinData {

	private Integer id;
	private String airportName;
	private Boolean arriveEnabled;
	private String code;
	private String deepLink;
	private Boolean departureEnabled;

	public String getAirportName() {
		return airportName;
	}

	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getArriveEnabled() {
		return arriveEnabled;
	}

	public void setArriveEnabled(Boolean arriveEnabled) {
		this.arriveEnabled = arriveEnabled;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDeepLink() {
		return deepLink;
	}

	public void setDeepLink(String deepLink) {
		this.deepLink = deepLink;
	}

	public Boolean getDepartureEnabled() {
		return departureEnabled;
	}

	public void setDepartureEnabled(Boolean departureEnabled) {
		this.departureEnabled = departureEnabled;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbAirportCheckinData other = (MmbAirportCheckinData) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbAirportCheckinData [id=" + id + ", airportName="
				+ airportName + ", arriveEnabled=" + arriveEnabled + ", code="
				+ code + ", deepLink=" + deepLink + ", departureEnabled="
				+ departureEnabled + "]";
	}

}
