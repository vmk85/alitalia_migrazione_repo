package com.alitalia.aem.common.data.home;


public class CountryData {
	
	private String code;
	private String description;
	private String stateCode;
	private String stateDescription;

	public CountryData() {
		super();
	}

	public CountryData(String code, String description, String stateCode,
			String stateDescription) {
		super();
		this.code = code;
		this.description = description;
		this.stateCode = stateCode;
		this.stateDescription = stateDescription;
	}

	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getStateCode() {
		return stateCode;
	}
	
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	
	public String getStateDescription() {
		return stateDescription;
	}
	
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result
				+ ((stateCode == null) ? 0 : stateCode.hashCode());
		result = prime
				* result
				+ ((stateDescription == null) ? 0 : stateDescription.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CountryData other = (CountryData) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (stateCode == null) {
			if (other.stateCode != null)
				return false;
		} else if (!stateCode.equals(other.stateCode))
			return false;
		if (stateDescription == null) {
			if (other.stateDescription != null)
				return false;
		} else if (!stateDescription.equals(other.stateDescription))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CountryData [code=" + code + ", description=" + description
				+ ", stateCode=" + stateCode + ", stateDescription="
				+ stateDescription + "]";
	}
	
}