package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.enumerations.TradeStatisticDataTypeEnum;
import com.alitalia.aem.common.messages.BaseRequest;

public class TradeRegisterStatisticRequest extends BaseRequest {

	private String clientIP;
	private String errorDescr;
	private String sessionId;
	private String siteCode;
	private boolean success;
	private TradeStatisticDataTypeEnum type;
	private String userId;
	
	public TradeRegisterStatisticRequest() {}
	
	public TradeRegisterStatisticRequest(String tid, String sid) {
		super(tid, sid);
	}

	public String getClientIP() {
		return clientIP;
	}
	
	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}
	
	public String getErrorDescr() {
		return errorDescr;
	}
	
	public void setErrorDescr(String errorDescr) {
		this.errorDescr = errorDescr;
	}
	
	public String getSessionId() {
		return sessionId;
	}
	
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	public String getSiteCode() {
		return siteCode;
	}
	
	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}
	
	public boolean isSuccess() {
		return success;
	}
	
	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	public TradeStatisticDataTypeEnum getType() {
		return type;
	}
	
	public void setType(TradeStatisticDataTypeEnum type) {
		this.type = type;
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((clientIP == null) ? 0 : clientIP.hashCode());
		result = prime * result
				+ ((errorDescr == null) ? 0 : errorDescr.hashCode());
		result = prime * result
				+ ((sessionId == null) ? 0 : sessionId.hashCode());
		result = prime * result
				+ ((siteCode == null) ? 0 : siteCode.hashCode());
		result = prime * result + (success ? 1231 : 1237);
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TradeRegisterStatisticRequest other = (TradeRegisterStatisticRequest) obj;
		if (clientIP == null) {
			if (other.clientIP != null)
				return false;
		} else if (!clientIP.equals(other.clientIP))
			return false;
		if (errorDescr == null) {
			if (other.errorDescr != null)
				return false;
		} else if (!errorDescr.equals(other.errorDescr))
			return false;
		if (sessionId == null) {
			if (other.sessionId != null)
				return false;
		} else if (!sessionId.equals(other.sessionId))
			return false;
		if (siteCode == null) {
			if (other.siteCode != null)
				return false;
		} else if (!siteCode.equals(other.siteCode))
			return false;
		if (success != other.success)
			return false;
		if (type != other.type)
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RegisterStatisticsRequest [clientIP=" + clientIP
				+ ", errorDescr=" + errorDescr + ", sessionId=" + sessionId
				+ ", siteCode=" + siteCode + ", success=" + success + ", type="
				+ type + ", userId=" + userId + "]";
	}
}