package com.alitalia.aem.common.data.home.mmb.ancillary;

import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;


public class MmbAncillaryPaymentData {

	private String address;
	private String cancellUrl;
	private String cardNumber;
	private String city;
	private String country;
	private String creditCardType;
	private String cvv;
	private String email;
	private String errorUrl;
	private Integer expirationMonth;
	private Integer expirationYear;
	private String ipAddress;
	private String okUrl;
	private String ownerLastName;
	private String ownerName;
	private PaymentTypeEnum paymentType;
	private String stare;
	private String userAgent;
	private String zipCode;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCreditCardType() {
		return creditCardType;
	}

	public void setCreditCardType(String creditCardType) {
		this.creditCardType = creditCardType;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getExpirationMonth() {
		return expirationMonth;
	}

	public void setExpirationMonth(Integer expirationMonth) {
		this.expirationMonth = expirationMonth;
	}

	public Integer getExpirationYear() {
		return expirationYear;
	}

	public void setExpirationYear(Integer expirationYear) {
		this.expirationYear = expirationYear;
	}

	public String getCancellUrl() {
		return cancellUrl;
	}

	public void setCancellUrl(String cancellUrl) {
		this.cancellUrl = cancellUrl;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	public String getErrorUrl() {
		return errorUrl;
	}

	public void setErrorUrl(String errorUrl) {
		this.errorUrl = errorUrl;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getOkUrl() {
		return okUrl;
	}

	public void setOkUrl(String okUrl) {
		this.okUrl = okUrl;
	}

	public PaymentTypeEnum getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentTypeEnum paymentType) {
		this.paymentType = paymentType;
	}

	public String getStare() {
		return stare;
	}

	public void setStare(String stare) {
		this.stare = stare;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getOwnerLastName() {
		return ownerLastName;
	}

	public void setOwnerLastName(String ownerLastName) {
		this.ownerLastName = ownerLastName;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cardNumber == null) ? 0 : cardNumber.hashCode());
		result = prime * result
				+ ((creditCardType == null) ? 0 : creditCardType.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result
				+ ((expirationMonth == null) ? 0 : expirationMonth.hashCode());
		result = prime * result
				+ ((expirationYear == null) ? 0 : expirationYear.hashCode());
		result = prime * result
				+ ((ownerLastName == null) ? 0 : ownerLastName.hashCode());
		result = prime * result
				+ ((ownerName == null) ? 0 : ownerName.hashCode());
		result = prime * result
				+ ((paymentType == null) ? 0 : paymentType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbAncillaryPaymentData other = (MmbAncillaryPaymentData) obj;
		if (cardNumber == null) {
			if (other.cardNumber != null)
				return false;
		} else if (!cardNumber.equals(other.cardNumber))
			return false;
		if (creditCardType == null) {
			if (other.creditCardType != null)
				return false;
		} else if (!creditCardType.equals(other.creditCardType))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (expirationMonth == null) {
			if (other.expirationMonth != null)
				return false;
		} else if (!expirationMonth.equals(other.expirationMonth))
			return false;
		if (expirationYear == null) {
			if (other.expirationYear != null)
				return false;
		} else if (!expirationYear.equals(other.expirationYear))
			return false;
		if (ownerLastName == null) {
			if (other.ownerLastName != null)
				return false;
		} else if (!ownerLastName.equals(other.ownerLastName))
			return false;
		if (ownerName == null) {
			if (other.ownerName != null)
				return false;
		} else if (!ownerName.equals(other.ownerName))
			return false;
		if (paymentType != other.paymentType)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbAncillaryPaymentData [address=" + address + ", cancellUrl="
				+ cancellUrl + ", cardNumber=" + cardNumber + ", city=" + city
				+ ", country=" + country + ", creditCardType=" + creditCardType
				+ ", cvv=" + cvv + ", email=" + email + ", errorUrl="
				+ errorUrl + ", expirationMonth=" + expirationMonth
				+ ", expirationYear=" + expirationYear + ", ipAddress="
				+ ipAddress + ", okUrl=" + okUrl + ", ownerLastName="
				+ ownerLastName + ", ownerName=" + ownerName + ", paymentType="
				+ paymentType + ", stare=" + stare + ", userAgent=" + userAgent
				+ ", zipCode=" + zipCode + "]";
	}

}
