package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class UserFromSocialRequest extends BaseRequest {

    protected String gigyaId;

    public String getGigyaId() {
        return gigyaId;
    }

    public void setGigyaId(String value) {
        this.gigyaId = value;
    }

}
