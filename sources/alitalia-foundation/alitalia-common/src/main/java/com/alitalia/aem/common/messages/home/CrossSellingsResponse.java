package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.CrossSellingsData;
import com.alitalia.aem.common.messages.BaseResponse;

public class CrossSellingsResponse extends BaseResponse {

	private CrossSellingsData crossSellingsData;

	public CrossSellingsData getCrossSellingsData() {
		return crossSellingsData;
	}

	public void setCrossSellingsData(CrossSellingsData crossSellingData) {
		this.crossSellingsData = crossSellingData;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((crossSellingsData == null) ? 0 : crossSellingsData.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CrossSellingsResponse other = (CrossSellingsResponse) obj;
		if (crossSellingsData == null) {
			if (other.crossSellingsData != null)
				return false;
		} else if (!crossSellingsData.equals(other.crossSellingsData))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CrossSellingsResponse [crossSellingsData=" + crossSellingsData + "]";
	}

}
