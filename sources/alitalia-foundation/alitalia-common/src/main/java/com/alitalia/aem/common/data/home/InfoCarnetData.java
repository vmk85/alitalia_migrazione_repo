package com.alitalia.aem.common.data.home;

import java.math.BigDecimal;
import java.util.Calendar;

public class InfoCarnetData {
	    private String buyerEmail;
	    private String buyerFirstName;
	    private String buyerLastName;
	    private String carnetCode;
	    private String carnetPassword;
	    private Calendar expiryDate;
	    private Short residualRoutes;
	    private BigDecimal totalFare;
	    private Short totalRoutes;
		
	    public String getBuyerEmail() {
			return buyerEmail;
		}
	    
		public void setBuyerEmail(String buyerEmail) {
			this.buyerEmail = buyerEmail;
		}
		
		public String getBuyerFirstName() {
			return buyerFirstName;
		}
		
		public void setBuyerFirstName(String buyerFirstName) {
			this.buyerFirstName = buyerFirstName;
		}
		
		public String getBuyerLastName() {
			return buyerLastName;
		}
		
		public void setBuyerLastName(String buyerLastName) {
			this.buyerLastName = buyerLastName;
		}
		
		public String getCarnetCode() {
			return carnetCode;
		}
		
		public void setCarnetCode(String carnetCode) {
			this.carnetCode = carnetCode;
		}
		
		public String getCarnetPassword() {
			return carnetPassword;
		}
		
		public void setCarnetPassword(String carnetPassword) {
			this.carnetPassword = carnetPassword;
		}
		
		public Calendar getExpiryDate() {
			return expiryDate;
		}
		
		public void setExpiryDate(Calendar expiryDate) {
			this.expiryDate = expiryDate;
		}
		
		public Short getResidualRoutes() {
			return residualRoutes;
		}
		
		public void setResidualRoutes(Short residualRoutes) {
			this.residualRoutes = residualRoutes;
		}
		
		public BigDecimal getTotalFare() {
			return totalFare;
		}
		
		public void setTotalFare(BigDecimal totalFare) {
			this.totalFare = totalFare;
		}
		
		public Short getTotalRoutes() {
			return totalRoutes;
		}
		
		public void setTotalRoutes(Short totalRoutes) {
			this.totalRoutes = totalRoutes;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((buyerEmail == null) ? 0 : buyerEmail.hashCode());
			result = prime
					* result
					+ ((buyerFirstName == null) ? 0 : buyerFirstName.hashCode());
			result = prime * result
					+ ((buyerLastName == null) ? 0 : buyerLastName.hashCode());
			result = prime * result
					+ ((carnetCode == null) ? 0 : carnetCode.hashCode());
			result = prime
					* result
					+ ((carnetPassword == null) ? 0 : carnetPassword.hashCode());
			result = prime * result
					+ ((expiryDate == null) ? 0 : expiryDate.hashCode());
			result = prime
					* result
					+ ((residualRoutes == null) ? 0 : residualRoutes.hashCode());
			result = prime * result
					+ ((totalFare == null) ? 0 : totalFare.hashCode());
			result = prime * result
					+ ((totalRoutes == null) ? 0 : totalRoutes.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			InfoCarnetData other = (InfoCarnetData) obj;
			if (buyerEmail == null) {
				if (other.buyerEmail != null)
					return false;
			} else if (!buyerEmail.equals(other.buyerEmail))
				return false;
			if (buyerFirstName == null) {
				if (other.buyerFirstName != null)
					return false;
			} else if (!buyerFirstName.equals(other.buyerFirstName))
				return false;
			if (buyerLastName == null) {
				if (other.buyerLastName != null)
					return false;
			} else if (!buyerLastName.equals(other.buyerLastName))
				return false;
			if (carnetCode == null) {
				if (other.carnetCode != null)
					return false;
			} else if (!carnetCode.equals(other.carnetCode))
				return false;
			if (carnetPassword == null) {
				if (other.carnetPassword != null)
					return false;
			} else if (!carnetPassword.equals(other.carnetPassword))
				return false;
			if (expiryDate == null) {
				if (other.expiryDate != null)
					return false;
			} else if (!expiryDate.equals(other.expiryDate))
				return false;
			if (residualRoutes == null) {
				if (other.residualRoutes != null)
					return false;
			} else if (!residualRoutes.equals(other.residualRoutes))
				return false;
			if (totalFare == null) {
				if (other.totalFare != null)
					return false;
			} else if (!totalFare.equals(other.totalFare))
				return false;
			if (totalRoutes == null) {
				if (other.totalRoutes != null)
					return false;
			} else if (!totalRoutes.equals(other.totalRoutes))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "InfoCarnetData [buyerEmail=" + buyerEmail
					+ ", buyerFirstName=" + buyerFirstName + ", buyerLastName="
					+ buyerLastName + ", carnetCode=" + carnetCode
					+ ", carnetPassword=" + carnetPassword + ", expiryDate="
					+ expiryDate + ", residualRoutes=" + residualRoutes
					+ ", totalFare=" + totalFare + ", totalRoutes="
					+ totalRoutes + "]";
		}

}
