package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.IPAddressData;
import com.alitalia.aem.common.messages.BaseRequest;

public class RetrieveGeoAirportsRequest extends BaseRequest {

	private String cityCode;
    private IPAddressData ipAddress;
    private String languageCode;
    private String marketCode;
	
    public String getCityCode() {
		return cityCode;
	}
	
    public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	
    public IPAddressData getIpAddress() {
		return ipAddress;
	}
	
    public void setIpAddress(IPAddressData ipAddress) {
		this.ipAddress = ipAddress;
	}
	
    public String getLanguageCode() {
		return languageCode;
	}
	
    public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	
    public String getMarketCode() {
		return marketCode;
	}
	
    public void setMarketCode(String marketCode) {
		this.marketCode = marketCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((cityCode == null) ? 0 : cityCode.hashCode());
		result = prime * result
				+ ((ipAddress == null) ? 0 : ipAddress.hashCode());
		result = prime * result
				+ ((languageCode == null) ? 0 : languageCode.hashCode());
		result = prime * result
				+ ((marketCode == null) ? 0 : marketCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveGeoAirportsRequest other = (RetrieveGeoAirportsRequest) obj;
		if (cityCode == null) {
			if (other.cityCode != null)
				return false;
		} else if (!cityCode.equals(other.cityCode))
			return false;
		if (ipAddress == null) {
			if (other.ipAddress != null)
				return false;
		} else if (!ipAddress.equals(other.ipAddress))
			return false;
		if (languageCode == null) {
			if (other.languageCode != null)
				return false;
		} else if (!languageCode.equals(other.languageCode))
			return false;
		if (marketCode == null) {
			if (other.marketCode != null)
				return false;
		} else if (!marketCode.equals(other.marketCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveGeoAirportsRequest [cityCode=" + cityCode
				+ ", ipAddress=" + ipAddress + ", languageCode=" + languageCode
				+ ", marketCode=" + marketCode + "]";
	}
}