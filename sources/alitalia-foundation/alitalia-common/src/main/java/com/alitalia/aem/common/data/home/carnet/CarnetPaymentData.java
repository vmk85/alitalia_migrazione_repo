package com.alitalia.aem.common.data.home.carnet;

import java.math.BigDecimal;

import com.alitalia.aem.common.data.home.enumerations.CarnetPaymentTypeEnum;

public class CarnetPaymentData {

    private String currency;
    private String description;
    private String email;
    private Boolean enabledNewsLetter;
    private BigDecimal grossAmount;
    private BigDecimal netAmount;
    private CarnetProcessInfoData process;
    private CarnetProviderData<? extends CarnetComunicationData> provider;
    private CarnetPaymentTypeEnum type;
    private String xsltMail;
    
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Boolean getEnabledNewsLetter() {
		return enabledNewsLetter;
	}
	public void setEnabledNewsLetter(Boolean enabledNewsLetter) {
		this.enabledNewsLetter = enabledNewsLetter;
	}
	public BigDecimal getGrossAmount() {
		return grossAmount;
	}
	public void setGrossAmount(BigDecimal grossAmount) {
		this.grossAmount = grossAmount;
	}
	public BigDecimal getNetAmount() {
		return netAmount;
	}
	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}
	public CarnetProcessInfoData getProcess() {
		return process;
	}
	public void setProcess(CarnetProcessInfoData process) {
		this.process = process;
	}
	public CarnetProviderData<?> getProvider() {
		return provider;
	}
	public void setProvider(CarnetProviderData<?> provider) {
		this.provider = provider;
	}
	public CarnetPaymentTypeEnum getType() {
		return type;
	}
	public void setType(CarnetPaymentTypeEnum type) {
		this.type = type;
	}
	public String getXsltMail() {
		return xsltMail;
	}
	public void setXsltMail(String xsltMail) {
		this.xsltMail = xsltMail;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((currency == null) ? 0 : currency.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime
				* result
				+ ((enabledNewsLetter == null) ? 0 : enabledNewsLetter
						.hashCode());
		result = prime * result
				+ ((grossAmount == null) ? 0 : grossAmount.hashCode());
		result = prime * result
				+ ((netAmount == null) ? 0 : netAmount.hashCode());
		result = prime * result + ((process == null) ? 0 : process.hashCode());
		result = prime * result
				+ ((provider == null) ? 0 : provider.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result
				+ ((xsltMail == null) ? 0 : xsltMail.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarnetPaymentData other = (CarnetPaymentData) obj;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (enabledNewsLetter == null) {
			if (other.enabledNewsLetter != null)
				return false;
		} else if (!enabledNewsLetter.equals(other.enabledNewsLetter))
			return false;
		if (grossAmount == null) {
			if (other.grossAmount != null)
				return false;
		} else if (!grossAmount.equals(other.grossAmount))
			return false;
		if (netAmount == null) {
			if (other.netAmount != null)
				return false;
		} else if (!netAmount.equals(other.netAmount))
			return false;
		if (process == null) {
			if (other.process != null)
				return false;
		} else if (!process.equals(other.process))
			return false;
		if (provider == null) {
			if (other.provider != null)
				return false;
		} else if (!provider.equals(other.provider))
			return false;
		if (type != other.type)
			return false;
		if (xsltMail == null) {
			if (other.xsltMail != null)
				return false;
		} else if (!xsltMail.equals(other.xsltMail))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "CarnetPaymentData [currency=" + currency + ", description="
				+ description + ", email=" + email + ", enabledNewsLetter="
				+ enabledNewsLetter + ", grossAmount=" + grossAmount
				+ ", netAmount=" + netAmount + ", process=" + process
				+ ", provider=" + provider + ", type=" + type + ", xsltMail="
				+ xsltMail + "]";
	}
}