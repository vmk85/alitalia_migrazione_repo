package com.alitalia.aem.common.data.home;

public class ResultBookingDetailsSolutionPricingExtPaxData {
	
	private int adultsField;
	private boolean adultsFieldSpecified;
	private int childrenField;
	private boolean childrenFieldSpecified;
	private int infantsInLapField;
	private boolean infantsInLapFieldSpecified;
	private int youthField;
	private boolean youthFieldSpecified;
	private int militaryField;
	private boolean militaryFieldSpecified;
	
	public int getAdultsField() {
		return adultsField;
	}
	
	public void setAdultsField(int adultsField) {
		this.adultsField = adultsField;
	}
	
	public boolean isAdultsFieldSpecified() {
		return adultsFieldSpecified;
	}
	
	public void setAdultsFieldSpecified(boolean adultsFieldSpecified) {
		this.adultsFieldSpecified = adultsFieldSpecified;
	}
	
	public int getChildrenField() {
		return childrenField;
	}
	
	public void setChildrenField(int childrenField) {
		this.childrenField = childrenField;
	}
	
	public boolean isChildrenFieldSpecified() {
		return childrenFieldSpecified;
	}
	
	public void setChildrenFieldSpecified(boolean childrenFieldSpecified) {
		this.childrenFieldSpecified = childrenFieldSpecified;
	}
	
	public int getInfantsInLapField() {
		return infantsInLapField;
	}
	
	public void setInfantsInLapField(int infantsInLapField) {
		this.infantsInLapField = infantsInLapField;
	}
	
	public boolean isInfantsInLapFieldSpecified() {
		return infantsInLapFieldSpecified;
	}
	
	public void setInfantsInLapFieldSpecified(boolean infantsInLapFieldSpecified) {
		this.infantsInLapFieldSpecified = infantsInLapFieldSpecified;
	}
	
	public int getYouthField() {
		return youthField;
	}
	
	public void setYouthField(int youthField) {
		this.youthField = youthField;
	}
	
	public boolean isYouthFieldSpecified() {
		return youthFieldSpecified;
	}
	
	public void setYouthFieldSpecified(boolean youthFieldSpecified) {
		this.youthFieldSpecified = youthFieldSpecified;
	}

	public int getMilitaryField() {
		return militaryField;
	}

	public void setMilitaryField(int militaryField) {
		this.militaryField = militaryField;
	}

	public boolean isMilitaryFieldSpecified() {
		return militaryFieldSpecified;
	}

	public void setMilitaryFieldSpecified(boolean militaryFieldSpecified) {
		this.militaryFieldSpecified = militaryFieldSpecified;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + adultsField;
		result = prime * result + (adultsFieldSpecified ? 1231 : 1237);
		result = prime * result + childrenField;
		result = prime * result + (childrenFieldSpecified ? 1231 : 1237);
		result = prime * result + infantsInLapField;
		result = prime * result + (infantsInLapFieldSpecified ? 1231 : 1237);
		result = prime * result + militaryField;
		result = prime * result + (militaryFieldSpecified ? 1231 : 1237);
		result = prime * result + youthField;
		result = prime * result + (youthFieldSpecified ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingDetailsSolutionPricingExtPaxData other = (ResultBookingDetailsSolutionPricingExtPaxData) obj;
		if (adultsField != other.adultsField)
			return false;
		if (adultsFieldSpecified != other.adultsFieldSpecified)
			return false;
		if (childrenField != other.childrenField)
			return false;
		if (childrenFieldSpecified != other.childrenFieldSpecified)
			return false;
		if (infantsInLapField != other.infantsInLapField)
			return false;
		if (infantsInLapFieldSpecified != other.infantsInLapFieldSpecified)
			return false;
		if (militaryField != other.militaryField)
			return false;
		if (militaryFieldSpecified != other.militaryFieldSpecified)
			return false;
		if (youthField != other.youthField)
			return false;
		if (youthFieldSpecified != other.youthFieldSpecified)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ResultBookingDetailsSolutionPricingExtPaxData [adultsField="
				+ adultsField + ", adultsFieldSpecified="
				+ adultsFieldSpecified + ", childrenField=" + childrenField
				+ ", childrenFieldSpecified=" + childrenFieldSpecified
				+ ", infantsInLapField=" + infantsInLapField
				+ ", infantsInLapFieldSpecified=" + infantsInLapFieldSpecified
				+ ", youthField=" + youthField + ", youthFieldSpecified="
				+ youthFieldSpecified + ", militaryField=" + militaryField
				+ ", militaryFieldSpecified=" + militaryFieldSpecified + "]";
	}
}