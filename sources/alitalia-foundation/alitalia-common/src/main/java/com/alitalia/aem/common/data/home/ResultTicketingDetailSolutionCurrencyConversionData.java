package com.alitalia.aem.common.data.home;

import java.math.BigDecimal;


public class ResultTicketingDetailSolutionCurrencyConversionData {

    private String applicationField;
    private String fromField;
    private BigDecimal rateField;
    private String rateTableField;
    private String toField;

    public String getApplicationField() {
        return applicationField;
    }

    public void setApplicationField(String value) {
        this.applicationField = value;
    }

    public String getFromField() {
        return fromField;
    }

    public void setFromField(String value) {
        this.fromField = value;
    }

    public BigDecimal getRateField() {
        return rateField;
    }

    public void setRateField(BigDecimal value) {
        this.rateField = value;
    }

    public String getRateTableField() {
        return rateTableField;
    }

    public void setRateTableField(String value) {
        this.rateTableField = value;
    }

    public String getToField() {
        return toField;
    }

    public void setToField(String value) {
        this.toField = value;
    }

}
