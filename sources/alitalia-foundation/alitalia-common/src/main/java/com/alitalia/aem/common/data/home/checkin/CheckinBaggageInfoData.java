package com.alitalia.aem.common.data.home.checkin;

import com.alitalia.aem.common.data.home.mmb.MmbBaggageOrderData;

public class CheckinBaggageInfoData extends CheckinBaseTransactionInfoData{

	private String eticket;
	private CheckinFlightData flight;
	private MmbBaggageOrderData order;
	private CheckinWebCheckInInfoData webCheckInInfo;
	
	public String getEticket() {
		return eticket;
	}
	public void setEticket(String eticket) {
		this.eticket = eticket;
	}
	public CheckinFlightData getFlight() {
		return flight;
	}
	public void setFlight(CheckinFlightData flight) {
		this.flight = flight;
	}
	public MmbBaggageOrderData getOrder() {
		return order;
	}
	public void setOrder(MmbBaggageOrderData order) {
		this.order = order;
	}
	public CheckinWebCheckInInfoData getWebCheckInInfo() {
		return webCheckInInfo;
	}
	public void setWebCheckInInfo(CheckinWebCheckInInfoData webCheckInInfo) {
		this.webCheckInInfo = webCheckInInfo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((eticket == null) ? 0 : eticket.hashCode());
		result = prime * result + ((flight == null) ? 0 : flight.hashCode());
		result = prime * result + ((order == null) ? 0 : order.hashCode());
		result = prime * result
				+ ((webCheckInInfo == null) ? 0 : webCheckInInfo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinBaggageInfoData other = (CheckinBaggageInfoData) obj;
		if (eticket == null) {
			if (other.eticket != null)
				return false;
		} else if (!eticket.equals(other.eticket))
			return false;
		if (flight == null) {
			if (other.flight != null)
				return false;
		} else if (!flight.equals(other.flight))
			return false;
		if (order == null) {
			if (other.order != null)
				return false;
		} else if (!order.equals(other.order))
			return false;
		if (webCheckInInfo == null) {
			if (other.webCheckInInfo != null)
				return false;
		} else if (!webCheckInInfo.equals(other.webCheckInInfo))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CheckinBaggageInfoData [eticket=" + eticket + ", flight="
				+ flight + ", order=" + order + ", webCheckInInfo="
				+ webCheckInInfo + ", toString()=" + super.toString() + "]";
	}

}
