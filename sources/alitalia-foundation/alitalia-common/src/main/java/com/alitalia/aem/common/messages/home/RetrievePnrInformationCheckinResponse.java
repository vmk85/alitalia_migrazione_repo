package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.checkin.CheckinRouteData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetrievePnrInformationCheckinResponse extends BaseResponse {

	private String pnr;
	private String eTicket;
	private String lastname;
	private String name;
	private List<CheckinPassengerData> passengersList;
	private List<CheckinRouteData> routesList;
	private boolean busCarrier;
	
	public String getPnr() {
		return pnr;
	}
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	public String geteTicket() {
		return eTicket;
	}
	public void seteTicket(String eTicket) {
		this.eTicket = eTicket;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<CheckinPassengerData> getPassengersList() {
		return passengersList;
	}
	public void setPassengersList(List<CheckinPassengerData> passengersList) {
		this.passengersList = passengersList;
	}
	public List<CheckinRouteData> getRoutesList() {
		return routesList;
	}
	public void setRoutesList(List<CheckinRouteData> routesList) {
		this.routesList = routesList;
	}
	
	public void setBusCarrier(boolean busCarrier) {
		this.busCarrier = busCarrier;
	}
	
	public boolean isBusCarrier() {
		return busCarrier;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((eTicket == null) ? 0 : eTicket.hashCode());
		result = prime * result
				+ ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((passengersList == null) ? 0 : passengersList.hashCode());
		result = prime * result + ((pnr == null) ? 0 : pnr.hashCode());
		result = prime * result
				+ ((routesList == null) ? 0 : routesList.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrievePnrInformationCheckinResponse other = (RetrievePnrInformationCheckinResponse) obj;
		if (eTicket == null) {
			if (other.eTicket != null)
				return false;
		} else if (!eTicket.equals(other.eTicket))
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (passengersList == null) {
			if (other.passengersList != null)
				return false;
		} else if (!passengersList.equals(other.passengersList))
			return false;
		if (pnr == null) {
			if (other.pnr != null)
				return false;
		} else if (!pnr.equals(other.pnr))
			return false;
		if (routesList == null) {
			if (other.routesList != null)
				return false;
		} else if (!routesList.equals(other.routesList))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CheckinRetrievePnrInformationResponse [pnr=" + pnr
				+ ", eTicket=" + eTicket + ", lastname=" + lastname + ", name="
				+ name + ", passengersList=" + passengersList + ", routesList="
				+ routesList + ", getPnr()=" + getPnr() + ", geteTicket()="
				+ geteTicket() + ", getLastname()=" + getLastname()
				+ ", getName()=" + getName() + ", getPassengersList()="
				+ getPassengersList() + ", getRoutesList()=" + getRoutesList()
				+ ", hashCode()=" + hashCode() + "]";
	}
}
