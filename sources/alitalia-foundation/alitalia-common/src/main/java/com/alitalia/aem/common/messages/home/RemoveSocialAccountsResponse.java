package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.SocialAccountData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RemoveSocialAccountsResponse extends BaseResponse {

	private boolean succeeded;
	private List<SocialAccountData> socialAccounts;

	public boolean isSucceeded() {
		return succeeded;
	}

	public void setSucceeded(boolean succeeded) {
		this.succeeded = succeeded;
	}

	public List<SocialAccountData> getSocialAccounts() {
		return socialAccounts;
	}

	public void setSocialAccounts(List<SocialAccountData> socialAccounts) {
		this.socialAccounts = socialAccounts;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((socialAccounts == null) ? 0 : socialAccounts.hashCode());
		result = prime * result + (succeeded ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RemoveSocialAccountsResponse other = (RemoveSocialAccountsResponse) obj;
		if (socialAccounts == null) {
			if (other.socialAccounts != null)
				return false;
		} else if (!socialAccounts.equals(other.socialAccounts))
			return false;
		if (succeeded != other.succeeded)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RemoveSocialAccountsResponse [succeeded=" + succeeded
				+ ", socialAccounts=" + socialAccounts + "]";
	}
}