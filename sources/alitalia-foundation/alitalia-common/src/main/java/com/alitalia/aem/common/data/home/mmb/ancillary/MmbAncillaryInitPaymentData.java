package com.alitalia.aem.common.data.home.mmb.ancillary;

import java.util.Map;

public class MmbAncillaryInitPaymentData {

	private Map<String, String> htmlForm;
	private String requestData;
	private String requestMethod;
	private String sessionID;
	private String transactionID;
	private String url;

	public Map<String, String> getHtmlForm() {
		return htmlForm;
	}

	public void setHtmlForm(Map<String, String> htmlForm) {
		this.htmlForm = htmlForm;
	}

	public String getRequestData() {
		return requestData;
	}

	public void setRequestData(String requestData) {
		this.requestData = requestData;
	}

	public String getRequestMethod() {
		return requestMethod;
	}

	public void setRequestMethod(String requestMethod) {
		this.requestMethod = requestMethod;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public String getTransactionID() {
		return transactionID;
	}

	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((sessionID == null) ? 0 : sessionID.hashCode());
		result = prime * result
				+ ((transactionID == null) ? 0 : transactionID.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbAncillaryInitPaymentData other = (MmbAncillaryInitPaymentData) obj;
		if (sessionID == null) {
			if (other.sessionID != null)
				return false;
		} else if (!sessionID.equals(other.sessionID))
			return false;
		if (transactionID == null) {
			if (other.transactionID != null)
				return false;
		} else if (!transactionID.equals(other.transactionID))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbInitPaymentData [htmlForm=" + htmlForm + ", requestData="
				+ requestData + ", requestMethod=" + requestMethod
				+ ", sessionID=" + sessionID + ", transactionID="
				+ transactionID + ", url=" + url + "]";
	}

}
