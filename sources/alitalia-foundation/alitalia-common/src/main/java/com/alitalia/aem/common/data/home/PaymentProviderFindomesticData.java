package com.alitalia.aem.common.data.home;


public class PaymentProviderFindomesticData extends PaymentProviderData {
	
	private UserInfoBaseData userInfo;
	
	public UserInfoBaseData getUserInfo() {
		return userInfo;
	}
	
	public void setUserInfo(UserInfoBaseData userInfo) {
		this.userInfo = userInfo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((userInfo == null) ? 0 : userInfo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentProviderFindomesticData other = (PaymentProviderFindomesticData) obj;
		if (userInfo == null) {
			if (other.userInfo != null)
				return false;
		} else if (!userInfo.equals(other.userInfo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PaymentProviderFindomesticData [userInfo=" + userInfo + "]";
	}
	
}
