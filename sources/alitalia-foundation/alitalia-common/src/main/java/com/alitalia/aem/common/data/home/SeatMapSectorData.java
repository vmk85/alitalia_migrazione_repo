package com.alitalia.aem.common.data.home;

import java.util.List;

public class SeatMapSectorData {

	private String startingRow;
	private String endingRow;
    private List<SeatMapRowData> seatMapRows;
	
    public String getStartingRow() {
		return startingRow;
	}
	
    public void setStartingRow(String startingRow) {
		this.startingRow = startingRow;
	}
	
    public String getEndingRow() {
		return endingRow;
	}
	
    public void setEndingRow(String endingRow) {
		this.endingRow = endingRow;
	}
	
    public List<SeatMapRowData> getSeatMapRows() {
		return seatMapRows;
	}
	
    public void setSeatMapRows(List<SeatMapRowData> seatMapRows) {
		this.seatMapRows = seatMapRows;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((endingRow == null) ? 0 : endingRow.hashCode());
		result = prime * result
				+ ((seatMapRows == null) ? 0 : seatMapRows.hashCode());
		result = prime * result
				+ ((startingRow == null) ? 0 : startingRow.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SeatMapSectorData other = (SeatMapSectorData) obj;
		if (endingRow == null) {
			if (other.endingRow != null)
				return false;
		} else if (!endingRow.equals(other.endingRow))
			return false;
		if (seatMapRows == null) {
			if (other.seatMapRows != null)
				return false;
		} else if (!seatMapRows.equals(other.seatMapRows))
			return false;
		if (startingRow == null) {
			if (other.startingRow != null)
				return false;
		} else if (!startingRow.equals(other.startingRow))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SeatMapSectorData [startingRow=" + startingRow + ", endingRow="
				+ endingRow + ", seatMapRows=" + seatMapRows + "]";
	}
}