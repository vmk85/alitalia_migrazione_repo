package com.alitalia.aem.common.data.home.enumerations;

public enum LegTypeEnum {

	NONE("None"),
	DOM("DOM"),
	INZ("INZ"),
	INC("INC");
    
    private final String value;

    LegTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LegTypeEnum fromValue(String v) {
        for (LegTypeEnum c: LegTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
