package com.alitalia.aem.common.data.home.enumerations;

public enum MMMaritalStatusTypeEnum {

    UNKNOWN("UnKnown"),
    MARRIED("Married"),
    SINGLE("Single");

    private final String value;
    
    MMMaritalStatusTypeEnum(String v) {
		value = v;
	}
	
	public String value() {
		return value;
	}

	public static MMMaritalStatusTypeEnum fromValue(String v) {
		for (MMMaritalStatusTypeEnum c: MMMaritalStatusTypeEnum.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
