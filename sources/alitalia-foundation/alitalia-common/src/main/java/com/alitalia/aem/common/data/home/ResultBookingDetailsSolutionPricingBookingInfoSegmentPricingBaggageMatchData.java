package com.alitalia.aem.common.data.home;

public class ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatchData {
	
	private String commercialNameField;
	private int countField;
	private boolean countFieldSpecified;
	private String subcodeField;
	
	public String getCommercialNameField() {
		return commercialNameField;
	}
	
	public void setCommercialNameField(String commercialNameField) {
		this.commercialNameField = commercialNameField;
	}
	
	public int getCountField() {
		return countField;
	}
	
	public void setCountField(int countField) {
		this.countField = countField;
	}
	
	public boolean isCountFieldSpecified() {
		return countFieldSpecified;
	}
	
	public void setCountFieldSpecified(boolean countFieldSpecified) {
		this.countFieldSpecified = countFieldSpecified;
	}
	
	public String getSubcodeField() {
		return subcodeField;
	}
	
	public void setSubcodeField(String subcodeField) {
		this.subcodeField = subcodeField;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((commercialNameField == null) ? 0 : commercialNameField
						.hashCode());
		result = prime * result + countField;
		result = prime * result + (countFieldSpecified ? 1231 : 1237);
		result = prime * result
				+ ((subcodeField == null) ? 0 : subcodeField.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatchData other = (ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatchData) obj;
		if (commercialNameField == null) {
			if (other.commercialNameField != null)
				return false;
		} else if (!commercialNameField.equals(other.commercialNameField))
			return false;
		if (countField != other.countField)
			return false;
		if (countFieldSpecified != other.countFieldSpecified)
			return false;
		if (subcodeField == null) {
			if (other.subcodeField != null)
				return false;
		} else if (!subcodeField.equals(other.subcodeField))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatch [commercialNameField="
				+ commercialNameField
				+ ", countField="
				+ countField
				+ ", countFieldSpecified="
				+ countFieldSpecified
				+ ", subcodeField=" + subcodeField + "]";
	}
	
}
