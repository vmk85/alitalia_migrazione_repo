package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.messages.BaseResponse;

public class MmbAncillarySendEmailResponse extends BaseResponse {

	private String machineName;
    private List<String> errors;
    private Boolean successful;
	
	public String getMachineName() {
		return machineName;
	}

	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	public Boolean getSuccessful() {
		return successful;
	}

	public void setSuccessful(Boolean successful) {
		this.successful = successful;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((errors == null) ? 0 : errors.hashCode());
		result = prime * result
				+ ((machineName == null) ? 0 : machineName.hashCode());
		result = prime * result
				+ ((successful == null) ? 0 : successful.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbAncillarySendEmailResponse other = (MmbAncillarySendEmailResponse) obj;
		if (errors == null) {
			if (other.errors != null)
				return false;
		} else if (!errors.equals(other.errors))
			return false;
		if (machineName == null) {
			if (other.machineName != null)
				return false;
		} else if (!machineName.equals(other.machineName))
			return false;
		if (successful == null) {
			if (other.successful != null)
				return false;
		} else if (!successful.equals(other.successful))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbAncillarySendEmailResponse [machineName=" + machineName
				+ ", errors=" + errors + ", successful=" + successful
				+ ", getTid()=" + getTid() + ", getSid()=" + getSid() + "]";
	}

}