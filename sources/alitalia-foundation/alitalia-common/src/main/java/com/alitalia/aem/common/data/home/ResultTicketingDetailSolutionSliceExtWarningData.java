package com.alitalia.aem.common.data.home;

public class ResultTicketingDetailSolutionSliceExtWarningData {

    private Boolean changeOfAirportField;
    private Boolean changeOfAirportFieldSpecified;
    private Boolean changeOfTerminalField;
    private Boolean changeOfTerminalFieldSpecified;
    private Boolean longLayoverField;
    private Boolean longLayoverFieldSpecified;
    private Boolean nonPreferredCabinField;
    private Boolean nonPreferredCabinFieldSpecified;
    private Boolean overnightField;
    private Boolean overnightFieldSpecified;
    private Boolean riskyConnectionField;
    private Boolean riskyConnectionFieldSpecified;
    private Boolean tightConnectionField;
    private Boolean tightConnectionFieldSpecified;

    public Boolean isChangeOfAirportField() {
        return changeOfAirportField;
    }

    public void setChangeOfAirportField(Boolean value) {
        this.changeOfAirportField = value;
    }

    public Boolean isChangeOfAirportFieldSpecified() {
        return changeOfAirportFieldSpecified;
    }

    public void setChangeOfAirportFieldSpecified(Boolean value) {
        this.changeOfAirportFieldSpecified = value;
    }

    public Boolean isChangeOfTerminalField() {
        return changeOfTerminalField;
    }

    public void setChangeOfTerminalField(Boolean value) {
        this.changeOfTerminalField = value;
    }

    public Boolean isChangeOfTerminalFieldSpecified() {
        return changeOfTerminalFieldSpecified;
    }

    public void setChangeOfTerminalFieldSpecified(Boolean value) {
        this.changeOfTerminalFieldSpecified = value;
    }

    public Boolean isLongLayoverField() {
        return longLayoverField;
    }

    public void setLongLayoverField(Boolean value) {
        this.longLayoverField = value;
    }

    public Boolean isLongLayoverFieldSpecified() {
        return longLayoverFieldSpecified;
    }

    public void setLongLayoverFieldSpecified(Boolean value) {
        this.longLayoverFieldSpecified = value;
    }

    public Boolean isNonPreferredCabinField() {
        return nonPreferredCabinField;
    }

    public void setNonPreferredCabinField(Boolean value) {
        this.nonPreferredCabinField = value;
    }

    public Boolean isNonPreferredCabinFieldSpecified() {
        return nonPreferredCabinFieldSpecified;
    }

    public void setNonPreferredCabinFieldSpecified(Boolean value) {
        this.nonPreferredCabinFieldSpecified = value;
    }

    public Boolean isOvernightField() {
        return overnightField;
    }

    public void setOvernightField(Boolean value) {
        this.overnightField = value;
    }

    public Boolean isOvernightFieldSpecified() {
        return overnightFieldSpecified;
    }

    public void setOvernightFieldSpecified(Boolean value) {
        this.overnightFieldSpecified = value;
    }

    public Boolean isRiskyConnectionField() {
        return riskyConnectionField;
    }

    public void setRiskyConnectionField(Boolean value) {
        this.riskyConnectionField = value;
    }

    public Boolean isRiskyConnectionFieldSpecified() {
        return riskyConnectionFieldSpecified;
    }

    public void setRiskyConnectionFieldSpecified(Boolean value) {
        this.riskyConnectionFieldSpecified = value;
    }

    public Boolean isTightConnectionField() {
        return tightConnectionField;
    }

    public void setTightConnectionField(Boolean value) {
        this.tightConnectionField = value;
    }

    public Boolean isTightConnectionFieldSpecified() {
        return tightConnectionFieldSpecified;
    }

    public void setTightConnectionFieldSpecified(Boolean value) {
        this.tightConnectionFieldSpecified = value;
    }

}
