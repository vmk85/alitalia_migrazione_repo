package com.alitalia.aem.common.data.home.enumerations;

public enum MMActivityTypeEnum {

    NOT_SPECIFIED("NotSpecified"),
    CAR_RENTAL("CarRental"),
    HOTEL("Hotel"),
    PROMOTIONAL("Promotional"),
    FINANCIAL("Financial"),
    FLIGHT("Flight"),
    CATALOGUE("Catalogue"),
    CDS("Cds"),
    CONVERSION("Conversion");

    private final String value;
    
    MMActivityTypeEnum(String v) {
		value = v;
	}
	
	public String value() {
		return value;
	}

	public static MMActivityTypeEnum fromValue(String v) {
		for (MMActivityTypeEnum c: MMActivityTypeEnum.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
