package com.alitalia.aem.common.data.home;

import java.util.List;

public class ResultTicketingDetailSolutionPricingFareCalculationData {

    private List<ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxData> boxedTaxField;
    private List<String> lineField;

    public List<ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxData> getBoxedTaxField() {
        return boxedTaxField;
    }

    public void setBoxedTaxField(List<ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxData> value) {
        this.boxedTaxField = value;
    }

    public List<String> getLineField() {
        return lineField;
    }

    public void setLineField(List<String> value) {
        this.lineField = value;
    }

}
