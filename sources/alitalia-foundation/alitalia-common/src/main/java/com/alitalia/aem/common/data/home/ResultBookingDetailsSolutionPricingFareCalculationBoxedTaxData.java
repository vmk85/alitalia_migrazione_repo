package com.alitalia.aem.common.data.home;

public class ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData {
	
	private String priceField;
	private String taxCodeField;
	
	public String getPriceField() {
		return priceField;
	}
	
	public void setPriceField(String priceField) {
		this.priceField = priceField;
	}
	
	public String getTaxCodeField() {
		return taxCodeField;
	}
	
	public void setTaxCodeField(String taxCodeField) {
		this.taxCodeField = taxCodeField;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((priceField == null) ? 0 : priceField.hashCode());
		result = prime * result
				+ ((taxCodeField == null) ? 0 : taxCodeField.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData other = (ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData) obj;
		if (priceField == null) {
			if (other.priceField != null)
				return false;
		} else if (!priceField.equals(other.priceField))
			return false;
		if (taxCodeField == null) {
			if (other.taxCodeField != null)
				return false;
		} else if (!taxCodeField.equals(other.taxCodeField))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData [priceField="
				+ priceField + ", taxCodeField=" + taxCodeField + "]";
	}
	
}
