package com.alitalia.aem.common.data.home;

public class ResultTicketingDetailSolutionSliceSegmentLegExtData {

    protected ResultTicketingDetailSolutionSliceSegmentLegExtWarningData warningField;

    public ResultTicketingDetailSolutionSliceSegmentLegExtWarningData getWarningField() {
        return warningField;
    }

    public void setWarningField(ResultTicketingDetailSolutionSliceSegmentLegExtWarningData value) {
        this.warningField = value;
    }

}
