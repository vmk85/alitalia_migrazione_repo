package com.alitalia.aem.common.data.home.enumerations;

public enum CheckinSearchReasonEnum {

	CHECK_IN("CheckIn"),
	UNDO_CHECK_IN("UndoCheckIn"),
	PRINT_BOARDING_PASS("PrintBoardingPass"),
	CHANGE_SEAT("ChangeSeat");
    
    private final String value;

    CheckinSearchReasonEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CheckinSearchReasonEnum fromValue(String v) {
        for (CheckinSearchReasonEnum c: CheckinSearchReasonEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
