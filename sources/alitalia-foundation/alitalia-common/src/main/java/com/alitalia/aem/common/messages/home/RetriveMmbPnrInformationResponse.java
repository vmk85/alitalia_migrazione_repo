package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.mmb.MmbPassengerData;
import com.alitalia.aem.common.data.home.mmb.MmbRouteData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetriveMmbPnrInformationResponse extends BaseResponse {

	private String pnr;
	private String eTicket;
	private String lastname;
	private String name;
	private List<MmbPassengerData> passengersList;
	private List<MmbRouteData> routesList;
	private boolean busCarrier;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String geteTicket() {
		return eTicket;
	}

	public void seteTicket(String eTicket) {
		this.eTicket = eTicket;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<MmbPassengerData> getPassengersList() {
		return passengersList;
	}

	public void setPassengersList(List<MmbPassengerData> passengersList) {
		this.passengersList = passengersList;
	}

	public List<MmbRouteData> getRoutesList() {
		return routesList;
	}

	public void setRoutesList(List<MmbRouteData> routesList) {
		this.routesList = routesList;
	}
	
	public void setBusCarrier(boolean busCarrier) {
		this.busCarrier = busCarrier;
	}
	
	public boolean isBusCarrier() {
		return busCarrier;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((pnr == null) ? 0 : pnr.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetriveMmbPnrInformationResponse other = (RetriveMmbPnrInformationResponse) obj;
		if (pnr == null) {
			if (other.pnr != null)
				return false;
		} else if (!pnr.equals(other.pnr))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetriveMmbPnrInformationResponse [pnr=" + pnr + ", eTicket="
				+ eTicket + ", lastname=" + lastname + ", name=" + name
				+ ", passengersList=" + passengersList + ", routesList="
				+ routesList + ", getTid()=" + getTid() + ", getSid()="
				+ getSid() + "]";
	}

}
