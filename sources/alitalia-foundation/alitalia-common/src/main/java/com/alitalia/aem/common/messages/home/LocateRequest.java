package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class LocateRequest extends BaseRequest {

	private String flxpar1;
	private String flxpar2;
	private String flxpar3;
	private String wpuser;
	private String wppasw;
	private String cdxipa;
	
	public String getFlxpar1() {
		return flxpar1;
	}
	
	public void setFlxpar1(String flxpar1) {
		this.flxpar1 = flxpar1;
	}
	
	public String getFlxpar2() {
		return flxpar2;
	}
	
	public void setFlxpar2(String flxpar2) {
		this.flxpar2 = flxpar2;
	}
	
	public String getFlxpar3() {
		return flxpar3;
	}
	
	public void setFlxpar3(String flxpar3) {
		this.flxpar3 = flxpar3;
	}
	
	public String getWpuser() {
		return wpuser;
	}
	
	public void setWpuser(String wpuser) {
		this.wpuser = wpuser;
	}
	
	public String getWppasw() {
		return wppasw;
	}
	
	public void setWppasw(String wppasw) {
		this.wppasw = wppasw;
	}
	
	public String getCdxipa() {
		return cdxipa;
	}
	
	public void setCdxipa(String cdxipa) {
		this.cdxipa = cdxipa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((cdxipa == null) ? 0 : cdxipa.hashCode());
		result = prime * result + ((flxpar1 == null) ? 0 : flxpar1.hashCode());
		result = prime * result + ((flxpar2 == null) ? 0 : flxpar2.hashCode());
		result = prime * result + ((flxpar3 == null) ? 0 : flxpar3.hashCode());
		result = prime * result + ((wppasw == null) ? 0 : wppasw.hashCode());
		result = prime * result + ((wpuser == null) ? 0 : wpuser.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		LocateRequest other = (LocateRequest) obj;
		if (cdxipa == null) {
			if (other.cdxipa != null)
				return false;
		} else if (!cdxipa.equals(other.cdxipa))
			return false;
		if (flxpar1 == null) {
			if (other.flxpar1 != null)
				return false;
		} else if (!flxpar1.equals(other.flxpar1))
			return false;
		if (flxpar2 == null) {
			if (other.flxpar2 != null)
				return false;
		} else if (!flxpar2.equals(other.flxpar2))
			return false;
		if (flxpar3 == null) {
			if (other.flxpar3 != null)
				return false;
		} else if (!flxpar3.equals(other.flxpar3))
			return false;
		if (wppasw == null) {
			if (other.wppasw != null)
				return false;
		} else if (!wppasw.equals(other.wppasw))
			return false;
		if (wpuser == null) {
			if (other.wpuser != null)
				return false;
		} else if (!wpuser.equals(other.wpuser))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "LocateRequest [flxpar1=" + flxpar1 + ", flxpar2=" + flxpar2
				+ ", flxpar3=" + flxpar3 + ", wpuser=" + wpuser + ", wppasw="
				+ wppasw + ", cdxipa=" + cdxipa + "]";
	}
}