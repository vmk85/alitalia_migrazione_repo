package com.alitalia.aem.common.data;

import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryShoppingCartServiceErrorEnum;

public abstract class MmbAncillaryErrorData {

	private String code;
	private String description;
	protected MmbAncillaryShoppingCartServiceErrorEnum type;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public MmbAncillaryShoppingCartServiceErrorEnum getType() {
		return type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbAncillaryErrorData other = (MmbAncillaryErrorData) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbAncillaryErrorData [code=" + code + ", description="
				+ description + ", type=" + type + "]";
	}

}
