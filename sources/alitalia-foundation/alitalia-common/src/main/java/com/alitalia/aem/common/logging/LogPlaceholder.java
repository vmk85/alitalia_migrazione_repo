package com.alitalia.aem.common.logging;

public enum LogPlaceholder {

	TID("TID"),
	SID("SID");
	
	private String value;
	
	private LogPlaceholder(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}