package com.alitalia.aem.common.data.home.enumerations;

public enum AreaValueEnum {

    DOM,
    INTZ,
    INTC;

    public String value() {
        return name();
    }

    public static AreaValueEnum fromValue(String v) {
        return valueOf(v);
    }

}
