package com.alitalia.aem.common.data.home;

import java.util.List;

public class ResultBookingDetailsSolutionPricingBookingInfoSegmentData {
	
	private ResultBookingDetailsSolutionPricingBookingInfoSegmentDepartureData departureField;
	private String destinationField;
	private ResultBookingDetailsSolutionPricingBookingInfoSegmentFlightData flightField;
	private String originField;
	private List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData> pricingField;
	
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentDepartureData getDepartureField() {
		return departureField;
	}
	
	public void setDepartureField(
			ResultBookingDetailsSolutionPricingBookingInfoSegmentDepartureData departureField) {
		this.departureField = departureField;
	}
	
	public String getDestinationField() {
		return destinationField;
	}
	
	public void setDestinationField(String destinationField) {
		this.destinationField = destinationField;
	}
	
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentFlightData getFlightField() {
		return flightField;
	}
	
	public void setFlightField(
			ResultBookingDetailsSolutionPricingBookingInfoSegmentFlightData flightField) {
		this.flightField = flightField;
	}
	
	public String getOriginField() {
		return originField;
	}
	
	public void setOriginField(String originField) {
		this.originField = originField;
	}
	
	public List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData> getPricingField() {
		return pricingField;
	}
	
	public void setPricingField(
			List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData> pricingField) {
		this.pricingField = pricingField;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((departureField == null) ? 0 : departureField.hashCode());
		result = prime
				* result
				+ ((destinationField == null) ? 0 : destinationField.hashCode());
		result = prime * result
				+ ((flightField == null) ? 0 : flightField.hashCode());
		result = prime * result
				+ ((originField == null) ? 0 : originField.hashCode());
		result = prime * result
				+ ((pricingField == null) ? 0 : pricingField.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingDetailsSolutionPricingBookingInfoSegmentData other = (ResultBookingDetailsSolutionPricingBookingInfoSegmentData) obj;
		if (departureField == null) {
			if (other.departureField != null)
				return false;
		} else if (!departureField.equals(other.departureField))
			return false;
		if (destinationField == null) {
			if (other.destinationField != null)
				return false;
		} else if (!destinationField.equals(other.destinationField))
			return false;
		if (flightField == null) {
			if (other.flightField != null)
				return false;
		} else if (!flightField.equals(other.flightField))
			return false;
		if (originField == null) {
			if (other.originField != null)
				return false;
		} else if (!originField.equals(other.originField))
			return false;
		if (pricingField == null) {
			if (other.pricingField != null)
				return false;
		} else if (!pricingField.equals(other.pricingField))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingDetailsSolutionPricingBookingInfoSegmentData [departureField="
				+ departureField
				+ ", destinationField="
				+ destinationField
				+ ", flightField="
				+ flightField
				+ ", originField="
				+ originField + ", pricingField=" + pricingField + "]";
	}
	
}
