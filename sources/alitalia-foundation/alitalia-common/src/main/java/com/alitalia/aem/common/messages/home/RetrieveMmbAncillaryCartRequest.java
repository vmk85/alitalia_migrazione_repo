package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.MmbBaseRequest;

public class RetrieveMmbAncillaryCartRequest extends MmbBaseRequest {

	private String sessionId;
	private String machineName;
	private String pnr;
	private Long cartId;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getMachineName() {
		return machineName;
	}

	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Long getCartId() {
		return cartId;
	}

	public void setCartId(Long cartId) {
		this.cartId = cartId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((cartId == null) ? 0 : cartId.hashCode());
		result = prime * result
				+ ((machineName == null) ? 0 : machineName.hashCode());
		result = prime * result + ((pnr == null) ? 0 : pnr.hashCode());
		result = prime * result
				+ ((sessionId == null) ? 0 : sessionId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveMmbAncillaryCartRequest other = (RetrieveMmbAncillaryCartRequest) obj;
		if (cartId == null) {
			if (other.cartId != null)
				return false;
		} else if (!cartId.equals(other.cartId))
			return false;
		if (machineName == null) {
			if (other.machineName != null)
				return false;
		} else if (!machineName.equals(other.machineName))
			return false;
		if (pnr == null) {
			if (other.pnr != null)
				return false;
		} else if (!pnr.equals(other.pnr))
			return false;
		if (sessionId == null) {
			if (other.sessionId != null)
				return false;
		} else if (!sessionId.equals(other.sessionId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveMmbAncillaryCartRequest [sessionId=" + sessionId
				+ ", machineName=" + machineName + ", pnr=" + pnr + ", cartId="
				+ cartId + ", getBaseInfo()=" + getBaseInfo()
				+ ", getClient()=" + getClient() + ", getTid()=" + getTid()
				+ ", getSid()=" + getSid() + "]";
	}

}
