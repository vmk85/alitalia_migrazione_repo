package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.messages.BaseResponse;

public class InfoPassengerSubmitPassengerResponse extends BaseResponse {

	private List<AdultPassengerData> adultPassengersWithWrongFrequentFlyer;

	public List<AdultPassengerData> getAdultPassengersWithWrongFrequentFlyer() {
		return adultPassengersWithWrongFrequentFlyer;
	}

	public void setAdultPassengersWithWrongFrequentFlyer(
			List<AdultPassengerData> adultPassengersWithWrongFrequentFlyer) {
		this.adultPassengersWithWrongFrequentFlyer = adultPassengersWithWrongFrequentFlyer;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((adultPassengersWithWrongFrequentFlyer == null) ? 0
						: adultPassengersWithWrongFrequentFlyer.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		InfoPassengerSubmitPassengerResponse other = (InfoPassengerSubmitPassengerResponse) obj;
		if (adultPassengersWithWrongFrequentFlyer == null) {
			if (other.adultPassengersWithWrongFrequentFlyer != null)
				return false;
		} else if (!adultPassengersWithWrongFrequentFlyer
				.equals(other.adultPassengersWithWrongFrequentFlyer))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "InfoPassengerSubmitPassengerResponse [adultPassengersWithWrongFrequentFlyer="
				+ adultPassengersWithWrongFrequentFlyer + "]";
	}
}