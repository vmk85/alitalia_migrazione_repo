package com.alitalia.aem.common.data.home.enumerations;


public enum RefreshTypeEnum {

    REFRESH_SEARCH("RefreshSearch"),
    BOOKING_SELLUP_REFRESH("BookingSellupRefresh"),
    SELLUP_REFRESH_PRICE("SellupRefreshPrice");
    private final String value;

    RefreshTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RefreshTypeEnum fromValue(String v) {
        for (RefreshTypeEnum c: RefreshTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
