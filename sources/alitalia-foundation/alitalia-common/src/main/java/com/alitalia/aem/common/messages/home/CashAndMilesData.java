package com.alitalia.aem.common.messages.home;

import java.math.BigDecimal;
import java.util.Calendar;

import com.alitalia.aem.common.data.home.MmCustomerData;

public class CashAndMilesData {
	
	private BigDecimal discountAmount;
	private MmCustomerData mmCustomer;
	private Calendar paymentDate;
	private Integer usedMileage;
	
	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}
	
	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}
	
	public MmCustomerData getMmCustomer() {
		return mmCustomer;
	}
	
	public void setMmCustomer(MmCustomerData mmCustomer) {
		this.mmCustomer = mmCustomer;
	}
	
	public Calendar getPaymentDate() {
		return paymentDate;
	}
	
	public void setPaymentDate(Calendar paymentDate) {
		this.paymentDate = paymentDate;
	}
	
	public Integer getUsedMileage() {
		return usedMileage;
	}
	
	public void setUsedMileage(Integer usedMileage) {
		this.usedMileage = usedMileage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((discountAmount == null) ? 0 : discountAmount.hashCode());
		result = prime * result
				+ ((mmCustomer == null) ? 0 : mmCustomer.hashCode());
		result = prime * result
				+ ((paymentDate == null) ? 0 : paymentDate.hashCode());
		result = prime * result
				+ ((usedMileage == null) ? 0 : usedMileage.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CashAndMilesData other = (CashAndMilesData) obj;
		if (discountAmount == null) {
			if (other.discountAmount != null)
				return false;
		} else if (!discountAmount.equals(other.discountAmount))
			return false;
		if (mmCustomer == null) {
			if (other.mmCustomer != null)
				return false;
		} else if (!mmCustomer.equals(other.mmCustomer))
			return false;
		if (paymentDate == null) {
			if (other.paymentDate != null)
				return false;
		} else if (!paymentDate.equals(other.paymentDate))
			return false;
		if (usedMileage == null) {
			if (other.usedMileage != null)
				return false;
		} else if (!usedMileage.equals(other.usedMileage))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CashAndMilesData [discountAmount=" + discountAmount
				+ ", mmCustomer=" + mmCustomer + ", paymentDate=" + paymentDate
				+ ", usedMileage=" + usedMileage + "]";
	}
	
}
