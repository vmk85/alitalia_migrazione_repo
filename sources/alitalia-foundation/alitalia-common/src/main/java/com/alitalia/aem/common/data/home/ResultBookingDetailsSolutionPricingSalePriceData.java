package com.alitalia.aem.common.data.home;

import java.math.BigDecimal;

public class ResultBookingDetailsSolutionPricingSalePriceData {
	
	private BigDecimal amountField;
	private String currencyField;
	
	public BigDecimal getAmountField() {
		return amountField;
	}
	
	public void setAmountField(BigDecimal amountField) {
		this.amountField = amountField;
	}
	
	public String getCurrencyField() {
		return currencyField;
	}
	
	public void setCurrencyField(String currencyField) {
		this.currencyField = currencyField;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((currencyField == null) ? 0 : currencyField.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingDetailsSolutionPricingSalePriceData other = (ResultBookingDetailsSolutionPricingSalePriceData) obj;
		if (currencyField == null) {
			if (other.currencyField != null)
				return false;
		} else if (!currencyField.equals(other.currencyField))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingDetailsSolutionPricingSalePriceData [currencyField="
				+ currencyField + "]";
	}
	
}
