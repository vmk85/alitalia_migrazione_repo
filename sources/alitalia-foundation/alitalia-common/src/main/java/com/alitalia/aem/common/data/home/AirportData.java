package com.alitalia.aem.common.data.home;

import java.io.Serializable;
import java.util.Calendar;

import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;

public class AirportData implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Boolean azDeserves;
	private AreaValueEnum area;
	private String city;
	private String cityCode;
	private String cityUrl;
	private String code;
	private String country;
	private String countryCode;
	private String description;
	private AirportDetailsData details;
	private Boolean eligibility;
	private Calendar etktAvlblFrom;
	private Boolean hiddenInFirstDeparture;
	private Integer idMsg;
	private String name;
	private String state;
	private String stateCode;
	private Calendar unavlblFrom;
	private Calendar unavlblUntil;

	public AirportData() {
		super();
	}

	public AirportData(AirportData clone) {
		super();
		this.azDeserves = clone.azDeserves;
		this.area = clone.area;
		this.city = clone.city;
		this.cityCode = clone.cityCode;
		this.cityUrl = clone.cityUrl;
		this.code = clone.code;
		this.country = clone.country;
		this.countryCode = clone.countryCode;
		this.description = clone.description;
		this.eligibility = clone.eligibility;
		this.etktAvlblFrom = clone.etktAvlblFrom;
		this.hiddenInFirstDeparture = clone.hiddenInFirstDeparture;
		this.idMsg = clone.idMsg;
		this.name = clone.name;
		this.state = clone.state;
		this.stateCode = clone.stateCode;
		this.unavlblFrom = clone.unavlblFrom;
		this.unavlblUntil = clone.unavlblUntil;
		AirportDetailsData cloneAirportDetails = clone.getDetails();
		if (cloneAirportDetails != null)
			this.details = new AirportDetailsData(cloneAirportDetails);
	}

	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getCountryCode() {
		return countryCode;
	}
	
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Boolean getAzDeserves() {
		return azDeserves;
	}

	public void setAzDeserves(Boolean azDeserves) {
		this.azDeserves = azDeserves;
	}

	public AreaValueEnum getArea() {
		return area;
	}

	public void setArea(AreaValueEnum area) {
		this.area = area;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityUrl() {
		return cityUrl;
	}

	public void setCityUrl(String cityUrl) {
		this.cityUrl = cityUrl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public AirportDetailsData getDetails() {
		return details;
	}

	public void setDetails(AirportDetailsData details) {
		this.details = details;
	}

	public Boolean getEligibility() {
		return eligibility;
	}

	public void setEligibility(Boolean eligibility) {
		this.eligibility = eligibility;
	}

	public Calendar getEtktAvlblFrom() {
		return etktAvlblFrom;
	}

	public void setEtktAvlblFrom(Calendar etktAvlblFrom) {
		this.etktAvlblFrom = etktAvlblFrom;
	}

	public Boolean getHiddenInFirstDeparture() {
		return hiddenInFirstDeparture;
	}

	public void setHiddenInFirstDeparture(Boolean hiddenInFirstDeparture) {
		this.hiddenInFirstDeparture = hiddenInFirstDeparture;
	}

	public Integer getIdMsg() {
		return idMsg;
	}

	public void setIdMsg(Integer idMsg) {
		this.idMsg = idMsg;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public Calendar getUnavlblFrom() {
		return unavlblFrom;
	}

	public void setUnavlblFrom(Calendar unavlblFrom) {
		this.unavlblFrom = unavlblFrom;
	}

	public Calendar getUnavlblUntil() {
		return unavlblUntil;
	}

	public void setUnavlblUntil(Calendar unavlblUntil) {
		this.unavlblUntil = unavlblUntil;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		AirportData other = (AirportData) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AirportData [azDeserves=" + azDeserves + ", area=" + area
				+ ", city=" + city + ", cityCode=" + cityCode + ", cityUrl="
				+ cityUrl + ", code=" + code + ", country=" + country
				+ ", countryCode=" + countryCode + ", description="
				+ description + ", details=" + details + ", eligibility="
				+ eligibility + ", etktAvlblFrom=" + etktAvlblFrom
				+ ", hiddenInFirstDeparture=" + hiddenInFirstDeparture
				+ ", idMsg=" + idMsg + ", name=" + name + ", state=" + state
				+ ", stateCode=" + stateCode + ", unavlblFrom=" + unavlblFrom
				+ ", unavlblUntil=" + unavlblUntil + "]";
	}

}