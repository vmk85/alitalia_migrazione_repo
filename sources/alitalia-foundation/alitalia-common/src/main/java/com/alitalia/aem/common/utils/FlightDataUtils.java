package com.alitalia.aem.common.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.enumerations.FlightTypeEnum;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;

public class FlightDataUtils {

	public static List<Integer> computeConnectingFlightsWait(FlightData flightData) {
		List<Integer> connectingFlightsWait = new ArrayList<Integer>();
		if(flightData.getFlightType() == FlightTypeEnum.CONNECTING){
			ConnectingFlightData connectingFlights = (ConnectingFlightData) flightData;
			
			Iterator<FlightData> it = connectingFlights.getFlights().iterator();
			DirectFlightData previousFlight = (DirectFlightData) it.next();
			while(it.hasNext()){
				DirectFlightData nextFlight = (DirectFlightData) it.next();
				Calendar previousArrivalDate = previousFlight.getArrivalDate();
				Calendar nextDepartureDate = nextFlight.getDepartureDate();
				long waitingTime = nextDepartureDate.getTime().getTime() - previousArrivalDate.getTime().getTime();
				int waiting = (int) (waitingTime/1000)/60;
				connectingFlightsWait.add(new Integer(waiting));
				previousFlight = nextFlight;
			}
		}
		connectingFlightsWait.add(0);
		return connectingFlightsWait;
	}
	
	public static List<Integer> computeConnectingFlightsWait(List<MmbFlightData> flightData) {
		List<Integer> connectingFlightsWait = new ArrayList<Integer>();

			Iterator<MmbFlightData> it = flightData.iterator();
			MmbFlightData previousFlight =  it.next();
			while(it.hasNext()){
				MmbFlightData nextFlight =  it.next();
				Calendar previousArrivalDate = previousFlight.getArrivalDateTime();
				Calendar nextDepartureDate = nextFlight.getDepartureDateTime();
				long waitingTime = nextDepartureDate.getTime().getTime() - previousArrivalDate.getTime().getTime();
				int waiting = (int) (waitingTime/1000)/60;
				connectingFlightsWait.add(new Integer(waiting));
				previousFlight = nextFlight;
			}
		
		connectingFlightsWait.add(0);
		return connectingFlightsWait;
	}
	
}
