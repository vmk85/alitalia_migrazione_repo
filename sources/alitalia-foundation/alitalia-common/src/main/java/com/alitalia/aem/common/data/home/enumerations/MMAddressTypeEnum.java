package com.alitalia.aem.common.data.home.enumerations;

public enum MMAddressTypeEnum {

    UNKNOWN("UnKnown"),
    HOME("Home"),
    BUSINESS("Business");

    private final String value;
    
    MMAddressTypeEnum(String v) {
		value = v;
	}
	
	public String value() {
		return value;
	}

	public static MMAddressTypeEnum fromValue(String v) {
		for (MMAddressTypeEnum c: MMAddressTypeEnum.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
