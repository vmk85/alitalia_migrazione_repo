package com.alitalia.aem.common.data.home;

public class ResultBookingDetailsSolutionPricingBookingInfoFareData {
	
	private String extendedFareCodeField;
	private String ticketDesignatorField;
	
	public String getExtendedFareCodeField() {
		return extendedFareCodeField;
	}
	
	public void setExtendedFareCodeField(String extendedFareCodeField) {
		this.extendedFareCodeField = extendedFareCodeField;
	}
	
	public String getTicketDesignatorField() {
		return ticketDesignatorField;
	}
	
	public void setTicketDesignatorField(String ticketDesignatorField) {
		this.ticketDesignatorField = ticketDesignatorField;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((extendedFareCodeField == null) ? 0 : extendedFareCodeField
						.hashCode());
		result = prime
				* result
				+ ((ticketDesignatorField == null) ? 0 : ticketDesignatorField
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingDetailsSolutionPricingBookingInfoFareData other = (ResultBookingDetailsSolutionPricingBookingInfoFareData) obj;
		if (extendedFareCodeField == null) {
			if (other.extendedFareCodeField != null)
				return false;
		} else if (!extendedFareCodeField.equals(other.extendedFareCodeField))
			return false;
		if (ticketDesignatorField == null) {
			if (other.ticketDesignatorField != null)
				return false;
		} else if (!ticketDesignatorField.equals(other.ticketDesignatorField))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ResultBookingDetailsSolutionPricingBookingInfoFareData [extendedFareCodeField="
				+ extendedFareCodeField
				+ ", ticketDesignatorField="
				+ ticketDesignatorField + "]";
	}
	
}
