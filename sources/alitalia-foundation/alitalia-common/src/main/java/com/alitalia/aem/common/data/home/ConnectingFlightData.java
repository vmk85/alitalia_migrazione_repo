package com.alitalia.aem.common.data.home;

import java.util.ArrayList;

public class ConnectingFlightData extends FlightData {

	private ArrayList<FlightData> flights;
	
	public ConnectingFlightData() {
		super();
	}
	
	public ConnectingFlightData(ConnectingFlightData clone) {
		if (clone.flights != null) {
			ArrayList<FlightData> flights = new ArrayList<FlightData>();
			for (FlightData item : clone.flights) {
				if (item instanceof DirectFlightData) {
					flights.add(new DirectFlightData((DirectFlightData) item));
				} else if (item instanceof ConnectingFlightData) {
					flights.add(new ConnectingFlightData((ConnectingFlightData) item));
				}
			}
			this.flights = flights;
		}
	}

	public ArrayList<FlightData> getFlights() {
		return flights;
	}

	public void setFlights(ArrayList<FlightData> flights) {
		this.flights = flights;
	}

	public void addFlight(FlightData flight) {
		this.flights.add(flight);
	}

	public void removeFlight(FlightData flight) {
		this.flights.remove(flight);
	}
}
