package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.common.messages.BaseRequest;

public class InfoPassengerCreatePNRRequest extends BaseRequest {
	
	private String cug;
	private String carnetCode;
	private String marketCode;
	private RoutesData prenotation;
	
	public InfoPassengerCreatePNRRequest() {
	}
	
	public InfoPassengerCreatePNRRequest(String carnetCode, String marketCode,
			RoutesData prenotation) {
		super();
		this.carnetCode = carnetCode;
		this.marketCode = marketCode;
		this.prenotation = prenotation;
	}
	
	public InfoPassengerCreatePNRRequest(String tid, String sid) {
		super(tid, sid);
	}
	
	public InfoPassengerCreatePNRRequest(String tid, String sid, String carnetCode,
			String marketCode, RoutesData prenotation) {
		super(tid, sid);
		this.carnetCode = carnetCode;
		this.marketCode = marketCode;
		this.prenotation = prenotation;
	}
	
	public String getCarnetCode() {
		return carnetCode;
	}
	
	public void setCarnetCode(String carnetCode) {
		this.carnetCode = carnetCode;
	}
	
	public String getMarketCode() {
		return marketCode;
	}
	
	public void setMarketCode(String marketCode) {
		this.marketCode = marketCode;
	}
	
	public RoutesData getPrenotation() {
		return prenotation;
	}
	
	public void setPrenotation(RoutesData prenotation) {
		this.prenotation = prenotation;
	}

	public String getCug() {
		return cug;
	}

	public void setCug(String cug) {
		this.cug = cug;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((carnetCode == null) ? 0 : carnetCode.hashCode());
		result = prime * result + ((cug == null) ? 0 : cug.hashCode());
		result = prime * result
				+ ((marketCode == null) ? 0 : marketCode.hashCode());
		result = prime * result
				+ ((prenotation == null) ? 0 : prenotation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		InfoPassengerCreatePNRRequest other = (InfoPassengerCreatePNRRequest) obj;
		if (carnetCode == null) {
			if (other.carnetCode != null)
				return false;
		} else if (!carnetCode.equals(other.carnetCode))
			return false;
		if (cug == null) {
			if (other.cug != null)
				return false;
		} else if (!cug.equals(other.cug))
			return false;
		if (marketCode == null) {
			if (other.marketCode != null)
				return false;
		} else if (!marketCode.equals(other.marketCode))
			return false;
		if (prenotation == null) {
			if (other.prenotation != null)
				return false;
		} else if (!prenotation.equals(other.prenotation))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "InfoPassengerCreatePNRRequest [cug=" + cug + ", carnetCode="
				+ carnetCode + ", marketCode=" + marketCode + ", prenotation="
				+ prenotation + "]";
	}
}