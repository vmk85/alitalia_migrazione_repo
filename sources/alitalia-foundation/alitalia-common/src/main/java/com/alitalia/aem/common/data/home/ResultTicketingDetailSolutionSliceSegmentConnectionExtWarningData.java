package com.alitalia.aem.common.data.home;

public class ResultTicketingDetailSolutionSliceSegmentConnectionExtWarningData {

    private Boolean longLayoverField;
    private Boolean longLayoverFieldSpecified;
    private Boolean riskyConnectionField;
    private Boolean riskyConnectionFieldSpecified;
    private Boolean tightConnectionField;
    private Boolean tightConnectionFieldSpecified;

    public Boolean isLongLayoverField() {
        return longLayoverField;
    }

    public void setLongLayoverField(Boolean value) {
        this.longLayoverField = value;
    }

    public Boolean isLongLayoverFieldSpecified() {
        return longLayoverFieldSpecified;
    }

    public void setLongLayoverFieldSpecified(Boolean value) {
        this.longLayoverFieldSpecified = value;
    }

    public Boolean isRiskyConnectionField() {
        return riskyConnectionField;
    }

    public void setRiskyConnectionField(Boolean value) {
        this.riskyConnectionField = value;
    }

    public Boolean isRiskyConnectionFieldSpecified() {
        return riskyConnectionFieldSpecified;
    }

    public void setRiskyConnectionFieldSpecified(Boolean value) {
        this.riskyConnectionFieldSpecified = value;
    }

    public Boolean isTightConnectionField() {
        return tightConnectionField;
    }

    public void setTightConnectionField(Boolean value) {
        this.tightConnectionField = value;
    }

    public Boolean isTightConnectionFieldSpecified() {
        return tightConnectionFieldSpecified;
    }

    public void setTightConnectionFieldSpecified(Boolean value) {
        this.tightConnectionFieldSpecified = value;
    }

}
