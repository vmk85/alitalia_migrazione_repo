package com.alitalia.aem.common.data.home;

import java.util.List;

public class ResultBookingPriceSolutionSliceData {
	
	private ResultBookingPriceSolutionSliceExtData extField;
	private List<ResultBookingPriceSolutionSliceSegmentPricingData> segmentField;
	
	public ResultBookingPriceSolutionSliceExtData getExtField() {
		return extField;
	}
	
	public void setExtField(ResultBookingPriceSolutionSliceExtData extField) {
		this.extField = extField;
	}
	
	public List<ResultBookingPriceSolutionSliceSegmentPricingData> getSegmentField() {
		return segmentField;
	}
	
	public void setSegmentField(
			List<ResultBookingPriceSolutionSliceSegmentPricingData> segmentField) {
		this.segmentField = segmentField;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((extField == null) ? 0 : extField.hashCode());
		result = prime * result
				+ ((segmentField == null) ? 0 : segmentField.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingPriceSolutionSliceData other = (ResultBookingPriceSolutionSliceData) obj;
		if (extField == null) {
			if (other.extField != null)
				return false;
		} else if (!extField.equals(other.extField))
			return false;
		if (segmentField == null) {
			if (other.segmentField != null)
				return false;
		} else if (!segmentField.equals(other.segmentField))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingPriceSolutionSliceData [extField=" + extField
				+ ", segmentField=" + segmentField + "]";
	}
	
}
