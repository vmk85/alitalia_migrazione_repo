package com.alitalia.aem.common.data.home.checkin;

public class EmdErrorData {

	private String code;
	private String docURL;
	private String nodeList;
	private String recordId;
	private String xshortText;
	private String status;
	private String tag;
	private String type;
	private String value;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDocURL() {
		return docURL;
	}
	public void setDocURL(String docURL) {
		this.docURL = docURL;
	}
	public String getNodeList() {
		return nodeList;
	}
	public void setNodeList(String nodeList) {
		this.nodeList = nodeList;
	}
	public String getRecordId() {
		return recordId;
	}
	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}
	public String getXshortText() {
		return xshortText;
	}
	public void setXshortText(String xshortText) {
		this.xshortText = xshortText;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((docURL == null) ? 0 : docURL.hashCode());
		result = prime * result
				+ ((nodeList == null) ? 0 : nodeList.hashCode());
		result = prime * result
				+ ((recordId == null) ? 0 : recordId.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((tag == null) ? 0 : tag.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		result = prime * result
				+ ((xshortText == null) ? 0 : xshortText.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmdErrorData other = (EmdErrorData) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (docURL == null) {
			if (other.docURL != null)
				return false;
		} else if (!docURL.equals(other.docURL))
			return false;
		if (nodeList == null) {
			if (other.nodeList != null)
				return false;
		} else if (!nodeList.equals(other.nodeList))
			return false;
		if (recordId == null) {
			if (other.recordId != null)
				return false;
		} else if (!recordId.equals(other.recordId))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (tag == null) {
			if (other.tag != null)
				return false;
		} else if (!tag.equals(other.tag))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		if (xshortText == null) {
			if (other.xshortText != null)
				return false;
		} else if (!xshortText.equals(other.xshortText))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "EmdErrorData [code=" + code + ", docURL=" + docURL
				+ ", nodeList=" + nodeList + ", recordId=" + recordId
				+ ", xshortText=" + xshortText + ", status=" + status
				+ ", tag=" + tag + ", type=" + type + ", value=" + value + "]";
	}
}