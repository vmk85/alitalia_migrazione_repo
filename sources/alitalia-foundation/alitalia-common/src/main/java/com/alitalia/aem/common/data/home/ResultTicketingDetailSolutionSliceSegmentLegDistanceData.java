package com.alitalia.aem.common.data.home;

import com.alitalia.aem.common.data.home.enumerations.DistanceUnitsEnum;

public class ResultTicketingDetailSolutionSliceSegmentLegDistanceData {

    private DistanceUnitsEnum unitsField;
    private int valueField;

    public DistanceUnitsEnum getUnitsField() {
        return unitsField;
    }

    public void setUnitsField(DistanceUnitsEnum value) {
        this.unitsField = value;
    }

    public int getValueField() {
        return valueField;
    }

    public void setValueField(int value) {
        this.valueField = value;
    }

}
