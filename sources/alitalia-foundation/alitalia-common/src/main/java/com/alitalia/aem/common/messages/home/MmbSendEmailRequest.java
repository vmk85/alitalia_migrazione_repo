package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.MmbBaseRequest;

public class MmbSendEmailRequest extends MmbBaseRequest {

	private Boolean htmlBody;
	private String pnr;
	private String mailBody;
    private String mailSubject;
    private String recipient;
    private String sender;
	
	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getMailBody() {
		return mailBody;
	}
    
	public void setMailBody(String mailBody) {
		this.mailBody = mailBody;
	}
	
	public String getMailSubject() {
		return mailSubject;
	}
	
	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public Boolean isHtmlBody() {
		return htmlBody;
	}

	public void setHtmlBody(Boolean htmlBody) {
		this.htmlBody = htmlBody;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((pnr == null) ? 0 : pnr.hashCode());
		result = prime * result
				+ ((recipient == null) ? 0 : recipient.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbSendEmailRequest other = (MmbSendEmailRequest) obj;
		if (pnr == null) {
			if (other.pnr != null)
				return false;
		} else if (!pnr.equals(other.pnr))
			return false;
		if (recipient == null) {
			if (other.recipient != null)
				return false;
		} else if (!recipient.equals(other.recipient))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbSendEmailRequest [htmlBody=" + htmlBody + ", pnr=" + pnr
				+ ", mailBody=" + mailBody + ", mailSubject=" + mailSubject
				+ ", recipient=" + recipient + ", sender=" + sender
				+ ", getBaseInfo()=" + getBaseInfo() + ", getClient()="
				+ getClient() + ", getTid()=" + getTid() + ", getSid()="
				+ getSid() + "]";
	}

}