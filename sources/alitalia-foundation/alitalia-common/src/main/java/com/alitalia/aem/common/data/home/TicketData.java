package com.alitalia.aem.common.data.home;

public class TicketData {

    private String dataVolo;
    private String numeroBiglietto;
    private String pnr;
    private String tratta;

    public TicketData() {
		super();
	}

	public TicketData(TicketData clone) {
		super();
		this.dataVolo = clone.getDataVolo();
		this.numeroBiglietto = clone.getNumeroBiglietto();
		this.pnr = clone.getPnr();
		this.tratta = clone.getTratta();
	}

	public TicketData(String dataVolo, String numeroBiglietto, String pnr,
			String tratta) {
		super();
		this.dataVolo = dataVolo;
		this.numeroBiglietto = numeroBiglietto;
		this.pnr = pnr;
		this.tratta = tratta;
	}

	public String getDataVolo() {
		return dataVolo;
	}

	public void setDataVolo(String dataVolo) {
		this.dataVolo = dataVolo;
	}

	public String getNumeroBiglietto() {
		return numeroBiglietto;
	}

	public void setNumeroBiglietto(String numeroBiglietto) {
		this.numeroBiglietto = numeroBiglietto;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getTratta() {
		return tratta;
	}

	public void setTratta(String tratta) {
		this.tratta = tratta;
	}

	@Override
	public String toString() {
		return "TicketData [dataVolo=" + dataVolo + ", numeroBiglietto="
				+ numeroBiglietto + ", pnr=" + pnr + ", tratta=" + tratta + "]";
	}
}
