package com.alitalia.aem.common.exception;

public class ResponseException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6296722971600906440L;
	private int errorCode;
	
	public ResponseException(int errorCode, String message, Throwable cause) {
		super(message, cause);
		this.errorCode = errorCode;
	}
	
	public ResponseException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ResponseException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ResponseException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ResponseException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ResponseException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	@Override
	public String toString() {
		return "ResponseException [errorCode=" + errorCode + ", toString()="
				+ super.toString() + "]";
	}
}