package com.alitalia.aem.common.messages.home;

import java.util.Date;

import com.alitalia.aem.common.messages.BaseRequest;

public class AgencyUpdateDataRequest extends BaseRequest {

	private String codiceAgenzia;
	private String ragioneSociale;
	private String partitaIVA;
	private String codiceFiscale;
	private String indirizzo;
	private String citta;
	private String provincia;
	private String zip;
	private String regione;
	private String stato;
	private String telefono;
	private String fax;
	private String codFamCon;
	private String emailTitolare;
	private String emailOperatore;
	private String codiceAccordo;
	private Boolean flagSendMail;
	private String codiceSirax;
	private Date dataValiditaAccordo;
	
	public AgencyUpdateDataRequest() {
		super();
	}
	
	public AgencyUpdateDataRequest(String tid, String sid) {
		super(tid, sid);
	}
	
	public AgencyUpdateDataRequest(String codiceAgenzia, String ragioneSociale,
			String partitaIVA, String codiceFiscale, String indirizzo,
			String citta, String provincia, String zip, String regione,
			String stato, String telefono, String fax, String codFamCon,
			String emailTitolare, String emailOperatore, String codiceAccordo,
			Boolean fgSendMail, String codiceSirax, Date dataValiditaAccordo) {
		super();
		this.codiceAgenzia = codiceAgenzia;
		this.ragioneSociale = ragioneSociale;
		this.partitaIVA = partitaIVA;
		this.codiceFiscale = codiceFiscale;
		this.indirizzo = indirizzo;
		this.citta = citta;
		this.provincia = provincia;
		this.zip = zip;
		this.regione = regione;
		this.stato = stato;
		this.telefono = telefono;
		this.fax = fax;
		this.codFamCon = codFamCon;
		this.emailTitolare = emailTitolare;
		this.emailOperatore = emailOperatore;
		this.codiceAccordo = codiceAccordo;
		this.flagSendMail = fgSendMail;
		this.codiceSirax = codiceSirax;
		this.dataValiditaAccordo = dataValiditaAccordo;
	}

	public AgencyUpdateDataRequest(String tid, String sid, String codiceAgenzia, String ragioneSociale,
			String partitaIVA, String codiceFiscale, String indirizzo,
			String citta, String provincia, String zip, String regione,
			String stato, String telefono, String fax, String codFamCon,
			String emailTitolare, String emailOperatore, String codiceAccordo,
			Boolean fgSendMail, String codiceSirax, Date dataValiditaAccordo) {
		super(tid, sid);
		this.codiceAgenzia = codiceAgenzia;
		this.ragioneSociale = ragioneSociale;
		this.partitaIVA = partitaIVA;
		this.codiceFiscale = codiceFiscale;
		this.indirizzo = indirizzo;
		this.citta = citta;
		this.provincia = provincia;
		this.zip = zip;
		this.regione = regione;
		this.stato = stato;
		this.telefono = telefono;
		this.fax = fax;
		this.codFamCon = codFamCon;
		this.emailTitolare = emailTitolare;
		this.emailOperatore = emailOperatore;
		this.codiceAccordo = codiceAccordo;
		this.flagSendMail = fgSendMail;
		this.codiceSirax = codiceSirax;
		this.dataValiditaAccordo = dataValiditaAccordo;
	}

	public String getCodiceAgenzia() {
		return codiceAgenzia;
	}

	public void setCodiceAgenzia(String codiceAgenzia) {
		this.codiceAgenzia = codiceAgenzia;
	}

	public String getRagioneSociale() {
		return ragioneSociale;
	}

	public void setRagioneSociale(String ragioneSociale) {
		this.ragioneSociale = ragioneSociale;
	}

	public String getPartitaIVA() {
		return partitaIVA;
	}

	public void setPartitaIVA(String partitaIVA) {
		this.partitaIVA = partitaIVA;
	}

	public String getCodiceFiscale() {
		return codiceFiscale;
	}

	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public String getCitta() {
		return citta;
	}

	public void setCitta(String citta) {
		this.citta = citta;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getRegione() {
		return regione;
	}

	public void setRegione(String regione) {
		this.regione = regione;
	}

	public String getStato() {
		return stato;
	}

	public void setStato(String stato) {
		this.stato = stato;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getCodFamCon() {
		return codFamCon;
	}

	public void setCodFamCon(String codFamCon) {
		this.codFamCon = codFamCon;
	}

	public String getEmailTitolare() {
		return emailTitolare;
	}

	public void setEmailTitolare(String emailTitolare) {
		this.emailTitolare = emailTitolare;
	}

	public String getEmailOperatore() {
		return emailOperatore;
	}

	public void setEmailOperatore(String emailOperatore) {
		this.emailOperatore = emailOperatore;
	}

	public String getCodiceAccordo() {
		return codiceAccordo;
	}

	public void setCodiceAccordo(String codiceAccordo) {
		this.codiceAccordo = codiceAccordo;
	}

	public Boolean getFlagSendMail() {
		return flagSendMail;
	}

	public void setFlagSendMail(Boolean flagSendMail) {
		this.flagSendMail = flagSendMail;
	}

	public String getCodiceSirax() {
		return codiceSirax;
	}

	public void setCodiceSirax(String codiceSirax) {
		this.codiceSirax = codiceSirax;
	}

	public Date getDataValiditaAccordo() {
		return dataValiditaAccordo;
	}

	public void setDataValiditaAccordo(Date dataValiditaAccordo) {
		this.dataValiditaAccordo = dataValiditaAccordo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((citta == null) ? 0 : citta.hashCode());
		result = prime * result
				+ ((codFamCon == null) ? 0 : codFamCon.hashCode());
		result = prime * result
				+ ((codiceAccordo == null) ? 0 : codiceAccordo.hashCode());
		result = prime * result
				+ ((codiceAgenzia == null) ? 0 : codiceAgenzia.hashCode());
		result = prime * result
				+ ((codiceFiscale == null) ? 0 : codiceFiscale.hashCode());
		result = prime * result
				+ ((codiceSirax == null) ? 0 : codiceSirax.hashCode());
		result = prime
				* result
				+ ((dataValiditaAccordo == null) ? 0 : dataValiditaAccordo
						.hashCode());
		result = prime * result
				+ ((emailOperatore == null) ? 0 : emailOperatore.hashCode());
		result = prime * result
				+ ((emailTitolare == null) ? 0 : emailTitolare.hashCode());
		result = prime * result + ((fax == null) ? 0 : fax.hashCode());
		result = prime * result
				+ ((flagSendMail == null) ? 0 : flagSendMail.hashCode());
		result = prime * result
				+ ((indirizzo == null) ? 0 : indirizzo.hashCode());
		result = prime * result
				+ ((partitaIVA == null) ? 0 : partitaIVA.hashCode());
		result = prime * result
				+ ((provincia == null) ? 0 : provincia.hashCode());
		result = prime * result
				+ ((ragioneSociale == null) ? 0 : ragioneSociale.hashCode());
		result = prime * result + ((regione == null) ? 0 : regione.hashCode());
		result = prime * result + ((stato == null) ? 0 : stato.hashCode());
		result = prime * result
				+ ((telefono == null) ? 0 : telefono.hashCode());
		result = prime * result + ((zip == null) ? 0 : zip.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgencyUpdateDataRequest other = (AgencyUpdateDataRequest) obj;
		if (citta == null) {
			if (other.citta != null)
				return false;
		} else if (!citta.equals(other.citta))
			return false;
		if (codFamCon == null) {
			if (other.codFamCon != null)
				return false;
		} else if (!codFamCon.equals(other.codFamCon))
			return false;
		if (codiceAccordo == null) {
			if (other.codiceAccordo != null)
				return false;
		} else if (!codiceAccordo.equals(other.codiceAccordo))
			return false;
		if (codiceAgenzia == null) {
			if (other.codiceAgenzia != null)
				return false;
		} else if (!codiceAgenzia.equals(other.codiceAgenzia))
			return false;
		if (codiceFiscale == null) {
			if (other.codiceFiscale != null)
				return false;
		} else if (!codiceFiscale.equals(other.codiceFiscale))
			return false;
		if (codiceSirax == null) {
			if (other.codiceSirax != null)
				return false;
		} else if (!codiceSirax.equals(other.codiceSirax))
			return false;
		if (dataValiditaAccordo == null) {
			if (other.dataValiditaAccordo != null)
				return false;
		} else if (!dataValiditaAccordo.equals(other.dataValiditaAccordo))
			return false;
		if (emailOperatore == null) {
			if (other.emailOperatore != null)
				return false;
		} else if (!emailOperatore.equals(other.emailOperatore))
			return false;
		if (emailTitolare == null) {
			if (other.emailTitolare != null)
				return false;
		} else if (!emailTitolare.equals(other.emailTitolare))
			return false;
		if (fax == null) {
			if (other.fax != null)
				return false;
		} else if (!fax.equals(other.fax))
			return false;
		if (flagSendMail == null) {
			if (other.flagSendMail != null)
				return false;
		} else if (!flagSendMail.equals(other.flagSendMail))
			return false;
		if (indirizzo == null) {
			if (other.indirizzo != null)
				return false;
		} else if (!indirizzo.equals(other.indirizzo))
			return false;
		if (partitaIVA == null) {
			if (other.partitaIVA != null)
				return false;
		} else if (!partitaIVA.equals(other.partitaIVA))
			return false;
		if (provincia == null) {
			if (other.provincia != null)
				return false;
		} else if (!provincia.equals(other.provincia))
			return false;
		if (ragioneSociale == null) {
			if (other.ragioneSociale != null)
				return false;
		} else if (!ragioneSociale.equals(other.ragioneSociale))
			return false;
		if (regione == null) {
			if (other.regione != null)
				return false;
		} else if (!regione.equals(other.regione))
			return false;
		if (stato == null) {
			if (other.stato != null)
				return false;
		} else if (!stato.equals(other.stato))
			return false;
		if (telefono == null) {
			if (other.telefono != null)
				return false;
		} else if (!telefono.equals(other.telefono))
			return false;
		if (zip == null) {
			if (other.zip != null)
				return false;
		} else if (!zip.equals(other.zip))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AgencyUpdateDataRequest [codiceAgenzia=" + codiceAgenzia
				+ ", ragioneSociale=" + ragioneSociale + ", partitaIVA="
				+ partitaIVA + ", codiceFiscale=" + codiceFiscale
				+ ", indirizzo=" + indirizzo + ", citta=" + citta
				+ ", provincia=" + provincia + ", zip=" + zip + ", regione="
				+ regione + ", stato=" + stato + ", telefono=" + telefono
				+ ", fax=" + fax + ", codFamCon=" + codFamCon
				+ ", emailTitolare=" + emailTitolare + ", emailOperatore="
				+ emailOperatore + ", codiceAccordo=" + codiceAccordo
				+ ", flagSendMail=" + flagSendMail + ", codiceSirax=" + codiceSirax
				+ ", dataValiditaAccordo=" + dataValiditaAccordo + "]";
	}
	
}
