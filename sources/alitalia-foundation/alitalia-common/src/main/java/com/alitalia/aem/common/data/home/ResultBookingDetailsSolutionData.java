package com.alitalia.aem.common.data.home;

import java.util.List;

public class ResultBookingDetailsSolutionData {
	
	private ResultBookingDetailsSolutionAddCollectPriceData addCollectPriceField;
	private List<ResultBookingDetailsSolutionPricingData> pricingField;
	private ResultBookingDetailsSolutionRefundPriceData refundPriceField;
	private String saleTaxTotalField;
	private String saleTotalField;
	private String timeField;
	
	public ResultBookingDetailsSolutionAddCollectPriceData getAddCollectPriceField() {
		return addCollectPriceField;
	}
	
	public void setAddCollectPriceField(
			ResultBookingDetailsSolutionAddCollectPriceData addCollectPriceField) {
		this.addCollectPriceField = addCollectPriceField;
	}
	
	public List<ResultBookingDetailsSolutionPricingData> getPricingField() {
		return pricingField;
	}
	
	public void setPricingField(
			List<ResultBookingDetailsSolutionPricingData> pricingField) {
		this.pricingField = pricingField;
	}
	
	public ResultBookingDetailsSolutionRefundPriceData getRefundPriceField() {
		return refundPriceField;
	}
	
	public void setRefundPriceField(
			ResultBookingDetailsSolutionRefundPriceData refundPriceField) {
		this.refundPriceField = refundPriceField;
	}
	
	public String getSaleTaxTotalField() {
		return saleTaxTotalField;
	}
	
	public void setSaleTaxTotalField(String saleTaxTotalField) {
		this.saleTaxTotalField = saleTaxTotalField;
	}
	
	public String getSaleTotalField() {
		return saleTotalField;
	}
	
	public void setSaleTotalField(String saleTotalField) {
		this.saleTotalField = saleTotalField;
	}
	
	public String getTimeField() {
		return timeField;
	}
	
	public void setTimeField(String timeField) {
		this.timeField = timeField;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((addCollectPriceField == null) ? 0 : addCollectPriceField
						.hashCode());
		result = prime * result
				+ ((pricingField == null) ? 0 : pricingField.hashCode());
		result = prime
				* result
				+ ((refundPriceField == null) ? 0 : refundPriceField.hashCode());
		result = prime
				* result
				+ ((saleTaxTotalField == null) ? 0 : saleTaxTotalField
						.hashCode());
		result = prime * result
				+ ((saleTotalField == null) ? 0 : saleTotalField.hashCode());
		result = prime * result
				+ ((timeField == null) ? 0 : timeField.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingDetailsSolutionData other = (ResultBookingDetailsSolutionData) obj;
		if (addCollectPriceField == null) {
			if (other.addCollectPriceField != null)
				return false;
		} else if (!addCollectPriceField.equals(other.addCollectPriceField))
			return false;
		if (pricingField == null) {
			if (other.pricingField != null)
				return false;
		} else if (!pricingField.equals(other.pricingField))
			return false;
		if (refundPriceField == null) {
			if (other.refundPriceField != null)
				return false;
		} else if (!refundPriceField.equals(other.refundPriceField))
			return false;
		if (saleTaxTotalField == null) {
			if (other.saleTaxTotalField != null)
				return false;
		} else if (!saleTaxTotalField.equals(other.saleTaxTotalField))
			return false;
		if (saleTotalField == null) {
			if (other.saleTotalField != null)
				return false;
		} else if (!saleTotalField.equals(other.saleTotalField))
			return false;
		if (timeField == null) {
			if (other.timeField != null)
				return false;
		} else if (!timeField.equals(other.timeField))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingDetailsSolutionData [addCollectPriceField="
				+ addCollectPriceField + ", pricingField=" + pricingField
				+ ", refundPriceField=" + refundPriceField
				+ ", saleTaxTotalField=" + saleTaxTotalField
				+ ", saleTotalField=" + saleTotalField + ", timeField="
				+ timeField + "]";
	}
	
}
