package com.alitalia.aem.common.data.home.enumerations;

public enum TimeTypeEnum {

	ANYTIME("Anytime"),
    AFTERNOON("Afternoon"),
    EVENING("Evening"),
    MORNING("Morning");
    private final String value;

    TimeTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TimeTypeEnum fromValue(String v) {
        for (TimeTypeEnum c: TimeTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
