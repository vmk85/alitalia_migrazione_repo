package com.alitalia.aem.common.data.home.enumerations;

public enum TradeStatisticDataTypeEnum {

	CHANGE_PSW("ChangePassword"),
	GET_PSW("GetPassword"),
	GET_REPORT("GetReport"),
	LOGIN("Login"),
	LOGOUT("Logout"),
	STATEMENT("Statement");

	private final String value;

	private TradeStatisticDataTypeEnum(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	public static TradeStatisticDataTypeEnum fromValue(String v) {
		for (TradeStatisticDataTypeEnum c: TradeStatisticDataTypeEnum.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}