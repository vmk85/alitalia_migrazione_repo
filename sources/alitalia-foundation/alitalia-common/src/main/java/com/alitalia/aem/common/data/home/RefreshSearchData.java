package com.alitalia.aem.common.data.home;

import java.math.BigDecimal;

import com.alitalia.aem.common.data.home.enumerations.RefreshTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;

public class RefreshSearchData extends GatewaySearchData {

	private RefreshTypeEnum refreshType;
	private String currency;
	private BigDecimal originalFare;
	private BigDecimal originalPrice;
	private RouteTypeEnum sliceType;
	
	public String getCurrency() {
		return currency;
	}
	
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public BigDecimal getOriginalFare() {
		return originalFare;
	}
	
	public void setOriginalFare(BigDecimal originalFare) {
		this.originalFare = originalFare;
	}
	
	public BigDecimal getOriginalPrice() {
		return originalPrice;
	}
	
	public void setOriginalPrice(BigDecimal originalPrice) {
		this.originalPrice = originalPrice;
	}
	
	public RouteTypeEnum getSliceType() {
		return sliceType;
	}
	
	public void setSliceType(RouteTypeEnum sliceType) {
		this.sliceType = sliceType;
	}
	
	public RefreshTypeEnum getRefreshType() {
		return refreshType;
	}
	
	public void setRefreshType(RefreshTypeEnum refreshType) {
		this.refreshType = refreshType;
	}

	@Override
	public String toString() {
		return "RefreshSearchData [refreshType=" + refreshType + ", currency="
				+ currency + ", originalFare=" + originalFare
				+ ", originalPrice=" + originalPrice + ", sliceType="
				+ sliceType + ", getPassengers()=" + getPassengers()
				+ ", getType()=" + getType() + ", getId()=" + getId()
				+ ", getSessionId()=" + getSessionId() + ", getSolutionSet()="
				+ getSolutionSet() + ", getSolutionId()=" + getSolutionId()
				+ ", getRefreshSolutionId()=" + getRefreshSolutionId()
				+ ", getFarmId()=" + getFarmId() + ", getProperties()="
				+ getProperties() + "]";
	}

}
