package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.carnet.CarnetPrenotationData;
import com.alitalia.aem.common.messages.MmbBaseRequest;

public class CarnetBuyCarnetRequest extends MmbBaseRequest {

    private String languageCode;
    private String marketCode;
    private CarnetPrenotationData prenotation;

	public CarnetBuyCarnetRequest(String tid, String sid) {
		super(tid, sid);
	}

	public CarnetBuyCarnetRequest() {
		super();
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getMarketCode() {
		return marketCode;
	}

	public void setMarketCode(String marketCode) {
		this.marketCode = marketCode;
	}

	public CarnetPrenotationData getPrenotation() {
		return prenotation;
	}

	public void setPrenotation(CarnetPrenotationData prenotation) {
		this.prenotation = prenotation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((languageCode == null) ? 0 : languageCode.hashCode());
		result = prime * result
				+ ((marketCode == null) ? 0 : marketCode.hashCode());
		result = prime * result
				+ ((prenotation == null) ? 0 : prenotation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarnetBuyCarnetRequest other = (CarnetBuyCarnetRequest) obj;
		if (languageCode == null) {
			if (other.languageCode != null)
				return false;
		} else if (!languageCode.equals(other.languageCode))
			return false;
		if (marketCode == null) {
			if (other.marketCode != null)
				return false;
		} else if (!marketCode.equals(other.marketCode))
			return false;
		if (prenotation == null) {
			if (other.prenotation != null)
				return false;
		} else if (!prenotation.equals(other.prenotation))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CarnetBuyCarnetRequest [languageCode=" + languageCode
				+ ", marketCode=" + marketCode + ", prenotation=" + prenotation
				+ "]";
	}
}