package com.alitalia.aem.common.data.home;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;

import com.alitalia.aem.common.data.home.enumerations.BestPriceTypeEnum;

public class BestPriceData implements Comparable<BestPriceData>, Serializable {

	private Calendar date;
	private BigDecimal price;
	private BestPriceTypeEnum type;
	
	public Calendar getDate() {
		return date;
	}
	
	public void setDate(Calendar date) {
		this.date = date;
	}
	
	public BigDecimal getPrice() {
		return price;
	}
	
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	public BestPriceTypeEnum getType() {
		return type;
	}
	
	public void setType(BestPriceTypeEnum type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BestPriceData other = (BestPriceData) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BestPriceData [date=" + date + ", price=" + price + ", type="
				+ type + "]";
	}

	@Override
	public int compareTo(BestPriceData o) {
		if (this.getDate() == null && o.getDate() == null) {
			return 0;
		} else if (this.getDate() == null) {
			return -1;
		} else if (o.getDate() == null) {
			return 1;
		} else {
			return this.getDate().getTimeInMillis() > o.getDate().getTimeInMillis() ? 1 : -1;
		}
	}
}