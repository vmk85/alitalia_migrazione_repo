package com.alitalia.aem.common.data.home;

public class ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryData {
	
	private ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesData penaltiesField;
	
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesData getPenaltiesField() {
		return penaltiesField;
	}
	
	public void setPenaltiesField(
			ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesData penaltiesField) {
		this.penaltiesField = penaltiesField;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((penaltiesField == null) ? 0 : penaltiesField.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryData other = (ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryData) obj;
		if (penaltiesField == null) {
			if (other.penaltiesField != null)
				return false;
		} else if (!penaltiesField.equals(other.penaltiesField))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryData [penaltiesField="
				+ penaltiesField + "]";
	}
	
}
