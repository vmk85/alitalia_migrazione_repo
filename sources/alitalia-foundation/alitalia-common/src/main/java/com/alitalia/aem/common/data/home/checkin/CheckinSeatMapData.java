package com.alitalia.aem.common.data.home.checkin;

import java.util.List;

public class CheckinSeatMapData{
	
	private List<String> columns;
	private Boolean hasExit;
	private Boolean isExist;
	private Integer number;
	private List<CheckinSeatData> seats;
	private Boolean secondLevel;
	
	public List<String> getColumns() {
		return columns;
	}
	public void setColumns(List<String> columns) {
		this.columns = columns;
	}
	public Boolean getHasExit() {
		return hasExit;
	}
	public void setHasExit(Boolean hasExit) {
		this.hasExit = hasExit;
	}
	public Boolean getIsExist() {
		return isExist;
	}
	public void setIsExist(Boolean isExist) {
		this.isExist = isExist;
	}
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	public List<CheckinSeatData> getSeats() {
		return seats;
	}
	public void setSeats(List<CheckinSeatData> seats) {
		this.seats = seats;
	}
	public Boolean getSecondLevel() {
		return secondLevel;
	}
	public void setSecondLevel(Boolean secondLevel) {
		this.secondLevel = secondLevel;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((columns == null) ? 0 : columns.hashCode());
		result = prime * result + ((hasExit == null) ? 0 : hasExit.hashCode());
		result = prime * result + ((isExist == null) ? 0 : isExist.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((seats == null) ? 0 : seats.hashCode());
		result = prime * result
				+ ((secondLevel == null) ? 0 : secondLevel.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinSeatMapData other = (CheckinSeatMapData) obj;
		if (columns == null) {
			if (other.columns != null)
				return false;
		} else if (!columns.equals(other.columns))
			return false;
		if (hasExit == null) {
			if (other.hasExit != null)
				return false;
		} else if (!hasExit.equals(other.hasExit))
			return false;
		if (isExist == null) {
			if (other.isExist != null)
				return false;
		} else if (!isExist.equals(other.isExist))
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (seats == null) {
			if (other.seats != null)
				return false;
		} else if (!seats.equals(other.seats))
			return false;
		if (secondLevel == null) {
			if (other.secondLevel != null)
				return false;
		} else if (!secondLevel.equals(other.secondLevel))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CheckinSeatMapData [columns=" + columns + ", hasExit="
				+ hasExit + ", isExist=" + isExist + ", number=" + number
				+ ", seats=" + seats + ", secondLevel=" + secondLevel + "]";
	}

}
