package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.data.home.enumerations.MailingListActionTypesEnum;
import com.alitalia.aem.common.messages.BaseRequest;

public class MailinglistRegistrationRequest extends BaseRequest {

	private MailingListActionTypesEnum action;
	private MMCustomerProfileData customerProfile;
	private String bodyWelcomeMail;
	private String subjectMail;
	
	public String getBodyWelcomeMail() {
		return bodyWelcomeMail;
	}
	
	public void setBodyWelcomeMail(String bodyWelcomeMail) {
		this.bodyWelcomeMail = bodyWelcomeMail;
	}
	
	public MailingListActionTypesEnum getAction() {
		return action;
	}
	
	public void setAction(MailingListActionTypesEnum action) {
		this.action = action;
	}
	
	public MMCustomerProfileData getCustomerProfile() {
		return customerProfile;
	}
	
	public void setCustomerProfile(MMCustomerProfileData customerProfile) {
		this.customerProfile = customerProfile;
	}
	
	public String getSubjectMail() {
		return subjectMail;
	}

	public void setSubjectMail(String subjectMail) {
		this.subjectMail = subjectMail;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((action == null) ? 0 : action.hashCode());
		result = prime * result
				+ ((customerProfile == null) ? 0 : customerProfile.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MailinglistRegistrationRequest other = (MailinglistRegistrationRequest) obj;
		if (action != other.action)
			return false;
		if (customerProfile == null) {
			if (other.customerProfile != null)
				return false;
		} else if (!customerProfile.equals(other.customerProfile))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MailingListRegistrationRequest [action=" + action
				+ ", customerProfile=" + customerProfile + "]";
	}
}