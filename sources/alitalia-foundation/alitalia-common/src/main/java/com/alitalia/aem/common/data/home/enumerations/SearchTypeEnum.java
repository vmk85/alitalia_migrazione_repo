package com.alitalia.aem.common.data.home.enumerations;

public enum SearchTypeEnum {

	BRAND_SEARCH("BrandSearch"),
	RIBBON_SEARCH("RibbonSearch"),
	FLEX_DATE_SEARCH("FlexDateSearch"),
	CALENDAR_SEARCH("CalendarSearch"),
	MULTI_DESTINATION_SEARCH("MultiDestinationSearch"),
	MULTI_SLICE_SEARCH("MultiSliceSearch"),
	YOUTH_SEARCH("YouthSearch"),
	CARNET_SEARCH("CarnetSearch"),
	CARNET_FLEX_DATE_SEARCH("CarnetFlexDateSearch"),
	CLOSED_USER_GROUP_SEARCH("ClosedUserGroupSearch"),
	JUMP_UP_SEARCH("JumpUpSearch"),
	META_SEARCH("MetaSearch"),
    AWARD_SEARCH("AwardFlightSearch"),
    AWARD_CALENDAR_SEARCH("AwardCalendarSearch"),
    AWARD_FLIGHT_SEARCH("AwardFlightSearch"),
    AWARD_FLIGHT_DETAIL_SEARCH("AwardFlightDetailSearch"),
	FLEX_DATE_MATRIX_SEARCH("FlexDateMatrixSearch");
	
    private final String value;

    SearchTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SearchTypeEnum fromValue(String v) {
        for (SearchTypeEnum c: SearchTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
