package com.alitalia.aem.common.data.home;

import java.util.ArrayList;

import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;

public class RouteData {

	private ArrayList<FlightData> flights;
	private Integer index;
	private RouteTypeEnum type;

	public ArrayList<FlightData> getFlights() {
		return flights;
	}

	public void setFlights(ArrayList<FlightData> flights) {
		this.flights = flights;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public RouteTypeEnum getType() {
		return type;
	}

	public void setType(RouteTypeEnum type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "RouteData [flights=" + flights + ", index=" + index + ", type="
				+ type + "]";
	}

}
