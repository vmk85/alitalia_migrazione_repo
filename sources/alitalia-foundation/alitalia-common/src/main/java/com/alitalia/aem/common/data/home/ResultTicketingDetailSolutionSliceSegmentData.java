package com.alitalia.aem.common.data.home;

import java.util.List;

public class ResultTicketingDetailSolutionSliceSegmentData {

    protected List<ResultTicketingDetailSolutionSliceSegmentBookingInfoData> bookingInfoField;
    protected ResultTicketingDetailSolutionSliceSegmentConnectionData connectionField;
    protected String departureField;
    protected String destinationField;
    protected ResultTicketingDetailSolutionSliceSegmentFlightData flightField;
    protected String hashField;
    protected List<ResultTicketingDetailSolutionSliceSegmentLegData> legField;
    protected String originField;
    protected List<ResultTicketingDetailSolutionSliceSegmentPricingFareData> pricingField;

    public List<ResultTicketingDetailSolutionSliceSegmentBookingInfoData> getBookingInfoField() {
        return bookingInfoField;
    }

    public void setBookingInfoField(List<ResultTicketingDetailSolutionSliceSegmentBookingInfoData> value) {
        this.bookingInfoField = value;
    }

    public ResultTicketingDetailSolutionSliceSegmentConnectionData getConnectionField() {
        return connectionField;
    }

    public void setConnectionField(ResultTicketingDetailSolutionSliceSegmentConnectionData value) {
        this.connectionField = value;
    }

    public String getDepartureField() {
        return departureField;
    }

    public void setDepartureField(String value) {
        this.departureField = value;
    }

    public String getDestinationField() {
        return destinationField;
    }

    public void setDestinationField(String value) {
        this.destinationField = value;
    }

    public ResultTicketingDetailSolutionSliceSegmentFlightData getFlightField() {
        return flightField;
    }

    public void setFlightField(ResultTicketingDetailSolutionSliceSegmentFlightData value) {
        this.flightField = value;
    }

    public String getHashField() {
        return hashField;
    }

    public void setHashField(String value) {
        this.hashField = value;
    }

    public List<ResultTicketingDetailSolutionSliceSegmentLegData> getLegField() {
        return legField;
    }

    public void setLegField(List<ResultTicketingDetailSolutionSliceSegmentLegData> value) {
        this.legField = value;
    }

    public String getOriginField() {
        return originField;
    }

    public void setOriginField(String value) {
        this.originField = value;
    }

    public List<ResultTicketingDetailSolutionSliceSegmentPricingFareData> getPricingField() {
        return pricingField;
    }

    public void setPricingField(List<ResultTicketingDetailSolutionSliceSegmentPricingFareData> value) {
        this.pricingField = value;
    }

}
