package com.alitalia.aem.common.messages.home;
import java.util.List;

import com.alitalia.aem.common.data.home.SeatTypeData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetrieveSeatTypeResponse extends BaseResponse{
	List<SeatTypeData> seatTypesData;

	public List<SeatTypeData> getSeatTypesData() {
		return seatTypesData;
	}

	public void setSeatTypesData(List<SeatTypeData> seatTypesData) {
		this.seatTypesData = seatTypesData;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((seatTypesData == null) ? 0 : seatTypesData.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveSeatTypeResponse other = (RetrieveSeatTypeResponse) obj;
		if (seatTypesData == null) {
			if (other.seatTypesData != null)
				return false;
		} else if (!seatTypesData.equals(other.seatTypesData))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveSeatTypeResponse [seatTypesData=" + seatTypesData + "]";
	}
	

}
