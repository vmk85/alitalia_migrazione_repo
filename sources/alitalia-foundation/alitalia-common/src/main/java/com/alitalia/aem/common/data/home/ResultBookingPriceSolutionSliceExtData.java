package com.alitalia.aem.common.data.home;

public class ResultBookingPriceSolutionSliceExtData {
	
	private String fareBrandField;
	private ResultBookingPriceSolutionSliceExtPriceData priceField;
	private ResultBookingPriceSolutionSliceExtRoundedPriceData roundedPriceField;
	
	public String getFareBrandField() {
		return fareBrandField;
	}
	
	public void setFareBrandField(String fareBrandField) {
		this.fareBrandField = fareBrandField;
	}
	
	public ResultBookingPriceSolutionSliceExtPriceData getPriceField() {
		return priceField;
	}
	
	public void setPriceField(
			ResultBookingPriceSolutionSliceExtPriceData priceField) {
		this.priceField = priceField;
	}
	
	public ResultBookingPriceSolutionSliceExtRoundedPriceData getRoundedPriceField() {
		return roundedPriceField;
	}
	
	public void setRoundedPriceField(
			ResultBookingPriceSolutionSliceExtRoundedPriceData roundedPriceField) {
		this.roundedPriceField = roundedPriceField;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((fareBrandField == null) ? 0 : fareBrandField.hashCode());
		result = prime * result
				+ ((priceField == null) ? 0 : priceField.hashCode());
		result = prime
				* result
				+ ((roundedPriceField == null) ? 0 : roundedPriceField
						.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingPriceSolutionSliceExtData other = (ResultBookingPriceSolutionSliceExtData) obj;
		if (fareBrandField == null) {
			if (other.fareBrandField != null)
				return false;
		} else if (!fareBrandField.equals(other.fareBrandField))
			return false;
		if (priceField == null) {
			if (other.priceField != null)
				return false;
		} else if (!priceField.equals(other.priceField))
			return false;
		if (roundedPriceField == null) {
			if (other.roundedPriceField != null)
				return false;
		} else if (!roundedPriceField.equals(other.roundedPriceField))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingPriceSolutionSliceExtData [fareBrandField="
				+ fareBrandField + ", priceField=" + priceField
				+ ", roundedPriceField=" + roundedPriceField + "]";
	}
	
}
