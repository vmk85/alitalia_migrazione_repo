package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class UserFromSocialResponse extends BaseResponse {

	private Boolean successful;
    private String userCode;
    private String pin;
    private String ssoToken;

	public UserFromSocialResponse() {
		super();
		this.successful = false;
	}

	public UserFromSocialResponse(Boolean successful, String userCode, String pin, String ssoToken) {
		super();
		this.successful = successful;
		this.userCode = userCode;
		this.pin = pin;
		this.ssoToken = ssoToken;
	}

	public UserFromSocialResponse(String tid, String sid,
			Boolean successful, String userCode, String pin, String ssoToken) {
		super(tid, sid);
		this.successful = successful;
		this.userCode = userCode;
		this.pin = pin;
		this.ssoToken = ssoToken;
	}

	public Boolean isSuccessful() {
		return successful;
	}
	
	public void setSuccessful(Boolean successful) {
		this.successful = successful;
	}
	
	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getSsoToken() {
		return ssoToken;
	}

	public void setSsoToken(String ssoToken) {
		this.ssoToken = ssoToken;
	}
	
	public Boolean getSuccessful() {
		return successful;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((pin == null) ? 0 : pin.hashCode());
		result = prime * result + ((ssoToken == null) ? 0 : ssoToken.hashCode());
		result = prime * result + ((successful == null) ? 0 : successful.hashCode());
		result = prime * result + ((userCode == null) ? 0 : userCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserFromSocialResponse other = (UserFromSocialResponse) obj;
		if (pin == null) {
			if (other.pin != null)
				return false;
		} else if (!pin.equals(other.pin))
			return false;
		if (ssoToken == null) {
			if (other.ssoToken != null)
				return false;
		} else if (!ssoToken.equals(other.ssoToken))
			return false;
		if (successful == null) {
			if (other.successful != null)
				return false;
		} else if (!successful.equals(other.successful))
			return false;
		if (userCode == null) {
			if (other.userCode != null)
				return false;
		} else if (!userCode.equals(other.userCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UserFromSocialResponse [successful=" + successful + ", userCode=" + userCode + ", pin=" + pin
				+ ", ssoToken=" + ssoToken + "]";
	}

}
