package com.alitalia.aem.common.data.home.checkin;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbFlightStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbLegTypeEnum;
import com.alitalia.aem.common.data.home.mmb.MmbDeepLinkData;

public class CheckinFlightData {
	
	private Integer id;
	private Calendar arrivalDateTime;
	private List<MmbCompartimentalClassEnum> cabins;
	private String carrier;
	private BigDecimal comfortSeatFare;
	private Boolean comfortSeatPaid;
	private MmbDeepLinkData deepLinkCode;
	private Calendar departureDateTime;
	private String eticketClass;
	private String eticket;
	private String flightNumber;
	private CheckinAirport from;
	private Boolean hasComfortSeat;
	private Integer index;
	private MmbLegTypeEnum legType;
	private String operatingCarrier;
	private String operatingFlightNumber;
	private String pnr;
	private String rph;
	private Integer routeId;
	private MmbCompartimentalClassEnum seatClass;
	private MmbFlightStatusEnum status;
	private CheckinAirport to;
	private boolean bus;
	private boolean fromBusStation;
	private boolean toBusStation;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Calendar getArrivalDateTime() {
		return arrivalDateTime;
	}
	public void setArrivalDateTime(Calendar arrivalDateTime) {
		this.arrivalDateTime = arrivalDateTime;
	}
	public List<MmbCompartimentalClassEnum> getCabins() {
		return cabins;
	}
	public void setCabins(List<MmbCompartimentalClassEnum> cabins) {
		this.cabins = cabins;
	}
	public String getCarrier() {
		return carrier;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	public BigDecimal getComfortSeatFare() {
		return comfortSeatFare;
	}
	public void setComfortSeatFare(BigDecimal comfortSeatFare) {
		this.comfortSeatFare = comfortSeatFare;
	}
	public Boolean getComfortSeatPaid() {
		return comfortSeatPaid;
	}
	public void setComfortSeatPaid(Boolean comfortSeatPaid) {
		this.comfortSeatPaid = comfortSeatPaid;
	}
	public MmbDeepLinkData getDeepLinkCode() {
		return deepLinkCode;
	}
	public void setDeepLinkCode(MmbDeepLinkData deepLinkCode) {
		this.deepLinkCode = deepLinkCode;
	}
	public Calendar getDepartureDateTime() {
		return departureDateTime;
	}
	public void setDepartureDateTime(Calendar departureDateTime) {
		this.departureDateTime = departureDateTime;
	}
	public String getEticketClass() {
		return eticketClass;
	}
	public void setEticketClass(String eticketClass) {
		this.eticketClass = eticketClass;
	}
	public String getEticket() {
		return eticket;
	}
	public void setEticket(String eticket) {
		this.eticket = eticket;
	}
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	public CheckinAirport getFrom() {
		return from;
	}
	public void setFrom(CheckinAirport from) {
		this.from = from;
	}
	public Boolean getHasComfortSeat() {
		return hasComfortSeat;
	}
	public void setHasComfortSeat(Boolean hasComfortSeat) {
		this.hasComfortSeat = hasComfortSeat;
	}
	public Integer getIndex() {
		return index;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	public MmbLegTypeEnum getLegType() {
		return legType;
	}
	public void setLegType(MmbLegTypeEnum legType) {
		this.legType = legType;
	}
	public String getOperatingCarrier() {
		return operatingCarrier;
	}
	public void setOperatingCarrier(String operatingCarrier) {
		this.operatingCarrier = operatingCarrier;
	}
	public String getOperatingFlightNumber() {
		return operatingFlightNumber;
	}
	public void setOperatingFlightNumber(String operatingFlightNumber) {
		this.operatingFlightNumber = operatingFlightNumber;
	}
	public String getPnr() {
		return pnr;
	}
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	public String getRph() {
		return rph;
	}
	public void setRph(String rph) {
		this.rph = rph;
	}
	public Integer getRouteId() {
		return routeId;
	}
	public void setRouteId(Integer routeId) {
		this.routeId = routeId;
	}
	public MmbCompartimentalClassEnum getSeatClass() {
		return seatClass;
	}
	public void setSeatClass(MmbCompartimentalClassEnum seatClass) {
		this.seatClass = seatClass;
	}
	public MmbFlightStatusEnum getStatus() {
		return status;
	}
	public void setStatus(MmbFlightStatusEnum status) {
		this.status = status;
	}
	public CheckinAirport getTo() {
		return to;
	}
	public void setTo(CheckinAirport to) {
		this.to = to;
	}
	
	public boolean isBus() {
		return bus;
	}

	public void setBus(boolean bus) {
		this.bus = bus;
	}
	
	public boolean isFromBusStation() {
		return fromBusStation;
	}

	public void setFromBusStation(boolean fromBusStation) {
		this.fromBusStation = fromBusStation;
	}
	
	public boolean isToBusStation() {
		return toBusStation;
	}

	public void setToBusStation(boolean toBusStation) {
		this.toBusStation = toBusStation;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((hasComfortSeat == null) ? 0 : hasComfortSeat.hashCode());
		result = prime * result
				+ ((arrivalDateTime == null) ? 0 : arrivalDateTime.hashCode());
		result = prime * result + ((cabins == null) ? 0 : cabins.hashCode());
		result = prime * result + ((carrier == null) ? 0 : carrier.hashCode());
		result = prime * result
				+ ((comfortSeatFare == null) ? 0 : comfortSeatFare.hashCode());
		result = prime * result
				+ ((comfortSeatPaid == null) ? 0 : comfortSeatPaid.hashCode());
		result = prime * result
				+ ((deepLinkCode == null) ? 0 : deepLinkCode.hashCode());
		result = prime
				* result
				+ ((departureDateTime == null) ? 0 : departureDateTime
						.hashCode());
		result = prime * result + ((eticket == null) ? 0 : eticket.hashCode());
		result = prime * result
				+ ((eticketClass == null) ? 0 : eticketClass.hashCode());
		result = prime * result
				+ ((flightNumber == null) ? 0 : flightNumber.hashCode());
		result = prime * result + ((from == null) ? 0 : from.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((index == null) ? 0 : index.hashCode());
		result = prime * result + ((legType == null) ? 0 : legType.hashCode());
		result = prime
				* result
				+ ((operatingCarrier == null) ? 0 : operatingCarrier.hashCode());
		result = prime
				* result
				+ ((operatingFlightNumber == null) ? 0 : operatingFlightNumber
						.hashCode());
		result = prime * result + ((pnr == null) ? 0 : pnr.hashCode());
		result = prime * result + ((routeId == null) ? 0 : routeId.hashCode());
		result = prime * result + ((rph == null) ? 0 : rph.hashCode());
		result = prime * result
				+ ((seatClass == null) ? 0 : seatClass.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((to == null) ? 0 : to.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinFlightData other = (CheckinFlightData) obj;
		if (hasComfortSeat == null) {
			if (other.hasComfortSeat != null)
				return false;
		} else if (!hasComfortSeat.equals(other.hasComfortSeat))
			return false;
		if (arrivalDateTime == null) {
			if (other.arrivalDateTime != null)
				return false;
		} else if (!arrivalDateTime.equals(other.arrivalDateTime))
			return false;
		if (cabins == null) {
			if (other.cabins != null)
				return false;
		} else if (!cabins.equals(other.cabins))
			return false;
		if (carrier == null) {
			if (other.carrier != null)
				return false;
		} else if (!carrier.equals(other.carrier))
			return false;
		if (comfortSeatFare == null) {
			if (other.comfortSeatFare != null)
				return false;
		} else if (!comfortSeatFare.equals(other.comfortSeatFare))
			return false;
		if (comfortSeatPaid == null) {
			if (other.comfortSeatPaid != null)
				return false;
		} else if (!comfortSeatPaid.equals(other.comfortSeatPaid))
			return false;
		if (deepLinkCode == null) {
			if (other.deepLinkCode != null)
				return false;
		} else if (!deepLinkCode.equals(other.deepLinkCode))
			return false;
		if (departureDateTime == null) {
			if (other.departureDateTime != null)
				return false;
		} else if (!departureDateTime.equals(other.departureDateTime))
			return false;
		if (eticket == null) {
			if (other.eticket != null)
				return false;
		} else if (!eticket.equals(other.eticket))
			return false;
		if (eticketClass == null) {
			if (other.eticketClass != null)
				return false;
		} else if (!eticketClass.equals(other.eticketClass))
			return false;
		if (flightNumber == null) {
			if (other.flightNumber != null)
				return false;
		} else if (!flightNumber.equals(other.flightNumber))
			return false;
		if (from == null) {
			if (other.from != null)
				return false;
		} else if (!from.equals(other.from))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (index == null) {
			if (other.index != null)
				return false;
		} else if (!index.equals(other.index))
			return false;
		if (legType != other.legType)
			return false;
		if (operatingCarrier == null) {
			if (other.operatingCarrier != null)
				return false;
		} else if (!operatingCarrier.equals(other.operatingCarrier))
			return false;
		if (operatingFlightNumber == null) {
			if (other.operatingFlightNumber != null)
				return false;
		} else if (!operatingFlightNumber.equals(other.operatingFlightNumber))
			return false;
		if (pnr == null) {
			if (other.pnr != null)
				return false;
		} else if (!pnr.equals(other.pnr))
			return false;
		if (routeId == null) {
			if (other.routeId != null)
				return false;
		} else if (!routeId.equals(other.routeId))
			return false;
		if (rph == null) {
			if (other.rph != null)
				return false;
		} else if (!rph.equals(other.rph))
			return false;
		if (seatClass != other.seatClass)
			return false;
		if (status != other.status)
			return false;
		if (to == null) {
			if (other.to != null)
				return false;
		} else if (!to.equals(other.to))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "MmbFlightData [id=" + id + ", arrivalDateTime="
				+ arrivalDateTime + ", cabins=" + cabins + ", carrier="
				+ carrier + ", comfortSeatFare=" + comfortSeatFare
				+ ", comfortSeatPaid=" + comfortSeatPaid + ", deepLinkCode="
				+ deepLinkCode + ", departureDateTime=" + departureDateTime
				+ ", eticketClass=" + eticketClass + ", eticket=" + eticket
				+ ", flightNumber=" + flightNumber + ", from=" + from
				+ ", hasComfortSeat=" + hasComfortSeat + ", index=" + index
				+ ", legType=" + legType + ", operatingCarrier="
				+ operatingCarrier + ", operatingFlightNumber="
				+ operatingFlightNumber + ", pnr=" + pnr + ", rph=" + rph
				+ ", routeId=" + routeId + ", seatClass=" + seatClass
				+ ", status=" + status + ", to=" + to + "]";
	}
}
