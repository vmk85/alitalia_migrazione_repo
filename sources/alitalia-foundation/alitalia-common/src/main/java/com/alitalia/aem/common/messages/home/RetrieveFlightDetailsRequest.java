package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.FlightDetailsModelData;
import com.alitalia.aem.common.messages.BaseRequest;

public class RetrieveFlightDetailsRequest extends BaseRequest {

	private FlightDetailsModelData flightDetailsModelData;

	public FlightDetailsModelData getFlightDetailsModelData() {
		return flightDetailsModelData;
	}

	public void setFlightDetailsModelData(
			FlightDetailsModelData flightDetailsModelData) {
		this.flightDetailsModelData = flightDetailsModelData;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((flightDetailsModelData == null) ? 0
						: flightDetailsModelData.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveFlightDetailsRequest other = (RetrieveFlightDetailsRequest) obj;
		if (flightDetailsModelData == null) {
			if (other.flightDetailsModelData != null)
				return false;
		} else if (!flightDetailsModelData.equals(other.flightDetailsModelData))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveFlightDetailsRequest [flightDetailsModelData="
				+ flightDetailsModelData + "]";
	}
}