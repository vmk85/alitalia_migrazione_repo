package com.alitalia.aem.common.data.home.enumerations;

public enum MmbFlightTypeEnum {

    DOMESTIC("Domestic"),
    INTERCONTINENTAL("Intercontinental"),
    INTERNATIONAL("International");
    private final String value;

    MmbFlightTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MmbFlightTypeEnum fromValue(String v) {
        for (MmbFlightTypeEnum c: MmbFlightTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
