package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class LoadWidgetOneClickRequest extends BaseRequest {
	
	private String site;
    private String type;
    private String lastName;
    private String mmCode;
    private String mmPin;
	
	public LoadWidgetOneClickRequest(String tid, String sid) {
		super(tid, sid);
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMmCode() {
		return mmCode;
	}

	public void setMmCode(String mmCode) {
		this.mmCode = mmCode;
	}

	public String getMmPin() {
		return mmPin;
	}

	public void setMmPin(String mmPin) {
		this.mmPin = mmPin;
	}

	@Override
	public String toString() {
		return "LoadWidgetOneClickRequest [site=" + site + ", type=" + type
				+ ", lastName=" + lastName + ", mmCode=" + mmCode + ", mmPin="
				+ mmPin + ", getSite()=" + getSite() + ", getType()="
				+ getType() + ", getLastName()=" + getLastName()
				+ ", getMmCode()=" + getMmCode() + ", getMmPin()=" + getMmPin()
				+ ", getTid()=" + getTid() + ", getSid()=" + getSid()
				+ ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass() + "]";
	}
}
