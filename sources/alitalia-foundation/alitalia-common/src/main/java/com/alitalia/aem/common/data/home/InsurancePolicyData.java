package com.alitalia.aem.common.data.home;

import java.math.BigDecimal;

public class InsurancePolicyData {
	
	protected Boolean buy;
	protected String cardType;
	protected String currency;
	protected BigDecimal discount;
	protected BigDecimal discountedPrice;
	protected String errorDescription;
	protected String policyNumber;
	protected BigDecimal totalInsuranceCost;
	protected String typeOfTrip;
	
	public Boolean getBuy() {
		return buy;
	}
	
	public void setBuy(Boolean buy) {
		this.buy = buy;
	}
	
	public String getCardType() {
		return cardType;
	}
	
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	
	public String getCurrency() {
		return currency;
	}
	
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public BigDecimal getDiscount() {
		return discount;
	}
	
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	
	public BigDecimal getDiscountedPrice() {
		return discountedPrice;
	}
	
	public void setDiscountedPrice(BigDecimal discountedPrice) {
		this.discountedPrice = discountedPrice;
	}
	
	public String getErrorDescription() {
		return errorDescription;
	}
	
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	
	public String getPolicyNumber() {
		return policyNumber;
	}
	
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	
	public BigDecimal getTotalInsuranceCost() {
		return totalInsuranceCost;
	}
	
	public void setTotalInsuranceCost(BigDecimal totalInsuranceCost) {
		this.totalInsuranceCost = totalInsuranceCost;
	}
	
	public String getTypeOfTrip() {
		return typeOfTrip;
	}
	
	public void setTypeOfTrip(String typeOfTrip) {
		this.typeOfTrip = typeOfTrip;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((buy == null) ? 0 : buy.hashCode());
		result = prime * result
				+ ((cardType == null) ? 0 : cardType.hashCode());
		result = prime * result
				+ ((currency == null) ? 0 : currency.hashCode());
		result = prime * result
				+ ((discount == null) ? 0 : discount.hashCode());
		result = prime * result
				+ ((discountedPrice == null) ? 0 : discountedPrice.hashCode());
		result = prime
				* result
				+ ((errorDescription == null) ? 0 : errorDescription.hashCode());
		result = prime * result
				+ ((policyNumber == null) ? 0 : policyNumber.hashCode());
		result = prime
				* result
				+ ((totalInsuranceCost == null) ? 0 : totalInsuranceCost
						.hashCode());
		result = prime * result
				+ ((typeOfTrip == null) ? 0 : typeOfTrip.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InsurancePolicyData other = (InsurancePolicyData) obj;
		if (buy == null) {
			if (other.buy != null)
				return false;
		} else if (!buy.equals(other.buy))
			return false;
		if (cardType == null) {
			if (other.cardType != null)
				return false;
		} else if (!cardType.equals(other.cardType))
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (discount == null) {
			if (other.discount != null)
				return false;
		} else if (!discount.equals(other.discount))
			return false;
		if (discountedPrice == null) {
			if (other.discountedPrice != null)
				return false;
		} else if (!discountedPrice.equals(other.discountedPrice))
			return false;
		if (errorDescription == null) {
			if (other.errorDescription != null)
				return false;
		} else if (!errorDescription.equals(other.errorDescription))
			return false;
		if (policyNumber == null) {
			if (other.policyNumber != null)
				return false;
		} else if (!policyNumber.equals(other.policyNumber))
			return false;
		if (totalInsuranceCost == null) {
			if (other.totalInsuranceCost != null)
				return false;
		} else if (!totalInsuranceCost.equals(other.totalInsuranceCost))
			return false;
		if (typeOfTrip == null) {
			if (other.typeOfTrip != null)
				return false;
		} else if (!typeOfTrip.equals(other.typeOfTrip))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "InsurancePolicyData [buy=" + buy + ", cardType=" + cardType
				+ ", currency=" + currency + ", discount=" + discount
				+ ", discountedPrice=" + discountedPrice
				+ ", errorDescription=" + errorDescription + ", policyNumber="
				+ policyNumber + ", totalInsuranceCost=" + totalInsuranceCost
				+ ", typeOfTrip=" + typeOfTrip + "]";
	}
	
}