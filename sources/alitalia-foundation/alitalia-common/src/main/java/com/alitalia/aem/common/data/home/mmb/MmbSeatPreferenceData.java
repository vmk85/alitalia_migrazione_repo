package com.alitalia.aem.common.data.home.mmb;

import com.alitalia.aem.common.data.home.SeatPreferencesData;
import com.alitalia.aem.common.data.home.enumerations.MmbUpdateSSREnum;

public class MmbSeatPreferenceData extends SeatPreferencesData {

	private String oldNumber;
	private String oldRow;
	private String travelerRefNumber;
	private String updateFlightRefNumberRPH;
	private Boolean updating;
	private Boolean comfort;
	private Boolean pending;
	private MmbUpdateSSREnum update;

	public MmbSeatPreferenceData() {
		super();
	}

	public MmbSeatPreferenceData(SeatPreferencesData clone) {
		super(clone);
	}

	public MmbSeatPreferenceData(MmbSeatPreferenceData clone) {
		this.setCabinSeat(clone.getCabinSeat());
		this.setFlightCarrier(clone.getFlightCarrier());
		this.setFlightNumber(clone.getFlightNumber());
		this.setNumber(clone.getNumber());
		this.setRow(clone.getRow());

		this.oldNumber = clone.getOldNumber();
		this.oldRow = clone.getOldRow();
		this.travelerRefNumber = clone.getTravelerRefNumber();
		this.updateFlightRefNumberRPH = clone.getUpdateFlightRefNumberRPH();
		this.updating = clone.isUpdating();
		this.comfort = clone.isComfort();
		this.pending = clone.isPending();
		this.update = clone.getUpdate();
	}

	public Boolean isComfort() {
		return comfort;
	}

	public void setComfort(Boolean comfort) {
		this.comfort = comfort;
	}

	public String getOldNumber() {
		return oldNumber;
	}

	public void setOldNumber(String oldNumber) {
		this.oldNumber = oldNumber;
	}

	public String getOldRow() {
		return oldRow;
	}

	public void setOldRow(String oldRow) {
		this.oldRow = oldRow;
	}

	public String getTravelerRefNumber() {
		return travelerRefNumber;
	}

	public void setTravelerRefNumber(String travelerRefNumber) {
		this.travelerRefNumber = travelerRefNumber;
	}

	public String getUpdateFlightRefNumberRPH() {
		return updateFlightRefNumberRPH;
	}

	public void setUpdateFlightRefNumberRPH(String updateFlightRefNumberRPH) {
		this.updateFlightRefNumberRPH = updateFlightRefNumberRPH;
	}

	public Boolean isUpdating() {
		return updating;
	}

	public void setUpdating(Boolean updating) {
		this.updating = updating;
	}

	public Boolean isPending() {
		return pending;
	}

	public void setPending(Boolean pending) {
		this.pending = pending;
	}

	public MmbUpdateSSREnum getUpdate() {
		return update;
	}

	public void setUpdate(MmbUpdateSSREnum update) {
		this.update = update;
	}

	@Override
	public String toString() {
		return "MmbSeatPreferenceData [oldNumber=" + oldNumber + ", oldRow="
				+ oldRow + ", travelerRefNumber=" + travelerRefNumber
				+ ", updateFlightRefNumberRPH=" + updateFlightRefNumberRPH
				+ ", updating=" + updating + ", comfort=" + comfort
				+ ", pending=" + pending + ", update=" + update
				+ ", getCabinSeat()=" + getCabinSeat()
				+ ", getFlightCarrier()=" + getFlightCarrier()
				+ ", getFlightNumber()=" + getFlightNumber() + ", getNumber()="
				+ getNumber() + ", getRow()=" + getRow() + "]";
	}

}
