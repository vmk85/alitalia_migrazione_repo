package com.alitalia.aem.common.data.home;

import java.util.List;

public class TicketOfficeData {
	private List<String> descriptions;
	private String linkUrl;
	
	public List<String> getDescriptions() {
		return descriptions;
	}
	
	public void setDescriptions(List<String> descriptions) {
		this.descriptions = descriptions;
	}
	
	public String getLinkUrl() {
		return linkUrl;
	}
	
	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((descriptions == null) ? 0 : descriptions.hashCode());
		result = prime * result + ((linkUrl == null) ? 0 : linkUrl.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TicketOfficeData other = (TicketOfficeData) obj;
		if (descriptions == null) {
			if (other.descriptions != null)
				return false;
		} else if (!descriptions.equals(other.descriptions))
			return false;
		if (linkUrl == null) {
			if (other.linkUrl != null)
				return false;
		} else if (!linkUrl.equals(other.linkUrl))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TicketOfficeData [descriptions=" + descriptions + ", linkUrl="
				+ linkUrl + "]";
	}
	
}
