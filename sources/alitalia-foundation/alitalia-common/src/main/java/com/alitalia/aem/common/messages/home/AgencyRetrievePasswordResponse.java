package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.AlitaliaTradeAgencyData;
import com.alitalia.aem.common.messages.BaseResponse;

public class AgencyRetrievePasswordResponse extends BaseResponse {

	private AlitaliaTradeAgencyData agencyData;
	private Boolean retrieveSuccessful;
	private Boolean passwordExpired;
	private Boolean accountLocked;

	public AgencyRetrievePasswordResponse() {
		super();
		this.retrieveSuccessful = false;
	}

	public AgencyRetrievePasswordResponse(AlitaliaTradeAgencyData agencyData, Boolean retrieveSuccessful,
			Boolean passwordExpired, Boolean accountLocked) {
		super();
		this.agencyData = agencyData;
		this.retrieveSuccessful = retrieveSuccessful;
		this.passwordExpired = passwordExpired;
		this.accountLocked = accountLocked;
	}

	public AgencyRetrievePasswordResponse(String tid, String sid,
			AlitaliaTradeAgencyData agencyData, Boolean retrieveSuccessful,
			Boolean passwordExpired, Boolean accountLocked) {
		super(tid, sid);
		this.agencyData = agencyData;
		this.retrieveSuccessful = retrieveSuccessful;
		this.passwordExpired = passwordExpired;
		this.accountLocked = accountLocked;
	}

	public AlitaliaTradeAgencyData getAgencyData() {
		return agencyData;
	}

	public void setAgencyData(AlitaliaTradeAgencyData agencyData) {
		this.agencyData = agencyData;
	}

	public Boolean isRetrieveSuccessful() {
		return retrieveSuccessful;
	}

	public void setRetrieveSuccessful(Boolean retrieveSuccessful) {
		this.retrieveSuccessful = retrieveSuccessful;
	}

	public Boolean isPasswordExpired() {
		return passwordExpired;
	}

	public void setPasswordExpired(Boolean passwordExpired) {
		this.passwordExpired = passwordExpired;
	}

	public Boolean isAccountLocked() {
		return accountLocked;
	}

	public void setAccountLocked(Boolean accountLocked) {
		this.accountLocked = accountLocked;
	}

	@Override
	public String toString() {
		return "AgencyRetrievePasswordByIDResponse [agencyData=" + agencyData
				+ ", retrieveSuccessful=" + retrieveSuccessful
				+ ", passwordExpired=" + passwordExpired + ", accountLocked="
				+ accountLocked + "]";
	}
}
