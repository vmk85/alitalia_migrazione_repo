package com.alitalia.aem.common.data.home.checkin;


public class CheckinUpgradeCabinInfoData extends CheckinBaseTransactionInfoData {

	private CheckinUpgradeCabinOrderData order;
	private CheckinPassengerData passenger;
	
	public CheckinUpgradeCabinOrderData getOrder() {
		return order;
	}
	public void setOrder(CheckinUpgradeCabinOrderData order) {
		this.order = order;
	}
	public CheckinPassengerData getPassenger() {
		return passenger;
	}
	public void setPassenger(CheckinPassengerData passenger) {
		this.passenger = passenger;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((order == null) ? 0 : order.hashCode());
		result = prime * result
				+ ((passenger == null) ? 0 : passenger.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinUpgradeCabinInfoData other = (CheckinUpgradeCabinInfoData) obj;
		if (order == null) {
			if (other.order != null)
				return false;
		} else if (!order.equals(other.order))
			return false;
		if (passenger == null) {
			if (other.passenger != null)
				return false;
		} else if (!passenger.equals(other.passenger))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CheckinUpgradeCabinInfoData [order=" + order + ", passenger="
				+ passenger + ", toString()=" + super.toString() + "]";
	}

}
