package com.alitalia.aem.common.data.home.checkin;

public abstract class CheckinBaseTransactionInfoData {

	private CheckinPaymentData paymentInfo;
	private CheckinPaymentReceiptData receiptInfo;
	
	public CheckinPaymentData getPaymentInfo() {
		return paymentInfo;
	}
	public void setPaymentInfo(CheckinPaymentData paymentInfo) {
		this.paymentInfo = paymentInfo;
	}
	public CheckinPaymentReceiptData getReceiptInfo() {
		return receiptInfo;
	}
	public void setReceiptInfo(CheckinPaymentReceiptData receiptInfo) {
		this.receiptInfo = receiptInfo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((paymentInfo == null) ? 0 : paymentInfo.hashCode());
		result = prime * result
				+ ((receiptInfo == null) ? 0 : receiptInfo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinBaseTransactionInfoData other = (CheckinBaseTransactionInfoData) obj;
		if (paymentInfo == null) {
			if (other.paymentInfo != null)
				return false;
		} else if (!paymentInfo.equals(other.paymentInfo))
			return false;
		if (receiptInfo == null) {
			if (other.receiptInfo != null)
				return false;
		} else if (!receiptInfo.equals(other.receiptInfo))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CheckinBaseTransactionInfoData [paymentInfo=" + paymentInfo
				+ ", receiptInfo=" + receiptInfo + "]";
	}

}