package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.MMCreditCardData;
import com.alitalia.aem.common.messages.BaseResponse;

public class LoadWidgetOneClickResponse extends BaseResponse {

	private List<MMCreditCardData> creditCards;

	public LoadWidgetOneClickResponse() {
		super();
	}
	
	public LoadWidgetOneClickResponse(String tid, String sid) {
		super(tid, sid);
	}

	public List<MMCreditCardData> getCreditCards() {
		return creditCards;
	}

	public void setCreditCards(List<MMCreditCardData> creditCards) {
		this.creditCards = creditCards;
	}

	@Override
	public String toString() {
		return "LoadWidgetOneClickResponse [creditCards=" + creditCards
				+ ", getCreditCards()=" + getCreditCards() + ", getTid()="
				+ getTid() + ", getSid()=" + getSid() + ", hashCode()="
				+ hashCode() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + "]";
	}
}