package com.alitalia.aem.common.data.home;

public class ResultTicketingDetailSolutionSliceSegmentLegAircraftData {

    private String codeField;
    private String nameField;

    public String getCodeField() {
        return codeField;
    }

    public void setCodeField(String value) {
        this.codeField = value;
    }

    public String getNameField() {
        return nameField;
    }

    public void setNameField(String value) {
        this.nameField = value;
    }

}
