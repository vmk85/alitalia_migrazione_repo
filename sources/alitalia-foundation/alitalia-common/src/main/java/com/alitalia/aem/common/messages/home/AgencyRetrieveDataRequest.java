package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class AgencyRetrieveDataRequest extends BaseRequest {

	private String idAgenzia;
	
	public AgencyRetrieveDataRequest(String tid, String sid) {
		super(tid, sid);
	}

	public AgencyRetrieveDataRequest(String tid, String sid, String idAgenzia) {
		super(tid, sid);
		this.idAgenzia = idAgenzia;
	}

	public String getIdAgenzia() {
		return idAgenzia;
	}
	
	public void setIdAgenzia(String idAgenzia) {
		this.idAgenzia = idAgenzia;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((idAgenzia == null) ? 0 : idAgenzia.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgencyRetrieveDataRequest other = (AgencyRetrieveDataRequest) obj;
		if (idAgenzia == null) {
			if (other.idAgenzia != null)
				return false;
		} else if (!idAgenzia.equals(other.idAgenzia))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AgencyRetrieveDataRequest [idAgenzia=" + idAgenzia + "]";
	}
}
