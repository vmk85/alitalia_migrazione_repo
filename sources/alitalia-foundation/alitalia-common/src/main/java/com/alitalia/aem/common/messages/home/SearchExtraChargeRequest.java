package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;
import com.alitalia.aem.common.messages.SearchExecuteRequestFilter;

public class SearchExtraChargeRequest extends BaseRequest {

	private SearchExecuteRequestFilter filter;

	public SearchExtraChargeRequest(String tid, String sid) {
		super(tid, sid);
	}

	public SearchExecuteRequestFilter getFilter() {
		return filter;
	}

	public void setFilter(SearchExecuteRequestFilter filter) {
		this.filter = filter;
	}

	@Override
	public String toString() {
		return "SearchFlightSolutionRequest [filter=" + filter
				+ ", getTid()=" + getTid() + ", getSid()=" + getSid() + "]";
	}
}
