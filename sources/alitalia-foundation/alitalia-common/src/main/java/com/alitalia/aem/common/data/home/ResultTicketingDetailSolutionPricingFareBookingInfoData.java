package com.alitalia.aem.common.data.home;

public class ResultTicketingDetailSolutionPricingFareBookingInfoData {

    private String bookingCodeField;
    private ResultTicketingDetailSolutionPricingFareBookingInfoSegmentData segmentField;

    public String getBookingCodeField() {
        return bookingCodeField;
    }

    public void setBookingCodeField(String value) {
        this.bookingCodeField = value;
    }

    public ResultTicketingDetailSolutionPricingFareBookingInfoSegmentData getSegmentField() {
        return segmentField;
    }

    public void setSegmentField(ResultTicketingDetailSolutionPricingFareBookingInfoSegmentData value) {
        this.segmentField = value;
    }

}
