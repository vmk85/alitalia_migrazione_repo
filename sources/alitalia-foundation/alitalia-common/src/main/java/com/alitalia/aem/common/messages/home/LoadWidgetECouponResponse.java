package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.ECouponData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.messages.BaseResponse;

public class LoadWidgetECouponResponse extends BaseResponse {

	private ECouponData eCoupon;
	private List<PassengerBaseData> passengers;

	public LoadWidgetECouponResponse() {
		super();
	}
	
	public LoadWidgetECouponResponse(String tid, String sid) {
		super(tid, sid);
	}

	public ECouponData geteCoupon() {
		return eCoupon;
	}

	public void seteCoupon(ECouponData eCoupon) {
		this.eCoupon = eCoupon;
	}

	public List<PassengerBaseData> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<PassengerBaseData> passengers) {
		this.passengers = passengers;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((eCoupon == null) ? 0 : eCoupon.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoadWidgetECouponResponse other = (LoadWidgetECouponResponse) obj;
		if (eCoupon == null) {
			if (other.eCoupon != null)
				return false;
		} else if (!eCoupon.equals(other.eCoupon))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "LoadWidgetECouponResponse [eCoupon=" + eCoupon
				+ ", passengers=" + passengers + "]";
	}
	
}