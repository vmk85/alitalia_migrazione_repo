package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.enumerations.AlitaliaTradeUserType;
import com.alitalia.aem.common.messages.BaseRequest;

public class AgencyPasswordChangeRequest extends BaseRequest {

	private String codiceAgenzia;
	private AlitaliaTradeUserType ruolo;
	private String oldPassword;
	private String newPassword;
	
	public AgencyPasswordChangeRequest(String tid, String sid) {
		super(tid, sid);
	}

	public String getCodiceAgenzia() {
		return codiceAgenzia;
	}
	
	public void setCodiceAgenzia(String codiceAgenzia) {
		this.codiceAgenzia = codiceAgenzia;
	}
	
	public AlitaliaTradeUserType getRuolo() {
		return ruolo;
	}
	
	public void setRuolo(AlitaliaTradeUserType ruolo) {
		this.ruolo = ruolo;
	}
	
	public String getOldPassword() {
		return oldPassword;
	}
	
	public void setOldPassword(String password) {
		this.oldPassword = password;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((codiceAgenzia == null) ? 0 : codiceAgenzia.hashCode());
		result = prime * result + ((ruolo == null) ? 0 : ruolo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgencyPasswordChangeRequest other = (AgencyPasswordChangeRequest) obj;
		if (codiceAgenzia == null) {
			if (other.codiceAgenzia != null)
				return false;
		} else if (!codiceAgenzia.equals(other.codiceAgenzia))
			return false;
		if (ruolo != other.ruolo)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AgencyPasswordChangeRequest [codiceAgenzia=" + codiceAgenzia
				+ ", ruolo=" + ruolo + "]";
	}
}
