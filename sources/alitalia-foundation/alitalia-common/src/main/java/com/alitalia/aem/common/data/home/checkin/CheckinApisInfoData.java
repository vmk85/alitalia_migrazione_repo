package com.alitalia.aem.common.data.home.checkin;

import com.alitalia.aem.common.data.home.enumerations.PassportTypeEnum;
import com.alitalia.aem.common.data.home.mmb.MmbApisInfoData;

public class CheckinApisInfoData extends MmbApisInfoData{
	private PassportTypeEnum passportType;

	public CheckinApisInfoData() {
		super();
	}

	public CheckinApisInfoData(CheckinApisInfoData clone) {
		super();
		this.passportType = clone.passportType;
	}

	public PassportTypeEnum getPassportType() {
		return passportType;
	}

	public void setPassportType(PassportTypeEnum passportType) {
		this.passportType = passportType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((passportType == null) ? 0 : passportType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinApisInfoData other = (CheckinApisInfoData) obj;
		if (passportType != other.passportType)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CheckinApisInfoData [passportType=" + passportType
				+ ", toString()=" + super.toString() + "]";
	}
	
}
