package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.MunicipalityData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetrieveCouncilsResponse extends BaseResponse{

	private List<MunicipalityData> councils;
	
	public RetrieveCouncilsResponse(){}
	
	public RetrieveCouncilsResponse(String tid, String sid) {
		super(tid, sid);
	}

	public List<MunicipalityData> getCouncils() {
		return councils;
	}

	public void setCouncils(List<MunicipalityData> councils) {
		this.councils = councils;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((councils == null) ? 0 : councils.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveCouncilsResponse other = (RetrieveCouncilsResponse) obj;
		if (councils == null) {
			if (other.councils != null)
				return false;
		} else if (!councils.equals(other.councils))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveCouncilsResponse [councils=" + councils + "]";
	}		
}