package com.alitalia.aem.common.data.home.enumerations;


public enum InitializePaymentErrorEnum {

	INFANT_INVENTORY_UNAVAILABLE,
	INSUFFICIENT_PLAFOND,
	INVALID_MERCHANT_ID,
	BLOCKED_MERCHANT_ID;
    
}