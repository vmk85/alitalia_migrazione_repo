package com.alitalia.aem.common.data.home;


public class ChildPassengerData extends PassengerBaseData {

	private PreferencesData preferences;

	public ChildPassengerData() {
		super();
	}
	
	public ChildPassengerData(ChildPassengerData clone) {
		super(clone);
		PreferencesData clonePreferences = clone.getPreferences();
		if (clonePreferences != null)
			this.preferences = new PreferencesData(clonePreferences);
	}

	public ChildPassengerData(PassengerBaseData clone) {
		super(clone);
		this.preferences = null;
	}

	public PreferencesData getPreferences() {
		return preferences;
	}
	
	public void setPreferences(PreferencesData preferences) {
		this.preferences = preferences;
	}
	
	@Override
	public String toString() {
		return "ChildPassengerData [preferences=" + preferences + "]";
	}
}
