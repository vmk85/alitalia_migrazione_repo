package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.enumerations.CheckinEmdReasonForIssuanceType;
import com.alitalia.aem.common.messages.MmbBaseRequest;

public class WebCheckinReasonForIssuanceRequest extends MmbBaseRequest {

	private CheckinEmdReasonForIssuanceType reasonForIssuanceType;

	public WebCheckinReasonForIssuanceRequest(String tid, String sid) {
		super(tid, sid);
	}

	public WebCheckinReasonForIssuanceRequest() {
		super();
	}

	public CheckinEmdReasonForIssuanceType getReasonForIssuanceType() {
		return reasonForIssuanceType;
	}

	public void setReasonForIssuanceType(
			CheckinEmdReasonForIssuanceType reasonForIssuanceType) {
		this.reasonForIssuanceType = reasonForIssuanceType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((reasonForIssuanceType == null) ? 0 : reasonForIssuanceType
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebCheckinReasonForIssuanceRequest other = (WebCheckinReasonForIssuanceRequest) obj;
		if (reasonForIssuanceType != other.reasonForIssuanceType)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WebCheckinReasonForIssuanceRequest [reasonForIssuanceType="
				+ reasonForIssuanceType + "]";
	}
}
