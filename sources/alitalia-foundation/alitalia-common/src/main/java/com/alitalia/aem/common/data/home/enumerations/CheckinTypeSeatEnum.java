package com.alitalia.aem.common.data.home.enumerations;

public enum CheckinTypeSeatEnum {

	NOT_EXIST("NotExist"),
	AISLE("Aisle"),
    NORMAL("Normal");
    
	private final String value;

	CheckinTypeSeatEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CheckinTypeSeatEnum fromValue(String v) {
        for (CheckinTypeSeatEnum c: CheckinTypeSeatEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}