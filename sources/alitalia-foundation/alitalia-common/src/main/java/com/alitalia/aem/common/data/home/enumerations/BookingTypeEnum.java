package com.alitalia.aem.common.data.home.enumerations;

public enum BookingTypeEnum {

    CONSUMER("Consumer"),
    AWARD("Award"),
    TRADE("Trade");
    
	private final String value;

	BookingTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BookingTypeEnum fromValue(String v) {
        for (BookingTypeEnum c: BookingTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
