package com.alitalia.aem.common.data.home;

import com.alitalia.aem.common.data.home.enumerations.MMContractStatusEnum;

public class MMContractData {

	private String name;
	private MMContractStatusEnum status;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MMContractStatusEnum getStatus() {
		return status;
	}

	public void setStatus(MMContractStatusEnum status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MMContractData other = (MMContractData) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (status != other.status)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MMContractData [name=" + name + ", status=" + status + "]";
	}

}
