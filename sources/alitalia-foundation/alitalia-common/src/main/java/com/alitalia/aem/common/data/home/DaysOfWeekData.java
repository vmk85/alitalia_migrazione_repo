package com.alitalia.aem.common.data.home;

public class DaysOfWeekData {

	private Boolean friday;
	private Boolean monday;
	private Boolean saturday;
	private Boolean sunday;
	private Boolean thursday;
	private Boolean tuesday;
	private Boolean wednesday;
	
	public Boolean getFriday() {
		return friday;
	}
	
	public void setFriday(Boolean friday) {
		this.friday = friday;
	}
	
	public Boolean getMonday() {
		return monday;
	}
	
	public void setMonday(Boolean monday) {
		this.monday = monday;
	}
	
	public Boolean getSaturday() {
		return saturday;
	}
	
	public void setSaturday(Boolean saturday) {
		this.saturday = saturday;
	}
	
	public Boolean getSunday() {
		return sunday;
	}
	
	public void setSunday(Boolean sunday) {
		this.sunday = sunday;
	}
	
	public Boolean getThursday() {
		return thursday;
	}
	
	public void setThursday(Boolean thursday) {
		this.thursday = thursday;
	}
	
	public Boolean getTuesday() {
		return tuesday;
	}
	
	public void setTuesday(Boolean tuesday) {
		this.tuesday = tuesday;
	}
	
	public Boolean getWednesday() {
		return wednesday;
	}
	
	public void setWednesday(Boolean wednesday) {
		this.wednesday = wednesday;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((friday == null) ? 0 : friday.hashCode());
		result = prime * result + ((monday == null) ? 0 : monday.hashCode());
		result = prime * result
				+ ((saturday == null) ? 0 : saturday.hashCode());
		result = prime * result + ((sunday == null) ? 0 : sunday.hashCode());
		result = prime * result
				+ ((thursday == null) ? 0 : thursday.hashCode());
		result = prime * result + ((tuesday == null) ? 0 : tuesday.hashCode());
		result = prime * result
				+ ((wednesday == null) ? 0 : wednesday.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DaysOfWeekData other = (DaysOfWeekData) obj;
		if (friday == null) {
			if (other.friday != null)
				return false;
		} else if (!friday.equals(other.friday))
			return false;
		if (monday == null) {
			if (other.monday != null)
				return false;
		} else if (!monday.equals(other.monday))
			return false;
		if (saturday == null) {
			if (other.saturday != null)
				return false;
		} else if (!saturday.equals(other.saturday))
			return false;
		if (sunday == null) {
			if (other.sunday != null)
				return false;
		} else if (!sunday.equals(other.sunday))
			return false;
		if (thursday == null) {
			if (other.thursday != null)
				return false;
		} else if (!thursday.equals(other.thursday))
			return false;
		if (tuesday == null) {
			if (other.tuesday != null)
				return false;
		} else if (!tuesday.equals(other.tuesday))
			return false;
		if (wednesday == null) {
			if (other.wednesday != null)
				return false;
		} else if (!wednesday.equals(other.wednesday))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DayOfWeekData [friday=" + friday + ", monday=" + monday
				+ ", saturday=" + saturday + ", sunday=" + sunday
				+ ", thursday=" + thursday + ", tuesday=" + tuesday
				+ ", wednesday=" + wednesday + "]";
	}	
}