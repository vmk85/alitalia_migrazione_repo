package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.MmbAncillaryErrorData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.common.messages.BaseResponse;

public class WebCheckinAncillaryResponse extends BaseResponse {

    private List<MmbAncillaryData> ancillaries;
    private List<MmbAncillaryErrorData> errors;
    private String machineName;
    
	public List<MmbAncillaryData> getAncillaries() {
		return ancillaries;
	}
	public void setAncillaries(List<MmbAncillaryData> ancillaries) {
		this.ancillaries = ancillaries;
	}
	public List<MmbAncillaryErrorData> getErrors() {
		return errors;
	}
	public void setErrors(List<MmbAncillaryErrorData> errors) {
		this.errors = errors;
	}
	public String getMachineName() {
		return machineName;
	}
	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((ancillaries == null) ? 0 : ancillaries.hashCode());
		result = prime * result + ((errors == null) ? 0 : errors.hashCode());
		result = prime * result
				+ ((machineName == null) ? 0 : machineName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebCheckinAncillaryResponse other = (WebCheckinAncillaryResponse) obj;
		if (ancillaries == null) {
			if (other.ancillaries != null)
				return false;
		} else if (!ancillaries.equals(other.ancillaries))
			return false;
		if (errors == null) {
			if (other.errors != null)
				return false;
		} else if (!errors.equals(other.errors))
			return false;
		if (machineName == null) {
			if (other.machineName != null)
				return false;
		} else if (!machineName.equals(other.machineName))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "WebCheckinAddToCartResponse [ancillaries=" + ancillaries
				+ ", errors=" + errors + ", machineName=" + machineName + "]";
	}
}