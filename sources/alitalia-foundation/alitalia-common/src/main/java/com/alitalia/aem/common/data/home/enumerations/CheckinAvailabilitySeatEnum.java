package com.alitalia.aem.common.data.home.enumerations;

public enum CheckinAvailabilitySeatEnum {

	FREE("Free"),
	OCCUPIED("Occupied"),
	NOT_APPLICABLE("NotApplicable");
    
	private final String value;

	CheckinAvailabilitySeatEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CheckinAvailabilitySeatEnum fromValue(String v) {
        for (CheckinAvailabilitySeatEnum c: CheckinAvailabilitySeatEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}