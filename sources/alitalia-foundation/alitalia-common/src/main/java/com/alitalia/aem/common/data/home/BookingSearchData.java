package com.alitalia.aem.common.data.home;

public class BookingSearchData extends GatewaySearchData {

	private String bookingSolutionId;
	private Boolean multiSlice;
	private Boolean neutral;

	public String getBookingSolutionId() {
		return bookingSolutionId;
	}

	public void setBookingSolutionId(String bookingSolutionId) {
		this.bookingSolutionId = bookingSolutionId;
	}
	
	public Boolean isMultiSlice() {
		return multiSlice;
	}
	
	public void setMultiSlice(Boolean multiSlice) {
		this.multiSlice = multiSlice;
	}
	
	public Boolean isNeutral() {
		return neutral;
	}
	
	public void setNeutral(Boolean neutral) {
		this.neutral = neutral;
	}

	@Override
	public String toString() {
		return "BookingSearchData [bookingSolutionId=" + bookingSolutionId
				+ ", multiSlice=" + multiSlice + ", neutral=" + neutral
				+ ", toString()=" + super.toString() + "]";
	}
}
