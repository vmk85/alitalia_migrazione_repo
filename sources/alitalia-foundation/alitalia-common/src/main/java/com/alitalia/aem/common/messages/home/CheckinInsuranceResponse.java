package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.checkin.CheckinInsurancePolicyData;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinInsuranceResponse extends BaseResponse {

	private CheckinInsurancePolicyData policy;

	public CheckinInsurancePolicyData getPolicy() {
		return policy;
	}

	public void setPolicy(CheckinInsurancePolicyData policy) {
		this.policy = policy;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((policy == null) ? 0 : policy.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinInsuranceResponse other = (CheckinInsuranceResponse) obj;
		if (policy == null) {
			if (other.policy != null)
				return false;
		} else if (!policy.equals(other.policy))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CheckinInsuranceResponse [policy=" + policy + "]";
	}
}