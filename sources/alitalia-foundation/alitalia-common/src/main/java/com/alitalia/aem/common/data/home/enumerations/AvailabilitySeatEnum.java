package com.alitalia.aem.common.data.home.enumerations;


public enum AvailabilitySeatEnum {

	FREE("Free"),
	OCCUPIED("Occupied"),
	NOT_APPLICABLE("NotApplicable");

	private final String value;

	AvailabilitySeatEnum(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	public static AvailabilitySeatEnum fromValue(String v) {
		for (AvailabilitySeatEnum c: AvailabilitySeatEnum.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}