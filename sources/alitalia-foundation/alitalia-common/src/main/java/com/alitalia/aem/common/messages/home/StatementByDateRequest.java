package com.alitalia.aem.common.messages.home;

import java.util.Calendar;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.data.home.enumerations.ActivityTypeEnum;
import com.alitalia.aem.common.messages.BaseRequest;

public class StatementByDateRequest extends BaseRequest {

	private MMCustomerProfileData customerProfile;
	private ActivityTypeEnum activityType;
    private Calendar dateFrom;
    private Calendar dateTo;
    private Integer maxResults;
	
    public MMCustomerProfileData getCustomerProfile() {
		return customerProfile;
	}

	public void setCustomerProfile(MMCustomerProfileData customerProfile) {
		this.customerProfile = customerProfile;
	}

	public ActivityTypeEnum getActivityType() {
		return activityType;
	}
	
    public void setActivityType(ActivityTypeEnum activityType) {
		this.activityType = activityType;
	}
	
    public Calendar getDateFrom() {
		return dateFrom;
	}
	
    public void setDateFrom(Calendar dateFrom) {
		this.dateFrom = dateFrom;
	}
	
    public Calendar getDateTo() {
		return dateTo;
	}
	
    public void setDateTo(Calendar dateTo) {
		this.dateTo = dateTo;
	}
	
    public Integer getMaxResults() {
		return maxResults;
	}
	
    public void setMaxResults(Integer maxResults) {
		this.maxResults = maxResults;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((activityType == null) ? 0 : activityType.hashCode());
		result = prime * result
				+ ((customerProfile == null) ? 0 : customerProfile.hashCode());
		result = prime * result
				+ ((dateFrom == null) ? 0 : dateFrom.hashCode());
		result = prime * result + ((dateTo == null) ? 0 : dateTo.hashCode());
		result = prime * result
				+ ((maxResults == null) ? 0 : maxResults.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		StatementByDateRequest other = (StatementByDateRequest) obj;
		if (activityType != other.activityType)
			return false;
		if (customerProfile == null) {
			if (other.customerProfile != null)
				return false;
		} else if (!customerProfile.equals(other.customerProfile))
			return false;
		if (dateFrom == null) {
			if (other.dateFrom != null)
				return false;
		} else if (!dateFrom.equals(other.dateFrom))
			return false;
		if (dateTo == null) {
			if (other.dateTo != null)
				return false;
		} else if (!dateTo.equals(other.dateTo))
			return false;
		if (maxResults == null) {
			if (other.maxResults != null)
				return false;
		} else if (!maxResults.equals(other.maxResults))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "StatementByDateRequest [customerProfile=" + customerProfile
				+ ", activityType=" + activityType + ", dateFrom=" + dateFrom
				+ ", dateTo=" + dateTo + ", maxResults=" + maxResults + "]";
	}
}