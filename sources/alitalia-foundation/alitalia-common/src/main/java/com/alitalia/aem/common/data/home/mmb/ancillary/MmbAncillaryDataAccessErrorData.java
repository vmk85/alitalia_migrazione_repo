package com.alitalia.aem.common.data.home.mmb.ancillary;

import com.alitalia.aem.common.data.MmbAncillaryErrorData;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryShoppingCartServiceErrorEnum;

public class MmbAncillaryDataAccessErrorData extends MmbAncillaryErrorData {

	public MmbAncillaryDataAccessErrorData() {
		super();
		this.type = MmbAncillaryShoppingCartServiceErrorEnum.DATA_ACCESS_ERROR;
	}

}
