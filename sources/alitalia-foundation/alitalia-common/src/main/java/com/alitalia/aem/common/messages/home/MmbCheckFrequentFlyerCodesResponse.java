package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.messages.BaseResponse;

public class MmbCheckFrequentFlyerCodesResponse extends BaseResponse {

	private List<String> invalidFFCodes;

	public List<String> getInvalidFFCodes() {
		return invalidFFCodes;
	}

	public void setInvalidFFCodes(List<String> invalidFFCodes) {
		this.invalidFFCodes = invalidFFCodes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((invalidFFCodes == null) ? 0 : invalidFFCodes
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbCheckFrequentFlyerCodesResponse other = (MmbCheckFrequentFlyerCodesResponse) obj;
		if (invalidFFCodes == null) {
			if (other.invalidFFCodes != null)
				return false;
		} else if (!invalidFFCodes.equals(other.invalidFFCodes))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbCheckFrequentFlyerCodesResponse [validFFCodes="
				+ invalidFFCodes + ", getTid()=" + getTid()
				+ ", getSid()=" + getSid() + "]";
	}

}
