package com.alitalia.aem.common.data.home;

import java.util.Calendar;

import com.alitalia.aem.common.data.home.enumerations.MMActivityTypeEnum;

public class MMActivityData {

	private Calendar activityDate;
	private Calendar statementDate;

	private Long amount;
	private Long basic;
	private Long bonus;
	private Long pointsTotal;

	private String customerNumber;
	private String description;
	private String referenceNumber;
	private String rowId;
	private String sequenceNumber;

	private Boolean qualified;

	private MMActivityTypeEnum activityType;

	public Calendar getActivityDate() {
		return activityDate;
	}

	public void setActivityDate(Calendar activityDate) {
		this.activityDate = activityDate;
	}

	public Calendar getStatementDate() {
		return statementDate;
	}

	public void setStatementDate(Calendar statementDate) {
		this.statementDate = statementDate;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Long getBasic() {
		return basic;
	}

	public void setBasic(Long basic) {
		this.basic = basic;
	}

	public Long getBonus() {
		return bonus;
	}

	public void setBonus(Long bonus) {
		this.bonus = bonus;
	}

	public Long getPointsTotal() {
		return pointsTotal;
	}

	public void setPointsTotal(Long pointsTotal) {
		this.pointsTotal = pointsTotal;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getRowId() {
		return rowId;
	}

	public void setRowId(String rowId) {
		this.rowId = rowId;
	}

	public String getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public Boolean getQualified() {
		return qualified;
	}

	public void setQualified(Boolean qualified) {
		this.qualified = qualified;
	}

	public MMActivityTypeEnum getActivityType() {
		return activityType;
	}

	public void setActivityType(MMActivityTypeEnum activityType) {
		this.activityType = activityType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((activityDate == null) ? 0 : activityDate.hashCode());
		result = prime * result
				+ ((activityType == null) ? 0 : activityType.hashCode());
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + ((basic == null) ? 0 : basic.hashCode());
		result = prime * result + ((bonus == null) ? 0 : bonus.hashCode());
		result = prime * result
				+ ((customerNumber == null) ? 0 : customerNumber.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result
				+ ((pointsTotal == null) ? 0 : pointsTotal.hashCode());
		result = prime * result
				+ ((qualified == null) ? 0 : qualified.hashCode());
		result = prime * result
				+ ((referenceNumber == null) ? 0 : referenceNumber.hashCode());
		result = prime * result + ((rowId == null) ? 0 : rowId.hashCode());
		result = prime * result
				+ ((sequenceNumber == null) ? 0 : sequenceNumber.hashCode());
		result = prime * result
				+ ((statementDate == null) ? 0 : statementDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MMActivityData other = (MMActivityData) obj;
		if (activityDate == null) {
			if (other.activityDate != null)
				return false;
		} else if (!activityDate.equals(other.activityDate))
			return false;
		if (activityType != other.activityType)
			return false;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (basic == null) {
			if (other.basic != null)
				return false;
		} else if (!basic.equals(other.basic))
			return false;
		if (bonus == null) {
			if (other.bonus != null)
				return false;
		} else if (!bonus.equals(other.bonus))
			return false;
		if (customerNumber == null) {
			if (other.customerNumber != null)
				return false;
		} else if (!customerNumber.equals(other.customerNumber))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (pointsTotal == null) {
			if (other.pointsTotal != null)
				return false;
		} else if (!pointsTotal.equals(other.pointsTotal))
			return false;
		if (qualified == null) {
			if (other.qualified != null)
				return false;
		} else if (!qualified.equals(other.qualified))
			return false;
		if (referenceNumber == null) {
			if (other.referenceNumber != null)
				return false;
		} else if (!referenceNumber.equals(other.referenceNumber))
			return false;
		if (rowId == null) {
			if (other.rowId != null)
				return false;
		} else if (!rowId.equals(other.rowId))
			return false;
		if (sequenceNumber == null) {
			if (other.sequenceNumber != null)
				return false;
		} else if (!sequenceNumber.equals(other.sequenceNumber))
			return false;
		if (statementDate == null) {
			if (other.statementDate != null)
				return false;
		} else if (!statementDate.equals(other.statementDate))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ActivityData [activityDate=" + activityDate
				+ ", statementDate=" + statementDate + ", amount=" + amount
				+ ", basic=" + basic + ", bonus=" + bonus + ", pointsTotal="
				+ pointsTotal + ", customerNumber=" + customerNumber
				+ ", description=" + description + ", referenceNumber="
				+ referenceNumber + ", rowId=" + rowId + ", sequenceNumber="
				+ sequenceNumber + ", qualified=" + qualified
				+ ", activityType=" + activityType + "]";
	}

}
