package com.alitalia.aem.common.data.home;

public class InfantPassengerData extends PassengerBaseData {

	private PreferencesData preferences;
	private Integer adultRefent;

	public InfantPassengerData() {
		super();
	}

	public InfantPassengerData(InfantPassengerData clone) {
		super(clone);
		this.adultRefent = clone.getAdultRefent();
		PreferencesData clonePreferences = clone.getPreferences();
		if (clonePreferences != null)
			this.preferences = new PreferencesData(clonePreferences);
	}

	public InfantPassengerData(PassengerBaseData clone) {
		super(clone);
		this.adultRefent = null;
		this.preferences = null;
	}

	public PreferencesData getPreferences() {
		return preferences;
	}
	
	public void setPreferences(PreferencesData preferences) {
		this.preferences = preferences;
	}
	
	public Integer getAdultRefent() {
		return adultRefent;
	}

	public void setAdultRefent(Integer adultRefent) {
		this.adultRefent = adultRefent;
	}

	@Override
	public String toString() {
		return "InfantPassengerData [preferences=" + preferences
				+ ", adultRefent=" + adultRefent + "]";
	}
}
