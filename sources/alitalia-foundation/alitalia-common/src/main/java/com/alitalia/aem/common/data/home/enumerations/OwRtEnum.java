
package com.alitalia.aem.common.data.home.enumerations;



public enum OwRtEnum {

    ALL("All"),
    OUTBOUND("Outbound"),
    RETURN("Return");
    
    private final String value;

    OwRtEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OwRtEnum fromValue(String v) {
        for (OwRtEnum c: OwRtEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
