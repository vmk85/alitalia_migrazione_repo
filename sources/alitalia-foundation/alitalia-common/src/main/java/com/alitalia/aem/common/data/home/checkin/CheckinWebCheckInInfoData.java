package com.alitalia.aem.common.data.home.checkin;

import java.util.Calendar;

public class CheckinWebCheckInInfoData {

	private Integer id;
	private String aptArrivalCode;
	private String aptDepartureCode;
	private Calendar arrivalDate; 
	private Calendar arrivalTime; 
	private Integer couponId; 
	private Calendar departureDate;
	private Calendar departureTime;
	private String selectedPassengerEticket;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAptArrivalCode() {
		return aptArrivalCode;
	}
	public void setAptArrivalCode(String aptArrivalCode) {
		this.aptArrivalCode = aptArrivalCode;
	}
	public String getAptDepartureCode() {
		return aptDepartureCode;
	}
	public void setAptDepartureCode(String aptDepartureCode) {
		this.aptDepartureCode = aptDepartureCode;
	}
	public Calendar getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(Calendar arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public Calendar getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(Calendar arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public Integer getCouponId() {
		return couponId;
	}
	public void setCouponId(Integer couponId) {
		this.couponId = couponId;
	}
	public Calendar getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(Calendar departureDate) {
		this.departureDate = departureDate;
	}
	public Calendar getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(Calendar departureTime) {
		this.departureTime = departureTime;
	}
	public String getSelectedPassengerEticket() {
		return selectedPassengerEticket;
	}
	public void setSelectedPassengerEticket(String selectedPassengerEticket) {
		this.selectedPassengerEticket = selectedPassengerEticket;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((aptArrivalCode == null) ? 0 : aptArrivalCode.hashCode());
		result = prime
				* result
				+ ((aptDepartureCode == null) ? 0 : aptDepartureCode.hashCode());
		result = prime * result
				+ ((arrivalDate == null) ? 0 : arrivalDate.hashCode());
		result = prime * result
				+ ((arrivalTime == null) ? 0 : arrivalTime.hashCode());
		result = prime * result
				+ ((couponId == null) ? 0 : couponId.hashCode());
		result = prime * result
				+ ((departureDate == null) ? 0 : departureDate.hashCode());
		result = prime * result
				+ ((departureTime == null) ? 0 : departureTime.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime
				* result
				+ ((selectedPassengerEticket == null) ? 0
						: selectedPassengerEticket.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinWebCheckInInfoData other = (CheckinWebCheckInInfoData) obj;
		if (aptArrivalCode == null) {
			if (other.aptArrivalCode != null)
				return false;
		} else if (!aptArrivalCode.equals(other.aptArrivalCode))
			return false;
		if (aptDepartureCode == null) {
			if (other.aptDepartureCode != null)
				return false;
		} else if (!aptDepartureCode.equals(other.aptDepartureCode))
			return false;
		if (arrivalDate == null) {
			if (other.arrivalDate != null)
				return false;
		} else if (!arrivalDate.equals(other.arrivalDate))
			return false;
		if (arrivalTime == null) {
			if (other.arrivalTime != null)
				return false;
		} else if (!arrivalTime.equals(other.arrivalTime))
			return false;
		if (couponId == null) {
			if (other.couponId != null)
				return false;
		} else if (!couponId.equals(other.couponId))
			return false;
		if (departureDate == null) {
			if (other.departureDate != null)
				return false;
		} else if (!departureDate.equals(other.departureDate))
			return false;
		if (departureTime == null) {
			if (other.departureTime != null)
				return false;
		} else if (!departureTime.equals(other.departureTime))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (selectedPassengerEticket == null) {
			if (other.selectedPassengerEticket != null)
				return false;
		} else if (!selectedPassengerEticket
				.equals(other.selectedPassengerEticket))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CheckinWebCheckInInfoData [id=" + id + ", aptArrivalCode="
				+ aptArrivalCode + ", aptDepartureCode=" + aptDepartureCode
				+ ", arrivalDate=" + arrivalDate + ", arrivalTime="
				+ arrivalTime + ", couponId=" + couponId + ", departureDate="
				+ departureDate + ", departureTime=" + departureTime
				+ ", selectedPassengerEticket=" + selectedPassengerEticket
				+ "]";
	}

}
