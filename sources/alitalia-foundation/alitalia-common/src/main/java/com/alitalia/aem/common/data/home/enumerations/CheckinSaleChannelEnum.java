package com.alitalia.aem.common.data.home.enumerations;

public enum CheckinSaleChannelEnum {

	WEB("Web");
    
    private final String value;

    CheckinSaleChannelEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CheckinSaleChannelEnum fromValue(String v) {
        for (CheckinSaleChannelEnum c: CheckinSaleChannelEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
