package com.alitalia.aem.common.data.home.enumerations;

public enum MmbAncillaryShoppingCartServiceErrorEnum {

	SERVICE_CLIENT_ERROR("ServiceClientError"),
	BUSINESS_ERROR("BusinessError"),
	DATA_ACCESS_ERROR("DataAccessError"),
	SERVICE_ERROR("ServiceError");

	private final String value;

    MmbAncillaryShoppingCartServiceErrorEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MmbAncillaryShoppingCartServiceErrorEnum fromValue(String v) {
        for (MmbAncillaryShoppingCartServiceErrorEnum c: MmbAncillaryShoppingCartServiceErrorEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
