package com.alitalia.aem.common.data.home.enumerations;

public enum PassengerTypeEnum {

	ADULT("Adult"),
    CHILD("Child"),
    INFANT("Infant"),
    YOUTH("Youth"),
    APPLICANT("Applicant"),
    NORMAL("Normal");
    private final String value;

    PassengerTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PassengerTypeEnum fromValue(String v) {
        for (PassengerTypeEnum c: PassengerTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
