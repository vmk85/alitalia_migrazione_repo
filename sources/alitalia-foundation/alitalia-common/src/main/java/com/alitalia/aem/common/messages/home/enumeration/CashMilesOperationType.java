package com.alitalia.aem.common.messages.home.enumeration;

public enum CashMilesOperationType {

    ADD("Add"),
    SUBTRACT("Subtract");
    private final String value;

    CashMilesOperationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CashMilesOperationType fromValue(String v) {
        for (CashMilesOperationType c: CashMilesOperationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
