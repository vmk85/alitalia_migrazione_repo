package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.common.messages.BaseRequest;

public class AwardValueRequest extends BaseRequest {

	private String mmCustomerNumber;
	private String mmCustomerSurname;
	private List<RouteData> routes;
	private Integer passengersNumber;

	public String getMmCustomerNumber() {
		return mmCustomerNumber;
	}

	public void setMmCustomerNumber(String mmCustomerNumber) {
		this.mmCustomerNumber = mmCustomerNumber;
	}

	public String getMmCustomerSurname() {
		return mmCustomerSurname;
	}

	public void setMmCustomerSurname(String mmCustomerSurname) {
		this.mmCustomerSurname = mmCustomerSurname;
	}

	public List<RouteData> getRoutes() {
		return routes;
	}

	public void setRoutes(List<RouteData> routes) {
		this.routes = routes;
	}

	public Integer getPassengersNumber() {
		return passengersNumber;
	}

	public void setPassengersNumber(Integer passengersNumber) {
		this.passengersNumber = passengersNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((routes == null) ? 0 : routes.hashCode());
		result = prime
				* result
				+ ((mmCustomerNumber == null) ? 0 : mmCustomerNumber.hashCode());
		result = prime
				* result
				+ ((mmCustomerSurname == null) ? 0 : mmCustomerSurname
						.hashCode());
		result = prime
				* result
				+ ((passengersNumber == null) ? 0 : passengersNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AwardValueRequest other = (AwardValueRequest) obj;
		if (routes == null) {
			if (other.routes != null)
				return false;
		} else if (!routes.equals(other.routes))
			return false;
		if (mmCustomerNumber == null) {
			if (other.mmCustomerNumber != null)
				return false;
		} else if (!mmCustomerNumber.equals(other.mmCustomerNumber))
			return false;
		if (mmCustomerSurname == null) {
			if (other.mmCustomerSurname != null)
				return false;
		} else if (!mmCustomerSurname.equals(other.mmCustomerSurname))
			return false;
		if (passengersNumber == null) {
			if (other.passengersNumber != null)
				return false;
		} else if (!passengersNumber.equals(other.passengersNumber))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AwardValueRequest [mmCustomerNumber=" + mmCustomerNumber
				+ ", mmCustomerSurname=" + mmCustomerSurname + ", routes="
				+ routes + ", passengersNumber=" + passengersNumber
				+ ", getTid()=" + getTid() + ", getSid()=" + getSid() + "]";
	}

}
