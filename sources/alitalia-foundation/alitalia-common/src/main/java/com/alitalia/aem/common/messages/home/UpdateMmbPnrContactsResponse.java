package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class UpdateMmbPnrContactsResponse extends BaseResponse {

	private Boolean successful;

	public Boolean getSuccessful() {
		return successful;
	}

	public void setSuccessful(Boolean successful) {
		this.successful = successful;
	}

	@Override
	public String toString() {
		return "UpdateMmbPnrContactsResponse [successful=" + successful
				+ ", getTid()=" + getTid() + ", getSid()=" + getSid() + "]";
	}

}
