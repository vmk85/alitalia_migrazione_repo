package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.AirportModelData;
import com.alitalia.aem.common.messages.BaseRequest;

public class RetrieveAirportRequest extends BaseRequest {

	private AirportModelData airport;

	public AirportModelData getAirport() {
		return airport;
	}

	public void setAirport(AirportModelData airport) {
		this.airport = airport;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((airport == null) ? 0 : airport.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveAirportRequest other = (RetrieveAirportRequest) obj;
		if (airport == null) {
			if (other.airport != null)
				return false;
		} else if (!airport.equals(other.airport))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveAirportRequest [airport=" + airport + "]";
	}
}