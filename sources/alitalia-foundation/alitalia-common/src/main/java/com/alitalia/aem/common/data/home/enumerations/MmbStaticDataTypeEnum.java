package com.alitalia.aem.common.data.home.enumerations;

public enum MmbStaticDataTypeEnum {

	AIRPORT("Airport"),
	CATEGORY_TYPE("CategoryType"),
	PHONE_PREFIX("PhonePrefix"),
	FREQUENT_FLYER_TYPE("FrequentFlyerType"),
	COUNTRY("Country"),
	SEAT_TYPE("SeatType"),
	PAYMENT_TYPE_ITEM("PaymentTypeItem"),
	MEAL("Meal"),
	TICKET_OFFICE("TicketOffice"),
	CREDIT_CARD_COUNTRY("CreditCardCountry"),
	PROVINCES_LIST("ProvincesList");

	private final String value;

	private MmbStaticDataTypeEnum(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	public static MmbStaticDataTypeEnum fromValue(String v) {
		for (MmbStaticDataTypeEnum c: MmbStaticDataTypeEnum.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}