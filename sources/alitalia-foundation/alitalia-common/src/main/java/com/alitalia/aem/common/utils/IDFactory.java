package com.alitalia.aem.common.utils;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IDFactory {
	
	private static Logger logger = LoggerFactory.getLogger(IDFactory.class);

	public static String getTid() {
		return UUID.randomUUID().toString();
	}
	
	public static String getSid(HttpServletRequest request) {
		String sid = "";
		try {
			sid = request.getSession() != null ? request.getSession().getId()
					: getSid();
		} catch (Exception e) {
			logger.info("Cannot get Id from session, using surrogate");
			sid = getSid();
		}
		return sid;
	}

	public static String getSid() {
		return "0";
	}
	
}
