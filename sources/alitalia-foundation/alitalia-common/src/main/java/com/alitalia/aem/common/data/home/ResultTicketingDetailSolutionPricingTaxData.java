package com.alitalia.aem.common.data.home;

public class ResultTicketingDetailSolutionPricingTaxData {

    private String codeField;
    private ResultTicketingDetailSolutionPricingTaxDisplayPriceData displayPriceField;
    private String idField;
    private ResultTicketingDetailSolutionPricingTaxSalePriceData salePriceField;
    private String statusField;

    public String getCodeField() {
        return codeField;
    }

    public void setCodeField(String value) {
        this.codeField = value;
    }

    public ResultTicketingDetailSolutionPricingTaxDisplayPriceData getDisplayPriceField() {
        return displayPriceField;
    }

    public void setDisplayPriceField(ResultTicketingDetailSolutionPricingTaxDisplayPriceData value) {
        this.displayPriceField = value;
    }

    public String getIdField() {
        return idField;
    }

    public void setIdField(String value) {
        this.idField = value;
    }

    public ResultTicketingDetailSolutionPricingTaxSalePriceData getSalePriceField() {
        return salePriceField;
    }

    public void setSalePriceField(ResultTicketingDetailSolutionPricingTaxSalePriceData value) {
        this.salePriceField = value;
    }

    public String getStatusField() {
        return statusField;
    }

    public void setStatusField(String value) {
        this.statusField = value;
    }

}
