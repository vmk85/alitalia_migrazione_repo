package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.SocialAccountData;
import com.alitalia.aem.common.messages.BaseRequest;

public class RemoveSocialAccountsRequest extends BaseRequest {
	
	private String pin;
	private String userCode;
	private List<SocialAccountData> socialAccounts;
	
	public String getPin() {
		return pin;
	}
	
	public void setPin(String pin) {
		this.pin = pin;
	}
	
	public String getUserCode() {
		return userCode;
	}
	
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	
	public List<SocialAccountData> getSocialAccounts() {
		return socialAccounts;
	}

	public void setSocialAccounts(List<SocialAccountData> socialAccounts) {
		this.socialAccounts = socialAccounts;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((pin == null) ? 0 : pin.hashCode());
		result = prime * result
				+ ((userCode == null) ? 0 : userCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RemoveSocialAccountsRequest other = (RemoveSocialAccountsRequest) obj;
		if (pin == null) {
			if (other.pin != null)
				return false;
		} else if (!pin.equals(other.pin))
			return false;
		if (userCode == null) {
			if (other.userCode != null)
				return false;
		} else if (!userCode.equals(other.userCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RemoveSocialAccountsRequest [pin=" + pin + ", userCode="
				+ userCode + ", socialAccounts=" + socialAccounts + "]";
	}
}