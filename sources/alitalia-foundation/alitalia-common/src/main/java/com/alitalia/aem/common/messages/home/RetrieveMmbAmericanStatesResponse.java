package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.FrequentFlyerTypeData;
import com.alitalia.aem.common.data.home.mmb.MmbAirportCheckinData;
import com.alitalia.aem.common.data.home.mmb.MmbAmericanStatesData;
import com.alitalia.aem.common.data.home.mmb.MmbFrequentFlyerCarrierData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetrieveMmbAmericanStatesResponse extends BaseResponse{

	private List<MmbAmericanStatesData> americanStatesList;
	
	public RetrieveMmbAmericanStatesResponse(){}
	
	public RetrieveMmbAmericanStatesResponse(String tid, String sid) {
		super(tid, sid);
	}

	public List<MmbAmericanStatesData> getAmericanStatesList() {
		return americanStatesList;
	}

	public void setAmericanStatesList(List<MmbAmericanStatesData> americanStatesList) {
		this.americanStatesList = americanStatesList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((americanStatesList == null) ? 0 : americanStatesList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveMmbAmericanStatesResponse other = (RetrieveMmbAmericanStatesResponse) obj;
		if (americanStatesList == null) {
			if (other.americanStatesList != null)
				return false;
		} else if (!americanStatesList.equals(other.americanStatesList))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveMmbAmericanStatesResponse [americanStatesList="
				+ americanStatesList + ", getTid()=" + getTid() + ", getSid()="
				+ getSid() + "]";
	}
}