package com.alitalia.aem.common.messages.home;

import java.math.BigDecimal;

import com.alitalia.aem.common.data.home.carnet.CarnetInfoCarnet;
import com.alitalia.aem.common.messages.BaseRequest;

public class CarnetUpdateRequest extends BaseRequest {

    private BigDecimal amount;
    private CarnetInfoCarnet infoCarnet;
    private Short routesNumber;

	public CarnetUpdateRequest(String tid, String sid) {
		super(tid, sid);
	}

	public CarnetUpdateRequest() {
		super();
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public CarnetInfoCarnet getInfoCarnet() {
		return infoCarnet;
	}

	public void setInfoCarnet(CarnetInfoCarnet infoCarnet) {
		this.infoCarnet = infoCarnet;
	}

	public Short getRoutesNumber() {
		return routesNumber;
	}

	public void setRoutesNumber(Short routesNumber) {
		this.routesNumber = routesNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result
				+ ((infoCarnet == null) ? 0 : infoCarnet.hashCode());
		result = prime * result
				+ ((routesNumber == null) ? 0 : routesNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarnetUpdateRequest other = (CarnetUpdateRequest) obj;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (infoCarnet == null) {
			if (other.infoCarnet != null)
				return false;
		} else if (!infoCarnet.equals(other.infoCarnet))
			return false;
		if (routesNumber == null) {
			if (other.routesNumber != null)
				return false;
		} else if (!routesNumber.equals(other.routesNumber))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CarnetUpdateRequest [amount=" + amount + ", infoCarnet="
				+ infoCarnet + ", routesNumber=" + routesNumber + "]";
	}
}