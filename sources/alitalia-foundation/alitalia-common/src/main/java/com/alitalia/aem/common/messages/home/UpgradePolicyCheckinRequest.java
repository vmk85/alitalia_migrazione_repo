package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.messages.MmbBaseRequest;

public class UpgradePolicyCheckinRequest extends MmbBaseRequest {

	private MmbCompartimentalClassEnum compartClass;
	private CheckinFlightData flight;
	private String marketCode;
	
	public MmbCompartimentalClassEnum getCompartClass() {
		return compartClass;
	}
	public void setCompartClass(MmbCompartimentalClassEnum compartClass) {
		this.compartClass = compartClass;
	}
	public CheckinFlightData getFlight() {
		return flight;
	}
	public void setFlight(CheckinFlightData flight) {
		this.flight = flight;
	}
	public String getMarketCode() {
		return marketCode;
	}
	public void setMarketCode(String marketCode) {
		this.marketCode = marketCode;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((compartClass == null) ? 0 : compartClass.hashCode());
		result = prime * result + ((flight == null) ? 0 : flight.hashCode());
		result = prime * result
				+ ((marketCode == null) ? 0 : marketCode.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		UpgradePolicyCheckinRequest other = (UpgradePolicyCheckinRequest) obj;
		if (compartClass != other.compartClass)
			return false;
		if (flight == null) {
			if (other.flight != null)
				return false;
		} else if (!flight.equals(other.flight))
			return false;
		if (marketCode == null) {
			if (other.marketCode != null)
				return false;
		} else if (!marketCode.equals(other.marketCode))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "UpgradePolicyCheckinRequest [compartClass=" + compartClass
				+ ", flight=" + flight + ", marketCode=" + marketCode
				+ ", toString()=" + super.toString() + "]";
	}

}
