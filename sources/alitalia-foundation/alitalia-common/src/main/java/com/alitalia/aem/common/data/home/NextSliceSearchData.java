package com.alitalia.aem.common.data.home;

import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.GatewayTypeEnum;

public class NextSliceSearchData extends GatewaySearchData {

	private CabinEnum preferredCabin;
	private int sliceIndex;

	public NextSliceSearchData() {
		super();
		this.setType(GatewayTypeEnum.MULTI_SLICE_NEXT_SEARCH);
	}

	public CabinEnum getPreferredCabin() {
		return preferredCabin;
	}

	public void setPreferredCabin(CabinEnum preferredCabin) {
		this.preferredCabin = preferredCabin;
	}

	public int getSliceIndex() {
		return sliceIndex;
	}

	public void setSliceIndex(int sliceIndex) {
		this.sliceIndex = sliceIndex;
	}

	@Override
	public String toString() {
		return "NextSliceSearchData [preferredCabin=" + preferredCabin
				+ ", sliceIndex=" + sliceIndex + ", toString()="
				+ super.toString() + "]";
	}

}
