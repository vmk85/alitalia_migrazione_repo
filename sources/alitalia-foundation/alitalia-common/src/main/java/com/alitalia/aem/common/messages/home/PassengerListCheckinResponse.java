package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.messages.BaseResponse;

public class PassengerListCheckinResponse extends BaseResponse{
	
	private Boolean isListComplete;
	private List<CheckinPassengerData> passengers;
	private String pnr;
	
	public Boolean getIsListComplete() {
		return isListComplete;
	}
	public void setIsListComplete(Boolean isListComplete) {
		this.isListComplete = isListComplete;
	}
	public List<CheckinPassengerData> getPassengers() {
		return passengers;
	}
	public void setPassengers(List<CheckinPassengerData> passengers) {
		this.passengers = passengers;
	}
	public String getPnr() {
		return pnr;
	}
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((isListComplete == null) ? 0 : isListComplete.hashCode());
		result = prime * result
				+ ((passengers == null) ? 0 : passengers.hashCode());
		result = prime * result + ((pnr == null) ? 0 : pnr.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PassengerListCheckinResponse other = (PassengerListCheckinResponse) obj;
		if (isListComplete == null) {
			if (other.isListComplete != null)
				return false;
		} else if (!isListComplete.equals(other.isListComplete))
			return false;
		if (passengers == null) {
			if (other.passengers != null)
				return false;
		} else if (!passengers.equals(other.passengers))
			return false;
		if (pnr == null) {
			if (other.pnr != null)
				return false;
		} else if (!pnr.equals(other.pnr))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "PassengerListCheckinResponse [isListComplete=" + isListComplete
				+ ", passengers=" + passengers + ", pnr=" + pnr
				+ ", toString()=" + super.toString() + "]";
	}

}
