package com.alitalia.aem.common.data.home;

import java.math.BigDecimal;

public class ResultTicketingDetailSolutionSaleTaxTotalData {

    private BigDecimal amountField;
    private String currencyField;

    public BigDecimal getAmountField() {
        return amountField;
    }

    public void setAmountField(BigDecimal value) {
        this.amountField = value;
    }

    public String getCurrencyField() {
        return currencyField;
    }

    public void setCurrencyField(String value) {
        this.currencyField = value;
    }

}
