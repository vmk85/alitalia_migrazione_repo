package com.alitalia.aem.common.data.home;

public class ResultBookingDetailsSolutionPricingNoteData {
	
	private String idField;
	private String nameField;
	private String valueField;
	
	public String getIdField() {
		return idField;
	}
	
	public void setIdField(String idField) {
		this.idField = idField;
	}
	
	public String getNameField() {
		return nameField;
	}
	
	public void setNameField(String nameField) {
		this.nameField = nameField;
	}
	
	public String getValueField() {
		return valueField;
	}
	
	public void setValueField(String valueField) {
		this.valueField = valueField;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idField == null) ? 0 : idField.hashCode());
		result = prime * result
				+ ((nameField == null) ? 0 : nameField.hashCode());
		result = prime * result
				+ ((valueField == null) ? 0 : valueField.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingDetailsSolutionPricingNoteData other = (ResultBookingDetailsSolutionPricingNoteData) obj;
		if (idField == null) {
			if (other.idField != null)
				return false;
		} else if (!idField.equals(other.idField))
			return false;
		if (nameField == null) {
			if (other.nameField != null)
				return false;
		} else if (!nameField.equals(other.nameField))
			return false;
		if (valueField == null) {
			if (other.valueField != null)
				return false;
		} else if (!valueField.equals(other.valueField))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingDetailsSolutionPricingNoteData [idField="
				+ idField + ", nameField=" + nameField + ", valueField="
				+ valueField + "]";
	}
	
}
