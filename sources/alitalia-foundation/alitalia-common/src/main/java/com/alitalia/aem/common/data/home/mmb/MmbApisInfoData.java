package com.alitalia.aem.common.data.home.mmb;

import java.util.Calendar;
import com.alitalia.aem.common.data.home.enumerations.DocumentTypeEnum;

public class MmbApisInfoData {

	private Calendar birthDate;
	private String destinationAddress;
	private String destinationCity;
	private String destinationState;
	private String destinationZipCode;
	private String docType;
	private Calendar extraDocumentExpirationDate;
	private String extraDocumentIssuingCountry;
	private String extraDocumentIssuingState;
	private String extraDocumentNumber;
	private DocumentTypeEnum extraDocumentType;
	private String gender;
	private String lastname;
	private String name;
	private String nationality;
	private Calendar passportExpirationDate;
	private String passportNumber;
	private String residenceCountry;
	private String secondName;

	public MmbApisInfoData() {
		super();
	}

	public MmbApisInfoData(MmbApisInfoData clone) {
		super();
		this.birthDate = (Calendar) clone.birthDate.clone();
		this.destinationAddress = clone.destinationAddress;
		this.destinationAddress = clone.destinationCity;
		this.destinationCity = clone.destinationAddress;
		this.destinationState = clone.destinationState;
		this.destinationZipCode = clone.destinationZipCode;
		this.docType = clone.docType;
		this.extraDocumentExpirationDate = (Calendar) clone.extraDocumentExpirationDate.clone();
		this.extraDocumentIssuingCountry = clone.extraDocumentIssuingCountry;
		this.extraDocumentIssuingState = clone.extraDocumentIssuingState;
		this.extraDocumentNumber = clone.extraDocumentNumber;
		this.extraDocumentType = clone.extraDocumentType;
		this.gender = clone.gender;
		this.lastname = clone.lastname;
		this.name = clone.name;
		this.nationality = clone.nationality;
		this.passportExpirationDate = (Calendar) clone.passportExpirationDate.clone();
		this.passportNumber = clone.passportNumber;
		this.residenceCountry = clone.residenceCountry;
		this.secondName = clone.secondName;
	}

	public Calendar getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Calendar birthDate) {
		this.birthDate = birthDate;
	}

	public String getDestinationAddress() {
		return destinationAddress;
	}

	public void setDestinationAddress(String destinationAddress) {
		this.destinationAddress = destinationAddress;
	}

	public String getDestinationCity() {
		return destinationCity;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	public String getDestinationState() {
		return destinationState;
	}

	public void setDestinationState(String destinationState) {
		this.destinationState = destinationState;
	}

	public String getDestinationZipCode() {
		return destinationZipCode;
	}

	public void setDestinationZipCode(String destinationZipCode) {
		this.destinationZipCode = destinationZipCode;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public Calendar getExtraDocumentExpirationDate() {
		return extraDocumentExpirationDate;
	}

	public void setExtraDocumentExpirationDate(Calendar extraDocumentExpirationDate) {
		this.extraDocumentExpirationDate = extraDocumentExpirationDate;
	}

	public String getExtraDocumentIssuingCountry() {
		return extraDocumentIssuingCountry;
	}

	public void setExtraDocumentIssuingCountry(String extraDocumentIssuingCountry) {
		this.extraDocumentIssuingCountry = extraDocumentIssuingCountry;
	}

	public String getExtraDocumentIssuingState() {
		return extraDocumentIssuingState;
	}

	public void setExtraDocumentIssuingState(String extraDocumentIssuingState) {
		this.extraDocumentIssuingState = extraDocumentIssuingState;
	}

	public String getExtraDocumentNumber() {
		return extraDocumentNumber;
	}

	public void setExtraDocumentNumber(String extraDocumentNumber) {
		this.extraDocumentNumber = extraDocumentNumber;
	}

	public DocumentTypeEnum getExtraDocumentType() {
		return extraDocumentType;
	}

	public void setExtraDocumentType(DocumentTypeEnum extraDocumentType) {
		this.extraDocumentType = extraDocumentType;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public Calendar getPassportExpirationDate() {
		return passportExpirationDate;
	}

	public void setPassportExpirationDate(Calendar passportExpirationDate) {
		this.passportExpirationDate = passportExpirationDate;
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public String getResidenceCountry() {
		return residenceCountry;
	}

	public void setResidenceCountry(String residenceCountry) {
		this.residenceCountry = residenceCountry;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((birthDate == null) ? 0 : birthDate.hashCode());
		result = prime * result
				+ ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((passportNumber == null) ? 0 : passportNumber.hashCode());
		result = prime * result
				+ ((secondName == null) ? 0 : secondName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbApisInfoData other = (MmbApisInfoData) obj;
		if (birthDate == null) {
			if (other.birthDate != null)
				return false;
		} else if (!birthDate.equals(other.birthDate))
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (passportNumber == null) {
			if (other.passportNumber != null)
				return false;
		} else if (!passportNumber.equals(other.passportNumber))
			return false;
		if (secondName == null) {
			if (other.secondName != null)
				return false;
		} else if (!secondName.equals(other.secondName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ApisInfoData [birthDate=" + birthDate + ", destinationAddress="
				+ destinationAddress + ", destinationCity=" + destinationCity
				+ ", destinationState=" + destinationState
				+ ", destinationZipCode=" + destinationZipCode + ", docType="
				+ docType + ", extraDocumentExpirationDate="
				+ extraDocumentExpirationDate
				+ ", extraDocumentIssuingCountry="
				+ extraDocumentIssuingCountry + ", extraDocumentIssuingState="
				+ extraDocumentIssuingState + ", extraDocumentNumber="
				+ extraDocumentNumber + ", extraDocumentType="
				+ extraDocumentType + ", gender=" + gender + ", lastname="
				+ lastname + ", name=" + name + ", nationality=" + nationality
				+ ", passportExpirationDate=" + passportExpirationDate
				+ ", passportNumber=" + passportNumber + ", residenceCountry="
				+ residenceCountry + ", secondName=" + secondName + "]";
	}
}
