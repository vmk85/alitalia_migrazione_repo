package com.alitalia.aem.common.data.home.enumerations;


public enum CanAddPassengerReasonEnum {

	CAN_ADD("CanAdd"),
	MAX_GROUP_PASSENGER_REACHED("MaxGroupPassengerReached"),
	ALL_AVAILABLE_PASSENGER_ADDED("AllAvailablePassengerAdded");
    
    private final String value;

    CanAddPassengerReasonEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CanAddPassengerReasonEnum fromValue(String v) {
        for (CanAddPassengerReasonEnum c: CanAddPassengerReasonEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}