package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.MmbAncillaryErrorData;
import com.alitalia.aem.common.messages.BaseResponse;

public class WebCheckinCancelCartResponse extends BaseResponse{
	
	private List<MmbAncillaryErrorData> errors;
    private String machineName;
    
	public List<MmbAncillaryErrorData> getErrors() {
		return errors;
	}
	public void setErrors(List<MmbAncillaryErrorData> errors) {
		this.errors = errors;
	}
	public String getMachineName() {
		return machineName;
	}
	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((errors == null) ? 0 : errors.hashCode());
		result = prime * result
				+ ((machineName == null) ? 0 : machineName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebCheckinCancelCartResponse other = (WebCheckinCancelCartResponse) obj;
		if (errors == null) {
			if (other.errors != null)
				return false;
		} else if (!errors.equals(other.errors))
			return false;
		if (machineName == null) {
			if (other.machineName != null)
				return false;
		} else if (!machineName.equals(other.machineName))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "WebCheckinCancelCartResponse [errors=" + errors
				+ ", machineName=" + machineName + "]";
	}
}