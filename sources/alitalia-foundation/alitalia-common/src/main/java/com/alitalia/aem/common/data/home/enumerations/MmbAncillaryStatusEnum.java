package com.alitalia.aem.common.data.home.enumerations;

public enum MmbAncillaryStatusEnum {

	AVAILABLE("Available"),
	ERROR("Error"),
	ISSUED("Issued"),
	PREVIOUSLY_ISSUED("PreviouslyIssued"),
	TO_BE_ISSUED("ToBeIssued"),
	PENDING("Pending");

	private final String value;

    MmbAncillaryStatusEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MmbAncillaryStatusEnum fromValue(String v) {
        for (MmbAncillaryStatusEnum c: MmbAncillaryStatusEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
