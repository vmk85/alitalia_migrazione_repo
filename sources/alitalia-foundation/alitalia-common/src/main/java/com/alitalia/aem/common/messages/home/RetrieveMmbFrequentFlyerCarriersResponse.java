package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.FrequentFlyerTypeData;
import com.alitalia.aem.common.data.home.mmb.MmbFrequentFlyerCarrierData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetrieveMmbFrequentFlyerCarriersResponse extends BaseResponse{

	private List<MmbFrequentFlyerCarrierData> frequentFlyerCarriers;
	
	public RetrieveMmbFrequentFlyerCarriersResponse(){}
	
	public RetrieveMmbFrequentFlyerCarriersResponse(String tid, String sid) {
		super(tid, sid);
	}

	public List<MmbFrequentFlyerCarrierData> getFrequentFlyers() {
		return frequentFlyerCarriers;
	}

	public void setFrequentFlyers(List<MmbFrequentFlyerCarrierData> frequentFlyers) {
		this.frequentFlyerCarriers = frequentFlyers;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((frequentFlyerCarriers == null) ? 0 : frequentFlyerCarriers.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveMmbFrequentFlyerCarriersResponse other = (RetrieveMmbFrequentFlyerCarriersResponse) obj;
		if (frequentFlyerCarriers == null) {
			if (other.frequentFlyerCarriers != null)
				return false;
		} else if (!frequentFlyerCarriers.equals(other.frequentFlyerCarriers))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveMmbFrequentFlyerCarriersResponse [frequentFlyerCarriers="
				+ frequentFlyerCarriers
				+ ", getTid()="
				+ getTid()
				+ ", getSid()=" + getSid() + "]";
	}
}