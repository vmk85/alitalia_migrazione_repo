package com.alitalia.aem.common.data.home.mmb.ancillary;

import java.util.List;

import com.alitalia.aem.common.data.home.enumerations.MmbFaultExceptionMessageCodeEnum;

public class MmbAncillaryCartData {

	private List<MmbAncillaryData> ancillaries;
	private List<MmbAncillaryFlightData> flights;
	private Long id;
	private String messageCodeDetail;
	private MmbFaultExceptionMessageCodeEnum messageCode;
	private List<MmbAncillaryPassengerData> passengers;

	public List<MmbAncillaryData> getAncillaries() {
		return ancillaries;
	}

	public void setAncillaries(List<MmbAncillaryData> ancillaries) {
		this.ancillaries = ancillaries;
	}

	public List<MmbAncillaryFlightData> getFlights() {
		return flights;
	}

	public void setFlights(List<MmbAncillaryFlightData> flights) {
		this.flights = flights;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMessageCodeDetail() {
		return messageCodeDetail;
	}

	public void setMessageCodeDetail(String messageCodeDetail) {
		this.messageCodeDetail = messageCodeDetail;
	}

	public MmbFaultExceptionMessageCodeEnum getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(MmbFaultExceptionMessageCodeEnum messageCode) {
		this.messageCode = messageCode;
	}

	public List<MmbAncillaryPassengerData> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<MmbAncillaryPassengerData> passengers) {
		this.passengers = passengers;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbAncillaryCartData other = (MmbAncillaryCartData) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbAncillaryCartData [ancillaries=" + ancillaries
				+ ", flights=" + flights + ", id=" + id
				+ ", messageCodeDetail=" + messageCodeDetail + ", messageCode="
				+ messageCode + ", passengers=" + passengers + "]";
	}

}
