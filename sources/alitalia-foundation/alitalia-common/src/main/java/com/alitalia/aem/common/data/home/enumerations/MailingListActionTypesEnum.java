package com.alitalia.aem.common.data.home.enumerations;

public enum MailingListActionTypesEnum {

	REGISTER("Register"),
    DELETE("Delete");
	
    private final String value;

    MailingListActionTypesEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MailingListActionTypesEnum fromValue(String v) {
        for (MailingListActionTypesEnum c: MailingListActionTypesEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}