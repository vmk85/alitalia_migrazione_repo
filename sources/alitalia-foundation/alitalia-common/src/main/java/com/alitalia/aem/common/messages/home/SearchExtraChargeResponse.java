package com.alitalia.aem.common.messages.home;

import java.util.ArrayList;
import java.util.List;

import com.alitalia.aem.common.data.PassengerBase;
import com.alitalia.aem.common.messages.BaseResponse;

public class SearchExtraChargeResponse extends BaseResponse {

	private List<PassengerBase> passengerList;

	public List<PassengerBase> getPassengerList() {
		return passengerList;
	}

	public void setPassengerList(List<PassengerBase> passengerList) {
		this.passengerList = passengerList;
	}

	public void addPassenger(PassengerBase passenger) {
		if (this.passengerList == null)
			this.passengerList = new ArrayList<PassengerBase>();

		this.passengerList.add(passenger);
	}
	@Override
	public String toString() {
		return "SearchFlightSolutionResponse [passengerList=" + passengerList
				+ ", getTid()=" + getTid() + ", getSid()=" + getSid() + "]";
	}

}
