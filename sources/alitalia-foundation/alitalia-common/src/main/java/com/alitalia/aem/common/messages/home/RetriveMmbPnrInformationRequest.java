package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.MmbBaseRequest;

public class RetriveMmbPnrInformationRequest extends MmbBaseRequest {

	private String lastName;
	private String name;
	private String locale;
	/** 
	 * Il TripId che puo' essere il codice MM del cliente con il formato Mm00000000,
	 * rispettando la capitalization e paddando con 0 a sinistra il codice MM,
	 * il PNR o il numero di biglietto
	 */
	private String tripId;

	public RetriveMmbPnrInformationRequest(String tid, String sid) {
		super(tid, sid);
	}

	public RetriveMmbPnrInformationRequest() {
		super();
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getTripId() {
		return tripId;
	}

	public void setTripId(String tripId) {
		this.tripId = tripId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((locale == null) ? 0 : locale.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((tripId == null) ? 0 : tripId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetriveMmbPnrInformationRequest other = (RetriveMmbPnrInformationRequest) obj;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (locale == null) {
			if (other.locale != null)
				return false;
		} else if (!locale.equals(other.locale))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (tripId == null) {
			if (other.tripId != null)
				return false;
		} else if (!tripId.equals(other.tripId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetriveMmbPnrInformationRequest [lastName=" + lastName
				+ ", name=" + name + ", locale=" + locale + ", tripId="
				+ tripId + ", getBaseInfo()=" + getBaseInfo()
				+ ", getClient()=" + getClient() + ", getTid()=" + getTid()
				+ ", getSid()=" + getSid() + "]";
	}

}
