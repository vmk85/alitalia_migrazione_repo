package com.alitalia.aem.common.data.home;

import java.util.Calendar;

import com.alitalia.aem.common.data.home.enumerations.GenderTypeEnum;

public class PassengerBaseInfoData {

	private Calendar birthDate;
	private GenderTypeEnum gender;

	public PassengerBaseInfoData() {
		super();
	}

	public PassengerBaseInfoData(PassengerBaseInfoData clone) {
		this.birthDate = clone.getBirthDate();
		this.gender = clone.getGender();
	}

	public Calendar getBirthDate() {
		return birthDate;
	}
	
	public void setBirthDate(Calendar birthDate) {
		this.birthDate = birthDate;
	}
	
	public GenderTypeEnum getGender() {
		return gender;
	}
	
	public void setGender(GenderTypeEnum gender) {
		this.gender = gender;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((birthDate == null) ? 0 : birthDate.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PassengerBaseInfoData other = (PassengerBaseInfoData) obj;
		if (birthDate == null) {
			if (other.birthDate != null)
				return false;
		} else if (!birthDate.equals(other.birthDate))
			return false;
		if (gender != other.gender)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PassengerBaseInfoData [birthDate=" + birthDate + ", gender="
				+ gender + "]";
	}
}
