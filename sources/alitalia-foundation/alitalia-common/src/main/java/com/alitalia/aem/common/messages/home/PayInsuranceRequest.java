package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.common.data.home.enumerations.CreditCardTypeEnum;
import com.alitalia.aem.common.messages.BaseRequest;

public class PayInsuranceRequest extends BaseRequest {

	private String nameTp;
    private RoutesData prenotationTp;
    private String creditCardCvcTp;
    private String creditCardNumberTp;
    private CreditCardTypeEnum creditCardTypeTp;
    private String eMailTp;
    private short expireMonthTp;
    private short expireYearTp;
    private String lastNameTp;
	
    public String getNameTp() {
		return nameTp;
	}
	public void setNameTp(String nameTp) {
		this.nameTp = nameTp;
	}
	public RoutesData getPrenotationTp() {
		return prenotationTp;
	}
	public void setPrenotationTp(RoutesData prenotationTp) {
		this.prenotationTp = prenotationTp;
	}
	public String getCreditCardCvcTp() {
		return creditCardCvcTp;
	}
	public void setCreditCardCvcTp(String creditCardCvcTp) {
		this.creditCardCvcTp = creditCardCvcTp;
	}
	public String getCreditCardNumberTp() {
		return creditCardNumberTp;
	}
	public void setCreditCardNumberTp(String creditCardNumberTp) {
		this.creditCardNumberTp = creditCardNumberTp;
	}
	public CreditCardTypeEnum getCreditCardTypeTp() {
		return creditCardTypeTp;
	}
	public void setCreditCardTypeTp(CreditCardTypeEnum creditCardTypeTp) {
		this.creditCardTypeTp = creditCardTypeTp;
	}
	public String geteMailTp() {
		return eMailTp;
	}
	public void seteMailTp(String eMailTp) {
		this.eMailTp = eMailTp;
	}
	public short getExpireMonthTp() {
		return expireMonthTp;
	}
	public void setExpireMonthTp(short expireMonthTp) {
		this.expireMonthTp = expireMonthTp;
	}
	public short getExpireYearTp() {
		return expireYearTp;
	}
	public void setExpireYearTp(short expireYearTp) {
		this.expireYearTp = expireYearTp;
	}
	public String getLastNameTp() {
		return lastNameTp;
	}
	public void setLastNameTp(String lastNameTp) {
		this.lastNameTp = lastNameTp;
	}
    
    
}