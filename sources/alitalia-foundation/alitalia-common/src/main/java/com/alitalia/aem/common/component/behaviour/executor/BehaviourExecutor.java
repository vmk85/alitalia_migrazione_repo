package com.alitalia.aem.common.component.behaviour.executor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.Request;
import com.alitalia.aem.common.messages.Response;

public class BehaviourExecutor <B extends Behaviour<S, T>, S extends Request, T extends Response>{

	private static final Logger logger = LoggerFactory.getLogger(BehaviourExecutor.class);

	public T executeBehaviour(B behaviour, S request){
		logger.debug("Executing behaviour {} on {}", behaviour, request);
		return behaviour.execute(request);
	}
}