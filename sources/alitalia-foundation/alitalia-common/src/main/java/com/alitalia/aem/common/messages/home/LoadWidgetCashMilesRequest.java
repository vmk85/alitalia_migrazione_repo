package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.TaxData;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.messages.BaseRequest;

public class LoadWidgetCashMilesRequest extends BaseRequest {
	
	private String site;
    private String language;
    private String mmCode;
    private String mmPin;
	
	public LoadWidgetCashMilesRequest(String tid, String sid) {
		super(tid, sid);
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getMmCode() {
		return mmCode;
	}

	public void setMmCode(String mmCode) {
		this.mmCode = mmCode;
	}

	public String getMmPin() {
		return mmPin;
	}

	public void setMmPin(String mmPin) {
		this.mmPin = mmPin;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((language == null) ? 0 : language.hashCode());
		result = prime * result + ((mmCode == null) ? 0 : mmCode.hashCode());
		result = prime * result + ((mmPin == null) ? 0 : mmPin.hashCode());
		result = prime * result + ((site == null) ? 0 : site.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoadWidgetCashMilesRequest other = (LoadWidgetCashMilesRequest) obj;
		if (language == null) {
			if (other.language != null)
				return false;
		} else if (!language.equals(other.language))
			return false;
		if (mmCode == null) {
			if (other.mmCode != null)
				return false;
		} else if (!mmCode.equals(other.mmCode))
			return false;
		if (mmPin == null) {
			if (other.mmPin != null)
				return false;
		} else if (!mmPin.equals(other.mmPin))
			return false;
		if (site == null) {
			if (other.site != null)
				return false;
		} else if (!site.equals(other.site))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "LoadWidgetCashMilesRequest [site=" + site + ", language="
				+ language + ", mmCode=" + mmCode + ", mmPin=" + mmPin
				+ ", getTid()=" + getTid() + ", getSid()=" + getSid() + "]";
	}
	
}
