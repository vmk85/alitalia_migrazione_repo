package com.alitalia.aem.common.data.home;

import java.util.List;

public class SeatMapRowData {

	private String number;
    private List<SeatData> seats;
	
    public String getNumber() {
		return number;
	}
	
    public void setNumber(String number) {
		this.number = number;
	}
	
    public List<SeatData> getSeats() {
		return seats;
	}
	
    public void setSeats(List<SeatData> seats) {
		this.seats = seats;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((seats == null) ? 0 : seats.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SeatMapRowData other = (SeatMapRowData) obj;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (seats == null) {
			if (other.seats != null)
				return false;
		} else if (!seats.equals(other.seats))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SeatMapRowData [number=" + number + ", seats=" + seats + "]";
	}
}