package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class MilleMigliaLoginRequest extends BaseRequest {

	private String customerCode;
	private String customerPin;
	
	public MilleMigliaLoginRequest() {
		super();
	}
	
	public MilleMigliaLoginRequest(String tid, String sid) {
		super(tid, sid);
	}
	
	public MilleMigliaLoginRequest(String tid, String sid, String customerCode, String customerPin) {
		super(tid, sid);
		this.customerCode = customerCode;
		this.customerPin = customerPin;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerPin() {
		return customerPin;
	}

	public void setCustomerPin(String customerPin) {
		this.customerPin = customerPin;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((customerCode == null) ? 0 : customerCode.hashCode());
		result = prime * result + ((customerPin == null) ? 0 : customerPin.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MilleMigliaLoginRequest other = (MilleMigliaLoginRequest) obj;
		if (customerCode == null) {
			if (other.customerCode != null)
				return false;
		} else if (!customerCode.equals(other.customerCode))
			return false;
		if (customerPin == null) {
			if (other.customerPin != null)
				return false;
		} else if (!customerPin.equals(other.customerPin))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MilleMigliaLoginRequest [customerCode=" + customerCode + ", customerPin=" + customerPin + "]";
	}
	
}
