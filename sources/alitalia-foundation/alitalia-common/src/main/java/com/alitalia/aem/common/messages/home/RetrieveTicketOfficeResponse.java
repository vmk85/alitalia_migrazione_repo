package com.alitalia.aem.common.messages.home;
import java.util.List;

import com.alitalia.aem.common.data.home.TicketOfficeData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetrieveTicketOfficeResponse extends BaseResponse{
	List<TicketOfficeData> ticketOfficeData;

	public List<TicketOfficeData> getTicketOfficeData() {
		return ticketOfficeData;
	}

	public void setTicketOfficeData(List<TicketOfficeData> ticketOfficeData) {
		this.ticketOfficeData = ticketOfficeData;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((ticketOfficeData == null) ? 0 : ticketOfficeData.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveTicketOfficeResponse other = (RetrieveTicketOfficeResponse) obj;
		if (ticketOfficeData == null) {
			if (other.ticketOfficeData != null)
				return false;
		} else if (!ticketOfficeData.equals(other.ticketOfficeData))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveTicketOfficeResponse [ticketOfficeData="
				+ ticketOfficeData + "]";
	}

	

}
