package com.alitalia.aem.common.data.home.enumerations;

public enum MmbGdsTypeEnum {

    NONE("None"),
    AZ("AZ"),
    NOT_AZ("notAZ");
    private final String value;

    MmbGdsTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MmbGdsTypeEnum fromValue(String v) {
        for (MmbGdsTypeEnum c: MmbGdsTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
