package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.enumerations.CreditCardTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;


public class RegisterPaymentAuthAttemptStatisticRequest extends RegisterStatisticsRequest {

	private CreditCardTypeEnum ccName;
	private String email;
	private PaymentTypeEnum paymentType;
	private String threeDSCheck;
	
	public RegisterPaymentAuthAttemptStatisticRequest(String tid, String sid) {
		super(tid,sid);
	}
	public CreditCardTypeEnum getCcName() {
		return ccName;
	}
	public void setCcName(CreditCardTypeEnum ccName) {
		this.ccName = ccName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public PaymentTypeEnum getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(PaymentTypeEnum paymentType) {
		this.paymentType = paymentType;
	}
	public String getThreeDSCheck() {
		return threeDSCheck;
	}
	public void setThreeDSCheck(String threeDSCheck) {
		this.threeDSCheck = threeDSCheck;
	}
	
	@Override
	public String toString() {
		return "RegisterPaymentAuthAttemptStatisticRequest [ccName=" + ccName
				+ ", email=" + email + ", paymentType=" + paymentType
				+ ", threeDSCheck=" + threeDSCheck + ", getCcName()="
				+ getCcName() + ", getEmail()=" + getEmail()
				+ ", getPaymentType()=" + getPaymentType()
				+ ", getThreeDSCheck()=" + getThreeDSCheck()
				+ ", getClientIP()=" + getClientIP() + ", getErrorDescr()="
				+ getErrorDescr() + ", getSessionId()=" + getSessionId()
				+ ", getSiteCode()=" + getSiteCode() + ", isSuccess()="
				+ isSuccess() + ", getType()=" + getType() + ", getUserId()="
				+ getUserId() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + ", getTid()=" + getTid() + ", getSid()="
				+ getSid() + ", getClass()=" + getClass() + "]";
	}
}