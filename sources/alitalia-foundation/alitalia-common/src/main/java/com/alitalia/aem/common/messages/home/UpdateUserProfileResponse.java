package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.BaseResponse;

public class UpdateUserProfileResponse extends BaseResponse {

	private MMCustomerProfileData customerProfile;
	private List<String> customerUpdateErrors;
	
	public MMCustomerProfileData getCustomerProfile() {
		return customerProfile;
	}
	
	public void setCustomerProfile(MMCustomerProfileData customerProfile) {
		this.customerProfile = customerProfile;
	}
	
	public List<String> getCustomerUpdateErrors() {
		return customerUpdateErrors;
	}
	
	public void setCustomerUpdateErrors(List<String> customerUpdateErrors) {
		this.customerUpdateErrors = customerUpdateErrors;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((customerProfile == null) ? 0 : customerProfile.hashCode());
		result = prime
				* result
				+ ((customerUpdateErrors == null) ? 0 : customerUpdateErrors
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		UpdateUserProfileResponse other = (UpdateUserProfileResponse) obj;
		if (customerProfile == null) {
			if (other.customerProfile != null)
				return false;
		} else if (!customerProfile.equals(other.customerProfile))
			return false;
		if (customerUpdateErrors == null) {
			if (other.customerUpdateErrors != null)
				return false;
		} else if (!customerUpdateErrors.equals(other.customerUpdateErrors))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UpdateUserProfileResponse [customerProfile=" + customerProfile
				+ ", customerUpdateErrors=" + customerUpdateErrors + "]";
	}
}