package com.alitalia.aem.common.data.home;

import java.util.ArrayList;
import java.util.List;

import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;

public class TabData {

    private List<FlightTabData> flightTabs;
    protected RouteTypeEnum type;
	
    public List<FlightTabData> getFlightTabs() {
		return flightTabs;
	}
	
    public void setFlightTabs(List<FlightTabData> flightTabs) {
		this.flightTabs = flightTabs;
	}
	
    public RouteTypeEnum getType() {
		return type;
	}
	
    public void setType(RouteTypeEnum type) {
		this.type = type;
	}

    public void addFlightTab(FlightTabData flightTabData) {
    	if (this.flightTabs == null)
    		this.flightTabs = new ArrayList<FlightTabData>();

    	this.flightTabs.add(flightTabData);
    }

	@Override
	public String toString() {
		return "TabData [flightTabs=" + flightTabs + ", type=" + type + "]";
	}
}
