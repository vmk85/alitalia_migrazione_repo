package com.alitalia.aem.common.data.home;

public class DestinationDetailsData {

	private String codeStatus;
	private String opertiveInfo;
	
	public String getCodeStatus() {
		return codeStatus;
	}
	
	public void setCodeStatus(String codeStatus) {
		this.codeStatus = codeStatus;
	}
	
	public String getOpertiveInfo() {
		return opertiveInfo;
	}
	
	public void setOpertiveInfo(String opertiveInfo) {
		this.opertiveInfo = opertiveInfo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((codeStatus == null) ? 0 : codeStatus.hashCode());
		result = prime * result
				+ ((opertiveInfo == null) ? 0 : opertiveInfo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DestinationDetailsData other = (DestinationDetailsData) obj;
		if (codeStatus == null) {
			if (other.codeStatus != null)
				return false;
		} else if (!codeStatus.equals(other.codeStatus))
			return false;
		if (opertiveInfo == null) {
			if (other.opertiveInfo != null)
				return false;
		} else if (!opertiveInfo.equals(other.opertiveInfo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DestinationDetailsData [codeStatus=" + codeStatus
				+ ", opertiveInfo=" + opertiveInfo + "]";
	}
}