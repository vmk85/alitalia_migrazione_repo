package com.alitalia.aem.common.data.home.carnet;

import com.alitalia.aem.common.data.home.enumerations.CarnetPaymentStepEnum;
import com.alitalia.aem.common.data.home.enumerations.CarnetResultTypeEnum;

public class CarnetProcessInfoData {

    private String marketCode;
    private Integer paymentAttemps;
    private String pointOfSale;
    private String redirectUrl;
    private CarnetResultTypeEnum result;
    private String sessionId;
    private String shopId;
    private String siteCode;
    private CarnetPaymentStepEnum step;
    private String tokenId;
    private String transactionId;

	public String getMarketCode() {
		return marketCode;
	}
	public void setMarketCode(String marketCode) {
		this.marketCode = marketCode;
	}
	public Integer getPaymentAttemps() {
		return paymentAttemps;
	}
	public void setPaymentAttemps(Integer paymentAttemps) {
		this.paymentAttemps = paymentAttemps;
	}
	public String getPointOfSale() {
		return pointOfSale;
	}
	public void setPointOfSale(String pointOfSale) {
		this.pointOfSale = pointOfSale;
	}
	public String getRedirectUrl() {
		return redirectUrl;
	}
	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
	public CarnetResultTypeEnum getResult() {
		return result;
	}
	public void setResult(CarnetResultTypeEnum result) {
		this.result = result;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getShopId() {
		return shopId;
	}
	public void setShopId(String shopId) {
		this.shopId = shopId;
	}
	public String getSiteCode() {
		return siteCode;
	}
	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}
	public CarnetPaymentStepEnum getStep() {
		return step;
	}
	public void setStep(CarnetPaymentStepEnum step) {
		this.step = step;
	}
	public String getTokenId() {
		return tokenId;
	}
	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((marketCode == null) ? 0 : marketCode.hashCode());
		result = prime * result
				+ ((paymentAttemps == null) ? 0 : paymentAttemps.hashCode());
		result = prime * result
				+ ((pointOfSale == null) ? 0 : pointOfSale.hashCode());
		result = prime * result
				+ ((redirectUrl == null) ? 0 : redirectUrl.hashCode());
		result = prime * result
				+ ((this.result == null) ? 0 : this.result.hashCode());
		result = prime * result
				+ ((sessionId == null) ? 0 : sessionId.hashCode());
		result = prime * result + ((shopId == null) ? 0 : shopId.hashCode());
		result = prime * result
				+ ((siteCode == null) ? 0 : siteCode.hashCode());
		result = prime * result + ((step == null) ? 0 : step.hashCode());
		result = prime * result + ((tokenId == null) ? 0 : tokenId.hashCode());
		result = prime * result
				+ ((transactionId == null) ? 0 : transactionId.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarnetProcessInfoData other = (CarnetProcessInfoData) obj;
		if (marketCode == null) {
			if (other.marketCode != null)
				return false;
		} else if (!marketCode.equals(other.marketCode))
			return false;
		if (paymentAttemps == null) {
			if (other.paymentAttemps != null)
				return false;
		} else if (!paymentAttemps.equals(other.paymentAttemps))
			return false;
		if (pointOfSale == null) {
			if (other.pointOfSale != null)
				return false;
		} else if (!pointOfSale.equals(other.pointOfSale))
			return false;
		if (redirectUrl == null) {
			if (other.redirectUrl != null)
				return false;
		} else if (!redirectUrl.equals(other.redirectUrl))
			return false;
		if (result != other.result)
			return false;
		if (sessionId == null) {
			if (other.sessionId != null)
				return false;
		} else if (!sessionId.equals(other.sessionId))
			return false;
		if (shopId == null) {
			if (other.shopId != null)
				return false;
		} else if (!shopId.equals(other.shopId))
			return false;
		if (siteCode == null) {
			if (other.siteCode != null)
				return false;
		} else if (!siteCode.equals(other.siteCode))
			return false;
		if (step != other.step)
			return false;
		if (tokenId == null) {
			if (other.tokenId != null)
				return false;
		} else if (!tokenId.equals(other.tokenId))
			return false;
		if (transactionId == null) {
			if (other.transactionId != null)
				return false;
		} else if (!transactionId.equals(other.transactionId))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CarnetProcessInfoData [marketCode=" + marketCode
				+ ", paymentAttemps=" + paymentAttemps + ", pointOfSale="
				+ pointOfSale + ", redirectUrl=" + redirectUrl + ", result="
				+ result + ", sessionId=" + sessionId + ", shopId=" + shopId
				+ ", siteCode=" + siteCode + ", step=" + step + ", tokenId="
				+ tokenId + ", transactionId=" + transactionId + "]";
	}
}
