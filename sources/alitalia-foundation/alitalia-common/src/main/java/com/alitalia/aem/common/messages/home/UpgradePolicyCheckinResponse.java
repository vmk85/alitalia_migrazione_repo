package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.checkin.CheckinUpgradeClassPolicyData;
import com.alitalia.aem.common.messages.BaseResponse;

public class UpgradePolicyCheckinResponse extends BaseResponse {

	private List<CheckinUpgradeClassPolicyData> policy;

	public List<CheckinUpgradeClassPolicyData> getPolicy() {
		return policy;
	}

	public void setPolicy(List<CheckinUpgradeClassPolicyData> policy) {
		this.policy = policy;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((policy == null) ? 0 : policy.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		UpgradePolicyCheckinResponse other = (UpgradePolicyCheckinResponse) obj;
		if (policy == null) {
			if (other.policy != null)
				return false;
		} else if (!policy.equals(other.policy))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UpgradePolicyCheckinResponse [policy=" + policy
				+ ", toString()=" + super.toString() + "]";
	}

}
