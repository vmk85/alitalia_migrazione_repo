package com.alitalia.aem.common.data.home;

public class ResultBookingPriceSolutionSliceSegmentPricingExtData {
	
	private ResultBookingPriceSolutionSliceSegmentPricingExtPaxData paxField;
	
	public ResultBookingPriceSolutionSliceSegmentPricingExtPaxData getPaxField() {
		return paxField;
	}
	
	public void setPaxField(
			ResultBookingPriceSolutionSliceSegmentPricingExtPaxData paxField) {
		this.paxField = paxField;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((paxField == null) ? 0 : paxField.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingPriceSolutionSliceSegmentPricingExtData other = (ResultBookingPriceSolutionSliceSegmentPricingExtData) obj;
		if (paxField == null) {
			if (other.paxField != null)
				return false;
		} else if (!paxField.equals(other.paxField))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingPriceSolutionSliceSegmentPricingExtData [paxField="
				+ paxField + "]";
	}
}
