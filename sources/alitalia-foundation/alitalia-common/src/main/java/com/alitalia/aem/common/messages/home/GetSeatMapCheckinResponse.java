package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.checkin.CheckinSeatMapData;
import com.alitalia.aem.common.messages.BaseResponse;

public class GetSeatMapCheckinResponse extends BaseResponse {

	private List<CheckinSeatMapData> seatMap;

	public List<CheckinSeatMapData> getSeatMap() {
		return seatMap;
	}

	public void setSeatMap(List<CheckinSeatMapData> seatMap) {
		this.seatMap = seatMap;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((seatMap == null) ? 0 : seatMap.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		GetSeatMapCheckinResponse other = (GetSeatMapCheckinResponse) obj;
		if (seatMap == null) {
			if (other.seatMap != null)
				return false;
		} else if (!seatMap.equals(other.seatMap))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "GetSeatMapCheckinResponse [seatMap=" + seatMap
				+ ", toString()=" + super.toString() + "]";
	}

}
