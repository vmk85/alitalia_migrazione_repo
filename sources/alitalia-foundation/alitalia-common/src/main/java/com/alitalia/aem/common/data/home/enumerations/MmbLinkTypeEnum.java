package com.alitalia.aem.common.data.home.enumerations;

public enum MmbLinkTypeEnum {

    NONE("None"),
    INFANT_ACCOMPANIST("InfantAccompanist");
    private final String value;

    MmbLinkTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MmbLinkTypeEnum fromValue(String v) {
        for (MmbLinkTypeEnum c: MmbLinkTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
