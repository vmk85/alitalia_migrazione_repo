package com.alitalia.aem.common.data.home.mmb.ancillary;

import com.alitalia.aem.common.data.MmbAncillaryDetail;

public class MmbAncillaryMealDetailData implements MmbAncillaryDetail {

	private String meal;

	public String getMeal() {
		return meal;
	}

	public void setMeal(String meal) {
		this.meal = meal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((meal == null) ? 0 : meal.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbAncillaryMealDetailData other = (MmbAncillaryMealDetailData) obj;
		if (meal == null) {
			if (other.meal != null)
				return false;
		} else if (!meal.equals(other.meal))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbMealDetailData [meal=" + meal + "]";
	}

}
