package com.alitalia.aem.common.data.home;

public class PassengerApisSecureFlightInfoData extends PassengerBaseInfoData {

    private String nationality;
    private String passportNumber;
    private String secondName;
	
    public String getNationality() {
		return nationality;
	}
	
    public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	
    public String getPassportNumber() {
		return passportNumber;
	}
	
    public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}
	
    public String getSecondName() {
		return secondName;
	}
	
    public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((nationality == null) ? 0 : nationality.hashCode());
		result = prime * result
				+ ((passportNumber == null) ? 0 : passportNumber.hashCode());
		result = prime * result
				+ ((secondName == null) ? 0 : secondName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PassengerApisSecureFlightInfoData other = (PassengerApisSecureFlightInfoData) obj;
		if (nationality == null) {
			if (other.nationality != null)
				return false;
		} else if (!nationality.equals(other.nationality))
			return false;
		if (passportNumber == null) {
			if (other.passportNumber != null)
				return false;
		} else if (!passportNumber.equals(other.passportNumber))
			return false;
		if (secondName == null) {
			if (other.secondName != null)
				return false;
		} else if (!secondName.equals(other.secondName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ApisSecureFlightInfoData [nationality=" + nationality
				+ ", passportNumber=" + passportNumber + ", secondName="
				+ secondName + "]";
	}

}
