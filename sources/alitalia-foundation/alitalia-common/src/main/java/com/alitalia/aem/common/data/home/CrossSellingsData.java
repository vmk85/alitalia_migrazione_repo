package com.alitalia.aem.common.data.home;

import java.util.List;

public class CrossSellingsData {

	private List<CodeDescriptionData> crossSellingInformations;
	private List<CrossSellingBaseData> items;

	public List<CodeDescriptionData> getCrossSellingInformations() {
		return crossSellingInformations;
	}

	public void setCrossSellingInformations(List<CodeDescriptionData> crossSellingInformations) {
		this.crossSellingInformations = crossSellingInformations;
	}

	public List<CrossSellingBaseData> getItems() {
		return items;
	}

	public void setItems(List<CrossSellingBaseData> items) {
		this.items = items;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((crossSellingInformations == null) ? 0 : crossSellingInformations.hashCode());
		result = prime * result + ((items == null) ? 0 : items.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CrossSellingsData other = (CrossSellingsData) obj;
		if (crossSellingInformations == null) {
			if (other.crossSellingInformations != null)
				return false;
		} else if (!crossSellingInformations.equals(other.crossSellingInformations))
			return false;
		if (items == null) {
			if (other.items != null)
				return false;
		} else if (!items.equals(other.items))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CrossSellingsData [crossSellingInformations=" + crossSellingInformations + ", items=" + items + "]";
	}

}
