package com.alitalia.aem.common.data.home.mmb;

import java.util.List;

import com.alitalia.aem.common.data.home.enumerations.ContactTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAQQTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbLinkTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbPassengerNoteEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbPassengerStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbUpdateSSREnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;

public class MmbPassengerData {

	private Integer id;
	private MmbApisInfoData apisDataToUpdate;
	private MmbApisInfoData apisData;
	private MmbAQQTypeEnum authorityPermission;
	private Integer baggageAllowanceQuantity;
	private String baggageAllowance;
	private String boardingPassRetrieveEmail;
	private String boardingPassRetrieveSms;
	private Boolean boardingPassViaEmail;
	private Boolean boardingPassViaSms;
	private Boolean comfortSeatFree;
	private Boolean corporate;
	private List<MmbCouponData> coupons;
	private String email1;
	private String email2;
	private String eticketClass;
	private String eticket;
	private List<MmbFlightData> flights;
	private MmbFrequentFlyerCarrierData frequentFlyerCarrier;
	private String frequentFlyerCode;
	private String frequentFlyerCustomerValue;
	private MmbFrequentFlyerTypeData frequentFlyerType;
	private String gender;
	private Boolean specialFare;
	private String lastName;
	private MmbLinkTypeEnum linkType;
	private String name;
	private MmbPassengerNoteEnum note;
	private MmbBaggageOrderData baggageOrder;
	private String paymentRetrieveEmail;
	private Boolean paymentViaEmail;
	private String pnr;
	private MmbPreferencesData preferences;
	private Integer routeId;
	private MmbCompartimentalClassEnum seatClass;
	private String seat;
	private Boolean selected;
	private String statusNote;
	private MmbPassengerStatusEnum status;
	private String telephone1;
	private String telephone2;
	private List<MmbTicketData> tickets;
	private String travelerRefNumber;
	private PassengerTypeEnum type;
	private String updateFlightRefNumberRPH;
	private ContactTypeEnum contactType;
	private MmbUpdateSSREnum update;

	public MmbApisInfoData getApisData() {
		return apisData;
	}

	public void setApisData(MmbApisInfoData apisData) {
		this.apisData = apisData;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public MmbApisInfoData getApisDataToUpdate() {
		return apisDataToUpdate;
	}

	public void setApisDataToUpdate(MmbApisInfoData apisDataToUpdate) {
		this.apisDataToUpdate = apisDataToUpdate;
	}

	public MmbAQQTypeEnum getAuthorityPermission() {
		return authorityPermission;
	}

	public void setAuthorityPermission(MmbAQQTypeEnum authorityPermission) {
		this.authorityPermission = authorityPermission;
	}

	public Integer getBaggageAllowanceQuantity() {
		return baggageAllowanceQuantity;
	}

	public void setBaggageAllowanceQuantity(Integer baggageAllowanceQuantity) {
		this.baggageAllowanceQuantity = baggageAllowanceQuantity;
	}

	public String getBaggageAllowance() {
		return baggageAllowance;
	}

	public void setBaggageAllowance(String baggageAllowance) {
		this.baggageAllowance = baggageAllowance;
	}

	public String getBoardingPassRetrieveEmail() {
		return boardingPassRetrieveEmail;
	}

	public void setBoardingPassRetrieveEmail(String boardingPassRetrieveEmail) {
		this.boardingPassRetrieveEmail = boardingPassRetrieveEmail;
	}

	public String getBoardingPassRetrieveSms() {
		return boardingPassRetrieveSms;
	}

	public void setBoardingPassRetrieveSms(String boardingPassRetrieveSms) {
		this.boardingPassRetrieveSms = boardingPassRetrieveSms;
	}

	public Boolean isBoardingPassViaEmail() {
		return boardingPassViaEmail;
	}

	public void setBoardingPassViaEmail(Boolean boardingPassViaEmail) {
		this.boardingPassViaEmail = boardingPassViaEmail;
	}

	public Boolean isBoardingPassViaSms() {
		return boardingPassViaSms;
	}

	public void setBoardingPassViaSms(Boolean boardingPassViaSms) {
		this.boardingPassViaSms = boardingPassViaSms;
	}

	public Boolean isComfortSeatFree() {
		return comfortSeatFree;
	}

	public void setComfortSeatFree(Boolean comfortSeatFree) {
		this.comfortSeatFree = comfortSeatFree;
	}

	public Boolean isCorporate() {
		return corporate;
	}

	public void setCorporate(Boolean corporate) {
		this.corporate = corporate;
	}

	public List<MmbCouponData> getCoupons() {
		return coupons;
	}

	public void setCoupons(List<MmbCouponData> coupons) {
		this.coupons = coupons;
	}

	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public String getEticketClass() {
		return eticketClass;
	}

	public void setEticketClass(String eticketClass) {
		this.eticketClass = eticketClass;
	}

	public String getEticket() {
		return eticket;
	}

	public void setEticket(String eticket) {
		this.eticket = eticket;
	}

	public List<MmbFlightData> getFlights() {
		return flights;
	}

	public void setFlights(List<MmbFlightData> flights) {
		this.flights = flights;
	}

	public MmbFrequentFlyerCarrierData getFrequentFlyerCarrier() {
		return frequentFlyerCarrier;
	}

	public void setFrequentFlyerCarrier(
			MmbFrequentFlyerCarrierData frequentFlyerCarrier) {
		this.frequentFlyerCarrier = frequentFlyerCarrier;
	}

	public String getFrequentFlyerCode() {
		return frequentFlyerCode;
	}

	public void setFrequentFlyerCode(String frequentFlyerCode) {
		this.frequentFlyerCode = frequentFlyerCode;
	}

	public String getFrequentFlyerCustomerValue() {
		return frequentFlyerCustomerValue;
	}

	public void setFrequentFlyerCustomerValue(String frequentFlyerCustomerValue) {
		this.frequentFlyerCustomerValue = frequentFlyerCustomerValue;
	}

	public MmbFrequentFlyerTypeData getFrequentFlyerType() {
		return frequentFlyerType;
	}

	public void setFrequentFlyerType(MmbFrequentFlyerTypeData frequentFlyerType) {
		this.frequentFlyerType = frequentFlyerType;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Boolean isSpecialFare() {
		return specialFare;
	}

	public void setSpecialFare(Boolean isSpecialFare) {
		this.specialFare = isSpecialFare;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public MmbLinkTypeEnum getLinkType() {
		return linkType;
	}

	public void setLinkType(MmbLinkTypeEnum linkType) {
		this.linkType = linkType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MmbPassengerNoteEnum getNote() {
		return note;
	}

	public void setNote(MmbPassengerNoteEnum note) {
		this.note = note;
	}

	public MmbBaggageOrderData getBaggageOrder() {
		return baggageOrder;
	}

	public void setBaggageOrder(MmbBaggageOrderData baggageOrder) {
		this.baggageOrder = baggageOrder;
	}

	public String getPaymentRetrieveEmail() {
		return paymentRetrieveEmail;
	}

	public void setPaymentRetrieveEmail(String paymentRetrieveEmail) {
		this.paymentRetrieveEmail = paymentRetrieveEmail;
	}

	public Boolean isPaymentViaEmail() {
		return paymentViaEmail;
	}

	public void setPaymentViaEmail(Boolean paymentViaEmail) {
		this.paymentViaEmail = paymentViaEmail;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public MmbPreferencesData getPreferences() {
		return preferences;
	}

	public void setPreferences(MmbPreferencesData preferences) {
		this.preferences = preferences;
	}

	public Integer getRouteId() {
		return routeId;
	}

	public void setRouteId(Integer routeId) {
		this.routeId = routeId;
	}

	public MmbCompartimentalClassEnum getSeatClass() {
		return seatClass;
	}

	public void setSeatClass(MmbCompartimentalClassEnum seatClass) {
		this.seatClass = seatClass;
	}

	public String getSeat() {
		return seat;
	}

	public void setSeat(String seat) {
		this.seat = seat;
	}

	public Boolean isSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public String getStatusNote() {
		return statusNote;
	}

	public void setStatusNote(String statusNote) {
		this.statusNote = statusNote;
	}

	public MmbPassengerStatusEnum getStatus() {
		return status;
	}

	public void setStatus(MmbPassengerStatusEnum status) {
		this.status = status;
	}

	public String getTelephone1() {
		return telephone1;
	}

	public void setTelephone1(String telephone1) {
		this.telephone1 = telephone1;
	}

	public String getTelephone2() {
		return telephone2;
	}

	public void setTelephone2(String telephone2) {
		this.telephone2 = telephone2;
	}

	public List<MmbTicketData> getTickets() {
		return tickets;
	}

	public void setTickets(List<MmbTicketData> tickets) {
		this.tickets = tickets;
	}

	public String getTravelerRefNumber() {
		return travelerRefNumber;
	}

	public void setTravelerRefNumber(String travelerRefNumber) {
		this.travelerRefNumber = travelerRefNumber;
	}

	public PassengerTypeEnum getType() {
		return type;
	}

	public void setType(PassengerTypeEnum type) {
		this.type = type;
	}

	public String getUpdateFlightRefNumberRPH() {
		return updateFlightRefNumberRPH;
	}

	public void setUpdateFlightRefNumberRPH(String updateFlightRefNumberRPH) {
		this.updateFlightRefNumberRPH = updateFlightRefNumberRPH;
	}

	public ContactTypeEnum getContactType() {
		return contactType;
	}

	public void setContactType(ContactTypeEnum contactType) {
		this.contactType = contactType;
	}

	public MmbUpdateSSREnum getUpdate() {
		return update;
	}

	public void setUpdate(MmbUpdateSSREnum update) {
		this.update = update;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbPassengerData other = (MmbPassengerData) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbPassengerData [id=" + id + ", apisDataToUpdate="
				+ apisDataToUpdate + ", apisData=" + apisData
				+ ", authorityPermission=" + authorityPermission
				+ ", baggageAllowanceQuantity=" + baggageAllowanceQuantity
				+ ", baggageAllowance=" + baggageAllowance
				+ ", boardingPassRetrieveEmail=" + boardingPassRetrieveEmail
				+ ", boardingPassRetrieveSms=" + boardingPassRetrieveSms
				+ ", boardingPassViaEmail=" + boardingPassViaEmail
				+ ", boardingPassViaSms=" + boardingPassViaSms
				+ ", comfortSeatFree=" + comfortSeatFree + ", corporate="
				+ corporate + ", coupons=" + coupons + ", email1=" + email1
				+ ", email2=" + email2 + ", eticketClass=" + eticketClass
				+ ", eticket=" + eticket + ", flights=" + flights
				+ ", frequentFlyerCarrier=" + frequentFlyerCarrier
				+ ", frequentFlyerCode=" + frequentFlyerCode
				+ ", frequentFlyerCustomerValue=" + frequentFlyerCustomerValue
				+ ", frequentFlyerType=" + frequentFlyerType + ", gender="
				+ gender + ", specialFare=" + specialFare + ", lastName="
				+ lastName + ", linkType=" + linkType + ", name=" + name
				+ ", note=" + note + ", baggageOrder=" + baggageOrder
				+ ", paymentRetrieveEmail=" + paymentRetrieveEmail
				+ ", paymentViaEmail=" + paymentViaEmail + ", pnr=" + pnr
				+ ", preferences=" + preferences + ", routeId=" + routeId
				+ ", seatClass=" + seatClass + ", seat=" + seat + ", selected="
				+ selected + ", statusNote=" + statusNote + ", status="
				+ status + ", telephone1=" + telephone1 + ", telephone2="
				+ telephone2 + ", tickets=" + tickets + ", travelerRefNumber="
				+ travelerRefNumber + ", type=" + type
				+ ", updateFlightRefNumberRPH=" + updateFlightRefNumberRPH
				+ ", contactType=" + contactType + ", update=" + update + "]";
	}

}
