package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.OfferData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetrieveGeoOffersResponse extends BaseResponse {

	private List<OfferData> offers;

	public List<OfferData> getOffers() {
		return offers;
	}

	public void setOffers(List<OfferData> offers) {
		this.offers = offers;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((offers == null) ? 0 : offers.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveGeoOffersResponse other = (RetrieveGeoOffersResponse) obj;
		if (offers == null) {
			if (other.offers != null)
				return false;
		} else if (!offers.equals(other.offers))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveGeoOffersResponse [offers=" + offers + "]";
	}
}