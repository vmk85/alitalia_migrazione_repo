package com.alitalia.aem.common.data.home;

public class ResultTicketingDetailSolutionPricingFareBookingInfoSegmentData {

    private String departureField;
    private String destinationField;
    private ResultTicketingDetailSolutionPricingFareBookingInfoSegmentFlightData flightField;
    private String hashField;
    private String originField;

    public String getDepartureField() {
        return departureField;
    }

    public void setDepartureField(String value) {
        this.departureField = value;
    }

    public String getDestinationField() {
        return destinationField;
    }

    public void setDestinationField(String value) {
        this.destinationField = value;
    }

    public ResultTicketingDetailSolutionPricingFareBookingInfoSegmentFlightData getFlightField() {
        return flightField;
    }

    public void setFlightField(ResultTicketingDetailSolutionPricingFareBookingInfoSegmentFlightData value) {
        this.flightField = value;
    }

    public String getHashField() {
        return hashField;
    }

    public void setHashField(String value) {
        this.hashField = value;
    }

    public String getOriginField() {
        return originField;
    }

    public void setOriginField(String value) {
        this.originField = value;
    }

}
