package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.enumerations.MmbUpdateSSREnum;
import com.alitalia.aem.common.data.home.mmb.MmbRouteData;
import com.alitalia.aem.common.messages.MmbBaseRequest;
import com.alitalia.aem.common.messages.home.enumeration.MmbUpdateReasonType;

public class UpdateMmbPnrContactsRequest extends MmbBaseRequest {

    private String pnr;
    private String lastName;
    private String name;
    private String mail;
    private String oldMail;
    private String oldTelephone;
    private String oldTelephone2;
    private MmbUpdateReasonType reason;
    private MmbRouteData selectedRoute;
    private String telephone;
    private String telephone2;
    private MmbUpdateSSREnum updateMailAction;
    private MmbUpdateSSREnum updateTelephoneAction;
    private MmbUpdateSSREnum updateTelephone2Action;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

    public String getLastName() {
		return lastName;
	}

    public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getOldMail() {
		return oldMail;
	}

	public void setOldMail(String oldMail) {
		this.oldMail = oldMail;
	}

	public String getOldTelephone() {
		return oldTelephone;
	}

	public void setOldTelephone(String oldTelephone) {
		this.oldTelephone = oldTelephone;
	}

	public String getOldTelephone2() {
		return oldTelephone2;
	}

	public void setOldTelephone2(String oldTelephone2) {
		this.oldTelephone2 = oldTelephone2;
	}

	public MmbUpdateReasonType getReason() {
		return reason;
	}

	public void setReason(MmbUpdateReasonType reason) {
		this.reason = reason;
	}

	public MmbRouteData getSelectedRoute() {
		return selectedRoute;
	}

	public void setSelectedRoute(MmbRouteData selectedRoute) {
		this.selectedRoute = selectedRoute;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getTelephone2() {
		return telephone2;
	}

	public void setTelephone2(String telephone2) {
		this.telephone2 = telephone2;
	}

	public MmbUpdateSSREnum getUpdateMailAction() {
		return updateMailAction;
	}

	public void setUpdateMailAction(MmbUpdateSSREnum updateMailAction) {
		this.updateMailAction = updateMailAction;
	}

	public MmbUpdateSSREnum getUpdateTelephoneAction() {
		return updateTelephoneAction;
	}

	public void setUpdateTelephoneAction(MmbUpdateSSREnum updateTelephoneAction) {
		this.updateTelephoneAction = updateTelephoneAction;
	}

	public MmbUpdateSSREnum getUpdateTelephone2Action() {
		return updateTelephone2Action;
	}

	public void setUpdateTelephone2Action(MmbUpdateSSREnum updateTelephone2Action) {
		this.updateTelephone2Action = updateTelephone2Action;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((pnr == null) ? 0 : pnr.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		UpdateMmbPnrContactsRequest other = (UpdateMmbPnrContactsRequest) obj;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (pnr == null) {
			if (other.pnr != null)
				return false;
		} else if (!pnr.equals(other.pnr))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UpdateMmbPnrContactsRequest [pnr=" + pnr + ", lastName="
				+ lastName + ", name=" + name + ", mail=" + mail + ", oldMail="
				+ oldMail + ", oldTelephone=" + oldTelephone
				+ ", oldTelephone2=" + oldTelephone2 + ", reason=" + reason
				+ ", selectedRoute=" + selectedRoute + ", telephone="
				+ telephone + ", telephone2=" + telephone2
				+ ", updateMailAction=" + updateMailAction
				+ ", updateTelephoneAction=" + updateTelephoneAction
				+ ", updateTelephone2Action=" + updateTelephone2Action
				+ ", getTid()=" + getTid() + ", getSid()=" + getSid() + "]";
	}

}
