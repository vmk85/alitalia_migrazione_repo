package com.alitalia.aem.common.data.home;

public class ResultTicketingDetailSolutionSliceSegmentLegExtWarningData {

    private Boolean overnightField;
    private Boolean overnightFieldSpecified;

    public Boolean isOvernightField() {
        return overnightField;
    }

    public void setOvernightField(Boolean value) {
        this.overnightField = value;
    }

    public Boolean isOvernightFieldSpecified() {
        return overnightFieldSpecified;
    }

    public void setOvernightFieldSpecified(Boolean value) {
        this.overnightFieldSpecified = value;
    }

}
