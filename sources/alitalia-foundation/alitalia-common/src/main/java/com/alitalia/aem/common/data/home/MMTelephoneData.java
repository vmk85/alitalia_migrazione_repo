package com.alitalia.aem.common.data.home;

import java.util.Calendar;

import com.alitalia.aem.common.data.home.enumerations.MMPhoneTypeEnum;

public class MMTelephoneData {

	private String countryNumber;
	private Calendar effectiveDate;
	private String email;
	private String invalidIndicator;
	private String istruction;
	private String number;
	private String origCustomerId;
	private String phoneZone;
	private String sequenceNumber;
	private String areaPrefix;
	private MMPhoneTypeEnum telephoneType;

	public String getCountryNumber() {
		return countryNumber;
	}

	public void setCountryNumber(String countryNumber) {
		this.countryNumber = countryNumber;
	}

	public Calendar getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Calendar effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getInvalidIndicator() {
		return invalidIndicator;
	}

	public void setInvalidIndicator(String invalidIndicator) {
		this.invalidIndicator = invalidIndicator;
	}

	public String getIstruction() {
		return istruction;
	}

	public void setIstruction(String istruction) {
		this.istruction = istruction;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getOrigCustomerId() {
		return origCustomerId;
	}

	public void setOrigCustomerId(String origCustomerId) {
		this.origCustomerId = origCustomerId;
	}

	public String getPhoneZone() {
		return phoneZone;
	}

	public void setPhoneZone(String phoneZone) {
		this.phoneZone = phoneZone;
	}
	
	public String getAreaPrefix() {
		return areaPrefix;
	}

	public void setAreaPrefix(String areaPrefix) {
		this.areaPrefix = areaPrefix;
	}

	public String getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public MMPhoneTypeEnum getTelephoneType() {
		return telephoneType;
	}

	public void setTelephoneType(MMPhoneTypeEnum telephoneType) {
		this.telephoneType = telephoneType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((countryNumber == null) ? 0 : countryNumber.hashCode());
		result = prime * result
				+ ((effectiveDate == null) ? 0 : effectiveDate.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime
				* result
				+ ((invalidIndicator == null) ? 0 : invalidIndicator.hashCode());
		result = prime * result
				+ ((istruction == null) ? 0 : istruction.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result
				+ ((origCustomerId == null) ? 0 : origCustomerId.hashCode());
		result = prime * result
				+ ((phoneZone == null) ? 0 : phoneZone.hashCode());
		result = prime * result
				+ ((sequenceNumber == null) ? 0 : sequenceNumber.hashCode());
		result = prime * result
				+ ((telephoneType == null) ? 0 : telephoneType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MMTelephoneData other = (MMTelephoneData) obj;
		if (countryNumber == null) {
			if (other.countryNumber != null)
				return false;
		} else if (!countryNumber.equals(other.countryNumber))
			return false;
		if (effectiveDate == null) {
			if (other.effectiveDate != null)
				return false;
		} else if (!effectiveDate.equals(other.effectiveDate))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (invalidIndicator == null) {
			if (other.invalidIndicator != null)
				return false;
		} else if (!invalidIndicator.equals(other.invalidIndicator))
			return false;
		if (istruction == null) {
			if (other.istruction != null)
				return false;
		} else if (!istruction.equals(other.istruction))
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (origCustomerId == null) {
			if (other.origCustomerId != null)
				return false;
		} else if (!origCustomerId.equals(other.origCustomerId))
			return false;
		if (phoneZone == null) {
			if (other.phoneZone != null)
				return false;
		} else if (!phoneZone.equals(other.phoneZone))
			return false;
		if (sequenceNumber == null) {
			if (other.sequenceNumber != null)
				return false;
		} else if (!sequenceNumber.equals(other.sequenceNumber))
			return false;
		if (telephoneType != other.telephoneType)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TelephoneData [countryNumber=" + countryNumber
				+ ", effectiveDate=" + effectiveDate + ", email=" + email
				+ ", invalidIndicator=" + invalidIndicator + ", istruction="
				+ istruction + ", number=" + number + ", origCustomerId="
				+ origCustomerId + ", phoneZone=" + phoneZone
				+ ", sequenceNumber=" + sequenceNumber + ", telephoneType="
				+ telephoneType + "areaPrefix=" + areaPrefix + "]";
	}

}
