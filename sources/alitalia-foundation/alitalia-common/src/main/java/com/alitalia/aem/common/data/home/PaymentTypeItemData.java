package com.alitalia.aem.common.data.home;

import java.util.List;

public class PaymentTypeItemData extends CodeDescriptionData {
	
	protected List<String> group;
	protected List<DictionaryItemData> otherInfo;

	public List<String> getGroup() {
		return group;
	}

	public void setGroup(List<String> group) {
		this.group = group;
	}

	public List<DictionaryItemData> getOtherInfo() {
		return otherInfo;
	}

	public void setOtherInfo(List<DictionaryItemData> otherInfo) {
		this.otherInfo = otherInfo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((group == null) ? 0 : group.hashCode());
		result = prime * result
				+ ((otherInfo == null) ? 0 : otherInfo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentTypeItemData other = (PaymentTypeItemData) obj;
		if (group == null) {
			if (other.group != null)
				return false;
		} else if (!group.equals(other.group))
			return false;
		if (otherInfo == null) {
			if (other.otherInfo != null)
				return false;
		} else if (!otherInfo.equals(other.otherInfo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PaymentTypeItemData [group=" + group + ", otherInfo="
				+ otherInfo + "]";
	}
	
}
