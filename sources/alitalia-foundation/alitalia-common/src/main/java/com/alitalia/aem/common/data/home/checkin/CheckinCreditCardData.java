package com.alitalia.aem.common.data.home.checkin;

public class CheckinCreditCardData {
	
	private Integer id;
	private String cvv;
	private CheckinCreditCardHolderData creditCardHolderData;
	private Integer expireMonth;
	private Integer expireYear;
	private Boolean is3DSecure;
	private String number;
	private String threeDSecureOpaqueParams64Encoded;
	private String type;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCvv() {
		return cvv;
	}
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	public CheckinCreditCardHolderData getCreditCardHolderData() {
		return creditCardHolderData;
	}
	public void setCreditCardHolderData(
			CheckinCreditCardHolderData creditCardHolderData) {
		this.creditCardHolderData = creditCardHolderData;
	}
	public Integer getExpireMonth() {
		return expireMonth;
	}
	public void setExpireMonth(Integer expireMonth) {
		this.expireMonth = expireMonth;
	}
	public Integer getExpireYear() {
		return expireYear;
	}
	public void setExpireYear(Integer expireYear) {
		this.expireYear = expireYear;
	}
	public Boolean getIs3DSecure() {
		return is3DSecure;
	}
	public void setIs3DSecure(Boolean is3dSecure) {
		is3DSecure = is3dSecure;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getThreeDSecureOpaqueParams64Encoded() {
		return threeDSecureOpaqueParams64Encoded;
	}
	public void setThreeDSecureOpaqueParams64Encoded(
			String threeDSecureOpaqueParams64Encoded) {
		this.threeDSecureOpaqueParams64Encoded = threeDSecureOpaqueParams64Encoded;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((creditCardHolderData == null) ? 0 : creditCardHolderData
						.hashCode());
		result = prime * result + ((cvv == null) ? 0 : cvv.hashCode());
		result = prime * result
				+ ((expireMonth == null) ? 0 : expireMonth.hashCode());
		result = prime * result
				+ ((expireYear == null) ? 0 : expireYear.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((is3DSecure == null) ? 0 : is3DSecure.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime
				* result
				+ ((threeDSecureOpaqueParams64Encoded == null) ? 0
						: threeDSecureOpaqueParams64Encoded.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinCreditCardData other = (CheckinCreditCardData) obj;
		if (creditCardHolderData == null) {
			if (other.creditCardHolderData != null)
				return false;
		} else if (!creditCardHolderData.equals(other.creditCardHolderData))
			return false;
		if (cvv == null) {
			if (other.cvv != null)
				return false;
		} else if (!cvv.equals(other.cvv))
			return false;
		if (expireMonth == null) {
			if (other.expireMonth != null)
				return false;
		} else if (!expireMonth.equals(other.expireMonth))
			return false;
		if (expireYear == null) {
			if (other.expireYear != null)
				return false;
		} else if (!expireYear.equals(other.expireYear))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (is3DSecure == null) {
			if (other.is3DSecure != null)
				return false;
		} else if (!is3DSecure.equals(other.is3DSecure))
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (threeDSecureOpaqueParams64Encoded == null) {
			if (other.threeDSecureOpaqueParams64Encoded != null)
				return false;
		} else if (!threeDSecureOpaqueParams64Encoded
				.equals(other.threeDSecureOpaqueParams64Encoded))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CheckinCreditCardData [id=" + id + ", cvv=" + cvv
				+ ", creditCardHolderData=" + creditCardHolderData
				+ ", expireMonth=" + expireMonth + ", expireYear=" + expireYear
				+ ", is3DSecure=" + is3DSecure + ", number=" + number
				+ ", threeDSecureOpaqueParams64Encoded="
				+ threeDSecureOpaqueParams64Encoded + ", type=" + type + "]";
	}

}
