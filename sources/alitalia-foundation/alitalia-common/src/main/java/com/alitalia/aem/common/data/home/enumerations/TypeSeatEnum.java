package com.alitalia.aem.common.data.home.enumerations;

public enum TypeSeatEnum {

	NOT_EXIST("NotExist"),
    AISLE("Aisle"),
    AISLE_SEAT("AisleSeat"),
    WINDOW_SEAT("WindowSeat"),
    WINDOW_EXIT_SEAT("WindowExitSeat"),
    CENTER_SEAT("CenterSeat"),
    COMFORT_SEAT("ComfortSeat"),
    EXIT_COMFORT_SEAT("ExitComfortSeat");
    
	private final String value;

	TypeSeatEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TypeSeatEnum fromValue(String v) {
        for (TypeSeatEnum c: TypeSeatEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}