package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class SapInvoiceResponse extends BaseResponse {

	private String result;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "SapInvoiceResponse [result=" + result + ", getTid()="
				+ getTid() + ", getSid()=" + getSid() + "]";
	}

}
