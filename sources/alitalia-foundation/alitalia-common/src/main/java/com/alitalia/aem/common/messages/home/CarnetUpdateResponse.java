package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.carnet.CarnetInfoCarnet;
import com.alitalia.aem.common.messages.BaseResponse;

public class CarnetUpdateResponse extends BaseResponse {

	private CarnetInfoCarnet infoCarnet;

	public CarnetInfoCarnet getInfoCarnet() {
		return infoCarnet;
	}

	public void setInfoCarnet(CarnetInfoCarnet infoCarnet) {
		this.infoCarnet = infoCarnet;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((infoCarnet == null) ? 0 : infoCarnet.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarnetUpdateResponse other = (CarnetUpdateResponse) obj;
		if (infoCarnet == null) {
			if (other.infoCarnet != null)
				return false;
		} else if (!infoCarnet.equals(other.infoCarnet))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CarnetUpdateResponse [infoCarnet=" + infoCarnet + "]";
	}
}