package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.MmbAncillaryErrorData;
import com.alitalia.aem.common.messages.BaseResponse;

public class MmbAncillaryCheckOutEndResponse extends BaseResponse {

	private String machine;
	private List<MmbAncillaryErrorData> errors;

	public String getMachine() {
		return machine;
	}

	public void setMachine(String machine) {
		this.machine = machine;
	}

	public List<MmbAncillaryErrorData> getErrors() {
		return errors;
	}

	public void setErrors(List<MmbAncillaryErrorData> errors) {
		this.errors = errors;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((errors == null) ? 0 : errors.hashCode());
		result = prime * result + ((machine == null) ? 0 : machine.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbAncillaryCheckOutEndResponse other = (MmbAncillaryCheckOutEndResponse) obj;
		if (errors == null) {
			if (other.errors != null)
				return false;
		} else if (!errors.equals(other.errors))
			return false;
		if (machine == null) {
			if (other.machine != null)
				return false;
		} else if (!machine.equals(other.machine))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbAncillaryCheckOutEndResponse [machine=" + machine
				+ ", errors=" + errors + ", getTid()=" + getTid()
				+ ", getSid()=" + getSid() + "]";
	}

}
