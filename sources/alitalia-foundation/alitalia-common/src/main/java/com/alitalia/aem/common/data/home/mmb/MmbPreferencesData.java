package com.alitalia.aem.common.data.home.mmb;

import java.util.ArrayList;
import java.util.List;

import com.alitalia.aem.common.data.home.MealData;
import com.alitalia.aem.common.data.home.SeatTypeData;

public class MmbPreferencesData {

	private MealData mealType;
	private List<MmbSeatPreferenceData> seatPreferences;
	private SeatTypeData seatType;

	public MmbPreferencesData() {
		super();
	}

	public MmbPreferencesData(MmbPreferencesData clone) {
		super();
		this.mealType = clone.getMealType();
		this.seatType = clone.getSeatType();
		
		ArrayList<MmbSeatPreferenceData> seatPrefs = new ArrayList<MmbSeatPreferenceData>();
		for (MmbSeatPreferenceData cloneSeatPreferencesData : clone.getSeatPreferences()) {
			MmbSeatPreferenceData seatPreferencesData = new MmbSeatPreferenceData(cloneSeatPreferencesData);
			seatPrefs.add(seatPreferencesData);
		}
		this.seatPreferences = seatPrefs;
	}

	public MealData getMealType() {
		return mealType;
	}
	
	public void setMealType(MealData mealType) {
		this.mealType = mealType;
	}
	
	public List<MmbSeatPreferenceData> getSeatPreferences() {
		return seatPreferences;
	}
	
	public void setSeatPreferences(List<MmbSeatPreferenceData> seatPreferences) {
		this.seatPreferences = seatPreferences;
	}
	
	public SeatTypeData getSeatType() {
		return seatType;
	}
	
	public void setSeatType(SeatTypeData seatType) {
		this.seatType = seatType;
	}

	@Override
	public String toString() {
		return "PreferencesData [mealType=" + mealType + ", seatPreferences="
				+ seatPreferences + ", seatType=" + seatType + "]";
	}
}
