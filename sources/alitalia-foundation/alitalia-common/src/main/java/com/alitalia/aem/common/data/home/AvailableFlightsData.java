package com.alitalia.aem.common.data.home;

import java.util.ArrayList;
import java.util.Collections;

public class AvailableFlightsData extends BoomBoxData {

	private ArrayList<String> fullTextRules;
	private ArrayList<RouteData> routes;
	private ArrayList<RoutesData> routesList;
	private ArrayList<TabData> tabs;
	private ArrayList<TaxData> taxes;

	public ArrayList<String> getFullTextRules() {
		return fullTextRules;
	}

	public void setFullTextRules(ArrayList<String> fullTextRules) {
		this.fullTextRules = fullTextRules;
	}

	public ArrayList<RouteData> getRoutes() {
		return routes;
	}

	public void setRoutes(ArrayList<RouteData> routes) {
		/*
		 * Prima di effettuare l'assegnazione si effettua il sort dei voli
		 */
		for (RouteData route : routes) {
			Collections.sort(route.getFlights(), FlightData.COMPARE_BY_DURATION_AND_DATE);
		}
		this.routes = routes;
	}

	public ArrayList<RoutesData> getRoutesList() {
		return routesList;
	}

	public void setRoutesList(ArrayList<RoutesData> routesList) {
		this.routesList = routesList;
	}

	public ArrayList<TabData> getTabs() {
		return tabs;
	}

	public void setTabs(ArrayList<TabData> tabs) {
		this.tabs = tabs;
	}

	public ArrayList<TaxData> getTaxes() {
		return taxes;
	}

	public void setTaxes(ArrayList<TaxData> taxes) {
		this.taxes = taxes;
	}

	@Override
	public String toString() {
		return "AvailableFlightsData [fullTextRules=" + fullTextRules
				+ ", routes=" + routes + ", routesList=" + routesList
				+ ", tabs=" + tabs + ", taxes=" + taxes + "]";
	}
}
