package com.alitalia.aem.common.data.home.carnet;


public class CarnetBuyerData {

    private String address;
    private String businessName;
    private String city;
    private String countryCode;
    private String fax;
    private String nationInsuranceNumber;
    private Boolean privacyFlag;
    private String sellerCode;
    private String stateCode;
    private String telephoneNumber;
    private String vatNumber;
    private String zipCode;
    private String email;
    private String lastName;
    private String mobileNumber;
    private String name;
    
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getNationInsuranceNumber() {
		return nationInsuranceNumber;
	}
	public void setNationInsuranceNumber(String nationInsuranceNumber) {
		this.nationInsuranceNumber = nationInsuranceNumber;
	}
	public Boolean getPrivacyFlag() {
		return privacyFlag;
	}
	public void setPrivacyFlag(Boolean privacyFlag) {
		this.privacyFlag = privacyFlag;
	}
	public String getSellerCode() {
		return sellerCode;
	}
	public void setSellerCode(String sellerCode) {
		this.sellerCode = sellerCode;
	}
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public String getTelephoneNumber() {
		return telephoneNumber;
	}
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}
	public String getVatNumber() {
		return vatNumber;
	}
	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result
				+ ((businessName == null) ? 0 : businessName.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result
				+ ((countryCode == null) ? 0 : countryCode.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((fax == null) ? 0 : fax.hashCode());
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result
				+ ((mobileNumber == null) ? 0 : mobileNumber.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime
				* result
				+ ((nationInsuranceNumber == null) ? 0 : nationInsuranceNumber
						.hashCode());
		result = prime * result
				+ ((privacyFlag == null) ? 0 : privacyFlag.hashCode());
		result = prime * result
				+ ((sellerCode == null) ? 0 : sellerCode.hashCode());
		result = prime * result
				+ ((stateCode == null) ? 0 : stateCode.hashCode());
		result = prime * result
				+ ((telephoneNumber == null) ? 0 : telephoneNumber.hashCode());
		result = prime * result
				+ ((vatNumber == null) ? 0 : vatNumber.hashCode());
		result = prime * result + ((zipCode == null) ? 0 : zipCode.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarnetBuyerData other = (CarnetBuyerData) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (businessName == null) {
			if (other.businessName != null)
				return false;
		} else if (!businessName.equals(other.businessName))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (countryCode == null) {
			if (other.countryCode != null)
				return false;
		} else if (!countryCode.equals(other.countryCode))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (fax == null) {
			if (other.fax != null)
				return false;
		} else if (!fax.equals(other.fax))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (mobileNumber == null) {
			if (other.mobileNumber != null)
				return false;
		} else if (!mobileNumber.equals(other.mobileNumber))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (nationInsuranceNumber == null) {
			if (other.nationInsuranceNumber != null)
				return false;
		} else if (!nationInsuranceNumber.equals(other.nationInsuranceNumber))
			return false;
		if (privacyFlag == null) {
			if (other.privacyFlag != null)
				return false;
		} else if (!privacyFlag.equals(other.privacyFlag))
			return false;
		if (sellerCode == null) {
			if (other.sellerCode != null)
				return false;
		} else if (!sellerCode.equals(other.sellerCode))
			return false;
		if (stateCode == null) {
			if (other.stateCode != null)
				return false;
		} else if (!stateCode.equals(other.stateCode))
			return false;
		if (telephoneNumber == null) {
			if (other.telephoneNumber != null)
				return false;
		} else if (!telephoneNumber.equals(other.telephoneNumber))
			return false;
		if (vatNumber == null) {
			if (other.vatNumber != null)
				return false;
		} else if (!vatNumber.equals(other.vatNumber))
			return false;
		if (zipCode == null) {
			if (other.zipCode != null)
				return false;
		} else if (!zipCode.equals(other.zipCode))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "CarnetBuyerData [address=" + address + ", businessName="
				+ businessName + ", city=" + city + ", countryCode="
				+ countryCode + ", fax=" + fax + ", nationInsuranceNumber="
				+ nationInsuranceNumber + ", privacyFlag=" + privacyFlag
				+ ", sellerCode=" + sellerCode + ", stateCode=" + stateCode
				+ ", telephoneNumber=" + telephoneNumber + ", vatNumber="
				+ vatNumber + ", zipCode=" + zipCode + ", email=" + email
				+ ", lastName=" + lastName + ", mobileNumber=" + mobileNumber
				+ ", name=" + name + "]";
	}
}