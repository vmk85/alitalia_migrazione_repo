package com.alitalia.aem.common.data.home.checkin;

import java.math.BigDecimal;

import com.alitalia.aem.common.data.home.enumerations.CheckinEmdTicketType;

public class EmdItemData {
	
	private BigDecimal commissionAmount;
    private String invoiceNumber;
    private String itemNumber;
    private BigDecimal netAmount;
    private CheckinEmdPassengerNameType passengerName;
    private String paymentType;
    private String ticketNumber;
    private String ticketingStatus;
    private BigDecimal totalAmount;
    private Boolean typeSpecified;
    private CheckinEmdTicketType type;
    
	public BigDecimal getCommissionAmount() {
		return commissionAmount;
	}
	public void setCommissionAmount(BigDecimal commissionAmount) {
		this.commissionAmount = commissionAmount;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getItemNumber() {
		return itemNumber;
	}
	public void setItemNumber(String itemNumber) {
		this.itemNumber = itemNumber;
	}
	public BigDecimal getNetAmount() {
		return netAmount;
	}
	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}
	public CheckinEmdPassengerNameType getPassengerName() {
		return passengerName;
	}
	public void setPassengerName(CheckinEmdPassengerNameType passengerName) {
		this.passengerName = passengerName;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getTicketNumber() {
		return ticketNumber;
	}
	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}
	public String getTicketingStatus() {
		return ticketingStatus;
	}
	public void setTicketingStatus(String ticketingStatus) {
		this.ticketingStatus = ticketingStatus;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	public Boolean getTypeSpecified() {
		return typeSpecified;
	}
	public void setTypeSpecified(Boolean typeSpecified) {
		this.typeSpecified = typeSpecified;
	}
	public CheckinEmdTicketType getType() {
		return type;
	}
	public void setType(CheckinEmdTicketType type) {
		this.type = type;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((commissionAmount == null) ? 0 : commissionAmount.hashCode());
		result = prime * result
				+ ((invoiceNumber == null) ? 0 : invoiceNumber.hashCode());
		result = prime * result
				+ ((itemNumber == null) ? 0 : itemNumber.hashCode());
		result = prime * result
				+ ((netAmount == null) ? 0 : netAmount.hashCode());
		result = prime * result
				+ ((passengerName == null) ? 0 : passengerName.hashCode());
		result = prime * result
				+ ((paymentType == null) ? 0 : paymentType.hashCode());
		result = prime * result
				+ ((ticketNumber == null) ? 0 : ticketNumber.hashCode());
		result = prime * result
				+ ((ticketingStatus == null) ? 0 : ticketingStatus.hashCode());
		result = prime * result
				+ ((totalAmount == null) ? 0 : totalAmount.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result
				+ ((typeSpecified == null) ? 0 : typeSpecified.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmdItemData other = (EmdItemData) obj;
		if (commissionAmount == null) {
			if (other.commissionAmount != null)
				return false;
		} else if (!commissionAmount.equals(other.commissionAmount))
			return false;
		if (invoiceNumber == null) {
			if (other.invoiceNumber != null)
				return false;
		} else if (!invoiceNumber.equals(other.invoiceNumber))
			return false;
		if (itemNumber == null) {
			if (other.itemNumber != null)
				return false;
		} else if (!itemNumber.equals(other.itemNumber))
			return false;
		if (netAmount == null) {
			if (other.netAmount != null)
				return false;
		} else if (!netAmount.equals(other.netAmount))
			return false;
		if (passengerName == null) {
			if (other.passengerName != null)
				return false;
		} else if (!passengerName.equals(other.passengerName))
			return false;
		if (paymentType == null) {
			if (other.paymentType != null)
				return false;
		} else if (!paymentType.equals(other.paymentType))
			return false;
		if (ticketNumber == null) {
			if (other.ticketNumber != null)
				return false;
		} else if (!ticketNumber.equals(other.ticketNumber))
			return false;
		if (ticketingStatus == null) {
			if (other.ticketingStatus != null)
				return false;
		} else if (!ticketingStatus.equals(other.ticketingStatus))
			return false;
		if (totalAmount == null) {
			if (other.totalAmount != null)
				return false;
		} else if (!totalAmount.equals(other.totalAmount))
			return false;
		if (type != other.type)
			return false;
		if (typeSpecified == null) {
			if (other.typeSpecified != null)
				return false;
		} else if (!typeSpecified.equals(other.typeSpecified))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "EmdItemData [commissionAmount=" + commissionAmount
				+ ", invoiceNumber=" + invoiceNumber + ", itemNumber="
				+ itemNumber + ", netAmount=" + netAmount + ", passengerName="
				+ passengerName + ", paymentType=" + paymentType
				+ ", ticketNumber=" + ticketNumber + ", ticketingStatus="
				+ ticketingStatus + ", totalAmount=" + totalAmount
				+ ", typeSpecified=" + typeSpecified + ", type=" + type + "]";
	}
}