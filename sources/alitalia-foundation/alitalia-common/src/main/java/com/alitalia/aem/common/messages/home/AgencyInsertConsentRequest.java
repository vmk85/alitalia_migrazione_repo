package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class AgencyInsertConsentRequest extends BaseRequest {

	private String idAgenzia;
	private boolean flagCondizioni;
	private boolean flagDatiPersonali;
	
	public String getIdAgenzia() {
		return idAgenzia;
	}
	
	public void setIdAgenzia(String idAgenzia) {
		this.idAgenzia = idAgenzia;
	}
	
	public boolean isFlagCondizioni() {
		return flagCondizioni;
	}
	
	public void setFlagCondizioni(boolean flagCondizioni) {
		this.flagCondizioni = flagCondizioni;
	}
	
	public boolean isFlagDatiPersonali() {
		return flagDatiPersonali;
	}
	
	public void setFlagDatiPersonali(boolean flagDatiPersonali) {
		this.flagDatiPersonali = flagDatiPersonali;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (flagCondizioni ? 1231 : 1237);
		result = prime * result + (flagDatiPersonali ? 1231 : 1237);
		result = prime * result
				+ ((idAgenzia == null) ? 0 : idAgenzia.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgencyInsertConsentRequest other = (AgencyInsertConsentRequest) obj;
		if (flagCondizioni != other.flagCondizioni)
			return false;
		if (flagDatiPersonali != other.flagDatiPersonali)
			return false;
		if (idAgenzia == null) {
			if (other.idAgenzia != null)
				return false;
		} else if (!idAgenzia.equals(other.idAgenzia))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AgencyInsertConsentRequest [idAgenzia=" + idAgenzia
				+ ", flagCondizioni=" + flagCondizioni + ", flagDatiPersonali="
				+ flagDatiPersonali + "]";
	}
}