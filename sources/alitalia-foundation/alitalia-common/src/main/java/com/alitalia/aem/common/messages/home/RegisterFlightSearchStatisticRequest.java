package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.enumerations.SearchTypeEnum;
import com.alitalia.aem.common.messages.SearchExecuteRequestFilter;

public class RegisterFlightSearchStatisticRequest extends RegisterStatisticsRequest {

	private SearchExecuteRequestFilter search;
	private SearchTypeEnum searchType;
	
	public RegisterFlightSearchStatisticRequest(String tid, String sid) {
		super(tid, sid);
	}
	public SearchExecuteRequestFilter getSearch() {
		return search;
	}
	public void setSearch(SearchExecuteRequestFilter search) {
		this.search = search;
	}
	public SearchTypeEnum getSearchType() {
		return searchType;
	}
	public void setSearchType(SearchTypeEnum searchType) {
		this.searchType = searchType;
	}
	
	@Override
	public String toString() {
		return "RegisterFlightSearchStatisticRequest [search=" + search
				+ ", searchType=" + searchType + ", getSearch()=" + getSearch()
				+ ", getSearchType()=" + getSearchType() + ", getClientIP()="
				+ getClientIP() + ", getErrorDescr()=" + getErrorDescr()
				+ ", getSessionId()=" + getSessionId() + ", getSiteCode()="
				+ getSiteCode() + ", isSuccess()=" + isSuccess()
				+ ", getType()=" + getType() + ", getUserId()=" + getUserId()
				+ ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + ", getTid()=" + getTid() + ", getSid()="
				+ getSid() + ", getClass()=" + getClass() + "]";
	}	
}