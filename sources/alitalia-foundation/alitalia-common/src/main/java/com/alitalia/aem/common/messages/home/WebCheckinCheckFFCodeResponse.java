package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.messages.BaseResponse;

public class WebCheckinCheckFFCodeResponse extends BaseResponse {

	private List<Integer> wrongFfPassengerIds;

	public List<Integer> getWrongFfPassengerIds() {
		return wrongFfPassengerIds;
	}

	public void setWrongFfPassengerIds(List<Integer> wrongFfPassengerIds) {
		this.wrongFfPassengerIds = wrongFfPassengerIds;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((wrongFfPassengerIds == null) ? 0 : wrongFfPassengerIds
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebCheckinCheckFFCodeResponse other = (WebCheckinCheckFFCodeResponse) obj;
		if (wrongFfPassengerIds == null) {
			if (other.wrongFfPassengerIds != null)
				return false;
		} else if (!wrongFfPassengerIds.equals(other.wrongFfPassengerIds))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WebCheckinCheckFFCodeResponse [wrongFfPassengerIds="
				+ wrongFfPassengerIds + "]";
	}
}