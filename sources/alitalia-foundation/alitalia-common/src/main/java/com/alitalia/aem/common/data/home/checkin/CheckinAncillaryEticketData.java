package com.alitalia.aem.common.data.home.checkin;

import java.util.List;

public class CheckinAncillaryEticketData {

	private String number;
	private String pnr;
	private List<CheckinAncillaryCouponData> coupons;

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public List<CheckinAncillaryCouponData> getCoupons() {
		return coupons;
	}

	public void setCoupons(List<CheckinAncillaryCouponData> coupons) {
		this.coupons = coupons;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((pnr == null) ? 0 : pnr.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinAncillaryEticketData other = (CheckinAncillaryEticketData) obj;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (pnr == null) {
			if (other.pnr != null)
				return false;
		} else if (!pnr.equals(other.pnr))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbAncillaryETicketData [number=" + number + ", pnr=" + pnr
				+ ", coupons=" + coupons + "]";
	}

}
