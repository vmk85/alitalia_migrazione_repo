package com.alitalia.aem.common.data.home;

import java.util.List;

public class FlightSeatMapData {

	private DirectFlightData directFlight;
    private List<SeatMapData> seatMaps;
	
    public DirectFlightData getDirectFlight() {
		return directFlight;
	}
	
    public void setDirectFlight(DirectFlightData directFlight) {
		this.directFlight = directFlight;
	}
	
    public List<SeatMapData> getSeatMaps() {
		return seatMaps;
	}
	
    public void setSeatMaps(List<SeatMapData> seatMaps) {
		this.seatMaps = seatMaps;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((directFlight == null) ? 0 : directFlight.hashCode());
		result = prime * result
				+ ((seatMaps == null) ? 0 : seatMaps.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FlightSeatMapData other = (FlightSeatMapData) obj;
		if (directFlight == null) {
			if (other.directFlight != null)
				return false;
		} else if (!directFlight.equals(other.directFlight))
			return false;
		if (seatMaps == null) {
			if (other.seatMaps != null)
				return false;
		} else if (!seatMaps.equals(other.seatMaps))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FlightSeatMapData [directFlight=" + directFlight
				+ ", seatMaps=" + seatMaps + "]";
	}
}