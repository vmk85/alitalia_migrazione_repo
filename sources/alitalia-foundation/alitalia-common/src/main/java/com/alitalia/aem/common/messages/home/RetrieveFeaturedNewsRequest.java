package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class RetrieveFeaturedNewsRequest extends BaseRequest{

	private boolean pubTest;

	public boolean isPubTest() {
		return pubTest;
	}

	public void setPubTest(boolean pubTest) {
		this.pubTest = pubTest;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (pubTest ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveFeaturedNewsRequest other = (RetrieveFeaturedNewsRequest) obj;
		if (pubTest != other.pubTest)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveFeaturedNewsRequest [pubTest=" + pubTest + "]";
	}
}