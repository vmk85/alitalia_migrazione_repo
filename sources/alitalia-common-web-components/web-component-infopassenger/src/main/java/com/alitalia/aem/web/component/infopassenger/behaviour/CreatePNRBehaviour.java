package com.alitalia.aem.web.component.infopassenger.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.InfoPassengerCreatePNRRequest;
import com.alitalia.aem.common.messages.home.InfoPassengerCreatePNRResponse;
import com.alitalia.aem.service.api.home.BookingInfoPassengerService;

@Component(immediate = true, metatype = false)
@Service(value = CreatePNRBehaviour.class)
public class CreatePNRBehaviour extends Behaviour<InfoPassengerCreatePNRRequest, InfoPassengerCreatePNRResponse> {

	@Reference
	BookingInfoPassengerService bookingInfoPassengerService;
	
	@Override
	public InfoPassengerCreatePNRResponse executeOrchestration(InfoPassengerCreatePNRRequest request) {
		if (request == null)
			throw new IllegalArgumentException("InfoPassengerCreatePNRRequest is null.");
		return bookingInfoPassengerService.executeCreatePNR(request);
	}

}
