package com.alitalia.aem.web.component.payment.behaviour;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.data.home.BillingData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentData;
import com.alitalia.aem.common.data.home.TicketData;
import com.alitalia.aem.common.data.home.TicketInfoData;
import com.alitalia.aem.common.messages.home.RetrieveTicketRequest;
import com.alitalia.aem.common.messages.home.RetrieveTicketResponse;
import com.alitalia.aem.common.messages.home.SapInvoiceRequest;
import com.alitalia.aem.common.messages.home.SapInvoiceResponse;
import com.alitalia.aem.service.api.home.BookingTicketService;
import com.alitalia.aem.service.api.home.PaymentService;

@Component(metatype=false, immediate=true)
@Service(value=RetrieveTicketBehaviour.class)
public class RetrieveTicketBehaviour extends Behaviour<RetrieveTicketRequest, RetrieveTicketResponse> {

	@Reference
	private PaymentService paymentService;
	
	@Reference
	private BookingTicketService ticketService;
	
	private static Logger logger = LoggerFactory.getLogger(RetrieveTicketBehaviour.class);
	
	@Override
	protected RetrieveTicketResponse executeOrchestration(RetrieveTicketRequest request) {
		if(request == null) throw new IllegalArgumentException("RetrieveTicket request is null.");
		RetrieveTicketResponse retrieveTicketResponse = paymentService.retrieveTicket(request);
		retrieveTicketResponse.setInvoiceRequestSuccessful(true);

		if (request.isInvoiceRequired()) {
			BillingData billingInformation = request.getRoutes().getBilling();
			/*
			 * Recupero in maniera univoca i numeri di biglietto dalla RetrieveTicketResponse
			 */
			HashSet<String> ticketNumbers = new HashSet<String>();
			for (PassengerBaseData passengerData : retrieveTicketResponse.getPassenger()) {
				for (TicketInfoData ticketInfoData : passengerData.getTickets()) {
					if (!ticketNumbers.contains(ticketInfoData.getTicketNumber()))
							ticketNumbers.add(ticketInfoData.getTicketNumber());
				}
			}

			/*
			 * Recupero da ResultBookingDetails della request la tratta
			 */
			ResultBookingDetailsData bookingDetails = (ResultBookingDetailsData) request.getRoutes().getProperties().get("BookingDetails");
			Iterator<ResultBookingDetailsSolutionPricingBookingInfoData> it = 
					bookingDetails.getSolutionField().get(0).getPricingField().get(0).getBookingInfoField().iterator();
			
			ResultBookingDetailsSolutionPricingBookingInfoData infoData = it.next();
			ResultBookingDetailsSolutionPricingBookingInfoSegmentData segmentField = infoData.getSegmentField();
			String tratta = segmentField.getOriginField() + "\\" + segmentField.getDestinationField();
			while (it.hasNext()) {
				segmentField = it.next().getSegmentField();
				tratta += "-" + segmentField.getOriginField() + "\\" + segmentField.getDestinationField();
			}
			

			/*
			 * Recupero PNR dalla RetrieveTicketResponse 
			 */
			String pnr = request.getRoutes().getPnr();
			//FIXME verificare se deve essere la data volo o la data di acquisto biglietto ovvero la data corrente
			String dataVolo = (new SimpleDateFormat("yyyy-MM-dd")).format(new Date());

			List<TicketData> biglietti = new ArrayList<TicketData>();
			for (String numeroBiglietto : ticketNumbers) {
				TicketData ticketData = new TicketData(dataVolo, numeroBiglietto, pnr, tratta);
				biglietti.add(ticketData);
			}
	
			try {
				SapInvoiceRequest invoiceRequest = new 
						SapInvoiceRequest(billingInformation.getCap(), billingInformation.getCfInt(), billingInformation.getCfPax(),
								billingInformation.getEmailInt(), billingInformation.getEnteEmit(), billingInformation.getEnteRich(),
								billingInformation.getIndSped(), billingInformation.getIntFattura(), billingInformation.getLocSped(), 
								billingInformation.getNome(), billingInformation.getPaese(), billingInformation.getProv(), 
								billingInformation.getSocieta(), biglietti, billingInformation.getTipoRichiesta());
				
				SapInvoiceResponse invoiceResponse = ticketService.callSAPService(invoiceRequest);
				retrieveTicketResponse.setInvoiceRequestSuccessful("OK".equals(invoiceResponse.getResult()));
			} catch (Exception e) {
				logger.debug("An exception occurred invoking callSAPService", e);
			}
		}

		return retrieveTicketResponse;
	}
	

}
