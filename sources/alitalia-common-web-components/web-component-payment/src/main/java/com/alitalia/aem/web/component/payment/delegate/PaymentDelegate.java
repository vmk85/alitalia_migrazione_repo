package com.alitalia.aem.web.component.payment.delegate;

import com.alitalia.aem.common.messages.home.AuthorizePaymentRequest;
import com.alitalia.aem.common.messages.home.AuthorizePaymentResponse;
import com.alitalia.aem.common.messages.home.CheckPaymentRequest;
import com.alitalia.aem.common.messages.home.CheckPaymentResponse;
import com.alitalia.aem.common.messages.home.InitializePaymentRequest;
import com.alitalia.aem.common.messages.home.InitializePaymentResponse;
import com.alitalia.aem.common.messages.home.RetrieveTicketRequest;
import com.alitalia.aem.common.messages.home.RetrieveTicketResponse;


public interface PaymentDelegate {

	InitializePaymentResponse initializePayment(InitializePaymentRequest request);
	AuthorizePaymentResponse authorizePayment(AuthorizePaymentRequest request);
	CheckPaymentResponse checkPayment(CheckPaymentRequest request);
	RetrieveTicketResponse retrieveTicket(RetrieveTicketRequest request);

}
