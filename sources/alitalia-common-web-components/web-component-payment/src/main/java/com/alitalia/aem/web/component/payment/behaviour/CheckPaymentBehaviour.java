package com.alitalia.aem.web.component.payment.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.CheckPaymentRequest;
import com.alitalia.aem.common.messages.home.CheckPaymentResponse;
import com.alitalia.aem.service.api.home.PaymentService;

@Component(immediate = true, metatype = false)
@Service(value = CheckPaymentBehaviour.class)
public class CheckPaymentBehaviour extends Behaviour<CheckPaymentRequest, CheckPaymentResponse> {

	@Reference
	private PaymentService paymentService;
	
	@Override
	public CheckPaymentResponse executeOrchestration(CheckPaymentRequest request) {
		if(request == null) throw new IllegalArgumentException("CheckPayment request is null.");
		return paymentService.checkPayment(request);
	}
	
	
}