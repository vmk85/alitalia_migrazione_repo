package com.alitalia.aem.web.component.geospatial.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveGeoNearestAirportByCoordsRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoNearestAirportByCoordsResponse;
import com.alitalia.aem.service.api.home.GeoSpatialService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveGeoNearestAirportByCoordsBehaviour.class)
public class RetrieveGeoNearestAirportByCoordsBehaviour 
		extends Behaviour<RetrieveGeoNearestAirportByCoordsRequest, RetrieveGeoNearestAirportByCoordsResponse> {

	@Reference
	private GeoSpatialService geoSpatialService;
	
	@Override
	public RetrieveGeoNearestAirportByCoordsResponse executeOrchestration(RetrieveGeoNearestAirportByCoordsRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Geo Nearest Airport By Coords request is null.");
		return geoSpatialService.retrieveGeoNearestAirportByCoords(request);
	}	
}