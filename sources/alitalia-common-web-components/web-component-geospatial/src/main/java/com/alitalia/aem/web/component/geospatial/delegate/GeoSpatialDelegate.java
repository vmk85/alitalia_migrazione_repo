package com.alitalia.aem.web.component.geospatial.delegate;

import com.alitalia.aem.common.messages.home.RetrieveGeoAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoAirportsResponse;
import com.alitalia.aem.common.messages.home.RetrieveGeoCitiesRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoCitiesResponse;
import com.alitalia.aem.common.messages.home.RetrieveGeoCountryRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoCountryResponse;
import com.alitalia.aem.common.messages.home.RetrieveGeoNearestAirportByCoordsRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoNearestAirportByCoordsResponse;
import com.alitalia.aem.common.messages.home.RetrieveGeoOffersRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoOffersResponse;

public interface GeoSpatialDelegate {

	RetrieveGeoOffersResponse retrieveGeoOffers(RetrieveGeoOffersRequest request);
	RetrieveGeoCitiesResponse retrieveGeoCities(RetrieveGeoCitiesRequest request);
	RetrieveGeoOffersResponse retrieveGeoOffersByTarget(RetrieveGeoOffersRequest request);
	RetrieveGeoCitiesResponse retrieveGeoCitiesByTarget(RetrieveGeoCitiesRequest request);
	RetrieveGeoAirportsResponse retrieveGeoAirports(RetrieveGeoAirportsRequest request);
	RetrieveGeoCountryResponse retrieveGeoCountry(RetrieveGeoCountryRequest request);
	RetrieveGeoNearestAirportByCoordsResponse retrieveGeoNearestAirportByCoords(RetrieveGeoNearestAirportByCoordsRequest request);

}