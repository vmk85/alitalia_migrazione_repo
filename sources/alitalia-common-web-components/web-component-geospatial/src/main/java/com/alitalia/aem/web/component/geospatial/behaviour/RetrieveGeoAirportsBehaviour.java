package com.alitalia.aem.web.component.geospatial.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveGeoAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoAirportsResponse;
import com.alitalia.aem.service.api.home.GeoSpatialService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveGeoAirportsBehaviour.class)
public class RetrieveGeoAirportsBehaviour extends Behaviour<RetrieveGeoAirportsRequest, RetrieveGeoAirportsResponse> {

	@Reference
	private GeoSpatialService geoSpatialService;
	
	@Override
	public RetrieveGeoAirportsResponse executeOrchestration(RetrieveGeoAirportsRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Geo Airports request is null.");
		return geoSpatialService.retrieveGeoAirports(request);
	}	
}