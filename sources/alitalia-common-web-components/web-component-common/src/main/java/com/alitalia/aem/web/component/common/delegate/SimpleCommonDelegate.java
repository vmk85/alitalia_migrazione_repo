package com.alitalia.aem.web.component.common.delegate;

import java.util.Hashtable;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.messages.home.AuthenticateMilleMigliaUserRequest;
import com.alitalia.aem.common.messages.home.AuthenticateMilleMigliaUserResponse;
import com.alitalia.aem.common.messages.home.RetrieveAirportDetailsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportDetailsResponse;
import com.alitalia.aem.common.messages.home.RetrieveFlightMileageRequest;
import com.alitalia.aem.common.messages.home.RetrieveFlightMileageResponse;
import com.alitalia.aem.common.messages.home.RetrieveFlightSeatMapRequest;
import com.alitalia.aem.common.messages.home.RetrieveFlightSeatMapResponse;
import com.alitalia.aem.web.component.common.behaviour.AuthenticateMilleMigliaUserBehaviour;
import com.alitalia.aem.web.component.common.behaviour.RetrieveAirportDetailsBehaviour;
import com.alitalia.aem.web.component.common.behaviour.RetrieveFlightMileageBehaviour;
import com.alitalia.aem.web.component.common.behaviour.RetrieveFlightSeatMapBehaviour;

@Component(immediate = true, metatype = false)
@Properties({
	@Property( name = SimpleCommonDelegate.CACHE_REFRESH_INTERVAL, label = "Refresh interval of the cache." ),
	@Property( name = SimpleCommonDelegate.IS_CACHE_ENABLED, label = "Flag to enable cache." )
})
@Service(value = CommonDelegate.class)
public class SimpleCommonDelegate implements CommonDelegate {

	private static final Logger logger = LoggerFactory.getLogger(SimpleCommonDelegate.class);

	@Reference
	private RetrieveAirportDetailsBehaviour retrieveAirportDetailsBehaviour;

	@Reference
	private RetrieveFlightMileageBehaviour retrieveFlightMileageBehaviour;

	@Reference
	private RetrieveFlightSeatMapBehaviour retrieveFlightSeatMapBehaviour;

	@Reference
	private AuthenticateMilleMigliaUserBehaviour authenticateMilleMigliaUserBehaviour;

	private Hashtable<FlightMileageKey, Object> cacheMap = new Hashtable<FlightMileageKey, Object>();
	private Object cacheMapLock = new Object();

	private long lastClean = System.currentTimeMillis();
	private long refreshInterval = 10000L;
	private boolean isCacheEnabled = false;

	public static final String CACHE_REFRESH_INTERVAL = "cache.refresh.interval";
	public static final String IS_CACHE_ENABLED = "cache.is_enabled";

	@Override
	public RetrieveAirportDetailsResponse retrieveAirportDetail(RetrieveAirportDetailsRequest request) {
		BehaviourExecutor<RetrieveAirportDetailsBehaviour, RetrieveAirportDetailsRequest, RetrieveAirportDetailsResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(retrieveAirportDetailsBehaviour, request);
	}

	@Override
	public RetrieveFlightMileageResponse retrieveFlightMileage(RetrieveFlightMileageRequest request) {
		BehaviourExecutor<RetrieveFlightMileageBehaviour, RetrieveFlightMileageRequest, RetrieveFlightMileageResponse> executor = new BehaviourExecutor<>();

		logger.debug("Executing delegate retrieveFlightMileage ...");

		if (isCacheEnabled) {
			synchronized(cacheMapLock){
				if(request.getDirectFlight() != null &&
						request.getDirectFlight().getCarrier() != null &&
						request.getDirectFlight().getFrom() != null &&
						request.getDirectFlight().getTo() != null &&
						request.getDirectFlight().getCompartimentalClass() != null &&
						request.getDirectFlight().getDepartureDate() != null) {	

					FlightMileageKey key = new FlightMileageKey(request.getDirectFlight().getCarrier(),
							request.getDirectFlight().getFrom(), request.getDirectFlight().getTo(),
							request.getDirectFlight().getCompartimentalClass(), request.getDirectFlight().getDepartureDate());

					checkIsCacheToBeRefreshed();

					RetrieveFlightMileageResponse retrieveFlightMileageResponse = (RetrieveFlightMileageResponse)cacheMap.get(key);

					if(retrieveFlightMileageResponse == null) {
						logger.debug("Called cache manager to retrieve flight mileage - cache miss");
						retrieveFlightMileageResponse = (RetrieveFlightMileageResponse) cacheMap.get(key);
						if(retrieveFlightMileageResponse == null || retrieveFlightMileageResponse.getMileage() <= 0) {
							retrieveFlightMileageResponse =  executor.executeBehaviour(retrieveFlightMileageBehaviour, request);
							cacheMap.put(key, retrieveFlightMileageResponse);

							logger.debug("Called cache manager to retrieve flight mileage - put flight mileage in cache");
						} else {
							logger.debug("Called cache manager to retrieve flight mileage - put flight mileage in cache from an other thread");
						}
						logger.debug("Executing delegate retrieveFlightMileage DONE");
						return retrieveFlightMileageResponse;
					} 

					logger.info("Called cache manager to retrieve flight mileage - returned flight mileage from cache");
					logger.debug("Executing delegate retrieveFlightMileage DONE");
					return retrieveFlightMileageResponse;
				} else {
					logger.debug("Bad key generation - starting service call to get flight mileage");
					logger.debug("Executing delegate retrieveFlightMileage DONE");
					return executor.executeBehaviour(retrieveFlightMileageBehaviour, request);
				}
			}
		} else {
			logger.debug("Cache manager disabled - starting service call to get flight mileage");
			logger.debug("Executing delegate retrieveFlightMileage DONE");
			return executor.executeBehaviour(retrieveFlightMileageBehaviour, request);
		}

	}	

	@Override
	public RetrieveFlightSeatMapResponse retrieveFlightSeatMap(RetrieveFlightSeatMapRequest request){
		BehaviourExecutor<RetrieveFlightSeatMapBehaviour, RetrieveFlightSeatMapRequest, RetrieveFlightSeatMapResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(retrieveFlightSeatMapBehaviour, request);
	}

	@Override
	public AuthenticateMilleMigliaUserResponse authenticateMilleMigliaUser(AuthenticateMilleMigliaUserRequest request) {
		BehaviourExecutor<AuthenticateMilleMigliaUserBehaviour, AuthenticateMilleMigliaUserRequest, AuthenticateMilleMigliaUserResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(authenticateMilleMigliaUserBehaviour, request);
	}

	private void checkIsCacheToBeRefreshed() {
		long currentTime = System.currentTimeMillis();
		logger.debug("SimpleCommonDelegate.checkIsCacheToBeRefreshed - current time is [" + currentTime + "], last clean is [" + lastClean + "] and "
				+ "refresh interval is [" + refreshInterval + "]");
		if (currentTime - lastClean > refreshInterval) {
			logger.debug("SimpleCommonDelegate.checkIsCacheToBeRefreshed - clear cache");
			cacheMap.clear();
			lastClean = currentTime;
		}
	}

	@Activate
	private void activate(ComponentContext componentContext) {
		try {		
			isCacheEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_CACHE_ENABLED), false);
			refreshInterval = PropertiesUtil.toLong(
					componentContext.getProperties().get(CACHE_REFRESH_INTERVAL), 3600000);
		} catch (Exception e) {
			logger.error("Error to modify SimpleCommonDelegate - Load default value", e);
			isCacheEnabled = false;			
		}

		logger.info("Activated SimpleCommonDelegate: "
				+ "IS_CACHE_ENABLED=[" + isCacheEnabled + "], "
				+ "CACHE_REFRESH_INTERVAL=[" + refreshInterval + "]");
	}

	@Modified
	private void modified(ComponentContext componentContext) {
		try {
			isCacheEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_CACHE_ENABLED), false);
			refreshInterval = PropertiesUtil.toLong(
					componentContext.getProperties().get(CACHE_REFRESH_INTERVAL), 3600000);
		} catch (Exception e) {
			logger.error("Error to modify SimpleCommonDelegate - Load default value", e);
			isCacheEnabled = false;			
		}

		logger.info("Modified SimpleCommonDelegate: "
				+ "IS_CACHE_ENABLED=[" + isCacheEnabled + "], "
				+ "CACHE_REFRESH_INTERVAL=[" + refreshInterval + "]");
	}

}