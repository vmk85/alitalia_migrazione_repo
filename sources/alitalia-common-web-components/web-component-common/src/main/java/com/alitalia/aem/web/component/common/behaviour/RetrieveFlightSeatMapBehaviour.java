package com.alitalia.aem.web.component.common.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveFlightSeatMapRequest;
import com.alitalia.aem.common.messages.home.RetrieveFlightSeatMapResponse;
import com.alitalia.aem.service.api.home.CommonService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveFlightSeatMapBehaviour.class)
public class RetrieveFlightSeatMapBehaviour extends Behaviour<RetrieveFlightSeatMapRequest, RetrieveFlightSeatMapResponse> {

	@Reference
	private CommonService commonService;
	
	@Override
	public RetrieveFlightSeatMapResponse executeOrchestration(RetrieveFlightSeatMapRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Flight Seat Map request is null.");
		return commonService.retrieveFlightSeatMap(request);
	}
}