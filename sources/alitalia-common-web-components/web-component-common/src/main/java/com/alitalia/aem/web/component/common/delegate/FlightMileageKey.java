package com.alitalia.aem.web.component.common.delegate;

import java.util.ArrayList;
import java.util.Calendar;

import com.alitalia.aem.common.data.home.AirportData;

public class FlightMileageKey {

	private String carrier;
	private AirportData from;
	private AirportData to;
	private ArrayList<String> compartimentalClass;
	private Calendar departureDate;

	public FlightMileageKey(String carrier, AirportData from, AirportData to, ArrayList<String> compartimentalClass,
			Calendar departureDate) {
		super();
		this.carrier = carrier;
		this.from = from;
		this.to = to;
		this.compartimentalClass = compartimentalClass;
		this.departureDate = departureDate;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public AirportData getFrom() {
		return from;
	}

	public void setFrom(AirportData from) {
		this.from = from;
	}

	public AirportData getTo() {
		return to;
	}

	public void setTo(AirportData to) {
		this.to = to;
	}

	public ArrayList<String> getCompartimentalClass() {
		return compartimentalClass;
	}

	public void setCompartimentalClass(ArrayList<String> compartimentalClass) {
		this.compartimentalClass = compartimentalClass;
	}

	public Calendar getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Calendar departureDate) {
		this.departureDate = departureDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((carrier == null) ? 0 : carrier.hashCode());
		result = prime * result + ((compartimentalClass == null) ? 0 : compartimentalClass.hashCode());
		result = prime * result + ((departureDate == null) ? 0 : departureDate.hashCode());
		result = prime * result + ((from == null) ? 0 : from.hashCode());
		result = prime * result + ((to == null) ? 0 : to.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FlightMileageKey other = (FlightMileageKey) obj;
		if (carrier == null) {
			if (other.carrier != null)
				return false;
		} else if (!carrier.equals(other.carrier))
			return false;
		if (compartimentalClass == null) {
			if (other.compartimentalClass != null)
				return false;
		} else if (!compartimentalClass.equals(other.compartimentalClass))
			return false;
		if (departureDate == null) {
			if (other.departureDate != null)
				return false;
		} else if (!departureDate.equals(other.departureDate))
			return false;
		if (from == null) {
			if (other.from != null)
				return false;
		} else if (!from.equals(other.from))
			return false;
		if (to == null) {
			if (other.to != null)
				return false;
		} else if (!to.equals(other.to))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FlightMileageKey [carrier=" + carrier + ", from=" + from + ", to=" + to + ", compartimentalClass="
				+ compartimentalClass + ", departureDate=" + departureDate + "]";
	}

}
