package com.alitalia.aem.web.component.common.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveFlightMileageRequest;
import com.alitalia.aem.common.messages.home.RetrieveFlightMileageResponse;
import com.alitalia.aem.service.api.home.CommonService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveFlightMileageBehaviour.class)
public class RetrieveFlightMileageBehaviour extends Behaviour<RetrieveFlightMileageRequest, RetrieveFlightMileageResponse> {

	@Reference
	private CommonService commonService;
	
	@Override
	public RetrieveFlightMileageResponse executeOrchestration(RetrieveFlightMileageRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Flight Mileage request is null.");
		return commonService.retrieveFlightMileage(request);
	}
}