package com.alitalia.aem.web.component.crmdata.delegaterest;

import com.alitalia.aem.common.messages.crmdatarest.SetCRMDataNewsLetterRequest;
import com.alitalia.aem.common.messages.crmdatarest.SetCRMDataNewsLetterResponse;

public interface ICRMDataDelegate {
    
    SetCRMDataNewsLetterResponse setCRMDataNewsLetter(SetCRMDataNewsLetterRequest request);
 
}