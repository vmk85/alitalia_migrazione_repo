package com.alitalia.aem.web.component.crmdata.delegaterest;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.messages.crmdatarest.SetCRMDataNewsLetterRequest;
import com.alitalia.aem.common.messages.crmdatarest.SetCRMDataNewsLetterResponse;
import com.alitalia.aem.web.component.crmdata.behaviourrest.SetCRMDataBehaviour;

@Component(immediate=true, metatype=false)
@Properties({
	@Property( name = CRMDataDelegateImpl.CACHE_REFRESH_INTERVAL, label = "Refresh interval of the cache." ),
	@Property( name = CRMDataDelegateImpl.IS_CACHE_ENABLED, label = "Flag to enable cache." )
})
@Service
public class CRMDataDelegateImpl implements ICRMDataDelegate {
	
	@Reference
	private SetCRMDataBehaviour setCRMDataBehaviour;
	

	private static final Logger logger = LoggerFactory.getLogger(CRMDataDelegateImpl.class);
	
	public static final String CACHE_REFRESH_INTERVAL = "cache.refresh.interval";
	public static final String IS_CACHE_ENABLED = "cache.is_enabled";
	
	private long refreshInterval = 3600000L;
	private boolean isCacheEnabled = false;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		try {		
			isCacheEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_CACHE_ENABLED), false);
			refreshInterval = PropertiesUtil.toLong(
					componentContext.getProperties().get(CACHE_REFRESH_INTERVAL), 3600000);
		} catch (Exception e) {
			logger.error("Error to modify CRMDataDelegateImpl - Load default value", e);
			isCacheEnabled = false;			
		}

		logger.info("Activated CRMDataDelegateImpl: "
				+ "IS_CACHE_ENABLED=[" + isCacheEnabled + "], "
				+ "CACHE_REFRESH_INTERVAL=[" + refreshInterval + "]");
	}

	@Modified
	private void modified(ComponentContext componentContext) {
		try {
			isCacheEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_CACHE_ENABLED), false);
			refreshInterval = PropertiesUtil.toLong(
					componentContext.getProperties().get(CACHE_REFRESH_INTERVAL), 3600000);
		} catch (Exception e) {
			logger.error("Error to modify CRMDataDelegateImpl - Load default value", e);
			isCacheEnabled = false;			
		}

		logger.info("Modified CRMDataDelegateImpl: "
				+ "IS_CACHE_ENABLED=[" + isCacheEnabled + "], "
				+ "CACHE_REFRESH_INTERVAL=[" + refreshInterval + "]");
	}
	


	@Override
	public SetCRMDataNewsLetterResponse setCRMDataNewsLetter(SetCRMDataNewsLetterRequest request) {
		SetCRMDataNewsLetterResponse response = null;
		BehaviourExecutor<SetCRMDataBehaviour, SetCRMDataNewsLetterRequest, SetCRMDataNewsLetterResponse> executor = new BehaviourExecutor<SetCRMDataBehaviour, SetCRMDataNewsLetterRequest, SetCRMDataNewsLetterResponse>();
		try {
			logger.debug("Executing delegate SetCRMDataNewsLetterResponse");
			response = executor.executeBehaviour(setCRMDataBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[CRMDataDelegateImpl - SetCRMDataNewsLetterResponse] - Errore durante il set delle informazioni CRM.", e);
		}
		return response;
	}


}
