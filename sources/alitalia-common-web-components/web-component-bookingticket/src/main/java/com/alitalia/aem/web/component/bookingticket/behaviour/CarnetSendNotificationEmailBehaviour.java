package com.alitalia.aem.web.component.bookingticket.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.CarnetSendNotificationEmailRequest;
import com.alitalia.aem.common.messages.home.CarnetSendNotificationEmailResponse;
import com.alitalia.aem.service.api.home.BookingTicketService;

@Component(immediate = true, metatype = false)
@Service(value = CarnetSendNotificationEmailBehaviour.class)
public class CarnetSendNotificationEmailBehaviour extends Behaviour<CarnetSendNotificationEmailRequest, CarnetSendNotificationEmailResponse> {

	@Reference
	private BookingTicketService bookingTicketService;

	@Override
	public CarnetSendNotificationEmailResponse executeOrchestration(CarnetSendNotificationEmailRequest request) {
		if(request == null) throw new IllegalArgumentException("CarnetSendNotificationEmailRequest is null.");
		return bookingTicketService.sendCarnetNotificationEmail(request);
	}
}
