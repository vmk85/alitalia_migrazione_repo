package com.alitalia.aem.web.component.bookingticket.delegate;

import com.alitalia.aem.common.messages.home.CarnetSendNotificationEmailRequest;
import com.alitalia.aem.common.messages.home.CarnetSendNotificationEmailResponse;
import com.alitalia.aem.common.messages.home.CrossSellingsRequest;
import com.alitalia.aem.common.messages.home.CrossSellingsResponse;

public interface BookingTicketDelegate {

	CrossSellingsResponse getCrossSellings(CrossSellingsRequest crossSellingsRequest);
	CarnetSendNotificationEmailResponse sendCarnetNotificationEmail(CarnetSendNotificationEmailRequest request);
	
}
