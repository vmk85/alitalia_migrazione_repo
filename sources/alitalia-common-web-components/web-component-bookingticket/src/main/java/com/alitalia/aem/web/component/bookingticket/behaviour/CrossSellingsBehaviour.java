package com.alitalia.aem.web.component.bookingticket.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.CrossSellingsRequest;
import com.alitalia.aem.common.messages.home.CrossSellingsResponse;
import com.alitalia.aem.service.api.home.BookingTicketService;

@Component(immediate = true, metatype = false)
@Service(value = CrossSellingsBehaviour.class)
public class CrossSellingsBehaviour extends Behaviour<CrossSellingsRequest, CrossSellingsResponse> {

	@Reference
	BookingTicketService bookingTicketService;

	@Override
	public CrossSellingsResponse executeOrchestration(CrossSellingsRequest request) {
		if(request == null) throw new IllegalArgumentException("Cross selling request is null.");
		return bookingTicketService.getCrossSellings(request);
	}

}
