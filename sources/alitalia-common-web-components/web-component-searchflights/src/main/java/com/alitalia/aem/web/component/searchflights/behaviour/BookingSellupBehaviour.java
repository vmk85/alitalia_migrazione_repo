package com.alitalia.aem.web.component.searchflights.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.SearchBookingSolutionResponse;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.service.api.home.BookingSearchService;

@Component(immediate = true, metatype = false)
@Service(value = BookingSellupBehaviour.class)
public class BookingSellupBehaviour extends Behaviour<SearchFlightSolutionRequest, SearchBookingSolutionResponse> {

	@Reference
	private BookingSearchService bookingSearchService;

	@Override
	public SearchBookingSolutionResponse executeOrchestration(SearchFlightSolutionRequest request) {
		if(request == null)
			throw new IllegalArgumentException("Search Flight Solution request is null.");

		if(request.getFilter() == null)
			throw new IllegalArgumentException("Search Flight Solution request filter is null.");

		SearchBookingSolutionResponse response = bookingSearchService.executeBookingSellupSearch(request);

		return response;
	}
}
