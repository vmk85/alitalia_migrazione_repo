package com.alitalia.aem.web.component.searchflights.behaviour;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.data.home.AvailableFlightsData;
import com.alitalia.aem.common.data.home.BrandSearchData;
import com.alitalia.aem.common.data.home.FlightTabData;
import com.alitalia.aem.common.data.home.RibbonSearchData;
import com.alitalia.aem.common.data.home.TabData;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.messages.SearchExecuteRequestFilter;
import com.alitalia.aem.common.messages.home.SearchExtraChargeRequest;
import com.alitalia.aem.common.messages.home.SearchExtraChargeResponse;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionResponse;
import com.alitalia.aem.service.api.home.BookingSearchService;

@Component(immediate = true, metatype = false)
@Service(value = SearchFlightsSolutionCustomRibbonBehaviour.class)
public class SearchFlightsSolutionCustomRibbonBehaviour extends Behaviour<SearchFlightSolutionRequest, SearchFlightSolutionResponse>{

	@Reference
	private BookingSearchService bookingSearchService;
	
	@Override
	protected SearchFlightSolutionResponse executeOrchestration(SearchFlightSolutionRequest request) {
		if(request == null)
			throw new IllegalArgumentException("Search Flight Solution request is null.");

		if(request.getFilter() == null)
			throw new IllegalArgumentException("Search Flight Solution request filter is null.");

		SearchFlightSolutionResponse response = bookingSearchService.executeBrandSearch(request);

		Calendar outboundRibbonStartDateMinusSix = (Calendar)request.getRibbonOutboundStartDate().clone();
		outboundRibbonStartDateMinusSix.add(Calendar.DATE, -6);
		
		Calendar returnRibbonStartDateMinusSix = null;
		if (request.getRibbonReturnStartDate() != null) {
			returnRibbonStartDateMinusSix = (Calendar)request.getRibbonReturnStartDate().clone();
			returnRibbonStartDateMinusSix.add(Calendar.DATE, -6);
		}
		
		RibbonSearchData ribbonSearchStartDateMinusSix = 
				new RibbonSearchData((BrandSearchData) request.getFilter(), outboundRibbonStartDateMinusSix, returnRibbonStartDateMinusSix);
		
		RibbonSearchData ribbonSearchStartDate = 
				new RibbonSearchData((BrandSearchData) request.getFilter(), request.getRibbonOutboundStartDate(), request.getRibbonReturnStartDate());

		AvailableFlightsData availableFlights = response.getAvailableFlights();
		if (availableFlights != null) {
			ribbonSearchStartDateMinusSix.setSolutionSet(availableFlights.getSolutionSet());
			ribbonSearchStartDate.setSolutionSet(availableFlights.getSolutionSet());
		}

		SearchFlightSolutionResponse ribbonResponseStartDateMinusSix = executeRibbonSearch(ribbonSearchStartDateMinusSix, request);
		SearchFlightSolutionResponse ribbonResponseStartDate = executeRibbonSearch(ribbonSearchStartDate, request);
		if (ribbonResponseStartDateMinusSix != null && ribbonResponseStartDate != null) {
			ArrayList<TabData> tabs = obtainTabs(ribbonResponseStartDateMinusSix,ribbonResponseStartDate);
			response.getAvailableFlights().setTabs(tabs);
		}
		
		//It is necessary to share the extracharge for business and consumer
		BrandSearchData extraChargeBrandSearch = new BrandSearchData((BrandSearchData) request.getFilter());
		extraChargeBrandSearch.setMarket(request.getMarketExtraCharge());
		
		SearchExtraChargeRequest extraChargeRequest = new SearchExtraChargeRequest(
				request.getTid(),
				request.getSid());
		
		extraChargeRequest.setFilter(extraChargeBrandSearch);
		SearchExtraChargeResponse extraChargeResponse = bookingSearchService.executeExtraChargeSearch(extraChargeRequest);
		if ((extraChargeResponse != null) && (extraChargeResponse.getPassengerList() != null)){
			response.setExtraChargePassengerList(extraChargeResponse.getPassengerList());
		}
		
		return response;
		
	}
	
	private ArrayList<TabData> obtainTabs(
			SearchFlightSolutionResponse ribbonResponseStartDateMinusSix,
			SearchFlightSolutionResponse ribbonResponseStartDate) {
		
		ArrayList<TabData> tabs = new ArrayList<TabData>();
		
		if (ribbonResponseStartDateMinusSix.getAvailableFlights() != null 
				&& ribbonResponseStartDate.getAvailableFlights() != null) {
			
			//recupera le tab delle due chiamate a servizio
			List<FlightTabData> firstSixTabsOutbound = null;
			List<FlightTabData> firstSixTabsReturn = null;
			for (TabData tabList : ribbonResponseStartDateMinusSix.getAvailableFlights().getTabs()) {
				if (tabList.getType() == RouteTypeEnum.OUTBOUND) {
					firstSixTabsOutbound = tabList.getFlightTabs();
				}
				if (tabList.getType() == RouteTypeEnum.RETURN) {
					firstSixTabsReturn = tabList.getFlightTabs();
				}
			}
			List<FlightTabData> otherSixTabsOutbound = null;
			List<FlightTabData> otherSixTabsReturn = null;
			for (TabData tabList : ribbonResponseStartDate.getAvailableFlights().getTabs()) {
				if (tabList.getType() == RouteTypeEnum.OUTBOUND) {
					otherSixTabsOutbound = tabList.getFlightTabs();
				}
				if (tabList.getType() == RouteTypeEnum.RETURN) {
					otherSixTabsReturn = tabList.getFlightTabs();
				}
			}
			
			//concatena le liste di tab
			List<FlightTabData> outboundTabs = null;
			if (firstSixTabsOutbound != null && otherSixTabsOutbound != null) {
				outboundTabs = new ArrayList<FlightTabData>(firstSixTabsOutbound);
				//rimuoviamo il primo elemento che è duplicato (è l'ultimo della prima lista)
				otherSixTabsOutbound.remove(0);
				outboundTabs.addAll(otherSixTabsOutbound);
			}
			List<FlightTabData> returnTabs = null;
			if (firstSixTabsReturn != null && otherSixTabsReturn != null) {
				returnTabs = new ArrayList<FlightTabData>(firstSixTabsReturn);
				//rimuoviamo il primo elemento che è duplicato (è l'ultimo della prima lista)
				otherSixTabsReturn.remove(0);
				returnTabs.addAll(otherSixTabsReturn);
			}
			
			//crea gli oggetti da settare nelle liste da ritornare
			TabData tabOutbound = null;
			TabData tabReturn = null;
			if (outboundTabs != null){
				tabOutbound = new TabData();
				tabOutbound.setType(RouteTypeEnum.OUTBOUND);
				tabOutbound.setFlightTabs(outboundTabs);
			}
			if (returnTabs != null){
				tabReturn = new TabData();
				tabReturn.setType(RouteTypeEnum.RETURN);
				tabReturn.setFlightTabs(returnTabs);
			}
			
			//setta le liste da ritornare
			if(tabOutbound != null) {
				tabs.add(tabOutbound);
			}
			if(tabReturn != null) {
				tabs.add(tabReturn);
			}
			
		}
		return tabs;
	}

	private SearchFlightSolutionResponse executeRibbonSearch(SearchExecuteRequestFilter ribbonSearch, SearchFlightSolutionRequest request){
		SearchFlightSolutionRequest ribbonRequest = new SearchFlightSolutionRequest(request.getTid(), request.getSid());
		ribbonRequest.setFilter(ribbonSearch);
		ribbonRequest.setLanguageCode(request.getLanguageCode());
		ribbonRequest.setMarket(request.getMarket());
		ribbonRequest.setResponseType(request.getResponseType());

		SearchFlightSolutionResponse ribbonResponse = bookingSearchService.executeRibbonSearch(ribbonRequest);
		return ribbonResponse;
	}

}
