package com.alitalia.aem.web.component.searchflights.behaviour;

import java.math.BigDecimal;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.data.home.BrandSearchData;
import com.alitalia.aem.common.data.home.CarnetSearchData;
import com.alitalia.aem.common.data.home.FlightTabData;
import com.alitalia.aem.common.data.home.TabData;
import com.alitalia.aem.common.data.home.enumerations.ResidencyTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.messages.home.SearchExtraChargeRequest;
import com.alitalia.aem.common.messages.home.SearchExtraChargeResponse;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionResponse;
import com.alitalia.aem.service.api.home.BookingSearchService;

@Component(immediate = true, metatype = false)
@Service(value = SearchFlightSolutionBehaviour.class)
public class SearchFlightSolutionBehaviour extends Behaviour<SearchFlightSolutionRequest, SearchFlightSolutionResponse> {

	@Reference
	private BookingSearchService bookingSearchService;
	
	@Override
	public SearchFlightSolutionResponse executeOrchestration(SearchFlightSolutionRequest request) {
		if(request == null)
			throw new IllegalArgumentException("Search Flight Solution request is null.");

		if(request.getFilter() == null)
			throw new IllegalArgumentException("Search Flight Solution request filter is null.");
		
		((BrandSearchData) request.getFilter()).setStartOutboundDate(request.getRibbonOutboundStartDate());
		((BrandSearchData) request.getFilter()).setStartReturnDate(request.getRibbonReturnStartDate());

		SearchFlightSolutionResponse response = bookingSearchService.executeBrandSearch(request);

		if ( ! (request.getFilter() instanceof CarnetSearchData)){
			BrandSearchData filter = (BrandSearchData) request.getFilter();
			if (filter.getDestinations() == null) {
				throw new IllegalArgumentException("Search Flight Solution request filter destinations is null.");
			}
		}
		
		boolean solutionFound = false;
		boolean solutionsFoundOtherDates = false;
		
		// Se si tratta di continutita' territoriale verifico se sono state trovate soluzioni e,
		// in caso contrario, effettuo una ricerca normale
		if (ResidencyTypeEnum.SARDINIA.equals(((BrandSearchData) request.getFilter()).getResidency())) {
			solutionFound = response.getAvailableFlights().getRoutes() != null && 
					!response.getAvailableFlights().getRoutes().isEmpty();
			boolean solutionsFoundOtherDatesOutbound = false;
			boolean solutionsFoundOtherDatesReturn = false;
			
			/**
			 * Verifico se ci sono valori maggiori di 0 nella RibbonSearch
			 */
			if ((response != null) && (response.getAvailableFlights() != null)) {
				for (TabData tabData: response.getAvailableFlights().getTabs()) {
					for (FlightTabData flightTabData : tabData.getFlightTabs()) {
						if (flightTabData.getPrice().compareTo(new BigDecimal(0)) > 0) {
							if (tabData.getType() == RouteTypeEnum.OUTBOUND) {
								solutionsFoundOtherDatesOutbound = true;
							} else {
								solutionsFoundOtherDatesReturn = true;
							}
							break;
						}
					}
				}
			}
			solutionsFoundOtherDates = response.getAvailableFlights().getTabs().size() == 1 ? 
					solutionsFoundOtherDatesOutbound : 
						(solutionsFoundOtherDatesOutbound && solutionsFoundOtherDatesReturn);
			
			/**
			 * Se non ci sono soluzioni, cambio il campo residency - e rieseguo la Search
			 */
			if (!solutionFound && !solutionsFoundOtherDates) {
				((BrandSearchData) request.getFilter()).setResidency(ResidencyTypeEnum.NONE);
				response = bookingSearchService.executeBrandSearch(request);
			}
		}
		
		response.setTerritorialContinuitySolutionsFound(solutionFound);
		response.setTerritorialContinuitySolutionsFoundOtherDates(solutionsFoundOtherDates);		
		
		//It is necessary to share the extracharge for business and consumer
		
		SearchExtraChargeRequest extraChargeRequest = new SearchExtraChargeRequest(
				request.getTid(),
				request.getSid());
		
		if (request.getFilter() instanceof CarnetSearchData){
			CarnetSearchData extraChargeCarnetSearch = new CarnetSearchData((CarnetSearchData) request.getFilter());
			extraChargeCarnetSearch.setMarket(request.getMarketExtraCharge());
			extraChargeRequest.setFilter(extraChargeCarnetSearch);
		} else {
			BrandSearchData extraChargeBrandSearch = new BrandSearchData((BrandSearchData) request.getFilter());
			extraChargeBrandSearch.setMarket(request.getMarketExtraCharge());
			extraChargeRequest.setFilter(extraChargeBrandSearch);
		}
		
		SearchExtraChargeResponse extraChargeResponse = bookingSearchService.executeExtraChargeSearch(extraChargeRequest);
		if ((extraChargeResponse != null) && (extraChargeResponse.getPassengerList() != null)){
			response.setExtraChargePassengerList(extraChargeResponse.getPassengerList());
		}
		
		return response;
	}

}
