package com.alitalia.aem.web.component.searchflights.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.BagsAndRuleRequest;
import com.alitalia.aem.common.messages.home.BagsAndRuleResponse;
import com.alitalia.aem.service.api.home.BookingBagsAndRulesService;

@Component(immediate = true, metatype = false)
@Service(value = ExecuteBagsAndRuleBehaviour.class)
public class ExecuteBagsAndRuleBehaviour extends Behaviour<BagsAndRuleRequest, BagsAndRuleResponse> {

	@Reference
	private BookingBagsAndRulesService bagsAndRulesService;

	@Override
	public BagsAndRuleResponse executeOrchestration(BagsAndRuleRequest request) {
		if(request == null) throw new IllegalArgumentException("BagsAndRuleRequest is null.");
		return bagsAndRulesService.execute(request);
	}

}
