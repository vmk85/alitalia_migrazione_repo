package com.alitalia.aem.web.component.searchflights.behaviour;

import java.math.BigDecimal;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.data.home.BrandSearchData;
import com.alitalia.aem.common.data.home.FlightTabData;
import com.alitalia.aem.common.data.home.PassengerNumbersData;
import com.alitalia.aem.common.data.home.TabData;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.messages.home.SearchExtraChargeRequest;
import com.alitalia.aem.common.messages.home.SearchExtraChargeResponse;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionResponse;
import com.alitalia.aem.service.api.home.BookingSearchService;

@Component(immediate = true, metatype = false)
@Service(value = SearchYouthSolutionBehaviour.class)
public class SearchYouthSolutionBehaviour extends Behaviour<SearchFlightSolutionRequest, SearchFlightSolutionResponse> {

	@Reference
	private BookingSearchService bookingSearchService;

	@Override
	public SearchFlightSolutionResponse executeOrchestration(SearchFlightSolutionRequest request) {
		if(request == null)
			throw new IllegalArgumentException("Search Flight Solution request is null.");
		Boolean youthSolutionsFound = false;
		Boolean youthSolutionsFoundOtherDates = false;
		Boolean youthSolutionsFoundOtherDatesOutbound = false;
		Boolean youthSolutionsFoundOtherDatesReturn = false;
		SearchFlightSolutionResponse response = null;
		if (request.getFilter() instanceof BrandSearchData) {
			((BrandSearchData) request.getFilter()).setStartOutboundDate(request.getRibbonOutboundStartDate());
			((BrandSearchData) request.getFilter()).setStartReturnDate(request.getRibbonReturnStartDate());
			response = bookingSearchService.executeYouthSearch(request);
		} else {
			//response = bookingSearchService.executeJumpUpSearch(request);
			throw new IllegalArgumentException("Filter data inside request is not BrandSearchData type");
		}

		/**
		 * Verifico se ci sono soluzioni nella YouthSearch
		 */
		youthSolutionsFound = !response.getAvailableFlights().getRoutes().isEmpty();

		BrandSearchData filter = (BrandSearchData) request.getFilter();
		if (filter.getDestinations() == null) {
			throw new IllegalArgumentException("Search Flight Solution request filter destinations is null.");
		}
		
		/**
		 * Verifico se ci sono valori maggiori di 0 nella RibbonSearch
		 */
		if ((response != null) && (response.getAvailableFlights() != null)) {
			for (TabData tabData: response.getAvailableFlights().getTabs()) {
				for (FlightTabData flightTabData : tabData.getFlightTabs()) {
					if (flightTabData.getPrice().compareTo(new BigDecimal(0)) > 0) {
						if (tabData.getType() == RouteTypeEnum.OUTBOUND) {
							youthSolutionsFoundOtherDatesOutbound = true;
						} else {
							youthSolutionsFoundOtherDatesReturn = true;
						}
						break;
					}
				}
			}
		}
		/**
		 * Controllo se non ci sono altre selezioni per le altre date, 
		 * in base a se ho fatto un andata ritorno o un solo andata
		 */
		youthSolutionsFoundOtherDates = response.getAvailableFlights().getTabs().size() == 1 ? 
				youthSolutionsFoundOtherDatesOutbound : 
					(youthSolutionsFoundOtherDatesOutbound && youthSolutionsFoundOtherDatesReturn);

		/**
		 * Se non ci sono soluzioni, cambio il tipo di passeggero - da Youth ad Adult - e rieseguo la Search
		 */
		if (!youthSolutionsFound && !youthSolutionsFoundOtherDates) {
			if (request.getFilter() instanceof BrandSearchData)
				filter = (BrandSearchData) request.getFilter();
			// Se e' di tipo diverso da BrandSearchData viene scartato all'inizio per
			// cui questo ramo non e' piu' gestito
			//else
			//	filter = (JumpUpSearchData) request.getFilter();
			for (PassengerNumbersData passengerNumbersData : filter.getPassengerNumbers()) {
				passengerNumbersData.setPassengerType(PassengerTypeEnum.ADULT);
			}
			if (request.getFilter() instanceof BrandSearchData) {
				((BrandSearchData) request.getFilter()).setStartOutboundDate(request.getRibbonOutboundStartDate());
				((BrandSearchData) request.getFilter()).setStartReturnDate(request.getRibbonReturnStartDate());
				response = bookingSearchService.executeYouthSearch(request);
			}
			//else
			//	response = bookingSearchService.executeJumpUpSearch(request);

			filter = (BrandSearchData) request.getFilter();
			if (filter.getDestinations() == null) {
				throw new IllegalArgumentException("Search Flight Solution request filter destinations is null.");
			}
		}
		response.setYouthSolutionsFound(youthSolutionsFound);
		response.setYouthSolutionsFoundOtherDates(youthSolutionsFoundOtherDates);
		
		BrandSearchData extraChargeBrandSearch = new BrandSearchData((BrandSearchData) request.getFilter());
		SearchExtraChargeRequest extraChargeRequest = new SearchExtraChargeRequest(request.getTid(), request.getSid());
		//change market code for manage extra charge
		extraChargeBrandSearch.setMarket(request.getMarketExtraCharge());
		
		extraChargeRequest.setFilter(extraChargeBrandSearch);
		SearchExtraChargeResponse extraChargeResponse = bookingSearchService.executeExtraChargeSearch(extraChargeRequest);
		if ((extraChargeResponse != null) && (extraChargeResponse.getPassengerList() != null)){
			response.setExtraChargePassengerList(extraChargeResponse.getPassengerList());
		}

		return response;
	}

}
