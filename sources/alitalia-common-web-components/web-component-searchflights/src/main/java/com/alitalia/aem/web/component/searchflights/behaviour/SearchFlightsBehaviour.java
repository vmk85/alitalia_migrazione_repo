package com.alitalia.aem.web.component.searchflights.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.SearchFlightsRequest;
import com.alitalia.aem.common.messages.home.SearchFlightsResponse;
import com.alitalia.aem.service.api.home.StaticDataService;
import com.alitalia.aem.service.impl.home.SimpleStaticDataService;

@Component(immediate = true, metatype = false)
@Service(value = SearchFlightsBehaviour.class)
public class SearchFlightsBehaviour extends Behaviour<SearchFlightsRequest, SearchFlightsResponse> {

	@Override
	public SearchFlightsResponse executeOrchestration(SearchFlightsRequest request) {
		if(request == null) throw new IllegalArgumentException("Search Flights request is null.");
		StaticDataService staticDataService = new SimpleStaticDataService();
		return staticDataService.searchFlights(request);
	}	
}