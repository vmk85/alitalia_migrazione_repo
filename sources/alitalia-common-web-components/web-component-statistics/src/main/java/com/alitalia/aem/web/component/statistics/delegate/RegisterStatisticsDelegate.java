package com.alitalia.aem.web.component.statistics.delegate;

import com.alitalia.aem.common.messages.home.RegisterStatisticsRequest;
import com.alitalia.aem.common.messages.home.RegisterStatisticsResponse;

public interface RegisterStatisticsDelegate {

	RegisterStatisticsResponse registerStatistics(RegisterStatisticsRequest request);
}