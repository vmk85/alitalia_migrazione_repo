package com.alitalia.aem.web.component.statistics.delegate;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.messages.home.RegisterStatisticsRequest;
import com.alitalia.aem.common.messages.home.RegisterStatisticsResponse;
import com.alitalia.aem.web.component.statistics.behaviour.RegisterStatisticsBehaviour;

@Component(immediate = true, metatype = false)
@Service(value = RegisterStatisticsDelegate.class)
public class SimpleRegisterStatisticsDelegate implements RegisterStatisticsDelegate {

	@Reference
	private RegisterStatisticsBehaviour registerStatisticsBehaviour;
	
	@Override
	public RegisterStatisticsResponse registerStatistics(RegisterStatisticsRequest request) {
		BehaviourExecutor<RegisterStatisticsBehaviour, RegisterStatisticsRequest, RegisterStatisticsResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(registerStatisticsBehaviour, request);
	}
}