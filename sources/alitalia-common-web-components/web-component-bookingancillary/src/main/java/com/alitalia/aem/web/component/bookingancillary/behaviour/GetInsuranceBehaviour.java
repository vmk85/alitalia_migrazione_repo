package com.alitalia.aem.web.component.bookingancillary.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.InsuranceRequest;
import com.alitalia.aem.common.messages.home.InsuranceResponse;
import com.alitalia.aem.service.api.home.AncillaryService;

@Component(immediate = true, metatype = false)
@Service(value = GetInsuranceBehaviour.class)
public class GetInsuranceBehaviour extends Behaviour<InsuranceRequest, InsuranceResponse> {

	@Reference
	private AncillaryService ancillaryService;

	@Override
	public InsuranceResponse executeOrchestration(InsuranceRequest request) {
		if(request == null) throw new IllegalArgumentException("Cross selling request is null.");
		return ancillaryService.getInsurance(request);
	}

}
