package com.alitalia.aem.web.component.bookingancillary.delegate;

import com.alitalia.aem.common.messages.home.InsuranceRequest;
import com.alitalia.aem.common.messages.home.InsuranceResponse;

public interface BookingAncillaryDelegate {

	public InsuranceResponse getInsurance(InsuranceRequest request);
}
