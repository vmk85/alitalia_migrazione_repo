package com.alitalia.aem.web.component.flightstatus.delegate;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.messages.home.RetrieveAirportRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportResponse;
import com.alitalia.aem.common.messages.home.RetrieveFlightDetailsRequest;
import com.alitalia.aem.common.messages.home.RetrieveFlightDetailsResponse;
import com.alitalia.aem.common.messages.home.RetrieveFlightInfoRequest;
import com.alitalia.aem.common.messages.home.RetrieveFlightInfoResponse;
import com.alitalia.aem.common.messages.home.RetrieveFullItinerariesRequest;
import com.alitalia.aem.common.messages.home.RetrieveFullItinerariesResponse;
import com.alitalia.aem.common.messages.home.RetrieveItinerariesRequest;
import com.alitalia.aem.common.messages.home.RetrieveItinerariesResponse;
import com.alitalia.aem.web.component.flightstatus.behaviour.RetrieveAirportBehaviour;
import com.alitalia.aem.web.component.flightstatus.behaviour.RetrieveFlightDetailsBehaviour;
import com.alitalia.aem.web.component.flightstatus.behaviour.RetrieveFlightInfoBehaviour;
import com.alitalia.aem.web.component.flightstatus.behaviour.RetrieveFullItinerariesBehaviour;
import com.alitalia.aem.web.component.flightstatus.behaviour.RetrieveItinerariesBehaviour;

@Component(immediate = true, metatype = false)
@Service(value = FlightStatusDelegate.class)
public class SimpleFlightStatusDelegate implements FlightStatusDelegate {

	@Reference
	private RetrieveAirportBehaviour retrieveAirportsBehaviour;
	
	@Reference
	private RetrieveFlightDetailsBehaviour retrieveFlightDetailsBehaviour;

	@Reference
	private RetrieveFlightInfoBehaviour retrieveFlightInfoBehaviour;
	
	@Reference
	private RetrieveFullItinerariesBehaviour retrieveFullItinerariesBehaviour;
	
	@Reference
	private RetrieveItinerariesBehaviour retrieveItinerariesBehaviour;

	@Override
	public RetrieveAirportResponse retrieveAirport(RetrieveAirportRequest request) {
		BehaviourExecutor<RetrieveAirportBehaviour, RetrieveAirportRequest, RetrieveAirportResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(retrieveAirportsBehaviour, request);
	}
	
	@Override
	public RetrieveFlightDetailsResponse retrieveFlightDetails(RetrieveFlightDetailsRequest request) {
		BehaviourExecutor<RetrieveFlightDetailsBehaviour, RetrieveFlightDetailsRequest, RetrieveFlightDetailsResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(retrieveFlightDetailsBehaviour, request);
	}
	
	@Override
	public RetrieveFlightInfoResponse retrieveFlightInfo(RetrieveFlightInfoRequest request) {
		BehaviourExecutor<RetrieveFlightInfoBehaviour, RetrieveFlightInfoRequest, RetrieveFlightInfoResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(retrieveFlightInfoBehaviour, request);
	}

	@Override
	public RetrieveFullItinerariesResponse retrieveFullItineraries(RetrieveFullItinerariesRequest request) {
		BehaviourExecutor<RetrieveFullItinerariesBehaviour, RetrieveFullItinerariesRequest, RetrieveFullItinerariesResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(retrieveFullItinerariesBehaviour, request);
	}
	
	@Override
	public RetrieveItinerariesResponse retrieveItineraries(RetrieveItinerariesRequest request) {
		BehaviourExecutor<RetrieveItinerariesBehaviour, RetrieveItinerariesRequest, RetrieveItinerariesResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(retrieveItinerariesBehaviour, request);
	}
}