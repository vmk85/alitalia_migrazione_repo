package com.alitalia.aem.web.component.flightstatus.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveAirportRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportResponse;
import com.alitalia.aem.service.api.home.FlightStatusService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveAirportBehaviour.class)
public class RetrieveAirportBehaviour extends Behaviour<RetrieveAirportRequest, RetrieveAirportResponse> {

	@Reference
	private FlightStatusService flightStatusService;
	
	@Override
	public RetrieveAirportResponse executeOrchestration(RetrieveAirportRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Airport request is null.");
		return flightStatusService.retrieveAirport(request);
	}	
}