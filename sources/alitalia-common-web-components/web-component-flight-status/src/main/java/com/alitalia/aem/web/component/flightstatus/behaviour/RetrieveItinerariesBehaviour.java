package com.alitalia.aem.web.component.flightstatus.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveItinerariesRequest;
import com.alitalia.aem.common.messages.home.RetrieveItinerariesResponse;
import com.alitalia.aem.service.api.home.FlightStatusService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveItinerariesBehaviour.class)
public class RetrieveItinerariesBehaviour extends Behaviour<RetrieveItinerariesRequest, RetrieveItinerariesResponse> {

	@Reference
	private FlightStatusService flightStatusService;
	
	@Override
	public RetrieveItinerariesResponse executeOrchestration(RetrieveItinerariesRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Itineraries request is null.");
		return flightStatusService.retrieveItineraries(request);
	}	
}