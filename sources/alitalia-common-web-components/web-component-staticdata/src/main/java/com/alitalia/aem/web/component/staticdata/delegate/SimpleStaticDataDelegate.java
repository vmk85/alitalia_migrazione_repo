package com.alitalia.aem.web.component.staticdata.delegate;

import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.common.messages.home.RetrieveCouncilsRequest;
import com.alitalia.aem.common.messages.home.RetrieveCouncilsResponse;
import com.alitalia.aem.common.messages.home.RetrieveCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCountriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveCreditCardCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCreditCardCountriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveFrequentFlayerRequest;
import com.alitalia.aem.common.messages.home.RetrieveFrequentFlyersResponse;
import com.alitalia.aem.common.messages.home.RetrieveMealsRequest;
import com.alitalia.aem.common.messages.home.RetrieveMealsResponse;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemRequest;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemResponse;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixRequest;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixResponse;
import com.alitalia.aem.common.messages.home.RetrieveProvincesRequest;
import com.alitalia.aem.common.messages.home.RetrieveProvincesResponse;
import com.alitalia.aem.common.messages.home.RetrieveSeatTypeRequest;
import com.alitalia.aem.common.messages.home.RetrieveSeatTypeResponse;
import com.alitalia.aem.common.messages.home.RetrieveStateListRequest;
import com.alitalia.aem.common.messages.home.RetrieveStateListResponse;
import com.alitalia.aem.common.messages.home.RetrieveTicketOfficeRequest;
import com.alitalia.aem.common.messages.home.RetrieveTicketOfficeResponse;
import com.alitalia.aem.web.component.staticdata.behaviour.RetrieveAirportsBehaviour;
import com.alitalia.aem.web.component.staticdata.behaviour.RetrieveCouncilsBehaviour;
import com.alitalia.aem.web.component.staticdata.behaviour.RetrieveCountriesBehaviour;
import com.alitalia.aem.web.component.staticdata.behaviour.RetrieveCreditCardCountriesBehaviour;
import com.alitalia.aem.web.component.staticdata.behaviour.RetrieveFrequentFlyersTypeBehaviour;
import com.alitalia.aem.web.component.staticdata.behaviour.RetrieveMealsBehaviour;
import com.alitalia.aem.web.component.staticdata.behaviour.RetrievePaymentTypeItemsBehaviour;
import com.alitalia.aem.web.component.staticdata.behaviour.RetrievePhonePrefixsBehaviour;
import com.alitalia.aem.web.component.staticdata.behaviour.RetrieveProvincesBehaviour;
import com.alitalia.aem.web.component.staticdata.behaviour.RetrieveSeatsBehaviour;
import com.alitalia.aem.web.component.staticdata.behaviour.RetrieveStateListBehaviour;
import com.alitalia.aem.web.component.staticdata.behaviour.RetrieveTicketsOfficeBehaviour;

@Component(immediate = true, metatype = false)
@Properties({
	@Property( name = SimpleStaticDataDelegate.CACHE_REFRESH_INTERVAL, label = "Refresh interval of the cache." ),
	@Property( name = SimpleStaticDataDelegate.IS_CACHE_ENABLED, label = "Flag to enable cache." )
})
@Service(value = StaticDataDelegate.class)
public class SimpleStaticDataDelegate implements StaticDataDelegate {

	private static final Logger logger = LoggerFactory.getLogger(SimpleStaticDataDelegate.class);

	@Reference
	private RetrieveCountriesBehaviour retrieveCountriesBehaviour;

	@Reference
	private RetrieveCreditCardCountriesBehaviour retrieveCreditCardCountriesBehaviour;

	@Reference
	private RetrieveAirportsBehaviour retrieveAirportsBehaviour;

	@Reference
	private RetrieveFrequentFlyersTypeBehaviour retrieveFrequentFlyersTypeBehaviour;

	@Reference
	private RetrieveMealsBehaviour retrieveMealsBehaviour;

	@Reference
	private RetrievePaymentTypeItemsBehaviour retrievePaymentTypeItemsBehaviour;

	@Reference
	private RetrievePhonePrefixsBehaviour retrievePhonePrefixsBehaviour;

	@Reference
	private RetrieveSeatsBehaviour retrieveSeatsBehaviour;

	@Reference
	private RetrieveTicketsOfficeBehaviour retrieveTicketsOfficeBehaviour;

	@Reference
	private RetrieveCouncilsBehaviour retrieveCouncilsBehaviour;

	@Reference
	private RetrieveProvincesBehaviour retrieveProvincesBehaviour;

	@Reference
	private RetrieveStateListBehaviour retrieveStateListBehaviour;
	
	private long refreshInterval = 3600000L;
	private boolean isCacheEnabled = false;
	
	private RetrieveCountriesResponse retrieveCountriesResponse;
	private RetrieveCreditCardCountriesResponse retrieveCreditCardCountriesResponse;
	private RetrieveFrequentFlyersResponse retrieveFrequentFlyersResponse;
	private RetrieveMealsResponse retrieveMealsResponse;
	private RetrievePaymentTypeItemResponse retrievePaymentTypeItemResponse;
	private RetrievePhonePrefixResponse retrievePhonePrefixResponse;
	private RetrieveSeatTypeResponse retrieveSeatTypeResponse;
	private RetrieveTicketOfficeResponse retrieveTicketOfficeResponse;
	private RetrieveAirportsResponse retrieveAirportsResponse;
	private RetrieveCouncilsResponse retrieveCouncilsResponse;
	private RetrieveProvincesResponse retrieveProvincesResponse;
	private Hashtable<String, RetrieveStateListResponse> cacheStatesMap = new Hashtable<String, RetrieveStateListResponse>();
	
	private Object retrieveCountriesResponseLock = new Object();
	private Object retrieveCreditCardCountriesResponseLock = new Object();
	private Object retrieveFrequentFlyersResponseLock = new Object();
	private Object retrieveMealsResponseLock = new Object();
	private Object retrievePaymentTypeItemResponseLock = new Object();
	private Object retrievePhonePrefixResponseLock = new Object();
	private Object retrieveSeatTypeResponseLock = new Object();
	private Object retrieveTicketOfficeResponseLock = new Object();
	private Object retrieveAirportsResponseLock = new Object();
	private Object retrieveCouncilsResponseLock = new Object();
	private Object retrieveProvincesResponseLock = new Object();
	private Object retrieveStateListResponseLock = new Object();
	
	private long lastCleanCountries = System.currentTimeMillis();
	private long lastCleanCreditCard = System.currentTimeMillis();
	private long lastCleanFrequentFlyers = System.currentTimeMillis();
	private long lastCleanMeals = System.currentTimeMillis();
	private long lastCleanPaymentType = System.currentTimeMillis();
	private long lastCleanPhonePrefix = System.currentTimeMillis();
	private long lastCleanSeatType = System.currentTimeMillis();
	private long lastCleanTicketOffice = System.currentTimeMillis();
	private long lastCleanAirports = System.currentTimeMillis();
	private long lastCleanCouncils = System.currentTimeMillis();
	private long lastCleanProvinces = System.currentTimeMillis();
	private long lastCleanStates = System.currentTimeMillis();


	public static final String CACHE_REFRESH_INTERVAL = "cache.refresh.interval";
	public static final String IS_CACHE_ENABLED = "cache.is_enabled";

	@Override
	public RetrieveCountriesResponse retrieveCountries(RetrieveCountriesRequest request) {
		BehaviourExecutor<RetrieveCountriesBehaviour, RetrieveCountriesRequest, RetrieveCountriesResponse> executor = new BehaviourExecutor<>();

		logger.debug("Executing delegate retrieveCountries");
		
		synchronized(retrieveCountriesResponseLock) {
			if (isCacheEnabled) {
				if (isCacheToBeRefreshed(lastCleanCountries)){
					retrieveCountriesResponse = null;
					lastCleanCountries = System.currentTimeMillis();
				}

				if (retrieveCountriesResponse == null || retrieveCountriesResponse.getCountries() == null || retrieveCountriesResponse.getCountries().size()==0) {
					retrieveCountriesResponse = executor.executeBehaviour(retrieveCountriesBehaviour, request);
				} else {
					logger.info("retrieveCountries: data retrieved from cache");
				}

				// Crea una copia del dato per evitare side effect
				RetrieveCountriesResponse response = new RetrieveCountriesResponse(retrieveCountriesResponse.getTid(),
						retrieveCountriesResponse.getSid());
				response.setCountries(new ArrayList<CountryData>());
				for (CountryData sourceData : retrieveCountriesResponse.getCountries())
					response.getCountries().add(new CountryData(sourceData.getCode(), sourceData.getDescription(), 
							sourceData.getStateCode(), sourceData.getStateDescription()));

				return response;
			} else {
				logger.debug("Cache manager disabled - starting service call to get countries list");
				return executor.executeBehaviour(retrieveCountriesBehaviour, request);
			}
		}
	}

	@Override
	public RetrieveCreditCardCountriesResponse retrieveCreditCardCountries(RetrieveCreditCardCountriesRequest request){
		BehaviourExecutor<RetrieveCreditCardCountriesBehaviour, RetrieveCreditCardCountriesRequest, RetrieveCreditCardCountriesResponse> executor = new BehaviourExecutor<>();

		logger.debug("Executing delegate retrieveCreditCardCountries");
		
		synchronized(retrieveCreditCardCountriesResponseLock){
			
			if (isCacheEnabled) {
				if (isCacheToBeRefreshed(lastCleanCreditCard)){
					retrieveCreditCardCountriesResponse = null;
					lastCleanCreditCard = System.currentTimeMillis();
				}
				
				if (retrieveCreditCardCountriesResponse == null || retrieveCreditCardCountriesResponse.getCountries() == null || retrieveCreditCardCountriesResponse.getCountries().size()==0) {
					retrieveCreditCardCountriesResponse = executor.executeBehaviour(retrieveCreditCardCountriesBehaviour, request);
				} else {
					logger.info("retrieveCreditCardCountries: data retrieved from cache");
				}
				
				return retrieveCreditCardCountriesResponse;
			} else {
				logger.debug("Cache manager disabled - starting service call to get credit cards for countries list");
				return executor.executeBehaviour(retrieveCreditCardCountriesBehaviour, request);
			}
			
		}
	}

	@Override
	public RetrieveFrequentFlyersResponse retrieveFrequentFlyersType(RetrieveFrequentFlayerRequest request) {
		BehaviourExecutor<RetrieveFrequentFlyersTypeBehaviour, RetrieveFrequentFlayerRequest, RetrieveFrequentFlyersResponse> executor = new BehaviourExecutor<>();

		logger.debug("Executing delegate retrieveFrequentFlyersType");
		
		synchronized(retrieveFrequentFlyersResponseLock) {
			
			if (isCacheEnabled) {
				if (isCacheToBeRefreshed(lastCleanFrequentFlyers)){
					retrieveFrequentFlyersResponse = null;
					lastCleanFrequentFlyers = System.currentTimeMillis();
				}

				if (retrieveFrequentFlyersResponse == null || retrieveFrequentFlyersResponse.getFrequentFlyers() == null || retrieveFrequentFlyersResponse.getFrequentFlyers().size()==0) {
					retrieveFrequentFlyersResponse = executor.executeBehaviour(retrieveFrequentFlyersTypeBehaviour, request);
				} else {
					logger.info("retrieveFrequentFlyersType: data retrieved from cache");
				}

				return retrieveFrequentFlyersResponse;
			} else {
				logger.debug("Cache manager disabled - starting service call to get credit cards for countries list");
				return executor.executeBehaviour(retrieveFrequentFlyersTypeBehaviour, request);
			}
			
		}
	}

	@Override
	public RetrieveMealsResponse retrieveMeals(RetrieveMealsRequest request) {
		BehaviourExecutor<RetrieveMealsBehaviour, RetrieveMealsRequest, RetrieveMealsResponse> executor = new BehaviourExecutor<RetrieveMealsBehaviour, RetrieveMealsRequest, RetrieveMealsResponse>();
		
		logger.debug("Executing delegate retrieveMeals");
		
		synchronized(retrieveMealsResponseLock) {
			
			if (isCacheEnabled) {
				if (isCacheToBeRefreshed(lastCleanMeals)){
					retrieveMealsResponse = null;
					lastCleanMeals = System.currentTimeMillis();
				}

				if (retrieveMealsResponse == null || retrieveMealsResponse.getMeals() == null || retrieveMealsResponse.getMeals().size()==0) {
					retrieveMealsResponse = executor.executeBehaviour(retrieveMealsBehaviour, request);
				} else {
					logger.info("retrieveMeals: data retrieved from cache");
				}

				return retrieveMealsResponse;
			} else {
				logger.debug("Cache manager disabled - starting service call to get meals list");
				return executor.executeBehaviour(retrieveMealsBehaviour, request);
			}
		}
	}

	@Override
	public RetrievePaymentTypeItemResponse retrievePaymentTypeItems(RetrievePaymentTypeItemRequest request) {
		BehaviourExecutor<RetrievePaymentTypeItemsBehaviour, RetrievePaymentTypeItemRequest, RetrievePaymentTypeItemResponse> executor = new BehaviourExecutor<RetrievePaymentTypeItemsBehaviour, RetrievePaymentTypeItemRequest, RetrievePaymentTypeItemResponse>();

		logger.debug("Executing delegate retrievePaymentTypeItems");
		
		synchronized(retrievePaymentTypeItemResponseLock) {
			
			if (isCacheEnabled) {
				if (isCacheToBeRefreshed(lastCleanPaymentType)){
					retrievePaymentTypeItemResponse = null;
					lastCleanPaymentType = System.currentTimeMillis();
				}

				if (retrievePaymentTypeItemResponse == null || retrievePaymentTypeItemResponse.getPaymentTypeItemsData() == null || retrievePaymentTypeItemResponse.getPaymentTypeItemsData().size()==0) {
					retrievePaymentTypeItemResponse = executor.executeBehaviour(retrievePaymentTypeItemsBehaviour, request);
				} else {
					logger.info("retrievePaymentTypeItems: data retrieved from cache");
				}

				return retrievePaymentTypeItemResponse;
			} else {
				logger.debug("Cache manager disabled - starting service call to get payments type list");
				return executor.executeBehaviour(retrievePaymentTypeItemsBehaviour, request);
			}
		}
	}

	@Override
	public RetrievePhonePrefixResponse retrievePhonePrefixs(RetrievePhonePrefixRequest request) {
		BehaviourExecutor<RetrievePhonePrefixsBehaviour, RetrievePhonePrefixRequest, RetrievePhonePrefixResponse> executor = new BehaviourExecutor<RetrievePhonePrefixsBehaviour, RetrievePhonePrefixRequest, RetrievePhonePrefixResponse>();

		logger.debug("Executing delegate retrievePhonePrefixs");
		
		synchronized(retrievePhonePrefixResponseLock) {
			
			if (isCacheEnabled) {
				if (isCacheToBeRefreshed(lastCleanPhonePrefix)){
					retrievePhonePrefixResponse = null;
					lastCleanPhonePrefix = System.currentTimeMillis();
				}

				if (retrievePhonePrefixResponse == null || retrievePhonePrefixResponse.getPhonePrefix() == null || retrievePhonePrefixResponse.getPhonePrefix().size()==0) {
					retrievePhonePrefixResponse = executor.executeBehaviour(retrievePhonePrefixsBehaviour, request);
				} else {
					logger.info("retrievePhonePrefixs: data retrieved from cache");
				}

				return retrievePhonePrefixResponse;
			} else {
				logger.debug("Cache manager disabled - starting service call to get phones prefix list");
				return executor.executeBehaviour(retrievePhonePrefixsBehaviour, request);
			}
		}
	}

	@Override
	public RetrieveSeatTypeResponse retrievesSeatTypes(RetrieveSeatTypeRequest request) {
		BehaviourExecutor<RetrieveSeatsBehaviour, RetrieveSeatTypeRequest, RetrieveSeatTypeResponse> executor = new BehaviourExecutor<RetrieveSeatsBehaviour, RetrieveSeatTypeRequest, RetrieveSeatTypeResponse>();

		logger.debug("Executing delegate retrievesSeatTypes");
		
		synchronized(retrieveSeatTypeResponseLock) {
			
			if (isCacheEnabled) {
				if (isCacheToBeRefreshed(lastCleanSeatType)){
					retrieveSeatTypeResponse = null;
					lastCleanSeatType = System.currentTimeMillis();
				}

				if (retrieveSeatTypeResponse == null || retrieveSeatTypeResponse.getSeatTypesData() == null || retrieveSeatTypeResponse.getSeatTypesData().size()==0) {
					retrieveSeatTypeResponse = executor.executeBehaviour(retrieveSeatsBehaviour, request);
				} else {
					logger.info("retrievePhonePrefixs: data retrieved from cache");
				}

				return retrieveSeatTypeResponse;
			} else {
				logger.debug("Cache manager disabled - starting service call to get seats type list");
				return executor.executeBehaviour(retrieveSeatsBehaviour, request);
			}
			
		}
	}

	@Override
	public RetrieveTicketOfficeResponse retrieveTicketsOffice(RetrieveTicketOfficeRequest request) {
		BehaviourExecutor<RetrieveTicketsOfficeBehaviour, RetrieveTicketOfficeRequest, RetrieveTicketOfficeResponse> executor = new BehaviourExecutor<RetrieveTicketsOfficeBehaviour, RetrieveTicketOfficeRequest, RetrieveTicketOfficeResponse>();

		logger.debug("Executing Delegate retrieveTicketsOffice");
		
		synchronized(retrieveTicketOfficeResponseLock) {
			
			if (isCacheEnabled) {
				if (isCacheToBeRefreshed(lastCleanTicketOffice)){
					retrieveTicketOfficeResponse = null;
					lastCleanTicketOffice = System.currentTimeMillis();
				}

				if (retrieveTicketOfficeResponse == null || retrieveTicketOfficeResponse.getTicketOfficeData() == null || retrieveTicketOfficeResponse.getTicketOfficeData().size()==0) {
					retrieveTicketOfficeResponse = executor.executeBehaviour(retrieveTicketsOfficeBehaviour, request);
				} else {
					logger.info("retrievePhonePrefixs: data retrieved from cache");
				}

				return retrieveTicketOfficeResponse;
			} else {
				logger.debug("Cache manager disabled - starting service call to get ticket offices list");
				return executor.executeBehaviour(retrieveTicketsOfficeBehaviour, request);
			}
			
		}
		
	}

	@Override
	public RetrieveAirportsResponse retrieveAirports(RetrieveAirportsRequest request) {
		BehaviourExecutor<RetrieveAirportsBehaviour, RetrieveAirportsRequest, RetrieveAirportsResponse> executor = new BehaviourExecutor<RetrieveAirportsBehaviour, RetrieveAirportsRequest, RetrieveAirportsResponse>();

		logger.debug("Executing delegate retrieveAirports");
		
		synchronized(retrieveAirportsResponseLock) {
			
			if (isCacheEnabled) {
				if (isCacheToBeRefreshed(lastCleanAirports)){
					retrieveAirportsResponse = null;
					lastCleanAirports = System.currentTimeMillis();
				}

				if (retrieveAirportsResponse == null || retrieveAirportsResponse.getAirports() == null || retrieveAirportsResponse.getAirports().size()==0) {
					retrieveAirportsResponse = executor.executeBehaviour(retrieveAirportsBehaviour, request);
				} else {
					logger.info("retrieveAirports: data retrieved from cache");
				}

				return retrieveAirportsResponse;
			} else {
				logger.debug("Cache manager disabled - starting service call to get airports list");
				return executor.executeBehaviour(retrieveAirportsBehaviour, request);
			}
			
		}
		
	}

	@Override
	public RetrieveCouncilsResponse retrieveCouncils(RetrieveCouncilsRequest request) {
		BehaviourExecutor<RetrieveCouncilsBehaviour, RetrieveCouncilsRequest, RetrieveCouncilsResponse> executor = new BehaviourExecutor<RetrieveCouncilsBehaviour, RetrieveCouncilsRequest, RetrieveCouncilsResponse>();

		logger.debug("Executing delegate retrieveCouncils");
		
		synchronized(retrieveCouncilsResponseLock) {
			
			if (isCacheEnabled) {
				if (isCacheToBeRefreshed(lastCleanCouncils)){
					retrieveCouncilsResponse = null;
					lastCleanCouncils = System.currentTimeMillis();
				}

				if (retrieveCouncilsResponse == null || retrieveCouncilsResponse.getCouncils() == null || retrieveCouncilsResponse.getCouncils().size()==0) {
					retrieveCouncilsResponse = executor.executeBehaviour(retrieveCouncilsBehaviour, request);
				} else {
					logger.info("retrieveCouncils: data retrieved from cache");
				}

				return retrieveCouncilsResponse;
			} else {
				logger.debug("Cache manager disabled - starting service call to get councils list");
				return executor.executeBehaviour(retrieveCouncilsBehaviour, request);
			}
			
		}

	}

	@Override
	public RetrieveProvincesResponse retrieveProvinces(RetrieveProvincesRequest request) {
		BehaviourExecutor<RetrieveProvincesBehaviour, RetrieveProvincesRequest, RetrieveProvincesResponse> executor = new BehaviourExecutor<RetrieveProvincesBehaviour, RetrieveProvincesRequest, RetrieveProvincesResponse>();

		logger.debug("Executing delegate retrieveProvinces");
		
		synchronized(retrieveProvincesResponseLock) {
			
			if (isCacheEnabled) {
				if (isCacheToBeRefreshed(lastCleanProvinces)){
					retrieveProvincesResponse = null;
					lastCleanProvinces = System.currentTimeMillis();
				}

				if (retrieveProvincesResponse == null || retrieveProvincesResponse.getProvinces() == null || retrieveProvincesResponse.getProvinces().size()==0) {
					retrieveProvincesResponse = executor.executeBehaviour(retrieveProvincesBehaviour, request);
				} else {
					logger.info("retrieveProvinces: data retrieved from cache");
				}

				return retrieveProvincesResponse;
			} else {
				logger.debug("Cache manager disabled - starting service call to get provinces list");
				return executor.executeBehaviour(retrieveProvincesBehaviour, request);
			}
			
		}
		
	}

	@Override
	public RetrieveStateListResponse retrieveStateList(RetrieveStateListRequest request) {
		BehaviourExecutor<RetrieveStateListBehaviour, RetrieveStateListRequest, RetrieveStateListResponse> executor = new BehaviourExecutor<RetrieveStateListBehaviour, RetrieveStateListRequest, RetrieveStateListResponse>();
		
		logger.debug("Executing delegate retrieveStateList. Request: [{}]", request);
		
		synchronized(retrieveStateListResponseLock) {
			
			if (isCacheEnabled) {
				if (isCacheToBeRefreshed(lastCleanStates)){
					cacheStatesMap.clear();
					lastCleanStates = System.currentTimeMillis();
				}

				if (cacheStatesMap == null || cacheStatesMap.size() == 0 || 
						cacheStatesMap.get(request.getItemCache()) == null || cacheStatesMap.get(request.getItemCache()).getStates() == null || 
						cacheStatesMap.get(request.getItemCache()).getStates().size() == 0) {
						cacheStatesMap.put(request.getItemCache(), executor.executeBehaviour(retrieveStateListBehaviour, request));
				} else {
					logger.info("retrieveStateList: data retrieved from cache");
				}

				return cacheStatesMap.get(request.getItemCache());
			} else {
				logger.debug("Cache manager disabled - starting service call to get states list");
				return executor.executeBehaviour(retrieveStateListBehaviour, request);
			}
			
		}

	}

	private boolean isCacheToBeRefreshed(Long lastClean) {
		long currentTime = System.currentTimeMillis();
		logger.debug("SimpleStaticDataDelegate.checkIsCacheToBeRefreshed - currentTime is [" + currentTime + "], lastClean is [" + lastClean + "] and refreshInterval is [" + refreshInterval + "]");
		if (currentTime - lastClean > refreshInterval) {
			logger.debug("SimpleStaticDataDelegate.checkIsCacheToBeRefreshed - clear cache");
//			lastClean = currentTime;
			return true;
		}
		return false;
	}

	@Activate
	private void activate(ComponentContext componentContext) {
		try {		
			isCacheEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_CACHE_ENABLED), false);
			refreshInterval = PropertiesUtil.toLong(
					componentContext.getProperties().get(CACHE_REFRESH_INTERVAL), 3600000);
		} catch (Exception e) {
			logger.error("Error to modify SimpleStaticDataDelegate - Load default value", e);
			isCacheEnabled = false;			
		}

		logger.info("Activated SimpleStaticDataDelegate: "
				+ "IS_CACHE_ENABLED=[" + isCacheEnabled + "], "
				+ "CACHE_REFRESH_INTERVAL=[" + refreshInterval + "]");
	}

	@Modified
	private void modified(ComponentContext componentContext) {
		try {
			isCacheEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_CACHE_ENABLED), false);
			refreshInterval = PropertiesUtil.toLong(
					componentContext.getProperties().get(CACHE_REFRESH_INTERVAL), 3600000);
		} catch (Exception e) {
			logger.error("Error to modify SimpleStaticDataDelegate - Load default value", e);
			isCacheEnabled = false;			
		}

		logger.info("Modified SimpleStaticDataDelegate: "
				+ "IS_CACHE_ENABLED=[" + isCacheEnabled + "], "
				+ "CACHE_REFRESH_INTERVAL=[" + refreshInterval + "]");
	}

}