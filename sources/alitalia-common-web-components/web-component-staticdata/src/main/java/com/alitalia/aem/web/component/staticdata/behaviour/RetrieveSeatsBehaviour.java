package com.alitalia.aem.web.component.staticdata.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveSeatTypeRequest;
import com.alitalia.aem.common.messages.home.RetrieveSeatTypeResponse;
import com.alitalia.aem.service.api.home.StaticDataService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveSeatsBehaviour.class)
public class RetrieveSeatsBehaviour extends Behaviour<RetrieveSeatTypeRequest, RetrieveSeatTypeResponse> {

	@Reference
	private StaticDataService staticDataService;
	
	@Override
	public RetrieveSeatTypeResponse executeOrchestration(RetrieveSeatTypeRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Seats request is null.");
		return staticDataService.retrieveSeatTypes(request);
	}
}