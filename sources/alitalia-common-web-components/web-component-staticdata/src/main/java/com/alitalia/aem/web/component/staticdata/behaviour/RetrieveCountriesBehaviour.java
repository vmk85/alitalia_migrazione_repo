package com.alitalia.aem.web.component.staticdata.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCountriesResponse;
import com.alitalia.aem.service.api.home.StaticDataService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveCountriesBehaviour.class)
public class RetrieveCountriesBehaviour extends Behaviour<RetrieveCountriesRequest, RetrieveCountriesResponse> {

	@Reference
	private StaticDataService staticDataService;
	
	@Override
	public RetrieveCountriesResponse executeOrchestration(RetrieveCountriesRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Countries request is null.");
		return staticDataService.retrieveCountries(request);
	}
}