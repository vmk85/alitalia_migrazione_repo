package com.alitalia.aem.web.component.staticdata.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveTicketOfficeRequest;
import com.alitalia.aem.common.messages.home.RetrieveTicketOfficeResponse;
import com.alitalia.aem.service.api.home.StaticDataService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveTicketsOfficeBehaviour.class)
public class RetrieveTicketsOfficeBehaviour extends Behaviour<RetrieveTicketOfficeRequest, RetrieveTicketOfficeResponse> {

	@Reference
	private StaticDataService staticDataService;
	
	@Override
	public RetrieveTicketOfficeResponse executeOrchestration(RetrieveTicketOfficeRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Tickets Office request is null.");
		return staticDataService.retrieveTicketsOffice(request);
	}
}