package com.alitalia.aem.web.component.staticdata.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveProvincesRequest;
import com.alitalia.aem.common.messages.home.RetrieveProvincesResponse;
import com.alitalia.aem.service.api.home.StaticDataService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveProvincesBehaviour.class)
public class RetrieveProvincesBehaviour extends Behaviour<RetrieveProvincesRequest, RetrieveProvincesResponse> {

	@Reference
	private StaticDataService staticDataService;
	
	@Override
	public RetrieveProvincesResponse executeOrchestration(RetrieveProvincesRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Provinces request is null.");
		return staticDataService.retrieveProvinces(request);
	}
}