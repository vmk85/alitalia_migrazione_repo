package com.alitalia.aem.web.component.staticdata.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveCouncilsRequest;
import com.alitalia.aem.common.messages.home.RetrieveCouncilsResponse;
import com.alitalia.aem.service.api.home.StaticDataService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveCouncilsBehaviour.class)
public class RetrieveCouncilsBehaviour extends Behaviour<RetrieveCouncilsRequest, RetrieveCouncilsResponse> {

	@Reference
	private StaticDataService staticDataService;
	
	@Override
	public RetrieveCouncilsResponse executeOrchestration(RetrieveCouncilsRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Councils request is null.");
		return staticDataService.retrieveCouncils(request);
	}
}