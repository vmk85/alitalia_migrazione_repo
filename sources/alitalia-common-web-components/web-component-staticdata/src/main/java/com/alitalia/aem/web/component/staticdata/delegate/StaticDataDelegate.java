package com.alitalia.aem.web.component.staticdata.delegate;

import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.common.messages.home.RetrieveCouncilsRequest;
import com.alitalia.aem.common.messages.home.RetrieveCouncilsResponse;
import com.alitalia.aem.common.messages.home.RetrieveCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCountriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveCreditCardCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCreditCardCountriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveFrequentFlayerRequest;
import com.alitalia.aem.common.messages.home.RetrieveFrequentFlyersResponse;
import com.alitalia.aem.common.messages.home.RetrieveMealsRequest;
import com.alitalia.aem.common.messages.home.RetrieveMealsResponse;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemRequest;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemResponse;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixRequest;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixResponse;
import com.alitalia.aem.common.messages.home.RetrieveProvincesRequest;
import com.alitalia.aem.common.messages.home.RetrieveProvincesResponse;
import com.alitalia.aem.common.messages.home.RetrieveSeatTypeRequest;
import com.alitalia.aem.common.messages.home.RetrieveSeatTypeResponse;
import com.alitalia.aem.common.messages.home.RetrieveStateListRequest;
import com.alitalia.aem.common.messages.home.RetrieveStateListResponse;
import com.alitalia.aem.common.messages.home.RetrieveTicketOfficeRequest;
import com.alitalia.aem.common.messages.home.RetrieveTicketOfficeResponse;

public interface StaticDataDelegate {

	RetrieveCountriesResponse retrieveCountries(RetrieveCountriesRequest request);
	RetrieveCreditCardCountriesResponse retrieveCreditCardCountries(RetrieveCreditCardCountriesRequest request);
	RetrieveFrequentFlyersResponse retrieveFrequentFlyersType(RetrieveFrequentFlayerRequest request);
	RetrieveMealsResponse retrieveMeals(RetrieveMealsRequest request);
	RetrievePaymentTypeItemResponse retrievePaymentTypeItems(RetrievePaymentTypeItemRequest request);
	RetrievePhonePrefixResponse retrievePhonePrefixs(RetrievePhonePrefixRequest request);
	RetrieveSeatTypeResponse retrievesSeatTypes(RetrieveSeatTypeRequest request);
	RetrieveTicketOfficeResponse retrieveTicketsOffice(RetrieveTicketOfficeRequest request);
	RetrieveAirportsResponse retrieveAirports(RetrieveAirportsRequest request);
	RetrieveCouncilsResponse retrieveCouncils(RetrieveCouncilsRequest request);
	RetrieveProvincesResponse retrieveProvinces(RetrieveProvincesRequest request);
	RetrieveStateListResponse retrieveStateList(RetrieveStateListRequest request);
}