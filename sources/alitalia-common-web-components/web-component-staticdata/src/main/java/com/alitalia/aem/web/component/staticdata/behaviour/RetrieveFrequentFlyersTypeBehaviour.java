package com.alitalia.aem.web.component.staticdata.behaviour;

import java.util.Iterator;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.data.home.FrequentFlyerTypeData;
import com.alitalia.aem.common.messages.home.RetrieveFrequentFlayerRequest;
import com.alitalia.aem.common.messages.home.RetrieveFrequentFlyersResponse;
import com.alitalia.aem.service.api.home.StaticDataService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveFrequentFlyersTypeBehaviour.class)
public class RetrieveFrequentFlyersTypeBehaviour extends Behaviour<RetrieveFrequentFlayerRequest, RetrieveFrequentFlyersResponse> {

	@Reference
	private StaticDataService staticDataService;
	
	private Logger logger = LoggerFactory.getLogger(RetrieveFrequentFlyersTypeBehaviour.class);


	@Override
	public RetrieveFrequentFlyersResponse executeOrchestration(RetrieveFrequentFlayerRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Frequent Flyers Type request is null.");
		RetrieveFrequentFlyersResponse response = staticDataService.retrieveFrequentFlyers(request);

		// WORKAROUND ELIMINAZIONE PAYBACK (NON RIPROTETTO DA SABRE AL CUTOVER)
//		if (response != null && response.getFrequentFlyers() != null){
//			logger.debug("RetrieveFrequentFlyerData. Size: {}", response.getFrequentFlyers().size());
//			Iterator<FrequentFlyerTypeData> it = response.getFrequentFlyers().iterator();
//			while(it.hasNext()) {
//				FrequentFlyerTypeData fft = it.next();
//				if("PB".equals(fft.getCode())){
//					it.remove();
//					break;
//				}
//			}
//			logger.debug("RetrieveFrequentFlyerData. Rimosso payback. New Size: {}", response.getFrequentFlyers().size());
//		}
		return response;
	}
}