package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.ChangeCabinCheckinRequest;
import com.alitalia.aem.common.messages.home.ChangeCabinCheckinResponse;
import com.alitalia.aem.service.api.home.WebCheckinComfortSeatService;

@Component(immediate = true, metatype = false)
@Service(value = ChangeCabinBehaviour.class)
public class ChangeCabinBehaviour extends Behaviour<ChangeCabinCheckinRequest, ChangeCabinCheckinResponse> {
	
	@Reference
	private WebCheckinComfortSeatService webcheckinComfortSeatService;
	
	@Override
	public ChangeCabinCheckinResponse executeOrchestration(ChangeCabinCheckinRequest request) {
		if (request == null)
			throw new IllegalArgumentException("ChangeCabinCheckinRequest is null.");
		return webcheckinComfortSeatService.changeCabin(request);
	}

}
