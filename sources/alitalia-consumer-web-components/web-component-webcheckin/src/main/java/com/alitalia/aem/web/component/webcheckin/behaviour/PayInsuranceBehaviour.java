package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.CheckinInsuranceResponse;
import com.alitalia.aem.common.messages.home.WebCheckinPayInsuranceRequest;
import com.alitalia.aem.service.api.home.WebCheckinInsuranceService;

@Component(immediate = true, metatype = false)
@Service(value = PayInsuranceBehaviour.class)
public class PayInsuranceBehaviour extends Behaviour<WebCheckinPayInsuranceRequest, CheckinInsuranceResponse> {
	
	@Reference
	private WebCheckinInsuranceService webCheckinInsuranceService;
	
	@Override
	public CheckinInsuranceResponse executeOrchestration(WebCheckinPayInsuranceRequest request) {
		if (request == null)
			throw new IllegalArgumentException("WebCheckinPayInsuranceRequest is null.");
		return webCheckinInsuranceService.payInsurance(request);
	}
}