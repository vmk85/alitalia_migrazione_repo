package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.ComfortSeatPurchaseCheckinRequest;
import com.alitalia.aem.common.messages.home.ComfortSeatPurchaseCheckinResponse;
import com.alitalia.aem.service.api.home.WebCheckinComfortSeatService;

@Component(immediate = true, metatype = false)
@Service(value = SelectComfortSeatPurchaseBehaviour.class)
public class SelectComfortSeatPurchaseBehaviour extends Behaviour<ComfortSeatPurchaseCheckinRequest, ComfortSeatPurchaseCheckinResponse> {
	
	@Reference
	private WebCheckinComfortSeatService webcheckinComfortSeatService;
	
	@Override
	public ComfortSeatPurchaseCheckinResponse executeOrchestration(ComfortSeatPurchaseCheckinRequest request) {
		if (request == null)
			throw new IllegalArgumentException("ComfortSeatPurchaseCheckinRequest is null.");
		return webcheckinComfortSeatService.selectComfortSeatPurchase(request);
	}

}
