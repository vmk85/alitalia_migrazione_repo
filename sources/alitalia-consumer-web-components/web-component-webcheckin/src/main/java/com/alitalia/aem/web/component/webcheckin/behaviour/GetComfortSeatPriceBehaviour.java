package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.ComfortSeatPriceCheckinRequest;
import com.alitalia.aem.common.messages.home.ComfortSeatPriceCheckinResponse;
import com.alitalia.aem.service.api.home.WebCheckinComfortSeatService;

@Component(immediate = true, metatype = false)
@Service(value = GetComfortSeatPriceBehaviour.class)
public class GetComfortSeatPriceBehaviour extends Behaviour<ComfortSeatPriceCheckinRequest, ComfortSeatPriceCheckinResponse> {
	
	@Reference
	private WebCheckinComfortSeatService webcheckinComfortSeatService;
	
	@Override
	public ComfortSeatPriceCheckinResponse executeOrchestration(ComfortSeatPriceCheckinRequest request) {
		if (request == null)
			throw new IllegalArgumentException("ComfortSeatPriceCheckinRequest is null.");
		return webcheckinComfortSeatService.getComfortSeatPrice(request);
	}

}
