package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.SendBoardingPassCheckinRequest;
import com.alitalia.aem.common.messages.home.SendBoardingPassCheckinResponse;
import com.alitalia.aem.service.api.home.WebCheckinBoardingPassService;

@Component(immediate = true, metatype = false)
@Service(value = SendBoardingPassBehaviour.class)
public class SendBoardingPassBehaviour extends Behaviour<SendBoardingPassCheckinRequest, SendBoardingPassCheckinResponse> {

	@Reference
	private WebCheckinBoardingPassService webcheckinBoardingPassService;

	@Override
	public SendBoardingPassCheckinResponse executeOrchestration(SendBoardingPassCheckinRequest request) {
		if (request == null)
			throw new IllegalArgumentException("SendBoardingPassCheckinRequest is null.");
		return webcheckinBoardingPassService.sendBoardingPass(request);
	}

}
