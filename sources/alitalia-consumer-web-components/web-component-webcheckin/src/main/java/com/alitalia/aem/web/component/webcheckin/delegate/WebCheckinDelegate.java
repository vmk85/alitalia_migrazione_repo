package com.alitalia.aem.web.component.webcheckin.delegate;

import com.alitalia.aem.common.messages.BaseRequest;
import com.alitalia.aem.common.messages.BaseResponse;
import com.alitalia.aem.common.messages.home.BoardingPassCheckinRequest;
import com.alitalia.aem.common.messages.home.BoardingPassCheckinResponse;
import com.alitalia.aem.common.messages.home.CanAddPassengerCheckinRequest;
import com.alitalia.aem.common.messages.home.CanAddPassengerCheckinResponse;
import com.alitalia.aem.common.messages.home.ChangeCabinCheckinRequest;
import com.alitalia.aem.common.messages.home.ChangeCabinCheckinResponse;
import com.alitalia.aem.common.messages.home.ChangeSeatCheckinRequest;
import com.alitalia.aem.common.messages.home.ChangeSeatCheckinResponse;
import com.alitalia.aem.common.messages.home.CheckinInsuranceResponse;
import com.alitalia.aem.common.messages.home.ComfortSeatOrderCheckinRequest;
import com.alitalia.aem.common.messages.home.ComfortSeatOrderUpdateCheckinRequest;
import com.alitalia.aem.common.messages.home.ComfortSeatPriceCheckinRequest;
import com.alitalia.aem.common.messages.home.ComfortSeatPriceCheckinResponse;
import com.alitalia.aem.common.messages.home.ComfortSeatPurchaseCheckinRequest;
import com.alitalia.aem.common.messages.home.ComfortSeatPurchaseCheckinResponse;
import com.alitalia.aem.common.messages.home.ExtraBaggageOrderCheckinRequest;
import com.alitalia.aem.common.messages.home.ExtraBaggageOrderCheckinResponse;
import com.alitalia.aem.common.messages.home.ExtraBaggagePolicyCheckinRequest;
import com.alitalia.aem.common.messages.home.ExtraBaggagePolicyCheckinResponse;
import com.alitalia.aem.common.messages.home.GenericServiceResponse;
import com.alitalia.aem.common.messages.home.GetCountryRequest;
import com.alitalia.aem.common.messages.home.GetCountryResponse;
import com.alitalia.aem.common.messages.home.GetSeatDisponibilityCheckinRequest;
import com.alitalia.aem.common.messages.home.GetSeatDisponibilityCheckinResponse;
import com.alitalia.aem.common.messages.home.GetSeatMapCheckinRequest;
import com.alitalia.aem.common.messages.home.GetSeatMapCheckinResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutInitRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutInitResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutStatusRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutStatusResponse;
import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesRequest;
import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesResponse;
import com.alitalia.aem.common.messages.home.PassengerListCheckinRequest;
import com.alitalia.aem.common.messages.home.PassengerListCheckinResponse;
import com.alitalia.aem.common.messages.home.PaymentCheckinRequest;
import com.alitalia.aem.common.messages.home.PaymentCheckinResponse;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixResponse;
import com.alitalia.aem.common.messages.home.RetrievePnrInformationCheckinResponse;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationRequest;
import com.alitalia.aem.common.messages.home.SendBoardingPassCheckinRequest;
import com.alitalia.aem.common.messages.home.SendBoardingPassCheckinResponse;
import com.alitalia.aem.common.messages.home.SendReceiptCheckinRequest;
import com.alitalia.aem.common.messages.home.SendReminderCheckinRequest;
import com.alitalia.aem.common.messages.home.SendReminderCheckinResponse;
import com.alitalia.aem.common.messages.home.UpgradePolicyCheckinRequest;
import com.alitalia.aem.common.messages.home.UpgradePolicyCheckinResponse;
import com.alitalia.aem.common.messages.home.ValidateMmUserCheckinRequest;
import com.alitalia.aem.common.messages.home.ValidateMmUserCheckinResponse;
import com.alitalia.aem.common.messages.home.WebCheckinAddToCartRequest;
import com.alitalia.aem.common.messages.home.WebCheckinAirportNameRequest;
import com.alitalia.aem.common.messages.home.WebCheckinAirportNameResponse;
import com.alitalia.aem.common.messages.home.WebCheckinAlertRequest;
import com.alitalia.aem.common.messages.home.WebCheckinAlertResponse;
import com.alitalia.aem.common.messages.home.WebCheckinAncillaryCartResponse;
import com.alitalia.aem.common.messages.home.WebCheckinAncillaryResponse;
import com.alitalia.aem.common.messages.home.WebCheckinBPPermissionRequest;
import com.alitalia.aem.common.messages.home.WebCheckinBPPermissionResponse;
import com.alitalia.aem.common.messages.home.WebCheckinCancelCartRequest;
import com.alitalia.aem.common.messages.home.WebCheckinCancelCartResponse;
import com.alitalia.aem.common.messages.home.WebCheckinEmdRequest;
import com.alitalia.aem.common.messages.home.WebCheckinEmdResponse;
import com.alitalia.aem.common.messages.home.WebCheckinFrequentFlyerResponse;
import com.alitalia.aem.common.messages.home.WebCheckinGetCartRequest;
import com.alitalia.aem.common.messages.home.WebCheckinGetCatalogRequest;
import com.alitalia.aem.common.messages.home.WebCheckinGetInsuranceRequest;
import com.alitalia.aem.common.messages.home.WebCheckinNewPassengerRequest;
import com.alitalia.aem.common.messages.home.WebCheckinNewPassengerResponse;
import com.alitalia.aem.common.messages.home.WebCheckinPayInsuranceRequest;
import com.alitalia.aem.common.messages.home.WebCheckinReasonForIssuanceRequest;
import com.alitalia.aem.common.messages.home.WebCheckinReasonForIssuanceResponse;
import com.alitalia.aem.common.messages.home.WebCheckinRemoveFromCartRequest;
import com.alitalia.aem.common.messages.home.WebCheckinRequest;
import com.alitalia.aem.common.messages.home.WebCheckinResponse;
import com.alitalia.aem.common.messages.home.WebCheckinStatesRequest;
import com.alitalia.aem.common.messages.home.WebCheckinStatesResponse;
import com.alitalia.aem.common.messages.home.WebCheckinUndoCheckinRequest;
import com.alitalia.aem.common.messages.home.WebCheckinUndoCheckinResponse;
import com.alitalia.aem.common.messages.home.WebCheckinUndoManyCheckinRequest;
import com.alitalia.aem.common.messages.home.WebCheckinUndoManyCheckinResponse;
import com.alitalia.aem.common.messages.home.WebCheckinUpdateCartRequest;

public interface WebCheckinDelegate {
	
	//Passenger Service
	PassengerListCheckinResponse getPassengerList(PassengerListCheckinRequest request);
	CanAddPassengerCheckinResponse canAddPassenger(CanAddPassengerCheckinRequest request);
	WebCheckinNewPassengerResponse getNewPassenger(WebCheckinNewPassengerRequest request);
	MmbCheckFrequentFlyerCodesResponse checkFFCode(MmbCheckFrequentFlyerCodesRequest request);

	// Search Service
	RetrievePnrInformationCheckinResponse retrievePnrInformation(RetriveMmbPnrInformationRequest request);
	ValidateMmUserCheckinResponse validateMmUser(ValidateMmUserCheckinRequest request);
	
	// Checkin Service
	WebCheckinResponse checkIn(WebCheckinRequest request);
	WebCheckinUndoCheckinResponse undoCheckin(WebCheckinUndoCheckinRequest request);
	WebCheckinUndoManyCheckinResponse undoManyCheckin(WebCheckinUndoManyCheckinRequest request);
	
	// Static data Service
	GetCountryResponse getCountries(GetCountryRequest request);
	RetrievePhonePrefixResponse getPhonePrefixes(GetCountryRequest request);
	WebCheckinBPPermissionResponse getBoardingPassPermissions(WebCheckinBPPermissionRequest request);
	WebCheckinFrequentFlyerResponse getFrequentFlyers(GetCountryRequest request);
	WebCheckinStatesResponse getAmericanStates(BaseRequest request);
	WebCheckinStatesResponse getStates(WebCheckinStatesRequest request);
	WebCheckinAirportNameResponse getAirportNames(WebCheckinAirportNameRequest request);
	
	//BoardingPassService
	BoardingPassCheckinResponse getBoardingPass(BoardingPassCheckinRequest request);
	SendBoardingPassCheckinResponse sendBoardingPass(SendBoardingPassCheckinRequest request);
	SendReminderCheckinResponse sendReminder(SendReminderCheckinRequest request);
	
	CheckinInsuranceResponse getInsuranceInfo(WebCheckinGetInsuranceRequest request);
	CheckinInsuranceResponse payInsurance(WebCheckinPayInsuranceRequest request);
	
	//Ancillary Service
	WebCheckinAncillaryCartResponse getCatalog(WebCheckinGetCatalogRequest request);
	WebCheckinAncillaryResponse addToCart(WebCheckinAddToCartRequest request);
	WebCheckinAncillaryResponse updateCart(WebCheckinUpdateCartRequest request);
	WebCheckinAncillaryResponse removeFromCart(WebCheckinRemoveFromCartRequest request);
	WebCheckinAncillaryCartResponse getCart(WebCheckinGetCartRequest request);
	MmbAncillaryCheckOutInitResponse checkOutInit(MmbAncillaryCheckOutInitRequest request);
	MmbAncillaryCheckOutStatusResponse getCheckOutStatus(MmbAncillaryCheckOutStatusRequest request);
	WebCheckinCancelCartResponse cancelCart(WebCheckinCancelCartRequest request);
	
	//ExtraBaggageService
	ExtraBaggageOrderCheckinResponse getExtraBaggageOrder(ExtraBaggageOrderCheckinRequest request);
	ExtraBaggagePolicyCheckinResponse getBaggagePolicy(ExtraBaggagePolicyCheckinRequest request);
	
	//PaymentService
	PaymentCheckinResponse executePayment(PaymentCheckinRequest request);
	GenericServiceResponse sendEmailReceipt(SendReceiptCheckinRequest request);
	
	//EMDService
	WebCheckinEmdResponse extraBaggageEmd(WebCheckinEmdRequest request);
	WebCheckinEmdResponse comfortSeatEmd(WebCheckinEmdRequest request);
	WebCheckinEmdResponse cabinUpgradeEmd(WebCheckinEmdRequest request);
	WebCheckinReasonForIssuanceResponse getReasonForIssuance(WebCheckinReasonForIssuanceRequest request);
	
	//ComfortSeatService
	ComfortSeatPurchaseCheckinResponse selectComfortSeatPurchase(ComfortSeatPurchaseCheckinRequest request);
	ComfortSeatPriceCheckinResponse getComfortSeatPrice(ComfortSeatPriceCheckinRequest request);
	GenericServiceResponse existsComfortSeatPurchaseIntoBO(ComfortSeatOrderCheckinRequest request);
	BaseResponse updateComfortSeatPurchaseIntoBO(ComfortSeatOrderUpdateCheckinRequest request);
	
	//SeatMapService
	GetSeatMapCheckinResponse getSeatMap(GetSeatMapCheckinRequest request);
	GetSeatDisponibilityCheckinResponse getSeatDisponibility(GetSeatDisponibilityCheckinRequest request);
	ChangeSeatCheckinResponse changeSeat(ChangeSeatCheckinRequest request);
	ChangeCabinCheckinResponse changeCabin(ChangeCabinCheckinRequest request);
	UpgradePolicyCheckinResponse getUpgradePolicy(UpgradePolicyCheckinRequest request);
	
	//AlertService
	WebCheckinAlertResponse submitUser(WebCheckinAlertRequest request);
}