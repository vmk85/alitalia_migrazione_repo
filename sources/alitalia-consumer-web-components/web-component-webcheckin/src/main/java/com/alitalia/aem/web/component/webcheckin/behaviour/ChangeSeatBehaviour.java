package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.ChangeSeatCheckinRequest;
import com.alitalia.aem.common.messages.home.ChangeSeatCheckinResponse;
import com.alitalia.aem.service.api.home.WebCheckinSeatMapService;

@Component(immediate = true, metatype = false)
@Service(value = ChangeSeatBehaviour.class)
public class ChangeSeatBehaviour extends Behaviour<ChangeSeatCheckinRequest, ChangeSeatCheckinResponse> {
	
	@Reference
	private WebCheckinSeatMapService webcheckinSeatMapService;
	
	@Override
	public ChangeSeatCheckinResponse executeOrchestration(ChangeSeatCheckinRequest request) {
		if (request == null)
			throw new IllegalArgumentException("ChangeSeatCheckinRequest is null.");
		return webcheckinSeatMapService.changeSeat(request);
	}

}
