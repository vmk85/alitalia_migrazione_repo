package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.WebCheckinAncillaryCartResponse;
import com.alitalia.aem.common.messages.home.WebCheckinGetCartRequest;
import com.alitalia.aem.service.api.home.WebCheckinAncillaryService;

@Component(immediate = true, metatype = false)
@Service(value = GetCartAncillaryBehaviour.class)
public class GetCartAncillaryBehaviour extends Behaviour<WebCheckinGetCartRequest, WebCheckinAncillaryCartResponse> {
	
	@Reference
	private WebCheckinAncillaryService webCheckinAncillaryService;
	
	@Override
	public WebCheckinAncillaryCartResponse executeOrchestration(WebCheckinGetCartRequest request) {
		if (request == null)
			throw new IllegalArgumentException("WebCheckinGetCartRequest is null.");
		return webCheckinAncillaryService.getCart(request);
	}
}