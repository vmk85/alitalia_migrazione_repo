package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.WebCheckinStatesRequest;
import com.alitalia.aem.common.messages.home.WebCheckinStatesResponse;
import com.alitalia.aem.service.api.home.WebCheckinStaticDataService;

@Component(immediate = true, metatype = false)
@Service(value = GetStatesBehaviour.class)
public class GetStatesBehaviour extends Behaviour<WebCheckinStatesRequest, WebCheckinStatesResponse> {
	
	@Reference
	private WebCheckinStaticDataService webcheckinStaticDataService;
	
	@Override
	public WebCheckinStatesResponse executeOrchestration(WebCheckinStatesRequest request) {
		if (request == null)
			throw new IllegalArgumentException("WebCheckinStatesRequest is null.");
		return webcheckinStaticDataService.getStates(request);
	}
}