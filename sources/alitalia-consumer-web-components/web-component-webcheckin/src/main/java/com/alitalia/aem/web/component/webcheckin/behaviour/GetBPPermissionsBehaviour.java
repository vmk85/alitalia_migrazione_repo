package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.WebCheckinBPPermissionRequest;
import com.alitalia.aem.common.messages.home.WebCheckinBPPermissionResponse;
import com.alitalia.aem.service.api.home.WebCheckinStaticDataService;

@Component(immediate = true, metatype = false)
@Service(value = GetBPPermissionsBehaviour.class)
public class GetBPPermissionsBehaviour extends Behaviour<WebCheckinBPPermissionRequest, WebCheckinBPPermissionResponse> {
	
	@Reference
	private WebCheckinStaticDataService webCheckinStaticDataService;
	
	@Override
	public WebCheckinBPPermissionResponse executeOrchestration(WebCheckinBPPermissionRequest request) {
		if (request == null)
			throw new IllegalArgumentException("BoardingPassPermissionRequest is null.");
		return webCheckinStaticDataService.getBoardingPassPermissions(request);
	}
}