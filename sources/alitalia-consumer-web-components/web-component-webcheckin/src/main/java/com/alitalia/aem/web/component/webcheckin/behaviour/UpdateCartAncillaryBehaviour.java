package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.WebCheckinAncillaryResponse;
import com.alitalia.aem.common.messages.home.WebCheckinUpdateCartRequest;
import com.alitalia.aem.service.api.home.WebCheckinAncillaryService;

@Component(immediate = true, metatype = false)
@Service(value = UpdateCartAncillaryBehaviour.class)
public class UpdateCartAncillaryBehaviour extends Behaviour<WebCheckinUpdateCartRequest, WebCheckinAncillaryResponse> {
	
	@Reference
	private WebCheckinAncillaryService webCheckinAncillaryService;
	
	@Override
	public WebCheckinAncillaryResponse executeOrchestration(WebCheckinUpdateCartRequest request) {
		if (request == null)
			throw new IllegalArgumentException("WebCheckinUpdateCartRequest is null.");
		return webCheckinAncillaryService.updateCart(request);
	}
}