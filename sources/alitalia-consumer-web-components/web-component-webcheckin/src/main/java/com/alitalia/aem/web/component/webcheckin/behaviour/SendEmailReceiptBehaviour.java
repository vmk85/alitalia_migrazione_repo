package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.GenericServiceResponse;
import com.alitalia.aem.common.messages.home.SendReceiptCheckinRequest;
import com.alitalia.aem.service.api.home.WebCheckinPaymentService;

@Component(immediate = true, metatype = false)
@Service(value = SendEmailReceiptBehaviour.class)
public class SendEmailReceiptBehaviour extends Behaviour<SendReceiptCheckinRequest, GenericServiceResponse> {
	
	@Reference
	private WebCheckinPaymentService webcheckinPaymentService;
	
	@Override
	public GenericServiceResponse executeOrchestration(SendReceiptCheckinRequest request) {
		if (request == null)
			throw new IllegalArgumentException("SendReceiptCheckinRequest is null.");
		return webcheckinPaymentService.sendEmailReceipt(request);
	}

}
