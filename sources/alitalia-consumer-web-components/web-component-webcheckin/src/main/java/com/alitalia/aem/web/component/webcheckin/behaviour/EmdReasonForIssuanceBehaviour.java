package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.WebCheckinReasonForIssuanceRequest;
import com.alitalia.aem.common.messages.home.WebCheckinReasonForIssuanceResponse;
import com.alitalia.aem.service.api.home.WebCheckinEmdService;

@Component(immediate = true, metatype = false)
@Service(value = EmdReasonForIssuanceBehaviour.class)
public class EmdReasonForIssuanceBehaviour extends Behaviour<WebCheckinReasonForIssuanceRequest, WebCheckinReasonForIssuanceResponse> {
	
	@Reference
	private WebCheckinEmdService webcheckinEmdService;
	
	@Override
	public WebCheckinReasonForIssuanceResponse executeOrchestration(WebCheckinReasonForIssuanceRequest request) {
		if (request == null)
			throw new IllegalArgumentException("WebCheckinReasonForIssuanceRequest is null.");
		return webcheckinEmdService.getReasonForIssuance(request);
	}
}