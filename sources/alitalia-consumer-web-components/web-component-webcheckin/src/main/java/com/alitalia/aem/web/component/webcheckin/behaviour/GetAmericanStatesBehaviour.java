package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.BaseRequest;
import com.alitalia.aem.common.messages.home.WebCheckinStatesResponse;
import com.alitalia.aem.service.api.home.WebCheckinStaticDataService;

@Component(immediate = true, metatype = false)
@Service(value = GetAmericanStatesBehaviour.class)
public class GetAmericanStatesBehaviour extends Behaviour<BaseRequest, WebCheckinStatesResponse> {
	
	@Reference
	private WebCheckinStaticDataService webcheckinStaticDataService;
	
	@Override
	public WebCheckinStatesResponse executeOrchestration(BaseRequest request) {
		return webcheckinStaticDataService.getAmericanStates(request);
	}
}