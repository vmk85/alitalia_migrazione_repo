package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.WebCheckinAlertRequest;
import com.alitalia.aem.common.messages.home.WebCheckinAlertResponse;
import com.alitalia.aem.service.api.home.WebCheckinAlertService;

@Component(immediate = true, metatype = false)
@Service(value = WebCheckinAlertBehaviour.class)
public class WebCheckinAlertBehaviour extends Behaviour<WebCheckinAlertRequest, WebCheckinAlertResponse> {

	@Reference
	private WebCheckinAlertService webcheckinAlertService;
	
	@Override
	public WebCheckinAlertResponse executeOrchestration(WebCheckinAlertRequest request) {
		if (request == null)
			throw new IllegalArgumentException("WebCheckinAlertRequest is null.");
		return webcheckinAlertService.submitUser(request);
	}
}
