package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.WebCheckinCancelCartRequest;
import com.alitalia.aem.common.messages.home.WebCheckinCancelCartResponse;
import com.alitalia.aem.service.api.home.WebCheckinAncillaryService;

@Component(immediate = true, metatype = false)
@Service(value = CancelCartBehaviour.class)
public class CancelCartBehaviour extends Behaviour<WebCheckinCancelCartRequest, WebCheckinCancelCartResponse> {
	
	@Reference
	private WebCheckinAncillaryService webCheckinAncillaryService;
	
	@Override
	public WebCheckinCancelCartResponse executeOrchestration(WebCheckinCancelCartRequest request) {
		if (request == null)
			throw new IllegalArgumentException("WebCheckinCancelCartRequest is null.");
		return webCheckinAncillaryService.cancelCart(request);
	}
}