package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.BaseResponse;
import com.alitalia.aem.common.messages.home.ComfortSeatOrderUpdateCheckinRequest;
import com.alitalia.aem.service.api.home.WebCheckinComfortSeatService;

@Component(immediate = true, metatype = false)
@Service(value = UpdateComfortSeatPurchaseIntoBOBehaviour.class)
public class UpdateComfortSeatPurchaseIntoBOBehaviour extends Behaviour<ComfortSeatOrderUpdateCheckinRequest, BaseResponse> {
	
	@Reference
	private WebCheckinComfortSeatService webcheckinComfortSeatService;
	
	@Override
	public BaseResponse executeOrchestration(ComfortSeatOrderUpdateCheckinRequest request) {
		if (request == null)
			throw new IllegalArgumentException("ComfortSeatOrderUpdateCheckinRequest is null.");
		return webcheckinComfortSeatService.updateComfortSeatPurchaseIntoBO(request);
	}

}
