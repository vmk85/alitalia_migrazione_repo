package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.PassengerListCheckinRequest;
import com.alitalia.aem.common.messages.home.PassengerListCheckinResponse;
import com.alitalia.aem.service.api.home.WebCheckinPassengerService;

@Component(immediate = true, metatype = false)
@Service(value = GetPassengerListBehaviour.class)
public class GetPassengerListBehaviour extends Behaviour<PassengerListCheckinRequest, PassengerListCheckinResponse> {
	
	@Reference
	private WebCheckinPassengerService webcheckinPassengerService;
	
	@Override
	public PassengerListCheckinResponse executeOrchestration(PassengerListCheckinRequest request) {
		if (request == null)
			throw new IllegalArgumentException("PassengerListCheckinRequest is null.");
		return webcheckinPassengerService.getPassengerList(request);
	}

}
