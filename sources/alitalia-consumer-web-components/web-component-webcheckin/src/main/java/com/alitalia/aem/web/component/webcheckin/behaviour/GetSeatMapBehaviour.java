package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.GetSeatMapCheckinRequest;
import com.alitalia.aem.common.messages.home.GetSeatMapCheckinResponse;
import com.alitalia.aem.service.api.home.WebCheckinSeatMapService;

@Component(immediate = true, metatype = false)
@Service(value = GetSeatMapBehaviour.class)
public class GetSeatMapBehaviour extends Behaviour<GetSeatMapCheckinRequest, GetSeatMapCheckinResponse> {
	
	@Reference
	private WebCheckinSeatMapService webcheckinSeatMapService;
	
	@Override
	public GetSeatMapCheckinResponse executeOrchestration(GetSeatMapCheckinRequest request) {
		if (request == null)
			throw new IllegalArgumentException("GetSeatMapCheckinRequest is null.");
		return webcheckinSeatMapService.getSeatMap(request);
	}

}
