package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.GetCountryRequest;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixResponse;
import com.alitalia.aem.service.api.home.WebCheckinStaticDataService;

@Component(immediate = true, metatype = false)
@Service(value = GetPrefixesBehaviour.class)
public class GetPrefixesBehaviour extends Behaviour<GetCountryRequest, RetrievePhonePrefixResponse> {
	
	@Reference
	private WebCheckinStaticDataService webcheckinStaticDataService;
	
	@Override
	public RetrievePhonePrefixResponse executeOrchestration(GetCountryRequest request) {
		if (request == null)
			throw new IllegalArgumentException("GetCountryRequest is null.");
		return webcheckinStaticDataService.getPrefixes(request);
	}
}