package com.alitalia.aem.web.component.webcheckin.delegate;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.component.AlitaliaWebComponentCommon;
import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.data.home.checkin.CheckinCountryData;
import com.alitalia.aem.common.data.home.checkin.CheckinCouponData;
import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.checkin.CheckinRouteData;
import com.alitalia.aem.common.messages.BaseRequest;
import com.alitalia.aem.common.messages.BaseResponse;
import com.alitalia.aem.common.messages.home.BoardingPassCheckinRequest;
import com.alitalia.aem.common.messages.home.BoardingPassCheckinResponse;
import com.alitalia.aem.common.messages.home.CanAddPassengerCheckinRequest;
import com.alitalia.aem.common.messages.home.CanAddPassengerCheckinResponse;
import com.alitalia.aem.common.messages.home.ChangeCabinCheckinRequest;
import com.alitalia.aem.common.messages.home.ChangeCabinCheckinResponse;
import com.alitalia.aem.common.messages.home.ChangeSeatCheckinRequest;
import com.alitalia.aem.common.messages.home.ChangeSeatCheckinResponse;
import com.alitalia.aem.common.messages.home.CheckinInsuranceResponse;
import com.alitalia.aem.common.messages.home.ComfortSeatOrderCheckinRequest;
import com.alitalia.aem.common.messages.home.ComfortSeatOrderUpdateCheckinRequest;
import com.alitalia.aem.common.messages.home.ComfortSeatPriceCheckinRequest;
import com.alitalia.aem.common.messages.home.ComfortSeatPriceCheckinResponse;
import com.alitalia.aem.common.messages.home.ComfortSeatPurchaseCheckinRequest;
import com.alitalia.aem.common.messages.home.ComfortSeatPurchaseCheckinResponse;
import com.alitalia.aem.common.messages.home.ExtraBaggageOrderCheckinRequest;
import com.alitalia.aem.common.messages.home.ExtraBaggageOrderCheckinResponse;
import com.alitalia.aem.common.messages.home.ExtraBaggagePolicyCheckinRequest;
import com.alitalia.aem.common.messages.home.ExtraBaggagePolicyCheckinResponse;
import com.alitalia.aem.common.messages.home.GenericServiceResponse;
import com.alitalia.aem.common.messages.home.GetCountryRequest;
import com.alitalia.aem.common.messages.home.GetCountryResponse;
import com.alitalia.aem.common.messages.home.GetSeatDisponibilityCheckinRequest;
import com.alitalia.aem.common.messages.home.GetSeatDisponibilityCheckinResponse;
import com.alitalia.aem.common.messages.home.GetSeatMapCheckinRequest;
import com.alitalia.aem.common.messages.home.GetSeatMapCheckinResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutInitRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutInitResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutStatusRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutStatusResponse;
import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesRequest;
import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesResponse;
import com.alitalia.aem.common.messages.home.PassengerListCheckinRequest;
import com.alitalia.aem.common.messages.home.PassengerListCheckinResponse;
import com.alitalia.aem.common.messages.home.PaymentCheckinRequest;
import com.alitalia.aem.common.messages.home.PaymentCheckinResponse;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixResponse;
import com.alitalia.aem.common.messages.home.RetrievePnrInformationCheckinResponse;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationRequest;
import com.alitalia.aem.common.messages.home.SendBoardingPassCheckinRequest;
import com.alitalia.aem.common.messages.home.SendBoardingPassCheckinResponse;
import com.alitalia.aem.common.messages.home.SendReceiptCheckinRequest;
import com.alitalia.aem.common.messages.home.SendReminderCheckinRequest;
import com.alitalia.aem.common.messages.home.SendReminderCheckinResponse;
import com.alitalia.aem.common.messages.home.UpgradePolicyCheckinRequest;
import com.alitalia.aem.common.messages.home.UpgradePolicyCheckinResponse;
import com.alitalia.aem.common.messages.home.ValidateMmUserCheckinRequest;
import com.alitalia.aem.common.messages.home.ValidateMmUserCheckinResponse;
import com.alitalia.aem.common.messages.home.WebCheckinAddToCartRequest;
import com.alitalia.aem.common.messages.home.WebCheckinAirportNameRequest;
import com.alitalia.aem.common.messages.home.WebCheckinAirportNameResponse;
import com.alitalia.aem.common.messages.home.WebCheckinAlertRequest;
import com.alitalia.aem.common.messages.home.WebCheckinAlertResponse;
import com.alitalia.aem.common.messages.home.WebCheckinAncillaryCartResponse;
import com.alitalia.aem.common.messages.home.WebCheckinAncillaryResponse;
import com.alitalia.aem.common.messages.home.WebCheckinBPPermissionRequest;
import com.alitalia.aem.common.messages.home.WebCheckinBPPermissionResponse;
import com.alitalia.aem.common.messages.home.WebCheckinCancelCartRequest;
import com.alitalia.aem.common.messages.home.WebCheckinCancelCartResponse;
import com.alitalia.aem.common.messages.home.WebCheckinEmdRequest;
import com.alitalia.aem.common.messages.home.WebCheckinEmdResponse;
import com.alitalia.aem.common.messages.home.WebCheckinFrequentFlyerResponse;
import com.alitalia.aem.common.messages.home.WebCheckinGetCartRequest;
import com.alitalia.aem.common.messages.home.WebCheckinGetCatalogRequest;
import com.alitalia.aem.common.messages.home.WebCheckinGetInsuranceRequest;
import com.alitalia.aem.common.messages.home.WebCheckinNewPassengerRequest;
import com.alitalia.aem.common.messages.home.WebCheckinNewPassengerResponse;
import com.alitalia.aem.common.messages.home.WebCheckinPayInsuranceRequest;
import com.alitalia.aem.common.messages.home.WebCheckinReasonForIssuanceRequest;
import com.alitalia.aem.common.messages.home.WebCheckinReasonForIssuanceResponse;
import com.alitalia.aem.common.messages.home.WebCheckinRemoveFromCartRequest;
import com.alitalia.aem.common.messages.home.WebCheckinRequest;
import com.alitalia.aem.common.messages.home.WebCheckinResponse;
import com.alitalia.aem.common.messages.home.WebCheckinStatesRequest;
import com.alitalia.aem.common.messages.home.WebCheckinStatesResponse;
import com.alitalia.aem.common.messages.home.WebCheckinUndoCheckinRequest;
import com.alitalia.aem.common.messages.home.WebCheckinUndoCheckinResponse;
import com.alitalia.aem.common.messages.home.WebCheckinUndoManyCheckinRequest;
import com.alitalia.aem.common.messages.home.WebCheckinUndoManyCheckinResponse;
import com.alitalia.aem.common.messages.home.WebCheckinUpdateCartRequest;
import com.alitalia.aem.common.utils.AlitaliaCommonUtils;
import com.alitalia.aem.web.component.webcheckin.behaviour.AddToCartAncillaryBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.AncillaryCartCheckOutInitBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.AncillaryCartCheckOutStatusInsuranceBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.CabinUpgradeEmdBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.CanAddPassengerBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.CancelCartBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.ChangeCabinBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.ChangeSeatBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.ComfortSeatEmdBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.EmdReasonForIssuanceBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.ExecutePaymentBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.ExistsComfortSeatPurchaseIntoBOBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.ExtraBaggageEmdBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.GetAirportNameBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.GetAmericanStatesBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.GetBPPermissionsBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.GetBaggagePolicyBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.GetBoardingPassBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.GetCartAncillaryBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.GetCatalogAncillaryBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.GetComfortSeatPriceBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.GetCountriesBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.GetExtraBaggageOrderBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.GetFrequentFlyersBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.GetInsuranceBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.GetPassengerListBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.GetPrefixesBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.GetSeatDisponibilityBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.GetSeatMapBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.GetStatesBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.GetUpgradePolicyBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.NewPassengerBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.PayInsuranceBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.RemoveFromCartAncillaryBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.SelectComfortSeatPurchaseBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.SendBoardingPassBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.SendEmailReceiptBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.SendReminderBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.UndoCheckinBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.UndoManyCheckinBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.UpdateCartAncillaryBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.UpdateComfortSeatPurchaseIntoBOBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.ValidateMmUserBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.WebCheckinAlertBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.WebCheckinBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.WebCheckinCheckFFCodeBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviour.WebCheckinRetrievePnrInformationBehaviour;

@Component(immediate=true, metatype=false)
@Properties({
	@Property( name = SimpleWebCheckinDelegate.CACHE_REFRESH_INTERVAL, label = "Refresh interval of the cache." ),
	@Property( name = SimpleWebCheckinDelegate.IS_CACHE_ENABLED, label = "Flag to enable cache." )
})
@Service
public class SimpleWebCheckinDelegate implements WebCheckinDelegate {
	
	private static final Logger logger = LoggerFactory.getLogger(SimpleWebCheckinDelegate.class);
	
	@Reference
	private AlitaliaWebComponentCommon alitaliaWebComponentCommon;

	@Reference
	private WebCheckinRetrievePnrInformationBehaviour retrievePnrInformationBehaviour;
	
	@Reference
	private ValidateMmUserBehaviour validateMmUserBehaviour;
	
	@Reference
	private WebCheckinBehaviour checkInBehaviour;
	
	@Reference
	private UndoCheckinBehaviour undoCheckinBehaviour;
	
	@Reference
	private GetPassengerListBehaviour getPassengerListBehaviour;
	
	@Reference
	private CanAddPassengerBehaviour canAddPassengerBehaviour;
	
	@Reference
	private NewPassengerBehaviour newPassengerBehaviour;
	
	@Reference
	private GetCountriesBehaviour getCountriesBehaviour;
	
	@Reference
	private GetBoardingPassBehaviour getBoardingPassBehaviour;
	
	@Reference
	private GetPrefixesBehaviour getPrefixesBehaviour;

	@Reference
	private SendBoardingPassBehaviour sendBoardingPassBehaviour;
	
	@Reference
	private SendReminderBehaviour sendReminderBehaviour;

	@Reference
	private GetBPPermissionsBehaviour getBPPermissionsBehaviour;
	
	@Reference
	private GetInsuranceBehaviour getInsuranceBehaviour;
	
	@Reference
	private PayInsuranceBehaviour payInsuranceBehaviour;
	
	@Reference
	private GetCatalogAncillaryBehaviour getCatalogBehaviour;
	
	@Reference
	private RemoveFromCartAncillaryBehaviour removeFromCartBehaviour;
	
	@Reference
	private UpdateCartAncillaryBehaviour updateCartBehaviour;
	
	@Reference
	private AddToCartAncillaryBehaviour addToCartBehaviour;
	
	@Reference
	private GetCartAncillaryBehaviour getCartBehaviour;
	
	@Reference
	private CancelCartBehaviour cancelCartBehaviour;
	
	@Reference
	private ExtraBaggageEmdBehaviour extraBaggageEmdBehaviour;
	
	@Reference
	private ComfortSeatEmdBehaviour comfortSeatEmdBehaviour;
	
	@Reference
	private CabinUpgradeEmdBehaviour cabinUpgradeEmdBehaviour;
	
	@Reference
	private EmdReasonForIssuanceBehaviour emdReasonForIssuanceBehaviour;

	@Reference
	private GetExtraBaggageOrderBehaviour getExtraBaggageOrderBehaviour;
	
	@Reference
	private GetBaggagePolicyBehaviour getBaggagePolicyBehaviour;
	
	@Reference
	private ExecutePaymentBehaviour executePaymentBehaviour;

	@Reference
	private SendEmailReceiptBehaviour sendEmailReceiptBehaviour;

	@Reference
	private SelectComfortSeatPurchaseBehaviour selectComfortSeatPurchaseBehaviour;
	
	@Reference
	private GetComfortSeatPriceBehaviour getComfortSeatPriceBehaviour;
	
	@Reference
	private ExistsComfortSeatPurchaseIntoBOBehaviour existsComfortSeatPurchaseIntoBOBehaviour;
	
	@Reference
	private UpdateComfortSeatPurchaseIntoBOBehaviour updateComfortSeatPurchaseIntoBOBehaviour;
	
	@Reference
	private GetFrequentFlyersBehaviour getFrequentFlyersBehaviour;
	
	@Reference
	private GetAmericanStatesBehaviour getAmericanStatesBehaviour;
	
	@Reference
	private GetStatesBehaviour getStatesBehaviour;
	
	@Reference
	private GetAirportNameBehaviour getAirportNameBehaviour;

	@Reference
	private GetSeatMapBehaviour getSeatMapBehaviour;

	@Reference
	private GetSeatDisponibilityBehaviour getSeatDisponibilityBehaviour;
	
	@Reference
	private ChangeSeatBehaviour changeSeatBehaviour;
	
	@Reference
	private ChangeCabinBehaviour changeCabinBehaviour;
	
	@Reference
	private GetUpgradePolicyBehaviour getUpgradePolicyBehaviour;
	
	@Reference
	private UndoManyCheckinBehaviour undoManyCheckinBehaviour;
	
	@Reference
	private WebCheckinAlertBehaviour webCheckinAlertBehaviour;
	
	@Reference
	private AncillaryCartCheckOutInitBehaviour ancillaryCartCheckOutInitBehaviour;

	@Reference
	private AncillaryCartCheckOutStatusInsuranceBehaviour ancillaryCartCheckOutStatusInsuranceBehaviour;
	
	@Reference
	private WebCheckinCheckFFCodeBehaviour webCheckinCheckFFCodeBehaviour;
	
	private long refreshInterval = 3600000L;
	private boolean isCacheEnabled = false;
	
	private static GetCountryResponse retrieveCountriesResponse;
	private static WebCheckinFrequentFlyerResponse retrieveFrequentFlyersResponse;
	private static RetrievePhonePrefixResponse retrievePhonePrefixResponse;
	private static WebCheckinAirportNameResponse retrieveAirportsResponse;
	private static WebCheckinStatesResponse retrieveUSStatesResponse;
	private static Hashtable<String, WebCheckinStatesResponse> cacheStatesMap = new Hashtable<String, WebCheckinStatesResponse>();


	
	private Object retrieveCountriesResponseLock = new Object();
	private Object retrieveFrequentFlyersResponseLock = new Object();
	private Object retrievePhonePrefixResponseLock = new Object();
	private Object retrieveAirportsResponseLock = new Object();
	private Object retrieveUSStateListResponseLock = new Object();
	private Object retrieveStateListResponseLock = new Object();
	
	private long lastCleanCountries = System.currentTimeMillis();
	private long lastCleanFrequentFlyers = System.currentTimeMillis();
	private long lastCleanPhonePrefix = System.currentTimeMillis();
	private long lastCleanAirports = System.currentTimeMillis();
	private long lastCleanStates = System.currentTimeMillis();
	private long lastCleanUSStates = System.currentTimeMillis();

	public static final String CACHE_REFRESH_INTERVAL = "cache.refresh.interval";
	public static final String IS_CACHE_ENABLED = "cache.is_enabled";

	//SEARCH
	@Override
	public RetrievePnrInformationCheckinResponse retrievePnrInformation(RetriveMmbPnrInformationRequest request) {
		BehaviourExecutor<WebCheckinRetrievePnrInformationBehaviour, RetriveMmbPnrInformationRequest, RetrievePnrInformationCheckinResponse> executor = new BehaviourExecutor<WebCheckinRetrievePnrInformationBehaviour, RetriveMmbPnrInformationRequest, RetrievePnrInformationCheckinResponse>();
		
		RetrievePnrInformationCheckinResponse checkinResponse = executor.executeBehaviour(retrievePnrInformationBehaviour, request);
		if (checkinResponse != null) {
			searchForABus(checkinResponse);
		}
		return checkinResponse;
	}
	
	@Override
	public ValidateMmUserCheckinResponse validateMmUser(ValidateMmUserCheckinRequest request) {
		BehaviourExecutor<ValidateMmUserBehaviour, ValidateMmUserCheckinRequest, ValidateMmUserCheckinResponse> executor = new BehaviourExecutor<ValidateMmUserBehaviour, ValidateMmUserCheckinRequest, ValidateMmUserCheckinResponse>();
		return executor.executeBehaviour(validateMmUserBehaviour, request);
	}
	
	@Override
	public WebCheckinResponse checkIn(WebCheckinRequest request) {
		BehaviourExecutor<WebCheckinBehaviour, WebCheckinRequest, WebCheckinResponse> executor = new BehaviourExecutor<WebCheckinBehaviour, WebCheckinRequest, WebCheckinResponse>();
		WebCheckinResponse checkinResponse = executor.executeBehaviour(checkInBehaviour, request);
		if (checkinResponse != null) {
			if (checkinResponse.getPassengers() != null) {
				searchForABusPassenger(checkinResponse.getPassengers());
			}
		}
		return checkinResponse;
	}
	
	@Override
	public WebCheckinUndoCheckinResponse undoCheckin(WebCheckinUndoCheckinRequest request) {
		BehaviourExecutor<UndoCheckinBehaviour, WebCheckinUndoCheckinRequest, WebCheckinUndoCheckinResponse> executor = new BehaviourExecutor<UndoCheckinBehaviour, WebCheckinUndoCheckinRequest, WebCheckinUndoCheckinResponse>();
		return executor.executeBehaviour(undoCheckinBehaviour, request);
	}
	
	@Override
	public WebCheckinUndoManyCheckinResponse undoManyCheckin(WebCheckinUndoManyCheckinRequest request) {
		BehaviourExecutor<UndoManyCheckinBehaviour, WebCheckinUndoManyCheckinRequest, WebCheckinUndoManyCheckinResponse> executor = new BehaviourExecutor<UndoManyCheckinBehaviour, WebCheckinUndoManyCheckinRequest, WebCheckinUndoManyCheckinResponse>();
		return executor.executeBehaviour(undoManyCheckinBehaviour, request);
	}

	
	@Override
	public PassengerListCheckinResponse getPassengerList(PassengerListCheckinRequest request) {
		BehaviourExecutor<GetPassengerListBehaviour, PassengerListCheckinRequest, PassengerListCheckinResponse> executor = new BehaviourExecutor<GetPassengerListBehaviour, PassengerListCheckinRequest, PassengerListCheckinResponse>();
		PassengerListCheckinResponse passengerListCheckinResponse = executor.executeBehaviour(getPassengerListBehaviour, request);
		if (passengerListCheckinResponse != null) {
			searchForABusPassenger(passengerListCheckinResponse.getPassengers());
		}
		return passengerListCheckinResponse;
	}
	
	@Override
	public CanAddPassengerCheckinResponse canAddPassenger(CanAddPassengerCheckinRequest request) {
		BehaviourExecutor<CanAddPassengerBehaviour, CanAddPassengerCheckinRequest, CanAddPassengerCheckinResponse> executor = new BehaviourExecutor<CanAddPassengerBehaviour, CanAddPassengerCheckinRequest, CanAddPassengerCheckinResponse>();
		return executor.executeBehaviour(canAddPassengerBehaviour, request);
	}
	
	@Override
	public WebCheckinNewPassengerResponse getNewPassenger(WebCheckinNewPassengerRequest request) {
		BehaviourExecutor<NewPassengerBehaviour, WebCheckinNewPassengerRequest, WebCheckinNewPassengerResponse> executor = new BehaviourExecutor<NewPassengerBehaviour, WebCheckinNewPassengerRequest, WebCheckinNewPassengerResponse>();
		WebCheckinNewPassengerResponse checkinNewPassengerResponse = executor.executeBehaviour(newPassengerBehaviour, request);
		if (checkinNewPassengerResponse != null) {
			if (checkinNewPassengerResponse.getPassenger() != null) {
				List<CheckinPassengerData> passengers  = new ArrayList<CheckinPassengerData>();
				passengers.add(checkinNewPassengerResponse.getPassenger());
				searchForABusPassenger(passengers);
			}
		}
		return checkinNewPassengerResponse;
	}
	
	@Override
	public GetCountryResponse getCountries(GetCountryRequest request) {

		BehaviourExecutor<GetCountriesBehaviour, GetCountryRequest, GetCountryResponse> executor = new BehaviourExecutor<GetCountriesBehaviour, GetCountryRequest, GetCountryResponse>();

		logger.debug("Executing checkin delegate getCountries");
		
		synchronized(retrieveCountriesResponseLock) {
			if (isCacheEnabled) {
				if (isCacheToBeRefreshed(lastCleanCountries)){
					retrieveCountriesResponse = null;
					lastCleanCountries = System.currentTimeMillis();
				}

				if (retrieveCountriesResponse == null || retrieveCountriesResponse.getCountries() == null || retrieveCountriesResponse.getCountries().size()==0) {
					retrieveCountriesResponse = executor.executeBehaviour(getCountriesBehaviour, request);
				} else {
					logger.info("Checkin getCountries: data retrieved from cache");
				}

				// Crea una copia del dato per evitare side effect
				GetCountryResponse response = new GetCountryResponse(retrieveCountriesResponse.getTid(), retrieveCountriesResponse.getSid());
				response.setCountries(new ArrayList<CheckinCountryData>());
				for (CheckinCountryData sourceData : retrieveCountriesResponse.getCountries())
					response.getCountries().add(new CheckinCountryData(sourceData.getCountryCode(), sourceData.getCountryISOCode(), 
							sourceData.getCountryName(), sourceData.getId()));

				return response;
			} else {
				logger.debug("Checkin Cache manager disabled - starting service call to get countries list");
				return executor.executeBehaviour(getCountriesBehaviour, request);
			}
		}
	}
	
	@Override
	public BoardingPassCheckinResponse getBoardingPass(BoardingPassCheckinRequest request) {
		BehaviourExecutor<GetBoardingPassBehaviour, BoardingPassCheckinRequest, BoardingPassCheckinResponse> executor = new BehaviourExecutor<GetBoardingPassBehaviour, BoardingPassCheckinRequest, BoardingPassCheckinResponse>();
		BoardingPassCheckinResponse boardingPassCheckinResponse = executor.executeBehaviour(getBoardingPassBehaviour, request);
		if (boardingPassCheckinResponse != null) {
			if (boardingPassCheckinResponse.getPassengers() != null) {
				searchForABusPassenger(boardingPassCheckinResponse.getPassengers());
			}
			
			if (alitaliaWebComponentCommon.getCheckinBusEnabled()) {
				if (boardingPassCheckinResponse.getSelectedRoute() != null) {
					if (manageCheckinFlightDataForBus(boardingPassCheckinResponse.getSelectedRoute().getFlights())) {
						boardingPassCheckinResponse.getSelectedRoute().setBus(true);
					}
				}
			}
		}
		return boardingPassCheckinResponse;
	}
	
	@Override
	public RetrievePhonePrefixResponse getPhonePrefixes(GetCountryRequest request) {
		
		BehaviourExecutor<GetPrefixesBehaviour, GetCountryRequest, RetrievePhonePrefixResponse> executor = new BehaviourExecutor<GetPrefixesBehaviour, GetCountryRequest, RetrievePhonePrefixResponse>();

		logger.debug("Executing checkin delegate getPhonePrefixes");
		
		synchronized(retrievePhonePrefixResponseLock) {
			
			if (isCacheEnabled) {
				if (isCacheToBeRefreshed(lastCleanPhonePrefix)){
					retrievePhonePrefixResponse = null;
					lastCleanPhonePrefix = System.currentTimeMillis();
				}

				if (retrievePhonePrefixResponse == null || retrievePhonePrefixResponse.getPhonePrefix() == null || retrievePhonePrefixResponse.getPhonePrefix().size()==0) {
					retrievePhonePrefixResponse = executor.executeBehaviour(getPrefixesBehaviour, request);
				} else {
					logger.info("Checkin getPhonePrefixes: data retrieved from cache");
				}

				return retrievePhonePrefixResponse;
			} else {
				logger.debug("Checkin Cache manager disabled - starting service call to get phones prefix list");
				return executor.executeBehaviour(getPrefixesBehaviour, request);
			}
		}
	}
	
	@Override
	public SendBoardingPassCheckinResponse sendBoardingPass(SendBoardingPassCheckinRequest request) {
		BehaviourExecutor<SendBoardingPassBehaviour, SendBoardingPassCheckinRequest, SendBoardingPassCheckinResponse> executor = new BehaviourExecutor<SendBoardingPassBehaviour, SendBoardingPassCheckinRequest, SendBoardingPassCheckinResponse>();
		return executor.executeBehaviour(sendBoardingPassBehaviour, request);
	}
	
	@Override
	public SendReminderCheckinResponse sendReminder(SendReminderCheckinRequest request) {
		BehaviourExecutor<SendReminderBehaviour, SendReminderCheckinRequest, SendReminderCheckinResponse> executor = new BehaviourExecutor<SendReminderBehaviour, SendReminderCheckinRequest, SendReminderCheckinResponse>();
		return executor.executeBehaviour(sendReminderBehaviour, request);
	}

	@Override
	public WebCheckinBPPermissionResponse getBoardingPassPermissions(WebCheckinBPPermissionRequest request) {
		BehaviourExecutor<GetBPPermissionsBehaviour, WebCheckinBPPermissionRequest, WebCheckinBPPermissionResponse> executor = new BehaviourExecutor<GetBPPermissionsBehaviour, WebCheckinBPPermissionRequest, WebCheckinBPPermissionResponse>();
		return executor.executeBehaviour(getBPPermissionsBehaviour, request);
	}
	
	@Override
	public WebCheckinFrequentFlyerResponse getFrequentFlyers(GetCountryRequest request) {
		
		BehaviourExecutor<GetFrequentFlyersBehaviour, GetCountryRequest, WebCheckinFrequentFlyerResponse> executor = new BehaviourExecutor<GetFrequentFlyersBehaviour, GetCountryRequest, WebCheckinFrequentFlyerResponse>();
		logger.debug("Executing checkin delegate getFrequentFlyers");
		
		synchronized(retrieveFrequentFlyersResponseLock) {
			
			if (isCacheEnabled) {
				if (isCacheToBeRefreshed(lastCleanFrequentFlyers)){
					retrieveFrequentFlyersResponse = null;
					lastCleanFrequentFlyers = System.currentTimeMillis();
				}

				if (retrieveFrequentFlyersResponse == null || retrieveFrequentFlyersResponse.getFrequentFlyers() == null || retrieveFrequentFlyersResponse.getFrequentFlyers().size()==0) {
					retrieveFrequentFlyersResponse = executor.executeBehaviour(getFrequentFlyersBehaviour, request);
				} else {
					logger.info("Checkin getFrequentFlyers: data retrieved from cache");
				}

				return retrieveFrequentFlyersResponse;
			} else {
				logger.debug("Checkin Cache manager disabled - starting service call to get frequent flyers type list");
				return executor.executeBehaviour(getFrequentFlyersBehaviour, request);
			}
			
		}
	}
	
	@Override
	public WebCheckinStatesResponse getAmericanStates(BaseRequest request) {
		
		BehaviourExecutor<GetAmericanStatesBehaviour, BaseRequest, WebCheckinStatesResponse> executor = new BehaviourExecutor<GetAmericanStatesBehaviour, BaseRequest, WebCheckinStatesResponse>();
		logger.debug("Executing checkin delegate getAmericanStates");
		
		synchronized(retrieveUSStateListResponseLock) {
			
			if (isCacheEnabled) {
				if (isCacheToBeRefreshed(lastCleanUSStates)){
					retrieveUSStatesResponse = null;
					lastCleanUSStates = System.currentTimeMillis();
				}
				
				if (retrieveUSStatesResponse == null || retrieveUSStatesResponse.getAmericanStates() == null || retrieveUSStatesResponse.getAmericanStates().size()==0) {
					retrieveUSStatesResponse = executor.executeBehaviour(getAmericanStatesBehaviour, request);
				} else {
					logger.info("Checkin getAmericanStates: data retrieved from cache");
				}

				return retrieveUSStatesResponse;
			} else {
				logger.debug("Checkin Cache manager disabled - starting service call to get american states list");
				return executor.executeBehaviour(getAmericanStatesBehaviour, request);
			}
		}
	}
	
	@Override
	public WebCheckinStatesResponse getStates(WebCheckinStatesRequest request) {

		BehaviourExecutor<GetStatesBehaviour, WebCheckinStatesRequest, WebCheckinStatesResponse> executor = new BehaviourExecutor<GetStatesBehaviour, WebCheckinStatesRequest, WebCheckinStatesResponse>();
		logger.debug("Executing checkin delegate getStates");
		
		synchronized(retrieveStateListResponseLock) {
			
			if (isCacheEnabled) {
				if (isCacheToBeRefreshed(lastCleanStates)){
					cacheStatesMap.clear();
					lastCleanStates = System.currentTimeMillis();
				}

				if (cacheStatesMap == null || cacheStatesMap.size() == 0 || 
						cacheStatesMap.get(request.getCountryCode()) == null || cacheStatesMap.get(request.getCountryCode()).getAmericanStates() == null || 
						cacheStatesMap.get(request.getCountryCode()).getAmericanStates().size() == 0) {
						cacheStatesMap.put(request.getCountryCode(), executor.executeBehaviour(getStatesBehaviour, request));
				} else {
					logger.info("Checkin getStates: data retrieved from cache");
				}

				return cacheStatesMap.get(request.getCountryCode());

			} else {
				logger.debug("Checkin Cache manager disabled - starting service call to get states list");
				return executor.executeBehaviour(getStatesBehaviour, request);
			}
		}
	}
	
	@Override
	public WebCheckinAirportNameResponse getAirportNames(WebCheckinAirportNameRequest request) {
		
		BehaviourExecutor<GetAirportNameBehaviour, WebCheckinAirportNameRequest, WebCheckinAirportNameResponse> executor = new BehaviourExecutor<GetAirportNameBehaviour, WebCheckinAirportNameRequest, WebCheckinAirportNameResponse>();
		logger.debug("Executing checkin delegate getAirportNames");
		
		synchronized(retrieveAirportsResponseLock) {
			
			if (isCacheEnabled) {
				if (isCacheToBeRefreshed(lastCleanAirports)){
					retrieveAirportsResponse = null;
					lastCleanAirports = System.currentTimeMillis();
				}
				
				if (retrieveAirportsResponse == null || retrieveAirportsResponse.getAirports() == null || retrieveAirportsResponse.getAirports().size()==0) {
					retrieveAirportsResponse = executor.executeBehaviour(getAirportNameBehaviour, request);
				} else {
					logger.info("Checkin getAirportNames: data retrieved from cache");
				}

				return retrieveAirportsResponse;
			} else {
				logger.debug("Checkin Cache manager disabled - starting service call to get airportNames list");
				return executor.executeBehaviour(getAirportNameBehaviour, request);
			}
		}
	}
	
	@Override
	public CheckinInsuranceResponse getInsuranceInfo(WebCheckinGetInsuranceRequest request) {
		BehaviourExecutor<GetInsuranceBehaviour, WebCheckinGetInsuranceRequest, CheckinInsuranceResponse> executor = new BehaviourExecutor<GetInsuranceBehaviour, WebCheckinGetInsuranceRequest, CheckinInsuranceResponse>();
		return executor.executeBehaviour(getInsuranceBehaviour, request);
	}
	
	@Override
	public CheckinInsuranceResponse payInsurance(WebCheckinPayInsuranceRequest request) {
		BehaviourExecutor<PayInsuranceBehaviour, WebCheckinPayInsuranceRequest, CheckinInsuranceResponse> executor = new BehaviourExecutor<PayInsuranceBehaviour, WebCheckinPayInsuranceRequest, CheckinInsuranceResponse>();
		return executor.executeBehaviour(payInsuranceBehaviour, request);
	}
	
	@Override
	public WebCheckinAncillaryCartResponse getCatalog(WebCheckinGetCatalogRequest request) {
		BehaviourExecutor<GetCatalogAncillaryBehaviour, WebCheckinGetCatalogRequest, WebCheckinAncillaryCartResponse> executor = new BehaviourExecutor<GetCatalogAncillaryBehaviour, WebCheckinGetCatalogRequest, WebCheckinAncillaryCartResponse>();
		return executor.executeBehaviour(getCatalogBehaviour, request);
	}
	
	@Override
	public WebCheckinAncillaryResponse addToCart(WebCheckinAddToCartRequest request) {
		BehaviourExecutor<AddToCartAncillaryBehaviour, WebCheckinAddToCartRequest, WebCheckinAncillaryResponse> executor = new BehaviourExecutor<AddToCartAncillaryBehaviour, WebCheckinAddToCartRequest, WebCheckinAncillaryResponse>();
		return executor.executeBehaviour(addToCartBehaviour, request);
	}
	
	@Override
	public WebCheckinAncillaryResponse updateCart(WebCheckinUpdateCartRequest request) {
		BehaviourExecutor<UpdateCartAncillaryBehaviour, WebCheckinUpdateCartRequest, WebCheckinAncillaryResponse> executor = new BehaviourExecutor<UpdateCartAncillaryBehaviour, WebCheckinUpdateCartRequest, WebCheckinAncillaryResponse>();
		return executor.executeBehaviour(updateCartBehaviour, request);
	}
	
	@Override
	public WebCheckinAncillaryResponse removeFromCart(WebCheckinRemoveFromCartRequest request) {
		BehaviourExecutor<RemoveFromCartAncillaryBehaviour, WebCheckinRemoveFromCartRequest, WebCheckinAncillaryResponse> executor = new BehaviourExecutor<RemoveFromCartAncillaryBehaviour, WebCheckinRemoveFromCartRequest, WebCheckinAncillaryResponse>();
		return executor.executeBehaviour(removeFromCartBehaviour, request);
	}

	@Override
	public WebCheckinAncillaryCartResponse getCart(WebCheckinGetCartRequest request) {
		BehaviourExecutor<GetCartAncillaryBehaviour, WebCheckinGetCartRequest, WebCheckinAncillaryCartResponse> executor = new BehaviourExecutor<GetCartAncillaryBehaviour, WebCheckinGetCartRequest, WebCheckinAncillaryCartResponse>();
		return executor.executeBehaviour(getCartBehaviour, request);
	}
	
	@Override
	public WebCheckinCancelCartResponse cancelCart(WebCheckinCancelCartRequest request) {
		BehaviourExecutor<CancelCartBehaviour, WebCheckinCancelCartRequest, WebCheckinCancelCartResponse> executor = new BehaviourExecutor<CancelCartBehaviour, WebCheckinCancelCartRequest, WebCheckinCancelCartResponse>();
		return executor.executeBehaviour(cancelCartBehaviour, request);
	}
	
	@Override
	public MmbAncillaryCheckOutInitResponse checkOutInit(MmbAncillaryCheckOutInitRequest request) {
		BehaviourExecutor<AncillaryCartCheckOutInitBehaviour, MmbAncillaryCheckOutInitRequest, MmbAncillaryCheckOutInitResponse> executor = new BehaviourExecutor<AncillaryCartCheckOutInitBehaviour, MmbAncillaryCheckOutInitRequest, MmbAncillaryCheckOutInitResponse>();
		return executor.executeBehaviour(ancillaryCartCheckOutInitBehaviour, request);
	}

	@Override
	public MmbAncillaryCheckOutStatusResponse getCheckOutStatus(MmbAncillaryCheckOutStatusRequest request) {
		BehaviourExecutor<AncillaryCartCheckOutStatusInsuranceBehaviour, MmbAncillaryCheckOutStatusRequest, MmbAncillaryCheckOutStatusResponse> executor = new BehaviourExecutor<AncillaryCartCheckOutStatusInsuranceBehaviour, MmbAncillaryCheckOutStatusRequest, MmbAncillaryCheckOutStatusResponse>();
		return executor.executeBehaviour(ancillaryCartCheckOutStatusInsuranceBehaviour, request);
	}
	
	@Override
	public WebCheckinEmdResponse extraBaggageEmd(WebCheckinEmdRequest request) {
		BehaviourExecutor<ExtraBaggageEmdBehaviour, WebCheckinEmdRequest, WebCheckinEmdResponse> executor = new BehaviourExecutor<ExtraBaggageEmdBehaviour, WebCheckinEmdRequest, WebCheckinEmdResponse>();
		return executor.executeBehaviour(extraBaggageEmdBehaviour, request);
	}
	
	@Override
	public WebCheckinEmdResponse comfortSeatEmd(WebCheckinEmdRequest request) {
		BehaviourExecutor<ComfortSeatEmdBehaviour, WebCheckinEmdRequest, WebCheckinEmdResponse> executor = new BehaviourExecutor<ComfortSeatEmdBehaviour, WebCheckinEmdRequest, WebCheckinEmdResponse>();
		return executor.executeBehaviour(comfortSeatEmdBehaviour, request);
	}
	
	@Override
	public WebCheckinEmdResponse cabinUpgradeEmd(WebCheckinEmdRequest request) {
		BehaviourExecutor<CabinUpgradeEmdBehaviour, WebCheckinEmdRequest, WebCheckinEmdResponse> executor = new BehaviourExecutor<CabinUpgradeEmdBehaviour, WebCheckinEmdRequest, WebCheckinEmdResponse>();
		return executor.executeBehaviour(cabinUpgradeEmdBehaviour, request);
	}
	
	@Override
	public WebCheckinReasonForIssuanceResponse getReasonForIssuance(WebCheckinReasonForIssuanceRequest request) {
		BehaviourExecutor<EmdReasonForIssuanceBehaviour, WebCheckinReasonForIssuanceRequest, WebCheckinReasonForIssuanceResponse> executor = new BehaviourExecutor<EmdReasonForIssuanceBehaviour, WebCheckinReasonForIssuanceRequest, WebCheckinReasonForIssuanceResponse>();
		return executor.executeBehaviour(emdReasonForIssuanceBehaviour, request);
	}
	
	@Override
	public ExtraBaggageOrderCheckinResponse getExtraBaggageOrder(ExtraBaggageOrderCheckinRequest request) {
		BehaviourExecutor<GetExtraBaggageOrderBehaviour, ExtraBaggageOrderCheckinRequest, ExtraBaggageOrderCheckinResponse> executor = new BehaviourExecutor<GetExtraBaggageOrderBehaviour, ExtraBaggageOrderCheckinRequest, ExtraBaggageOrderCheckinResponse>();
		return executor.executeBehaviour(getExtraBaggageOrderBehaviour, request);
	}
	
	@Override
	public ExtraBaggagePolicyCheckinResponse getBaggagePolicy(ExtraBaggagePolicyCheckinRequest request) {
		BehaviourExecutor<GetBaggagePolicyBehaviour, ExtraBaggagePolicyCheckinRequest, ExtraBaggagePolicyCheckinResponse> executor = new BehaviourExecutor<GetBaggagePolicyBehaviour, ExtraBaggagePolicyCheckinRequest, ExtraBaggagePolicyCheckinResponse>();
		return executor.executeBehaviour(getBaggagePolicyBehaviour, request);
	}
	
	@Override
	public PaymentCheckinResponse executePayment(PaymentCheckinRequest request) {
		BehaviourExecutor<ExecutePaymentBehaviour, PaymentCheckinRequest, PaymentCheckinResponse> executor = new BehaviourExecutor<ExecutePaymentBehaviour, PaymentCheckinRequest, PaymentCheckinResponse>();
		return executor.executeBehaviour(executePaymentBehaviour, request);
	}
	
	@Override
	public GenericServiceResponse sendEmailReceipt(SendReceiptCheckinRequest request) {
		BehaviourExecutor<SendEmailReceiptBehaviour, SendReceiptCheckinRequest, GenericServiceResponse> executor = new BehaviourExecutor<SendEmailReceiptBehaviour, SendReceiptCheckinRequest, GenericServiceResponse>();
		return executor.executeBehaviour(sendEmailReceiptBehaviour, request);
	}
	
	@Override
	public ComfortSeatPurchaseCheckinResponse selectComfortSeatPurchase(ComfortSeatPurchaseCheckinRequest request) {
		BehaviourExecutor<SelectComfortSeatPurchaseBehaviour, ComfortSeatPurchaseCheckinRequest, ComfortSeatPurchaseCheckinResponse> executor = new BehaviourExecutor<SelectComfortSeatPurchaseBehaviour, ComfortSeatPurchaseCheckinRequest, ComfortSeatPurchaseCheckinResponse>();
		return executor.executeBehaviour(selectComfortSeatPurchaseBehaviour, request);
	}
	
	@Override
	public ComfortSeatPriceCheckinResponse getComfortSeatPrice(ComfortSeatPriceCheckinRequest request) {
		BehaviourExecutor<GetComfortSeatPriceBehaviour, ComfortSeatPriceCheckinRequest, ComfortSeatPriceCheckinResponse> executor = new BehaviourExecutor<GetComfortSeatPriceBehaviour, ComfortSeatPriceCheckinRequest, ComfortSeatPriceCheckinResponse>();
		return executor.executeBehaviour(getComfortSeatPriceBehaviour, request);
	}
	
	@Override
	public GenericServiceResponse existsComfortSeatPurchaseIntoBO(ComfortSeatOrderCheckinRequest request) {
		BehaviourExecutor<ExistsComfortSeatPurchaseIntoBOBehaviour, ComfortSeatOrderCheckinRequest, GenericServiceResponse> executor = new BehaviourExecutor<ExistsComfortSeatPurchaseIntoBOBehaviour, ComfortSeatOrderCheckinRequest, GenericServiceResponse>();
		return executor.executeBehaviour(existsComfortSeatPurchaseIntoBOBehaviour, request);
	}
	
	@Override
	public BaseResponse updateComfortSeatPurchaseIntoBO(ComfortSeatOrderUpdateCheckinRequest request) {
		BehaviourExecutor<UpdateComfortSeatPurchaseIntoBOBehaviour, ComfortSeatOrderUpdateCheckinRequest, BaseResponse> executor = new BehaviourExecutor<UpdateComfortSeatPurchaseIntoBOBehaviour, ComfortSeatOrderUpdateCheckinRequest, BaseResponse>();
		return executor.executeBehaviour(updateComfortSeatPurchaseIntoBOBehaviour, request);
	}
	
	@Override
	public GetSeatMapCheckinResponse getSeatMap(GetSeatMapCheckinRequest request) {
		BehaviourExecutor<GetSeatMapBehaviour, GetSeatMapCheckinRequest, GetSeatMapCheckinResponse> executor = new BehaviourExecutor<GetSeatMapBehaviour, GetSeatMapCheckinRequest, GetSeatMapCheckinResponse>();
		return executor.executeBehaviour(getSeatMapBehaviour, request);
	}
	
	@Override
	public GetSeatDisponibilityCheckinResponse getSeatDisponibility(GetSeatDisponibilityCheckinRequest request) {
		BehaviourExecutor<GetSeatDisponibilityBehaviour, GetSeatDisponibilityCheckinRequest, GetSeatDisponibilityCheckinResponse> executor = new BehaviourExecutor<GetSeatDisponibilityBehaviour, GetSeatDisponibilityCheckinRequest, GetSeatDisponibilityCheckinResponse>();
		return executor.executeBehaviour(getSeatDisponibilityBehaviour, request);
	}
	
	@Override
	public ChangeSeatCheckinResponse changeSeat(ChangeSeatCheckinRequest request) {
		BehaviourExecutor<ChangeSeatBehaviour, ChangeSeatCheckinRequest, ChangeSeatCheckinResponse> executor = new BehaviourExecutor<ChangeSeatBehaviour, ChangeSeatCheckinRequest, ChangeSeatCheckinResponse>();
		return executor.executeBehaviour(changeSeatBehaviour, request);
	}
	
	@Override
	public ChangeCabinCheckinResponse changeCabin(ChangeCabinCheckinRequest request) {
		BehaviourExecutor<ChangeCabinBehaviour, ChangeCabinCheckinRequest, ChangeCabinCheckinResponse> executor = new BehaviourExecutor<ChangeCabinBehaviour, ChangeCabinCheckinRequest, ChangeCabinCheckinResponse>();
		return executor.executeBehaviour(changeCabinBehaviour, request);
	}
	
	@Override
	public UpgradePolicyCheckinResponse getUpgradePolicy(UpgradePolicyCheckinRequest request) {
		BehaviourExecutor<GetUpgradePolicyBehaviour, UpgradePolicyCheckinRequest, UpgradePolicyCheckinResponse> executor = new BehaviourExecutor<GetUpgradePolicyBehaviour, UpgradePolicyCheckinRequest, UpgradePolicyCheckinResponse>();
		return executor.executeBehaviour(getUpgradePolicyBehaviour, request);
	}
	
	@Override
	public WebCheckinAlertResponse submitUser(WebCheckinAlertRequest request) {
		BehaviourExecutor<WebCheckinAlertBehaviour, WebCheckinAlertRequest, WebCheckinAlertResponse> executor = new BehaviourExecutor<WebCheckinAlertBehaviour, WebCheckinAlertRequest, WebCheckinAlertResponse>();
		return executor.executeBehaviour(webCheckinAlertBehaviour, request);
	}
	
	@Override
	public MmbCheckFrequentFlyerCodesResponse checkFFCode(MmbCheckFrequentFlyerCodesRequest request) {
		BehaviourExecutor<WebCheckinCheckFFCodeBehaviour, MmbCheckFrequentFlyerCodesRequest, MmbCheckFrequentFlyerCodesResponse> executor = new BehaviourExecutor<WebCheckinCheckFFCodeBehaviour, MmbCheckFrequentFlyerCodesRequest, MmbCheckFrequentFlyerCodesResponse>();
		return executor.executeBehaviour(webCheckinCheckFFCodeBehaviour, request);
	}
	
	private boolean isCacheToBeRefreshed(Long lastClean) {
		long currentTime = System.currentTimeMillis();
		logger.debug("WebCheckinStaticDataDelegate.checkIsCacheToBeRefreshed - currentTime is [" + currentTime + "], lastClean is [" + lastClean + "] and refreshInterval is [" + refreshInterval + "]");
		if (currentTime - lastClean > refreshInterval) {
			logger.debug("WebCheckinStaticDataDelegate.checkIsCacheToBeRefreshed - clear cache");
			return true;
		}
		return false;
	}
	
	/**
	 * It obtains a list of directFligth for the current response
	 * @param response
	 */
	private void searchForABus(RetrievePnrInformationCheckinResponse response) {
		boolean isBusCarrier = false;
		
		if (!alitaliaWebComponentCommon.getCheckinBusEnabled()) {
			return;
		}
		
		if (response != null) {
			List<CheckinRouteData> routes = response.getRoutesList();
			if (routes != null) {
				for (CheckinRouteData route : routes) {
					if (manageCheckinFlightDataForBus(route.getFlights())) {
						isBusCarrier = true;
						route.setBus(true);
					}
					if (manageCheckinFlightDataForBus(route.getInterlineFlights())) {
						isBusCarrier = true;
						route.setBus(true);
					}
					manageCheckinRouteDataForBus(route);
				}
			}
			searchForABusPassenger(response.getPassengersList());
			response.setBusCarrier(isBusCarrier);
		}
	}
	
	private void searchForABusPassenger(List<CheckinPassengerData> passengers) {
		if (!alitaliaWebComponentCommon.getCheckinBusEnabled()) {
			return;
		}
		
		if (passengers != null) {
			for (CheckinPassengerData pax : passengers) {
				if (pax.getCoupons() != null) {
					for (CheckinCouponData coupon : pax.getCoupons()) {
						List<CheckinFlightData> flightList = new ArrayList<CheckinFlightData>();
						if (coupon.getFlight() != null) {
							flightList.add(coupon.getFlight());
						}
						manageCheckinFlightDataForBus(flightList);
					}
				}
			}
		}
	}
	
	/**
	 * It set the attribut fromBusStation and toBusStation according to the airport
	 * in configuration in order to check if the selectedRoute for the checkin is departing 
	 * from a bus station or is arriving to a bus station.
	 * @param route
	 */
	private void manageCheckinRouteDataForBus(CheckinRouteData route) {
		List<CheckinFlightData> flights = AlitaliaCommonUtils.getFlightsList(route);
		if (flights != null && !flights.isEmpty()) {
			CheckinFlightData firstFlight = flights.get(0);
			CheckinFlightData lastFlight = flights.get(flights.size() - 1);
			route.setFromBusStation(alitaliaWebComponentCommon.isBusStation(firstFlight.getFrom().getCode()));
			route.setToBusStation(alitaliaWebComponentCommon.isBusStation(lastFlight.getTo().getCode()));
		}
	}

	/**
	 * It sets the bus attribute of a DirectFlightData
	 * according to its fligthNumber and the range provided by configuration 
	 * @param flightList
	 */
	private boolean manageCheckinFlightDataForBus(List<CheckinFlightData> flightList) {
		boolean isBusCarrier = false;
		if (flightList != null) {
			for (CheckinFlightData flight : flightList) {
				int fligthNumber = -1;
				try {
					fligthNumber = Integer.parseInt(flight.getFlightNumber());
				} catch (NumberFormatException e) {
					logger.error("Error converting fligthNumber in Integer: ", e);
				}
				boolean bus = alitaliaWebComponentCommon.isBusNumber(fligthNumber);
				boolean fromBusStation = false;
				boolean toBusStation = false;
				if (bus) {
					isBusCarrier = true;
					fromBusStation = alitaliaWebComponentCommon.isBusStation(flight.getFrom().getCode());
					toBusStation = alitaliaWebComponentCommon.isBusStation(flight.getTo().getCode());
				}
				flight.setBus(bus);
				flight.setFromBusStation(fromBusStation);
				flight.setToBusStation(toBusStation);
			}
		}
		return isBusCarrier;
	}
	
	@Activate
	private void activate(ComponentContext componentContext) {
		try {		
			isCacheEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_CACHE_ENABLED), false);
			refreshInterval = PropertiesUtil.toLong(
					componentContext.getProperties().get(CACHE_REFRESH_INTERVAL), 3600000);
		} catch (Exception e) {
			logger.error("Error to modify SimpleWebCheckinDelegate - Load default value", e);
			isCacheEnabled = false;			
		}

		logger.info("Activated SimpleWebCheckinDelegate: "
				+ "IS_CACHE_ENABLED=[" + isCacheEnabled + "], "
				+ "CACHE_REFRESH_INTERVAL=[" + refreshInterval + "]");
	}

	@Modified
	private void modified(ComponentContext componentContext) {
		try {
			isCacheEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_CACHE_ENABLED), false);
			refreshInterval = PropertiesUtil.toLong(
					componentContext.getProperties().get(CACHE_REFRESH_INTERVAL), 3600000);
		} catch (Exception e) {
			logger.error("Error to modify SimpleWebCheckinDelegate - Load default value", e);
			isCacheEnabled = false;			
		}

		logger.info("Modified SimpleWebCheckinDelegate: "
				+ "IS_CACHE_ENABLED=[" + isCacheEnabled + "], "
				+ "CACHE_REFRESH_INTERVAL=[" + refreshInterval + "]");
	}
}