package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutInitRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutInitResponse;
import com.alitalia.aem.service.api.home.WebCheckinAncillaryService;

@Component(immediate=true, metatype=false)
@Service(value=AncillaryCartCheckOutInitBehaviour.class)
public class AncillaryCartCheckOutInitBehaviour extends Behaviour<MmbAncillaryCheckOutInitRequest, MmbAncillaryCheckOutInitResponse> {

	@Reference
	private WebCheckinAncillaryService webCheckinAncillaryService;

	@Override
	protected MmbAncillaryCheckOutInitResponse executeOrchestration(MmbAncillaryCheckOutInitRequest request) {
		if (request == null)
			throw new IllegalArgumentException("MmbAncillaryCheckOutInitRequest is null.");
		return webCheckinAncillaryService.checkOutInit(request);
	}

}
