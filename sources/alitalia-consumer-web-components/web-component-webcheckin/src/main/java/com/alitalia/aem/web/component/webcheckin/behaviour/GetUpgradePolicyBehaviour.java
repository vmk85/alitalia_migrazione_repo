package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.UpgradePolicyCheckinRequest;
import com.alitalia.aem.common.messages.home.UpgradePolicyCheckinResponse;
import com.alitalia.aem.service.api.home.WebCheckinComfortSeatService;

@Component(immediate = true, metatype = false)
@Service(value = GetUpgradePolicyBehaviour.class)
public class GetUpgradePolicyBehaviour extends Behaviour<UpgradePolicyCheckinRequest, UpgradePolicyCheckinResponse> {

	@Reference
	private WebCheckinComfortSeatService webcheckinComfortSeatService;

	@Override
	public UpgradePolicyCheckinResponse executeOrchestration(UpgradePolicyCheckinRequest request) {
		if (request == null)
			throw new IllegalArgumentException("UpgradePolicyCheckinRequest is null.");
		return webcheckinComfortSeatService.getUpgradePolicy(request);
	}
}
