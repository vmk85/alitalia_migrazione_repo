package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.WebCheckinRequest;
import com.alitalia.aem.common.messages.home.WebCheckinResponse;
import com.alitalia.aem.service.api.home.WebCheckinService;

@Component(immediate = true, metatype = false)
@Service(value = WebCheckinBehaviour.class)
public class WebCheckinBehaviour extends Behaviour<WebCheckinRequest, WebCheckinResponse> {

	@Reference
	private WebCheckinService webcheckinService;
	
	@Override
	public WebCheckinResponse executeOrchestration(WebCheckinRequest request) {
		if (request == null)
			throw new IllegalArgumentException("WebCheckinRequest is null.");
		return webcheckinService.checkIn(request);
	}

}
