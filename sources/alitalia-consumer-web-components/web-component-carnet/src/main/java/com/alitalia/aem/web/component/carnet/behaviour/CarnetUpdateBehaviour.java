package com.alitalia.aem.web.component.carnet.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.CarnetUpdateRequest;
import com.alitalia.aem.common.messages.home.CarnetUpdateResponse;
import com.alitalia.aem.service.api.home.CarnetService;

@Component(immediate = true, metatype = false)
@Service(value = CarnetUpdateBehaviour.class)
public class CarnetUpdateBehaviour extends Behaviour<CarnetUpdateRequest, CarnetUpdateResponse> {

	@Reference
	private CarnetService carnetService;
	
	@Override
	public CarnetUpdateResponse executeOrchestration(CarnetUpdateRequest request) {
		if (request == null)
			throw new IllegalArgumentException("CarnetUpdateRequest is null.");
		return carnetService.updateCarnet(request);
	}
}