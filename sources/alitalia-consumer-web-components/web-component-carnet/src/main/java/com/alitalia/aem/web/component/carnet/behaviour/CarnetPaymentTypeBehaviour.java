package com.alitalia.aem.web.component.carnet.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.CarnetPaymentTypeResponse;
import com.alitalia.aem.common.messages.home.CarnetStaticDataRequest;
import com.alitalia.aem.service.api.home.CarnetStaticDataService;

@Component(immediate = true, metatype = false)
@Service(value = CarnetPaymentTypeBehaviour.class)
public class CarnetPaymentTypeBehaviour extends Behaviour<CarnetStaticDataRequest, CarnetPaymentTypeResponse> {

	@Reference
	private CarnetStaticDataService carnetStaticDataService;
	
	@Override
	public CarnetPaymentTypeResponse executeOrchestration(CarnetStaticDataRequest request) {
		if (request == null)
			throw new IllegalArgumentException("CarnetStaticDataRequest is null.");
		return carnetStaticDataService.getPaymentList(request);
	}
}