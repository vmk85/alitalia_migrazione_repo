package com.alitalia.aem.web.component.carnet.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.CarnetBuyCarnetRequest;
import com.alitalia.aem.common.messages.home.CarnetBuyCarnetResponse;
import com.alitalia.aem.service.api.home.CarnetPaymentService;

@Component(immediate = true, metatype = false)
@Service(value = BuyCarnetBehaviour.class)
public class BuyCarnetBehaviour extends Behaviour<CarnetBuyCarnetRequest, CarnetBuyCarnetResponse> {

	@Reference
	private CarnetPaymentService carnetPaymentService;
	
	@Override
	public CarnetBuyCarnetResponse executeOrchestration(CarnetBuyCarnetRequest request) {
		if (request == null)
			throw new IllegalArgumentException("CarnetBuyCarnetRequest is null.");
		return carnetPaymentService.buyCarnet(request);
	}
}