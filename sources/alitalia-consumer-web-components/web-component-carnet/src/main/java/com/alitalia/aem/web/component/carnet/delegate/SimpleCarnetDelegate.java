package com.alitalia.aem.web.component.carnet.delegate;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.messages.home.CarnetBuyCarnetRequest;
import com.alitalia.aem.common.messages.home.CarnetBuyCarnetResponse;
import com.alitalia.aem.common.messages.home.CarnetCountryStaticDataResponse;
import com.alitalia.aem.common.messages.home.CarnetGetListRequest;
import com.alitalia.aem.common.messages.home.CarnetGetListResponse;
import com.alitalia.aem.common.messages.home.CarnetLoginRequest;
import com.alitalia.aem.common.messages.home.CarnetLoginResponse;
import com.alitalia.aem.common.messages.home.CarnetPaymentTypeResponse;
import com.alitalia.aem.common.messages.home.CarnetRecoveryCodeRequest;
import com.alitalia.aem.common.messages.home.CarnetRecoveryCodeResponse;
import com.alitalia.aem.common.messages.home.CarnetSendEmailRequest;
import com.alitalia.aem.common.messages.home.CarnetSendEmailResponse;
import com.alitalia.aem.common.messages.home.CarnetStaticDataRequest;
import com.alitalia.aem.common.messages.home.CarnetUpdateRequest;
import com.alitalia.aem.common.messages.home.CarnetUpdateResponse;
import com.alitalia.aem.web.component.carnet.behaviour.BuyCarnetBehaviour;
import com.alitalia.aem.web.component.carnet.behaviour.CarnetCountryBehaviour;
import com.alitalia.aem.web.component.carnet.behaviour.CarnetCreditCardCountryBehaviour;
import com.alitalia.aem.web.component.carnet.behaviour.CarnetGetListBehaviour;
import com.alitalia.aem.web.component.carnet.behaviour.CarnetLoginBehaviour;
import com.alitalia.aem.web.component.carnet.behaviour.CarnetPaymentTypeBehaviour;
import com.alitalia.aem.web.component.carnet.behaviour.CarnetRecoveryCodeBehaviour;
import com.alitalia.aem.web.component.carnet.behaviour.CarnetUpdateBehaviour;
import com.alitalia.aem.web.component.carnet.behaviour.SendEmailBehaviour;

@Component(immediate=true, metatype=false)
@Service
public class SimpleCarnetDelegate implements CarnetDelegate {

	@Reference
	private BuyCarnetBehaviour buyCarnetBehaviour;
	
	@Reference
	private SendEmailBehaviour sendEmailBehaviour;
	
	@Reference
	private CarnetGetListBehaviour carnetGetListBehaviour;
	
	@Reference
	private CarnetLoginBehaviour carnetLoginBehaviour;
	
	@Reference
	private CarnetRecoveryCodeBehaviour carnetRecoveryCodeBehaviour;
	
	@Reference
	private CarnetUpdateBehaviour carnetUpdateBehaviour;
	
	@Reference
	private CarnetPaymentTypeBehaviour carnetPaymentTypeBehaviour;
	
	@Reference
	private CarnetCreditCardCountryBehaviour carnetCreditCardCountryBehaviour;
	
	@Reference
	private CarnetCountryBehaviour carnetCountryBehaviour;
	
	//PAYMENT
	@Override
	public CarnetBuyCarnetResponse buyCarnet(CarnetBuyCarnetRequest request) {
		BehaviourExecutor<BuyCarnetBehaviour, CarnetBuyCarnetRequest, CarnetBuyCarnetResponse> executor = new BehaviourExecutor<BuyCarnetBehaviour, CarnetBuyCarnetRequest, CarnetBuyCarnetResponse>();
		return executor.executeBehaviour(buyCarnetBehaviour, request);
	}
	
	@Override
	public CarnetSendEmailResponse sendEmail(CarnetSendEmailRequest request) {
		BehaviourExecutor<SendEmailBehaviour, CarnetSendEmailRequest, CarnetSendEmailResponse> executor = new BehaviourExecutor<SendEmailBehaviour, CarnetSendEmailRequest, CarnetSendEmailResponse>();
		return executor.executeBehaviour(sendEmailBehaviour, request);
	}
	
	//CARNET
	@Override
	public CarnetGetListResponse getCarnetList(CarnetGetListRequest request) {
		BehaviourExecutor<CarnetGetListBehaviour, CarnetGetListRequest, CarnetGetListResponse> executor = new BehaviourExecutor<CarnetGetListBehaviour, CarnetGetListRequest, CarnetGetListResponse>();
		return executor.executeBehaviour(carnetGetListBehaviour, request);
	}
	
	@Override
	public CarnetLoginResponse loginCarnet(CarnetLoginRequest request) {
		BehaviourExecutor<CarnetLoginBehaviour, CarnetLoginRequest, CarnetLoginResponse> executor = new BehaviourExecutor<CarnetLoginBehaviour, CarnetLoginRequest, CarnetLoginResponse>();
		return executor.executeBehaviour(carnetLoginBehaviour, request);
	}
	
	@Override
	public CarnetRecoveryCodeResponse recoveryCarnetCode(CarnetRecoveryCodeRequest request) {
		BehaviourExecutor<CarnetRecoveryCodeBehaviour, CarnetRecoveryCodeRequest, CarnetRecoveryCodeResponse> executor = new BehaviourExecutor<CarnetRecoveryCodeBehaviour, CarnetRecoveryCodeRequest, CarnetRecoveryCodeResponse>();
		return executor.executeBehaviour(carnetRecoveryCodeBehaviour, request);
	}
	
	@Override
	public CarnetUpdateResponse updateCarnet(CarnetUpdateRequest request) {
		BehaviourExecutor<CarnetUpdateBehaviour, CarnetUpdateRequest, CarnetUpdateResponse> executor = new BehaviourExecutor<CarnetUpdateBehaviour, CarnetUpdateRequest, CarnetUpdateResponse>();
		return executor.executeBehaviour(carnetUpdateBehaviour, request);
	}
	
	@Override
	public CarnetPaymentTypeResponse getPaymentList(CarnetStaticDataRequest request) {
		BehaviourExecutor<CarnetPaymentTypeBehaviour, CarnetStaticDataRequest, CarnetPaymentTypeResponse> executor = new BehaviourExecutor<CarnetPaymentTypeBehaviour, CarnetStaticDataRequest, CarnetPaymentTypeResponse>();
		return executor.executeBehaviour(carnetPaymentTypeBehaviour, request);
	}
	
	@Override
	public CarnetCountryStaticDataResponse getCreditCardCountryList(CarnetStaticDataRequest request) {
		BehaviourExecutor<CarnetCreditCardCountryBehaviour, CarnetStaticDataRequest, CarnetCountryStaticDataResponse> executor = new BehaviourExecutor<CarnetCreditCardCountryBehaviour, CarnetStaticDataRequest, CarnetCountryStaticDataResponse>();
		return executor.executeBehaviour(carnetCreditCardCountryBehaviour, request);
	}
	
	@Override
	public CarnetCountryStaticDataResponse getStateList(CarnetStaticDataRequest request) {
		BehaviourExecutor<CarnetCountryBehaviour, CarnetStaticDataRequest, CarnetCountryStaticDataResponse> executor = new BehaviourExecutor<CarnetCountryBehaviour, CarnetStaticDataRequest, CarnetCountryStaticDataResponse>();
		return executor.executeBehaviour(carnetCountryBehaviour, request);
	}
}