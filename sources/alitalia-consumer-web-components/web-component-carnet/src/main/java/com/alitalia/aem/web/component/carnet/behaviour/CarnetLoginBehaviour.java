package com.alitalia.aem.web.component.carnet.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.CarnetLoginRequest;
import com.alitalia.aem.common.messages.home.CarnetLoginResponse;
import com.alitalia.aem.service.api.home.CarnetService;

@Component(immediate = true, metatype = false)
@Service(value = CarnetLoginBehaviour.class)
public class CarnetLoginBehaviour extends Behaviour<CarnetLoginRequest, CarnetLoginResponse> {

	@Reference
	private CarnetService carnetService;
	
	@Override
	public CarnetLoginResponse executeOrchestration(CarnetLoginRequest request) {
		if (request == null)
			throw new IllegalArgumentException("CarnetLoginRequest is null.");
		return carnetService.loginCarnet(request);
	}
}