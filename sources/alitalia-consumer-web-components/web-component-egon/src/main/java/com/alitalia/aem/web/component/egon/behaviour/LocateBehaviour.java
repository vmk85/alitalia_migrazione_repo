package com.alitalia.aem.web.component.egon.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.LocateRequest;
import com.alitalia.aem.common.messages.home.LocateResponse;
import com.alitalia.aem.service.api.home.EgonService;

@Component(immediate = true, metatype = false)
@Service(value = LocateBehaviour.class)
public class LocateBehaviour extends Behaviour<LocateRequest, LocateResponse> {

	@Reference
	private EgonService egonService;
	
	@Override
	public LocateResponse executeOrchestration(LocateRequest request) {
		if(request == null) throw new IllegalArgumentException("Register Statistics request is null.");
		return egonService.locate(request);
	}	
}