package com.alitalia.aem.web.component.managemybooking.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveMmbFlightSeatMapRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbFlightSeatMapResponse;
import com.alitalia.aem.service.api.home.MmbCommonService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveFlightSeatMapBehaviour.class)
public class RetrieveFlightSeatMapBehaviour extends Behaviour<RetrieveMmbFlightSeatMapRequest, RetrieveMmbFlightSeatMapResponse> {

	@Reference
	private MmbCommonService mmbCommonService;
	
	@Override
	public RetrieveMmbFlightSeatMapResponse executeOrchestration(RetrieveMmbFlightSeatMapRequest request) {
		if (request == null)
			throw new IllegalArgumentException("RetrieveMmbFlightSeatMapRequest is null.");
		return mmbCommonService.getMmbFlightSeatsMap(request);
	}

}
