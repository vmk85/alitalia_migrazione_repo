package com.alitalia.aem.web.component.managemybooking.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.MmbAncillaryUpdateCartRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryUpdateCartResponse;
import com.alitalia.aem.service.api.home.MmbAncillaryService;

@Component(immediate=true, metatype=false)
@Service(value=UpdateAncillaryCartBehaviour.class)
public class UpdateAncillaryCartBehaviour extends Behaviour<MmbAncillaryUpdateCartRequest, MmbAncillaryUpdateCartResponse> {

	@Reference
	private MmbAncillaryService mmbAncillaryService;

	@Override
	protected MmbAncillaryUpdateCartResponse executeOrchestration(MmbAncillaryUpdateCartRequest request) {
		if (request == null)
			throw new IllegalArgumentException("MmbAncillaryUpdateCartRequest is null.");
		return mmbAncillaryService.updateCart(request);
	}

}
