package com.alitalia.aem.web.component.managemybooking.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCartRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCartResponse;
import com.alitalia.aem.service.api.home.MmbAncillaryService;

@Component(immediate=true, metatype=false)
@Service(value=GetAncillaryCartBehaviour.class)
public class GetAncillaryCartBehaviour extends Behaviour<RetrieveMmbAncillaryCartRequest, RetrieveMmbAncillaryCartResponse> {

	@Reference
	private MmbAncillaryService mmbAncillaryService;

	@Override
	protected RetrieveMmbAncillaryCartResponse executeOrchestration(RetrieveMmbAncillaryCartRequest request) {
		if (request == null)
			throw new IllegalArgumentException("RetrieveMmbAncillaryCartRequest is null.");
		return mmbAncillaryService.getCart(request);
	}

}
