package com.alitalia.aem.web.component.managemybooking.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemRequest;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemResponse;
import com.alitalia.aem.service.api.home.MmbStaticDataService;

@Component(immediate = true, metatype = false)
@Service(value = RetrievePaymentTypeItemsBehaviour.class)
public class RetrievePaymentTypeItemsBehaviour extends Behaviour<RetrievePaymentTypeItemRequest, RetrievePaymentTypeItemResponse> {

	@Reference
	private MmbStaticDataService mmbStaticDataService;
	
	@Override
	public RetrievePaymentTypeItemResponse executeOrchestration(RetrievePaymentTypeItemRequest request) {
		if (request == null)
			throw new IllegalArgumentException("RetrievePaymentTypeItemRequest is null.");
		return mmbStaticDataService.retrievePaymentTypeItem(request);
	}

}
