package com.alitalia.aem.web.component.managemybooking.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveCreditCardCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCreditCardCountriesResponse;
import com.alitalia.aem.service.api.home.MmbStaticDataService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveCreditCardCountriesBehaviour.class)
public class RetrieveCreditCardCountriesBehaviour extends Behaviour<RetrieveCreditCardCountriesRequest, RetrieveCreditCardCountriesResponse> {

	@Reference
	private MmbStaticDataService mmbStaticDataService;
	
	@Override
	public RetrieveCreditCardCountriesResponse executeOrchestration(RetrieveCreditCardCountriesRequest request) {
		if (request == null)
			throw new IllegalArgumentException("RetrieveProvincesRequest is null.");
		return mmbStaticDataService.retrieveCreditCardCountries(request);
	}

}
