package com.alitalia.aem.web.component.managemybooking.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCatalogRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCatalogResponse;
import com.alitalia.aem.service.api.home.MmbAncillaryService;

@Component(immediate=true, metatype=false)
@Service(value=GetAncillaryCatalogBehaviour.class)
public class GetAncillaryCatalogBehaviour extends Behaviour<RetrieveMmbAncillaryCatalogRequest, RetrieveMmbAncillaryCatalogResponse> {

	@Reference
	private MmbAncillaryService mmbAncillaryService;

	@Override
	protected RetrieveMmbAncillaryCatalogResponse executeOrchestration(RetrieveMmbAncillaryCatalogRequest request) {
		if (request == null)
			throw new IllegalArgumentException("RetrieveMmbAncillaryCatalogRequest is null.");
		return mmbAncillaryService.getCatalog(request);
	}

}
