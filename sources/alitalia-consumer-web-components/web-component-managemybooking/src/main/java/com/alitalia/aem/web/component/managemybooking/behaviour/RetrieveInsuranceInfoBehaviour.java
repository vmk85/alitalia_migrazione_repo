package com.alitalia.aem.web.component.managemybooking.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.InsuranceResponse;
import com.alitalia.aem.common.messages.home.MmbInsuranceInfoRequest;
import com.alitalia.aem.service.api.home.MmbInsuranceService;

@Component(immediate=true, metatype=false)
@Service(value=RetrieveInsuranceInfoBehaviour.class)
public class RetrieveInsuranceInfoBehaviour extends Behaviour<MmbInsuranceInfoRequest, InsuranceResponse> {

	@Reference
	private MmbInsuranceService mmbInsuranceService;

	@Override
	protected InsuranceResponse executeOrchestration(MmbInsuranceInfoRequest request) {
		return mmbInsuranceService.getInsurance(request);
	}

}
