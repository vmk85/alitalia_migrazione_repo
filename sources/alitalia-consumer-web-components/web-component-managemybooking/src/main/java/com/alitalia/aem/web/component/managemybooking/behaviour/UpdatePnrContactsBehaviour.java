package com.alitalia.aem.web.component.managemybooking.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrContactsRequest;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrContactsResponse;
import com.alitalia.aem.service.api.home.MmbSearchService;

@Component(immediate = true, metatype = false)
@Service(value = UpdatePnrContactsBehaviour.class)
public class UpdatePnrContactsBehaviour extends Behaviour<UpdateMmbPnrContactsRequest, UpdateMmbPnrContactsResponse> {

	@Reference
	private MmbSearchService mmbSearchService;
	
	@Override
	public UpdateMmbPnrContactsResponse executeOrchestration(UpdateMmbPnrContactsRequest request) {
		if (request == null)
			throw new IllegalArgumentException("UpdateMmbPnrContactsRequest is null.");
		return mmbSearchService.updatePnrContacts(request);
	}

}
