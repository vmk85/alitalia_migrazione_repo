package com.alitalia.aem.web.component.managemybooking.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.InsuranceResponse;
import com.alitalia.aem.common.messages.home.MmbPayInsuranceRequest;
import com.alitalia.aem.service.api.home.MmbInsuranceService;

@Component(immediate=true, metatype=false)
@Service(value=PayInsuranceBehaviour.class)
public class PayInsuranceBehaviour extends Behaviour<MmbPayInsuranceRequest, InsuranceResponse> {

	@Reference
	private MmbInsuranceService mmbInsuranceService;

	@Override
	protected InsuranceResponse executeOrchestration(MmbPayInsuranceRequest request) {
		if (request == null)
			throw new IllegalArgumentException("MmbPayInsuranceRequest is null.");
		return mmbInsuranceService.payInsurance(request);
	}

}
