package com.alitalia.aem.web.component.managemybooking.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveMealsRequest;
import com.alitalia.aem.common.messages.home.RetrieveMealsResponse;
import com.alitalia.aem.service.api.home.MmbStaticDataService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveMealsBehaviour.class)
public class RetrieveMealsBehaviour extends Behaviour<RetrieveMealsRequest, RetrieveMealsResponse> {

	@Reference
	private MmbStaticDataService mmbStaticDataService;
	
	@Override
	public RetrieveMealsResponse executeOrchestration(RetrieveMealsRequest request) {
		if (request == null)
			throw new IllegalArgumentException("RetrieveMealsRequest is null.");
		return mmbStaticDataService.retrieveMeals(request);
	}

}
