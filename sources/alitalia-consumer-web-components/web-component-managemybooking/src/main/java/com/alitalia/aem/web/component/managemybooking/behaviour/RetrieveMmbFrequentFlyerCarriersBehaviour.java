package com.alitalia.aem.web.component.managemybooking.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveFrequentFlayerRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbFrequentFlyerCarriersResponse;
import com.alitalia.aem.service.api.home.MmbStaticDataService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveMmbFrequentFlyerCarriersBehaviour.class)
public class RetrieveMmbFrequentFlyerCarriersBehaviour extends Behaviour<RetrieveFrequentFlayerRequest, RetrieveMmbFrequentFlyerCarriersResponse> {

	@Reference
	private MmbStaticDataService mmbStaticDataService;
	
	@Override
	public RetrieveMmbFrequentFlyerCarriersResponse executeOrchestration(RetrieveFrequentFlayerRequest request) {
		if (request == null)
			throw new IllegalArgumentException("RetrieveProvincesRequest is null.");
		return mmbStaticDataService.retrieveFrequentFlyers(request);
	}

}
