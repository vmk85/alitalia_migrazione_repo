package com.alitalia.aem.web.component.managemybooking.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.MmbModifyPnrApisInfoRequest;
import com.alitalia.aem.common.messages.home.MmbModifyPnrApisInfoResponse;
import com.alitalia.aem.service.api.home.MmbSearchService;

@Component(immediate = true, metatype = false)
@Service(value = ModifyPnrPassengerApisInfoBehaviour.class)
public class ModifyPnrPassengerApisInfoBehaviour extends Behaviour<MmbModifyPnrApisInfoRequest, MmbModifyPnrApisInfoResponse> {

	@Reference
	private MmbSearchService mmbSearchService;
	
	@Override
	public MmbModifyPnrApisInfoResponse executeOrchestration(MmbModifyPnrApisInfoRequest request) {
		if (request == null)
			throw new IllegalArgumentException("RetrieveProvincesRequest is null.");
		return mmbSearchService.modifyPnrPassengerApisInfo(request);
	}

}
