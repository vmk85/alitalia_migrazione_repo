package com.alitalia.aem.web.component.managemybooking.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.MmbAncillarySendEmailRequest;
import com.alitalia.aem.common.messages.home.MmbAncillarySendEmailResponse;
import com.alitalia.aem.service.api.home.MmbAncillaryService;

@Component(immediate=true, metatype=false)
@Service(value=SendEmailForPurchasedAncillaryBehaviour.class)
public class SendEmailForPurchasedAncillaryBehaviour extends Behaviour<MmbAncillarySendEmailRequest, MmbAncillarySendEmailResponse> {

	@Reference
	private  MmbAncillaryService mmbAncillaryService;

	@Override
	protected MmbAncillarySendEmailResponse executeOrchestration(MmbAncillarySendEmailRequest request) {
		if (request == null)
			throw new IllegalArgumentException("MmbAncillarySendEmailRequest is null.");
		return mmbAncillaryService.sendEmail(request);
	}

}
