package com.alitalia.aem.web.component.managemybooking.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.GetSabrePNRRequest;
import com.alitalia.aem.common.messages.home.GetSabrePNRResponse;
import com.alitalia.aem.service.api.home.ReservationListService;

@Component(immediate=true, metatype=false)
@Service(value=GetSabrePNRBehaviour.class)
public class GetSabrePNRBehaviour extends Behaviour<GetSabrePNRRequest, GetSabrePNRResponse> {

	@Reference
	private ReservationListService reservationListService;

	@Override
	protected GetSabrePNRResponse executeOrchestration(GetSabrePNRRequest request) {
		if (request == null)
			throw new IllegalArgumentException("GetSabrePNRRequest is null.");
		return reservationListService.getSabrePNR(request);
	}
}