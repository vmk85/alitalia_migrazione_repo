package com.alitalia.aem.web.component.managemybooking.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixRequest;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixResponse;
import com.alitalia.aem.service.api.home.MmbStaticDataService;

@Component(immediate = true, metatype = false)
@Service(value = RetrievePhonePrefixesBehaviour.class)
public class RetrievePhonePrefixesBehaviour extends Behaviour<RetrievePhonePrefixRequest, RetrievePhonePrefixResponse> {

	@Reference
	private MmbStaticDataService mmbStaticDataService;
	
	@Override
	public RetrievePhonePrefixResponse executeOrchestration(RetrievePhonePrefixRequest request) {
		if (request == null)
			throw new IllegalArgumentException("RetrievePhonePrefixRequest is null.");
		return mmbStaticDataService.retrievePhonePrefix(request);
	}

}
