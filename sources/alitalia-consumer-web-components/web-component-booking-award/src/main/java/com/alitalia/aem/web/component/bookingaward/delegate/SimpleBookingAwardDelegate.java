package com.alitalia.aem.web.component.bookingaward.delegate;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.messages.home.AwardValueRequest;
import com.alitalia.aem.common.messages.home.AwardValueResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbFlightSeatMapRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbFlightSeatMapResponse;
import com.alitalia.aem.common.messages.home.SearchBookingSolutionResponse;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionResponse;
import com.alitalia.aem.web.component.bookingaward.behaviour.RetrieveAwardCalendarSolutionsBehaviour;
import com.alitalia.aem.web.component.bookingaward.behaviour.RetrieveAwardFlightInformationBehaviour;
import com.alitalia.aem.web.component.bookingaward.behaviour.RetrieveAwardSellupInformationBehaviour;
import com.alitalia.aem.web.component.bookingaward.behaviour.RetrieveAwardTaxInformationBehaviour;
import com.alitalia.aem.web.component.bookingaward.behaviour.RetrieveAwardValueBehaviour;

@Component(immediate=true, metatype=false)
@Service
public class SimpleBookingAwardDelegate implements BookingAwardDelegate {

	@Reference
	private RetrieveAwardCalendarSolutionsBehaviour retrieveAwardCalendarSolutionsBehaviour;

	@Reference
	private RetrieveAwardFlightInformationBehaviour retrieveAwardFlightInformationBehaviour;

	@Reference
	private RetrieveAwardSellupInformationBehaviour retrieveAwardSellupInformation;

	@Reference
	private RetrieveAwardTaxInformationBehaviour retrieveAwardTaxInformationBehaviour;

	@Reference
	private RetrieveAwardValueBehaviour retrieveAwardValueBehaviour;

	@Override
	public SearchFlightSolutionResponse retrieveAwardCalendarSolutions(SearchFlightSolutionRequest request) {
		BehaviourExecutor<RetrieveAwardCalendarSolutionsBehaviour, SearchFlightSolutionRequest, SearchFlightSolutionResponse> executor = new BehaviourExecutor();
		return executor.executeBehaviour(retrieveAwardCalendarSolutionsBehaviour, request);
	}

	@Override
	public SearchFlightSolutionResponse retrieveAwardFlightInformation(SearchFlightSolutionRequest request) {
		BehaviourExecutor<RetrieveAwardFlightInformationBehaviour, SearchFlightSolutionRequest, SearchFlightSolutionResponse> executor = new BehaviourExecutor();
		return executor.executeBehaviour(retrieveAwardFlightInformationBehaviour, request);
	}

	@Override
	public SearchBookingSolutionResponse retrieveAwardSellupInformation(SearchFlightSolutionRequest request) {
		BehaviourExecutor<RetrieveAwardSellupInformationBehaviour, SearchFlightSolutionRequest, SearchBookingSolutionResponse> executor = new BehaviourExecutor();
		return executor.executeBehaviour(retrieveAwardSellupInformation, request);
	}

	@Override
	public SearchFlightSolutionResponse retrieveAwardTaxInformation(SearchFlightSolutionRequest request) {
		BehaviourExecutor<RetrieveAwardTaxInformationBehaviour, SearchFlightSolutionRequest, SearchFlightSolutionResponse> executor = new BehaviourExecutor();
		return executor.executeBehaviour(retrieveAwardTaxInformationBehaviour, request);
	}

	@Override
	public AwardValueResponse retrieveAwardValue(AwardValueRequest request) {
		BehaviourExecutor<RetrieveAwardValueBehaviour, AwardValueRequest, AwardValueResponse> executor = new BehaviourExecutor();
		return executor.executeBehaviour(retrieveAwardValueBehaviour, request);
	}

}
