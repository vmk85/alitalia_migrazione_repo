package com.alitalia.aem.web.component.bookingaward.delegate;

import com.alitalia.aem.common.messages.home.AwardValueRequest;
import com.alitalia.aem.common.messages.home.AwardValueResponse;
import com.alitalia.aem.common.messages.home.SearchBookingSolutionResponse;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionResponse;

public interface BookingAwardDelegate {

	SearchFlightSolutionResponse retrieveAwardCalendarSolutions(SearchFlightSolutionRequest request);
	SearchFlightSolutionResponse retrieveAwardFlightInformation(SearchFlightSolutionRequest request);
	SearchBookingSolutionResponse retrieveAwardSellupInformation(SearchFlightSolutionRequest request);
	SearchFlightSolutionResponse retrieveAwardTaxInformation(SearchFlightSolutionRequest request);
	AwardValueResponse retrieveAwardValue(AwardValueRequest request);

}
