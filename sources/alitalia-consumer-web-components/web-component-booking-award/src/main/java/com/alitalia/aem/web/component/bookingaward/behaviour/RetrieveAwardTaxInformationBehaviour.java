package com.alitalia.aem.web.component.bookingaward.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionResponse;
import com.alitalia.aem.service.api.home.BookingAwardSearchService;

@Component(immediate=true, metatype=false)
@Service(value=RetrieveAwardTaxInformationBehaviour.class)
public class RetrieveAwardTaxInformationBehaviour extends Behaviour<SearchFlightSolutionRequest, SearchFlightSolutionResponse> {

	@Reference
	private BookingAwardSearchService bookingAwardSearchService;

	@Override
	protected SearchFlightSolutionResponse executeOrchestration(SearchFlightSolutionRequest request) {
		if (request == null)
			throw new IllegalArgumentException("SearchFlightSolutionRequest is null in RetrieveAwardTaxInformationBehaviour executeOrchestration.");
		return bookingAwardSearchService.executeAwardTaxSearch(request);
	}

}
