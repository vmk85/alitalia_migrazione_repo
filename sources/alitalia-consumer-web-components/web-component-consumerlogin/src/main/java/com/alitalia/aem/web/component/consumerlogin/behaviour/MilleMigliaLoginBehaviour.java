package com.alitalia.aem.web.component.consumerlogin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.MilleMigliaLoginRequest;
import com.alitalia.aem.common.messages.home.MilleMigliaLoginResponse;
import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;

@Component(immediate = true, metatype = false)
@Service(value = MilleMigliaLoginBehaviour.class)
public class MilleMigliaLoginBehaviour 
		extends Behaviour<MilleMigliaLoginRequest, MilleMigliaLoginResponse> {

	@Reference
	private MyPersonalAreaMilleMigliaService personalAreaMilleMigliaService;

	@Override
	public MilleMigliaLoginResponse executeOrchestration(MilleMigliaLoginRequest request) {
		if(request == null)
			throw new IllegalArgumentException("Request is null.");

		MilleMigliaLoginResponse response = personalAreaMilleMigliaService.login(request);
		
		return response;
	}

}
