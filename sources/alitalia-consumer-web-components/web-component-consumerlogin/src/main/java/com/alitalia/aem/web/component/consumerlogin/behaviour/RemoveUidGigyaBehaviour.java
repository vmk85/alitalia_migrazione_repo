package com.alitalia.aem.web.component.consumerlogin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RemoveSocialAccountsRequest;
import com.alitalia.aem.common.messages.home.RemoveSocialAccountsResponse;
import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;

@Component(immediate = true, metatype = false)
@Service(value = RemoveUidGigyaBehaviour.class)
public class RemoveUidGigyaBehaviour extends Behaviour<RemoveSocialAccountsRequest, RemoveSocialAccountsResponse> {

	@Reference
	private MyPersonalAreaMilleMigliaService personalAreaMilleMigliaService;

	@Override
	public RemoveSocialAccountsResponse executeOrchestration(RemoveSocialAccountsRequest request) {
		if(request == null) throw new IllegalArgumentException("Request is null.");
		return personalAreaMilleMigliaService.removeUidGigya(request);
	}
}