package com.alitalia.aem.web.component.consumerlogin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.data.home.EmailMessage;
import com.alitalia.aem.common.data.home.enumerations.DeliveryNotificationOptionsEnum;
import com.alitalia.aem.common.data.home.enumerations.MailPriorityEnum;
import com.alitalia.aem.common.messages.home.AgencySendMailRequest;
import com.alitalia.aem.common.messages.home.AgencySendMailResponse;
import com.alitalia.aem.common.messages.home.RetrievePinRequest;
import com.alitalia.aem.common.messages.home.RetrievePinResponse;
import com.alitalia.aem.service.api.home.BusinessLoginService;
import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;

@Component(immediate = true, metatype = false)
@Service(value = RetrievePinByCustomerNumberBehaviour.class)
public class RetrievePinByCustomerNumberBehaviour extends Behaviour<RetrievePinRequest, RetrievePinResponse> {

	@Reference
	private MyPersonalAreaMilleMigliaService personalAreaMilleMigliaService;

	@Reference
	private volatile BusinessLoginService businessLoginService;
	
	@Override
	public RetrievePinResponse executeOrchestration(RetrievePinRequest request) {
		if(request == null) throw new IllegalArgumentException("Request is null.");
		RetrievePinResponse retrievePinResponse = personalAreaMilleMigliaService.retrievePinByCustomerNumber(request);
		retrievePinResponse.setRetrieved(retrievePinResponse.getCustomerProfile() != null &&
				retrievePinResponse.getCustomerProfile().getEmail() != null &&
				retrievePinResponse.getCustomerProfile().getCustomerPinCode() != null);
		if (retrievePinResponse.isRetrieved()) {
			String mailTo = retrievePinResponse.getCustomerProfile().getEmail();
			String pin = retrievePinResponse.getCustomerProfile().getCustomerPinCode();
			String mailBody = request.getMailBody();
			mailBody = "<![CDATA[" + mailBody.replace("{PLACEHOLDER_PIN}", pin) + "]]>";

			AgencySendMailRequest mailRequest = new AgencySendMailRequest();
			EmailMessage emailMessage = new EmailMessage();

			emailMessage.setFrom(request.getMailSender());
			emailMessage.setTo(mailTo);
			emailMessage.setSubject(request.getMailSubject());
			emailMessage.setMessageText(mailBody);
			emailMessage.setIsBodyHtml(true);
			emailMessage.setDeliveryNotification(DeliveryNotificationOptionsEnum.NONE);
			emailMessage.setPriority(MailPriorityEnum.NORMAL);
			mailRequest.setEmailMessage(emailMessage);
			AgencySendMailResponse mailResponse =  businessLoginService.sendMail(mailRequest);
			retrievePinResponse.setMailSent(mailResponse.isSendMailSuccessful());
		}
		return retrievePinResponse;
	}
}