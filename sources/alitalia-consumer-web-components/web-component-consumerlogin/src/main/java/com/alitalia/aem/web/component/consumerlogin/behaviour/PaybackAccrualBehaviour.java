package com.alitalia.aem.web.component.consumerlogin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.MMPaybackAccrualRequest;
import com.alitalia.aem.common.messages.home.MMPaybackAccrualResponse;
import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;

@Component(immediate = true, metatype = false)
@Service(value = PaybackAccrualBehaviour.class)
public class PaybackAccrualBehaviour extends Behaviour<MMPaybackAccrualRequest, MMPaybackAccrualResponse> {

	@Reference
	private MyPersonalAreaMilleMigliaService personalAreaMilleMigliaService;

	@Override
	public MMPaybackAccrualResponse executeOrchestration(MMPaybackAccrualRequest request) {
		if(request == null) throw new IllegalArgumentException("Request is null.");
		return personalAreaMilleMigliaService.paybackAccrual(request);
	}
}