package com.alitalia.aem.web.component.consumerlogin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.UpdateNicknameRequest;
import com.alitalia.aem.common.messages.home.UpdateNicknameResponse;
import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;

@Component(immediate = true, metatype = false)
@Service(value = UpdateNicknameBehaviour.class)
public class UpdateNicknameBehaviour extends Behaviour<UpdateNicknameRequest, UpdateNicknameResponse> {

	@Reference
	private MyPersonalAreaMilleMigliaService personalAreaMilleMigliaService;

	@Override
	public UpdateNicknameResponse executeOrchestration(UpdateNicknameRequest request) {
		if(request == null) throw new IllegalArgumentException("Request is null.");
		return personalAreaMilleMigliaService.updateNickname(request);
	}
}