package com.alitalia.aem.web.component.consumerlogin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.UpdateUserProfileRequest;
import com.alitalia.aem.common.messages.home.UpdateUserProfileResponse;
import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;

@Component(immediate = true, metatype = false)
@Service(value = UpdateUserProfileBehaviour.class)
public class UpdateUserProfileBehaviour extends Behaviour<UpdateUserProfileRequest, UpdateUserProfileResponse> {

	@Reference
	private MyPersonalAreaMilleMigliaService personalAreaMilleMigliaService;

	@Override
	public UpdateUserProfileResponse executeOrchestration(UpdateUserProfileRequest request) {
		if(request == null) throw new IllegalArgumentException("Request is null.");
		return personalAreaMilleMigliaService.updateProfile(request);
	}
}