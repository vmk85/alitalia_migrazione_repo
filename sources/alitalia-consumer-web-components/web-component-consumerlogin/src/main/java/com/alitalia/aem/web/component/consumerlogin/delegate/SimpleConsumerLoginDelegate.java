package com.alitalia.aem.web.component.consumerlogin.delegate;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.messages.home.AddFlightActivityRequest;
import com.alitalia.aem.common.messages.home.AddFlightActivityResponse;
import com.alitalia.aem.common.messages.home.AddressesRequest;
import com.alitalia.aem.common.messages.home.AddressesResponse;
import com.alitalia.aem.common.messages.home.BalanceRequest;
import com.alitalia.aem.common.messages.home.BalanceResponse;
import com.alitalia.aem.common.messages.home.CreditCardsListRequest;
import com.alitalia.aem.common.messages.home.CreditCardsListResponse;
import com.alitalia.aem.common.messages.home.DeletePanRequest;
import com.alitalia.aem.common.messages.home.DeletePanResponse;
import com.alitalia.aem.common.messages.home.MMPaybackAccrualRequest;
import com.alitalia.aem.common.messages.home.MMPaybackAccrualResponse;
import com.alitalia.aem.common.messages.home.MailinglistRegistrationRequest;
import com.alitalia.aem.common.messages.home.MailinglistRegistrationResponse;
import com.alitalia.aem.common.messages.home.MessagesRequest;
import com.alitalia.aem.common.messages.home.MessagesResponse;
import com.alitalia.aem.common.messages.home.MilleMigliaLoginRequest;
import com.alitalia.aem.common.messages.home.MilleMigliaLoginResponse;
import com.alitalia.aem.common.messages.home.MillemigliaSendEmailRequest;
import com.alitalia.aem.common.messages.home.MillemigliaSendEmailResponse;
import com.alitalia.aem.common.messages.home.ProfileRequest;
import com.alitalia.aem.common.messages.home.ProfileResponse;
import com.alitalia.aem.common.messages.home.RemoveSocialAccountsRequest;
import com.alitalia.aem.common.messages.home.RemoveSocialAccountsResponse;
import com.alitalia.aem.common.messages.home.RetrievePinRequest;
import com.alitalia.aem.common.messages.home.RetrievePinResponse;
import com.alitalia.aem.common.messages.home.SendNewMailingListRegistrationMailRequest;
import com.alitalia.aem.common.messages.home.SetSocialAccountRequest;
import com.alitalia.aem.common.messages.home.SetSocialAccountResponse;
import com.alitalia.aem.common.messages.home.SocialAccountsRequest;
import com.alitalia.aem.common.messages.home.SocialAccountsResponse;
import com.alitalia.aem.common.messages.home.SocialLoginRequest;
import com.alitalia.aem.common.messages.home.SocialLoginResponse;
import com.alitalia.aem.common.messages.home.StatementByDateRequest;
import com.alitalia.aem.common.messages.home.StatementByDateResponse;
import com.alitalia.aem.common.messages.home.SubscribeRequest;
import com.alitalia.aem.common.messages.home.SubscribeResponse;
import com.alitalia.aem.common.messages.home.UpdateNicknameRequest;
import com.alitalia.aem.common.messages.home.UpdateNicknameResponse;
import com.alitalia.aem.common.messages.home.UpdatePinRequest;
import com.alitalia.aem.common.messages.home.UpdatePinResponse;
import com.alitalia.aem.common.messages.home.UpdateUserProfileRequest;
import com.alitalia.aem.common.messages.home.UpdateUserProfileResponse;
import com.alitalia.aem.web.component.consumerlogin.behaviour.AddFlightActivityBehaviour;
import com.alitalia.aem.web.component.consumerlogin.behaviour.CreditCardsListBehaviour;
import com.alitalia.aem.web.component.consumerlogin.behaviour.DeletePanBehaviour;
import com.alitalia.aem.web.component.consumerlogin.behaviour.GetAddressesBehaviour;
import com.alitalia.aem.web.component.consumerlogin.behaviour.GetBalanceBehaviour;
import com.alitalia.aem.web.component.consumerlogin.behaviour.GetMessagesBehaviour;
import com.alitalia.aem.web.component.consumerlogin.behaviour.GetProfileBehaviour;
import com.alitalia.aem.web.component.consumerlogin.behaviour.GetSocialAccountsBehaviour;
import com.alitalia.aem.web.component.consumerlogin.behaviour.GetStatementByDateBehaviour;
import com.alitalia.aem.web.component.consumerlogin.behaviour.MailinglistRegistrationBehaviour;
import com.alitalia.aem.web.component.consumerlogin.behaviour.MilleMigliaLoginBehaviour;
import com.alitalia.aem.web.component.consumerlogin.behaviour.PaybackAccrualBehaviour;
import com.alitalia.aem.web.component.consumerlogin.behaviour.RemoveUidGigyaBehaviour;
import com.alitalia.aem.web.component.consumerlogin.behaviour.RetrievePinByCustomerNumberBehaviour;
import com.alitalia.aem.web.component.consumerlogin.behaviour.RetrievePinByNicknameBehaviour;
import com.alitalia.aem.web.component.consumerlogin.behaviour.SendNewMailingListRegistrationMailBehaviour;
import com.alitalia.aem.web.component.consumerlogin.behaviour.SendNewRegistrationEmailBehaviour;
import com.alitalia.aem.web.component.consumerlogin.behaviour.SetSocialAccountBehaviour;
import com.alitalia.aem.web.component.consumerlogin.behaviour.SocialLoginBehaviour;
import com.alitalia.aem.web.component.consumerlogin.behaviour.SubscribeBehaviour;
import com.alitalia.aem.web.component.consumerlogin.behaviour.UpdateNicknameBehaviour;
import com.alitalia.aem.web.component.consumerlogin.behaviour.UpdatePinBehaviour;
import com.alitalia.aem.web.component.consumerlogin.behaviour.UpdateUserProfileBehaviour;

@Service
@Component(immediate = true, metatype = false)
public class SimpleConsumerLoginDelegate implements ConsumerLoginDelegate {
	
	private static final Logger logger = LoggerFactory.getLogger(SimpleConsumerLoginDelegate.class);
	
	@Reference
	private MilleMigliaLoginBehaviour milleMigliaLoginBehaviour;

	@Reference
	private SocialLoginBehaviour socialLoginBehaviour;
	
	@Reference
	private SetSocialAccountBehaviour setSocialAccountBehavior;
	
	@Reference
	private GetProfileBehaviour getProfileBehaviour;
	
	@Reference
	private GetAddressesBehaviour getAddressesBehaviour;
	
	@Reference
	private GetMessagesBehaviour getMessagesBehaviour;
	
	@Reference
	private GetBalanceBehaviour getBalanceBehaviour;
	
	@Reference
	private UpdateUserProfileBehaviour updateUserProfileBehaviour;
	
	@Reference
	private GetStatementByDateBehaviour getStatementByDateBehaviour;
	
	@Reference
	private AddFlightActivityBehaviour addFlightActivityBehaviour;
	
	@Reference
	private UpdatePinBehaviour updatePinBehaviour;
	
	@Reference
	private GetSocialAccountsBehaviour getSocialAccountsBehaviour;
	
	@Reference
	private SubscribeBehaviour subscribeBehaviour;
	
	@Reference
	private UpdateNicknameBehaviour updateNicknameBehaviour;
	
	@Reference
	private RetrievePinByCustomerNumberBehaviour retrievePinByCustomerNumberBehaviour;
	
	@Reference
	private RetrievePinByNicknameBehaviour retrievePinByNicknameBehaviour;
	
	@Reference
	private SendNewRegistrationEmailBehaviour sendNewRegistrationEmailBehaviour;
	
	@Reference
	private SendNewMailingListRegistrationMailBehaviour sendNewMailingListRegistrationMailBehaviour;
	
	@Reference
	private MailinglistRegistrationBehaviour mailinglistRegistrationBehaviour;
	
	@Reference
	private DeletePanBehaviour deletePanBehaviour;
	
	@Reference
	private RemoveUidGigyaBehaviour removeUidGigyaBehaviour;
	
	@Reference
	private CreditCardsListBehaviour creditCardsListBehaviour;
	
	@Reference
	private PaybackAccrualBehaviour paybackAccrualBehaviour;

	@Override
	public MilleMigliaLoginResponse milleMigliaLogin(MilleMigliaLoginRequest request) {
		BehaviourExecutor<MilleMigliaLoginBehaviour, MilleMigliaLoginRequest, 
				MilleMigliaLoginResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(milleMigliaLoginBehaviour, request);
	}

	@Override
	public SocialLoginResponse socialLogin(SocialLoginRequest request) {
		BehaviourExecutor<SocialLoginBehaviour, SocialLoginRequest, SocialLoginResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(socialLoginBehaviour, request);
	}
	
	@Override
	public SetSocialAccountResponse setSocialAccount(SetSocialAccountRequest request) {
		BehaviourExecutor<SetSocialAccountBehaviour, SetSocialAccountRequest, SetSocialAccountResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(setSocialAccountBehavior, request);
	}
	
	@Override
	public ProfileResponse getProfile(ProfileRequest request) {
		BehaviourExecutor<GetProfileBehaviour, ProfileRequest, ProfileResponse> executor = new BehaviourExecutor<GetProfileBehaviour, ProfileRequest, ProfileResponse>();
		return executor.executeBehaviour(getProfileBehaviour, request);
	}

	@Override
	public AddressesResponse getAddresses(AddressesRequest request) {
		BehaviourExecutor<GetAddressesBehaviour, AddressesRequest, AddressesResponse> executor = new BehaviourExecutor<GetAddressesBehaviour, AddressesRequest, AddressesResponse>();
		return executor.executeBehaviour(getAddressesBehaviour, request);
	}

	@Override
	public UpdateUserProfileResponse updateProfile(UpdateUserProfileRequest request) {
		BehaviourExecutor<UpdateUserProfileBehaviour, UpdateUserProfileRequest, UpdateUserProfileResponse> executor = new BehaviourExecutor<UpdateUserProfileBehaviour, UpdateUserProfileRequest, UpdateUserProfileResponse>();
		return executor.executeBehaviour(updateUserProfileBehaviour, request);
	}

	@Override
	public MessagesResponse getMessages(MessagesRequest request) {
		BehaviourExecutor<GetMessagesBehaviour, MessagesRequest, MessagesResponse> executor = new BehaviourExecutor<GetMessagesBehaviour, MessagesRequest, MessagesResponse>();
		return executor.executeBehaviour(getMessagesBehaviour, request);
	}

	@Override
	public BalanceResponse getBalance(BalanceRequest request) {
		BehaviourExecutor<GetBalanceBehaviour, BalanceRequest, BalanceResponse> executor = new BehaviourExecutor<GetBalanceBehaviour, BalanceRequest, BalanceResponse>();
		return executor.executeBehaviour(getBalanceBehaviour, request);
	}

	@Override
	public StatementByDateResponse getStatementByDate(StatementByDateRequest request) {
		BehaviourExecutor<GetStatementByDateBehaviour, StatementByDateRequest, StatementByDateResponse> executor = new BehaviourExecutor<GetStatementByDateBehaviour, StatementByDateRequest, StatementByDateResponse>();
		return executor.executeBehaviour(getStatementByDateBehaviour, request);
	}

	@Override
	public AddFlightActivityResponse addFlightActivity(AddFlightActivityRequest request) {
		BehaviourExecutor<AddFlightActivityBehaviour, AddFlightActivityRequest, AddFlightActivityResponse> executor = new BehaviourExecutor<AddFlightActivityBehaviour, AddFlightActivityRequest, AddFlightActivityResponse>();
		return executor.executeBehaviour(addFlightActivityBehaviour, request);
	}

	@Override
	public UpdatePinResponse updatePin(UpdatePinRequest request) {
		BehaviourExecutor<UpdatePinBehaviour, UpdatePinRequest, UpdatePinResponse> executor = new BehaviourExecutor<UpdatePinBehaviour, UpdatePinRequest, UpdatePinResponse>();
		return executor.executeBehaviour(updatePinBehaviour, request);
	}

	@Override
	public SocialAccountsResponse getSocialAccounts(SocialAccountsRequest request) {
		BehaviourExecutor<GetSocialAccountsBehaviour, SocialAccountsRequest, SocialAccountsResponse> executor = new BehaviourExecutor<GetSocialAccountsBehaviour, SocialAccountsRequest, SocialAccountsResponse>();
		return executor.executeBehaviour(getSocialAccountsBehaviour, request);
	}

	@Override
	public SubscribeResponse subscribeByCustomer(SubscribeRequest request) {
		BehaviourExecutor<SubscribeBehaviour, SubscribeRequest, SubscribeResponse> executor = new BehaviourExecutor<SubscribeBehaviour, SubscribeRequest, SubscribeResponse>();
		return executor.executeBehaviour(subscribeBehaviour, request);
	}

	@Override
	public UpdateNicknameResponse updateNickname(UpdateNicknameRequest request) {
		BehaviourExecutor<UpdateNicknameBehaviour, UpdateNicknameRequest, UpdateNicknameResponse> executor = new BehaviourExecutor<UpdateNicknameBehaviour, UpdateNicknameRequest, UpdateNicknameResponse>();
		return executor.executeBehaviour(updateNicknameBehaviour, request);
	}

	@Override
	public RetrievePinResponse retrievePinByCustomerNumber(RetrievePinRequest request) {
		BehaviourExecutor<RetrievePinByCustomerNumberBehaviour, RetrievePinRequest, RetrievePinResponse> executor = new BehaviourExecutor<RetrievePinByCustomerNumberBehaviour, RetrievePinRequest, RetrievePinResponse>();
		return executor.executeBehaviour(retrievePinByCustomerNumberBehaviour, request);
	}

	@Override
	public RetrievePinResponse retrievePinByNickname(RetrievePinRequest request) {
		BehaviourExecutor<RetrievePinByNicknameBehaviour, RetrievePinRequest, RetrievePinResponse> executor = new BehaviourExecutor<RetrievePinByNicknameBehaviour, RetrievePinRequest, RetrievePinResponse>();
		return executor.executeBehaviour(retrievePinByNicknameBehaviour, request);
	}

	@Override
	public MillemigliaSendEmailResponse sendNewRegistrationEmail(MillemigliaSendEmailRequest request) {
		BehaviourExecutor<SendNewRegistrationEmailBehaviour, MillemigliaSendEmailRequest, MillemigliaSendEmailResponse> executor = new BehaviourExecutor<SendNewRegistrationEmailBehaviour, MillemigliaSendEmailRequest, MillemigliaSendEmailResponse>();
		return executor.executeBehaviour(sendNewRegistrationEmailBehaviour, request);
	}

	@Override
	public MillemigliaSendEmailResponse sendNewMailingListRegistrationMail(SendNewMailingListRegistrationMailRequest request) {
		BehaviourExecutor<SendNewMailingListRegistrationMailBehaviour, SendNewMailingListRegistrationMailRequest, MillemigliaSendEmailResponse> executor = new BehaviourExecutor<SendNewMailingListRegistrationMailBehaviour, SendNewMailingListRegistrationMailRequest, MillemigliaSendEmailResponse>();
		return executor.executeBehaviour(sendNewMailingListRegistrationMailBehaviour, request);
	}

	@Override
	public MailinglistRegistrationResponse registerToMailinglist(MailinglistRegistrationRequest request) {
		BehaviourExecutor<MailinglistRegistrationBehaviour, MailinglistRegistrationRequest, MailinglistRegistrationResponse> executor = new BehaviourExecutor<MailinglistRegistrationBehaviour, MailinglistRegistrationRequest, MailinglistRegistrationResponse>();
		
		MailinglistRegistrationResponse mailinglistRegistrationResponse = executor.executeBehaviour(mailinglistRegistrationBehaviour, request);

		if(mailinglistRegistrationResponse != null && mailinglistRegistrationResponse.isRegistrationIsSuccessful()){


			if ( request.getAction().value().equals("Register")){

				String body = request.getBodyWelcomeMail();
				SendNewMailingListRegistrationMailRequest sendEmailRequest = new SendNewMailingListRegistrationMailRequest();
				sendEmailRequest.setCustomer(request.getCustomerProfile());
				sendEmailRequest.setMailBody(body);
				sendEmailRequest.setMailSubject(request.getSubjectMail());
				MillemigliaSendEmailResponse sendNewMailingListRegistrationMail = sendNewMailingListRegistrationMail(sendEmailRequest);
				if (sendNewMailingListRegistrationMail != null && sendNewMailingListRegistrationMail.isMailSent()) {
					logger.debug("[NewsletterSubscribeEmail] - success");
				} else {
					logger.error("[NewsletterSubscribeEmail] - fail");
				}
			}else{
				logger.debug("[NewsletterDeleteEmail] - success");
			}
		}

		return mailinglistRegistrationResponse;
	}

	@Override
	public DeletePanResponse deletePan(DeletePanRequest request) {
		BehaviourExecutor<DeletePanBehaviour, DeletePanRequest, DeletePanResponse> executor = new BehaviourExecutor<DeletePanBehaviour, DeletePanRequest, DeletePanResponse>();
		return executor.executeBehaviour(deletePanBehaviour, request);
	}
	
	@Override
	public RemoveSocialAccountsResponse removeUidGigya(RemoveSocialAccountsRequest request) {
		BehaviourExecutor<RemoveUidGigyaBehaviour, RemoveSocialAccountsRequest, RemoveSocialAccountsResponse> executor = new BehaviourExecutor<RemoveUidGigyaBehaviour, RemoveSocialAccountsRequest, RemoveSocialAccountsResponse>();
		return executor.executeBehaviour(removeUidGigyaBehaviour, request);
	}

	@Override
	public CreditCardsListResponse getCreditCardList(CreditCardsListRequest request) {
		BehaviourExecutor<CreditCardsListBehaviour, CreditCardsListRequest, CreditCardsListResponse> executor = new BehaviourExecutor<CreditCardsListBehaviour, CreditCardsListRequest, CreditCardsListResponse>();
		return executor.executeBehaviour(creditCardsListBehaviour, request);
	}
	
	@Override
	public MMPaybackAccrualResponse paybackAccrual(MMPaybackAccrualRequest request) {
		BehaviourExecutor<PaybackAccrualBehaviour, MMPaybackAccrualRequest, MMPaybackAccrualResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(paybackAccrualBehaviour, request);
	}
}