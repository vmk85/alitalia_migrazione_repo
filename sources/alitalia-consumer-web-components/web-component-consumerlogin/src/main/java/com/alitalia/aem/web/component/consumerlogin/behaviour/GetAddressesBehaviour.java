package com.alitalia.aem.web.component.consumerlogin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.AddressesRequest;
import com.alitalia.aem.common.messages.home.AddressesResponse;
import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;

@Component(immediate = true, metatype = false)
@Service(value = GetAddressesBehaviour.class)
public class GetAddressesBehaviour extends Behaviour<AddressesRequest, AddressesResponse> {

	@Reference
	private MyPersonalAreaMilleMigliaService personalAreaMilleMigliaService;

	@Override
	public AddressesResponse executeOrchestration(AddressesRequest request) {
		if(request == null) throw new IllegalArgumentException("Request is null.");
		return personalAreaMilleMigliaService.getAddresses(request);
	}
}