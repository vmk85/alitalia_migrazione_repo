package com.alitalia.aem.web.component.consumerlogin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.UpdatePinRequest;
import com.alitalia.aem.common.messages.home.UpdatePinResponse;
import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;

@Component(immediate = true, metatype = false)
@Service(value = UpdatePinBehaviour.class)
public class UpdatePinBehaviour extends Behaviour<UpdatePinRequest, UpdatePinResponse> {

	@Reference
	private MyPersonalAreaMilleMigliaService personalAreaMilleMigliaService;

	@Override
	public UpdatePinResponse executeOrchestration(UpdatePinRequest request) {
		if(request == null) throw new IllegalArgumentException("Request is null.");
		return personalAreaMilleMigliaService.updatePin(request);
	}
}