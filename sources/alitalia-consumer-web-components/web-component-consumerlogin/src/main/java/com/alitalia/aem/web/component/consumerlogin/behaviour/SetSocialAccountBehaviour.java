package com.alitalia.aem.web.component.consumerlogin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.SetSocialAccountRequest;
import com.alitalia.aem.common.messages.home.SetSocialAccountResponse;
import com.alitalia.aem.common.messages.home.UserFromSocialRequest;
import com.alitalia.aem.common.messages.home.UserFromSocialResponse;
import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;

@Component(immediate = true, metatype = false)
@Service(value = SetSocialAccountBehaviour.class)
public class SetSocialAccountBehaviour 
		extends Behaviour<SetSocialAccountRequest, SetSocialAccountResponse> {
	
	@Reference
	private MyPersonalAreaMilleMigliaService personalAreaMilleMigliaService;

	@Override
	public SetSocialAccountResponse executeOrchestration(SetSocialAccountRequest request) {
		if(request == null)
			throw new IllegalArgumentException("Request is null.");
		
		// TODO [social-login] we should perform gigya signature validation, which ws method?
		// (current site expose it as https://mm.alitalia.com/IT_IT/MM/HelperGigya.aspx?op=verifyTheSignature)
		// ... request.getGigyaSignature()
		
		SetSocialAccountResponse response = personalAreaMilleMigliaService.setSocialAccount(request);
		if(response.isSucceeded()){
			UserFromSocialRequest userRequest = new UserFromSocialRequest();
			userRequest.setGigyaId(request.getGigyaId());
			userRequest.setSid(request.getSid());
			userRequest.setTid(request.getTid());
			UserFromSocialResponse userResponse = 
					personalAreaMilleMigliaService.getMMCode(userRequest);
			response.setSsoToken(userResponse.getSsoToken());
		}
		
		// TODO [social-login] should we also notify SiteUID association to gigya with a separate ws operation? which one?
		
		return response;
	}

}
