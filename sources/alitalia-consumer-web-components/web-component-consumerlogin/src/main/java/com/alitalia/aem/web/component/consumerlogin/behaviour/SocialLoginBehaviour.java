package com.alitalia.aem.web.component.consumerlogin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.SocialLoginRequest;
import com.alitalia.aem.common.messages.home.SocialLoginResponse;
import com.alitalia.aem.common.messages.home.UserFromSocialRequest;
import com.alitalia.aem.common.messages.home.UserFromSocialResponse;
import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;

@Component(immediate = true, metatype = false)
@Service(value = SocialLoginBehaviour.class)
public class SocialLoginBehaviour 
		extends Behaviour<SocialLoginRequest, SocialLoginResponse> {
	
	@Reference
	private MyPersonalAreaMilleMigliaService personalAreaMilleMigliaService;

	@Override
	public SocialLoginResponse executeOrchestration(SocialLoginRequest request) {
		if(request == null)
			throw new IllegalArgumentException("Request is null.");
		
		SocialLoginResponse socialLoginResponse = new SocialLoginResponse();
		
		// TODO [social-login] we should perform gigya signature validation, which ws method?
		// (current site expose it as https://mm.alitalia.com/IT_IT/MM/HelperGigya.aspx?op=verifyTheSignature)
		// ... request.getGigyaSignature()
		
		UserFromSocialRequest userRequest = new UserFromSocialRequest();
		userRequest.setGigyaId(request.getGigyaId());
		userRequest.setSid(request.getSid());
		userRequest.setTid(request.getTid());
		UserFromSocialResponse userResponse = 
				personalAreaMilleMigliaService.getMMCode(userRequest);
		
		socialLoginResponse.setLoginSuccessful(userResponse.isSuccessful());
		socialLoginResponse.setUserCode(userResponse.getUserCode());
		socialLoginResponse.setPin(userResponse.getPin());
		socialLoginResponse.setSid(request.getSid());
		socialLoginResponse.setTid(request.getTid());
		socialLoginResponse.setSsoToken(userResponse.getSsoToken());
		return socialLoginResponse;
	}

}
