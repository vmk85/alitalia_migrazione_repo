@echo off
SetLocal EnableDelayedExpansion

set HOST=%1
set PORT=%2
set USER=%3
set PASSWORD=%4
set PROJECTS_HOME=%5
set CURL="C:\Program Files\curl-7.33.0-win64-ssl-sspi"\curl.exe

set DEPLOY_DIR=%PROJECTS_HOME%\localdev_deploy
set BUNDLE_DIR=%DEPLOY_DIR%\bundle
set BUNDLE_LIST_FILE=%DEPLOY_DIR%\bundle-list.csv

set LOG_FILE=%DEPLOY_DIR%\log.out

echo Stopping all bundles
echo ######################## %date:~6,4%/%date:~3,2%/%date:~0,2% %TIME% - Stopping bundle ########################> %LOG_FILE%

for /f "tokens=1-2* delims=;" %%a in (%BUNDLE_LIST_FILE%) do (
	set JAR_NAME=%%a
	set SYMBOLIC_NAME=%%b
	set BUNDLE_START_LEVEL=%%c
  
	echo --- %date:~6,4%/%date:~3,2%/%date:~0,2% %TIME% - Stopping bundle !JAR_NAME! --->> %LOG_FILE%
  
	echo Stopping bundle !JAR_NAME! >> %LOG_FILE% 
	echo Stopping bundle !JAR_NAME!
  
	set CURL_LOG="curl -F action=stop http://%HOST%:%PORT%/system/console/bundles/!SYMBOLIC_NAME! -u *****:***** -f"
	echo Executing !CURL_LOG! >> %LOG_FILE%
  
	%CURL% -F action=stop http://%HOST%:%PORT%/system/console/bundles/!SYMBOLIC_NAME! -u %USER%:%PASSWORD% -f >> %LOG_FILE% 2>&1

	echo Return code !ERRORLEVEL! >> %LOG_FILE%
)

echo All bundles stopped
echo ######################## %date:~6,4%/%date:~3,2%/%date:~0,2% %TIME% - All bundles stopped ########################>> %LOG_FILE% 
	
echo Starting uninstall
echo ######################## %date:~6,4%/%date:~3,2%/%date:~0,2% %TIME% - Starting uninstall ########################>> %LOG_FILE%

for /f "tokens=1-2* delims=;" %%a in (%BUNDLE_LIST_FILE%) do (
	set JAR_NAME=%%a
	set SYMBOLIC_NAME=%%b
	set BUNDLE_START_LEVEL=%%c
 
	echo Uninstalling bundle !JAR_NAME! >> %LOG_FILE% 
	echo Uninstalling bundle !JAR_NAME!

	set CURL_LOG="curl -F action=uninstall http://%HOST%:%PORT%/system/console/bundles/!SYMBOLIC_NAME! -u *****:***** -f
	echo !CURL_LOG! >> %LOG_FILE%"
  
	%CURL% -F action=uninstall http://%HOST%:%PORT%/system/console/bundles/!SYMBOLIC_NAME! -u %USER%:%PASSWORD% -f >> %LOG_FILE% 2>&1	
	
	echo Return code !ERRORLEVEL! >> %LOG_FILE%
)

echo All bundles uninstalled
echo ######################## %date:~6,4%/%date:~3,2%/%date:~0,2% %TIME% - All bundles uninstalled ########################>> %LOG_FILE% 

echo Starting installation
echo ######################## %date:~6,4%/%date:~3,2%/%date:~0,2% %TIME% - Starting installation ########################>> %LOG_FILE%

for /f "tokens=1-2* delims=;" %%a in (%BUNDLE_LIST_FILE%) do (		
	set JAR_NAME=%%a
	set SYMBOLIC_NAME=%%b
	set BUNDLE_START_LEVEL=%%c
	
	echo Installing bundle !JAR_NAME! >> %LOG_FILE% 
	echo Installing bundle !JAR_NAME!
	
	set CURL_LOG="curl -X POST -F action=install http://%HOST%:%PORT%/system/console/bundles -F bundlestartlevel=!BUNDLE_START_LEVEL! -F bundlefile=@%BUNDLE_DIR%\!JAR_NAME! -u *****:***** -f"
	echo !CURL_LOG! >> %LOG_FILE%

	%CURL% -X POST -F action=install http://%HOST%:%PORT%/system/console/bundles -F bundlestartlevel=!BUNDLE_START_LEVEL! -F bundlefile=@%BUNDLE_DIR%\!JAR_NAME! -u %USER%:%PASSWORD% -f >> %LOG_FILE% 2>&1	
	
	echo Return code !ERRORLEVEL! >> %LOG_FILE%
)

echo All bundles installed
echo ######################## %date:~6,4%/%date:~3,2%/%date:~0,2% %TIME% - All bundles installed ########################>> %LOG_FILE% 

echo Starting activation
echo ######################## %date:~6,4%/%date:~3,2%/%date:~0,2% %TIME% - Starting activation ########################>> %LOG_FILE% 

for /f "tokens=1-2* delims=;" %%a in (%BUNDLE_LIST_FILE%) do (
	set JAR_NAME=%%a
	set SYMBOLIC_NAME=%%b
	set BUNDLE_START_LEVEL=%%c
	
	echo Starting bundle !JAR_NAME! >> %LOG_FILE% 
	echo Starting bundle !JAR_NAME!
	
	set CURL_LOG="curl -F action=start http://%HOST%:%PORT%/system/console/bundles/!SYMBOLIC_NAME! -u *****:***** -f"
	echo !CURL_LOG! >> %LOG_FILE%	
	%CURL% -F action=start http://%HOST%:%PORT%/system/console/bundles/!SYMBOLIC_NAME! -u %USER%:%PASSWORD% -f >> %LOG_FILE% 2>&1	
	echo Return code !ERRORLEVEL! >> %LOG_FILE%
	
	rem set /p X= [Press Enter]
)

echo Activation completed
echo ######################## %date:~6,4%/%date:~3,2%/%date:~0,2% %TIME% - Activation completed ########################>> %LOG_FILE% 



