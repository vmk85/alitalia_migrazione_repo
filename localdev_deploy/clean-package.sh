#!/bin/sh

# MODE=[alitalia|alitaliatrade|all]
MODE=$1
# INSTANCE=[author|publish]
INSTANCE=$2
HOST=$3
PORT=$4
USER=$5
PASSWORD=$6
PROJECTS_HOME=$7
LOG=$8
PACKAGE_UNINSTALL_SLEEP=$9
PACKAGE_REMOVE_SLEEP=${10}
PACKAGE_UPLOAD_SLEEP=${11}
PACKAGE_INSTALL_SLEEP=${12}
PHASE_SLEEP=${13}
GROUP=${14}

CURL=curl

DEPLOY_DIR=$PROJECTS_HOME/localdev_deploy
LOG_DIR=$DEPLOY_DIR/log
PACKAGE_DIR=$DEPLOY_DIR/package
PACKAGE_LIST_FILE=$DEPLOY_DIR/package-list.csv

if [ "x$LOG" == "x" ]; then
	LOG_FILE=$LOG_DIR/deploy-package.log
else
	LOG_FILE=$LOG_DIR/$LOG
fi

if [ "x$PACKAGE_UNINSTALL_SLEEP" == "x" ]; then
	PACKAGE_UNINSTALL_SLEEP=1s
fi

if [ "x$PACKAGE_REMOVE_SLEEP" == "x" ]; then
	PACKAGE_REMOVE_SLEEP=1s
fi

if [ "x$PACKAGE_UPLOAD_SLEEP" == "x" ]; then
	PACKAGE_UPLOAD_SLEEP=1s
fi

if [ "x$PACKAGE_INSTALL_SLEEP" == "x" ]; then
	PACKAGE_INSTALL_SLEEP=2s
fi

if [ "x$PHASE_SLEEP" == "x" ]; then
	PHASE_SLEEP=5s
fi

TEMP_CSV=tmp-`date +%s`.csv

tr -d '\015' < $PACKAGE_LIST_FILE > $TEMP_CSV
echo "" >> $TEMP_CSV

OLDIFS=$IFS
IFS=";"

install_pkg () {
	PACKAGE_FILE_NAME_UN=$1
	PACKAGE_NAME_UN=$2
	
	echo Installing package $PACKAGE_NAME_UN >> $LOG_FILE
	echo Installing package $PACKAGE_NAME_UN
	
	CURL_LOG="curl -u *****:***** -F -F cmd=inst -F name=$PACKAGE_NAME_UN -F force=true http://$HOST:$PORT/crx/packmgr/service.jsp"
	echo $CURL_LOG >> $LOG_FILE
	
	$CURL -F cmd=inst -F name=$PACKAGE_NAME_UN http://$HOST:$PORT/crx/packmgr/service.jsp -u $USER:$PASSWORD -f >> $LOG_FILE 2>&1	
	
	echo "" >> $LOG_FILE
	echo Return code $? >> $LOG_FILE
}

upload_pkg () {
	PACKAGE_FILE_NAME_UN=$1
	PACKAGE_NAME_UN=$2
	
	echo Uploading package $PACKAGE_NAME_UN >> $LOG_FILE
	echo Uploading package $PACKAGE_NAME_UN
	
	CURL_LOG="curl -u *****:***** -F file=@$PACKAGE_DIR/$PACKAGE_FILE_NAME_UN -F name=$PACKAGE_NAME_UN -F force=true -F install=false http://$HOST:$PORT/crx/packmgr/service.jsp"
	echo $CURL_LOG >> $LOG_FILE
	
	$CURL -F file=@$PACKAGE_DIR/$PACKAGE_FILE_NAME_UN -F name=$PACKAGE_NAME_UN -F force=true -F install=false http://$HOST:$PORT/crx/packmgr/service.jsp -u $USER:$PASSWORD -f >> $LOG_FILE 2>&1	
	
	echo "" >> $LOG_FILE
	echo Return code $? >> $LOG_FILE
}

uninstall_pkg () {
	PACKAGE_FILE_NAME_UN=$1
	PACKAGE_NAME_UN=$2
	
	echo Uninstalling package $PACKAGE_NAME_UN >> $LOG_FILE
	echo Uninstalling package $PACKAGE_NAME_UN
	
	CURL_LOG="curl -F cmd=uninst -F name=$PACKAGE_NAME_UN http://$HOST:$PORT/crx/packmgr/service.jsp -u *****:***** -f"
	echo $CURL_LOG >> $LOG_FILE
	
	$CURL -F cmd=uninst -F name=$PACKAGE_NAME_UN http://$HOST:$PORT/crx/packmgr/service.jsp -u $USER:$PASSWORD -f >> $LOG_FILE 2>&1	
	
	echo "" >> $LOG_FILE
	echo Return code $? >> $LOG_FILE
}

remove_pkg () {
	PACKAGE_FILE_NAME_UN=$1
	PACKAGE_NAME_UN=$2
	
	echo Removing package $PACKAGE_NAME_UN >> $LOG_FILE
	echo Removing package $PACKAGE_NAME_UN
	
	CURL_LOG="curl -F cmd=rm -F name=$PACKAGE_NAME_UN http://$HOST:$PORT/crx/packmgr/service.jsp -u *****:***** -f"
	echo $CURL_LOG >> $LOG_FILE
	
	$CURL -F cmd=rm -F name=$PACKAGE_NAME_UN http://$HOST:$PORT/crx/packmgr/service.jsp -u $USER:$PASSWORD -f >> $LOG_FILE 2>&1	
	
	echo "" >> $LOG_FILE
	echo Return code $? >> $LOG_FILE
}

echo Starting installation for $INSTANCE [$HOST:$PORT]
echo ------------------------- `date +%d%m%Y%H%M%S` - Starting installation for $INSTANCE [$HOST:$PORT] ------------------------- > $LOG_FILE

echo "-------------- (pkgs uninstallation) --------------"
while read -r PACKAGE_FILE_NAME PACKAGE_NAME PROJECT TYPE
do

	if [ $PROJECT == "common" ]; then
		uninstall_pkg $PACKAGE_FILE_NAME $PACKAGE_NAME		
	else
		if [ $INSTANCE == "author" ]; then
			if [ $PROJECT == $MODE ] || [ $MODE == "all" ]; then
				uninstall_pkg $PACKAGE_FILE_NAME $PACKAGE_NAME
			fi
		else
			if [ $PROJECT == $MODE ] || [ $MODE == "all" ]; then
				if [ "$TYPE" == "app" ]; then
					uninstall_pkg $PACKAGE_FILE_NAME $PACKAGE_NAME
				else
					echo Running on $INSTANCE - No contents uninstalled
					echo Running on $INSTANCE - No contents uninstalled >> $LOG_FILE
				fi
			fi
		fi
	fi
	sleep $PACKAGE_UNINSTALL_SLEEP
done < $TEMP_CSV

echo "-------------- (pkgs removal) --------------"
sleep $PHASE_SLEEP

while read -r PACKAGE_FILE_NAME PACKAGE_NAME PROJECT TYPE
do
	if [ $PROJECT == "common" ]; then
		remove_pkg $PACKAGE_FILE_NAME $PACKAGE_NAME		
	else
		if [ $INSTANCE == "author" ]; then
			if [ $PROJECT == $MODE ] || [ $MODE == "all" ]; then
				remove_pkg $PACKAGE_FILE_NAME $PACKAGE_NAME
			fi
		else
			if [ $PROJECT == $MODE ] || [ $MODE == "all" ]; then
				if [ "$TYPE" == "app" ]; then
					remove_pkg $PACKAGE_FILE_NAME $PACKAGE_NAME
				else
					echo Running on $INSTANCE - No contents removed
					echo Running on $INSTANCE - No contents removed >> $LOG_FILE
				fi
			fi
		fi
	fi
	sleep $PACKAGE_REMOVE_SLEEP
done < $TEMP_CSV

rm $TEMP_CSV

echo All packages on $INSTANCE [$HOST] installed
echo -------------------------  `date +%d%m%Y%H%M%S` - All packages on $INSTANCE [$HOST:$PORT] processed ------------------------- >> $LOG_FILE


