@echo off
SetLocal EnableDelayedExpansion

set VERSION=1.1-SNAPSHOT

rem MODE=[alitalia|alitaliatrade|all]
set MODE=%1
rem INSTANCE=[author|publish]
set INSTANCE=%2
set HOST=%3
set PORT=%4
set USER=%5
set PASSWORD=%6
set PROJECTS_HOME=%7
set LOG=%8

set CURL=curl.exe

set DEPLOY_DIR=%PROJECTS_HOME%\localdev_deploy
set LOG_DIR=%DEPLOY_DIR%\log
set PACKAGE_DIR=%DEPLOY_DIR%\package
set PACKAGE_LIST_FILE=%DEPLOY_DIR%\package-list.2.0.csv

if "x%LOG%" == "x" (
	set LOG_FILE=%LOG_DIR%\deploy-package.log
) else (
	set LOG_FILE=%LOG_DIR%\%LOG%
)

echo Starting uploading for %INSTANCE% [%HOST%:%PORT%]
echo ######################## %date:~6,4%/%date:~3,2%/%date:~0,2% %TIME% - Starting uploading for %INSTANCE% [%HOST%:%PORT%] ########################> %LOG_FILE%

for /f "tokens=1-3* delims=;" %%a in (%PACKAGE_LIST_FILE%) do (
	set PACKAGE_NAME=%%a
	set PKG_CTG=%%b
	set TYPE=%%c
	
	set PACKAGE_FILE_NAME=!PACKAGE_NAME!-%VERSION%.zip
	
	if "%MODE%" == "all" (
		if "%INSTANCE%" == "author" (			
				call:upload !PACKAGE_FILE_NAME! !PACKAGE_NAME!
		) else (
			if "!TYPE!" == "app" (
				call:upload !PACKAGE_FILE_NAME! !PACKAGE_NAME!
			) else (
				echo Running on %INSTANCE% - No contents uploaded
				echo Running on %INSTANCE% - No contents uploaded>> %LOG_FILE% 
			)
		)
	) else (
		if "!PKG_CTG!" == "common" (
			call:upload !PACKAGE_FILE_NAME! !PACKAGE_NAME!		
		)
		
		if "%INSTANCE%" == "author" (
			if "!PKG_CTG!" == "%MODE%" (
				call:upload !PACKAGE_FILE_NAME! !PACKAGE_NAME!
			)
		) else (
			if "!PKG_CTG!" == "%MODE%" (
				if "!TYPE!" == "app" (
					call:upload !PACKAGE_FILE_NAME! !PACKAGE_NAME!
				) else (
					echo Running on %INSTANCE% - No contents uploaded
					echo Running on %INSTANCE% - No contents uploaded>> %LOG_FILE% 
				)
			)
		)
	)
	timeout 40 > NUL
	
)

echo All packages on %INSTANCE% [%HOST%] uploaded
echo ######################## %date:~6,4%/%date:~3,2%/%date:~0,2% %TIME% - All packages on %INSTANCE% [%HOST%:%PORT%] uploaded ########################>> %LOG_FILE% 


echo Starting uploading for %INSTANCE% [%HOST%:%PORT%]
echo ######################## %date:~6,4%/%date:~3,2%/%date:~0,2% %TIME% - Starting uploading for %INSTANCE% [%HOST%:%PORT%] ########################>> %LOG_FILE%

for /f "tokens=1-3* delims=;" %%a in (%PACKAGE_LIST_FILE%) do (
	set PACKAGE_NAME=%%a
	set PKG_CTG=%%b
	set TYPE=%%c
	
	set PACKAGE_FILE_NAME=!PACKAGE_NAME!-%VERSION%.zip

	if "%MODE%" == "all" (
		if "%INSTANCE%" == "author" (			
			rem call:install !PACKAGE_FILE_NAME! !PACKAGE_NAME!
		) else (
			if "!TYPE!" == "app" (
			rem call:install !PACKAGE_FILE_NAME! !PACKAGE_NAME!
			) else (
				echo Running on %INSTANCE% - No contents installed
				echo Running on %INSTANCE% - No contents installed>> %LOG_FILE% 
			)
		)
	) else (
		if "!PKG_CTG!" == "common" (
			rem call:install !PACKAGE_FILE_NAME! !PACKAGE_NAME!		
		)
		
		if "%INSTANCE%" == "author" (
			if "!PKG_CTG!" == "%MODE%" (
				rem call:install !PACKAGE_FILE_NAME! !PACKAGE_NAME!
			)
		) else (
			if "!PKG_CTG!" == "%MODE%" (
				if "!TYPE!" == "app" (
					rem call:install !PACKAGE_FILE_NAME! !PACKAGE_NAME!
				) else (
					echo Running on %INSTANCE% - No contents installed
					echo Running on %INSTANCE% - No contents installed>> %LOG_FILE% 
				)
			)
		)
	)
	rem timeout 45 > NUL
	
)

echo All packages on %INSTANCE% [%HOST%] installed
echo ######################## %date:~6,4%/%date:~3,2%/%date:~0,2% %TIME% - All packages on %INSTANCE% [%HOST%:%PORT%] installed ########################>> %LOG_FILE% 


:upload 
	set PACKAGE_FILE_NAME_UN=%1
	set PACKAGE_NAME_UN=%2
	
	echo Uploading package !PACKAGE_NAME_UN! >> %LOG_FILE% 
	echo Uploading package !PACKAGE_NAME_UN!
	
	set CURL_LOG="curl -u *****:***** -F file=@%PACKAGE_DIR%\%PACKAGE_FILE_NAME_UN% -F name=%PACKAGE_NAME_UN% -F force=true -F install=false http://%HOST%:%PORT%/crx/packmgr/service.jsp
	echo !CURL_LOG! >> %LOG_FILE%"
  
	%CURL% -F file=@%PACKAGE_DIR%\%PACKAGE_FILE_NAME_UN% -F name=%PACKAGE_NAME_UN% -F force=true -F install=false http://%HOST%:%PORT%/crx/packmgr/service.jsp -u %USER%:%PASSWORD% -f >> %LOG_FILE% 2>&1	
	
	echo Return code !ERRORLEVEL! >> %LOG_FILE%	
goto:eof

:install 
	set PACKAGE_FILE_NAME_UN=%1
	set PACKAGE_NAME_UN=%2
	
	echo Installing package !PACKAGE_NAME_UN! >> %LOG_FILE% 
	echo Installing package !PACKAGE_NAME_UN!
	
	set CURL_LOG="curl -F action=update cmd=inst -F name=%PACKAGE_NAME_UN% http://%HOST%:%PORT%/crx/packmgr/service.jsp -u *****:***** "
	echo !CURL_LOG! >> %LOG_FILE%"
  
	%CURL% -F action=update -F name=%PACKAGE_NAME_UN%  http://%HOST%:%PORT%/crx/packmgr/service.jsp -u %USER%:%PASSWORD% -f >> %LOG_FILE% 2>&1	
	
	echo Return code !ERRORLEVEL! >> %LOG_FILE%	
goto:eof
