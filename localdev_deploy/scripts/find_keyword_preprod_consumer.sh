#!/bin/bash

IPS=("52.28.195.12" "52.28.234.237" "52.28.237.98")
HOSTNAMES=("alitalia-preprod-publish1eucentral1" "alitalia-preprod-publish2eucentral1" "alitalia-preprod-publish3eucentral1")
PASSWD="aUjghR4<Jbe7<"

if [ "x$1" == "x" ]; then
	echo "Inserire parte della parola chiave:"
	read JSESSIONID
else
	JSESSIONID=$1
fi

re='^[0-9]+$'

i=0
rows=0
while [[ $rows == 0 && $i -lt ${#IPS[@]} ]]
do
	rows=`sshpass -p $PASSWD ssh jailed-user@${IPS[i]} "grep $JSESSIONID /var/log/crx/portals-consumer.log | wc -l"`
	if [[ $rows =~ $re && $rows != 0 ]]
	then
		echo "${HOSTNAMES[i]} IP: ${IPS[i]}"
	fi
	i=$((i+1))
done

if ! [[ $rows =~ $re && $rows != 0 ]]
then
	echo "Not found"
fi
