#!/bin/bash

IPS=("54.93.32.220" "52.28.188.112" "52.28.7.164" "52.29.213.54" "52.29.30.72" "52.58.1.247" "52.29.52.226" "52.29.129.98" "52.58.154.141" "52.58.152.137" "52.58.198.182" "52.29.210.235" "52.19.172.109")
HOSTNAMES=("alitalia-prod-publish1eucentral1" "alitalia-prod-publish2eucentral1" "alitalia-prod-publish3eucentral1" "alitalia-prod-publish5eucentral1" "alitalia-prod-publish6eucentral1" "alitalia-prod-publish7eucentral1" "alitalia-prod-publish8eucentral1" "alitalia-prod-publish9eucentral1" "alitalia-prod-publish10eucentral1" "alitalia-prod-publish11eucentral1" "alitalia-prod-publish12eucentral1" "alitalia-prod-publish13eucentral1" "alitalia-prod-publish1euwest1")
PASSWD="rDVR2DAd"

if [ "x$1" == "x" ]; then
	echo "Inserire parte della parola chiave:"
	read JSESSIONID
else
	JSESSIONID=$1
fi

re='^[0-9]+$'

i=0
rows=0
while [[ $rows == 0 && $i -lt ${#IPS[@]} ]]
do
	rows=`sshpass -p $PASSWD ssh jailed-user@${IPS[i]} "grep $JSESSIONID /var/log/crx/portals-consumer.log | wc -l"`
	if [[ $rows =~ $re && $rows != 0 ]]
	then
		echo "${HOSTNAMES[i]} IP: ${IPS[i]}"
	fi
	i=$((i+1))
done

if ! [[ $rows =~ $re && $rows != 0 ]]
then
	echo "Not found"
fi
