#!/bin/sh

export PROJECT=all
export GROUP=com.alitalia.aem
export LOGENV="localdev"
export INSTANCE_TYPE=author
export PORT=11111
export IP=localhost
export PROJECTS_HOME=/Sviluppo/ProgettiSVN-Reply/svn_repository/repos/sources/trunk
export USER=admin
export PACKAGE_UNINSTALL_SLEEP=1s
export PACKAGE_REMOVE_SLEEP=1s
export PACKAGE_UPLOAD_SLEEP=1s
export PACKAGE_INSTALL_SLEEP=1s
export PHASE_SLEEP=1s

echo "Installing on $INSTANCE_TYPE host(s) in $LOGENV environment"

read -p "Enter admin password: " PASSWORD

bash ./localdev_deploy/deploy-package.sh $PROJECT $INSTANCE_TYPE $IP $PORT $USER $PASSWORD $PROJECTS_HOME "deploy-package-$PROJECT-$LOGENV-$INSTANCE_TYPE-$IP.log" $PACKAGE_UNINSTALL_SLEEP $PACKAGE_REMOVE_SLEEP $PACKAGE_UPLOAD_SLEEP $PACKAGE_INSTALL_SLEEP $PHASE_SLEEP $GROUP