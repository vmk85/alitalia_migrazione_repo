#!/bin/sh

export PROJECT=all
export GROUP=com.alitalia.aem
export LOGENV="cloud-dev"
export INSTANCE_TYPE=publish
export PORT=4503
export IP=52.28.206.97
export PROJECTS_HOME=`pwd`/../..
export USER=admin
export PACKAGE_UNINSTALL_SLEEP=45s
export PACKAGE_REMOVE_SLEEP=5s
export PACKAGE_UPLOAD_SLEEP=5s
export PACKAGE_INSTALL_SLEEP=45s
export PHASE_SLEEP=1s

echo "Installing on $INSTANCE_TYPE host(s) in $LOGENV environment"

read -p "Enter admin password: " PASSWORD

bash ../deploy-package.sh $PROJECT $INSTANCE_TYPE $IP $PORT $USER $PASSWORD $PROJECTS_HOME "deploy-package-$PROJECT-$LOGENV-$INSTANCE_TYPE-$IP.log" $PACKAGE_UNINSTALL_SLEEP $PACKAGE_REMOVE_SLEEP $PACKAGE_UPLOAD_SLEEP $PACKAGE_INSTALL_SLEEP $PHASE_SLEEP $GROUP
