@echo off
SetLocal EnableDelayedExpansion

rem MODE=[alitalia|alitaliatrade|all]
set MODE=%1
rem INSTANCE=[author|publish]
set INSTANCE=%2
set HOST=%3
set PORT=%4
set USER=%5
set PASSWORD=%6
set PROJECTS_HOME=%7
set LOG=%8

set CURL=curl.exe

set DEPLOY_DIR=%PROJECTS_HOME%\localdev_deploy
set LOG_DIR=%DEPLOY_DIR%\log
set PACKAGE_DIR=%DEPLOY_DIR%\package
set PACKAGE_LIST_FILE=%DEPLOY_DIR%\package-list.csv

if "x%LOG%" == "x" (
	set LOG_FILE=%LOG_DIR%\deploy-package.log
) else (
	set LOG_FILE=%LOG_DIR%\%LOG%
)

echo Starting uninstallation for %INSTANCE% [%HOST%:%PORT%]
echo ######################## %date:~6,4%/%date:~3,2%/%date:~0,2% %TIME% - Starting uninstallation for %INSTANCE% [%HOST%:%PORT%] ########################> %LOG_FILE%

for /f "tokens=1-3* delims=;" %%a in (%PACKAGE_LIST_FILE%) do (
	set PACKAGE_FILE_NAME=%%a
	set PACKAGE_NAME=%%b
	set PKG_CTG=%%c
	set TYPE=%%d

	if "%MODE%" == "all" (
		if "%INSTANCE%" == "author" (
			call:uninstall !PACKAGE_FILE_NAME! !PACKAGE_NAME!
		) else (
			if "!TYPE!" == "app" (
				call:uninstall !PACKAGE_FILE_NAME! !PACKAGE_NAME!
			) else (
				echo Running on %INSTANCE% - No contents uninstalled
				echo Running on %INSTANCE% - No contents uninstalled>> %LOG_FILE% 
			)
		)		
	) else (
		if "!PKG_CTG!" == "common" (
			call:uninstall !PACKAGE_FILE_NAME! !PACKAGE_NAME!		
		)
		
		if "%INSTANCE%" == "author" (
			if "!PKG_CTG!" == "%MODE%" (
				call:uninstall !PACKAGE_FILE_NAME! !PACKAGE_NAME!
			)
		) else (
			if "!PKG_CTG!" == "%MODE%" (
				if "!TYPE!" == "app" (
					call:uninstall !PACKAGE_FILE_NAME! !PACKAGE_NAME!
				) else (
					echo Running on %INSTANCE% - No contents uninstalled
					echo Running on %INSTANCE% - No contents uninstalled>> %LOG_FILE% 
				)
			)
		)
	)
	
	timeout 45 > NUL
)

echo All packages on %INSTANCE% [%HOST%] uninstalled
echo ######################## %date:~6,4%/%date:~3,2%/%date:~0,2% %TIME% - All packages on %INSTANCE% [%HOST%:%PORT%] uninstalled ########################>> %LOG_FILE% 

rem set /p X= [Press Enter]


echo Starting deletion for %INSTANCE% [%HOST%:%PORT%]
echo ######################## %date:~6,4%/%date:~3,2%/%date:~0,2% %TIME% - Starting deletion for %INSTANCE% [%HOST%:%PORT%] ########################>> %LOG_FILE%

for /f "tokens=1-3* delims=;" %%a in (%PACKAGE_LIST_FILE%) do (
	set PACKAGE_FILE_NAME=%%a
	set PACKAGE_NAME=%%b
	set PKG_CTG=%%c
	set TYPE=%%d

	if "%MODE%" == "all" (	
		if "%INSTANCE%" == "author" (
			call:remove !PACKAGE_FILE_NAME! !PACKAGE_NAME!
		) else (
			if "!TYPE!" == "app" (
				call:remove !PACKAGE_FILE_NAME! !PACKAGE_NAME!
			) else (
				echo Running on %INSTANCE% - No contents deleted
				echo Running on %INSTANCE% - No contents deleted>> %LOG_FILE% 
			)
		)
	) else (
		if "!PKG_CTG!" == "common" (
			call:remove !PACKAGE_FILE_NAME! !PACKAGE_NAME!		
		)
		
		if "%INSTANCE%" == "author" (
			if "!PKG_CTG!" == "%MODE%" (
				call:remove !PACKAGE_FILE_NAME! !PACKAGE_NAME!
			)
		) else (
			if "!PKG_CTG!" == "%MODE%" (
				if "!TYPE!" == "app" (
					call:remove !PACKAGE_FILE_NAME! !PACKAGE_NAME!
				) else (
					echo Running on %INSTANCE% - No contents deleted
					echo Running on %INSTANCE% - No contents deleted>> %LOG_FILE% 
				)
			)
		)
	)
	timeout 5 > NUL
)

echo All packages on %INSTANCE% [%HOST%] deleted
echo ######################## %date:~6,4%/%date:~3,2%/%date:~0,2% %TIME% - All packages on %INSTANCE% [%HOST%:%PORT%] deleted ########################>> %LOG_FILE% 

	
echo Starting installation for %INSTANCE% [%HOST%:%PORT%]
echo ######################## %date:~6,4%/%date:~3,2%/%date:~0,2% %TIME% - Starting installation for %INSTANCE% [%HOST%:%PORT%] ########################>> %LOG_FILE%

for /f "tokens=1-3* delims=;" %%a in (%PACKAGE_LIST_FILE%) do (
	set PACKAGE_FILE_NAME=%%a
	set PACKAGE_NAME=%%b
	set PKG_CTG=%%c
	set TYPE=%%d

	if "%MODE%" == "all" (
		if "%INSTANCE%" == "author" (			
				call:install !PACKAGE_FILE_NAME! !PACKAGE_NAME!
		) else (
			if "!TYPE!" == "app" (
				call:install !PACKAGE_FILE_NAME! !PACKAGE_NAME!
			) else (
				echo Running on %INSTANCE% - No contents installed
				echo Running on %INSTANCE% - No contents installed>> %LOG_FILE% 
			)
		)
	) else (
		if "!PKG_CTG!" == "common" (
			call:install !PACKAGE_FILE_NAME! !PACKAGE_NAME!		
		)
		
		if "%INSTANCE%" == "author" (
			if "!PKG_CTG!" == "%MODE%" (
				call:install !PACKAGE_FILE_NAME! !PACKAGE_NAME!
			)
		) else (
			if "!PKG_CTG!" == "%MODE%" (
				if "!TYPE!" == "app" (
					call:install !PACKAGE_FILE_NAME! !PACKAGE_NAME!
				) else (
					echo Running on %INSTANCE% - No contents installed
					echo Running on %INSTANCE% - No contents installed>> %LOG_FILE% 
				)
			)
		)
	)
	timeout 45 > NUL
	
)

echo All packages on %INSTANCE% [%HOST%] installed
echo ######################## %date:~6,4%/%date:~3,2%/%date:~0,2% %TIME% - All packages on %INSTANCE% [%HOST%:%PORT%] installed ########################>> %LOG_FILE% 

goto:eof

:install 
	set PACKAGE_FILE_NAME_UN=%1
	set PACKAGE_NAME_UN=%2
	
	echo Installing package !PACKAGE_NAME_UN! >> %LOG_FILE% 
	echo Installing package !PACKAGE_NAME_UN!
	
	set CURL_LOG="curl -u *****:***** -F file=@%PACKAGE_DIR%\%PACKAGE_FILE_NAME_UN% -F name=%PACKAGE_NAME_UN% -F force=true -F install=true http://%HOST%:%PORT%/crx/packmgr/service.jsp
	echo !CURL_LOG! >> %LOG_FILE%"
  
	%CURL% -F file=@%PACKAGE_DIR%\%PACKAGE_FILE_NAME_UN% -F name=%PACKAGE_NAME_UN% -F force=true -F install=true http://%HOST%:%PORT%/crx/packmgr/service.jsp -u %USER%:%PASSWORD% -f >> %LOG_FILE% 2>&1	
	
	echo Return code !ERRORLEVEL! >> %LOG_FILE%	
goto:eof

:uninstall 
	set PACKAGE_FILE_NAME_UN=%1
	set PACKAGE_NAME_UN=%2
	
	echo Uninstalling package !PACKAGE_NAME_UN! >> %LOG_FILE% 
	echo Uninstalling package !PACKAGE_NAME_UN!
	
	set CURL_LOG="curl -u *****:***** -F file=@%PACKAGE_DIR%\%PACKAGE_FILE_NAME_UN% -F name=%PACKAGE_NAME_UN% -F force=true -F cmd=uninst http://%HOST%:%PORT%/crx/packmgr/service.jsp
	echo !CURL_LOG! >> %LOG_FILE%"
	
	%CURL% -F file=@%PACKAGE_DIR%\%PACKAGE_FILE_NAME_UN% -F name=%PACKAGE_NAME_UN% -F force=true -F cmd=uninst http://%HOST%:%PORT%/crx/packmgr/service.jsp -u %USER%:%PASSWORD% -f >> %LOG_FILE% 2>&1	
	
	echo Return code !ERRORLEVEL! >> %LOG_FILE%	
goto:eof

:remove 
	set PACKAGE_FILE_NAME_UN=%1
	set PACKAGE_NAME_UN=%2
	
	echo Deleting package !PACKAGE_NAME_UN! >> %LOG_FILE% 
	echo Deleting package !PACKAGE_NAME_UN!
	
	set CURL_LOG="curl -u *****:***** -F file=@%PACKAGE_DIR%\%PACKAGE_FILE_NAME_UN% -F name=%PACKAGE_NAME_UN% -F force=true -F cmd=rm http://%HOST%:%PORT%/crx/packmgr/service.jsp
	echo !CURL_LOG! >> %LOG_FILE%"
	
	%CURL% -F file=@%PACKAGE_DIR%\%PACKAGE_FILE_NAME_UN% -F name=%PACKAGE_NAME_UN% -F force=true -F cmd=rm http://%HOST%:%PORT%/crx/packmgr/service.jsp -u %USER%:%PASSWORD% -f >> %LOG_FILE% 2>&1	
	
	echo Return code !ERRORLEVEL! >> %LOG_FILE%	
goto:eof


