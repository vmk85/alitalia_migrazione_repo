#!/bin/sh

HOST=$1
PORT=$2
USER=$3
PASSWORD=$4
PROJECTS_HOME=$5

DEPLOY_DIR=$PROJECTS_HOME/localdev_deploy
BUNDLE_DIR=$DEPLOY_DIR/bundle
BUNDLE_LIST_FILE=$DEPLOY_DIR/bundle-list.csv

LOG_FILE=$DEPLOY_DIR/log.out

TEMP_CSV=tmp.csv

echo Stopping all bundles
echo ------------------------- `date +%d%m%Y%H%M%S` - Stopping bundle ------------------------- > $LOG_FILE

tr -d '\015' < $BUNDLE_LIST_FILE > $TEMP_CSV
echo "" >> $TEMP_CSV

OLDIFS=$IFS
IFS=";"
while read JAR_NAME SYMBOLIC_NAME BUNDLE_START_LEVEL
do
	echo --- `date +%d%m%Y%H%M%S` - Stopping bundle $JAR_NAME --->> $LOG_FILE
  
	echo Stopping bundle $JAR_NAME >> $LOG_FILE
	echo Stopping bundle $JAR_NAME
	
	CURL_LOG="curl -F action=stop http://$HOST:$PORT/system/console/bundles/$SYMBOLIC_NAME -u *****:***** -f"
	echo Executing $CURL_LOG >> $LOG_FILE
  
	curl -F action=stop http://$HOST:$PORT/system/console/bundles/$SYMBOLIC_NAME -u $USER:$PASSWORD -f >> $LOG_FILE 2>&1
	
	echo "" >> $LOG_FILE
	echo Return code $? >> $LOG_FILE
done < $TEMP_CSV

echo All bundles stopped
echo ------------------------- `date +%d%m%Y%H%M%S` - All bundles stopped ------------------------- >> $LOG_FILE

echo Starting uninstall
echo ------------------------- `date +%d%m%Y%H%M%S` - Starting uninstall ------------------------- >> $LOG_FILE

while read JAR_NAME SYMBOLIC_NAME BUNDLE_START_LEVEL
do
	echo --- `date +%d%m%Y%H%M%S` - Uninstalling bundle $JAR_NAME --->> $LOG_FILE
  
	echo Uninstalling bundle $JAR_NAME >> $LOG_FILE
	echo Uninstalling bundle $JAR_NAME	
	
	CURL_LOG="curl -F action=uninstall http://$HOST:$PORT/system/console/bundles/$SYMBOLIC_NAME -u *****:***** -f"
	echo Executing $CURL_LOG >> $LOG_FILE
  
	curl -F action=uninstall http://$HOST:$PORT/system/console/bundles/$SYMBOLIC_NAME -u $USER:$PASSWORD -f >> $LOG_FILE 2>&1
	
	echo "" >> $LOG_FILE
	echo Return code $? >> $LOG_FILE
done < $TEMP_CSV

echo All bundles uninstalled
echo ------------------------- `date +%d%m%Y%H%M%S` - All bundles uninstalled ------------------------- >> $LOG_FILE
	
echo Starting installation
echo ------------------------- `date +%d%m%Y%H%M%S` - Starting installation ------------------------- >> $LOG_FILE

while read JAR_NAME SYMBOLIC_NAME BUNDLE_START_LEVEL
do
	echo --- `date +%d%m%Y%H%M%S` - Installing bundle $JAR_NAME --->> $LOG_FILE
  
	echo Installing bundle $JAR_NAME >> $LOG_FILE
	echo Installing bundle $JAR_NAME	

	CURL_LOG="curl -X POST -F action=install http://$HOST:$PORT/system/console/bundles -F bundlestartlevel=$BUNDLE_START_LEVEL -F bundlefile=@$BUNDLE_DIR/$JAR_NAME -u *****:***** -f"
	echo Executing $CURL_LOG >> $LOG_FILE
  
	curl -X POST -F action=install http://$HOST:$PORT/system/console/bundles -F bundlestartlevel=$BUNDLE_START_LEVEL -F bundlefile=@$BUNDLE_DIR/$JAR_NAME -u $USER:$PASSWORD -f >> $LOG_FILE 2>&1
	
	echo "" >> $LOG_FILE
	echo Return code $? >> $LOG_FILE
done < $TEMP_CSV

echo All bundles installed
echo ------------------------- `date +%d%m%Y%H%M%S` - All bundles installed ------------------------- >> $LOG_FILE

echo Starting activation
echo ------------------------- `date +%d%m%Y%H%M%S` - Starting activation ------------------------- >> $LOG_FILE

while read JAR_NAME SYMBOLIC_NAME BUNDLE_START_LEVEL
do
	echo --- `date +%d%m%Y%H%M%S` - Starting bundle $JAR_NAME --->> $LOG_FILE
  
	echo Starting bundle $JAR_NAME >> $LOG_FILE
	echo Starting bundle $JAR_NAME	

	CURL_LOG="curl -F action=start http://$HOST:$PORT/system/console/bundles/$SYMBOLIC_NAME -u *****:***** -f"
	echo Executing $CURL_LOG >> $LOG_FILE
  
	curl -F action=start http://$HOST:$PORT/system/console/bundles/$SYMBOLIC_NAME -u $USER:$PASSWORD -f >> $LOG_FILE 2>&1
	
	echo "" >> $LOG_FILE
	echo Return code $? >> $LOG_FILE
done < $TEMP_CSV

IFS=$OLDIFS


rm $TEMP_CSV