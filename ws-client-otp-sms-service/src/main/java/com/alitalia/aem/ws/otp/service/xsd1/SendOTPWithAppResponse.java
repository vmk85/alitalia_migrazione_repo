
package com.alitalia.aem.ws.otp.service.xsd1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per SendOTPWithAppResponse complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="SendOTPWithAppResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OTP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SentDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExpiryDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SendOTPWithAppResponse", propOrder = {
    "otp",
    "sentDate",
    "expiryDate"
})
public class SendOTPWithAppResponse {

    @XmlElementRef(name = "OTP", namespace = "http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otp;
    @XmlElementRef(name = "SentDate", namespace = "http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sentDate;
    @XmlElementRef(name = "ExpiryDate", namespace = "http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", type = JAXBElement.class, required = false)
    protected JAXBElement<String> expiryDate;

    /**
     * Recupera il valore della proprietà otp.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTP() {
        return otp;
    }

    /**
     * Imposta il valore della proprietà otp.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTP(JAXBElement<String> value) {
        this.otp = value;
    }

    /**
     * Recupera il valore della proprietà sentDate.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSentDate() {
        return sentDate;
    }

    /**
     * Imposta il valore della proprietà sentDate.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSentDate(JAXBElement<String> value) {
        this.sentDate = value;
    }

    /**
     * Recupera il valore della proprietà expiryDate.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExpiryDate() {
        return expiryDate;
    }

    /**
     * Imposta il valore della proprietà expiryDate.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExpiryDate(JAXBElement<String> value) {
        this.expiryDate = value;
    }

}
