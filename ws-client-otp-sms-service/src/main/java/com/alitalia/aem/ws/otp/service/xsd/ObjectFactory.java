
package com.alitalia.aem.ws.otp.service.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import com.alitalia.aem.ws.otp.service.xsd1.GetOTPRequest;
import com.alitalia.aem.ws.otp.service.xsd1.SendOTPRequest;
import com.alitalia.aem.ws.otp.service.xsd1.SendOTPWithAppRequest;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.alitalia.aem.ws.otp.service.xsd package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SendOTPResponseSendOTPResult_QNAME = new QName("http://tempuri.org/", "SendOTPResult");
    private final static QName _SendOTPWithAppResponseSendOTPWithAppResult_QNAME = new QName("http://tempuri.org/", "SendOTPWithAppResult");
    private final static QName _SendOTPRequest_QNAME = new QName("http://tempuri.org/", "request");
    private final static QName _GetOTPResponseGetOTPResult_QNAME = new QName("http://tempuri.org/", "GetOTPResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.alitalia.aem.ws.otp.service.xsd
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link com.alitalia.aem.ws.otp.service.xsd.SendOTPResponse }
     * 
     */
    public com.alitalia.aem.ws.otp.service.xsd.SendOTPResponse createSendOTPResponse() {
        return new com.alitalia.aem.ws.otp.service.xsd.SendOTPResponse();
    }

    /**
     * Create an instance of {@link com.alitalia.aem.ws.otp.service.xsd.SendOTPWithAppResponse }
     * 
     */
    public com.alitalia.aem.ws.otp.service.xsd.SendOTPWithAppResponse createSendOTPWithAppResponse() {
        return new com.alitalia.aem.ws.otp.service.xsd.SendOTPWithAppResponse();
    }

    /**
     * Create an instance of {@link com.alitalia.aem.ws.otp.service.xsd.GetOTPResponse }
     * 
     */
    public com.alitalia.aem.ws.otp.service.xsd.GetOTPResponse createGetOTPResponse() {
        return new com.alitalia.aem.ws.otp.service.xsd.GetOTPResponse();
    }

    /**
     * Create an instance of {@link SendOTPWithApp }
     * 
     */
    public SendOTPWithApp createSendOTPWithApp() {
        return new SendOTPWithApp();
    }

    /**
     * Create an instance of {@link SendOTP }
     * 
     */
    public SendOTP createSendOTP() {
        return new SendOTP();
    }

    /**
     * Create an instance of {@link GetOTP }
     * 
     */
    public GetOTP createGetOTP() {
        return new GetOTP();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link com.alitalia.aem.ws.otp.service.xsd1.SendOTPResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "SendOTPResult", scope = com.alitalia.aem.ws.otp.service.xsd.SendOTPResponse.class)
    public JAXBElement<com.alitalia.aem.ws.otp.service.xsd1.SendOTPResponse> createSendOTPResponseSendOTPResult(com.alitalia.aem.ws.otp.service.xsd1.SendOTPResponse value) {
        return new JAXBElement<com.alitalia.aem.ws.otp.service.xsd1.SendOTPResponse>(_SendOTPResponseSendOTPResult_QNAME, com.alitalia.aem.ws.otp.service.xsd1.SendOTPResponse.class, com.alitalia.aem.ws.otp.service.xsd.SendOTPResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link com.alitalia.aem.ws.otp.service.xsd1.SendOTPWithAppResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "SendOTPWithAppResult", scope = com.alitalia.aem.ws.otp.service.xsd.SendOTPWithAppResponse.class)
    public JAXBElement<com.alitalia.aem.ws.otp.service.xsd1.SendOTPWithAppResponse> createSendOTPWithAppResponseSendOTPWithAppResult(com.alitalia.aem.ws.otp.service.xsd1.SendOTPWithAppResponse value) {
        return new JAXBElement<com.alitalia.aem.ws.otp.service.xsd1.SendOTPWithAppResponse>(_SendOTPWithAppResponseSendOTPWithAppResult_QNAME, com.alitalia.aem.ws.otp.service.xsd1.SendOTPWithAppResponse.class, com.alitalia.aem.ws.otp.service.xsd.SendOTPWithAppResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendOTPRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "request", scope = SendOTP.class)
    public JAXBElement<SendOTPRequest> createSendOTPRequest(SendOTPRequest value) {
        return new JAXBElement<SendOTPRequest>(_SendOTPRequest_QNAME, SendOTPRequest.class, SendOTP.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link com.alitalia.aem.ws.otp.service.xsd1.GetOTPResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetOTPResult", scope = com.alitalia.aem.ws.otp.service.xsd.GetOTPResponse.class)
    public JAXBElement<com.alitalia.aem.ws.otp.service.xsd1.GetOTPResponse> createGetOTPResponseGetOTPResult(com.alitalia.aem.ws.otp.service.xsd1.GetOTPResponse value) {
        return new JAXBElement<com.alitalia.aem.ws.otp.service.xsd1.GetOTPResponse>(_GetOTPResponseGetOTPResult_QNAME, com.alitalia.aem.ws.otp.service.xsd1.GetOTPResponse.class, com.alitalia.aem.ws.otp.service.xsd.GetOTPResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOTPRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "request", scope = GetOTP.class)
    public JAXBElement<GetOTPRequest> createGetOTPRequest(GetOTPRequest value) {
        return new JAXBElement<GetOTPRequest>(_SendOTPRequest_QNAME, GetOTPRequest.class, GetOTP.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendOTPWithAppRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "request", scope = SendOTPWithApp.class)
    public JAXBElement<SendOTPWithAppRequest> createSendOTPWithAppRequest(SendOTPWithAppRequest value) {
        return new JAXBElement<SendOTPWithAppRequest>(_SendOTPRequest_QNAME, SendOTPWithAppRequest.class, SendOTPWithApp.class, value);
    }

}
