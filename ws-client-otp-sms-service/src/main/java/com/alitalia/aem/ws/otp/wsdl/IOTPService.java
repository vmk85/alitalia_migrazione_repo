
package com.alitalia.aem.ws.otp.wsdl;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import com.alitalia.aem.ws.otp.service.xsd1.GetOTPRequest;
import com.alitalia.aem.ws.otp.service.xsd1.GetOTPResponse;
import com.alitalia.aem.ws.otp.service.xsd1.SendOTPRequest;
import com.alitalia.aem.ws.otp.service.xsd1.SendOTPResponse;
import com.alitalia.aem.ws.otp.service.xsd1.SendOTPWithAppRequest;
import com.alitalia.aem.ws.otp.service.xsd1.SendOTPWithAppResponse;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.8
 * Generated source version: 2.2
 * 
 */
@WebService(name = "IOTPService", targetNamespace = "http://tempuri.org/")
@XmlSeeAlso({
    com.alitalia.aem.ws.otp.service.xsd.ObjectFactory.class,
    com.alitalia.aem.ws.otp.service.xsd1.ObjectFactory.class
})
public interface IOTPService {


    /**
     * 
     * @param request
     * @return
     *     returns com.alitalia.aem.ws.otp.service.xsd1.GetOTPResponse
     */
    @WebMethod(operationName = "GetOTP", action = "http://tempuri.org/IOTPService/GetOTP")
    @WebResult(name = "GetOTPResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "GetOTP", targetNamespace = "http://tempuri.org/", className = "com.alitalia.aem.ws.otp.service.xsd.GetOTP")
    @ResponseWrapper(localName = "GetOTPResponse", targetNamespace = "http://tempuri.org/", className = "com.alitalia.aem.ws.otp.service.xsd.GetOTPResponse")
    public GetOTPResponse getOTP(
        @WebParam(name = "request", targetNamespace = "http://tempuri.org/")
        GetOTPRequest request);

    /**
     * 
     * @param request
     * @return
     *     returns com.alitalia.aem.ws.otp.service.xsd1.SendOTPResponse
     */
    @WebMethod(operationName = "SendOTP", action = "http://tempuri.org/IOTPService/SendOTP")
    @WebResult(name = "SendOTPResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "SendOTP", targetNamespace = "http://tempuri.org/", className = "com.alitalia.aem.ws.otp.service.xsd.SendOTP")
    @ResponseWrapper(localName = "SendOTPResponse", targetNamespace = "http://tempuri.org/", className = "com.alitalia.aem.ws.otp.service.xsd.SendOTPResponse")
    public SendOTPResponse sendOTP(
        @WebParam(name = "request", targetNamespace = "http://tempuri.org/")
        SendOTPRequest request);

    /**
     * 
     * @param request
     * @return
     *     returns com.alitalia.aem.ws.otp.service.xsd1.SendOTPWithAppResponse
     */
    @WebMethod(operationName = "SendOTPWithApp", action = "http://tempuri.org/IOTPService/SendOTPWithApp")
    @WebResult(name = "SendOTPWithAppResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "SendOTPWithApp", targetNamespace = "http://tempuri.org/", className = "com.alitalia.aem.ws.otp.service.xsd.SendOTPWithApp")
    @ResponseWrapper(localName = "SendOTPWithAppResponse", targetNamespace = "http://tempuri.org/", className = "com.alitalia.aem.ws.otp.service.xsd.SendOTPWithAppResponse")
    public SendOTPWithAppResponse sendOTPWithApp(
        @WebParam(name = "request", targetNamespace = "http://tempuri.org/")
        SendOTPWithAppRequest request);

}
