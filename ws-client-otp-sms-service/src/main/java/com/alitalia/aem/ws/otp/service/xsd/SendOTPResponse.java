
package com.alitalia.aem.ws.otp.service.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SendOTPResult" type="{http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract}SendOTPResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sendOTPResult"
})
@XmlRootElement(name = "SendOTPResponse")
public class SendOTPResponse {

    @XmlElementRef(name = "SendOTPResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<com.alitalia.aem.ws.otp.service.xsd1.SendOTPResponse> sendOTPResult;

    /**
     * Recupera il valore della proprietà sendOTPResult.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link com.alitalia.aem.ws.otp.service.xsd1.SendOTPResponse }{@code >}
     *     
     */
    public JAXBElement<com.alitalia.aem.ws.otp.service.xsd1.SendOTPResponse> getSendOTPResult() {
        return sendOTPResult;
    }

    /**
     * Imposta il valore della proprietà sendOTPResult.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link com.alitalia.aem.ws.otp.service.xsd1.SendOTPResponse }{@code >}
     *     
     */
    public void setSendOTPResult(JAXBElement<com.alitalia.aem.ws.otp.service.xsd1.SendOTPResponse> value) {
        this.sendOTPResult = value;
    }

}
