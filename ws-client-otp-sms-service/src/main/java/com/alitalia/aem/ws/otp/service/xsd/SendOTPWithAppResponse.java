
package com.alitalia.aem.ws.otp.service.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SendOTPWithAppResult" type="{http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract}SendOTPWithAppResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sendOTPWithAppResult"
})
@XmlRootElement(name = "SendOTPWithAppResponse")
public class SendOTPWithAppResponse {

    @XmlElementRef(name = "SendOTPWithAppResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<com.alitalia.aem.ws.otp.service.xsd1.SendOTPWithAppResponse> sendOTPWithAppResult;

    /**
     * Recupera il valore della proprietà sendOTPWithAppResult.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link com.alitalia.aem.ws.otp.service.xsd1.SendOTPWithAppResponse }{@code >}
     *     
     */
    public JAXBElement<com.alitalia.aem.ws.otp.service.xsd1.SendOTPWithAppResponse> getSendOTPWithAppResult() {
        return sendOTPWithAppResult;
    }

    /**
     * Imposta il valore della proprietà sendOTPWithAppResult.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link com.alitalia.aem.ws.otp.service.xsd1.SendOTPWithAppResponse }{@code >}
     *     
     */
    public void setSendOTPWithAppResult(JAXBElement<com.alitalia.aem.ws.otp.service.xsd1.SendOTPWithAppResponse> value) {
        this.sendOTPWithAppResult = value;
    }

}
