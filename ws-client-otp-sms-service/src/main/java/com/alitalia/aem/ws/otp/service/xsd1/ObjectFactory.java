
package com.alitalia.aem.ws.otp.service.xsd1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.alitalia.aem.ws.otp.service.xsd1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SendOTPResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", "SendOTPResponse");
    private final static QName _SendOTPWithAppResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", "SendOTPWithAppResponse");
    private final static QName _GetOTPResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", "GetOTPResponse");
    private final static QName _SendOTPRequest_QNAME = new QName("http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", "SendOTPRequest");
    private final static QName _SendOTPWithAppRequest_QNAME = new QName("http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", "SendOTPWithAppRequest");
    private final static QName _GetOTPRequest_QNAME = new QName("http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", "GetOTPRequest");
    private final static QName _SendOTPWithAppResponseExpiryDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", "ExpiryDate");
    private final static QName _SendOTPWithAppResponseSentDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", "SentDate");
    private final static QName _SendOTPWithAppResponseOTP_QNAME = new QName("http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", "OTP");
    private final static QName _GetOTPRequestEmailAddress_QNAME = new QName("http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", "EmailAddress");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.alitalia.aem.ws.otp.service.xsd1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SendOTPResponse }
     * 
     */
    public SendOTPResponse createSendOTPResponse() {
        return new SendOTPResponse();
    }

    /**
     * Create an instance of {@link SendOTPWithAppResponse }
     * 
     */
    public SendOTPWithAppResponse createSendOTPWithAppResponse() {
        return new SendOTPWithAppResponse();
    }

    /**
     * Create an instance of {@link GetOTPResponse }
     * 
     */
    public GetOTPResponse createGetOTPResponse() {
        return new GetOTPResponse();
    }

    /**
     * Create an instance of {@link SendOTPWithAppRequest }
     * 
     */
    public SendOTPWithAppRequest createSendOTPWithAppRequest() {
        return new SendOTPWithAppRequest();
    }

    /**
     * Create an instance of {@link SendOTPRequest }
     * 
     */
    public SendOTPRequest createSendOTPRequest() {
        return new SendOTPRequest();
    }

    /**
     * Create an instance of {@link GetOTPRequest }
     * 
     */
    public GetOTPRequest createGetOTPRequest() {
        return new GetOTPRequest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendOTPResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", name = "SendOTPResponse")
    public JAXBElement<SendOTPResponse> createSendOTPResponse(SendOTPResponse value) {
        return new JAXBElement<SendOTPResponse>(_SendOTPResponse_QNAME, SendOTPResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendOTPWithAppResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", name = "SendOTPWithAppResponse")
    public JAXBElement<SendOTPWithAppResponse> createSendOTPWithAppResponse(SendOTPWithAppResponse value) {
        return new JAXBElement<SendOTPWithAppResponse>(_SendOTPWithAppResponse_QNAME, SendOTPWithAppResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOTPResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", name = "GetOTPResponse")
    public JAXBElement<GetOTPResponse> createGetOTPResponse(GetOTPResponse value) {
        return new JAXBElement<GetOTPResponse>(_GetOTPResponse_QNAME, GetOTPResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendOTPRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", name = "SendOTPRequest")
    public JAXBElement<SendOTPRequest> createSendOTPRequest(SendOTPRequest value) {
        return new JAXBElement<SendOTPRequest>(_SendOTPRequest_QNAME, SendOTPRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendOTPWithAppRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", name = "SendOTPWithAppRequest")
    public JAXBElement<SendOTPWithAppRequest> createSendOTPWithAppRequest(SendOTPWithAppRequest value) {
        return new JAXBElement<SendOTPWithAppRequest>(_SendOTPWithAppRequest_QNAME, SendOTPWithAppRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOTPRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", name = "GetOTPRequest")
    public JAXBElement<GetOTPRequest> createGetOTPRequest(GetOTPRequest value) {
        return new JAXBElement<GetOTPRequest>(_GetOTPRequest_QNAME, GetOTPRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", name = "ExpiryDate", scope = SendOTPWithAppResponse.class)
    public JAXBElement<String> createSendOTPWithAppResponseExpiryDate(String value) {
        return new JAXBElement<String>(_SendOTPWithAppResponseExpiryDate_QNAME, String.class, SendOTPWithAppResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", name = "SentDate", scope = SendOTPWithAppResponse.class)
    public JAXBElement<String> createSendOTPWithAppResponseSentDate(String value) {
        return new JAXBElement<String>(_SendOTPWithAppResponseSentDate_QNAME, String.class, SendOTPWithAppResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", name = "OTP", scope = SendOTPWithAppResponse.class)
    public JAXBElement<String> createSendOTPWithAppResponseOTP(String value) {
        return new JAXBElement<String>(_SendOTPWithAppResponseOTP_QNAME, String.class, SendOTPWithAppResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", name = "EmailAddress", scope = GetOTPRequest.class)
    public JAXBElement<String> createGetOTPRequestEmailAddress(String value) {
        return new JAXBElement<String>(_GetOTPRequestEmailAddress_QNAME, String.class, GetOTPRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", name = "ExpiryDate", scope = GetOTPResponse.class)
    public JAXBElement<String> createGetOTPResponseExpiryDate(String value) {
        return new JAXBElement<String>(_SendOTPWithAppResponseExpiryDate_QNAME, String.class, GetOTPResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", name = "OTP", scope = GetOTPResponse.class)
    public JAXBElement<String> createGetOTPResponseOTP(String value) {
        return new JAXBElement<String>(_SendOTPWithAppResponseOTP_QNAME, String.class, GetOTPResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", name = "ExpiryDate", scope = SendOTPResponse.class)
    public JAXBElement<String> createSendOTPResponseExpiryDate(String value) {
        return new JAXBElement<String>(_SendOTPWithAppResponseExpiryDate_QNAME, String.class, SendOTPResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", name = "SentDate", scope = SendOTPResponse.class)
    public JAXBElement<String> createSendOTPResponseSentDate(String value) {
        return new JAXBElement<String>(_SendOTPWithAppResponseSentDate_QNAME, String.class, SendOTPResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract", name = "OTP", scope = SendOTPResponse.class)
    public JAXBElement<String> createSendOTPResponseOTP(String value) {
        return new JAXBElement<String>(_SendOTPWithAppResponseOTP_QNAME, String.class, SendOTPResponse.class, value);
    }

}
