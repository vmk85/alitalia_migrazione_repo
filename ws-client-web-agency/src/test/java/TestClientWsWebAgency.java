import javax.xml.ws.Holder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.webagency.WsWebAgencyClient;
import com.alitalia.aem.ws.webagency.xsd.ClAgencyData;

public class TestClientWsWebAgency {
	
	private WsWebAgencyClient wsWebAgencyClient;
	private String serviceEndpoint = "http://172.31.32.24:9000/WCF_WEBAGENCY/ws_WebAgency.svc";
//	private String serviceEndpoint = "http://localhost:8088/ws-client-web-agency?WSDL";
	private static final Logger log = LoggerFactory.getLogger(TestClientWsWebAgency.class);
	
	public TestClientWsWebAgency() throws Exception {
		try {
			wsWebAgencyClient = new WsWebAgencyClient(serviceEndpoint);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void execute() {
		try {
			String codiceAgenzia = "160354";

			Holder<ClAgencyData> agencyData = new Holder<ClAgencyData>();
			Holder<Short> agyIsEnabledResult = new Holder<Short>();
			wsWebAgencyClient.agyIsEnabled(codiceAgenzia, agencyData, agyIsEnabledResult);;

			log.info("call ok: ret code " + agyIsEnabledResult.value);
			log.info("ret values: " + agencyData.value.getAGNCode());
		} catch (Exception e) {
			log.info("error");
			e.printStackTrace();
		}
	}
	
	public static void main(String arg []) throws Exception {
		log.info("calling..");
		TestClientWsWebAgency client = new TestClientWsWebAgency();
		client.execute();
		log.info("call completed");
	} 

	

}
