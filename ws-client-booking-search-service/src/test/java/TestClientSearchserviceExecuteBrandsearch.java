import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.booking.searchservice.SearchServiceClient;
import com.alitalia.aem.ws.booking.searchservice.xsd1.SearchRequest;
import com.alitalia.aem.ws.booking.searchservice.xsd1.SearchResponse;
import com.alitalia.aem.ws.booking.searchservice.xsd15.AvailableFlights;
import com.alitalia.aem.ws.booking.searchservice.xsd2.BrandSearch;
import com.alitalia.aem.ws.booking.searchservice.xsd2.ModelResponse;
import com.alitalia.aem.ws.booking.searchservice.xsd2.SearchResponseType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.Airport;
import com.alitalia.aem.ws.booking.searchservice.xsd3.AreaValue;
import com.alitalia.aem.ws.booking.searchservice.xsd3.PassengerNumbers;
import com.alitalia.aem.ws.booking.searchservice.xsd3.PassengerType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.RouteType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.SearchDestination;
import com.alitalia.aem.ws.booking.searchservice.xsd4.Cabin;
import com.alitalia.aem.ws.booking.searchservice.xsd4.CabinType;
import com.alitalia.aem.ws.booking.searchservice.xsd4.ResidencyType;
import com.alitalia.aem.ws.booking.searchservice.xsd4.SearchType;
import com.alitalia.aem.ws.booking.searchservice.xsd4.TimeType;
import com.alitalia.aem.ws.booking.searchservice.xsd5.ArrayOfanyType;

public class TestClientSearchserviceExecuteBrandsearch {
	
	private SearchServiceClient searchServiceClient;
	private String serviceEndpoint = "http://azwtapb002.user.alitalia.local:9000/WcfBookingR1/Alitalia.Portal.WebFrontend.LCWP.Booking.Services.Lib.SearchService.svc";
	private static final Logger log = LoggerFactory.getLogger(TestClientSearchserviceExecuteBrandsearch.class);
	
	public TestClientSearchserviceExecuteBrandsearch() throws Exception {
		try {
			searchServiceClient = new SearchServiceClient(serviceEndpoint);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void execute() {
		try {
			DatatypeFactory dtf = DatatypeFactory.newInstance();
			com.alitalia.aem.ws.booking.searchservice.xsd1.ObjectFactory factory1 = new com.alitalia.aem.ws.booking.searchservice.xsd1.ObjectFactory();
			com.alitalia.aem.ws.booking.searchservice.xsd2.ObjectFactory factory2 = new com.alitalia.aem.ws.booking.searchservice.xsd2.ObjectFactory();
			com.alitalia.aem.ws.booking.searchservice.xsd3.ObjectFactory factory3 = new com.alitalia.aem.ws.booking.searchservice.xsd3.ObjectFactory();
			com.alitalia.aem.ws.booking.searchservice.xsd5.ObjectFactory factory5 = new com.alitalia.aem.ws.booking.searchservice.xsd5.ObjectFactory();
			
			Airport fromAirport = factory3.createAirport();
			fromAirport.setAZDeserves(true);
			fromAirport.setArea(AreaValue.DOM);
			fromAirport.setCity(factory3.createAirportCity("Roma"));
			fromAirport.setCityCode(factory3.createAirportCityCode("ROM"));
			fromAirport.setCityUrl(factory3.createAirportCityUrl(null));
			fromAirport.setCode(factory3.createAirportCode("FCO"));
			fromAirport.setCountry(factory3.createAirportCountry("Italia"));
			fromAirport.setCountryCode(factory3.createAirportCountryCode("IT"));
			fromAirport.setDescription(factory3.createAirportDescription("Roma, Fiumicino, Italia"));
			fromAirport.setDetails(factory3.createAirportDetails(null));
			fromAirport.setEligibility(true);
			fromAirport.setEtktAvlblFrom(factory3.createAirportEtktAvlblFrom(null));
			fromAirport.setHiddenInFirstDeparture(false);
			fromAirport.setIdMsg(0);
			fromAirport.setName(factory3.createAirportName("Roma"));
			fromAirport.setState(factory3.createAirportState(null));
			fromAirport.setStateCode(factory3.createAirportStateCode(null));
			fromAirport.setUnavlblFrom(factory3.createAirportUnavlblFrom(null));
			fromAirport.setUnavlblUntil(factory3.createAirportUnavlblUntil(null));

			Airport toAirport = factory3.createAirport();
			toAirport.setAZDeserves(true);
			toAirport.setArea(AreaValue.INTZ);
			toAirport.setCity(factory3.createAirportCity("Madrid"));
			toAirport.setCityCode(factory3.createAirportCityCode("MAD"));
			toAirport.setCityUrl(factory3.createAirportCityUrl(null));
			toAirport.setCode(factory3.createAirportCode("MAD"));
			toAirport.setCountry(factory3.createAirportCountry("Spagna"));
			toAirport.setCountryCode(factory3.createAirportCountryCode("ES"));
			toAirport.setDescription(factory3.createAirportDescription("Madrid, Madrid, Spagna"));
			toAirport.setDetails(factory3.createAirportDetails(null));
			toAirport.setEligibility(true);
			toAirport.setEtktAvlblFrom(factory3.createAirportEtktAvlblFrom(null));
			toAirport.setHiddenInFirstDeparture(false);
			toAirport.setIdMsg(0);
			toAirport.setName(factory3.createAirportName("Madrid"));
			toAirport.setState(factory3.createAirportState(null));
			toAirport.setStateCode(factory3.createAirportStateCode(""));
			toAirport.setUnavlblFrom(factory3.createAirportUnavlblFrom(null));
			toAirport.setUnavlblUntil(factory3.createAirportUnavlblUntil(null));

			SearchDestination sdOutbound = factory3.createSearchDestination();
			XMLGregorianCalendar dateOutbound = dtf.newXMLGregorianCalendar(2016, 07, 15, 00, 00, 00, 0000, DatatypeConstants.FIELD_UNDEFINED);
			sdOutbound.setDepartureDate(dateOutbound);
			sdOutbound.setFrom(factory3.createASearchDestinationFrom(fromAirport));
			sdOutbound.setSelectedOffer(null);
			sdOutbound.setType(RouteType.OUTBOUND);
			sdOutbound.setTimeType(TimeType.ANYTIME);
			sdOutbound.setTo(factory3.createSearchDestinationTo(toAirport));

//			SearchDestination sdReturn = factory3.createSearchDestination();
//			XMLGregorianCalendar dateReturn = dtf.newXMLGregorianCalendar(2015, 07, 21, 00, 00, 00, 0000, DatatypeConstants.FIELD_UNDEFINED);
//			sdReturn.setDepartureDate(dateReturn);
//			sdReturn.setFrom(factory3.createASearchDestinationFrom(toAirport));
//			sdReturn.setSelectedOffer(factory3.createASearchDestinationSelectedOffer(null));
//			sdReturn.setType(RouteType.);
//			sdReturn.setTimeType(TimeType.ANYTIME);
//			sdReturn.setTo(factory3.createSearchDestinationTo(fromAirport));
			

			ArrayOfanyType destinations = factory5.createArrayOfanyType();
			destinations.getAnyType().add(sdOutbound);
//			destinations.getAnyType().add(sdReturn);

			PassengerNumbers pnAdult = factory3.createPassengerNumbers();
			pnAdult.setType(PassengerType.ADULT);
			pnAdult.setNumber(2);

			PassengerNumbers pnChild = factory3.createPassengerNumbers();
			pnChild.setType(PassengerType.CHILD);
			pnChild.setNumber(1);

			PassengerNumbers pnInfant = factory3.createPassengerNumbers();
			pnInfant.setType(PassengerType.INFANT);
			pnInfant.setNumber(1);

			ArrayOfanyType passengerNumbers = factory5.createArrayOfanyType();
			passengerNumbers.getAnyType().add(pnAdult);
//			passengerNumbers.getAnyType().add(pnChild);
//			passengerNumbers.getAnyType().add(pnInfant);

			BrandSearch brandSearch = factory2.createBrandSearch();
			brandSearch.setProperties(factory3.createABoomBoxGenericInfoProperties(null));
			brandSearch.setFarmId(factory3.createABoomBoxInfoFarmId(null));
			brandSearch.setId(factory3.createABoomBoxInfoId(null));
			brandSearch.setSessionId(factory3.createABoomBoxInfoSessionId(null));
			brandSearch.setSolutionSet(factory3.createABoomBoxInfoSolutionSet(null));
			brandSearch.setCUG(factory3.createASearchCUG("ADT"));
			brandSearch.setDestinations(factory3.createASearchDestinations(destinations));
			brandSearch.setInnerSearch(factory3.createASearchInnerSearch(null));
			brandSearch.setMarket(factory3.createASearchMarket("IT"));
			brandSearch.setOnlyDirectFlight(false);
			brandSearch.setPassengerNumbers(factory3.createASearchPassengerNumbers(passengerNumbers));
			brandSearch.setSearchCabin(Cabin.ECONOMY);
			brandSearch.setSearchCabinType(CabinType.PERMITTED);
			brandSearch.setType(SearchType.BRAND_SEARCH);
			brandSearch.setResidency(ResidencyType.NONE);

			JAXBElement<Object> filter = factory1.createSearchRequestFilter(brandSearch);
					
			SearchRequest request = factory1.createSearchRequest(); 
			request.setFilter(filter);
			request.setLanguageCode(factory1.createSearchRequestLanguageCode("IT"));
			request.setMarketCode(factory1.createSearchRequestMarketCode("IT"));
			request.setResponseType(SearchResponseType.MODEL);
			request.setExecution(factory1.createSearchRequestExecution(null));
			request.setCookie(factory1.createSearchRequestCookie(null));

			SearchResponse response = searchServiceClient.execute(request);
			ModelResponse result = (ModelResponse) response.getResult().getValue();
			AvailableFlights flights = (AvailableFlights) result.getModel().getValue();
	
			log.info("call ok: " + flights.toString());
		} catch (Exception e) {
			log.info("error");
			e.printStackTrace();
		}
	}
	
	public static void main(String arg []) throws Exception {
		log.info("calling..");
		TestClientSearchserviceExecuteBrandsearch client = new TestClientSearchserviceExecuteBrandsearch();
		client.execute();
		log.info("call completed");
	} 

	

}
