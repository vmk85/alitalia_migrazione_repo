import java.math.BigDecimal;

import javax.xml.bind.JAXBElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.booking.searchservice.SearchServiceClient;
import com.alitalia.aem.ws.booking.searchservice.xsd1.SearchRequest;
import com.alitalia.aem.ws.booking.searchservice.xsd1.SearchResponse;
import com.alitalia.aem.ws.booking.searchservice.xsd2.BookingSearch;
import com.alitalia.aem.ws.booking.searchservice.xsd2.SearchResponseType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ARTaxInfoTypes;
import com.alitalia.aem.ws.booking.searchservice.xsd3.AdultPassenger;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ApplicantPassenger;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ArrayOfAPassengerBase;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ChildPassenger;
import com.alitalia.aem.ws.booking.searchservice.xsd3.GatewayType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.InfantPassenger;
import com.alitalia.aem.ws.booking.searchservice.xsd3.PassengerType;

public class TestClientSearchserviceExecuteBookingsearchBookingtaxsearch {
	
	private SearchServiceClient searchServiceClient;
	private String serviceEndpoint = "http://romcs0944.user.alitalia.local:9000/WcfBooking/Alitalia.Portal.WebFrontend.LCWP.Booking.Services.Lib.SearchService.svc";
	private static final Logger log = LoggerFactory.getLogger(TestClientSearchserviceExecuteBookingsearchBookingtaxsearch.class);
	
	public TestClientSearchserviceExecuteBookingsearchBookingtaxsearch() throws Exception {
		try {
			searchServiceClient = new SearchServiceClient(serviceEndpoint);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void execute() {
		try {
			com.alitalia.aem.ws.booking.searchservice.xsd1.ObjectFactory factory1 = new com.alitalia.aem.ws.booking.searchservice.xsd1.ObjectFactory();
			com.alitalia.aem.ws.booking.searchservice.xsd2.ObjectFactory factory2 = new com.alitalia.aem.ws.booking.searchservice.xsd2.ObjectFactory();
			com.alitalia.aem.ws.booking.searchservice.xsd3.ObjectFactory factory3 = new com.alitalia.aem.ws.booking.searchservice.xsd3.ObjectFactory();
			com.alitalia.aem.ws.booking.searchservice.xsd5.ObjectFactory factory5 = new com.alitalia.aem.ws.booking.searchservice.xsd5.ObjectFactory();
			
			ApplicantPassenger pnApplicant = factory3.createApplicantPassenger();
			pnApplicant.setCouponPrice(new BigDecimal(0));
			pnApplicant.setExtraCharge(new BigDecimal(0));
			pnApplicant.setFee(new BigDecimal(0));
			pnApplicant.setGrossFare(new BigDecimal(0));
			pnApplicant.setInfo(factory3.createAPassengerBaseInfo(null));
			pnApplicant.setLastName(factory3.createAPassengerBaseLastName(null));
			pnApplicant.setName(factory3.createAPassengerBaseName(null));
			pnApplicant.setNetFare(new BigDecimal(0));
			pnApplicant.setTickets(factory3.createAPassengerBaseTickets(null));
			pnApplicant.setType(PassengerType.ADULT);
			pnApplicant.setPreferences(factory3.createARegularPassengerPreferences(null));
			pnApplicant.setFrequentFlyerCode(factory3.createAdultPassengerFrequentFlyerCode(null));
			pnApplicant.setFrequentFlyerTier(factory3.createAdultPassengerFrequentFlyerTier(null));
			pnApplicant.setFrequentFlyerType(factory3.createAdultPassengerFrequentFlyerType(null));
			pnApplicant.setARTaxInfoType(ARTaxInfoTypes.NONE);
			pnApplicant.setBlueBizCode(factory3.createApplicantPassengerBlueBizCode(""));
			pnApplicant.setCUIT(factory3.createApplicantPassengerCUIT(null));
			pnApplicant.setContact(factory3.createApplicantPassengerContact(factory5.createArrayOfanyType()));
			pnApplicant.setCountry(factory3.createApplicantPassengerCountry(null));
			pnApplicant.setEmail(factory3.createApplicantPassengerEmail(null));
			pnApplicant.setSkyBonusCode(factory3.createApplicantPassengerSkyBonusCode(""));
			pnApplicant.setSubscribeToNewsletter(factory3.createApplicantPassengerSubscribeToNewsletter(null));

			AdultPassenger pnAdult = factory3.createAdultPassenger();
			pnAdult.setCouponPrice(new BigDecimal(0));
			pnAdult.setExtraCharge(new BigDecimal(0));
			pnAdult.setFee(new BigDecimal(0));
			pnAdult.setGrossFare(new BigDecimal(0));
			pnAdult.setInfo(factory3.createAPassengerBaseInfo(null));
			pnAdult.setLastName(factory3.createAPassengerBaseLastName(null));
			pnAdult.setName(factory3.createAPassengerBaseName(null));
			pnAdult.setNetFare(new BigDecimal(0));
			pnAdult.setTickets(factory3.createAPassengerBaseTickets(null));
			pnAdult.setType(PassengerType.ADULT);
			pnAdult.setPreferences(factory3.createARegularPassengerPreferences(null));
			pnAdult.setFrequentFlyerCode(factory3.createAdultPassengerFrequentFlyerCode(null));
			pnAdult.setFrequentFlyerTier(factory3.createAdultPassengerFrequentFlyerTier(null));
			pnAdult.setFrequentFlyerType(factory3.createAdultPassengerFrequentFlyerType(null));

			ChildPassenger pnChild = factory3.createChildPassenger();
			pnChild.setCouponPrice(new BigDecimal(0));
			pnChild.setExtraCharge(new BigDecimal(0));
			pnChild.setFee(new BigDecimal(0));
			pnChild.setGrossFare(new BigDecimal(0));
			pnChild.setInfo(factory3.createAPassengerBaseInfo(null));
			pnChild.setLastName(factory3.createAPassengerBaseLastName(null));
			pnChild.setName(factory3.createAPassengerBaseName(null));
			pnChild.setNetFare(new BigDecimal(0));
			pnChild.setTickets(factory3.createAPassengerBaseTickets(null));
			pnChild.setType(PassengerType.CHILD);
			pnChild.setPreferences(factory3.createARegularPassengerPreferences(null));

			InfantPassenger pnInfant = factory3.createInfantPassenger();
			pnInfant.setCouponPrice(new BigDecimal(0));
			pnInfant.setExtraCharge(new BigDecimal(0));
			pnInfant.setFee(new BigDecimal(0));
			pnInfant.setGrossFare(new BigDecimal(0));
			pnInfant.setInfo(factory3.createAPassengerBaseInfo(null));
			pnInfant.setLastName(factory3.createAPassengerBaseLastName(null));
			pnInfant.setName(factory3.createAPassengerBaseName(null));
			pnInfant.setNetFare(new BigDecimal(0));
			pnInfant.setTickets(factory3.createAPassengerBaseTickets(null));
			pnInfant.setType(PassengerType.INFANT);
			pnInfant.setAdultReferent(0);

			ArrayOfAPassengerBase passengers = factory3.createArrayOfAPassengerBase();
			passengers.getAPassengerBase().add(pnApplicant);
			passengers.getAPassengerBase().add(pnAdult);
			passengers.getAPassengerBase().add(pnChild);
			passengers.getAPassengerBase().add(pnInfant);

			BookingSearch bookingSearch = factory2.createBookingSearch();
			bookingSearch.setProperties(factory3.createABoomBoxGenericInfoProperties(null));
			bookingSearch.setFarmId(factory3.createABoomBoxInfoFarmId(null));
			bookingSearch.setId(factory3.createABoomBoxInfoId("7pAh73JOUg9euXwcF0MAXB"));
			bookingSearch.setSessionId(factory3.createABoomBoxInfoSessionId("6Pr0MAXBCsDKNukYtfCjQJwPO"));
			bookingSearch.setSolutionSet(factory3.createABoomBoxInfoSolutionSet("0CyOktkyvJ2SN1E0iV7c2Y8"));
			bookingSearch.setPassengers(factory2.createGateWayBrandSearchPassengers(passengers));
			bookingSearch.setSolutionId(factory2.createGateWayBrandSearchSolutionId("Xdp8yxqK8TAUF6mh8UQaCS001"));
			bookingSearch.setType(GatewayType.BOOKING_TAX_SEARCH);
			bookingSearch.setBookingSolutionId(factory2.createBookingSearchBookingSolutionId("Xdp8yxqK8TAUF6mh8UQaCS001"));  //valore di una ricerca precedente?
			bookingSearch.setIsMultiSlice(false);
			bookingSearch.setIsNeutral(false);

			JAXBElement<Object> o = factory1.createSearchRequestFilter(bookingSearch);
					
			SearchRequest request = factory1.createSearchRequest(); 
			request.setFilter(o);
			request.setLanguageCode(factory1.createSearchRequestLanguageCode("IT"));
			request.setMarketCode(factory1.createSearchRequestMarketCode("IT"));
			request.setResponseType(SearchResponseType.MODEL);

			@SuppressWarnings("unused")
			SearchResponse execute = searchServiceClient.execute(request);
	
			log.info("call ok");
		} catch (Exception e) {
			log.info("error");
			e.printStackTrace();
		}
	}
	
	public static void main(String arg []) throws Exception {
		log.info("calling..");
		TestClientSearchserviceExecuteBookingsearchBookingtaxsearch client = new TestClientSearchserviceExecuteBookingsearchBookingtaxsearch();
		client.execute();
		log.info("call completed");
	} 

	

}
