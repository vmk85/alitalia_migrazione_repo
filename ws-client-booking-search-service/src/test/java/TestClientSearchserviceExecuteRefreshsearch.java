import java.math.BigDecimal;

import javax.xml.bind.JAXBElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.booking.searchservice.SearchServiceClient;
import com.alitalia.aem.ws.booking.searchservice.xsd1.SearchRequest;
import com.alitalia.aem.ws.booking.searchservice.xsd1.SearchResponse;
import com.alitalia.aem.ws.booking.searchservice.xsd2.RefreshSearch;
import com.alitalia.aem.ws.booking.searchservice.xsd2.SearchResponseType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.GatewayType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.RefreshType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.RouteType;

public class TestClientSearchserviceExecuteRefreshsearch {
	
	private SearchServiceClient searchServiceClient;
	private String serviceEndpoint = "http://romcs0944.user.alitalia.local:9000/WcfBooking/Alitalia.Portal.WebFrontend.LCWP.Booking.Services.Lib.SearchService.svc";
	private static final Logger log = LoggerFactory.getLogger(TestClientSearchserviceExecuteRefreshsearch.class);
	
	public TestClientSearchserviceExecuteRefreshsearch() throws Exception {
		try {
			searchServiceClient = new SearchServiceClient(serviceEndpoint);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void execute() {
		try {
			com.alitalia.aem.ws.booking.searchservice.xsd1.ObjectFactory factory1 = new com.alitalia.aem.ws.booking.searchservice.xsd1.ObjectFactory();
			com.alitalia.aem.ws.booking.searchservice.xsd2.ObjectFactory factory2 = new com.alitalia.aem.ws.booking.searchservice.xsd2.ObjectFactory();
			com.alitalia.aem.ws.booking.searchservice.xsd3.ObjectFactory factory3 = new com.alitalia.aem.ws.booking.searchservice.xsd3.ObjectFactory();
			
			RefreshSearch refreshSearch = factory2.createRefreshSearch();
			refreshSearch.setProperties(factory3.createABoomBoxGenericInfoProperties(null));
			refreshSearch.setFarmId(factory3.createABoomBoxInfoFarmId(null));
			refreshSearch.setId(factory3.createABoomBoxInfoId(null));
			refreshSearch.setSessionId(factory3.createABoomBoxInfoSessionId("Otd0MMVt9ZIXRQ7xrb58k29Qz"));			//sessionId di una precedente ricerca (BrandSearch?)
			refreshSearch.setSolutionSet(factory3.createABoomBoxInfoSolutionSet("0ONP3A2QPm10TG0tZ4ufRrK"));		//solutionSet di una precedente ricerca (BrandSearch?)
			refreshSearch.setPassengers(factory2.createGateWayBrandSearchPassengers(null));
			refreshSearch.setSolutionId(factory2.createGateWayBrandSearchSolutionId("6ssP2QeU9fCMD2E4ZaABEC002"));	//solutionId di una precedente ricerca (BrandSearch?)
			refreshSearch.setType(GatewayType.GATEWAY_SEARCH);
			refreshSearch.setCurrency(factory2.createRefreshSearchCurrency("EUR"));
			refreshSearch.setOriginalFare(new BigDecimal(222.92));
			refreshSearch.setOriginalPrice(new BigDecimal(396.23));
			refreshSearch.setSliceType(RouteType.RETURN);
			refreshSearch.setRefreshType(RefreshType.REFRESH_SEARCH);

			JAXBElement<Object> searchRequestFilter = factory1.createSearchRequestFilter(refreshSearch);
					
			SearchRequest request = factory1.createSearchRequest(); 
			request.setFilter(searchRequestFilter);
			request.setLanguageCode(factory1.createSearchRequestLanguageCode("IT"));
			request.setMarketCode(factory1.createSearchRequestMarketCode("IT"));
			request.setResponseType(SearchResponseType.MODEL);

			@SuppressWarnings("unused")
			SearchResponse execute = searchServiceClient.execute(request);
	
			log.info("call ok");
		} catch (Exception e) {
			log.info("error");
			e.printStackTrace();
		}
	}
	
	public static void main(String arg []) throws Exception {
		log.info("calling..");
		TestClientSearchserviceExecuteRefreshsearch client = new TestClientSearchserviceExecuteRefreshsearch();
		client.execute();
		log.info("call completed");
	} 

	

}
