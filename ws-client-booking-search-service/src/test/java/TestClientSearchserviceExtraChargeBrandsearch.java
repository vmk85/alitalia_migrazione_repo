import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.booking.searchservice.SearchServiceClient;
import com.alitalia.aem.ws.booking.searchservice.xsd1.ExtraChargeRequest;
import com.alitalia.aem.ws.booking.searchservice.xsd1.ExtraChargeResponse;
import com.alitalia.aem.ws.booking.searchservice.xsd2.BrandSearch;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ASearch;
import com.alitalia.aem.ws.booking.searchservice.xsd3.Airport;
import com.alitalia.aem.ws.booking.searchservice.xsd3.PassengerNumbers;
import com.alitalia.aem.ws.booking.searchservice.xsd3.PassengerType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.RouteType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.SearchDestination;
import com.alitalia.aem.ws.booking.searchservice.xsd4.SearchType;
import com.alitalia.aem.ws.booking.searchservice.xsd4.TimeType;
import com.alitalia.aem.ws.booking.searchservice.xsd5.ArrayOfanyType;

public class TestClientSearchserviceExtraChargeBrandsearch {
	
	private SearchServiceClient searchServiceClient;
	private String serviceEndpoint = "http://aem-author.replynet.prv:9001/WcfBooking/Alitalia.Portal.WebFrontend.LCWP.Booking.Services.Lib.SearchService.svc";
	private static final Logger log = LoggerFactory.getLogger(TestClientSearchserviceExtraChargeBrandsearch.class);
	
	public TestClientSearchserviceExtraChargeBrandsearch() throws Exception {
		try {
			searchServiceClient = new SearchServiceClient(serviceEndpoint);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void execute() {
		try {
			DatatypeFactory dtf = DatatypeFactory.newInstance();
			com.alitalia.aem.ws.booking.searchservice.xsd1.ObjectFactory factory1 = new com.alitalia.aem.ws.booking.searchservice.xsd1.ObjectFactory();
			com.alitalia.aem.ws.booking.searchservice.xsd2.ObjectFactory factory2 = new com.alitalia.aem.ws.booking.searchservice.xsd2.ObjectFactory();
			com.alitalia.aem.ws.booking.searchservice.xsd3.ObjectFactory factory3 = new com.alitalia.aem.ws.booking.searchservice.xsd3.ObjectFactory();
			com.alitalia.aem.ws.booking.searchservice.xsd5.ObjectFactory factory5 = new com.alitalia.aem.ws.booking.searchservice.xsd5.ObjectFactory();
			
			Airport fromAirport = factory3.createAirport();
//			fromAirport.setAZDeserves(true);
//			fromAirport.setArea(AreaValue.DOM);
//			fromAirport.setCity(factory3.createAirportCity("Roma"));
			fromAirport.setCityCode(factory3.createAirportCityCode("ROM"));
//			fromAirport.setCityUrl(factory3.createAirportCityUrl(null));
			fromAirport.setCode(factory3.createAirportCode("FCO"));
//			fromAirport.setCountry(factory3.createAirportCountry("Italia"));
			fromAirport.setCountryCode(factory3.createAirportCountryCode("IT"));
//			fromAirport.setDescription(factory3.createAirportDescription("Roma, Fiumicino, Italia"));
//			fromAirport.setDetails(factory3.createAirportDetails(null));
//			fromAirport.setEligibility(true);
//			fromAirport.setEtktAvlblFrom(factory3.createAirportEtktAvlblFrom(null));
//			fromAirport.setHiddenInFirstDeparture(false);
//			fromAirport.setIdMsg(1954);
//			fromAirport.setName(factory3.createAirportName("Fiumicino"));
//			fromAirport.setState(factory3.createAirportState(null));
//			fromAirport.setStateCode(factory3.createAirportStateCode(""));
//			fromAirport.setUnavlblFrom(factory3.createAirportUnavlblFrom(null));
//			fromAirport.setUnavlblUntil(factory3.createAirportUnavlblUntil(null));

			Airport toAirport = factory3.createAirport();
//			toAirport.setAZDeserves(true);
//			toAirport.setArea(AreaValue.DOM);
//			toAirport.setCity(factory3.createAirportCity("Palermo"));
			toAirport.setCityCode(factory3.createAirportCityCode("PMO"));
//			toAirport.setCityUrl(factory3.createAirportCityUrl(null));
			toAirport.setCode(factory3.createAirportCode("PMO"));
//			toAirport.setCountry(factory3.createAirportCountry("Italia"));
			toAirport.setCountryCode(factory3.createAirportCountryCode("IT"));
//			toAirport.setDescription(factory3.createAirportDescription("Palermo, Palermo, Italia"));
//			toAirport.setDetails(factory3.createAirportDetails(null));
//			toAirport.setEligibility(true);
//			toAirport.setEtktAvlblFrom(factory3.createAirportEtktAvlblFrom(null));
//			toAirport.setHiddenInFirstDeparture(false);
//			toAirport.setIdMsg(0);
//			toAirport.setName(factory3.createAirportName("Palermo"));
//			toAirport.setState(factory3.createAirportState(null));
//			toAirport.setStateCode(factory3.createAirportStateCode(""));
//			toAirport.setUnavlblFrom(factory3.createAirportUnavlblFrom(null));
//			toAirport.setUnavlblUntil(factory3.createAirportUnavlblUntil(null));

			SearchDestination sdOutbound = factory3.createSearchDestination();
			XMLGregorianCalendar dateOutbound = dtf.newXMLGregorianCalendar(2015, 07, 15, 00, 00, 00, 0000, DatatypeConstants.FIELD_UNDEFINED);
			sdOutbound.setDepartureDate(dateOutbound);
			sdOutbound.setFrom(factory3.createASearchDestinationFrom(fromAirport));
			sdOutbound.setType(RouteType.OUTBOUND);
			sdOutbound.setTimeType(TimeType.ANYTIME);
			sdOutbound.setTo(factory3.createSearchDestinationTo(toAirport));

			SearchDestination sdReturn = factory3.createSearchDestination();
			XMLGregorianCalendar dateReturn = dtf.newXMLGregorianCalendar(2015, 07, 21, 00, 00, 00, 0000, DatatypeConstants.FIELD_UNDEFINED);
			sdReturn.setDepartureDate(dateReturn);
			sdReturn.setFrom(factory3.createASearchDestinationFrom(toAirport));
			sdReturn.setType(RouteType.RETURN);
			sdReturn.setTimeType(TimeType.ANYTIME);
			sdReturn.setTo(factory3.createSearchDestinationTo(fromAirport));

			ArrayOfanyType destinations = factory5.createArrayOfanyType();
			destinations.getAnyType().add(sdOutbound);
			destinations.getAnyType().add(sdReturn);

			PassengerNumbers pnAdult = factory3.createPassengerNumbers();
			pnAdult.setType(PassengerType.ADULT);
			pnAdult.setNumber(2);

			PassengerNumbers pnChild = factory3.createPassengerNumbers();
			pnChild.setType(PassengerType.CHILD);
			pnChild.setNumber(1);

			PassengerNumbers pnInfant = factory3.createPassengerNumbers();
			pnInfant.setType(PassengerType.INFANT);
			pnInfant.setNumber(1);

			ArrayOfanyType passengerNumbers = factory5.createArrayOfanyType();
			passengerNumbers.getAnyType().add(pnAdult);
			passengerNumbers.getAnyType().add(pnChild);
			passengerNumbers.getAnyType().add(pnInfant);

			BrandSearch brandSearch = factory2.createBrandSearch();
//			brandSearch.setProperties(factory3.createABoomBoxGenericInfoProperties(null));
//			brandSearch.setFarmId(factory3.createABoomBoxInfoFarmId(null));
//			brandSearch.setId(factory3.createABoomBoxInfoId(null));
//			brandSearch.setSessionId(factory3.createABoomBoxInfoSessionId(null));
//			brandSearch.setSolutionSet(factory3.createABoomBoxInfoSolutionSet(null));
			brandSearch.setCUG(factory3.createASearchCUG("ADT"));
			brandSearch.setDestinations(factory3.createASearchDestinations(destinations));
//			brandSearch.setInnerSearch(factory3.createASearchInnerSearch(null));
			brandSearch.setMarket(factory3.createASearchMarket("IT"));
			brandSearch.setOnlyDirectFlight(false);
			brandSearch.setPassengerNumbers(factory3.createASearchPassengerNumbers(passengerNumbers));
//			brandSearch.setSearchCabin(Cabin.ECONOMY);
//			brandSearch.setSearchCabinType(CabinType.PERMITTED);
			brandSearch.setType(SearchType.BRAND_SEARCH);
//			brandSearch.setResidency(ResidencyType.NONE);

			JAXBElement<ASearch> filter = factory1.createExtraChargeRequestFilter(brandSearch);
					
			ExtraChargeRequest request = factory1.createExtraChargeRequest(); 
			request.setFilter(filter);

			@SuppressWarnings("unused")
			ExtraChargeResponse extraCharge = searchServiceClient.extraCharge(request);
	
			log.info("call ok: ");
//			log.info("call ok: " + flights.toString());
		} catch (Exception e) {
			log.info("error");
			e.printStackTrace();
		}
	}
	
	public static void main(String arg []) throws Exception {
		log.info("calling..");
		TestClientSearchserviceExtraChargeBrandsearch client = new TestClientSearchserviceExtraChargeBrandsearch();
		client.execute();
		log.info("call completed");
	} 

	

}
