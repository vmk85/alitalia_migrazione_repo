import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.booking.staticdataservice.StaticDataServiceClient;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.StaticDataRequest;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.StaticDataResponse;
import com.alitalia.aem.ws.booking.staticdataservice.xsd2.StaticDataType;

public class TestClientStaticDataServiceGetDataSeatType {
	
	private StaticDataServiceClient staticDataServiceClient;
	private String serviceEndpoint = "http://romcs0944.user.alitalia.local:9000/WcfBooking/Alitalia.Portal.WebFrontend.LCWP.Booking.Services.Lib.staticdataservice.svc";
	private static final Logger log = LoggerFactory.getLogger(TestClientStaticDataServiceGetDataSeatType.class);
	
	public TestClientStaticDataServiceGetDataSeatType() throws Exception {
		try {
			staticDataServiceClient = new StaticDataServiceClient(serviceEndpoint);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void execute() {
		try {
//			DatatypeFactory dtf = DatatypeFactory.newInstance();
//			com.alitalia.aem.ws.booking.staticdataservice.xsd.ObjectFactory factory = new com.alitalia.aem.ws.booking.staticdataservice.xsd.ObjectFactory();
			com.alitalia.aem.ws.booking.staticdataservice.xsd1.ObjectFactory factory1 = new com.alitalia.aem.ws.booking.staticdataservice.xsd1.ObjectFactory();
//			com.alitalia.aem.ws.booking.staticdataservice.xsd2.ObjectFactory factory2 = new com.alitalia.aem.ws.booking.staticdataservice.xsd2.ObjectFactory();
//			com.alitalia.aem.ws.booking.staticdataservice.xsd3.ObjectFactory factory3 = new com.alitalia.aem.ws.booking.staticdataservice.xsd3.ObjectFactory();
//			com.alitalia.aem.ws.booking.staticdataservice.xsd4.ObjectFactory factory4 = new com.alitalia.aem.ws.booking.staticdataservice.xsd4.ObjectFactory();
//			com.alitalia.aem.ws.booking.staticdataservice.xsd5.ObjectFactory factory5 = new com.alitalia.aem.ws.booking.staticdataservice.xsd5.ObjectFactory();

			StaticDataRequest request = factory1.createStaticDataRequest();
			request.setLanguageCode(factory1.createStaticDataRequestLanguageCode("IT"));
			request.setMarket(factory1.createStaticDataRequestMarket("IT"));
			request.setType(StaticDataType.SEAT_TYPE);
			request.setItemCache(factory1.createStaticDataRequestItemCache(null));

			@SuppressWarnings("unused")
			StaticDataResponse data = staticDataServiceClient.getData(request);
	
			log.info("call ok");
		} catch (Exception e) {
			log.info("error");
			e.printStackTrace();
		}
	}
	
	public static void main(String arg []) throws Exception {
		log.info("calling..");
		TestClientStaticDataServiceGetDataSeatType client = new TestClientStaticDataServiceGetDataSeatType();
		client.execute();
		log.info("call completed");
	} 

	

}
