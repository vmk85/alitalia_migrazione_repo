import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Main {

	private static final Logger logger = LoggerFactory.getLogger(Main.class);
	
	public static void main(String arg []) throws Exception {
		
		int i = 0;
		while(true) {
			logger.info("calling..");
			
			TestCacheThread cacheThread = new TestCacheThread();
			cacheThread.start();
			i++;
			if (i>5)
				Thread.sleep(1000);
			if (i==10)
				break;
		}
		
		
	} 
	
}
