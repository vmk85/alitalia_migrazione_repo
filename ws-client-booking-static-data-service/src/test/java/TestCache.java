import java.util.Hashtable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.booking.staticdataservice.StaticDataServiceClient;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.StaticDataRequest;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.StaticDataResponse;
import com.alitalia.aem.ws.booking.staticdataservice.xsd2.StaticDataType;


public class TestCache {

	private static final Logger logger = LoggerFactory.getLogger(TestCache.class);
	
	private StaticDataServiceClient staticDataServiceClient;
	private String serviceEndpoint = "http://10.46.90.186:9001/WcfBooking/Alitalia.Portal.WebFrontend.LCWP.Booking.Services.Lib.staticdataservice.svc";
	
	private Hashtable<String, Object> cacheMap = new Hashtable<String, Object>();
	private static final String COUNTRY = "COUNTRY";
	
	private Object lock = new Object();
	
	private long lastUpdate = System.currentTimeMillis();
	private long refreshInterval = 10000L;
	
	private static TestCache testCache = null;
	
	public static TestCache getInstance() {
		if (testCache == null) {
			synchronized (TestCache.class) {
				if (testCache == null) 
					testCache = new TestCache();
			}
		}
			
		return testCache;
	}
	
	private TestCache() {
		try {
			staticDataServiceClient = new StaticDataServiceClient(serviceEndpoint);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public StaticDataResponse retrieveCountries() {
		StaticDataResponse staticDataResponse  = (StaticDataResponse)cacheMap.get(COUNTRY);
		
		checkIsCacheToBeRefreshed();
		
		if(staticDataResponse == null) {
			logger.info("TestCache.retrieveCountries - cache miss");
			synchronized (lock) {
				if(cacheMap.get(COUNTRY) == null) {
					staticDataResponse =  this.getCountryList();
					cacheMap.put(COUNTRY, staticDataResponse);
					lastUpdate = System.currentTimeMillis();
					
					logger.info("TestCache.retrieveCountries - put country list in cache");
				} else {
					logger.info("TestCache.retrieveCountries - country list put in cache from other thread");
				}
			}
			return (StaticDataResponse) cacheMap.get(COUNTRY);
		}
		
		logger.info("TestCache.retrieveCountries - return cached country list");
		return staticDataResponse;
	}
	
	private void checkIsCacheToBeRefreshed() {
		long currentTime = System.currentTimeMillis();
		if (currentTime - lastUpdate > refreshInterval) {
			logger.debug("SimpleStaticDataDelegate.checkIsCacheToBeRefreshed - clear cache");
			cacheMap.clear();
		}
	}
	
	public StaticDataResponse getCountryList() {
		StaticDataResponse data = null;
		try {
			com.alitalia.aem.ws.booking.staticdataservice.xsd1.ObjectFactory factory1 = new com.alitalia.aem.ws.booking.staticdataservice.xsd1.ObjectFactory();

			StaticDataRequest request = factory1.createStaticDataRequest();
			request.setLanguageCode(factory1.createStaticDataRequestLanguageCode("IT"));
			request.setMarket(factory1.createStaticDataRequestMarket("IT"));
			request.setType(StaticDataType.COUNTRY);
			request.setItemCache(factory1.createStaticDataRequestItemCache(null));

			data = staticDataServiceClient.getData(request);
	
			logger.info("call ok");
		} catch (Exception e) {
			logger.error("error", e);
		} 
		
		return data;
	}
	

	
	
}
