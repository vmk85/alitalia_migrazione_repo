package com.alitalia.aem.ws.mypersonalarea.millemigliaservice;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;
import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import javax.xml.ws.Binding;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;

import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.wsdl.*;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.*;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd3.ArrayOfKeyValueOfstringstring;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.common.exception.WSClientException;
import com.alitalia.aem.ws.common.logginghandler.SOAPLoggingHandler;
import com.alitalia.aem.ws.common.messagehandler.SOAPMessageHandler;
import com.alitalia.aem.ws.common.utils.WSClientConstants;


@Service(value = MilleMigliaServiceClient.class)
@Component(immediate = true, metatype = true)
@Properties({ @Property(name = "service.vendor", value = "Reply") })
public class MilleMigliaServiceClient {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Property(description = "URL of the 'Static Data Web Service WSDL' used to invoke the Web Service.")
	private static final String WS_ENDPOINT_PROPERTY = "ws.endpoint";

	@Property(description = "Flag used to invoke Web Service in HTTPS.")
	private static final String WS_HTTPS_MODE_PROPERTY = "ws.https_mode";

	@Property(description = "Flag used to load identity store.")
	private static final String WS_LOAD_IDENTITY_STORE_PROPERTY = "ws.load_identity_store";

	@Property(description = "Flag used to enable hostname verifier.")
	private static final String WS_HOSTNAME_VERIFIER_PROPERTY = "ws.hostname_verifier";

	@Property(description = "Property used to set identity store path.")
	private static final String WS_IDENTITY_STORE_PATH_PROPERTY = "ws.identity_store_path";

	@Property(description = "Property used to set identity store password.")
	private static final String WS_IDENTITY_STORE_PASSWORD_PROPERTY = "ws.identity_store_password";
	
	@Property(description = "Property used to set identity key password.")
	private static final String WS_IDENTITY_KEY_PASSWORD_PROPERTY = "ws.identity_key_password";

	@Property(description = "Property used to set trust store path.")
	private static final String WS_TRUST_STORE_PATH_PROPERTY = "ws.trust_store_path";

	@Property(description = "Property used to set trust store password.")
	private static final String WS_TRUST_STORE_PASSWORD_PROPERTY = "ws.trust_store_password";

	@Property(description = "Property used to set connection timout")
	private static final String WS_CONNECTION_TIMEOUT_PROPERTY = "ws.connection_timeout";

	@Property(description = "Property used to set request timout")
	private static final String WS_REQUEST_TIMEOUT_PROPERTY = "ws.request_timeout";

	@Property(description = "Property used to set request timout")
	private static final String WS_SSL_PROTOCL = "ws.ssl_protocol";
	
	private static final String[] ELEMENTS_CONTAINING_TYPE_DECLARATION = {"anyType", "Address", "Lifestyle", "Preference", "Telephone"};
	
	@Property(description = "Property used to print the request")
	private static final String WS_PRINT_REQUET = "ws.print_request";
	
	@Property(description = "Property used to print the response")
	private static final String WS_PRINT_RESPONSE = "ws.print_response";
	
	public String serviceEndpoint;
	private String serviceQNameNamespace;
	private String serviceQNameLocalPart;
	private Integer connectionTimeout;
	private Integer requestTimeout;
	private String identityStorePath;
	private String identityStorePassword;
	private String identityKeyPassword;
	private String trustStorePath;
	private String trustStorePassword;
	private String sslProtocol;
	private boolean hostnameVerifier;
	private boolean loadIdentityStore;
	private boolean httpsMode;
	private boolean printRequest;
	private boolean printResponse;
	private boolean initializationRequired = false;

	private MilleMigliaService milleMigliaService;
	private IMilleMigliaService iMilleMigliaService;
	
	private static final String[] ELEMENT_TO_ANONIMIZE_PATTERN_LIST = {"(<.*PIN>)(.*)(</.*PIN>)"};

	private static final String[] ELEMENT_TO_ANONIMIZE_REPLACEMENT_LIST = {"$1********$3"};

	public MilleMigliaServiceClient() throws WSClientException {
		this.serviceQNameLocalPart = "MilleMigliaService";
		this.serviceQNameNamespace = "http://tempuri.org/";
		setDefault();
	}

	public MilleMigliaServiceClient(String serviceEndpoint) throws WSClientException {
		this.serviceQNameLocalPart = "MilleMigliaService";
		this.serviceQNameNamespace = "http://tempuri.org/";
		setDefault();

		this.serviceEndpoint = serviceEndpoint;

		try {
			init();
		} catch (WSClientException e) {
			initializationRequired = true;
		}
	}

	public MilleMigliaServiceClient(String serviceEndpoint, Integer connectionTimeout, Integer requestTimeout)
			throws WSClientException {
		this.serviceQNameLocalPart = "MilleMigliaService";
		this.serviceQNameNamespace = "http://tempuri.org/";
		setDefault();

		this.serviceEndpoint = serviceEndpoint;
		this.connectionTimeout = connectionTimeout;
		this.requestTimeout = requestTimeout;

		try {
			init();
		} catch (WSClientException e) {
			initializationRequired = true;
		}
	}

	public MilleMigliaServiceClient(String serviceEndpoint, Integer connectionTimeout, Integer requestTimeout,
			Boolean httpsMode, Boolean hostnameVerifier, Boolean loadIdentityStore, String identityStorePath, String identityStorePassword, 
			String identityKeyPassword, String trustStorePath, String trustStorePassword, String sslProtocol) throws WSClientException {
		this.serviceEndpoint = serviceEndpoint;
		this.connectionTimeout = connectionTimeout;
		this.requestTimeout = requestTimeout;
		this.httpsMode = httpsMode;
		this.hostnameVerifier = hostnameVerifier;
		this.loadIdentityStore = loadIdentityStore;
		this.identityStorePath = identityStorePath;
		this.identityStorePassword = identityStorePassword;
		this.identityKeyPassword = identityKeyPassword;
		this.trustStorePath = trustStorePath;
		this.trustStorePassword = trustStorePassword;
		this.sslProtocol = sslProtocol;

		this.serviceQNameLocalPart = "MilleMigliaService";
		this.serviceQNameNamespace = "http://tempuri.org/";
		
		try {
			init();
		} catch (WSClientException e) {
			initializationRequired = true;
		}
	}

	private void init() {
		FileInputStream fisTruststore = null;
		FileInputStream fisIdentitystore = null;
		try {
			URL classURL = MilleMigliaServiceClient.class.getClassLoader().getResource(this.serviceEndpoint);
			QName serviceQName = new QName(serviceQNameNamespace, serviceQNameLocalPart);

			milleMigliaService = new MilleMigliaService(classURL, serviceQName);
			// TODO patch temporanea in attesa dei wsdl corretti
			iMilleMigliaService = milleMigliaService.getBasicHttpEndPoint1();

			BindingProvider bindingProviderNetwork = (BindingProvider) iMilleMigliaService;
			Binding bindingNetwork = bindingProviderNetwork.getBinding();

			@SuppressWarnings("rawtypes")
			List<Handler> handlerListNetwork = bindingNetwork.getHandlerChain();
			SOAPLoggingHandler soapLoggingHandlerNetwork = new SOAPLoggingHandler(this.printRequest, this.printResponse, true,
					new ArrayList<String>(Arrays.asList(ELEMENT_TO_ANONIMIZE_PATTERN_LIST)), 
					new ArrayList<String>(Arrays.asList(ELEMENT_TO_ANONIMIZE_REPLACEMENT_LIST))
					);
			SOAPMessageHandler msgHandler = new SOAPMessageHandler(true);
			msgHandler.setElementsContainingTypeDeclaration(ELEMENTS_CONTAINING_TYPE_DECLARATION);
			handlerListNetwork.add(msgHandler);
			handlerListNetwork.add(soapLoggingHandlerNetwork);

			bindingNetwork.setHandlerChain(handlerListNetwork);

			logger.debug("Https mode [" + httpsMode + "]");

			if (httpsMode) {
				logger.debug("Loaded identity store path [" + identityStorePath + "], trust store path [" + trustStorePath + "], " 
					+ "hostname verifier [" + hostnameVerifier + "] and loading identity store flag [" + loadIdentityStore + "]");

				SSLContext sslContext = SSLContext.getInstance(sslProtocol);

				TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
				KeyStore trustKeystore = KeyStore.getInstance(KeyStore.getDefaultType());
				fisTruststore = new FileInputStream(trustStorePath);
				trustKeystore.load(fisTruststore, trustStorePassword.toCharArray());
				tmf.init(trustKeystore);

				if (loadIdentityStore) {
					KeyStore identityKeystore = KeyStore.getInstance(WSClientConstants.keystoreTypeDefault);
					fisIdentitystore = new FileInputStream(identityStorePath);
					identityKeystore.load(fisIdentitystore, identityStorePassword.toCharArray());

					KeyManagerFactory kmf = KeyManagerFactory.getInstance(WSClientConstants.keymanagerFactoryAlgorithmDefault);
					kmf.init(identityKeystore, identityKeyPassword.toCharArray());
					sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

					logger.debug("Loaded identity and trust store");
				} else {
					sslContext.init(null, tmf.getTrustManagers(), null);
					logger.debug("Loaded trust store only");
				}

				SSLContext.setDefault(sslContext);
				bindingProviderNetwork.getRequestContext().put(WSClientConstants.JAXWS_SSL_SOCKET_FACTORY_PROPERTY,
						sslContext.getSocketFactory());

				if (!hostnameVerifier) {
					bindingProviderNetwork.getRequestContext().put(WSClientConstants.JAXWS_HOSTNAME_VERIFIER_PROPERTY,
							new HostnameVerifier() {

								@Override
								public boolean verify(String arg0, SSLSession arg1) {
									return true;
								}

							});
				}
			}

			((BindingProvider) iMilleMigliaService).getRequestContext().put(WSClientConstants.BINDING_PROVIDER_CONNECT_TIMEOUT_PROPERTY,
					connectionTimeout);
			((BindingProvider) iMilleMigliaService).getRequestContext().put(WSClientConstants.BINDING_PROVIDER_REQUEST_TIMEOUT_PROPERTY,
					requestTimeout);

			logger.debug("Set up request timeout ["
					+ ((BindingProvider) iMilleMigliaService).getRequestContext().get(
							WSClientConstants.BINDING_PROVIDER_REQUEST_TIMEOUT_PROPERTY)
					+ "] and "
					+ "connection timeout ["
					+ ((BindingProvider) iMilleMigliaService).getRequestContext().get(
							WSClientConstants.BINDING_PROVIDER_CONNECT_TIMEOUT_PROPERTY) + "]");
		} catch (java.net.MalformedURLException e) {
			logger.error("Non-blocking error: MilleMigliaServiceClient Inizialization Failed, invalid endpoint = {}", this.serviceEndpoint);
			throw new WSClientException("Error to initialize MilleMigliaServiceClient", e);
		} catch (Exception e) {
			logger.error("Non-blocking error: MilleMigliaServiceClient Inizialization Failed.", e);
			throw new WSClientException("Error to initialize MilleMigliaServiceClient", e);
		} finally {
			if (fisIdentitystore != null) {
				try {
					fisIdentitystore.close();
				} catch (IOException e) {
					logger.error("Error to close input stream", e);
				}
			}
			
			if (fisTruststore != null) {
				try {
					fisTruststore.close();
				} catch (IOException e) {
					logger.error("Error to close input stream", e);
				}
			}
		}
	}

	@Activate
	protected void activate(final Map<String, Object> config) {
		String serviceEndpoint = null;
		boolean httpsMode;
		boolean hostnameVerifier;
		boolean loadIdentityStore;
		boolean printRequest;
		boolean printResponse;
		String identityStorePath = null;
		String identityStorePassword = null;
		String identityKeyPassword = null;
		String trustStorePath = null;
		String trustStorePassword = null;
		Integer connectionTimeout = null;
		Integer requestTimeout = null;
		String sslProtocol = null;

		try {			
			httpsMode = Boolean.valueOf(String.valueOf(config.get(WS_HTTPS_MODE_PROPERTY)));
			this.httpsMode = httpsMode;
			
			hostnameVerifier = Boolean.valueOf(String.valueOf(config.get(WS_HOSTNAME_VERIFIER_PROPERTY)));
			this.hostnameVerifier = hostnameVerifier;
			
			loadIdentityStore = Boolean.valueOf(String.valueOf(config.get(WS_LOAD_IDENTITY_STORE_PROPERTY)));
			this.loadIdentityStore = loadIdentityStore;
			
			identityStorePath = String.valueOf(config.get(WS_IDENTITY_STORE_PATH_PROPERTY));
			this.identityStorePath = identityStorePath;
			
			identityStorePassword = String.valueOf(config.get(WS_IDENTITY_STORE_PASSWORD_PROPERTY));
			this.identityStorePassword = identityStorePassword;
			
			identityKeyPassword = String.valueOf(config.get(WS_IDENTITY_KEY_PASSWORD_PROPERTY));
			this.identityKeyPassword = identityKeyPassword;
			
			trustStorePath = String.valueOf(config.get(WS_TRUST_STORE_PATH_PROPERTY));
			this.trustStorePath = trustStorePath;
			
			trustStorePassword = String.valueOf(config.get(WS_TRUST_STORE_PASSWORD_PROPERTY));
			this.trustStorePassword = trustStorePassword;
			
			connectionTimeout = Integer.valueOf(String.valueOf(config.get(WS_CONNECTION_TIMEOUT_PROPERTY)));
			this.connectionTimeout = connectionTimeout;	
			
			requestTimeout = Integer.valueOf(String.valueOf(config.get(WS_REQUEST_TIMEOUT_PROPERTY)));
			this.requestTimeout = requestTimeout;	
			
			sslProtocol = String.valueOf(config.get(WS_SSL_PROTOCL));
			this.sslProtocol = sslProtocol;	
			
			printRequest = Boolean.valueOf(String.valueOf(config.get(WS_PRINT_REQUET)));
			this.printRequest = printRequest;	
			
			printResponse = Boolean.valueOf(String.valueOf(config.get(WS_PRINT_RESPONSE)));
			this.printResponse = printResponse;			
		} catch (Exception e) {
			logger.error("Error to read custom configuration to activate MilleMigliaServiceClient - default configuration used", e);
			setDefault();
		}
		
		try {			
			serviceEndpoint = String.valueOf(config.get(WS_ENDPOINT_PROPERTY));
			this.serviceEndpoint = serviceEndpoint;
			
			logger.info("Activated MilleMigliaServiceClient: "
					+ "WS_ENDPOINT_PROPERTY=[" + this.serviceEndpoint + "], "
					+ "WS_HTTPS_MODE_PROPERTY=[" + this.httpsMode + "], "
					+ "WS_HOSTNAME_VERIFIER_PROPERTY=[" + this.hostnameVerifier +"], "
					+ "WS_LOAD_IDENTITY_STORE_PROPERTY=[" + this.loadIdentityStore + "], "
					+ "WS_IDENTITY_STORE_PATH_PROPERTY=[" + this.identityStorePath + "], "
					+ "WS_TRUST_STORE_PATH_PROPERTY=[" + this.trustStorePath + "], "
					+ "WS_CONNECTION_TIMEOUT_PROPERTY=[" + this.connectionTimeout + "], "
					+ "WS_REQUEST_TIMEOUT_PROPERTY=[" + this.requestTimeout + "], "
					+ "WS_SSL_PROTOCOL=[" + this.sslProtocol + "], "
					+ "WS_PRINT_RESPONSE=[" + this.printRequest + "], "
					+ "WS_PRINT_REQUET=[" + this.printResponse + "]");
		} catch (Exception e) {
			logger.error("Error to read WS_ENDPOINT_PROPERTY to activate MilleMigliaServiceClient", e);
			throw new WSClientException("Error to initialize MilleMigliaServiceClient", e);
		}
		
		try {
			init();
		} catch (WSClientException e) {
			initializationRequired = true;
		}
	}

	@Modified
	protected void modified(final Map<String, Object> config) {
		String serviceEndpoint = null;
		boolean httpsMode;
		boolean hostnameVerifier;
		boolean loadIdentityStore;
		boolean printRequest;
		boolean printResponse;
		String identityStorePath = null;
		String identityStorePassword = null;
		String identityKeyPassword = null;
		String trustStorePath = null;
		String trustStorePassword = null;
		Integer connectionTimeout = null;
		Integer requestTimeout = null;
		String sslProtocol = null;

		try {			
			httpsMode = Boolean.valueOf(String.valueOf(config.get(WS_HTTPS_MODE_PROPERTY)));
			this.httpsMode = httpsMode;
			
			hostnameVerifier = Boolean.valueOf(String.valueOf(config.get(WS_HOSTNAME_VERIFIER_PROPERTY)));
			this.hostnameVerifier = hostnameVerifier;
			
			loadIdentityStore = Boolean.valueOf(String.valueOf(config.get(WS_LOAD_IDENTITY_STORE_PROPERTY)));
			this.loadIdentityStore = loadIdentityStore;
			
			identityStorePath = String.valueOf(config.get(WS_IDENTITY_STORE_PATH_PROPERTY));
			this.identityStorePath = identityStorePath;
			
			identityStorePassword = String.valueOf(config.get(WS_IDENTITY_STORE_PASSWORD_PROPERTY));
			this.identityStorePassword = identityStorePassword;
			
			identityKeyPassword = String.valueOf(config.get(WS_IDENTITY_KEY_PASSWORD_PROPERTY));
			this.identityKeyPassword = identityKeyPassword;
			
			trustStorePath = String.valueOf(config.get(WS_TRUST_STORE_PATH_PROPERTY));
			this.trustStorePath = trustStorePath;
			
			trustStorePassword = String.valueOf(config.get(WS_TRUST_STORE_PASSWORD_PROPERTY));
			this.trustStorePassword = trustStorePassword;
			
			connectionTimeout = Integer.valueOf(String.valueOf(config.get(WS_CONNECTION_TIMEOUT_PROPERTY)));
			this.connectionTimeout = connectionTimeout;	
			
			requestTimeout = Integer.valueOf(String.valueOf(config.get(WS_REQUEST_TIMEOUT_PROPERTY)));
			this.requestTimeout = requestTimeout;	
			
			sslProtocol = String.valueOf(config.get(WS_SSL_PROTOCL));
			this.sslProtocol = sslProtocol;	
			
			printRequest = Boolean.valueOf(String.valueOf(config.get(WS_PRINT_REQUET)));
			this.printRequest = printRequest;	
			
			printResponse = Boolean.valueOf(String.valueOf(config.get(WS_PRINT_RESPONSE)));
			this.printResponse = printResponse;			
		} catch (Exception e) {
			logger.error("Error to read custom configuration to modify MilleMigliaServiceClient - default configuration used", e);
			setDefault();
		}
		
		try {			
			serviceEndpoint = String.valueOf(config.get(WS_ENDPOINT_PROPERTY));
			this.serviceEndpoint = serviceEndpoint;
			
			logger.info("Modified MilleMigliaServiceClient: "
					+ "WS_ENDPOINT_PROPERTY=[" + this.serviceEndpoint + "], "
					+ "WS_HTTPS_MODE_PROPERTY=[" + this.httpsMode + "], "
					+ "WS_HOSTNAME_VERIFIER_PROPERTY=[" + this.hostnameVerifier +"], "
					+ "WS_LOAD_IDENTITY_STORE_PROPERTY=[" + this.loadIdentityStore + "], "
					+ "WS_IDENTITY_STORE_PATH_PROPERTY=[" + this.identityStorePath + "], "
					+ "WS_TRUST_STORE_PATH_PROPERTY=[" + this.trustStorePath + "], "
					+ "WS_CONNECTION_TIMEOUT_PROPERTY=[" + this.connectionTimeout + "], "
					+ "WS_REQUEST_TIMEOUT_PROPERTY=[" + this.requestTimeout + "], "
					+ "WS_SSL_PROTOCOL=[" + this.sslProtocol + "], "
					+ "WS_PRINT_RESPONSE=[" + this.printRequest + "], "
					+ "WS_PRINT_REQUET=[" + this.printResponse + "]");
		} catch (Exception e) {
			logger.error("Error to read WS_ENDPOINT_PROPERTY to modify MilleMigliaServiceClient", e);
			throw new WSClientException("Error to initialize MilleMigliaServiceClient", e);
		}

		milleMigliaService = null;
		try {
			init();
		} catch(WSClientException e){
			initializationRequired = true;
	}
	}
	
	private void setDefault() {
		this.httpsMode = WSClientConstants.httpsModeDefault;
		this.hostnameVerifier = WSClientConstants.hostnameVerifierDefault;
		this.loadIdentityStore = WSClientConstants.loadIdentityStoreDefault;
		this.connectionTimeout = WSClientConstants.connectionTimeoutDefault;
		this.requestTimeout = WSClientConstants.requestTimeoutDefault;
		this.sslProtocol = WSClientConstants.sslProtocolDefault;
		this.printRequest = WSClientConstants.printRequest;
		this.printResponse = WSClientConstants.printResponse;
	}

	public LoginResponse login(LoginRequest request) throws IMilleMigliaServiceLoginServiceExceptionFaultFaultMessage {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-login...");
			long before = System.currentTimeMillis();
			LoginResponse response = iMilleMigliaService.login(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-login...done execution time=" + execTime);
			return response;
		} catch (IMilleMigliaServiceLoginServiceExceptionFaultFaultMessage e) {
			throw e;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-login", e);
		}
	}

	public GetProfileResponse getProfile(GetProfileRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-getProfile...");
			long before = System.currentTimeMillis();
			GetProfileResponse response = iMilleMigliaService.getProfile(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-getProfile...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-getProfile", e);
		}
	}

	public GetStatementResponse getStatement(GetStatementRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-getStatement...");
			long before = System.currentTimeMillis();
			GetStatementResponse response = iMilleMigliaService.getStatement(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-getStatement...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-getStatement", e);
		}
	}

	public GetStatementByDateResponse getStatementByDate(GetStatementByDateRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-getStatementByDate...");
			long before = System.currentTimeMillis();
			GetStatementByDateResponse response = iMilleMigliaService.getStatementByDate(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-getStatementByDate...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-getStatementByDate", e);
		}
	}

	public GetBalanceResponse getBalance(CustomerRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-getBalance...");
			long before = System.currentTimeMillis();
			GetBalanceResponse response = iMilleMigliaService.getBalance(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-getBalance...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-getBalance", e);
		}
	}

	public GetAddressesResponse getAddresses(GetAddressesRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-getAddresses...");
			long before = System.currentTimeMillis();
			GetAddressesResponse response = iMilleMigliaService.getAddresses(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-getAddresses...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-getAddresses", e);
		}
	}

	public GetMessagesResponse getMessages(GetMessagesRequest request) throws IMilleMigliaServiceGetMessagesServiceFaultFaultFaultMessage {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-getMessages...");
			long before = System.currentTimeMillis();
			GetMessagesResponse response = iMilleMigliaService.getMessages(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-getMessages...done execution time=" + execTime);
			return response;
		} catch (IMilleMigliaServiceGetMessagesServiceFaultFaultFaultMessage e) {
			throw e;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-getMessages", e);
		}
	}

	public GetPreferencesResponse getPreferences(GetPreferencesRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-getPreferences...");
			long before = System.currentTimeMillis();
			GetPreferencesResponse response = iMilleMigliaService.getPreferences(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-getPreferences...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-getPreferences", e);
		}
	}

	public GetLifeStylesResponse getLifeStyles(GetLifeStylesRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-getLifeStyles...");
			long before = System.currentTimeMillis();
			GetLifeStylesResponse response = iMilleMigliaService.getLifeStyles(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-getLifeStyles...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-getLifeStyles", e);
		}
	}

	public GetSmsAuthorizationResponse getSmsAuthorization(GetSmsAuthorizationRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-getSmsAuthorization...");
			long before = System.currentTimeMillis();
			GetSmsAuthorizationResponse response = iMilleMigliaService.getSmsAuthorization(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-getSmsAuthorization...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-getSmsAuthorization", e);
		}
	}

	public UpdateSmsAuthorizationResponse updateSmsAuthorization(UpdateSmsAuthorizationRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-updateSmsAuthorization...");
			long before = System.currentTimeMillis();
			UpdateSmsAuthorizationResponse response = iMilleMigliaService.updateSmsAuthorization(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-updateSmsAuthorization...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-updateSmsAuthorization", e);
		}
	}

	public UpdateMessageViewResponse updateMessageView(UpdateMessageViewRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-updateMessageView...");
			long before = System.currentTimeMillis();
			UpdateMessageViewResponse response = iMilleMigliaService.updateMessageView(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-updateMessageView...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-updateMessageView", e);
		}
	}

	public UpdateProfileResponse updateProfile(UpdateProfileRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-updateProfile...");
			long before = System.currentTimeMillis();
			UpdateProfileResponse response = iMilleMigliaService.updateProfile(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-updateProfile...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-updateProfile", e);
		}
	}

	public RetrievePINResponse retrievePinByCustomerNumber(RetrievePINRequest request) throws IMilleMigliaServiceRetrievePinByCustomerNumberServiceFaultFaultFaultMessage {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-retrievePinByCustomerNumber...");
			long before = System.currentTimeMillis();
			RetrievePINResponse response = iMilleMigliaService.retrievePinByCustomerNumber(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-retrievePinByCustomerNumber...done execution time=" + execTime);
			return response;
		} catch (IMilleMigliaServiceRetrievePinByCustomerNumberServiceFaultFaultFaultMessage e) {
			throw e;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-retrievePinByCustomerNumber", e);
		}
	}

	public RetrievePINResponse retrievePinByNickName(RetrievePINRequest request) throws IMilleMigliaServiceRetrievePinByNickNameServiceFaultFaultFaultMessage {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-retrievePinByNickName...");
			long before = System.currentTimeMillis();
			RetrievePINResponse response = iMilleMigliaService.retrievePinByNickName(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-retrievePinByNickName...done execution time=" + execTime);
			return response;
		} catch (IMilleMigliaServiceRetrievePinByNickNameServiceFaultFaultFaultMessage e) {
			throw e;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-retrievePinByNickName", e);
		}
	}

	public void messageRead(MessageReadRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-messageRead...");
			long before = System.currentTimeMillis();
			iMilleMigliaService.messageRead(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-messageRead...done execution time=" + execTime);
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-messageRead", e);
		}
	}

	public UpdatePINResponse updatePIN(UpdatePINRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-updatePIN...");
			long before = System.currentTimeMillis();
			UpdatePINResponse response = iMilleMigliaService.updatePIN(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-updatePIN...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-updatePIN", e);
		}
	}

	public UpdateNickNameResponse updateNickName(UpdateNickNameRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-updateNickName...");
			long before = System.currentTimeMillis();
			UpdateNickNameResponse response = iMilleMigliaService.updateNickName(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-updateNickName...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-updateNickName", e);
		}
	}

	public SubscribeByDataResponse subscribeByData(CustomerRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-subscribeByData...");
			long before = System.currentTimeMillis();
			SubscribeByDataResponse response = iMilleMigliaService.subscribeByData(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-subscribeByData...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-subscribeByData", e);
		}
	}

	public SubscribeByCustomerResponse subscribeByCustomer(CustomerRequest request) throws IMilleMigliaServiceSubscribeByCustomerServiceExceptionFaultFaultMessage {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-subscribeByCustomer...");
			long before = System.currentTimeMillis();
			SubscribeByCustomerResponse response = iMilleMigliaService.subscribeByCustomer(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-subscribeByCustomer...done execution time=" + execTime);
			return response;
		} catch (IMilleMigliaServiceSubscribeByCustomerServiceExceptionFaultFaultMessage e) {
			throw e;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-subscribeByCustomer", e);
		}
	}

	public SendMailResponse sendRetrieveMail(SendMailRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-sendRetrieveMail...");
			long before = System.currentTimeMillis();
			SendMailResponse response = iMilleMigliaService.sendRetrieveMail(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-sendRetrieveMail...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-sendRetrieveMail", e);
		}
	}

	public SendMailResponse sendNewRegistrationMail(SendMailRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-sendNewRegistrationMail...");
			long before = System.currentTimeMillis();
			SendMailResponse response = iMilleMigliaService.sendNewRegistrationMail(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-sendNewRegistrationMail...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-sendNewRegistrationMail", e);
		}
	}

	public SendMailResponse sendNewMailingListRegistrationMail(SendMailRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-sendNewMailingListRegistrationMail...");
			long before = System.currentTimeMillis();
			SendMailResponse response = iMilleMigliaService.sendNewMailingListRegistrationMail(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-sendNewMailingListRegistrationMail...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-sendNewMailingListRegistrationMail", e);
		}
	}

	public RegisterProgramsResponse registerPrograms(RegisterProgramsRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-registerPrograms...");
			long before = System.currentTimeMillis();
			RegisterProgramsResponse response = iMilleMigliaService.registerPrograms(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-registerPrograms...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-registerPrograms", e);
		}
	}

	public MailingListRegistrationResponse mailingListRegistration(MailingListRegistrationRequest request) throws IMilleMigliaServiceMailingListRegistrationServiceExceptionFaultFaultMessage {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-mailingListRegistration...");
			long before = System.currentTimeMillis();
			MailingListRegistrationResponse response = iMilleMigliaService.mailingListRegistration(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-mailingListRegistration...done execution time=" + execTime);
			return response;
		} catch (IMilleMigliaServiceMailingListRegistrationServiceExceptionFaultFaultMessage e) {
			throw e;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-mailingListRegistration", e);
		}
	}

	public InsertFlightActivityResponse insertFlightActivity(InsertFlightActivityRequest request) throws IMilleMigliaServiceInsertFlightActivityServiceExceptionFaultFaultMessage {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-insertFlightActivity...");
			long before = System.currentTimeMillis();
			InsertFlightActivityResponse response = iMilleMigliaService.insertFlightActivity(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-insertFlightActivity...done execution time=" + execTime);
			return response;
		} catch (IMilleMigliaServiceInsertFlightActivityServiceExceptionFaultFaultMessage e) {
			throw e;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-insertFlightActivity", e);
		}
	}

	public ConvertMilesResponse convertMilesToPayback(ConvertMilesRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-convertMilesToPayback...");
			long before = System.currentTimeMillis();
			ConvertMilesResponse response = iMilleMigliaService.convertMilesToPayback(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-convertMilesToPayback...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-convertMilesToPayback", e);
		}
	}

	public PaybackAccrualResponse paybackAccrual(PaybackAccrualRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-paybackAccrual...");
			long before = System.currentTimeMillis();
			PaybackAccrualResponse response = iMilleMigliaService.paybackAccrual(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-paybackAccrual...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-paybackAccrual", e);
		}
	}

	public GetSocialAccountResponse getSocialAccounts(GetSocialAccountRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-getSocialAccounts...");
			long before = System.currentTimeMillis();
			GetSocialAccountResponse response = iMilleMigliaService.getSocialAccounts(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-getSocialAccounts...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-getSocialAccounts", e);
		}
	}

	public SetSocialAccountResponse setSocialAccount(SetSocialAccountRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-setSocialAccount...");
			long before = System.currentTimeMillis();
			SetSocialAccountResponse response = iMilleMigliaService.setSocialAccount(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-setSocialAccount...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-setSocialAccount", e);
		}
	}

	public RemoveSocialAccountResponse removeUidGigya(RemoveSocialAccountRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-removeUidGigya...");
			long before = System.currentTimeMillis();
			RemoveSocialAccountResponse response = iMilleMigliaService.removeUidGigya(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-removeUidGigya...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-removeUidGigya", e);
		}
	}

	public GetMMUserFromSocialResponse getMMCode(GetMMUserFromSocialRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-getMMCode...");
			long before = System.currentTimeMillis();
			GetMMUserFromSocialResponse response = iMilleMigliaService.getMMCode(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-getMMCode...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-getMMCode", e);
		}
	}

	public CreateSignatureResponse createSignature(CreateSignatureRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-createSignature...");
			long before = System.currentTimeMillis();
			CreateSignatureResponse response = iMilleMigliaService.createSignature(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-createSignature...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-createSignature", e);
		}
	}

	public GetCookieNameResponse getCookieName(GetCookieNameRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-getCookieName...");
			long before = System.currentTimeMillis();
			GetCookieNameResponse response = iMilleMigliaService.getCookieName(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-getCookieName...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-getCookieName", e);
		}
	}

	public GetUidFromCookieResponse getUidFromCookie(GetUidFromCookieRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-getUidFromCookie...");
			long before = System.currentTimeMillis();
			GetUidFromCookieResponse response = iMilleMigliaService.getUidFromCookie(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-getUidFromCookie...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-getUidFromCookie", e);
		}
	}

	public NotifyLoginResponse notifyLogin(NotifyLoginRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-notifyLogin...");
			long before = System.currentTimeMillis();
			NotifyLoginResponse response = iMilleMigliaService.notifyLogin(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-notifyLogin...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-notifyLogin", e);
		}
	}

	public GetCreditCardTypeListResponse getCreditCardTypeList(GetCreditCardTypeListRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-getCreditCardTypeList...");
			long before = System.currentTimeMillis();
			GetCreditCardTypeListResponse response = iMilleMigliaService.getCreditCardTypeList(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-getCreditCardTypeList...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-getCreditCardTypeList", e);
		}
	}

	public GetCreditCardListResponse getCreditCardList(GetCreditCardListRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-getCreditCardList...");
			long before = System.currentTimeMillis();
			GetCreditCardListResponse response = iMilleMigliaService.getCreditCardList(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-getCreditCardList...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-getCreditCardList", e);
		}
	}

	public DeletePanSIAResponse deletePanSIA(DeletePanSIARequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}
		
		try {
			logger.info("Calling WS-MilleMigliaService Operation-deletePanSIA...");
			long before = System.currentTimeMillis();
			DeletePanSIAResponse response = iMilleMigliaService.deletePanSIA(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-deletePanSIA...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-deletePanSIA", e);
		}
	}

	public Integer getConnectionTimeout() {
		return connectionTimeout;
	}

	public Integer getRequestTimeout() {
		return requestTimeout;
	}

	public String getIdentityStorePath() {
		return identityStorePath;
	}

	public String getIdentityStorePassword() {
		return identityStorePassword;
	}

	public String getTrustStorePath() {
		return trustStorePath;
	}

	public String getTrustStorePassword() {
		return trustStorePassword;
	}

	public boolean getHostnameVerifier() {
		return hostnameVerifier;
	}

	public boolean getLoadIdentityStore() {
		return loadIdentityStore;
	}

	public boolean getHttpsMode() {
		return httpsMode;
	}
	
	public String getSslProtocol() {
		return sslProtocol;
	}
	
	public String getIdentityKeyPassword() {
		return identityKeyPassword;
	}
	
	public boolean isPrintRequest() {
		return printRequest;
	}

	public boolean isPrintResponse() {
		return printResponse;
	}

	// inizio - implementazione metodi wsClient per "Millemiglia - nuove credenziali con Strong Authentication"


	//CheckCredenzialiOLD
	public CheckCredenzialiOLDResponse CheckCredenzialiOLD(CheckCredenzialiOLDRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}

		try {
			logger.info("Calling WS-MilleMigliaService Operation-CheckCredenzialiOLD...");
			long before = System.currentTimeMillis();
			CheckCredenzialiOLDResponse response = iMilleMigliaService.checkCredenzialiOLD(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-CheckCredenzialiOLD...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-CheckCredenzialiOLD", e);
		}
	}

	//FindProfile
	public FindProfileResponse CheckCredenzialiOLD(FindProfileRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}

		try {
			logger.info("Calling WS-MilleMigliaService Operation-FindProfile...");
			long before = System.currentTimeMillis();
			FindProfileResponse response = iMilleMigliaService.findProfile(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-FindProfile...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-FindProfile", e);
		}
	}


	//GetDatiSicurezzaProfiloOLD
	public GetDatiSicurezzaProfiloOLDResponse GetDatiSicurezzaProfiloOLD(GetDatiSicurezzaProfiloOLDRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}

		try {
			logger.info("Calling WS-MilleMigliaService Operation-GetDatiSicurezzaProfiloOLD...");
			long before = System.currentTimeMillis();
			GetDatiSicurezzaProfiloOLDResponse response = iMilleMigliaService.getDatiSicurezzaProfiloOLD(request) ;
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-GetDatiSicurezzaProfiloOLD...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-GetDatiSicurezzaProfiloOLD", e);
		}
	}

	//SetDatiSicurezzaProfiloOLD
	public SetDatiSicurezzaProfiloOLDResponse SetDatiSicurezzaProfiloOLD(SetDatiSicurezzaProfiloOLDRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}

		try {
			logger.info("Calling WS-MilleMigliaService Operation-CheckCredenziali...");
			long before = System.currentTimeMillis();
			SetDatiSicurezzaProfiloOLDResponse response = iMilleMigliaService.setDatiSicurezzaProfiloOLD(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-CheckCredenziali...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-CheckCredenziali", e);
		}
	}

	//CheckCredenziali
	public CheckCredenzialiResponse CheckCredenziali(CheckCredenzialiRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}

		try {
			logger.info("Calling WS-MilleMigliaService Operation-CheckCredenziali...");
			long before = System.currentTimeMillis();
			CheckCredenzialiResponse response = iMilleMigliaService.checkCredenziali(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-CheckCredenziali...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-CheckCredenziali", e);
		}
	}

	//GetDatiSicurezzaProfilo
	public GetDatiSicurezzaProfiloResponse GetDatiSicurezzaProfilo(GetDatiSicurezzaProfiloRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}

		try {
			logger.info("Calling WS-MilleMigliaService Operation-GetDatiSicurezzaProfilo...");
			long before = System.currentTimeMillis();
			GetDatiSicurezzaProfiloResponse response = iMilleMigliaService.getDatiSicurezzaProfilo(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-GetDatiSicurezzaProfilo...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-GetDatiSicurezzaProfilo", e);
		}
	}

//	//GetDatiProfilo NON c'e piu' - 09 agosto 2018
//	public GetDatiProfiloResponse GetDatiProfilo(GetDatiProfiloRequest request) {
//		if (initializationRequired) {
//			init();
//			initializationRequired = false;
//		}
//
//		try {
//			logger.info("Calling WS-MilleMigliaService Operation-GetDatiProfilo...");
//			long before = System.currentTimeMillis();
//			GetDatiProfiloResponse response = iMilleMigliaService.getDatiProfilo(request) ;
//			long after = System.currentTimeMillis();
//			long execTime = after - before;
//
//			logger.info("Calling WS-MilleMigliaService Operation-GetDatiProfilo...done execution time=" + execTime);
//			return response;
//		} catch (Exception e) {
//			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-GetDatiProfilo", e);
//		}
//	}

	//SetDatiSicurezzaProfilo
	public SetDatiSicurezzaProfiloResponse SetDatiSicurezzaProfilo(SetDatiSicurezzaProfiloRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}

		try {
			logger.info("Calling WS-MilleMigliaService Operation-SetDatiSicurezzaProfilo...");
			long before = System.currentTimeMillis();
			SetDatiSicurezzaProfiloResponse response = iMilleMigliaService.setDatiSicurezzaProfilo(request);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-SetDatiSicurezzaProfilo...done execution time=" + execTime);
			return response;

		} catch (IMilleMigliaServiceSetDatiSicurezzaProfiloServiceFaultFaultFaultMessage e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-SetDatiSicurezzaProfilo", e);
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-SetDatiSicurezzaProfilo", e);
		}


	}

//	//SetDatiProfilo - non esiste piu - 08 agosto 2018
//	public SetDatiProfiloResponse SetDatiProfilo(SetDatiProfiloRequest request) {
//		if (initializationRequired) {
//			init();
//			initializationRequired = false;
//		}
//
//		try {
//			logger.info("Calling WS-MilleMigliaService Operation-SetDatiProfilo...");
//			long before = System.currentTimeMillis();
//			SetDatiProfiloResponse response = iMilleMigliaService.setDatiProfilo(request) ;
//			long after = System.currentTimeMillis();
//			long execTime = after - before;
//
//			logger.info("Calling WS-MilleMigliaService Operation-SetDatiProfilo...done execution time=" + execTime);
//			return response;
//		} catch (Exception e) {
//			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-SetDatiProfilo", e);
//		}
//	}

	//CheckEmailUsernameDuplicate
	public CheckEmailUsernameDuplicateResponse CheckEmailUsernameDuplicate(CheckEmailUsernameDuplicateRequest request) {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}

		try {
			logger.info("Calling WS-MilleMigliaService Operation-CheckEmailUsernameDuplicate...");
			long before = System.currentTimeMillis();

			CheckEmailUsernameDuplicateResponse response = iMilleMigliaService.checkEmailUsernameDuplicate(request);

			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-CheckEmailUsernameDuplicate...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-CheckEmailUsernameDuplicate", e);
		}
	}

	public LoginSAResponse loginSA(LoginSARequest request) throws IMilleMigliaServiceLoginSAServiceExceptionFaultFaultMessage {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}

		try {
			logger.info("Calling WS-MilleMigliaService Operation-loginSA...");
			long before = System.currentTimeMillis();
			LoginSAResponse response = iMilleMigliaService.loginSA(request) ;
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-loginSA...done execution time=" + execTime);
			return response;
		} catch (IMilleMigliaServiceLoginSAServiceExceptionFaultFaultMessage e) {
			throw e;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-loginSA", e);
		}
	}
	//SendMailMessageToEmailAccount
	// Non c'e' - useremo MilleMigliaServiceClient.sendRetrieveMail

	//SendOtpViaCellulare
	// Non c'e'. - useremo   OTPServiceClient.sendOTP

	//getOTP pare che non sia ancora pubblicato in DEV
	// Non disponibile. - useremo   OTPServiceClient.sendOTP(numcell=0) non manda messaggi
	// ma restituisce OTP e scadenza


	public SubscribeSAResponse subscribeSA(SubscribeSARequest request) throws IMilleMigliaServiceSubscribeSAServiceExceptionFaultFaultMessage {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}

		try {
			logger.info("Calling WS-MilleMigliaService Operation-subscribeSA...");
			long before = System.currentTimeMillis();
			SubscribeSAResponse response = iMilleMigliaService.subscribeSA(request) ;
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-subscribeSA...done execution time=" + execTime);
			return response;
		} catch (IMilleMigliaServiceSubscribeSAServiceExceptionFaultFaultMessage e) {
			throw e;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-subscribeSA", e);
		}
	}

	// nuovi metodi ///////////////////

	public CheckEmailUsernameDuplicateResponse CheckEmailUserNameDuplicate (CheckEmailUsernameDuplicateRequest request) throws IMilleMigliaServiceCheckEmailUsernameDuplicateServiceFaultFaultFaultMessage {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}

		try {
			logger.info("Calling WS-MilleMigliaService Operation-CheckEmailUserNameDuplicate...");
			long before = System.currentTimeMillis();
			CheckEmailUsernameDuplicateResponse response = iMilleMigliaService.checkEmailUsernameDuplicate(request) ;
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-CheckEmailUserNameDuplicate...done execution time=" + execTime);
			return response;
		} catch (IMilleMigliaServiceCheckEmailUsernameDuplicateServiceFaultFaultFaultMessage e) {
			throw e;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-CheckEmailUserNameDuplicate", e);
		}
	}




	public CheckUsernameDuplicateResponse CheckUserNameDuplicate (CheckUsernameDuplicateRequest request) throws IMilleMigliaServiceCheckUsernameDuplicateServiceFaultFaultFaultMessage {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}

		try {
			logger.info("Calling WS-MilleMigliaService Operation-CheckUserNameDuplicate...");
			long before = System.currentTimeMillis();
			CheckUsernameDuplicateResponse response = iMilleMigliaService.checkUsernameDuplicate(request) ;
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-CheckUserNameDuplicate...done execution time=" + execTime);
			return response;
		} catch (IMilleMigliaServiceCheckUsernameDuplicateServiceFaultFaultFaultMessage e) {
			throw e;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-CheckUserNameDuplicate", e);
		}
	}

	public FindProfileResponse FindProfile (FindProfileRequest request) throws IMilleMigliaServiceFindProfileServiceFaultFaultFaultMessage {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}

		try {
			logger.info("Calling WS-MilleMigliaService Operation-FindProfile...");
			long before = System.currentTimeMillis();
			FindProfileResponse response = iMilleMigliaService.findProfile(request) ;
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-FindProfile...done execution time=" + execTime);
			return response;
		} catch (IMilleMigliaServiceFindProfileServiceFaultFaultFaultMessage e) {
			//workaround: in caso di serviceFault >> "Customer not found"
			// restituisco io la response.FlagIDProfiloExists=false
			/*
			 <ServiceFault xmlns="http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.MyPersonalArea.Services.Lib.DataContract" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
               <ReasonErrorMessageCode>FindProfile</ReasonErrorMessageCode>
               <ReasonInformationMessageCode>None</ReasonInformationMessageCode>
               <ReasonType>Error</ReasonType>
               <ReasonWarningMessageCode>None</ReasonWarningMessageCode>
               <RemoteSystemMessageDictionary xmlns:a="http://schemas.microsoft.com/2003/10/Serialization/Arrays">
                  <a:KeyValueOfstringstring>
                     <a:Key>ErrorMessage</a:Key>
                     <a:Value>; Customer not found</a:Value>
                  </a:KeyValueOfstringstring>
               </RemoteSystemMessageDictionary>
            </ServiceFault>
			* */
			ServiceFault faultInfo = e.getFaultInfo();
			ArrayOfKeyValueOfstringstring dict = faultInfo.getRemoteSystemMessageDictionary().getValue();
			List<ArrayOfKeyValueOfstringstring.KeyValueOfstringstring> values = dict.getKeyValueOfstringstring();

			if(values.contains("; Customer not found"))
			{
				FindProfileResponse response=new FindProfileResponse();
				response.setFlagIDProfiloExists(false);
				return response;
			}else
			{
				throw e;
			}
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-FindProfile", e);
		}
	}

	public UpdateSmsAuthorizationResponse UpdateSmsAuthorization (UpdateSmsAuthorizationRequest request) throws IMilleMigliaServiceUpdateSmsAuthorizationServiceFaultFaultFaultMessage {
		if (initializationRequired) {
			init();
			initializationRequired = false;
		}

		try {
			logger.info("Calling WS-MilleMigliaService Operation-UpdateSmsAuthorization...");
			long before = System.currentTimeMillis();
			UpdateSmsAuthorizationResponse response = iMilleMigliaService.updateSmsAuthorization(request) ;
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-MilleMigliaService Operation-UpdateSmsAuthorization...done execution time=" + execTime);
			return response;
		} catch (IMilleMigliaServiceUpdateSmsAuthorizationServiceFaultFaultFaultMessage e) {
			throw e;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-MilleMigliaService Operation-UpdateSmsAuthorization", e);
		}
	}

	////////////////////////////////

	// fine - implementazione metodi wsClient per "Millemiglia - nuove credenziali con Strong Authentication"
}