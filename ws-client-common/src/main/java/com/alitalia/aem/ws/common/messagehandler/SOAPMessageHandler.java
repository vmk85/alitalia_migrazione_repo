package com.alitalia.aem.ws.common.messagehandler;

import java.util.Set;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import com.alitalia.aem.ws.common.utils.WSClientUtility;

@SuppressWarnings("rawtypes")
public class SOAPMessageHandler implements SOAPHandler {

	private static Logger log = LoggerFactory.getLogger(SOAPMessageHandler.class);

	private String[] outboundElementsContainingTypeDeclaration = null;
	private String namespaceToRemove = null;
	private boolean inboundTraverse = false;

	public SOAPMessageHandler() {
	}

	public SOAPMessageHandler(boolean inboundTraverse) {
		this.inboundTraverse = inboundTraverse;
	}

	@Override
	public Set getHeaders() {
		return null;
	}

	@Override
	public boolean handleMessage(MessageContext messageContext) {
		Boolean outboundProperty = (Boolean) messageContext.get (MessageContext.MESSAGE_OUTBOUND_PROPERTY);

		if (outboundProperty.booleanValue()) 
			removeOutboundUnwantedNamespace(messageContext);

		if (this.inboundTraverse && !outboundProperty.booleanValue()){
			log.debug("Removing inbound unwanted namespace");
			removeInboundUnwantedNamespace(messageContext);
		}

		return true;
	}

	@Override
	public void close(MessageContext messageContext) {}

	private void removeOutboundUnwantedNamespace(MessageContext mc) {
		try {
			SOAPMessage soapMessage = ((SOAPMessageContext) mc).getMessage();
			SOAPBody soapBody = soapMessage.getSOAPBody();

			RemoveOutboundUnusedUnwantedNamespaces nsRemover = new RemoveOutboundUnusedUnwantedNamespaces();

			Document bodyDoc = soapBody.getOwnerDocument();

			if (outboundElementsContainingTypeDeclaration == null)
				nsRemover.process(bodyDoc);
			else
				nsRemover.process(bodyDoc, outboundElementsContainingTypeDeclaration, namespaceToRemove);
			soapMessage.saveChanges();
		} catch (Exception e) {			
			log.error("Error getting output soap message", e);		
		}
	}

	private void removeInboundUnwantedNamespace(MessageContext mc) {
		try {
			SOAPMessage soapMessage = ((SOAPMessageContext) mc).getMessage();
			SOAPBody soapBody = soapMessage.getSOAPBody();

			RemoveInboundUnusedUnwantedNamespaces nsRemover = new RemoveInboundUnusedUnwantedNamespaces();

			Document bodyDoc = soapBody.getOwnerDocument();

			nsRemover.process(bodyDoc);

			soapMessage.saveChanges();
		} catch (Exception e) {			
			log.error("Error getting input soap message", e);		
		}
	}

	@Override
	public boolean handleFault(MessageContext context) {
		log.debug("Fault detected in SOAPMessageHandler");
		//logging(context);
		return false;
	}

	public String[] getOutboundElementsContainingTypeDeclaration() {
		return outboundElementsContainingTypeDeclaration;
	}

	public void setElementsContainingTypeDeclaration(String[] outboundElementsContainingTypeDeclaration) {
		this.outboundElementsContainingTypeDeclaration = outboundElementsContainingTypeDeclaration;
	}

	public String getNamespaceToRemove() {
		return namespaceToRemove;
	}

	public void setNamespaceToRemove(String namespaceToRemove) {
		this.namespaceToRemove = namespaceToRemove;
	}

	private void logging(MessageContext mc) {

		try {
			
			log.debug("SOAPMessageHandler logging SOAP message...");
			
			if(mc != null){
				StringBuffer logMessage = new StringBuffer();

				String service = null;
				String operation = null;
				try {
					service = mc.get(MessageContext.WSDL_SERVICE).toString();
					operation = mc.get(MessageContext.WSDL_OPERATION).toString();
					logMessage.append("[" + service + " / " + operation + "] ");
				} catch (Exception e) {
					log.warn("SOAPMessageHandler. Cannot obtain WSDL service name or operation name for logging purpose.");
				}

				Boolean outboundProperty = (Boolean) mc.get (MessageContext.MESSAGE_OUTBOUND_PROPERTY);

				if (log.isDebugEnabled())
					print(mc, logMessage, outboundProperty);
			}

		} catch (Exception e) {			
			log.error("SOAPMessageHandler. Error getting output soap message", e);		
		}
	}
	
	private void print(MessageContext mc, StringBuffer logMessage, Boolean outboundProperty) throws Exception {
		
		logMessage.append((outboundProperty.booleanValue()) ? "SOAPMessageHandler Request message. " : "SOAPMessageHandler Response message. ");
		logMessage.append(WSClientUtility.deserializeSoapMessage(((SOAPMessageContext) mc).getMessage()));
		log.debug(logMessage.toString());
		
	}

}
