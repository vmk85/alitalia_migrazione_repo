package com.alitalia.aem.ws.common.messagehandler;

import java.util.HashSet;
import java.util.Set;

import org.w3c.dom.Element;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class RemoveOutboundUnusedUnwantedNamespaces {

    private static final String XML_NAMESPACE_SCHEMA_INSTANCE = "http://www.w3.org/2001/XMLSchema-instance";

    private static final String XML_NAMESPACE_NAMESPACE = "http://www.w3.org/2000/xmlns/";
    
    /**
     * ELEMENTS_CONTAINING_TYPE_DECLARATION contains the name of the elements that can have an attribute xsi:type
     * that contains a namespace prefix before the definition of the type. This leads to an error
     * on the server side. Add here element name if needed 
     */
    private static final String[] ELEMENTS_CONTAINING_TYPE_DECLARATION = {"anyType"};

    private String prefixNamespaceToRemove = null;

    private interface ElementVisitor {

        void visit(Element element);

    }

    /**
     * Traverses an XML document searching for unused namespaces or unwanted namespaces in xsi:type attributes.
     * Elements list is the one defined as default in ELEMENTS_CONTAINING_TYPE_DECLARATION constant
     * 
     * @param document XML document to be processed
     */
    public void process(Document document) {
    	this.process(document, ELEMENTS_CONTAINING_TYPE_DECLARATION, null);
    }

    /**
     * Traverses an XML document searching for unused namespaces or unwanted namespaces in xsi:type attributes.
     * 
     * @param document XML document to be processed
     * @param elementsContainingTypeDeclaration array of elements names for which namespace prefix in xsi:type
     * must be removed
     */
    public void process(Document document, final String[] elementsContainingTypeDeclaration) {
    	this.process(document, elementsContainingTypeDeclaration, null);
    }

    /**
     * Traverses an XML document searching for unused namespaces or unwanted namespaces in xsi:type attributes.
     * 
     * @param document XML document to be processed
     * @param elementsContainingTypeDeclaration array of elements names for which namespace prefix in xsi:type
     * must be removed
     * @param namespaceToRemove specific namespace for which the prefix must be removed in xsi:type attribute
     */
    public void process(Document document, final String[] elementsContainingTypeDeclaration, final String namespaceToRemove) {
        final Set<String> namespaces = new HashSet<String>();

        Element element = document.getDocumentElement();
        traverse(element, new ElementVisitor() {

        	@Override
            public void visit(Element element) {
                String namespace = element.getNamespaceURI();
                if (namespace == null)
                    namespace = "";
                namespaces.add(namespace);

                /*
                 * Gets namespace prefix for specific namespace passed in input that must be removed  
                 */
                if (namespaceToRemove != null && namespaceToRemove.equals(namespace) && prefixNamespaceToRemove == null) {
                	prefixNamespaceToRemove = element.getPrefix();
                }

                NamedNodeMap attributes = element.getAttributes();
                for (int i = 0; i < attributes.getLength(); i++) {
                    Node node = attributes.item(i);
                    if (XML_NAMESPACE_NAMESPACE.equals(node.getNamespaceURI()))
                        continue;
                    String prefix;
                    if (XML_NAMESPACE_SCHEMA_INSTANCE.equals(node.getNamespaceURI())) {
                        if ("type".equals(node.getLocalName())) {
                            String value = node.getNodeValue();
                            if (value.contains(":")) 
                                prefix = value.substring(0, value.indexOf(":"));
                            else
                                prefix = null;
                            if (value.contains(":")) {
                            	for(String elemName : elementsContainingTypeDeclaration) {
                            		if (elemName.equals(element.getLocalName()))
                            		{
                            			if (namespaceToRemove == null ||
                            					(prefix != null && prefix.equals(prefixNamespaceToRemove))) {
	    	                            	node.setNodeValue(value.substring(value.indexOf(":") + 1));
	    	                            	prefix = null;
	    	                            	break;
                            			}
                            		}
                            	}
                            }
                        } else {
                            continue;
                        }
                    } else {
                        prefix = node.getPrefix();
                    }
                    namespace = element.lookupNamespaceURI(prefix);
                    if (namespace == null)
                        namespace = "";
                    namespaces.add(namespace);
                }
            }

        });
        traverse(element, new ElementVisitor() {

        	@Override
            public void visit(Element element) {
                Set<String> removeLocalNames = new HashSet<String>();
                NamedNodeMap attributes = element.getAttributes();
                for (int i = 0; i < attributes.getLength(); i++) {
                    Node node = attributes.item(i);
                    if (!XML_NAMESPACE_NAMESPACE.equals(node.getNamespaceURI()))
                        continue;
                    if (namespaces.contains(node.getNodeValue()))
                        continue;
                    removeLocalNames.add(node.getLocalName());
                }
                for (String localName : removeLocalNames)
                    element.removeAttributeNS(XML_NAMESPACE_NAMESPACE, localName);
            }

        });
    }

    private final void traverse(Element element, ElementVisitor visitor) {
        visitor.visit(element);
        NodeList children = element.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node node = children.item(i);
            if (node.getNodeType() != Node.ELEMENT_NODE)
                continue;
            traverse((Element) node, visitor);
        }
    }

}