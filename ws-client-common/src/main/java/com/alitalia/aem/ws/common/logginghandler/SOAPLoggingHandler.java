package com.alitalia.aem.ws.common.logginghandler;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.common.utils.WSClientUtility;

@SuppressWarnings("rawtypes")
public class SOAPLoggingHandler implements SOAPHandler {

	private static Logger log = LoggerFactory.getLogger(SOAPLoggingHandler.class);
	private boolean printRequest = true;
	private boolean printResponse = true;
	private Hashtable<String, SOAPMessage> mapSoapMsg = null;
	private List<String> anonymizePatternsList = null;
	private List<String> anonymizeReplacementsList = null;
	private long lastCleanMap = System.currentTimeMillis();
	private long cleanInterval = 3600000L;
	private long timeObsoleteMessage = 180000L;

	public SOAPLoggingHandler() {}
	
	public SOAPLoggingHandler(boolean printRequest, boolean printResponse) {
		this.printRequest = printRequest;
		this.printResponse = printResponse;
	}
	
	public SOAPLoggingHandler(boolean printRequest, boolean printResponse, boolean cacheSoapMsg) {
		this.printRequest = printRequest;
		this.printResponse = printResponse;
		if (cacheSoapMsg)
			this.mapSoapMsg = new Hashtable<String, SOAPMessage>();
	}
		
	public SOAPLoggingHandler(boolean printRequest, boolean printResponse, boolean cacheSoapMsg,
			List<String> patternsList, List<String> replacementsList) {
		this.printRequest = printRequest;
		this.printResponse = printResponse;
		if (cacheSoapMsg)
			this.mapSoapMsg = new Hashtable<String, SOAPMessage>();
		this.anonymizePatternsList = patternsList;
		this.anonymizeReplacementsList = replacementsList;
	}
		
	@Override
	public Set getHeaders() {
		return null;
	}

	@Override
	public boolean handleMessage(MessageContext messageContext) {
		if (mapSoapMsg!= null && isMapToBeCleaned()){
			cleanMap();
		}
		loggingOk(messageContext);
		return true;
	}

	@Override
	public boolean handleFault(MessageContext messageContext) {
		if (mapSoapMsg!= null && isMapToBeCleaned()){
			cleanMap();
		}
		loggingKo(messageContext);
		return true;
	}

	@Override
	public void close(MessageContext messageContext) {}

	private void loggingOk(MessageContext mc) {
		
		if (this.printRequest || this.printResponse) {
			try {
				
				StringBuffer logMessage = new StringBuffer();
				
				String service = null;
				String operation = null;
				try {
					service = mc.get(MessageContext.WSDL_SERVICE).toString();
					operation = mc.get(MessageContext.WSDL_OPERATION).toString();
					logMessage.append("[" + service + " / " + operation + "] ");
				} catch (Exception e) {
					log.warn("Cannot obtain WSDL service name or operation name for logging purpose.");
				}
				
				Boolean outboundProperty = (Boolean) mc.get (MessageContext.MESSAGE_OUTBOUND_PROPERTY);
				
				if (outboundProperty.booleanValue()) { //Request
					if (this.printRequest) {
						if (log.isDebugEnabled()) {
							print(mc, logMessage, outboundProperty);
						} else if (this.mapSoapMsg != null) { //Debug disabilitato e meccanismo cache abilitato
							String correlationID = System.currentTimeMillis() + "_" + UUID.randomUUID().toString(); //Genero correlationid univoco
							//log.debug("TEST SOAPLOGGINGHANDLER Request Ok: correlationId ["+correlationID+"]");
							this.mapSoapMsg.put(correlationID, ((SOAPMessageContext) mc).getMessage()); //Inserisco la request sulla mappa
							mc.put("INBOUND_ID", correlationID); //Faccio put del correlation id per response
							//log.debug("TEST SOAPLOGGINGHANDLER Request Ok: Inserita request su mappa e INBOUND_ID in MessageContext. Nuova dimensione mappa: "+this.mapSoapMsg.size());
						}
					} else {
						log.debug("Logging for request disabled by ws client configuration");
					}
				} else { //Response
					if (this.printResponse) {
						if (log.isDebugEnabled()){
							print(mc, logMessage, outboundProperty);
						} else if (this.mapSoapMsg != null){
							String correlationID = (String)mc.get("INBOUND_ID"); // Recupero correlationId da MessageContext
							this.mapSoapMsg.remove(correlationID); //Pulisco mappa
							//log.debug("TEST SOAPLOGGINGHANDLER Response Ok: Elemento rimosso da Mappa. Nuova dimensione mappa: "+this.mapSoapMsg.size());
						}
					} else {
						log.debug("Logging for response disabled by ws client configuration");
					}
				}

			} catch (Exception e) {		
				if (mc.get("INBOUND_ID") != null){
					this.mapSoapMsg.remove((String)mc.get("INBOUND_ID"));
				}
				log.error("Error getting output soap message", e);		
			}
		}
	}		
	
	private void loggingKo(MessageContext mc) {
		
		if (this.printRequest || this.printResponse) {
			try {
				
				StringBuffer logMessage = new StringBuffer();
				
				String service = null;
				String operation = null;
				try {
					service = mc.get(MessageContext.WSDL_SERVICE).toString();
					operation = mc.get(MessageContext.WSDL_OPERATION).toString();
					logMessage.append("[" + service + " / " + operation + "] ");
				} catch (Exception e) {
					log.warn("Cannot obtain WSDL service name or operation name for logging purpose.");
				}
				
				Boolean outboundProperty = (Boolean) mc.get (MessageContext.MESSAGE_OUTBOUND_PROPERTY);
				
				if (outboundProperty.booleanValue()) { //Request
					if (this.printRequest) {
						if (log.isDebugEnabled()) {
							print(mc, logMessage, outboundProperty);
						} else if (this.mapSoapMsg != null) { //Debug disabilitato e meccanismo cache abilitato
							String correlationID = System.currentTimeMillis() + "_" + UUID.randomUUID().toString(); //Genero correlationid univoco
							//log.debug("TEST SOAPLOGGINGHANDLER Request Ko: correlationId ["+correlationID+"]");
							this.mapSoapMsg.put(correlationID, ((SOAPMessageContext) mc).getMessage()); //Inserisco la request sulla mappa
							mc.put("INBOUND_ID", correlationID); //Faccio put del correlation id per response
							//log.debug("TEST SOAPLOGGINGHANDLER Request Ko: Inserita request su mappa e INBOUND_ID in MessageContext. Nuova dimensione mappa: "+this.mapSoapMsg.size());
						}
					} else {
						log.debug("Logging for request disabled by ws client configuration");
					}
				} else { //Response
					if (this.printResponse) {
						if (log.isDebugEnabled()){
							print(mc, logMessage, outboundProperty);
						} else if (this.mapSoapMsg != null){
							SOAPMessage response = ((SOAPMessageContext) mc).getMessage(); //Recupero Response
							String correlationID = (String)mc.get("INBOUND_ID"); // Recupero correlationId da MessageContext
							//log.debug("TEST SOAPLOGGINGHANDLER Response Ko: CorrelationID recuperata da MessageContext: ["+correlationID+"]");
							SOAPMessage request = correlationID!=null ? this.mapSoapMsg.get(correlationID) : null; // Recupero request dalla mappa
							//log.debug("TEST SOAPLOGGINGHANDLER Response Ko: Stampo request e response");
							WSClientUtility.printSoapMessage(request, response, service, operation, this.anonymizePatternsList, this.anonymizeReplacementsList ); // Stampo request e response
							this.mapSoapMsg.remove(correlationID); //Pulisco mappa
							//log.debug("TEST SOAPLOGGINGHANDLER Response Ko: Elemento rimosso da Mappa. Nuova dimensione mappa: "+this.mapSoapMsg.size());
						}
					} else {
						log.debug("Logging for response disabled by ws client configuration");
					}
				}

			} catch (Exception e) {	
				if (mc.get("INBOUND_ID") != null){
					this.mapSoapMsg.remove((String)mc.get("INBOUND_ID"));
				}
				log.error("Error getting output soap message", e);		
			}
		}
	}		
	
	private void print(MessageContext mc, StringBuffer logMessage, Boolean outboundProperty) throws SOAPException, IOException {
		
		logMessage.append((outboundProperty.booleanValue()) ? "Request message. " : "Response message. ");
		if (anonymizePatternsList == null)
			logMessage.append(WSClientUtility.deserializeSoapMessage(((SOAPMessageContext) mc).getMessage()));
		else
			logMessage.append(WSClientUtility.deserializeSoapMessage(((SOAPMessageContext) mc).getMessage(),
																	anonymizePatternsList, anonymizeReplacementsList));
		log.debug(logMessage.toString());
		
	}
	
	private boolean isMapToBeCleaned() {
		long currentTime = System.currentTimeMillis();
		if (currentTime - lastCleanMap > cleanInterval) {
			log.debug("SOAPLoggingHandler - SoapMessage Map to be cleaned");
			return true;
		}
		return false;
	}
	
	private void cleanMap(){
		for(Iterator<Map.Entry<String, SOAPMessage>> it = mapSoapMsg.entrySet().iterator(); it.hasNext(); ) {
			Map.Entry<String, SOAPMessage> entry = it.next();
			if(isMessageToBeCleaned(entry.getKey())) {
				log.debug("TEST SOAPLoggingHandler Removing object from Map");
				it.remove();
			}
		}
		lastCleanMap = System.currentTimeMillis();
	}
	
	private boolean isMessageToBeCleaned(String key) {
		if (key!=null){
			String timeStampMessage = key.split("_")[0];
			//log.debug("TEST SOAPLoggingHandler. timeStampMessage: ["+timeStampMessage+"]");
			long currentTime = System.currentTimeMillis();
			if (currentTime - Long.parseLong(timeStampMessage) > timeObsoleteMessage) {
				return true;
			}
		}
		return false;
	}
}
