package com.alitalia.aem.ws.common.messagehandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This class traverse a DOM and, for nodes corresponding to leaf elements,
 * perform a consistency check on the namespace assigned by the parser.
 * 
 * <p>This is required because the internally used parser seems to
 * perform incorrectly when assigning the namespace of an element,
 * in case a default namespace reset is declared on the element
 * itself (through <code>xmls=""</code>).</p>
 */
public class RemoveInboundUnusedUnwantedNamespaces {
	
	private static Logger log = LoggerFactory.getLogger(SOAPMessageHandler.class);

	private interface ElementVisitor {
		void visit(Element element);
	}

	public void process(Document document) {
		Element element = document.getDocumentElement();
		traverse(element, new ElementVisitor() {

			@Override
			public void visit(Element element) {
				// in the described conditions, it seems that the xmlns="" in the DOM  
				// is available as a plain attribute, but not actually applied to the 
				// element namespace URI itself, so we check for this difference
				boolean shouldBeUnqualified = element.hasAttribute("xmlns") && "".equals(element.getAttribute("xmlns")) && element.getPrefix() == null;
				boolean wasParsedAsQualified = element.getNamespaceURI() != null && !"".equals(element.getNamespaceURI());
				if (wasParsedAsQualified && shouldBeUnqualified) {
					if (element.getOwnerDocument() == null) {
						log.warn("Detected possibily incorrect namespace for element {} but ignored: owner document is null.", element.getLocalName());
					} else {
						log.trace("Detected possibily incorrect namespace for element {}: forcing namespace to unqualified.", element.getLocalName());
						Node newNode = null;
						log.debug("Element Name: "+element.getLocalName());
						if ("NewDataSet".equals(element.getLocalName())){
							newNode = element.getOwnerDocument().renameNode(element, element.getParentNode().getParentNode().getNamespaceURI(), element.getLocalName());
							log.debug("Modifica namespace NewDataSet");
						} else if ("Table".equals(element.getLocalName())){
							newNode = element.getOwnerDocument().renameNode(element, element.getParentNode().getNamespaceURI(), element.getLocalName());
							log.debug("Modifica namespace Table");
						} else {
							newNode = element.getOwnerDocument().renameNode(element, "", element.getLocalName());
						}
						if (newNode != element) {
							log.warn("The namespace change for element {} caused a node replacement.", element.getLocalName());
						}
					}
				}
			}
		});

	}
	
	private final void traverse(Element element, ElementVisitor visitor) {
		visitor.visit(element);
		NodeList children = element.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			Node node = children.item(i);
			if (node.getNodeType() != Node.ELEMENT_NODE)
				continue;
			traverse((Element) node, visitor);
		}
	}

}