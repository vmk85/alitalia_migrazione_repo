package com.alitalia.aem.ws.common.utils.exc;

public class WSClientUtilityException extends RuntimeException {

	public WSClientUtilityException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public WSClientUtilityException(String arg0, Throwable arg1, boolean arg2,
			boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

	public WSClientUtilityException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public WSClientUtilityException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public WSClientUtilityException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

}
