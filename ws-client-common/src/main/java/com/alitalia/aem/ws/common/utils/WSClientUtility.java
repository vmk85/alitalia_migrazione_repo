package com.alitalia.aem.ws.common.utils;

import java.io.ByteArrayOutputStream;
import java.io.StringWriter;
import java.util.Hashtable;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.soap.SOAPMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.common.utils.exc.WSClientUtilityException;

public class WSClientUtility {

	private static Logger logger = LoggerFactory.getLogger(WSClientUtility.class);

	public static StringBuffer deserializeSoapMessage(SOAPMessage soapMessage) {

		try {
			StringBuffer logMsgResponse = new StringBuffer();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			soapMessage.writeTo(out);
			String unindentedSoapMessageString = new String(out.toByteArray());
			logMsgResponse.append(unindentedSoapMessageString);
			return logMsgResponse;
		} catch (Exception e){
			throw new WSClientUtilityException("Exception while deserializing SOAP Message: "+ soapMessage, e);
		}

	}

	public static StringBuffer deserializeSoapMessage(SOAPMessage soapMessage, List<String> patternsList, List<String> replacementsList) {

		try {
			StringBuffer logMsgResponse = new StringBuffer();
			String logMsgResponseToAnonymize = deserializeSoapMessage(soapMessage).toString();
			logMsgResponse.append(anonymizeString(logMsgResponseToAnonymize, patternsList, replacementsList));
			return logMsgResponse;
		} catch (Exception e){
			throw new WSClientUtilityException("Exception while deserializing SOAP Message: "+ soapMessage, e);
		}

	}

	private static String anonymizeString(String stringToBeAnonymized, List<String> patternsList, List<String> replacementsList) {
		String anonymizedString = stringToBeAnonymized;
		if (patternsList == null || (patternsList.size() != replacementsList.size())) {
			logger.warn("WSClientUtility.anonymizeString: patternsList empty or patternsList/replacementsList size mismatch, cannot anonymize! Input has been returned as is");
			return anonymizedString;
		}

		for (int i = 0; i < patternsList.size(); i++) {
			String workingString = anonymizedString;
			anonymizedString = workingString.replaceAll(patternsList.get(i), replacementsList.get(i));
		}

		return anonymizedString;
	}

	public static void printSoapMessage(Hashtable<String, SOAPMessage> mapSoapMsg) {
		if (!logger.isDebugEnabled()) {
			printSoapMessage(mapSoapMsg, null, null);
		}
	}

	public static void printSoapMessage(SOAPMessage request, SOAPMessage response) {
		if (!logger.isDebugEnabled()) {
			printSoapMessage(request, response, null, null);
		}
	}

	public static void printSoapMessage(Hashtable<String, SOAPMessage> mapSoapMsg, List<String> patternsList, List<String> replacementsList) {
		if (!logger.isDebugEnabled()) {
			if (patternsList == null) {
				String soapRequest = (mapSoapMsg.get(WSClientConstants.REQUEST) != null ? WSClientUtility
						.deserializeSoapMessage(mapSoapMsg.get(WSClientConstants.REQUEST)).toString() : "UNDEFINED");
				logger.error("Request sent:[" + soapRequest + "]");
				String soapResponse = (mapSoapMsg.get(WSClientConstants.RESPONSE) != null ? WSClientUtility
						.deserializeSoapMessage(mapSoapMsg.get(WSClientConstants.RESPONSE)).toString() : "UNDEFINED");
				logger.error("Response received:[" + soapResponse + "]");
			}
			else
			{
				String soapRequest = (mapSoapMsg.get(WSClientConstants.REQUEST) != null ? WSClientUtility
						.deserializeSoapMessage(mapSoapMsg.get(WSClientConstants.REQUEST), patternsList, replacementsList).toString() : "UNDEFINED");
				logger.error("Request sent:[" + soapRequest + "]");
				String soapResponse = (mapSoapMsg.get(WSClientConstants.RESPONSE) != null ? WSClientUtility
						.deserializeSoapMessage(mapSoapMsg.get(WSClientConstants.RESPONSE), patternsList, replacementsList).toString() : "UNDEFINED");
				logger.error("Response received:[" + soapResponse + "]");
			}
		}
	}

	public static void printSoapMessage(SOAPMessage request, SOAPMessage response, List<String> patternsList, List<String> replacementsList) {
		if (!logger.isDebugEnabled()) {
			if (patternsList == null) {
				String soapRequest = (request != null ? WSClientUtility.deserializeSoapMessage(request).toString() : "UNDEFINED");
				logger.error("Request sent:[" + soapRequest + "]");
				String soapResponse = (response != null ? WSClientUtility.deserializeSoapMessage(response).toString() : "UNDEFINED");
				logger.error("Response received:[" + soapResponse + "]");
			}
			else
			{
				String soapRequest = (request != null ? WSClientUtility.deserializeSoapMessage(request, patternsList, replacementsList).toString() : "UNDEFINED");
				logger.error("Request sent:[" + soapRequest + "]");
				String soapResponse = (response != null ? WSClientUtility.deserializeSoapMessage(response, patternsList, replacementsList).toString() : "UNDEFINED");
				logger.error("Response received:[" + soapResponse + "]");
			}
		}
	}

	public static void printSoapMessage(SOAPMessage request, SOAPMessage response, String service, String operation, List<String> patternsList, List<String> replacementsList) {
		if (!logger.isDebugEnabled()) {
			if (patternsList == null) {
				String soapRequest = (request != null ? WSClientUtility.deserializeSoapMessage(request).toString() : "UNDEFINED");
				logger.error("Service:["+service+"] Operation:["+operation+"] Request sent:[" + soapRequest + "]");
				String soapResponse = (response != null ? WSClientUtility.deserializeSoapMessage(response).toString() : "UNDEFINED");
				logger.error("Service:["+service+"] Operation:["+operation+"] Response received:[" + soapResponse + "]");
			}
			else
			{
				String soapRequest = (request != null ? WSClientUtility.deserializeSoapMessage(request, patternsList, replacementsList).toString() : "UNDEFINED");
				logger.error("Service:["+service+"] Operation:["+operation+"] Request sent:[" + soapRequest + "]");
				String soapResponse = (response != null ? WSClientUtility.deserializeSoapMessage(response, patternsList, replacementsList).toString() : "UNDEFINED");
				logger.error("Service:["+service+"] Operation:["+operation+"] Response received:[" + soapResponse + "]");
			}
		}
	}

	public static void printXMLMessage(Object message, String serviceName, boolean isRequest) {
		if (!logger.isDebugEnabled()) {
			try {
				JAXBContext context = JAXBContext.newInstance(message.getClass());
				Marshaller marshaller = context.createMarshaller();
				StringWriter writer = new StringWriter();
				marshaller.marshal(message, writer);
				logger.error(serviceName + " execution failed. "+ (isRequest ? "XML Request: [" : "XML Response: [") + writer.getBuffer().toString() + "]");
			} catch (Exception e) {
				logger.error("Exception while marshal "+message.getClass(), e);
			}
		}
	}

}