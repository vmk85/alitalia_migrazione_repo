package com.alitalia.aem.ws.common.exception;

public class WSClientException extends RuntimeException {

	private static final long serialVersionUID = -4975678971503293519L;

	public WSClientException() {
		super();
	}

	public WSClientException(String message, Throwable cause) {
		super(message, cause);
	}

	public WSClientException(String message) {
		super(message);
	}

	public WSClientException(Throwable cause) {
		super(cause);
	}

}
