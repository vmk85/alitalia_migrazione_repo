package com.alitalia.aem.ws.common.utils;

public interface WSClientConstants {

	public static final Boolean httpsModeDefault = false;
	public static final Boolean loadIdentityStoreDefault = false;
	public static final Boolean hostnameVerifierDefault = false;
	
	public static final Integer connectionTimeoutDefault = 0;
	public static final Integer requestTimeoutDefault = 0;
	
	public static final boolean printRequest = true;
	public static final boolean printResponse = true;
	
	public static final String sslProtocolDefault = "TLSv1";
	public static final String keystoreTypeDefault = "jks";
	public static final String keymanagerFactoryAlgorithmDefault = "SunX509";
	
	
	public static final String BINDING_PROVIDER_CONNECT_TIMEOUT_PROPERTY = "com.sun.xml.internal.ws.connect.timeout";
	public static final String BINDING_PROVIDER_REQUEST_TIMEOUT_PROPERTY = "com.sun.xml.internal.ws.request.timeout";
	
	public static final String JAXWS_SSL_SOCKET_FACTORY_PROPERTY = "com.sun.xml.internal.ws.transport.https.client.SSLSocketFactory";
	public static final String JAXWS_HOSTNAME_VERIFIER_PROPERTY = "com.sun.xml.internal.ws.transport.https.client.hostname.verifier";
	
	public static final String REQUEST = "REQUEST";
	public static final String RESPONSE = "RESPONSE";
	
}
