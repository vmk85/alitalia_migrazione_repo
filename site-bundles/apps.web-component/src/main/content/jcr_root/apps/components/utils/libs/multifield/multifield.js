(function () {
    var DATA_EAEM_NESTED = "data-eaem-multifield",
    DATA_EAEM_NESTEDMULTIFIELD = "data-eaem-multifield-fieldset",
    	CFFW = ".coral-Form-fieldwrapper",
        THUMBNAIL_IMG_CLASS = "cq-FileUpload-thumbnail-img",
        SEP_SUFFIX = "-",
        SEL_FILE_UPLOAD = ".coral-FileUpload",
        SEL_FILE_REFERENCE = ".cq-FileUpload-filereference",
        SEL_FILE_NAME = ".cq-FileUpload-filename",
        SEL_FILE_MOVEFROM = ".cq-FileUpload-filemovefrom";

    function getStringBeforeAtSign(str){
        if(_.isEmpty(str)){
            return str;
        }

        if(str.indexOf("@") != -1){
            str = str.substring(0, str.indexOf("@"));
        }

        return str;
    }

    function getStringAfterAtSign(str){
        if(_.isEmpty(str)){
            return str;
        }

        return (str.indexOf("@") != -1) ? str.substring(str.indexOf("@")) : "";
    }

    function getStringAfterLastSlash(str){
        if(!str || (str.indexOf("/") == -1)){
            return "";
        }

        return str.substr(str.lastIndexOf("/") + 1);
    }

    function getStringBeforeLastSlash(str){
        if(!str || (str.indexOf("/") == -1)){
            return "";
        }

        return str.substr(0, str.lastIndexOf("/"));
    }

    function removeFirstDot(str){
        if(str.indexOf(".") != 0){
            return str;
        }

        return str.substr(1);
    }

    function modifyJcrContent(url){
        return url.replace(new RegExp("^" + Granite.HTTP.getContextPath()), "")
                .replace("_jcr_content", "jcr:content");
    }

    /**
     * Removes multifield number suffix and returns just the fileRefName
     * Input: paintingRef-1, Output: paintingRef
     *
     * @param fileRefName
     * @returns {*}
     */
    function getJustName(fileRefName){
        if(!fileRefName || (fileRefName.indexOf(SEP_SUFFIX) == -1)){
            return fileRefName;
        }

        var value = fileRefName.substring(0, fileRefName.lastIndexOf(SEP_SUFFIX));

		return value;

        /*if(fileRefName.lastIndexOf(SEP_SUFFIX) + SEP_SUFFIX.length + 1 == fileRefName.length){
            return value;
        }
        return value + fileRefName.substring(fileRefName.lastIndexOf(SEP_SUFFIX) + SEP_SUFFIX.length + 1);*/
    }

    function getMultiFieldNames($multifields){
        var mNames = {}, mName;

        $multifields.each(function (i, multifield) {
            mName = $(multifield).children("[name$='@Delete']").attr("name");
            mName = mName.substring(0, mName.indexOf("@"));
            mName = mName.substring(2);
            mNames[mName] = $(multifield);
        });

        return mNames;
    }

	function fillDialogField($field, value) {
		if (_.isEmpty($field)) {
	        return;
	    }

	    if ($field.hasClass("coral-ColorPicker")) {
	    	// se dovrà essere valorizzato un colorpicker
	    	var cpButton = $field.children("button"),
	    		cpInput = $field.children("input");
	    	cpButton.attr("style", "background-color: " + value + "");
	    	cpInput.attr("value", value);
	    } else if ($field.attr("type") == "checkbox") {
	    	// se dovrà essere valorizzata una checkbox
	    	if (value == "true") {
	    		$field.attr("checked", true);
	    	} else {
	    		$field.attr("checked", false);
	    	}
        } else if($field.prop("tagName")=="SELECT") {
					$field.val(value);
            		$field.prev().find('span').text($field.children('[value='+value+']').text());
        }else {

	    	$field.val(value);
	    }
	} 

   function buildMultiField(data, $multifield, mName){
        if(_.isEmpty(mName) || _.isEmpty(data)){
            return;
        }
 
        _.each(data, function(value, key){
            if(key == "jcr:primaryType"){
                return;
            }
 
			$multifield.find(".js-coral-Multifield-add").last().click();
 
            _.each(value, function(fValue, fKey){
                if(fKey == "jcr:primaryType" || _.isObject(fValue)){
                    return;
                }
 
				var $field, $nestedField,
				colorRegex = /^rgba[(]([0-9]{1,3},){3}[0-1][.]?[0-9]{0,2}[)]/g,
				nestedMultifieldRegex = /^\[(\{("[A-z0-9\-\.]*"\:".*"\,?)*\}\,?)*\]/g;                    
				
				if (colorRegex.test(fValue)) {
				// Controllo se il valore è un colore, in tal caso si dovrà prendere l'elemento span del colorpicker con data-name e non con name
				$field = $multifield.find("[data-name='./" + fKey + "']").last();
				
				fillDialogField($field, fValue);
				
				} else if (nestedMultifieldRegex.test(fValue)) {
				//Controllo se il valore è un multifield annidato
				
				var nestedMultifieldData = JSON.parse(fValue);
				$field = $multifield.find(".coral-Multifield")
				
				_.each(nestedMultifieldData, function(nValue, nKey) {
					
					$field.find(".js-coral-Multifield-add").last().click();
					
					_.each(nValue, function(nfValue, nfKey) {
						
						if (colorRegex.test(nfValue)) {
							// Controllo se il valore è un colore, in tal caso si dovrà prendere l'elemento span del colorpicker con data-name e non con name
							$nestedField = $field.find("[data-name='./" + nfKey + "']").last();
						} else {
							$nestedField = $field.find("[name='./" + nfKey + "']").last();
						}
						
						fillDialogField($nestedField, nfValue);
					});
				});
				
				} else {
					$field = $multifield.find("[name='./" + fKey + "']").last();
					fillDialogField($field, fValue);
				}

            });
        });
    }
 
    function buildImageField($multifield, mName){
        $multifield.find(".coral-FileUpload:last").each(function () {
            var $element = $(this), widget = $element.data("fileUpload"),
                resourceURL = $element.parents("form.cq-dialog").attr("action"),
                counter = $multifield.find(SEL_FILE_UPLOAD).length;
 
            if (!widget) {
                return;
            }
 
            var fuf = new Granite.FileUploadField(widget, resourceURL);
 
            addThumbnail(fuf, mName, counter);
        });
    }
 
    function addThumbnail(imageField, mName, counter){
        var $element = imageField.widget.$element,
            $thumbnail = $element.find("." + THUMBNAIL_IMG_CLASS),
            thumbnailDom;
 
        $thumbnail.empty();
 
        $.ajax({
            url: imageField.resourceURL + ".2.json",
            cache: false
        }).done(handler);
 
        function handler(data){
            var fName = getJustName(getStringAfterLastSlash(imageField.fieldNames.fileName)),
                fRef = getJustName(getStringAfterLastSlash(imageField.fieldNames.fileReference));
 
            if(isFileNotFilled(data, counter, fRef)){
                return;
            }
 
            var fileName = data[mName][counter][fName],
                fileRef = data[mName][counter][fRef];
 
            if (fileRef) {
                if (imageField._isImageMimeType(fileRef)) {
                    thumbnailDom = imageField._createImageThumbnailDom(fileRef);
                } else {
                    thumbnailDom = $("<p>" + fileRef + "</p>");
                }
            }
 
            if (!thumbnailDom) {
                return;
            }
 
            $element.addClass("is-filled");
            $thumbnail.append(thumbnailDom);
 
            var $fileName = $element.find("[name=\"" + imageField.fieldNames.fileName + "\"]"),
                $fileRef = $element.find("[name=\"" + imageField.fieldNames.fileReference + "\"]");
 
            $fileRef.val(fileRef);
            $fileName.val(fileName);
        }
 
        function isFileNotFilled(data, counter, fRef){
            return _.isEmpty(data[mName])
                    || _.isEmpty(data[mName][counter])
                    || _.isEmpty(data[mName][counter][fRef])
        }
    }
 
    //reads multifield data from server, creates the nested composite multifields and fills them
    function addDataInFields() {
        $(document).on("dialog-ready", function() {
            var $multifields = $("[" + DATA_EAEM_NESTED + "]");
 
            if(_.isEmpty($multifields)){
                return;
            }
 
            var mNames = getMultiFieldNames($multifields),
                $form = $(".cq-dialog"),
                actionUrl = $form.attr("action") + ".infinity.json";
 
            $.ajax(actionUrl).done(postProcess);
 
            function postProcess(data){
                _.each(mNames, function($multifield, mName){
                    $multifield.on("click", ".js-coral-Multifield-add", function () {
                        buildImageField($multifield, mName);
                    });
 
                    buildMultiField(data[mName], $multifield, mName);
                });
            }
        });
    }
 
    function collectImageFields($form, $fieldSet, counter){
        var $fields = $fieldSet.children().children(CFFW).not(function(index, ele){
            return $(ele).find(SEL_FILE_UPLOAD).length == 0;
        });
 
        $fields.each(function (j, field) {
            var $field = $(field),
                $widget = $field.find(SEL_FILE_UPLOAD).data("fileUpload");
 
            if(!$widget){
                return;
            }
 
            var prefix = $fieldSet.data("name") + "/" + (counter + 1) + "/",
 
                $fileRef = $widget.$element.find(SEL_FILE_REFERENCE),
                refPath = prefix + getJustName($fileRef.attr("name")),
 
                $fileName = $widget.$element.find(SEL_FILE_NAME),
                namePath = prefix + getJustName($fileName.attr("name")),
 
                $fileMoveRef = $widget.$element.find(SEL_FILE_MOVEFROM),
                moveSuffix =   $widget.inputElement.attr("name") + "/" + new Date().getTime()
                                        + SEP_SUFFIX + $fileName.val(),
                moveFromPath =  moveSuffix + "@MoveFrom";
 
            $('<input />').attr('type', 'hidden').attr('name', refPath)
                .attr('value', $fileRef.val() || ($form.attr("action") + removeFirstDot(moveSuffix)))
                .appendTo($form);
 
            $('<input />').attr('type', 'hidden').attr('name', namePath)
                .attr('value', $fileName.val()).appendTo($form);
 
            $('<input />').attr('type', 'hidden').attr('name', moveFromPath)
                .attr('value', modifyJcrContent($fileMoveRef.val())).appendTo($form);
 
            $field.remove();
        });
    }
 
    function collectNonImageFields($form, $fieldSet, counter){
        var $fields = $fieldSet.children().children(CFFW).not(function(index, ele){
            return $(ele).find(SEL_FILE_UPLOAD).length > 0;
        });
 
        $fields.each(function (j, field) {
            fillValue($form, $fieldSet.data("name"), $(field), (counter + 1));
        });
    }

	function getFieldName($field, attrNameOfName) {
		var name = $field.attr(attrNameOfName);
	
	    if (!name || name.indexOf("@") > -1) {
	        return;
	    }
	
	    // strip ./
	    if (name.indexOf("./") == 0) {
	        name = name.substring(2);
	    }
	    
	    return name;
	}
	
	function getFieldValue($field) {
		var value;
		
		if ($field.attr("type") == "checkbox") {
			if ($field.is(":checked")) {
				value = "true";
			} else {
				value = "false";
			}
		} else {
			value = $field.val();
		}
		
		// remove the field, so that individual values are not POSTed
		$field.remove();
		
		return value;
	}
	
	//for getting the nested multifield data as js objects
	function getRecordFromMultiField($multifield) {
    	var $fieldSets = $multifield.find("[class='coral-Form-fieldset']");

        var records = [], record, $fields, name;

        $fieldSets.each(function (i, fieldSet) {
            $fields = $(fieldSet).find("[name]");

            record = {};

            $fields.each(function (j, field) {
            	var name = getFieldName($(field), "name");
            	if (name != undefined) { 
            		record[name] = getFieldValue($(field));
            	}
            });

            if(!$.isEmptyObject(record)){
                records.push(record)
            }
        });

        return records;
    }

    function fillValue($form, fieldSetName, $field, counter){
        var name,
			fieldValue;

		var $nestedMultifield = $field.find("[data-init='multifield']");
		if ($nestedMultifield && $nestedMultifield.length > 0) {
			//if NestedMultifield
			$field = $nestedMultifield;
			name = getFieldName($field.find("[class='coral-Form-fieldset']").last(), "data-name");
			fieldValue = JSON.stringify(getRecordFromMultiField($field));
		} else {
			//if NOT NestedMultifield
			$field = $field.find("[name]");
			name = getFieldName($field, "name");
			fieldValue = getFieldValue($field);
		}

        $('<input />').attr('type', 'hidden')
            .attr('name', fieldSetName + "/" + counter + "/" + name)
            .attr('value', fieldValue)
            .appendTo($form);
    }
 
    //collect data from widgets in multifield and POST them to CRX
    function collectDataFromFields(){
        $(document).on("click", ".cq-dialog-submit", function () {
            var $multifields = $("[" + DATA_EAEM_NESTED + "]");
 
            if(_.isEmpty($multifields)){
                return;
            }
 
            var $form = $(this).closest("form.foundation-form"),
                $fieldSets;
 
            $multifields.each(function(i, multifield){
                $fieldSets = $(multifield).find("[" + DATA_EAEM_NESTEDMULTIFIELD + "]");
 
                $fieldSets.each(function (counter, fieldSet) {
                    collectNonImageFields($form, $(fieldSet), counter);
 					
                    collectImageFields($form, $(fieldSet), counter);
                });
            });
        });
    }
 
    function overrideGranite_computeFieldNames(){
        var prototype = Granite.FileUploadField.prototype,
            ootbFunc = prototype._computeFieldNames;
 
        prototype._computeFieldNames = function(){
            ootbFunc.call(this);
 
            var $imageMulti = this.widget.$element.closest("[" + DATA_EAEM_NESTED + "]");
 
            if(_.isEmpty($imageMulti)){
                return;
            }
 
            var fieldNames = {},
                fileFieldName = $imageMulti.find("input[type=file]").attr("name"),
                $fieldSet = $imageMulti.find(SEL_FILE_UPLOAD).closest("[class='coral-Form-fieldset']"),
                counter = $imageMulti.find(SEL_FILE_UPLOAD).length;
 
            _.each(this.fieldNames, function(value, key){
                if(value.indexOf("./jcr:") == 0){
                    fieldNames[key] = value;
                }else if(key == "tempFileName" || key == "tempFileDelete"){
                    value = value.substring(0, value.indexOf(".sftmp")) + getStringAfterAtSign(value);
                    fieldNames[key] = fileFieldName + removeFirstDot(getStringBeforeAtSign(value))
                                        + SEP_SUFFIX + counter + ".sftmp" + getStringAfterAtSign(value);
                }else{
                    fieldNames[key] = getStringBeforeAtSign(value) + SEP_SUFFIX
                                                    + counter + getStringAfterAtSign(value);
                }
            });
 
            this.fieldNames = fieldNames;
 
            this._tempFilePath = getStringBeforeLastSlash(this._tempFilePath);
            this._tempFilePath = getStringBeforeLastSlash(this._tempFilePath) + removeFirstDot(fieldNames.tempFileName);
        }
    }
 
    $(document).ready(function () {
        addDataInFields();
        collectDataFromFields();
    });

    overrideGranite_computeFieldNames();
})();