<%@page session="false"%><%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Tree Activation

  Implements the tree activation component.

--%><%@ page contentType="text/html"
             pageEncoding="utf-8"
             import="com.day.cq.wcm.foundation.Image,
    com.day.cq.wcm.api.components.DropTarget,
    com.day.cq.wcm.api.components.EditConfig,
    com.day.cq.wcm.commons.WCMUtils,
    com.day.cq.replication.Replicator,
    com.day.cq.replication.Agent,
    com.day.cq.replication.AgentConfig,
    com.day.cq.widget.HtmlLibraryManager,
    com.day.cq.wcm.api.WCMMode,
    com.day.cq.wcm.api.components.Toolbar,
    com.day.cq.replication.ReplicationQueue,
    com.day.cq.i18n.I18n,
    com.day.cq.replication.AgentManager,
    java.util.Iterator" %><%
%><%@include file="/libs/foundation/global.jsp"%><%
    AgentManager agentMgr = sling.getService(AgentManager.class);
    I18n i18n = new I18n(slingRequest);

%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html>
<head>
    <title><%= i18n.get("AEM Replication") %> | <%= i18n.get("Activate Tree") %></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="/libs/cq/ui/resources/cq-ui.js" type="text/javascript"></script>
    <%
    HtmlLibraryManager htmlMgr = sling.getService(HtmlLibraryManager.class);
    if (htmlMgr != null) {
        htmlMgr.writeIncludes(slingRequest, out, "cq.widgets");
    }
    %>
    <style type="text/css">
        #treeProgress {
            display: block;
            background-color: white;
            width:100%;
            min-height:400px;
            height:100%;
            border: 1px solid #888888;
            overflow: scroll;
            overflow-x: auto;
        }
    </style>
</head>
<body>
<h1><%= i18n.get("Activate Tree") %></h1>
<form target="treeProgress" action="<%= resource.getPath() %>.html" method="POST" id="treeProgress_form">
    <input type="hidden" name="_charset_" value="UTF-8">
    <input type="hidden" id="path" name="path" value="/apps/alitalia/i18n_booking_award">
    <input type="hidden" id="onlymodified" name="onlymodified" value="false">
    <input type="hidden" id="reactivate" name="reactivate" value="false">
    <input type="hidden" id="ignoredeactivated" name="ignoredeactivated" value="false">
    <table class="form">
        <tr>
            <td><label for="fakePathField"><%= i18n.get("Start Path") %>:</label></td>
            <td><div id="fakePath">&nbsp;</div><br>
                <small><%= i18n.get("Select location to activate") %></small>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <input type="hidden" name="cmd" value="dryrun" id="cmd">
                <input type="button" value="<%= i18n.get("Dry Run") %>" onclick="document.getElementById('cmd').value='dryrun'; document.getElementById('treeProgress_form').submit();">
                <input type="button" value="<%= i18n.get("Activate") %>" onclick="document.getElementById('cmd').value='activate'; document.getElementById('treeProgress_form').submit();">
            </td>
        </tr>
    </table>
</form><br>
    <iframe name="treeProgress" id="treeProgress">
    </iframe>
        <script>
            //use a SlingTreeLoader to replace the default tree loader (that uses ExtTreeServlet to render and doesn't work)
            var slingTreeLoader = new CQ.tree.SlingTreeLoader({
                baseAttrs: {
                    singleClickExpand: true
                }
            });

            // provide a path selector field with a repository browse dialog
            CQ.Ext.onReady(function() {
                var w = new CQ.form.PathField({
                   // applyTo: "path",
                    renderTo: "CQ",
                    //                    "content": "/apps/alitalia/i18n_booking_award",
                    rootPath: "/apps/alitalia/i18n_booking_award",
                    predicate: "hierarchy",
                    hideTrigger: false,
                    showTitlesInTree: false,
                    name: "fakePathField",
                    value: "/apps/alitalia/i18n_booking_award",
                    width: 400,
                    treeLoader: slingTreeLoader,
                    listeners: {
                        render: function() {
                            this.wrap.anchorTo("fakePath", "tl");
                        },
                        change: function (fld, newValue, oldValue) {
                            document.getElementById("path").value = newValue;
                        },
                        dialogselect: function(fld, newValue) {
                            document.getElementById("path").value = newValue;
                        }
                    }
                });
            });
        </script>
</body>
</html>
