package apps.sitealitaliapoc.components.content.listairports;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.poc.services.api.AirportsService;
import com.alitalia.aem.poc.services.api.dto.AirportDTO;

public class Controller extends WCMUse {

	@Override
	public void activate() {
    }

	public AirportDTO[] getListAirports() {
		AirportsService service = getSlingScriptHelper().getService(AirportsService.class);
		return service.getListAirports();
	}

}