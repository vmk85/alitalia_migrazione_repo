package com.alitalia.aem.poc.services.api.dto;

public class AirportDTO {
	
	private String airportCode;
	
	private String airportDescription;

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getAirportDescription() {
		return airportDescription;
	}

	public void setAirportDescription(String airportDescription) {
		this.airportDescription = airportDescription;
	}
	
}
