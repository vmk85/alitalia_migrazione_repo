package com.alitalia.aem.poc.services.api;

import com.alitalia.aem.poc.services.api.dto.AirportDTO;

public interface AirportsService {
	
	AirportDTO[] getListAirports();
	
}
