package com.alitalia.aem.poc.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.poc.services.api.AirportsService;
import com.alitalia.aem.poc.services.api.dto.AirportDTO;
import com.alitalia.aem.ws.booking.staticdataservice.StaticDataServiceClient;
import com.alitalia.aem.ws.booking.staticdataservice.wsdl.IStaticDataServiceGetDataServiceFaultFaultFaultMessage;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.StaticDataRequest;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.StaticDataResponse;
import com.alitalia.aem.ws.booking.staticdataservice.xsd2.StaticDataType;
import com.alitalia.aem.ws.booking.staticdataservice.xsd5.Airport;

/**
 * Service to obtain information about available airports.
 */
@Component
@Service(value=AirportsService.class)
@Properties({
	@Property(name="service.vendor", value="Reply")
})
public class AirportsServiceImpl implements AirportsService {
	
	/**
	 * Returns the list of available airports.
	 */
	@Override
	public AirportDTO[] getListAirports() {
		try {
			logger.debug("getListAirports requested.");
			// perform web service invocation
			ObjectFactory factory = new ObjectFactory();
			StaticDataServiceClient serviceClient = getServiceClient();
			StaticDataRequest serviceRequest = new StaticDataRequest();
			serviceRequest.setType(StaticDataType.AIRPORT);
			serviceRequest.setMarket(factory.createStaticDataRequestMarket("it_it"));
			serviceRequest.setLanguageCode(factory.createStaticDataRequestLanguageCode("it"));
			StaticDataResponse serviceResponse = serviceClient.getData(serviceRequest);
			// translate results to DTOs
			List<Object> entitiesList = serviceResponse.getEntities().getValue().getAnyType();
			List<AirportDTO> resultList = new ArrayList<AirportDTO>();
			for (Object entity : entitiesList) {
				if (entity instanceof Airport) {
					Airport source = (Airport) entity;
					AirportDTO target = new AirportDTO();
					target.setAirportCode(source.getCode().getValue());
					target.setAirportDescription(source.getName().getValue());
					resultList.add(target);
				}
			}
			return resultList.toArray(new AirportDTO[resultList.size()]);
		} catch (IStaticDataServiceGetDataServiceFaultFaultFaultMessage faultMessage) {
			logger.error("Web service invocation error", faultMessage);
			throw new RuntimeException(faultMessage);
		}
	}
	
	
	/* BOILERPLATE CODE */
	
	/**
	 * Logger.
	 */
    private static Logger logger = LoggerFactory.getLogger(AirportsServiceImpl.class);
	
	/**
	 * URL of the WSDL to use to invoke the external service.
	 */
	@Property(description="URL of the WSDL to use to invoke the external service.")
	private static final String SERVICE_ENDPOINT_PROPERTY = "serviceEndpoint";
	
	/**
	 * The component context provided by OSGi runtime.
	 */
	private ComponentContext componentContext = null;
	
	/**
	 * A reference to the current instance of the web service client,
	 * for performance reasons.
	 */
	private StaticDataServiceClient wsClient = null;
	
	/**
	 * Keep a reference to the WSDL URL of the current wsClient instance,
	 * to check and re-create it when the URL property is modified.
	 */
	private String wsClientUrl = null;
	
	/**
	 * Utility method to obtain an instance of the web service client
	 * with the currently set WSDL URL.
	 */
	private synchronized StaticDataServiceClient getServiceClient() {
		String serviceEndpoint = (String) componentContext.getProperties().get(SERVICE_ENDPOINT_PROPERTY);
		return new StaticDataServiceClient(serviceEndpoint);
		/*
		if (wsClient != null && !wsClientUrl.equals(serviceEndpoint)) {
			wsClient = null;
			wsClientUrl = null;
		}
		if (wsClient == null && serviceEndpoint != null && !"".equals(serviceEndpoint)) {
			logger.info("Web Service endpoint URL changed, creating new client at {0}.", serviceEndpoint); 
			wsClient = new StaticDataServiceClient(serviceEndpoint);
			wsClientUrl = serviceEndpoint;
		}
		return wsClient;
		*/
	}
	
	/**
	 * OSGi Declarative Service activate method.
	 */
	@Activate
	protected synchronized void activate(ComponentContext componentContext) {
		logger.info("Service activated.");
		this.componentContext = componentContext;
		if (wsClient != null) {
			// force re-creation of cached web service instance on next call
			wsClient = null;
			wsClientUrl = null;
		}
	}
	
	/**
	 * OSGi Declarative Service modified method.
	 */
	@Modified
	protected synchronized void modified(ComponentContext componentContext) {
		logger.info("Service modified.");
		this.componentContext = componentContext;
		if (wsClient != null) {
			// force re-creation of cached web service instance on next call
			wsClient = null; 
			wsClientUrl = null;
		}
	}
	
	/**
	 * OSGi Declarative Service deactivate method.
	 */
	@Deactivate
	protected synchronized void deactivate(ComponentContext componentContext) {
		logger.info("Service deactivated.");
		this.componentContext = componentContext;
		if (wsClient != null) {
			// force re-creation of cached web service instance on next call
			wsClient = null; 
			wsClientUrl = null;
		}
	}
	
}
