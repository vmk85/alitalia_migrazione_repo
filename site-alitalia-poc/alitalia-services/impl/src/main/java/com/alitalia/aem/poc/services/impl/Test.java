package com.alitalia.aem.poc.services.impl;

import java.util.ArrayList;
import java.util.List;

import com.alitalia.aem.poc.services.api.dto.AirportDTO;
import com.alitalia.aem.ws.booking.staticdataservice.StaticDataServiceClient;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.StaticDataRequest;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.StaticDataResponse;
import com.alitalia.aem.ws.booking.staticdataservice.xsd2.StaticDataType;
import com.alitalia.aem.ws.booking.staticdataservice.xsd3.ArrayOfanyType;
import com.alitalia.aem.ws.booking.staticdataservice.xsd5.Airport;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.ObjectFactory;

public class Test {
	
	public static void main(String[] args) throws Exception {
		ObjectFactory factory = new ObjectFactory();
		StaticDataServiceClient client = new StaticDataServiceClient(
				"http://romcs0944.user.alitalia.local:9000/WcfBooking/Alitalia.Portal.WebFrontend.LCWP.Booking.Services.Lib.StaticDataService.svc");
		StaticDataRequest serviceRequest = new StaticDataRequest();
		serviceRequest.setType(StaticDataType.AIRPORT);
		serviceRequest.setMarket(factory.createStaticDataRequestMarket("it_it"));
		serviceRequest.setLanguageCode(factory.createStaticDataRequestLanguageCode("it"));
		StaticDataResponse serviceResponse = client.getData(serviceRequest);
		
		// translate results to DTOs
		List<Object> entitiesList = serviceResponse.getEntities().getValue().getAnyType();
		List<AirportDTO> resultList = new ArrayList<AirportDTO>();
		for (Object entity : entitiesList) {
			Airport source = (Airport) entity;
			AirportDTO target = new AirportDTO();
			target.setAirportCode(source.getCode().getValue());
			target.setAirportDescription(source.getName().getValue());
			resultList.add(target);
		}
	}
	
}
