package com.alitalia.aem.poc.authentication.identityprovider.millemiglia;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.jackrabbit.oak.spi.security.authentication.external.ExternalIdentityException;
import org.apache.jackrabbit.oak.spi.security.authentication.external.ExternalIdentityRef;
import org.apache.jackrabbit.oak.spi.security.authentication.external.ExternalUser;

public class MilleMigliaUser implements ExternalUser {
	
	protected ArrayList<ExternalIdentityRef> declaredGroups;
	protected ExternalIdentityRef externalId;
	protected String id;
	protected String intermediatePath;
	protected String principalName;
	protected Map<String, Object> properties;
	
	public MilleMigliaUser(ExternalIdentityRef externalId, String id, String intermediatePath,
			String principalName) {
		super();
		this.declaredGroups = new ArrayList<ExternalIdentityRef>();
		this.externalId = externalId;
		this.id = id;
		this.intermediatePath = intermediatePath;
		this.principalName = principalName;
		this.properties = new HashMap<String, Object>();
	}

	public void addDeclaredGroup(ExternalIdentityRef groupRef) {
		declaredGroups.add(groupRef);
	}
	
	public void setProperty(String name, Object value) {
		this.properties.put(name, value);
	}
	
	@Override
	public Iterable<ExternalIdentityRef> getDeclaredGroups()
			throws ExternalIdentityException {
		return declaredGroups;
	}
	
	@Override
	public ExternalIdentityRef getExternalId() {
		return externalId;
	}
	
	@Override
	public String getId() {
		return id;
	}
	
	@Override
	public String getIntermediatePath() {
		return intermediatePath;
	}
	
	@Override
	public String getPrincipalName() {
		return principalName;
	}
	
	@Override
	public Map<String, ?> getProperties() {
		return properties;
	}
	
}
