package com.alitalia.aem.poc.authentication.authenticationhandler.rememberme;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.auth.core.spi.AuthenticationInfo;
import org.apache.sling.auth.core.spi.AuthenticationInfoPostProcessor;

@Component(label = "Alitalia POC - Remember Me Authentication Post Processor", metatype = true)
@Properties({
		@Property(label = "Vendor", name = "service.vendor", value = "Reply", propertyPrivate = true),
		@Property(label = "Vendor", name = "service.description", value = "Alitalia POC - Remember Me Authentication Post Processor", propertyPrivate = true) })
@Service
public class RememberMeAuthenticationInfoPostProcessor implements
		AuthenticationInfoPostProcessor {
	
	@Override
	public void postProcess(AuthenticationInfo authenticationInfo, HttpServletRequest request, 
			HttpServletResponse response) throws LoginException {
		if (RememberMeAuthenticationHandler.AUTH_TYPE.equals(authenticationInfo.getAuthType())) {
			
		}
	}
	
}
