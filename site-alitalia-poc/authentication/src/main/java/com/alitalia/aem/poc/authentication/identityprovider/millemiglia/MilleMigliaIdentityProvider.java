package com.alitalia.aem.poc.authentication.identityprovider.millemiglia;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.jcr.Credentials;
import javax.jcr.SimpleCredentials;
import javax.security.auth.login.LoginException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.oak.spi.security.authentication.external.ExternalGroup;
import org.apache.jackrabbit.oak.spi.security.authentication.external.ExternalIdentity;
import org.apache.jackrabbit.oak.spi.security.authentication.external.ExternalIdentityException;
import org.apache.jackrabbit.oak.spi.security.authentication.external.ExternalIdentityProvider;
import org.apache.jackrabbit.oak.spi.security.authentication.external.ExternalIdentityRef;
import org.apache.jackrabbit.oak.spi.security.authentication.external.ExternalUser;
import org.apache.sling.auth.core.spi.AuthenticationInfo;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.poc.authentication.authenticationhandler.rememberme.RememberMeAuthenticationHandler;

/**
 * MilleMiglia OAK Identity Provider POC.
 */
@Component(
	label="Alitalia POC - Custom Identity Provider",
	metatype=true)
@Service(value=ExternalIdentityProvider.class)
@Properties({
	@Property(
		label="Authentication service endpoint URI", 
		description="URI of the web service endpoint that will be used to authenticate the credentials",
		name=MilleMigliaIdentityProvider.AUTHENTICATION_ENDPOINT_URI,
		value=""),
	@Property(
		label="Vendor",
		name="service.vendor",
		value="Reply",
		propertyPrivate=true)
	})
public class MilleMigliaIdentityProvider implements ExternalIdentityProvider {
	
	/**
	 * Logger.
	 */
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());
	
    /**
     * Unique name of the identity provider.
     */
	public static final String IDENTITY_PROVIDER_NAME = "site-alitalia-poc-millemiglia-identity-provider";
	
	/**
	 * Name of the property used to specify the URI of the external authentication web service.
	 */
	public static final String AUTHENTICATION_ENDPOINT_URI = "authentication.endpoint.uri";
	
	/**
	 * A map of the default MilleMiglia groups, locally defined on activation.
	 */
	private Map<String, MilleMigliaGroup> groups = null;
	
	/**
	 * The component context provided by OSGi runtime.
	 */
	@SuppressWarnings("unused")
	private ComponentContext componentContext = null;
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		//logger.debug("getName() invoked.");
		return IDENTITY_PROVIDER_NAME;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public ExternalUser authenticate(Credentials credentials)
			throws ExternalIdentityException, LoginException {
		logger.debug("authenticate() invoked.");
		SimpleCredentials simpleCredentials;
		if (credentials == null) {
			logger.debug("Null credentials provided.");
			throw new ExternalIdentityException("Null credentials provided.");
		}
		ExternalUser externalUser = null;
		if (credentials instanceof SimpleCredentials) {
			simpleCredentials = (SimpleCredentials) credentials;
			if (simpleCredentials.getUserID() == null) {
				logger.debug("Null user ID provided.");
				throw new ExternalIdentityException("Null user ID provided.");
			}
			String authType = (String) simpleCredentials.getAttribute(AuthenticationInfo.AUTH_TYPE);
			String username = simpleCredentials.getUserID();
			String password = new String(simpleCredentials.getPassword());
			logger.debug("Credentials provided for user {}, authType is {}", username, authType);
			if (RememberMeAuthenticationHandler.AUTH_TYPE.equals(authType)) {
				externalUser = authenticateWithRememberMe(username);
			} else {
				if (password.startsWith("##SocialUID##")) {
					String socialUid = password.substring(("##SocialUID##".length()));
					externalUser = authenticateWithSocialUID(username, socialUid);
				} else {
					externalUser = authenticateWithPinCode(username, password);
				}
				boolean setRememberMeCookie = true; // TODO should be driven by login page UI someway
				if (externalUser != null && setRememberMeCookie) {
					for (String attributeName : simpleCredentials.getAttributeNames()) {
						logger.debug("attribute {} = {}", attributeName, simpleCredentials.getAttribute(attributeName));
					}
				}
			}
		}
		return externalUser;
	}
	
	/**
	 * Authenticate plain customercode/pincode user credentials and returns user details.
	 */
	private ExternalUser authenticateWithPinCode(String userID, String password) throws LoginException {
		// TODO mock implementation, here we should contact the external API
		MilleMigliaUser user = null;
		if (("0006648236".equals(userID) && "4428".equals(password))) {
			ExternalIdentityRef userRef = new ExternalIdentityRef(userID, IDENTITY_PROVIDER_NAME);
			user = new MilleMigliaUser(userRef, userID, null, userID);
			user.addDeclaredGroup(this.groups.get("millemiglia-users").externalId);
		}
		if (user != null) {
			logger.debug("Allowed pincode authentication for user {}.", userID);
			return user;
		} else {
			logger.debug("Denied pincode authentication for user {}, wrong user id or password.", userID);
			throw new LoginException("Login denied, incorrect user ID or password provided.");
		}
	}
	
	/**
	 * Authenticate plain customercode/socialUID user credentials and returns user details.
	 */
	private ExternalUser authenticateWithSocialUID(String userID, String socialUid) throws LoginException {
		// TODO mock implementation, here we should contact the external API
		MilleMigliaUser user = null;
		if ("0006648236".equals(userID)) {
			if ("_guid_9K8R8p23IE3DUMwdjbidcC1JibKKQxPvDwe9d9ku9u4=".equals(socialUid) ||
					"_guid_R44VrRa972pWSQmaO1e47w==".equals(socialUid)) {
				ExternalIdentityRef userRef = new ExternalIdentityRef(userID, IDENTITY_PROVIDER_NAME);
				user = new MilleMigliaUser(userRef, userID, null, userID);
				user.addDeclaredGroup(this.groups.get("millemiglia-users").externalId);
			}
		}
		if (user != null) {
			logger.debug("Allowed social authentication for user {} and social guid {}.", userID, socialUid);
			return user;
		} else {
			logger.debug("Denied social authentication for user {} and social guid {}, invalid uid.", userID, socialUid);
			throw new LoginException("Login denied, invalid social UID provided.");
		}
	}
	
	/**
	 * Authenticate just the customercode, here we are trusting a processed remember-me token.
	 */
	private ExternalUser authenticateWithRememberMe(String userID) throws LoginException {
		// TODO mock implementation, here we should contact the external API to retrieve user info
		MilleMigliaUser user = null;
		if ("0006648236".equals(userID)) {
			ExternalIdentityRef userRef = new ExternalIdentityRef(userID, IDENTITY_PROVIDER_NAME);
			user = new MilleMigliaUser(userRef, userID, null, userID);
			user.addDeclaredGroup(this.groups.get("millemiglia-users").externalId);
		}
		if (user != null) {
			logger.debug("Allowed remember-me authentication for user {}.", userID);
			return user;
		} else {
			logger.debug("Denied remember-me authentication for user {}, wrong user id or password.", userID);
			throw new LoginException("Login denied, incorrect user ID or password provided.");
		}
	}
	
	private void loadGroups() {
		logger.info("Refreshing locally defined groups.");
		this.groups = new HashMap<String, MilleMigliaGroup>();
		// base MilleMiglia users group
		ExternalIdentityRef milleMigliaUsersRef = new ExternalIdentityRef("millemiglia-users", IDENTITY_PROVIDER_NAME);
		MilleMigliaGroup milleMigliaUsers = new MilleMigliaGroup(null, Arrays.asList(milleMigliaUsersRef), 
				milleMigliaUsersRef, "millemiglia-users", null, "millemiglia-users");
		milleMigliaUsers.setProperty("isMilleMigliaGroup", true);
		milleMigliaUsers.setProperty("milleMigliaGroupName", "base");
		groups.put("millemiglia-users", milleMigliaUsers);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ExternalIdentity getIdentity(ExternalIdentityRef ref) throws ExternalIdentityException {
		logger.debug("getIdentity() invoked for {}", ref.getId());
		return this.groups.get(ref.getId());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ExternalGroup getGroup(String name) throws ExternalIdentityException {
		logger.error("getGroup() invoked - unsupported in this implementation");
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public ExternalUser getUser(String userId) throws ExternalIdentityException {
		logger.error("getUser() invoked - unsupported in this implementation.");
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<ExternalGroup> listGroups() throws ExternalIdentityException {
		logger.error("listGroups() invoked - unsupported in this implementation");
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<ExternalUser> listUsers() throws ExternalIdentityException {
		logger.error("listUsers() invoked - unsupported in this implementation.");
		return null;
	}
	
	/**
	 * OSGi Declarative Service activate method.
	 */
	@Activate
	protected synchronized void activate(ComponentContext componentContext) {
		logger.info("Service activated.");
		this.componentContext = componentContext;
		loadGroups();
	}
	
	/**
	 * OSGi Declarative Service modified method.
	 */
	@Modified
	protected synchronized void modified(ComponentContext componentContext) {
		logger.info("Service modified.");
		this.componentContext = componentContext;
		loadGroups();
	}
	
	/**
	 * OSGi Declarative Service deactivate method.
	 */
	@Deactivate
	protected synchronized void deactivate(ComponentContext componentContext) {
		logger.info("Service deactivated.");
		this.componentContext = componentContext;
		this.groups = null;
	}
	
}
