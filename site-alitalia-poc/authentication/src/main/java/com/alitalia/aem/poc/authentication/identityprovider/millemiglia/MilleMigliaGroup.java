package com.alitalia.aem.poc.authentication.identityprovider.millemiglia;

import java.util.HashMap;
import java.util.Map;

import org.apache.jackrabbit.oak.spi.security.authentication.external.ExternalGroup;
import org.apache.jackrabbit.oak.spi.security.authentication.external.ExternalIdentityException;
import org.apache.jackrabbit.oak.spi.security.authentication.external.ExternalIdentityRef;

public class MilleMigliaGroup implements ExternalGroup {
	
	protected Iterable<ExternalIdentityRef> declaredGroups;
	protected Iterable<ExternalIdentityRef> declaredMembers;
	protected ExternalIdentityRef externalId;
	protected String id;
	protected String intermediatePath;
	protected String principalName;
	protected Map<String, Object> properties;
	
	public MilleMigliaGroup(Iterable<ExternalIdentityRef> declaredGroups,
			Iterable<ExternalIdentityRef> declaredMembers,
			ExternalIdentityRef externalId, String id, String intermediatePath,
			String principalName) {
		super();
		this.declaredGroups = declaredGroups;
		this.declaredMembers = declaredMembers;
		this.externalId = externalId;
		this.id = id;
		this.intermediatePath = intermediatePath;
		this.principalName = principalName;
		this.properties = new HashMap<String, Object>();
	}
	
	public void setProperty(String name, Object value) {
		this.properties.put(name, value);
	}
	
	@Override
	public Iterable<ExternalIdentityRef> getDeclaredGroups()
			throws ExternalIdentityException {
		return declaredGroups;
	}
	
	@Override
	public Iterable<ExternalIdentityRef> getDeclaredMembers()
			throws ExternalIdentityException {
		return declaredMembers;
	}
	
	@Override
	public ExternalIdentityRef getExternalId() {
		return externalId;
	}
	
	@Override
	public String getId() {
		return id;
	}
	
	@Override
	public String getIntermediatePath() {
		return intermediatePath;
	}
	
	@Override
	public String getPrincipalName() {
		return principalName;
	}
	
	@Override
	public Map<String, ?> getProperties() {
		return properties;
	}
	
}
