package com.alitalia.aem.poc.authentication.authenticationhandler.rememberme;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Dictionary;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.auth.core.spi.AuthenticationHandler;
import org.apache.sling.auth.core.spi.AuthenticationInfo;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Custom authentication handler that add supports for cookie-based persistent login.
 * 
 * <p>This handler is not designed to replace the default HTTP form-based authentication handler,
 * that will be still used to manage credentials check, but to add a layer on top of it to
 * capture and manage authentication checks and challenge them against a cookie-based persistent
 * login data.</p>
 */

@Component(label = "Alitalia POC - Remember Me Authentication Handler", metatype = true)
@Properties({
		@Property(label = "Enabled", description = "Can be used to enable or disable the persistent login feature through configuration.", name = RememberMeAuthenticationHandler.ENABLED_PROPERTY, value = { "false" }, cardinality = 1),
		@Property(label = "Authentication Paths", description = "JCR Paths which this Authentication Handler will authenticate.", name = AuthenticationHandler.PATH_PROPERTY, value = { "/content/sitealitaliapoc" }, cardinality = Integer.MAX_VALUE),
		@Property(label = "Service Ranking", description = "Service ranking. Higher gives more priority. IMPORTANT THIS SITS AT THE TOP OF THE STACK.", name = "service.ranking", intValue = 10000, propertyPrivate = false),
		@Property(label = "Vendor", name = "service.vendor", value = "Reply", propertyPrivate = true),
		@Property(label = "Vendor", name = "service.description", value = "Alitalia POC - Remember Me Authentication Handler", propertyPrivate = true) })
@Service(value=AuthenticationHandler.class)
public class RememberMeAuthenticationHandler implements AuthenticationHandler {
	
	/**
	 * Logger.
	 */
	private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());
	
	/**
	 * Name of the enabled component property.
	 */
	public static final String ENABLED_PROPERTY = "enabled";
	
	/**
	 * Name of the request attribute used to mark the request as processed
	 * by the remember-me management.
	 */
	public static final String REQUEST_MARKER = "alitalia-process.remember-me";
	
	/**
	 * Authentication type.
	 */
	public static final String AUTH_TYPE = "alitalia-remember-me";
	
	/**
	 * Name of the cookie used to store and retrieve remember me info.
	 */
	public static final String COOKIE_NAME = "alitalia-remember-me";

	/**
	 * Secret key of the generated HMAC-SHA1 signatures of tokens.
	 */
	public static final String COOKIE_HMAC_KEY = "testpwd"; // FIXME change for production (set a real password)
	
	/**
	 * Domain of the cookie used to store and retrieve remember me info.
	 */
	public static final String COOKIE_DOMAIN = ""; // FIXME change for production (empty string is for localhost)
	
	/**
	 * Path of the cookie used to store and retrieve remember me info.
	 */
	public static final String COOKIE_PATH = "/";
	
	/**
	 * Max age of the cookie used to store and retrieve remember me info.
	 */
	public static final int COOKIE_MAX_AGE = 7776000; // 90 days
	
	/**
	 * The component context provided by OSGi runtime.
	 */
	private ComponentContext componentContext = null;
	
	/**
	 * Try to extract the current user credentials from the remember-me persistent cookie.
	 * 
	 * <p>Due to the high priority, this is called for requests before the default handler.
	 * Using a request attribute flag, in the first invocation it will be ignored, in order
	 * to let the default handler to try verification of standard authentication data.</p>
	 * 
	 * <p>A second invocation will be triggered if the default handler fails, due to the
	 * custom logic put in the <code>requestCredentials</code> method. In this second
	 * invocation the request attribute flag will be set, thus this handler will try
	 * to check the remember-me cookie and process it.</p>
	 */
	@Override
	@SuppressWarnings("rawtypes")
	public AuthenticationInfo extractCredentials(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("extractCredentials invoked");
		Dictionary properties = componentContext.getProperties();
		// manage enable switch configuration
		String enabled = (String) properties.get(RememberMeAuthenticationHandler.ENABLED_PROPERTY);
		if (!"true".equalsIgnoreCase(enabled)) {
			logger.debug("Ignoring request, component is disabled through configuration.");
			return null;
		}
		// verify request marker
		if (!Boolean.TRUE.equals(request.getAttribute(REQUEST_MARKER))) {
			logger.debug("Unmarked request, ignoring");
			return null;
		}
		logger.debug("Marker found in request, processing");
		logger.debug("Matched configured JCR path was {}", request.getAttribute("path"));
		logger.debug("Actual Requested path is {}", request.getPathInfo());
		// should manage request, verify cookie
		String userName = processRememberMeUser(request, response);
		if (userName != null) {
			logger.debug("Found credentials for user {} in remember-me cookie", userName);
			AuthenticationInfo authenticationInfo = new AuthenticationInfo(AUTH_TYPE, userName);
			return authenticationInfo;
		} else {
			logger.debug("No valid user credentials found in remember-me cookie");
			return null;
		}
	}
	
	/**
	 * This method is invoked when credentials are not found for the request, and
	 * a login should be requested to the user for authentication.
	 * 
	 * <p>Thanks to the high priority, this should be called before the standard
	 * handler, so we can check for the presence of a valid remember-me cookie.
	 * If found, a forward to ourselves will be performed after flagging the request
	 * with a marker, to activate the custom <code>extractCredentials</code> of the
	 * handler and have it process the cookie. Otherwise, the processing is delegated
	 * to the standard handler, that will forward user to the login page.</p>
	 */
	@Override
	public boolean requestCredentials(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("requestCredentials invoked");
		if (Boolean.TRUE.equals(request.getAttribute(REQUEST_MARKER))) {
			// another requestCredentials for an already marked request, remember-me was attempted and failed
			return false;
		} else {
			String cookieValue = getCookieValue(request, response, COOKIE_NAME);
			if (validateRememberMeCookieValue(cookieValue) == null) {
				// no valid remember-me cookie available to use
				return false;
			} else {
				// remember-me cookie available, set the marker and just forward the request on same resource:
				// the extractCredentials will detected the marker and try to process the cookie value
				request.setAttribute(REQUEST_MARKER, Boolean.TRUE);
				try {
		            request.getRequestDispatcher(request.getRequestURI()).forward(request, response);
		            return true;
				} catch (ServletException e) {
		            logger.error("Could not forward request back into authentication process due to ServletException");
		            return false;
				} catch (IOException e) {
		            logger.error("Could not forward request back into authentication process due to IOException");
		            return false;
		        }
			}
		}
	}
	
	/**
	 * This method is invoked when credentials must be dropped permanently for 
	 * the session, e.g. in case of a logout.
	 * 
	 * <p>This implementation will make sure that the remember-me cookie is cleared
	 * to avoid an automatic and immediate remember-me auto-login again.</p>
	 */
	@Override
	public void dropCredentials(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("dropCredentials invoked");
		clearRememberMeUser(request, response);
	}
	
	/**
	 * Used to process the remember-me cookie from the request.
	 * 
	 * <p>If a valid cookie is found, the related token will be invalidated
	 * and a new one will be issued and returned to the client.</p>
	 * 
	 * <p>If an invalid cookie is found, all issued tokens
	 * 
	 * @param request The HTTP request to work with.
	 * @param response The HTTP response to work with.
	 * @return The user name if a valid cookie was found, or null otherwise.
	 */
	private String processRememberMeUser(final HttpServletRequest request, final HttpServletResponse response) {
		String cookieValue = getCookieValue(request, response, COOKIE_NAME);
		if (cookieValue != null) {
			// validate cookie value (HMAC or server-side storage check)
			String userName = validateRememberMeCookieValue(cookieValue);
			if (userName == null) {
				// invalid: clear current in the response and return null
				// (in case of server-side storage, we should clear all other tokens of the same user now)
				clearCookie(request, response, COOKIE_NAME);
				return null;
			} else {
				// valid: generate a new fresh cookie value and return the username
				// (in case of server-side storage, we should invalidate current cookie token)
				String newCookieValue = createRememberMeCookieValue(userName);
				setCookieValue(request, response, COOKIE_NAME, COOKIE_DOMAIN, COOKIE_PATH, COOKIE_MAX_AGE, newCookieValue);
				return userName;
			}
		}
		return null;
	}

	/**
	 * Used to clear the remember-me cookie trough the response.
	 * 
	 * @param request The HTTP request to work with.
	 * @param response The HTTP response to work with.
	 */
	private void clearRememberMeUser(final HttpServletRequest request, HttpServletResponse response) {
		clearCookie(request, response, COOKIE_NAME);
	}
	
	/**
	 * Utility method to read a cookie value.
	 *
	 * @param request The request to work with.
	 * @param response The response to work with.
	 * @param cookieName The name of the cookie.
	 * @return The cookie value, or null if not found.
	 */
	private String getCookieValue(HttpServletRequest request, HttpServletResponse response, String cookieName) {
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (COOKIE_NAME.equals(cookie.getName())) {
					return cookie.getValue();
				}
			}
		}
		return null;
	}
	
	/**
	 * Utility method to clear a cookie.
	 *
	 * @param request The request to work with.
	 * @param response The response to work with.
	 * @param cookieName The name of the cookie.
	 * @return The cookie value, or null if not found.
	 */
	private String clearCookie(HttpServletRequest request, HttpServletResponse response, String cookieName) {
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (COOKIE_NAME.equals(cookie.getName())) {
					cookie.setMaxAge(0);
	                response.addCookie(cookie);
	                break;
				}
			}
		}
		return null;
	}
	
	/**
	 * Utility method to set a persistent cookie value.
	 *
	 * @param request The request to work with.
	 * @param response The response to work with.
	 * @param cookieName The name of the cookie.
	 * @param domain The domain of the cookie to set.
	 * @param path The path of the cookie to set.
	 * @param maxAge The max age of the cookie.
	 * @param value The value to set.
	 */
	private void setCookieValue(HttpServletRequest request, HttpServletResponse response, String cookieName, 
			String domain, String path, int maxAge, String value) {
		Cookie cookie = new Cookie(cookieName, value);
		cookie.setDomain(domain);
		cookie.setPath(path);
		cookie.setMaxAge(maxAge);
		response.addCookie(cookie);
	}
	
	/**
	 * Internal utility method to encode user credentials in a HMAC-SHA1 cookie value.
	 * 
	 * @param userName The user name.
	 * @return The encoded String.
	 */
	private String createRememberMeCookieValue(String userName) throws RuntimeException {
		try {
			String escapedUserName = URLEncoder.encode(userName, "UTF-8");
			String clearTextValue = escapedUserName + "#" + System.currentTimeMillis();
			String signatureValue = URLEncoder.encode(hmacSha1(clearTextValue, COOKIE_HMAC_KEY), "UTF-8");
			String cookieValue = clearTextValue + "#" + signatureValue;
			return cookieValue;
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e); // (should never happen with UTF-8)
		}
	}

	/**
	 * Internal utility method to validate the value of a remember-me token.
	 * <p>Recognized but timed out values are considered invalid by this method.</p>
	 * 
	 * @param cookieValue The value.
	 * @return The username for a valid value, or null.
	 * @throws Exception
	 */
	private String validateRememberMeCookieValue(String cookieValue) {
		logger.debug("Validating cookie value {}...", cookieValue);
		try {
			if (cookieValue == null || cookieValue.length() == 0) {
				logger.error("Invalid cookie: empty cookie value.");
				return null;
			}
			String[] elements = cookieValue.split("#");
			if (elements.length != 3) {
				logger.error("Invalid cookie: unrecognized value format.");
				return null;
			}
			String escapedUserName = elements[0];
			String timestampValue = elements[1];
			String signatureValue = elements[2];
			String userName = URLDecoder.decode(escapedUserName, "UTF-8");
			if (userName == null || userName.length() == 0) {
				logger.error("Invalid cookie: empty username.");
				return null;
			}
			int timestamp = Integer.parseInt(timestampValue);
			if (timestamp > System.currentTimeMillis()
					|| timestamp < System.currentTimeMillis() - COOKIE_MAX_AGE) {
				logger.debug("Invalid cookie: timed out.");
				return null;
			}
			if (signatureValue == null || signatureValue.length() == 0) {
				logger.error("Invalid cookie: empty signature.");
				return null;
			}
			String clearTextValue = escapedUserName + "#" + timestampValue;
			String checkSignatureValue = URLEncoder.encode(hmacSha1(clearTextValue, COOKIE_HMAC_KEY), "UTF-8");
			if (signatureValue.equals(checkSignatureValue)) {
				logger.debug("Cookie is valid, username is {}.", userName);
				return userName;
			} else {
				logger.error("Invalid cookie: unrecognized signature.");
				return null;
			}
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e); // (should never happen with UTF-8)
		} catch (NumberFormatException e) {
			logger.error("Invalid cookie: cannot parse timestampe value.");
			return null;
		}
	}
	
	/**
	 * Compute and returns an HMAC-SHA1 of the provided value using the specified key.
	 * 
	 * @param value The value to encode.
	 * @param key The key to use.
	 * @return The encoded value as a UTF-8 string.
	 */
	public static String hmacSha1(String value, String key) {
        try {
            SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(), "HmacSHA1");
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(signingKey);
            byte[] rawHmac = mac.doFinal(value.getBytes());
            String hmac = new String(rawHmac, "UTF-8");
            return hmac;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
	
	/**
	 * OSGi Declarative Service activate method.
	 */
	@Activate
	protected synchronized void activate(ComponentContext componentContext) {
		logger.info("Service activated.");
		this.componentContext = componentContext;
	}
	
	/**
	 * OSGi Declarative Service modified method.
	 */
	@Modified
	protected synchronized void modified(ComponentContext componentContext) {
		logger.info("Service modified.");
		this.componentContext = componentContext;
	}
	
	/**
	 * OSGi Declarative Service deactivate method.
	 */
	@Deactivate
	protected synchronized void deactivate(ComponentContext componentContext) {
		logger.info("Service deactivated.");
		this.componentContext = componentContext;
	}
	
}
