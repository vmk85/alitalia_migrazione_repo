package com.alitalia.aem.ws.geospatial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.geospatial.service.GeoSpatialServiceClient;
import com.alitalia.aem.ws.geospatial.service.xsd1.GetOffersRequest;
import com.alitalia.aem.ws.geospatial.service.xsd1.GetOffersResponse;
import com.alitalia.aem.ws.geospatial.service.xsd2.IPAddress;
import com.alitalia.aem.ws.geospatial.service.xsd3.AddressFamily;
import com.alitalia.aem.ws.geospatial.service.xsd5.ArrayOfunsignedShort;

public class TestGeoSpatialClient {
	
	private GeoSpatialServiceClient geoSpatialServiceClient;

	private static final Logger log = LoggerFactory.getLogger(TestGeoSpatialClient.class);
	
	public TestGeoSpatialClient() throws Exception {
		try {
			geoSpatialServiceClient = new GeoSpatialServiceClient("http://romcs0944.user.alitalia.local:9000/WcfBooking/Alitalia.Portal.WebFrontend.LCWP.Booking.Services.Lib.GeoSpatialService.svc");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void execute() {
		try {
//			DatatypeFactory dtf = DatatypeFactory.newInstance();
//			com.alitalia.aem.ws.geospatial.service.xsd.ObjectFactory factory = new com.alitalia.aem.ws.geospatial.service.xsd.ObjectFactory();
			com.alitalia.aem.ws.geospatial.service.xsd1.ObjectFactory factory1 = new com.alitalia.aem.ws.geospatial.service.xsd1.ObjectFactory();
			com.alitalia.aem.ws.geospatial.service.xsd2.ObjectFactory factory2 = new com.alitalia.aem.ws.geospatial.service.xsd2.ObjectFactory();
			new com.alitalia.aem.ws.geospatial.service.xsd3.ObjectFactory();
//			com.alitalia.aem.ws.geospatial.service.xsd4.ObjectFactory factory4 = new com.alitalia.aem.ws.geospatial.service.xsd4.ObjectFactory();
			com.alitalia.aem.ws.geospatial.service.xsd5.ObjectFactory factory5 = new com.alitalia.aem.ws.geospatial.service.xsd5.ObjectFactory();
//			com.alitalia.aem.ws.geospatial.service.xsd6.ObjectFactory factory6 = new com.alitalia.aem.ws.geospatial.service.xsd6.ObjectFactory();
//			com.alitalia.aem.ws.geospatial.service.xsd7.ObjectFactory factory7 = new com.alitalia.aem.ws.geospatial.service.xsd7.ObjectFactory();
//			com.alitalia.aem.ws.geospatial.service.xsd8.ObjectFactory factory8 = new com.alitalia.aem.ws.geospatial.service.xsd8.ObjectFactory();

			long address = 357466922;
			ArrayOfunsignedShort numbers = factory5.createArrayOfunsignedShort();
			numbers.getUnsignedShort().add(new Integer(0));
			numbers.getUnsignedShort().add(new Integer(0));
			numbers.getUnsignedShort().add(new Integer(0));
			numbers.getUnsignedShort().add(new Integer(0));
			numbers.getUnsignedShort().add(new Integer(0));
			numbers.getUnsignedShort().add(new Integer(0));
			numbers.getUnsignedShort().add(new Integer(0));
			numbers.getUnsignedShort().add(new Integer(0));

			IPAddress ipAddress = factory2.createIPAddress();
			ipAddress.setMAddress(address);
			ipAddress.setMFamily(AddressFamily.INTER_NETWORK);
			ipAddress.setMHashCode(0);
			ipAddress.setMNumbers(numbers);
			ipAddress.setMScopeId(0);

			GetOffersRequest request = factory1.createGetOffersRequest();
			request.setIPAddress(factory1.createGetOffersRequestIPAddress(ipAddress));
			request.setLanguageCode(factory1.createGetOffersRequestLanguageCode("IT"));
			request.setMarketCode(factory1.createGetCitiesRequestMarketCode("IT"));

			@SuppressWarnings("unused")
			GetOffersResponse offers = geoSpatialServiceClient.getOffers(request);
	
			log.info("call ok");
		} catch (Exception e) {
			log.info("error");
			e.printStackTrace();
		}
	}
	
	public static void main(String arg []) throws Exception {
		log.info("calling..");
		TestGeoSpatialClient client = new TestGeoSpatialClient();
		client.execute();
		log.info("call completed");
	}
}