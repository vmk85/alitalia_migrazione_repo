package it.reply.sytel.toservices;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.PassengerBase;
import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.data.home.AvailableFlightsData;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.BrandSearchData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.PassengerNumbersData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.common.data.home.SearchDestinationData;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.CabinTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.SearchTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.TimeTypeEnum;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionResponse;
import com.alitalia.aem.common.messages.home.enumeration.SearchExecuteResponseType;
import com.alitalia.aem.web.component.searchflights.delegate.SearchFlightsDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(name = "TestFirstSolutionSearchServlet", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
	@Property(name = "sling.servlet.selectors", value = {"testsearchsolution"}),
	@Property(name = "sling.servlet.extensions", value = {"json"}),
	@Property(name = "sling.servlet.methods", value = {"GET"})
})
public class TestFirstSolutionSearchServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;	
	private static final Logger logger = LoggerFactory.getLogger(TestFirstSolutionSearchServlet.class);
	

	@Reference(policy=ReferencePolicy.DYNAMIC)
	private volatile SearchFlightsDelegate searchFlightsDelegate;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		
		try {
			logger.info("Executing doGet.");
			
			response.setContentType("application/json");
			
			String valuePartenza = request.getParameter("partenza");
			String valueArrivo = request.getParameter("arrivo");
			Calendar flightDepartureDate = Calendar.getInstance();
			flightDepartureDate.setTime(new SimpleDateFormat("dd/MM/yyyy").parse("10/08/2015"));

			Calendar ribbonOutboundStart = Calendar.getInstance();
			ribbonOutboundStart.setTimeInMillis(flightDepartureDate.getTimeInMillis());
			ribbonOutboundStart.setTimeZone(flightDepartureDate.getTimeZone());
			ribbonOutboundStart.add(Calendar.DATE, -3);

			AirportData fromAirport = new AirportData();
//			fromAirport.setCityCode("ROM");
//			fromAirport.setCode("FCO");
			fromAirport.setCityCode(valuePartenza);
			fromAirport.setCode(valuePartenza);
			fromAirport.setCountryCode("IT");

			AirportData toAirport = new AirportData();
//			toAirport.setCityCode("PMO");
//			toAirport.setCode("PMO");
			toAirport.setCityCode(valueArrivo);
			toAirport.setCode(valueArrivo);
			toAirport.setCountryCode("IT");

			SearchDestinationData destination1 = new SearchDestinationData();
			destination1.setFromAirport(fromAirport);
			destination1.setToAirport(toAirport);
			destination1.setRouteType(RouteTypeEnum.OUTBOUND);
			destination1.setTimeType(TimeTypeEnum.ANYTIME);
			destination1.setDepartureDate(flightDepartureDate);

			SearchDestinationData destination2 = new SearchDestinationData();
			destination2.setFromAirport(toAirport);
			destination2.setToAirport(fromAirport);
			destination2.setRouteType(RouteTypeEnum.RETURN);
			destination2.setTimeType(TimeTypeEnum.ANYTIME);
			Calendar flightArrivalDate = Calendar.getInstance();
			flightArrivalDate.setTime(new SimpleDateFormat("dd/MM/yyyy").parse("20/08/2015"));
			destination2.setDepartureDate(flightArrivalDate);

			Calendar ribbonReturnStart = Calendar.getInstance();
			ribbonReturnStart.setTimeInMillis(flightArrivalDate.getTimeInMillis());
			ribbonReturnStart.setTimeZone(flightArrivalDate.getTimeZone());
			ribbonReturnStart.add(Calendar.DATE, -3);


			PassengerNumbersData passengerNumbers = new PassengerNumbersData();
			passengerNumbers.setNumber(1);
			passengerNumbers.setPassengerType(PassengerTypeEnum.ADULT);

			BrandSearchData brandSearchData = new BrandSearchData();
			brandSearchData.setCug("ADT");
			brandSearchData.setOnlyDirectFlight(false);
			brandSearchData.setType(SearchTypeEnum.BRAND_SEARCH);
			brandSearchData.addDestination(destination1);
			brandSearchData.addDestination(destination2);
			brandSearchData.addPassengerNumbers(passengerNumbers);
			brandSearchData.setMarket("IT");
			brandSearchData.setSearchCabin(CabinEnum.ECONOMY);
			brandSearchData.setSearchCabinType(CabinTypeEnum.PERMITTED);

			SearchFlightSolutionRequest searchRequest = new SearchFlightSolutionRequest("TID", "SID");
			searchRequest.setFilter(brandSearchData);
			searchRequest.setLanguageCode("IT");
			searchRequest.setMarket("IT");
			searchRequest.setResponseType(SearchExecuteResponseType.MODEL);
			searchRequest.setRibbonOutboundStartDate(ribbonOutboundStart);
			searchRequest.setRibbonReturnStartDate(ribbonReturnStart);

			SearchFlightSolutionResponse searchResponse = searchFlightsDelegate.searchInitialSolutions(searchRequest);
			AvailableFlightsData availableFlights = null;
			List<PassengerBase> extraChargePassengerList = null;
			if (searchResponse != null) {
				logger.info("Ha funzionato: " + searchResponse.toString());
				availableFlights = searchResponse.getAvailableFlights();
				extraChargePassengerList = searchResponse.getExtraChargePassengerList();
			}

//			AgencyLoginRequest loginRequest = new AgencyLoginRequest("1249812", "gsiweug");
//			loginRequest.setRuolo(AlitaliaTradeUserType.fromValue(valueRuolo));
//			loginRequest.setCodiceAgenzia(valueAgenzia);
//			loginRequest.setPassword(valuePassword);
//			AgencyLoginResponse loginResponse = businessLoginDelegate.agencyLogin(loginRequest);
//			AlitaliaTradeAgencyData agencyData = null;
//			if (loginResponse != null) {
//				logger.info("Ha funzionato: " + loginResponse.toString());
//				agencyData = loginResponse.getAgencyData();
//			}

			TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
			out.object();
			
//			if (agencyData != null) {
//				out.key("result").value(agencyData.getEmailAlitalia());
//				out.key("iataAgency").value(agencyData.isIataAgency());
//				out.key("groupEnabled").value(agencyData.isGroupEnabled());
//			}
			if (availableFlights != null) {
				out.key("solutionset").value(availableFlights.getSolutionSet());
				out.key("sessionid").value(availableFlights.getSessionId());
				for (RouteData route : availableFlights.getRoutes()) {
					out.key("direction").value(route.getType().toString());
					for (FlightData flight : route.getFlights()) {
						if (flight instanceof DirectFlightData) {
							for (BrandData brand : ((DirectFlightData) flight).getBrands()) {
								out.key("solutionid").value(brand.getSolutionId());
							}
						}
					}
				}
//				out.key("routes").value(availableFlights.getRoutes().toString());
				out.key("tabs").value(availableFlights.getTabs().toString());
			}
			else
				out.key("result").value("availableFlights is null!!!");

			if (extraChargePassengerList != null) 
				out.key("passenegerList").value(extraChargePassengerList);
			else
				out.key("resultExtraCharge").value("extraChargePassengerList is null!!!");

			out.endObject();
		
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			logger.error("Exception while executing doGet.", e);
		}
		
		logger.info("Executed doGet.");
	}
	
	
}
