package it.reply.sytel.toservices;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.AlitaliaTradeAgencyData;
import com.alitalia.aem.common.data.home.enumerations.AlitaliaTradeUserType;
import com.alitalia.aem.common.messages.home.AgencyRetrievePasswordByIDRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrievePasswordResponse;
import com.alitalia.aem.web.component.businesslogin.delegate.BusinessLoginDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(name = "TestBusinessRetrievePassword", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
	@Property(name = "sling.servlet.selectors", value = {"testbusinessretrievepassword"}),
	@Property(name = "sling.servlet.extensions", value = {"json"}),
	@Property(name = "sling.servlet.methods", value = {"GET"})
})
public class TestBusinessRetrievePasswordServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;	
	private static final Logger logger = LoggerFactory.getLogger(TestBusinessRetrievePasswordServlet.class);
	
//	@Reference
//	private SearchFlightsDelegate searchFlightsDelegate;

	@Reference
	private BusinessLoginDelegate businessLoginDelegate;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		
		try {
			logger.info("Executing doGet.");
			
			response.setContentType("application/json");
			
			String valueRuolo = request.getParameter("ruolo");
			String valueAgenzia = request.getParameter("agenzia");

			AgencyRetrievePasswordByIDRequest retrievePasswordRequest = new AgencyRetrievePasswordByIDRequest("1249812", "gsiweug");
			retrievePasswordRequest.setRuolo(AlitaliaTradeUserType.fromValue(valueRuolo));
			retrievePasswordRequest.setCodiceAgenzia(valueAgenzia);
			AgencyRetrievePasswordResponse retrievePasswordResponse = businessLoginDelegate.retrievePasswordByID(retrievePasswordRequest);
			AlitaliaTradeAgencyData agencyData = null;
			if (retrievePasswordResponse != null) {
				logger.info("Ha funzionato: " + retrievePasswordResponse.toString());
				agencyData = retrievePasswordResponse.getAgencyData();
			}

			TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
			out.object();
			
			if (agencyData != null)
				out.key("result").value("password: " + agencyData.getPassword());
			else
				out.key("result").value("error retrieving password");

			out.endObject();
		
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			logger.error("Exception while executing doGet.", e);
		}
		
		logger.info("Executed doGet.");
	}
	
	
}
