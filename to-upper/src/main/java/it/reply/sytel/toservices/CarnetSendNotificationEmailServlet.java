package it.reply.sytel.toservices;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.InfoCarnetData;
import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.common.messages.home.CarnetSendNotificationEmailRequest;
import com.alitalia.aem.common.messages.home.CarnetSendNotificationEmailResponse;
import com.alitalia.aem.web.component.bookingticket.delegate.BookingTicketDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(name = "CarnetSendNotificationEmailServlet", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({ @Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "sling.servlet.selectors", value = { "testsendnotificationemailcarnet" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "sling.servlet.methods", value = { "GET" }) })
public class CarnetSendNotificationEmailServlet extends SlingSafeMethodsServlet {


	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(CarnetSendNotificationEmailServlet.class);

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile BookingTicketDelegate ticketDelegate;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException,
			IOException {
		try {
			logger.info("Executing doGet.");
			logger.info("contextPath: " + request.getContextPath());
			logger.info("localeAddr: " + request.getLocalAddr());
			logger.info("localeName: " + request.getLocalName());
			logger.info("localPort: " + request.getLocalPort());
			logger.info("sceheme: " + request.getScheme());
			logger.info("servername: " + request.getServerName());
			logger.info("serverport: " + request.getServerPort());

			response.setContentType("application/json");

			CarnetSendNotificationEmailRequest servletRequest = new CarnetSendNotificationEmailRequest("123456789", "abcdefgh");
			servletRequest.setLanguageCode("IT");
			servletRequest.setMarketCode("IT");
			RoutesData prenotation = new RoutesData();
			prenotation.setFarmId("id");
			prenotation.setMseType("mseType");
			prenotation.setNationalInsuranceNumber("insNumber");
			servletRequest.setPrenotation(prenotation);
			servletRequest.setCarnetInfo(new InfoCarnetData());
			servletRequest.setNotificationMailHtml("<body></body>");
			
			logger.info("Carnet sendCarnetNotificationEmail Request: " + servletRequest);
			CarnetSendNotificationEmailResponse servletResponse = ticketDelegate.sendCarnetNotificationEmail(servletRequest);
			logger.info("Carnet sendCarnetNotificationEmail Response: " + servletResponse);
			
			TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
			out.object().key("JSON").value("OK").endObject();

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception while executing doGet.", e);
		}

		logger.info("Executed doGet.");
	}

}
