package it.reply.sytel.toservices;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.TicketData;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.ContactTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.CreditCardTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbApisTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbFlightStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbFlightTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbLegTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbLinkTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbPaymentGroupMaskEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbUpdateSSREnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.data.home.mmb.MmbAirportData;
import com.alitalia.aem.common.data.home.mmb.MmbApisInfoData;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.MmbPassengerData;
import com.alitalia.aem.common.data.home.mmb.MmbPaymentData;
import com.alitalia.aem.common.data.home.mmb.MmbRequestBaseInfoData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryCartItemData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryCouponData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryEticketData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryFlightData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryInitPaymentData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryPassengerData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryPaymentData;
import com.alitalia.aem.common.messages.home.InsuranceResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryAddToCartRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryAddToCartResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutInitRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutInitResponse;
import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesRequest;
import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesResponse;
import com.alitalia.aem.common.messages.home.MmbInsuranceInfoRequest;
import com.alitalia.aem.common.messages.home.MmbPayInsuranceRequest;
import com.alitalia.aem.common.messages.home.MmbModifyPnrApisInfoRequest;
import com.alitalia.aem.common.messages.home.MmbModifyPnrApisInfoResponse;
import com.alitalia.aem.common.messages.home.MmbSendEmailRequest;
import com.alitalia.aem.common.messages.home.MmbSendEmailResponse;
import com.alitalia.aem.common.messages.home.RetrieveCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCountriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveCreditCardCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCreditCardCountriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveFrequentFlayerRequest;
import com.alitalia.aem.common.messages.home.RetrieveMealsRequest;
import com.alitalia.aem.common.messages.home.RetrieveMealsResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbAirportCheckinRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAirportCheckinResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbAmericanStatesRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAmericanStatesResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCatalogRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCatalogResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbFlightSeatMapRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbFlightSeatMapResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbFrequentFlyerCarriersResponse;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemRequest;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemResponse;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixRequest;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixResponse;
import com.alitalia.aem.common.messages.home.RetrieveProvincesRequest;
import com.alitalia.aem.common.messages.home.RetrieveProvincesResponse;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationRequest;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationResponse;
import com.alitalia.aem.common.messages.home.SapInvoiceRequest;
import com.alitalia.aem.common.messages.home.SapInvoiceResponse;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrContactsRequest;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrContactsResponse;
import com.alitalia.aem.service.api.home.MmbCommonService;
import com.alitalia.aem.web.component.managemybooking.delegate.ManageMyBookingDelegate;
import com.day.cq.commons.TidyJSONWriter;

/*import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.web.component.searchflights.delegate.SearchFlightsDelegate;
import com.day.cq.commons.TidyJSONWriter;*/

@Component(name = "TestMmbDelegateServlet", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
	@Property(name = "sling.servlet.selectors", value = {"testmmbdelegate"}),
	@Property(name = "sling.servlet.extensions", value = {"json"}),
	@Property(name = "sling.servlet.methods", value = {"GET"})
})
public class TestMmbDelegateServlet extends SlingSafeMethodsServlet {

	private static final String MACHINE_NAME = "CLIENT_REPLY";

	private static final String CLIENT_ID_MMB = "MMB";

	private static final long serialVersionUID = 1L;	
	
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(TestMmbDelegateServlet.class);
	
	@Reference(policy=ReferencePolicy.DYNAMIC)
	private volatile ManageMyBookingDelegate manageMyBookingDelegate;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		
		try {
			logger.info("Executing doGet.");

			response.setContentType("application/json");
			
			TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
			out.object();
			
//			RetrieveMmbFlightSeatMapRequest serviceRequest = new RetrieveMmbFlightSeatMapRequest("pippo", "pluto");
//
//			List<String> compartimentalClass = new ArrayList<String>();
//			compartimentalClass.add("C");
//			serviceRequest.setCompartimentalClass(compartimentalClass);
//
//			MmbFlightData flightInfo = new MmbFlightData();
//			flightInfo.setId(0);
//			flightInfo.setApisTypeRequired(MmbApisTypeEnum.NONE);
//
//			Calendar arrivalDateTime = Calendar.getInstance();
//			arrivalDateTime.set(2015, 12, 17, 10, 55, 0);
//			flightInfo.setArrivalDateTime(arrivalDateTime);
//
//			flightInfo.setBaggageAllow(2);
//			flightInfo.setCabin(CabinEnum.BUSINESS);
//			flightInfo.setCarrier("AZ");
//			flightInfo.setComfortSeatFare(new BigDecimal("60"));
//			flightInfo.setComfortSeatPaid(false);
//
//			Calendar departureDateTime = Calendar.getInstance();
//			departureDateTime.set(2015, 12, 17, 7, 25, 0);
//			flightInfo.setDepartureDateTime(departureDateTime);
//
//			flightInfo.setEnabledSeatMap(false);
//			flightInfo.setEticketClass("");
//			flightInfo.setEticket("0552322303339");
//			flightInfo.setExtraBaggage(3);
//			flightInfo.setFareClass("");
//			flightInfo.setFlightNumber("0201");
//			flightInfo.setFromCity("Londra");
//			flightInfo.setFromTerminal("4");
//
//			MmbAirportData from = new MmbAirportData();
//			from.setId(1);
//			from.setArea(AreaValueEnum.INTZ);
//			from.setCity("LON");
//			from.setCode("LHR");
//			from.setCountryCode("GB");
//			flightInfo.setFrom(from);
//
//			flightInfo.setHasComfortSeat(true);
//			flightInfo.setIndex(0);
//			flightInfo.setLegType(MmbLegTypeEnum.INZ);
//			flightInfo.setMultitratta(2);
//			flightInfo.setOperatingCarrier("AZ");
//			flightInfo.setOperatingFlightNumber("0201");
//
//			List<MmbPassengerData> passengers = new ArrayList<MmbPassengerData>();
//			flightInfo.setPassengers(passengers);
//
//			flightInfo.setPnr("JU2Z4Z");
//			flightInfo.setRph(2);
//			flightInfo.setRouteId(1);
//			flightInfo.setSeatClass(MmbCompartimentalClassEnum.UNKNOWN);
//			flightInfo.setStatus(MmbFlightStatusEnum.AVAILABLE);
//			flightInfo.setToCity("Roma");
//			flightInfo.setToTerminal("3");
//
//			MmbAirportData to = new MmbAirportData();
//			to.setId(1);
//			to.setArea(AreaValueEnum.DOM);
//			to.setCity("ROM");
//			to.setCode("FCO");
//			to.setCountryCode("IT");
//			flightInfo.setTo(to);
//
//			flightInfo.setType(RouteTypeEnum.RETURN);
//			flightInfo.setFirstFlight(true);
//			flightInfo.setMiniFare(false);
//
//			serviceRequest.setFlightInfo(flightInfo);
//			RetrieveMmbFlightSeatMapResponse serviceResponse = manageMyBookingDelegate.getFlightSeatMap(serviceRequest);
			
//			if (serviceResponse.getSeatMapMatrix() != null) {
//				String value = serviceResponse.getSeatMapMatrix().toString();
//				out.key("result").value(value.toUpperCase());
//			}
//			else
//				out.key("result").value("seatmap null!!!");

//			RetrieveProvincesRequest provincesRequest = new RetrieveProvincesRequest("prova", "province");
//			provincesRequest.setItemCache("false");
//			provincesRequest.setLanguageCode("IT");
//			provincesRequest.setMarket("IT");
//
//			RetrieveProvincesResponse provincesResponse = manageMyBookingDelegate.getProvinces(provincesRequest);
//			if (provincesResponse.getProvinces() != null && !provincesResponse.getProvinces().isEmpty()) {
//				out.key("result").value(provincesResponse.getProvinces().toString());
//			}
//			else
//				out.key("result").value("provinceslist null or empty!!!");
//
//			RetrieveCreditCardCountriesRequest creditCardCountriesRequest = new RetrieveCreditCardCountriesRequest("prova", "province");
//			creditCardCountriesRequest.setItemCache("false");
//			creditCardCountriesRequest.setLanguageCode("IT");
//			creditCardCountriesRequest.setMarket("IT");
//
//			RetrieveCreditCardCountriesResponse creditCardCountriesResponse = manageMyBookingDelegate.getCreditCardCountries(creditCardCountriesRequest);
//			if (creditCardCountriesResponse.getCountries() != null && !creditCardCountriesResponse.getCountries().isEmpty()) {
//				out.key("result").value(creditCardCountriesResponse.getCountries().toString());
//			}
//			else
//				out.key("result").value("countrieslist null or empty!!!");
//
//			RetrieveCountriesRequest countriesRequest = new RetrieveCountriesRequest("prova", "province");
//			countriesRequest.setItemCache("false");
//			countriesRequest.setLanguageCode("IT");
//			countriesRequest.setMarket("IT");
//
//			RetrieveCountriesResponse countriesResponse = manageMyBookingDelegate.getCountries(countriesRequest);
//			if (countriesResponse.getCountries() != null && !countriesResponse.getCountries().isEmpty()) {
//				out.key("result").value(countriesResponse.getCountries().toString());
//			}
//			else
//				out.key("result").value("countrieslist null or empty!!!");
//
//			RetrieveMealsRequest mealsRequest = new RetrieveMealsRequest("prova", "province");
//			mealsRequest.setItemCache("false");
//			mealsRequest.setLanguageCode("IT");
//			mealsRequest.setMarket("IT");
//
//			RetrieveMealsResponse mealsResponse = manageMyBookingDelegate.getMeals(mealsRequest);
//			if (mealsResponse.getMeals() != null && !mealsResponse.getMeals().isEmpty()) {
//				out.key("result").value(mealsResponse.getMeals().toString());
//			}
//			else
//				out.key("result").value("mealslist null or empty!!!");
//
//			RetrievePhonePrefixRequest phonePrefixRequest = new RetrievePhonePrefixRequest("prova", "province");
//			phonePrefixRequest.setItemCache("false");
//			phonePrefixRequest.setLanguageCode("IT");
//			phonePrefixRequest.setMarket("IT");
//
//			RetrievePhonePrefixResponse phonePrefixResponse = manageMyBookingDelegate.getPhonePrefix(phonePrefixRequest);
//			if (phonePrefixResponse.getPhonePrefix() != null && !phonePrefixResponse.getPhonePrefix().isEmpty()) {
//				out.key("result").value(phonePrefixResponse.getPhonePrefix().toString());
//			}
//			else
//				out.key("result").value("phoneprefixeslist null or empty!!!");
//
//			RetrievePaymentTypeItemRequest paymentTypeItemRequest = new RetrievePaymentTypeItemRequest();
//			paymentTypeItemRequest.setItemCache("false");
//			paymentTypeItemRequest.setLanguageCode("IT");
//			paymentTypeItemRequest.setMarket("IT");
//
//			RetrievePaymentTypeItemResponse paymentTypeItemResponse = manageMyBookingDelegate.getPaymentTypeItem(paymentTypeItemRequest);
//			if (paymentTypeItemResponse.getPaymentTypeItemsData() != null && !paymentTypeItemResponse.getPaymentTypeItemsData().isEmpty()) {
//				out.key("result").value(paymentTypeItemResponse.getPaymentTypeItemsData().toString());
//			}
//			else
//				out.key("result").value("phoneprefixeslist null or empty!!!");
//
//			RetrieveFrequentFlayerRequest frequentFlyerRequest = new RetrieveFrequentFlayerRequest("prova", "province");
//			frequentFlyerRequest.setItemCache("false");
//			frequentFlyerRequest.setLanguageCode("IT");
//			frequentFlyerRequest.setMarket("IT");
//
//			RetrieveMmbFrequentFlyerCarriersResponse mmbFrequentFlyerCarriersResponse = 
//					manageMyBookingDelegate.getFrequentFlyers(frequentFlyerRequest);
//			if (mmbFrequentFlyerCarriersResponse.getFrequentFlyers() != null && !mmbFrequentFlyerCarriersResponse.getFrequentFlyers().isEmpty()) {
//				out.key("result").value(mmbFrequentFlyerCarriersResponse.getFrequentFlyers().toString());
//			}
//			else
//				out.key("result").value("frequentflyerslist null or empty!!!");
//
//			RetrieveMmbAirportCheckinRequest mmbAirportCheckinRequest = new RetrieveMmbAirportCheckinRequest("prova", "province");
//			mmbAirportCheckinRequest.setItemCache("false");
//			mmbAirportCheckinRequest.setLanguageCode("IT");
//			mmbAirportCheckinRequest.setMarket("IT");
//
//			RetrieveMmbAirportCheckinResponse mmbAirportCheckinResponse = 
//					manageMyBookingDelegate.getAirportCheckIn(mmbAirportCheckinRequest);
//			if (mmbAirportCheckinResponse.getAirportCheckinList() != null && !mmbAirportCheckinResponse.getAirportCheckinList().isEmpty()) {
//				out.key("result").value(mmbAirportCheckinResponse.getAirportCheckinList().toString());
//			}
//			else
//				out.key("result").value("frequentflyerslist null or empty!!!");
//
//			RetrieveMmbAmericanStatesRequest mmbAmericanStatesRequest = new RetrieveMmbAmericanStatesRequest("prova", "province");
//			mmbAmericanStatesRequest.setItemCache("false");
//			mmbAmericanStatesRequest.setLanguageCode("IT");
//			mmbAmericanStatesRequest.setMarket("IT");
//
//			RetrieveMmbAmericanStatesResponse mmbAmericanStatesResponse = 
//					manageMyBookingDelegate.getAmericanStates(mmbAmericanStatesRequest);
//			if (mmbAmericanStatesResponse.getAmericanStatesList() != null && !mmbAmericanStatesResponse.getAmericanStatesList().isEmpty()) {
//				out.key("result").value(mmbAmericanStatesResponse.getAmericanStatesList().toString());
//			}
//			else
//				out.key("result").value("frequentflyerslist null or empty!!!");

			String pnr = request.getParameter("pnr");
			String lastname = request.getParameter("lastname");
			String name = request.getParameter("name");

//			UpdateMmbPnrContactsRequest updateContactsRequest = new UpdateMmbPnrContactsRequest();
//			updateContactsRequest.setPnr(pnr);
//			updateContactsRequest.setOldTelephone2("M/39/67899900000");
//			updateContactsRequest.setTelephone2("M/39/678999112233");
//			updateContactsRequest.setUpdateMailAction(MmbUpdateSSREnum.NONE);
//			updateContactsRequest.setUpdateTelephone2Action(MmbUpdateSSREnum.UPDATE);
//			updateContactsRequest.setUpdateTelephoneAction(MmbUpdateSSREnum.NONE);
//			UpdateMmbPnrContactsResponse updateContactsResponse = manageMyBookingDelegate.updatePnrContacts(updateContactsRequest);
//			if (updateContactsResponse != null &&
//					updateContactsResponse.getSuccessful()) {
//				out.key("result").value("aggiornamento riuscito");
//			}
//			else
//				out.key("result").value("RoutesList null or empty!!!");

			MmbRequestBaseInfoData baseInfo = new MmbRequestBaseInfoData();
			baseInfo.setClientIp("10.8.40.92");
			baseInfo.setSessionId("urytwongbwogqoug");
			baseInfo.setSiteCode("IT");

//			MmbCheckFrequentFlyerCodesRequest checkFrequentFlyerCodesRequest = 
//					new MmbCheckFrequentFlyerCodesRequest(0, "994259", "RICCARDO", "CAPITINI");
//			checkFrequentFlyerCodesRequest.setBaseInfo(baseInfo);
//			checkFrequentFlyerCodesRequest.setClient("MMB");
//			checkFrequentFlyerCodesRequest.setTid("TID");
//			checkFrequentFlyerCodesRequest.setSid(baseInfo.getSessionId());
//			MmbCheckFrequentFlyerCodesResponse checkFrequentFlyerCodesResponse = manageMyBookingDelegate.checkFrequentFlyerCodes(checkFrequentFlyerCodesRequest);
//			if (checkFrequentFlyerCodesResponse != null &&
//					checkFrequentFlyerCodesResponse.getValidFFCodes() != null &&
//					!checkFrequentFlyerCodesResponse.getValidFFCodes().isEmpty()) {
//				out.key("checkFrequentFlyerCodesResponse").value(checkFrequentFlyerCodesResponse.getValidFFCodes());
//			}
//			else
//				out.key("checkFrequentFlyerCodesResponse").value("validFFCodes is null or empty!!!!");

			RetriveMmbPnrInformationRequest retriveMmbPnrInformationRequest = new RetriveMmbPnrInformationRequest("TID", baseInfo.getSessionId());
			retriveMmbPnrInformationRequest.setBaseInfo(baseInfo);
			retriveMmbPnrInformationRequest.setClient(CLIENT_ID_MMB);
			retriveMmbPnrInformationRequest.setLastName(lastname);
			retriveMmbPnrInformationRequest.setLocale("IT_IT");
			retriveMmbPnrInformationRequest.setName(name);
			retriveMmbPnrInformationRequest.setTripId(pnr);

			RetriveMmbPnrInformationResponse retriveMmbPnrInformationResponse = manageMyBookingDelegate.retrivePnrInformation(retriveMmbPnrInformationRequest);
			if (retriveMmbPnrInformationResponse != null &&
					retriveMmbPnrInformationResponse.getRoutesList() != null &&
					!retriveMmbPnrInformationResponse.getRoutesList().isEmpty()) {
				out.key("retriveMmbPnrInformationResponse").value(retriveMmbPnrInformationResponse.getRoutesList().toString());
//
//				if (retriveMmbPnrInformationResponse.getRoutesList().get(0).getFlights() != null &&
//						!retriveMmbPnrInformationResponse.getRoutesList().get(0).getFlights().isEmpty()) {
//
//					RetrieveMmbAncillaryCatalogRequest ancillaryCatalogRequest = 
//							manageMyBookingDelegate.buildCatalogRequestFromMmbRouteData(baseInfo, CLIENT_ID_MMB, "IT", MACHINE_NAME, "IT", pnr, 
//									retriveMmbPnrInformationResponse.getRoutesList().get(0));
//					ancillaryCatalogRequest.setSid(baseInfo.getSessionId());
//					ancillaryCatalogRequest.setTid("TID");
//
//					RetrieveMmbAncillaryCatalogResponse ancillaryCatalogReseponse = manageMyBookingDelegate.getCatalog(ancillaryCatalogRequest);
//					if (ancillaryCatalogReseponse != null &&
//							ancillaryCatalogReseponse.getCart() != null) {
//						out.key("ancillaryCatalogReseponse").value(ancillaryCatalogReseponse.getCart().toString());
//
//						MmbAncillaryAddToCartRequest addToCartRequest = new MmbAncillaryAddToCartRequest();
//						addToCartRequest.setClient(CLIENT_ID_MMB);
//						addToCartRequest.setBaseInfo(baseInfo);
//						addToCartRequest.setCartId(ancillaryCatalogReseponse.getCart().getId());
//						addToCartRequest.setMachineName(MACHINE_NAME);
//						addToCartRequest.setSessionId(baseInfo.getSessionId());
//						List<MmbAncillaryCartItemData> itemsToAdd = new ArrayList<MmbAncillaryCartItemData>();
//						for (MmbAncillaryData ancillaryData : ancillaryCatalogReseponse.getCart().getAncillaries()) {
//							if (ancillaryData.getId() == 5) {
//								MmbAncillaryCartItemData cartItem = new MmbAncillaryCartItemData();
//								cartItem.setAncillaryId(ancillaryData.getId());
//								cartItem.setAncillaryDetail(ancillaryData.getAncillaryDetail());
//								cartItem.setQuantity(1);
//								itemsToAdd.add(cartItem);
//							}
//						}
//						addToCartRequest.setItemsToAdd(itemsToAdd);
//						MmbAncillaryAddToCartResponse addToCartResponse = manageMyBookingDelegate.addToCart(addToCartRequest);
//						if (addToCartResponse != null &&
//								addToCartResponse.getAncillaries() != null &&
//								!addToCartResponse.getAncillaries().isEmpty()) {
//							out.key("addToCartResponse").value(addToCartResponse.getAncillaries().toString());
//
//							MmbAncillaryCheckOutInitRequest checkOutInitRequest = new MmbAncillaryCheckOutInitRequest();
//							checkOutInitRequest.setClient(CLIENT_ID_MMB);
//							checkOutInitRequest.setMachineName(MACHINE_NAME);
//							checkOutInitRequest.setBaseInfo(baseInfo);
//							checkOutInitRequest.setSessionId(baseInfo.getSessionId());
//							checkOutInitRequest.setCartId(ancillaryCatalogReseponse.getCart().getId());
//
//							MmbAncillaryPaymentData ancillaryPayment = new MmbAncillaryPaymentData();
//							ancillaryPayment.setCardNumber("4444333322221111");
//							ancillaryPayment.setCreditCardType("VS");
//							ancillaryPayment.setCvv("123");
//							ancillaryPayment.setEmail("m.pantaleone@reply.it");
//							ancillaryPayment.setExpirationMonth(6);
//							ancillaryPayment.setExpirationYear(2018);
//							ancillaryPayment.setIpAddress("172.31.251.55");
//							ancillaryPayment.setOwnerLastName("AAABBBCCC");
//							ancillaryPayment.setOwnerName("DDDEEEFFF");
//							ancillaryPayment.setPaymentType(PaymentTypeEnum.CREDIT_CARD);
//							ancillaryPayment.setUserAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36");
//							
//							checkOutInitRequest.setPayment(ancillaryPayment);
//							MmbAncillaryCheckOutInitResponse checkOutInitResponse = manageMyBookingDelegate.checkOutInit(checkOutInitRequest);
//							if (checkOutInitResponse != null &&
//									checkOutInitResponse.getInitPayment() != null) {
//								out.key("checkOutInitResponse").value(checkOutInitResponse.getInitPayment().toString());
//							}
//							else
//								out.key("checkOutInitResponse").value("initpayment is null!!!");
//						}
//						else
//							out.key("addToCartResponse").value("ancillaries list null or empty!!!");
//					}
//					else
//						out.key("retriveMmbPnrInformationResponse").value("cart null or empty!!!");
//				}
			}
			else
				out.key("retriveMmbPnrInformationResponse").value("RoutesList null or empty!!!");

//			MmbPassengerData passengerToUpdate = retriveMmbPnrInformationResponse.getRoutesList().get(0)
//					.getFlights().get(0).getPassengers().get(0);
//			MmbApisInfoData apisInfoNew = new MmbApisInfoData(passengerToUpdate.getApisData());
//			apisInfoNew.setDestinationAddress("NUOVO INDIRIZZO");
//			apisInfoNew.setDestinationCity("NEW YORK");
//			apisInfoNew.setDestinationState("NJ");
//			apisInfoNew.setDestinationZipCode("90210");
//			Calendar passportExpirationDate = Calendar.getInstance();
//			passportExpirationDate.set(2018, 9, 3);
//			apisInfoNew.setPassportExpirationDate(passportExpirationDate);
//			apisInfoNew.setPassportNumber("987654321098");
//			apisInfoNew.setResidenceCountry("IT");
//			passengerToUpdate.setApisDataToUpdate(passengerToUpdate.getApisData());
//			passengerToUpdate.setApisData(apisInfoNew);
//			passengerToUpdate.setUpdate(MmbUpdateSSREnum.UPDATE);
//
//			MmbModifyPnrApisInfoRequest modifyPnrPassengerApisInfoRequest = new MmbModifyPnrApisInfoRequest();
//			modifyPnrPassengerApisInfoRequest.setClient("MMB");
//			modifyPnrPassengerApisInfoRequest.setBaseInfo(baseInfo);
//			modifyPnrPassengerApisInfoRequest.setPassenger(passengerToUpdate);
//			modifyPnrPassengerApisInfoRequest.setPnr(pnr);
//			modifyPnrPassengerApisInfoRequest.setSid(baseInfo.getSessionId());
//			modifyPnrPassengerApisInfoRequest.setTid("TID");
//
//			MmbModifyPnrApisInfoResponse modifyPnrPassengerApisInfoResponse = 
//					manageMyBookingDelegate.modifyPnrPassengerApisInfo(modifyPnrPassengerApisInfoRequest);
//			
//			if (modifyPnrPassengerApisInfoResponse != null &&
//					modifyPnrPassengerApisInfoResponse.getSuccessful())
//				out.key("result").value("aggiornamento riuscito");
//			else
//				out.key("result").value("RoutesList null or empty!!!");

			MmbInsuranceInfoRequest insuranceInfoRequest = new MmbInsuranceInfoRequest();
			insuranceInfoRequest.setArea(AreaValueEnum.INTC);
			insuranceInfoRequest.setArrivalAirport("LIN");
			Calendar arrivalDate = Calendar.getInstance();
			arrivalDate.set(2016, Calendar.JANUARY, 13, 8, 10, 0);
			insuranceInfoRequest.setArrivalDate(arrivalDate);
			insuranceInfoRequest.setCountryCode("IT");
			insuranceInfoRequest.setDepartureAirport("FCO");
			Calendar departureDate = Calendar.getInstance();
			departureDate.set(2016, Calendar.JANUARY, 13, 7, 0, 0);
			insuranceInfoRequest.setDepartureDate(departureDate);
			insuranceInfoRequest.setFlgUsaCa(false);
			insuranceInfoRequest.setLanguageCode("IT");
			insuranceInfoRequest.setPassengersCount(1);
			insuranceInfoRequest.setPnr("L6VIWA");
			insuranceInfoRequest.setRouteType(RouteTypeEnum.RETURN);

			InsuranceResponse insuranceResponse = manageMyBookingDelegate.getInsurance(insuranceInfoRequest);
			if (insuranceResponse != null &&
					insuranceResponse.getPolicy() != null) {
				out.key("result").value(insuranceResponse.getPolicy().toString());

				MmbPaymentData paymentData = new MmbPaymentData();
				paymentData.setAddress("address");
				paymentData.setCap("00100");
				paymentData.setCity("city");
				paymentData.setContactPhoneNumber("123456789");
				paymentData.setContactPrefix("003906");
				paymentData.setContactType(ContactTypeEnum.B);
				paymentData.setCreditCardNumber("4444333322221111");
				paymentData.setCreditCardType(CreditCardTypeEnum.VISA);
				paymentData.setCvc("123");
				paymentData.setEmail("r.capitini@reply.it");
				paymentData.setExpirationMonth("06");
				paymentData.setExpirationYear("2018");
				paymentData.setLastName("yifyuf");
				paymentData.setName("kjgkhgkj");
				paymentData.setSelectedCountry("IT");
				paymentData.setSelectedPaymentGroup(MmbPaymentGroupMaskEnum.CREDIT_CARDS);

				MmbPayInsuranceRequest payInsuranceRequest = new MmbPayInsuranceRequest();
				payInsuranceRequest.setInsurancePolicy(insuranceResponse.getPolicy());
				payInsuranceRequest.getInsurancePolicy().setBuy(true);
				payInsuranceRequest.getInsurancePolicy().setPolicyNumber(null);
				payInsuranceRequest.setPaymentData(paymentData);
				payInsuranceRequest.setRoute(retriveMmbPnrInformationResponse.getRoutesList().get(0));
				payInsuranceRequest.getRoute().setPnr("L6VIWA");
				InsuranceResponse payInsuranceResponse = manageMyBookingDelegate.payInsurance(payInsuranceRequest);
				if (payInsuranceResponse != null &&
						payInsuranceResponse.getPolicy() != null) 
					out.key("result").value(payInsuranceResponse.getPolicy().toString());
				else
					out.key("result").value("Payment Policy null or empty!!!");
			}
			else
				out.key("result").value("Policy null or empty!!!");

//			SapInvoiceRequest sapInvoiceRequest = new SapInvoiceRequest();
//			sapInvoiceRequest.setCap("00147");
//			sapInvoiceRequest.setCodiceFiscale("CPTRCR72B04F205U");
//			sapInvoiceRequest.setEmailIntestatario("R.CAPITINI@REPLY.IT");
//			sapInvoiceRequest.setEnteEmittente("");
//			sapInvoiceRequest.setEnteRichiedente("");
//			sapInvoiceRequest.setIndirizzoSpedizione("VIA DEL GIORGIONE 59");
//			sapInvoiceRequest.setIntestatarioFattura("PERSONA FISICA");
//			sapInvoiceRequest.setLocalitaSpedizione("ROMA");
//			sapInvoiceRequest.setNome("RICCARDO CAPITINI");
//			sapInvoiceRequest.setPaese("IT");
//			sapInvoiceRequest.setProvincia("RM");
//			sapInvoiceRequest.setSocieta("");
//			sapInvoiceRequest.setTipoRichiesta("");
//
//			TicketData biglietto = new TicketData();
//			biglietto.setDataVolo("2015-11-20");
//			biglietto.setNumeroBiglietto("0552323017766C1");
//			biglietto.setPnr("KTZ88V");
//			biglietto.setTratta("FCO\\JFK");
//			List<TicketData> biglietti = new ArrayList<TicketData>();
//			biglietti.add(biglietto);
//			sapInvoiceRequest.setBiglietti(biglietti);
//
//			SapInvoiceResponse sapInvoiceResponse = manageMyBookingDelegate.callSAPService(sapInvoiceRequest);
//			if (sapInvoiceResponse != null &&
//					sapInvoiceResponse.getResult() != null &&
//					!"".equals(sapInvoiceResponse.getResult())) 
//				out.key("sapInvoiceResponse").value(sapInvoiceResponse.getResult());
//			else
//				out.key("sapInvoiceResponse").value("Result null or empty!!!");

//			MmbSendEmailRequest emailRequest = new MmbSendEmailRequest();
//			emailRequest.setBaseInfo(baseInfo);
//			emailRequest.setSid(baseInfo.getSessionId());
//			emailRequest.setTid("TID");
//			emailRequest.setMailBody("<html><body><h1>PROVA!!!!</h1><p>Questa e' una mail di prova mandata dal SendMail di Manage My Booking.</p></body></html>");
//			emailRequest.setMailSubject("PROVA MAIL");
//			emailRequest.setPnr(pnr);
////			emailRequest.setRecipient("r.capitini@reply.it; ro.orlando@reply.it; m.pantaleone@reply.it; p.bizzarri@reply.it");
//			emailRequest.setRecipient("r.capitini@reply.it");
//			emailRequest.setHtmlBody(true);
//			emailRequest.setSender("noreply@alitalia.com");
//			MmbSendEmailResponse emailResponse = manageMyBookingDelegate.sendMail(emailRequest);
//			if (emailResponse != null &&
//					emailResponse.isMailSent()) {
//				out.key("emailResponse").value("mail sent");
//			}
//			else
//				out.key("emailResponse").value("Is null or empty!!!!");

			out.endObject();
		
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			logger.error("Exception while executing doGet.", e);
		}
		
		logger.info("Executed doGet."); 
	}
	
	
}
