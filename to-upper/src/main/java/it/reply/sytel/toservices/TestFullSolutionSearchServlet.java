package it.reply.sytel.toservices;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.social.commons.cors.CORSAuthenticationFilter;
import com.alitalia.aem.common.data.PassengerBase;
import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.data.home.ApplicantPassengerData;
import com.alitalia.aem.common.data.home.AvailableFlightsData;
import com.alitalia.aem.common.data.home.BookingSearchData;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.BrandSearchData;
import com.alitalia.aem.common.data.home.ChildPassengerData;
import com.alitalia.aem.common.data.home.ContactData;
import com.alitalia.aem.common.data.home.CrossSellingBaseData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.FullTextSearchData;
import com.alitalia.aem.common.data.home.InfantPassengerData;
import com.alitalia.aem.common.data.home.InfoCarnetData;
import com.alitalia.aem.common.data.home.PassengerBaseInfoData;
import com.alitalia.aem.common.data.home.PassengerNumbersData;
import com.alitalia.aem.common.data.home.PaymentComunicationCreditCardData;
import com.alitalia.aem.common.data.home.PaymentData;
import com.alitalia.aem.common.data.home.PaymentProcessInfoData;
import com.alitalia.aem.common.data.home.PaymentProviderCreditCardData;
import com.alitalia.aem.common.data.home.PhonePrefixData;
import com.alitalia.aem.common.data.home.PreferencesData;
import com.alitalia.aem.common.data.home.PropertiesData;
import com.alitalia.aem.common.data.home.RefreshSearchData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.common.data.home.SearchDestinationData;
import com.alitalia.aem.common.data.home.SeatPreferencesData;
import com.alitalia.aem.common.data.home.TaxData;
import com.alitalia.aem.common.data.home.UserInfoStandardData;
import com.alitalia.aem.common.data.home.enumerations.ARTaxInfoTypesEnum;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.ContactTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.CreditCardTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.GatewayTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.GenderTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PaymentStepEnum;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RefreshTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.ResultTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.SearchTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.TimeTypeEnum;
import com.alitalia.aem.common.messages.home.AuthorizePaymentRequest;
import com.alitalia.aem.common.messages.home.AuthorizePaymentResponse;
import com.alitalia.aem.common.messages.home.CheckPaymentRequest;
import com.alitalia.aem.common.messages.home.CheckPaymentResponse;
import com.alitalia.aem.common.messages.home.CrossSellingsRequest;
import com.alitalia.aem.common.messages.home.CrossSellingsResponse;
import com.alitalia.aem.common.messages.home.InfoPassengerCreatePNRRequest;
import com.alitalia.aem.common.messages.home.InfoPassengerCreatePNRResponse;
import com.alitalia.aem.common.messages.home.InitializePaymentRequest;
import com.alitalia.aem.common.messages.home.InitializePaymentResponse;
import com.alitalia.aem.common.messages.home.InsuranceRequest;
import com.alitalia.aem.common.messages.home.InsuranceResponse;
import com.alitalia.aem.common.messages.home.RetrieveTicketRequest;
import com.alitalia.aem.common.messages.home.RetrieveTicketResponse;
import com.alitalia.aem.common.messages.home.SearchBookingSolutionResponse;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionResponse;
import com.alitalia.aem.common.messages.home.enumeration.SearchExecuteResponseType;
import com.alitalia.aem.web.component.bookingancillary.delegate.BookingAncillaryDelegate;
import com.alitalia.aem.web.component.bookingticket.delegate.BookingTicketDelegate;
import com.alitalia.aem.web.component.infopassenger.delegate.InfoPassengerDelegate;
import com.alitalia.aem.web.component.managemybooking.delegate.ManageMyBookingDelegate;
import com.alitalia.aem.web.component.payment.delegate.PaymentDelegate;
import com.alitalia.aem.web.component.searchflights.delegate.SearchFlightsDelegate;
import com.alitalia.aem.ws.booking.searchservice.xsd2.FullTextRulesSearch;
import com.day.cq.commons.TidyJSONWriter;

@Component(name = "TestFullSolutionSearchServlet", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
	@Property(name = "sling.servlet.selectors", value = {"testfullsolution"}),
	@Property(name = "sling.servlet.extensions", value = {"json"}),
	@Property(name = "sling.servlet.methods", value = {"GET"})
})
public class TestFullSolutionSearchServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;	
	private static final Logger logger = LoggerFactory.getLogger(TestFullSolutionSearchServlet.class);
	

	@Reference(policy=ReferencePolicy.DYNAMIC)
	private volatile SearchFlightsDelegate searchFlightsDelegate;

	@Reference(policy=ReferencePolicy.DYNAMIC)
	private volatile InfoPassengerDelegate infoPassengerDelegate;
	
	@Reference(policy=ReferencePolicy.DYNAMIC)
	private volatile BookingAncillaryDelegate ancillaryDelegate;

	@Reference(policy=ReferencePolicy.DYNAMIC)
	private volatile PaymentDelegate paymentDelegate;

	@Reference(policy=ReferencePolicy.DYNAMIC)
	private volatile BookingTicketDelegate ticketDelegate;

	@Reference(policy=ReferencePolicy.DYNAMIC)
	private volatile ManageMyBookingDelegate manageMyBookingDelegate;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		
		try {
			logger.info("Executing doGet.");
			
			response.setContentType("application/json");
			
			String valuePartenza = request.getParameter("partenza");
			String valueArrivo = request.getParameter("arrivo");
			String valueDataPartenza = 
					((request.getParameter("datapartenza") != null) &&
							! "".equals(request.getParameter("datapartenza"))) ? request.getParameter("datapartenza") : "11/12/2015";
			String valueDataRitorno = 
					((request.getParameter("dataritorno") != null) &&
							! "".equals(request.getParameter("dataritorno"))) ? request.getParameter("dataritorno") : "25/12/2015";

			AirportData fromAirport = new AirportData();
			fromAirport.setCityCode(valuePartenza); //decommentare
			fromAirport.setCode(valuePartenza);		//decommentare
			fromAirport.setCountryCode("IT");

			AirportData toAirport = new AirportData();
			toAirport.setCityCode(valueArrivo); //decommentare
			toAirport.setCode(valueArrivo);     //decommentare
			toAirport.setCountryCode("IT");

			SearchDestinationData destination1 = new SearchDestinationData();
			destination1.setFromAirport(fromAirport);
			destination1.setToAirport(toAirport);
			destination1.setRouteType(RouteTypeEnum.OUTBOUND);
			destination1.setTimeType(TimeTypeEnum.ANYTIME);
			Calendar flightDepartureDate = Calendar.getInstance();
			flightDepartureDate.setTime(new SimpleDateFormat("dd/MM/yyyy").parse(valueDataPartenza));
			destination1.setDepartureDate(flightDepartureDate);

			SearchDestinationData destination2 = new SearchDestinationData();
			destination2.setFromAirport(toAirport);
			destination2.setToAirport(fromAirport);
			destination2.setRouteType(RouteTypeEnum.RETURN);
			destination2.setTimeType(TimeTypeEnum.ANYTIME);
			Calendar flightArrivalDate = Calendar.getInstance();
			flightArrivalDate.setTime(new SimpleDateFormat("dd/MM/yyyy").parse(valueDataRitorno));
			destination2.setDepartureDate(flightArrivalDate);

			PassengerNumbersData passengerNumbers = new PassengerNumbersData();
			passengerNumbers.setNumber(1);
			passengerNumbers.setPassengerType(PassengerTypeEnum.ADULT);

			BrandSearchData brandSearchData = new BrandSearchData();
			brandSearchData.setCug("ADT");
			brandSearchData.setOnlyDirectFlight(false);
			brandSearchData.setType(SearchTypeEnum.BRAND_SEARCH);
			brandSearchData.addDestination(destination1);
			brandSearchData.addDestination(destination2);
			brandSearchData.addPassengerNumbers(passengerNumbers);
			brandSearchData.setMarket("IT");
			
//			brandSearchData.setSearchCabin(CabinEnum.ECONOMY);
//			brandSearchData.setSearchCabinType(CabinTypeEnum.PERMITTED);

			SearchFlightSolutionRequest searchRequest = new SearchFlightSolutionRequest("TID", "SID");
			searchRequest.setFilter(brandSearchData);
			searchRequest.setLanguageCode("IT");
			searchRequest.setMarket("IT");
			searchRequest.setResponseType(SearchExecuteResponseType.MODEL);
			searchRequest.setMarketExtraCharge("2B");
			searchRequest.setCug("ADT");
			searchRequest.setFromSearchElement(valuePartenza);
			searchRequest.setToSearchElement(valueArrivo);
			searchRequest.setRibbonOutboundStartDate(flightDepartureDate);
			searchRequest.setRibbonReturnStartDate(flightArrivalDate);

			SearchFlightSolutionResponse searchResponse = searchFlightsDelegate.searchInitialSolutions(searchRequest);
			AvailableFlightsData availableFlights = null;
			if (searchResponse != null && searchResponse.getAvailableFlights() != null) {
				availableFlights = searchResponse.getAvailableFlights();

				String valueSessionId = "";
				String valueSolutionSet = "";
				String valueSolutionId = "";
				RouteTypeEnum direction = null;
				BigDecimal originalFare = null;
				BigDecimal originalPrice = new BigDecimal("0");
				String outboundFlight = "";
				String returnFlight = "";
				CabinEnum outboundCabin = null;
				CabinEnum returnCabin = null;
				Boolean solutionFound = false;
				valueSessionId = availableFlights.getSessionId();
				valueSolutionSet = availableFlights.getSolutionSet();
				for (RouteData route : availableFlights.getRoutes()) {
					direction = route.getType();
					for (FlightData flight : route.getFlights()) {
						if (flight instanceof DirectFlightData) {
							for (BrandData brand : ((DirectFlightData) flight).getBrands()) {
								if (!brand.isEnabled())
									continue;
								valueSolutionId = brand.getSolutionId();
								originalFare = brand.getNetFare();
								originalPrice.add(brand.getGrossFare());
								outboundFlight = ((DirectFlightData) flight).getFlightNumber();
								outboundCabin = ((DirectFlightData) flight).getCabin();
								solutionFound = true;
								break;
							}
						}
						if (solutionFound)
							break;
					}
					if (solutionFound)
						break;
				}

				if (solutionFound) {
					RefreshSearchData refreshSearchData = new RefreshSearchData();
					refreshSearchData.setSessionId(valueSessionId);
					refreshSearchData.setSolutionSet(valueSolutionSet);
					refreshSearchData.setSolutionId(valueSolutionId);
					refreshSearchData.setCurrency("EUR");
					refreshSearchData.setOriginalFare(originalFare);
					refreshSearchData.setOriginalPrice(originalPrice);
					if (direction.equals(RouteTypeEnum.OUTBOUND))
						refreshSearchData.setSliceType(RouteTypeEnum.RETURN);
					else
						refreshSearchData.setSliceType(RouteTypeEnum.OUTBOUND);
					refreshSearchData
							.setRefreshType(RefreshTypeEnum.REFRESH_SEARCH);
					SearchFlightSolutionRequest searchRefreshRequest = new SearchFlightSolutionRequest(
							"TID", "SID");
					searchRefreshRequest.setFilter(refreshSearchData);
					searchRefreshRequest.setLanguageCode("IT");
					searchRefreshRequest.setMarket("IT");
					searchRefreshRequest.setResponseType(SearchExecuteResponseType.MODEL);
					searchRefreshRequest.setCug("ADT");
					searchRefreshRequest.setFromSearchElement(valuePartenza);
					searchRefreshRequest.setToSearchElement(valueArrivo);
					SearchFlightSolutionResponse searchRefreshResponse = searchFlightsDelegate
							.searchRefreshSolutions(searchRefreshRequest);
					AvailableFlightsData refreshAvailableFlights = null;
					if (searchRefreshResponse != null
							&& searchRefreshResponse.getAvailableFlights() != null) {
						refreshAvailableFlights = searchRefreshResponse
								.getAvailableFlights();

						String otherSolutionId = "";
						solutionFound = false;

						for (RouteData route : refreshAvailableFlights
								.getRoutes()) {
							if (!route.getType().equals(direction)) {
								for (FlightData flight : route.getFlights()) {
									if (flight instanceof DirectFlightData) {
										for (BrandData brand : ((DirectFlightData) flight)
												.getBrands()) {
											if (brand.getRefreshSolutionId() == null || "".equals(brand.getRefreshSolutionId()))
												continue;
											otherSolutionId = brand.getRefreshSolutionId();
											returnFlight = ((DirectFlightData) flight).getFlightNumber();
											returnCabin = ((DirectFlightData) flight).getCabin();
											solutionFound = true;
											break;
										}
									}
									if (solutionFound)
										break;
								}
							}
							if (solutionFound)
								break;
						}

						if (solutionFound) {
							ApplicantPassengerData applicantPassengerData = new ApplicantPassengerData();
							applicantPassengerData.setFee(new BigDecimal(0));
							applicantPassengerData
									.setCouponPrice(new BigDecimal(0));
							applicantPassengerData
									.setExtraCharge(new BigDecimal(0));
							applicantPassengerData.setGrossFare(new BigDecimal(
									0));
							applicantPassengerData
									.setNetFare(new BigDecimal(0));
							applicantPassengerData
									.setType(PassengerTypeEnum.ADULT);
							applicantPassengerData
									.setArTaxInfoType(ARTaxInfoTypesEnum.NONE);
							applicantPassengerData.setBlueBizCode("");
							applicantPassengerData.setSkyBonusCode("");
							List<PassengerBase> passengers = new ArrayList<PassengerBase>();
							passengers.add(applicantPassengerData);
							BookingSearchData bookingSellupSearchData = new BookingSearchData();
							bookingSellupSearchData
									.setSessionId(valueSessionId);
							bookingSellupSearchData
									.setSolutionSet(valueSolutionSet);
							bookingSellupSearchData
									.setSolutionId(otherSolutionId);
							bookingSellupSearchData
									.setBookingSolutionId(otherSolutionId);
							bookingSellupSearchData.setPassengers(passengers);
							bookingSellupSearchData.setMultiSlice(false);
							bookingSellupSearchData.setNeutral(false);
							bookingSellupSearchData
									.setType(GatewayTypeEnum.BOOKING_SELLUP);
							SearchFlightSolutionRequest bookingSellupRequest = new SearchFlightSolutionRequest(
									"TID", "SID");
							bookingSellupRequest
									.setFilter(bookingSellupSearchData);
							bookingSellupRequest.setLanguageCode("IT");
							bookingSellupRequest.setMarket("IT");
							bookingSellupRequest
									.setResponseType(SearchExecuteResponseType.MODEL);
							SearchBookingSolutionResponse bookingSellupResponse = searchFlightsDelegate
									.bookingSellup(bookingSellupRequest);
							RoutesData routes = null;
							if (bookingSellupResponse != null) {
								logger.info("Ha funzionato: "
										+ searchResponse.toString());
								routes = bookingSellupResponse.getRoutesData();
							}
							if (routes != null) {
								BookingSearchData bookingTaxSearchData = new BookingSearchData();
								bookingTaxSearchData.setId(routes.getId());
								bookingTaxSearchData
										.setSessionId(valueSessionId);
								bookingTaxSearchData
										.setSolutionSet(valueSolutionSet);
								bookingTaxSearchData
										.setSolutionId(otherSolutionId);
								bookingTaxSearchData
										.setBookingSolutionId(otherSolutionId);
								@SuppressWarnings("unchecked")
								List<PassengerBase> routesPassengersList = (List<PassengerBase>) (List<?>) routes
										.getPassengers();
								bookingTaxSearchData
										.setPassengers(routesPassengersList);
								bookingTaxSearchData.setMultiSlice(false);
								bookingTaxSearchData.setNeutral(false);
								bookingTaxSearchData
										.setType(GatewayTypeEnum.BOOKING_TAX_SEARCH);

								SearchFlightSolutionRequest bookingTaxSearchRequest = new SearchFlightSolutionRequest(
										"TID", "SID");
								bookingTaxSearchRequest
										.setFilter(bookingTaxSearchData);
								bookingTaxSearchRequest.setLanguageCode("IT");
								bookingTaxSearchRequest.setMarket("IT");
								bookingTaxSearchRequest
										.setResponseType(SearchExecuteResponseType.MODEL);

								SearchFlightSolutionResponse bookingTaxSearchResponse = searchFlightsDelegate
										.bookingTaxSearch(bookingTaxSearchRequest);

								AvailableFlightsData taxSearchAvailableFlights = null;
								if (bookingTaxSearchResponse != null
										&& bookingTaxSearchResponse
												.getAvailableFlights() != null) {
									taxSearchAvailableFlights = bookingTaxSearchResponse
											.getAvailableFlights();

									TidyJSONWriter out = new TidyJSONWriter(
											response.getWriter());
									out.object();
									if (taxSearchAvailableFlights.getTaxes() != null) {
										out.key("solutionset").value(
												taxSearchAvailableFlights
														.getSolutionSet());
										out.key("sessionid").value(
												taxSearchAvailableFlights
														.getSessionId());
										out.key("taxes").value(
												taxSearchAvailableFlights
														.getTaxes());
										double totalFare = 0;
										double totalPrice = 0;
										for (TaxData taxData : taxSearchAvailableFlights.getTaxes()) {
											totalPrice = totalPrice + taxData.getAmount().doubleValue();
											if (!taxData.getCode().startsWith("yq") && 
													!taxData.getCode().startsWith("tax"))
												totalFare = totalFare + taxData.getAmount().doubleValue();
										}
										originalFare = new BigDecimal(Double.toString(totalFare));
										originalPrice = new BigDecimal(Double.toString(totalPrice));
									} else
										out.key("result").value(
												"taxes is empty!!!!");

									out.endObject();
								}

//								FullTextSearchData fullTextSearchData = new FullTextSearchData();
//								fullTextSearchData.setSessionId(valueSessionId);
//								fullTextSearchData.setSolutionSet(valueSolutionSet);
//								fullTextSearchData.setSolutionId(valueSolutionId);
//								SearchFlightSolutionRequest searchFullTextRequest = new SearchFlightSolutionRequest(
//										"TID", "SID");
//								searchFullTextRequest.setFilter(fullTextSearchData);
//								searchFullTextRequest.setLanguageCode("EN");
//								searchFullTextRequest.setMarket("US");
//								searchFullTextRequest.setResponseType(SearchExecuteResponseType.MODEL);
//								SearchFlightSolutionResponse searchFullTextResponse = 
//										searchFlightsDelegate.executeFullTextRulesSearch(searchFullTextRequest);
//								if (searchFullTextResponse.getAvailableFlights() != null &&
//										searchFullTextResponse.getAvailableFlights().getFullTextRules() != null &&
//										!searchFullTextResponse.getAvailableFlights().getFullTextRules().isEmpty()) {
//									TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
//									out.object();
//									for (String fareRule : searchFullTextResponse.getAvailableFlights().getFullTextRules()) 
//										out.key("fareRule").value(fareRule);
//									out.endObject();
//								}

								InsuranceRequest insuranceRequest = new InsuranceRequest();
								insuranceRequest.setArea(((DirectFlightData)routes.getRoutesList().get(0).getFlights().get(0)).getFrom().getArea());
								insuranceRequest.setArrivalAirport(((DirectFlightData)routes.getRoutesList().get(0).getFlights().get(0)).getTo().getCode());
								insuranceRequest.setArrivalDate(((DirectFlightData)routes.getRoutesList().get(0).getFlights().get(0)).getArrivalDate());
								insuranceRequest.setCountryCode("IT");
								insuranceRequest.setDepartureAirport(((DirectFlightData)routes.getRoutesList().get(0).getFlights().get(0)).getFrom().getCode());
								insuranceRequest.setDepartureDate(((DirectFlightData)routes.getRoutesList().get(0).getFlights().get(0)).getDepartureDate());
								insuranceRequest.setFlgUsaCa(false);
								insuranceRequest.setLanguageCode("IT");
								insuranceRequest.setPassengers(passengers);
								insuranceRequest.setRouteType(RouteTypeEnum.OUTBOUND);

								InsuranceResponse insuranceResponse = ancillaryDelegate.getInsurance(insuranceRequest);
								if (insuranceResponse != null && insuranceResponse.getPolicy() != null) {
									TidyJSONWriter out = new TidyJSONWriter(
											response.getWriter());
									out.object();
									out.key("insurance").value(insuranceResponse.getPolicy().toString());
									out.endObject();
								}

								InfoPassengerCreatePNRRequest createPNRRequest = new InfoPassengerCreatePNRRequest(
										"TID", "SID");

								for (PassengerBase pax : routes.getPassengers()) {
									if (pax instanceof ApplicantPassengerData) {
										((ApplicantPassengerData) pax)
												.setInfo(new PassengerBaseInfoData());
										((ApplicantPassengerData) pax)
												.getInfo().setBirthDate(
														GregorianCalendar
																.getInstance());
										((ApplicantPassengerData) pax)
												.getInfo().getBirthDate()
												.setTime(new Date());
										((ApplicantPassengerData) pax)
												.getInfo().setGender(
														GenderTypeEnum.UNKNOWN);
										((ApplicantPassengerData) pax)
												.setLastName("test");
										((ApplicantPassengerData) pax)
												.setName("Max");
										((ApplicantPassengerData) pax)
												.setPreferences(new PreferencesData());
										((ApplicantPassengerData) pax)
												.getPreferences().setMealType(
														null);
										((ApplicantPassengerData) pax)
												.getPreferences()
												.setSeatPreferences(
														new ArrayList<SeatPreferencesData>());
										((ApplicantPassengerData) pax)
												.getPreferences()
												.getSeatPreferences()
												.add(new SeatPreferencesData());
										((ApplicantPassengerData) pax)
												.getPreferences()
												.getSeatPreferences()
												.get(0)
												.setCabinSeat(outboundCabin);
										((ApplicantPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(0)
												.setFlightCarrier("AZ");
										((ApplicantPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(0)
												.setFlightNumber(outboundFlight);
										((ApplicantPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(0)
												.setNumber("C");
										((ApplicantPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(0)
												.setRow("8");
										((ApplicantPassengerData) pax)
												.getPreferences()
												.getSeatPreferences()
												.add(new SeatPreferencesData());
										((ApplicantPassengerData) pax)
												.getPreferences()
												.getSeatPreferences()
												.get(1)
												.setCabinSeat(returnCabin);
										((ApplicantPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(1)
												.setFlightCarrier("AZ");
										((ApplicantPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(1)
												.setFlightNumber(returnFlight);
										((ApplicantPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(1)
												.setNumber("A");
										((ApplicantPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(1)
												.setRow("9");
										((ApplicantPassengerData) pax)
												.getPreferences().setSeatType(
														null);
										((ApplicantPassengerData) pax)
												.setFrequentFlyerCode("");
										((ApplicantPassengerData) pax)
												.setFrequentFlyerTier(null);
										((ApplicantPassengerData) pax)
												.setFrequentFlyerType(null);
										((ApplicantPassengerData) pax)
												.setBlueBizCode(null);
										((ApplicantPassengerData) pax)
												.setCuit(null);
										((ApplicantPassengerData) pax)
												.setContact(new ArrayList<ContactData>());
										((ApplicantPassengerData) pax)
												.getContact().add(
														new ContactData());
										((ApplicantPassengerData) pax)
												.getContact()
												.get(0)
												.setContactType(
														ContactTypeEnum.M);
										((ApplicantPassengerData) pax)
												.getContact().get(0)
												.setPhoneNumber("32131212");
										((ApplicantPassengerData) pax)
												.getContact()
												.get(0)
												.setPrefix(
														new PhonePrefixData());
										((ApplicantPassengerData) pax)
												.getContact().get(0)
												.getPrefix().setCode("IT");
										((ApplicantPassengerData) pax)
												.getContact().get(0)
												.getPrefix()
												.setDescription("Italy");
										((ApplicantPassengerData) pax)
												.getContact().get(0)
												.getPrefix().setPrefix("39");
										((ApplicantPassengerData) pax)
												.getContact().add(
														new ContactData());
										((ApplicantPassengerData) pax)
												.getContact()
												.get(1)
												.setContactType(
														ContactTypeEnum.M);
										((ApplicantPassengerData) pax)
												.getContact()
												.get(1)
												.setPhoneNumber(
														"r.capitini@reply.it");
										((ApplicantPassengerData) pax)
												.getContact()
												.get(1)
												.setPrefix(
														new PhonePrefixData());
										((ApplicantPassengerData) pax)
												.getContact().get(1)
												.getPrefix().setCode("");
										((ApplicantPassengerData) pax)
												.getContact().get(1)
												.getPrefix()
												.setDescription("Email");
										((ApplicantPassengerData) pax)
												.getContact().get(1)
												.getPrefix().setPrefix("");
										((ApplicantPassengerData) pax)
												.setCountry(null);
										((ApplicantPassengerData) pax)
												.setEmail("r.capitini@reply.it");
										((ApplicantPassengerData) pax)
												.setSkyBonusCode("");
										((ApplicantPassengerData) pax)
												.setSubscribeToNewsletter(Boolean.TRUE);
									} else if (pax instanceof AdultPassengerData) {
										((AdultPassengerData) pax)
												.setInfo(new PassengerBaseInfoData());
										((AdultPassengerData) pax).getInfo()
												.setBirthDate(
														GregorianCalendar
																.getInstance());
										((AdultPassengerData) pax).getInfo()
												.getBirthDate()
												.setTime(new Date());
										((AdultPassengerData) pax).getInfo()
												.setGender(
														GenderTypeEnum.UNKNOWN);
										((AdultPassengerData) pax)
												.setLastName("test");
										((AdultPassengerData) pax)
												.setName("Max");
										((AdultPassengerData) pax)
												.setPreferences(new PreferencesData());
										((AdultPassengerData) pax)
												.getPreferences().setMealType(
														null);
										((AdultPassengerData) pax)
												.getPreferences()
												.setSeatPreferences(
														new ArrayList<SeatPreferencesData>());
										((AdultPassengerData) pax)
												.getPreferences()
												.getSeatPreferences()
												.add(new SeatPreferencesData());
										((AdultPassengerData) pax)
												.getPreferences()
												.getSeatPreferences()
												.get(0)
												.setCabinSeat(CabinEnum.ECONOMY);
										((AdultPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(0)
												.setFlightCarrier("AZ");
										((AdultPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(0)
												.setFlightNumber("1779");
										((AdultPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(0)
												.setNumber("C");
										((AdultPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(0)
												.setRow("8");
										((AdultPassengerData) pax)
												.getPreferences()
												.getSeatPreferences()
												.add(new SeatPreferencesData());
										((AdultPassengerData) pax)
												.getPreferences()
												.getSeatPreferences()
												.get(1)
												.setCabinSeat(CabinEnum.ECONOMY);
										((AdultPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(1)
												.setFlightCarrier("AZ");
										((AdultPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(1)
												.setFlightNumber("1792");
										((AdultPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(1)
												.setNumber("A");
										((AdultPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(1)
												.setRow("9");
										((AdultPassengerData) pax)
												.getPreferences().setSeatType(
														null);
										((AdultPassengerData) pax)
												.setFrequentFlyerCode("");
										((AdultPassengerData) pax)
												.setFrequentFlyerTier(null);
										((AdultPassengerData) pax)
												.setFrequentFlyerType(null);
									} else if (pax instanceof ChildPassengerData) {
										((ChildPassengerData) pax)
												.setInfo(new PassengerBaseInfoData());
										((ChildPassengerData) pax).getInfo()
												.setBirthDate(
														GregorianCalendar
																.getInstance());
										((ChildPassengerData) pax).getInfo()
												.getBirthDate()
												.setTime(new Date());
										((ChildPassengerData) pax).getInfo()
												.setGender(
														GenderTypeEnum.UNKNOWN);
										((ChildPassengerData) pax)
												.setLastName("test");
										((ChildPassengerData) pax)
												.setName("Max");
										((ChildPassengerData) pax)
												.setPreferences(new PreferencesData());
										((ChildPassengerData) pax)
												.getPreferences().setMealType(
														null);
										((ChildPassengerData) pax)
												.getPreferences()
												.setSeatPreferences(
														new ArrayList<SeatPreferencesData>());
										((ChildPassengerData) pax)
												.getPreferences()
												.getSeatPreferences()
												.add(new SeatPreferencesData());
										((ChildPassengerData) pax)
												.getPreferences()
												.getSeatPreferences()
												.get(0)
												.setCabinSeat(CabinEnum.ECONOMY);
										((ChildPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(0)
												.setFlightCarrier("AZ");
										((ChildPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(0)
												.setFlightNumber("1779");
										((ChildPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(0)
												.setNumber("C");
										((ChildPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(0)
												.setRow("8");
										((ChildPassengerData) pax)
												.getPreferences()
												.getSeatPreferences()
												.add(new SeatPreferencesData());
										((ChildPassengerData) pax)
												.getPreferences()
												.getSeatPreferences()
												.get(1)
												.setCabinSeat(CabinEnum.ECONOMY);
										((ChildPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(1)
												.setFlightCarrier("AZ");
										((ChildPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(1)
												.setFlightNumber("1792");
										((ChildPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(1)
												.setNumber("A");
										((ChildPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(1)
												.setRow("9");
										((ChildPassengerData) pax)
												.getPreferences().setSeatType(
														null);
									} else if (pax instanceof InfantPassengerData) {
										((InfantPassengerData) pax)
												.setInfo(new PassengerBaseInfoData());
										((InfantPassengerData) pax).getInfo()
												.setBirthDate(
														GregorianCalendar
																.getInstance());
										((InfantPassengerData) pax).getInfo()
												.getBirthDate()
												.setTime(new Date());
										((InfantPassengerData) pax).getInfo()
												.setGender(
														GenderTypeEnum.UNKNOWN);
										((InfantPassengerData) pax)
												.setLastName("test");
										((InfantPassengerData) pax)
												.setName("Max");
										((InfantPassengerData) pax)
												.setPreferences(new PreferencesData());
										((InfantPassengerData) pax)
												.getPreferences().setMealType(
														null);
										((InfantPassengerData) pax)
												.getPreferences()
												.setSeatPreferences(
														new ArrayList<SeatPreferencesData>());
										((InfantPassengerData) pax)
												.getPreferences()
												.getSeatPreferences()
												.add(new SeatPreferencesData());
										((InfantPassengerData) pax)
												.getPreferences()
												.getSeatPreferences()
												.get(0)
												.setCabinSeat(CabinEnum.ECONOMY);
										((InfantPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(0)
												.setFlightCarrier("AZ");
										((InfantPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(0)
												.setFlightNumber("1779");
										((InfantPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(0)
												.setNumber("C");
										((InfantPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(0)
												.setRow("8");
										((InfantPassengerData) pax)
												.getPreferences()
												.getSeatPreferences()
												.add(new SeatPreferencesData());
										((InfantPassengerData) pax)
												.getPreferences()
												.getSeatPreferences()
												.get(1)
												.setCabinSeat(CabinEnum.ECONOMY);
										((InfantPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(1)
												.setFlightCarrier("AZ");
										((InfantPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(1)
												.setFlightNumber("1792");
										((InfantPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(1)
												.setNumber("A");
										((InfantPassengerData) pax)
												.getPreferences()
												.getSeatPreferences().get(1)
												.setRow("9");
										((InfantPassengerData) pax)
												.getPreferences().setSeatType(
														null);
										((InfantPassengerData) pax)
												.setAdultRefent(0);
									}
								}
								routes.setFarmId(null);
								routes.addProperty("WebSession", "s3zfbyf2anap1wykwtmdtr54");
								routes.setNationalInsuranceNumber("RBNMSM74M24E472K");
								routes.setSliceCount(routes.getRoutesList()
										.size());
								if (insuranceResponse.getPolicy() != null) {
									routes.setInsurance(insuranceResponse.getPolicy());
//									routes.getInsurance().setBuy(true);
									routes.getInsurance().setBuy(false);
								}
								createPNRRequest.setPrenotation(routes);
								createPNRRequest.setMarketCode("IT");
								createPNRRequest.setCarnetCode("");
								InfoPassengerCreatePNRResponse createPNRResponse = infoPassengerDelegate
										.createPNRRequest(createPNRRequest);
								RoutesData prenotation = createPNRResponse.getPrenotation();

								if (prenotation != null) {
									TidyJSONWriter out = new TidyJSONWriter(
											response.getWriter());
									out.object();
									out.key("prenotation PNR ***").value(prenotation.toString());
									out.endObject();

//									//GET CROSS SELLING
//									CrossSellingsRequest crossSellingsRequest = new CrossSellingsRequest();
//									crossSellingsRequest.setIpAddress("10.8.50.15");
//									crossSellingsRequest.setLanguageCode("IT");
//									crossSellingsRequest.setMarketCode("IT");
//									crossSellingsRequest.setPrenotation(prenotation);
//									CrossSellingsResponse crossSellingsResponse = ticketDelegate.getCrossSellings(crossSellingsRequest);
//									if (crossSellingsResponse != null &&
//											crossSellingsResponse.getCrossSellingsData() != null &&
//											crossSellingsResponse.getCrossSellingsData().getItems() != null &&
//											!crossSellingsResponse.getCrossSellingsData().getItems().isEmpty()) {
//										TidyJSONWriter out2 = new TidyJSONWriter(response.getWriter());
//										out.object();
//										out.key("cross selling ***").value(prenotation.toString());
//										out.endObject();
//									}

									//INITIALIZE PAYMENT
									PaymentTypeEnum type = PaymentTypeEnum.CREDIT_CARD;
									
									InfoCarnetData carnetInfo = null;
								    String ipAddress = null;
								    String userAgent = null;
								    
								    prenotation.setBilling(createPNRRequest.getPrenotation().getBilling());
								    PaymentData paymentData = new PaymentData();
								    paymentData.setCurrency("EUR");
								    paymentData.setDescription("Cart containing {PNR:"+ prenotation.getPnr() +"}");
								    paymentData.setEmail("r.capitini@reply.it");
								    paymentData.setEnabledNewsLetter(false);
								    paymentData.setGrossAmount(originalPrice);
								    paymentData.setMailSubject("RICEVUTA DEL BIGLIETTO ELETTRONICO");
								    paymentData.setNetAmount(originalPrice);
									PaymentProcessInfoData process = new PaymentProcessInfoData();
										process.setMarketCode("IT");
										process.setPaRes(null);
										process.setPaymentAttemps(0);
										process.setPointOfSale(null);
										process.setRedirectUrl(null);
										process.setResult(ResultTypeEnum.OK);
										process.setSessionId(null);
										process.setShopId("WB");
										process.setSiteCode("IT");
										process.setStep(PaymentStepEnum.INITIALIZE);
										process.setTokenId(null);
										process.setTransactionId(null);
									paymentData.setProcess(process);
									
									PaymentProviderCreditCardData providerCC = new PaymentProviderCreditCardData();
									PaymentComunicationCreditCardData comunication = new PaymentComunicationCreditCardData();
											comunication.setDescription("Cart containing {PNR:"+ prenotation.getPnr() +"}");
											comunication.setLanguageCode("IT");
											comunication.setReturnUrl("https://booking.uat1.az/Booking/IT_IT/Checkout/RedirectOkPayment");
											comunication.setErrorUrl("https://booking.uat1.az/Booking/IT_IT/Checkout/RedirectOkPayment");
											comunication.setIpAddress("172.31.17.213");
											comunication.setAcceptHeader("*/*");
											comunication.setUserAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.65 Safari/537.36");
										providerCC.setComunication(comunication);
										providerCC.setCreditCardNumber("4444333322221111");
										providerCC.setCvv("123");
										providerCC.setExpiryMonth((short)6);
										providerCC.setExpiryYear((short)2018);
										providerCC.setIs3DSecure(true);
										providerCC.setToken(null);
										providerCC.setType(CreditCardTypeEnum.VISA);
										providerCC.setUseOneClick(false);
										UserInfoStandardData userInfo = new UserInfoStandardData();
											userInfo.setLastname("asd");
											userInfo.setName("asd");
										providerCC.setUserInfo(userInfo);
								    paymentData.setProvider(providerCC);
								    paymentData.setType(type);
//								    paymentData.setXsltMail("<![CDATA["
								    paymentData.setXsltMail(
								    		"<xsl:stylesheet version=\"2.0\" "
								    		+ "xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" xmlns:msxsl=\"urn:schemas-microsoft-com:xslt\" "
								    		+ "xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"  exclude-result-prefixes=\"msxsl\">"
								    		+ "<xsl:output method=\"html\" indent=\"yes\" encoding =\"utf-8\" />"
								    		+ "<xsl:key name=\"distE-Ticket\" match=\"//a:rsPax/a:Tickets/a:rsTicket\" use=\"substring(a:Number, 1,13)\" />"
								    		+ "<xsl:template match=\"node() | *\"><html xmlns=\"http://www.w3.org/1999/xhtml\"><head id=\"Head1\">"
								    		+ "<title>RICEVUTA DEL BIGLIETTO ELETTRONICO</title>"
								    		+ "<meta content=\"text/html; charset=ISO-8859-1\" http-equiv=\"Content-Type\" />"
								    		+ "</head><body><h3>Grazie per aver volato con Alitalia<h3></body></html></xsl:template></xsl:stylesheet>"
								    		);
//								    StringBuilder xsltMail = new StringBuilder();
//									xsltMail.append("<xsl:stylesheet version=\"2.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" xmlns:msxsl=\"urn:schemas-microsoft-com:xslt\" xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:a=\"http://www.alitalia.com/services/payment/contracts\" exclude-result-prefixes=\"msxsl\"> <xsl:output method=\"html\" indent=\"yes\" encoding =\"utf-8\" /> <xsl:key name=\"distE-Ticket\" match=\"//a:rsPax/a:Tickets/a:rsTicket\" use=\"substring(a:Number, 1,13)\" /> <xsl:template match=\"node() | *\"> <html xmlns=\"http://www.w3.org/1999/xhtml\"> <head id=\"Head1\"> <title>RICEVUTA DEL BIGLIETTO ELETTRONICO</title> <meta content=\"text/html; charset=ISO-8859-1\" http-equiv=\"Content-Type\" /> </head> <body> <table style=\"width: 100%; \" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\"> <tr> <td align=\"center\"> <table width=\"595\" bgcolor=\"#ffffff\" class=\"responsive\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: Arial; width: 595px;\"> <tbody> <tr> <td height=\"24\" style=\"font-size: 0px;\">&#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; font-family: Arial;\"> <tr> <td width=\"40\">&#160; </td> <td height=\"30\" width=\"522\" align=\"left\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; font-family: Arial;\"> <tr> <td width=\"168\" align=\"left\"> <a href=\"http://www.alitalia.com/\" style=\"text-decoration: none; border: 0;\" title=\"alitalia.com\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/alitalia_logo.png\" alt=\"alitalia\" align=\"left\" width=\"167\" style=\"border: 0;\" /> </a> </td> <td width=\"20\">&#160;</td> <td align=\"right\"> <a href=\"http://www.etihad.com\" style=\"text-decoration: none; border-left: 1px #d1d1d1 solid; padding-left: 21px;\" title=\"etihad.com\"> <img src=\"http://172.31.251.236/it_it/Images/logo_etihad_tcm12-9413.png\" alt=\"Etihad.com\" style=\"border: 0; vertical-align: bottom;\" width=\"86\" /> </a> </td> </tr> </table> </td> <td width=\"33\">&#160; </td> </tr> <tr> <td width=\"40\">&#160; </td> <td height=\"17\" width=\"522\" style=\"font-size: 0px; border-bottom: 1px solid #d1d1d1;\">&#160; </td> <td width=\"33\">&#160; </td> </tr> </table> </td> </tr> <tr> <td height=\"18\" style=\"font-size: 0px;\">&#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; font-family: Arial; font-size: 12px; text-align: center;\"> <tr> <td style=\"color: #006643; font-family: Arial; font-weight: 700; text-transform: uppercase; font-size: 15px;\"> IL TUO PAGAMENTO &#200; STATO EFFETTUATO CON SUCCESSO! </td> </tr> <tr> <td height=\"6\" style=\"font-size: 0px;\">&#160; </td> </tr> <tr> <td style=\"color: #000; font-family: Arial; font-weight: 300; text-transform: uppercase; font-size: 13px;\"> Il tuo codice di prenotazione (PNR) &#232; : <font style=\"font-weight: 700; font-size: 20px;\">NYDDQZ</font> </td> </tr> </table> </td> </tr> <tr> <td height=\"25\" style=\"font-size: 0px;\">&#160; </td> </tr> <tr> <td bgcolor=\"#f4f4f4\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; text-align: left;\"> <tr> <td colspan=\"3\" height=\"18\" style=\"font-size: 0px;\">&#160;</td> </tr> <tr> <td width=\"40\"> &#160; </td> <td width=\"522\" class=\"responsive\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; font-family: 'Trebuchet MS'; font-size: 11px; text-align: left;\"> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"> <tr> <td width=\"174\" style=\"vertical-align: top;\" class=\"collTable\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"text-align: left;\"> <tr> <td style=\"padding-right: 10px; width: 15px; font-family: Arial; font-size: 12px; font-weight: 700; color: #006643; text-transform: uppercase; line-height: 18px;\" width=\"15\"> 1 </td> <td style=\"font-family: Arial; font-size: 12px; font-weight: 700; color: #006643; text-transform: uppercase; line-height: 18px;\"> MAX TEST </td> </tr> <tr> <td> &#160; </td> <td style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 300; color: #000; line-height: 17px;\"> Adulto </td> </tr> <tr> <td> &#160; </td> <td style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 300; color: #000; line-height: 17px;\"> Numero biglietto: <xsl:for-each select=\"//a:rsPax[a:Name = 'MAX' and a:Surname = 'TEST']/a:Tickets/a:rsTicket[generate-id() = generate-id(key('distE-Ticket', substring(a:Number,1,13)))]\"> <xsl:value-of select=\"substring(a:Number,1,13)\" /> </xsl:for-each> </td> </tr> </table> </td> <td width=\"174\" style=\"vertical-align: top;\" class=\"collTable\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"text-align: left;\"> <tr> <td style=\"padding-right: 10px; width: 15px; font-family: Arial; font-size: 12px; font-weight: 700; color: #006643; text-transform: uppercase; line-height: 18px;\" width=\"15\"> 2 </td> <td style=\"font-family: Arial; font-size: 12px; font-weight: 700; color: #006643; text-transform: uppercase; line-height: 18px;\"> MED TEST </td> </tr> <tr> <td> &#160; </td> <td style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 300; color: #000; line-height: 17px;\"> Adulto </td> </tr> <tr> <td> &#160; </td> <td style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 300; color: #000; line-height: 17px;\"> Numero biglietto: <xsl:for-each select=\"//a:rsPax[a:Name = 'MED' and a:Surname = 'TEST']/a:Tickets/a:rsTicket[generate-id() = generate-id(key('distE-Ticket', substring(a:Number,1,13)))]\"> <xsl:value-of select=\"substring(a:Number,1,13)\" /> </xsl:for-each> </td> </tr> </table> </td> </tr> <tr/> </table> </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"> <tr> <td width=\"174\" style=\"vertical-align: top;\" class=\"collTable\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"text-align: left;\"> <tr> <td style=\"padding-right: 10px; width: 15px; font-family: Arial; font-size: 12px; font-weight: 700; color: #006643; text-transform: uppercase; line-height: 18px;\" width=\"15\"> 3 </td> <td style=\"font-family: Arial; font-size: 12px; font-weight: 700; color: #006643; text-transform: uppercase; line-height: 18px;\"> MIN TEST </td> </tr> <tr> <td> &#160; </td> <td style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 300; color: #000; line-height: 17px;\"> Bambino </td> </tr> <tr> <td> &#160; </td> <td style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 300; color: #000; line-height: 17px;\"> Numero biglietto: <xsl:for-each select=\"//a:rsPax[a:Name = 'MIN' and a:Surname = 'TEST']/a:Tickets/a:rsTicket[generate-id() = generate-id(key('distE-Ticket', substring(a:Number,1,13)))]\"> <xsl:value-of select=\"substring(a:Number,1,13)\" /> </xsl:for-each> </td> </tr> </table> </td> </tr> <tr/> </table> </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"> <tr/> <tr> <td width=\"174\" style=\"vertical-align: top;\" class=\"collTable\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"text-align: left;\"> <tr> <td style=\"padding-right: 10px; width: 15px; font-family: Arial; font-size: 12px; font-weight: 700; color: #006643; text-transform: uppercase; line-height: 18px;\" width=\"15\"> 4 </td> <td style=\"font-family: Arial; font-size: 12px; font-weight: 700; color: #006643; text-transform: uppercase; line-height: 18px;\"> NEO TEST </td> </tr> <tr> <td> &#160; </td> <td style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 300; color: #000; line-height: 17px;\"> Neonato </td> </tr> <tr> <td> &#160; </td> <td style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 300; color: #000; line-height: 17px;\"> Numero biglietto: <xsl:for-each select=\"//a:rsPax[a:Name = 'NEO' and a:Surname = 'TEST']/a:Tickets/a:rsTicket[generate-id() = generate-id(key('distE-Ticket', substring(a:Number,1,13)))]\"> <xsl:value-of select=\"substring(a:Number,1,13)\" /> </xsl:for-each> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr> <td height=\"13\" style=\"font-size: 0px; border-bottom: 1px solid #d1d1d1;\"> &#160; </td> </tr> <tr> <td height=\"19\" style=\"font-size: 0px;\"> &#160; </td> </tr> <tr> <td style=\"font-family: Arial; font-size: 12px; font-weight: 700; color: #006643; text-transform: uppercase;\"> Andata - diretto </td> </tr> <tr> <td height=\"8\" style=\"font-size: 0px;\"> &#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 700; color: #000; text-align: left; text-transform: uppercase; width: 100%;\" width=\"100%\"> <tr> <td width=\"73\" style=\"vertical-align: top;\"> AZ 1779<br /> </td> <td width=\"101\" style=\"vertical-align: top;\"> 15 giu 2015 </td> <td style=\"vertical-align: top;\" class=\"collTable\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 700; color: #000; text-align: left; text-transform: uppercase; width: 100%;\" width=\"100%\"> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 700; color: #000; text-align: left; text-transform: uppercase;\"> <tr> <td width=\"39\" style=\"vertical-align: top;\"> 12:30 </td> <td width=\"135\" style=\"vertical-align: top;\"> Roma Fiumicino </td> </tr> </table> </td> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 700; color: #000; text-align: left; text-transform: uppercase;\"> <tr> <td width=\"39\" style=\"vertical-align: top; text-transform: none;\"> 13:40 </td> <td width=\"135\" style=\"vertical-align: top;\"> Palermo </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr> <td height=\"4\" style=\"font-size: 0px;\"> &#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 11px; color: #000; text-align: left;\"> <tr> <td width=\"174\" class=\"spacerOperator\"> <font style=\"font-weight: 300; text-transform: none;\">Economy</font> </td> <td style=\"color: #006643;\"/> </tr> </table> </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 700; color: #000;\"> <tr> <td> <img src=\"http://booking.uat1.az/booking/_Content/images/email/seat.jpg\" alt=\"posto\" align=\"left\" /> </td> <td width=\"7\">&#160;</td> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 10px; font-weight: 700; color: #000;\"> <tr> <td width=\"14\" style=\"color:#006643; font-family: Arial; font-size: 11px; text-align: right;\">1</td> <td width=\"14\">&#160;</td> <td width=\"16\" style=\"text-align: left;\">8C</td> <td width=\"22\">&#160;</td> </tr> </table> </td> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 10px; font-weight: 700; color: #000;\"> <tr> <td width=\"14\" style=\"color:#006643; font-family: Arial; font-size: 11px; text-align: right;\">2</td> <td width=\"14\">&#160;</td> <td width=\"16\" style=\"text-align: left;\">8B</td> <td width=\"22\">&#160;</td> </tr> </table> </td> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 10px; font-weight: 700; color: #000;\"> <tr> <td width=\"14\" style=\"color:#006643; font-family: Arial; font-size: 11px; text-align: right;\">3</td> <td width=\"14\">&#160;</td> <td width=\"16\" style=\"text-align: left;\">8A</td> <td width=\"22\">&#160;</td> </tr> </table> </td> </tr> </table> </td> </tr> <tr> <td height=\"11\" style=\"font-size: 0px;\"> &#160; </td> </tr> <tr> <td style=\"font-family: Arial; font-size: 12px; font-weight: 700; color: #006643; text-transform: uppercase;\"> Ritorno - diretto </td> </tr> <tr> <td height=\"8\" style=\"font-size: 0px;\"> &#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 700; color: #000; text-align: left; text-transform: uppercase; width: 100%;\" width=\"100%\"> <tr> <td width=\"73\" style=\"vertical-align: top;\"> AZ 1792<br /> </td> <td width=\"101\" style=\"vertical-align: top;\"> 21 giu 2015 </td> <td style=\"vertical-align: top;\" class=\"collTable\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 700; color: #000; text-align: left; text-transform: uppercase; width: 100%;\" width=\"100%\"> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 700; color: #000; text-align: left; text-transform: uppercase;\"> <tr> <td width=\"39\" style=\"vertical-align: top;\"> 15:30 </td> <td width=\"135\" style=\"vertical-align: top;\"> Palermo </td> </tr> </table> </td> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 700; color: #000; text-align: left; text-transform: uppercase;\"> <tr> <td width=\"39\" style=\"vertical-align: top; text-transform: none;\"> 16:40 </td> <td width=\"135\" style=\"vertical-align: top;\"> Roma Fiumicino </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr> <td height=\"4\" style=\"font-size: 0px;\"> &#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 11px; color: #000; text-align: left;\"> <tr> <td width=\"174\" class=\"spacerOperator\"> <font style=\"font-weight: 300; text-transform: none;\">Economy</font> </td> <td style=\"color: #006643;\"/> </tr> </table> </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 700; color: #000;\"> <tr> <td> <img src=\"http://booking.uat1.az/booking/_Content/images/email/seat.jpg\" alt=\"posto\" align=\"left\" /> </td> <td width=\"7\">&#160;</td> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 10px; font-weight: 700; color: #000;\"> <tr> <td width=\"14\" style=\"color:#006643; font-family: Arial; font-size: 11px; text-align: right;\">1</td> <td width=\"14\">&#160;</td> <td width=\"16\" style=\"text-align: left;\">9A</td> <td width=\"22\">&#160;</td> </tr> </table> </td> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 10px; font-weight: 700; color: #000;\"> <tr> <td width=\"14\" style=\"color:#006643; font-family: Arial; font-size: 11px; text-align: right;\">2</td> <td width=\"14\">&#160;</td> <td width=\"16\" style=\"text-align: left;\">9B</td> <td width=\"22\">&#160;</td> </tr> </table> </td> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 10px; font-weight: 700; color: #000;\"> <tr> <td width=\"14\" style=\"color:#006643; font-family: Arial; font-size: 11px; text-align: right;\">3</td> <td width=\"14\">&#160;</td> <td width=\"16\" style=\"text-align: left;\">9C</td> <td width=\"22\">&#160;</td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> <td width=\"33\"> &#160; </td> </tr> <tr> <td colspan=\"3\" height=\"15\" style=\"font-size: 0px;\">&#160;</td> </tr> </table> </td> </tr> <tr> <td height=\"28\" style=\"font-size: 0px;\"> &#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; text-align: left;\"> <tr> <td width=\"40\"> &#160; </td> <td width=\"522\" class=\"responsive\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; font-family: 'Trebuchet MS'; font-size: 11px; text-align: left;\"> <tr> <td style=\"font-family: Arial; font-size: 10px; font-weight: 700; color: #006643; text-transform: uppercase;\"> CONDIZIONI E REGOLE TARIFFARIE PER IL VOLO DI ANDATA - Tariffa: EconomyEasy EUR 396,23 </td> </tr> <tr> <td height=\"7\" style=\"font-size: 0px;\"> &#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; font-family: 'Trebuchet MS'; font-size: 11px; text-align: left;\" class=\"info\"> <tbody> <tr> <td width=\"251\" style=\"vertical-align: top;\" class=\"collTable\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"width: 100%; font-family: 'Trebuchet MS'; font-size: 10px; font-weight:700; text-align: left;\"> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> BAGAGLIO A MANO </td> <td/> <td style=\"color:#006643; font-weight:bold; text-transform:none; margin-left:5px; background: #fff;\" align=\"right\" width=\"15\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/checked.png\" alt=\"checked\" width=\"15\" height=\"15\" style=\"width: 15px; height: 15px;\" /> </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> BAGAGLIO IN STIVA </td> <td/> <td style=\"color: #006643; text-transform: none; font-weight: bold; margin-left: 5px; font-size: 10px; line-height: 17px;\" align=\"right\"> 1 X 23kg </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> SCELTA DEL POSTO </td> <td/> <td style=\"color:#006643; font-weight:bold; text-transform:none; margin-left:5px; background: #fff;\" align=\"right\" width=\"15\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/null.png\" alt=\"null\" width=\"15\" height=\"15\" style=\"width: 15px; height: 15px;\" /> </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> CASH&amp;MILES </td> <td/> <td style=\"color:#006643; font-weight:bold; text-transform:none; margin-left:5px; background: #fff;\" align=\"right\" width=\"15\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/checked.png\" alt=\"checked\" width=\"15\" height=\"15\" style=\"width: 15px; height: 15px;\" /> </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> CHECK-IN </td> <td/> <td style=\"color:#006643; font-weight:bold; text-transform:none; margin-left:5px; background: #fff;\" align=\"right\" width=\"15\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/checked.png\" alt=\"checked\" width=\"15\" height=\"15\" style=\"width: 15px; height: 15px;\" /> </td> </tr> </tbody> </table> </td> </tr> </table> </td> <td width=\"14\" class=\"collTable infoSpacer\"> &#160; </td> <td width=\"257\" style=\"vertical-align: top;\" class=\"collTable\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"width: 100%; font-family: 'Trebuchet MS'; font-size: 10px; font-weight: 700; text-align: left;\"> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> Cambio Prima della partenza </td> <td/> <td style=\"color: #006643; text-transform: none; font-weight: bold; margin-left: 5px; font-size: 10px; line-height: 17px;\" align=\"right\"> 50,00 EUR </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> Cambio Dopo la partenza </td> <td/> <td style=\"color: #006643; text-transform: none; font-weight: bold; margin-left: 5px; font-size: 10px; line-height: 17px;\" align=\"right\"> 50,00 EUR </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> Rimborso Prima della partenza </td> <td/> <td style=\"color:#006643; font-weight:bold; text-transform:none; margin-left:5px; background: #fff;\" align=\"right\" width=\"15\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/null.png\" alt=\"null\" width=\"15\" height=\"15\" style=\"width: 15px; height: 15px;\" /> </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> Rimborso Dopo la partenza </td> <td/> <td style=\"color:#006643; font-weight:bold; text-transform:none; margin-left:5px; background: #fff;\" align=\"right\" width=\"15\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/null.png\" alt=\"null\" width=\"15\" height=\"15\" style=\"width: 15px; height: 15px;\" /> </td> </tr> </tbody> </table> </td> </tr> </table> </td> </tr> </tbody> </table> </td> </tr> </table> </td> <td width=\"33\"> &#160; </td> </tr> </table> </td> </tr> <tr> <td height=\"16\" style=\"font-size: 0px;\"> &#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; text-align: left;\"> <tr> <td width=\"40\"> &#160; </td> <td width=\"522\" class=\"responsive\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; font-family: 'Trebuchet MS'; font-size: 11px; text-align: left;\"> <tr> <td style=\"font-family: Arial; font-size: 10px; font-weight: 700; color: #006643; text-transform: uppercase;\"> CONDIZIONI E REGOLE TARIFFARIE PER IL VOLO DI RITORNO - Tariffa: EconomyEasy EUR 376,83 </td> </tr> <tr> <td height=\"7\" style=\"font-size: 0px;\"> &#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; font-family: 'Trebuchet MS'; font-size: 11px; text-align: left;\" class=\"info\"> <tbody> <tr> <td width=\"251\" style=\"vertical-align: top;\" class=\"collTable\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"width: 100%; font-family: 'Trebuchet MS'; font-size: 10px; font-weight:700; text-align: left;\"> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> BAGAGLIO A MANO </td> <td/> <td style=\"color:#006643; font-weight:bold; text-transform:none; margin-left:5px; background: #fff;\" align=\"right\" width=\"15\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/checked.png\" alt=\"checked\" width=\"15\" height=\"15\" style=\"width: 15px; height: 15px;\" /> </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> BAGAGLIO IN STIVA </td> <td/> <td style=\"color: #006643; text-transform: none; font-weight: bold; margin-left: 5px; font-size: 10px; line-height: 17px;\" align=\"right\"> 1 X 23kg </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> SCELTA DEL POSTO </td> <td/> <td style=\"color:#006643; font-weight:bold; text-transform:none; margin-left:5px; background: #fff;\" align=\"right\" width=\"15\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/null.png\" alt=\"null\" width=\"15\" height=\"15\" style=\"width: 15px; height: 15px;\" /> </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> CASH&amp;MILES </td> <td/> <td style=\"color:#006643; font-weight:bold; text-transform:none; margin-left:5px; background: #fff;\" align=\"right\" width=\"15\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/checked.png\" alt=\"checked\" width=\"15\" height=\"15\" style=\"width: 15px; height: 15px;\" /> </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> CHECK-IN </td> <td/> <td style=\"color:#006643; font-weight:bold; text-transform:none; margin-left:5px; background: #fff;\" align=\"right\" width=\"15\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/checked.png\" alt=\"checked\" width=\"15\" height=\"15\" style=\"width: 15px; height: 15px;\" /> </td> </tr> </tbody> </table> </td> </tr> </table> </td> <td width=\"14\" class=\"collTable infoSpacer\"> &#160; </td> <td width=\"257\" style=\"vertical-align: top;\" class=\"collTable\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"width: 100%; font-family: 'Trebuchet MS'; font-size: 10px; font-weight: 700; text-align: left;\"> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> Cambio Prima della partenza </td> <td/> <td style=\"color: #006643; text-transform: none; font-weight: bold; margin-left: 5px; font-size: 10px; line-height: 17px;\" align=\"right\"> 50,00 EUR </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> Cambio Dopo la partenza </td> <td/> <td style=\"color: #006643; text-transform: none; font-weight: bold; margin-left: 5px; font-size: 10px; line-height: 17px;\" align=\"right\"> 50,00 EUR </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> Rimborso Prima della partenza </td> <td/> <td style=\"color:#006643; font-weight:bold; text-transform:none; margin-left:5px; background: #fff;\" align=\"right\" width=\"15\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/null.png\" alt=\"null\" width=\"15\" height=\"15\" style=\"width: 15px; height: 15px;\" /> </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> Rimborso Dopo la partenza </td> <td/> <td style=\"color:#006643; font-weight:bold; text-transform:none; margin-left:5px; background: #fff;\" align=\"right\" width=\"15\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/null.png\" alt=\"null\" width=\"15\" height=\"15\" style=\"width: 15px; height: 15px;\" /> </td> </tr> </tbody> </table> </td> </tr> </table> </td> </tr> </tbody> </table> </td> </tr> </table> </td> <td width=\"33\"> &#160; </td> </tr> </table> </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; text-align: left;\"> <tr> <td width=\"40\"> &#160; </td> <td width=\"522\" class=\"responsive\" height=\"16\" style=\"font-size: 0px; border-bottom: 1px solid #d1d1d1;\"> &#160; </td> <td width=\"33\"> &#160; </td> </tr> </table> </td> </tr> <tr> <td height=\"16\" style=\"font-size: 0px;\"> &#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; text-align: left;\"> <tr> <td width=\"40\"> &#160; </td> <td width=\"522\" class=\"responsive\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; text-align: left;\"> <tr> <td width=\"243\" class=\"collTable\"> <table style=\"border-collapse: collapse; font-family: 'Trebuchet MS'; font-size: 10px; font-weight: 700;\" border=\"0\" cellspacing=\"0\" cellpadding=\"3px\" width=\"100%\"> <tbody> <tr> <td align=\"left\" style=\"text-transform: uppercase;\"> 2 Tariffa adulti </td> <td width=\"25\" align=\"right\" style=\"text-transform: uppercase;\"> EUR </td> <td align=\"right\" width=\"50\" style=\"text-transform: uppercase; width: 50px;\"> 308,00 </td> </tr> <tr> <td align=\"left\" style=\"text-transform: uppercase;\"> 1 Tariffa bambini </td> <td width=\"25\" align=\"right\" style=\"text-transform: uppercase;\"> EUR </td> <td align=\"right\" width=\"50\" style=\"text-transform: uppercase; width: 50px;\"> 104,00 </td> </tr> <tr> <td align=\"left\" style=\"text-transform: uppercase;\"> 1 Tariffa neonati </td> <td width=\"25\" align=\"right\" style=\"text-transform: uppercase;\"> EUR </td> <td align=\"right\" width=\"50\" style=\"text-transform: uppercase; width: 50px;\"> 40,00 </td> </tr> <tr> <td align=\"left\" style=\"text-transform: uppercase;\"> Tasse </td> <td width=\"25\" align=\"right\" style=\"text-transform: uppercase;\"> EUR </td> <td align=\"right\" width=\"50\" style=\"text-transform: uppercase; width: 50px;\"> 153,06 </td> </tr> <tr> <td align=\"left\" style=\"text-transform: uppercase;\"> Supplemento carburante </td> <td width=\"25\" align=\"right\" style=\"text-transform: uppercase;\"> EUR </td> <td align=\"right\" width=\"50\" style=\"text-transform: uppercase; width: 50px;\"> 168,00 </td> </tr> <tr> <td colspan=\"3\" style=\"border-top: 1px dotted #999; font-size: 0;\" width=\"100%\" height=\"1\"> &#160; </td> </tr> <tr> <td style=\"text-transform: uppercase; padding: 0;\"> <span class=\"no_uppercased\" xmlns=\"http://www.w3.org/1999/xhtml\">Hai pagato con</span> Visa </td> <td colspan=\"2\" rowspan=\"2\" align=\"right\" style=\"vertical-align: bottom;\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/CreditCards_Visa.png\" /> </td> </tr> <tr> <td style=\"text-transform: uppercase;\"> <font style=\"letter-spacing: 2px;\">*</font> <font style=\"letter-spacing: 2px;\">*</font> <font style=\"letter-spacing: 2px;\">*</font> <font style=\"letter-spacing: 2px;\">*</font> <font style=\"letter-spacing: 2px;\">*</font> <font style=\"letter-spacing: 2px;\">*</font> <font style=\"letter-spacing: 2px;\">*</font> <font style=\"letter-spacing: 2px;\">*</font> <font style=\"letter-spacing: 2px;\">*</font> <font style=\"letter-spacing: 2px;\">*</font> <font style=\"letter-spacing: 2px;\">*</font> 1111 </td> </tr> </tbody> </table> </td> <td width=\"36\" class=\"collTable infoSpacer\"> &#160; </td> <td width=\"243\" class=\"collTable\" style=\"vertical-align: bottom;\"> <table style=\"border-collapse: collapse; font-family: 'Trebuchet MS'; font-size: 10px; font-weight: 700;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> <tbody> <tr> <td align=\"right\" style=\"font-size: 10px; font-family: 'Trebuchet MS'; text-transform: uppercase; font-weight: 700;\"> TOTALE </td> </tr> <tr> <td align=\"right\" style=\"width: 233px; color: #006643; font-weight: 700; font-family: 'Trebuchet MS'; font-size: 25px; text-transform: uppercase;\"> EUR 773,<font style=\"font-size: 16px;\">06</font> </td> </tr> </tbody> </table> </td> </tr> </table> </td> <td width=\"33\"> &#160; </td> </tr> </table> </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; text-align: left;\"> <tr> <td width=\"40\"> &#160; </td> <td width=\"522\" class=\"responsive\" height=\"16\" style=\"font-size: 0px; border-bottom: 1px solid #d1d1d1;\"> &#160; </td> <td width=\"33\"> &#160; </td> </tr> </table> </td> </tr> <tr> <td height=\"17\" style=\"font-size: 0px;\"> &#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; text-align: left;\"> <tr> <td width=\"40\"> &#160; </td> <td width=\"522\" class=\"responsive\" style=\"font-family: 'Trebuchet MS'; font-size: 11px; color: #000;\"> Per ulteriori informazioni contatta il nostro Customer Center al numero 06 65640<br /> La tariffa e la prenotazione sono valide per un biglietto utilizzato integralmente, rispettando l&#8217;ordine di successione dei voli per il viaggio nelle date indicate. Le penali si applicano per ciascun passeggero/biglietto. Il cambio potrebbe comportare un&#8217;integrazione tariffaria se la tariffa precedentemente acquistata non &#232; pi&#249; disponibile. Il cambio, se consentito, va richiesto prima della partenza del volo che si desidera cambiare. </td> <td width=\"33\"> &#160; </td> </tr> </table> </td> </tr> <tr> <td height=\"13\" style=\"font-size: 0px;\">&#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; text-align: left;\"> <tr> <td width=\"40\">&#160; </td> <td width=\"522\" class=\"responsive\" style=\"font-family: Arial; font-weight: 700; font-size: 13px; color: #006643; text-transform: uppercase;\"> ti aspettiamo! </td> <td width=\"33\">&#160; </td> </tr> </table> </td> </tr> <tr> <td height=\"31\" style=\"font-size: 0px;\" class=\"noMobile\">&#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; text-align: left;\" class=\"noMobile\"> <tr> <td width=\"40\">&#160; </td> <td width=\"522\"> <table> <tbody> <tr> <td bgcolor=\"#f4f4f4\" width=\"170\" height=\"189\"> <span xmlns=\"http://www.w3.org/1999/xhtml\"/> </td> <td width=\"6\">&#160;</td> <td bgcolor=\"#f4f4f4\" width=\"170\" height=\"189\"> <span xmlns=\"http://www.w3.org/1999/xhtml\"/> </td> <td width=\"6\">&#160;</td> <td bgcolor=\"#f4f4f4\" width=\"170\" height=\"189\"> <span xmlns=\"http://www.w3.org/1999/xhtml\"/> </td> </tr> </tbody> </table> </td> <td width=\"33\">&#160; </td> </tr> </table> </td> </tr> <tr> <td height=\"29\" style=\"font-size: 0px;\">&#160; </td> </tr> <tr> <td height=\"28\" bgcolor=\"#006643\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; font-family: Arial;\"> <tr> <td width=\"40\" style=\"width: 40px;\">&#160;</td> <td align=\"left\" width=\"60%\"> <a href=\"http://www.alitalia.com/it_it/\" style=\"font-weight: 700; color: #fff; text-decoration: none; font-size: 14px;\">alitalia.com</a> </td> <td align=\"right\" valign=\"bottom\"> <table> <tr> <td style=\"padding-right: 13px;\"> <a href=\"https://www.facebook.com/alitalia\" style=\"border: 0; text-decoration: none;\" title=\"facebook\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/NS-f.jpg\" alt=\"facebook\" style=\"vertical-align: text-bottom; border: 0;\" /> </a> </td> <td style=\"padding-right: 12px;\"> <a href=\"https://twitter.com/Alitalia\" style=\"border: 0; text-decoration: none;\" title=\"twitter\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/NS-t.jpg\" alt=\"twitter\" style=\"vertical-align: text-bottom; border: 0;\" /> </a> </td> <td> <a href=\"https://plus.google.com/113375153773105297464/posts\" style=\"border: 0; text-decoration: none;\" title=\"google plus\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/NS-g.jpg\" alt=\"gplus\" style=\"vertical-align: text-bottom; border: 0;\" /> </a> </td> </tr> </table> </td> <td width=\"33\" style=\"width: 33px;\">&#160;</td> </tr> </table> </td> </tr> <tr> <td height=\"20\" style=\"font-size: 0px;\">&#160; </td> </tr> </tbody> </table> </td> </tr> </table> </body> </html> </xsl:template> </xsl:stylesheet>");
//									paymentData.setXsltMail(xsltMail.toString());

									prenotation.setPayment(paymentData);
								    
									InitializePaymentRequest initializePaymentRequest = new InitializePaymentRequest();
								    initializePaymentRequest.setCarnetInfo(carnetInfo);
								    initializePaymentRequest.setIpAddress(ipAddress);
								    initializePaymentRequest.setPrenotation(prenotation);
								    initializePaymentRequest.setUserAgent(userAgent);
								    
								    
								    InitializePaymentResponse initializePaymentResponse = paymentDelegate.initializePayment(initializePaymentRequest);
								    PaymentData payFrominitializePaymentResponse = initializePaymentResponse.getPaymentData();
								    
								    
									out = new TidyJSONWriter(response.getWriter());
									out.object();
									out.key("InitializePayment response ***").value(
									payFrominitializePaymentResponse!=null?payFrominitializePaymentResponse.toString():null);
									out.endObject();
									
									
									//AUTHORIZE PAYMENT
									if(payFrominitializePaymentResponse!=null){
										prenotation.setPayment(payFrominitializePaymentResponse);
								    AuthorizePaymentRequest requestAutorizeP = new AuthorizePaymentRequest();
								    requestAutorizeP.setFlgUsaCa(false);
								    requestAutorizeP.setPrenotation(prenotation);
								    
								    AuthorizePaymentResponse authorizePaymentResponse = paymentDelegate.authorizePayment(requestAutorizeP);
								    if(authorizePaymentResponse!=null){
								    	
										//CHECK PAYMENT
										if(authorizePaymentResponse.getProcess().getStep().equals(PaymentStepEnum.CHECK)){
											CheckPaymentRequest checkPaymentRequest = new CheckPaymentRequest();
												checkPaymentRequest.setProcess(authorizePaymentResponse.getProcess());
												checkPaymentRequest.setType(type); //Per sofort è GlobalCollect
											CheckPaymentResponse checkPaymentResponse = paymentDelegate.checkPayment(checkPaymentRequest);
											if(checkPaymentResponse!=null){
												
												//RETRIEVE TICKET
												RetrieveTicketRequest ticketRequest = new RetrieveTicketRequest();
												ticketRequest.setProcess(checkPaymentResponse.getProcess());
												
												
												List<TaxData> taxes = new ArrayList<TaxData>();
												TaxData t1 = new TaxData();
												t1.setAmount(new BigDecimal("90.00"));
												t1.setCode("Fare Adults");
												t1.setCurrency(null);
												t1.setId(null);
												t1.setName("Fare Adults");
												
												taxes.add(t1);
												
												t1 = new TaxData();
												t1.setAmount(new BigDecimal("49.96"));
												t1.setCode("taxTotal*Adult");
												t1.setCurrency("EUR");
												t1.setId(null);
												t1.setName("taxTotal");
												
												taxes.add(t1);
												ticketRequest.setTaxs(taxes);
												RoutesData routesData = new RoutesData();
												routesData.setRoutesList(createPNRResponse.getPrenotation().getRoutesList());
												routesData.setBilling(createPNRResponse.getPrenotation().getBilling());
												routesData.setProperties(createPNRResponse.getPrenotation().getProperties());
												routesData.setFarmId(createPNRResponse.getPrenotation().getFarmId());
												routesData.setId(createPNRResponse.getPrenotation().getId());
												routesData.setSolutionId(createPNRResponse.getPrenotation().getSolutionId());
												routesData.setSolutionSet(createPNRResponse.getPrenotation().getSolutionSet());
												routesData.setCabin(createPNRResponse.getPrenotation().getCabin());
												routesData.setCashAndMiles(createPNRResponse.getPrenotation().getCashAndMiles());
												routesData.setCoupon(createPNRResponse.getPrenotation().getCoupon());
												routesData.setInsurance(createPNRResponse.getPrenotation().getInsurance());
												routesData.setMilleMigliaCustomer(createPNRResponse.getPrenotation().getMilleMigliaCustomer());
												routesData.setMseType(createPNRResponse.getPrenotation().getMseType());
												routesData.setNationalInsuranceNumber(createPNRResponse.getPrenotation().getNationalInsuranceNumber());
												routesData.setPassengers(createPNRResponse.getPrenotation().getPassengers());
												routesData.setPayment(createPNRResponse.getPrenotation().getPayment());
												routesData.setPnr(createPNRResponse.getPrenotation().getPnr());
												routesData.setSliceCount(createPNRResponse.getPrenotation().getSliceCount());
												routesData.setSessionId(createPNRResponse.getPrenotation().getSessionId());
												
												ticketRequest.setRoutes(routesData);
												
												CrossSellingsRequest crossSellingsRequest = new CrossSellingsRequest();
												crossSellingsRequest.setIpAddress("10.8.50.15");
												crossSellingsRequest.setLanguageCode("IT");
												crossSellingsRequest.setMarketCode("IT");
												crossSellingsRequest.setPrenotation(routesData);
												CrossSellingsResponse crossSellingsResponse = manageMyBookingDelegate.getCrossSellingsByPrenotation(crossSellingsRequest);
												if (crossSellingsResponse != null &&
														crossSellingsResponse.getCrossSellingsData() != null &&
														crossSellingsResponse.getCrossSellingsData().getItems() != null &&
														!crossSellingsResponse.getCrossSellingsData().getItems().isEmpty()) {
													TidyJSONWriter out2 = new TidyJSONWriter(response.getWriter());
													for(CrossSellingBaseData crossSelling: crossSellingsResponse.getCrossSellingsData().getItems()) {
														out2.object();
														out2.key("cross selling ***").value(crossSelling.getDescription() + " " + 
																crossSelling.getName() + " " +
																crossSelling.getUrl() + " " +
																crossSelling.getImageName());
														out2.endObject();
													}
												}

												RetrieveTicketResponse tickets = paymentDelegate.retrieveTicket(ticketRequest);
												if(tickets != null){
													out = new TidyJSONWriter(response.getWriter());
													out.object();
													out.key("Biglietti **").value(tickets!=null?tickets.toString():null);
													out.endObject();
												}
												
											}
										}else{ //TODO
										
										} 
									}
								    

								    
									}
									
									
								}
								
								
								
							} else
								logger.error("Non ha funzionato!!!");
						} else {
							TidyJSONWriter out = new TidyJSONWriter(
									response.getWriter());
							out.object();
							out.key("refresh")
									.value("no valid return solution");
							out.endObject();
						}
					} else
						logger.error("Non ha funzionato!!!");
				} else {
					TidyJSONWriter out = new TidyJSONWriter(
							response.getWriter());
					out.object();
					out.key("brand search")
							.value("no valid return solution");
					out.endObject();
				}
			}
			else
				logger.error("Non ha funzionato!!!");
		
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			logger.error("Exception while executing doGet.", e);
		}
		
		logger.info("Executed doGet.");
	}
	
	
}
