package it.reply.sytel.toservices;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.FlightDetailsModelData;
import com.alitalia.aem.common.messages.home.RetrieveFlightDetailsRequest;
import com.alitalia.aem.common.messages.home.RetrieveFlightDetailsResponse;
import com.alitalia.aem.web.component.flightstatus.delegate.FlightStatusDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(name = "TestFlightDetailsServlet", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({ @Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "sling.servlet.selectors", value = { "testflightdetails" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "sling.servlet.methods", value = { "GET" }) })
public class TestFlightDetailsServlet extends SlingSafeMethodsServlet {

	private static final Logger logger = LoggerFactory.getLogger(TestFlightDetailsServlet.class);
	private static final long serialVersionUID = 1L;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile FlightStatusDelegate flightStatus;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		try {
			logger.info("Executing doGet.");

			RetrieveFlightDetailsRequest retrieveFlightDetails = new RetrieveFlightDetailsRequest();
			retrieveFlightDetails.setSid("Sid1234");
			retrieveFlightDetails.setTid("Tid1234");
			
			FlightDetailsModelData flightDetailsModelData = new FlightDetailsModelData();
			
			Calendar flightDate = Calendar.getInstance();
			flightDetailsModelData.setFlightDate(flightDate);
			flightDetailsModelData.setFlightNumber("1460");
			flightDetailsModelData.setVector("AZ");
			
			retrieveFlightDetails.setFlightDetailsModelData(flightDetailsModelData);
			
			RetrieveFlightDetailsResponse retrieveFlightResponse = flightStatus.retrieveFlightDetails(retrieveFlightDetails);
			
			response.setContentType("application/json");
			TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
			out.object().key("JSON").value(retrieveFlightResponse).endObject();

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception while executing doGet.", e);
		}
		logger.info("Executed doGet.");
	}
}