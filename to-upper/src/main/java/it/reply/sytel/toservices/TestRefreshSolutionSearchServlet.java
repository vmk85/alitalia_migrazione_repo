package it.reply.sytel.toservices;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.AvailableFlightsData;
import com.alitalia.aem.common.data.home.RefreshSearchData;
import com.alitalia.aem.common.data.home.enumerations.RefreshTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionResponse;
import com.alitalia.aem.common.messages.home.enumeration.SearchExecuteResponseType;
import com.alitalia.aem.web.component.searchflights.delegate.SearchFlightsDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(name = "TestRefreshSolutionSearchServlet", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
	@Property(name = "sling.servlet.selectors", value = {"testrefreshsearchsolution"}),
	@Property(name = "sling.servlet.extensions", value = {"json"}),
	@Property(name = "sling.servlet.methods", value = {"GET"})
})
public class TestRefreshSolutionSearchServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;	
	private static final Logger logger = LoggerFactory.getLogger(TestRefreshSolutionSearchServlet.class);
	

	@Reference(policy=ReferencePolicy.DYNAMIC)
	private volatile SearchFlightsDelegate searchFlightsDelegate;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		
		try {
			logger.info("Executing doGet.");
			
			response.setContentType("application/json");
			
			String valueSessionId = request.getParameter("sessionid");
			String valueSolutionId = request.getParameter("solutionid");
			String valueSolutionSet = request.getParameter("solutionset");

			RefreshSearchData refreshSearchData = new RefreshSearchData();
			refreshSearchData.setSessionId(valueSessionId);
			refreshSearchData.setSolutionSet(valueSolutionSet);
			refreshSearchData.setSolutionId(valueSolutionId);
			refreshSearchData.setSliceType(RouteTypeEnum.RETURN);
			refreshSearchData.setRefreshType(RefreshTypeEnum.REFRESH_SEARCH);

			SearchFlightSolutionRequest searchRequest = new SearchFlightSolutionRequest("TID", "SID");
			searchRequest.setFilter(refreshSearchData);
			searchRequest.setLanguageCode("IT");
			searchRequest.setMarket("IT");
			searchRequest.setResponseType(SearchExecuteResponseType.MODEL);

			SearchFlightSolutionResponse searchResponse = searchFlightsDelegate.searchRefreshSolutions(searchRequest);
			AvailableFlightsData availableFlights = null;
			if (searchResponse != null) {
				logger.info("Ha funzionato: " + searchResponse.toString());
				availableFlights = searchResponse.getAvailableFlights();
			}

//			AgencyLoginRequest loginRequest = new AgencyLoginRequest("1249812", "gsiweug");
//			loginRequest.setRuolo(AlitaliaTradeUserType.fromValue(valueRuolo));
//			loginRequest.setCodiceAgenzia(valueAgenzia);
//			loginRequest.setPassword(valuePassword);
//			AgencyLoginResponse loginResponse = businessLoginDelegate.agencyLogin(loginRequest);
//			AlitaliaTradeAgencyData agencyData = null;
//			if (loginResponse != null) {
//				logger.info("Ha funzionato: " + loginResponse.toString());
//				agencyData = loginResponse.getAgencyData();
//			}

			TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
			out.object();
			
//			if (agencyData != null) {
//				out.key("result").value(agencyData.getEmailAlitalia());
//				out.key("iataAgency").value(agencyData.isIataAgency());
//				out.key("groupEnabled").value(agencyData.isGroupEnabled());
//			}
			if (availableFlights != null) {
				out.key("solutionset").value(availableFlights.getSolutionSet());
				out.key("sessionid").value(availableFlights.getSessionId());
				out.key("routes").value(availableFlights.getRoutes().toString());
				out.key("tabs").value(availableFlights.getTabs().toString());
			}
			else
				out.key("result").value("invalid login");

			out.endObject();
		
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			logger.error("Exception while executing doGet.", e);
		}
		
		logger.info("Executed doGet.");
	}
	
	
}
