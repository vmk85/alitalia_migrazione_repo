package it.reply.sytel.toservices;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.web.component.searchflights.delegate.SearchFlightsDelegate;
import com.day.cq.commons.TidyJSONWriter;*/

@Component(name = "ToUpper", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
	@Property(name = "sling.servlet.selectors", value = {"toupper"}),
	@Property(name = "sling.servlet.extensions", value = {"json"}),
	@Property(name = "sling.servlet.methods", value = {"GET"})
})
public class ToUpperServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;	
	
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(ToUpperServlet.class);
	
	//@Reference
	//private SearchFlightsDelegate searchFlightsDelegate;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		
		/* try {
			logger.info("Executing doGet.");
			
			response.setContentType("application/json");
			
			RetrieveAirportsRequest retrieveAirportsRequest = new RetrieveAirportsRequest("12345", "abcde");
			retrieveAirportsRequest.setLanguageCode("IT");;
			retrieveAirportsRequest.setMarket("IT");
			retrieveAirportsRequest.setItemCache("false");
			
			RetrieveAirportsResponse airportsResponse = searchFlightsDelegate.retrieveAirports(retrieveAirportsRequest);
			if (airportsResponse != null) logger.info("Ha funzionato: " + airportsResponse.toString());
			TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
			out.object();
			
			String value = request.getParameter("email");
			out.key("result").value(value.toUpperCase());
			out.endObject();
		
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			logger.error("Exception while executing doGet.", e);
		}
		
		logger.info("Executed doGet."); */
	}
	
	
}
