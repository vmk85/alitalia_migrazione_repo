package it.reply.sytel.toservices;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.data.home.enumerations.MailingListActionTypesEnum;
import com.alitalia.aem.common.messages.home.MailinglistRegistrationRequest;
import com.alitalia.aem.common.messages.home.MailinglistRegistrationResponse;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(name = "TestRegisterMailistServlet", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({ @Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "sling.servlet.selectors", value = { "testregistrationmailinglist" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "sling.servlet.methods", value = { "GET" }) })
public class TestRegisterMailistServlet extends SlingSafeMethodsServlet {

	private static final Logger logger = LoggerFactory.getLogger(TestRegisterMailistServlet.class);
	private static final long serialVersionUID = 1L;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ConsumerLoginDelegate loginDelegate;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		try {
			logger.info("Executing doGet.");

			
			MailinglistRegistrationRequest registrationRequest = new MailinglistRegistrationRequest();
			MMCustomerProfileData customerProfile = new MMCustomerProfileData();
			customerProfile.setEmail("s.pluto@gmail.com");
			
			registrationRequest.setTid("T12345678910");
			registrationRequest.setSid("S12345678910");
			registrationRequest.setCustomerProfile(customerProfile);
			registrationRequest.setAction(MailingListActionTypesEnum.REGISTER);
			MailinglistRegistrationResponse registerResponse = loginDelegate.registerToMailinglist(registrationRequest);
			response.setContentType("application/json");
			TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
			out.object().key("JSON").value(registerResponse).endObject();

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception while executing doGet.", e);
		}
		logger.info("Executed doGet.");
	}
}