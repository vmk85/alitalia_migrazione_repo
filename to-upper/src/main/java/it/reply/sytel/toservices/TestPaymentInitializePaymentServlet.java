package it.reply.sytel.toservices;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.data.home.ApplicantPassengerData;
import com.alitalia.aem.common.data.home.BaggageAllowanceData;
import com.alitalia.aem.common.data.home.BillingData;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.BrandPenaltiesData;
import com.alitalia.aem.common.data.home.BrandPenaltyData;
import com.alitalia.aem.common.data.home.ChildPassengerData;
import com.alitalia.aem.common.data.home.ContactData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.FlightDetailsData;
import com.alitalia.aem.common.data.home.InfantPassengerData;
import com.alitalia.aem.common.data.home.InfoCarnetData;
import com.alitalia.aem.common.data.home.InsurancePolicyData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.PassengerBaseInfoData;
import com.alitalia.aem.common.data.home.PaymentComunicationCreditCardData;
import com.alitalia.aem.common.data.home.PaymentData;
import com.alitalia.aem.common.data.home.PaymentProcessInfoData;
import com.alitalia.aem.common.data.home.PaymentProviderCreditCardData;
import com.alitalia.aem.common.data.home.PhonePrefixData;
import com.alitalia.aem.common.data.home.PreferencesData;
import com.alitalia.aem.common.data.home.PropertiesData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoFareData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentDepartureData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentFlightData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatchData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingCocFareTotalData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingExtData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingExtEndorsementData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingExtPaxData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareCalculationData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareRestrictionData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareRestrictionLatestReturnData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingNoteData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingSaleFareTotalData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingSalePriceData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingSaleTaxTotalData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingTaxData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingTaxSalePriceData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.common.data.home.SeatPreferencesData;
import com.alitalia.aem.common.data.home.UserInfoStandardData;
import com.alitalia.aem.common.data.home.enumerations.ARTaxInfoTypesEnum;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.ContactTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.CreditCardTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.FlightTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.GenderTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PaymentStepEnum;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.ResultTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.messages.home.InitializePaymentRequest;
import com.alitalia.aem.common.messages.home.InitializePaymentResponse;
import com.alitalia.aem.web.component.payment.delegate.PaymentDelegate;
//import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultBookingDetails;

@Component(name = "TestPaymentInitializePaymentServlet", metatype=false, immediate=true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
	@Property(name = "sling.servlet.selectors", value = "testinitializepaymentservice"),
	@Property(name = "sling.servlet.extensions", value = {"json"}),
	@Property(name = "sling.servlet.methods", value = {"GET"})
})
public class TestPaymentInitializePaymentServlet extends SlingSafeMethodsServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6175526601368303446L;

	private static final Logger logger = LoggerFactory.getLogger(TestPaymentInitializePaymentServlet.class);
	
	@Reference(policy=ReferencePolicy.DYNAMIC)
	private volatile PaymentDelegate paymentDelegate;
	
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		
		//mock della request
		InfoCarnetData carnetInfo = null;
	    String ipAddress = null;
	    String userAgent = null;
	    
	    
	    PropertiesData properties = new PropertiesData();
	    properties.addElement("WebSession", "s3zfbyf2anap1wykwtmdtr54");
	    
	    //---***********************
	    ResultBookingDetailsData resultBookingDetailsData = new ResultBookingDetailsData();
	    List<ResultBookingDetailsSolutionData> solutionField = new ArrayList<ResultBookingDetailsSolutionData>();
	    ResultBookingDetailsSolutionData resultBookingDetailsSolutionData = new ResultBookingDetailsSolutionData();
	    resultBookingDetailsSolutionData.setAddCollectPriceField(null);
	    
	    //MM######################
	    //pricingField
	    List<ResultBookingDetailsSolutionPricingData> pricingField = new ArrayList<ResultBookingDetailsSolutionPricingData>();
	    ResultBookingDetailsSolutionPricingData resultBookingDetailsSolutionPricingData = new ResultBookingDetailsSolutionPricingData();
	    resultBookingDetailsSolutionPricingData.setAddCollectPriceField(null);
	    List<ResultBookingDetailsSolutionPricingBookingInfoData> bookingInfoFields = new ArrayList<ResultBookingDetailsSolutionPricingBookingInfoData>();
	    ResultBookingDetailsSolutionPricingBookingInfoData resultBookingDetailsSolutionPricingBookingInfoData = new ResultBookingDetailsSolutionPricingBookingInfoData();
	    	resultBookingDetailsSolutionPricingBookingInfoData.setBookingCodeCountField("7");
		    resultBookingDetailsSolutionPricingBookingInfoData.setBookingCodeField("Q");
		    resultBookingDetailsSolutionPricingBookingInfoData.setCabinField("COACH");
		    	ResultBookingDetailsSolutionPricingBookingInfoFareData fareField = new ResultBookingDetailsSolutionPricingBookingInfoFareData();
		    	 fareField.setExtendedFareCodeField("QSXRTN9");
		 	     fareField.setTicketDesignatorField(null);
			resultBookingDetailsSolutionPricingBookingInfoData.setFareField(fareField);
			resultBookingDetailsSolutionPricingBookingInfoData.setMarriedSegmentIndexField(0);
			resultBookingDetailsSolutionPricingBookingInfoData.setMarriedSegmentIndexFieldSpecified(true);
				ResultBookingDetailsSolutionPricingBookingInfoSegmentData segmentField = new ResultBookingDetailsSolutionPricingBookingInfoSegmentData();
				ResultBookingDetailsSolutionPricingBookingInfoSegmentDepartureData departureField = new ResultBookingDetailsSolutionPricingBookingInfoSegmentDepartureData();
					departureField.setDateField("2015-06-15");
					departureField.setTimeField("12:30");
				segmentField.setDepartureField(departureField);
				ResultBookingDetailsSolutionPricingBookingInfoSegmentFlightData flightField = new ResultBookingDetailsSolutionPricingBookingInfoSegmentFlightData();
					flightField.setCarrierField("AZ");
					flightField.setNumberField(1779);
					flightField.setNumberFieldSpecified(true);
				segmentField.setFlightField(flightField);
				segmentField.setOriginField("FCO");
				List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData> pricingField1 = new ArrayList<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData>();
					ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData resultBookingDetailsSolutionPricingBookingInfoSegmentPricingData = new ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData();
					List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatchData> extField = new ArrayList<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatchData>();
						ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatchData extField_0 = new ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatchData();
							extField_0.setCommercialNameField("UPTO50LB 23KG AND62LI 158LCM");
							extField_0.setCountField(1);
							extField_0.setCountFieldSpecified(true);
							extField_0.setSubcodeField("0GO");
						extField.add(extField_0);
					resultBookingDetailsSolutionPricingBookingInfoSegmentPricingData.setExtField(extField);
					
					List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData> fareField_0 = new ArrayList<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData>();
						ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData fareField_0_0 = new ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData();
							ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryData ruleSummaryField = new ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryData();
								ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesData penaltiesField = new ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesData();
								List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData> cancellationAndRefundPenaltyField = new ArrayList<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData>();
								ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData resultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData = new ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData();
								resultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData.setApplicationField("NO-SHOW");
								resultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData.setMaxPriceField("EUR50.00");
								resultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData.setMinPriceField("EUR50.00");
								cancellationAndRefundPenaltyField.add(resultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData );
								penaltiesField.setCancellationAndRefundPenaltyField(cancellationAndRefundPenaltyField);
								List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData> changePenaltyField = new ArrayList<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData>();
								ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData resultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData0 = new ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData();
								resultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData0.setApplicationField("BEFORE-DEPARTURE");
								resultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData0.setMaxPriceField("EUR50.00");
								resultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData0.setMinPriceField("EUR50.00");
								changePenaltyField.add(resultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData0);
								ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData resultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData1 = new ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData();
								resultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData1.setApplicationField("AFTER-DEPARTURE");
								resultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData1.setMaxPriceField("EUR50.00");
								resultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData1.setMinPriceField("EUR50.00");
								changePenaltyField.add(resultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData1 );
								penaltiesField.setChangePenaltyField(changePenaltyField);
								penaltiesField.setNonrefundableField(true);
								penaltiesField.setNonrefundableFieldSpecified(true);
								ruleSummaryField.setPenaltiesField(penaltiesField);	
								penaltiesField.setChangePenaltyField(changePenaltyField);
								ruleSummaryField.setPenaltiesField(penaltiesField);
							fareField_0_0.setRuleSummaryField(ruleSummaryField);
						fareField_0.add(fareField_0_0);
					resultBookingDetailsSolutionPricingBookingInfoSegmentPricingData.setFareField(fareField_0);
					pricingField1.add(resultBookingDetailsSolutionPricingBookingInfoSegmentPricingData);
				segmentField.setPricingField(pricingField1);
			resultBookingDetailsSolutionPricingBookingInfoData.setSegmentField(segmentField);
	    bookingInfoFields.add(resultBookingDetailsSolutionPricingBookingInfoData); //TODO
	    //bookingInfoFields.add(null);//TODO inserire altro oggetto resultBookingDetailsSolutionPricingBookingInfo
		resultBookingDetailsSolutionPricingData.setBookingInfoField(bookingInfoFields);
		resultBookingDetailsSolutionPricingData.setCocFareDifferenceField(null);
			ResultBookingDetailsSolutionPricingCocFareTotalData cocFareTotalField = new ResultBookingDetailsSolutionPricingCocFareTotalData();
			cocFareTotalField.setAmountField(new BigDecimal(154.00D));
			cocFareTotalField.setCurrencyField("EUR");
		resultBookingDetailsSolutionPricingData.setCocFareTotalField(cocFareTotalField);
		ResultBookingDetailsSolutionPricingExtData extField1 = new ResultBookingDetailsSolutionPricingExtData();
		List<ResultBookingDetailsSolutionPricingExtEndorsementData> endorsementField = new ArrayList<ResultBookingDetailsSolutionPricingExtEndorsementData>();
		ResultBookingDetailsSolutionPricingExtEndorsementData resultBookingDetailsSolutionPricingExtEndorsementData = new ResultBookingDetailsSolutionPricingExtEndorsementData();
			resultBookingDetailsSolutionPricingExtEndorsementData.setBoxesField("ENDORSEMENT-ORIGINAL");
			resultBookingDetailsSolutionPricingExtEndorsementData.setValueField("AZ/AP ONLY");
		endorsementField.add(resultBookingDetailsSolutionPricingExtEndorsementData);
			ResultBookingDetailsSolutionPricingExtPaxData paxField = new ResultBookingDetailsSolutionPricingExtPaxData();
			paxField.setAdultsField(2);
			paxField.setAdultsFieldSpecified(true);
			paxField.setChildrenField(0);
			paxField.setChildrenFieldSpecified(false);
			paxField.setInfantsInLapField(0);
			paxField.setInfantsInLapFieldSpecified(false);
			paxField.setYouthField(0);
			paxField.setYouthFieldSpecified(false);
		extField1.setPaxField(paxField);
		extField1.setEndorsementField(endorsementField);
		resultBookingDetailsSolutionPricingData.setExtField(extField1);
			List<ResultBookingDetailsSolutionPricingFareCalculationData> fareCalculationField = new ArrayList<ResultBookingDetailsSolutionPricingFareCalculationData>();
				ResultBookingDetailsSolutionPricingFareCalculationData resultBookingDetailsSolutionPricingFareCalculation = new ResultBookingDetailsSolutionPricingFareCalculationData();
					resultBookingDetailsSolutionPricingFareCalculation.setBoxedTaxField(null);
					resultBookingDetailsSolutionPricingFareCalculation.setLineField(Arrays.asList("ROM AZ PMO76.00QSXRTN9 AZ ROM Q2.00 76.00QSXRTN9 EUR154.00END XT 3.76EX5.08FN14.00HB26.31IT1.57MJ5.12VT56.00YR"));
				fareCalculationField.add(resultBookingDetailsSolutionPricingFareCalculation);
		resultBookingDetailsSolutionPricingData.setFareCalculationField(fareCalculationField);
		List<ResultBookingDetailsSolutionPricingFareData> fareField_0_1 = new ArrayList<ResultBookingDetailsSolutionPricingFareData>();
			ResultBookingDetailsSolutionPricingFareData resultBookingDetailsSolutionPricingFareData = new ResultBookingDetailsSolutionPricingFareData();
					resultBookingDetailsSolutionPricingFareData.setCarrierField("AZ");
					resultBookingDetailsSolutionPricingFareData.setDestinationCityField("PMO");
					resultBookingDetailsSolutionPricingFareData.setExtendedFareCodeField("QSXRTN9");
					resultBookingDetailsSolutionPricingFareData.setOriginCityField("ROM");
					List<String> ptcFields = new ArrayList();
					ptcFields.add("ADT");
					resultBookingDetailsSolutionPricingFareData.setPtcField(ptcFields);
					resultBookingDetailsSolutionPricingFareData.setTagField("ROUND-TRIP");
					resultBookingDetailsSolutionPricingFareData.setTicketDesignatorField(null);
			fareField_0_1.add(resultBookingDetailsSolutionPricingFareData);
			
			fareField_0_1 = new ArrayList<ResultBookingDetailsSolutionPricingFareData>();
			resultBookingDetailsSolutionPricingFareData = new ResultBookingDetailsSolutionPricingFareData();
					resultBookingDetailsSolutionPricingFareData.setCarrierField("AZ");
					resultBookingDetailsSolutionPricingFareData.setDestinationCityField("ROM");
					resultBookingDetailsSolutionPricingFareData.setExtendedFareCodeField("QSXRTN9");
					resultBookingDetailsSolutionPricingFareData.setOriginCityField("POM");
					ptcFields = new ArrayList();
					ptcFields.add("ADT");
					resultBookingDetailsSolutionPricingFareData.setPtcField(ptcFields);
					resultBookingDetailsSolutionPricingFareData.setTagField("ROUND-TRIP");
					resultBookingDetailsSolutionPricingFareData.setTicketDesignatorField(null);
			fareField_0_1.add(resultBookingDetailsSolutionPricingFareData);
		resultBookingDetailsSolutionPricingData.setFareField(fareField_0_1);
		List<ResultBookingDetailsSolutionPricingFareRestrictionData> fareRestrictionField = new ArrayList<ResultBookingDetailsSolutionPricingFareRestrictionData>();
			ResultBookingDetailsSolutionPricingFareRestrictionData resultBookingDetailsSolutionPricingFareRestrictionData = new ResultBookingDetailsSolutionPricingFareRestrictionData();
			resultBookingDetailsSolutionPricingFareRestrictionData.setEarliestReturnField(null);
			ResultBookingDetailsSolutionPricingFareRestrictionLatestReturnData latestReturnField = new ResultBookingDetailsSolutionPricingFareRestrictionLatestReturnData();
			latestReturnField.setDateField("2016-05-22");
			latestReturnField.setTimeField("23:59");
			resultBookingDetailsSolutionPricingFareRestrictionData.setLatestReturnField(latestReturnField);
			fareRestrictionField.add(resultBookingDetailsSolutionPricingFareRestrictionData);
		resultBookingDetailsSolutionPricingData.setFareRestrictionField(fareRestrictionField);
		
		List<ResultBookingDetailsSolutionPricingNoteData> noteFields = new ArrayList<ResultBookingDetailsSolutionPricingNoteData>();
			ResultBookingDetailsSolutionPricingNoteData resultBookingDetailsSolutionPricingNoteData = new ResultBookingDetailsSolutionPricingNoteData();
			resultBookingDetailsSolutionPricingNoteData.setIdField("6");
			resultBookingDetailsSolutionPricingNoteData.setNameField("NONREFUNDABLE-FARE");
			resultBookingDetailsSolutionPricingNoteData.setValueField("This ticket is non-refundable.");
			noteFields.add(resultBookingDetailsSolutionPricingNoteData);
			resultBookingDetailsSolutionPricingNoteData = new ResultBookingDetailsSolutionPricingNoteData();
			resultBookingDetailsSolutionPricingNoteData.setIdField("12");
			resultBookingDetailsSolutionPricingNoteData.setNameField("PENALTY-FARE");
			resultBookingDetailsSolutionPricingNoteData.setValueField("Changes to this ticket will incur a penalty fee.");
			noteFields.add(resultBookingDetailsSolutionPricingNoteData);
		resultBookingDetailsSolutionPricingData.setNoteField(noteFields);
		resultBookingDetailsSolutionPricingData.setPaxCountField(2);
		
		resultBookingDetailsSolutionPricingData.setPenaltyPriceField(null);
		resultBookingDetailsSolutionPricingData.setPreviousCocFareTotalField(null);
		resultBookingDetailsSolutionPricingData.setRefundPriceField(null);
		resultBookingDetailsSolutionPricingData.setReissueInfoField(null);
		resultBookingDetailsSolutionPricingData.setReissueTaxTotalDifferenceField(null);
			ResultBookingDetailsSolutionPricingSaleFareTotalData saleFareTotalField = new ResultBookingDetailsSolutionPricingSaleFareTotalData();
			saleFareTotalField.setAmountField(new BigDecimal("154.00"));
			saleFareTotalField.setCurrencyField("EUR");
		resultBookingDetailsSolutionPricingData.setSaleFareTotalField(saleFareTotalField);
			ResultBookingDetailsSolutionPricingSalePriceData salePriceField = new ResultBookingDetailsSolutionPricingSalePriceData();
			salePriceField.setAmountField(new BigDecimal("256.84"));
			salePriceField.setCurrencyField("EUR");
		resultBookingDetailsSolutionPricingData.setSalePriceField(salePriceField);
			ResultBookingDetailsSolutionPricingSaleTaxTotalData saleTaxTotalField = new ResultBookingDetailsSolutionPricingSaleTaxTotalData();
			saleTaxTotalField.setAmountField(new BigDecimal("111.84"));
			saleTaxTotalField.setCurrencyField("EUR");
		resultBookingDetailsSolutionPricingData.setSaleTaxTotalField(saleTaxTotalField);
			List<ResultBookingDetailsSolutionPricingTaxData> taxFields = new ArrayList<ResultBookingDetailsSolutionPricingTaxData>();
			ResultBookingDetailsSolutionPricingTaxData resultBookingDetailsSolutionPricingTaxData = new ResultBookingDetailsSolutionPricingTaxData();
			resultBookingDetailsSolutionPricingTaxData.setCodeField("IT");
			resultBookingDetailsSolutionPricingTaxData.setPreviousSalePriceField(null);
			ResultBookingDetailsSolutionPricingTaxSalePriceData salePriceFieldTax = new ResultBookingDetailsSolutionPricingTaxSalePriceData();
			salePriceFieldTax.setAmountField(new BigDecimal("16.90"));
			salePriceFieldTax.setCurrencyField("EUR");
			resultBookingDetailsSolutionPricingTaxData.setSalePriceField(salePriceFieldTax);
			taxFields.add(resultBookingDetailsSolutionPricingTaxData);
			
			resultBookingDetailsSolutionPricingTaxData = new ResultBookingDetailsSolutionPricingTaxData();
			resultBookingDetailsSolutionPricingTaxData.setCodeField("FN");
			resultBookingDetailsSolutionPricingTaxData.setPreviousSalePriceField(null);
			salePriceFieldTax = new ResultBookingDetailsSolutionPricingTaxSalePriceData();
			salePriceFieldTax.setAmountField(new BigDecimal("6.90"));
			salePriceFieldTax.setCurrencyField("EUR");
			resultBookingDetailsSolutionPricingTaxData.setSalePriceField(salePriceFieldTax);
			taxFields.add(resultBookingDetailsSolutionPricingTaxData);
			
			resultBookingDetailsSolutionPricingTaxData = new ResultBookingDetailsSolutionPricingTaxData();
			resultBookingDetailsSolutionPricingTaxData.setCodeField("VT");
			resultBookingDetailsSolutionPricingTaxData.setPreviousSalePriceField(null);
			salePriceFieldTax = new ResultBookingDetailsSolutionPricingTaxSalePriceData();
			salePriceFieldTax.setAmountField(new BigDecimal("2.68"));
			salePriceFieldTax.setCurrencyField("EUR");
			resultBookingDetailsSolutionPricingTaxData.setSalePriceField(salePriceFieldTax);
			taxFields.add(resultBookingDetailsSolutionPricingTaxData);
			
			resultBookingDetailsSolutionPricingTaxData = new ResultBookingDetailsSolutionPricingTaxData();
			resultBookingDetailsSolutionPricingTaxData.setCodeField("FN");
			resultBookingDetailsSolutionPricingTaxData.setPreviousSalePriceField(null);
			salePriceFieldTax = new ResultBookingDetailsSolutionPricingTaxSalePriceData();
			salePriceFieldTax.setAmountField(new BigDecimal("0.21"));
			salePriceFieldTax.setCurrencyField("EUR");
			resultBookingDetailsSolutionPricingTaxData.setSalePriceField(salePriceFieldTax);
			taxFields.add(resultBookingDetailsSolutionPricingTaxData);
			
			resultBookingDetailsSolutionPricingTaxData = new ResultBookingDetailsSolutionPricingTaxData();
			resultBookingDetailsSolutionPricingTaxData.setCodeField("HB");
			resultBookingDetailsSolutionPricingTaxData.setPreviousSalePriceField(null);
			salePriceFieldTax = new ResultBookingDetailsSolutionPricingTaxSalePriceData();
			salePriceFieldTax.setAmountField(new BigDecimal("7.50"));
			salePriceFieldTax.setCurrencyField("EUR");
			resultBookingDetailsSolutionPricingTaxData.setSalePriceField(salePriceFieldTax);
			taxFields.add(resultBookingDetailsSolutionPricingTaxData);
			
			resultBookingDetailsSolutionPricingTaxData = new ResultBookingDetailsSolutionPricingTaxData();
			resultBookingDetailsSolutionPricingTaxData.setCodeField("FN");
			resultBookingDetailsSolutionPricingTaxData.setPreviousSalePriceField(null);
			salePriceFieldTax = new ResultBookingDetailsSolutionPricingTaxSalePriceData();
			salePriceFieldTax.setAmountField(new BigDecimal("0.75"));
			salePriceFieldTax.setCurrencyField("EUR");
			resultBookingDetailsSolutionPricingTaxData.setSalePriceField(salePriceFieldTax);
			taxFields.add(resultBookingDetailsSolutionPricingTaxData);
			
			resultBookingDetailsSolutionPricingTaxData = new ResultBookingDetailsSolutionPricingTaxData();
			resultBookingDetailsSolutionPricingTaxData.setCodeField("MJ");
			resultBookingDetailsSolutionPricingTaxData.setPreviousSalePriceField(null);
			salePriceFieldTax = new ResultBookingDetailsSolutionPricingTaxSalePriceData();
			salePriceFieldTax.setAmountField(new BigDecimal("0.72"));
			salePriceFieldTax.setCurrencyField("EUR");
			resultBookingDetailsSolutionPricingTaxData.setSalePriceField(salePriceFieldTax);
			taxFields.add(resultBookingDetailsSolutionPricingTaxData);
			
			resultBookingDetailsSolutionPricingTaxData = new ResultBookingDetailsSolutionPricingTaxData();
			resultBookingDetailsSolutionPricingTaxData.setCodeField("FN");
			resultBookingDetailsSolutionPricingTaxData.setPreviousSalePriceField(null);
			salePriceFieldTax = new ResultBookingDetailsSolutionPricingTaxSalePriceData();
			salePriceFieldTax.setAmountField(new BigDecimal("0.07"));
			salePriceFieldTax.setCurrencyField("EUR");
			resultBookingDetailsSolutionPricingTaxData.setSalePriceField(salePriceFieldTax);
			taxFields.add(resultBookingDetailsSolutionPricingTaxData);
			
			resultBookingDetailsSolutionPricingTaxData = new ResultBookingDetailsSolutionPricingTaxData();
			resultBookingDetailsSolutionPricingTaxData.setCodeField("YR");
			resultBookingDetailsSolutionPricingTaxData.setPreviousSalePriceField(null);
			salePriceFieldTax = new ResultBookingDetailsSolutionPricingTaxSalePriceData();
			salePriceFieldTax.setAmountField(new BigDecimal("28.00"));
			salePriceFieldTax.setCurrencyField("EUR");
			resultBookingDetailsSolutionPricingTaxData.setSalePriceField(salePriceFieldTax);
			taxFields.add(resultBookingDetailsSolutionPricingTaxData);
			
			resultBookingDetailsSolutionPricingTaxData = new ResultBookingDetailsSolutionPricingTaxData();
			resultBookingDetailsSolutionPricingTaxData.setCodeField("IT");
			resultBookingDetailsSolutionPricingTaxData.setPreviousSalePriceField(null);
			salePriceFieldTax = new ResultBookingDetailsSolutionPricingTaxSalePriceData();
			salePriceFieldTax.setAmountField(new BigDecimal("9.41"));
			salePriceFieldTax.setCurrencyField("EUR");
			resultBookingDetailsSolutionPricingTaxData.setSalePriceField(salePriceFieldTax);
			taxFields.add(resultBookingDetailsSolutionPricingTaxData);
			
			resultBookingDetailsSolutionPricingTaxData = new ResultBookingDetailsSolutionPricingTaxData();
			resultBookingDetailsSolutionPricingTaxData.setCodeField("FN");
			resultBookingDetailsSolutionPricingTaxData.setPreviousSalePriceField(null);
			salePriceFieldTax = new ResultBookingDetailsSolutionPricingTaxSalePriceData();
			salePriceFieldTax.setAmountField(new BigDecimal("0.94"));
			salePriceFieldTax.setCurrencyField("EUR");
			resultBookingDetailsSolutionPricingTaxData.setSalePriceField(salePriceFieldTax);
			taxFields.add(resultBookingDetailsSolutionPricingTaxData);
			
			resultBookingDetailsSolutionPricingTaxData = new ResultBookingDetailsSolutionPricingTaxData();
			resultBookingDetailsSolutionPricingTaxData.setCodeField("VT");
			resultBookingDetailsSolutionPricingTaxData.setPreviousSalePriceField(null);
			salePriceFieldTax = new ResultBookingDetailsSolutionPricingTaxSalePriceData();
			salePriceFieldTax.setAmountField(new BigDecimal("2.44"));
			salePriceFieldTax.setCurrencyField("EUR");
			resultBookingDetailsSolutionPricingTaxData.setSalePriceField(salePriceFieldTax);
			taxFields.add(resultBookingDetailsSolutionPricingTaxData);
			
			resultBookingDetailsSolutionPricingTaxData = new ResultBookingDetailsSolutionPricingTaxData();
			resultBookingDetailsSolutionPricingTaxData.setCodeField("FN");
			resultBookingDetailsSolutionPricingTaxData.setPreviousSalePriceField(null);
			salePriceFieldTax = new ResultBookingDetailsSolutionPricingTaxSalePriceData();
			salePriceFieldTax.setAmountField(new BigDecimal("0.24"));
			salePriceFieldTax.setCurrencyField("EUR");
			resultBookingDetailsSolutionPricingTaxData.setSalePriceField(salePriceFieldTax);
			taxFields.add(resultBookingDetailsSolutionPricingTaxData);
			
			resultBookingDetailsSolutionPricingTaxData = new ResultBookingDetailsSolutionPricingTaxData();
			resultBookingDetailsSolutionPricingTaxData.setCodeField("EX");
			resultBookingDetailsSolutionPricingTaxData.setPreviousSalePriceField(null);
			salePriceFieldTax = new ResultBookingDetailsSolutionPricingTaxSalePriceData();
			salePriceFieldTax.setAmountField(new BigDecimal("1.71"));
			salePriceFieldTax.setCurrencyField("EUR");
			resultBookingDetailsSolutionPricingTaxData.setSalePriceField(salePriceFieldTax);
			taxFields.add(resultBookingDetailsSolutionPricingTaxData);
			
			resultBookingDetailsSolutionPricingTaxData = new ResultBookingDetailsSolutionPricingTaxData();
			resultBookingDetailsSolutionPricingTaxData.setCodeField("FN");
			resultBookingDetailsSolutionPricingTaxData.setPreviousSalePriceField(null);
			salePriceFieldTax = new ResultBookingDetailsSolutionPricingTaxSalePriceData();
			salePriceFieldTax.setAmountField(new BigDecimal("0.17"));
			salePriceFieldTax.setCurrencyField("EUR");
			resultBookingDetailsSolutionPricingTaxData.setSalePriceField(salePriceFieldTax);
			taxFields.add(resultBookingDetailsSolutionPricingTaxData);
			
			resultBookingDetailsSolutionPricingTaxData = new ResultBookingDetailsSolutionPricingTaxData();
			resultBookingDetailsSolutionPricingTaxData.setCodeField("HB");
			resultBookingDetailsSolutionPricingTaxData.setPreviousSalePriceField(null);
			salePriceFieldTax = new ResultBookingDetailsSolutionPricingTaxSalePriceData();
			salePriceFieldTax.setAmountField(new BigDecimal("6.50"));
			salePriceFieldTax.setCurrencyField("EUR");
			resultBookingDetailsSolutionPricingTaxData.setSalePriceField(salePriceFieldTax);
			taxFields.add(resultBookingDetailsSolutionPricingTaxData);
			
			resultBookingDetailsSolutionPricingTaxData = new ResultBookingDetailsSolutionPricingTaxData();
			resultBookingDetailsSolutionPricingTaxData.setCodeField("FN");
			resultBookingDetailsSolutionPricingTaxData.setPreviousSalePriceField(null);
			salePriceFieldTax = new ResultBookingDetailsSolutionPricingTaxSalePriceData();
			salePriceFieldTax.setAmountField(new BigDecimal("0.65"));
			salePriceFieldTax.setCurrencyField("EUR");
			resultBookingDetailsSolutionPricingTaxData.setSalePriceField(salePriceFieldTax);
			taxFields.add(resultBookingDetailsSolutionPricingTaxData);
			
			resultBookingDetailsSolutionPricingTaxData = new ResultBookingDetailsSolutionPricingTaxData();
			resultBookingDetailsSolutionPricingTaxData.setCodeField("MJ");
			resultBookingDetailsSolutionPricingTaxData.setPreviousSalePriceField(null);
			salePriceFieldTax = new ResultBookingDetailsSolutionPricingTaxSalePriceData();
			salePriceFieldTax.setAmountField(new BigDecimal("0.85"));
			salePriceFieldTax.setCurrencyField("EUR");
			resultBookingDetailsSolutionPricingTaxData.setSalePriceField(salePriceFieldTax);
			taxFields.add(resultBookingDetailsSolutionPricingTaxData);
			
			resultBookingDetailsSolutionPricingTaxData = new ResultBookingDetailsSolutionPricingTaxData();
			resultBookingDetailsSolutionPricingTaxData.setCodeField("FN");
			resultBookingDetailsSolutionPricingTaxData.setPreviousSalePriceField(null);
			salePriceFieldTax = new ResultBookingDetailsSolutionPricingTaxSalePriceData();
			salePriceFieldTax.setAmountField(new BigDecimal("0.09"));
			salePriceFieldTax.setCurrencyField("EUR");
			resultBookingDetailsSolutionPricingTaxData.setSalePriceField(salePriceFieldTax);
			taxFields.add(resultBookingDetailsSolutionPricingTaxData);
			
			resultBookingDetailsSolutionPricingTaxData = new ResultBookingDetailsSolutionPricingTaxData();
			resultBookingDetailsSolutionPricingTaxData.setCodeField("YR");
			resultBookingDetailsSolutionPricingTaxData.setPreviousSalePriceField(null);
			salePriceFieldTax = new ResultBookingDetailsSolutionPricingTaxSalePriceData();
			salePriceFieldTax.setAmountField(new BigDecimal("28.00"));
			salePriceFieldTax.setCurrencyField("EUR");
			resultBookingDetailsSolutionPricingTaxData.setSalePriceField(salePriceFieldTax);
			taxFields.add(resultBookingDetailsSolutionPricingTaxData);
		resultBookingDetailsSolutionPricingData.setTaxField(taxFields);
		
		
		
		pricingField.add(resultBookingDetailsSolutionPricingData);
		
		//TODO inserire altri due pricing fields
		
	    //MM####################
		
		resultBookingDetailsSolutionData.setPricingField(pricingField);
		resultBookingDetailsSolutionData.setRefundPriceField(null);
		resultBookingDetailsSolutionData.setSaleTaxTotalField("EUR321.06");
		resultBookingDetailsSolutionData.setSaleTotalField("EUR773.06");
		resultBookingDetailsSolutionData.setTimeField("2015-05-22T09:53+00:00");
		solutionField.add(resultBookingDetailsSolutionData);
		resultBookingDetailsData.setSolutionField(solutionField);
	    
		
		properties.addElement("BookingDetails", resultBookingDetailsData);
	    //---*******************
	    
	    
	    RoutesData prenotation = new RoutesData();
	    prenotation.setProperties(properties);
	    prenotation.setFarmId(null);
	    prenotation.setId("7pAh73JOUg9euXwcF0MAXB");
	    prenotation.setSessionId("6Pr0MAXBCsDKNukYtfCjQJwPO");
	    prenotation.setSolutionSet("0CyOktkyvJ2SN1E0iV7c2Y8");
	    
	    BillingData billing = new BillingData();
	    billing.setBill(true);
	    billing.setCFPIVA(null);
	    billing.setCap("04100");
	    billing.setCfInt(null);
	    billing.setCfPax("RBNMSM74M24E472K");
	    billing.setEmailInt("massimo.rubinelli@eng.it");
	    billing.setEnteEmit(null);
	    billing.setEnteRich(null);
	    billing.setFormaSocietaria("PF");
	    billing.setIndSped("via di test");
	    billing.setIntFattura(null);
	    billing.setLocSped("Latina");
	    billing.setName("max");
	    billing.setNome("max test");
	    billing.setPaese("IT");
	    billing.setPivaInt(null);
	    billing.setProv("lt");
	    billing.setSelectedCountry("IT");
	    billing.setSocieta(null);
	    billing.setSurname("test");
	    billing.setTipoRichiesta(null);
	    prenotation.setBilling(billing);
	    
	    prenotation.setCabin(CabinEnum.ECONOMY);
	    prenotation.setCashAndMiles(null);
	    prenotation.setCoupon(null);
	    
	    InsurancePolicyData insurance = new InsurancePolicyData();
	    insurance.setBuy(true);
	    insurance.setCardType(null);
	    insurance.setCurrency("EUR");
	    insurance.setDiscount(new BigDecimal(52));
	    insurance.setDiscountedPrice(new BigDecimal(52));
	    insurance.setErrorDescription(null);
	    insurance.setPolicyNumber(null);
	    insurance.setTotalInsuranceCost(new BigDecimal(52));
	    insurance.setTypeOfTrip(null);
	    prenotation.setInsurance(insurance);

	    prenotation.setMilleMigliaCustomer(null);
	    prenotation.setMseType(null);
	    prenotation.setNationalInsuranceNumber("RBNMSM74M24E472K");
	    prenotation.setPnr("NYDDQZ");
	    List<PassengerBaseData> passengers = new ArrayList<PassengerBaseData>();
	    
	    
	    
	    
	    ApplicantPassengerData passenger = new ApplicantPassengerData();
	    passenger.setCouponPrice(new BigDecimal(0));
	    passenger.setExtraCharge(new BigDecimal(0.0D));
	    passenger.setFee(new BigDecimal(0));
	    passenger.setGrossFare(new BigDecimal(256.84));
	    PassengerBaseInfoData info = new PassengerBaseInfoData();
	    Calendar calendar = Calendar.getInstance();
	    calendar.set(1, 1, 1, 0, 0 , 0);
	    info.setBirthDate(calendar);
	    info.setGender(GenderTypeEnum.UNKNOWN);
	    passenger.setInfo(info);
	    passenger.setLastName("test");
	    passenger.setName("max");
	    passenger.setNetFare(new BigDecimal(154.00D));
	    passenger.setTickets(null);
	    passenger.setType(PassengerTypeEnum.ADULT);
	    
	    PreferencesData preferences = new PreferencesData();
	    preferences.setMealType(null);
	    
	    List<SeatPreferencesData> seatPreferencesData = new ArrayList<SeatPreferencesData>();
	    
	    SeatPreferencesData seatPreferenceData = new SeatPreferencesData();
	    seatPreferenceData.setCabinSeat(CabinEnum.ECONOMY);
	    seatPreferenceData.setFlightCarrier("AZ");
	    seatPreferenceData.setFlightNumber("1792");
	    seatPreferenceData.setNumber(null);
	    seatPreferenceData.setRow("9A");
	    seatPreferencesData.add(seatPreferenceData);
	    
	    seatPreferenceData = new SeatPreferencesData();
	    seatPreferenceData.setCabinSeat(CabinEnum.ECONOMY);
	    seatPreferenceData.setFlightCarrier("AZ");
	    seatPreferenceData.setFlightNumber("1779");
	    seatPreferenceData.setNumber(null);
	    seatPreferenceData.setRow("8C");
	    seatPreferencesData.add(seatPreferenceData);
	    
	    preferences.setSeatPreferences(seatPreferencesData);
	    preferences.setSeatType(null);
	    
	    passenger.setPreferences(preferences);
	    
	    passenger.setFrequentFlyerCode(null);
	    passenger.setFrequentFlyerTier(null);
	    passenger.setFrequentFlyerType(null);
	    passenger.setArTaxInfoType(ARTaxInfoTypesEnum.NONE);
	    passenger.setBlueBizCode(null);
	    passenger.setCuit(null);
	    
	    List<ContactData> contacts = new ArrayList<ContactData>();
	    
	    ContactData contact = new ContactData();
	    contact.setContactType(ContactTypeEnum.M);
	    contact.setPhoneNumber("32131212");

	    PhonePrefixData prefix = new PhonePrefixData();
	    prefix.setCode("IT");
	    prefix.setDescription("Italy");
	    prefix.setPrefix("39");
	    
	    contact.setPrefix(prefix);
	    contacts.add(contact);
	    
	    contact = new ContactData();
	    contact.setContactType(ContactTypeEnum.M);
	    contact.setPhoneNumber("massimo.rubinelli@eng.it");
	    

	    prefix = new PhonePrefixData();
	    prefix.setCode(null);
	    prefix.setDescription("email");
	    prefix.setPrefix(null);
	    
	    contact.setPrefix(prefix);
	    contacts.add(contact);
	    
	    passenger.setContact(contacts);
	    passenger.setCountry(null);
	    passenger.setEmail("massimo.rubinelli@eng.it");
	    passenger.setSkyBonusCode(null);
	    passenger.setSubscribeToNewsletter(null);
	    passengers.add(passenger);
	    
	    AdultPassengerData passenger1 = new AdultPassengerData();
	    passenger1.setCouponPrice(new BigDecimal(0));
	    passenger1.setExtraCharge(new BigDecimal(0.00D));
	    passenger1.setFee(new BigDecimal(0));
	    passenger1.setGrossFare(new BigDecimal(265.84));
	    
	    info = new PassengerBaseInfoData();
	    Calendar calendar1 = Calendar.getInstance();
	    calendar1.set(1, 1, 1, 0, 0 , 0);
	    info.setBirthDate(calendar1);
	    info.setGender(GenderTypeEnum.UNKNOWN);
	    passenger1.setInfo(info);
	    passenger1.setLastName("test");
	    passenger1.setName("mad");
	    passenger1.setNetFare(new BigDecimal(154.0));
	    passenger1.setTickets(null);
	    
	    preferences = new PreferencesData();
	    preferences.setMealType(null);
	    
	    seatPreferencesData = new ArrayList<SeatPreferencesData>();
	    
	    seatPreferenceData = new SeatPreferencesData();
	    seatPreferenceData.setCabinSeat(CabinEnum.ECONOMY);
	    seatPreferenceData.setFlightCarrier("AZ");
	    seatPreferenceData.setFlightNumber("1779");
	    seatPreferenceData.setNumber(null);
	    seatPreferenceData.setRow("8B");
	    seatPreferencesData.add(seatPreferenceData);
	    
	    seatPreferenceData = new SeatPreferencesData();
	    seatPreferenceData.setCabinSeat(CabinEnum.ECONOMY);
	    seatPreferenceData.setFlightCarrier("AZ");
	    seatPreferenceData.setFlightNumber("1792");
	    seatPreferenceData.setNumber(null);
	    seatPreferenceData.setRow("9B");
	    seatPreferencesData.add(seatPreferenceData);
	    
	    preferences.setSeatPreferences(seatPreferencesData);
	    preferences.setSeatType(null);
	    passenger1.setPreferences(preferences);
	    passenger1.setFrequentFlyerCode(null);
	    passenger1.setFrequentFlyerTier(null);
	    passenger1.setFrequentFlyerType(null);
	    
	    passengers.add(passenger1);

	    ChildPassengerData passenger2 = new ChildPassengerData();
	    passenger2.setCouponPrice(new BigDecimal(0));
	    passenger2.setExtraCharge(new BigDecimal(0.00D));
	    passenger2.setFee(new BigDecimal(0));
	    passenger2.setGrossFare(new BigDecimal(201.38));
	    
	    info = new PassengerBaseInfoData();
	    Calendar calendar2 = Calendar.getInstance();
	    calendar2.set(2007, 9, 13, 0, 0 , 0);
	    info.setBirthDate(calendar2);
	    info.setGender(GenderTypeEnum.MALE);
	    passenger2.setInfo(info);
	    passenger2.setLastName("test");
	    passenger2.setName("min");
	    passenger2.setNetFare(new BigDecimal(104.00));
	    passenger2.setTickets(null);
	    
	    preferences = new PreferencesData();
	    preferences.setMealType(null);
	    
	    seatPreferencesData = new ArrayList<SeatPreferencesData>();
	    
	    seatPreferenceData = new SeatPreferencesData();
	    seatPreferenceData.setCabinSeat(CabinEnum.ECONOMY);
	    seatPreferenceData.setFlightCarrier("AZ");
	    seatPreferenceData.setFlightNumber("1779");
	    seatPreferenceData.setNumber(null);
	    seatPreferenceData.setRow("8A");
	    seatPreferencesData.add(seatPreferenceData);
	    
	    seatPreferenceData = new SeatPreferencesData();
	    seatPreferenceData.setCabinSeat(CabinEnum.ECONOMY);
	    seatPreferenceData.setFlightCarrier("AZ");
	    seatPreferenceData.setFlightNumber("1792");
	    seatPreferenceData.setNumber(null);
	    seatPreferenceData.setRow("9C");
	    seatPreferencesData.add(seatPreferenceData);
	    
	    preferences.setSeatPreferences(seatPreferencesData);
	    preferences.setSeatType(null);
	    passenger2.setPreferences(preferences);
	    passengers.add(passenger2);

	    InfantPassengerData passenger3 = new InfantPassengerData();
	    passenger3.setCouponPrice(new BigDecimal(0));
	    passenger3.setExtraCharge(new BigDecimal(0.00D));
	    passenger3.setFee(new BigDecimal(0));
	    passenger3.setGrossFare(new BigDecimal(40.00));
	    
	    info = new PassengerBaseInfoData();
	    Calendar calendar3 = Calendar.getInstance();
	    calendar3.set(2014, 10, 14, 0, 0 , 0);
	    info.setBirthDate(calendar3);
	    info.setGender(GenderTypeEnum.FEMALE);
	    passenger3.setInfo(info);
	    passenger3.setLastName("test");
	    passenger3.setName("neo");
	    passenger3.setNetFare(new BigDecimal(40.00));
	    passenger3.setTickets(null);
	    passenger3.setType(PassengerTypeEnum.INFANT);
	    passenger3.setAdultRefent(0);
	    passengers.add(passenger3);
	    
	    
	    prenotation.setPassengers(passengers);
	    PaymentData paymentData = new PaymentData();
	    paymentData.setCurrency("EUR");
	    paymentData.setDescription("Cart containing {PNR:NYDDQZ}");
	    paymentData.setEmail("massimo.rubinelli@eng.it");
	    paymentData.setEnabledNewsLetter(false);
	    paymentData.setGrossAmount(new BigDecimal(773.06));
	    paymentData.setMailSubject("RICEVUTA DEL BIGLIETTO ELETTRONICO");
	    paymentData.setNetAmount(new BigDecimal(773.06));
	    PaymentProcessInfoData process = new PaymentProcessInfoData();
	    process.setMarketCode("EUR");
	    process.setPaRes(null);
	    process.setPaymentAttemps(0);
	    process.setPointOfSale(null);
	    process.setRedirectUrl(null);
	    process.setResult(ResultTypeEnum.OK);
	    process.setSessionId(null);
	    process.setShopId("WB");
	    process.setSiteCode("IT");
	    process.setStep(PaymentStepEnum.INITIALIZE);
	    process.setTokenId(null);
	    process.setTransactionId(null);
	    
	    paymentData.setProcess(process);
	    PaymentProviderCreditCardData provider = new PaymentProviderCreditCardData();
	    
	    PaymentComunicationCreditCardData paymentComunicationCreditCardData = new PaymentComunicationCreditCardData();
	    paymentComunicationCreditCardData.setDescription("Cart containing {PNR:NYDDQZ}");
	    paymentComunicationCreditCardData.setLanguageCode("IT");
	    paymentComunicationCreditCardData.setReturnUrl("https://booking.uat1.az/Booking/IT_IT/Checkout/RedirectOkPayment");
	    paymentComunicationCreditCardData.setErrorUrl("https://booking.uat1.az/Booking/IT_IT/Checkout/RedirectOkPayment");
	    paymentComunicationCreditCardData.setAcceptHeader("*/*");
	    paymentComunicationCreditCardData.setIpAddress("172.31.17.213");
	    paymentComunicationCreditCardData.setUserAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.65 Safari/537.36");
		provider.setComunication(paymentComunicationCreditCardData);
	    provider.setCvv("123");
	    provider.setCreditCardNumber("4444333322221111");
	    provider.setExpiryMonth((short)6);
	    provider.setExpiryYear((short)2018);
	    provider.setIs3DSecure(true);
		provider.setToken(null);
		provider.setType(CreditCardTypeEnum.VISA);
		provider.setUseOneClick(true);
		UserInfoStandardData userInfoStandardData = new UserInfoStandardData();
		userInfoStandardData.setLastname("asd");
		userInfoStandardData.setName("asd");
		provider.setUserInfo(userInfoStandardData);
		
		paymentData.setProvider(provider);
		paymentData.setType(PaymentTypeEnum.CREDIT_CARD);
		
		
		StringBuilder xsltMail = new StringBuilder();
		
		
		
		
		
		xsltMail.append("<xsl:stylesheet version=\"2.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" xmlns:msxsl=\"urn:schemas-microsoft-com:xslt\" xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:a=\"http://www.alitalia.com/services/payment/contracts\" exclude-result-prefixes=\"msxsl\"> <xsl:output method=\"html\" indent=\"yes\" encoding =\"utf-8\" /> <xsl:key name=\"distE-Ticket\" match=\"//a:rsPax/a:Tickets/a:rsTicket\" use=\"substring(a:Number, 1,13)\" /> <xsl:template match=\"node() | *\"> <html xmlns=\"http://www.w3.org/1999/xhtml\"> <head id=\"Head1\"> <title>RICEVUTA DEL BIGLIETTO ELETTRONICO</title> <meta content=\"text/html; charset=ISO-8859-1\" http-equiv=\"Content-Type\" /> </head> <body> <table style=\"width: 100%; \" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\"> <tr> <td align=\"center\"> <table width=\"595\" bgcolor=\"#ffffff\" class=\"responsive\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: Arial; width: 595px;\"> <tbody> <tr> <td height=\"24\" style=\"font-size: 0px;\">&#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; font-family: Arial;\"> <tr> <td width=\"40\">&#160; </td> <td height=\"30\" width=\"522\" align=\"left\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; font-family: Arial;\"> <tr> <td width=\"168\" align=\"left\"> <a href=\"http://www.alitalia.com/\" style=\"text-decoration: none; border: 0;\" title=\"alitalia.com\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/alitalia_logo.png\" alt=\"alitalia\" align=\"left\" width=\"167\" style=\"border: 0;\" /> </a> </td> <td width=\"20\">&#160;</td> <td align=\"right\"> <a href=\"http://www.etihad.com\" style=\"text-decoration: none; border-left: 1px #d1d1d1 solid; padding-left: 21px;\" title=\"etihad.com\"> <img src=\"http://172.31.251.236/it_it/Images/logo_etihad_tcm12-9413.png\" alt=\"Etihad.com\" style=\"border: 0; vertical-align: bottom;\" width=\"86\" /> </a> </td> </tr> </table> </td> <td width=\"33\">&#160; </td> </tr> <tr> <td width=\"40\">&#160; </td> <td height=\"17\" width=\"522\" style=\"font-size: 0px; border-bottom: 1px solid #d1d1d1;\">&#160; </td> <td width=\"33\">&#160; </td> </tr> </table> </td> </tr> <tr> <td height=\"18\" style=\"font-size: 0px;\">&#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; font-family: Arial; font-size: 12px; text-align: center;\"> <tr> <td style=\"color: #006643; font-family: Arial; font-weight: 700; text-transform: uppercase; font-size: 15px;\"> IL TUO PAGAMENTO &#200; STATO EFFETTUATO CON SUCCESSO! </td> </tr> <tr> <td height=\"6\" style=\"font-size: 0px;\">&#160; </td> </tr> <tr> <td style=\"color: #000; font-family: Arial; font-weight: 300; text-transform: uppercase; font-size: 13px;\"> Il tuo codice di prenotazione (PNR) &#232; : <font style=\"font-weight: 700; font-size: 20px;\">NYDDQZ</font> </td> </tr> </table> </td> </tr> <tr> <td height=\"25\" style=\"font-size: 0px;\">&#160; </td> </tr> <tr> <td bgcolor=\"#f4f4f4\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; text-align: left;\"> <tr> <td colspan=\"3\" height=\"18\" style=\"font-size: 0px;\">&#160;</td> </tr> <tr> <td width=\"40\"> &#160; </td> <td width=\"522\" class=\"responsive\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; font-family: 'Trebuchet MS'; font-size: 11px; text-align: left;\"> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"> <tr> <td width=\"174\" style=\"vertical-align: top;\" class=\"collTable\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"text-align: left;\"> <tr> <td style=\"padding-right: 10px; width: 15px; font-family: Arial; font-size: 12px; font-weight: 700; color: #006643; text-transform: uppercase; line-height: 18px;\" width=\"15\"> 1 </td> <td style=\"font-family: Arial; font-size: 12px; font-weight: 700; color: #006643; text-transform: uppercase; line-height: 18px;\"> MAX TEST </td> </tr> <tr> <td> &#160; </td> <td style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 300; color: #000; line-height: 17px;\"> Adulto </td> </tr> <tr> <td> &#160; </td> <td style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 300; color: #000; line-height: 17px;\"> Numero biglietto: <xsl:for-each select=\"//a:rsPax[a:Name = 'MAX' and a:Surname = 'TEST']/a:Tickets/a:rsTicket[generate-id() = generate-id(key('distE-Ticket', substring(a:Number,1,13)))]\"> <xsl:value-of select=\"substring(a:Number,1,13)\" /> </xsl:for-each> </td> </tr> </table> </td> <td width=\"174\" style=\"vertical-align: top;\" class=\"collTable\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"text-align: left;\"> <tr> <td style=\"padding-right: 10px; width: 15px; font-family: Arial; font-size: 12px; font-weight: 700; color: #006643; text-transform: uppercase; line-height: 18px;\" width=\"15\"> 2 </td> <td style=\"font-family: Arial; font-size: 12px; font-weight: 700; color: #006643; text-transform: uppercase; line-height: 18px;\"> MED TEST </td> </tr> <tr> <td> &#160; </td> <td style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 300; color: #000; line-height: 17px;\"> Adulto </td> </tr> <tr> <td> &#160; </td> <td style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 300; color: #000; line-height: 17px;\"> Numero biglietto: <xsl:for-each select=\"//a:rsPax[a:Name = 'MED' and a:Surname = 'TEST']/a:Tickets/a:rsTicket[generate-id() = generate-id(key('distE-Ticket', substring(a:Number,1,13)))]\"> <xsl:value-of select=\"substring(a:Number,1,13)\" /> </xsl:for-each> </td> </tr> </table> </td> </tr> <tr/> </table> </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"> <tr> <td width=\"174\" style=\"vertical-align: top;\" class=\"collTable\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"text-align: left;\"> <tr> <td style=\"padding-right: 10px; width: 15px; font-family: Arial; font-size: 12px; font-weight: 700; color: #006643; text-transform: uppercase; line-height: 18px;\" width=\"15\"> 3 </td> <td style=\"font-family: Arial; font-size: 12px; font-weight: 700; color: #006643; text-transform: uppercase; line-height: 18px;\"> MIN TEST </td> </tr> <tr> <td> &#160; </td> <td style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 300; color: #000; line-height: 17px;\"> Bambino </td> </tr> <tr> <td> &#160; </td> <td style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 300; color: #000; line-height: 17px;\"> Numero biglietto: <xsl:for-each select=\"//a:rsPax[a:Name = 'MIN' and a:Surname = 'TEST']/a:Tickets/a:rsTicket[generate-id() = generate-id(key('distE-Ticket', substring(a:Number,1,13)))]\"> <xsl:value-of select=\"substring(a:Number,1,13)\" /> </xsl:for-each> </td> </tr> </table> </td> </tr> <tr/> </table> </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"> <tr/> <tr> <td width=\"174\" style=\"vertical-align: top;\" class=\"collTable\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"text-align: left;\"> <tr> <td style=\"padding-right: 10px; width: 15px; font-family: Arial; font-size: 12px; font-weight: 700; color: #006643; text-transform: uppercase; line-height: 18px;\" width=\"15\"> 4 </td> <td style=\"font-family: Arial; font-size: 12px; font-weight: 700; color: #006643; text-transform: uppercase; line-height: 18px;\"> NEO TEST </td> </tr> <tr> <td> &#160; </td> <td style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 300; color: #000; line-height: 17px;\"> Neonato </td> </tr> <tr> <td> &#160; </td> <td style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 300; color: #000; line-height: 17px;\"> Numero biglietto: <xsl:for-each select=\"//a:rsPax[a:Name = 'NEO' and a:Surname = 'TEST']/a:Tickets/a:rsTicket[generate-id() = generate-id(key('distE-Ticket', substring(a:Number,1,13)))]\"> <xsl:value-of select=\"substring(a:Number,1,13)\" /> </xsl:for-each> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr> <td height=\"13\" style=\"font-size: 0px; border-bottom: 1px solid #d1d1d1;\"> &#160; </td> </tr> <tr> <td height=\"19\" style=\"font-size: 0px;\"> &#160; </td> </tr> <tr> <td style=\"font-family: Arial; font-size: 12px; font-weight: 700; color: #006643; text-transform: uppercase;\"> Andata - diretto </td> </tr> <tr> <td height=\"8\" style=\"font-size: 0px;\"> &#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 700; color: #000; text-align: left; text-transform: uppercase; width: 100%;\" width=\"100%\"> <tr> <td width=\"73\" style=\"vertical-align: top;\"> AZ 1779<br /> </td> <td width=\"101\" style=\"vertical-align: top;\"> 15 giu 2015 </td> <td style=\"vertical-align: top;\" class=\"collTable\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 700; color: #000; text-align: left; text-transform: uppercase; width: 100%;\" width=\"100%\"> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 700; color: #000; text-align: left; text-transform: uppercase;\"> <tr> <td width=\"39\" style=\"vertical-align: top;\"> 12:30 </td> <td width=\"135\" style=\"vertical-align: top;\"> Roma Fiumicino </td> </tr> </table> </td> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 700; color: #000; text-align: left; text-transform: uppercase;\"> <tr> <td width=\"39\" style=\"vertical-align: top; text-transform: none;\"> 13:40 </td> <td width=\"135\" style=\"vertical-align: top;\"> Palermo </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr> <td height=\"4\" style=\"font-size: 0px;\"> &#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 11px; color: #000; text-align: left;\"> <tr> <td width=\"174\" class=\"spacerOperator\"> <font style=\"font-weight: 300; text-transform: none;\">Economy</font> </td> <td style=\"color: #006643;\"/> </tr> </table> </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 700; color: #000;\"> <tr> <td> <img src=\"http://booking.uat1.az/booking/_Content/images/email/seat.jpg\" alt=\"posto\" align=\"left\" /> </td> <td width=\"7\">&#160;</td> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 10px; font-weight: 700; color: #000;\"> <tr> <td width=\"14\" style=\"color:#006643; font-family: Arial; font-size: 11px; text-align: right;\">1</td> <td width=\"14\">&#160;</td> <td width=\"16\" style=\"text-align: left;\">8C</td> <td width=\"22\">&#160;</td> </tr> </table> </td> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 10px; font-weight: 700; color: #000;\"> <tr> <td width=\"14\" style=\"color:#006643; font-family: Arial; font-size: 11px; text-align: right;\">2</td> <td width=\"14\">&#160;</td> <td width=\"16\" style=\"text-align: left;\">8B</td> <td width=\"22\">&#160;</td> </tr> </table> </td> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 10px; font-weight: 700; color: #000;\"> <tr> <td width=\"14\" style=\"color:#006643; font-family: Arial; font-size: 11px; text-align: right;\">3</td> <td width=\"14\">&#160;</td> <td width=\"16\" style=\"text-align: left;\">8A</td> <td width=\"22\">&#160;</td> </tr> </table> </td> </tr> </table> </td> </tr> <tr> <td height=\"11\" style=\"font-size: 0px;\"> &#160; </td> </tr> <tr> <td style=\"font-family: Arial; font-size: 12px; font-weight: 700; color: #006643; text-transform: uppercase;\"> Ritorno - diretto </td> </tr> <tr> <td height=\"8\" style=\"font-size: 0px;\"> &#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 700; color: #000; text-align: left; text-transform: uppercase; width: 100%;\" width=\"100%\"> <tr> <td width=\"73\" style=\"vertical-align: top;\"> AZ 1792<br /> </td> <td width=\"101\" style=\"vertical-align: top;\"> 21 giu 2015 </td> <td style=\"vertical-align: top;\" class=\"collTable\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 700; color: #000; text-align: left; text-transform: uppercase; width: 100%;\" width=\"100%\"> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 700; color: #000; text-align: left; text-transform: uppercase;\"> <tr> <td width=\"39\" style=\"vertical-align: top;\"> 15:30 </td> <td width=\"135\" style=\"vertical-align: top;\"> Palermo </td> </tr> </table> </td> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 700; color: #000; text-align: left; text-transform: uppercase;\"> <tr> <td width=\"39\" style=\"vertical-align: top; text-transform: none;\"> 16:40 </td> <td width=\"135\" style=\"vertical-align: top;\"> Roma Fiumicino </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr> <td height=\"4\" style=\"font-size: 0px;\"> &#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 11px; color: #000; text-align: left;\"> <tr> <td width=\"174\" class=\"spacerOperator\"> <font style=\"font-weight: 300; text-transform: none;\">Economy</font> </td> <td style=\"color: #006643;\"/> </tr> </table> </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 11px; font-weight: 700; color: #000;\"> <tr> <td> <img src=\"http://booking.uat1.az/booking/_Content/images/email/seat.jpg\" alt=\"posto\" align=\"left\" /> </td> <td width=\"7\">&#160;</td> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 10px; font-weight: 700; color: #000;\"> <tr> <td width=\"14\" style=\"color:#006643; font-family: Arial; font-size: 11px; text-align: right;\">1</td> <td width=\"14\">&#160;</td> <td width=\"16\" style=\"text-align: left;\">9A</td> <td width=\"22\">&#160;</td> </tr> </table> </td> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 10px; font-weight: 700; color: #000;\"> <tr> <td width=\"14\" style=\"color:#006643; font-family: Arial; font-size: 11px; text-align: right;\">2</td> <td width=\"14\">&#160;</td> <td width=\"16\" style=\"text-align: left;\">9B</td> <td width=\"22\">&#160;</td> </tr> </table> </td> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Trebuchet MS'; font-size: 10px; font-weight: 700; color: #000;\"> <tr> <td width=\"14\" style=\"color:#006643; font-family: Arial; font-size: 11px; text-align: right;\">3</td> <td width=\"14\">&#160;</td> <td width=\"16\" style=\"text-align: left;\">9C</td> <td width=\"22\">&#160;</td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> <td width=\"33\"> &#160; </td> </tr> <tr> <td colspan=\"3\" height=\"15\" style=\"font-size: 0px;\">&#160;</td> </tr> </table> </td> </tr> <tr> <td height=\"28\" style=\"font-size: 0px;\"> &#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; text-align: left;\"> <tr> <td width=\"40\"> &#160; </td> <td width=\"522\" class=\"responsive\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; font-family: 'Trebuchet MS'; font-size: 11px; text-align: left;\"> <tr> <td style=\"font-family: Arial; font-size: 10px; font-weight: 700; color: #006643; text-transform: uppercase;\"> CONDIZIONI E REGOLE TARIFFARIE PER IL VOLO DI ANDATA - Tariffa: EconomyEasy EUR 396,23 </td> </tr> <tr> <td height=\"7\" style=\"font-size: 0px;\"> &#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; font-family: 'Trebuchet MS'; font-size: 11px; text-align: left;\" class=\"info\"> <tbody> <tr> <td width=\"251\" style=\"vertical-align: top;\" class=\"collTable\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"width: 100%; font-family: 'Trebuchet MS'; font-size: 10px; font-weight:700; text-align: left;\"> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> BAGAGLIO A MANO </td> <td/> <td style=\"color:#006643; font-weight:bold; text-transform:none; margin-left:5px; background: #fff;\" align=\"right\" width=\"15\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/checked.png\" alt=\"checked\" width=\"15\" height=\"15\" style=\"width: 15px; height: 15px;\" /> </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> BAGAGLIO IN STIVA </td> <td/> <td style=\"color: #006643; text-transform: none; font-weight: bold; margin-left: 5px; font-size: 10px; line-height: 17px;\" align=\"right\"> 1 X 23kg </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> SCELTA DEL POSTO </td> <td/> <td style=\"color:#006643; font-weight:bold; text-transform:none; margin-left:5px; background: #fff;\" align=\"right\" width=\"15\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/null.png\" alt=\"null\" width=\"15\" height=\"15\" style=\"width: 15px; height: 15px;\" /> </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> CASH&amp;MILES </td> <td/> <td style=\"color:#006643; font-weight:bold; text-transform:none; margin-left:5px; background: #fff;\" align=\"right\" width=\"15\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/checked.png\" alt=\"checked\" width=\"15\" height=\"15\" style=\"width: 15px; height: 15px;\" /> </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> CHECK-IN </td> <td/> <td style=\"color:#006643; font-weight:bold; text-transform:none; margin-left:5px; background: #fff;\" align=\"right\" width=\"15\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/checked.png\" alt=\"checked\" width=\"15\" height=\"15\" style=\"width: 15px; height: 15px;\" /> </td> </tr> </tbody> </table> </td> </tr> </table> </td> <td width=\"14\" class=\"collTable infoSpacer\"> &#160; </td> <td width=\"257\" style=\"vertical-align: top;\" class=\"collTable\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"width: 100%; font-family: 'Trebuchet MS'; font-size: 10px; font-weight: 700; text-align: left;\"> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> Cambio Prima della partenza </td> <td/> <td style=\"color: #006643; text-transform: none; font-weight: bold; margin-left: 5px; font-size: 10px; line-height: 17px;\" align=\"right\"> 50,00 EUR </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> Cambio Dopo la partenza </td> <td/> <td style=\"color: #006643; text-transform: none; font-weight: bold; margin-left: 5px; font-size: 10px; line-height: 17px;\" align=\"right\"> 50,00 EUR </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> Rimborso Prima della partenza </td> <td/> <td style=\"color:#006643; font-weight:bold; text-transform:none; margin-left:5px; background: #fff;\" align=\"right\" width=\"15\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/null.png\" alt=\"null\" width=\"15\" height=\"15\" style=\"width: 15px; height: 15px;\" /> </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> Rimborso Dopo la partenza </td> <td/> <td style=\"color:#006643; font-weight:bold; text-transform:none; margin-left:5px; background: #fff;\" align=\"right\" width=\"15\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/null.png\" alt=\"null\" width=\"15\" height=\"15\" style=\"width: 15px; height: 15px;\" /> </td> </tr> </tbody> </table> </td> </tr> </table> </td> </tr> </tbody> </table> </td> </tr> </table> </td> <td width=\"33\"> &#160; </td> </tr> </table> </td> </tr> <tr> <td height=\"16\" style=\"font-size: 0px;\"> &#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; text-align: left;\"> <tr> <td width=\"40\"> &#160; </td> <td width=\"522\" class=\"responsive\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; font-family: 'Trebuchet MS'; font-size: 11px; text-align: left;\"> <tr> <td style=\"font-family: Arial; font-size: 10px; font-weight: 700; color: #006643; text-transform: uppercase;\"> CONDIZIONI E REGOLE TARIFFARIE PER IL VOLO DI RITORNO - Tariffa: EconomyEasy EUR 376,83 </td> </tr> <tr> <td height=\"7\" style=\"font-size: 0px;\"> &#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; font-family: 'Trebuchet MS'; font-size: 11px; text-align: left;\" class=\"info\"> <tbody> <tr> <td width=\"251\" style=\"vertical-align: top;\" class=\"collTable\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"width: 100%; font-family: 'Trebuchet MS'; font-size: 10px; font-weight:700; text-align: left;\"> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> BAGAGLIO A MANO </td> <td/> <td style=\"color:#006643; font-weight:bold; text-transform:none; margin-left:5px; background: #fff;\" align=\"right\" width=\"15\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/checked.png\" alt=\"checked\" width=\"15\" height=\"15\" style=\"width: 15px; height: 15px;\" /> </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> BAGAGLIO IN STIVA </td> <td/> <td style=\"color: #006643; text-transform: none; font-weight: bold; margin-left: 5px; font-size: 10px; line-height: 17px;\" align=\"right\"> 1 X 23kg </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> SCELTA DEL POSTO </td> <td/> <td style=\"color:#006643; font-weight:bold; text-transform:none; margin-left:5px; background: #fff;\" align=\"right\" width=\"15\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/null.png\" alt=\"null\" width=\"15\" height=\"15\" style=\"width: 15px; height: 15px;\" /> </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> CASH&amp;MILES </td> <td/> <td style=\"color:#006643; font-weight:bold; text-transform:none; margin-left:5px; background: #fff;\" align=\"right\" width=\"15\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/checked.png\" alt=\"checked\" width=\"15\" height=\"15\" style=\"width: 15px; height: 15px;\" /> </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> CHECK-IN </td> <td/> <td style=\"color:#006643; font-weight:bold; text-transform:none; margin-left:5px; background: #fff;\" align=\"right\" width=\"15\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/checked.png\" alt=\"checked\" width=\"15\" height=\"15\" style=\"width: 15px; height: 15px;\" /> </td> </tr> </tbody> </table> </td> </tr> </table> </td> <td width=\"14\" class=\"collTable infoSpacer\"> &#160; </td> <td width=\"257\" style=\"vertical-align: top;\" class=\"collTable\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"width: 100%; font-family: 'Trebuchet MS'; font-size: 10px; font-weight: 700; text-align: left;\"> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> Cambio Prima della partenza </td> <td/> <td style=\"color: #006643; text-transform: none; font-weight: bold; margin-left: 5px; font-size: 10px; line-height: 17px;\" align=\"right\"> 50,00 EUR </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> Cambio Dopo la partenza </td> <td/> <td style=\"color: #006643; text-transform: none; font-weight: bold; margin-left: 5px; font-size: 10px; line-height: 17px;\" align=\"right\"> 50,00 EUR </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> Rimborso Prima della partenza </td> <td/> <td style=\"color:#006643; font-weight:bold; text-transform:none; margin-left:5px; background: #fff;\" align=\"right\" width=\"15\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/null.png\" alt=\"null\" width=\"15\" height=\"15\" style=\"width: 15px; height: 15px;\" /> </td> </tr> </tbody> </table> </td> </tr> <tr> <td style=\"height:15px; line-height:12px; border-bottom:1px dotted #878787; width:100%; padding-bottom: 0;\" height=\"15\"> <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: 'Trebuchet MS'; font-weight:bold;\" width=\"100%\" height=\"15\"> <tbody> <tr> <td style=\"background: #fff; font-size: 10px; text-transform: uppercase;\"> Rimborso Dopo la partenza </td> <td/> <td style=\"color:#006643; font-weight:bold; text-transform:none; margin-left:5px; background: #fff;\" align=\"right\" width=\"15\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/null.png\" alt=\"null\" width=\"15\" height=\"15\" style=\"width: 15px; height: 15px;\" /> </td> </tr> </tbody> </table> </td> </tr> </table> </td> </tr> </tbody> </table> </td> </tr> </table> </td> <td width=\"33\"> &#160; </td> </tr> </table> </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; text-align: left;\"> <tr> <td width=\"40\"> &#160; </td> <td width=\"522\" class=\"responsive\" height=\"16\" style=\"font-size: 0px; border-bottom: 1px solid #d1d1d1;\"> &#160; </td> <td width=\"33\"> &#160; </td> </tr> </table> </td> </tr> <tr> <td height=\"16\" style=\"font-size: 0px;\"> &#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; text-align: left;\"> <tr> <td width=\"40\"> &#160; </td> <td width=\"522\" class=\"responsive\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; text-align: left;\"> <tr> <td width=\"243\" class=\"collTable\"> <table style=\"border-collapse: collapse; font-family: 'Trebuchet MS'; font-size: 10px; font-weight: 700;\" border=\"0\" cellspacing=\"0\" cellpadding=\"3px\" width=\"100%\"> <tbody> <tr> <td align=\"left\" style=\"text-transform: uppercase;\"> 2 Tariffa adulti </td> <td width=\"25\" align=\"right\" style=\"text-transform: uppercase;\"> EUR </td> <td align=\"right\" width=\"50\" style=\"text-transform: uppercase; width: 50px;\"> 308,00 </td> </tr> <tr> <td align=\"left\" style=\"text-transform: uppercase;\"> 1 Tariffa bambini </td> <td width=\"25\" align=\"right\" style=\"text-transform: uppercase;\"> EUR </td> <td align=\"right\" width=\"50\" style=\"text-transform: uppercase; width: 50px;\"> 104,00 </td> </tr> <tr> <td align=\"left\" style=\"text-transform: uppercase;\"> 1 Tariffa neonati </td> <td width=\"25\" align=\"right\" style=\"text-transform: uppercase;\"> EUR </td> <td align=\"right\" width=\"50\" style=\"text-transform: uppercase; width: 50px;\"> 40,00 </td> </tr> <tr> <td align=\"left\" style=\"text-transform: uppercase;\"> Tasse </td> <td width=\"25\" align=\"right\" style=\"text-transform: uppercase;\"> EUR </td> <td align=\"right\" width=\"50\" style=\"text-transform: uppercase; width: 50px;\"> 153,06 </td> </tr> <tr> <td align=\"left\" style=\"text-transform: uppercase;\"> Supplemento carburante </td> <td width=\"25\" align=\"right\" style=\"text-transform: uppercase;\"> EUR </td> <td align=\"right\" width=\"50\" style=\"text-transform: uppercase; width: 50px;\"> 168,00 </td> </tr> <tr> <td colspan=\"3\" style=\"border-top: 1px dotted #999; font-size: 0;\" width=\"100%\" height=\"1\"> &#160; </td> </tr> <tr> <td style=\"text-transform: uppercase; padding: 0;\"> <span class=\"no_uppercased\" xmlns=\"http://www.w3.org/1999/xhtml\">Hai pagato con</span> Visa </td> <td colspan=\"2\" rowspan=\"2\" align=\"right\" style=\"vertical-align: bottom;\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/CreditCards_Visa.png\" /> </td> </tr> <tr> <td style=\"text-transform: uppercase;\"> <font style=\"letter-spacing: 2px;\">*</font> <font style=\"letter-spacing: 2px;\">*</font> <font style=\"letter-spacing: 2px;\">*</font> <font style=\"letter-spacing: 2px;\">*</font> <font style=\"letter-spacing: 2px;\">*</font> <font style=\"letter-spacing: 2px;\">*</font> <font style=\"letter-spacing: 2px;\">*</font> <font style=\"letter-spacing: 2px;\">*</font> <font style=\"letter-spacing: 2px;\">*</font> <font style=\"letter-spacing: 2px;\">*</font> <font style=\"letter-spacing: 2px;\">*</font> 1111 </td> </tr> </tbody> </table> </td> <td width=\"36\" class=\"collTable infoSpacer\"> &#160; </td> <td width=\"243\" class=\"collTable\" style=\"vertical-align: bottom;\"> <table style=\"border-collapse: collapse; font-family: 'Trebuchet MS'; font-size: 10px; font-weight: 700;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> <tbody> <tr> <td align=\"right\" style=\"font-size: 10px; font-family: 'Trebuchet MS'; text-transform: uppercase; font-weight: 700;\"> TOTALE </td> </tr> <tr> <td align=\"right\" style=\"width: 233px; color: #006643; font-weight: 700; font-family: 'Trebuchet MS'; font-size: 25px; text-transform: uppercase;\"> EUR 773,<font style=\"font-size: 16px;\">06</font> </td> </tr> </tbody> </table> </td> </tr> </table> </td> <td width=\"33\"> &#160; </td> </tr> </table> </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; text-align: left;\"> <tr> <td width=\"40\"> &#160; </td> <td width=\"522\" class=\"responsive\" height=\"16\" style=\"font-size: 0px; border-bottom: 1px solid #d1d1d1;\"> &#160; </td> <td width=\"33\"> &#160; </td> </tr> </table> </td> </tr> <tr> <td height=\"17\" style=\"font-size: 0px;\"> &#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; text-align: left;\"> <tr> <td width=\"40\"> &#160; </td> <td width=\"522\" class=\"responsive\" style=\"font-family: 'Trebuchet MS'; font-size: 11px; color: #000;\"> Per ulteriori informazioni contatta il nostro Customer Center al numero 06 65640<br /> La tariffa e la prenotazione sono valide per un biglietto utilizzato integralmente, rispettando l&#8217;ordine di successione dei voli per il viaggio nelle date indicate. Le penali si applicano per ciascun passeggero/biglietto. Il cambio potrebbe comportare un&#8217;integrazione tariffaria se la tariffa precedentemente acquistata non &#232; pi&#249; disponibile. Il cambio, se consentito, va richiesto prima della partenza del volo che si desidera cambiare. </td> <td width=\"33\"> &#160; </td> </tr> </table> </td> </tr> <tr> <td height=\"13\" style=\"font-size: 0px;\">&#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; text-align: left;\"> <tr> <td width=\"40\">&#160; </td> <td width=\"522\" class=\"responsive\" style=\"font-family: Arial; font-weight: 700; font-size: 13px; color: #006643; text-transform: uppercase;\"> ti aspettiamo! </td> <td width=\"33\">&#160; </td> </tr> </table> </td> </tr> <tr> <td height=\"31\" style=\"font-size: 0px;\" class=\"noMobile\">&#160; </td> </tr> <tr> <td> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; text-align: left;\" class=\"noMobile\"> <tr> <td width=\"40\">&#160; </td> <td width=\"522\"> <table> <tbody> <tr> <td bgcolor=\"#f4f4f4\" width=\"170\" height=\"189\"> <span xmlns=\"http://www.w3.org/1999/xhtml\"/> </td> <td width=\"6\">&#160;</td> <td bgcolor=\"#f4f4f4\" width=\"170\" height=\"189\"> <span xmlns=\"http://www.w3.org/1999/xhtml\"/> </td> <td width=\"6\">&#160;</td> <td bgcolor=\"#f4f4f4\" width=\"170\" height=\"189\"> <span xmlns=\"http://www.w3.org/1999/xhtml\"/> </td> </tr> </tbody> </table> </td> <td width=\"33\">&#160; </td> </tr> </table> </td> </tr> <tr> <td height=\"29\" style=\"font-size: 0px;\">&#160; </td> </tr> <tr> <td height=\"28\" bgcolor=\"#006643\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; font-family: Arial;\"> <tr> <td width=\"40\" style=\"width: 40px;\">&#160;</td> <td align=\"left\" width=\"60%\"> <a href=\"http://www.alitalia.com/it_it/\" style=\"font-weight: 700; color: #fff; text-decoration: none; font-size: 14px;\">alitalia.com</a> </td> <td align=\"right\" valign=\"bottom\"> <table> <tr> <td style=\"padding-right: 13px;\"> <a href=\"https://www.facebook.com/alitalia\" style=\"border: 0; text-decoration: none;\" title=\"facebook\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/NS-f.jpg\" alt=\"facebook\" style=\"vertical-align: text-bottom; border: 0;\" /> </a> </td> <td style=\"padding-right: 12px;\"> <a href=\"https://twitter.com/Alitalia\" style=\"border: 0; text-decoration: none;\" title=\"twitter\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/NS-t.jpg\" alt=\"twitter\" style=\"vertical-align: text-bottom; border: 0;\" /> </a> </td> <td> <a href=\"https://plus.google.com/113375153773105297464/posts\" style=\"border: 0; text-decoration: none;\" title=\"google plus\"> <img src=\"http://booking.uat1.az/booking/_Content/images/email/NS-g.jpg\" alt=\"gplus\" style=\"vertical-align: text-bottom; border: 0;\" /> </a> </td> </tr> </table> </td> <td width=\"33\" style=\"width: 33px;\">&#160;</td> </tr> </table> </td> </tr> <tr> <td height=\"20\" style=\"font-size: 0px;\">&#160; </td> </tr> </tbody> </table> </td> </tr> </table> </body> </html> </xsl:template> </xsl:stylesheet>");
		paymentData.setXsltMail(xsltMail.toString());
		
		
		
	    prenotation.setPayment(paymentData);
	    
	    
	    List<RouteData> routesList = new ArrayList<RouteData>();
	    RouteData route = new RouteData();
	    ArrayList<FlightData> flights = new ArrayList<FlightData>();
	    DirectFlightData flightData = new DirectFlightData();
	    flightData.setProperties(null);
	    
	    List<BrandData> brands = new ArrayList<BrandData>();
	    BrandData brandData = new BrandData();
	    brandData.setProperties(null);
	    brandData.setSolutionId("Xdp8yxqK8TAUF6mh8UQaCS001");
	    brandData.setRefreshSolutionId(null);
	    brandData.setBaggageAllowanceList(null);
	    brandData.setBestFare(true);
	    brandData.setCode("EconomyEasy");
	    brandData.setCompartimentalClass(null);
	    brandData.setCurrency("EUR");
	    brandData.setEnabled(true);
	    brandData.setGrossFare(new BigDecimal(396.23));
	    brandData.setIndex(0);
	    List<String> notes = new ArrayList<String>();
	    notes.add("NONREFUNDABLE-FARE");
	    notes.add("PENALTY-FARE");
		brandData.setNotes(notes);
	    
	    BrandPenaltiesData penalties = new BrandPenaltiesData();
	    BrandPenaltyData changeAfterDepature = new BrandPenaltyData();
	    changeAfterDepature.setMaxPrice(new BigDecimal(50.0));
	    changeAfterDepature.setMaxPriceCurrency("EUR");
	    changeAfterDepature.setMinPrice(new BigDecimal(50.0));
	    changeAfterDepature.setMinPriceCurrency("EUR");
	    changeAfterDepature.setPermitted(true);
		penalties.setChangeAfterDepature(changeAfterDepature);
		
		BrandPenaltyData changeBeforeDepature = new BrandPenaltyData();
		changeBeforeDepature.setMaxPrice(new BigDecimal(50.0));
		changeBeforeDepature.setMaxPriceCurrency("EUR");
		changeBeforeDepature.setMinPrice(new BigDecimal(50.0));
		changeBeforeDepature.setMinPriceCurrency("EUR");
		changeBeforeDepature.setPermitted(true);
		penalties.setChangeBeforeDepature(changeBeforeDepature);
		
		BrandPenaltyData refundAfterDepature = new BrandPenaltyData();
		refundAfterDepature.setMaxPrice(new BigDecimal(0));
		refundAfterDepature.setMaxPriceCurrency(null);
		refundAfterDepature.setMinPrice(new BigDecimal(0));
		refundAfterDepature.setMinPriceCurrency(null);
		refundAfterDepature.setPermitted(false);
		penalties.setRefundAfterDepature(refundAfterDepature);
		
		BrandPenaltyData refundBeforeDepature = new BrandPenaltyData();
		refundBeforeDepature.setMaxPrice(new BigDecimal(0));
		refundBeforeDepature.setMaxPriceCurrency(null);
		refundBeforeDepature.setMinPrice(new BigDecimal(0));
		refundBeforeDepature.setMinPriceCurrency(null);
		refundBeforeDepature.setPermitted(false);
		penalties.setRefundBeforeDepature(refundBeforeDepature);
		
		brandData.setPenalties(penalties);
	    brandData.setRealPrice(new BigDecimal(396.23));
	    brandData.setSeatsAvailable(0);
	    brandData.setSelected(true);
	    brandData.setAdvancePurchaseDays(0);
		brands.add(brandData);
		
		
		//----
		brandData = new BrandData();
	    brandData.setProperties(null);
	    brandData.setSolutionId("7jUgbLfWJ6OMSb67KhUp41002");
	    brandData.setRefreshSolutionId(null);
	    brandData.setBaggageAllowanceList(null);
	    brandData.setBestFare(false);
	    brandData.setCode("EconomyComfort");
	    brandData.setCompartimentalClass(null);
	    brandData.setCurrency("EUR");
	    brandData.setEnabled(true);
	    brandData.setGrossFare(new BigDecimal(760.23));
	    brandData.setIndex(0);
	    brandData.setNetFare(new BigDecimal(552.67));
		brandData.setNotes(null);
	    
	    penalties = new BrandPenaltiesData();
	    changeAfterDepature = new BrandPenaltyData();
	    changeAfterDepature.setMaxPrice(new BigDecimal(25.0));
	    changeAfterDepature.setMaxPriceCurrency("EUR");
	    changeAfterDepature.setMinPrice(new BigDecimal(25.0));
	    changeAfterDepature.setMinPriceCurrency("EUR");
	    changeAfterDepature.setPermitted(true);
		penalties.setChangeAfterDepature(changeAfterDepature);
		
		changeBeforeDepature = new BrandPenaltyData();
		changeBeforeDepature.setMaxPrice(new BigDecimal(25.0));
		changeBeforeDepature.setMaxPriceCurrency("EUR");
		changeBeforeDepature.setMinPrice(new BigDecimal(25.0));
		changeBeforeDepature.setMinPriceCurrency("EUR");
		changeBeforeDepature.setPermitted(true);
		penalties.setChangeBeforeDepature(changeBeforeDepature);
		
		refundAfterDepature = new BrandPenaltyData();
		refundAfterDepature.setMaxPrice(new BigDecimal(0));
		refundAfterDepature.setMaxPriceCurrency(null);
		refundAfterDepature.setMinPrice(new BigDecimal(0));
		refundAfterDepature.setMinPriceCurrency(null);
		refundAfterDepature.setPermitted(false);
		penalties.setRefundAfterDepature(refundAfterDepature);
		
		refundBeforeDepature = new BrandPenaltyData();
		refundBeforeDepature.setMaxPrice(new BigDecimal(50.00));
		refundBeforeDepature.setMaxPriceCurrency("EUR");
		refundBeforeDepature.setMinPrice(new BigDecimal(50.00));
		refundBeforeDepature.setMinPriceCurrency("EUR");
		refundBeforeDepature.setPermitted(true);
		penalties.setRefundBeforeDepature(refundBeforeDepature);
		
		brandData.setPenalties(penalties);
	    brandData.setRealPrice(new BigDecimal(0));
	    brandData.setSeatsAvailable(0);
	    brandData.setSelected(false);
	    brandData.setAdvancePurchaseDays(0);
		brands.add(brandData);
		//-----
		brandData = new BrandData();
	    brandData.setProperties(null);
	    brandData.setSolutionId("7jUgbLfWJ6OMSb67KhUp41003");
	    brandData.setRefreshSolutionId(null);
	    brandData.setBaggageAllowanceList(null);
	    brandData.setBestFare(false);
	    brandData.setCode("EconomyFreedom");
	    brandData.setCompartimentalClass(null);
	    brandData.setCurrency("EUR");
	    brandData.setEnabled(true);
	    brandData.setGrossFare(new BigDecimal(1024.23));
	    brandData.setIndex(0);
	    brandData.setNetFare(new BigDecimal(817.00));
		brandData.setNotes(null);
	    
	    penalties = new BrandPenaltiesData();
	    changeAfterDepature = new BrandPenaltyData();
	    changeAfterDepature.setMaxPrice(new BigDecimal(0.0));
	    changeAfterDepature.setMaxPriceCurrency("EUR");
	    changeAfterDepature.setMinPrice(new BigDecimal(0.0));
	    changeAfterDepature.setMinPriceCurrency("EUR");
	    changeAfterDepature.setPermitted(true);
		penalties.setChangeAfterDepature(changeAfterDepature);
		
		changeBeforeDepature = new BrandPenaltyData();
		changeBeforeDepature.setMaxPrice(new BigDecimal(0.0));
		changeBeforeDepature.setMaxPriceCurrency("EUR");
		changeBeforeDepature.setMinPrice(new BigDecimal(0.0));
		changeBeforeDepature.setMinPriceCurrency("EUR");
		changeBeforeDepature.setPermitted(true);
		penalties.setChangeBeforeDepature(changeBeforeDepature);
		
		refundAfterDepature = new BrandPenaltyData();
		refundAfterDepature.setMaxPrice(new BigDecimal(0));
		refundAfterDepature.setMaxPriceCurrency("USD");
		refundAfterDepature.setMinPrice(new BigDecimal(0));
		refundAfterDepature.setMinPriceCurrency("USD");
		refundAfterDepature.setPermitted(true);
		penalties.setRefundAfterDepature(refundAfterDepature);
		
		refundBeforeDepature = new BrandPenaltyData();
		refundBeforeDepature.setMaxPrice(new BigDecimal(0.00));
		refundBeforeDepature.setMaxPriceCurrency("USD");
		refundBeforeDepature.setMinPrice(new BigDecimal(0.00));
		refundBeforeDepature.setMinPriceCurrency("USD");
		refundBeforeDepature.setPermitted(true);
		penalties.setRefundBeforeDepature(refundBeforeDepature);
		
		brandData.setPenalties(penalties);
	    brandData.setRealPrice(new BigDecimal(0));
	    brandData.setSeatsAvailable(0);
	    brandData.setSelected(false);
	    brandData.setAdvancePurchaseDays(0);
		brands.add(brandData);
		
		//------
		flightData.setBrands(brands);
		//flightData.setD //FIXME [testcode] manca il duration???
		flightData.setFlightType(FlightTypeEnum.DIRECT);
		Calendar arrivalDate = Calendar.getInstance();
		arrivalDate.set(2015, 06, 15, 13, 40);
		flightData.setArrivalDate(arrivalDate);
	    
		ArrayList<BaggageAllowanceData> baggageAllowanceList = new ArrayList<BaggageAllowanceData>();
	    BaggageAllowanceData baggageAllowanceData = new BaggageAllowanceData();
	    baggageAllowanceData.setCode("0GO");
	    baggageAllowanceData.setCount(1);
	    baggageAllowanceList.add(baggageAllowanceData);
		flightData.setBaggageAllowanceList(baggageAllowanceList);
		flightData.setCabin(CabinEnum.ECONOMY);
		flightData.setCarrier("AZ");
		flightData.setCarrierForLogo("AZ");
		flightData.setDurationHour(1);
		flightData.setDurationMinutes(10);
		ArrayList<String> compartimentalClass = new ArrayList<String>();
		compartimentalClass.add("Q");
		flightData.setCompartimentalClass(compartimentalClass);
		Calendar departureDate = Calendar.getInstance();
		departureDate.set(2015, 06, 15, 12, 30, 0);
		flightData.setDepartureDate(departureDate);
		FlightDetailsData details = new FlightDetailsData();
		details.setAeromobile("32S");
		details.setArrivalTerminal(null);
		details.setDepartureTerminal("1");
		details.setDistance("254");
		details.setOtherInformation(null);
		flightData.setDetails(details);
		flightData.setEnabledSeatMap(true);
		flightData.setFlightNumber("1779");
		
		AirportData from = new AirportData();
		from.setAzDeserves(true);
		from.setArea(AreaValueEnum.DOM);
		from.setCity("Roma");
		from.setCityCode("ROM");
		from.setCode("FCO");
		from.setCountry("Italia");
		from.setCountryCode("IT");
		from.setDescription("Roma, Fiumicino, Italia");
		from.setDetails(null);
		from.setEligibility(true);
		from.setEtktAvlblFrom(null);
		from.setHiddenInFirstDeparture(false);
		from.setIdMsg(1954);
		from.setName("Fiumicino");
		from.setState(null);
		from.setStateCode(null);
		from.setUnavlblFrom(null);
		from.setUnavlblUntil(null);
		flightData.setFrom(from);
		
		flightData.setMarriageGroup("0");
		flightData.setMileage(0L);
		flightData.setOperationalCarrier(null);
		flightData.setOperationalFlightNumber(null);
		flightData.setOperationalFlightText(null);
		flightData.setStopOver(false);
		
		AirportData to = new AirportData();
		to.setAzDeserves(true);
		to.setArea(AreaValueEnum.DOM);
		to.setCity("Palermo");
		to.setCityCode("PMO");
		to.setCode("PMO");
		to.setCountry("Italia");
		to.setCountryCode("IT");
		to.setDescription("Palermo, Palermo, Italia");
		to.setDetails(null);
		to.setEligibility(true);
		to.setEtktAvlblFrom(null);
		to.setHiddenInFirstDeparture(false);
		to.setIdMsg(0);
		to.setName("Palermo");
		to.setState(null);
		to.setStateCode(null);
		to.setUnavlblFrom(null);
		to.setUnavlblUntil(null);
		flightData.setTo(to);
		flightData.setUrlConditions(null);
		flights.add(flightData);
		
		
	    flights.add(flightData);
		route.setIndex(0);
	    route.setFlights(flights );
	    route.setType(RouteTypeEnum.OUTBOUND);
	    routesList.add(route);
	    
		prenotation.setRoutesList(routesList);
	    prenotation.setSliceCount(2);
	    
		InitializePaymentRequest initializePaymentRequest = new InitializePaymentRequest();
	    initializePaymentRequest.setCarnetInfo(carnetInfo);
	    initializePaymentRequest.setIpAddress(ipAddress);
	    initializePaymentRequest.setPrenotation(prenotation);
	    initializePaymentRequest.setUserAgent(userAgent);
	    
	    //prenotation.getProperties().put("BookingDetails", new ResultBookingDetails());
		
	    InitializePaymentResponse response1 = paymentDelegate.initializePayment(initializePaymentRequest);
	    
	    PrintWriter writer = response.getWriter();
	    writer.write(response1.getPaymentData().toString());	
	    
	    
	    
		
	}
}
