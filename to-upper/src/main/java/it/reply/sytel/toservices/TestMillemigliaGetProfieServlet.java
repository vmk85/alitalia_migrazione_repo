package it.reply.sytel.toservices;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.home.ProfileRequest;
import com.alitalia.aem.common.messages.home.ProfileResponse;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(name = "TestMillemigliaGetProfieServlet", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({ @Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "sling.servlet.selectors", value = { "testgetprofile" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "sling.servlet.methods", value = { "GET" }) })
public class TestMillemigliaGetProfieServlet extends SlingSafeMethodsServlet {

	private static final Logger logger = LoggerFactory.getLogger(TestMillemigliaGetProfieServlet.class);
	private static final long serialVersionUID = 1L;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ConsumerLoginDelegate loginDelegate;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		try {
			logger.info("Executing doGet.");

			ProfileRequest profileRequest = new ProfileRequest();
			profileRequest.setSid("S12345");
			profileRequest.setTid("T12345");
			MMCustomerProfileData customerProfile = new MMCustomerProfileData();
			customerProfile.setCustomerNumber("7426528");
			customerProfile.setCustomerPinCode("3655");
			profileRequest.setCustomerProfile(customerProfile);
			
			ProfileResponse profileResponse = loginDelegate.getProfile(profileRequest);
			
			response.setContentType("application/json");
			TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
			out.object().key("JSON").value(profileResponse).endObject();

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception while executing doGet.", e);
		}
		logger.info("Executed doGet.");
	}
}