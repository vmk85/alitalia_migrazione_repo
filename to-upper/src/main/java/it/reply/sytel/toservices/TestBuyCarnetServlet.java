package it.reply.sytel.toservices;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.carnet.CarnetCreditCardProviderData;
import com.alitalia.aem.common.data.home.carnet.CarnetPaymentData;
import com.alitalia.aem.common.data.home.carnet.CarnetPrenotationData;
import com.alitalia.aem.common.messages.home.CarnetBuyCarnetRequest;
import com.alitalia.aem.common.messages.home.CarnetBuyCarnetResponse;
import com.alitalia.aem.web.component.carnet.delegate.CarnetDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(name = "TestBuyCarnetServlet", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({ @Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "sling.servlet.selectors", value = { "testbuycarnet" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "sling.servlet.methods", value = { "GET" }) })
public class TestBuyCarnetServlet extends SlingSafeMethodsServlet {


	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(TestBuyCarnetServlet.class);

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile CarnetDelegate carnetDelegate;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException,
			IOException {
		try {
			logger.info("Executing doGet.");
			logger.info("contextPath: " + request.getContextPath());
			logger.info("localeAddr: " + request.getLocalAddr());
			logger.info("localeName: " + request.getLocalName());
			logger.info("localPort: " + request.getLocalPort());
			logger.info("sceheme: " + request.getScheme());
			logger.info("servername: " + request.getServerName());
			logger.info("serverport: " + request.getServerPort());

			response.setContentType("application/json");

			CarnetBuyCarnetRequest servletRequest = new CarnetBuyCarnetRequest("123456789", "abcdefgh");
			servletRequest.setLanguageCode("IT");
			servletRequest.setMarketCode("IT");
			CarnetPrenotationData prenotation = new CarnetPrenotationData();
			prenotation.setCarnetCode("1234");
			prenotation.setPassword("abcd");
			prenotation.setBuyer(null);
			CarnetPaymentData payData = new CarnetPaymentData();
			payData.setCurrency("EUR");
			CarnetCreditCardProviderData provider = new CarnetCreditCardProviderData();
			provider.setCvv("123");
			provider.setCreditCardNumber("1234567812345678");
			payData.setProvider(provider);
			prenotation.setPayment(payData);
			servletRequest.setPrenotation(prenotation);
			
			logger.info("Carnet buyCarnet Request: " + servletRequest);
			CarnetBuyCarnetResponse servletResponse = carnetDelegate.buyCarnet(servletRequest);
			logger.info("Carnet buyCarnet Response: " + servletResponse);
			
			TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
			out.object().key("JSON").value("OK").endObject();

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception while executing doGet.", e);
		}

		logger.info("Executed doGet.");
	}

}
