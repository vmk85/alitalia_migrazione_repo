package it.reply.sytel.toservices;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.PassengerBase;
import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.data.home.AvailableFlightsData;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.BrandSearchData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.MmCustomerData;
import com.alitalia.aem.common.data.home.PassengerNumbersData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.common.data.home.SearchDestinationData;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.CabinTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.GenderTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.SearchTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.TimeTypeEnum;
import com.alitalia.aem.common.messages.home.CashAndMilesData;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionResponse;
import com.alitalia.aem.common.messages.home.UpdateWidgetCashMilesRequest;
import com.alitalia.aem.common.messages.home.UpdateWidgetCashMilesResponse;
import com.alitalia.aem.common.messages.home.enumeration.CashMilesOperationType;
import com.alitalia.aem.common.messages.home.enumeration.SearchExecuteResponseType;
import com.alitalia.aem.service.api.home.BookingWidgetService;
import com.alitalia.aem.web.component.searchflights.delegate.SearchFlightsDelegate;
import com.alitalia.aem.web.component.widget.delegate.BookingWidgetDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(name = "TestCashMilesServlet", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
	@Property(name = "sling.servlet.selectors", value = {"testcashmiles"}),
	@Property(name = "sling.servlet.extensions", value = {"json"}),
	@Property(name = "sling.servlet.methods", value = {"GET"})
})
public class TestCashMilesServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;	
	private static final Logger logger = LoggerFactory.getLogger(TestCashMilesServlet.class);
	

	@Reference(policy=ReferencePolicy.DYNAMIC)
	private volatile BookingWidgetDelegate widgetDelegate;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		
		try {
			logger.info("Executing doGet.");
			
			response.setContentType("application/json");
			String pnr = request.getParameter("PNR");
			String operation = request.getParameter("Operation");
			UpdateWidgetCashMilesRequest updateCMReq = new UpdateWidgetCashMilesRequest("TID", "SID");

			MmCustomerData mmCustomer = new MmCustomerData();
			Calendar birthDate = Calendar.getInstance();
			birthDate.set(1992, 02, 04);
			mmCustomer.setBirthDate(birthDate);
			mmCustomer.setCode("0000994259");
			mmCustomer.setLastName("CAPITINI");
			mmCustomer.setGender(GenderTypeEnum.MALE);
			mmCustomer.setIgnore(false);
			mmCustomer.setMilesBalance(new Integer(0));
			mmCustomer.setMilesEarned(new Integer(0));
			mmCustomer.setMilesQualify(new Integer(0));
			mmCustomer.setPin("2408");

			CashAndMilesData cashMiles = new CashAndMilesData();
			cashMiles.setMmCustomer(mmCustomer);
			cashMiles.setDiscountAmount(new BigDecimal(25));
			cashMiles.setUsedMileage(new Integer(5000));

			Calendar paymentDate = Calendar.getInstance();
			paymentDate.setTime(new Date());
			cashMiles.setPaymentDate(paymentDate);

			updateCMReq.setOperationType(CashMilesOperationType.fromValue(operation));
			updateCMReq.setPnr(pnr);
			updateCMReq.setSite("IT");
			updateCMReq.setCashMiles(cashMiles);
			UpdateWidgetCashMilesResponse updateCMResp = widgetDelegate.updateWidgetCashMiles(updateCMReq);

			TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
			out.object();
			
			if (updateCMResp != null && updateCMResp.isSucceded()) {
				out.key("newAvailableMilesBalance").value(updateCMResp.getCashMiles().getMmCustomer().getMilesBalance());
			}
			else
				out.key("result").value("error in update miles balance");

			out.endObject();
		
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			logger.error("Exception while executing doGet.", e);
		}
		
		logger.info("Executed doGet.");
	}
	
	
}
