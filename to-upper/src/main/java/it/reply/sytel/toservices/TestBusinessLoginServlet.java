package it.reply.sytel.toservices;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.AlitaliaTradeAgencyData;
import com.alitalia.aem.common.data.home.enumerations.AlitaliaTradeUserType;
import com.alitalia.aem.common.messages.home.AgencyLoginRequest;
import com.alitalia.aem.common.messages.home.AgencyLoginResponse;
import com.alitalia.aem.web.component.businesslogin.delegate.BusinessLoginDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(name = "TestBusinessLogin", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
	@Property(name = "sling.servlet.selectors", value = {"testbusinesslogin"}),
	@Property(name = "sling.servlet.extensions", value = {"json"}),
	@Property(name = "sling.servlet.methods", value = {"GET"})
})
public class TestBusinessLoginServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;	
	private static final Logger logger = LoggerFactory.getLogger(TestBusinessLoginServlet.class);
	
//	@Reference
//	private SearchFlightsDelegate searchFlightsDelegate;

	@Reference
	private BusinessLoginDelegate businessLoginDelegate;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		
		try {
			logger.info("Executing doGet.");
			
			response.setContentType("application/json");
			
			String valueRuolo = request.getParameter("ruolo");
			String valueAgenzia = request.getParameter("agenzia");
			String valuePassword = request.getParameter("password");

			AgencyLoginRequest loginRequest = new AgencyLoginRequest("1249812", "gsiweug");
			loginRequest.setRuolo(AlitaliaTradeUserType.fromValue(valueRuolo));
			loginRequest.setCodiceAgenzia(valueAgenzia);
			loginRequest.setPassword(valuePassword);
			AgencyLoginResponse loginResponse = businessLoginDelegate.agencyLogin(loginRequest);
			AlitaliaTradeAgencyData agencyData = null;
			if (loginResponse != null) {
				logger.info("Ha funzionato: " + loginResponse.toString());
				agencyData = loginResponse.getAgencyData();
			}

			TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
			out.object();
			
			if (agencyData != null) {
				out.key("result").value(agencyData.getEmailAlitalia());
				out.key("iataAgency").value(agencyData.isIataAgency());
				out.key("groupEnabled").value(agencyData.isGroupEnabled());
			}
			else
				out.key("result").value("invalid login");

			out.endObject();
		
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			logger.error("Exception while executing doGet.", e);
		}
		
		logger.info("Executed doGet.");
	}
	
	
}
