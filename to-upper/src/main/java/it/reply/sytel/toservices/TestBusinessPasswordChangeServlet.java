package it.reply.sytel.toservices;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.enumerations.AlitaliaTradeUserType;
import com.alitalia.aem.common.messages.home.AgencyPasswordChangeRequest;
import com.alitalia.aem.common.messages.home.AgencyPasswordChangeResponse;
import com.alitalia.aem.web.component.businesslogin.delegate.BusinessLoginDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(name = "TestBusinessPasswordChange", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
	@Property(name = "sling.servlet.selectors", value = {"testbusinesspasswordchange"}),
	@Property(name = "sling.servlet.extensions", value = {"json"}),
	@Property(name = "sling.servlet.methods", value = {"GET"})
})
public class TestBusinessPasswordChangeServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;	
	private static final Logger logger = LoggerFactory.getLogger(TestBusinessPasswordChangeServlet.class);
	
//	@Reference
//	private SearchFlightsDelegate searchFlightsDelegate;

	@Reference
	private BusinessLoginDelegate businessLoginDelegate;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		
		try {
			logger.info("Executing doGet.");
			
			response.setContentType("application/json");
			
			String valueRuolo = request.getParameter("ruolo");
			String valueAgenzia = request.getParameter("agenzia");
			String valueOldPassword = request.getParameter("oldPassword");
			String valueNewPassword = request.getParameter("newPassword");

			AgencyPasswordChangeRequest changePasswordRequest = new AgencyPasswordChangeRequest("1249812", "gsiweug");
			changePasswordRequest.setRuolo(AlitaliaTradeUserType.fromValue(valueRuolo));
			changePasswordRequest.setCodiceAgenzia(valueAgenzia);
			changePasswordRequest.setOldPassword(valueOldPassword);
			changePasswordRequest.setNewPassword(valueNewPassword);
			AgencyPasswordChangeResponse changePasswordResponse = businessLoginDelegate.changePassword(changePasswordRequest);
			if (changePasswordResponse != null)
				logger.info("Ha funzionato: " + changePasswordResponse.toString());

			TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
			out.object();
			
			if ((changePasswordResponse != null) && changePasswordResponse.isOperationSuccessful())
				out.key("result").value("password changed");
			else
				out.key("result").value("error changing password");

			out.endObject();
		
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			logger.error("Exception while executing doGet.", e);
		}
		
		logger.info("Executed doGet.");
	}
	
	
}
