package it.reply.sytel.toservices;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.data.home.ApplicantPassengerData;
import com.alitalia.aem.common.data.home.BaggageAllowanceData;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.BrandPenaltiesData;
import com.alitalia.aem.common.data.home.BrandPenaltyData;
import com.alitalia.aem.common.data.home.ContactData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.FlightDetailsData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.PassengerBaseInfoData;
import com.alitalia.aem.common.data.home.PhonePrefixData;
import com.alitalia.aem.common.data.home.PreferencesData;
import com.alitalia.aem.common.data.home.SeatPreferencesData;
import com.alitalia.aem.common.data.home.enumerations.ARTaxInfoTypesEnum;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.ContactTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.FlightTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.GenderTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.common.messages.home.LoadWidgetECouponRequest;
import com.alitalia.aem.common.messages.home.LoadWidgetECouponResponse;
import com.alitalia.aem.web.component.widget.delegate.BookingWidgetDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(name = "TestLoadWidgetServlet", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({ @Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "sling.servlet.selectors", value = { "testloadwidget" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "sling.servlet.methods", value = { "GET" }) })
public class TestLoadWidgetServlet extends SlingSafeMethodsServlet {

	private static final Logger logger = LoggerFactory.getLogger(TestLoadWidgetServlet.class);
	private static final long serialVersionUID = 1L;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile BookingWidgetDelegate loadWidgetDelegate;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		try {
			logger.info("Executing doGet.");

			LoadWidgetECouponRequest loadWidgetRequest = new LoadWidgetECouponRequest("tid12345", "sid12345");
			loadWidgetRequest.setSite("IT");
			loadWidgetRequest.setCabinClass(CabinEnum.ECONOMY);
			loadWidgetRequest.seteCouponCode("6efb-c017");
			loadWidgetRequest.setIsRoundTrip(false);
			
			List<FlightData> itineraries = new ArrayList<>();
			DirectFlightData flightData = new DirectFlightData();
		    flightData.setProperties(null);
		    
		    List<BrandData> brands = new ArrayList<BrandData>();
		    BrandData brandData = new BrandData();
		    brandData.setProperties(null);
		    brandData.setSolutionId("zIab678Kye7UF6mh8UQaZK001");
		    brandData.setRefreshSolutionId(null);
		    brandData.setBaggageAllowanceList(null);
		    brandData.setBestFare(true);
		    brandData.setCode("EconomyEasy");
		    brandData.setCompartimentalClass(null);
		    brandData.setCurrency("EUR");
		    brandData.setEnabled(true);
		    brandData.setGrossFare(new BigDecimal(396.23));
		    brandData.setIndex(0);
		    List<String> notes = new ArrayList<String>();
		    notes.add("NONREFUNDABLE-FARE");
		    notes.add("PENALTY-FARE");
			brandData.setNotes(notes);
		    
		    BrandPenaltiesData penalties = new BrandPenaltiesData();
		    BrandPenaltyData changeAfterDepature = new BrandPenaltyData();
		    changeAfterDepature.setMaxPrice(new BigDecimal(50.0));
		    changeAfterDepature.setMaxPriceCurrency("EUR");
		    changeAfterDepature.setMinPrice(new BigDecimal(50.0));
		    changeAfterDepature.setMinPriceCurrency("EUR");
		    changeAfterDepature.setPermitted(true);
			penalties.setChangeAfterDepature(changeAfterDepature);
			
			BrandPenaltyData changeBeforeDepature = new BrandPenaltyData();
			changeBeforeDepature.setMaxPrice(new BigDecimal(50.0));
			changeBeforeDepature.setMaxPriceCurrency("EUR");
			changeBeforeDepature.setMinPrice(new BigDecimal(50.0));
			changeBeforeDepature.setMinPriceCurrency("EUR");
			changeBeforeDepature.setPermitted(true);
			penalties.setChangeBeforeDepature(changeBeforeDepature);
			
			BrandPenaltyData refundAfterDepature = new BrandPenaltyData();
			refundAfterDepature.setMaxPrice(new BigDecimal(0));
			refundAfterDepature.setMaxPriceCurrency(null);
			refundAfterDepature.setMinPrice(new BigDecimal(0));
			refundAfterDepature.setMinPriceCurrency(null);
			refundAfterDepature.setPermitted(false);
			penalties.setRefundAfterDepature(refundAfterDepature);
			
			BrandPenaltyData refundBeforeDepature = new BrandPenaltyData();
			refundBeforeDepature.setMaxPrice(new BigDecimal(0));
			refundBeforeDepature.setMaxPriceCurrency(null);
			refundBeforeDepature.setMinPrice(new BigDecimal(0));
			refundBeforeDepature.setMinPriceCurrency(null);
			refundBeforeDepature.setPermitted(false);
			penalties.setRefundBeforeDepature(refundBeforeDepature);
			
			brandData.setPenalties(penalties);
		    brandData.setRealPrice(new BigDecimal(396.23));
		    brandData.setSeatsAvailable(0);
		    brandData.setSelected(true);
		    brandData.setAdvancePurchaseDays(0);
			brands.add(brandData);
		    flightData.setBrands(brands);
		    
			flightData.setFlightType(FlightTypeEnum.DIRECT);
			Calendar arrivalDate = Calendar.getInstance();
			arrivalDate.set(2015, 06, 15, 13, 40);
			flightData.setArrivalDate(arrivalDate);
		    
			ArrayList<BaggageAllowanceData> baggageAllowanceList = new ArrayList<BaggageAllowanceData>();
		    BaggageAllowanceData baggageAllowanceData = new BaggageAllowanceData();
		    baggageAllowanceData.setCode("0GO");
		    baggageAllowanceData.setCount(1);
		    baggageAllowanceList.add(baggageAllowanceData);
			flightData.setBaggageAllowanceList(baggageAllowanceList);
			flightData.setCabin(CabinEnum.ECONOMY);
			flightData.setCarrier("AZ");
			flightData.setCarrierForLogo("AZ");
			flightData.setDurationHour(1);
			flightData.setDurationMinutes(10);
			ArrayList<String> compartimentalClass = new ArrayList<String>();
			compartimentalClass.add("Q");
			flightData.setCompartimentalClass(compartimentalClass);
			Calendar departureDate = Calendar.getInstance();
			departureDate.set(2015, 06, 15, 12, 30, 0);
			flightData.setDepartureDate(departureDate);
			FlightDetailsData details = new FlightDetailsData();
			details.setAeromobile("32S");
			details.setArrivalTerminal(null);
			details.setDepartureTerminal("1");
			details.setDistance("254");
			details.setOtherInformation(null);
			flightData.setDetails(details);
			flightData.setEnabledSeatMap(true);
			flightData.setFlightNumber("1779");
			
			AirportData from = new AirportData();
			from.setAzDeserves(true);
			from.setArea(AreaValueEnum.DOM);
			from.setCity("Roma");
			from.setCityCode("ROM");
			from.setCode("FCO");
			from.setCountry("Italia");
			from.setCountryCode("IT");
			from.setDescription("Roma, Fiumicino, Italia");
			from.setDetails(null);
			from.setEligibility(true);
			from.setEtktAvlblFrom(null);
			from.setHiddenInFirstDeparture(false);
			from.setIdMsg(1954);
			from.setName("Fiumicino");
			from.setState(null);
			from.setStateCode(null);
			from.setUnavlblFrom(null);
			from.setUnavlblUntil(null);
			flightData.setFrom(from);
			
			flightData.setMarriageGroup("0");
			flightData.setMileage(0L);
			flightData.setOperationalCarrier(null);
			flightData.setOperationalFlightNumber(null);
			flightData.setOperationalFlightText(null);
			flightData.setStopOver(false);
			
			AirportData to = new AirportData();
			to.setAzDeserves(true);
			to.setArea(AreaValueEnum.DOM);
			to.setCity("Palermo");
			to.setCityCode("PMO");
			to.setCode("PMO");
			to.setCountry("Italia");
			to.setCountryCode("IT");
			to.setDescription("Palermo, Palermo, Italia");
			to.setDetails(null);
			to.setEligibility(true);
			to.setEtktAvlblFrom(null);
			to.setHiddenInFirstDeparture(false);
			to.setIdMsg(0);
			to.setName("Palermo");
			to.setState(null);
			to.setStateCode(null);
			to.setUnavlblFrom(null);
			to.setUnavlblUntil(null);
			flightData.setTo(to);
			flightData.setUrlConditions(null);
			itineraries.add(flightData);
			
			loadWidgetRequest.setItineraries(itineraries);
			
			List<PassengerBaseData> passengers = new ArrayList<>();
			ApplicantPassengerData passenger = new ApplicantPassengerData();
		    passenger.setCouponPrice(new BigDecimal(0));
		    passenger.setExtraCharge(new BigDecimal(0.0D));
		    passenger.setFee(new BigDecimal(0));
		    passenger.setGrossFare(new BigDecimal(256.84));
		    PassengerBaseInfoData info = new PassengerBaseInfoData();
		    Calendar calendar = Calendar.getInstance();
		    calendar.set(1, 1, 1, 0, 0 , 0);
		    info.setBirthDate(calendar);
		    info.setGender(GenderTypeEnum.UNKNOWN);
		    passenger.setInfo(info);
		    passenger.setLastName("Rossi");
		    passenger.setName("Mario");
		    passenger.setNetFare(new BigDecimal(154.00D));
		    passenger.setTickets(null);
		    passenger.setType(PassengerTypeEnum.ADULT);
		    
		    PreferencesData preferences = new PreferencesData();
		    preferences.setMealType(null);
		    
		    List<SeatPreferencesData> seatPreferencesData = new ArrayList<SeatPreferencesData>();
		    
		    SeatPreferencesData seatPreferenceData = new SeatPreferencesData();
		    seatPreferenceData.setCabinSeat(CabinEnum.ECONOMY);
		    seatPreferenceData.setFlightCarrier("AZ");
		    seatPreferenceData.setFlightNumber("1792");
		    seatPreferenceData.setNumber(null);
		    seatPreferenceData.setRow("9A");
		    seatPreferencesData.add(seatPreferenceData);
		    
		    preferences.setSeatPreferences(seatPreferencesData);
		    preferences.setSeatType(null);
		    
		    passenger.setPreferences(preferences);
		    
		    passenger.setFrequentFlyerCode(null);
		    passenger.setFrequentFlyerTier(null);
		    passenger.setFrequentFlyerType(null);
		    passenger.setArTaxInfoType(ARTaxInfoTypesEnum.NONE);
		    passenger.setBlueBizCode(null);
		    passenger.setCuit(null);
		    
		    List<ContactData> contacts = new ArrayList<ContactData>();
		    
		    ContactData contact = new ContactData();
		    contact.setContactType(ContactTypeEnum.M);
		    contact.setPhoneNumber("32131212");

		    PhonePrefixData prefix = new PhonePrefixData();
		    prefix.setCode("IT");
		    prefix.setDescription("Italy");
		    prefix.setPrefix("39");
		    
		    contact.setPrefix(prefix);
		    contacts.add(contact);
		    
		    contact = new ContactData();
		    contact.setContactType(ContactTypeEnum.M);
		    contact.setPhoneNumber("063125668");

		    prefix = new PhonePrefixData();
		    prefix.setCode(null);
		    prefix.setDescription("email");
		    prefix.setPrefix(null);
		    
		    contact.setPrefix(prefix);
		    contacts.add(contact);
		    
		    passenger.setContact(contacts);
		    passenger.setCountry(null);
		    passenger.setEmail("mario.rossi@email.it");
		    passenger.setSkyBonusCode(null);
		    passenger.setSubscribeToNewsletter(null);
		    passengers.add(passenger);
			loadWidgetRequest.setPassengers(passengers);
			
			LoadWidgetECouponResponse loadWidgetResponse = loadWidgetDelegate.loadWidgetECoupon(loadWidgetRequest);

			logger.info("Load Widget ECoupon response:", loadWidgetResponse);
			
			response.setContentType("application/json");
			TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
			out.object().key("JSON").value("OK").endObject();

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception while executing doGet.", e);
		}
		logger.info("Executed doGet.");
	}
}