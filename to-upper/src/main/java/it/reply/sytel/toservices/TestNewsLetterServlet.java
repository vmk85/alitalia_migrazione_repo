package it.reply.sytel.toservices;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.RegisterUserToAgencyNewsletterRequest;
import com.alitalia.aem.common.messages.home.RegisterUserToAgencyNewsletterResponse;
import com.alitalia.aem.web.component.newsletter.delegate.NewsletterDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(name = "TestNewsLetterServlet", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({ @Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "sling.servlet.selectors", value = { "testnewsletter" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "sling.servlet.methods", value = { "GET" }) })
public class TestNewsLetterServlet extends SlingSafeMethodsServlet {

	private static final Logger logger = LoggerFactory.getLogger(TestNewsLetterServlet.class);
	private static final long serialVersionUID = 1L;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile NewsletterDelegate newsletterDelegate;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		try {
			logger.info("Executing doGet.");

			RegisterUserToAgencyNewsletterRequest registerRequest = new RegisterUserToAgencyNewsletterRequest();
			registerRequest.setTid("T12345");
			registerRequest.setSid("S12345");
			registerRequest.setAgencyId("240288");
			
			RegisterUserToAgencyNewsletterResponse registerResponse = newsletterDelegate.registerUserToAgencyNewsletter(registerRequest);
			
			response.setContentType("application/json");
			TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
			out.object().key("JSON").value(registerResponse).endObject();

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception while executing doGet.", e);
		}
		logger.info("Executed doGet.");
	}
}