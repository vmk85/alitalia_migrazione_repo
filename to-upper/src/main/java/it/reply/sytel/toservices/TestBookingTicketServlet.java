package it.reply.sytel.toservices;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.CrossSellingsRequest;
import com.alitalia.aem.common.messages.home.CrossSellingsResponse;
import com.alitalia.aem.web.component.bookingticket.delegate.BookingTicketDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(name = "TestBookingTicketServlet", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({ @Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "sling.servlet.selectors", value = { "testbookingticket" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "sling.servlet.methods", value = { "GET" }) })
public class TestBookingTicketServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(TestBookingTicketServlet.class);

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile BookingTicketDelegate bookingTicketDelegate;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException,
			IOException {
		try {
			logger.info("Executing doGet.");

			response.setContentType("application/json");

			request.getParameter("sessionid");
			request.getParameter("solutionid");
			request.getParameter("solutionset");
			request.getParameter("bookingsolutionid");

			CrossSellingsRequest crossSellingsRequest = new CrossSellingsRequest();
			crossSellingsRequest.setIpAddress("");
			crossSellingsRequest.setLanguageCode("IT");
			crossSellingsRequest.setMarketCode("IT");
			
			@SuppressWarnings("unused")
			CrossSellingsResponse crossSellings = bookingTicketDelegate.getCrossSellings(crossSellingsRequest);

			TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
			out.object().key("JSON").value("Hello, World!").endObject();

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception while executing doGet.", e);
		}

		logger.info("Executed doGet.");
	}

}
