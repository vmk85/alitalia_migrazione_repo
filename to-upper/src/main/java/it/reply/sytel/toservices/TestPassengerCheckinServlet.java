package it.reply.sytel.toservices;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.PassengerListCheckinRequest;
import com.alitalia.aem.common.messages.home.PassengerListCheckinResponse;
import com.alitalia.aem.common.messages.home.RetrievePnrInformationCheckinResponse;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationRequest;
import com.alitalia.aem.web.component.webcheckin.delegate.WebCheckinDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(name = "TestPassengerCheckinServlet", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({ @Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "sling.servlet.selectors", value = { "testpassengercheckin" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "sling.servlet.methods", value = { "GET" }) })
public class TestPassengerCheckinServlet extends SlingSafeMethodsServlet {


	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(TestPassengerCheckinServlet.class);

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile WebCheckinDelegate webCheckinDelegate;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException,
			IOException {
		try {
			logger.info("Executing doGet.");
			logger.info("contextPath: " + request.getContextPath());
			logger.info("localeAddr: " + request.getLocalAddr());
			logger.info("localeName: " + request.getLocalName());
			logger.info("localPort: " + request.getLocalPort());
			logger.info("sceheme: " + request.getScheme());
			logger.info("servername: " + request.getServerName());
			logger.info("serverport: " + request.getServerPort());

			response.setContentType("application/json");
			
			RetriveMmbPnrInformationRequest servletRequest = new RetriveMmbPnrInformationRequest("123456789", "abcdefgh");
			servletRequest.setLastName("CAPITINI");
			servletRequest.setName("RICCARDO");
			servletRequest.setLocale("IT_IT");
			servletRequest.setTripId("N8BCPZ");
			
			RetrievePnrInformationCheckinResponse servletResponse = webCheckinDelegate.retrievePnrInformation(servletRequest);

			PassengerListCheckinRequest servletRequestPass = new PassengerListCheckinRequest();
			servletRequestPass.setLastName("CAPITINI");
			servletRequestPass.setName("RICCARDO");
			servletRequestPass.setPnr("N8BCPZ");
			servletRequestPass.setSelectedRoute(servletResponse.getRoutesList().get(0));
			servletRequestPass.setTid("abcdef");
			servletRequestPass.setSid("123456");
//			servletRequestPass.setReason(MmbUpdateReasonType.);
			
			logger.info("Passenger Checkin Request: " + servletRequestPass);
			PassengerListCheckinResponse servletResponsePass = webCheckinDelegate.getPassengerList(servletRequestPass);
			logger.info("Passenger Checkin Response: " + servletResponsePass);
			
			TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
			out.object().key("JSON").value("OK").endObject();

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception while executing doGet.", e);
		}

		logger.info("Executed doGet.");
	}

}
