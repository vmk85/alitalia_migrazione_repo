package it.reply.sytel.toservices;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.AgencyRetrieveDataRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrieveDataResponse;
import com.alitalia.aem.common.messages.home.AgencyUpdateDataRequest;
import com.alitalia.aem.common.messages.home.AgencyUpdateDataResponse;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.web.component.businesslogin.delegate.BusinessLoginDelegate;

@Component(name = "TestBusinessUpdateAgencyData", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
	@Property(name = "sling.servlet.selectors", value = {"testbusinessupdateagencydata"}),
	@Property(name = "sling.servlet.methods", value = {"GET", "POST"})
})
public class TestBusinessUpdateAgencyDataServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;	
	private static final Logger logger = LoggerFactory.getLogger(TestBusinessUpdateAgencyDataServlet.class);

	@Reference
	private BusinessLoginDelegate businessLoginDelegate;
	
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter writer = response.getWriter();
		
		try {
			logger.info("Executing doGet.");
			
			String codiceAgenzia = request.getParameter("codiceAgenzia");
			
			AgencyRetrieveDataResponse delegateResponse = null;
			if (codiceAgenzia != null && codiceAgenzia.length() > 0) {
				AgencyRetrieveDataRequest delegateRequest = 
						new AgencyRetrieveDataRequest("test", "TestBusinessUpdateAgencyDataServlet");
				delegateRequest.setIdAgenzia(codiceAgenzia);
				delegateResponse = businessLoginDelegate.retrieveAgencyData(delegateRequest);
			}
			
			writer.write("<html><body>");
			writer.write("<form method='POST'>");
			writer.write("<br/>codiceAgenzia <input type='text' readonly='true' name='codiceAgenzia' value='" + (delegateResponse != null ? delegateResponse.getCodiceAgenzia() : "") + "'></input>");
			writer.write("<br/>ragioneSociale <input type='text' name='ragioneSociale' value='" + (delegateResponse != null ? delegateResponse.getRagioneSociale() : "") + "'></input>");
			writer.write("<br/>codiceFiscale <input type='text' name='codiceFiscale' value='" + (delegateResponse != null ? delegateResponse.getCodiceFiscale() : "") + "'></input>");
			writer.write("<br/>indirizzo <input type='text' name='indirizzo' value='" + (delegateResponse != null ? delegateResponse.getIndirizzo() : "") + "'></input>");
			writer.write("<br/>citta <input type='text' name='citta' value='" + (delegateResponse != null ? delegateResponse.getCitta() : "") + "'></input>");
			writer.write("<br/>provincia <input type='text' name='provincia' value='" + (delegateResponse != null ? delegateResponse.getProvincia() : "") + "'></input>");
			writer.write("<br/>zip <input type='text' name='zip' value='" + (delegateResponse != null ? delegateResponse.getZip() : "") + "'></input>");
			writer.write("<br/>regione <input type='text' name='regione' value='" + (delegateResponse != null ? delegateResponse.getRegione() : "") + "'></input>");
			writer.write("<br/>stato <input type='text' name='stato' value='" + (delegateResponse != null ? delegateResponse.getStato() : "") + "'></input>");
			writer.write("<br/>telefono <input type='text' name='telefono' value='" + (delegateResponse != null ? delegateResponse.getTelefono() : "") + "'></input>");
			writer.write("<br/>fax <input type='text' name='fax' value='" + (delegateResponse != null ? delegateResponse.getFax() : "") + "'></input>");
			writer.write("<br/>codFamCon <input type='text' name='codFamCon' value='" + (delegateResponse != null ? delegateResponse.getCodFamCom() : "") + "'></input>");
			writer.write("<br/>emailTitolare <input type='text' name='emailTitolare' value='" + (delegateResponse != null ? delegateResponse.getEmailTitolare() : "") + "'></input>");
			writer.write("<br/>emailOperatore <input type='text' name='emailOperatore' value='" + (delegateResponse != null ? delegateResponse.getEmailPubblica() : "") + "'></input>");
			writer.write("<br/>codiceAccordo <input type='text' name='codiceAccordo' value='" + (delegateResponse != null ? delegateResponse.getCodiceAccordo() : "") + "'></input>");
			writer.write("<br/>flagSendMail <input type='text' name='flagSendMail' value='false'></input>");
			writer.write("<br/>codiceSirax <input type='text' name='codiceSirax' value='" + (delegateResponse != null ? delegateResponse.getCodiceSirax() : "") + "'></input>");
			writer.write("<br/>dataValiditaAccordo <input type='text' name='dataValiditaAccordo' value='" + (delegateResponse != null ? XsdConvertUtils.printDateTime(delegateResponse.getDataValiditaAccordo()) : "") + "'></input>");
			writer.write("<br/><input type='submit'></input>");
			writer.write("</form>");
			writer.write("</body></html>");
		
		} catch (Exception e) {
			e.printStackTrace(writer);
			logger.error("Exception while executing doGet.", e);
		}
		
		logger.info("Executed doGet.");
	}
	
	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter writer = response.getWriter();
		
		try {
			logger.info("Executing doPost.");
			
			AgencyUpdateDataRequest delegateRequest = 
						new AgencyUpdateDataRequest("test", "TestBusinessUpdateAgencyDataServlet");
			
			delegateRequest.setCodiceAgenzia(request.getParameter("codiceAgenzia"));
			delegateRequest.setRagioneSociale(request.getParameter("ragioneSociale"));
			delegateRequest.setCodiceFiscale(request.getParameter("codiceFiscale"));
			delegateRequest.setIndirizzo(request.getParameter("indirizzo"));
			delegateRequest.setCitta(request.getParameter("citta"));
			delegateRequest.setProvincia(request.getParameter("provincia"));
			delegateRequest.setZip(request.getParameter("zip"));
			delegateRequest.setRegione(request.getParameter("regione"));
			delegateRequest.setStato(request.getParameter("stato"));
			delegateRequest.setTelefono(request.getParameter("telefono"));
			delegateRequest.setFax(request.getParameter("fax"));
			delegateRequest.setCodFamCon(request.getParameter("codFamCon"));
			delegateRequest.setEmailTitolare(request.getParameter("emailTitolare"));
			delegateRequest.setEmailOperatore(request.getParameter("emailOperatore"));
			delegateRequest.setCodiceAccordo(request.getParameter("codiceAccordo"));
			delegateRequest.setFlagSendMail(XsdConvertUtils.parseBoolean(request.getParameter("flagSendMail")));
			delegateRequest.setCodiceSirax(request.getParameter("codiceSirax"));
			delegateRequest.setDataValiditaAccordo(XsdConvertUtils.parseDate(request.getParameter("dataValiditaAccordo")));
			
			AgencyUpdateDataResponse delegateResponse = 
						businessLoginDelegate.updateAgencyData(delegateRequest);
			
			if (delegateResponse != null) {
				writer.write(delegateResponse.toString());
			}
		
		} catch (Exception e) {
			e.printStackTrace(writer);
			logger.error("Exception while executing doPost.", e);
		}
		
		logger.info("Executed doPost.");
		
	}
	
}
