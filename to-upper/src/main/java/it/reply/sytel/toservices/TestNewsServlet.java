package it.reply.sytel.toservices;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.RetrieveNewsRequest;
import com.alitalia.aem.common.messages.home.RetrieveNewsResponse;
import com.alitalia.aem.web.component.news.delegate.SearchNewsDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(name = "TestNewsServlet", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({ @Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "sling.servlet.selectors", value = { "testnews" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "sling.servlet.methods", value = { "GET" }) })
public class TestNewsServlet extends SlingSafeMethodsServlet {

	private static final Logger logger = LoggerFactory.getLogger(TestNewsServlet.class);
	private static final long serialVersionUID = 1L;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile SearchNewsDelegate searchNews;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		try {
			logger.info("Executing doGet.");

			RetrieveNewsRequest retrieveRequest = new RetrieveNewsRequest();
			retrieveRequest.setCategory("Tariffe");
			
			Date date = new Date();
			Calendar calendarFrom = Calendar.getInstance();
			calendarFrom.setTime(date);
			calendarFrom.add(Calendar.YEAR, -7);
			
			Calendar calendarTo = Calendar.getInstance();
			calendarTo.setTime(date);
			
//			retrieveRequest.setDateFrom(calendarFrom);
//			retrieveRequest.setDateTo(calendarTo);
			
			RetrieveNewsResponse retrieveNewsResponse = searchNews.searchNews(retrieveRequest);
			response.setContentType("application/json");
			TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
			out.object().key("JSON").value(retrieveNewsResponse).endObject();

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception while executing doGet.", e);
		}
		logger.info("Executed doGet.");
	}
}