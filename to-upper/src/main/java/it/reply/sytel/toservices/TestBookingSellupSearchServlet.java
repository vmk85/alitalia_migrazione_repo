package it.reply.sytel.toservices;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.PassengerBase;
import com.alitalia.aem.common.data.home.ApplicantPassengerData;
import com.alitalia.aem.common.data.home.BookingSearchData;
import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.common.data.home.enumerations.ARTaxInfoTypesEnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.common.messages.home.SearchBookingSolutionResponse;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.common.messages.home.enumeration.SearchExecuteResponseType;
import com.alitalia.aem.web.component.searchflights.delegate.SearchFlightsDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(name = "TestBookingSellupSearchServlet", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
	@Property(name = "sling.servlet.selectors", value = {"testbookingsellup"}),
	@Property(name = "sling.servlet.extensions", value = {"json"}),
	@Property(name = "sling.servlet.methods", value = {"GET"})
})
public class TestBookingSellupSearchServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;	
	private static final Logger logger = LoggerFactory.getLogger(TestBookingSellupSearchServlet.class);
	

	@Reference(policy=ReferencePolicy.DYNAMIC)
	private volatile SearchFlightsDelegate searchFlightsDelegate;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		
		try {
			logger.info("Executing doGet.");
			
			response.setContentType("application/json");
			
			String valueSessionId = request.getParameter("sessionid");
			String valueSolutionId = request.getParameter("solutionid");
			String valueSolutionSet = request.getParameter("solutionset");
			String valueBookingSolutionId = request.getParameter("bookingsolutionid");

			ApplicantPassengerData applicantPassengerData = new ApplicantPassengerData();
			applicantPassengerData.setFee(new BigDecimal(0));
			applicantPassengerData.setCouponPrice(new BigDecimal(0));
			applicantPassengerData.setExtraCharge(new BigDecimal(0));
			applicantPassengerData.setGrossFare(new BigDecimal(0));
			applicantPassengerData.setNetFare(new BigDecimal(0));
			applicantPassengerData.setType(PassengerTypeEnum.ADULT);
			applicantPassengerData.setArTaxInfoType(ARTaxInfoTypesEnum.NONE);
			applicantPassengerData.setBlueBizCode("");
			applicantPassengerData.setSkyBonusCode("");

			List<PassengerBase> passengers = new ArrayList<PassengerBase>();
			passengers.add(applicantPassengerData);

			BookingSearchData bookingSellupSearchData = new BookingSearchData();
			bookingSellupSearchData.setSessionId(valueSessionId);
			bookingSellupSearchData.setSolutionSet(valueSolutionSet);
			bookingSellupSearchData.setSolutionId(valueSolutionId);
			bookingSellupSearchData.setBookingSolutionId(valueBookingSolutionId);
			bookingSellupSearchData.setPassengers(passengers);
			bookingSellupSearchData.setMultiSlice(false);
			bookingSellupSearchData.setNeutral(false);

			SearchFlightSolutionRequest searchRequest = new SearchFlightSolutionRequest("TID", "SID");
			searchRequest.setFilter(bookingSellupSearchData);
			searchRequest.setLanguageCode("IT");
			searchRequest.setMarket("IT");
			searchRequest.setResponseType(SearchExecuteResponseType.MODEL);

			SearchBookingSolutionResponse searchResponse = searchFlightsDelegate.bookingSellup(searchRequest);
			RoutesData routes = null;
			if (searchResponse != null) {
				logger.info("Ha funzionato: " + searchResponse.toString());
				routes = searchResponse.getRoutesData();
			}

//			AgencyLoginRequest loginRequest = new AgencyLoginRequest("1249812", "gsiweug");
//			loginRequest.setRuolo(AlitaliaTradeUserType.fromValue(valueRuolo));
//			loginRequest.setCodiceAgenzia(valueAgenzia);
//			loginRequest.setPassword(valuePassword);
//			AgencyLoginResponse loginResponse = businessLoginDelegate.agencyLogin(loginRequest);
//			AlitaliaTradeAgencyData agencyData = null;
//			if (loginResponse != null) {
//				logger.info("Ha funzionato: " + loginResponse.toString());
//				agencyData = loginResponse.getAgencyData();
//			}

			TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
			out.object();
			
//			if (agencyData != null) {
//				out.key("result").value(agencyData.getEmailAlitalia());
//				out.key("iataAgency").value(agencyData.isIataAgency());
//				out.key("groupEnabled").value(agencyData.isGroupEnabled());
//			}
			if (routes != null) {
				out.key("solutionset").value(routes.getSolutionSet());
				out.key("sessionid").value(routes.getSessionId());
				out.key("passengers").value(routes.getPassengers());
				out.key("tabs").value(routes.getProperties());
			}
			else
				out.key("result").value("invalid login");

			out.endObject();
		
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			logger.error("Exception while executing doGet.", e);
		}
		
		logger.info("Executed doGet.");
	}
	
	
}
