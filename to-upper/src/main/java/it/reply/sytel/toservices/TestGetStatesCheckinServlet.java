package it.reply.sytel.toservices;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.mmb.MmbRequestBaseInfoData;
import com.alitalia.aem.common.messages.home.WebCheckinStatesRequest;
import com.alitalia.aem.common.messages.home.WebCheckinStatesResponse;
import com.alitalia.aem.web.component.webcheckin.delegate.WebCheckinDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(name = "TestGetStatesCheckinServlet", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({ @Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "sling.servlet.selectors", value = { "testgetstatescheckin" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "sling.servlet.methods", value = { "GET" }) })
public class TestGetStatesCheckinServlet extends SlingSafeMethodsServlet {


	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(TestGetStatesCheckinServlet.class);

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile WebCheckinDelegate webCheckinDelegate;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException,
			IOException {
		try {
			logger.info("Executing doGet.");
			logger.info("contextPath: " + request.getContextPath());
			logger.info("localeAddr: " + request.getLocalAddr());
			logger.info("localeName: " + request.getLocalName());
			logger.info("localPort: " + request.getLocalPort());
			logger.info("sceheme: " + request.getScheme());
			logger.info("servername: " + request.getServerName());
			logger.info("serverport: " + request.getServerPort());
			
			String countryCode = "it";
			if (request.getParameter("countryCode") != null)
				countryCode = (String) request.getParameter("countryCode");
			
			logger.info("countryCode: [" + countryCode + "]");

			response.setContentType("application/json");

			WebCheckinStatesRequest servletRequest = new WebCheckinStatesRequest("123456789", "abcdefgh");
			servletRequest.setCountryCode(countryCode);
			servletRequest.setClient("client");
			MmbRequestBaseInfoData info = new MmbRequestBaseInfoData();
			info.setClientIp("127.0.0.1");
			info.setSessionId("sessionId");
			info.setSiteCode("it");
			servletRequest.setBaseInfo(info);
			
			logger.info("GetStates Checkin Request: " + servletRequest);
			WebCheckinStatesResponse servletResponse = webCheckinDelegate.getStates(servletRequest);
			logger.info("GetStates Checkin Response: " + servletResponse);
			
			TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
			out.object().key("JSON").value("OK").endObject();

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception while executing doGet.", e);
		}

		logger.info("Executed doGet.");
	}

}
