package it.reply.sytel.toservices;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.AgencyInsertConsentRequest;
import com.alitalia.aem.common.messages.home.AgencyInsertConsentResponse;
import com.alitalia.aem.web.component.businesslogin.delegate.BusinessLoginDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(name = "TestInsertConsentServlet", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({ @Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "sling.servlet.selectors", value = { "testinsertconsent" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "sling.servlet.methods", value = { "GET" }) })
public class TestInsertConsentServlet extends SlingSafeMethodsServlet {

	private static final Logger logger = LoggerFactory.getLogger(TestInsertConsentServlet.class);
	private static final long serialVersionUID = 1L;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile BusinessLoginDelegate businessLoginDelegate;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		try {
			logger.info("Executing doGet.");

			AgencyInsertConsentRequest agencyInsertConsentRequest = new AgencyInsertConsentRequest();
			agencyInsertConsentRequest.setTid("T12345");
			agencyInsertConsentRequest.setSid("S12345");
			agencyInsertConsentRequest.setIdAgenzia("240288");
			agencyInsertConsentRequest.setFlagCondizioni(true);
			agencyInsertConsentRequest.setFlagDatiPersonali(true);
			
			AgencyInsertConsentResponse agencyInsertConsentResponse = businessLoginDelegate.insertConsent(agencyInsertConsentRequest);
			
			response.setContentType("application/json");
			TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
			out.object().key("JSON").value(agencyInsertConsentResponse).endObject();

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception while executing doGet.", e);
		}
		logger.info("Executed doGet.");
	}
}