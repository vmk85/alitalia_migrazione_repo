package it.reply.sytel.toservices;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.AgencyRetrieveDataRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrieveDataResponse;
import com.alitalia.aem.web.component.businesslogin.delegate.BusinessLoginDelegate;

@Component(name = "TestBusinessRetrieveAgencyData", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
	@Property(name = "sling.servlet.selectors", value = {"testbusinessretrieveagencydata"}),
	@Property(name = "sling.servlet.methods", value = {"GET"})
})
public class TestBusinessRetrieveAgencyDataServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;	
	private static final Logger logger = LoggerFactory.getLogger(TestBusinessRetrieveAgencyDataServlet.class);

	@Reference
	private BusinessLoginDelegate businessLoginDelegate;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter writer = response.getWriter();
		
		try {
			logger.info("Executing doGet.");
			
			String idAgenzia = request.getParameter("idAgenzia");
			AgencyRetrieveDataRequest delegateRequest = 
						new AgencyRetrieveDataRequest("test", "TestBusinessRetrieveAgencyDataServlet");
			delegateRequest.setIdAgenzia(idAgenzia);
			AgencyRetrieveDataResponse delegateResponse = 
						businessLoginDelegate.retrieveAgencyData(delegateRequest);
			
			if (delegateResponse != null) {
				writer.write(delegateResponse.toString());
			}
		
		} catch (Exception e) {
			e.printStackTrace(writer);
			logger.error("Exception while executing doGet.", e);
		}
		
		logger.info("Executed doGet.");
	}
	
	
}
