package it.reply.sytel.toservices;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.data.home.AvailableFlightsData;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.MultiSliceSearchData;
import com.alitalia.aem.common.data.home.NextSliceSearchData;
import com.alitalia.aem.common.data.home.PassengerNumbersData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.common.data.home.SearchDestinationData;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.CabinTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.SearchTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.TimeTypeEnum;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionResponse;
import com.alitalia.aem.common.messages.home.enumeration.SearchExecuteResponseType;
import com.alitalia.aem.web.component.searchflights.delegate.SearchFlightsDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(name = "TestFullMultiSliceSearchServlet", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
	@Property(name = "sling.servlet.selectors", value = {"testfullmultislicesearch"}),
	@Property(name = "sling.servlet.extensions", value = {"json"}),
	@Property(name = "sling.servlet.methods", value = {"GET"})
})
public class TestFullMultiSliceSearchServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;	
	private static final Logger logger = LoggerFactory.getLogger(TestFullMultiSliceSearchServlet.class);
	

	@Reference(policy=ReferencePolicy.DYNAMIC)
	private volatile SearchFlightsDelegate searchFlightsDelegate;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		
		try {
			logger.info("Executing doGet.");
			
			response.setContentType("application/json");
			
			String valuePartenza = request.getParameter("partenza");
			String valueSosta = request.getParameter("sosta");
			String valueArrivo = request.getParameter("arrivo");
			Calendar flightDepartureDate = Calendar.getInstance();
			flightDepartureDate.setTime(new SimpleDateFormat("dd/MM/yyyy").parse("10/09/2015"));

			AirportData fromAirport = new AirportData();
//			fromAirport.setCityCode("ROM");
//			fromAirport.setCode("FCO");
			fromAirport.setCityCode(valuePartenza);
			fromAirport.setCode(valuePartenza);
			fromAirport.setCountryCode("IT");

			AirportData stopAirport = new AirportData();
//			toAirport.setCityCode("PMO");
//			toAirport.setCode("PMO");
			stopAirport.setCityCode(valueSosta);
			stopAirport.setCode(valueSosta);
			stopAirport.setCountryCode("IT");

			AirportData toAirport = new AirportData();
//			toAirport.setCityCode("PMO");
//			toAirport.setCode("PMO");
			toAirport.setCityCode(valueArrivo);
			toAirport.setCode(valueArrivo);
			toAirport.setCountryCode("IT");

			SearchDestinationData destination1 = new SearchDestinationData();
			destination1.setFromAirport(fromAirport);
			destination1.setToAirport(stopAirport);
			destination1.setRouteType(RouteTypeEnum.OUTBOUND);
			destination1.setTimeType(TimeTypeEnum.ANYTIME);
			destination1.setDepartureDate(flightDepartureDate);

			SearchDestinationData destination2 = new SearchDestinationData();
			destination2.setFromAirport(stopAirport);
			destination2.setToAirport(toAirport);
			destination2.setRouteType(RouteTypeEnum.OUTBOUND);
			destination2.setTimeType(TimeTypeEnum.ANYTIME);
			Calendar flightArrivalDate = Calendar.getInstance();
			flightArrivalDate.setTime(new SimpleDateFormat("dd/MM/yyyy").parse("20/09/2015"));
			destination2.setDepartureDate(flightArrivalDate);

			PassengerNumbersData passengerNumbers = new PassengerNumbersData();
			passengerNumbers.setNumber(1);
			passengerNumbers.setPassengerType(PassengerTypeEnum.ADULT);

			MultiSliceSearchData multiSliceSearchData = new MultiSliceSearchData();
			multiSliceSearchData.setCug("ADT");
			multiSliceSearchData.setOnlyDirectFlight(false);
			multiSliceSearchData.setType(SearchTypeEnum.MULTI_SLICE_SEARCH);
			multiSliceSearchData.addDestination(destination1);
			multiSliceSearchData.addDestination(destination2);
			multiSliceSearchData.addPassengerNumbers(passengerNumbers);
			multiSliceSearchData.setMarket("IT");
			multiSliceSearchData.setSearchCabin(CabinEnum.ECONOMY);
			multiSliceSearchData.setSearchCabinType(CabinTypeEnum.PERMITTED);

			SearchFlightSolutionRequest searchRequest = new SearchFlightSolutionRequest("TID", "SID");
			searchRequest.setFilter(multiSliceSearchData);
			searchRequest.setLanguageCode("IT");
			searchRequest.setMarket("IT");
			searchRequest.setResponseType(SearchExecuteResponseType.MODEL);
			searchRequest.setRibbonOutboundStartDate(null);
			searchRequest.setRibbonReturnStartDate(null);

			SearchFlightSolutionResponse searchResponse = searchFlightsDelegate.searchMultipleLegsSolutions(searchRequest);
			AvailableFlightsData availableFlights = null;
			if (searchResponse != null) {
				logger.info("Ha funzionato: " + searchResponse.toString());
				availableFlights = searchResponse.getAvailableFlights();
			}

			TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
			out.object();
			
			if (availableFlights != null) {
				out.key("solutionset").value(availableFlights.getSolutionSet());
				out.key("sessionid").value(availableFlights.getSessionId());
				boolean solutionSelected = false;
				String selectedSolutionId = "";
				for (RouteData route : availableFlights.getRoutes()) {
					out.key("direction").value(route.getType().toString());
					for (FlightData flight : route.getFlights()) {
						if (flight instanceof DirectFlightData) {
							for (BrandData brand : ((DirectFlightData) flight).getBrands()) {
								out.key("solutionid").value(brand.getSolutionId());
								if (!solutionSelected && brand.getSeatsAvailable() >0) {
									selectedSolutionId = brand.getSolutionId();
									solutionSelected = true;
								}
							}
						} else {
							out.key("connectingflight").value(((ConnectingFlightData) flight).toString());
						}
					}
				}

				NextSliceSearchData nextSliceSearchData = new NextSliceSearchData();
				nextSliceSearchData.setId(availableFlights.getId());
				nextSliceSearchData.setSessionId(availableFlights.getSessionId());
				nextSliceSearchData.setSolutionSet(availableFlights.getSolutionSet());
				nextSliceSearchData.setSolutionId(selectedSolutionId);
				nextSliceSearchData.setPreferredCabin(CabinEnum.ECONOMY);
				nextSliceSearchData.setSliceIndex(1);

				SearchFlightSolutionRequest nextSliceSearchRequest = new SearchFlightSolutionRequest("TID", "SID");
				nextSliceSearchRequest.setFilter(nextSliceSearchData);
				nextSliceSearchRequest.setLanguageCode("IT");
				nextSliceSearchRequest.setMarket("IT");
				nextSliceSearchRequest.setResponseType(SearchExecuteResponseType.MODEL);

				SearchFlightSolutionResponse nextSliceSearchResponse = searchFlightsDelegate.searchMultipleLegsNextSolution(nextSliceSearchRequest);

				if (nextSliceSearchResponse != null) {
					logger.info("Ha funzionato: " + nextSliceSearchResponse.toString());
					nextSliceSearchResponse.getAvailableFlights();
				}

				if (availableFlights != null) {
					out.key("solutionset").value(availableFlights.getSolutionSet());
					out.key("sessionid").value(availableFlights.getSessionId());
					for (RouteData route : availableFlights.getRoutes()) {
						out.key("direction").value(route.getType().toString());
						for (FlightData flight : route.getFlights()) {
							if (flight instanceof DirectFlightData) {
								for (BrandData brand : ((DirectFlightData) flight).getBrands()) {
									out.key("solutionid").value(brand.getSolutionId());
								}
							} else {
								out.key("connectingflight").value(((ConnectingFlightData) flight).toString());
							}
						}
					}
				}
			}
			else
				out.key("result").value("availableFlights is null!!!");

			out.endObject();
		
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			logger.error("Exception while executing doGet.", e);
		}
		
		logger.info("Executed doGet.");
	}
	
	
}
