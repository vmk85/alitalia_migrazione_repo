package it.reply.sytel.toservices;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbApisTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbFlightStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbLegTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.data.home.mmb.MmbAirportData;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.MmbPassengerData;
import com.alitalia.aem.common.data.home.mmb.MmbRequestBaseInfoData;
import com.alitalia.aem.common.messages.home.RetrieveCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCountriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveCreditCardCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCreditCardCountriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveFrequentFlayerRequest;
import com.alitalia.aem.common.messages.home.RetrieveMealsRequest;
import com.alitalia.aem.common.messages.home.RetrieveMealsResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbAirportCheckinRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAirportCheckinResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbAmericanStatesRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAmericanStatesResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbFlightSeatMapRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbFlightSeatMapResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbFrequentFlyerCarriersResponse;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemRequest;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemResponse;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixRequest;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixResponse;
import com.alitalia.aem.common.messages.home.RetrieveProvincesRequest;
import com.alitalia.aem.common.messages.home.RetrieveProvincesResponse;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationRequest;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationResponse;
import com.alitalia.aem.service.api.home.MmbCommonService;
import com.alitalia.aem.service.api.home.MmbSearchService;
import com.alitalia.aem.service.api.home.MmbStaticDataService;
import com.day.cq.commons.TidyJSONWriter;

/*import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.web.component.searchflights.delegate.SearchFlightsDelegate;
import com.day.cq.commons.TidyJSONWriter;*/

@Component(name = "TestServiceImplServlet", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
	@Property(name = "sling.servlet.selectors", value = {"testservice"}),
	@Property(name = "sling.servlet.extensions", value = {"json"}),
	@Property(name = "sling.servlet.methods", value = {"GET"})
})
public class TestServiceImplServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;	
	
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(TestServiceImplServlet.class);
	
	@Reference(policy=ReferencePolicy.DYNAMIC)
	private volatile MmbCommonService mmbCommonService;

	@Reference(policy=ReferencePolicy.DYNAMIC)
	private volatile MmbStaticDataService mmbStaticDataService;

	@Reference(policy=ReferencePolicy.DYNAMIC)
	private volatile MmbSearchService mmbSearchService;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		
		try {
			logger.info("Executing doGet.");

			TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
			out.object();
//			RetrieveMmbFlightSeatMapRequest serviceRequest = new RetrieveMmbFlightSeatMapRequest("pippo", "pluto");
//
//			List<String> compartimentalClass = new ArrayList<String>();
//			compartimentalClass.add("C");
//			serviceRequest.setCompartimentalClass(compartimentalClass);
//
//			MmbFlightData flightInfo = new MmbFlightData();
//			flightInfo.setId(0);
//			flightInfo.setApisTypeRequired(MmbApisTypeEnum.NONE);
//
//			Calendar arrivalDateTime = Calendar.getInstance();
//			arrivalDateTime.set(2015, 12, 17, 10, 55, 0);
//			flightInfo.setArrivalDateTime(arrivalDateTime);
//
//			flightInfo.setBaggageAllow(2);
//			flightInfo.setCabin(CabinEnum.BUSINESS);
//			flightInfo.setCarrier("AZ");
//			flightInfo.setComfortSeatFare(new BigDecimal("60"));
//			flightInfo.setComfortSeatPaid(false);
//
//			Calendar departureDateTime = Calendar.getInstance();
//			departureDateTime.set(2015, 12, 17, 7, 25, 0);
//			flightInfo.setDepartureDateTime(departureDateTime);
//
//			flightInfo.setEnabledSeatMap(false);
//			flightInfo.setEticketClass("");
//			flightInfo.setEticket("0552322303339");
//			flightInfo.setExtraBaggage(3);
//			flightInfo.setFareClass("");
//			flightInfo.setFlightNumber("0201");
//			flightInfo.setFromCity("Londra");
//			flightInfo.setFromTerminal("4");
//
//			MmbAirportData from = new MmbAirportData();
//			from.setId(1);
//			from.setArea(AreaValueEnum.INTZ);
//			from.setCity("LON");
//			from.setCode("LHR");
//			from.setCountryCode("GB");
//			flightInfo.setFrom(from);
//
//			flightInfo.setHasComfortSeat(true);
//			flightInfo.setIndex(0);
//			flightInfo.setLegType(MmbLegTypeEnum.INZ);
//			flightInfo.setMultitratta(2);
//			flightInfo.setOperatingCarrier("AZ");
//			flightInfo.setOperatingFlightNumber("0201");
//
//			List<MmbPassengerData> passengers = new ArrayList<MmbPassengerData>();
//			flightInfo.setPassengers(passengers);
//
//			flightInfo.setPnr("JU2Z4Z");
//			flightInfo.setRph(2);
//			flightInfo.setRouteId(1);
//			flightInfo.setSeatClass(MmbCompartimentalClassEnum.UNKNOWN);
//			flightInfo.setStatus(MmbFlightStatusEnum.AVAILABLE);
//			flightInfo.setToCity("Roma");
//			flightInfo.setToTerminal("3");
//
//			MmbAirportData to = new MmbAirportData();
//			to.setId(1);
//			to.setArea(AreaValueEnum.DOM);
//			to.setCity("ROM");
//			to.setCode("FCO");
//			to.setCountryCode("IT");
//			flightInfo.setTo(to);
//
//			flightInfo.setType(RouteTypeEnum.RETURN);
//			flightInfo.setFirstFlight(true);
//			flightInfo.setMiniFare(false);
//
//			serviceRequest.setFlightInfo(flightInfo);
//			RetrieveMmbFlightSeatMapResponse serviceResponse = mmbCommonService.getMmbFlightSeatsMap(serviceRequest);
//			
//			response.setContentType("application/json");
//			
//			
//			if (serviceResponse.getSeatMapMatrix() != null) {
//				String value = serviceResponse.getSeatMapMatrix().toString();
//				out.key("result").value(value.toUpperCase());
//			}
//			else
//				out.key("result").value("seatmap null!!!");
//
//			RetrieveProvincesRequest provincesRequest = new RetrieveProvincesRequest("prova", "province");
//			provincesRequest.setItemCache("false");
//			provincesRequest.setLanguageCode("IT");
//			provincesRequest.setMarket("IT");
//
//			RetrieveProvincesResponse provincesResponse = mmbStaticDataService.retrieveProvinces(provincesRequest);
//			if (provincesResponse.getProvinces() != null && !provincesResponse.getProvinces().isEmpty()) {
//				out.key("result").value(provincesResponse.getProvinces().toString());
//			}
//			else
//				out.key("result").value("provinceslist null or empty!!!");
//
//			RetrieveCreditCardCountriesRequest creditCardCountriesRequest = new RetrieveCreditCardCountriesRequest("prova", "province");
//			creditCardCountriesRequest.setItemCache("false");
//			creditCardCountriesRequest.setLanguageCode("IT");
//			creditCardCountriesRequest.setMarket("IT");
//
//			RetrieveCreditCardCountriesResponse creditCardCountriesResponse = mmbStaticDataService.retrieveCreditCardCountries(creditCardCountriesRequest);
//			if (creditCardCountriesResponse.getCountries() != null && !creditCardCountriesResponse.getCountries().isEmpty()) {
//				out.key("result").value(creditCardCountriesResponse.getCountries().toString());
//			}
//			else
//				out.key("result").value("countrieslist null or empty!!!");
//
//			RetrieveCountriesRequest countriesRequest = new RetrieveCountriesRequest("prova", "province");
//			countriesRequest.setItemCache("false");
//			countriesRequest.setLanguageCode("IT");
//			countriesRequest.setMarket("IT");
//
//			RetrieveCountriesResponse countriesResponse = mmbStaticDataService.retrieveCountries(countriesRequest);
//			if (countriesResponse.getCountries() != null && !countriesResponse.getCountries().isEmpty()) {
//				out.key("result").value(countriesResponse.getCountries().toString());
//			}
//			else
//				out.key("result").value("countrieslist null or empty!!!");

//			RetrieveMealsRequest mealsRequest = new RetrieveMealsRequest("prova", "province");
//			mealsRequest.setItemCache("false");
//			mealsRequest.setLanguageCode("IT");
//			mealsRequest.setMarket("IT");
//
//			RetrieveMealsResponse mealsResponse = mmbStaticDataService.retrieveMeals(mealsRequest);
//			if (mealsResponse.getMeals() != null && !mealsResponse.getMeals().isEmpty()) {
//				out.key("result").value(mealsResponse.getMeals().toString());
//			}
//			else
//				out.key("result").value("mealslist null or empty!!!");
//
//			RetrievePhonePrefixRequest phonePrefixRequest = new RetrievePhonePrefixRequest("prova", "province");
//			phonePrefixRequest.setItemCache("false");
//			phonePrefixRequest.setLanguageCode("IT");
//			phonePrefixRequest.setMarket("IT");
//
//			RetrievePhonePrefixResponse phonePrefixResponse = mmbStaticDataService.retrievePhonePrefix(phonePrefixRequest);
//			if (phonePrefixResponse.getPhonePrefix() != null && !phonePrefixResponse.getPhonePrefix().isEmpty()) {
//				out.key("result").value(phonePrefixResponse.getPhonePrefix().toString());
//			}
//			else
//				out.key("result").value("phoneprefixeslist null or empty!!!");
//
//			RetrievePaymentTypeItemRequest paymentTypeItemRequest = new RetrievePaymentTypeItemRequest();
//			paymentTypeItemRequest.setItemCache("false");
//			paymentTypeItemRequest.setLanguageCode("IT");
//			paymentTypeItemRequest.setMarket("IT");
//
//			RetrievePaymentTypeItemResponse paymentTypeItemResponse = mmbStaticDataService.retrievePaymentTypeItem(paymentTypeItemRequest);
//			if (paymentTypeItemResponse.getPaymentTypeItemsData() != null && !paymentTypeItemResponse.getPaymentTypeItemsData().isEmpty()) {
//				out.key("result").value(paymentTypeItemResponse.getPaymentTypeItemsData().toString());
//			}
//			else
//				out.key("result").value("phoneprefixeslist null or empty!!!");
//
//			RetrieveFrequentFlayerRequest frequentFlyerRequest = new RetrieveFrequentFlayerRequest("prova", "province");
//			frequentFlyerRequest.setItemCache("false");
//			frequentFlyerRequest.setLanguageCode("IT");
//			frequentFlyerRequest.setMarket("IT");
//
//			RetrieveMmbFrequentFlyerCarriersResponse mmbFrequentFlyerCarriersResponse = 
//					mmbStaticDataService.retrieveFrequentFlyers(frequentFlyerRequest);
//			if (mmbFrequentFlyerCarriersResponse.getFrequentFlyers() != null && !mmbFrequentFlyerCarriersResponse.getFrequentFlyers().isEmpty()) {
//				out.key("result").value(mmbFrequentFlyerCarriersResponse.getFrequentFlyers().toString());
//			}
//			else
//				out.key("result").value("frequentflyerslist null or empty!!!");
//
//			RetrieveMmbAirportCheckinRequest mmbAirportCheckinRequest = new RetrieveMmbAirportCheckinRequest("prova", "province");
//			mmbAirportCheckinRequest.setItemCache("false");
//			mmbAirportCheckinRequest.setLanguageCode("IT");
//			mmbAirportCheckinRequest.setMarket("IT");
//
//			RetrieveMmbAirportCheckinResponse mmbAirportCheckinResponse = 
//					mmbStaticDataService.retrieveAirportCheckIn(mmbAirportCheckinRequest);
//			if (mmbAirportCheckinResponse.getAirportCheckinList() != null && !mmbAirportCheckinResponse.getAirportCheckinList().isEmpty()) {
//				out.key("result").value(mmbAirportCheckinResponse.getAirportCheckinList().toString());
//			}
//			else
//				out.key("result").value("frequentflyerslist null or empty!!!");
//
//			RetrieveMmbAmericanStatesRequest mmbAmericanStatesRequest = new RetrieveMmbAmericanStatesRequest("prova", "province");
//			mmbAmericanStatesRequest.setItemCache("false");
//			mmbAmericanStatesRequest.setLanguageCode("IT");
//			mmbAmericanStatesRequest.setMarket("IT");
//
//			RetrieveMmbAmericanStatesResponse mmbAmericanStatesResponse = 
//					mmbStaticDataService.retrieveAmericanStates(mmbAmericanStatesRequest);
//			if (mmbAmericanStatesResponse.getAmericanStatesList() != null && !mmbAmericanStatesResponse.getAmericanStatesList().isEmpty()) {
//				out.key("result").value(mmbAmericanStatesResponse.getAmericanStatesList().toString());
//			}
//			else
//				out.key("result").value("frequentflyerslist null or empty!!!");

			String pnr = request.getParameter("pnr");
			String lastname = request.getParameter("lastname");
			String name = request.getParameter("name");
			
			RetriveMmbPnrInformationRequest retriveMmbPnrInformationRequest = new RetriveMmbPnrInformationRequest("TID", "urytwongbwogqoug");
			MmbRequestBaseInfoData baseInfo = new MmbRequestBaseInfoData();
			baseInfo.setClientIp("10.8.40.92");
			baseInfo.setSessionId(retriveMmbPnrInformationRequest.getSid());
			baseInfo.setSiteCode("IT");
			retriveMmbPnrInformationRequest.setBaseInfo(baseInfo);
			retriveMmbPnrInformationRequest.setClient("MMB");
			retriveMmbPnrInformationRequest.setLastName(lastname);
			retriveMmbPnrInformationRequest.setLocale("IT_IT");
			retriveMmbPnrInformationRequest.setName(name);
			retriveMmbPnrInformationRequest.setTripId(pnr);

			RetriveMmbPnrInformationResponse retriveMmbPnrInformationResponse = mmbSearchService.retrivePnrInformation(retriveMmbPnrInformationRequest);
			if (retriveMmbPnrInformationResponse != null &&
					retriveMmbPnrInformationResponse.getRoutesList() != null &&
					!retriveMmbPnrInformationResponse.getRoutesList().isEmpty()) {
				out.key("result").value(retriveMmbPnrInformationResponse.getRoutesList().toString());
			}
			else
				out.key("result").value("RoutesList null or empty!!!");

			out.endObject();
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			logger.error("Exception while executing doGet.", e);
		}
		
		logger.info("Executed doGet."); 
	}
	
	
}
