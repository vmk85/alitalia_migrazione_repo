package it.reply.sytel.toservices;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.AgencySubscriptionRequest;
import com.alitalia.aem.common.messages.home.AgencySubscriptionResponse;
import com.alitalia.aem.web.component.businesslogin.delegate.BusinessLoginDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(name = "TestBusinessSubscriptionServlet", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
	@Property(name = "sling.servlet.selectors", value = {"testbusinesssubscription"}),
	@Property(name = "sling.servlet.extensions", value = {"json"}),
	@Property(name = "sling.servlet.methods", value = {"GET"})
})
public class TestBusinessSubscriptionServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;	
	private static final Logger logger = LoggerFactory.getLogger(TestBusinessSubscriptionServlet.class);
	
	@Reference
	private BusinessLoginDelegate businessLoginDelegate;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		
		try {
			logger.info("Executing doGet.");
			
			response.setContentType("application/json");
			
			String valuePartitaIva = request.getParameter("partitaiva");
			String valueCodiceFiscale = request.getParameter("codicefiscale");

			AgencySubscriptionRequest subscriptionRequest = new AgencySubscriptionRequest();

			subscriptionRequest.setPartitaIva(valuePartitaIva);
			subscriptionRequest.setCodiceFiscale(valueCodiceFiscale);
			subscriptionRequest.setCap("00100");
			subscriptionRequest.setCitta("Roma");
			subscriptionRequest.setEmailOperatore("f.pacilio@reply.it");
			subscriptionRequest.setEmailTitolare("r.capitini@reply.it");
			subscriptionRequest.setIndirizzo("Via Marchetti");
			subscriptionRequest.setNomeCompagnia("Iscrivi Agenzia WS");
			subscriptionRequest.setNumeroFax("06844341");
			subscriptionRequest.setNumeroTelefono("06844341");
			subscriptionRequest.setProvincia("RM");
			subscriptionRequest.setStato("");

			AgencySubscriptionResponse subscriptionResponse = businessLoginDelegate.subscribeAgency(subscriptionRequest);

			TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
			out.object();
		
			if (subscriptionResponse != null) {
				logger.info("Ha funzionato: " + subscriptionResponse.toString());

				out.key("errorcode").value(subscriptionResponse.getErrorCode());
				out.key("errormessage").value(subscriptionResponse.getErrorMessage());
			}
			else
				out.key("result").value("subcription call failed");

			out.endObject();
		
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			logger.error("Exception while executing doGet.", e);
		}
		
		logger.info("Executed doGet.");
	}
	
	
}
