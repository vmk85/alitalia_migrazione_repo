package it.reply.sytel.toservices;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.ApplicantPassengerData;
import com.alitalia.aem.common.data.home.ContactData;
import com.alitalia.aem.common.data.home.InsurancePolicyData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.PassengerBaseInfoData;
import com.alitalia.aem.common.data.home.PhonePrefixData;
import com.alitalia.aem.common.data.home.PreferencesData;
import com.alitalia.aem.common.data.home.PropertiesData;
import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.common.data.home.SeatPreferencesData;
import com.alitalia.aem.common.data.home.enumerations.ARTaxInfoTypesEnum;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.ContactTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.GenderTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.common.messages.home.InfoPassengerCreatePNRRequest;
import com.alitalia.aem.common.messages.home.InfoPassengerCreatePNRResponse;
import com.alitalia.aem.web.component.infopassenger.delegate.InfoPassengerDelegate;

@Component(name = "TestInfoPassengerCreatePNRServlet", immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
	@Property(name = "sling.servlet.selectors", value = {"testinfopassengercreatepnr"}),
	@Property(name = "sling.servlet.methods", value = {"GET"})
})
public class TestInfoPassengerCreatePNRServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;	
	private static final Logger logger = LoggerFactory.getLogger(TestInfoPassengerCreatePNRServlet.class);

	@Reference
	private InfoPassengerDelegate infoPassengerDelegate;
	
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter writer = response.getWriter();
		
		try {
			logger.info("Executing doGet.");
			
			InfoPassengerCreatePNRRequest delegateRequest = new InfoPassengerCreatePNRRequest();
			
			delegateRequest.setMarketCode("IT");
			delegateRequest.setCarnetCode(null);
			
			delegateRequest.setPrenotation(new RoutesData());
			
			RoutesData prn = delegateRequest.getPrenotation();
			prn.setFarmId(null);
			prn.setId("7pAh73JOUg9euXwcF0MAXB");
			prn.setSessionId("6Pr0MAXBCsDKNukYtfCjQJwPO");
			prn.setSolutionSet("0CyOktkyvJ2SN1E0iV7c2Y8");
			prn.setCabin(CabinEnum.ECONOMY);
			prn.setMseType(null);
			prn.setNationalInsuranceNumber("RBNMSM74M24E472K");
			prn.setPnr("");
			
			prn.setProperties(new PropertiesData());
			// prn.getProperties().addElement("BookingDetails", value); // FIXME
			prn.getProperties().addElement("WebSession", "s3zfbyf2anap1wykwtmdtr54");
			
			prn.setCashAndMiles(null);
			
			prn.setCoupon(null);
			
			prn.setMilleMigliaCustomer(null);
			
			prn.setInsurance(new InsurancePolicyData());
			prn.getInsurance().setBuy(Boolean.TRUE);
			prn.getInsurance().setCardType(null);
			prn.getInsurance().setCurrency("EUR");
			prn.getInsurance().setDiscount(BigDecimal.valueOf(52));
			prn.getInsurance().setDiscountedPrice(BigDecimal.valueOf(52));
			prn.getInsurance().setErrorDescription(null);
			prn.getInsurance().setPolicyNumber(null);
			prn.getInsurance().setTotalInsuranceCost(BigDecimal.valueOf(52));
			prn.getInsurance().setTypeOfTrip(null);
			
			prn.setPassengers(new ArrayList<PassengerBaseData>());
			
			ApplicantPassengerData pax1 = new ApplicantPassengerData();
			pax1.setCouponPrice(BigDecimal.valueOf(52));
			pax1.setExtraCharge(BigDecimal.valueOf(0));
			pax1.setFee(BigDecimal.valueOf(0));
			pax1.setGrossFare(BigDecimal.valueOf(265.84));
			pax1.setInfo(new PassengerBaseInfoData());
			pax1.getInfo().setBirthDate(GregorianCalendar.getInstance());
			pax1.getInfo().getBirthDate().setTime(new Date());
			pax1.getInfo().setGender(GenderTypeEnum.UNKNOWN);
			pax1.setLastName("test");
			pax1.setName("Max");
			pax1.setNetFare(BigDecimal.valueOf(154.84));
			pax1.setTickets(null);
			pax1.setType(PassengerTypeEnum.ADULT);
			pax1.setPreferences(new PreferencesData());
			pax1.getPreferences().setMealType(null);
			pax1.getPreferences().setSeatPreferences(new ArrayList<SeatPreferencesData>());
			pax1.getPreferences().getSeatPreferences().add(new SeatPreferencesData());
			pax1.getPreferences().getSeatPreferences().get(0).setCabinSeat(CabinEnum.ECONOMY);
			pax1.getPreferences().getSeatPreferences().get(0).setFlightCarrier("AZ");
			pax1.getPreferences().getSeatPreferences().get(0).setFlightNumber("1779");
			pax1.getPreferences().getSeatPreferences().get(0).setNumber("C");
			pax1.getPreferences().getSeatPreferences().get(0).setNumber("8");
			pax1.getPreferences().getSeatPreferences().add(new SeatPreferencesData());
			pax1.getPreferences().getSeatPreferences().get(1).setCabinSeat(CabinEnum.ECONOMY);
			pax1.getPreferences().getSeatPreferences().get(1).setFlightCarrier("AZ");
			pax1.getPreferences().getSeatPreferences().get(1).setFlightNumber("1792");
			pax1.getPreferences().getSeatPreferences().get(1).setNumber("A");
			pax1.getPreferences().getSeatPreferences().get(1).setNumber("9");
			pax1.getPreferences().setSeatType(null);
			pax1.setFrequentFlyerCode(null);
			pax1.setFrequentFlyerTier(null);
			pax1.setFrequentFlyerType(null);
			pax1.setArTaxInfoType(ARTaxInfoTypesEnum.NONE);
			pax1.setBlueBizCode(null);
			pax1.setCuit(null);
			pax1.setContact(new ArrayList<ContactData>());
			pax1.getContact().add(new ContactData());
			pax1.getContact().get(0).setContactType(ContactTypeEnum.M);
			pax1.getContact().get(0).setPhoneNumber("32131212");
			pax1.getContact().get(0).setPrefix(new PhonePrefixData());
			pax1.getContact().get(0).getPrefix().setCode("IT");
			pax1.getContact().get(0).getPrefix().setDescription("Italy");
			pax1.getContact().get(0).getPrefix().setPrefix("39");
			pax1.getContact().add(new ContactData());
			pax1.getContact().get(1).setContactType(ContactTypeEnum.M);
			pax1.getContact().get(1).setPhoneNumber("massimo.rubinelli@eng.it");
			pax1.getContact().get(1).setPrefix(new PhonePrefixData());
			pax1.getContact().get(1).getPrefix().setCode("");
			pax1.getContact().get(1).getPrefix().setDescription("Email");
			pax1.getContact().get(1).getPrefix().setPrefix("");
			pax1.setCountry(null);
			pax1.setEmail("massimo.rubinelli@eng.it");
			pax1.setSkyBonusCode(null);
			pax1.setSubscribeToNewsletter(Boolean.TRUE);
			prn.getPassengers().add(pax1);
			
			prn.setPayment(null);
			
			//prn.setRoutesList(new ArrayList<RouteData>());
			//prn.getRoutesList().add(new RouteData());
			//prn.getRoutesList().get(0).set
			
			InfoPassengerCreatePNRResponse delegateResponse = 
					infoPassengerDelegate.createPNRRequest(delegateRequest);
			
			writer.write(delegateResponse.getPrenotation().toString());	
		
		} catch (Exception e) {
			e.printStackTrace(writer);
			logger.error("Exception while executing doGet.", e);
		}
		
		logger.info("Executed doGet.");
	}
	
}
