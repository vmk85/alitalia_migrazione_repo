package com.alitalia.aem.testbundle.service.impl;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.jcr.api.SlingRepository;

import com.alitalia.aem.testbundle.service.HelloService;

/**
 * One implementation of the {@link HelloService}. Note that
 * the repository is injected, not retrieved.
 */
@Service
@Component(metatype = false)
public class HelloServiceImpl implements HelloService {
    
    @Reference
    private SlingRepository repository;

    public String getRepositoryName() {
//    	return "test";
//        return repository.getDescriptor(Repository.REP_NAME_DESC);
    	try {
			com.alitalia.aem.ws.booking.TestClient testClient = new com.alitalia.aem.ws.booking.TestClient();
			testClient.getData();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Servizio prova errore";
		}
    	return "Servizio prova";
    }

}
