<%@include file="/libs/foundation/global.jsp" %>
<body class="">

	<div class="mainWrapper">
        <cq:include script="header.jsp"/>
        <cq:include script="main.jsp"/>
        <cq:include script="footer.jsp"/>
    </div>

	<script>
       // handle injection of items from PHP
       var patternPartial = "pages-homepage";
       var lineage        = [{"lineagePattern":"templates-homepage","lineagePath":"..\/..\/patterns\/03-templates-01-homepage\/03-templates-01-homepage.html","lineageState":"inprogress"}];
       var lineageR       = [];
       var patternState   = "inprogress";
       var cssEnabled     = false;
    </script>

    <script>       
       var scriptLoader = {
       	
       	run: function(js,cb,target) {
       		var s  = document.getElementById(target+'-'+cb); 
       		for (var i = 0; i < js.length; i++) {
       			var src = (typeof js[i] != "string") ? js[i].src : js[i];
       			var c   = document.createElement('script');
       			c.src   = '../../'+src+'?'+cb;
       			if (typeof js[i] != "string") {
       				if (js[i].dep !== undefined) {
       					c.onload = function(dep,cb,target) {
       						return function() {
       							scriptLoader.run(dep,cb,target);
       						}
       					}(js[i].dep,cb,target);
       				}
       			}
       			s.parentNode.insertBefore(c,s);
       		}
       	}
       	
       }
    </script>
    
    <script id="pl-js-polyfill-insert-1432027941">
       (function() {
       	if (self != top) {
       		var cb = '1432027941';
       		var js = [];
       		if (typeof document !== "undefined" && !("classList" in document.documentElement)) {
       			js.push("styleguide/js/vendor/classlist-polyfill.js");
       		}
       		scriptLoader.run(js,cb,'pl-js-polyfill-insert');
       	}
       })();
    </script>
    <script id="pl-js-insert-1432027941">
       (function() {
       	if (self != top) {
       		var cb = '1432027941';
       		var js = [ { "src": "styleguide/js/vendor/jwerty.js", "dep": [ "styleguide/js/postmessage.js", { "src": "data/annotations.js", "dep": [ "styleguide/js/annotations-pattern.js" ] }, "styleguide/js/code-pattern.js" ] } ];
       		scriptLoader.run(js,cb,'pl-js-insert');
       	}
       })();
    </script>

</body>
