<%@page import="com.day.cq.wcm.foundation.Image,
	java.util.List,
	java.util.ArrayList,
	java.util.Iterator,
	org.apache.sling.commons.json.JSONArray,
	org.apache.sling.commons.json.JSONException,
	com.day.cq.wcm.api.WCMMode" %>
<%@include file="/libs/foundation/global.jsp"%>

<%!
	public static class SlideShowImage extends Image {

        private String titleText;
        private String descPrice;
        private String descText;

        public String getDescText() { return descText; }
        public void setDescText(String descText) { this.descText = descText; }

        public String getDescPrice() { return descPrice; }
        public void setDescPrice(String descPrice) { this.descPrice = descPrice; }

        public String getTitleText() { return titleText; }
        public void setTitleText(String titleText) { this.titleText = titleText; }

        public SlideShowImage(Resource resource) { super(resource); }
    }

    public static List<SlideShowImage> getImages(Resource resource, String name) {

        List<SlideShowImage> images = new ArrayList<SlideShowImage>();
        Resource imagesResource = resource.getChild(name);

        if (imagesResource == null) {
            return images;
        }

        ValueMap map = imagesResource.adaptTo(ValueMap.class);
        String order = map.get("order", String.class);

        if (order == null) {
            return images;
        }

        JSONArray array;
        ValueMap vMap;

        try {
            array = new JSONArray(order);
        } catch (JSONException e) {
            array = new JSONArray();
        }

        for (int i = 0; i < array.length(); i++) {

            String imageResourceName;

            try {
                imageResourceName = array.getString(i);
            } catch (JSONException e) {
                imageResourceName = null;
            }

            if (imageResourceName != null) {

                Resource imageResource = imagesResource.getChild(imageResourceName);
                if (imageResource != null) {

                    Iterator childImagesItr = imageResource.listChildren();
                    while (childImagesItr.hasNext()) {

                        Resource childImage = (Resource)childImagesItr.next();
                        SlideShowImage img = new SlideShowImage(childImage);
                        img.setItemName(Image.PN_REFERENCE, "imageReference");
                        img.setSelector("img");
                        img.setAlt(childImage.getName());

                        vMap = imageResource.adaptTo(ValueMap.class);
                        img.setTitleText(vMap.get("titleText", String.class));
                        img.setDescPrice(vMap.get("descPrice", String.class));
                        img.setDescText(vMap.get("descText", String.class));

                        images.add(img);
                    }
                }
            }
        }

        return images;
    }
%>

<% pageContext.setAttribute("images", getImages(resource, "images")); %>

<cq:includeClientLib categories="experience-aem.components"/>
<c:choose>
    <c:when test="${empty images}">
        <% if (WCMMode.fromRequest(request) != WCMMode.PREVIEW) { %>
        	<br/>Aggiungi banner al carosello<br/><br/>
        <% } %>
    </c:when>

    <c:otherwise>
        <div class="heroCarousel">
            <ul class="heroCarousel__list">
                <c:forEach var="image" varStatus="status" items="${images}">
                    <li class="heroCarousel__item${status.first ? ' active' : ''}">
                        <div style="background-image: url(${image.src})" alt="${image.alt}"
                        	class="heroCarousel__image"></div>
                        <div class="heroCarousel__textContainer">
                            <h2>${image.titleText}</h2>
                            <p class="offer">da <span>${image.descPrice}</span> &euro;</p>
                            <p class="subtitles">${image.descText}</p>
						</div>
                    </li>
                </c:forEach>
            </ul>
        </div>
    </c:otherwise>
</c:choose>