<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/libs/foundation/global.jsp" %>
<section class="menu mainMenu">
		<div class="mod">
			<a class="mainMenu__hamburger mobile j-openMobileMenu"
				href="javascript:;"> <span class="hamburger">&#9776;</span> <span
				class="close">&times;</span>
			</a> <span class="mobileSearch1"></span>

			<!--<a href="javascript:;" class="j-openLoignMenu mainMenu__login mobile"></a>-->
			<div class="mainMenu__logo"></div>


			<nav class="mainMenu__navigation">
				<ul>
					<li><a href="#">Destinazioni</a></li>
					<li><a href="#">Offerte</a></li>
					<li><a href="#">Volare Alitalia</a></li>
					<li><a href="#">Millemiglia</a></li>

					<li class="mainMenu__navigationSearch"><label
						class="mainMenu__navigationSearchIcon" for="mainMenu__search"></label>
						<input class="mainMenu__navigationSearchIcon" type="text"
						id="mainMenu__search" name="mainMenu__search" placeholder="Cerca">


						<a class="mainMenu__navigationSearchSubmit" href="javascript:;">Vai</a>
					</li>
				</ul>
			</nav>

		</div>
	</section>

	<div class="mobileMenuWrapper">
		<div class="mobileMenuWrapper__container">
			<a class="j-mobileMenuWrapper__closer" href="javscript:;"><h2
					class="mobileMenuWrapper__title">MENU</h2></a>
			<ul class="mobileMenuWrapper__listTop">
				<li class="mobileMenuWrapper__listItem--search"><label
					class="mobileSearch" for="search"></label> <input
					class="mobileSearch" type="text" id="search" name="search"
					placeholder="Cerca"> <a class=""
					href="javascript:;">Vai</a></li>
				<li class="mobileMenuWrapper__listItem"><a class="" href="#">Home</a>
				</li>
				<li class="mobileMenuWrapper__listItem"><a class="j-mobileLink"
					href="#" data-menu="menuMobileDestination">Destinazioni</a></li>
				<li class="mobileMenuWrapper__listItem"><a class="j-mobileLink"
					href="#" data-menu="menuMobileOffers">Offerte</a></li>
				<li class="mobileMenuWrapper__listItem"><a class="j-mobileLink"
					href="#" data-menu="menuMobileOffers">Volare Alitalia</a></li>
				<li class="mobileMenuWrapper__listItem"><a class="j-mobileLink"
					href="#" data-menu="menuMobileMiles">Millemiglia</a></li>
			</ul>
		</div>
		<div class="mobileMenuWrapper__container secondLevel"
			data-menu="menuMobileDestination">
			<h2 class="mobileMenuWrapper__title">
				<a class="mobileMenuWrapper__goBack j-mobileLinkBack"
					href="javascript:;">Destinazioni</a>
			</h2>

			<a class="openAccordionMobile j-openAccordionMobile"
				href="javascript:;"><h3>Italia</h3></a>
			<ul class="mobileMenuWrapper__listAccordion">
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						TBD </a></li>
			</ul>


			<a class="openAccordionMobile j-openAccordionMobile"
				href="javascript:;"><h3>Mondo</h3></a>
			<ul class="mobileMenuWrapper__listAccordion">
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						TBD </a></li>
			</ul>

		</div>
		<div class="mobileMenuWrapper__container secondLevel"
			data-menu="menuMobileOffers">
			<h2 class="mobileMenuWrapper__title">
				<a class="mobileMenuWrapper__goBack j-mobileLinkBack"
					href="javascript:;">Offerte</a>
			</h2>

			<a class="openAccordionMobile j-openAccordionMobile"
				href="javascript:;"><h3>offerte giovani</h3></a>
			<ul class="mobileMenuWrapper__listAccordion">
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						TBD </a></li>
			</ul>


			<a class="openAccordionMobile j-openAccordionMobile"
				href="javascript:;"><h3>giovani last minute</h3></a>
			<ul class="mobileMenuWrapper__listAccordion">
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						TBD </a></li>
			</ul>


			<a class="openAccordionMobile j-openAccordionMobile"
				href="javascript:;"><h3>offerte family</h3></a>
			<ul class="mobileMenuWrapper__listAccordion">
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						TBD </a></li>
			</ul>


			<a class="openAccordionMobile j-openAccordionMobile"
				href="javascript:;"><h3>offerta militari/polizia</h3></a>
			<ul class="mobileMenuWrapper__listAccordion">
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						TBD </a></li>
			</ul>

		</div>
		<div class="mobileMenuWrapper__container secondLevel"
			data-menu="menuMobileMiles">
			<h2 class="mobileMenuWrapper__title">
				<a class="mobileMenuWrapper__goBack j-mobileLinkBack"
					href="javascript:;">Millemiglia</a>
			</h2>

			<a class="openAccordionMobile j-openAccordionMobile"
				href="javascript:;"><h3>Il Programma</h3></a>
			<ul class="mobileMenuWrapper__listAccordion">
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						Il Programma </a></li>
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						Regolamento </a></li>
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						Perch� iscriverti? </a></li>
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						Novit� del programma </a></li>
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						MilleMiglia per le aziende </a></li>
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						MilleMiglia Young </a></li>
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						MilleMiglia Kids </a></li>
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						Mostra di più</a></li>
			</ul>


			<a class="openAccordionMobile j-openAccordionMobile"
				href="javascript:;"><h3>club esclusivi</h3></a>
			<ul class="mobileMenuWrapper__listAccordion">
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						Vantaggi </a></li>
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						Club Ulisse </a></li>
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						Club Freccia Alata </a></li>
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						Club Freccia Alata Plus </a></li>
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						Carta Freccia Alata Plus Per Sempre </a></li>
			</ul>


			<a class="openAccordionMobile j-openAccordionMobile"
				href="javascript:;"><h3>le miglia con alitalia</h3></a>
			<ul class="mobileMenuWrapper__listAccordion">
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						Accumula miglia </a></li>
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						Spendi miglia in voli </a></li>
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						Cash &amp; Miles </a></li>
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						Spendi miglia in servizi </a></li>
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						MilleMiglia Gallery </a></li>
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						Mancati accrediti </a></li>
			</ul>


			<a class="openAccordionMobile j-openAccordionMobile"
				href="javascript:;"><h3>le miglia con i partner aerei</h3></a>
			<ul class="mobileMenuWrapper__listAccordion">
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						TBD </a></li>
			</ul>


			<a class="openAccordionMobile j-openAccordionMobile"
				href="javascript:;"><h3>Le miglia con altri Partner</h3></a>
			<ul class="mobileMenuWrapper__listAccordion">
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						TBD </a></li>
			</ul>


			<a class="openAccordionMobile j-openAccordionMobile"
				href="javascript:;"><h3>acquista miglia</h3></a>
			<ul class="mobileMenuWrapper__listAccordion">
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						TBD </a></li>
			</ul>


			<a class="openAccordionMobile j-openAccordionMobile"
				href="javascript:;"><h3>News e promozioni</h3></a>
			<ul class="mobileMenuWrapper__listAccordion">
				<li class="mobileMenuWrapper__listAccordionItem"><a href="#">
						TBD </a></li>
			</ul>

		</div>

	</div>
	<a class="j-mobileMenuWrapper__closer mobileMenuWrapper__closer"
		href="javascript:;"></a>