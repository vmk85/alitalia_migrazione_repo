<%@include file="/libs/foundation/global.jsp" %>
<div class="flightFinder ff-demo ff-stage4">
  <form action="">
     <div class="flightFinder__container">
        <a href="javascript:;" class="flightFinder__containerClose j-flightFinder__containerClose">x chiudi</a>
        <div class="flightFinder__row selection">
           <label class="departure" for="departureInput">Dove vuoi andare?</label>
           <div class="customInput--flightFinder departure">
              <span class="apt">ROMA</span>
              <span class="city">FCO</span>
              <input class="departure" id="departureInput" name="departureInput" value="Fiumicino" required="" type="text">
           </div>
           <div class="customSelect departureMobileInput">
              <select name="" id="">
                 <optgroup label="ITALIA">
                    <option value="">Alghero</option>
                    <option value="">Ancona</option>
                    <option value="">Bari</option>
                    <option value="">Milano - Lin</option>
                    <option value="" selected="selected">Roma - FCO</option>
                 </optgroup>
                 <optgroup label="EUROPA">
                    <option value="">Parigi</option>
                    <option value="">Londra</option>
                    <option value="">Berlino</option>
                 </optgroup>
              </select>
           </div>
           <label class="arrival" for="arrivalInput">destinazione</label>
           <div class="customInput--flightFinder arrival">
              <span class="apt">MILANO</span>
              <span class="city">LIN</span>
              <input class="arrival" id="arrivalInput" name="arrivalInput" placeholder="LA TUA DESTINAZIONE" required="" type="text">
              <div class="flightFinder__suggest">
                 <ul>
                    <li>
                       <h4>Le tue ultime ricerche</h4>
                    </li>
                    <li><a href="#"><strong>Milano</strong> - Italia <span class="apt">LIN</span></a></li>
                    <li><a href="#"><strong>New York</strong> - USA <span class="apt">JFK</span></a></li>
                    <li><a href="#"><strong>Parigi</strong> - Francia <span class="apt">CDG</span></a></li>
                    <li><a href="#"><strong>Amsterdam</strong> - Paesi Bassi <span class="apt">AMS</span></a></li>
                 </ul>
              </div>
              <div class="flightFinder__complete">
                 <ul>
                    <li><a href="#"><strong>Milano</strong> - Italia <span class="apt">LINATE</span></a></li>
                    <li><a href="#"><strong>Milano</strong> - Italia <span class="apt">MALPENSA</span></a></li>
                    <li><a href="#"><strong>Milwaukee</strong> - USA <span class="apt">Gneral Mithell Field</span></a></li>
                 </ul>
              </div>
           </div>
           <div class="customSelect arrivalMobileInput">
              <select name="" id="">
                 <optgroup label="ITALIA">
                    <option value="">Alghero</option>
                    <option value="">Ancona</option>
                    <option value="">Bari</option>
                    <option value="" selected="selected">Milano - Lin</option>
                    <option value="">Roma - FCO</option>
                 </optgroup>
                 <optgroup label="EUROPA">
                    <option value="">Parigi</option>
                    <option value="">Londra</option>
                    <option value="">Berlino</option>
                 </optgroup>
              </select>
           </div>
        </div>
        
        <div class="flightFinder__row">
           <div class="flightFinder__rowLeft">
              Quando vuoi partire 
           </div>
           <div class="flightFinder__rowRight">
              <div class="flightFinder__tratte typeOfFlight">
                 <ol>
                    <li>
                       <label class="" for="andataeritorno">
                       <input name="tipologia_tratta" value="a+r" id="andataeritorno" checked="checked" type="radio">
                       andata e ritorno
                       </label>
                    </li>
                    <li>
                       <label class="" for="soloandata">
                       <input name="tipologia_tratta" value="a" id="soloandata" type="radio">
                       solo andata
                       </label>
                    </li>
                    <li>
                       <a href="#">multitratta &gt;</a>
                    </li>
                 </ol>
              </div>
              <div class="flightFinder__tratte datePicker">
                 <ol>
                    <li>
                       <label class="" for="andata">Andata</label>
                       <input class="" id="andata" name="andata" placeholder="Andata" required="" type="text">
                    </li>
                    <li>
                       <label class="" for="ritorno">Ritorno</label>
                       <input class="" id="ritorno" name="ritorno" placeholder="Ritorno" required="" type="text">
                    </li>
                    <li>
                       <div class="formInput class">
                          <input class="" id="flexibleDate" name="flexibleDate" type="checkbox"><label class="class" for="flexibleDate">Date Flessibli</label>
                       </div>
                    </li>
                 </ol>
              </div>
           </div>
        </div>
        
        <div class="flightFinder__row">
           <div class="flightFinder__rowLeft">
              Chi viagger&agrave;
           </div>
           <div class="flightFinder__rowRight">
              <div class="flightFinder__tratte passengers">
                 <ol>
                    <li class="lineLabel">
                       <label class="" for="adult">Adulti</label>
                       <input class="" id="adult" name="adult" required="" type="hidden">
                       <div class="numberSelector j-numberSelector">
                          <a class="numberSelector__sign--plus j-numberSelector-plus" href="javascript:;">+</a>
                          <div class="numberSelector__container j-numberSelector-container">1</div>
                          <a class="numberSelector__sign--minus j-numberSelector-minus" href="javascript:;">-</a>
                          <input class="j-numberSelector-input" value="0" type="hidden">
                       </div>
                    </li>
                    <li class="lineLabel">
                       <label class="" for="kids">Bambini 2-14 anni</label>
                       <input class="" id="kids" name="kids" required="" type="hidden">
                       <div class="numberSelector j-numberSelector">
                          <a class="numberSelector__sign--plus j-numberSelector-plus" href="javascript:;">+</a>
                          <div class="numberSelector__container j-numberSelector-container">0</div>
                          <a class="numberSelector__sign--minus j-numberSelector-minus" href="javascript:;">-</a>
                          <input class="j-numberSelector-input" value="0" type="hidden">
                       </div>
                    </li>
                    <li class="lineLabel">
                       <label class="" for="adult">Neonati 0-23 mesi</label>
                       <input class="" id="adult" name="adult" required="" type="hidden">
                       <div class="numberSelector j-numberSelector">
                          <a class="numberSelector__sign--plus j-numberSelector-plus" href="javascript:;">+</a>
                          <div class="numberSelector__container j-numberSelector-container">0</div>
                          <a class="numberSelector__sign--minus j-numberSelector-minus" href="javascript:;">-</a>
                          <input class="j-numberSelector-input" value="0" type="hidden">
                       </div>
                    </li>
                 </ol>
              </div>
              <div class="flightFinder__tratte militaryPolice">
                 <ol>
                    <li>
                       <div class="formInput class">
                          <input class="" id="militariaPolizia" name="militariaPolizia" type="checkbox"><label class="class" for="militariaPolizia">Militari / Polizia</label>
                       </div>
                    </li>
                    <li>
                       <div class="formInput class">
                          <input class="" id="young" name="young" type="checkbox"><label class="class" for="young">Giovane fino a 26 anni</label>
                       </div>
                    </li>
                 </ol>
              </div>
           </div>
        </div>
        
        <div class="flightFinder__row">
           <div class="flightFinder__rowLeft">
              Classe di viaggio
           </div>
           <div class="flightFinder__rowRight">
              <div class="flightFinder__tratte cabinClass">
                 <ol>
                    <li>
                       <label class="" for="economy">
                       <input name="classOfFlight" value="economy" id="economy" checked="checked" type="radio">
                       Economy
                       </label>
                    </li>
                    <li>
                       <label class="" for="business">
                       <input name="classOfFlight" value="business" id="business" type="radio">
                       Business
                       </label>
                    </li>
                 </ol>
              </div>
           </div>
        </div>
        
        <div class="flightFinder__row--button">
           <a class="flightFinder__button" href="javascript:;">mostra voli</a>
           <a class="flightFinder__link" href="javascript:;">Biglietti premio &gt;</a>
        </div>
     </div>
  </form>
</div>