<%@page import="com.day.cq.wcm.foundation.Image,
	java.util.List,
	java.util.ArrayList,
	java.util.Iterator,
	org.apache.sling.commons.json.JSONArray,
	org.apache.sling.commons.json.JSONException,
	com.day.cq.wcm.api.WCMMode" %>
<%@include file="/libs/foundation/global.jsp"%>

<%!
	public static class ImageIcon{

        private String link;
        private Image image;


        public String getLink() { return link; }
        public Image getImage() { return image; }

        public void setLink(String l) { this.link = l; }
        public void setImage(Image i) { this.image = i; }
    }

    public static List<ImageIcon> getImages(Resource resource) {

        List<ImageIcon> linkimage = new ArrayList<ImageIcon>();
        Resource imagesResource = resource.getChild("images");

        if (imagesResource == null) {
            return linkimage;
        }

        ValueMap map = imagesResource.adaptTo(ValueMap.class);
        String order = map.get("order", String.class);

        if (order == null) {
            return linkimage;
        }

        JSONArray array;
        ValueMap vMap;

        try {
            array = new JSONArray(order);
        } catch (JSONException e) {
            array = new JSONArray();
        }

        for (int i = 0; i < array.length(); i++) {

            String imageResourceName;

            try {
                imageResourceName = array.getString(i);
            } catch (JSONException e) {
                imageResourceName = null;
            }

            if (imageResourceName != null) {

                Resource imageResource = imagesResource.getChild(imageResourceName);
                if (imageResource != null) {

                    Iterator childImagesItr = imageResource.listChildren();
                    while (childImagesItr.hasNext()) {

                        Resource childImage = (Resource)childImagesItr.next();
                        Image img = new Image(childImage);
                        img.setItemName(Image.PN_REFERENCE, "imageReference");
                        img.setSelector("img");
                        img.setAlt(childImage.getName());

                        vMap = imageResource.adaptTo(ValueMap.class);

						ImageIcon elem = new ImageIcon();
                        elem.setLink(vMap.get("link", String.class));
						elem.setImage(img);
                        linkimage.add(elem);
                    }
                }
            }
        }

        return linkimage;
    }
%>

<% pageContext.setAttribute("images", getImages(resource)); %>

<cq:includeClientLib categories="experience-aem.components"/>
<c:choose>
    <c:when test="${empty images}">
        <% if (WCMMode.fromRequest(request) != WCMMode.PREVIEW) { %>
        	<br/>Aggiungi Social Icon<br/><br/>
        <% } %>
    </c:when>

    <c:otherwise>
        <div class="socialImage">
            <ul class="socialImage__list">
                <c:forEach var="elem" varStatus="status" items="${images}">
                    <li class="socialImage__item${status.first ? ' active' : ''}">
						<div class="heroCarousel__image">
                        <a href="${elem.link}">
                            <img src="${elem.image.src}" width="20px" height="20px" alt="${image.alt}" />
						</a>
                        </div>
                    </li>
                </c:forEach>
            </ul>
        </div>
    </c:otherwise>
</c:choose>