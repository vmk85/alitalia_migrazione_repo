<%@include file="/libs/foundation/global.jsp" %>
<%@page import="com.day.cq.commons.Doctype,com.day.cq.wcm.foundation.Image" %>

<% if (resource == null) { %>
		<img src="/content/dam/sitealitaliademo/ffffff_002.gif" />

<% } else {
		Image img = new Image(resource);
        img.setSelector(".img");
    	img.setDoctype(Doctype.fromRequest(request));
    	img.draw(out);
   } %>