<%@page import="com.day.cq.wcm.foundation.Image,
	java.util.List,
	java.util.ArrayList,
	java.util.Iterator,
	org.apache.sling.commons.json.JSONArray,
	org.apache.sling.commons.json.JSONException,
	com.day.cq.wcm.api.WCMMode" %>
<%@include file="/libs/foundation/global.jsp"%>

<%!
	public static class MenuJustClick{

		private String titolo;
        private String link;


        public String getLink() { return link; }
        public String getTitolo() { return titolo; }

        public void setLink(String l) { this.link = l; }
        public void setTitolo(String t) { this.titolo = t; }
    }

    public static List<MenuJustClick> getMenuJustClick(Resource resource) {

        List<MenuJustClick> items = new ArrayList<MenuJustClick>();
        Resource linksResource = resource.getChild("links");

        if (linksResource == null) {
            return items;
        }

        ValueMap map = linksResource.adaptTo(ValueMap.class);
        String order = map.get("order", String.class);

        if (order == null) {
            return items;
        }

        JSONArray array;
        ValueMap vMap;

        try {
            array = new JSONArray(order);
        } catch (JSONException e) {
            array = new JSONArray();
        }

        for (int i = 0; i < array.length(); i++) {

            String linkResourceName;

            try {
                linkResourceName = array.getString(i);
            } catch (JSONException e) {
                linkResourceName = null;
            }

            if (linkResourceName != null) {

                Resource linkResource = linksResource.getChild(linkResourceName);
                if (linkResource != null) {

                    //Iterator childImagesItr = linkResource.listChildren();
                    // while (childImagesItr.hasNext()) {

                        /*Resource childImage = (Resource)childImagesItr.next();
                        Image img = new Image(childImage);
                        img.setItemName(Image.PN_REFERENCE, "imageReference");
                        img.setSelector("img");
                        img.setAlt(childImage.getName());*/

                        vMap = linkResource.adaptTo(ValueMap.class);

						MenuJustClick elem = new MenuJustClick();
                        elem.setLink(vMap.get("link_SM", String.class));
						elem.setTitolo(vMap.get("titolo_SM", String.class));
                        String t = elem.getTitolo();
                        //out.println(t);
                        items.add(elem);
                    //}
                }
            }
        }

        return items;
    }
%>

<%  pageContext.setAttribute("links", getMenuJustClick(resource));  %>
<cq:includeClientLib categories="experience-aem.components"/>
<c:choose>
    <c:when test="${empty links}">
        <% if (WCMMode.fromRequest(request) != WCMMode.PREVIEW) { %>
        	<br/>Aggiungi Voce Menu<br/><br/>
        <% } %>
    </c:when>

    <c:otherwise> 
        <c:forEach var="elem" varStatus="status" items="${links}">
        	<nav class="footer__nav ">

	 				<div class="footer__navTitle justLink">
	 					<div class="verticalAlignBottom">
                            <a href="${elem.link}"><h3>${elem.titolo}</h3></a>
	 					</div>
	 				</div>
	 		</nav>
		</c:forEach>
    </c:otherwise>
</c:choose>