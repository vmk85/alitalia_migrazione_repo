<%@include file="/libs/foundation/global.jsp" %>
<div class="destinationCarousel">
  <div class="destinationCarousel__header">
     <h2 class="destinationCarousel__title">offerte SELEZIONATE PER TE Da</h2>
     <div class="customSelect">
        <select name="" id="">
           <option selected="selected" value="rome">Roma (FCO)</option>
           <option value="milano">Milano (LIN)</option>
           <option value="palermo">Palermo (PMO)</option>
        </select>
     </div>
  </div>
  <div class="destinationCarousel__listContainer">
     <a class="destinationCarousel__arrow left" href="#"></a>
     <ul class="destinationCarousel__list">
        <li class="destinationCarousel__item">
           <a class="destinationCarousel__itemLink" href="javascript:;">
              <div class="destinationCarousel__imageContainer">
                  <img class="destinationCarousel__image" src="/content/dam/sitealitaliademo/666666.gif" alt="whatever">
              </div>
              <div class="destinationCarousel__textContainer">
                 <div class="leftBox">
                    <h2>lamezia terme</h2>
                    <p class="subtitles">Andata e ritorno</p>
                 </div>
                 <div class="rightBox">
                    <p class="offer">da <span class="price">59</span><span class="currency">&euro;</span> </p>
                 </div>
              </div>
           </a>
        </li>
        <li class="destinationCarousel__item">
           <a class="destinationCarousel__itemLink" href="javascript:;">
              <div class="destinationCarousel__imageContainer">
                 <img class="destinationCarousel__image" src="/content/dam/sitealitaliademo/666666.gif" alt="whatever">
              </div>
              <div class="destinationCarousel__textContainer">
                 <div class="leftBox">
                    <h2>francoforte</h2>
                    <p class="subtitles">Andata e ritorno</p>
                 </div>
                 <div class="rightBox">
                    <p class="offer">da <span class="price">131</span><span class="currency">&euro;</span> </p>
                 </div>
              </div>
           </a>
        </li>
        <li class="destinationCarousel__item">
           <a class="destinationCarousel__itemLink" href="javascript:;">
              <div class="destinationCarousel__imageContainer">
                 <img class="destinationCarousel__image" src="/content/dam/sitealitaliademo/666666.gif" alt="whatever">
              </div>
              <div class="destinationCarousel__textContainer">
                 <div class="leftBox">
                    <h2>boston</h2>
                    <p class="subtitles">Andata e ritorno</p>
                 </div>
                 <div class="rightBox">
                    <p class="offer">da <span class="price">606</span><span class="currency">&euro;</span> </p>
                 </div>
              </div>
           </a>
        </li>
     </ul>
     <a class="destinationCarousel__arrow right" href="#"></a>
  </div>
</div>