<%@include file="/libs/foundation/global.jsp" %>
<footer class="footer">
    <cq:include path="box-prefooter" resourceType="sitealitaliademo/components/content/box-prefooter" />
    <cq:include path="footer-menu" resourceType="sitealitaliademo/components/content/footer-menu" />
	<cq:include path="box-footer-loghi" resourceType="sitealitaliademo/components/content/box-footer-loghi" />
	<cq:include path="box-footer-federato" resourceType="sitealitaliademo/components/content/box-footer-federato" />
</footer>
