<%@page session="false" contentType="text/html; charset=utf-8"
    import="com.day.cq.commons.Doctype, com.day.cq.wcm.api.WCMMode, com.day.cq.wcm.foundation.ELEvaluator" %>

<%@taglib prefix="cq" uri="http://www.day.com/taglibs/cq/1.0" %>
<cq:defineObjects/>

<%
    String location = properties.get("redirectTarget", "");
    location = ELEvaluator.evaluate(location, slingRequest, pageContext);
    boolean wcmModeIsDisabled = WCMMode.fromRequest(request) == WCMMode.DISABLED;
    boolean wcmModeIsPreview = WCMMode.fromRequest(request) == WCMMode.PREVIEW;
    if ( (location.length() > 0) && ((wcmModeIsDisabled) || (wcmModeIsPreview)) ) {
        if (currentPage != null && !location.equals(currentPage.getPath()) && location.length() > 0) {
            final int protocolIndex = location.indexOf(":/");
            final int queryIndex = location.indexOf('?');
            final String redirectPath;
            if ( protocolIndex > -1 && (queryIndex == -1 || queryIndex > protocolIndex) ) {
                redirectPath = location;
            } else {
                redirectPath = slingRequest.getResourceResolver().map(request, location) + ".html";
            }
            response.sendRedirect(redirectPath);
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
        return;
    }
    if (currentDesign != null) {
        currentDesign.getDoctype(currentStyle).toRequest(request);
    }
%>

<!DOCTYPE html>
<html class="no-js<%= wcmModeIsPreview ? ' preview' : ''%>" lang="it">
    <cq:include script="head.jsp"/>
    <cq:include script="body.jsp"/>
</html>
