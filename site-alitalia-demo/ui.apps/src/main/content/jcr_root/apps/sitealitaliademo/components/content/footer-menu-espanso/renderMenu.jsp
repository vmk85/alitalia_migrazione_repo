<%@page import="com.day.cq.wcm.foundation.Image,
	java.util.List,
	java.util.ArrayList,
	java.util.Iterator,
	org.apache.sling.commons.json.JSONArray,
	org.apache.sling.commons.json.JSONException,
	com.day.cq.wcm.api.WCMMode" %>
<%@include file="/libs/foundation/global.jsp"%>

<%!
	public static class MenuEspanso{

		private String titolo;
        private String link;


        public String getLink() { return link; }
        public String getTitolo() { return titolo; }

        public void setLink(String l) { this.link = l; }
        public void setTitolo(String t) { this.titolo = t; }
    }

    public static List<MenuEspanso> getMenuEspanso(Resource resource) {

        List<MenuEspanso> items = new ArrayList<MenuEspanso>();
        Resource linksResource = resource.getChild("links");

        if (linksResource == null) {
            return items;
        }

        ValueMap map = linksResource.adaptTo(ValueMap.class);
        String order = map.get("order", String.class);

        if (order == null) {
            return items;
        }

        JSONArray array;
        ValueMap vMap;

        try {
            array = new JSONArray(order);
        } catch (JSONException e) {
            array = new JSONArray();
        }

        for (int i = 0; i < array.length(); i++) {

            String linkResourceName;

            try {
                linkResourceName = array.getString(i);
            } catch (JSONException e) {
                linkResourceName = null;
            }

            if (linkResourceName != null) {

                Resource linkResource = linksResource.getChild(linkResourceName);
                if (linkResource != null) {



                        vMap = linkResource.adaptTo(ValueMap.class);

						MenuEspanso elem = new MenuEspanso();
                        elem.setLink(vMap.get("link_ME", String.class));
						elem.setTitolo(vMap.get("titolo_ME", String.class));
                        String t = elem.getTitolo();

                        items.add(elem);

                }
            }
        }

        return items;
    }
%>

<%  pageContext.setAttribute("links", getMenuEspanso(resource));  %>
<cq:includeClientLib categories="experience-aem.components"/>
<c:choose>
    <c:when test="${empty links}">
        <% if (WCMMode.fromRequest(request) != WCMMode.PREVIEW) { %>
        	<br/>Aggiungi Menu<br/><br/>
        <% } %>
    </c:when>

    <c:otherwise> 
        <c:forEach var="elem" varStatus="status" items="${links}">
			<li><a href="${elem.link}">${elem.titolo}</a></li>
		</c:forEach>
    </c:otherwise>
</c:choose>