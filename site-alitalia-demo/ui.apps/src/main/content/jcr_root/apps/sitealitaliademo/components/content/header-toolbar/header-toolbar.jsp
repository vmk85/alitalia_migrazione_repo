<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/libs/foundation/global.jsp" %>
<section class="userMenu">
		<div class="mod">
			<div class="userMenu__groupLeft">

				<a class="userMenu__language j-userMenuLink" href="javascript:;"
					data-group="languageGroup"> <span class="userMenu__icon"></span>


					<span class="userMenu__text">Italia - Italiano</span>
				</a> <a class="userMenu__support j-userMenuLink" href="javascript:;"
					data-group="supportGroup"> <span class="userMenu__icon"></span>


					<span class="userMenu__text">Supporto</span>
				</a>
			</div>
			<div class="userMenu__groupRight">

				<a class="userMenu__recent j-userMenuLink" href="javascript:;"
					data-group="recentGroup"> <span class="userMenu__icon"></span>


					<span class="userMenu__text">Visti di recente</span>
				</a> <a class="userMenu__notifications j-userMenuLink"
					href="javascript:;" data-group="notificationGroup"> <span
					class="userMenu__icon"> <span
						class="userMenu__notificationsCount">2</span>
				</span> <span class="userMenu__text">Notifiche</span>
				</a> <a class="userMenu__login j-userMenuLink" href="javascript:;"
					data-group="loginGroup"> <span class="userMenu__icon"></span> <span
					class="userMenu__text">Accedi</span>
				</a>
			</div>
		</div>
	</section>

	<div class="languageMenu j-groupContainer" data-group="languageGroup">
		<div class="mod">

			<div class="languageMenu__group first">

				<div class="customSelect">
					<select name="" id="">
						<option value="ITALIA">ITALIA</option>
					</select>
				</div>

				<div class="customSelect">
					<select name="" id="">
						<option value="ITALIANO">ITALIANO</option>
					</select>
				</div>

				<a href="javascript:;" class="languageMenu__button">Conferma</a>

				<div class="languageMenu__checkbox">
					<input id="rememberMe" type="checkbox"> <label
						for="rememberMe">ricorda la scelta</label>
				</div>

			</div>
			<div class="languageMenu__group second">

				<p>Oppure seleziona un paese</p>
				<a class="it" href="#">IT</a> <a class="us" href="#">US</a> <a
					class="uk" href="#">UK</a> <a class="fr" href="#">FR</a> <a
					class="es" href="#">ES</a>
			</div>

		</div>
	</div>
	<div class="supportMenu j-groupContainer" data-group="supportGroup">
		<div class="mod">
			<div class="supportMenu__container">
				<div class="supportMenu__group">
					<h3>ASSISTENZA BAGAGLI</h3>
					<ul>
						<li><a href="#">Consegna in ritardo</a></li>
						<li><a href="#">Bagaglio smarrito</a></li>
						<li><a href="#">Bagaglio danneggiato</a></li>
						<li><a href="#">Bagaglio manomesso</a></li>
					</ul>
				</div>
				<div class="supportMenu__group">
					<h3>UTILIT&agrave;</h3>
					<ul>
						<li><a href="#">Oggetti smarriti</a></li>
						<li><a href="#">In caso di sciopero</a></li>
						<li><a href="#">Volo cancellato</a></li>
						<li><a href="#">Assistenze speciali</a></li>
					</ul>
				</div>
				<div class="supportMenu__group">
					<h3>CONTATTI E ASSISTENZA</h3>
					<ul>
						<li><a href="#">Cambio prenotazione</a></li>
						<li><a href="#">Servizio clienti Millemiglia</a></li>
						<li><a href="#">Contatto Alitalia nel mondo</a></li>
						<li><a href="#">Segnalazioni o reclami</a></li>
					</ul>
				</div>
				<div class="supportMenu__group">
					<h3 class="withPhoneIcon">supporto telefonico</h3>
					<ul>
						<li>Dall'Italia: 89 20 10</li>
						<li><span class="infoIcon">Numero a pagamento </span></li>
						<li>Dall'estero: +39 06 65649</li>
					</ul>
				</div>
			</div>
		</div>
	</div>


	<div class="loginMenu j-groupContainer" data-group="loginGroup">
		<div class="mod">
			<div class="loginMenu__container">
				<h3 class="loginMenu__title deviceOnly">Accedi o crea un
					profilo</h3>
				<div class="loginMenu__containerPart login">
					<div class="loginMenu__block">


						<label class="login__name" for="loginId">Codice
							MilleMiglia</label> <input class="login__name" type="text" id="loginId"
							name="loginId" placeholder="Codice MilleMiglia" required>



						<div class="formInput login__rememberMe">
							<input class="" id="rememberMe" name="rememberememberMe"
								type="checkbox"><label class="login__rememberMe"
								for="rememberMe">Remember Me</label>
						</div>
					</div>
					<div class="loginMenu__block">

						<label class="login__pin" for="loginPin">Pin</label> <input
							class="login__pin" type="password" id="loginPin" name="loginPin"
							placeholder="PIN" required> <a class="login__recoverPin"
							href="javascript:;">Recupera PIN</a>
					</div>
					<div class="loginMenu__block">
						<a class="loginMenu__buttonAccess" href="javascript:;">Accedi</a>
					</div>
				</div>

				<div class="loginMenu__containerPart social">
					<h3>Oppure effettua il Social Login</h3>
					<a href="javascript:;" class="loginMenu__social"></a>
				</div>
				<div class="loginMenu__containerPart newUser">

					<h3>Non sei registrato?</h3>
					<a class="loginMenu__register" href="">REGISTRATI</a>
				</div>

			</div>
		</div>
	</div>

	<div class="notificationMenu j-groupContainer"
		data-group="notificationGroup">
		<div class="notificationMenu__container">
			<ul class="notificationMenu__list">
				<li class="notificationMenu__item"><a
					class="notificationMenu__itemLink j-readNotification"
					href="javascript:;"> Se parti entro il 23 giugno Roma - Berlino
						60 € Andata e Ritorno tutto incluso <span>Vai all'offerta</span>
				</a></li>
				<li class="notificationMenu__item"><a
					class="notificationMenu__itemLink j-readNotification"
					href="javascript:;"> Solo questa settimana <bold> Roma -
						New York da 549 €</bold> Andata e Ritorno tutto incluso <span>Vai
							all'offerta</span>
				</a></li>
			</ul>
			<!--<a class="notificationMenu__button" href="javascript:;">
				Accedi
			</a>-->
			<p class="notificationMenu__buttonDesc">Per ricevere offerte
				personalizzate e bonus col programma "Millemiglia".</p>
		</div>
	</div>

	<div class="recentMenu j-groupContainer" data-group="recentGroup">
		<div class="recentMenu__container">
			<ul class="recentMenu__list">
				<li>
					<div>ROMA - LONDRA</div>
					<div>Andata e ritorno</div>
					<div>28 GEN - 04 FEB - Economy / 2 passeggeri</div>
				</li>
				<li>
					<div>ROMA - MIAMI</div>
					<div>Andata</div>
					<div>22 GEN - 01 FEB - Business / 1 passeggero</div>
				</li>
				<li>
					<div>ROMA - TORINO</div>
					<div>Andata e ritorno</div>
					<div>28 GEN - 04 FEB - Econ. / 3 passeggeri</div>
				</li>
			</ul>
			<!--<a class="recentMenu__button" href="javascript:;">
				Accedi
			</a>-->
			<p class="recentMenu__buttonDesc">per avere questa lista su tutti
				i tuoi dispositivi</p>

		</div>
	</div>