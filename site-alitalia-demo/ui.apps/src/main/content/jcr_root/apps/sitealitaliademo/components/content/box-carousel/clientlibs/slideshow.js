$(function() {

    var slideSwitch = function() {

        var shows = $('.heroCarousel__item.active');

        if (!shows || (shows.length === 0)) {
            return;
        }

        console.log(shows);

		$('.heroCarousel__item').removeClass('active');
        $(shows).next().length ? $(shows).next().addClass('active') : $('.heroCarousel__item:first').addClass('active');
    };

    setInterval(slideSwitch, 5000);
});