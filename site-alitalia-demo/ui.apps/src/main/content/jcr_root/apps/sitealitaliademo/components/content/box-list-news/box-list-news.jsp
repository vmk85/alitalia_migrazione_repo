<%@include file="/libs/foundation/global.jsp" %>

<%String titolo = properties.get("titolo", String.class);
		if(titolo == null){
			titolo = "Titolo";
        }
%>

<div class="newsBox list">
  <div class="newsBox__listNewsHeader">
     <h3><%= titolo %></h3>
     <a href="javascript:;">Tutte</a>
  </div>
  <ul class="newsBox__listNewsBody">
     <li>
        <a href="javascript:;">Titolo News</a>
        <p>Lorem ipsum dolor sit amet</p>
     </li>
     <li>
        <a href="javascript:;">Titolo News</a>
        <p>Lorem ipsum dolor sit amet</p>
     </li>
     <li>
        <a href="javascript:;">Titolo News</a>
        <p>Lorem ipsum dolor sit amet</p>
     </li>
  </ul>
</div>