"use strict";

$(document).ready(function(){function stickyHeaderNavigation() {
    var a = 0, b = 40, c = $(window);
    c.scroll(function(d) {
        a = c.scrollTop();
        $("body").hasClass("userMenuIsActive");
        0 > a && (a = 0), $(".menu.mainMenu").css(b > a ? {
            top: b - a
        } : {
            top: 0
        });
    }), c.trigger("scroll");
}

function breakpoint(a, b, c) {
    var d, e = 641, f = 901, g = $(window).width();
    return g > f ? (console.log("l", a), d = a) : g > e ? (console.log("m", b), d = b) : (console.log("s", c), 
    d = c), d;
}

function numberSelector() {
    $(".j-numberSelector-plus").on("click", function() {
        var a, b = $(this), c = b.closest(".j-numberSelector"), d = c.find(".j-numberSelector-input"), e = c.find(".j-numberSelector-container"), f = parseInt(d.val());
        a = f + 1, a > 10 && (a = 10), e.text(a), d.val(a);
    }), $(".j-numberSelector-minus").on("click", function() {
        var a, b = $(this), c = b.closest(".j-numberSelector"), d = c.find(".j-numberSelector-input"), e = c.find(".j-numberSelector-container"), f = parseInt(d.val());
        a = f - 1, 0 > a && (a = 0), e.text(a), d.val(a);
    });
}

function fixPositions() {
    if ($('.j-groupContainer[data-group="notificationGroup"]').length > 0) {
        var a = $('.j-groupContainer[data-group="notificationGroup"]'), b = $('.j-groupContainer[data-group="recentGroup"]'), c = $('.j-userMenuLink[data-group="notificationGroup"]'), d = $('.j-userMenuLink[data-group="recentGroup"]'), e = parseInt($(".header").find(".mod").css("margin-left").replace("px", "")), f = c.position().left, g = c.outerWidth(), h = parseInt(c.css("margin-left").replace("px", "")), i = a.width(), j = d.position().left, k = d.outerWidth(), l = parseInt(d.css("margin-left").replace("px", "")), m = b.width();
        a.css({
            left: f + e + g + h - i
        }), b.css({
            left: j + e + k + l - m
        });
    }
}

$(function() {
    $(".j-userMenuLink").on("click", function() {
        var a = $(this), b = a.hasClass("isActive"), c = a.data("group");
        if ($(".j-userMenuLink").removeClass("isActive"), $(".j-groupContainer").removeClass("isActive"), 
        $("body").removeClass("userMenuIsActive"), !b) {
            a.addClass("isActive"), $("body").addClass("userMenuIsActive");
            var d = $('.j-groupContainer[data-group="' + c + '"]');
            if ("notificationGroup" === c || "recentGroup" === c) {
                d.css({
                    left: a.position().left
                });
                var e = parseInt($(".header").find(".mod").css("margin-left").replace("px", "")), f = a.position().left, g = a.outerWidth(), h = parseInt(a.css("margin-left").replace("px", "")), i = d.width();
                d.css({
                    left: f + e + g + h - i
                }).addClass("isActive");
            } else d.addClass("isActive");
        }
    }), $(".j-readNotification").on("click", function() {
        $(this).parent().addClass("isRead");
        var a = $(".j-readNotification").parent().not(".isRead").length;
        $(".userMenu__notificationsCount").text(a);
    }), $(".j-openMobileMenu , .j-mobileMenuWrapper__closer").on("click", function() {
        $("body").toggleClass("showMenuMobile");
    }), $(".j-mobileLinkBack").on("click", function() {
        $(".mobileMenuWrapper__container").removeClass("isActive"), $(".j-openAccordionMobile").removeClass("isActive");
    }), $(".j-openLoignMenu").on("click", function() {
        $("body").toggleClass("showLoginMobile");
    }), $(".j-mobileLink").on("click", function() {
        var a = $(this).data("menu");
        $('.mobileMenuWrapper__container[data-menu="' + a + '"]').addClass("isActive");
    }), $(".j-openAccordionMobile").on("click", function() {
        var a = $(this), b = a.hasClass("isActive");
        $(".j-openAccordionMobile").removeClass("isActive"), b || a.addClass("isActive");
    }), $(".j-openFooterSections").on("click", function() {
        var a = $(this), b = a.closest(".mobileExpendable").hasClass("isActive");
        $(".mobileExpendable").removeClass("isActive"), b || a.closest(".mobileExpendable").addClass("isActive");
    }), $(".mainMenu__navigationSearchIcon").focusin(function(a) {
        var b = $(".mainMenu__navigation").width();
        $(".mainMenu__navigationSearch").width(b);
    }).focusout(function(a) {
        $(".mainMenu__navigationSearch").attr("style", "");
    }), $("#arrivalInput").focusin(function(a) {
        $(".flightFinder__container").addClass("showFlightFinderRow");
    }), $("#departureInput").focusin(function(a) {
        $(".flightFinder__container").removeClass("showFlightFinderRow");
    }), $(".customSelect.arrivalMobileInput select").on("change", function(a) {
        $(".flightFinder__container").addClass("showFlightFinderRow");
    }), $(".customSelect.departureMobileInput select").on("change", function(a) {
        $(".flightFinder__container").removeClass("showFlightFinderRow");
    }), $(".j-flightFinder__containerClose").on("click", function() {
        $(".flightFinder__container").removeClass("showFlightFinderRow");
    }), stickyHeaderNavigation(), numberSelector(), fixPositions();
}), $(window).resize(function() {});
});