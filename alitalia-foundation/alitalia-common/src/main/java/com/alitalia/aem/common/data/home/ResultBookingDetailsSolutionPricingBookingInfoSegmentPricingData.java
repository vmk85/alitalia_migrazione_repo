package com.alitalia.aem.common.data.home;

import java.util.List;

public class ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData {
	
	private List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatchData> extField;
	private List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData> fareField;
	
	public List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatchData> getExtField() {
		return extField;
	}
	
	public void setExtField(
			List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatchData> extField) {
		this.extField = extField;
	}
	
	public List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData> getFareField() {
		return fareField;
	}
	
	public void setFareField(
			List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData> fareField) {
		this.fareField = fareField;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((extField == null) ? 0 : extField.hashCode());
		result = prime * result
				+ ((fareField == null) ? 0 : fareField.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData other = (ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData) obj;
		if (extField == null) {
			if (other.extField != null)
				return false;
		} else if (!extField.equals(other.extField))
			return false;
		if (fareField == null) {
			if (other.fareField != null)
				return false;
		} else if (!fareField.equals(other.fareField))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData [extField="
				+ extField + ", fareField=" + fareField + "]";
	}
	
}
