package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.boardingpass.BoardingPassInfo;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinGetBoardingPassInfoByNameResponse extends BaseResponse{

	private BoardingPassInfo boardingPassInfo;

	public CheckinGetBoardingPassInfoByNameResponse() {}

	public CheckinGetBoardingPassInfoByNameResponse(String tid, String sid) {
		super(tid, sid);
	}

	public BoardingPassInfo getBoardingPassInfo() {
		return boardingPassInfo;
	}

	public void setBoardingPassInfo(BoardingPassInfo boardingPassInfo) {
		this.boardingPassInfo = boardingPassInfo;
	}
	
}
