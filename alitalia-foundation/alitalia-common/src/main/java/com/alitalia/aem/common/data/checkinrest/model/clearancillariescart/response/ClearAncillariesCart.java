package com.alitalia.aem.common.data.checkinrest.model.clearancillariescart.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClearAncillariesCart {

    @SerializedName("outcome")
    @Expose
    private String[] outcome;
    @SerializedName("conversationID")
    @Expose
    private String conversationID;

    public String[] getOutcome() {
        return outcome;
    }

    public void setOutcome(String[] outcome) {
        this.outcome = outcome;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

}
