package com.alitalia.aem.common.data.home;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.enumerations.CreditCardTypeEnum;

public class MACreditCardData {
	private Logger logger = LoggerFactory.getLogger("MACreditCardData");
	
	private final String ONECLICK = "ONECLICK";
	private final char HOLDERNAME_SPLIT_PATTERN = ' ';

	public MACreditCardData(){}

	public MACreditCardData(String circuitCode) {
		this.circuitCode = circuitCode;
		decodeCircuitCode();
	}
	
	public MACreditCardData(String circuitCode, String number, String cvv, String holderFirstName, String holderLastName, String expireMonth, String expireYear, String cardBin) {
		super();
		logger.debug("MAPayment - Building credit card: circuitCode ["+circuitCode+"], number ["+number+"], cvv ["+cvv+"], holderFirstName ["+holderFirstName+"], holderLastName ["+holderLastName+"], expireMonth ["+
		expireMonth+"], expireYear ["+expireYear+"]");
		this.circuitCode = circuitCode;
		this.number = number;
		this.cvv = cvv;
		this.holderFirstName = holderFirstName;
		this.holderLastName = holderLastName;
		this.expireMonth = expireMonth;
		this.expireYear = expireYear;
		
		this.expirationDate = this.expireMonth + "/" + this.expireYear;

		this.cardBin = cardBin;
		
		obfuscateNumber();
		decodeCircuitCode();
		buildHolderName();

		if (circuitCode!= null && circuitCode.equalsIgnoreCase(CreditCardTypeEnum.AMERICAN_EXPRESS.value())) {
			this.obfuscatedCvv = "****";
		}

		logger.debug("MAPayment - Built credit card: "+toString());
	}
	

	
	public MACreditCardData(String type, String paymentMethodVariant, String number, String cvv, String holderName, String expireMonth, String expireYear, String alias, String firstPspReference, String recurringDetailReference, String cardBin) {
		super();
		logger.debug("MAPayment - Building credit card: type ["+type+"], paymentMethodVariant ["+paymentMethodVariant+"], number ["+number+"], cvv ["+cvv+"], holderName ["+holderName+"], expireMonth ["+
		expireMonth+"], expireYear ["+expireYear+"], alias ["+alias+"], firstPspReference ["+firstPspReference+"], recurringDetailReference ["+recurringDetailReference+"]");
		this.type = type;
		this.paymentMethodVariant = paymentMethodVariant;
		this.number = number;
		this.cvv = cvv;
		this.holderName = holderName;
		this.expireMonth = expireMonth;
		this.expireYear = expireYear;
		this.alias = alias;
		this.firstPspReference = firstPspReference;
		this.recurringDetailReference = recurringDetailReference;
		
		this.expirationDate = this.expireMonth + "/" + this.expireYear;

		this.cardBin = cardBin;
		
		obfuscateNumber();
		decodeType();
		splitHolderName();

		if (circuitCode!= null && circuitCode.equalsIgnoreCase(CreditCardTypeEnum.AMERICAN_EXPRESS.value())) {
			this.obfuscatedCvv = "****";
		}

		logger.debug("MAPayment - Built credit card: "+toString());
	}
	
	public void fillAddressData(String zip, String city, String country, String address) {
		logger.debug("MAPayment - Filling address data: zip ["+zip+"], city ["+city+"], country ["+country+"], address ["+address+"]");
		this.zip = zip;
		this.city = city;
		this.country = country;
		this.address = address;

		logger.debug("MAPayment - Built credit card: "+toString());
	}
	
	public void fillAddressData(String zip, String city, String country, String address, String state) {
		logger.debug("MAPayment - Filling address data: zip ["+zip+"], city ["+city+"], country ["+country+"], address ["+address+"], state ["+state+"]");
		this.zip = zip;
		this.city = city;
		this.country = country;
		this.address = address;
		this.state = state;
		
		//Recupera la provincia solo se il paese è USA
		if (country!=null && country.equalsIgnoreCase("us")==false) {
			state = null;
		}

		logger.debug("MAPayment - Built credit card: "+toString());
	}
	
	private String circuitCode;
	private String number;
	private String cvv;
	private String holderName;
	private String holderFirstName;
	private String holderLastName;
	private String expireMonth;
	private String expireYear;
	private String type;
	private String zip;
	private String city;
	private String country;
	private String address;
	private String state;
	
	private String alias;
	private String firstPspReference;
	private String recurringDetailReference;
	private String paymentMethodVariant;

	private List<String> recurring = Collections.singletonList(ONECLICK);;
	
	private String obfuscatedNumber;
	private String obfuscatedCvv = "***";
	private String expirationDate;

	private String cardBin;

	public String getCardBin() {
		return cardBin;
	}

	public void setCardBin(String cardBin) {
		this.cardBin = cardBin;
	}

	public int getId() {
		return this.hashCode();
	}

	public String getCircuitCode() {
		return circuitCode;
	}

	public void setCircuitCode(String circuitCode) {
		this.circuitCode = circuitCode;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
		obfuscateNumber();
	}

	public String getObfuscatedNumber() {
		return obfuscatedNumber;
	}

	public void setObfuscatedNumber(String obfuscatedNumber) {
		this.obfuscatedNumber = obfuscatedNumber;
	}
	
	public String getObfuscatedCvv() {
		return obfuscatedCvv;
	}

	public void setObfuscatedCvv(String obfuscatedCvv) {
		this.obfuscatedCvv = obfuscatedCvv;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	public String getHolderName() {
		return holderName;
	}

	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	public String getHolderFirstName() {
		return holderFirstName;
	}

	public void setHolderFirstName(String holderFirstName) {
		this.holderFirstName = holderFirstName;
	}

	public String getHolderLastName() {
		return holderLastName;
	}

	public void setHolderLastName(String holderLastName) {
		this.holderLastName = holderLastName;
	}

	public String getExpireMonth() {
		return expireMonth;
	}

	public void setExpireMonth(String expireMonth) {
		this.expireMonth = expireMonth;
	}

	public String getExpireYear() {
		return expireYear;
	}

	public void setExpireYear(String expireYear) {
		this.expireYear = expireYear;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getFirstPspReference() {
		return firstPspReference;
	}

	public void setFirstPspReference(String firstPspReference) {
		this.firstPspReference = firstPspReference;
	}

	public String getRecurringDetailReference() {
		return recurringDetailReference;
	}

	public void setRecurringDetailReference(String recurringDetailReference) {
		this.recurringDetailReference = recurringDetailReference;
	}
	
	public String getPaymentMethodVariant() {
		return paymentMethodVariant;
	}

	public void setPaymentMethodVariant(String paymentMethodVariant) {
		this.paymentMethodVariant = paymentMethodVariant;
	}

	public List<String> getRecurring() {
		return recurring;
	}

	public void setRecurring(List<String> recurring) {
		this.recurring = recurring;
	}

	private void obfuscateNumber() {
		this.obfuscatedNumber = "";
		
		int defLength = 16;
		int digLength = 4;
		if (this.number!=null && this.number.isEmpty()==false) {
			if (this.number.length()<4) {
				digLength = this.number.length();
			} else if (this.number.length()>defLength) {
				defLength = this.number.length();
			}
			int i;
			for (i=0; i<defLength-digLength; i++){
				this.obfuscatedNumber += "*";
			}
			this.obfuscatedNumber+=this.number.substring(this.number.length()-digLength);
		}
	}

	private void decodeType() {
		if (this.type!=null) {
			switch (this.type) {
			case "visa":
//Fix Tolentino 06/11/2018: invertito test su paymentMethdVariant, se vale "electron" la carta è una Visa Electron altrimenti è una Visa
				if (this.paymentMethodVariant.equalsIgnoreCase("electron")) {
					//this.circuitCode = CreditCardTypeEnum.VISA_ELECTRON.value();  //Booking
					this.circuitCode = "visaelectron";  //Checkin
				} else {
					//this.circuitCode = CreditCardTypeEnum.VISA.value();  //Booking
					this.circuitCode = "visa";  //Checkin
				}
//				if (this.paymentMethodVariant.equalsIgnoreCase("visacorporatecredit")) {
//					//this.circuitCode = CreditCardTypeEnum.VISA.value();  //Booking
//					this.circuitCode = "visa";  //Checkin
//				} else if (this.paymentMethodVariant.equalsIgnoreCase("electron")) {
//					//this.circuitCode = CreditCardTypeEnum.VISA_ELECTRON.value();  //Booking
//					this.circuitCode = "visaelectron";  //Checkin
//				}
				break;

			case "mc":
				//this.circuitCode = CreditCardTypeEnum.MASTER_CARD.value();  //Booking
				this.circuitCode = "mastercard";  //Checkin
				break;

			case "amex":
				//this.circuitCode = CreditCardTypeEnum.AMERICAN_EXPRESS.value();  //Booking
				this.circuitCode = "americanexpress";  //Checkin
				break;

			case "diners":
				//this.circuitCode = CreditCardTypeEnum.DINERS.value();  //Booking
				this.circuitCode = "diners_club_international";  //Checkin
				break;

			case "uatp":
				this.circuitCode = CreditCardTypeEnum.UATP.value();  //Booking
				break;
				
			case "maestro":
				this.circuitCode = CreditCardTypeEnum.MAESTRO.value();  //Booking
				break;
				
			default:
				logger.error("MAPayment - Unable to decode Type ["+type+"]");
				break;
			}
		}
	}
	
	private void decodeCircuitCode() {
		if (this.circuitCode!=null) {
			switch (this.circuitCode) {
			//from Checkin 
			case "visa":
				this.type = "Visa";
				break;

			case "visaelectron":
				this.type = "Visa Electron";
				break;

			case "mastercard":
				this.type = "Mastercard";
				break;

			case "americanexpress":
				this.type = "American Express";
				break;

			case "diners_club_international":
				this.type = "Diners";
				break;

			//from Booking 
			case "Visa":
				this.type = "Visa";
				break;

			case "VisaElectron":
				this.type = "Visa Electron";
				break;

			case "MasterCard":
				this.type = "Mastercard";
				break;

			case "AmericanExpress":
				this.type = "American Express";
				break;

			case "Diners":
				this.type = "Diners";
				break;

			case "Uatp":
				this.type = "UATP";
				break;
				
			case "Maestro":
				this.type = "Maestro";
				break;
				
			default:
				logger.error("MAPayment - Unable to decode CircuitCode ["+circuitCode+"]");
				break;
			}
		}
	}
	
	private void splitHolderName() {
		if (holderName==null || holderName.trim().isEmpty()) {
			logger.error("MAPayment - Unable to split HolderName, is empty or null");
			return;
		}

		if (holderName.contains(String.valueOf(HOLDERNAME_SPLIT_PATTERN))) {
			holderFirstName = holderName.substring(0, holderName.indexOf(HOLDERNAME_SPLIT_PATTERN));
			holderLastName = holderName.substring(holderName.indexOf(HOLDERNAME_SPLIT_PATTERN)+1);
		} else {
			logger.warn("MAPayment - Unable to split HolderName because split pattern ["+HOLDERNAME_SPLIT_PATTERN+"] is missing");
			holderFirstName = holderName;
		}
	}

	private void buildHolderName() {
		if (holderFirstName!=null && !holderFirstName.trim().isEmpty() &&
			holderLastName!=null && !holderLastName.trim().isEmpty()) {
			holderName = holderFirstName + HOLDERNAME_SPLIT_PATTERN + holderLastName;
		} else if (holderFirstName!=null && !holderFirstName.trim().isEmpty()) {
			logger.warn("MAPayment - HolderLastName is empty or null");
			holderName = holderFirstName;
		} else if (holderLastName!=null && !holderLastName.trim().isEmpty()) {
			logger.warn("MAPayment - HolderFirstName is empty or null");
			holderName = holderLastName;
		} else {
			logger.error("MAPayment - Unable to build HolderName, HolderFirstName and HolderLastName are empty or null");
		}
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((circuitCode == null) ? 0 : circuitCode.hashCode());
		result = prime * result + ((expirationDate == null) ? 0 : expirationDate.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((obfuscatedNumber == null) ? 0 : obfuscatedNumber.hashCode());
		result = prime * result	+ ((cvv == null) ? 0 : cvv.hashCode());
		result = prime * result + ((expireMonth == null) ? 0 : expireMonth.hashCode());
		result = prime * result + ((expireYear == null) ? 0 : expireYear.hashCode());
		result = prime * result + ((zip == null) ? 0 : zip.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((holderFirstName == null) ? 0 : holderFirstName.hashCode());
		result = prime * result + ((holderLastName == null) ? 0 : holderLastName.hashCode());
		result = prime * result + ((holderName == null) ? 0 : holderName.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((alias == null) ? 0 : alias.hashCode());
		result = prime * result + ((firstPspReference == null) ? 0 : firstPspReference.hashCode());
		result = prime * result + ((recurringDetailReference == null) ? 0 : recurringDetailReference.hashCode());
		result = prime * result + ((paymentMethodVariant == null) ? 0 : paymentMethodVariant.hashCode());
		result = prime * result + ((recurring == null) ? 0 : recurring.hashCode());

		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MACreditCardData other = (MACreditCardData) obj;
		if (circuitCode == null) {
			if (other.circuitCode != null)
				return false;
		} else if (!circuitCode.equals(other.circuitCode))
			return false;
		if (expirationDate == null) {
			if (other.expirationDate != null)
				return false;
		} else if (!expirationDate.equals(other.expirationDate))
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (obfuscatedNumber == null) {
			if (other.obfuscatedNumber != null)
				return false;
		} else if (!obfuscatedNumber.equals(other.obfuscatedNumber))
			return false;
		if (cvv == null) {
			if (other.cvv != null)
				return false;
		} else if (!cvv.equals(other.cvv))
			return false;
	
		if (expireMonth == null) {
			if (other.expireMonth != null)
				return false;
		} else if (!expireMonth.equals(other.expireMonth))
			return false;
		if (expireYear == null) {
			if (other.expireYear != null)
				return false;
		} else if (!expireYear.equals(other.expireYear))
			return false;
		if (zip == null) {
			if (other.zip != null)
				return false;
		} else if (!zip.equals(other.zip))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (holderName == null) {
			if (other.holderName != null)
				return false;
		} else if (!holderName.equals(other.holderName))
			return false;
		if (holderFirstName == null) {
			if (other.holderFirstName != null)
				return false;
		} else if (!holderFirstName.equals(other.holderFirstName))
			return false;
		if (holderLastName == null) {
			if (other.holderLastName != null)
				return false;
		} else if (!holderLastName.equals(other.holderLastName))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (alias == null) {
			if (other.alias != null)
				return false;
		} else if (!alias.equals(other.alias))
			return false;
		if (firstPspReference == null) {
			if (other.firstPspReference != null)
				return false;
		} else if (!firstPspReference.equals(other.firstPspReference))
			return false;
		if (recurringDetailReference == null) {
			if (other.recurringDetailReference != null)
				return false;
		} else if (!recurringDetailReference.equals(other.recurringDetailReference))
			return false;
		if (paymentMethodVariant == null) {
			if (other.paymentMethodVariant != null)
				return false;
		} else if (!paymentMethodVariant.equals(other.paymentMethodVariant))
			return false;
		if (recurring == null) {
			if (other.recurring != null)
				return false;
		} else if (!recurring.equals(other.recurring))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "circuitCode=[" + circuitCode + "], number=[" + number + "], obfuscatedNumber=[" + obfuscatedNumber+ "], cvv=[" + cvv   
				+ "], expirationDate=[" + expirationDate + "], expireMonth=[" + expireMonth + "], expireYear=[" + expireYear 
				+ "], holderName=[" + holderName + "], holderFirstName=[" + holderFirstName + "], holderLastName=[" + holderLastName + "], type=[" + type + "], alias=[" + alias
				+ "], firstPspReference=[" + firstPspReference + "], recurringDetailReference=[" + recurringDetailReference + "], paymentMethodVariant=[" + paymentMethodVariant
				+ "], recurring=[" + recurring
				+ "], zip=[" + zip + "], city=[" + city + "], country=[" + country + "], address=[" + address + "], state=[" + state + "]";
	}

	public String print() {
		return circuitCode + " " + obfuscatedNumber + " " + expirationDate + " " + holderName;
	}
}
