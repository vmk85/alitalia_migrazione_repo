
package com.alitalia.aem.common.data.checkinrest.model.enhancedseatmap.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EnhancedSeatMapResp {

    @SerializedName("seatMAp")
    @Expose
    private SeatMAp seatMAp;
    @SerializedName("conversationID")
    @Expose
    private String conversationID;

    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public SeatMAp getSeatMAp() {
        return seatMAp;
    }

    public void setSeatMAp(SeatMAp seatMAp) {
        this.seatMAp = seatMAp;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

}
