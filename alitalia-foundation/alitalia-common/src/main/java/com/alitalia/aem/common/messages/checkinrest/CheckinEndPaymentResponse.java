package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.endpayment.response.EndPaymentResponse;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinEndPaymentResponse extends BaseResponse{
	
	public EndPaymentResponse endPaymentResponse;

	public EndPaymentResponse getEndPaymentResponse() {
		return endPaymentResponse;
	}

	public void setEndPaymentResponse(EndPaymentResponse endPaymentResponse) {
		this.endPaymentResponse = endPaymentResponse;
	}

	public CheckinEndPaymentResponse(){}
	
	public CheckinEndPaymentResponse(String tid, String sid) {
		super(tid, sid);
	}

}
