package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class RetrieveAirportDetailsRequest extends BaseRequest {

	private String airportCode;

	public String getAirportCode() {
		return airportCode;
	}

	private String market;

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((airportCode == null) ? 0 : airportCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveAirportDetailsRequest other = (RetrieveAirportDetailsRequest) obj;
		if (airportCode == null) {
			if (other.airportCode != null)
				return false;
		} else if (!airportCode.equals(other.airportCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveAirportDetailsRequest{" +
				"airportCode='" + airportCode + '\'' +
				", market='" + market + '\'' +
				'}';
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}
}