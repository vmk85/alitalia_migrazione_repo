
package com.alitalia.aem.common.data.checkinrest.model.passenger;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TotalCarryOnWeight {

    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("unitSpecified")
    @Expose
    private Boolean unitSpecified;
    @SerializedName("value")
    @Expose
    private String value;

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Boolean getUnitSpecified() {
        return unitSpecified;
    }

    public void setUnitSpecified(Boolean unitSpecified) {
        this.unitSpecified = unitSpecified;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
