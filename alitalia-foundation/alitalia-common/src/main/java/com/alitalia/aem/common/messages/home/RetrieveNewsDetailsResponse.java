package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.DetailedNewsData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetrieveNewsDetailsResponse extends BaseResponse{

	private DetailedNewsData detailedNews;

	public DetailedNewsData getDetailedNews() {
		return detailedNews;
	}

	public void setDetailedNews(DetailedNewsData detailedNews) {
		this.detailedNews = detailedNews;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((detailedNews == null) ? 0 : detailedNews.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveNewsDetailsResponse other = (RetrieveNewsDetailsResponse) obj;
		if (detailedNews == null) {
			if (other.detailedNews != null)
				return false;
		} else if (!detailedNews.equals(other.detailedNews))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveNewsDetailsResponse [detailedNews=" + detailedNews
				+ "]";
	}
}