
package com.alitalia.aem.common.data.checkinrest.model.endpayment.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InsurancePayment {

    @SerializedName("buyInsurance")
    @Expose
    private Boolean buyInsurance;
    @SerializedName("insuranceAmount")
    @Expose
    private Integer insuranceAmount;
    @SerializedName("policyNumber")
    @Expose
    private String policyNumber;

    public Boolean getBuyInsurance() {
        return buyInsurance;
    }

    public void setBuyInsurance(Boolean buyInsurance) {
        this.buyInsurance = buyInsurance;
    }

    public Integer getInsuranceAmount() {
        return insuranceAmount;
    }

    public void setInsuranceAmount(Integer insuranceAmount) {
        this.insuranceAmount = insuranceAmount;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

}
