package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class SetDatiSicurezzaProfiloRequest extends BaseRequest {


	protected String accessFailedCount;
	private String prefissoNazioneCellulare;
	protected String cellulare;
	protected String emailAccount;
	protected Boolean flagRememberPassword;
	protected Boolean flagVerificaCellulareOK;
	protected Boolean flagVerificaEmailAccountOK;
	protected Boolean flagVerificaPasswordOK;
	protected Boolean flagVerificaRispostaOK;
	protected Boolean flagVerificaUserNameOK;
	protected String idProfilo;
	protected Boolean lockedout;
	protected String password;
	protected String secureAnswer;
	protected String secureQuestionCode;
	protected String userName;
    protected int secureQuestionID;


	public String getAccessFailedCount() {
		return accessFailedCount;
	}

	public void setAccessFailedCount(String accessFailedCount) {
		this.accessFailedCount = accessFailedCount;
	}

	public String getCellulare() {
		return cellulare;
	}

	public void setCellulare(String cellulare) {
		this.cellulare = cellulare;
	}

	public String getPrefissoNazioneCellulare() {
		return prefissoNazioneCellulare;
	}

	public void setPrefissoNazioneCellulare(String prefissoNazioneCellulare) {
		this.prefissoNazioneCellulare = prefissoNazioneCellulare;
	}

	public String getEmailAccount() {
		return emailAccount;
	}

	public void setEmailAccount(String emailAccount) {
		this.emailAccount = emailAccount;
	}

	public Boolean getFlagRememberPassword() {
		return flagRememberPassword;
	}

	public void setFlagRememberPassword(Boolean flagRememberPassword) {
		this.flagRememberPassword = flagRememberPassword;
	}

	public Boolean getFlagVerificaCellulareOK() {
		return flagVerificaCellulareOK;
	}

	public void setFlagVerificaCellulareOK(Boolean flagVerificaCellulareOK) {
		this.flagVerificaCellulareOK = flagVerificaCellulareOK;
	}

	public Boolean getFlagVerificaEmailAccountOK() {
		return flagVerificaEmailAccountOK;
	}

	public void setFlagVerificaEmailAccountOK(Boolean flagVerificaEmailAccountOK) {
		this.flagVerificaEmailAccountOK = flagVerificaEmailAccountOK;
	}

	public Boolean getFlagVerificaPasswordOK() {
		return flagVerificaPasswordOK;
	}

	public void setFlagVerificaPasswordOK(Boolean flagVerificaPasswordOK) {
		this.flagVerificaPasswordOK = flagVerificaPasswordOK;
	}

	public Boolean getFlagVerificaRispostaOK() {
		return flagVerificaRispostaOK;
	}

	public void setFlagVerificaRispostaOK(Boolean flagVerificaRispostaOK) {
		this.flagVerificaRispostaOK = flagVerificaRispostaOK;
	}

	public Boolean getFlagVerificaUserNameOK() {
		return flagVerificaUserNameOK;
	}

	public void setFlagVerificaUserNameOK(Boolean flagVerificaUserNameOK) {
		this.flagVerificaUserNameOK = flagVerificaUserNameOK;
	}

	public String getIdProfilo() {
		return idProfilo;
	}

	public void setIdProfilo(String idProfilo) {
		this.idProfilo = idProfilo;
	}

	public Boolean getLockedout() {
		return lockedout;
	}

	public void setLockedout(Boolean lockedout) {
		this.lockedout = lockedout;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSecureAnswer() {
		return secureAnswer;
	}

	public void setSecureAnswer(String secureAnswer) {
		this.secureAnswer = secureAnswer;
	}

	public int getSecureQuestionID() {
		return secureQuestionID;
	}

	public void setSecureQuestionID(int secureQuestionID) {
		this.secureQuestionID = secureQuestionID;
	}

    public String getSecureQuestionCode() {
        return secureQuestionCode;
    }

    public void setSecureQuestionCode(String secureQuestionCode) {
        this.secureQuestionCode = secureQuestionCode;
    }



    public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String toString() {
		return "SetDatiSicurezzaProfiloRequest{" +
				"accessFailedCount='" + accessFailedCount + '\'' +
				", cellulare='" + cellulare + '\'' +
				", emailAccount='" + emailAccount + '\'' +
				", flagRememberPassword=" + flagRememberPassword +
				", flagVerificaCellulareOK=" + flagVerificaCellulareOK +
				", flagVerificaEmailAccountOK=" + flagVerificaEmailAccountOK +
				", flagVerificaPasswordOK=" + flagVerificaPasswordOK +
				", flagVerificaRispostaOK=" + flagVerificaRispostaOK +
				", flagVerificaUserNameOK=" + flagVerificaUserNameOK +
				", idProfilo='" + idProfilo + '\'' +
				", lockedout=" + lockedout +
				", password='" + password + '\'' +
				", secureAnswer='" + secureAnswer + '\'' +
				", secureQuestionCode='" + secureQuestionCode + '\'' +
				", userName='" + userName + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;

		SetDatiSicurezzaProfiloRequest that = (SetDatiSicurezzaProfiloRequest) o;

		if (accessFailedCount != null ? !accessFailedCount.equals(that.accessFailedCount) : that.accessFailedCount != null)
			return false;
		if (cellulare != null ? !cellulare.equals(that.cellulare) : that.cellulare != null) return false;
		if (emailAccount != null ? !emailAccount.equals(that.emailAccount) : that.emailAccount != null) return false;
		if (flagRememberPassword != null ? !flagRememberPassword.equals(that.flagRememberPassword) : that.flagRememberPassword != null)
			return false;
		if (flagVerificaCellulareOK != null ? !flagVerificaCellulareOK.equals(that.flagVerificaCellulareOK) : that.flagVerificaCellulareOK != null)
			return false;
		if (flagVerificaEmailAccountOK != null ? !flagVerificaEmailAccountOK.equals(that.flagVerificaEmailAccountOK) : that.flagVerificaEmailAccountOK != null)
			return false;
		if (flagVerificaPasswordOK != null ? !flagVerificaPasswordOK.equals(that.flagVerificaPasswordOK) : that.flagVerificaPasswordOK != null)
			return false;
		if (flagVerificaRispostaOK != null ? !flagVerificaRispostaOK.equals(that.flagVerificaRispostaOK) : that.flagVerificaRispostaOK != null)
			return false;
		if (flagVerificaUserNameOK != null ? !flagVerificaUserNameOK.equals(that.flagVerificaUserNameOK) : that.flagVerificaUserNameOK != null)
			return false;
		if (idProfilo != null ? !idProfilo.equals(that.idProfilo) : that.idProfilo != null) return false;
		if (lockedout != null ? !lockedout.equals(that.lockedout) : that.lockedout != null) return false;
		if (password != null ? !password.equals(that.password) : that.password != null) return false;
		if (secureAnswer != null ? !secureAnswer.equals(that.secureAnswer) : that.secureAnswer != null) return false;
		if (secureQuestionCode != null ? !secureQuestionCode.equals(that.secureQuestionCode) : that.secureQuestionCode != null)
			return false;
		return userName != null ? userName.equals(that.userName) : that.userName == null;
	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (accessFailedCount != null ? accessFailedCount.hashCode() : 0);
		result = 31 * result + (cellulare != null ? cellulare.hashCode() : 0);
		result = 31 * result + (emailAccount != null ? emailAccount.hashCode() : 0);
		result = 31 * result + (flagRememberPassword != null ? flagRememberPassword.hashCode() : 0);
		result = 31 * result + (flagVerificaCellulareOK != null ? flagVerificaCellulareOK.hashCode() : 0);
		result = 31 * result + (flagVerificaEmailAccountOK != null ? flagVerificaEmailAccountOK.hashCode() : 0);
		result = 31 * result + (flagVerificaPasswordOK != null ? flagVerificaPasswordOK.hashCode() : 0);
		result = 31 * result + (flagVerificaRispostaOK != null ? flagVerificaRispostaOK.hashCode() : 0);
		result = 31 * result + (idProfilo != null ? idProfilo.hashCode() : 0);
		result = 31 * result + (lockedout != null ? lockedout.hashCode() : 0);
		result = 31 * result + (password != null ? password.hashCode() : 0);
		result = 31 * result + (secureAnswer != null ? secureAnswer.hashCode() : 0);
		result = 31 * result + (secureQuestionCode != null ? secureQuestionCode.hashCode() : 0);
		result = 31 * result + (userName != null ? userName.hashCode() : 0);
		return result;
	}
}