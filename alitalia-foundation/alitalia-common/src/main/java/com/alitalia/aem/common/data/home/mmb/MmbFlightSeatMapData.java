package com.alitalia.aem.common.data.home.mmb;

import java.util.List;

import com.alitalia.aem.common.data.home.SeatMapData;

public class MmbFlightSeatMapData {

	private MmbFlightData flight;
    private List<SeatMapData> seatMaps;
	
    public MmbFlightData getFlight() {
		return flight;
	}
	
    public void setFlight(MmbFlightData directFlight) {
		this.flight = directFlight;
	}
	
    public List<SeatMapData> getSeatMaps() {
		return seatMaps;
	}
	
    public void setSeatMaps(List<SeatMapData> seatMaps) {
		this.seatMaps = seatMaps;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((flight == null) ? 0 : flight.hashCode());
		result = prime * result
				+ ((seatMaps == null) ? 0 : seatMaps.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbFlightSeatMapData other = (MmbFlightSeatMapData) obj;
		if (flight == null) {
			if (other.flight != null)
				return false;
		} else if (!flight.equals(other.flight))
			return false;
		if (seatMaps == null) {
			if (other.seatMaps != null)
				return false;
		} else if (!seatMaps.equals(other.seatMaps))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbFlightSeatMapData [flight=" + flight + ", seatMaps="
				+ seatMaps + "]";
	}
}