package com.alitalia.aem.common.messages.home;

import java.util.Arrays;
import java.util.List;

import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.messages.MmbBaseRequest;

public class SendBoardingPassCheckinRequest extends MmbBaseRequest {

    private byte[] attachment;
    private String attachmentName;
    private String emailBody;
    private String emailSubject;
    private List<CheckinPassengerData> passengers;
    private String smsBodyText;
    
	public byte[] getAttachment() {
		return attachment;
	}
	public void setAttachment(byte[] attachment) {
		this.attachment = attachment;
	}
	public String getAttachmentName() {
		return attachmentName;
	}
	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}
	public String getEmailBody() {
		return emailBody;
	}
	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}
	public String getEmailSubject() {
		return emailSubject;
	}
	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}
	public List<CheckinPassengerData> getPassengers() {
		return passengers;
	}
	public void setPassengers(List<CheckinPassengerData> passengers) {
		this.passengers = passengers;
	}
	public String getSmsBodyText() {
		return smsBodyText;
	}
	public void setSmsBodyText(String smsBodyText) {
		this.smsBodyText = smsBodyText;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Arrays.hashCode(attachment);
		result = prime * result
				+ ((attachmentName == null) ? 0 : attachmentName.hashCode());
		result = prime * result
				+ ((emailBody == null) ? 0 : emailBody.hashCode());
		result = prime * result
				+ ((emailSubject == null) ? 0 : emailSubject.hashCode());
		result = prime * result
				+ ((passengers == null) ? 0 : passengers.hashCode());
		result = prime * result
				+ ((smsBodyText == null) ? 0 : smsBodyText.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SendBoardingPassCheckinRequest other = (SendBoardingPassCheckinRequest) obj;
		if (!Arrays.equals(attachment, other.attachment))
			return false;
		if (attachmentName == null) {
			if (other.attachmentName != null)
				return false;
		} else if (!attachmentName.equals(other.attachmentName))
			return false;
		if (emailBody == null) {
			if (other.emailBody != null)
				return false;
		} else if (!emailBody.equals(other.emailBody))
			return false;
		if (emailSubject == null) {
			if (other.emailSubject != null)
				return false;
		} else if (!emailSubject.equals(other.emailSubject))
			return false;
		if (passengers == null) {
			if (other.passengers != null)
				return false;
		} else if (!passengers.equals(other.passengers))
			return false;
		if (smsBodyText == null) {
			if (other.smsBodyText != null)
				return false;
		} else if (!smsBodyText.equals(other.smsBodyText))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "SendBoardingPassCheckinRequest [attachment="
				+ Arrays.toString(attachment) + ", attachmentName="
				+ attachmentName + ", emailBody=" + emailBody
				+ ", emailSubject=" + emailSubject + ", passengers="
				+ passengers + ", smsBodyText=" + smsBodyText + ", toString()="
				+ super.toString() + "]";
	}
	
}
