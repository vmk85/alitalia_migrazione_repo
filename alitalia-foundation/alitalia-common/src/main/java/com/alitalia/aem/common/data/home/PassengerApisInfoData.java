package com.alitalia.aem.common.data.home;

public class PassengerApisInfoData extends PassengerBaseInfoData {

    private String nationality;
    private String passportNumber;
	
    public String getNationality() {
		return nationality;
	}
	
    public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	
    public String getPassportNumber() {
		return passportNumber;
	}
	
    public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((nationality == null) ? 0 : nationality.hashCode());
		result = prime * result
				+ ((passportNumber == null) ? 0 : passportNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PassengerApisInfoData other = (PassengerApisInfoData) obj;
		if (nationality == null) {
			if (other.nationality != null)
				return false;
		} else if (!nationality.equals(other.nationality))
			return false;
		if (passportNumber == null) {
			if (other.passportNumber != null)
				return false;
		} else if (!passportNumber.equals(other.passportNumber))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ApisInfoData [nationality=" + nationality + ", passportNumber="
				+ passportNumber + "]";
	}

}
