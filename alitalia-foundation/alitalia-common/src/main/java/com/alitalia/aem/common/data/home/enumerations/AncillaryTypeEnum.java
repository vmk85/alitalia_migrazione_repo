package com.alitalia.aem.common.data.home.enumerations;


public enum AncillaryTypeEnum {

    SEAT("Seat"),
    EXTRA_BAGGAGE("ExtraBaggage"),
    MEAL("Meal"),
    VIP_LOUNGE("VipLounge"),
    FAST_TRACK("FastTrack"),
    INSURANCE("Insurance"),
    UPGRADE("Upgrade");
    
    private final String value;

    AncillaryTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AncillaryTypeEnum fromValue(String v) {
        for (AncillaryTypeEnum c: AncillaryTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}