package com.alitalia.aem.common.data.home.mmb.ancillary;

import com.alitalia.aem.common.data.MmbAncillaryErrorData;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryShoppingCartServiceErrorEnum;

public class MmbAncillaryServiceErrorData extends MmbAncillaryErrorData {

	public MmbAncillaryServiceErrorData() {
		super();
		this.type = MmbAncillaryShoppingCartServiceErrorEnum.SERVICE_ERROR;
	}

}
