package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class WebCheckinBPPermissionResponse extends BaseResponse {

    private Boolean isBPPdfEnabled;
    private Boolean isBPViaEmailEnabled;
    private Boolean isBPViaSmsEnabled;
    private Boolean isDelayedDeliveryTypeEnabled;
    
	public Boolean getIsBPPdfEnabled() {
		return isBPPdfEnabled;
	}
	public void setIsBPPdfEnabled(Boolean isBPPdfEnabled) {
		this.isBPPdfEnabled = isBPPdfEnabled;
	}
	public Boolean getIsBPViaEmailEnabled() {
		return isBPViaEmailEnabled;
	}
	public void setIsBPViaEmailEnabled(Boolean isBPViaEmailEnabled) {
		this.isBPViaEmailEnabled = isBPViaEmailEnabled;
	}
	public Boolean getIsBPViaSmsEnabled() {
		return isBPViaSmsEnabled;
	}
	public void setIsBPViaSmsEnabled(Boolean isBPViaSmsEnabled) {
		this.isBPViaSmsEnabled = isBPViaSmsEnabled;
	}
	public Boolean getIsDelayedDeliveryTypeEnabled() {
		return isDelayedDeliveryTypeEnabled;
	}
	public void setIsDelayedDeliveryTypeEnabled(Boolean isDelayedDeliveryTypeEnabled) {
		this.isDelayedDeliveryTypeEnabled = isDelayedDeliveryTypeEnabled;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((isBPPdfEnabled == null) ? 0 : isBPPdfEnabled.hashCode());
		result = prime
				* result
				+ ((isBPViaEmailEnabled == null) ? 0 : isBPViaEmailEnabled
						.hashCode());
		result = prime
				* result
				+ ((isBPViaSmsEnabled == null) ? 0 : isBPViaSmsEnabled
						.hashCode());
		result = prime
				* result
				+ ((isDelayedDeliveryTypeEnabled == null) ? 0
						: isDelayedDeliveryTypeEnabled.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebCheckinBPPermissionResponse other = (WebCheckinBPPermissionResponse) obj;
		if (isBPPdfEnabled == null) {
			if (other.isBPPdfEnabled != null)
				return false;
		} else if (!isBPPdfEnabled.equals(other.isBPPdfEnabled))
			return false;
		if (isBPViaEmailEnabled == null) {
			if (other.isBPViaEmailEnabled != null)
				return false;
		} else if (!isBPViaEmailEnabled.equals(other.isBPViaEmailEnabled))
			return false;
		if (isBPViaSmsEnabled == null) {
			if (other.isBPViaSmsEnabled != null)
				return false;
		} else if (!isBPViaSmsEnabled.equals(other.isBPViaSmsEnabled))
			return false;
		if (isDelayedDeliveryTypeEnabled == null) {
			if (other.isDelayedDeliveryTypeEnabled != null)
				return false;
		} else if (!isDelayedDeliveryTypeEnabled
				.equals(other.isDelayedDeliveryTypeEnabled))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "WebCheckinBPPermissionResponse [isBPPdfEnabled="
				+ isBPPdfEnabled + ", isBPViaEmailEnabled="
				+ isBPViaEmailEnabled + ", isBPViaSmsEnabled="
				+ isBPViaSmsEnabled + ", isDelayedDeliveryTypeEnabled="
				+ isDelayedDeliveryTypeEnabled + ", toString()="
				+ super.toString() + "]";
	}
}
