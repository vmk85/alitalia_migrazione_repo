package com.alitalia.aem.common.data.home;

public class ResultTicketingDetailSolutionSliceSegmentLegConnectionData {

    private Integer durationField;

    public Integer getDurationField() {
        return durationField;
    }

    public void setDurationField(Integer value) {
        this.durationField = value;
    }

}
