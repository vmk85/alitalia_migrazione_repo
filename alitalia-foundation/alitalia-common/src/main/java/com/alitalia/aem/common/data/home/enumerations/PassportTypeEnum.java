package com.alitalia.aem.common.data.home.enumerations;


public enum PassportTypeEnum {
	
	PASSPORT("Passport"),
    ID("ID");
	
    private final String value;

    PassportTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PassportTypeEnum fromValue(String v) {
        for (PassportTypeEnum c: PassportTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
