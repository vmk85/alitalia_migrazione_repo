package com.alitalia.aem.common.messages;

public class AdyenBaseResponse extends BaseResponse {
	
	private String language;
	private String market;
	private String conversationID;
	private String caller;
	
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public String getConversationID() {
		return conversationID;
	}

	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}

	public String getCaller() {
		return caller;
	}

	public void setCaller(String caller) {
		this.caller = caller;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result	+ ((language == null) ? 0 : language.hashCode());
		result = prime * result	+ ((market == null) ? 0 : market.hashCode());
		result = prime * result	+ ((conversationID == null) ? 0 : conversationID.hashCode());
		result = prime * result	+ ((caller == null) ? 0 : caller.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdyenBaseResponse other = (AdyenBaseResponse) obj;
		if (language == null) {
			if (other.language != null)
				return false;
		} else if (!language.equals(other.language))
			return false;
		if (market == null) {
			if (other.market != null)
				return false;
		} else if (!market.equals(other.market))
			return false;
		if (conversationID == null) {
			if (other.conversationID != null)
				return false;
		} else if (!conversationID.equals(other.conversationID))
			return false;
		if (caller == null) {
			if (other.caller != null)
				return false;
		} else if (!caller.equals(other.caller))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "AdyenBaseResponse ["
				+ "language=" + language
				+ "market=" + market
				+ "conversationID=" + conversationID
				+ "caller=" + caller
				+ "]";
	}
	
}