
package com.alitalia.aem.common.data.checkinrest.model.endpayment.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AzAddressPayment {

 
    private String address;

    private String postalCode;

    private String cityName;

    private String countryName;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

}
