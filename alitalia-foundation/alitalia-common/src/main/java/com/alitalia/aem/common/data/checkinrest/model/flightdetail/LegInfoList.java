
package com.alitalia.aem.common.data.checkinrest.model.flightdetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LegInfoList {

    @SerializedName("legNoOperationInd")
    @Expose
    private String legNoOperationInd;
    @SerializedName("legCity")
    @Expose
    private String legCity;
    @SerializedName("controllingCityInd")
    @Expose
    private String controllingCityInd;
    @SerializedName("legStatus")
    @Expose
    private String legStatus;
    @SerializedName("legDate")
    @Expose
    private String legDate;
    @SerializedName("legArrivalTime")
    @Expose
    private String legArrivalTime;
    @SerializedName("legDepartureTime")
    @Expose
    private String legDepartureTime;
    @SerializedName("legApp")
    @Expose
    private String legApp;
    @SerializedName("changeOfGauge")
    @Expose
    private String changeOfGauge;

    public String getLegNoOperationInd() {
        return legNoOperationInd;
    }

    public void setLegNoOperationInd(String legNoOperationInd) {
        this.legNoOperationInd = legNoOperationInd;
    }

    public String getLegCity() {
        return legCity;
    }

    public void setLegCity(String legCity) {
        this.legCity = legCity;
    }

    public String getControllingCityInd() {
        return controllingCityInd;
    }

    public void setControllingCityInd(String controllingCityInd) {
        this.controllingCityInd = controllingCityInd;
    }

    public String getLegStatus() {
        return legStatus;
    }

    public void setLegStatus(String legStatus) {
        this.legStatus = legStatus;
    }

    public String getLegDate() {
        return legDate;
    }

    public void setLegDate(String legDate) {
        this.legDate = legDate;
    }

    public String getLegArrivalTime() {
        return legArrivalTime;
    }

    public void setLegArrivalTime(String legArrivalTime) {
        this.legArrivalTime = legArrivalTime;
    }

    public String getLegDepartureTime() {
        return legDepartureTime;
    }

    public void setLegDepartureTime(String legDepartureTime) {
        this.legDepartureTime = legDepartureTime;
    }

    public String getLegApp() {
        return legApp;
    }

    public void setLegApp(String legApp) {
        this.legApp = legApp;
    }

    public String getChangeOfGauge() {
        return changeOfGauge;
    }

    public void setChangeOfGauge(String changeOfGauge) {
        this.changeOfGauge = changeOfGauge;
    }

}
