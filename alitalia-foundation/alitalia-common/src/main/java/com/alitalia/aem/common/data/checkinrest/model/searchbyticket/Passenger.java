
package com.alitalia.aem.common.data.checkinrest.model.searchbyticket;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Passenger {

    @SerializedName("passengerID")
    @Expose
    private String passengerID;
    @SerializedName("checkInLimited")
    @Expose
    private Boolean checkInLimited;
    @SerializedName("checkInComplete")
    @Expose
    private Boolean checkInComplete;
    @SerializedName("nome")
    @Expose
    private String nome;
    @SerializedName("cognome")
    @Expose
    private String cognome;
    @SerializedName("bookingClass")
    @Expose
    private String bookingClass;
    @SerializedName("postoAssegnato")
    @Expose
    private Boolean postoAssegnato;
    @SerializedName("numeroPostoAssegnato")
    @Expose
    private String numeroPostoAssegnato;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("frequentFlyer")
    @Expose
    private List<FrequentFlyer> frequentFlyer = null;
    @SerializedName("airline")
    @Expose
    private String airline;
    @SerializedName("nationality")
    @Expose
    private String nationality;
    @SerializedName("residence")
    @Expose
    private String residence;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("zipCode")
    @Expose
    private String zipCode;
    @SerializedName("documents")
    @Expose
    private List<Document> documents = null;
    @SerializedName("codeFFchanged")
    @Expose
    private String codeFFchanged;
    @SerializedName("numberFFchanged")
    @Expose
    private String numberFFchanged;
    @SerializedName("isChanged")
    @Expose
    private Boolean isChanged;
    @SerializedName("ticketStatus")
    @Expose
    private String ticketStatus;
    @SerializedName("ticket")
    @Expose
    private String ticket;
    @SerializedName("passengerType")
    @Expose
    private String passengerType;

    public String getPassengerID() {
        return passengerID;
    }

    public void setPassengerID(String passengerID) {
        this.passengerID = passengerID;
    }

    public Boolean getCheckInLimited() {
        return checkInLimited;
    }

    public void setCheckInLimited(Boolean checkInLimited) {
        this.checkInLimited = checkInLimited;
    }

    public Boolean getCheckInComplete() {
        return checkInComplete;
    }

    public void setCheckInComplete(Boolean checkInComplete) {
        this.checkInComplete = checkInComplete;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getBookingClass() {
        return bookingClass;
    }

    public void setBookingClass(String bookingClass) {
        this.bookingClass = bookingClass;
    }

    public Boolean getPostoAssegnato() {
        return postoAssegnato;
    }

    public void setPostoAssegnato(Boolean postoAssegnato) {
        this.postoAssegnato = postoAssegnato;
    }

    public String getNumeroPostoAssegnato() {
        return numeroPostoAssegnato;
    }

    public void setNumeroPostoAssegnato(String numeroPostoAssegnato) {
        this.numeroPostoAssegnato = numeroPostoAssegnato;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<FrequentFlyer> getFrequentFlyer() {
        return frequentFlyer;
    }

    public void setFrequentFlyer(List<FrequentFlyer> frequentFlyer) {
        this.frequentFlyer = frequentFlyer;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getResidence() {
        return residence;
    }

    public void setResidence(String residence) {
        this.residence = residence;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    public String getCodeFFchanged() {
        return codeFFchanged;
    }

    public void setCodeFFchanged(String codeFFchanged) {
        this.codeFFchanged = codeFFchanged;
    }

    public String getNumberFFchanged() {
        return numberFFchanged;
    }

    public void setNumberFFchanged(String numberFFchanged) {
        this.numberFFchanged = numberFFchanged;
    }

    public Boolean getIsChanged() {
        return isChanged;
    }

    public void setIsChanged(Boolean isChanged) {
        this.isChanged = isChanged;
    }

    public String getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(String ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public String getPassengerType() {
        return passengerType;
    }

    public void setPassengerType(String passengerType) {
        this.passengerType = passengerType;
    }

}
