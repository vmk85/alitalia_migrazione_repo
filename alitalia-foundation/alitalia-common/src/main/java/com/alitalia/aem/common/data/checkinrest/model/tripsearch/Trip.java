
package com.alitalia.aem.common.data.checkinrest.model.tripsearch;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Trip implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@SerializedName("items")
    @Expose
    private List<Item> items = null;
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("warnings")
    @Expose
    private List<Warning> warnings = null;
    @SerializedName("errors")
    @Expose
    private List<Error> errors = null;
    @SerializedName("conversationId")
    @Expose
    private String conversationId;
    @SerializedName("compression")
    @Expose
    private Boolean compression;
    @SerializedName("echoToken")
    @Expose
    private String echoToken;
    @SerializedName("timeStamp")
    @Expose
    private String timeStamp;
    @SerializedName("timeStampSpecified")
    @Expose
    private Boolean timeStampSpecified;
    @SerializedName("target")
    @Expose
    private String target;
    @SerializedName("version")
    @Expose
    private String version;
    @SerializedName("sequenceNmbr")
    @Expose
    private String sequenceNmbr;
    @SerializedName("primaryLangID")
    @Expose
    private String primaryLangID;
    @SerializedName("altLangID")
    @Expose
    private String altLangID;
    @SerializedName("returnContent")
    @Expose
    private Boolean returnContent;

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<Warning> getWarnings() {
        return warnings;
    }

    public void setWarnings(List<Warning> warnings) {
        this.warnings = warnings;
    }

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }

    public String getConversationId() {
        return conversationId;
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    public Boolean getCompression() {
        return compression;
    }

    public void setCompression(Boolean compression) {
        this.compression = compression;
    }

    public String getEchoToken() {
        return echoToken;
    }

    public void setEchoToken(String echoToken) {
        this.echoToken = echoToken;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Boolean getTimeStampSpecified() {
        return timeStampSpecified;
    }

    public void setTimeStampSpecified(Boolean timeStampSpecified) {
        this.timeStampSpecified = timeStampSpecified;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getSequenceNmbr() {
        return sequenceNmbr;
    }

    public void setSequenceNmbr(String sequenceNmbr) {
        this.sequenceNmbr = sequenceNmbr;
    }

    public String getPrimaryLangID() {
        return primaryLangID;
    }

    public void setPrimaryLangID(String primaryLangID) {
        this.primaryLangID = primaryLangID;
    }

    public String getAltLangID() {
        return altLangID;
    }

    public void setAltLangID(String altLangID) {
        this.altLangID = altLangID;
    }

    public Boolean getReturnContent() {
        return returnContent;
    }

    public void setReturnContent(Boolean returnContent) {
        this.returnContent = returnContent;
    }

}
