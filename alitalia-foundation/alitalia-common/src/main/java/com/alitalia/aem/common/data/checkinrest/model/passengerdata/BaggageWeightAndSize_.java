
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaggageWeightAndSize_ {

    @SerializedName("weightInfo")
    @Expose
    private List<WeightInfo_> weightInfo = null;
    @SerializedName("sizeInfo")
    @Expose
    private List<SizeInfo_> sizeInfo = null;

    public List<WeightInfo_> getWeightInfo() {
        return weightInfo;
    }

    public void setWeightInfo(List<WeightInfo_> weightInfo) {
        this.weightInfo = weightInfo;
    }

    public List<SizeInfo_> getSizeInfo() {
        return sizeInfo;
    }

    public void setSizeInfo(List<SizeInfo_> sizeInfo) {
        this.sizeInfo = sizeInfo;
    }

}
