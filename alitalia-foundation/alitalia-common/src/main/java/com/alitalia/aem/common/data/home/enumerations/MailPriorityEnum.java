package com.alitalia.aem.common.data.home.enumerations;

public enum MailPriorityEnum {

    NORMAL("Normal"),
    LOW("Low"),
    HIGH("High");
    private final String value;

    MailPriorityEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MailPriorityEnum fromValue(String v) {
        for (MailPriorityEnum c: MailPriorityEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
