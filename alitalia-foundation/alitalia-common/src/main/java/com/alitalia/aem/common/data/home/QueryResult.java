package com.alitalia.aem.common.data.home;

public class QueryResult {

    private MACustomerData data;

    private String socialProviders;

    public String getSocialProviders() {
        return socialProviders;
    }

    public void setSocialProviders(String socialProviders) {
        this.socialProviders = socialProviders;
    }

    public MACustomerData getData() {
        return data;
    }

    public void setData(MACustomerData data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "QueryResult{" +
                "data=" + data +
                ", socialProviders='" + socialProviders + '\'' +
                '}';
    }
}
