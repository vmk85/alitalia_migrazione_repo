package com.alitalia.aem.common.data.checkinrest.model.searchbyfrequentflyer;


import com.google.common.base.Strings;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.google.common.base.Strings.nullToEmpty;

public class DurationRenderer {

	String durata;

	/**
	 *
	 * @param durata in forma HH:mm:ss
	 */
	public DurationRenderer(String durata) {
		this.durata = nullToEmpty( durata );
	}
	
	public String toString() {

		String[] split = durata.split(":");
		if (split.length == 3) {
			if (Integer.parseInt(split[1]) > 0 ) {
				return Integer.parseInt(split[0]) + "h " + Integer.parseInt(split[1]) + "m";
			} else {
				return Integer.parseInt(split[0]) + "h";
			}
		} else {
			return durata;
		}
	}
}
