package com.alitalia.aem.common.data.checkinrest.model.endpayment.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AzSegmentPayedTP {
	
	@SerializedName("airlineCode")
	@Expose
	private String airlineCode;
	@SerializedName("flightNumber")
	@Expose
	private String flightNumber;
	@SerializedName("departureDate")
	@Expose
	private String departureDate;
	@SerializedName("arrivalDate")
	@Expose
	private String arrivalDate;
	@SerializedName("boardPoint")
	@Expose
	private String boardPoint;
	@SerializedName("boardPointCity")
	@Expose
	private String boardPointCity;
	@SerializedName("offPoint")
	@Expose
	private String offPoint;
	@SerializedName("offPointCity")
	@Expose
	private String offPointCity;
	@SerializedName("emdNumber")
	@Expose
	private String emdNumber;
	
	public String getAirlineCode() {
		return airlineCode;
	}
	
	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}
	
	public String getFlightNumber() {
		return flightNumber;
	}
	
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	
	public String getDepartureDate() {
		return departureDate;
	}
	
	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}
	
	public String getArrivalDate() {
		return arrivalDate;
	}
	
	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	
	public String getBoardPoint() {
		return boardPoint;
	}
	
	public void setBoardPoint(String boardPoint) {
		this.boardPoint = boardPoint;
	}
	
	public String getBoardPointCity() {
		return boardPointCity;
	}
	
	public void setBoardPointCity(String boardPointCity) {
		this.boardPointCity = boardPointCity;
	}
	
	public String getOffPoint() {
		return offPoint;
	}
	
	public void setOffPoint(String offPoint) {
		this.offPoint = offPoint;
	}
	
	public String getOffPointCity() {
		return offPointCity;
	}
	
	public void setOffPointCity(String offPointCity) {
		this.offPointCity = offPointCity;
	}
	
	public String getEmdNumber() {
		return emdNumber;
	}
	
	public void setEmdNumber(String emdNumber) {
		this.emdNumber = emdNumber;
	}


}
