package com.alitalia.aem.common.data.checkinrest.model.lounge.request;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoungeRequest {

    @SerializedName("pnr")
    @Expose
    private String pnr;
    @SerializedName("flightLounge")
    @Expose
    private List<FlightLounge> flightLounge = null;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("market")
    @Expose
    private String market;
    @SerializedName("conversationID")
    @Expose
    private String conversationID;

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public List<FlightLounge> getFlightLounge() {
        return flightLounge;
    }

    public void setFlightLounge(List<FlightLounge> flightLounge) {
        this.flightLounge = flightLounge;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

}
