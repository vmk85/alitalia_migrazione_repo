
package com.alitalia.aem.common.data.checkinrest.model.searchbyticket;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Segment {

    @SerializedName("airline")
    @Expose
    private String airline;
    @SerializedName("flight")
    @Expose
    private String flight;
    @SerializedName("origin")
    @Expose
    private Origin_ origin;
    @SerializedName("destination")
    @Expose
    private Destination_ destination;
    @SerializedName("arrivalDate")
    @Expose
    private String arrivalDate;
    @SerializedName("departureDate")
    @Expose
    private String departureDate;
    @SerializedName("flightStatus")
    @Expose
    private String flightStatus;
    @SerializedName("checkInGate")
    @Expose
    private String checkInGate;
    @SerializedName("passengers")
    @Expose
    private List<Passenger> passengers = null;

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getFlight() {
        return flight;
    }

    public void setFlight(String flight) {
        this.flight = flight;
    }

    public Origin_ getOrigin() {
        return origin;
    }

    public void setOrigin(Origin_ origin) {
        this.origin = origin;
    }

    public Destination_ getDestination() {
        return destination;
    }

    public void setDestination(Destination_ destination) {
        this.destination = destination;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getFlightStatus() {
        return flightStatus;
    }

    public void setFlightStatus(String flightStatus) {
        this.flightStatus = flightStatus;
    }

    public String getCheckInGate() {
        return checkInGate;
    }

    public void setCheckInGate(String checkInGate) {
        this.checkInGate = checkInGate;
    }

    public List<Passenger> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<Passenger> passengers) {
        this.passengers = passengers;
    }

}
