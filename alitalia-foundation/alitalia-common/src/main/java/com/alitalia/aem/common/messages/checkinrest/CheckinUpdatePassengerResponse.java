package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.updatepassenger.response.UpdatePassengerResp;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinUpdatePassengerResponse extends BaseResponse{
	
	private UpdatePassengerResp _updatepassengerResp;

	public UpdatePassengerResp get_updatepassengerResp() {
		return _updatepassengerResp;
	}

	public void set_updatepassengerResp(UpdatePassengerResp _updatepassengerResp) {
		this._updatepassengerResp = _updatepassengerResp;
	}

	public CheckinUpdatePassengerResponse(String tid, String sid) {
		super(tid, sid);
	}

	public CheckinUpdatePassengerResponse() {}
}
