
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PassengerItineraryList {

    @SerializedName("segmentID")
    @Expose
    private Long segmentID;
    @SerializedName("segmentIDSpecified")
    @Expose
    private Boolean segmentIDSpecified;
    @SerializedName("priority")
    @Expose
    private String priority;
    @SerializedName("airline")
    @Expose
    private String airline;
    @SerializedName("flight")
    @Expose
    private String flight;
    @SerializedName("operatingAirline")
    @Expose
    private String operatingAirline;
    @SerializedName("operatingFlight")
    @Expose
    private String operatingFlight;
    @SerializedName("bookingClass")
    @Expose
    private String bookingClass;
    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("departureDate")
    @Expose
    private String departureDate;
    @SerializedName("estimatedDepartureTime")
    @Expose
    private String estimatedDepartureTime;
    @SerializedName("departureGate")
    @Expose
    private String departureGate;
    @SerializedName("aircraftType")
    @Expose
    private String aircraftType;
    @SerializedName("cabin")
    @Expose
    private String cabin;
    @SerializedName("seat")
    @Expose
    private String seat;
    @SerializedName("boardingPassFlag")
    @Expose
    private String boardingPassFlag;
    @SerializedName("passengerType")
    @Expose
    private String passengerType;
    @SerializedName("bagCount")
    @Expose
    private String bagCount;
    @SerializedName("totalBagWeightAndUnit")
    @Expose
    private TotalBagWeightAndUnit totalBagWeightAndUnit;
    @SerializedName("editCodeList")
    @Expose
    private EditCodeList editCodeList;
    @SerializedName("checkInNumber")
    @Expose
    private Long checkInNumber;
    @SerializedName("checkInNumberSpecified")
    @Expose
    private Boolean checkInNumberSpecified;
    @SerializedName("upgradeCode")
    @Expose
    private String upgradeCode;
    @SerializedName("seniorityDate")
    @Expose
    private String seniorityDate;
    @SerializedName("thruIndicator")
    @Expose
    private Boolean thruIndicator;
    @SerializedName("thruIndicatorSpecified")
    @Expose
    private Boolean thruIndicatorSpecified;
    @SerializedName("bookedInventoryIndicator")
    @Expose
    private String bookedInventoryIndicator;
    @SerializedName("tierLevel")
    @Expose
    private String tierLevel;
    @SerializedName("freeTextInfoList")
    @Expose
    private List<FreeTextInfoList> freeTextInfoList = null;
    @SerializedName("bagTagInfoList")
    @Expose
    private List<BagTagInfoList> bagTagInfoList = null;
    @SerializedName("aeDetailsList")
    @Expose
    private List<AeDetailsList> aeDetailsList = null;
    @SerializedName("vcrInfoList")
    @Expose
    private List<VcrInfoList> vcrInfoList = null;
    @SerializedName("ssrInfoList")
    @Expose
    private List<String> ssrInfoList = null;
    @SerializedName("requiredInfoList")
    @Expose
    private List<RequiredInfoList> requiredInfoList = null;

    public Long getSegmentID() {
        return segmentID;
    }

    public void setSegmentID(Long segmentID) {
        this.segmentID = segmentID;
    }

    public Boolean getSegmentIDSpecified() {
        return segmentIDSpecified;
    }

    public void setSegmentIDSpecified(Boolean segmentIDSpecified) {
        this.segmentIDSpecified = segmentIDSpecified;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getFlight() {
        return flight;
    }

    public void setFlight(String flight) {
        this.flight = flight;
    }

    public String getOperatingAirline() {
        return operatingAirline;
    }

    public void setOperatingAirline(String operatingAirline) {
        this.operatingAirline = operatingAirline;
    }

    public String getOperatingFlight() {
        return operatingFlight;
    }

    public void setOperatingFlight(String operatingFlight) {
        this.operatingFlight = operatingFlight;
    }

    public String getBookingClass() {
        return bookingClass;
    }

    public void setBookingClass(String bookingClass) {
        this.bookingClass = bookingClass;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getEstimatedDepartureTime() {
        return estimatedDepartureTime;
    }

    public void setEstimatedDepartureTime(String estimatedDepartureTime) {
        this.estimatedDepartureTime = estimatedDepartureTime;
    }

    public String getDepartureGate() {
        return departureGate;
    }

    public void setDepartureGate(String departureGate) {
        this.departureGate = departureGate;
    }

    public String getAircraftType() {
        return aircraftType;
    }

    public void setAircraftType(String aircraftType) {
        this.aircraftType = aircraftType;
    }

    public String getCabin() {
        return cabin;
    }

    public void setCabin(String cabin) {
        this.cabin = cabin;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public String getBoardingPassFlag() {
        return boardingPassFlag;
    }

    public void setBoardingPassFlag(String boardingPassFlag) {
        this.boardingPassFlag = boardingPassFlag;
    }

    public String getPassengerType() {
        return passengerType;
    }

    public void setPassengerType(String passengerType) {
        this.passengerType = passengerType;
    }

    public String getBagCount() {
        return bagCount;
    }

    public void setBagCount(String bagCount) {
        this.bagCount = bagCount;
    }

    public TotalBagWeightAndUnit getTotalBagWeightAndUnit() {
        return totalBagWeightAndUnit;
    }

    public void setTotalBagWeightAndUnit(TotalBagWeightAndUnit totalBagWeightAndUnit) {
        this.totalBagWeightAndUnit = totalBagWeightAndUnit;
    }

    public EditCodeList getEditCodeList() {
        return editCodeList;
    }

    public void setEditCodeList(EditCodeList editCodeList) {
        this.editCodeList = editCodeList;
    }

    public Long getCheckInNumber() {
        return checkInNumber;
    }

    public void setCheckInNumber(Long checkInNumber) {
        this.checkInNumber = checkInNumber;
    }

    public Boolean getCheckInNumberSpecified() {
        return checkInNumberSpecified;
    }

    public void setCheckInNumberSpecified(Boolean checkInNumberSpecified) {
        this.checkInNumberSpecified = checkInNumberSpecified;
    }

    public String getUpgradeCode() {
        return upgradeCode;
    }

    public void setUpgradeCode(String upgradeCode) {
        this.upgradeCode = upgradeCode;
    }

    public String getSeniorityDate() {
        return seniorityDate;
    }

    public void setSeniorityDate(String seniorityDate) {
        this.seniorityDate = seniorityDate;
    }

    public Boolean getThruIndicator() {
        return thruIndicator;
    }

    public void setThruIndicator(Boolean thruIndicator) {
        this.thruIndicator = thruIndicator;
    }

    public Boolean getThruIndicatorSpecified() {
        return thruIndicatorSpecified;
    }

    public void setThruIndicatorSpecified(Boolean thruIndicatorSpecified) {
        this.thruIndicatorSpecified = thruIndicatorSpecified;
    }

    public String getBookedInventoryIndicator() {
        return bookedInventoryIndicator;
    }

    public void setBookedInventoryIndicator(String bookedInventoryIndicator) {
        this.bookedInventoryIndicator = bookedInventoryIndicator;
    }

    public String getTierLevel() {
        return tierLevel;
    }

    public void setTierLevel(String tierLevel) {
        this.tierLevel = tierLevel;
    }

    public List<FreeTextInfoList> getFreeTextInfoList() {
        return freeTextInfoList;
    }

    public void setFreeTextInfoList(List<FreeTextInfoList> freeTextInfoList) {
        this.freeTextInfoList = freeTextInfoList;
    }

    public List<BagTagInfoList> getBagTagInfoList() {
        return bagTagInfoList;
    }

    public void setBagTagInfoList(List<BagTagInfoList> bagTagInfoList) {
        this.bagTagInfoList = bagTagInfoList;
    }

    public List<AeDetailsList> getAeDetailsList() {
        return aeDetailsList;
    }

    public void setAeDetailsList(List<AeDetailsList> aeDetailsList) {
        this.aeDetailsList = aeDetailsList;
    }

    public List<VcrInfoList> getVcrInfoList() {
        return vcrInfoList;
    }

    public void setVcrInfoList(List<VcrInfoList> vcrInfoList) {
        this.vcrInfoList = vcrInfoList;
    }

    public List<String> getSsrInfoList() {
        return ssrInfoList;
    }

    public void setSsrInfoList(List<String> ssrInfoList) {
        this.ssrInfoList = ssrInfoList;
    }

    public List<RequiredInfoList> getRequiredInfoList() {
        return requiredInfoList;
    }

    public void setRequiredInfoList(List<RequiredInfoList> requiredInfoList) {
        this.requiredInfoList = requiredInfoList;
    }

}
