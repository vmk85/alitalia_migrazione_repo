package com.alitalia.aem.common.component.behaviour;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.alitalia.aem.common.component.behaviour.exc.BehaviourException;
import com.alitalia.aem.common.logging.LogPlaceholder;
import com.alitalia.aem.common.messages.Request;
import com.alitalia.aem.common.messages.Response;

public abstract class Behaviour <S extends Request, T extends Response> implements IBehaviour{
	
	private static final Logger logger = LoggerFactory.getLogger(Behaviour.class);
	
	public final T execute(S request){
		T response = null;
		
		try{
			MDC.put(LogPlaceholder.TID.getValue(), "");
			logger.debug("Executing method. The request is {}", request);
			response = executeOrchestration(request);
			logger.debug("Executed method. The response is {}", response);
			return response;
		}catch(Exception e){
			logger.error("Exception while executing method.", e);
			throw new BehaviourException(e.getMessage(), e);
		}finally{
			MDC.remove(LogPlaceholder.TID.getValue());
		}
		
	}
	
	protected abstract T executeOrchestration(S request);
}