
package com.alitalia.aem.common.data.checkinrest.model.enhancedseatmap.request;

public class ListItemsFareAvailQualifier {


    private String givenName;

    private String surname;

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

}
