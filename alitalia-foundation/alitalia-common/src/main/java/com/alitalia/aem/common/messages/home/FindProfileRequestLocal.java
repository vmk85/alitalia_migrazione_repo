package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.BaseRequest;

public class FindProfileRequestLocal extends BaseRequest {

	private String alias;
	private String codiceMM;

	private String cognome;

	private String username;

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getCodiceMM() {
		return codiceMM;
	}

	public void setCodiceMM(String codiceMM) {
		this.codiceMM = codiceMM;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;

		FindProfileRequestLocal that = (FindProfileRequestLocal) o;

		if (alias != null ? !alias.equals(that.alias) : that.alias != null) return false;
		if (codiceMM != null ? !codiceMM.equals(that.codiceMM) : that.codiceMM != null) return false;
		if (cognome != null ? !cognome.equals(that.cognome) : that.cognome != null) return false;
		return username != null ? username.equals(that.username) : that.username == null;
	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (alias != null ? alias.hashCode() : 0);
		result = 31 * result + (codiceMM != null ? codiceMM.hashCode() : 0);
		result = 31 * result + (cognome != null ? cognome.hashCode() : 0);
		result = 31 * result + (username != null ? username.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "FindProfileRequestLocal{" +
				"alias='" + alias + '\'' +
				", codiceMM='" + codiceMM + '\'' +
				", cognome='" + cognome + '\'' +
				", username='" + username + '\'' +
				'}';
	}
}