package com.alitalia.aem.common.data.home.enumerations;

public enum DocumentTypeEnum {

    NONE("None"),
    VISA("Visa"),
    GREEN_CARD("GreenCard");
    
    private final String value;

    DocumentTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DocumentTypeEnum fromValue(String v) {
        for (DocumentTypeEnum c: DocumentTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
