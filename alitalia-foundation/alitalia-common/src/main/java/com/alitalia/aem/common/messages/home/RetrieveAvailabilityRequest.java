package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class RetrieveAvailabilityRequest extends BaseRequest {
	
	private String originIATA;
	private String destinationIATA;
	private String market;
	private String language;
	private String conversationID;
	
	public RetrieveAvailabilityRequest() {}

	public RetrieveAvailabilityRequest(String tid, String sid) {
		super(tid, sid);
	}

	public String getOriginIATA() {
		return originIATA;
	}
	
	public void setOriginIATA(String originIATA) {
		this.originIATA = originIATA;
	}
	
	public String getDestinationIATA() {
		return destinationIATA;
	}
	
	public void setDestinationIATA(String destinationIATA) {
		this.destinationIATA = destinationIATA;
	}
	
	public String getMarket() {
		return market;
	}
	
	public void setMarket(String market) {
		this.market = market;
	}
	
	public String getLanguage() {
		return language;
	}
	
	public void setLanguage(String language) {
		this.language = language;
	}

	public String getConversationID() {
		return conversationID;
	}

	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}
	
	

}
