
package com.alitalia.aem.common.data.checkinrest.model.updatepassenger.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InfantDocumentOther {

    @SerializedName("birthPlace")
    @Expose
    private String birthPlace;
    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("issueCity")
    @Expose
    private String issueCity;
    @SerializedName("issueDate")
    @Expose
    private String issueDate;
    @SerializedName("applicableCountryCode")
    @Expose
    private String applicableCountryCode;

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getIssueCity() {
        return issueCity;
    }

    public void setIssueCity(String issueCity) {
        this.issueCity = issueCity;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getApplicableCountryCode() {
        return applicableCountryCode;
    }

    public void setApplicableCountryCode(String applicableCountryCode) {
        this.applicableCountryCode = applicableCountryCode;
    }

}
