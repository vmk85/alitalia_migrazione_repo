package com.alitalia.aem.common.data.home;


public class SocialAccountData {

	private String email;
    private String gigyaUID;
    private Boolean logicalDelete;
    private String loginProvider;
	
    public String getEmail() {
		return email;
	}
    
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getGigyaUID() {
		return gigyaUID;
	}
	
	public void setGigyaUID(String gigyaUID) {
		this.gigyaUID = gigyaUID;
	}
	
	public Boolean getLogicalDelete() {
		return logicalDelete;
	}
	
	public void setLogicalDelete(Boolean logicalDelete) {
		this.logicalDelete = logicalDelete;
	}
	
	public String getLoginProvider() {
		return loginProvider;
	}
	
	public void setLoginProvider(String loginProvider) {
		this.loginProvider = loginProvider;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SocialAccountData other = (SocialAccountData) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SocialAccountData [email=" + email + ", gigyaUID=" + gigyaUID
				+ ", logicalDelete=" + logicalDelete + ", loginProvider="
				+ loginProvider + "]";
	}
}