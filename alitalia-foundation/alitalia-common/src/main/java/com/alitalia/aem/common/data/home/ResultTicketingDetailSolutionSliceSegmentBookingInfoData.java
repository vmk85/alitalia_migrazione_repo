package com.alitalia.aem.common.data.home;

public class ResultTicketingDetailSolutionSliceSegmentBookingInfoData {

    private String bookingCodeCountField;
    private String bookingCodeField;
    private String cabinField;
    private Integer marriedSegmentIndexField;
    private Boolean marriedSegmentIndexFieldSpecified;

    public String getBookingCodeCountField() {
        return bookingCodeCountField;
    }

    public void setBookingCodeCountField(String value) {
        this.bookingCodeCountField = value;
    }

    public String getBookingCodeField() {
        return bookingCodeField;
    }

    public void setBookingCodeField(String value) {
        this.bookingCodeField = value;
    }

    public String getCabinField() {
        return cabinField;
    }

    public void setCabinField(String value) {
        this.cabinField = value;
    }

    public Integer getMarriedSegmentIndexField() {
        return marriedSegmentIndexField;
    }

    public void setMarriedSegmentIndexField(int value) {
        this.marriedSegmentIndexField = value;
    }

    public Boolean isMarriedSegmentIndexFieldSpecified() {
        return marriedSegmentIndexFieldSpecified;
    }

    public void setMarriedSegmentIndexFieldSpecified(Boolean value) {
        this.marriedSegmentIndexFieldSpecified = value;
    }

}
