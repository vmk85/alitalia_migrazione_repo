package com.alitalia.aem.common.data.home.mmb.ancillary;

public class MmbAncillaryFrequentFlyerData {

	private String code;
	private String ffType;
	private String tier;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getFfType() {
		return ffType;
	}

	public void setFfType(String ffType) {
		this.ffType = ffType;
	}

	public String getTier() {
		return tier;
	}

	public void setTier(String tier) {
		this.tier = tier;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((ffType == null) ? 0 : ffType.hashCode());
		result = prime * result + ((tier == null) ? 0 : tier.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbAncillaryFrequentFlyerData other = (MmbAncillaryFrequentFlyerData) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (ffType == null) {
			if (other.ffType != null)
				return false;
		} else if (!ffType.equals(other.ffType))
			return false;
		if (tier == null) {
			if (other.tier != null)
				return false;
		} else if (!tier.equals(other.tier))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbAncillaryFrequentFlyerData [code=" + code + ", ffType="
				+ ffType + ", MMTier=" + tier + "]";
	}

}
