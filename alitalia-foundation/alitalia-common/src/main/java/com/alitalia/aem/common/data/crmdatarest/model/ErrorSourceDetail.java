package com.alitalia.aem.common.data.crmdatarest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ErrorSourceDetail {

    @Override
    public String toString() {
        return "ErrorSourceDetail [details=" + details + ", " +
                ", source=" + source + "]";
    }

    @SerializedName("details")
    @Expose
    private String details;

    @SerializedName("source")
    @Expose
    private String source;



    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
