package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.messages.BaseRequest;

public class CheckinTripSearchRequest extends BaseRequest{
	
	//TODO - gestire la cache ?
	private String firstName;
	private String lastName;
	private String ticket;
	private String pnr;
	private String frequentFlyer;
	
	public CheckinTripSearchRequest(String tid, String sid) {
		super(tid, sid);
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	public String getPnr() {
		return pnr;
	}
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	public String getFrequentFlyer() {
		return frequentFlyer;
	}
	public void setFrequentFlyer(String frequentFlyer) {
		this.frequentFlyer = frequentFlyer;
	}
}
