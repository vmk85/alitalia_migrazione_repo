package com.alitalia.aem.common.data.home;

import java.util.Arrays;

public class MACustomer {

    public static final String NAME = "MACustomer";
    private String UID;
    private String UIDSignature;
    private boolean isGlobal;
    private boolean newUser;
    private MACustomerProfile profile;
    private String loginMode;
    private MACustomerData data;
    private String socialProviders;
    private SessionInfoGigya sessionInfo;
    private String errorDetail;
    private int errorCode;
    private String errorMessage;
    private QueryResult[] results;

    public QueryResult[] getResults() {
        return results;
    }

    public void setResults(QueryResult[] results) {
        this.results = results;
    }

    public String getErrorDetail() {
        return errorDetail;
    }

    public void setErrorDetail(String errorDetail) {
        this.errorDetail = errorDetail;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    private MAProvider[] identities;

    public SessionInfoGigya getSessionInfo() {
        return sessionInfo;
    }

    public void setSessionInfo(SessionInfoGigya sessionInfo) {
        this.sessionInfo = sessionInfo;
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public String getUIDSignature() {
        return UIDSignature;
    }

    public void setUIDSignature(String UIDSignature) {
        this.UIDSignature = UIDSignature;
    }

    public boolean isGlobal() {
        return isGlobal;
    }

    public void setGlobal(boolean global) {
        isGlobal = global;
    }

    public boolean isNewUser() {
        return newUser;
    }

    public void setNewUser(boolean newUser) {
        this.newUser = newUser;
    }

    public MACustomerProfile getProfile() {
        return profile;
    }

    public void setProfile(MACustomerProfile profile) {
        this.profile = profile;
    }

    public MACustomerData getData() {
        return data;
    }

    public void setData(MACustomerData data) {
        this.data = data;
    }

    public String getLoginMode() {
        return loginMode;
    }

    public void setLoginMode(String loginMode) {
        this.loginMode = loginMode;
    }

    public static String getNAME() {
        return NAME;
    }

    public String getSocialProviders() {
        return socialProviders;
    }

    public void setSocialProviders(String socialProviders) {
        this.socialProviders = socialProviders;
    }

    public MAProvider[] getIdentities() {
        return identities;
    }

    public void setIdentities(MAProvider[] identities) {
        this.identities = identities;
    }

    @Override
    public String toString() {
        return "MACustomer{" +
                "UID='" + UID + '\'' +
                ", UIDSignature='" + UIDSignature + '\'' +
                ", isGlobal=" + isGlobal +
                ", newUser=" + newUser +
                ", profile=" + profile +
                ", loginMode='" + loginMode + '\'' +
                ", data=" + data +
                ", socialProviders='" + socialProviders + '\'' +
                ", sessionInfo=" + sessionInfo +
                ", errorDetail='" + errorDetail + '\'' +
                ", errorCode=" + errorCode +
                ", errorMessage='" + errorMessage + '\'' +
                ", results=" + Arrays.toString(results) +
                ", identities=" + Arrays.toString(identities) +
                '}';
    }
}
