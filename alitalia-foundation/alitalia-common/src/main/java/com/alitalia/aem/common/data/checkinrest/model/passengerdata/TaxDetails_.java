
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TaxDetails_ {

    @SerializedName("totalTaxAmount")
    @Expose
    private TotalTaxAmount_ totalTaxAmount;
    @SerializedName("taxAmount")
    @Expose
    private List<TaxAmount_> taxAmount = null;

    public TotalTaxAmount_ getTotalTaxAmount() {
        return totalTaxAmount;
    }

    public void setTotalTaxAmount(TotalTaxAmount_ totalTaxAmount) {
        this.totalTaxAmount = totalTaxAmount;
    }

    public List<TaxAmount_> getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(List<TaxAmount_> taxAmount) {
        this.taxAmount = taxAmount;
    }

}
