package com.alitalia.aem.common.data.checkinrest.model.getcart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class checkinCartObj {

    @SerializedName("pnr")
    @Expose
    private String pnr;
    @SerializedName("cartPriceToBeIssued")
    @Expose
    private String cartPriceToBeIssued;
    @SerializedName("cartPriceIssued")
    @Expose
    private String cartPriceIssued;
    @SerializedName("aZFlightCart")
    @Expose
    private List<AZFlightCart> aZFlightCart = null;

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public String getCartPriceToBeIssued(String separatore) {

        String ret = "";

        if (separatore != null){
             ret = cartPriceToBeIssued.replace(separatore,"");
        } else {
            ret = cartPriceToBeIssued;
        }
        return ret;
    }

    public void setCartPriceToBeIssued(String cartPriceToBeIssued) {
        this.cartPriceToBeIssued = cartPriceToBeIssued;
    }

    public String getCartPriceIssued() {
        return cartPriceIssued;
    }

    public void setCartPriceIssued(String cartPriceIssued) {
        this.cartPriceIssued = cartPriceIssued;
    }

    public List<AZFlightCart> getAZFlightCart() {
        return aZFlightCart;
    }

    public void setAZFlightCart(List<AZFlightCart> aZFlightCart) {
        this.aZFlightCart = aZFlightCart;
    }

}
