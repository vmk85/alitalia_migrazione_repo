package com.alitalia.aem.common.data.home.enumerations;

public enum CreditCardTypeEnum {

    AMERICAN_EXPRESS("AmericanExpress"),
    MASTER_CARD("MasterCard"),
    VISA("Visa"),
    VISA_ELECTRON("VisaElectron"),
    DINERS("Diners"),
    UATP("Uatp"),
    JCB("JCB"),
    Discover("Discover"),
    MAESTRO("Maestro");
    
    private final String value;

    CreditCardTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CreditCardTypeEnum fromValue(String v) {
        for (CreditCardTypeEnum c: CreditCardTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
