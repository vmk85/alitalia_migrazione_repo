package com.alitalia.aem.common.messages;

public class GigyaBaseRequest implements Request {

	protected String apiKey;
	protected String secretKey;
    protected String userKey;
    protected String domain;
	
	public GigyaBaseRequest(){}
	
	public GigyaBaseRequest(String apiKey, String secretKey, String userKey){
		this.apiKey = apiKey;
		this.secretKey = secretKey;
		this.userKey = userKey;
	}
	
	public GigyaBaseRequest(String apiKey, String secretKey, String userKey, String domain){
		this.apiKey = apiKey;
		this.secretKey = secretKey;
		this.userKey = userKey;
		this.domain = domain;
	}
	
	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getUserKey() {
		return userKey;
	}

	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((apiKey == null) ? 0 : apiKey.hashCode());
		result = prime * result	+ ((secretKey == null) ? 0 : secretKey.hashCode());
		result = prime * result + ((userKey == null) ? 0 : userKey.hashCode());
		result = prime * result + ((domain == null) ? 0 : domain.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GigyaBaseRequest other = (GigyaBaseRequest) obj;
		if (apiKey == null) {
			if (other.apiKey != null)
				return false;
		} else if (!apiKey.equals(other.apiKey))
			return false;
		if (secretKey == null) {
			if (other.secretKey != null)
				return false;
		} else if (!secretKey.equals(other.secretKey))
			return false;
		if (userKey == null) {
			if (other.userKey != null)
				return false;
		} else if (!userKey.equals(other.userKey))
			return false;
		if (domain == null) {
			if (other.domain != null)
				return false;
		} else if (!domain.equals(other.domain))
			return false;
		return true;
		
	}

	@Override
	public String toString() {
		return "GigyaBaseRequest [apiKey=" + apiKey + ", secretKey=" + secretKey 
				+ ", userKey=" + userKey + ", domain=" + domain + "]";
	}
}