package com.alitalia.aem.common.messages.home;

import java.util.Calendar;

import com.alitalia.aem.common.data.home.enumerations.MmbLegTypeEnum;
import com.alitalia.aem.common.messages.MmbBaseRequest;

public class WebCheckinGetInsuranceRequest extends MmbBaseRequest {

	private MmbLegTypeEnum area;
    private String arrivalAirport;
    private Calendar arrivalDate;
    private String countryCode;
    private String departureAirport;
    private Calendar departureDate;
    private Boolean hasReturn;
    private String languageCode;
    private Integer passengersCount;
    private String pnr;
    private Boolean flgUsaCa;

	public WebCheckinGetInsuranceRequest(String tid, String sid) {
		super(tid, sid);
	}

	public WebCheckinGetInsuranceRequest() {
		super();
	}

	public MmbLegTypeEnum getArea() {
		return area;
	}

	public void setArea(MmbLegTypeEnum area) {
		this.area = area;
	}

	public String getArrivalAirport() {
		return arrivalAirport;
	}

	public void setArrivalAirport(String arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

	public Calendar getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Calendar arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getDepartureAirport() {
		return departureAirport;
	}

	public void setDepartureAirport(String departureAirport) {
		this.departureAirport = departureAirport;
	}

	public Calendar getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Calendar departureDate) {
		this.departureDate = departureDate;
	}

	public Boolean getHasReturn() {
		return hasReturn;
	}

	public void setHasReturn(Boolean hasReturn) {
		this.hasReturn = hasReturn;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public Integer getPassengersCount() {
		return passengersCount;
	}

	public void setPassengersCount(Integer passengersCount) {
		this.passengersCount = passengersCount;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Boolean getFlgUsaCa() {
		return flgUsaCa;
	}

	public void setFlgUsaCa(Boolean flgUsaCa) {
		this.flgUsaCa = flgUsaCa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((area == null) ? 0 : area.hashCode());
		result = prime * result
				+ ((arrivalAirport == null) ? 0 : arrivalAirport.hashCode());
		result = prime * result
				+ ((arrivalDate == null) ? 0 : arrivalDate.hashCode());
		result = prime * result
				+ ((countryCode == null) ? 0 : countryCode.hashCode());
		result = prime
				* result
				+ ((departureAirport == null) ? 0 : departureAirport.hashCode());
		result = prime * result
				+ ((departureDate == null) ? 0 : departureDate.hashCode());
		result = prime * result
				+ ((flgUsaCa == null) ? 0 : flgUsaCa.hashCode());
		result = prime * result
				+ ((hasReturn == null) ? 0 : hasReturn.hashCode());
		result = prime * result
				+ ((languageCode == null) ? 0 : languageCode.hashCode());
		result = prime * result
				+ ((passengersCount == null) ? 0 : passengersCount.hashCode());
		result = prime * result + ((pnr == null) ? 0 : pnr.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebCheckinGetInsuranceRequest other = (WebCheckinGetInsuranceRequest) obj;
		if (area != other.area)
			return false;
		if (arrivalAirport == null) {
			if (other.arrivalAirport != null)
				return false;
		} else if (!arrivalAirport.equals(other.arrivalAirport))
			return false;
		if (arrivalDate == null) {
			if (other.arrivalDate != null)
				return false;
		} else if (!arrivalDate.equals(other.arrivalDate))
			return false;
		if (countryCode == null) {
			if (other.countryCode != null)
				return false;
		} else if (!countryCode.equals(other.countryCode))
			return false;
		if (departureAirport == null) {
			if (other.departureAirport != null)
				return false;
		} else if (!departureAirport.equals(other.departureAirport))
			return false;
		if (departureDate == null) {
			if (other.departureDate != null)
				return false;
		} else if (!departureDate.equals(other.departureDate))
			return false;
		if (flgUsaCa == null) {
			if (other.flgUsaCa != null)
				return false;
		} else if (!flgUsaCa.equals(other.flgUsaCa))
			return false;
		if (hasReturn == null) {
			if (other.hasReturn != null)
				return false;
		} else if (!hasReturn.equals(other.hasReturn))
			return false;
		if (languageCode == null) {
			if (other.languageCode != null)
				return false;
		} else if (!languageCode.equals(other.languageCode))
			return false;
		if (passengersCount == null) {
			if (other.passengersCount != null)
				return false;
		} else if (!passengersCount.equals(other.passengersCount))
			return false;
		if (pnr == null) {
			if (other.pnr != null)
				return false;
		} else if (!pnr.equals(other.pnr))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WebCheckinGetInsuranceRequest [area=" + area
				+ ", arrivalAirport=" + arrivalAirport + ", arrivalDate="
				+ arrivalDate + ", countryCode=" + countryCode
				+ ", departureAirport=" + departureAirport + ", departureDate="
				+ departureDate + ", hasReturn=" + hasReturn
				+ ", languageCode=" + languageCode + ", passengersCount="
				+ passengersCount + ", pnr=" + pnr + ", flgUsaCa=" + flgUsaCa
				+ ", toString()=" + super.toString() + "]";
	}
}
