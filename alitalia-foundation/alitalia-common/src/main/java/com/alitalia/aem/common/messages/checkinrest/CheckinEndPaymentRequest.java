package com.alitalia.aem.common.messages.checkinrest;

import java.util.List;

import com.alitalia.aem.common.data.checkinrest.model.endpayment.request.FraudNetParam;
import com.alitalia.aem.common.data.checkinrest.model.endpayment.request.InsurancePayment;
import com.alitalia.aem.common.data.checkinrest.model.endpayment.request.PaymentDetail;
import com.alitalia.aem.common.messages.BaseRequest;

public class CheckinEndPaymentRequest extends BaseRequest{
	
	public CheckinEndPaymentRequest(String tid, String sid) {
		super(tid, sid);
	}

    private String pnr;
 
    private String client;

    private String iP_Address;

    private String paymentStatus;
 
    private PaymentDetail paymentDetail;

    private InsurancePayment insurancePayment;

    private List<FraudNetParam> fraudNetParam = null;
 
    private String language;

    private String market;

    private String conversationID;

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getIP_Address() {
        return iP_Address;
    }

    public void setIP_Address(String iPAddress) {
        this.iP_Address = iPAddress;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public PaymentDetail getPaymentDetail() {
        return paymentDetail;
    }

    public void setPaymentDetail(PaymentDetail paymentDetail) {
        this.paymentDetail = paymentDetail;
    }

    public InsurancePayment getInsurancePayment() {
        return insurancePayment;
    }

    public void setInsurancePayment(InsurancePayment insurancePayment) {
        this.insurancePayment = insurancePayment;
    }

    public List<FraudNetParam> getFraudNetParam() {
        return fraudNetParam;
    }

    public void setFraudNetParam(List<FraudNetParam> fraudNetParam) {
        this.fraudNetParam = fraudNetParam;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }


}
