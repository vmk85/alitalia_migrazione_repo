package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.GigyaBaseRequest;

public class RegisterCreditCardRequest  extends GigyaBaseRequest {

//	private String creditCardType;
//	private String creditCardNumber;
//	private String creditCardCVV;
//	private String creditCardName;
//	private String creditCardLastName;
//	private Short creditCardExpiryMonth;
//	private Short creditCardExpiryYear;
//	private String zip;
//	private String city;
//	private String country;
//	private String address;
//	private String state;
	
	private String UID;
	private String creditCardDataToken;

	
	public RegisterCreditCardRequest(String UID, String creditCardDataToken) {
		super();
		this.UID = UID;
		this.creditCardDataToken = creditCardDataToken;
	}

//	public RegisterCreditCardRequest(String creditCardType, String creditCardNumber, String creditCardCVV,
//			String creditCardName, String creditCardLastName, Short creditCardExpiryMonth, Short creditCardExpiryYear) {
//		super();
//		this.creditCardType = creditCardType;
//		this.creditCardNumber = creditCardNumber;
//		this.creditCardCVV = creditCardCVV;
//		this.creditCardName = creditCardName;
//		this.creditCardLastName = creditCardLastName;
//		this.creditCardExpiryMonth = creditCardExpiryMonth;
//		this.creditCardExpiryYear = creditCardExpiryYear;
//	}
//
//	public void fillAddressData(String zip, String city, String country, String address, String state) {
//		this.zip = zip;
//		this.city = city;
//		this.country = country;
//		this.address = address;
//		this.state = state;
//		
//		//Recupera la provincia solo se il paese è USA
//		if (country.equalsIgnoreCase("us")==false) {
//			state = null;
//		}
//	}
	
	public void fillGigyaData(String apiKey, String secretKey, String userKey){
		this.apiKey = apiKey;
		this.secretKey = secretKey;
		this.userKey = userKey;
	}
	
	public void fillGigyaData(String apiKey, String secretKey, String userKey, String domain){
		this.apiKey = apiKey;
		this.secretKey = secretKey;
		this.userKey = userKey;
		this.domain = domain;
	}
	
//	public String getCreditCardType() {
//		return creditCardType;
//	}
//
//	public void setCreditCardType(String creditCardType) {
//		this.creditCardType = creditCardType;
//	}
//
//	public String getCreditCardNumber() {
//		return creditCardNumber;
//	}
//
//	public void setCreditCardNumber(String creditCardNumber) {
//		this.creditCardNumber = creditCardNumber;
//	}
//
//	public String getCreditCardCVV() {
//		return creditCardCVV;
//	}
//
//	public void setCreditCardCVV(String creditCardCVV) {
//		this.creditCardCVV = creditCardCVV;
//	}
//
//	public String getCreditCardName() {
//		return creditCardName;
//	}
//
//	public void setCreditCardName(String creditCardName) {
//		this.creditCardName = creditCardName;
//	}
//
//	public String getCreditCardLastName() {
//		return creditCardLastName;
//	}
//
//	public void setCreditCardLastName(String creditCardLastName) {
//		this.creditCardLastName = creditCardLastName;
//	}
//
//	public Short getCreditCardExpiryMonth() {
//		return creditCardExpiryMonth;
//	}
//
//	public void setCreditCardExpiryMonth(Short creditCardExpiryMonth) {
//		this.creditCardExpiryMonth = creditCardExpiryMonth;
//	}
//
//	public Short getCreditCardExpiryYear() {
//		return creditCardExpiryYear;
//	}
//
//	public void setCreditCardExpiryYear(Short creditCardExpiryYear) {
//		this.creditCardExpiryYear = creditCardExpiryYear;
//	}
//
//	public String getZip() {
//		return zip;
//	}
//
//	public void setZip(String zip) {
//		this.zip = zip;
//	}
//
//	public String getCity() {
//		return city;
//	}
//
//	public void setCity(String city) {
//		this.city = city;
//	}
//
//	public String getCountry() {
//		return country;
//	}
//
//	public void setCountry(String country) {
//		this.country = country;
//	}
//
//	public String getAddress() {
//		return address;
//	}
//
//	public void setAddress(String address) {
//		this.address = address;
//	}
//
//	public String getState() {
//		return state;
//	}
//
//	public void setState(String state) {
//		this.state = state;
//	}

	public String getUID() {
		return UID;
	}

	public void setUID(String UID) {
		this.UID = UID;
	}

	public String getCreditCardDataToken() {
		return creditCardDataToken;
	}

	public void setCreditCardDataToken(String creditCardDataToken) {
		this.creditCardDataToken = creditCardDataToken;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
//		result = prime * result	+ ((creditCardType == null) ? 0 : creditCardType.hashCode());
//		result = prime * result	+ ((creditCardNumber == null) ? 0 : creditCardNumber.hashCode());
//		result = prime * result	+ ((creditCardCVV == null) ? 0 : creditCardCVV.hashCode());
//		result = prime * result	+ ((creditCardName == null) ? 0 : creditCardName.hashCode());
//		result = prime * result + (creditCardLastName == null ? 0 : creditCardLastName.hashCode());
//		result = prime * result + ((creditCardExpiryMonth == null) ? 0 : creditCardExpiryMonth.hashCode());
//		result = prime * result + ((creditCardExpiryYear == null) ? 0 : creditCardExpiryYear.hashCode());
//		result = prime * result + ((zip == null) ? 0 : zip.hashCode());
//		result = prime * result + ((city == null) ? 0 : city.hashCode());
//		result = prime * result + ((country == null) ? 0 : country.hashCode());
//		result = prime * result + ((address == null) ? 0 : address.hashCode());
//		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((UID == null) ? 0 : UID.hashCode());
		result = prime * result + ((creditCardDataToken == null) ? 0 : creditCardDataToken.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegisterCreditCardRequest other = (RegisterCreditCardRequest) obj;
//		if (creditCardType == null) {
//			if (other.creditCardType != null)
//				return false;
//		} else if (!creditCardType.equals(other.creditCardType))
//			return false;
//		if (creditCardNumber == null) {
//			if (other.creditCardNumber != null)
//				return false;
//		} else if (!creditCardNumber.equals(other.creditCardNumber))
//			return false;
//		if (creditCardCVV == null) {
//			if (other.creditCardCVV != null)
//				return false;
//		} else if (!creditCardCVV.equals(other.creditCardCVV))
//			return false;
//		if (creditCardName == null) {
//			if (other.creditCardName != null)
//				return false;
//		} else if (!creditCardName.equals(other.creditCardName))
//			return false;
//		if (creditCardLastName == null) {
//			if (other.creditCardLastName != null)
//				return false;
//		} else if (!creditCardLastName.equals(other.creditCardLastName))
//			return false;
//
//		if (creditCardExpiryMonth == null) {
//			if (other.creditCardExpiryMonth != null)
//				return false;
//		} else if (!creditCardExpiryMonth.equals(other.creditCardExpiryMonth))
//			return false;
//		if (creditCardExpiryYear == null) {
//			if (other.creditCardExpiryYear != null)
//				return false;
//		} else if (!creditCardExpiryYear.equals(other.creditCardExpiryYear))
//			return false;
//		if (zip == null) {
//			if (other.zip != null)
//				return false;
//		} else if (!zip.equals(other.zip))
//			return false;
//		if (city == null) {
//			if (other.city != null)
//				return false;
//		} else if (!city.equals(other.city))
//			return false;
//		if (country == null) {
//			if (other.country != null)
//				return false;
//		} else if (!country.equals(other.country))
//			return false;
//		if (address == null) {
//			if (other.address != null)
//				return false;
//		} else if (!address.equals(other.address))
//			return false;
//		if (state == null) {
//			if (other.state != null)
//				return false;
//		} else if (!state.equals(other.state))
//			return false;
		if (UID == null) {
			if (other.UID != null)
				return false;
		} else if (!UID.equals(other.UID))
			return false;
		if (creditCardDataToken == null) {
			if (other.creditCardDataToken != null)
				return false;
		} else if (!creditCardDataToken.equals(other.creditCardDataToken))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "RegisterCreditCardRequest ["
//				+ "creditCardType=" + creditCardType
//				+ ", creditCardNumber=" + creditCardNumber + ", creditCardCVV=" + creditCardCVV
//				+ ", creditCardName=" + creditCardName + ", creditCardLastName=" + creditCardLastName + ", creditCardExpiryMonth=" + creditCardExpiryMonth
//				+ ", creditCardExpiryYear=" + creditCardExpiryYear + ", zip=" + zip + ", city=" + city
//				+ ", country=" + country + ", address=" + address + ", state=" + state + ", "
				+ "UID=" + UID + ", creditCardDataToken=" + creditCardDataToken + "]";
	}
}