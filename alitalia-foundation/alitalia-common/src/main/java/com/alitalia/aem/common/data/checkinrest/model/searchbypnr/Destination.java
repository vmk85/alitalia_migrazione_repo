
package com.alitalia.aem.common.data.checkinrest.model.searchbypnr;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Destination {

    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("cityUrl")
    @Expose
    private String cityUrl;
    @SerializedName("cityCode")
    @Expose
    private String cityCode;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("stateCode")
    @Expose
    private String stateCode;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("marketAreaCode")
    @Expose
    private String marketAreaCode;
    @SerializedName("checkInEligibility")
    @Expose
    private Boolean checkInEligibility;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCityUrl() {
        return cityUrl;
    }

    public void setCityUrl(String cityUrl) {
        this.cityUrl = cityUrl;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMarketAreaCode() {
        return marketAreaCode;
    }

    public void setMarketAreaCode(String marketAreaCode) {
        this.marketAreaCode = marketAreaCode;
    }

    public Boolean getCheckInEligibility() {
        return checkInEligibility;
    }

    public void setCheckInEligibility(Boolean checkInEligibility) {
        this.checkInEligibility = checkInEligibility;
    }

}
