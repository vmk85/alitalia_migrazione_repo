package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.BaseRequest;

public class GetDatiSicurezzaProfiloRequestLocal extends BaseRequest {

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;

		GetDatiSicurezzaProfiloRequestLocal that = (GetDatiSicurezzaProfiloRequestLocal) o;

		return idProfilo != null ? idProfilo.equals(that.idProfilo) : that.idProfilo == null;
	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (idProfilo != null ? idProfilo.hashCode() : 0);
		return result;
	}

	private String  idProfilo;


	public String getIdProfilo() {
		return idProfilo;
	}

	public void setIdProfilo(String idProfilo) {
		this.idProfilo = idProfilo;
	}

	@Override
	public String toString() {
		return "GetDatiSicurezzaProfiloRequestLocal{" +
				"idProfilo='" + idProfilo + '\'' +
				'}';
	}
}
