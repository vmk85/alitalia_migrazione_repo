package com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AncillaryOffer {

    @SerializedName("fastTrack")
    @Expose
    private Boolean fastTrack;
    @SerializedName("lounge")
    @Expose
    private Boolean lounge;
    @SerializedName("bags")
    @Expose
    private Long bags;
    @SerializedName("fastTrackAllow")
    @Expose
    private Boolean fastTrackAllow;
    @SerializedName("lougeAllow")
    @Expose
    private Boolean lougeAllow;
    @SerializedName("bagsAllow")
    @Expose
    private Long bagsAllow;
    @SerializedName("fastTrackPrice")
    @Expose
    private Float fastTrackPrice;
    @SerializedName("loungePrice")
    @Expose
    private Float loungePrice;
    @SerializedName("firstBagPrice")
    @Expose
    private Float firstBagPrice;
    @SerializedName("secondBagPrice")
    @Expose
    private Float secondBagPrice;
    @SerializedName("moreBagsPrice")
    @Expose
    private Float moreBagsPrice;

    public Boolean getFastTrack() {
        return fastTrack;
    }

    public void setFastTrack(Boolean fastTrack) {
        this.fastTrack = fastTrack;
    }

    public Boolean getLounge() {
        return lounge;
    }

    public void setLounge(Boolean lounge) {
        this.lounge = lounge;
    }

    public Long getBags() {
        return bags;
    }

    public void setBags(Long bags) {
        this.bags = bags;
    }

    public Boolean getFastTrackAllow() {
        return fastTrackAllow;
    }

    public void setFastTrackAllow(Boolean fastTrackAllow) {
        this.fastTrackAllow = fastTrackAllow;
    }

    public Boolean getLougeAllow() {
        return lougeAllow;
    }

    public void setLougeAllow(Boolean lougeAllow) {
        this.lougeAllow = lougeAllow;
    }

    public Long getBagsAllow() {
        return bagsAllow;
    }

    public void setBagsAllow(Long bagsAllow) {
        this.bagsAllow = bagsAllow;
    }

    public Float getFastTrackPrice() {
        return fastTrackPrice;
    }

    public void setFastTrackPrice(Float fastTrackPrice) {
        this.fastTrackPrice = fastTrackPrice;
    }

    public Float getLoungePrice() {
        return loungePrice;
    }

    public void setLoungePrice(Float loungePrice) {
        this.loungePrice = loungePrice;
    }

    public Float getFirstBagPrice() {
        return firstBagPrice;
    }

    public void setFirstBagPrice(Float firstBagPrice) {
        this.firstBagPrice = firstBagPrice;
    }

    public Float getSecondBagPrice() {
        return secondBagPrice;
    }

    public void setSecondBagPrice(Float secondBagPrice) {
        this.secondBagPrice = secondBagPrice;
    }

    public Float getMoreBagsPrice() {
        return moreBagsPrice;
    }

    public void setMoreBagsPrice(Float moreBagsPrice) {
        this.moreBagsPrice = moreBagsPrice;
    }

}
