package com.alitalia.aem.common.data.home;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

import com.alitalia.aem.common.data.home.enumerations.DiscountTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.FareTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;

public class ECouponData {
	
	private BigDecimal amount;
	private String amountBus;
	private String amountEco;
	private String amountEcoPlus;
	private String anyPaxMAXNumPax;
	private String anyPaxMinNumPax;
	private String code;
	private Calendar datBlkFrm;
	private Calendar datBlkFrmDue;
	private Calendar datBlkFrmTre;
	private Calendar datBlkTo;
	private Calendar datBlkToDue;
	private Calendar datBlkToTre;
	private Calendar datUseFrm;
	private Calendar datUseTo;
	private Calendar datVolFrm;
	private Calendar datVolTo;
	private Integer errorCode;
	private String errorDescription;
	private String familyEcoupon;
	private FareTypeEnum fare;
	private Boolean isBrandLight;
	private Boolean singleUse;
	private Boolean valid;
	private String minAmount;
	private List<PassengerTypeEnum> paxTypes;
	private DiscountTypeEnum type;
	private String weekSalesDay;
	private String weekTravellDay;
	
	public BigDecimal getAmount() {
		return amount;
	}
	
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	public String getAmountBus() {
		return amountBus;
	}
	
	public void setAmountBus(String amountBus) {
		this.amountBus = amountBus;
	}
	
	public String getAmountEco() {
		return amountEco;
	}
	
	public void setAmountEco(String amountEco) {
		this.amountEco = amountEco;
	}
	
	public String getAmountEcoPlus() {
		return amountEcoPlus;
	}
	
	public void setAmountEcoPlus(String amountEcoPlus) {
		this.amountEcoPlus = amountEcoPlus;
	}
	
	public String getAnyPaxMAXNumPax() {
		return anyPaxMAXNumPax;
	}
	
	public void setAnyPaxMAXNumPax(String anyPaxMAXNumPax) {
		this.anyPaxMAXNumPax = anyPaxMAXNumPax;
	}
	
	public String getAnyPaxMinNumPax() {
		return anyPaxMinNumPax;
	}
	
	public void setAnyPaxMinNumPax(String anyPaxMinNumPax) {
		this.anyPaxMinNumPax = anyPaxMinNumPax;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public Calendar getDatBlkFrm() {
		return datBlkFrm;
	}
	
	public void setDatBlkFrm(Calendar datBlkFrm) {
		this.datBlkFrm = datBlkFrm;
	}
	
	public Calendar getDatBlkFrmDue() {
		return datBlkFrmDue;
	}
	
	public void setDatBlkFrmDue(Calendar datBlkFrmDue) {
		this.datBlkFrmDue = datBlkFrmDue;
	}
	
	public Calendar getDatBlkFrmTre() {
		return datBlkFrmTre;
	}
	
	public void setDatBlkFrmTre(Calendar datBlkFrmTre) {
		this.datBlkFrmTre = datBlkFrmTre;
	}
	
	public Calendar getDatBlkTo() {
		return datBlkTo;
	}
	
	public void setDatBlkTo(Calendar datBlkTo) {
		this.datBlkTo = datBlkTo;
	}
	
	public Calendar getDatBlkToDue() {
		return datBlkToDue;
	}
	
	public void setDatBlkToDue(Calendar datBlkToDue) {
		this.datBlkToDue = datBlkToDue;
	}
	
	public Calendar getDatBlkToTre() {
		return datBlkToTre;
	}
	
	public void setDatBlkToTre(Calendar datBlkToTre) {
		this.datBlkToTre = datBlkToTre;
	}
	
	public Calendar getDatUseFrm() {
		return datUseFrm;
	}
	
	public void setDatUseFrm(Calendar datUseFrm) {
		this.datUseFrm = datUseFrm;
	}
	
	public Calendar getDatUseTo() {
		return datUseTo;
	}
	
	public void setDatUseTo(Calendar datUseTo) {
		this.datUseTo = datUseTo;
	}
	
	public Calendar getDatVolFrm() {
		return datVolFrm;
	}
	
	public void setDatVolFrm(Calendar datVolFrm) {
		this.datVolFrm = datVolFrm;
	}
	
	public Calendar getDatVolTo() {
		return datVolTo;
	}
	
	public void setDatVolTo(Calendar datVolTo) {
		this.datVolTo = datVolTo;
	}
	
	public Integer getErrorCode() {
		return errorCode;
	}
	
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	
	public String getErrorDescription() {
		return errorDescription;
	}
	
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	
	public String getFamilyEcoupon() {
		return familyEcoupon;
	}
	
	public void setFamilyEcoupon(String familyEcoupon) {
		this.familyEcoupon = familyEcoupon;
	}
	
	public FareTypeEnum getFare() {
		return fare;
	}
	
	public void setFare(FareTypeEnum fare) {
		this.fare = fare;
	}
	
	public Boolean isSingleUse() {
		return singleUse;
	}
	
	public void setSingleUse(Boolean isSingleUse) {
		this.singleUse = isSingleUse;
	}
	
	public Boolean isValid() {
		return valid;
	}
	
	public void setValid(Boolean isValid) {
		this.valid = isValid;
	}
	
	public String getMinAmount() {
		return minAmount;
	}
	
	public void setMinAmount(String minAmount) {
		this.minAmount = minAmount;
	}
	
	public List<PassengerTypeEnum> getPaxTypes() {
		return paxTypes;
	}
	
	public void setPaxTypes(List<PassengerTypeEnum> paxTypes) {
		this.paxTypes = paxTypes;
	}
	
	public DiscountTypeEnum getType() {
		return type;
	}
	
	public void setType(DiscountTypeEnum type) {
		this.type = type;
	}
	
	public String getWeekSalesDay() {
		return weekSalesDay;
	}
	
	public void setWeekSalesDay(String weekSalesDay) {
		this.weekSalesDay = weekSalesDay;
	}
	
	public String getWeekTravellDay() {
		return weekTravellDay;
	}
	
	public void setWeekTravellDay(String weekTravellDay) {
		this.weekTravellDay = weekTravellDay;
	}
	
	public Boolean getIsBrandLight() {
		return isBrandLight;
	}

	public void setIsBrandLight(Boolean isBrandLight) {
		this.isBrandLight = isBrandLight;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result
				+ ((amountBus == null) ? 0 : amountBus.hashCode());
		result = prime * result
				+ ((amountEco == null) ? 0 : amountEco.hashCode());
		result = prime * result
				+ ((amountEcoPlus == null) ? 0 : amountEcoPlus.hashCode());
		result = prime * result
				+ ((anyPaxMAXNumPax == null) ? 0 : anyPaxMAXNumPax.hashCode());
		result = prime * result
				+ ((anyPaxMinNumPax == null) ? 0 : anyPaxMinNumPax.hashCode());
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result
				+ ((datBlkFrm == null) ? 0 : datBlkFrm.hashCode());
		result = prime * result
				+ ((datBlkFrmDue == null) ? 0 : datBlkFrmDue.hashCode());
		result = prime * result
				+ ((datBlkFrmTre == null) ? 0 : datBlkFrmTre.hashCode());
		result = prime * result
				+ ((datBlkTo == null) ? 0 : datBlkTo.hashCode());
		result = prime * result
				+ ((datBlkToDue == null) ? 0 : datBlkToDue.hashCode());
		result = prime * result
				+ ((datBlkToTre == null) ? 0 : datBlkToTre.hashCode());
		result = prime * result
				+ ((datUseFrm == null) ? 0 : datUseFrm.hashCode());
		result = prime * result
				+ ((datUseTo == null) ? 0 : datUseTo.hashCode());
		result = prime * result
				+ ((datVolFrm == null) ? 0 : datVolFrm.hashCode());
		result = prime * result
				+ ((datVolTo == null) ? 0 : datVolTo.hashCode());
		result = prime * result
				+ ((errorCode == null) ? 0 : errorCode.hashCode());
		result = prime
				* result
				+ ((errorDescription == null) ? 0 : errorDescription.hashCode());
		result = prime * result
				+ ((familyEcoupon == null) ? 0 : familyEcoupon.hashCode());
		result = prime * result + ((fare == null) ? 0 : fare.hashCode());
		result = prime * result
				+ ((isBrandLight == null) ? 0 : isBrandLight.hashCode());
		result = prime * result
				+ ((minAmount == null) ? 0 : minAmount.hashCode());
		result = prime * result
				+ ((paxTypes == null) ? 0 : paxTypes.hashCode());
		result = prime * result
				+ ((singleUse == null) ? 0 : singleUse.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((valid == null) ? 0 : valid.hashCode());
		result = prime * result
				+ ((weekSalesDay == null) ? 0 : weekSalesDay.hashCode());
		result = prime * result
				+ ((weekTravellDay == null) ? 0 : weekTravellDay.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ECouponData other = (ECouponData) obj;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (amountBus == null) {
			if (other.amountBus != null)
				return false;
		} else if (!amountBus.equals(other.amountBus))
			return false;
		if (amountEco == null) {
			if (other.amountEco != null)
				return false;
		} else if (!amountEco.equals(other.amountEco))
			return false;
		if (amountEcoPlus == null) {
			if (other.amountEcoPlus != null)
				return false;
		} else if (!amountEcoPlus.equals(other.amountEcoPlus))
			return false;
		if (anyPaxMAXNumPax == null) {
			if (other.anyPaxMAXNumPax != null)
				return false;
		} else if (!anyPaxMAXNumPax.equals(other.anyPaxMAXNumPax))
			return false;
		if (anyPaxMinNumPax == null) {
			if (other.anyPaxMinNumPax != null)
				return false;
		} else if (!anyPaxMinNumPax.equals(other.anyPaxMinNumPax))
			return false;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (datBlkFrm == null) {
			if (other.datBlkFrm != null)
				return false;
		} else if (!datBlkFrm.equals(other.datBlkFrm))
			return false;
		if (datBlkFrmDue == null) {
			if (other.datBlkFrmDue != null)
				return false;
		} else if (!datBlkFrmDue.equals(other.datBlkFrmDue))
			return false;
		if (datBlkFrmTre == null) {
			if (other.datBlkFrmTre != null)
				return false;
		} else if (!datBlkFrmTre.equals(other.datBlkFrmTre))
			return false;
		if (datBlkTo == null) {
			if (other.datBlkTo != null)
				return false;
		} else if (!datBlkTo.equals(other.datBlkTo))
			return false;
		if (datBlkToDue == null) {
			if (other.datBlkToDue != null)
				return false;
		} else if (!datBlkToDue.equals(other.datBlkToDue))
			return false;
		if (datBlkToTre == null) {
			if (other.datBlkToTre != null)
				return false;
		} else if (!datBlkToTre.equals(other.datBlkToTre))
			return false;
		if (datUseFrm == null) {
			if (other.datUseFrm != null)
				return false;
		} else if (!datUseFrm.equals(other.datUseFrm))
			return false;
		if (datUseTo == null) {
			if (other.datUseTo != null)
				return false;
		} else if (!datUseTo.equals(other.datUseTo))
			return false;
		if (datVolFrm == null) {
			if (other.datVolFrm != null)
				return false;
		} else if (!datVolFrm.equals(other.datVolFrm))
			return false;
		if (datVolTo == null) {
			if (other.datVolTo != null)
				return false;
		} else if (!datVolTo.equals(other.datVolTo))
			return false;
		if (errorCode == null) {
			if (other.errorCode != null)
				return false;
		} else if (!errorCode.equals(other.errorCode))
			return false;
		if (errorDescription == null) {
			if (other.errorDescription != null)
				return false;
		} else if (!errorDescription.equals(other.errorDescription))
			return false;
		if (familyEcoupon == null) {
			if (other.familyEcoupon != null)
				return false;
		} else if (!familyEcoupon.equals(other.familyEcoupon))
			return false;
		if (fare != other.fare)
			return false;
		if (isBrandLight == null) {
			if (other.isBrandLight != null)
				return false;
		} else if (!isBrandLight.equals(other.isBrandLight))
			return false;
		if (minAmount == null) {
			if (other.minAmount != null)
				return false;
		} else if (!minAmount.equals(other.minAmount))
			return false;
		if (paxTypes == null) {
			if (other.paxTypes != null)
				return false;
		} else if (!paxTypes.equals(other.paxTypes))
			return false;
		if (singleUse == null) {
			if (other.singleUse != null)
				return false;
		} else if (!singleUse.equals(other.singleUse))
			return false;
		if (type != other.type)
			return false;
		if (valid == null) {
			if (other.valid != null)
				return false;
		} else if (!valid.equals(other.valid))
			return false;
		if (weekSalesDay == null) {
			if (other.weekSalesDay != null)
				return false;
		} else if (!weekSalesDay.equals(other.weekSalesDay))
			return false;
		if (weekTravellDay == null) {
			if (other.weekTravellDay != null)
				return false;
		} else if (!weekTravellDay.equals(other.weekTravellDay))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ECouponData [amount=" + amount + ", amountBus=" + amountBus
				+ ", amountEco=" + amountEco + ", amountEcoPlus="
				+ amountEcoPlus + ", anyPaxMAXNumPax=" + anyPaxMAXNumPax
				+ ", anyPaxMinNumPax=" + anyPaxMinNumPax + ", code=" + code
				+ ", datBlkFrm=" + datBlkFrm + ", datBlkFrmDue=" + datBlkFrmDue
				+ ", datBlkFrmTre=" + datBlkFrmTre + ", datBlkTo=" + datBlkTo
				+ ", datBlkToDue=" + datBlkToDue + ", datBlkToTre="
				+ datBlkToTre + ", datUseFrm=" + datUseFrm + ", datUseTo="
				+ datUseTo + ", datVolFrm=" + datVolFrm + ", datVolTo="
				+ datVolTo + ", errorCode=" + errorCode + ", errorDescription="
				+ errorDescription + ", familyEcoupon=" + familyEcoupon
				+ ", fare=" + fare + ", isBrandLight=" + isBrandLight
				+ ", singleUse=" + singleUse + ", valid=" + valid
				+ ", minAmount=" + minAmount + ", paxTypes=" + paxTypes
				+ ", type=" + type + ", weekSalesDay=" + weekSalesDay
				+ ", weekTravellDay=" + weekTravellDay + "]";
	}
}