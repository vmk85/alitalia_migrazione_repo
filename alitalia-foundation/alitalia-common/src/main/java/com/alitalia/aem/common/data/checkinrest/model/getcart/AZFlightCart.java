
package com.alitalia.aem.common.data.checkinrest.model.getcart;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AZFlightCart {

    @SerializedName("boardPoint")
    @Expose
    private String boardPoint;
    @SerializedName("offPoint")
    @Expose
    private String offPoint;
    @SerializedName("classOfService")
    @Expose
    private String classOfService;
    @SerializedName("departureDate")
    @Expose
    private String departureDate;
    @SerializedName("arrivalDate")
    @Expose
    private String arrivalDate;
    @SerializedName("aZSegmentCart")
    @Expose
    private List<AZSegmentCart> aZSegmentCart = null;

    public String getBoardPoint() {
        return boardPoint;
    }

    public void setBoardPoint(String boardPoint) {
        this.boardPoint = boardPoint;
    }

    public String getOffPoint() {
        return offPoint;
    }

    public void setOffPoint(String offPoint) {
        this.offPoint = offPoint;
    }

    public String getClassOfService() {
        return classOfService;
    }

    public void setClassOfService(String classOfService) {
        this.classOfService = classOfService;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public List<AZSegmentCart> getAZSegmentCart() {
        return aZSegmentCart;
    }

    public void setAZSegmentCart(List<AZSegmentCart> aZSegmentCart) {
        this.aZSegmentCart = aZSegmentCart;
    }

}
