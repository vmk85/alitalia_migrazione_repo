package com.alitalia.aem.common.messages.home;

public class RegisterPaymentFromAuthToTicketStatisticRequest extends RegisterStatisticsRequest {

    private String backofficeID;
    private Long backofficeTimeMillis;
    private String cartID;
    
	public RegisterPaymentFromAuthToTicketStatisticRequest(String tid,
			String sid) {
		super(tid,sid);
	}
	public String getBackofficeID() {
		return backofficeID;
	}
	public void setBackofficeID(String backofficeID) {
		this.backofficeID = backofficeID;
	}
	public Long getBackofficeTimeMillis() {
		return backofficeTimeMillis;
	}
	public void setBackofficeTimeMillis(Long backofficeTimeMillis) {
		this.backofficeTimeMillis = backofficeTimeMillis;
	}
	public String getCartID() {
		return cartID;
	}
	public void setCartID(String cartID) {
		this.cartID = cartID;
	}
	
	@Override
	public String toString() {
		return "RegisterPaymentFromAuthToTicketStatisticRequest [backofficeID="
				+ backofficeID + ", backofficeTimeMillis="
				+ backofficeTimeMillis + ", cartID=" + cartID
				+ ", getBackofficeID()=" + getBackofficeID()
				+ ", getBackofficeTimeMillis()=" + getBackofficeTimeMillis()
				+ ", getCartID()=" + getCartID() + ", getClientIP()="
				+ getClientIP() + ", getErrorDescr()=" + getErrorDescr()
				+ ", getSessionId()=" + getSessionId() + ", getSiteCode()="
				+ getSiteCode() + ", isSuccess()=" + isSuccess()
				+ ", getType()=" + getType() + ", getUserId()=" + getUserId()
				+ ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + ", getTid()=" + getTid() + ", getSid()="
				+ getSid() + ", getClass()=" + getClass() + "]";
	}
}