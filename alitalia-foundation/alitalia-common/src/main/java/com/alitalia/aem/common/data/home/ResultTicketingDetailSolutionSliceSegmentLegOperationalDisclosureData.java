package com.alitalia.aem.common.data.home;

public class ResultTicketingDetailSolutionSliceSegmentLegOperationalDisclosureData {

    private String carrierField;
    private String disclosureTextField;

    public String getCarrierField() {
        return carrierField;
    }

    public void setCarrierField(String value) {
        this.carrierField = value;
    }

    public String getDisclosureTextField() {
        return disclosureTextField;
    }

    public void setDisclosureTextField(String value) {
        this.disclosureTextField = value;
    }

}
