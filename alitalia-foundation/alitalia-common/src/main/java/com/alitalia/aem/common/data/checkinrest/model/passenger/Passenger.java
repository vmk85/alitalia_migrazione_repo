package com.alitalia.aem.common.data.checkinrest.model.passenger;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Passenger {
	
	@SerializedName("freeTextInfoList")
	@Expose
	private List<FreeTextInfoListPassenger> freeTextInfoList = null;
	
	@SerializedName("itineraryPassengerList")
	@Expose
	private List<ItineraryPassengerList> itineraryPassengerList = null;
	
	@SerializedName("flightSeatMapInfoList")
	@Expose
	private List<FlightSeatMapInfoList> flightSeatMapInfoList = null;
	
	@SerializedName("pectabDataList")
	@Expose
	private List<PectabDataList> pectabDataList = null;
	
	@SerializedName("result")
	@Expose
	private ResultPassenger result;

	public List<FreeTextInfoListPassenger> getFreeTextInfoList() {
		return freeTextInfoList;
	}

	public void setFreeTextInfoList(List<FreeTextInfoListPassenger> freeTextInfoList) {
		this.freeTextInfoList = freeTextInfoList;
	}

	public List<ItineraryPassengerList> getItineraryPassengerList() {
		return itineraryPassengerList;
	}

	public void setItineraryPassengerList(List<ItineraryPassengerList> itineraryPassengerList) {
		this.itineraryPassengerList = itineraryPassengerList;
	}

	public List<FlightSeatMapInfoList> getFlightSeatMapInfoList() {
		return flightSeatMapInfoList;
	}

	public void setFlightSeatMapInfoList(List<FlightSeatMapInfoList> flightSeatMapInfoList) {
		this.flightSeatMapInfoList = flightSeatMapInfoList;
	}

	public List<PectabDataList> getPectabDataList() {
		return pectabDataList;
	}

	public void setPectabDataList(List<PectabDataList> pectabDataList) {
		this.pectabDataList = pectabDataList;
	}

	public ResultPassenger getResult() {
		return result;
	}

	public void setResult(ResultPassenger result) {
		this.result = result;
	}


}
