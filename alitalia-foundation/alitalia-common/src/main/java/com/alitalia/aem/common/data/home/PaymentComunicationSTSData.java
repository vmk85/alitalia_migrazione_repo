package com.alitalia.aem.common.data.home;


public class PaymentComunicationSTSData extends PaymentComunicationBaseData {
	
    private String acceptHeader;
    private String ipAddress;
    private Boolean shippingEnabled;
    private String userAgent;
    
	public String getAcceptHeader() {
		return acceptHeader;
	}
	public void setAcceptHeader(String acceptHeader) {
		this.acceptHeader = acceptHeader;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public Boolean getShippingEnabled() {
		return shippingEnabled;
	}
	public void setShippingEnabled(Boolean shippingEnabled) {
		this.shippingEnabled = shippingEnabled;
	}
	public String getUserAgent() {
		return userAgent;
	}
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((acceptHeader == null) ? 0 : acceptHeader.hashCode());
		result = prime * result
				+ ((ipAddress == null) ? 0 : ipAddress.hashCode());
		result = prime * result
				+ ((shippingEnabled == null) ? 0 : shippingEnabled.hashCode());
		result = prime * result
				+ ((userAgent == null) ? 0 : userAgent.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentComunicationSTSData other = (PaymentComunicationSTSData) obj;
		if (acceptHeader == null) {
			if (other.acceptHeader != null)
				return false;
		} else if (!acceptHeader.equals(other.acceptHeader))
			return false;
		if (ipAddress == null) {
			if (other.ipAddress != null)
				return false;
		} else if (!ipAddress.equals(other.ipAddress))
			return false;
		if (shippingEnabled == null) {
			if (other.shippingEnabled != null)
				return false;
		} else if (!shippingEnabled.equals(other.shippingEnabled))
			return false;
		if (userAgent == null) {
			if (other.userAgent != null)
				return false;
		} else if (!userAgent.equals(other.userAgent))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "PaymentComunicationSTSData [acceptHeader=" + acceptHeader
				+ ", ipAddress=" + ipAddress + ", shippingEnabled="
				+ shippingEnabled + ", userAgent=" + userAgent
				+ ", getDescription()=" + getDescription()
				+ ", getLanguageCode()=" + getLanguageCode()
				+ ", getReturnUrl()=" + getReturnUrl() + "]";
	}
}