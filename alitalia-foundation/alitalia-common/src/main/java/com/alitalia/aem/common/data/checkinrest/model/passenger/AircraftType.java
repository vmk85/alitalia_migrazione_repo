package com.alitalia.aem.common.data.checkinrest.model.passenger;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AircraftType {
	
	@SerializedName("changeOfGauge")
	@Expose
	private Boolean changeOfGauge;
	
	@SerializedName("changeOfGaugeSpecified")
	@Expose
	private Boolean changeOfGaugeSpecified;
	
	@SerializedName("value")
	@Expose
	private String value;

	public Boolean getChangeOfGauge() {
		return changeOfGauge;
	}

	public void setChangeOfGauge(Boolean changeOfGauge) {
		this.changeOfGauge = changeOfGauge;
	}

	public Boolean getChangeOfGaugeSpecified() {
		return changeOfGaugeSpecified;
	}

	public void setChangeOfGaugeSpecified(Boolean changeOfGaugeSpecified) {
		this.changeOfGaugeSpecified = changeOfGaugeSpecified;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
