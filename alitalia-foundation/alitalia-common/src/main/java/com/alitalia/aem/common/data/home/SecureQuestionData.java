package com.alitalia.aem.common.data.home;

public class SecureQuestionData {

	private int SecureQuestionID;
	private String SecureQuestionCode;
	private String SecureQuestionDescription;
	
	public String getSecureQuestionCode() {
		return SecureQuestionCode;
	}
	
	public void setSecureQuestionCode(String secureQuestionCode) {
		this.SecureQuestionCode = secureQuestionCode;
	}
	
	public String getSecureQuestionDescription() {
		return SecureQuestionDescription;
	}
	
	public void setSecureQuestionDescription(String stateDescription) {this.SecureQuestionDescription = stateDescription;	}

	public int getSecureQuestionID() {
		return SecureQuestionID;
	}

	public void setSecureQuestionID(int id) {this.SecureQuestionID = id;	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((SecureQuestionCode == null) ? 0 : SecureQuestionCode.hashCode());
		result = prime
				* result
				+ ((SecureQuestionDescription == null) ? 0 : SecureQuestionDescription.hashCode());
		result = prime
				* result
				+ ((SecureQuestionID <0) ? 0 : SecureQuestionID);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SecureQuestionData other = (SecureQuestionData) obj;
		if (SecureQuestionCode == null) {
			if (other.SecureQuestionCode != null)
				return false;
		} else if (!SecureQuestionCode.equals(other.SecureQuestionCode))
			return false;
		if (SecureQuestionDescription == null) {
			if (other.SecureQuestionDescription != null)
				return false;
		} else if (!SecureQuestionDescription.equals(other.SecureQuestionDescription)) {
			return false;
		}else if (SecureQuestionID!=other.SecureQuestionID) {
			return false;
		}

		 return true;
	}

	@Override
	public String toString() {
		return "SecureQuestion [SecureQuestionCode=" + SecureQuestionCode
				+ ", SecureQuestionDescription=" + SecureQuestionDescription
				+ ", SecureQuestionID=" + SecureQuestionID
				+ "]";
	}
}