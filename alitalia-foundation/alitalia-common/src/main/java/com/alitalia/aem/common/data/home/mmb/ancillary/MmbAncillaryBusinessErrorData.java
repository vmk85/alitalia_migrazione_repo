package com.alitalia.aem.common.data.home.mmb.ancillary;

import com.alitalia.aem.common.data.MmbAncillaryErrorData;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryShoppingCartServiceErrorEnum;

public class MmbAncillaryBusinessErrorData extends MmbAncillaryErrorData {

	public MmbAncillaryBusinessErrorData() {
		super();
		this.type = MmbAncillaryShoppingCartServiceErrorEnum.BUSINESS_ERROR;	}

}
