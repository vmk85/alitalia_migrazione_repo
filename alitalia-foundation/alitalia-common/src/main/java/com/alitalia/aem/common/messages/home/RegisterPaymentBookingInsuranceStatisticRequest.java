package com.alitalia.aem.common.messages.home;


public class RegisterPaymentBookingInsuranceStatisticRequest extends RegisterStatisticsRequest {

	private String cardType;
    private String policyNumber;
    private String typeOfTrip;
    
    public RegisterPaymentBookingInsuranceStatisticRequest(String tid,
			String sid) {
		super(tid, sid);
	}
    
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	public String getTypeOfTrip() {
		return typeOfTrip;
	}
	public void setTypeOfTrip(String typeOfTrip) {
		this.typeOfTrip = typeOfTrip;
	}
	
	@Override
	public String toString() {
		return "RegisterPaymentBookingEtktStatisticRequest [cardType="
				+ cardType + ", policyNumber=" + policyNumber + ", typeOfTrip="
				+ typeOfTrip + ", getCardType()=" + getCardType()
				+ ", getPolicyNumber()=" + getPolicyNumber()
				+ ", getTypeOfTrip()=" + getTypeOfTrip() + ", getClientIP()="
				+ getClientIP() + ", getErrorDescr()=" + getErrorDescr()
				+ ", getSessionId()=" + getSessionId() + ", getSiteCode()="
				+ getSiteCode() + ", isSuccess()=" + isSuccess()
				+ ", getType()=" + getType() + ", getUserId()=" + getUserId()
				+ ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + ", getTid()=" + getTid() + ", getSid()="
				+ getSid() + ", getClass()=" + getClass() + "]";
	}
}