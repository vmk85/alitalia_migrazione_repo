package com.alitalia.aem.common.data.home.enumerations;

public enum MMSystemChannelTypeEnum {

    UNKNOWN("UnKnown"),
    INTERNET("Internet"),
    CRM("CRM");

    private final String value;
    
    MMSystemChannelTypeEnum(String v) {
		value = v;
	}
	
	public String value() {
		return value;
	}

	public static MMSystemChannelTypeEnum fromValue(String v) {
		for (MMSystemChannelTypeEnum c: MMSystemChannelTypeEnum.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
