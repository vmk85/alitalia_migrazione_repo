package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.checkin.CheckinBaseTransactionInfoData;
import com.alitalia.aem.common.messages.MmbBaseRequest;


public class PaymentCheckinRequest extends MmbBaseRequest {

	private CheckinBaseTransactionInfoData tInfo;

	public CheckinBaseTransactionInfoData getTInfo() {
		return tInfo;
	}

	public void setTInfo(CheckinBaseTransactionInfoData tInfo) {
		this.tInfo = tInfo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((tInfo == null) ? 0 : tInfo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentCheckinRequest other = (PaymentCheckinRequest) obj;
		if (tInfo == null) {
			if (other.tInfo != null)
				return false;
		} else if (!tInfo.equals(other.tInfo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PaymentCheckinRequest [tInfo=" + tInfo + ", toString()="
				+ super.toString() + "]";
	}

}
