package com.alitalia.aem.common.data.home;

public class RegisterCreditCardResult {
	String token;
	
	public RegisterCreditCardResult(String token) {
		super();
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
}