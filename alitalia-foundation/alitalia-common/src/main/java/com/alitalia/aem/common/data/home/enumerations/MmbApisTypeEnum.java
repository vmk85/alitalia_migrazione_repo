package com.alitalia.aem.common.data.home.enumerations;

public enum MmbApisTypeEnum {

	NONE("None"),
	BASE("Base"),
	PLUS("Plus");
    private final String value;

    MmbApisTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MmbApisTypeEnum fromValue(String v) {
        for (MmbApisTypeEnum c: MmbApisTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
