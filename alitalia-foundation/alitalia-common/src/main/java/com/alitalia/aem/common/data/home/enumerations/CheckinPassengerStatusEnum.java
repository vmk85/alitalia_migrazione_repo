package com.alitalia.aem.common.data.home.enumerations;

public enum CheckinPassengerStatusEnum {

	INVALID("Invalid"),
    REGULAR("Regular"),
    ALREADY_CHECKED_IN("AlreadyCheckedIn"),
    NOT_CHECKED_IN_PREVIOUS_FLIGHT("NotCheckedInPreviousFlight"),
    INFANT("Infant"),
    INFANT_ACCOMPANIST("InfantAccompanist"),
    SPECIAL_PASSENGER("SpecialPassenger"),
    CHECKED_IN("CheckedIn"),
    NOT_CHECKED_IN("NotCheckedIn");
    
    private final String value;

    CheckinPassengerStatusEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CheckinPassengerStatusEnum fromValue(String v) {
        for (CheckinPassengerStatusEnum c: CheckinPassengerStatusEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
