package com.alitalia.aem.common.data.home.enumerations;

public enum FFCardStatusEnum {

    DISABLE("Disable"),
    OK("Ok");
    private final String value;

    FFCardStatusEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FFCardStatusEnum fromValue(String v) {
        for (FFCardStatusEnum c: FFCardStatusEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
