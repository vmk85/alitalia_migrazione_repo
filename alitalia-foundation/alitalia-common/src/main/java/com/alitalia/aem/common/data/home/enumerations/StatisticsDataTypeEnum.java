package com.alitalia.aem.common.data.home.enumerations;

public enum StatisticsDataTypeEnum {

	AVAIL("Avail"),
	ECOUPON("Ecoupon"),
	MILLE_MIGLIA("MilleMiglia"),
	PNR("PNR"),
	PAYMENT_CHECKOUT("PaymentCheckout"),
	PAYMENT_AUTHORIZATION_ATTEMPT("PaymentAuthorizationAttempt"),
	PAYMENT_AUTHORIZATION_ATTEMPT_BANCO_POSTA("PaymentAuthorizationAttemptBancoPosta"),
	PAYMENT_AUTHORIZATION_ATTEMPT_UNICREDIT("PaymentAuthorizationAttemptUnicredit"),
	PAYMENT_AUTHORIZATION_ATTEMPT_PAY_PAL("PaymentAuthorizationAttemptPayPal"),
	PAYMENT_AUTHORIZATION_PAY_PAL("PaymentAuthorizationPayPal"),
	PAYMENT_FROM_AUTHORIZATION_TO_TICKET("PaymentFromAuthorizationToTicket"),
	PAYMENT_BOOKING_ETKT("PaymentBookingEtkt"),
	PAYMENT_BOOKING_INSURANCE("PaymentBookingInsurance"),
	PAYMENT_AUTHORIZATION_ATTEMPT_LOTTOMATICA("PaymentAuthorizationAttemptLottomatica"),
	PAYMENT_AUTHORIZATION_LOTTOMATICA("PaymentAuthorizationLottomatica"),
	PAYMENT_AUTHORIZATION_ATTEMPT_FINDOMESTIC("PaymentAuthorizationAttemptFindomestic"),
	PAYMENT_AUTHORIZATION_FINDOMESTIC("PaymentAuthorizationFindomestic"),
	PAYMENT_AUTHORIZATION_ATTEMPT_INSTALMENTS_BRAZIL("PaymentAuthorizationAttemptInstalmentsBrazil"),
	PAYMENT_AUTHORIZATION_INSTALMENTS_BRAZIL("PaymentAuthorizationInstalmentsBrazil"),
	PAYMENT_AUTHORIZATION_ATTEMPT_PAY_AT_TO("PaymentAuthorizationAttemptPayAtTo"),
	PAYMENT_AUTHORIZATION_PAY_AT_TO("PaymentAuthorizationPayAtTo"),
	BILLING_REQUEST("BillingRequest");

	private final String value;

	private StatisticsDataTypeEnum(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	public static StatisticsDataTypeEnum fromValue(String v) {
		for (StatisticsDataTypeEnum c: StatisticsDataTypeEnum.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}