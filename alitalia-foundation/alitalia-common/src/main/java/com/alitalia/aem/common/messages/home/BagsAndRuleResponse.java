package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.messages.BaseResponse;

public class BagsAndRuleResponse extends BaseResponse {

	private List<BrandData> brands;

	public List<BrandData> getBrands() {
		return brands;
	}

	public void setBrands(List<BrandData> brands) {
		this.brands = brands;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((brands == null) ? 0 : brands.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		BagsAndRuleResponse other = (BagsAndRuleResponse) obj;
		if (brands == null) {
			if (other.brands != null)
				return false;
		} else if (!brands.equals(other.brands))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BagsAndRuleResponse [brands=" + brands + ", getCookie()="
				+ getCookie() + ", getExecute()=" + getExecute() + "]";
	}
}