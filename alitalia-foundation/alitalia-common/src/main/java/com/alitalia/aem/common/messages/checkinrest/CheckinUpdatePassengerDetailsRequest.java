package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.updatepassengerdetails.request.UpdatePassengerDetailsReq;
import com.alitalia.aem.common.messages.BaseRequest;

public class CheckinUpdatePassengerDetailsRequest extends BaseRequest{
	
	private UpdatePassengerDetailsReq _updatepassengerdetailsReq;

	public UpdatePassengerDetailsReq get_updatepassengerdetailsReq() {
		return _updatepassengerdetailsReq;
	}

	public void set_updatepassengerdetailsReq(UpdatePassengerDetailsReq _updatepassengerdetailsReq) {
		this._updatepassengerdetailsReq = _updatepassengerdetailsReq;
	}
	
	public CheckinUpdatePassengerDetailsRequest(String tid, String sid) {
		super(tid, sid);
	}


}
