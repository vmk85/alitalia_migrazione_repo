package com.alitalia.aem.common.data.home;

import java.util.List;

public class ResultTicketingDetailPagesData {

    protected Integer countField;
    protected Integer currentField;
    protected List<Object> pageField;

    public Integer getCountField() {
        return countField;
    }

    public void setCountField(Integer value) {
        this.countField = value;
    }

    public Integer getCurrentField() {
        return currentField;
    }

    public void setCurrentField(Integer value) {
        this.currentField = value;
    }

    public List<Object> getPageField() {
        return pageField;
    }

    public void setPageField(List<Object> value) {
        this.pageField = value;
    }

}
