package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class CarnetRecoveryCodeRequest extends BaseRequest {

	private String carnetCode;
    private String email;
    private String languageCode;
    private String marketCode;

	public CarnetRecoveryCodeRequest(String tid, String sid) {
		super(tid, sid);
	}

	public CarnetRecoveryCodeRequest() {
		super();
	}

	public String getCarnetCode() {
		return carnetCode;
	}

	public void setCarnetCode(String carnetCode) {
		this.carnetCode = carnetCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getMarketCode() {
		return marketCode;
	}

	public void setMarketCode(String marketCode) {
		this.marketCode = marketCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((carnetCode == null) ? 0 : carnetCode.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result
				+ ((languageCode == null) ? 0 : languageCode.hashCode());
		result = prime * result
				+ ((marketCode == null) ? 0 : marketCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarnetRecoveryCodeRequest other = (CarnetRecoveryCodeRequest) obj;
		if (carnetCode == null) {
			if (other.carnetCode != null)
				return false;
		} else if (!carnetCode.equals(other.carnetCode))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (languageCode == null) {
			if (other.languageCode != null)
				return false;
		} else if (!languageCode.equals(other.languageCode))
			return false;
		if (marketCode == null) {
			if (other.marketCode != null)
				return false;
		} else if (!marketCode.equals(other.marketCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CarnetRecoveryCodeRequest [carnetCode=" + carnetCode
				+ ", email=" + email + ", languageCode=" + languageCode
				+ ", marketCode=" + marketCode + "]";
	}
}