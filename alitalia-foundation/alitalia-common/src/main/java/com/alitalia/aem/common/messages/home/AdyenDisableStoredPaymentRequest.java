package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.AdyenBaseRequest;

public class AdyenDisableStoredPaymentRequest  extends AdyenBaseRequest {
	
	/*
	{
	  "shopperReference": "Raniero",
	  "recurringDetailReference": "8315333089093719",
	  "contract": "ONECLICK",
	  "language": "IT",
	  "market": "IT",
	  "conversationID": "1234",
	  "caller": "WebCheckin"
	}
	*/

	private String recurringDetailReference;
	private String contract = "ONECLICK";//ONECLICK o RECURRING o ONECLICK, RECURRING

	public String getRecurringDetailReference() {
		return recurringDetailReference;
	}

	public void setRecurringDetailReference(String recurringDetailReference) {
		this.recurringDetailReference = recurringDetailReference;
	}

	public String getContract() {
		return contract;
	}

	public void setContract(String contract) {
		this.contract = contract;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result	+ ((recurringDetailReference == null) ? 0 : recurringDetailReference.hashCode());
		result = prime * result	+ ((contract == null) ? 0 : contract.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdyenDisableStoredPaymentRequest other = (AdyenDisableStoredPaymentRequest) obj;
		if (recurringDetailReference == null) {
			if (other.recurringDetailReference != null)
				return false;
		} else if (!recurringDetailReference.equals(other.recurringDetailReference))
			return false;
		if (contract == null) {
			if (other.contract != null)
				return false;
		} else if (!contract.equals(other.contract))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "AdyenRetrievalStoredPaymentRequest ["
				+ "shopperReference=" + super.getShopperReference()
				+ "recurringDetailReference=" + recurringDetailReference
				+ "contract=" + contract
				+ "language=" + super.getLanguage()
				+ "market=" + super.getMarket()
				+ "conversationID=" + super.getConversationID()
				+ "caller=" + super.getCaller()
				+ "]";
	}
	
}