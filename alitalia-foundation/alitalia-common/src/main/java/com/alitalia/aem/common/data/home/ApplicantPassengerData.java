package com.alitalia.aem.common.data.home;

import java.util.ArrayList;
import java.util.List;

import com.alitalia.aem.common.data.home.enumerations.ARTaxInfoTypesEnum;

public class ApplicantPassengerData extends AdultPassengerData {

	private ARTaxInfoTypesEnum arTaxInfoType;
	private String blueBizCode;
	private String cuit;
	private List<ContactData> contact;
	private String country;
	private String email;
	private String skyBonusCode;
	private Boolean subscribeToNewsletter;

	public ApplicantPassengerData() {
		super();
	}

	public ApplicantPassengerData(ApplicantPassengerData clone) {
		super();
		this.arTaxInfoType = clone.getArTaxInfoType();
		this.blueBizCode = clone.getBlueBizCode();
		this.country = clone.getCountry();
		this.cuit = clone.getCuit();
		this.email = clone.getEmail();
		this.skyBonusCode = clone.getSkyBonusCode();
		this.subscribeToNewsletter = clone.isSubscribeToNewsletter();

		ArrayList<ContactData> contactList = new ArrayList<ContactData>();
		for (ContactData sourceContact : clone.getContact()) {
			ContactData contactElem = new ContactData(sourceContact);
			contactList.add(contactElem);
		}
		this.contact = contactList;
	}

	public ApplicantPassengerData(AdultPassengerData clone) {
		super(clone);
		this.arTaxInfoType = null;
		this.blueBizCode = null;
		this.contact = null;
		this.country = null;
		this.cuit = null;
		this.email = null;
		this.skyBonusCode = null;
		this.subscribeToNewsletter = false;
	}

	public ApplicantPassengerData(PassengerBaseData clone) {
		super(clone);
		this.arTaxInfoType = null;
		this.blueBizCode = null;
		this.contact = null;
		this.country = null;
		this.cuit = null;
		this.email = null;
		this.skyBonusCode = null;
		this.subscribeToNewsletter = false;
	}

	public ARTaxInfoTypesEnum getArTaxInfoType() {
		return arTaxInfoType;
	}
	
	public void setArTaxInfoType(ARTaxInfoTypesEnum arTaxInfoType) {
		this.arTaxInfoType = arTaxInfoType;
	}
	
	public String getBlueBizCode() {
		return blueBizCode;
	}
	
	public void setBlueBizCode(String blueBizCode) {
		this.blueBizCode = blueBizCode;
	}
	
	public String getCuit() {
		return cuit;
	}
	
	public void setCuit(String cuit) {
		this.cuit = cuit;
	}
	
	public List<ContactData> getContact() {
		return contact;
	}
	
	public void setContact(List<ContactData> contact) {
		this.contact = contact;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getSkyBonusCode() {
		return skyBonusCode;
	}
	
	public void setSkyBonusCode(String skyBonusCode) {
		this.skyBonusCode = skyBonusCode;
	}
	
	public Boolean isSubscribeToNewsletter() {
		return subscribeToNewsletter;
	}
	
	public void setSubscribeToNewsletter(Boolean subscribeToNewsletter) {
		this.subscribeToNewsletter = subscribeToNewsletter;
	}

	@Override
	public String toString() {
		return "ApplicantPassengerData [arTaxInfoType=" + arTaxInfoType
				+ ", blueBizCode=" + blueBizCode + ", cuit=" + cuit
				+ ", contact=" + contact + ", country=" + country + ", email="
				+ email + ", skyBonusCode=" + skyBonusCode
				+ ", subscribeToNewsletter=" + subscribeToNewsletter + "]";
	}
}
