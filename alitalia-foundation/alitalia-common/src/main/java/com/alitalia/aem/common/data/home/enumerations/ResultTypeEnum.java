package com.alitalia.aem.common.data.home.enumerations;

public enum ResultTypeEnum {

    OK,
    KO;

    public String value() {
        return name();
    }

    public static ResultTypeEnum fromValue(String v) {
        return valueOf(v);
    }

}
