package com.alitalia.aem.common.data.home;

public class MMTicketNumberType {
	
    private String airlineCode;
    private String ticketNumberId;
    
	public String getAirlineCode() {
		return airlineCode;
	}
	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}
	public String getTicketNumberId() {
		return ticketNumberId;
	}
	public void setTicketNumberId(String ticketNumberId) {
		this.ticketNumberId = ticketNumberId;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((airlineCode == null) ? 0 : airlineCode.hashCode());
		result = prime * result
				+ ((ticketNumberId == null) ? 0 : ticketNumberId.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MMTicketNumberType other = (MMTicketNumberType) obj;
		if (airlineCode == null) {
			if (other.airlineCode != null)
				return false;
		} else if (!airlineCode.equals(other.airlineCode))
			return false;
		if (ticketNumberId == null) {
			if (other.ticketNumberId != null)
				return false;
		} else if (!ticketNumberId.equals(other.ticketNumberId))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "MMTicketNumberType [airlineCode=" + airlineCode
				+ ", ticketNumberId=" + ticketNumberId + "]";
	}
}