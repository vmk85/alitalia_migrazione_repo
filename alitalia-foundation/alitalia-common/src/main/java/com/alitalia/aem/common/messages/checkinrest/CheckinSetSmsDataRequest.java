package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.boardingpass.BoardingPassSms;
import com.alitalia.aem.common.messages.BaseRequest;

public class CheckinSetSmsDataRequest extends BaseRequest
{
    private BoardingPassSms boardingPassData;
    private String language;
    private String market;
    private String conversationID;

	public CheckinSetSmsDataRequest() {}

	public CheckinSetSmsDataRequest(String tid, String sid) {
        super(tid, sid);
    }

    public BoardingPassSms getBoardingPassData() {
        return boardingPassData;
    }

    public void setBoardingPassData(BoardingPassSms boardingPassData) {
        this.boardingPassData = boardingPassData;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }
}
