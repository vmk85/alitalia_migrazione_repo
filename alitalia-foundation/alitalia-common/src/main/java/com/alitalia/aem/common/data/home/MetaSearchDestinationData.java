package com.alitalia.aem.common.data.home;

import java.util.Calendar;

import com.alitalia.aem.common.data.home.enumerations.CabinEnum;


public class MetaSearchDestinationData extends SearchDestinationData {

	private String code;
	private Integer flight;
	private Integer slices;
	private Calendar arrivalDate;
	private String bookingClass;
	private CabinEnum cabinClass;

	public MetaSearchDestinationData() {
		super();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getFlight() {
		return flight;
	}

	public void setFlight(Integer flight) {
		this.flight = flight;
	}

	public Integer getSlices() {
		return slices;
	}

	public void setSlices(Integer slices) {
		this.slices = slices;
	}

	public Calendar getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Calendar arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	public CabinEnum getCabinClass() {
		return cabinClass;
	}

	public void setCabinClass(CabinEnum cabinClass) {
		this.cabinClass = cabinClass;
	}

	@Override
	public String toString() {
		return "MetaSearchDestinationData [bookingClass=" + bookingClass + ", cabinClass="
				+ cabinClass + "code=" + code + ", flight=" + flight
				+ ", slices=" + slices + ", arrivalDate=" + arrivalDate
				+ ", getCode()=" + getCode() + ", getFlight()=" + getFlight()
				+ ", getSlices()=" + getSlices() + ", getArrivalDate()="
				+ getArrivalDate() + ", getFromAirport()=" + getFromAirport()
				+ ", getToAirport()=" + getToAirport() + ", getRouteType()="
				+ getRouteType() + ", getTimeType()=" + getTimeType()
				+ ", getDepartureDate()=" + getDepartureDate()
				+ ", getSelectedOffer()=" + getSelectedOffer()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}
}