package com.alitalia.aem.common.data.home;

public class BillingData {
	private boolean bill;
	private String CFPIVA;
	private String cap;
	private String cfInt;
	private String cfPax;
	private String emailInt;
	private String enteEmit;
	private String enteRich; 
	private String formaSocietaria;
	private String indSped;
	private String intFattura;
	private String locSped;
	private String name;
	private String nome;
	private String paese;
	private String pivaInt;
	private String prov;
	private String selectedCountry;
	private String societa;
	private String surname;
	private String tipoRichiesta;
	
	public boolean isBill() {
		return bill;
	}

	public void setBill(boolean bill) {
		this.bill = bill;
	}
	
	public String getCFPIVA() {
		return CFPIVA;
	}
	
	public void setCFPIVA(String cFPIVA) {
		CFPIVA = cFPIVA;
	}
	
	public String getCap() {
		return cap;
	}
	
	public void setCap(String cap) {
		this.cap = cap;
	}
	
	public String getCfInt() {
		return cfInt;
	}
	
	public void setCfInt(String cfInt) {
		this.cfInt = cfInt;
	}
	
	public String getCfPax() {
		return cfPax;
	}
	
	public void setCfPax(String cfPax) {
		this.cfPax = cfPax;
	}
	
	public String getEmailInt() {
		return emailInt;
	}
	
	public void setEmailInt(String emailInt) {
		this.emailInt = emailInt;
	}
	
	public String getEnteEmit() {
		return enteEmit;
	}
	
	public void setEnteEmit(String enteEmit) {
		this.enteEmit = enteEmit;
	}
	
	public String getEnteRich() {
		return enteRich;
	}
	
	public void setEnteRich(String enteRich) {
		this.enteRich = enteRich;
	}
	
	public String getFormaSocietaria() {
		return formaSocietaria;
	}
	
	public void setFormaSocietaria(String formaSocietaria) {
		this.formaSocietaria = formaSocietaria;
	}
	
	public String getIndSped() {
		return indSped;
	}
	
	public void setIndSped(String indSped) {
		this.indSped = indSped;
	}
	
	public String getIntFattura() {
		return intFattura;
	}
	
	public void setIntFattura(String intFattura) {
		this.intFattura = intFattura;
	}
	
	public String getLocSped() {
		return locSped;
	}
	
	public void setLocSped(String locSped) {
		this.locSped = locSped;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getPaese() {
		return paese;
	}
	
	public void setPaese(String paese) {
		this.paese = paese;
	}
	
	public String getPivaInt() {
		return pivaInt;
	}
	
	public void setPivaInt(String pivaInt) {
		this.pivaInt = pivaInt;
	}
	
	public String getProv() {
		return prov;
	}
	
	public void setProv(String prov) {
		this.prov = prov;
	}
	
	public String getSelectedCountry() {
		return selectedCountry;
	}
	
	public void setSelectedCountry(String selectedCountry) {
		this.selectedCountry = selectedCountry;
	}
	
	public String getSocieta() {
		return societa;
	}
	
	public void setSocieta(String societa) {
		this.societa = societa;
	}
	
	public String getSurname() {
		return surname;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public String getTipoRichiesta() {
		return tipoRichiesta;
	}
	
	public void setTipoRichiesta(String tipoRichiesta) {
		this.tipoRichiesta = tipoRichiesta;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((CFPIVA == null) ? 0 : CFPIVA.hashCode());
		result = prime * result + (bill ? 1231 : 1237);
		result = prime * result + ((cap == null) ? 0 : cap.hashCode());
		result = prime * result + ((cfInt == null) ? 0 : cfInt.hashCode());
		result = prime * result + ((cfPax == null) ? 0 : cfPax.hashCode());
		result = prime * result
				+ ((emailInt == null) ? 0 : emailInt.hashCode());
		result = prime * result
				+ ((enteEmit == null) ? 0 : enteEmit.hashCode());
		result = prime * result
				+ ((enteRich == null) ? 0 : enteRich.hashCode());
		result = prime * result
				+ ((formaSocietaria == null) ? 0 : formaSocietaria.hashCode());
		result = prime * result + ((indSped == null) ? 0 : indSped.hashCode());
		result = prime * result
				+ ((intFattura == null) ? 0 : intFattura.hashCode());
		result = prime * result + ((locSped == null) ? 0 : locSped.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((paese == null) ? 0 : paese.hashCode());
		result = prime * result + ((pivaInt == null) ? 0 : pivaInt.hashCode());
		result = prime * result + ((prov == null) ? 0 : prov.hashCode());
		result = prime * result
				+ ((selectedCountry == null) ? 0 : selectedCountry.hashCode());
		result = prime * result + ((societa == null) ? 0 : societa.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		result = prime * result
				+ ((tipoRichiesta == null) ? 0 : tipoRichiesta.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BillingData other = (BillingData) obj;
		if (CFPIVA == null) {
			if (other.CFPIVA != null)
				return false;
		} else if (!CFPIVA.equals(other.CFPIVA))
			return false;
		if (bill != other.bill)
			return false;
		if (cap == null) {
			if (other.cap != null)
				return false;
		} else if (!cap.equals(other.cap))
			return false;
		if (cfInt == null) {
			if (other.cfInt != null)
				return false;
		} else if (!cfInt.equals(other.cfInt))
			return false;
		if (cfPax == null) {
			if (other.cfPax != null)
				return false;
		} else if (!cfPax.equals(other.cfPax))
			return false;
		if (emailInt == null) {
			if (other.emailInt != null)
				return false;
		} else if (!emailInt.equals(other.emailInt))
			return false;
		if (enteEmit == null) {
			if (other.enteEmit != null)
				return false;
		} else if (!enteEmit.equals(other.enteEmit))
			return false;
		if (enteRich == null) {
			if (other.enteRich != null)
				return false;
		} else if (!enteRich.equals(other.enteRich))
			return false;
		if (formaSocietaria == null) {
			if (other.formaSocietaria != null)
				return false;
		} else if (!formaSocietaria.equals(other.formaSocietaria))
			return false;
		if (indSped == null) {
			if (other.indSped != null)
				return false;
		} else if (!indSped.equals(other.indSped))
			return false;
		if (intFattura == null) {
			if (other.intFattura != null)
				return false;
		} else if (!intFattura.equals(other.intFattura))
			return false;
		if (locSped == null) {
			if (other.locSped != null)
				return false;
		} else if (!locSped.equals(other.locSped))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (paese == null) {
			if (other.paese != null)
				return false;
		} else if (!paese.equals(other.paese))
			return false;
		if (pivaInt == null) {
			if (other.pivaInt != null)
				return false;
		} else if (!pivaInt.equals(other.pivaInt))
			return false;
		if (prov == null) {
			if (other.prov != null)
				return false;
		} else if (!prov.equals(other.prov))
			return false;
		if (selectedCountry == null) {
			if (other.selectedCountry != null)
				return false;
		} else if (!selectedCountry.equals(other.selectedCountry))
			return false;
		if (societa == null) {
			if (other.societa != null)
				return false;
		} else if (!societa.equals(other.societa))
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		if (tipoRichiesta == null) {
			if (other.tipoRichiesta != null)
				return false;
		} else if (!tipoRichiesta.equals(other.tipoRichiesta))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BillingData [bill=" + bill + ", CFPIVA=" + CFPIVA + ", cap="
				+ cap + ", cfInt=" + cfInt + ", cfPax=" + cfPax + ", emailInt="
				+ emailInt + ", enteEmit=" + enteEmit + ", enteRich="
				+ enteRich + ", formaSocietaria=" + formaSocietaria
				+ ", indSped=" + indSped + ", intFattura=" + intFattura
				+ ", locSped=" + locSped + ", name=" + name + ", nome=" + nome
				+ ", paese=" + paese + ", pivaInt=" + pivaInt + ", prov="
				+ prov + ", selectedCountry=" + selectedCountry + ", societa="
				+ societa + ", surname=" + surname + ", tipoRichiesta="
				+ tipoRichiesta + "]";
	}

	
	
}
