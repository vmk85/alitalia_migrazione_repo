package com.alitalia.aem.common.messages.home;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alitalia.aem.common.data.home.mmb.MmbRequestBaseInfoData;
import com.alitalia.aem.common.messages.MmbBaseRequest;

public class MmbCheckFrequentFlyerCodesRequest extends MmbBaseRequest {

	private Map<Integer, List<String>> ffCodesToCheck;

	public MmbCheckFrequentFlyerCodesRequest(Integer key, String ffCode, String lastname, String name) {
		super();
		this.ffCodesToCheck = new HashMap<Integer, List<String>>();
		List<String> arguments = new ArrayList<String>();
		arguments.add(ffCode);
		arguments.add(name);
		arguments.add(lastname);
		this.ffCodesToCheck.put(key, arguments);
	}
	
	public MmbCheckFrequentFlyerCodesRequest(Integer key, String ffCode) {
		super();
		this.ffCodesToCheck = new HashMap<Integer, List<String>>();
		List<String> arguments = new ArrayList<String>();
		arguments.add(ffCode);
		this.ffCodesToCheck.put(key, arguments);
	}

	public MmbCheckFrequentFlyerCodesRequest() {
		super();
	}

	public MmbCheckFrequentFlyerCodesRequest(MmbRequestBaseInfoData baseInfo, String client, String tid, String sid) {
		super(baseInfo, client, tid, sid);
	}

	public MmbCheckFrequentFlyerCodesRequest(MmbRequestBaseInfoData baseInfo, String client, String tid, String sid,
			Integer key, String ffCode, String lastname, String name) {
		super(baseInfo, client, tid, sid);
		this.ffCodesToCheck = new HashMap<Integer, List<String>>();
		List<String> arguments = new ArrayList<String>();
		arguments.add(ffCode);
		arguments.add(name);
		arguments.add(lastname);
		this.ffCodesToCheck.put(key, arguments);
	}

	public MmbCheckFrequentFlyerCodesRequest(MmbRequestBaseInfoData baseInfo,
			String client) {
		super(baseInfo, client);
	}

	public MmbCheckFrequentFlyerCodesRequest(String tid, String sid) {
		super(tid, sid);
	}

	public Map<Integer, List<String>> getFfCodesToCheck() {
		return ffCodesToCheck;
	}

	public void setFfCodesToCheck(Map<Integer, List<String>> ffCodesToCheck) {
		this.ffCodesToCheck = ffCodesToCheck;
	}

	public void add(Integer key, String ffCode, String lastname, String name) {
		if (this.ffCodesToCheck == null)
			this.ffCodesToCheck = new HashMap<Integer, List<String>>();
		List<String> arguments = new ArrayList<String>();
		arguments.add(ffCode);
		arguments.add(name);
		arguments.add(lastname);
		this.ffCodesToCheck.put(key, arguments);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((ffCodesToCheck == null) ? 0 : ffCodesToCheck.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbCheckFrequentFlyerCodesRequest other = (MmbCheckFrequentFlyerCodesRequest) obj;
		if (ffCodesToCheck == null) {
			if (other.ffCodesToCheck != null)
				return false;
		} else if (!ffCodesToCheck.equals(other.ffCodesToCheck))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbCheckFrequentFlyerCodesRequest [ffCodesToCheck="
				+ ffCodesToCheck + ", getBaseInfo()=" + getBaseInfo()
				+ ", getClient()=" + getClient() + ", getTid()=" + getTid()
				+ ", getSid()=" + getSid() + "]";
	}

}