package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.checkin.CheckinBaggagePolicyData;
import com.alitalia.aem.common.messages.BaseResponse;

public class ExtraBaggagePolicyCheckinResponse extends BaseResponse {

	private CheckinBaggagePolicyData policy;

	public CheckinBaggagePolicyData getPolicy() {
		return policy;
	}

	public void setPolicy(CheckinBaggagePolicyData policy) {
		this.policy = policy;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((policy == null) ? 0 : policy.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExtraBaggagePolicyCheckinResponse other = (ExtraBaggagePolicyCheckinResponse) obj;
		if (policy == null) {
			if (other.policy != null)
				return false;
		} else if (!policy.equals(other.policy))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ExtraBaggagePolicyCheckinResponse [policy=" + policy
				+ ", toString()=" + super.toString() + "]";
	}

}
