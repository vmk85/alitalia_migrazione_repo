package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.AdyenAdditionalData;
import com.alitalia.aem.common.data.home.AdyenAddressData;
import com.alitalia.aem.common.data.home.AdyenCardData;
import com.alitalia.aem.common.data.home.AdyenRecurringData;
import com.alitalia.aem.common.messages.AdyenBaseRequest;

public class AdyenStoringPaymentRequest extends AdyenBaseRequest {
	
	/*
	{
	  "card": {
	    "cvc": "737",
	    "expiryMonth": "08",
	    "expiryYear": 2018,
	    "holderName": "Test Uno",
	    "number": "4444333322221111",
	    "type": "Visa"
	  },
	  "address": {
	    "city": "Roma",
	    "country": "IT",
	    "postalCode": "00100",
	    "province": "RM",
	    "street": "via roma 100"
	  },
	  "shopperEmail": "test@test.com",
	  "shopperReference": "39fbe13b0ffd46ef9ee08d09ec577034",
	  "recurring": {
	    "contract": "RECURRING"
	  },
	  "language": "IT",
	  "market": "IT",
	  "conversationID": "987654321",
  	  "caller": "WebCheckin"
	}
	*/

	private AdyenCardData card;
	private String type;
	private AdyenAddressData address;
	private String shopperEmail;
	private AdyenRecurringData recurring = new AdyenRecurringData();
	private AdyenAdditionalData additionalData = new AdyenAdditionalData();

	public AdyenAdditionalData getAdditionalData() {
		return additionalData;
	}

	public void setAdditionalData(AdyenAdditionalData adyenAdditionalData) {
		this.additionalData = adyenAdditionalData;
	}

	public AdyenCardData getCard() {
		return card;
	}

	public void setCard(AdyenCardData card) {
		this.card = card;
	}

	public AdyenAddressData getAddress() {
		return address;
	}

	public void setAddress(AdyenAddressData address) {
		this.address = address;
	}

	public String getShopperEmail() {
		return shopperEmail;
	}

	public void setShopperEmail(String shopperEmail) {
		this.shopperEmail = shopperEmail;
	}

	public AdyenRecurringData getRecurring() {
		return recurring;
	}

	public void setRecurring(AdyenRecurringData recurring) {
		this.recurring = recurring;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result	+ ((card == null) ? 0 : card.hashCode());
		result = prime * result	+ ((address == null) ? 0 : address.hashCode());
		result = prime * result	+ ((shopperEmail == null) ? 0 : shopperEmail.hashCode());
		result = prime * result	+ ((recurring == null) ? 0 : recurring.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdyenStoringPaymentRequest other = (AdyenStoringPaymentRequest) obj;
		if (card == null) {
			if (other.card != null)
				return false;
		} else if (!card.equals(other.card))
			return false;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (shopperEmail == null) {
			if (other.shopperEmail != null)
				return false;
		} else if (!shopperEmail.equals(other.shopperEmail))
			return false;
		if (recurring == null) {
			if (other.recurring != null)
				return false;
		} else if (!recurring.equals(other.recurring))
			return false;

		return true;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "RegisterCreditCardRequest ["
				+ "card=" + card
				+ "address=" + address
				+ "shopperEmail=" + shopperEmail
				+ "shopperReference=" + super.getShopperReference()
				+ "recurring=" + recurring
				+ "additionalData=" + additionalData
				+ "language=" + super.getLanguage()
				+ "market=" + super.getMarket()
				+ "conversationID=" + super.getConversationID()
				+ "caller=" + super.getCaller()
				+ "]";
	}
	
}