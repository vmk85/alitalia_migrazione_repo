package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class RetrieveFlightMileageResponse extends BaseResponse {

	private long mileage;

	public long getMileage() {
		return mileage;
	}

	public void setMileage(long mileage) {
		this.mileage = mileage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (int) (mileage ^ (mileage >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveFlightMileageResponse other = (RetrieveFlightMileageResponse) obj;
		if (mileage != other.mileage)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveFlightMileageResponse [mileage=" + mileage + "]";
	}
}