package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetrievePinResponse extends BaseResponse {

	private boolean retrieved;
	private boolean mailSent;
	private MMCustomerProfileData customerProfile;
	private Integer returnCode;
	private String errorMessage;

	public boolean isRetrieved() {
		return retrieved;
	}

	public void setRetrieved(boolean retrieved) {
		this.retrieved = retrieved;
	}

	public boolean isMailSent() {
		return mailSent;
	}

	public void setMailSent(boolean mailSent) {
		this.mailSent = mailSent;
	}

	public MMCustomerProfileData getCustomerProfile() {
		return customerProfile;
	}

	public void setCustomerProfile(MMCustomerProfileData customerProfile) {
		this.customerProfile = customerProfile;
	}

	public Integer getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(Integer returnCode) {
		this.returnCode = returnCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((customerProfile == null) ? 0 : customerProfile.hashCode());
		result = prime * result
				+ ((errorMessage == null) ? 0 : errorMessage.hashCode());
		result = prime * result + (retrieved ? 1231 : 1237);
		result = prime * result
				+ ((returnCode == null) ? 0 : returnCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrievePinResponse other = (RetrievePinResponse) obj;
		if (customerProfile == null) {
			if (other.customerProfile != null)
				return false;
		} else if (!customerProfile.equals(other.customerProfile))
			return false;
		if (errorMessage == null) {
			if (other.errorMessage != null)
				return false;
		} else if (!errorMessage.equals(other.errorMessage))
			return false;
		if (retrieved != other.retrieved)
			return false;
		if (returnCode == null) {
			if (other.returnCode != null)
				return false;
		} else if (!returnCode.equals(other.returnCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrievePinResponse [retrieved=" + retrieved
				+ ", customerProfile=" + customerProfile + ", returnCode="
				+ returnCode + ", errorMessage=" + errorMessage + ", getTid()="
				+ getTid() + ", getSid()=" + getSid() + "]";
	}
}