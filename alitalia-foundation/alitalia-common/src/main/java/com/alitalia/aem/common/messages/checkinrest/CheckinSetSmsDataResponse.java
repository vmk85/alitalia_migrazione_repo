package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinSetSmsDataResponse extends BaseResponse {

    private String key;
    private String result;
    private String conversationID;
    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public CheckinSetSmsDataResponse() {}

    public CheckinSetSmsDataResponse(String tid, String sid) {
        super(tid, sid);
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }
}
