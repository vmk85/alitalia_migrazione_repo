
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaggageAllowance {

    @SerializedName("count")
    @Expose
    private Long count;
    @SerializedName("countSpecified")
    @Expose
    private Boolean countSpecified;
    @SerializedName("subCodeOfBaggageItemList")
    @Expose
    private List<SubCodeOfBaggageItemList> subCodeOfBaggageItemList = null;

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Boolean getCountSpecified() {
        return countSpecified;
    }

    public void setCountSpecified(Boolean countSpecified) {
        this.countSpecified = countSpecified;
    }

    public List<SubCodeOfBaggageItemList> getSubCodeOfBaggageItemList() {
        return subCodeOfBaggageItemList;
    }

    public void setSubCodeOfBaggageItemList(List<SubCodeOfBaggageItemList> subCodeOfBaggageItemList) {
        this.subCodeOfBaggageItemList = subCodeOfBaggageItemList;
    }

}
