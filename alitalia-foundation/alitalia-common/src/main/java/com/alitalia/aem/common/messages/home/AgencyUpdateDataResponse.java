package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class AgencyUpdateDataResponse extends BaseResponse {
	
	private Boolean isSuccessful;
	
	public AgencyUpdateDataResponse() {
		super();
	}
	
	public AgencyUpdateDataResponse(String tid, String sid) {
		super(tid, sid);
	}

	public AgencyUpdateDataResponse(Boolean isSuccessful) {
		super();
		this.isSuccessful = isSuccessful;
	}
	
	public AgencyUpdateDataResponse(String tid, String sid, Boolean isSuccessful) {
		super();
		this.isSuccessful = isSuccessful;
	}

	public Boolean isSuccessful() {
		return isSuccessful;
	}

	public void setIsSuccessful(Boolean isSuccessful) {
		this.isSuccessful = isSuccessful;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((isSuccessful == null) ? 0 : isSuccessful.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgencyUpdateDataResponse other = (AgencyUpdateDataResponse) obj;
		if (isSuccessful == null) {
			if (other.isSuccessful != null)
				return false;
		} else if (!isSuccessful.equals(other.isSuccessful))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AgencyUpdateDataResponse [isSuccessful=" + isSuccessful + "]";
	}
	
}
