package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.messages.BaseRequest;

public class RetrieveFlightSeatMapRequest extends BaseRequest {

	private List<String> compartimentalClass;
	private DirectFlightData flightInfo;
	private String market;

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public RetrieveFlightSeatMapRequest(String tid, String sid) {
		super(tid, sid);
	}
	
	public List<String> getCompartimentalClass() {
		return compartimentalClass;
	}
	
	public void setCompartimentalClass(List<String> compartimentalClass) {
		this.compartimentalClass = compartimentalClass;
	}
	
	public DirectFlightData getFlightInfo() {
		return flightInfo;
	}
	
	public void setFlightInfo(DirectFlightData flightInfo) {
		this.flightInfo = flightInfo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((compartimentalClass == null) ? 0 : compartimentalClass
						.hashCode());
		result = prime * result
				+ ((flightInfo == null) ? 0 : flightInfo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveFlightSeatMapRequest other = (RetrieveFlightSeatMapRequest) obj;
		if (compartimentalClass == null) {
			if (other.compartimentalClass != null)
				return false;
		} else if (!compartimentalClass.equals(other.compartimentalClass))
			return false;
		if (flightInfo == null) {
			if (other.flightInfo != null)
				return false;
		} else if (!flightInfo.equals(other.flightInfo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveFlightSeatMapRequest{" +
				"compartimentalClass=" + compartimentalClass +
				", flightInfo=" + flightInfo +
				", market='" + market + '\'' +
				'}';
	}
}