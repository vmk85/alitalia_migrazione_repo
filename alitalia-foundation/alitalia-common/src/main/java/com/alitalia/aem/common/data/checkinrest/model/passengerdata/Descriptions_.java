
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Descriptions_ {

    @SerializedName("commercialName")
    @Expose
    private String commercialName;
    @SerializedName("description")
    @Expose
    private List<Description_> description = null;

    public String getCommercialName() {
        return commercialName;
    }

    public void setCommercialName(String commercialName) {
        this.commercialName = commercialName;
    }

    public List<Description_> getDescription() {
        return description;
    }

    public void setDescription(List<Description_> description) {
        this.description = description;
    }

}
