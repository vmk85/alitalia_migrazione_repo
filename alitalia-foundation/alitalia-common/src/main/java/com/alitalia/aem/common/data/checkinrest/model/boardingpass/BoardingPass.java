package com.alitalia.aem.common.data.checkinrest.model.boardingpass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BoardingPass {

	@SerializedName("boardingPass")
	@Expose
	private String boardingPass;

	public String getBoardingPass() {
		return boardingPass;
	}

	public void setBoardingPass(String boardingPass) {
		this.boardingPass = boardingPass;
	}

}