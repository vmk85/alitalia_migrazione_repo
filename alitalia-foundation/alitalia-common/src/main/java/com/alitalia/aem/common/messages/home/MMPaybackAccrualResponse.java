package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class MMPaybackAccrualResponse extends BaseResponse {

	private Long paybackResponse;

	public Long getPaybackResponse() {
		return paybackResponse;
	}

	public void setPaybackResponse(Long paybackResponse) {
		this.paybackResponse = paybackResponse;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((paybackResponse == null) ? 0 : paybackResponse.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MMPaybackAccrualResponse other = (MMPaybackAccrualResponse) obj;
		if (paybackResponse == null) {
			if (other.paybackResponse != null)
				return false;
		} else if (!paybackResponse.equals(other.paybackResponse))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MMPaybackAccrualResponse [paybackResponse=" + paybackResponse
				+ "]";
	}
}