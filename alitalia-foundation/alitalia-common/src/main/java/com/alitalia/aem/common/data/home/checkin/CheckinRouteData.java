package com.alitalia.aem.common.data.home.checkin;

import java.util.List;

import com.alitalia.aem.common.data.home.enumerations.MmbApisTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbRouteStatusEnum;
import com.alitalia.aem.common.data.home.mmb.MmbDeepLinkData;

public class CheckinRouteData {

	private Integer id;
	private MmbApisTypeEnum apisTypeRequired;
	private Boolean checkinEnabled;
	private MmbDeepLinkData deepLink;
	private List<CheckinFlightData> flights;
	private Boolean idApis;
	private List<CheckinFlightData> interlineFlights;
	private Boolean isOutwardRoute;
	private String pnr;
	private Boolean selected;
	private MmbRouteStatusEnum status;
	private Boolean undoEnabled;
	private boolean bus;
	private boolean fromBusStation;
	private boolean toBusStation;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public MmbApisTypeEnum getApisTypeRequired() {
		return apisTypeRequired;
	}
	public void setApisTypeRequired(MmbApisTypeEnum apisTypeRequired) {
		this.apisTypeRequired = apisTypeRequired;
	}
	public Boolean getCheckinEnabled() {
		return checkinEnabled;
	}
	public void setCheckinEnabled(Boolean checkinEnabled) {
		this.checkinEnabled = checkinEnabled;
	}
	public MmbDeepLinkData getDeepLink() {
		return deepLink;
	}
	public void setDeepLink(MmbDeepLinkData deepLink) {
		this.deepLink = deepLink;
	}
	public List<CheckinFlightData> getFlights() {
		return flights;
	}
	public void setFlights(List<CheckinFlightData> flights) {
		this.flights = flights;
	}
	public Boolean getIdApis() {
		return idApis;
	}
	public void setIdApis(Boolean idApis) {
		this.idApis = idApis;
	}
	public List<CheckinFlightData> getInterlineFlights() {
		return interlineFlights;
	}
	public void setInterlineFlights(List<CheckinFlightData> interlineFlights) {
		this.interlineFlights = interlineFlights;
	}
	public Boolean getIsOutwardRoute() {
		return isOutwardRoute;
	}
	public void setIsOutwardRoute(Boolean isOutwardRoute) {
		this.isOutwardRoute = isOutwardRoute;
	}
	public String getPnr() {
		return pnr;
	}
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	public Boolean getSelected() {
		return selected;
	}
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
	public MmbRouteStatusEnum getStatus() {
		return status;
	}
	public void setStatus(MmbRouteStatusEnum status) {
		this.status = status;
	}
	public Boolean getUndoEnabled() {
		return undoEnabled;
	}
	public void setUndoEnabled(Boolean undoEnabled) {
		this.undoEnabled = undoEnabled;
	}
	
	public boolean isBus() {
		return bus;
	}

	public void setBus(boolean bus) {
		this.bus = bus;
	}
	
	public boolean isFromBusStation() {
		return fromBusStation;
	}

	public void setFromBusStation(boolean fromBusStation) {
		this.fromBusStation = fromBusStation;
	}
	
	public boolean isToBusStation() {
		return toBusStation;
	}

	public void setToBusStation(boolean toBusStation) {
		this.toBusStation = toBusStation;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((apisTypeRequired == null) ? 0 : apisTypeRequired.hashCode());
		result = prime * result
				+ ((checkinEnabled == null) ? 0 : checkinEnabled.hashCode());
		result = prime * result
				+ ((deepLink == null) ? 0 : deepLink.hashCode());
		result = prime * result + ((flights == null) ? 0 : flights.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((idApis == null) ? 0 : idApis.hashCode());
		result = prime
				* result
				+ ((interlineFlights == null) ? 0 : interlineFlights.hashCode());
		result = prime * result
				+ ((isOutwardRoute == null) ? 0 : isOutwardRoute.hashCode());
		result = prime * result + ((pnr == null) ? 0 : pnr.hashCode());
		result = prime * result
				+ ((selected == null) ? 0 : selected.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result
				+ ((undoEnabled == null) ? 0 : undoEnabled.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinRouteData other = (CheckinRouteData) obj;
		if (apisTypeRequired != other.apisTypeRequired)
			return false;
		if (checkinEnabled == null) {
			if (other.checkinEnabled != null)
				return false;
		} else if (!checkinEnabled.equals(other.checkinEnabled))
			return false;
		if (deepLink == null) {
			if (other.deepLink != null)
				return false;
		} else if (!deepLink.equals(other.deepLink))
			return false;
		if (flights == null) {
			if (other.flights != null)
				return false;
		} else if (!flights.equals(other.flights))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (idApis == null) {
			if (other.idApis != null)
				return false;
		} else if (!idApis.equals(other.idApis))
			return false;
		if (interlineFlights == null) {
			if (other.interlineFlights != null)
				return false;
		} else if (!interlineFlights.equals(other.interlineFlights))
			return false;
		if (isOutwardRoute == null) {
			if (other.isOutwardRoute != null)
				return false;
		} else if (!isOutwardRoute.equals(other.isOutwardRoute))
			return false;
		if (pnr == null) {
			if (other.pnr != null)
				return false;
		} else if (!pnr.equals(other.pnr))
			return false;
		if (selected == null) {
			if (other.selected != null)
				return false;
		} else if (!selected.equals(other.selected))
			return false;
		if (status != other.status)
			return false;
		if (undoEnabled == null) {
			if (other.undoEnabled != null)
				return false;
		} else if (!undoEnabled.equals(other.undoEnabled))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CheckinRouteData [id=" + id + ", apisTypeRequired="
				+ apisTypeRequired + ", checkinEnabled=" + checkinEnabled
				+ ", deepLink=" + deepLink + ", flights=" + flights
				+ ", idApis=" + idApis + ", interlineFlights="
				+ interlineFlights + ", isOutwardRoute=" + isOutwardRoute
				+ ", pnr=" + pnr + ", selected=" + selected + ", status="
				+ status + ", undoEnabled=" + undoEnabled + "]";
	}

}
