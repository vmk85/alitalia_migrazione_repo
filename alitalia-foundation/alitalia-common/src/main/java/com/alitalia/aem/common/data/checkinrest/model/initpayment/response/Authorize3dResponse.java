package com.alitalia.aem.common.data.checkinrest.model.initpayment.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Authorize3dResponse {

    @SerializedName("resultCode")
    @Expose
    private String resultCode;
    @SerializedName("paymentRef")
    @Expose
    private String paymentRef;
    @SerializedName("responseCode")
    @Expose
    private String responseCode;
    @SerializedName("approvalCode")
    @Expose
    private String approvalCode;
    @SerializedName("supplierID")
    @Expose
    private String supplierID;
    @SerializedName("supplierTransID")
    @Expose
    private String supplierTransID;
    @SerializedName("supplierResponseCode")
    @Expose
    private String supplierResponseCode;
    @SerializedName("authRemarks1")
    @Expose
    private String authRemarks1;
    @SerializedName("authRemarks2")
    @Expose
    private String authRemarks2;
    @SerializedName("csC_ResultCode")
    @Expose
    private String csC_ResultCode;
    @SerializedName("csC_Remarks")
    @Expose
    private String csC_Remarks;
    @SerializedName("avS_ResultCode")
    @Expose
    private String avS_ResultCode;
    @SerializedName("avS_Remarks")
    @Expose
    private String avS_Remarks;
    @SerializedName("merchantID")
    @Expose
    private String merchantID;
    @SerializedName("mac")
    @Expose
    private String mac;
    @SerializedName("pspReference")
    @Expose
    private String pspReference;
    @SerializedName("refusalReason")
    @Expose
    private String refusalReason;

    public String getPspReference() {
        return pspReference;
    }

    public void setPspReference(String pspReference) {
        this.pspReference = pspReference;
    }

    public String getRefusalReason() {
        return refusalReason;
    }

    public void setRefusalReason(String refusalReason) {
        this.refusalReason = refusalReason;
    }

    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getPaymentRef() {
        return paymentRef;
    }

    public void setPaymentRef(String paymentRef) {
        this.paymentRef = paymentRef;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public String getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(String supplierID) {
        this.supplierID = supplierID;
    }

    public String getSupplierTransID() {
        return supplierTransID;
    }

    public void setSupplierTransID(String supplierTransID) {
        this.supplierTransID = supplierTransID;
    }

    public String getSupplierResponseCode() {
        return supplierResponseCode;
    }

    public void setSupplierResponseCode(String supplierResponseCode) {
        this.supplierResponseCode = supplierResponseCode;
    }

    public String getAuthRemarks1() {
        return authRemarks1;
    }

    public void setAuthRemarks1(String authRemarks1) {
        this.authRemarks1 = authRemarks1;
    }

    public String getAuthRemarks2() {
        return authRemarks2;
    }

    public void setAuthRemarks2(String authRemarks2) {
        this.authRemarks2 = authRemarks2;
    }

    public String getCsC_ResultCode() {
        return csC_ResultCode;
    }

    public void setCsC_ResultCode(String csC_ResultCode) {
        this.csC_ResultCode = csC_ResultCode;
    }

    public String getCsC_Remarks() {
        return csC_Remarks;
    }

    public void setCsC_Remarks(String csC_Remarks) {
        this.csC_Remarks = csC_Remarks;
    }

    public String getAvS_ResultCode() {
        return avS_ResultCode;
    }

    public void setAvS_ResultCode(String avS_ResultCode) {
        this.avS_ResultCode = avS_ResultCode;
    }

    public String getAvS_Remarks() {
        return avS_Remarks;
    }

    public void setAvS_Remarks(String avS_Remarks) {
        this.avS_Remarks = avS_Remarks;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }
}
