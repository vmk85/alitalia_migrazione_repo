package com.alitalia.aem.common.data.checkinrest.model.boardingpass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BoardingPassSms {

    @SerializedName("airline")
    @Expose
    private String airline;

    @SerializedName("flight")
    @Expose
    private String flight;

    @SerializedName("origin")
    @Expose
    private String origin;

    @SerializedName("destination")
    @Expose
    private String destination;

    @SerializedName("departureDate")
    @Expose
    private String departureDate;

    @SerializedName("pnr")
    @Expose
    private String pnr;

    @SerializedName("seats")
    @Expose
    private List<String> seats = null;

    @SerializedName("surname")
    @Expose
    private String surname;

    @SerializedName("passengerID")
    @Expose
    private String passengerID;

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getFlight() {
        return flight;
    }

    public void setFlight(String flight) {
        this.flight = flight;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public List<String> getSeats() {
        return seats;
    }

    public void setSeats(List<String> seats) {
        this.seats = seats;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassengerID() {
        return passengerID;
    }

    public void setPassengerID(String passengerID) {
        this.passengerID = passengerID;
    }

    public boolean addSeats(String seats) {
        this.seats.add(seats);
        return true;
    }
}
