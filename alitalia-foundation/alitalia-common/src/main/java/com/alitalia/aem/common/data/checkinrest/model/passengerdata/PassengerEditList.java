
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PassengerEditList {

    @SerializedName("editCodeAttributeList")
    @Expose
    private List<EditCodeAttributeList> editCodeAttributeList = null;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("infant")
    @Expose
    private Boolean infant;
    @SerializedName("infantSpecified")
    @Expose
    private Boolean infantSpecified;

    public List<EditCodeAttributeList> getEditCodeAttributeList() {
        return editCodeAttributeList;
    }

    public void setEditCodeAttributeList(List<EditCodeAttributeList> editCodeAttributeList) {
        this.editCodeAttributeList = editCodeAttributeList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getInfant() {
        return infant;
    }

    public void setInfant(Boolean infant) {
        this.infant = infant;
    }

    public Boolean getInfantSpecified() {
        return infantSpecified;
    }

    public void setInfantSpecified(Boolean infantSpecified) {
        this.infantSpecified = infantSpecified;
    }

}
