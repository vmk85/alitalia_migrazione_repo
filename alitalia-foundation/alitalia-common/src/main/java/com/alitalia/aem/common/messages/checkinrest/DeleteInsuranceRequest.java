package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.messages.BaseRequest;
import com.google.gson.annotations.Expose;

public class DeleteInsuranceRequest extends BaseRequest {

	@Expose
	private String pnr;
	@Expose
	private String language;
	@Expose
	private String market;
	@Expose
	private String conversationID;
	
	public DeleteInsuranceRequest() {}
	
	public DeleteInsuranceRequest(String tid, String sid) {
		super(tid, sid);
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public String getConversationID() {
		return conversationID;
	}

	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}
	
}
