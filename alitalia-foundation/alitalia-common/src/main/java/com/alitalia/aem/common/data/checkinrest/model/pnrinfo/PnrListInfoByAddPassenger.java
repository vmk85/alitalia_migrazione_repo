
package com.alitalia.aem.common.data.checkinrest.model.pnrinfo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PnrListInfoByAddPassenger {

    @SerializedName("pnr")
    @Expose
    private List<Pnr> pnr = null;
    @SerializedName("conversationID")
    @Expose
    private String conversationID;
    @SerializedName("outcome")
    @Expose
    private String outcome;

    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<Pnr> getPnr() {
        return pnr;
    }

    public void setPnr(List<Pnr> pnr) {
        this.pnr = pnr;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

    public String getOutcome() {
        return outcome;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }

}
