
package com.alitalia.aem.common.data.checkinrest.model.getcart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cart {

    @SerializedName("totalPriceToBeIssued")
    @Expose
    private float totalPriceToBeIssued;
    @SerializedName("totalPriceIssued")
    @Expose
    private float totalPriceIssued;
    @SerializedName("checkinCart")
    @Expose
    private CheckinCart checkinCart;

    @SerializedName("checkinCartObj")
    @Expose
    private checkinCartObj checkinCartObj;
    @SerializedName("aziInsurancePolicy")
    @Expose
    private AziInsurancePolicy aziInsurancePolicy;
    @SerializedName("conversationID")
    @Expose
    private String conversationID;

    @SerializedName("totalPrice")
    @Expose
    private String totalPrice;


    private String error;

	public float getTotalPriceToBeIssued() {
		return totalPriceToBeIssued;
	}

	public void setTotalPriceToBeIssued(float totalPriceToBeIssued) {
		this.totalPriceToBeIssued = totalPriceToBeIssued;
	}

	public float getTotalPriceIssued() {
		return totalPriceIssued;
	}

	public void setTotalPriceIssued(float totalPriceIssued) {
		this.totalPriceIssued = totalPriceIssued;
	}

	public CheckinCart getCheckinCart() {
        return checkinCart;
    }

    public void setCheckinCart(CheckinCart checkinCart) {
        this.checkinCart = checkinCart;
    }

    public AziInsurancePolicy getAziInsurancePolicy() {
        return aziInsurancePolicy;
    }

    public void setAziInsurancePolicy(AziInsurancePolicy aziInsurancePolicy) {
        this.aziInsurancePolicy = aziInsurancePolicy;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

    public com.alitalia.aem.common.data.checkinrest.model.getcart.checkinCartObj getCheckinCartObj() {
        return checkinCartObj;
    }

    public void setCheckinCartObj(com.alitalia.aem.common.data.checkinrest.model.getcart.checkinCartObj checkinCartObj) {
        this.checkinCartObj = checkinCartObj;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

}
