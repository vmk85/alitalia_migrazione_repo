package com.alitalia.aem.common.data.home;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Availability implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@SerializedName("iataOrigine")
	@Expose
	private String iataOrigine;
	
    @SerializedName("iataDestinazione")
    @Expose
    private String iataDestinazione;
  
    @SerializedName("mondaySummer")
    @Expose
    private boolean mondaySummer;
    
    @SerializedName("tuesdaySummer")
    @Expose
    private boolean tuesdaySummer;
    
    @SerializedName("wednesdaySummer")
    @Expose
    private boolean wednesdaySummer;
    
    @SerializedName("thursdaySummer")
    @Expose
    private boolean thursdaySummer;
    
    @SerializedName("fridaySummer")
    @Expose
    private boolean fridaySummer;
    
    @SerializedName("saturdaySummer")
    @Expose
    private boolean saturdaySummer;
    
    @SerializedName("sundaySummer")
    @Expose
    private boolean sundaySummer;

    @SerializedName("mondayWinter")
    @Expose
    private boolean mondayWinter;
    
    @SerializedName("tuesdayWinter")
    @Expose
    private boolean tuesdayWinter;
    
    @SerializedName("wednesdayWinter")
    @Expose
    private boolean wednesdayWinter;
    
    @SerializedName("thursdayWinter")
    @Expose
    private boolean thursdayWinter;
    
    @SerializedName("fridayWinter")
    @Expose
    private boolean fridayWinter;
    
    @SerializedName("saturdayWinter")
    @Expose
    private boolean saturdayWinter;
    
    @SerializedName("sundayWinter")
    @Expose
    private boolean sundayWinter;
    
    @SerializedName("startSummer")
    @Expose
    private String startSummer;
    
    @SerializedName("endSummer")
    @Expose
    private String endSummer;
    
    @SerializedName("startWinter")
    @Expose
    private String startWinter;
    
    @SerializedName("endWinter")
    @Expose
    private String endWinter;

	public String getIataOrigine() {
		return iataOrigine;
	}

	public void setIataOrigine(String iataOrigine) {
		this.iataOrigine = iataOrigine;
	}

	public String getIataDestinazione() {
		return iataDestinazione;
	}

	public void setIataDestinazione(String iataDestinazione) {
		this.iataDestinazione = iataDestinazione;
	}

	public boolean isMondaySummer() {
		return mondaySummer;
	}

	public void setMondaySummer(boolean mondaySummer) {
		this.mondaySummer = mondaySummer;
	}

	public boolean isTuesdaySummer() {
		return tuesdaySummer;
	}

	public void setTuesdaySummer(boolean tuesdaySummer) {
		this.tuesdaySummer = tuesdaySummer;
	}

	public boolean isWednesdaySummer() {
		return wednesdaySummer;
	}

	public void setWednesdaySummer(boolean wednesdaySummer) {
		this.wednesdaySummer = wednesdaySummer;
	}

	public boolean isThursdaySummer() {
		return thursdaySummer;
	}

	public void setThursdaySummer(boolean thursdaySummer) {
		this.thursdaySummer = thursdaySummer;
	}

	public boolean isFridaySummer() {
		return fridaySummer;
	}

	public void setFridaySummer(boolean fridaySummer) {
		this.fridaySummer = fridaySummer;
	}

	public boolean isSaturdaySummer() {
		return saturdaySummer;
	}

	public void setSaturdaySummer(boolean saturdaySummer) {
		this.saturdaySummer = saturdaySummer;
	}

	public boolean isSundaySummer() {
		return sundaySummer;
	}

	public void setSundaySummer(boolean sundaySummer) {
		this.sundaySummer = sundaySummer;
	}

	public boolean isMondayWinter() {
		return mondayWinter;
	}

	public void setMondayWinter(boolean mondayWinter) {
		this.mondayWinter = mondayWinter;
	}

	public boolean isTuesdayWinter() {
		return tuesdayWinter;
	}

	public void setTuesdayWinter(boolean tuesdayWinter) {
		this.tuesdayWinter = tuesdayWinter;
	}

	public boolean isWednesdayWinter() {
		return wednesdayWinter;
	}

	public void setWednesdayWinter(boolean wednesdayWinter) {
		this.wednesdayWinter = wednesdayWinter;
	}

	public boolean isThursdayWinter() {
		return thursdayWinter;
	}

	public void setThursdayWinter(boolean thursdayWinter) {
		this.thursdayWinter = thursdayWinter;
	}

	public boolean isFridayWinter() {
		return fridayWinter;
	}

	public void setFridayWinter(boolean fridayWinter) {
		this.fridayWinter = fridayWinter;
	}

	public boolean isSaturdayWinter() {
		return saturdayWinter;
	}

	public void setSaturdayWinter(boolean saturdayWinter) {
		this.saturdayWinter = saturdayWinter;
	}

	public boolean isSundayWinter() {
		return sundayWinter;
	}

	public void setSundayWinter(boolean sundayWinter) {
		this.sundayWinter = sundayWinter;
	}

	public String getStartSummer() {
		return startSummer;
	}

	public void setStartSummer(String startSummer) {
		this.startSummer = startSummer;
	}

	public String getEndSummer() {
		return endSummer;
	}

	public void setEndSummer(String endSummer) {
		this.endSummer = endSummer;
	}

	public String getStartWinter() {
		return startWinter;
	}

	public void setStartWinter(String startWinter) {
		this.startWinter = startWinter;
	}

	public String getEndWinter() {
		return endWinter;
	}

	public void setEndWinter(String endWinter) {
		this.endWinter = endWinter;
	}
   

}
