package com.alitalia.aem.common.data.checkinrest.model.passenger;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ColumnDetailList {
	
	@SerializedName("items")
	@Expose
	private List<Item> items = null;
	
	@SerializedName("itemsElementName")
	@Expose
	private List<String> itemsElementName = null;

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public List<String> getItemsElementName() {
		return itemsElementName;
	}

	public void setItemsElementName(List<String> itemsElementName) {
		this.itemsElementName = itemsElementName;
	}

}
