package com.alitalia.aem.common.data.home;

public class MMMemberIdentificationType {
	
    private String alias;
    private Integer aliasType;
    
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public Integer getAliasType() {
		return aliasType;
	}
	public void setAliasType(Integer aliasType) {
		this.aliasType = aliasType;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alias == null) ? 0 : alias.hashCode());
		result = prime * result
				+ ((aliasType == null) ? 0 : aliasType.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MMMemberIdentificationType other = (MMMemberIdentificationType) obj;
		if (alias == null) {
			if (other.alias != null)
				return false;
		} else if (!alias.equals(other.alias))
			return false;
		if (aliasType == null) {
			if (other.aliasType != null)
				return false;
		} else if (!aliasType.equals(other.aliasType))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "MMMemberIdentificationType [alias=" + alias + ", aliasType="
				+ aliasType + "]";
	}
}