package com.alitalia.aem.common.data.home;

public class AirportModelData {

	private String azTerminal;
    private Integer checkInTimeDomestic;
    private Integer checkInTimeIntercontinental;
    private Integer checkInTimeInternational;
    private Double cityDistance;
    private Integer gmt;
    private String name;
	
    public String getAzTerminal() {
		return azTerminal;
	}
    
	public void setAzTerminal(String azTerminal) {
		this.azTerminal = azTerminal;
	}
	
	public Integer getCheckInTimeDomestic() {
		return checkInTimeDomestic;
	}
	
	public void setCheckInTimeDomestic(Integer checkInTimeDomestic) {
		this.checkInTimeDomestic = checkInTimeDomestic;
	}
	
	public Integer getCheckInTimeIntercontinental() {
		return checkInTimeIntercontinental;
	}
	
	public void setCheckInTimeIntercontinental(Integer checkInTimeIntercontinental) {
		this.checkInTimeIntercontinental = checkInTimeIntercontinental;
	}
	
	public Integer getCheckInTimeInternational() {
		return checkInTimeInternational;
	}
	
	public void setCheckInTimeInternational(Integer checkInTimeInternational) {
		this.checkInTimeInternational = checkInTimeInternational;
	}
	
	public Double getCityDistance() {
		return cityDistance;
	}
	
	public void setCityDistance(Double cityDistance) {
		this.cityDistance = cityDistance;
	}
	
	public Integer getGmt() {
		return gmt;
	}
	
	public void setGmt(Integer gmt) {
		this.gmt = gmt;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "AiportModelData [azTerminal=" + azTerminal
				+ ", checkInTimeDomestic=" + checkInTimeDomestic
				+ ", checkInTimeIntercontinental="
				+ checkInTimeIntercontinental + ", checkInTimeInternational="
				+ checkInTimeInternational + ", cityDistance=" + cityDistance
				+ ", gmt=" + gmt + ", name=" + name + "]";
	}
}
