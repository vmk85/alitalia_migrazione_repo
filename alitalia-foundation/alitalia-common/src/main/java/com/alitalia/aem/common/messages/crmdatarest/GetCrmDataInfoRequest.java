package com.alitalia.aem.common.messages.crmdatarest;

import com.alitalia.aem.common.messages.BaseRequest;

public class GetCrmDataInfoRequest extends BaseRequest {

    private String idMyAlitalia;
    private String canale;
    private String language;
    private String market;
    private String conversationID;

    public GetCrmDataInfoRequest() {
    }

    public GetCrmDataInfoRequest(String tid, String sid) {
        super(tid, sid);
    }

    public String getIdMyAlitalia() {
        return idMyAlitalia;
    }

    public void setIdMyAlitalia(String idMyAlitalia) {
        this.idMyAlitalia = idMyAlitalia;
    }

    public String getCanale() {
        return canale;
    }

    public void setCanale(String canale) {
        this.canale = canale;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

    @Override
    public String toString() {
        return "GetCrmDataInfoRequest{" +
                "idMyAlitalia='" + idMyAlitalia + '\'' +
                ", canale='" + canale + '\'' +
                ", language='" + language + '\'' +
                ", market='" + market + '\'' +
                ", conversationID='" + conversationID + '\'' +
                '}';
    }
}
