package com.alitalia.aem.common.data.home.enumerations;

public enum CabinEnum {

    ECONOMY("Economy"),
    PREMIUM_ECONOMY("PremiumEconomy"),
    BUSINESS("Business"),
    MAGNIFICA("Magnifica");
    
    private final String value;

    CabinEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CabinEnum fromValue(String v) {
        for (CabinEnum c: CabinEnum.values()) {
            if (c.value.equalsIgnoreCase(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}