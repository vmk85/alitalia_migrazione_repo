package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.checkinselectedpassenger.response.Outcome;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinSelectedPassengerResponse extends BaseResponse {
	
	private Outcome outcome;

	public CheckinSelectedPassengerResponse() {}

	public CheckinSelectedPassengerResponse(String tid, String sid) {
		super(tid, sid);
	}

	public Outcome getOutcome() {
		return outcome;
	}

	public void setOutcome(Outcome outcome) {
		this.outcome = outcome;
	}
	
}
