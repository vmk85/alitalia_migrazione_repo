
package com.alitalia.aem.common.data.checkinrest.model.endpayment.request;

public class FraudNetParam {


    private String name;

    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
