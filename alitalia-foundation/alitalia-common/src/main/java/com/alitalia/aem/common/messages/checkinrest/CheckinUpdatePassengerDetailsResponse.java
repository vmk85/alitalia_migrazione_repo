package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.updatepassengerdetails.response.UpdatePassengerDetailsResp;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinUpdatePassengerDetailsResponse extends BaseResponse{
	
	private UpdatePassengerDetailsResp _updatepassengerdetailsResp;

	public UpdatePassengerDetailsResp get_updatepassengerdetailsResp() {
		return _updatepassengerdetailsResp;
	}

	public void set_updatepassengerdetailsResp(UpdatePassengerDetailsResp _updatepassengerdetailsResp) {
		this._updatepassengerdetailsResp = _updatepassengerdetailsResp;
	}

	public CheckinUpdatePassengerDetailsResponse(String tid, String sid) {
		super(tid, sid);
	}

	public CheckinUpdatePassengerDetailsResponse() {}
}
