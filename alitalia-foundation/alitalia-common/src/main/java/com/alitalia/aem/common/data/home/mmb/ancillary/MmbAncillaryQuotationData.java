package com.alitalia.aem.common.data.home.mmb.ancillary;

import java.math.BigDecimal;

public class MmbAncillaryQuotationData {

	private Integer maxQuantity;
	private Integer minQuantity;
	private BigDecimal price;

	public Integer getMaxQuantity() {
		return maxQuantity;
	}

	public void setMaxQuantity(Integer maxQuantity) {
		this.maxQuantity = maxQuantity;
	}

	public Integer getMinQuantity() {
		return minQuantity;
	}

	public void setMinQuantity(Integer minQuantity) {
		this.minQuantity = minQuantity;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((maxQuantity == null) ? 0 : maxQuantity.hashCode());
		result = prime * result
				+ ((minQuantity == null) ? 0 : minQuantity.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbAncillaryQuotationData other = (MmbAncillaryQuotationData) obj;
		if (maxQuantity == null) {
			if (other.maxQuantity != null)
				return false;
		} else if (!maxQuantity.equals(other.maxQuantity))
			return false;
		if (minQuantity == null) {
			if (other.minQuantity != null)
				return false;
		} else if (!minQuantity.equals(other.minQuantity))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbQuotationData [maxQuantity=" + maxQuantity
				+ ", minQuantity=" + minQuantity + ", price=" + price + "]";
	}

}
