
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WeightInfo_ {

    @SerializedName("bagWeightLimit")
    @Expose
    private BagWeightLimit__ bagWeightLimit;
    @SerializedName("bagWeightRestriction")
    @Expose
    private String bagWeightRestriction;
    @SerializedName("bagWeightRestrictionSpecified")
    @Expose
    private Boolean bagWeightRestrictionSpecified;

    public BagWeightLimit__ getBagWeightLimit() {
        return bagWeightLimit;
    }

    public void setBagWeightLimit(BagWeightLimit__ bagWeightLimit) {
        this.bagWeightLimit = bagWeightLimit;
    }

    public String getBagWeightRestriction() {
        return bagWeightRestriction;
    }

    public void setBagWeightRestriction(String bagWeightRestriction) {
        this.bagWeightRestriction = bagWeightRestriction;
    }

    public Boolean getBagWeightRestrictionSpecified() {
        return bagWeightRestrictionSpecified;
    }

    public void setBagWeightRestrictionSpecified(Boolean bagWeightRestrictionSpecified) {
        this.bagWeightRestrictionSpecified = bagWeightRestrictionSpecified;
    }

}
