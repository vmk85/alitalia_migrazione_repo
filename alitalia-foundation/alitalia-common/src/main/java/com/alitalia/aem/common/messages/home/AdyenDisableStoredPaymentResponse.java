package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.AdyenBaseResponse;

public class AdyenDisableStoredPaymentResponse extends AdyenBaseResponse {
	/*
	{
	  "response": "[detail-successfully-disabled]",
	  "conversationID": "1234"
	}
	*/
	
	private String response;

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result	+ ((response == null) ? 0 : response.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdyenDisableStoredPaymentResponse other = (AdyenDisableStoredPaymentResponse) obj;
		if (response == null) {
			if (other.response != null)
				return false;
		} else if (!response.equals(other.response))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "RegisterCreditCardRequest ["
				+ "response=" + response
				+ "conversationID=" + super.getConversationID()
				+ "]";
	}
}