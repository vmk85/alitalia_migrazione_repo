
package com.alitalia.aem.common.data.checkinrest.model.updatepassenger.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdatePassengerResp {

    @SerializedName("passengers")
    @Expose
    private List<Passenger> passengers = null;
    @SerializedName("conversationID")
    @Expose
    private String conversationID;

    public List<Passenger> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<Passenger> passengers) {
        this.passengers = passengers;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

}
