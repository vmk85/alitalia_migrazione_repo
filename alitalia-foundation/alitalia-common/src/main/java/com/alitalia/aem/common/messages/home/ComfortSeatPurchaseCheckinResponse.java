package com.alitalia.aem.common.messages.home;

import java.math.BigDecimal;

import com.alitalia.aem.common.messages.BaseResponse;

public class ComfortSeatPurchaseCheckinResponse extends BaseResponse {

	private BigDecimal amount;
	private Boolean comfortSeatPaid;
	private String currency;
	private String seat;
	
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public Boolean getComfortSeatPaid() {
		return comfortSeatPaid;
	}
	public void setComfortSeatPaid(Boolean comfortSeatPaid) {
		this.comfortSeatPaid = comfortSeatPaid;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getSeat() {
		return seat;
	}
	public void setSeat(String seat) {
		this.seat = seat;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result
				+ ((comfortSeatPaid == null) ? 0 : comfortSeatPaid.hashCode());
		result = prime * result
				+ ((currency == null) ? 0 : currency.hashCode());
		result = prime * result + ((seat == null) ? 0 : seat.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComfortSeatPurchaseCheckinResponse other = (ComfortSeatPurchaseCheckinResponse) obj;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (comfortSeatPaid == null) {
			if (other.comfortSeatPaid != null)
				return false;
		} else if (!comfortSeatPaid.equals(other.comfortSeatPaid))
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (seat == null) {
			if (other.seat != null)
				return false;
		} else if (!seat.equals(other.seat))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ComfortSeatPurchaseCheckinResponse [amount=" + amount
				+ ", comfortSeatPaid=" + comfortSeatPaid + ", currency="
				+ currency + ", seat=" + seat + ", toString()="
				+ super.toString() + "]";
	}

}
