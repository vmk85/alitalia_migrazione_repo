package com.alitalia.aem.common.data.home;

import java.util.ArrayList;
import java.util.List;

import com.alitalia.aem.common.data.home.enumerations.SearchTypeEnum;

public class AwardFlightDetailSearchData extends AwardSearchData {

    private Boolean multiSlice;
    private Boolean neutral;
    private List<PassengerBaseData> passengers;

    public AwardFlightDetailSearchData() {
		super();
		this.setType(SearchTypeEnum.AWARD_FLIGHT_DETAIL_SEARCH);
	}

    public AwardFlightDetailSearchData(AwardFlightDetailSearchData clone) {
		super(clone);
		this.multiSlice = clone.multiSlice;
		this.neutral = clone.neutral;
		if (clone.passengers != null) {
			this.passengers = new ArrayList<PassengerBaseData>();
			for (PassengerBaseData clonePassengerElement : clone.passengers) {
				PassengerBaseData passengerElement = new PassengerBaseData(clonePassengerElement);
				this.passengers.add(passengerElement);
			}
		}
	}

    public AwardFlightDetailSearchData(SearchTypeEnum searchType) {
		super();
		this.setType(searchType);
	}

    public Boolean isMultiSlice() {
		return multiSlice;
	}

    public void setMultiSlice(Boolean multiSlice) {
		this.multiSlice = multiSlice;
	}

    public Boolean isNeutral() {
		return neutral;
	}

    public void setNeutral(Boolean neutral) {
		this.neutral = neutral;
	}

    public List<PassengerBaseData> getPassengers() {
		return passengers;
	}

    public void setPassengers(List<PassengerBaseData> passengers) {
		this.passengers = passengers;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((multiSlice == null) ? 0 : multiSlice.hashCode());
		result = prime * result + ((neutral == null) ? 0 : neutral.hashCode());
		result = prime * result
				+ ((passengers == null) ? 0 : passengers.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AwardFlightDetailSearchData other = (AwardFlightDetailSearchData) obj;
		if (multiSlice == null) {
			if (other.multiSlice != null)
				return false;
		} else if (!multiSlice.equals(other.multiSlice))
			return false;
		if (neutral == null) {
			if (other.neutral != null)
				return false;
		} else if (!neutral.equals(other.neutral))
			return false;
		if (passengers == null) {
			if (other.passengers != null)
				return false;
		} else if (!passengers.equals(other.passengers))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AwardFlightDetailSearchData [multiSlice=" + multiSlice
				+ ", neutral=" + neutral + ", passengers=" + passengers
				+ ", getCug()=" + getCug() + ", getDestinations()="
				+ getDestinations() + ", getInnerSearch()=" + getInnerSearch()
				+ ", getMarket()=" + getMarket() + ", isOnlyDirectFlight()="
				+ isOnlyDirectFlight() + ", getPassengerNumbers()="
				+ getPassengerNumbers() + ", getSearchCabin()="
				+ getSearchCabin() + ", getSearchCabinType()="
				+ getSearchCabinType() + ", getType()=" + getType()
				+ ", getResidency()=" + getResidency() + ", getId()=" + getId()
				+ ", getSessionId()=" + getSessionId() + ", getSolutionSet()="
				+ getSolutionSet() + ", getSolutionId()=" + getSolutionId()
				+ ", getRefreshSolutionId()=" + getRefreshSolutionId()
				+ ", getFarmId()=" + getFarmId() + ", getProperties()="
				+ getProperties() + "]";
	}

}
