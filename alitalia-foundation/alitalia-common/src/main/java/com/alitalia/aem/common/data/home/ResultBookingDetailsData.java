package com.alitalia.aem.common.data.home;

import java.util.List;

public class ResultBookingDetailsData {

    private List<ResultBookingDetailsSolutionData> solutionField;

	public List<ResultBookingDetailsSolutionData> getSolutionField() {
		return solutionField;
	}

	public void setSolutionField(List<ResultBookingDetailsSolutionData> solutionField) {
		this.solutionField = solutionField;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((solutionField == null) ? 0 : solutionField.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingDetailsData other = (ResultBookingDetailsData) obj;
		if (solutionField == null) {
			if (other.solutionField != null)
				return false;
		} else if (!solutionField.equals(other.solutionField))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ResultBookingDetailsData [solutionField=" + solutionField + "]";
	}

}
