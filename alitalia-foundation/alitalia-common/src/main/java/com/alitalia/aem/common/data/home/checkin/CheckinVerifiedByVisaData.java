package com.alitalia.aem.common.data.home.checkin;

public class CheckinVerifiedByVisaData {
	
	private Integer id;
	private String threeDSecureOpaqueParams64Encoded;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getThreeDSecureOpaqueParams64Encoded() {
		return threeDSecureOpaqueParams64Encoded;
	}
	public void setThreeDSecureOpaqueParams64Encoded(
			String threeDSecureOpaqueParams64Encoded) {
		this.threeDSecureOpaqueParams64Encoded = threeDSecureOpaqueParams64Encoded;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime
				* result
				+ ((threeDSecureOpaqueParams64Encoded == null) ? 0
						: threeDSecureOpaqueParams64Encoded.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinVerifiedByVisaData other = (CheckinVerifiedByVisaData) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (threeDSecureOpaqueParams64Encoded == null) {
			if (other.threeDSecureOpaqueParams64Encoded != null)
				return false;
		} else if (!threeDSecureOpaqueParams64Encoded
				.equals(other.threeDSecureOpaqueParams64Encoded))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CheckinVerifiedByVisaData [id=" + id
				+ ", threeDSecureOpaqueParams64Encoded="
				+ threeDSecureOpaqueParams64Encoded + "]";
	}

}
