package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.countrystates.CountryStates;
import com.alitalia.aem.common.messages.BaseResponse;

public class CountryStatesResponse extends BaseResponse {

	private CountryStates countryStates;

	public CountryStatesResponse() {}

	public CountryStatesResponse(String tid, String sid) {
		super(tid, sid);
	}

	public CountryStates getCountryStates() {
		return countryStates;
	}

	public void setCountryStates(CountryStates countryStates) {
		this.countryStates = countryStates;
	}
	
}
