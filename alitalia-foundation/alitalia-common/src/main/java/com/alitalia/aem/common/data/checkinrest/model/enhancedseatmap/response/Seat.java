
package com.alitalia.aem.common.data.checkinrest.model.enhancedseatmap.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Seat {

    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("availability")
    @Expose
    private String availability;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("price")
    @Expose
    private Float price;
    @SerializedName("confortSeat")
    @Expose
    private Boolean confortSeat;
    @SerializedName("occupatoDa")
    @Expose
    private String occupatoDa;
    @SerializedName("cognome")
    @Expose
    private String cognome;
    @SerializedName("nome")
    @Expose
    private String nome;
    @SerializedName("pnr")
    @Expose
    private String pnr;

    private String priceString = "0.0";

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Boolean getConfortSeat() {
        return confortSeat;
    }

    public void setConfortSeat(Boolean confortSeat) {
        this.confortSeat = confortSeat;
    }

    public String getOccupatoDa() {
        return occupatoDa;
    }

    public void setOccupatoDa(String occupatoDa) {
        this.occupatoDa = occupatoDa;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public String getPriceString() {
        return priceString;
    }

    public void setPriceString(String priceString) {
        this.priceString = priceString;
    }

}
