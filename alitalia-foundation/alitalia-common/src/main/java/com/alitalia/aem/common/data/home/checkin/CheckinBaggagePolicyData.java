package com.alitalia.aem.common.data.home.checkin;

import java.math.BigDecimal;

public class CheckinBaggagePolicyData {
	
	private Integer id;
	private BigDecimal airportFareForFirstBaggage;
	private BigDecimal airportFareForSpecialPassenger;
	private BigDecimal airportFare;
	private int allowanceQuantity;
	private int allowanceWeight;
	private String currency;
	private Boolean enableForFirstBaggage;
	private int extraBaggageMaxQuantity;
	private int extraBaggageWeight;
	private BigDecimal webFareForFirstBaggage;
	private BigDecimal webFareForSpecialPassenger;
	private BigDecimal WebFare;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public BigDecimal getAirportFareForFirstBaggage() {
		return airportFareForFirstBaggage;
	}
	public void setAirportFareForFirstBaggage(BigDecimal airportFareForFirstBaggage) {
		this.airportFareForFirstBaggage = airportFareForFirstBaggage;
	}
	public BigDecimal getAirportFareForSpecialPassenger() {
		return airportFareForSpecialPassenger;
	}
	public void setAirportFareForSpecialPassenger(
			BigDecimal airportFareForSpecialPassenger) {
		this.airportFareForSpecialPassenger = airportFareForSpecialPassenger;
	}
	public BigDecimal getAirportFare() {
		return airportFare;
	}
	public void setAirportFare(BigDecimal airportFare) {
		this.airportFare = airportFare;
	}
	public int getAllowanceQuantity() {
		return allowanceQuantity;
	}
	public void setAllowanceQuantity(int allowanceQuantity) {
		this.allowanceQuantity = allowanceQuantity;
	}
	public int getAllowanceWeight() {
		return allowanceWeight;
	}
	public void setAllowanceWeight(int allowanceWeight) {
		this.allowanceWeight = allowanceWeight;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public Boolean getEnableForFirstBaggage() {
		return enableForFirstBaggage;
	}
	public void setEnableForFirstBaggage(Boolean enableForFirstBaggage) {
		this.enableForFirstBaggage = enableForFirstBaggage;
	}
	public int getExtraBaggageMaxQuantity() {
		return extraBaggageMaxQuantity;
	}
	public void setExtraBaggageMaxQuantity(int extraBaggageMaxQuantity) {
		this.extraBaggageMaxQuantity = extraBaggageMaxQuantity;
	}
	public int getExtraBaggageWeight() {
		return extraBaggageWeight;
	}
	public void setExtraBaggageWeight(int extraBaggageWeight) {
		this.extraBaggageWeight = extraBaggageWeight;
	}
	public BigDecimal getWebFareForFirstBaggage() {
		return webFareForFirstBaggage;
	}
	public void setWebFareForFirstBaggage(BigDecimal webFareForFirstBaggage) {
		this.webFareForFirstBaggage = webFareForFirstBaggage;
	}
	public BigDecimal getWebFareForSpecialPassenger() {
		return webFareForSpecialPassenger;
	}
	public void setWebFareForSpecialPassenger(BigDecimal webFareForSpecialPassenger) {
		this.webFareForSpecialPassenger = webFareForSpecialPassenger;
	}
	public BigDecimal getWebFare() {
		return WebFare;
	}
	public void setWebFare(BigDecimal webFare) {
		WebFare = webFare;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((WebFare == null) ? 0 : WebFare.hashCode());
		result = prime * result
				+ ((airportFare == null) ? 0 : airportFare.hashCode());
		result = prime
				* result
				+ ((airportFareForFirstBaggage == null) ? 0
						: airportFareForFirstBaggage.hashCode());
		result = prime
				* result
				+ ((airportFareForSpecialPassenger == null) ? 0
						: airportFareForSpecialPassenger.hashCode());
		result = prime * result + allowanceQuantity;
		result = prime * result + allowanceWeight;
		result = prime * result
				+ ((currency == null) ? 0 : currency.hashCode());
		result = prime
				* result
				+ ((enableForFirstBaggage == null) ? 0 : enableForFirstBaggage
						.hashCode());
		result = prime * result + extraBaggageMaxQuantity;
		result = prime * result + extraBaggageWeight;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime
				* result
				+ ((webFareForFirstBaggage == null) ? 0
						: webFareForFirstBaggage.hashCode());
		result = prime
				* result
				+ ((webFareForSpecialPassenger == null) ? 0
						: webFareForSpecialPassenger.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinBaggagePolicyData other = (CheckinBaggagePolicyData) obj;
		if (WebFare == null) {
			if (other.WebFare != null)
				return false;
		} else if (!WebFare.equals(other.WebFare))
			return false;
		if (airportFare == null) {
			if (other.airportFare != null)
				return false;
		} else if (!airportFare.equals(other.airportFare))
			return false;
		if (airportFareForFirstBaggage == null) {
			if (other.airportFareForFirstBaggage != null)
				return false;
		} else if (!airportFareForFirstBaggage
				.equals(other.airportFareForFirstBaggage))
			return false;
		if (airportFareForSpecialPassenger == null) {
			if (other.airportFareForSpecialPassenger != null)
				return false;
		} else if (!airportFareForSpecialPassenger
				.equals(other.airportFareForSpecialPassenger))
			return false;
		if (allowanceQuantity != other.allowanceQuantity)
			return false;
		if (allowanceWeight != other.allowanceWeight)
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (enableForFirstBaggage == null) {
			if (other.enableForFirstBaggage != null)
				return false;
		} else if (!enableForFirstBaggage.equals(other.enableForFirstBaggage))
			return false;
		if (extraBaggageMaxQuantity != other.extraBaggageMaxQuantity)
			return false;
		if (extraBaggageWeight != other.extraBaggageWeight)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (webFareForFirstBaggage == null) {
			if (other.webFareForFirstBaggage != null)
				return false;
		} else if (!webFareForFirstBaggage.equals(other.webFareForFirstBaggage))
			return false;
		if (webFareForSpecialPassenger == null) {
			if (other.webFareForSpecialPassenger != null)
				return false;
		} else if (!webFareForSpecialPassenger
				.equals(other.webFareForSpecialPassenger))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CheckinBaggagePolicyData [id=" + id
				+ ", airportFareForFirstBaggage=" + airportFareForFirstBaggage
				+ ", airportFareForSpecialPassenger="
				+ airportFareForSpecialPassenger + ", airportFare="
				+ airportFare + ", allowanceQuantity=" + allowanceQuantity
				+ ", allowanceWeight=" + allowanceWeight + ", currency="
				+ currency + ", enableForFirstBaggage=" + enableForFirstBaggage
				+ ", extraBaggageMaxQuantity=" + extraBaggageMaxQuantity
				+ ", extraBaggageWeight=" + extraBaggageWeight
				+ ", webFareForFirstBaggage=" + webFareForFirstBaggage
				+ ", webFareForSpecialPassenger=" + webFareForSpecialPassenger
				+ ", WebFare=" + WebFare + ", toString()=" + super.toString()
				+ "]";
	}

}
