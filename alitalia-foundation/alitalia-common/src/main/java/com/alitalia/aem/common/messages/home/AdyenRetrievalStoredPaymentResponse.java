package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.AdyenRetrievalStoredPaymentData;
import com.alitalia.aem.common.messages.BaseResponse;

public class AdyenRetrievalStoredPaymentResponse extends BaseResponse {
	/*
	[
	  {
	    "creationDate": "2018-09-06T14:24:40Z",
	    "recurringDetailList": [
	      {
	        "additionalDataList": [
	          {
	            "key": "cardBin",
	            "cardBin": "444433"
	          }
	        ],
	        "card": {
	          "cvc": null,
	          "expiryMonth": "8",
	          "expiryYear": "2018",
	          "holderName": "Test Uno",
	          "number": "1111",
	          "type": "visa"
	        },
	        "alias": "P106674586198742",
	        "aliasType": "Default",
	        "creationDate": "2018-09-06T14:24:40Z",
	        "firstPspReference": "8835362438795089",
	        "recurringDetailReference": "8315362438809277",
	        "recurringList": [
	          {
	            "contract": "RECURRING"
	          }
	        ],
	        "paymentMethodVariant": "visacorporatecredit",
	        "variant": "visa",
	        "address": {
	          "city": "Roma",
	          "country": "IT",
	          "postalCode": "00100",
	          "province": "RM",
	          "street": "via roma 100"
	        },
	        "bank": null,
	        "shopperName": null
	      },
	      {
	        "additionalDataList": [
	          {
	            "key": "cardBin",
	            "cardBin": "677179"
	          }
	        ],
	        "card": {
	          "cvc": null,
	          "expiryMonth": "10",
	          "expiryYear": "2020",
	          "holderName": "Test Uno",
	          "number": "0004",
	          "type": "mc"
	        },
	        "alias": "P663519338012674",
	        "aliasType": "Default",
	        "creationDate": "2018-09-06T15:36:59Z",
	        "firstPspReference": "8525362482191933",
	        "recurringDetailReference": "8415362482191209",
	        "recurringList": [
	          {
	            "contract": "RECURRING"
	          }
	        ],
	        "paymentMethodVariant": "mc",
	        "variant": "mc",
	        "address": {
	          "city": "Roma",
	          "country": "IT",
	          "postalCode": "00100",
	          "province": "RM",
	          "street": "via roma 100"
	        },
	        "bank": null,
	        "shopperName": null
	      },
	      {
	        "additionalDataList": [
	          {
	            "key": "cardBin",
	            "cardBin": "370000"
	          }
	        ],
	        "card": {
	          "cvc": null,
	          "expiryMonth": "10",
	          "expiryYear": "2020",
	          "holderName": "Test Uno",
	          "number": "0002",
	          "type": "amex"
	        },
	        "alias": "K961600404152739",
	        "aliasType": "Default",
	        "creationDate": "2018-09-10T09:57:43Z",
	        "firstPspReference": "8535365734622231",
	        "recurringDetailReference": "8315365734635718",
	        "recurringList": [
	          {
	            "contract": "RECURRING"
	          }
	        ],
	        "paymentMethodVariant": "amex",
	        "variant": "amex",
	        "address": {
	          "city": "Roma",
	          "country": "IT",
	          "postalCode": "00100",
	          "province": "RM",
	          "street": "via roma 100"
	        },
	        "bank": null,
	        "shopperName": null
	      },
	      {
	        "additionalDataList": [
	          {
	            "key": "cardBin",
	            "cardBin": "360066"
	          }
	        ],
	        "card": {
	          "cvc": null,
	          "expiryMonth": "10",
	          "expiryYear": "2020",
	          "holderName": "Test Uno",
	          "number": "3344",
	          "type": "diners"
	        },
	        "alias": "A760956278894287",
	        "aliasType": "Default",
	        "creationDate": "2018-09-10T10:18:41Z",
	        "firstPspReference": "8535365747210743",
	        "recurringDetailReference": "8315365747216647",
	        "recurringList": [
	          {
	            "contract": "RECURRING"
	          }
	        ],
	        "paymentMethodVariant": "diners",
	        "variant": "diners",
	        "address": {
	          "city": "Roma",
	          "country": "IT",
	          "postalCode": "00100",
	          "province": "RM",
	          "street": "via roma 100"
	        },
	        "bank": null,
	        "shopperName": null
	      },
	      {
	        "additionalDataList": [
	          {
	            "key": "cardBin",
	            "cardBin": "400102"
	          }
	        ],
	        "card": {
	          "cvc": null,
	          "expiryMonth": "10",
	          "expiryYear": "2020",
	          "holderName": "Test Uno",
	          "number": "0009",
	          "type": "visa"
	        },
	        "alias": "F826190531534372",
	        "aliasType": "Default",
	        "creationDate": "2018-09-10T10:23:41Z",
	        "firstPspReference": "8825365750203634",
	        "recurringDetailReference": "8315365750219587",
	        "recurringList": [
	          {
	            "contract": "RECURRING"
	          }
	        ],
	        "paymentMethodVariant": "electron",
	        "variant": "visa",
	        "address": {
	          "city": "Roma",
	          "country": "IT",
	          "postalCode": "00100",
	          "province": "RM",
	          "street": "via roma 100"
	        },
	        "bank": null,
	        "shopperName": null
	      },
	      {
	        "additionalDataList": [
	          {
	            "key": "cardBin",
	            "cardBin": "135410"
	          }
	        ],
	        "card": {
	          "cvc": null,
	          "expiryMonth": "10",
	          "expiryYear": "2020",
	          "holderName": "Test Uno",
	          "number": "4955",
	          "type": "uatp"
	        },
	        "alias": "C725475035333744",
	        "aliasType": "Default",
	        "creationDate": "2018-09-10T12:30:54Z",
	        "firstPspReference": "8825365826548111",
	        "recurringDetailReference": "8315365826541691",
	        "recurringList": [
	          {
	            "contract": "RECURRING"
	          }
	        ],
	        "paymentMethodVariant": "uatp",
	        "variant": "uatp",
	        "address": {
	          "city": "Roma",
	          "country": "IT",
	          "postalCode": "00100",
	          "province": "RM",
	          "street": "via roma 100"
	        },
	        "bank": null,
	        "shopperName": null
	      }
	    ],
	    "lastKnownShopperEmail": "test@test.com",
	    "shopperReference": "39fbe13b0ffd46ef9ee08d09ec577034",
	    "conversationID": "987654321"
	  }
	]
	*/
	
	private List<AdyenRetrievalStoredPaymentData> adyenRetrievalStoredPaymentList;
	
	public List<AdyenRetrievalStoredPaymentData> getAdyenRetrievalStoredPaymentList() {
		return adyenRetrievalStoredPaymentList;
	}
	public void setAdyenRetrievalStoredPaymentList(List<AdyenRetrievalStoredPaymentData> adyenRetrievalStoredPaymentList) {
		this.adyenRetrievalStoredPaymentList = adyenRetrievalStoredPaymentList;
	}

}