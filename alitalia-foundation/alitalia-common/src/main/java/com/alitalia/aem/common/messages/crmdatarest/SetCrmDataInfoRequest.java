package com.alitalia.aem.common.messages.crmdatarest;

import com.alitalia.aem.common.data.crmdatarest.model.*;
import com.alitalia.aem.common.messages.BaseRequest;

import java.util.List;

public class SetCrmDataInfoRequest extends BaseRequest {

    private InfoCliente infoCliente;
    private List<CanaleComunicazionePreferito> canaleComunicazionePreferito;
    private List<Consenso> listaConsensi;
    private String email;
    private boolean unsubscribeNewsletter = false;
    private String frequenzaComunicazioni;
    private RecapitiTelefonici recapitiTelefonici;
    private List<Documento> elencoDocumenti;
    private PreferenzeViaggio preferenzeViaggio;
    private PreferenzePersonali preferenzePersonali;
    private String idMilleMiglia;
    private String idMyAlitalia;
    private String canale;
    private String language;
    private String market;
    private String conversationID;

    public SetCrmDataInfoRequest() {
    }

    public SetCrmDataInfoRequest(String tid, String sid) {
        super(tid, sid);
    }

    public InfoCliente getInfoCliente() {
        return infoCliente;
    }

    public void setInfoCliente(InfoCliente infoCliente) {
        this.infoCliente = infoCliente;
    }

    public List<CanaleComunicazionePreferito> getCanaleComunicazionePreferito() {
        return canaleComunicazionePreferito;
    }

    public void setCanaleComunicazionePreferito(List<CanaleComunicazionePreferito> canaleComunicazionePreferito) {
        this.canaleComunicazionePreferito = canaleComunicazionePreferito;
    }

    public List<Consenso> getListaConsensi() {
        return listaConsensi;
    }

    public void setListaConsensi(List<Consenso> listaConsensi) {
        this.listaConsensi = listaConsensi;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isUnsubscribeNewsletter() {
        return unsubscribeNewsletter;
    }

    public void setUnsubscribeNewsletter(boolean unsubscribeNewsletter) {
        this.unsubscribeNewsletter = unsubscribeNewsletter;
    }

    public String getFrequenzaComunicazioni() {
        return frequenzaComunicazioni;
    }

    public void setFrequenzaComunicazioni(String frequenzaComunicazioni) {
        this.frequenzaComunicazioni = frequenzaComunicazioni;
    }

    public RecapitiTelefonici getRecapitiTelefonici() {
        return recapitiTelefonici;
    }

    public void setRecapitiTelefonici(RecapitiTelefonici recapitiTelefonici) {
        this.recapitiTelefonici = recapitiTelefonici;
    }

    public List<Documento> getElencoDocumenti() {
        return elencoDocumenti;
    }

    public void setElencoDocumenti(List<Documento> elencoDocumenti) {
        this.elencoDocumenti = elencoDocumenti;
    }

    public PreferenzeViaggio getPreferenzeViaggio() {
        return preferenzeViaggio;
    }

    public void setPreferenzeViaggio(PreferenzeViaggio preferenzeViaggio) {
        this.preferenzeViaggio = preferenzeViaggio;
    }

    public PreferenzePersonali getPreferenzePersonali() {
        return preferenzePersonali;
    }

    public void setPreferenzePersonali(PreferenzePersonali preferenzePersonali) {
        this.preferenzePersonali = preferenzePersonali;
    }

    public String getIdMilleMiglia() {
        return idMilleMiglia;
    }

    public void setIdMilleMiglia(String idMilleMiglia) {
        this.idMilleMiglia = idMilleMiglia;
    }

    public String getIdMyAlitalia() {
        return idMyAlitalia;
    }

    public void setIdMyAlitalia(String idMyAlitalia) {
        this.idMyAlitalia = idMyAlitalia;
    }

    public String getCanale() {
        return canale;
    }

    public void setCanale(String canale) {
        this.canale = canale;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

    @Override
    public String toString() {
        return "SetCrmDataInfoRequest{" +
                "infoCliente=" + infoCliente +
                ", canaleComunicazionePreferito=" + canaleComunicazionePreferito +
                ", listaConsensi=" + listaConsensi +
                ", email='" + email + '\'' +
                ", unsubscribeNewsletter=" + unsubscribeNewsletter +
                ", frequenzaComunicazioni='" + frequenzaComunicazioni + '\'' +
                ", recapitiTelefonici=" + recapitiTelefonici +
                ", elencoDocumenti=" + elencoDocumenti +
                ", preferenzeViaggio=" + preferenzeViaggio +
                ", preferenzePersonali=" + preferenzePersonali +
                ", idMilleMiglia='" + idMilleMiglia + '\'' +
                ", idMyAlitalia='" + idMyAlitalia + '\'' +
                ", canale='" + canale + '\'' +
                ", language='" + language + '\'' +
                ", market='" + market + '\'' +
                ", conversationID='" + conversationID + '\'' +
                '}';
    }
}
