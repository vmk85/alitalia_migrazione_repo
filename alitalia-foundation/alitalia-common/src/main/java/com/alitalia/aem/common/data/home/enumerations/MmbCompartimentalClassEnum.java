package com.alitalia.aem.common.data.home.enumerations;

public enum MmbCompartimentalClassEnum {

    UNKNOWN("Unknown"),
    Y("Y"),
    P("P"),
    C("C"),
    J("J"),
    EP("EP"),
    F("F");
    private final String value;

    MmbCompartimentalClassEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MmbCompartimentalClassEnum fromValue(String v) {
        for (MmbCompartimentalClassEnum c: MmbCompartimentalClassEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
