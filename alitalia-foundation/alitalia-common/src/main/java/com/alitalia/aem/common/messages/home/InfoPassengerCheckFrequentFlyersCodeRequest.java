package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.messages.BaseRequest;

public class InfoPassengerCheckFrequentFlyersCodeRequest extends BaseRequest {
	
	private List<AdultPassengerData> adultPassengers;
	
	public InfoPassengerCheckFrequentFlyersCodeRequest() {
	}
	
	public InfoPassengerCheckFrequentFlyersCodeRequest(List<AdultPassengerData> adultPassengers) {
		super();
		this.adultPassengers = adultPassengers;
	}
	
	public InfoPassengerCheckFrequentFlyersCodeRequest(String tid, String sid) {
		super(tid, sid);
	}
	
	public InfoPassengerCheckFrequentFlyersCodeRequest(String tid, String sid, List<AdultPassengerData> adultPassengers) {
		super(tid, sid);
		this.adultPassengers = adultPassengers;
	}
	
	public List<AdultPassengerData> getAdultPassengers() {
		return adultPassengers;
	}

	public void setAdultPassengers(List<AdultPassengerData> adultPassengers) {
		this.adultPassengers = adultPassengers;
	}

	@Override
	public String toString() {
		return "InfoPassengerCheckFrequentFlyersCodeRequest [adultPassengers=" + adultPassengers + "]";
	}
	
}
