package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryCartItemData;
import com.alitalia.aem.common.messages.MmbBaseRequest;

public class WebCheckinUpdateCartRequest extends MmbBaseRequest {

    private Long cartId;
    private String client;
    private List<MmbAncillaryCartItemData> items;
    private String machineName;
    private String sessionId;

	public WebCheckinUpdateCartRequest(String tid, String sid) {
		super(tid, sid);
	}

	public WebCheckinUpdateCartRequest() {
		super();
	}

	public Long getCartId() {
		return cartId;
	}

	public void setCartId(Long cartId) {
		this.cartId = cartId;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public List<MmbAncillaryCartItemData> getItems() {
		return items;
	}

	public void setItems(List<MmbAncillaryCartItemData> items) {
		this.items = items;
	}

	public String getMachineName() {
		return machineName;
	}

	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((cartId == null) ? 0 : cartId.hashCode());
		result = prime * result + ((client == null) ? 0 : client.hashCode());
		result = prime * result + ((items == null) ? 0 : items.hashCode());
		result = prime * result
				+ ((machineName == null) ? 0 : machineName.hashCode());
		result = prime * result
				+ ((sessionId == null) ? 0 : sessionId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebCheckinUpdateCartRequest other = (WebCheckinUpdateCartRequest) obj;
		if (cartId == null) {
			if (other.cartId != null)
				return false;
		} else if (!cartId.equals(other.cartId))
			return false;
		if (client == null) {
			if (other.client != null)
				return false;
		} else if (!client.equals(other.client))
			return false;
		if (items == null) {
			if (other.items != null)
				return false;
		} else if (!items.equals(other.items))
			return false;
		if (machineName == null) {
			if (other.machineName != null)
				return false;
		} else if (!machineName.equals(other.machineName))
			return false;
		if (sessionId == null) {
			if (other.sessionId != null)
				return false;
		} else if (!sessionId.equals(other.sessionId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WebCheckinUpdateCartRequest [cartId=" + cartId + ", client="
				+ client + ", items=" + items + ", machineName=" + machineName
				+ ", sessionId=" + sessionId + "]";
	}
}