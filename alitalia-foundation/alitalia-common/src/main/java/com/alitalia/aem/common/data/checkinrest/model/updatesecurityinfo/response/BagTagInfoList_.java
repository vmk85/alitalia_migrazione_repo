
package com.alitalia.aem.common.data.checkinrest.model.updatesecurityinfo.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BagTagInfoList_ {

    @SerializedName("bagTagInfo")
    @Expose
    private List<BagTagInfo__> bagTagInfo = null;
    @SerializedName("totalWeightAndUnit")
    @Expose
    private TotalWeightAndUnit_ totalWeightAndUnit;

    public List<BagTagInfo__> getBagTagInfo() {
        return bagTagInfo;
    }

    public void setBagTagInfo(List<BagTagInfo__> bagTagInfo) {
        this.bagTagInfo = bagTagInfo;
    }

    public TotalWeightAndUnit_ getTotalWeightAndUnit() {
        return totalWeightAndUnit;
    }

    public void setTotalWeightAndUnit(TotalWeightAndUnit_ totalWeightAndUnit) {
        this.totalWeightAndUnit = totalWeightAndUnit;
    }

}
