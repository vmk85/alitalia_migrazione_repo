package com.alitalia.aem.common.messages.checkinrest;

import java.util.List;

import com.alitalia.aem.common.data.checkinrest.model.checkinselectedpassenger.request.Passenger;
import com.alitalia.aem.common.messages.BaseRequest;
import com.google.gson.annotations.Expose;

public class CheckinSelectedPassengerRequest extends BaseRequest {

	@Expose
	private String pnr;

	@Expose
	private String flight;

	@Expose
	private String airline;

	@Expose
	private String origin;

	@Expose
	private String departureDate;

	@Expose
	private List<Passenger> passengers = null;

	@Expose
	private String language;

	@Expose
	private String market;

	@Expose
	private String conversationID;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getFlight() {
		return flight;
	}

	public void setFlight(String flight) {
		this.flight = flight;
	}

	public String getAirline() {
		return airline;
	}

	public void setAirline(String airline) {
		this.airline= airline;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public List<Passenger> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<Passenger> passengers) {
		this.passengers = passengers;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public String getConversationID() {
		return conversationID;
	}

	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}

	//	private Passengers selectedPassengerRequest;

	public CheckinSelectedPassengerRequest() {}

	public CheckinSelectedPassengerRequest(String tid, String sid) {
		super(tid, sid);
	}

	//	public Passengers getSelectedPassengerRequest() {
		//		return selectedPassengerRequest;
		//	}
	//
	//	public void setSelectedPassengerRequest(Passengers selectedPassengerRequest) {
	//		this.selectedPassengerRequest = selectedPassengerRequest;
	//	}

}
