package com.alitalia.aem.common.data.checkinrest.model.fasttrack.request;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FlightFastTrackReq {

    @SerializedName("flightFastTrack")
    @Expose
    private List<FlightFastTrack> flightFastTrack = null;
    @SerializedName("pnr")
    @Expose
    private String pnr;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("market")
    @Expose
    private String market;
    @SerializedName("conversationID")
    @Expose
    private String conversationID;

    public List<FlightFastTrack> getFlightFastTrack() {
        return flightFastTrack;
    }

    public void setFlightFastTrack(List<FlightFastTrack> flightFastTrack) {
        this.flightFastTrack = flightFastTrack;
    }

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

}
