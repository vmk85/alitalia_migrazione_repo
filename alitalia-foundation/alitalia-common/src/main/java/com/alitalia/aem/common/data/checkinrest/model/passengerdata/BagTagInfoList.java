
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BagTagInfoList {

    @SerializedName("bagTagNumber")
    @Expose
    private String bagTagNumber;
    @SerializedName("bagTypeInfoList")
    @Expose
    private List<BagTypeInfoList> bagTypeInfoList = null;
    @SerializedName("weightAndSize")
    @Expose
    private WeightAndSize weightAndSize;
    @SerializedName("airline")
    @Expose
    private String airline;
    @SerializedName("bagOrigin")
    @Expose
    private String bagOrigin;
    @SerializedName("bagFinalDestination")
    @Expose
    private String bagFinalDestination;
    @SerializedName("aeItemID")
    @Expose
    private List<String> aeItemID = null;

    public String getBagTagNumber() {
        return bagTagNumber;
    }

    public void setBagTagNumber(String bagTagNumber) {
        this.bagTagNumber = bagTagNumber;
    }

    public List<BagTypeInfoList> getBagTypeInfoList() {
        return bagTypeInfoList;
    }

    public void setBagTypeInfoList(List<BagTypeInfoList> bagTypeInfoList) {
        this.bagTypeInfoList = bagTypeInfoList;
    }

    public WeightAndSize getWeightAndSize() {
        return weightAndSize;
    }

    public void setWeightAndSize(WeightAndSize weightAndSize) {
        this.weightAndSize = weightAndSize;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getBagOrigin() {
        return bagOrigin;
    }

    public void setBagOrigin(String bagOrigin) {
        this.bagOrigin = bagOrigin;
    }

    public String getBagFinalDestination() {
        return bagFinalDestination;
    }

    public void setBagFinalDestination(String bagFinalDestination) {
        this.bagFinalDestination = bagFinalDestination;
    }

    public List<String> getAeItemID() {
        return aeItemID;
    }

    public void setAeItemID(List<String> aeItemID) {
        this.aeItemID = aeItemID;
    }

}
