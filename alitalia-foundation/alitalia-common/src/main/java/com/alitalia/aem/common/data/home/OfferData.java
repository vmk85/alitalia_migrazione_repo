package com.alitalia.aem.common.data.home;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

import com.alitalia.aem.common.data.home.OfferData;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;

public class OfferData implements Comparable<OfferData>, Serializable {

	private AreaValueEnum area;
    private AirportData arrivalAirport;
    private BigDecimal bestPrice;
    private List<BestPriceData> bestPrices;
    private CabinEnum cabin;
    private AirportData departureAirport;
    private Calendar expireOfferData;
    private boolean isHomePageOffer;
    private String numberOfNights;
    private int priority;
    private List<PassengerNumbersData> passengerList;
    private RouteTypeEnum routeType;
	
    public AreaValueEnum getArea() {
		return area;
	}
	
    public void setArea(AreaValueEnum area) {
		this.area = area;
	}
	
    public AirportData getArrivalAirport() {
		return arrivalAirport;
	}
	
    public void setArrivalAirport(AirportData arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}
	
    public BigDecimal getBestPrice() {
		return bestPrice;
	}
	
    public void setBestPrice(BigDecimal bestPrice) {
		this.bestPrice = bestPrice;
	}
	
    public List<BestPriceData> getBestPrices() {
		return bestPrices;
	}
	
    public void setBestPrices(List<BestPriceData> bestPrices) {
		this.bestPrices = bestPrices;
	}
	
    public CabinEnum getCabin() {
		return cabin;
	}
	
    public void setCabin(CabinEnum cabin) {
		this.cabin = cabin;
	}
	
    public AirportData getDepartureAirport() {
		return departureAirport;
	}
	
    public void setDepartureAirport(AirportData departureAirport) {
		this.departureAirport = departureAirport;
	}
	
    public Calendar getExpireOfferData() {
		return expireOfferData;
	}
	
    public void setExpireOfferData(Calendar expireOfferData) {
		this.expireOfferData = expireOfferData;
	}
	
    public boolean isHomePageOffer() {
		return isHomePageOffer;
	}
	
    public void setHomePageOffer(boolean isHomePageOffer) {
		this.isHomePageOffer = isHomePageOffer;
	}
	
    public String getNumberOfNights() {
		return numberOfNights;
	}
	
    public void setNumberOfNights(String numberOfNights) {
		this.numberOfNights = numberOfNights;
	}
	
    public int getPriority() {
		return priority;
	}
	
    public void setPriority(int priority) {
		this.priority = priority;
	}
	
    public List<PassengerNumbersData> getPassengerList() {
		return passengerList;
	}
	
    public void setPassengerList(List<PassengerNumbersData> passengerList) {
		this.passengerList = passengerList;
	}
	
    public RouteTypeEnum getRouteType() {
		return routeType;
	}
	
    public void setRouteType(RouteTypeEnum routeType) {
		this.routeType = routeType;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((area == null) ? 0 : area.hashCode());
		result = prime * result + ((arrivalAirport == null) ? 0 : arrivalAirport.hashCode());
		result = prime * result + ((bestPrice == null) ? 0 : bestPrice.hashCode());
		result = prime * result + ((bestPrices == null) ? 0 : bestPrices.hashCode());
		result = prime * result + ((cabin == null) ? 0 : cabin.hashCode());
		result = prime * result + ((departureAirport == null) ? 0 : departureAirport.hashCode());
		result = prime * result + ((expireOfferData == null) ? 0 : expireOfferData.hashCode());
		result = prime * result + (isHomePageOffer ? 1231 : 1237);
		result = prime * result + ((numberOfNights == null) ? 0 : numberOfNights.hashCode());
		result = prime * result + ((passengerList == null) ? 0 : passengerList.hashCode());
		result = prime * result + priority;
		result = prime * result + ((routeType == null) ? 0 : routeType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OfferData other = (OfferData) obj;
		if (area != other.area)
			return false;
		if (arrivalAirport == null) {
			if (other.arrivalAirport != null)
				return false;
		} else if (!arrivalAirport.equals(other.arrivalAirport))
			return false;
		if (bestPrice == null) {
			if (other.bestPrice != null)
				return false;
		} else if (!bestPrice.equals(other.bestPrice))
			return false;
		if (bestPrices == null) {
			if (other.bestPrices != null)
				return false;
		} else if (!bestPrices.equals(other.bestPrices))
			return false;
		if (cabin != other.cabin)
			return false;
		if (departureAirport == null) {
			if (other.departureAirport != null)
				return false;
		} else if (!departureAirport.equals(other.departureAirport))
			return false;
		if (expireOfferData == null) {
			if (other.expireOfferData != null)
				return false;
		} else if (!expireOfferData.equals(other.expireOfferData))
			return false;
		if (isHomePageOffer != other.isHomePageOffer)
			return false;
		if (numberOfNights == null) {
			if (other.numberOfNights != null)
				return false;
		} else if (!numberOfNights.equals(other.numberOfNights))
			return false;
		if (passengerList == null) {
			if (other.passengerList != null)
				return false;
		} else if (!passengerList.equals(other.passengerList))
			return false;
		if (priority != other.priority)
			return false;
		if (routeType != other.routeType)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "OfferData [area=" + area + ", arrivalAirport=" + arrivalAirport
				+ ", bestPrice=" + bestPrice + ", bestPrices=" + bestPrices
				+ ", cabin=" + cabin + ", departureAirport=" + departureAirport
				+ ", expireOfferData=" + expireOfferData + ", isHomePageOffer="
				+ isHomePageOffer + ", numberOfNights=" + numberOfNights
				+ ", priority=" + priority + ", passengerList=" + passengerList
				+ ", routeType=" + routeType + "]";
	}
	
	@Override
	public int compareTo(OfferData o) {
		
		int result = 0;
		
		if (this.getPriority() < o.getPriority()) {
			result = -1;
		} else if (this.getPriority() > o.getPriority()) {
			result = 1;
		}
		
		return result;
	}
}