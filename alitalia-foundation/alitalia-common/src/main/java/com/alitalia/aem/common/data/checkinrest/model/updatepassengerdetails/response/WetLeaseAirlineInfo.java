
package com.alitalia.aem.common.data.checkinrest.model.updatepassengerdetails.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WetLeaseAirlineInfo {

    @SerializedName("wetleaseOperatedBy")
    @Expose
    private String wetleaseOperatedBy;
    @SerializedName("wetleaseAirlineName")
    @Expose
    private String wetleaseAirlineName;
    @SerializedName("wetleaseFlightConfig")
    @Expose
    private String wetleaseFlightConfig;

    public String getWetleaseOperatedBy() {
        return wetleaseOperatedBy;
    }

    public void setWetleaseOperatedBy(String wetleaseOperatedBy) {
        this.wetleaseOperatedBy = wetleaseOperatedBy;
    }

    public String getWetleaseAirlineName() {
        return wetleaseAirlineName;
    }

    public void setWetleaseAirlineName(String wetleaseAirlineName) {
        this.wetleaseAirlineName = wetleaseAirlineName;
    }

    public String getWetleaseFlightConfig() {
        return wetleaseFlightConfig;
    }

    public void setWetleaseFlightConfig(String wetleaseFlightConfig) {
        this.wetleaseFlightConfig = wetleaseFlightConfig;
    }

}
