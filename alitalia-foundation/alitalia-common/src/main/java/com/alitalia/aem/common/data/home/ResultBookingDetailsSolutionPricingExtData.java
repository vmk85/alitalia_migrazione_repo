package com.alitalia.aem.common.data.home;

import java.util.List;

public class ResultBookingDetailsSolutionPricingExtData {
	
	private List<ResultBookingDetailsSolutionPricingExtEndorsementData> endorsementField;
	private ResultBookingDetailsSolutionPricingExtPaxData paxField;
	
	public List<ResultBookingDetailsSolutionPricingExtEndorsementData> getEndorsementField() {
		return endorsementField;
	}
	
	public void setEndorsementField(
			List<ResultBookingDetailsSolutionPricingExtEndorsementData> endorsementField) {
		this.endorsementField = endorsementField;
	}
	
	public ResultBookingDetailsSolutionPricingExtPaxData getPaxField() {
		return paxField;
	}
	
	public void setPaxField(
			ResultBookingDetailsSolutionPricingExtPaxData paxField) {
		this.paxField = paxField;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((endorsementField == null) ? 0 : endorsementField.hashCode());
		result = prime * result
				+ ((paxField == null) ? 0 : paxField.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingDetailsSolutionPricingExtData other = (ResultBookingDetailsSolutionPricingExtData) obj;
		if (endorsementField == null) {
			if (other.endorsementField != null)
				return false;
		} else if (!endorsementField.equals(other.endorsementField))
			return false;
		if (paxField == null) {
			if (other.paxField != null)
				return false;
		} else if (!paxField.equals(other.paxField))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingDetailsSolutionPricingExtData [endorsementField="
				+ endorsementField + ", paxField=" + paxField + "]";
	}
	
}
