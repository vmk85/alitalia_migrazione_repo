
package com.alitalia.aem.common.data.checkinrest.model.endpayment.request;

public class PaymentDetail {


    private String cardCode;

    private String cardNumber;

    private String expireMonth;

    private String expireYear;
 
    private String cardSecurityCode;

    private String cardHolderFirstName;

    private String cardHolderLastName;

    private String amount;

    private String currencyCode;
 
    private String approvalCode;

    private String transactionID;

    private String approvedURL;

    private String errorURL;
 
    private AzAddressPayment azAddressPayment;

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpireMonth() {
        return expireMonth;
    }

    public void setExpireMonth(String expireMonth) {
        this.expireMonth = expireMonth;
    }

    public String getExpireYear() {
        return expireYear;
    }

    public void setExpireYear(String expireYear) {
        this.expireYear = expireYear;
    }

    public String getCardSecurityCode() {
        return cardSecurityCode;
    }

    public void setCardSecurityCode(String cardSecurityCode) {
        this.cardSecurityCode = cardSecurityCode;
    }

    public String getCardHolderFirstName() {
        return cardHolderFirstName;
    }

    public void setCardHolderFirstName(String cardHolderFirstName) {
        this.cardHolderFirstName = cardHolderFirstName;
    }

    public String getCardHolderLastName() {
        return cardHolderLastName;
    }

    public void setCardHolderLastName(String cardHolderLastName) {
        this.cardHolderLastName = cardHolderLastName;
    }

    public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getApprovedURL() {
        return approvedURL;
    }

    public void setApprovedURL(String approvedURL) {
        this.approvedURL = approvedURL;
    }

    public String getErrorURL() {
        return errorURL;
    }

    public void setErrorURL(String errorURL) {
        this.errorURL = errorURL;
    }

    public AzAddressPayment getAzAddressPayment() {
        return azAddressPayment;
    }

    public void setAzAddressPayment(AzAddressPayment azAddressPayment) {
        this.azAddressPayment = azAddressPayment;
    }

}
