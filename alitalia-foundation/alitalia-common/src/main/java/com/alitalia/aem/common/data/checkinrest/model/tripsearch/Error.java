package com.alitalia.aem.common.data.checkinrest.model.tripsearch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Error {

    @SerializedName("errorInfo")
    @Expose
    private ErrorInfo errorInfo;
    @SerializedName("errorCode")
    @Expose
    private String errorCode;
    @SerializedName("severity")
    @Expose
    private String severity;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;

    public ErrorInfo getErrorInfo() {
        return errorInfo;
    }

    public void setErrorInfo(ErrorInfo errorInfo) {
        this.errorInfo = errorInfo;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
