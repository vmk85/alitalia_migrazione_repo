package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.RegisterCreditCardResult;
import com.alitalia.aem.common.messages.Response;

public class RegisterCreditCardResponse implements Response {
	
	private RegisterCreditCardResult result;
	
	public RegisterCreditCardResponse(RegisterCreditCardResult result) {
		super();
		this.result = result;
	}

	public RegisterCreditCardResult getResult() {
		return result;
	}

	public void setResult(RegisterCreditCardResult result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "RegisterCreditCardResponse [result=" + result
				+ ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + ", getClass()="
				+ getClass() + "]";
	}
}
