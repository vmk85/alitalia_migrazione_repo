
package com.alitalia.aem.common.data.home.carnet;


public class CarnetComunicationPayPalData extends CarnetComunicationData {

    private String cancelUrl;
    private Boolean shippingEnabled;
    
	public String getCancelUrl() {
		return cancelUrl;
	}
	public void setCancelUrl(String cancelUrl) {
		this.cancelUrl = cancelUrl;
	}
	public Boolean getShippingEnabled() {
		return shippingEnabled;
	}
	public void setShippingEnabled(Boolean shippingEnabled) {
		this.shippingEnabled = shippingEnabled;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((cancelUrl == null) ? 0 : cancelUrl.hashCode());
		result = prime * result
				+ ((shippingEnabled == null) ? 0 : shippingEnabled.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarnetComunicationPayPalData other = (CarnetComunicationPayPalData) obj;
		if (cancelUrl == null) {
			if (other.cancelUrl != null)
				return false;
		} else if (!cancelUrl.equals(other.cancelUrl))
			return false;
		if (shippingEnabled == null) {
			if (other.shippingEnabled != null)
				return false;
		} else if (!shippingEnabled.equals(other.shippingEnabled))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CarnetComunicationPayPalData [cancelUrl=" + cancelUrl
				+ ", shippingEnabled=" + shippingEnabled + ", description="
				+ description + ", languageCode=" + languageCode
				+ ", returnUrl=" + returnUrl + "]";
	}
}
