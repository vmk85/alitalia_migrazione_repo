package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.checkin.CheckinFrequentFlyerCode;
import com.alitalia.aem.common.messages.MmbBaseRequest;

public class WebCheckinCheckFFCodeRequest extends MmbBaseRequest {

	private List<CheckinFrequentFlyerCode> frequentFlyerCodes;

	public WebCheckinCheckFFCodeRequest(String tid, String sid) {
		super(tid, sid);
	}

	public WebCheckinCheckFFCodeRequest() {
		super();
	}

	public List<CheckinFrequentFlyerCode> getFrequentFlyerCodes() {
		return frequentFlyerCodes;
	}

	public void setFrequentFlyerCodes(
			List<CheckinFrequentFlyerCode> frequentFlyerCodes) {
		this.frequentFlyerCodes = frequentFlyerCodes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((frequentFlyerCodes == null) ? 0 : frequentFlyerCodes
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebCheckinCheckFFCodeRequest other = (WebCheckinCheckFFCodeRequest) obj;
		if (frequentFlyerCodes == null) {
			if (other.frequentFlyerCodes != null)
				return false;
		} else if (!frequentFlyerCodes.equals(other.frequentFlyerCodes))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WebCheckinCheckFFCodeRequest [frequentFlyerCodes="
				+ frequentFlyerCodes + "]";
	}
}