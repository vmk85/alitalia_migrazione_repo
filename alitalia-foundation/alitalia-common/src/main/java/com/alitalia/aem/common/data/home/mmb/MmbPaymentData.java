package com.alitalia.aem.common.data.home.mmb;

import com.alitalia.aem.common.data.home.enumerations.ContactTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.CreditCardTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbPaymentGroupMaskEnum;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;

public class MmbPaymentData {

	private String address;
	private String cap;
	private String cvc;
	private String captcha;
	private String city;
	private String contactPhoneNumber;
	private String contactPrefix;
	private ContactTypeEnum contactType;
	private String creditCardNumber;
	private CreditCardTypeEnum creditCardType;
	private String email;
	private String expirationMonth;
	private String expirationYear;
	private String lastName;
	private String name;
	private String selectedCountry;
	private MmbPaymentGroupMaskEnum selectedPaymentGroup;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCap() {
		return cap;
	}

	public void setCap(String cap) {
		this.cap = cap;
	}

	public String getCvc() {
		return cvc;
	}

	public void setCvc(String cvc) {
		this.cvc = cvc;
	}

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getContactPhoneNumber() {
		return contactPhoneNumber;
	}

	public void setContactPhoneNumber(String contactPhoneNumber) {
		this.contactPhoneNumber = contactPhoneNumber;
	}

	public String getContactPrefix() {
		return contactPrefix;
	}

	public void setContactPrefix(String contactPrefix) {
		this.contactPrefix = contactPrefix;
	}

	public ContactTypeEnum getContactType() {
		return contactType;
	}

	public void setContactType(ContactTypeEnum contactType) {
		this.contactType = contactType;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public CreditCardTypeEnum getCreditCardType() {
		return creditCardType;
	}

	public void setCreditCardType(CreditCardTypeEnum creditCardType) {
		this.creditCardType = creditCardType;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getExpirationMonth() {
		return expirationMonth;
	}

	public void setExpirationMonth(String expirationMonth) {
		this.expirationMonth = expirationMonth;
	}

	public String getExpirationYear() {
		return expirationYear;
	}

	public void setExpirationYear(String expirationYear) {
		this.expirationYear = expirationYear;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSelectedCountry() {
		return selectedCountry;
	}

	public void setSelectedCountry(String selectedCountry) {
		this.selectedCountry = selectedCountry;
	}

	public MmbPaymentGroupMaskEnum getSelectedPaymentGroup() {
		return selectedPaymentGroup;
	}

	public void setSelectedPaymentGroup(MmbPaymentGroupMaskEnum selectedPaymentGroup) {
		this.selectedPaymentGroup = selectedPaymentGroup;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbPaymentData other = (MmbPaymentData) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbPaymentData [address=" + address + ", cap=" + cap + ", cvc="
				+ cvc + ", captcha=" + captcha + ", city=" + city
				+ ", contactPhoneNumber=" + contactPhoneNumber
				+ ", contactPrefix=" + contactPrefix + ", contactType="
				+ contactType + ", creditCardNumber=" + creditCardNumber
				+ ", creditCardType=" + creditCardType + ", email=" + email
				+ ", expirationMonth=" + expirationMonth + ", expirationYear="
				+ expirationYear + ", lastName=" + lastName + ", name=" + name
				+ ", selectedCountry=" + selectedCountry
				+ ", selectedPaymentGroup=" + selectedPaymentGroup + "]";
	}

}
