package com.alitalia.aem.common.data.home.enumerations;

public enum MmbPassengerNoteEnum {

    NONE("None"),
    CHILD("Child"),
    INFANT_ACCOMPANIST("InfantAccompanist"),
    OTHER_ACCOMPANIST("OtherAccompanist");
    private final String value;

    MmbPassengerNoteEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MmbPassengerNoteEnum fromValue(String v) {
        for (MmbPassengerNoteEnum c: MmbPassengerNoteEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
