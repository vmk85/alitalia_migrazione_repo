package com.alitalia.aem.common.messages.checkinrest;

import java.util.List;

import com.alitalia.aem.common.data.checkinrest.model.fasttrack.request.FlightFastTrack;
import com.alitalia.aem.common.messages.BaseRequest;

public class CheckinSetFastTrackRequest extends BaseRequest{
	
	private List<FlightFastTrack> flightFastTrack = null;
	private String pnr;
	private String language;
	private String market;
	private String conversationID;

	public CheckinSetFastTrackRequest(String tid, String sid) {
		super(tid, sid);
	}


	public List<FlightFastTrack> getFlightFastTrack() {
		return flightFastTrack;
	}


	public void setFlightFastTrack(List<FlightFastTrack> flightFastTrack) {
		this.flightFastTrack = flightFastTrack;
	}


	public String getPnr() {
		return pnr;
	}


	public void setPnr(String pnr) {
		this.pnr = pnr;
	}


	public String getLanguage() {
		return language;
	}


	public void setLanguage(String language) {
		this.language = language;
	}


	public String getMarket() {
		return market;
	}


	public void setMarket(String market) {
		this.market = market;
	}


	public String getConversationID() {
		return conversationID;
	}


	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}


}
