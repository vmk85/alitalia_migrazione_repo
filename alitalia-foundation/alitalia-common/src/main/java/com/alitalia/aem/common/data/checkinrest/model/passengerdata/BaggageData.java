
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaggageData {

    @SerializedName("firstOccurence")
    @Expose
    private Long firstOccurence;
    @SerializedName("firstOccurenceSpecified")
    @Expose
    private Boolean firstOccurenceSpecified;
    @SerializedName("lastOccurence")
    @Expose
    private Long lastOccurence;
    @SerializedName("lastOccurenceSpecified")
    @Expose
    private Boolean lastOccurenceSpecified;
    @SerializedName("bagWeightLimit")
    @Expose
    private BagWeightLimit_ bagWeightLimit;

    public Long getFirstOccurence() {
        return firstOccurence;
    }

    public void setFirstOccurence(Long firstOccurence) {
        this.firstOccurence = firstOccurence;
    }

    public Boolean getFirstOccurenceSpecified() {
        return firstOccurenceSpecified;
    }

    public void setFirstOccurenceSpecified(Boolean firstOccurenceSpecified) {
        this.firstOccurenceSpecified = firstOccurenceSpecified;
    }

    public Long getLastOccurence() {
        return lastOccurence;
    }

    public void setLastOccurence(Long lastOccurence) {
        this.lastOccurence = lastOccurence;
    }

    public Boolean getLastOccurenceSpecified() {
        return lastOccurenceSpecified;
    }

    public void setLastOccurenceSpecified(Boolean lastOccurenceSpecified) {
        this.lastOccurenceSpecified = lastOccurenceSpecified;
    }

    public BagWeightLimit_ getBagWeightLimit() {
        return bagWeightLimit;
    }

    public void setBagWeightLimit(BagWeightLimit_ bagWeightLimit) {
        this.bagWeightLimit = bagWeightLimit;
    }

}
