package com.alitalia.aem.common.data.home;

public class StateData {
	
	private String stateCode;
	private String stateDescription;
	
	public String getStateCode() {
		return stateCode;
	}
	
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	
	public String getStateDescription() {
		return stateDescription;
	}
	
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((stateCode == null) ? 0 : stateCode.hashCode());
		result = prime
				* result
				+ ((stateDescription == null) ? 0 : stateDescription.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StateData other = (StateData) obj;
		if (stateCode == null) {
			if (other.stateCode != null)
				return false;
		} else if (!stateCode.equals(other.stateCode))
			return false;
		if (stateDescription == null) {
			if (other.stateDescription != null)
				return false;
		} else if (!stateDescription.equals(other.stateDescription))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "StateData [stateCode=" + stateCode + ", stateDescription="
				+ stateDescription + "]";
	}
}