package com.alitalia.aem.common.data.home;

public class MMTier {

    public int lvl;
    public String value;
    public String ico;

    public MMTier(int lvl, String value, String ico){
        this.lvl = lvl;
        this.value = value;
        this.ico = ico;
    }

    public int getLvl(){
        return this.lvl;
    }
    public String getValue(){
        return this.value;
    }
    public String getIco(){
        return this.ico;
    }
}
