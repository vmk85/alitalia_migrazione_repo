package com.alitalia.aem.common.data.home;

import java.util.ArrayList;
import java.util.Calendar;

import com.alitalia.aem.common.data.home.enumerations.CabinEnum;

public class DirectFlightData extends FlightData {

	private Calendar arrivalDate;
	private Calendar departureDate;
	private ArrayList<BaggageAllowanceData> baggageAllowanceList;
	private CabinEnum cabin;
	private String carrier;
	private String carrierForLogo;
	private ArrayList<String> compartimentalClass;
	private FlightDetailsData details;
	private Boolean enabledSeatMap;
	private String flightNumber;
	private AirportData from;
	private String marriageGroup;
	private Long mileage;
	private String operationalCarrier;
	private String operationalFlightNumber;
	private String operationalFlightText;
	private boolean stopOver;
	private AirportData to;
	private String urlConditions;
	private boolean bus;
	
	public DirectFlightData() {
		super();
	}
	
	public DirectFlightData(DirectFlightData clone) {
		super();
		this.arrivalDate = clone.arrivalDate != null ? (Calendar) clone.arrivalDate.clone() : null;
		this.departureDate = clone.departureDate != null ? (Calendar) clone.departureDate.clone() : null;
		
		if (clone.baggageAllowanceList != null) {
			ArrayList<BaggageAllowanceData> baggageAllowanceList = new ArrayList<BaggageAllowanceData>();
			for (BaggageAllowanceData item : clone.baggageAllowanceList) {
				baggageAllowanceList.add(new BaggageAllowanceData(item));
			}
			this.baggageAllowanceList = baggageAllowanceList;
		}
		
		this.cabin = clone.cabin;
		this.carrier = clone.carrier;
		this.carrierForLogo = clone.carrierForLogo;
		
		if (clone.compartimentalClass != null) {
			ArrayList<String> compartimentalClass = new ArrayList<String>();
			for (String item : clone.compartimentalClass) {
				compartimentalClass.add(item);
			}
			this.compartimentalClass = compartimentalClass;
		}
		
		this.details = clone.details != null ? new FlightDetailsData(clone.details) : null;
		this.enabledSeatMap = clone.enabledSeatMap != null ? new Boolean(clone.enabledSeatMap.booleanValue()) : null;
		this.flightNumber = clone.flightNumber;
		this.from = clone.from != null ? new AirportData(clone.from) : null;
		this.marriageGroup = clone.marriageGroup;
		this.mileage = clone.mileage != null ? new Long(clone.mileage.longValue()) : null;
		this.operationalCarrier = clone.operationalCarrier;
		this.operationalFlightNumber = clone.operationalFlightNumber;
		this.operationalFlightText = clone.operationalFlightText;
		this.stopOver = clone.stopOver;
		this.to = clone.to != null ? new AirportData(clone.to) : null;
		this.urlConditions = clone.urlConditions;
		
		this.properties = clone.properties != null ? (PropertiesData) clone.properties.clone() : null;
		// FIXME this.brands
		this.flightType = clone.flightType;
		this.durationHour = clone.durationHour != null ? new Integer(clone.durationHour.intValue()) : null;
		this.durationMinutes = clone.durationMinutes != null ? new Integer(clone.durationMinutes.intValue()) : null;
	}

	public Calendar getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Calendar arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public ArrayList<BaggageAllowanceData> getBaggageAllowanceList() {
		return baggageAllowanceList;
	}

	public void setBaggageAllowanceList(
			ArrayList<BaggageAllowanceData> baggageAllowanceList) {
		this.baggageAllowanceList = baggageAllowanceList;
	}

	public CabinEnum getCabin() {
		return cabin;
	}

	public void setCabin(CabinEnum cabin) {
		this.cabin = cabin;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getCarrierForLogo() {
		return carrierForLogo;
	}
	
	public void setCarrierForLogo(String carrierForLogo) {
		this.carrierForLogo = carrierForLogo;
	}

	public ArrayList<String> getCompartimentalClass() {
		return compartimentalClass;
	}

	public void setCompartimentalClass(ArrayList<String> compartimentalClass) {
		this.compartimentalClass = compartimentalClass;
	}

	public Calendar getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Calendar departureDate) {
		this.departureDate = departureDate;
	}

	public FlightDetailsData getDetails() {
		return details;
	}

	public void setDetails(FlightDetailsData details) {
		this.details = details;
	}

	public Boolean getEnabledSeatMap() {
		return enabledSeatMap;
	}

	public void setEnabledSeatMap(Boolean enabledSeatMap) {
		this.enabledSeatMap = enabledSeatMap;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public AirportData getFrom() {
		return from;
	}

	public void setFrom(AirportData from) {
		this.from = from;
	}

	public String getMarriageGroup() {
		return marriageGroup;
	}

	public void setMarriageGroup(String marriageGroup) {
		this.marriageGroup = marriageGroup;
	}

	public Long getMileage() {
		return mileage;
	}

	public void setMileage(Long mileage) {
		this.mileage = mileage;
	}

	public String getOperationalCarrier() {
		return operationalCarrier;
	}

	public void setOperationalCarrier(String operationalCarrier) {
		this.operationalCarrier = operationalCarrier;
	}

	public String getOperationalFlightNumber() {
		return operationalFlightNumber;
	}

	public void setOperationalFlightNumber(String operationalFlightNumber) {
		this.operationalFlightNumber = operationalFlightNumber;
	}

	public String getOperationalFlightText() {
		return operationalFlightText;
	}

	public void setOperationalFlightText(String operationalFlightText) {
		this.operationalFlightText = operationalFlightText;
	}

	public Boolean getStopOver() {
		return stopOver;
	}

	public void setStopOver(Boolean stopOver) {
		this.stopOver = stopOver;
	}

	public AirportData getTo() {
		return to;
	}

	public void setTo(AirportData to) {
		this.to = to;
	}

	public String getUrlConditions() {
		return urlConditions;
	}

	public void setUrlConditions(String urlConditions) {
		this.urlConditions = urlConditions;
	}
	
	public boolean isBus() {
		return bus;
	}

	public void setBus(boolean bus) {
		this.bus = bus;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((carrier == null) ? 0 : carrier.hashCode());
		result = prime * result
				+ ((departureDate == null) ? 0 : departureDate.hashCode());
		result = prime * result
				+ ((flightNumber == null) ? 0 : flightNumber.hashCode());
		result = prime * result + ((from == null) ? 0 : from.hashCode());
		result = prime * result + ((to == null) ? 0 : to.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DirectFlightData other = (DirectFlightData) obj;
		if (carrier == null) {
			if (other.carrier != null)
				return false;
		} else if (!carrier.equals(other.carrier))
			return false;
		if (departureDate == null) {
			if (other.departureDate != null)
				return false;
		} else if (!departureDate.equals(other.departureDate))
			return false;
		if (flightNumber == null) {
			if (other.flightNumber != null)
				return false;
		} else if (!flightNumber.equals(other.flightNumber))
			return false;
		if (from == null) {
			if (other.from != null)
				return false;
		} else if (!from.equals(other.from))
			return false;
		if (to == null) {
			if (other.to != null)
				return false;
		} else if (!to.equals(other.to))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DirectFlightData [arrivalDate=" + arrivalDate
				+ ", baggageAllowanceList=" + baggageAllowanceList + ", cabin="
				+ cabin + ", carrier=" + carrier + ", carrierForLogo="
				+ carrierForLogo + ", compartimentalClass="
				+ compartimentalClass + ", departureDate=" + departureDate
				+ ", details=" + details + ", enabledSeatMap=" + enabledSeatMap
				+ ", flightNumber=" + flightNumber + ", from=" + from
				+ ", marriageGroup=" + marriageGroup + ", mileage=" + mileage
				+ ", operationalCarrier=" + operationalCarrier
				+ ", operationalFlightNumber=" + operationalFlightNumber
				+ ", operationalFlightText=" + operationalFlightText
				+ ", stopOver=" + stopOver + ", to=" + to + ", urlConditions="
				+ urlConditions + "]";
	}

}
