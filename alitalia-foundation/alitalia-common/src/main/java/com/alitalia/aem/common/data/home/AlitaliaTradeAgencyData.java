package com.alitalia.aem.common.data.home;

import com.alitalia.aem.common.data.home.enumerations.AlitaliaTradeUserType;

public class AlitaliaTradeAgencyData {

	private String codiceAgenzia;
	private String ragioneSociale;
	private String partitaIVA;
	private String codiceFiscale;
	private String indirizzo;
	private String citta;
	private String provincia;
	private String regione;
	private String stato;
	private String zip;
	private AlitaliaTradeUserType ruolo;
	private String email;
	private String password;
	private String CODFAMCON;
	private Boolean nuovoUtente;
	private String codiceAccordo;
	private String emailAlitalia;
	private String codiceSirax;
	private String dataValiditaAccordo;
	private Boolean passwordExpired;
	private Boolean accountLocked;
	private Boolean condizioniAccettate;
	private Boolean iataAgency;
	private Boolean groupEnabled;
	
	public String getCodiceAgenzia() {
		return codiceAgenzia;
	}
	
	public void setCodiceAgenzia(String codiceAgenzia) {
		this.codiceAgenzia = codiceAgenzia;
	}
	
	public String getRagioneSociale() {
		return ragioneSociale;
	}
	
	public void setRagioneSociale(String ragioneSociale) {
		this.ragioneSociale = ragioneSociale;
	}
	
	public String getPartitaIVA() {
		return partitaIVA;
	}
	
	public void setPartitaIVA(String partitaIVA) {
		this.partitaIVA = partitaIVA;
	}
	
	public String getCodiceFiscale() {
		return codiceFiscale;
	}
	
	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}
	
	public String getIndirizzo() {
		return indirizzo;
	}
	
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	
	public String getCitta() {
		return citta;
	}
	
	public void setCitta(String citta) {
		this.citta = citta;
	}
	
	public String getProvincia() {
		return provincia;
	}
	
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	
	public String getRegione() {
		return regione;
	}
	
	public void setRegione(String regione) {
		this.regione = regione;
	}
	
	public String getStato() {
		return stato;
	}
	
	public void setStato(String stato) {
		this.stato = stato;
	}
	
	public String getZip() {
		return zip;
	}
	
	public void setZip(String zip) {
		this.zip = zip;
	}
	
	public AlitaliaTradeUserType getRuolo() {
		return ruolo;
	}
	
	public void setRuolo(AlitaliaTradeUserType ruolo) {
		this.ruolo = ruolo;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getCODFAMCON() {
		return CODFAMCON;
	}
	
	public void setCODFAMCON(String codFamCon) {
		this.CODFAMCON = codFamCon;
	}
	
	public Boolean isNuovoUtente() {
		return nuovoUtente;
	}
	
	public void setNuovoUtente(Boolean nuovoUtente) {
		this.nuovoUtente = nuovoUtente;
	}
	
	public String getCodiceAccordo() {
		return codiceAccordo;
	}
	
	public void setCodiceAccordo(String codiceAccordo) {
		this.codiceAccordo = codiceAccordo;
	}
	
	public String getEmailAlitalia() {
		return emailAlitalia;
	}
	
	public void setEmailAlitalia(String emailAlitalia) {
		this.emailAlitalia = emailAlitalia;
	}
	
	public String getCodiceSirax() {
		return codiceSirax;
	}
	
	public void setCodiceSirax(String codiceSirax) {
		this.codiceSirax = codiceSirax;
	}
	
	public String getDataValiditaAccordo() {
		return dataValiditaAccordo;
	}
	
	public void setDataValiditaAccordo(String dataValiditaAccordo) {
		this.dataValiditaAccordo = dataValiditaAccordo;
	}
	
	public Boolean isPasswordExpired() {
		return passwordExpired;
	}
	
	public void setPasswordExpired(Boolean passwordExpired) {
		this.passwordExpired = passwordExpired;
	}
	
	public Boolean isAccountLocked() {
		return accountLocked;
	}
	
	public void setAccountLocked(Boolean accountLocked) {
		this.accountLocked = accountLocked;
	}

	public Boolean isCondizioniAccettate() {
		return condizioniAccettate;
	}

	public void setCondizioniAccettate(Boolean condizioniAccettate) {
		this.condizioniAccettate = condizioniAccettate;
	}

	public Boolean isIataAgency() {
		return iataAgency;
	}

	public void setIataAgency(Boolean iataAgency) {
		this.iataAgency = iataAgency;
	}

	public Boolean isGroupEnabled() {
		return groupEnabled;
	}

	public void setGroupEnabled(Boolean groupEnabled) {
		this.groupEnabled = groupEnabled;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((codiceAgenzia == null) ? 0 : codiceAgenzia.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlitaliaTradeAgencyData other = (AlitaliaTradeAgencyData) obj;
		if (codiceAgenzia == null) {
			if (other.codiceAgenzia != null)
				return false;
		} else if (!codiceAgenzia.equals(other.codiceAgenzia))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AlitaliaTradeAgencyData [codiceAgenzia=" + codiceAgenzia
				+ ", ragioneSociale=" + ragioneSociale + ", partitaIVA="
				+ partitaIVA + ", codiceFiscale=" + codiceFiscale
				+ ", indirizzo=" + indirizzo + ", citta=" + citta
				+ ", provincia=" + provincia + ", regione=" + regione
				+ ", stato=" + stato + ", zip=" + zip + ", ruolo=" + ruolo
				+ ", email=" + email + ", password=" + password
				+ ", CODFAMCON=" + CODFAMCON + ", nuovoUtente=" + nuovoUtente
				+ ", codiceAccordo=" + codiceAccordo + ", emailAlitalia="
				+ emailAlitalia + ", codiceSirax=" + codiceSirax
				+ ", dataValiditaAccordo=" + dataValiditaAccordo
				+ ", passwordExpired=" + passwordExpired + ", accountLocked="
				+ accountLocked + ", condizioniAccettate="
				+ condizioniAccettate + ", iataAgency=" + iataAgency
				+ ", groupEnabled=" + groupEnabled + "]";
	}

}
