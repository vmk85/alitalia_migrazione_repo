
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DiagnosticResults {

    @SerializedName("any")
    @Expose
    private List<Any> any = null;
    @SerializedName("lineNumber")
    @Expose
    private Long lineNumber;
    @SerializedName("lineNumberSpecified")
    @Expose
    private Boolean lineNumberSpecified;
    @SerializedName("programName")
    @Expose
    private String programName;

    public List<Any> getAny() {
        return any;
    }

    public void setAny(List<Any> any) {
        this.any = any;
    }

    public Long getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(Long lineNumber) {
        this.lineNumber = lineNumber;
    }

    public Boolean getLineNumberSpecified() {
        return lineNumberSpecified;
    }

    public void setLineNumberSpecified(Boolean lineNumberSpecified) {
        this.lineNumberSpecified = lineNumberSpecified;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

}
