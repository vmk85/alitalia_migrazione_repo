package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.FlightSeatMapData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetrieveFlightSeatMapResponse extends BaseResponse {

	private FlightSeatMapData seatMapMatrix;

	public FlightSeatMapData getSeatMapMatrix() {
		return seatMapMatrix;
	}

	public void setSeatMapMatrix(FlightSeatMapData seatMapMatrix) {
		this.seatMapMatrix = seatMapMatrix;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((seatMapMatrix == null) ? 0 : seatMapMatrix.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveFlightSeatMapResponse other = (RetrieveFlightSeatMapResponse) obj;
		if (seatMapMatrix == null) {
			if (other.seatMapMatrix != null)
				return false;
		} else if (!seatMapMatrix.equals(other.seatMapMatrix))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveFlightSeatMapResponse [seatMapMatrix=" + seatMapMatrix + "]";
	}	
}