package com.alitalia.aem.common.data.home.enumerations;

public enum CrossSellingTypeEnum {

    HOTEL("Hotel"),
    CAR("Car"),
    PACKAGE("Package"),
    SERVICE("Service");
    
    private final String value;

    CrossSellingTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CrossSellingTypeEnum fromValue(String v) {
        for (CrossSellingTypeEnum c: CrossSellingTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
    
}
