package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class RetrievePaymentResponse  extends BaseResponse {
	
	/*
	{
		  "card": {
		    "cvc": 0,
		    "expiryMonth": 0,
		    "expiryYear": 0,
		    "holderName": 0,
		    "number": 0
		  },
		  "merchantAccount": "string",
		  "shopperEmail": "string",
		  "shopperReference": "string",
		  "recurring": {
		    "contract": "string"
		  },
		  "language": "string",
		  "market": "string",
		  "conversationID": "string"
		}
	*/

	private Card card;
	private String merchantAccount = "MyAlitalia";
	private String shopperEmail;
	private String shopperReference;
	private Recurring recurring;
	private String language;
	private String market;
	private String conversationID;
	
	public Card getCard() {
		return card;
	}

	public void setCard(Card card) {
		this.card = card;
	}

	public String getMerchantAccount() {
		return merchantAccount;
	}

	public void setMerchantAccount(String merchantAccount) {
		this.merchantAccount = merchantAccount;
	}

	public String getShopperEmail() {
		return shopperEmail;
	}

	public void setShopperEmail(String shopperEmail) {
		this.shopperEmail = shopperEmail;
	}

	public String getShopperReference() {
		return shopperReference;
	}

	public void setShopperReference(String shopperReference) {
		this.shopperReference = shopperReference;
	}

	public Recurring getRecurring() {
		return recurring;
	}

	public void setRecurring(Recurring recurring) {
		this.recurring = recurring;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public String getConversationID() {
		return conversationID;
	}

	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result	+ ((card == null) ? 0 : card.hashCode());
		result = prime * result	+ ((merchantAccount == null) ? 0 : merchantAccount.hashCode());
		result = prime * result	+ ((shopperEmail == null) ? 0 : shopperEmail.hashCode());
		result = prime * result	+ ((shopperReference == null) ? 0 : shopperReference.hashCode());
		result = prime * result	+ ((recurring == null) ? 0 : recurring.hashCode());
		result = prime * result	+ ((language == null) ? 0 : language.hashCode());
		result = prime * result	+ ((market == null) ? 0 : market.hashCode());
		result = prime * result	+ ((conversationID == null) ? 0 : conversationID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrievePaymentResponse other = (RetrievePaymentResponse) obj;
		if (card == null) {
			if (other.card != null)
				return false;
		} else if (!card.equals(other.card))
			return false;
		if (merchantAccount == null) {
			if (other.merchantAccount != null)
				return false;
		} else if (!merchantAccount.equals(other.merchantAccount))
			return false;
		if (shopperEmail == null) {
			if (other.shopperEmail != null)
				return false;
		} else if (!shopperEmail.equals(other.shopperEmail))
			return false;
		if (shopperReference == null) {
			if (other.shopperReference != null)
				return false;
		} else if (!shopperReference.equals(other.shopperReference))
			return false;
		if (recurring == null) {
			if (other.recurring != null)
				return false;
		} else if (!recurring.equals(other.recurring))
			return false;
		if (language == null) {
			if (other.language != null)
				return false;
		} else if (!language.equals(other.language))
			return false;
		if (market == null) {
			if (other.market != null)
				return false;
		} else if (!market.equals(other.market))
			return false;
		if (conversationID == null) {
			if (other.conversationID != null)
				return false;
		} else if (!conversationID.equals(other.conversationID))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "RegisterCreditCardRequest ["
				+ "card=" + card
				+ "merchantAccount=" + merchantAccount
				+ "shopperEmail=" + shopperEmail
				+ "shopperReference=" + shopperReference
				+ "recurring=" + recurring
				+ "language=" + language
				+ "market=" + market
				+ "conversationID=" + conversationID
				+ "]";
	}
	
	public class Card {
		private Integer cvc;
		private String expiryMonth;
		private String expiryYear;
		private String holderName;
		private Long number;
		
		public Integer getCvc() {
			return cvc;
		}
		public void setCvc(Integer cvc) {
			this.cvc = cvc;
		}
		public String getExpiryMonth() {
			return expiryMonth;
		}
		public void setExpiryMonth(String expiryMonth) {
			this.expiryMonth = expiryMonth;
		}
		public String getExpiryYear() {
			return expiryYear;
		}
		public void setExpiryYear(String expiryYear) {
			this.expiryYear = expiryYear;
		}
		public String getHolderName() {
			return holderName;
		}
		public void setHolderName(String holderName) {
			this.holderName = holderName;
		}
		public Long getNumber() {
			return number;
		}
		public void setNumber(Long number) {
			this.number = number;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result	+ ((cvc == null) ? 0 : cvc.hashCode());
			result = prime * result	+ ((expiryMonth == null) ? 0 : expiryMonth.hashCode());
			result = prime * result	+ ((expiryYear == null) ? 0 : expiryYear.hashCode());
			result = prime * result	+ ((holderName == null) ? 0 : holderName.hashCode());
			result = prime * result	+ ((number == null) ? 0 : number.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			Card other = (Card) obj;
			if (cvc == null) {
				if (other.cvc != null)
					return false;
			} else if (!cvc.equals(other.cvc))
				return false;
			if (expiryMonth == null) {
				if (other.expiryMonth != null)
					return false;
			} else if (!expiryMonth.equals(other.expiryMonth))
				return false;
			if (expiryYear == null) {
				if (other.expiryYear != null)
					return false;
			} else if (!expiryYear.equals(other.expiryYear))
				return false;
			if (holderName == null) {
				if (other.holderName != null)
					return false;
			} else if (!holderName.equals(other.holderName))
				return false;
			if (number == null) {
				if (other.number != null)
					return false;
			} else if (!number.equals(other.number))
				return false;

			return true;
		}

		@Override
		public String toString() {
			return "Card ["
					+ "cvc=" + cvc
					+ "expiryMonth=" + expiryMonth
					+ "expiryYear=" + expiryYear
					+ "holderName=" + holderName
					+ "number=" + number
					+ "]";
		}

	}

	public class Recurring {
		private String contract = "ONECLICK, RECURRING";//ONECLICK o RECURRING o ONECLICK, RECURRING

		public String getContract() {
			return contract;
		}

		public void setContract(String contract) {
			this.contract = contract;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result	+ ((contract == null) ? 0 : contract.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			Recurring other = (Recurring) obj;
			if (contract == null) {
				if (other.contract != null)
					return false;
			} else if (!contract.equals(other.contract))
				return false;

			return true;
		}

		@Override
		public String toString() {
			return "Recurring ["
					+ "contract=" + contract
					+ "]";
		}

	}
}