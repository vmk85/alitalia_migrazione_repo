package com.alitalia.aem.common.data.home;

public class AdyenAddressData {
	/*
	{
	  "city": "Roma",
	  "country": "IT",
	  "postalCode": "00100",
	  "province": "RM",
	  "street": "via roma 100"
	} 
	*/

	private String city;
	private String country;
	private String postalCode;
	private String province;
	private String street;
	
	public AdyenAddressData(String city, String country, String postalCode, String province, String street) {
		super();
		this.city = city;
		this.country = country;
		this.postalCode = postalCode;
		this.province = province;
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result	+ ((city == null) ? 0 : city.hashCode());
		result = prime * result	+ ((country == null) ? 0 : country.hashCode());
		result = prime * result	+ ((postalCode == null) ? 0 : postalCode.hashCode());
		result = prime * result	+ ((province == null) ? 0 : province.hashCode());
		result = prime * result	+ ((street == null) ? 0 : street.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdyenAddressData other = (AdyenAddressData) obj;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (postalCode == null) {
			if (other.postalCode != null)
				return false;
		} else if (!postalCode.equals(other.postalCode))
			return false;
		if (province == null) {
			if (other.province != null)
				return false;
		} else if (!province.equals(other.province))
			return false;
		if (street == null) {
			if (other.street != null)
				return false;
		} else if (!street.equals(other.street))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "Address ["
				+ "city=" + city
				+ ", country=" + country
				+ ", postalCode=" + postalCode
				+ ", province=" + province 
				+ ", street=" + street
				+ "]";
	}
}
