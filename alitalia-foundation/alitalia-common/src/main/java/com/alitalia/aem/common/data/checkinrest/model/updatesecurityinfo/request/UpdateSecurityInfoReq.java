
package com.alitalia.aem.common.data.checkinrest.model.updatesecurityinfo.request;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateSecurityInfoReq {

    @SerializedName("pnr")
    @Expose
    private String pnr;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("passengerCode")
    @Expose
    private String passengerCode;
    @SerializedName("routes")
    @Expose
    private List<Route> routes = null;
    @SerializedName("ancillaryId")
    @Expose
    private String ancillaryId;
    @SerializedName("ancillaryOperation")
    @Expose
    private String ancillaryOperation;
    @SerializedName("ancillary")
    @Expose
    private String ancillary;
    @SerializedName("price")
    @Expose
    private Float price;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("subcode")
    @Expose
    private String subcode;
    @SerializedName("commercialName")
    @Expose
    private String commercialName;

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassengerCode() {
        return passengerCode;
    }

    public void setPassengerCode(String passengerCode) {
        this.passengerCode = passengerCode;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public String getAncillaryId() {
        return ancillaryId;
    }

    public void setAncillaryId(String ancillaryId) {
        this.ancillaryId = ancillaryId;
    }

    public String getAncillaryOperation() {
        return ancillaryOperation;
    }

    public void setAncillaryOperation(String ancillaryOperation) {
        this.ancillaryOperation = ancillaryOperation;
    }

    public String getAncillary() {
        return ancillary;
    }

    public void setAncillary(String ancillary) {
        this.ancillary = ancillary;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSubcode() {
        return subcode;
    }

    public void setSubcode(String subcode) {
        this.subcode = subcode;
    }

    public String getCommercialName() {
        return commercialName;
    }

    public void setCommercialName(String commercialName) {
        this.commercialName = commercialName;
    }

}
