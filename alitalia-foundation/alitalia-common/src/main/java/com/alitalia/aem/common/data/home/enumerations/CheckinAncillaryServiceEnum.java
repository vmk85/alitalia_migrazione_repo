package com.alitalia.aem.common.data.home.enumerations;

public enum CheckinAncillaryServiceEnum {

	COMFORT_SEAT("ComfortSeat"),
	EXTRA_BAGGAGE("ExtraBaggage");

	private final String value;

    CheckinAncillaryServiceEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CheckinAncillaryServiceEnum fromValue(String v) {
        for (CheckinAncillaryServiceEnum c: CheckinAncillaryServiceEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
