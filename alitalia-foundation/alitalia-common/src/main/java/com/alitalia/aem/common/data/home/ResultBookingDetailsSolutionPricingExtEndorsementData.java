package com.alitalia.aem.common.data.home;

public class ResultBookingDetailsSolutionPricingExtEndorsementData {
	
	private String boxesField;
	private String valueField;
	
	public String getBoxesField() {
		return boxesField;
	}
	
	public void setBoxesField(String boxesField) {
		this.boxesField = boxesField;
	}
	
	public String getValueField() {
		return valueField;
	}
	
	public void setValueField(String valueField) {
		this.valueField = valueField;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((boxesField == null) ? 0 : boxesField.hashCode());
		result = prime * result
				+ ((valueField == null) ? 0 : valueField.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingDetailsSolutionPricingExtEndorsementData other = (ResultBookingDetailsSolutionPricingExtEndorsementData) obj;
		if (boxesField == null) {
			if (other.boxesField != null)
				return false;
		} else if (!boxesField.equals(other.boxesField))
			return false;
		if (valueField == null) {
			if (other.valueField != null)
				return false;
		} else if (!valueField.equals(other.valueField))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingDetailsSolutionPricingExtEndorsementData [boxesField="
				+ boxesField + ", valueField=" + valueField + "]";
	}
	
}
