
package com.alitalia.aem.common.data.checkinrest.model.updatesecurityinfo.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EditCodeList {

    @SerializedName("editCode")
    @Expose
    private List<String> editCode = null;
    @SerializedName("editPattern")
    @Expose
    private List<String> editPattern = null;

    public List<String> getEditCode() {
        return editCode;
    }

    public void setEditCode(List<String> editCode) {
        this.editCode = editCode;
    }

    public List<String> getEditPattern() {
        return editPattern;
    }

    public void setEditPattern(List<String> editPattern) {
        this.editPattern = editPattern;
    }

}
