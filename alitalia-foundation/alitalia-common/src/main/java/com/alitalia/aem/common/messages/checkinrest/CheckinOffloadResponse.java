package com.alitalia.aem.common.messages.checkinrest;

import java.util.List;

import com.alitalia.aem.common.data.checkinrest.model.offload.response.PnrFlightInfoR;
import com.alitalia.aem.common.messages.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckinOffloadResponse extends BaseResponse {

    @SerializedName("pnrFlightInfoRS")
    @Expose
    private List<PnrFlightInfoR> pnrFlightInfoRS = null;
    @SerializedName("conversationID")
    @Expose
    private String conversationID;
    @SerializedName("outcome")
    @Expose
    private String outcome;

    private String error;

    public List<PnrFlightInfoR> getPnrFlightInfoRS() {
        return pnrFlightInfoRS;
    }

    public void setPnrFlightInfoRS(List<PnrFlightInfoR> pnrFlightInfoRS) {
        this.pnrFlightInfoRS = pnrFlightInfoRS;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getOutcome() {
        return outcome;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }
}
