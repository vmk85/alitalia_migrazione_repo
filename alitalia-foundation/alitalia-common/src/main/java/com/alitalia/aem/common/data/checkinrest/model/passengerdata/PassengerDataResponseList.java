
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PassengerDataResponseList {

    @SerializedName("passengerDataResponse")
    @Expose
    private List<PassengerDataResponse> passengerDataResponse = null;
    @SerializedName("similarNameFound")
    @Expose
    private Boolean similarNameFound;
    @SerializedName("similarNameFoundSpecified")
    @Expose
    private Boolean similarNameFoundSpecified;

    public List<PassengerDataResponse> getPassengerDataResponse() {
        return passengerDataResponse;
    }

    public void setPassengerDataResponse(List<PassengerDataResponse> passengerDataResponse) {
        this.passengerDataResponse = passengerDataResponse;
    }

    public Boolean getSimilarNameFound() {
        return similarNameFound;
    }

    public void setSimilarNameFound(Boolean similarNameFound) {
        this.similarNameFound = similarNameFound;
    }

    public Boolean getSimilarNameFoundSpecified() {
        return similarNameFoundSpecified;
    }

    public void setSimilarNameFoundSpecified(Boolean similarNameFoundSpecified) {
        this.similarNameFoundSpecified = similarNameFoundSpecified;
    }

}
