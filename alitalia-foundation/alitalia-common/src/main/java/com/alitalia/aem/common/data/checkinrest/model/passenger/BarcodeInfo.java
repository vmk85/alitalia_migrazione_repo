package com.alitalia.aem.common.data.checkinrest.model.passenger;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BarcodeInfo {
	
	@SerializedName("infraeroBarcode")
	@Expose
	private String infraeroBarcode;
	
	@SerializedName("version")
	@Expose
	private String version;
	
	@SerializedName("barcode")
	@Expose
	private String barcode;
	
	@SerializedName("signedDigitalSignature")
	@Expose
	private String signedDigitalSignature;

	public String getInfraeroBarcode() {
		return infraeroBarcode;
	}

	public void setInfraeroBarcode(String infraeroBarcode) {
		this.infraeroBarcode = infraeroBarcode;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getSignedDigitalSignature() {
		return signedDigitalSignature;
	}

	public void setSignedDigitalSignature(String signedDigitalSignature) {
		this.signedDigitalSignature = signedDigitalSignature;
	}
	
}
