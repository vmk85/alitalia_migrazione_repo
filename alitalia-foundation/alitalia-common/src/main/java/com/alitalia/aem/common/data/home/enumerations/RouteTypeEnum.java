package com.alitalia.aem.common.data.home.enumerations;

public enum RouteTypeEnum {

    OUTBOUND("Outbound"),
    RETURN("Return");
    private final String value;

    RouteTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RouteTypeEnum fromValue(String v) {
        for (RouteTypeEnum c: RouteTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
