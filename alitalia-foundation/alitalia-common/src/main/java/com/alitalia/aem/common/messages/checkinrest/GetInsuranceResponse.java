package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.insurance.InsurancePolicy;
import com.alitalia.aem.common.messages.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetInsuranceResponse extends BaseResponse {
	
	@SerializedName("insurancePolicy")
	@Expose
	private InsurancePolicy insurancePolicy;
	@SerializedName("conversationID")
	@Expose
	private String conversationID;
	
	private String error;
	
	public GetInsuranceResponse() {}
	
	public GetInsuranceResponse(String tid, String sid) {
		super(tid, sid);
	}

	public InsurancePolicy getInsurancePolicy() {
		return insurancePolicy;
	}

	public void setInsurancePolicy(InsurancePolicy insurancePolicy) {
		this.insurancePolicy = insurancePolicy;
	}

	public String getConversationID() {
		return conversationID;
	}

	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	
}
