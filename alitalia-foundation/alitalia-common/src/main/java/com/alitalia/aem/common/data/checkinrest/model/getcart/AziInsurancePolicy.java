
package com.alitalia.aem.common.data.checkinrest.model.getcart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AziInsurancePolicy {

    @SerializedName("buy")
    @Expose
    private Boolean buy;
    @SerializedName("totalInsuranceCost")
    @Expose
    private Float totalInsuranceCost;
    @SerializedName("policyNumber")
    @Expose
    private String policyNumber;
    @SerializedName("errorDescription")
    @Expose
    private String errorDescription;
    @SerializedName("routeType")
    @Expose
    private String routeType;
    @SerializedName("departureAirport")
    @Expose
    private String departureAirport;
    @SerializedName("arrivalAirport")
    @Expose
    private String arrivalAirport;
    @SerializedName("departureDate")
    @Expose
    private String departureDate;
    @SerializedName("departureDateLastFlight")
    @Expose
    private String departureDateLastFlight;
    @SerializedName("arrivalDate")
    @Expose
    private String arrivalDate;
    @SerializedName("passengersNumber")
    @Expose
    private Long passengersNumber;

    public Boolean getBuy() {
        return buy;
    }

    public void setBuy(Boolean buy) {
        this.buy = buy;
    }

    public Float getTotalInsuranceCost() {
        return totalInsuranceCost;
    }

    public void setTotalInsuranceCost(Float totalInsuranceCost) {
        this.totalInsuranceCost = totalInsuranceCost;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getRouteType() {
        return routeType;
    }

    public void setRouteType(String routeType) {
        this.routeType = routeType;
    }

    public String getDepartureAirport() {
        return departureAirport;
    }

    public void setDepartureAirport(String departureAirport) {
        this.departureAirport = departureAirport;
    }

    public String getArrivalAirport() {
        return arrivalAirport;
    }

    public void setArrivalAirport(String arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getDepartureDateLastFlight() {
        return departureDateLastFlight;
    }

    public void setDepartureDateLastFlight(String departureDateLastFlight) {
        this.departureDateLastFlight = departureDateLastFlight;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public Long getPassengersNumber() {
        return passengersNumber;
    }

    public void setPassengersNumber(Long passengersNumber) {
        this.passengersNumber = passengersNumber;
    }

}
