package com.alitalia.aem.common.messages.home;

import java.util.List;
import com.alitalia.aem.common.data.home.carnet.CarnetPaymentTypeItemData;
import com.alitalia.aem.common.messages.BaseResponse;

public class CarnetPaymentTypeResponse extends BaseResponse {

	private List<CarnetPaymentTypeItemData> paymentTypeItem;

	public List<CarnetPaymentTypeItemData> getPaymentTypeItem() {
		return paymentTypeItem;
	}

	public void setPaymentTypeItem(List<CarnetPaymentTypeItemData> paymentTypeItem) {
		this.paymentTypeItem = paymentTypeItem;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((paymentTypeItem == null) ? 0 : paymentTypeItem.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarnetPaymentTypeResponse other = (CarnetPaymentTypeResponse) obj;
		if (paymentTypeItem == null) {
			if (other.paymentTypeItem != null)
				return false;
		} else if (!paymentTypeItem.equals(other.paymentTypeItem))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CarnetPaymentTypeResponse [paymentTypeItem=" + paymentTypeItem
				+ "]";
	}
}