package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.PaymentData;
import com.alitalia.aem.common.data.home.enumerations.InitializePaymentErrorEnum;
import com.alitalia.aem.common.messages.BaseResponse;

public class InitializePaymentResponse extends BaseResponse {
	
	private PaymentData paymentData;
	private InitializePaymentErrorEnum errorCode;
	private String errorStatisticMessage;

	public PaymentData getPaymentData() {
		return paymentData;
	}

	public void setPaymentData(PaymentData paymentData) {
		this.paymentData = paymentData;
	}

	public InitializePaymentErrorEnum getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(InitializePaymentErrorEnum errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorStatisticMessage() {
		return errorStatisticMessage;
	}

	public void setErrorStatisticMessage(String errorStatisticMessage) {
		this.errorStatisticMessage = errorStatisticMessage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((errorCode == null) ? 0 : errorCode.hashCode());
		result = prime
				* result
				+ ((errorStatisticMessage == null) ? 0 : errorStatisticMessage
						.hashCode());
		result = prime * result
				+ ((paymentData == null) ? 0 : paymentData.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		InitializePaymentResponse other = (InitializePaymentResponse) obj;
		if (errorCode != other.errorCode)
			return false;
		if (errorStatisticMessage == null) {
			if (other.errorStatisticMessage != null)
				return false;
		} else if (!errorStatisticMessage.equals(other.errorStatisticMessage))
			return false;
		if (paymentData == null) {
			if (other.paymentData != null)
				return false;
		} else if (!paymentData.equals(other.paymentData))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "InitializePaymentResponse [paymentData=" + paymentData
				+ ", errorCode=" + errorCode + ", errorStatisticMessage="
				+ errorStatisticMessage + "]";
	}
}
