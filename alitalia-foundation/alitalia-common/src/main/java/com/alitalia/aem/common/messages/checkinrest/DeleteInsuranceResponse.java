package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.messages.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeleteInsuranceResponse extends BaseResponse {
	
	@SerializedName("success")
	@Expose
	private String success;
	@SerializedName("errorDescription")
	@Expose
	private String errorDescription;
	@SerializedName("conversationID")
	@Expose
	private String conversationID;
	
	private String error;
	
	public DeleteInsuranceResponse() {}
	
	public DeleteInsuranceResponse(String tid, String sid) {
		super(tid, sid);
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public String getConversationID() {
		return conversationID;
	}

	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
	
}
