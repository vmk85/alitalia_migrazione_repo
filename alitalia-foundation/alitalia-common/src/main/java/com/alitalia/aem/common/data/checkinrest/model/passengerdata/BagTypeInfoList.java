
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BagTypeInfoList {

    @SerializedName("reasonList")
    @Expose
    private List<ReasonList_> reasonList = null;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("description")
    @Expose
    private String description;

    public List<ReasonList_> getReasonList() {
        return reasonList;
    }

    public void setReasonList(List<ReasonList_> reasonList) {
        this.reasonList = reasonList;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
