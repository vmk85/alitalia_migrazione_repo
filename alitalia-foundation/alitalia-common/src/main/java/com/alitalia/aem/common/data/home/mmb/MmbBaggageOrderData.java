package com.alitalia.aem.common.data.home.mmb;

import java.math.BigDecimal;

public class MmbBaggageOrderData {

	private Integer id;
	private String currency;
	private Integer quantity;
	private BigDecimal savings;
	private BigDecimal totalAirportFare;
	private BigDecimal totalFare;

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getSavings() {
		return savings;
	}

	public void setSavings(BigDecimal savings) {
		this.savings = savings;
	}

	public BigDecimal getTotalAirportFare() {
		return totalAirportFare;
	}

	public void setTotalAirportFare(BigDecimal totalAirportFare) {
		this.totalAirportFare = totalAirportFare;
	}

	public BigDecimal getTotalFare() {
		return totalFare;
	}

	public void setTotalFare(BigDecimal totalFare) {
		this.totalFare = totalFare;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbBaggageOrderData other = (MmbBaggageOrderData) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbBaggageOrder [id=" + id + ", currency=" + currency
				+ ", quantity=" + quantity + ", savings=" + savings
				+ ", totalAirportFare=" + totalAirportFare + ", totalFare="
				+ totalFare + "]";
	}

}
