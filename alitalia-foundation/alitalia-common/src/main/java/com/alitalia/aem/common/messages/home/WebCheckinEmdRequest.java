package com.alitalia.aem.common.messages.home;

import java.math.BigDecimal;
import java.util.List;
import com.alitalia.aem.common.data.home.checkin.CheckinCreditCardData;
import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.checkin.CheckinReasonForIssuance;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.messages.MmbBaseRequest;

public class WebCheckinEmdRequest extends MmbBaseRequest {

    private CheckinReasonForIssuance reasonForIssuance;
    private String authorizationId;
    private MmbCompartimentalClassEnum cabin;
    private String cartId;
    private CheckinCreditCardData creditCardData;
    private List<CheckinFlightData> flights;
    private Integer indexOfFlight;
    private String orderCode;
    private CheckinPassengerData passenger;
    private String seat;
    private Integer unitOfMeasureQuantity;
    private String approvalCode;
    private String city;
    private String countryCode;
    private BigDecimal cuponeAmount;
    private String currencyCode;
    private String languageCode;
    private String originCity;
    private String segments;
    private BigDecimal totalAmount;

	public WebCheckinEmdRequest(String tid, String sid) {
		super(tid, sid);
	}

	public WebCheckinEmdRequest() {
		super();
	}

	public CheckinReasonForIssuance getReasonForIssuance() {
		return reasonForIssuance;
	}

	public void setReasonForIssuance(CheckinReasonForIssuance reasonForIssuance) {
		this.reasonForIssuance = reasonForIssuance;
	}

	public String getAuthorizationId() {
		return authorizationId;
	}

	public void setAuthorizationId(String authorizationId) {
		this.authorizationId = authorizationId;
	}

	public MmbCompartimentalClassEnum getCabin() {
		return cabin;
	}

	public void setCabin(MmbCompartimentalClassEnum cabin) {
		this.cabin = cabin;
	}

	public String getCartId() {
		return cartId;
	}

	public void setCartId(String cartId) {
		this.cartId = cartId;
	}

	public CheckinCreditCardData getCreditCardData() {
		return creditCardData;
	}

	public void setCreditCardData(CheckinCreditCardData creditCardData) {
		this.creditCardData = creditCardData;
	}

	public List<CheckinFlightData> getFlights() {
		return flights;
	}

	public void setFlights(List<CheckinFlightData> flights) {
		this.flights = flights;
	}

	public Integer getIndexOfFlight() {
		return indexOfFlight;
	}

	public void setIndexOfFlight(Integer indexOfFlight) {
		this.indexOfFlight = indexOfFlight;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public CheckinPassengerData getPassenger() {
		return passenger;
	}

	public void setPassenger(CheckinPassengerData passenger) {
		this.passenger = passenger;
	}

	public String getSeat() {
		return seat;
	}

	public void setSeat(String seat) {
		this.seat = seat;
	}

	public Integer getUnitOfMeasureQuantity() {
		return unitOfMeasureQuantity;
	}

	public void setUnitOfMeasureQuantity(Integer unitOfMeasureQuantity) {
		this.unitOfMeasureQuantity = unitOfMeasureQuantity;
	}

	public String getApprovalCode() {
		return approvalCode;
	}

	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public BigDecimal getCuponeAmount() {
		return cuponeAmount;
	}

	public void setCuponeAmount(BigDecimal cuponeAmount) {
		this.cuponeAmount = cuponeAmount;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getOriginCity() {
		return originCity;
	}

	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}

	public String getSegments() {
		return segments;
	}

	public void setSegments(String segments) {
		this.segments = segments;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((approvalCode == null) ? 0 : approvalCode.hashCode());
		result = prime * result
				+ ((authorizationId == null) ? 0 : authorizationId.hashCode());
		result = prime * result + ((cabin == null) ? 0 : cabin.hashCode());
		result = prime * result + ((cartId == null) ? 0 : cartId.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result
				+ ((countryCode == null) ? 0 : countryCode.hashCode());
		result = prime * result
				+ ((creditCardData == null) ? 0 : creditCardData.hashCode());
		result = prime * result
				+ ((cuponeAmount == null) ? 0 : cuponeAmount.hashCode());
		result = prime * result
				+ ((currencyCode == null) ? 0 : currencyCode.hashCode());
		result = prime * result + ((flights == null) ? 0 : flights.hashCode());
		result = prime * result
				+ ((indexOfFlight == null) ? 0 : indexOfFlight.hashCode());
		result = prime * result
				+ ((languageCode == null) ? 0 : languageCode.hashCode());
		result = prime * result
				+ ((orderCode == null) ? 0 : orderCode.hashCode());
		result = prime * result
				+ ((originCity == null) ? 0 : originCity.hashCode());
		result = prime * result
				+ ((passenger == null) ? 0 : passenger.hashCode());
		result = prime
				* result
				+ ((reasonForIssuance == null) ? 0 : reasonForIssuance
						.hashCode());
		result = prime * result + ((seat == null) ? 0 : seat.hashCode());
		result = prime * result
				+ ((segments == null) ? 0 : segments.hashCode());
		result = prime * result
				+ ((totalAmount == null) ? 0 : totalAmount.hashCode());
		result = prime
				* result
				+ ((unitOfMeasureQuantity == null) ? 0 : unitOfMeasureQuantity
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebCheckinEmdRequest other = (WebCheckinEmdRequest) obj;
		if (approvalCode == null) {
			if (other.approvalCode != null)
				return false;
		} else if (!approvalCode.equals(other.approvalCode))
			return false;
		if (authorizationId == null) {
			if (other.authorizationId != null)
				return false;
		} else if (!authorizationId.equals(other.authorizationId))
			return false;
		if (cabin != other.cabin)
			return false;
		if (cartId == null) {
			if (other.cartId != null)
				return false;
		} else if (!cartId.equals(other.cartId))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (countryCode == null) {
			if (other.countryCode != null)
				return false;
		} else if (!countryCode.equals(other.countryCode))
			return false;
		if (creditCardData == null) {
			if (other.creditCardData != null)
				return false;
		} else if (!creditCardData.equals(other.creditCardData))
			return false;
		if (cuponeAmount == null) {
			if (other.cuponeAmount != null)
				return false;
		} else if (!cuponeAmount.equals(other.cuponeAmount))
			return false;
		if (currencyCode == null) {
			if (other.currencyCode != null)
				return false;
		} else if (!currencyCode.equals(other.currencyCode))
			return false;
		if (flights == null) {
			if (other.flights != null)
				return false;
		} else if (!flights.equals(other.flights))
			return false;
		if (indexOfFlight == null) {
			if (other.indexOfFlight != null)
				return false;
		} else if (!indexOfFlight.equals(other.indexOfFlight))
			return false;
		if (languageCode == null) {
			if (other.languageCode != null)
				return false;
		} else if (!languageCode.equals(other.languageCode))
			return false;
		if (orderCode == null) {
			if (other.orderCode != null)
				return false;
		} else if (!orderCode.equals(other.orderCode))
			return false;
		if (originCity == null) {
			if (other.originCity != null)
				return false;
		} else if (!originCity.equals(other.originCity))
			return false;
		if (passenger == null) {
			if (other.passenger != null)
				return false;
		} else if (!passenger.equals(other.passenger))
			return false;
		if (reasonForIssuance == null) {
			if (other.reasonForIssuance != null)
				return false;
		} else if (!reasonForIssuance.equals(other.reasonForIssuance))
			return false;
		if (seat == null) {
			if (other.seat != null)
				return false;
		} else if (!seat.equals(other.seat))
			return false;
		if (segments == null) {
			if (other.segments != null)
				return false;
		} else if (!segments.equals(other.segments))
			return false;
		if (totalAmount == null) {
			if (other.totalAmount != null)
				return false;
		} else if (!totalAmount.equals(other.totalAmount))
			return false;
		if (unitOfMeasureQuantity == null) {
			if (other.unitOfMeasureQuantity != null)
				return false;
		} else if (!unitOfMeasureQuantity.equals(other.unitOfMeasureQuantity))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WebCheckinEmdRequest [reasonForIssuance=" + reasonForIssuance
				+ ", authorizationId=" + authorizationId + ", cabin=" + cabin
				+ ", cartId=" + cartId + ", creditCardData=" + creditCardData
				+ ", flights=" + flights + ", indexOfFlight=" + indexOfFlight
				+ ", orderCode=" + orderCode + ", passenger=" + passenger
				+ ", seat=" + seat + ", unitOfMeasureQuantity="
				+ unitOfMeasureQuantity + ", approvalCode=" + approvalCode
				+ ", city=" + city + ", countryCode=" + countryCode
				+ ", cuponeAmount=" + cuponeAmount + ", currencyCode="
				+ currencyCode + ", languageCode=" + languageCode
				+ ", originCity=" + originCity + ", segments=" + segments
				+ ", totalAmount=" + totalAmount + "]";
	}
}