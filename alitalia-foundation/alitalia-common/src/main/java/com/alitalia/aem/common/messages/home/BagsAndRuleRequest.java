package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.messages.BaseRequest;

public class BagsAndRuleRequest extends BaseRequest {

	private List<String> solutionIds;

	public List<String> getSolutionIds() {
		return solutionIds;
	}

	public void setSolutionIds(List<String> solutionIds) {
		this.solutionIds = solutionIds;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((solutionIds == null) ? 0 : solutionIds.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		BagsAndRuleRequest other = (BagsAndRuleRequest) obj;
		if (solutionIds == null) {
			if (other.solutionIds != null)
				return false;
		} else if (!solutionIds.equals(other.solutionIds))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BagsAndRuleRequest [solutionIds=" + solutionIds
				+ ", getCookie()=" + getCookie() + ", getExecution()="
				+ getExecution() + "]";
	}
}