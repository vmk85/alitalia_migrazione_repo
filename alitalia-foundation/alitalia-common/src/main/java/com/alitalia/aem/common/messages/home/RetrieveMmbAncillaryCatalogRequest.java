package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryFlightData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryPassengerData;
import com.alitalia.aem.common.messages.MmbBaseRequest;

public class RetrieveMmbAncillaryCatalogRequest extends MmbBaseRequest {

	private String language;
	private String sessionId;
	private String machineName;
	private String market;
	private String pnr;
	private List<MmbAncillaryPassengerData> passengers;
	private List<MmbAncillaryFlightData> flights;

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getMachineName() {
		return machineName;
	}

	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public List<MmbAncillaryPassengerData> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<MmbAncillaryPassengerData> passengers) {
		this.passengers = passengers;
	}

	public List<MmbAncillaryFlightData> getFlights() {
		return flights;
	}

	public void setFlights(List<MmbAncillaryFlightData> flights) {
		this.flights = flights;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((language == null) ? 0 : language.hashCode());
		result = prime * result
				+ ((machineName == null) ? 0 : machineName.hashCode());
		result = prime * result + ((market == null) ? 0 : market.hashCode());
		result = prime * result + ((pnr == null) ? 0 : pnr.hashCode());
		result = prime * result
				+ ((sessionId == null) ? 0 : sessionId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveMmbAncillaryCatalogRequest other = (RetrieveMmbAncillaryCatalogRequest) obj;
		if (language == null) {
			if (other.language != null)
				return false;
		} else if (!language.equals(other.language))
			return false;
		if (machineName == null) {
			if (other.machineName != null)
				return false;
		} else if (!machineName.equals(other.machineName))
			return false;
		if (market == null) {
			if (other.market != null)
				return false;
		} else if (!market.equals(other.market))
			return false;
		if (pnr == null) {
			if (other.pnr != null)
				return false;
		} else if (!pnr.equals(other.pnr))
			return false;
		if (sessionId == null) {
			if (other.sessionId != null)
				return false;
		} else if (!sessionId.equals(other.sessionId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveMmbAncillaryCatalogRequest [language=" + language
				+ ", sessionId=" + sessionId + ", machineName=" + machineName
				+ ", market=" + market + ", pnr=" + pnr + ", passengers="
				+ passengers + ", flights=" + flights + ", getBaseInfo()="
				+ getBaseInfo() + ", getClient()=" + getClient()
				+ ", getTid()=" + getTid() + ", getSid()=" + getSid() + "]";
	}

}
