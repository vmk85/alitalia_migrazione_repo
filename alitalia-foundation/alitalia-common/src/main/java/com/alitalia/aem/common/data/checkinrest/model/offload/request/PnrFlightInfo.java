package com.alitalia.aem.common.data.checkinrest.model.offload.request;

/**
 * Created by ggadaleta on 25/02/2018.
 */

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PnrFlightInfo {

    @SerializedName("pnr")
    @Expose
    private String pnr;
    @SerializedName("flightPass")
    @Expose
    private List<FlightPas> flightPass = null;

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public List<FlightPas> getFlightPass() {
        return flightPass;
    }

    public void setFlightPass(List<FlightPas> flightPass) {
        this.flightPass = flightPass;
    }

}
