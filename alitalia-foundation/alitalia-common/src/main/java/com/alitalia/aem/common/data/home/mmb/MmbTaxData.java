package com.alitalia.aem.common.data.home.mmb;

import java.math.BigDecimal;

import com.alitalia.aem.common.data.home.TaxData;

public class MmbTaxData extends TaxData {

	private String passengerTypeQuantity;
	private BigDecimal totalAmount;

	public String getPassengerTypeQuantity() {
		return passengerTypeQuantity;
	}

	public void setPassengerTypeQuantity(String passengerTypeQuantity) {
		this.passengerTypeQuantity = passengerTypeQuantity;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	@Override
	public String toString() {
		return "MmbTaxData [passengerTypeQuantity=" + passengerTypeQuantity
				+ ", totalAmount=" + totalAmount + ", getAmount()="
				+ getAmount() + ", getCode()=" + getCode() + ", getCurrency()="
				+ getCurrency() + ", getId()=" + getId() + ", getName()="
				+ getName() + "]";
	}

}
