package com.alitalia.aem.common.messages.home;

import java.util.Arrays;

import com.alitalia.aem.common.messages.MmbBaseRequest;

public class SendReceiptCheckinRequest extends MmbBaseRequest {

    private byte[] attachment;
    private String attachmentName;
    private String emailAddress;
    private String emailBody;
    private String emailSubject;
    
	public byte[] getAttachment() {
		return attachment;
	}
	public void setAttachment(byte[] attachment) {
		this.attachment = attachment;
	}
	public String getAttachmentName() {
		return attachmentName;
	}
	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getEmailBody() {
		return emailBody;
	}
	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}
	public String getEmailSubject() {
		return emailSubject;
	}
	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Arrays.hashCode(attachment);
		result = prime * result
				+ ((attachmentName == null) ? 0 : attachmentName.hashCode());
		result = prime * result
				+ ((emailAddress == null) ? 0 : emailAddress.hashCode());
		result = prime * result
				+ ((emailBody == null) ? 0 : emailBody.hashCode());
		result = prime * result
				+ ((emailSubject == null) ? 0 : emailSubject.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SendReceiptCheckinRequest other = (SendReceiptCheckinRequest) obj;
		if (!Arrays.equals(attachment, other.attachment))
			return false;
		if (attachmentName == null) {
			if (other.attachmentName != null)
				return false;
		} else if (!attachmentName.equals(other.attachmentName))
			return false;
		if (emailAddress == null) {
			if (other.emailAddress != null)
				return false;
		} else if (!emailAddress.equals(other.emailAddress))
			return false;
		if (emailBody == null) {
			if (other.emailBody != null)
				return false;
		} else if (!emailBody.equals(other.emailBody))
			return false;
		if (emailSubject == null) {
			if (other.emailSubject != null)
				return false;
		} else if (!emailSubject.equals(other.emailSubject))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "SendReceiptCheckinRequest [attachment="
				+ Arrays.toString(attachment) + ", attachmentName="
				+ attachmentName + ", emailAddress=" + emailAddress
				+ ", emailBody=" + emailBody + ", emailSubject=" + emailSubject
				+ "]";
	}  

}
