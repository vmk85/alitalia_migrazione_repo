package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class CarnetSendEmailRequest extends BaseRequest {

    private String mailHtml;
    private String mailTo;

	public CarnetSendEmailRequest(String tid, String sid) {
		super(tid, sid);
	}

	public CarnetSendEmailRequest() {
		super();
	}

	public String getMailHtml() {
		return mailHtml;
	}

	public void setMailHtml(String mailHtml) {
		this.mailHtml = mailHtml;
	}

	public String getMailTo() {
		return mailTo;
	}

	public void setMailTo(String mailTo) {
		this.mailTo = mailTo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((mailHtml == null) ? 0 : mailHtml.hashCode());
		result = prime * result + ((mailTo == null) ? 0 : mailTo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarnetSendEmailRequest other = (CarnetSendEmailRequest) obj;
		if (mailHtml == null) {
			if (other.mailHtml != null)
				return false;
		} else if (!mailHtml.equals(other.mailHtml))
			return false;
		if (mailTo == null) {
			if (other.mailTo != null)
				return false;
		} else if (!mailTo.equals(other.mailTo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CarnetSendEmailRequest [mailHtml=" + mailHtml + ", mailTo="
				+ mailTo + "]";
	}
}