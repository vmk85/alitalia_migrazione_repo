package com.alitalia.aem.common.data.home.carnet;

import java.util.List;

import com.alitalia.aem.common.data.home.CodeDescriptionData;
import com.alitalia.aem.common.data.home.DictionaryItemData;

public class CarnetPaymentTypeItemData extends CodeDescriptionData {
	
	private List<DictionaryItemData> otherInfo;

	public List<DictionaryItemData> getOtherInfo() {
		return otherInfo;
	}

	public void setOtherInfo(List<DictionaryItemData> otherInfo) {
		this.otherInfo = otherInfo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((otherInfo == null) ? 0 : otherInfo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarnetPaymentTypeItemData other = (CarnetPaymentTypeItemData) obj;
		if (otherInfo == null) {
			if (other.otherInfo != null)
				return false;
		} else if (!otherInfo.equals(other.otherInfo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CarnetPaymentTypeItemData [otherInfo=" + otherInfo + "]";
	}
}