
package com.alitalia.aem.common.data.checkinrest.model.searchbypnr;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchByPnr {

    @Override
	public String toString() {
		return "SearchByPnr [pnr=" + pnr + ", conversationID=" + conversationID + ", erroreSearch=" + erroreSearch
				+ "]";
	}

	@SerializedName("pnr")
    @Expose
    private List<Pnr> pnr = null;
    @SerializedName("conversationID")
    @Expose
    private String conversationID;
    
    private String erroreSearch;

    public List<Pnr> getPnr() {
        return pnr;
    }

    public void setPnr(List<Pnr> pnr) {
        this.pnr = pnr;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

	public String getErroreSearch() {
		return erroreSearch;
	}

	public void setErroreSearch(String erroreSearch) {
		this.erroreSearch = erroreSearch;
	}

}
