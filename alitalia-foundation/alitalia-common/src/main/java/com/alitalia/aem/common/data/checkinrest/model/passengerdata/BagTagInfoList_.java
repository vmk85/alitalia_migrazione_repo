
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BagTagInfoList_ {

    @SerializedName("bagTagInfo")
    @Expose
    private List<BagTagInfo> bagTagInfo = null;
    @SerializedName("totalWeightAndUnit")
    @Expose
    private TotalWeightAndUnit totalWeightAndUnit;

    public List<BagTagInfo> getBagTagInfo() {
        return bagTagInfo;
    }

    public void setBagTagInfo(List<BagTagInfo> bagTagInfo) {
        this.bagTagInfo = bagTagInfo;
    }

    public TotalWeightAndUnit getTotalWeightAndUnit() {
        return totalWeightAndUnit;
    }

    public void setTotalWeightAndUnit(TotalWeightAndUnit totalWeightAndUnit) {
        this.totalWeightAndUnit = totalWeightAndUnit;
    }

}
