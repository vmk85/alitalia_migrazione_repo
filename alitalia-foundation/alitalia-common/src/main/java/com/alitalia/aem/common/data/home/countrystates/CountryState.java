package com.alitalia.aem.common.data.home.countrystates;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CountryState {
	
	@SerializedName("stateID")
	@Expose
	private String stateID;
	@SerializedName("stateCode")
	@Expose
	private String stateCode;
	@SerializedName("state")
	@Expose
	private String state;
	@SerializedName("countryCode")
	@Expose
	private String countryCode;
	@SerializedName("country")
	@Expose
	private String country;
	@SerializedName("fullStateName")
	@Expose
	private String fullStateName;
	
	public String getStateID() {
		return stateID;
	}
	
	public void setStateID(String stateID) {
		this.stateID = stateID;
	}
	
	public String getStateCode() {
		return stateCode;
	}
	
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public String getCountryCode() {
		return countryCode;
	}
	
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getFullStateName() {
		return fullStateName;
	}
	
	public void setFullStateName(String fullStateName) {
		this.fullStateName = fullStateName;
	}
	
}
