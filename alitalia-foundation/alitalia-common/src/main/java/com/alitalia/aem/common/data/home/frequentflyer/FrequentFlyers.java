
package com.alitalia.aem.common.data.home.frequentflyer;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FrequentFlyers {

    @SerializedName("frequentFlyer")
    @Expose
    private List<FrequentFlyer> frequentFlyer = null;
    @SerializedName("conversationID")
    @Expose
    private String conversationID;

    public List<FrequentFlyer> getFrequentFlyer() {
        return frequentFlyer;
    }

    public void setFrequentFlyer(List<FrequentFlyer> frequentFlyer) {
        this.frequentFlyer = frequentFlyer;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

}
