package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.MmbAncillaryErrorData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryCartData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetrieveMmbAncillaryCatalogResponse extends BaseResponse {

	private MmbAncillaryCartData cart;
	private String machine;
	private List<MmbAncillaryErrorData> errors;

	public MmbAncillaryCartData getCart() {
		return cart;
	}

	public void setCart(MmbAncillaryCartData cart) {
		this.cart = cart;
	}

	public String getMachine() {
		return machine;
	}

	public void setMachine(String machine) {
		this.machine = machine;
	}

	public List<MmbAncillaryErrorData> getErrors() {
		return errors;
	}

	public void setErrors(List<MmbAncillaryErrorData> errors) {
		this.errors = errors;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((cart == null) ? 0 : cart.hashCode());
		result = prime * result + ((machine == null) ? 0 : machine.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveMmbAncillaryCatalogResponse other = (RetrieveMmbAncillaryCatalogResponse) obj;
		if (cart == null) {
			if (other.cart != null)
				return false;
		} else if (!cart.equals(other.cart))
			return false;
		if (machine == null) {
			if (other.machine != null)
				return false;
		} else if (!machine.equals(other.machine))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveMmbAncillaryCatalogResponse [cart=" + cart
				+ ", machine=" + machine + ", errors=" + errors + ", getTid()="
				+ getTid() + ", getSid()=" + getSid() + "]";
	}

}
