package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class AgencyPasswordChangeResponse extends BaseResponse {

	private Boolean operationSuccessful;

	public AgencyPasswordChangeResponse() {
		super();
		this.operationSuccessful = false;
	}

	public AgencyPasswordChangeResponse(Boolean operationSuccessful) {
		super();
		this.operationSuccessful = operationSuccessful;
	}

	public AgencyPasswordChangeResponse(String tid, String sid, Boolean operationSuccessful) {
		super(tid, sid);
		this.operationSuccessful = operationSuccessful;
	}

	public Boolean isOperationSuccessful() {
		return operationSuccessful;
	}

	public void setOperationSuccessful(Boolean operationSuccessful) {
		this.operationSuccessful = operationSuccessful;
	}

	@Override
	public String toString() {
		return "AgencyPasswordChangeResponse [operationSuccessful="
				+ operationSuccessful + "]";
	}
}
