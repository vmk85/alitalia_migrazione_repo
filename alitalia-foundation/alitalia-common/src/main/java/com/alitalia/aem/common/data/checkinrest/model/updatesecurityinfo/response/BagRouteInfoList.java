
package com.alitalia.aem.common.data.checkinrest.model.updatesecurityinfo.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BagRouteInfoList {

    @SerializedName("flightInfo")
    @Expose
    private FlightInfo_ flightInfo;
    @SerializedName("checkinSequenceNo")
    @Expose
    private Integer checkinSequenceNo;
    @SerializedName("checkinSequenceNoSpecified")
    @Expose
    private Boolean checkinSequenceNoSpecified;
    @SerializedName("destinationInfo")
    @Expose
    private DestinationInfo destinationInfo;
    @SerializedName("operatingTypeAirlineInfo")
    @Expose
    private List<OperatingTypeAirlineInfo> operatingTypeAirlineInfo = null;
    @SerializedName("airportZoneGroup")
    @Expose
    private String airportZoneGroup;

    public FlightInfo_ getFlightInfo() {
        return flightInfo;
    }

    public void setFlightInfo(FlightInfo_ flightInfo) {
        this.flightInfo = flightInfo;
    }

    public Integer getCheckinSequenceNo() {
        return checkinSequenceNo;
    }

    public void setCheckinSequenceNo(Integer checkinSequenceNo) {
        this.checkinSequenceNo = checkinSequenceNo;
    }

    public Boolean getCheckinSequenceNoSpecified() {
        return checkinSequenceNoSpecified;
    }

    public void setCheckinSequenceNoSpecified(Boolean checkinSequenceNoSpecified) {
        this.checkinSequenceNoSpecified = checkinSequenceNoSpecified;
    }

    public DestinationInfo getDestinationInfo() {
        return destinationInfo;
    }

    public void setDestinationInfo(DestinationInfo destinationInfo) {
        this.destinationInfo = destinationInfo;
    }

    public List<OperatingTypeAirlineInfo> getOperatingTypeAirlineInfo() {
        return operatingTypeAirlineInfo;
    }

    public void setOperatingTypeAirlineInfo(List<OperatingTypeAirlineInfo> operatingTypeAirlineInfo) {
        this.operatingTypeAirlineInfo = operatingTypeAirlineInfo;
    }

    public String getAirportZoneGroup() {
        return airportZoneGroup;
    }

    public void setAirportZoneGroup(String airportZoneGroup) {
        this.airportZoneGroup = airportZoneGroup;
    }

}
