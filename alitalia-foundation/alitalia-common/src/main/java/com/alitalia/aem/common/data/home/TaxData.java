package com.alitalia.aem.common.data.home;

import java.math.BigDecimal;

public class TaxData {

    private BigDecimal amount;
    private String code;
    private String currency;
    private String id;
    private String name;
    
	public BigDecimal getAmount() {
		return amount;
	}
	
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getCurrency() {
		return currency;
	}
	
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "TaxData [amount=" + amount + ", code=" + code + ", currency="
				+ currency + ", cid=" + id + ", name=" + name + "]";
	}
    
}
