
package com.alitalia.aem.common.data.checkinrest.model.fasttrack.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Passenger {

    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("passengerID")
    @Expose
    private String passengerID;
    @SerializedName("fastTrack")
    @Expose
    private Boolean fastTrack;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassengerID() {
        return passengerID;
    }

    public void setPassengerID(String passengerID) {
        this.passengerID = passengerID;
    }

    public Boolean getFastTrack() {
        return fastTrack;
    }

    public void setFastTrack(Boolean fastTrack) {
        this.fastTrack = fastTrack;
    }

}
