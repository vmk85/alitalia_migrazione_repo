
package com.alitalia.aem.common.data.checkinrest.model.updatesecurityinfo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WeightAndSize_ {

    @SerializedName("weightAndUnit")
    @Expose
    private WeightAndUnit_ weightAndUnit;
    @SerializedName("lengthAndUnit")
    @Expose
    private LengthAndUnit_ lengthAndUnit;

    public WeightAndUnit_ getWeightAndUnit() {
        return weightAndUnit;
    }

    public void setWeightAndUnit(WeightAndUnit_ weightAndUnit) {
        this.weightAndUnit = weightAndUnit;
    }

    public LengthAndUnit_ getLengthAndUnit() {
        return lengthAndUnit;
    }

    public void setLengthAndUnit(LengthAndUnit_ lengthAndUnit) {
        this.lengthAndUnit = lengthAndUnit;
    }

}
