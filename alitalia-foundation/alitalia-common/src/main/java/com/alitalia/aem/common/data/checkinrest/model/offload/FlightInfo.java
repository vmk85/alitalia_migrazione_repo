package com.alitalia.aem.common.data.checkinrest.model.offload;

import java.util.List;

import com.alitalia.aem.common.data.checkinrest.model.offload.request.SeatList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
@Deprecated
public class FlightInfo {

    @SerializedName("airline")
    @Expose
    private String airline;
    @SerializedName("flight")
    @Expose
    private String flight;
    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("departureDate")
    @Expose
    private String departureDate;
    @SerializedName("seatList")
    @Expose
    private List<SeatList> seatList = null;

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getFlight() {
        return flight;
    }

    public void setFlight(String flight) {
        this.flight = flight;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public List<SeatList> getSeatList() {
        return seatList;
    }

    public void setSeatList(List<SeatList> seatList) {
        this.seatList = seatList;
    }

}