
package com.alitalia.aem.common.data.checkinrest.model.reprintbp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SeatDetails {

    @SerializedName("seatNumber")
    @Expose
    private String seatNumber;
    @SerializedName("seatAllowedIndicator")
    @Expose
    private Boolean seatAllowedIndicator;
    @SerializedName("seatChargeableIndicator")
    @Expose
    private Boolean seatChargeableIndicator;
    @SerializedName("entitlementRuleId")
    @Expose
    private String entitlementRuleId;
    @SerializedName("feeWaiverRuleId")
    @Expose
    private String feeWaiverRuleId;
    @SerializedName("aeItemID")
    @Expose
    private String aeItemID;

    public String getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(String seatNumber) {
        this.seatNumber = seatNumber;
    }

    public Boolean getSeatAllowedIndicator() {
        return seatAllowedIndicator;
    }

    public void setSeatAllowedIndicator(Boolean seatAllowedIndicator) {
        this.seatAllowedIndicator = seatAllowedIndicator;
    }

    public Boolean getSeatChargeableIndicator() {
        return seatChargeableIndicator;
    }

    public void setSeatChargeableIndicator(Boolean seatChargeableIndicator) {
        this.seatChargeableIndicator = seatChargeableIndicator;
    }

    public String getEntitlementRuleId() {
        return entitlementRuleId;
    }

    public void setEntitlementRuleId(String entitlementRuleId) {
        this.entitlementRuleId = entitlementRuleId;
    }

    public String getFeeWaiverRuleId() {
        return feeWaiverRuleId;
    }

    public void setFeeWaiverRuleId(String feeWaiverRuleId) {
        this.feeWaiverRuleId = feeWaiverRuleId;
    }

    public String getAeItemID() {
        return aeItemID;
    }

    public void setAeItemID(String aeItemID) {
        this.aeItemID = aeItemID;
    }

}
