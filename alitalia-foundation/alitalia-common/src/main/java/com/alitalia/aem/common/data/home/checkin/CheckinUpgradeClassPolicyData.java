package com.alitalia.aem.common.data.home.checkin;

import java.math.BigDecimal;

import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;

public class CheckinUpgradeClassPolicyData {
	
	private Integer id;
	private MmbCompartimentalClassEnum compartimentalClass;
	private String currency;
	private BigDecimal price;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public MmbCompartimentalClassEnum getCompartimentalClass() {
		return compartimentalClass;
	}
	public void setCompartimentalClass(
			MmbCompartimentalClassEnum compartimentalClass) {
		this.compartimentalClass = compartimentalClass;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((compartimentalClass == null) ? 0 : compartimentalClass
						.hashCode());
		result = prime * result
				+ ((currency == null) ? 0 : currency.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinUpgradeClassPolicyData other = (CheckinUpgradeClassPolicyData) obj;
		if (compartimentalClass != other.compartimentalClass)
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CheckinUpgradeClassPolicyData [id=" + id + ", compartimentalClass="
				+ compartimentalClass + ", currency=" + currency + ", price="
				+ price + "]";
	}

}
