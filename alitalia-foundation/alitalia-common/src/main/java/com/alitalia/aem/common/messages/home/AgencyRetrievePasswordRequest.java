package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.enumerations.AlitaliaTradeUserType;
import com.alitalia.aem.common.messages.BaseRequest;

public class AgencyRetrievePasswordRequest extends BaseRequest {

	private String identificativo;
	private AlitaliaTradeUserType ruolo;
	
	public AgencyRetrievePasswordRequest(String tid, String sid) {
		super(tid, sid);
	}

	public AgencyRetrievePasswordRequest(String tid, String sid,
			String identificativo, AlitaliaTradeUserType ruolo) {
		super(tid, sid);
		this.identificativo = identificativo;
		this.ruolo = ruolo;
	}

	public String getIdentificativo() {
		return identificativo;
	}
	
	public void setIdentificativo(String identificativo) {
		this.identificativo = identificativo;
	}
	
	public AlitaliaTradeUserType getRuolo() {
		return ruolo;
	}
	
	public void setRuolo(AlitaliaTradeUserType ruolo) {
		this.ruolo = ruolo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((identificativo == null) ? 0 : identificativo.hashCode());
		result = prime * result + ((ruolo == null) ? 0 : ruolo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgencyRetrievePasswordRequest other = (AgencyRetrievePasswordRequest) obj;
		if (identificativo == null) {
			if (other.identificativo != null)
				return false;
		} else if (!identificativo.equals(other.identificativo))
			return false;
		if (ruolo != other.ruolo)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AgencyRetrievePasswordRequest [identificativo="
				+ identificativo + ", ruolo=" + ruolo + ", getTid()="
				+ getTid() + ", getSid()=" + getSid() + "]";
	}
}
