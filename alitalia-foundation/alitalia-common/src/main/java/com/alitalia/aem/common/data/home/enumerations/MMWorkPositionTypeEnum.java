package com.alitalia.aem.common.data.home.enumerations;

public enum MMWorkPositionTypeEnum {

	UNKNOWN("UnKnown"),
	LIBERO_PROFESSIONISTA("LiberoProfessionista"),
	IMPRENDITORE("Imprenditore"),
	GIORNALISTA("Giornalista"),
	IMPIEGATO("Impiegato"),
	QUADRO("Quadro"),
	SYSTEM("SYSTEM"),
	DIRETTORE_ACQUISTI("DirettoreAcquisti"),
	DIRETTORE_FINANZIARIO("DirettoreFinanziario"),
	DIRIGENTE("Dirigente"),
	ALTRO("Altro"),
	TRAVEL_MANAGER("TravelManager");

	private final String value;

	private MMWorkPositionTypeEnum(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	public static MMWorkPositionTypeEnum fromValue(String v) {
		for (MMWorkPositionTypeEnum c: MMWorkPositionTypeEnum.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}


}
