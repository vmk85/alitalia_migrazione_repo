package com.alitalia.aem.common.data.home.mmb;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbApisTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbFlightStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbLegTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;

public class MmbTicketData {

	private Integer id;
	private MmbApisTypeEnum apisTypeRequired;
	private Date arrivalDate;
	private Integer baggageAllow;
	private CabinEnum cabin;
	private String carrier;
	private BigDecimal comfortSeatFare;
	private Boolean comfortSeatPaid;
	private MmbDeepLinkData deepLinkCode;
	private Date departureDate;
	private Boolean enabledSeatMap;
	private String eticketClass;
	private String eticket;
	private Integer extraBaggage;
	private String fareClass;
	private String flightNumber;
	private String fromCity;
	private String fromTerminal;
	private MmbAirportData from;
	private Boolean hasComfortSeat;
	private Integer index;
	private MmbLegTypeEnum legType;
	private Integer multitratta;
	private String operatingCarrier;
	private String operatingFlightNumber;
	private List<MmbPassengerData> passengers;
	private String pnr;
	private Integer rph;
	private Integer routeId;
	private MmbCompartimentalClassEnum seatClass;
	private MmbFlightStatusEnum status;
	private String toCity;
	private String toTerminal;
	private MmbAirportData to;
	private RouteTypeEnum type;
	private Boolean firstFlight;
	private Boolean miniFare;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public MmbApisTypeEnum getApisTypeRequired() {
		return apisTypeRequired;
	}

	public void setApisTypeRequired(MmbApisTypeEnum apisTypeRequired) {
		this.apisTypeRequired = apisTypeRequired;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public Integer getBaggageAllow() {
		return baggageAllow;
	}

	public void setBaggageAllow(Integer baggageAllow) {
		this.baggageAllow = baggageAllow;
	}

	public CabinEnum getCabin() {
		return cabin;
	}

	public void setCabin(CabinEnum cabin) {
		this.cabin = cabin;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public BigDecimal getComfortSeatFare() {
		return comfortSeatFare;
	}

	public void setComfortSeatFare(BigDecimal comfortSeatFare) {
		this.comfortSeatFare = comfortSeatFare;
	}

	public Boolean isComfortSeatPaid() {
		return comfortSeatPaid;
	}

	public void setComfortSeatPaid(Boolean comfortSeatPaid) {
		this.comfortSeatPaid = comfortSeatPaid;
	}

	public MmbDeepLinkData getDeepLinkCode() {
		return deepLinkCode;
	}

	public void setDeepLinkCode(MmbDeepLinkData deepLinkCode) {
		this.deepLinkCode = deepLinkCode;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public Boolean isEnabledSeatMap() {
		return enabledSeatMap;
	}

	public void setEnabledSeatMap(Boolean enabledSeatMap) {
		this.enabledSeatMap = enabledSeatMap;
	}

	public String getEticketClass() {
		return eticketClass;
	}

	public void setEticketClass(String eticketClass) {
		this.eticketClass = eticketClass;
	}

	public String getEticket() {
		return eticket;
	}

	public void setEticket(String eticket) {
		this.eticket = eticket;
	}

	public Integer getExtraBaggage() {
		return extraBaggage;
	}

	public void setExtraBaggage(Integer extraBaggage) {
		this.extraBaggage = extraBaggage;
	}

	public String getFareClass() {
		return fareClass;
	}

	public void setFareClass(String fareClass) {
		this.fareClass = fareClass;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getFromCity() {
		return fromCity;
	}

	public void setFromCity(String fromCity) {
		this.fromCity = fromCity;
	}

	public String getFromTerminal() {
		return fromTerminal;
	}

	public void setFromTerminal(String fromTerminal) {
		this.fromTerminal = fromTerminal;
	}

	public MmbAirportData getFrom() {
		return from;
	}

	public void setFrom(MmbAirportData from) {
		this.from = from;
	}

	public Boolean hasComfortSeat() {
		return hasComfortSeat;
	}

	public void setHasComfortSeat(Boolean hasComfortSeat) {
		this.hasComfortSeat = hasComfortSeat;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public MmbLegTypeEnum getLegType() {
		return legType;
	}

	public void setLegType(MmbLegTypeEnum legType) {
		this.legType = legType;
	}

	public Integer getMultitratta() {
		return multitratta;
	}

	public void setMultitratta(Integer multitratta) {
		this.multitratta = multitratta;
	}

	public String getOperatingCarrier() {
		return operatingCarrier;
	}

	public void setOperatingCarrier(String operatingCarrier) {
		this.operatingCarrier = operatingCarrier;
	}

	public String getOperatingFlightNumber() {
		return operatingFlightNumber;
	}

	public void setOperatingFlightNumber(String operatingFlightNumber) {
		this.operatingFlightNumber = operatingFlightNumber;
	}

	public List<MmbPassengerData> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<MmbPassengerData> passengers) {
		this.passengers = passengers;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Integer getRph() {
		return rph;
	}

	public void setRph(Integer rph) {
		this.rph = rph;
	}

	public Integer getRouteId() {
		return routeId;
	}

	public void setRouteId(Integer routeId) {
		this.routeId = routeId;
	}

	public MmbCompartimentalClassEnum getSeatClass() {
		return seatClass;
	}

	public void setSeatClass(MmbCompartimentalClassEnum seatClass) {
		this.seatClass = seatClass;
	}

	public MmbFlightStatusEnum getStatus() {
		return status;
	}

	public void setStatus(MmbFlightStatusEnum status) {
		this.status = status;
	}

	public String getToCity() {
		return toCity;
	}

	public void setToCity(String toCity) {
		this.toCity = toCity;
	}

	public String getToTerminal() {
		return toTerminal;
	}

	public void setToTerminal(String toTerminal) {
		this.toTerminal = toTerminal;
	}

	public MmbAirportData getTo() {
		return to;
	}

	public void setTo(MmbAirportData to) {
		this.to = to;
	}

	public RouteTypeEnum getType() {
		return type;
	}

	public void setType(RouteTypeEnum type) {
		this.type = type;
	}

	public Boolean isFirstFlight() {
		return firstFlight;
	}

	public void setFirstFlight(Boolean firstFlight) {
		this.firstFlight = firstFlight;
	}

	public Boolean isMiniFare() {
		return miniFare;
	}

	public void setMiniFare(Boolean miniFare) {
		this.miniFare = miniFare;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbTicketData other = (MmbTicketData) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbTicketData [id=" + id + ", apisTypeRequired="
				+ apisTypeRequired + ", arrivalDate=" + arrivalDate
				+ ", baggageAllow=" + baggageAllow + ", cabin=" + cabin
				+ ", carrier=" + carrier + ", comfortSeatFare="
				+ comfortSeatFare + ", comfortSeatPaid=" + comfortSeatPaid
				+ ", deepLinkCode=" + deepLinkCode + ", departureDate="
				+ departureDate + ", enabledSeatMap=" + enabledSeatMap
				+ ", eticketClass=" + eticketClass + ", eticket=" + eticket
				+ ", extraBaggage=" + extraBaggage + ", fareClass=" + fareClass
				+ ", flightNumber=" + flightNumber + ", fromCity=" + fromCity
				+ ", fromTerminal=" + fromTerminal + ", from=" + from
				+ ", hasComfortSeat=" + hasComfortSeat + ", index=" + index
				+ ", legType=" + legType + ", multitratta=" + multitratta
				+ ", operatingCarrier=" + operatingCarrier
				+ ", operatingFlightNumber=" + operatingFlightNumber
				+ ", passengers=" + passengers + ", pnr=" + pnr + ", rph="
				+ rph + ", routeId=" + routeId + ", seatClass=" + seatClass
				+ ", status=" + status + ", toCity=" + toCity + ", toTerminal="
				+ toTerminal + ", to=" + to + ", type=" + type
				+ ", firstFlight=" + firstFlight + ", miniFare=" + miniFare
				+ "]";
	}

}
