package com.alitalia.aem.common.data.home.mmb;

import com.alitalia.aem.common.data.home.MACustomer;
import com.alitalia.aem.common.data.home.MACustomerData;
import com.alitalia.aem.common.data.home.MACustomerProfile;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.twelvemonkeys.imageio.metadata.exif.EXIF;

public class ProfileComplete {

    private String URL;
    private String text;
    private int completed;
    private int errorCode;
    private String errorMessage;
    private String stackTrace;

    public ProfileComplete() {}

    public ProfileComplete(String URL, String text, int completed, int errorCode, String errorMessage, String stackTrace) {
        this.URL = URL;
        this.text = text;
        this.completed = completed;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.stackTrace = stackTrace;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getCompleted() {
        return completed;
    }

    public void setCompleted(int completed) {
        this.completed = completed;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getStackTrace() {
        return stackTrace;
    }

    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }

    public int checkDatiPersonaliMA(MACustomer maCustomer) {
        try {
            MACustomerProfile profile = maCustomer.getProfile();
            MACustomerData data = maCustomer.getData();
            int total = 0;

            if(profile.getFirstName()!=null && profile.getLastName()!=null){
                total += 4;
            }
            if( (profile.getGender()!=null && profile.getGender()!="u") && profile.getBirthYear()!=null && profile.getBirthMonth()!=null && profile.getBirthDay()!=null && profile.getProfessionalHeadline()!=null){
                total += 4;
            }
            if(data.getPhone1Type()!=null && data.getPhone1Number()!=null && data.getPhone1CountryCode()!=null){
                total += 4;
            }
            if(data.getAddress1_Address()!=null && data.getAddress1_Zip()!=null){
                total += 4;
            }
            if(data.getAddress1_City()!=null && data.getAddress1_CountryCode()!=null && data.getAddress1_StateCode()!=null){
                total += 4;
            }

            return total;
            //return profile.getFirstName()!=null && profile.getLastName()!=null && profile.getGender()!=null && profile.getBirthYear()!=null && profile.getBirthMonth()!=null && profile.getBirthDay()!=null;
            //return data.getAddress1_Type()!=null && data.getAddress1_Zip()!=null && data.getAddress1_City()!=null && data.getAddress1_CountryCode()!=null &&
                   // data.getAddress1_StateCode()!=null && data.getAddress1_Address()!=null && data.getPhone1Type()!=null && data.getPhone1Number()!=null && data.getPhone1CountryCode()!=null && data.getPhone1AreaCode()!=null && profile.getProfessionalHeadline()!=null;

        } catch (Exception e) {
            return 0;
        }
    }

//    public boolean checkDatiPersonaliCompleteMA(MACustomer maCustomer) {
//        try {
//            MACustomerProfile profile = maCustomer.getProfile();
//            MACustomerData data = maCustomer.getData();
//
//            return profile.getFirstName()!=null && profile.getLastName()!=null && profile.getGender()!=null && profile.getBirthYear()!=null && profile.getBirthMonth()!=null && profile.getBirthDay()!=null
//                    && data.getAddress1_Type()!=null && data.getAddress1_Zip()!=null && data.getAddress1_City()!=null && data.getAddress1_CountryCode()!=null && data.getAddress1_StateCode()!=null
//                    && data.getAddress1_Address()!=null && data.getPhone1Type()!=null && data.getPhone1Number()!=null && data.getPhone1CountryCode()!=null && profile.getProfessionalHeadline()!=null;
//
//        } catch (Exception e) {
//            return false;
//        }
//    }

    public int checkDatiViaggioMA(MACustomer maCustomer) {
        try {
            MACustomerData data = maCustomer.getData();
            int numDocs = 0;
            if(data.getIdentityCard_Number() != null && data.getIdentityCard_ValidFrom() != null && data.getIdentityCard_ExpireOn() != null && data.getIdentityCard_CountryCode() != null)
                numDocs++;
            if(data.getPassport_Number() != null && data.getPassport_ValidFrom() != null && data.getPassport_ExpireOn() != null && data.getPassport_CountryCode() != null)
                numDocs++;
            if(data.getGreen_Number() != null && data.getGreen_CountryCode() != null && data.getGreen_NationalityCode() != null && data.getGreen_ExpireOn() != null && data.getGreen_BornDate() != null)
                numDocs++;
            if(data.getVisa_Number() != null && data.getVisa_ValidFrom() != null && data.getVisa_CountryCode() != null)
                numDocs++;

            if(numDocs==1){
                return 10;
            }else if(numDocs>=2){
                return 20;
            }else{
                return 0;
            }
        } catch (Exception e) {
            return 0;
        }
    }

    public int checkPreferenzeViaggioMA(MACustomer maCustomer) {
        try {
            MACustomerData data = maCustomer.getData();
            int total = 0;
            if(data.getFlightPref_Seat() != null) {
                total+=5;
            }
            if(data.getFlightPref_Meal() != null) {
                total+=5;
            }
            if(data.getFlightPref_FF() != null && data.getFlightPref_FF().length>0) {
                total+=5;
            }
            if(data.getFlightPref_Airport() != null) {
                total+=10;
            }
            if(data.getFlightPref_Family()!= null) {
                total+=5;
            }
            return total;
        } catch (Exception e) {
            return 0;
        }
    }

    public int checkDatiPagamentoFatturazione(MACustomer maCustomer) {
        try {
            return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    public boolean checkMMConnection(MACustomer maCustomer) {
        try {
            /*return maCustomer.getData().getId_mm() != null;*/
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean checkDatiPersonaliMM(MMCustomerProfileData mmCustomer) {
        try {
            return mmCustomer.getDefaultAddressStreetFreeText()!=null && mmCustomer.getDefaultAddressPostalCode()!=null && mmCustomer.getDefaultAddressStateCode()!=null
                    && mmCustomer.getDefaultAddressCountry()!=null && mmCustomer.getDefaultAddressMunicipalityName()!=null;
        } catch (Exception e) {
            return false;
        }
    }

}
