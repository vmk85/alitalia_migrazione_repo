package com.alitalia.aem.common.data.home;

public class ResultBookingDetailsSolutionPricingTaxData {
	
	private String codeField;
	private ResultBookingDetailsSolutionPricingTaxPreviousSalePriceData previousSalePriceField;
	private ResultBookingDetailsSolutionPricingTaxSalePriceData salePriceField;
	
	public String getCodeField() {
		return codeField;
	}
	
	public void setCodeField(String codeField) {
		this.codeField = codeField;
	}

	public ResultBookingDetailsSolutionPricingTaxPreviousSalePriceData getPreviousSalePriceField() {
		return previousSalePriceField;
	}
	
	public void setPreviousSalePriceField(
			ResultBookingDetailsSolutionPricingTaxPreviousSalePriceData previousSalePriceField) {
		this.previousSalePriceField = previousSalePriceField;
	}
	
	public ResultBookingDetailsSolutionPricingTaxSalePriceData getSalePriceField() {
		return salePriceField;
	}
	
	public void setSalePriceField(
			ResultBookingDetailsSolutionPricingTaxSalePriceData salePriceField) {
		this.salePriceField = salePriceField;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((codeField == null) ? 0 : codeField.hashCode());
		result = prime
				* result
				+ ((previousSalePriceField == null) ? 0
						: previousSalePriceField.hashCode());
		result = prime * result
				+ ((salePriceField == null) ? 0 : salePriceField.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingDetailsSolutionPricingTaxData other = (ResultBookingDetailsSolutionPricingTaxData) obj;
		if (codeField == null) {
			if (other.codeField != null)
				return false;
		} else if (!codeField.equals(other.codeField))
			return false;
		if (previousSalePriceField == null) {
			if (other.previousSalePriceField != null)
				return false;
		} else if (!previousSalePriceField.equals(other.previousSalePriceField))
			return false;
		if (salePriceField == null) {
			if (other.salePriceField != null)
				return false;
		} else if (!salePriceField.equals(other.salePriceField))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingDetailsSolutionPricingTaxData [codeField="
				+ codeField + ", previousSalePriceField="
				+ previousSalePriceField + ", salePriceField=" + salePriceField
				+ "]";
	}
	
}
