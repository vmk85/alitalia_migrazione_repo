package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.BaseRequest;

public class UpdateNicknameRequest extends BaseRequest {

	private String newNickname;
	private MMCustomerProfileData customerProfile;

	public String getNewNickname() {
		return newNickname;
	}

	public void setNewNickname(String newNickname) {
		this.newNickname = newNickname;
	}

	public MMCustomerProfileData getCustomerProfile() {
		return customerProfile;
	}

	public void setCustomerProfile(MMCustomerProfileData customerProfile) {
		this.customerProfile = customerProfile;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((customerProfile == null) ? 0 : customerProfile.hashCode());
		result = prime * result + ((newNickname == null) ? 0 : newNickname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		UpdateNicknameRequest other = (UpdateNicknameRequest) obj;
		if (customerProfile == null) {
			if (other.customerProfile != null)
				return false;
		} else if (!customerProfile.equals(other.customerProfile))
			return false;
		if (newNickname == null) {
			if (other.newNickname != null)
				return false;
		} else if (!newNickname.equals(other.newNickname))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UpdateNicknameRequest [newNickname=" + newNickname + ", customerProfile="
				+ customerProfile + "]";
	}
}