
package com.alitalia.aem.common.data.checkinrest.model.boardingpass;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BoardingPassNew {

    @SerializedName("models")
    @Expose
    private List<Model> models = null;
    @SerializedName("conversationID")
    @Expose
    private String conversationID;

    public List<Model> getModels() {
        return models;
    }

    public void setModels(List<Model> models) {
        this.models = models;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

}
