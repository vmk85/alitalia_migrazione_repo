package com.alitalia.aem.common.data.home;

public class ResultTicketingDetailSolutionSliceExtData {

    private ResultTicketingDetailSolutionSliceExtWarningData warningField;

    public ResultTicketingDetailSolutionSliceExtWarningData getWarningField() {
        return warningField;
    }

    public void setWarningField(ResultTicketingDetailSolutionSliceExtWarningData value) {
        this.warningField = value;
    }

}
