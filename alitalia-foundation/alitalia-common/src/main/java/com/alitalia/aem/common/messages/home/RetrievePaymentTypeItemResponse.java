package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.PaymentTypeItemData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetrievePaymentTypeItemResponse extends BaseResponse{
	
	private List<PaymentTypeItemData> paymentTypeItemsData;

	public List<PaymentTypeItemData> getPaymentTypeItemsData() {
		return paymentTypeItemsData;
	}

	public void setPaymentTypeItemsData(
			List<PaymentTypeItemData> paymentTypeItemsData) {
		this.paymentTypeItemsData = paymentTypeItemsData;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((paymentTypeItemsData == null) ? 0 : paymentTypeItemsData
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrievePaymentTypeItemResponse other = (RetrievePaymentTypeItemResponse) obj;
		if (paymentTypeItemsData == null) {
			if (other.paymentTypeItemsData != null)
				return false;
		} else if (!paymentTypeItemsData.equals(other.paymentTypeItemsData))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrievePaymentTypeItemResponse [paymentTypeItemsData="
				+ paymentTypeItemsData + ", getTid()=" + getTid()
				+ ", getSid()=" + getSid() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + "]";
	}

	

}