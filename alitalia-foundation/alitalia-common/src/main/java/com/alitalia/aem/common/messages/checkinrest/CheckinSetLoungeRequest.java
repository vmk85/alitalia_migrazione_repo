package com.alitalia.aem.common.messages.checkinrest;

import java.util.List;

import com.alitalia.aem.common.data.checkinrest.model.lounge.request.FlightLounge;
import com.alitalia.aem.common.messages.BaseRequest;

public class CheckinSetLoungeRequest extends BaseRequest{
	private List<FlightLounge> flightLounge = null;
	private String pnr;
	private String language;
	private String market;
	private String conversationID;

	public CheckinSetLoungeRequest(String tid, String sid) {
		super(tid, sid);
	}

	public List<FlightLounge> getFlightLounge() {
		return flightLounge;
	}

	public void setFlightLounge(List<FlightLounge> flightLounge) {
		this.flightLounge = flightLounge;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public String getConversationID() {
		return conversationID;
	}

	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}
	
	
}
