
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PriceDetails_ {

    @SerializedName("totalPrice")
    @Expose
    private TotalPrice_ totalPrice;
    @SerializedName("basePrice")
    @Expose
    private BasePrice_ basePrice;
    @SerializedName("equivalentPrice")
    @Expose
    private EquivalentPrice_ equivalentPrice;
    @SerializedName("taxDetails")
    @Expose
    private TaxDetails_ taxDetails;

    public TotalPrice_ getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(TotalPrice_ totalPrice) {
        this.totalPrice = totalPrice;
    }

    public BasePrice_ getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BasePrice_ basePrice) {
        this.basePrice = basePrice;
    }

    public EquivalentPrice_ getEquivalentPrice() {
        return equivalentPrice;
    }

    public void setEquivalentPrice(EquivalentPrice_ equivalentPrice) {
        this.equivalentPrice = equivalentPrice;
    }

    public TaxDetails_ getTaxDetails() {
        return taxDetails;
    }

    public void setTaxDetails(TaxDetails_ taxDetails) {
        this.taxDetails = taxDetails;
    }

}
