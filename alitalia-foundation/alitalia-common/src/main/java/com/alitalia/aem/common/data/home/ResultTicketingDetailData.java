package com.alitalia.aem.common.data.home;

import java.util.List;

public class ResultTicketingDetailData {

    private ResultTicketingDetailPagesData pagesField;
    private List<ResultTicketingDetailSolutionData> solutionField;

    public ResultTicketingDetailPagesData getPagesField() {
        return pagesField;
    }

    public void setPagesField(ResultTicketingDetailPagesData value) {
        this.pagesField = value;
    }

    public List<ResultTicketingDetailSolutionData> getSolutionField() {
        return solutionField;
    }

    public void setSolutionField(List<ResultTicketingDetailSolutionData> value) {
        this.solutionField = value;
    }

}
