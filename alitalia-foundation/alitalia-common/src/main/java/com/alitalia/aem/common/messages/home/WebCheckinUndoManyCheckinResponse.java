package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.checkin.UndoCheckinPassengerData;
import com.alitalia.aem.common.messages.BaseResponse;

public class WebCheckinUndoManyCheckinResponse extends BaseResponse {

	private List<UndoCheckinPassengerData> passengers;

	public List<UndoCheckinPassengerData> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<UndoCheckinPassengerData> passengers) {
		this.passengers = passengers;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((passengers == null) ? 0 : passengers.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebCheckinUndoManyCheckinResponse other = (WebCheckinUndoManyCheckinResponse) obj;
		if (passengers == null) {
			if (other.passengers != null)
				return false;
		} else if (!passengers.equals(other.passengers))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WebCheckinUndoManyCheckinResponse [passengers=" + passengers
				+ "]";
	}
}