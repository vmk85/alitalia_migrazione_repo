package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.ECouponData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.messages.BaseResponse;

public class LoadWidgetCashMilesResponse extends BaseResponse {

	private CashAndMilesData cashMiles;

	public LoadWidgetCashMilesResponse() {
		super();
	}
	
	public LoadWidgetCashMilesResponse(String tid, String sid) {
		super(tid, sid);
	}

	public CashAndMilesData getCashMiles() {
		return cashMiles;
	}

	public void setCashMiles(CashAndMilesData cashMiles) {
		this.cashMiles = cashMiles;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((cashMiles == null) ? 0 : cashMiles.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoadWidgetCashMilesResponse other = (LoadWidgetCashMilesResponse) obj;
		if (cashMiles == null) {
			if (other.cashMiles != null)
				return false;
		} else if (!cashMiles.equals(other.cashMiles))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "LoadWidgetCashMilesResponse [cashMiles=" + cashMiles
				+ ", getTid()=" + getTid() + ", getSid()=" + getSid() + "]";
	}

}