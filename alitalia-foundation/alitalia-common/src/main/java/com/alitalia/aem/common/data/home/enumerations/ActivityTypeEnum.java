package com.alitalia.aem.common.data.home.enumerations;

public enum ActivityTypeEnum {

    NOT_SPECIFIED("NotSpecified"),
    CAR_RENTAL("CarRental"),
    HOTEL("Hotel"),
    PROMOTIONAL("Promotional"),
    FINANCIAL("Financial"),
    FLIGHT("Flight"),
    CATALOGUE("Catalogue"),
    CDS("Cds"),
    CONVERSION("Conversion");
   
    private final String value;

    ActivityTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ActivityTypeEnum fromValue(String v) {
        for (ActivityTypeEnum c: ActivityTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}