package com.alitalia.aem.common.data.home.enumerations;

public enum BancoPostaTypeEnum {
	
	BPIOL("BPIOL"),
	BPOL("BPOL"),
	CREDIT_CARD("CreditCard"),
	POSTE_PAY_CARD("PostePayCard"),
	POSTE_PAY_IMPRESA("PostePayImpresa");
	
	private final String value;
	
	BancoPostaTypeEnum(String v) {
		value = v;
	}
	
	public String value() {
		return value;
	}
	
	public static BancoPostaTypeEnum fromValue(String v) {
		for (BancoPostaTypeEnum c : BancoPostaTypeEnum.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
	
}
