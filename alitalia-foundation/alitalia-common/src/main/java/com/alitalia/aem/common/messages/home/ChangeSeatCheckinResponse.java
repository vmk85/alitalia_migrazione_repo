package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.messages.BaseResponse;

public class ChangeSeatCheckinResponse extends BaseResponse {

	private List<String> messages;
	private String newSeat;
	
	public List<String> getMessages() {
		return messages;
	}
	public void setMessages(List<String> messages) {
		this.messages = messages;
	}
	public String getNewSeat() {
		return newSeat;
	}
	public void setNewSeat(String newSeat) {
		this.newSeat = newSeat;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((messages == null) ? 0 : messages.hashCode());
		result = prime * result + ((newSeat == null) ? 0 : newSeat.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChangeSeatCheckinResponse other = (ChangeSeatCheckinResponse) obj;
		if (messages == null) {
			if (other.messages != null)
				return false;
		} else if (!messages.equals(other.messages))
			return false;
		if (newSeat == null) {
			if (other.newSeat != null)
				return false;
		} else if (!newSeat.equals(other.newSeat))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ChangeSeatCheckinResponse [messages=" + messages + ", newSeat="
				+ newSeat + ", toString()=" + super.toString() + "]";
	}

}
