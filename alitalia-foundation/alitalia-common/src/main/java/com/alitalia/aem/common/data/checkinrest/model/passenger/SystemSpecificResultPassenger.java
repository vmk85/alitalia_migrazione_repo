package com.alitalia.aem.common.data.checkinrest.model.passenger;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SystemSpecificResultPassenger {
	
	@SerializedName("errorMessage")
	@Expose
	private ErrorMessagePassenger errorMessage;
	
	@SerializedName("shortText")
	@Expose
	private String shortText;
	
	@SerializedName("element")
	@Expose
	private String element;
	
	@SerializedName("recordID")
	@Expose
	private String recordID;
	
	@SerializedName("docURL")
	@Expose
	private String docURL;
	
	@SerializedName("diagnosticResults")
	@Expose
	private DiagnosticResultsPassenger diagnosticResults;
	
	@SerializedName("serviceName")
	@Expose
	private String serviceName;

}
