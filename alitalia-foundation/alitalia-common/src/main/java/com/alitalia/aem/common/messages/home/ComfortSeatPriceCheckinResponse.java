package com.alitalia.aem.common.messages.home;

import java.math.BigDecimal;

import com.alitalia.aem.common.data.home.enumerations.LegTypeEnum;
import com.alitalia.aem.common.messages.BaseResponse;

public class ComfortSeatPriceCheckinResponse extends BaseResponse {

	private Boolean isComfortSeatFlight;
	private LegTypeEnum legType;
	private BigDecimal price;
	
	public Boolean getIsComfortSeatFlight() {
		return isComfortSeatFlight;
	}
	public void setIsComfortSeatFlight(Boolean isComfortSeatFlight) {
		this.isComfortSeatFlight = isComfortSeatFlight;
	}
	public LegTypeEnum getLegType() {
		return legType;
	}
	public void setLegType(LegTypeEnum legType) {
		this.legType = legType;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((isComfortSeatFlight == null) ? 0 : isComfortSeatFlight
						.hashCode());
		result = prime * result + ((legType == null) ? 0 : legType.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComfortSeatPriceCheckinResponse other = (ComfortSeatPriceCheckinResponse) obj;
		if (isComfortSeatFlight == null) {
			if (other.isComfortSeatFlight != null)
				return false;
		} else if (!isComfortSeatFlight.equals(other.isComfortSeatFlight))
			return false;
		if (legType != other.legType)
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ComfortSeatPriceCheckinResponse [isComfortSeatFlight="
				+ isComfortSeatFlight + ", legType=" + legType + ", price="
				+ price + ", toString()=" + super.toString() + "]";
	}

}
