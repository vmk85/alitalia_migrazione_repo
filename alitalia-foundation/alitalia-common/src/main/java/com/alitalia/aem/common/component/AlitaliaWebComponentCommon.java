package com.alitalia.aem.common.component;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Generic configuration service used to hold and expose
 * a set of misc properties (configurable through OSGI).
 * 
 * <p>This component can be used, for example, to define system-wide 
 * properties whose value must be managed through OSGI nodes, or
 * other configurations not tied to specific components.</p>
 * 
 */
@Component(metatype = true, immediate = true)
@Service(value=AlitaliaWebComponentCommon.class)
@Properties({
	@Property(name = AlitaliaWebComponentCommon.BUS_NUMBER_RANGE, description = "It is an array of fligth numbers to be mapped as a bus"),
	@Property(name = AlitaliaWebComponentCommon.BUS_STATION_CODE, description = "It is an array of ariportCode to map as bus station"),
	@Property(name = AlitaliaWebComponentCommon.BOOKING_BUS_ENABLED, description = "It is used to enable the BUS on booking Flow"),
	@Property(name = AlitaliaWebComponentCommon.CHECKIN_BUS_ENABLED, description = "It is used to enable the BUS on checkin Flow"),
	@Property(name = AlitaliaWebComponentCommon.MMB_BUS_ENABLED, description = "It is used to enable the BUS on mmb Flow")
})
public class AlitaliaWebComponentCommon {
	
	private static Logger logger = LoggerFactory.getLogger(AlitaliaWebComponentCommon.class);
	
	public static final String BUS_NUMBER_RANGE = "busNumber.range.array";
	public static final String BUS_STATION_CODE = "busStation.code.array";
	public static final String BOOKING_BUS_ENABLED = "booking.busEnabled";
	public static final String CHECKIN_BUS_ENABLED = "checkin.busEnabled";
	public static final String MMB_BUS_ENABLED = "mmb.busEnabled";
	
	
	/**
	 * It return true is the fligthNumber is into the range provided by configuration.
	 * @param fligthNumber
	 * @return 
	 */
	public boolean isBusNumber(int fligthNumber) {
		
		String[] busNumberRange = getBusNumberRanges();
		if (busNumberRange == null || busNumberRange.length == 0) {
			logger.error("busNumberRange configuration is null or empty. See AlitaliaWebComponentConfiguration");
			return false;
		}
		
		for (int i = 0; i < busNumberRange.length; i++) {
			String range = busNumberRange[i];
			String[] boundaries = range.split("-");
			int lowerBound = Integer.parseInt(boundaries[0]);
			int upperBound = Integer.parseInt(boundaries[1]);
			if ((fligthNumber >= lowerBound) && (fligthNumber <= upperBound)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * It return true is the airportCode is into the array provided by configuration.
	 * @param fligthNumber
	 * @return 
	 */
	public boolean isBusStation(String airportCode) {
		
		String[] busStationArray = getBusStationCode();
		if (busStationArray == null || busStationArray.length == 0) {
			logger.error("busStationArray configuration is null or empty. See AlitaliaWebComponentConfiguration");
			return false;
		}
		
		for (int i = 0; i < busStationArray.length; i++) {
			String busStation = busStationArray[i];
			if (airportCode.equalsIgnoreCase(busStation)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * It return an array of bus number by configuration.
	 * Each range is a string and it has the form: "lowerBound-upperBound"
	 * @return string array or null
	 */
	public String[] getBusNumberRanges() {
		return PropertiesUtil.toStringArray(
				getConfigurationProperty(BUS_NUMBER_RANGE), new String[0]);
	}
	
	/**
	 * It return an array of AirportCode by configuration.
	 * Each airportCode is a bus station
	 * @return string array or null
	 */
	public String[] getBusStationCode() {
		return PropertiesUtil.toStringArray(
				getConfigurationProperty(BUS_STATION_CODE), new String[0]);
	}
	
	/**
	 * It return true to enable the bus on the bookingFlow
	 * @return
	 */
	public boolean getBookingBusEnabled() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(BOOKING_BUS_ENABLED), false);
	}
	
	/**
	 * It return true to enable the bus on the checkinFlow
	 * @return
	 */
	public boolean getCheckinBusEnabled() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(CHECKIN_BUS_ENABLED), false);
	}
	
	/**
	 * It return true to enable the bus on the bookingFlow
	 * @return
	 */
	public boolean getMmbBusEnabled() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(MMB_BUS_ENABLED), false);
	}
	
	/* internals */
	
	private ComponentContext ctx = null;
	
	protected Object getConfigurationProperty(String propertyName) {
		if (ctx != null) {
			return ctx.getProperties().get(propertyName);
		}
		return null;
	}
	
	@Activate
	protected void activate(final ComponentContext ctx) {
		this.ctx = ctx;
	}
}
