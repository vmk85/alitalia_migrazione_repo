package com.alitalia.aem.common.data.home;

import java.util.ArrayList;
import java.util.List;

public class MACustomerDataPayment {

	private List<String> token;

	public List<String> getToken() {
		return token;
	}

	public void setToken(List<String> token) {
		this.token = token;
	}
	
	private ArrayList<MACreditCardData> creditCardsData;

	public ArrayList<MACreditCardData> getCreditCardsData() {
		return creditCardsData;
	}

	public void setCreditCardsData(ArrayList<MACreditCardData> creditCardsData) {
		this.creditCardsData = creditCardsData;
	}
	
}
