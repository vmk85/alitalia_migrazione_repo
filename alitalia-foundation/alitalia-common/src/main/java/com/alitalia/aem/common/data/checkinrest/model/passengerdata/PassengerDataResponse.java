
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PassengerDataResponse {

    @SerializedName("lineNumber")
    @Expose
    private Long lineNumber;
    @SerializedName("lineNumberSpecified")
    @Expose
    private Boolean lineNumberSpecified;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("passengerID")
    @Expose
    private String passengerID;
    @SerializedName("pnrLocator")
    @Expose
    private PnrLocator pnrLocator;
    @SerializedName("mcppnrLocator")
    @Expose
    private String mcppnrLocator;
    @SerializedName("groupCode")
    @Expose
    private String groupCode;
    @SerializedName("vcrNumber")
    @Expose
    private VcrNumber vcrNumber;
    @SerializedName("atpcoPassengerType")
    @Expose
    private String atpcoPassengerType;
    @SerializedName("bagPoolID")
    @Expose
    private Long bagPoolID;
    @SerializedName("bagPoolIDSpecified")
    @Expose
    private Boolean bagPoolIDSpecified;
    @SerializedName("boardingPassFlag")
    @Expose
    private String boardingPassFlag;
    @SerializedName("baggageRouteList")
    @Expose
    private List<BaggageRouteList> baggageRouteList = null;
    @SerializedName("passengerItineraryList")
    @Expose
    private List<PassengerItineraryList> passengerItineraryList = null;
    @SerializedName("requiredInfoSumList")
    @Expose
    private List<RequiredInfoSumList> requiredInfoSumList = null;
    @SerializedName("passengerEditList")
    @Expose
    private List<PassengerEditList> passengerEditList = null;
    @SerializedName("freeTextInfoList")
    @Expose
    private List<FreeTextInfoList_> freeTextInfoList = null;
    @SerializedName("timaticInfoList")
    @Expose
    private List<TimaticInfoList> timaticInfoList = null;

    public Long getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(Long lineNumber) {
        this.lineNumber = lineNumber;
    }

    public Boolean getLineNumberSpecified() {
        return lineNumberSpecified;
    }

    public void setLineNumberSpecified(Boolean lineNumberSpecified) {
        this.lineNumberSpecified = lineNumberSpecified;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPassengerID() {
        return passengerID;
    }

    public void setPassengerID(String passengerID) {
        this.passengerID = passengerID;
    }

    public PnrLocator getPnrLocator() {
        return pnrLocator;
    }

    public void setPnrLocator(PnrLocator pnrLocator) {
        this.pnrLocator = pnrLocator;
    }

    public String getMcppnrLocator() {
        return mcppnrLocator;
    }

    public void setMcppnrLocator(String mcppnrLocator) {
        this.mcppnrLocator = mcppnrLocator;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public VcrNumber getVcrNumber() {
        return vcrNumber;
    }

    public void setVcrNumber(VcrNumber vcrNumber) {
        this.vcrNumber = vcrNumber;
    }

    public String getAtpcoPassengerType() {
        return atpcoPassengerType;
    }

    public void setAtpcoPassengerType(String atpcoPassengerType) {
        this.atpcoPassengerType = atpcoPassengerType;
    }

    public Long getBagPoolID() {
        return bagPoolID;
    }

    public void setBagPoolID(Long bagPoolID) {
        this.bagPoolID = bagPoolID;
    }

    public Boolean getBagPoolIDSpecified() {
        return bagPoolIDSpecified;
    }

    public void setBagPoolIDSpecified(Boolean bagPoolIDSpecified) {
        this.bagPoolIDSpecified = bagPoolIDSpecified;
    }

    public String getBoardingPassFlag() {
        return boardingPassFlag;
    }

    public void setBoardingPassFlag(String boardingPassFlag) {
        this.boardingPassFlag = boardingPassFlag;
    }

    public List<BaggageRouteList> getBaggageRouteList() {
        return baggageRouteList;
    }

    public void setBaggageRouteList(List<BaggageRouteList> baggageRouteList) {
        this.baggageRouteList = baggageRouteList;
    }

    public List<PassengerItineraryList> getPassengerItineraryList() {
        return passengerItineraryList;
    }

    public void setPassengerItineraryList(List<PassengerItineraryList> passengerItineraryList) {
        this.passengerItineraryList = passengerItineraryList;
    }

    public List<RequiredInfoSumList> getRequiredInfoSumList() {
        return requiredInfoSumList;
    }

    public void setRequiredInfoSumList(List<RequiredInfoSumList> requiredInfoSumList) {
        this.requiredInfoSumList = requiredInfoSumList;
    }

    public List<PassengerEditList> getPassengerEditList() {
        return passengerEditList;
    }

    public void setPassengerEditList(List<PassengerEditList> passengerEditList) {
        this.passengerEditList = passengerEditList;
    }

    public List<FreeTextInfoList_> getFreeTextInfoList() {
        return freeTextInfoList;
    }

    public void setFreeTextInfoList(List<FreeTextInfoList_> freeTextInfoList) {
        this.freeTextInfoList = freeTextInfoList;
    }

    public List<TimaticInfoList> getTimaticInfoList() {
        return timaticInfoList;
    }

    public void setTimaticInfoList(List<TimaticInfoList> timaticInfoList) {
        this.timaticInfoList = timaticInfoList;
    }

}
