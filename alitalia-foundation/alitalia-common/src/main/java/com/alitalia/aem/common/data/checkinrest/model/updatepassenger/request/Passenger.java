
package com.alitalia.aem.common.data.checkinrest.model.updatepassenger.request;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Passenger {

    @SerializedName("passengerID")
    @Expose
    private String passengerID;
    @SerializedName("checkInLimited")
    @Expose
    private Boolean checkInLimited;
    @SerializedName("checkInComplete")
    @Expose
    private Boolean checkInComplete;
    @SerializedName("nome")
    @Expose
    private String nome;
    @SerializedName("cognome")
    @Expose
    private String cognome;
    @SerializedName("bookingClass")
    @Expose
    private String bookingClass;
    @SerializedName("postoAssegnato")
    @Expose
    private Boolean postoAssegnato;
    @SerializedName("numeroPostoAssegnato")
    @Expose
    private String numeroPostoAssegnato;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("frequentFlyer")
    @Expose
    private List<FrequentFlyer> frequentFlyer = null;
    @SerializedName("airline")
    @Expose
    private String airline;
    @SerializedName("nationality")
    @Expose
    private String nationality;
    @SerializedName("residence")
    @Expose
    private String residence;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("zipCode")
    @Expose
    private String zipCode;
    @SerializedName("documents")
    @Expose
    private List<Document> documents = null;
    @SerializedName("documentRequired")
    @Expose
    private List<DocumentRequired> documentRequired = null;
    @SerializedName("codeFFchanged")
    @Expose
    private String codeFFchanged;
    @SerializedName("numberFFchanged")
    @Expose
    private String numberFFchanged;
    @SerializedName("isChanged")
    @Expose
    private Boolean isChanged;
    @SerializedName("ticketStatus")
    @Expose
    private String ticketStatus;
    @SerializedName("ticket")
    @Expose
    private String ticket;
    @SerializedName("passengerType")
    @Expose
    private String passengerType;
    @SerializedName("adultDocumentsSRequired")
    @Expose
    private Boolean adultDocumentsSRequired;
    @SerializedName("adultDocumentsS")
    @Expose
    private List<AdultDocuments> adultDocumentsS = null;
    @SerializedName("infantDocumentsSRequired")
    @Expose
    private Boolean infantDocumentsSRequired;
    @SerializedName("infantDocumentsS")
    @Expose
    private List<InfantDocuments> infantDocumentsS = null;
    @SerializedName("adultDocumentOtherRequired")
    @Expose
    private Boolean adultDocumentOtherRequired;
    @SerializedName("adultDocumentOther")
    @Expose
    private AdultDocumentOther adultDocumentOther;
    @SerializedName("infantDocumentOtherRequired")
    @Expose
    private Boolean infantDocumentOtherRequired;
    @SerializedName("infantDocumentOther")
    @Expose
    private InfantDocumentOther infantDocumentOther;
    @SerializedName("adultResidentDocumentRequired")
    @Expose
    private Boolean adultResidentDocumentRequired;
    @SerializedName("adultResidentDocument")
    @Expose
    private AdultResidentDocument adultResidentDocument;
    @SerializedName("infantResidentDocumentRequired")
    @Expose
    private Boolean infantResidentDocumentRequired;
    @SerializedName("infantResidentDocument")
    @Expose
    private InfantResidentDocument infantResidentDocument;
    @SerializedName("adultDestinationDocumentRequired")
    @Expose
    private Boolean adultDestinationDocumentRequired;
    @SerializedName("adultDestinationDocument")
    @Expose
    private AdultDestinationDocument adultDestinationDocument;
    @SerializedName("infantDestinationDocumentRequired")
    @Expose
    private Boolean infantDestinationDocumentRequired;
    @SerializedName("infantDestinationDocument")
    @Expose
    private InfantDestinationDocument infantDestinationDocument;
    @SerializedName("adultDocVRequired")
    @Expose
    private Boolean adultDocVRequired;
    @SerializedName("infantDocVRequired")
    @Expose
    private Boolean infantDocVRequired;

    public String getPassengerID() {
        return passengerID;
    }

    public void setPassengerID(String passengerID) {
        this.passengerID = passengerID;
    }

    public Boolean getCheckInLimited() {
        return checkInLimited;
    }

    public void setCheckInLimited(Boolean checkInLimited) {
        this.checkInLimited = checkInLimited;
    }

    public Boolean getCheckInComplete() {
        return checkInComplete;
    }

    public void setCheckInComplete(Boolean checkInComplete) {
        this.checkInComplete = checkInComplete;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getBookingClass() {
        return bookingClass;
    }

    public void setBookingClass(String bookingClass) {
        this.bookingClass = bookingClass;
    }

    public Boolean getPostoAssegnato() {
        return postoAssegnato;
    }

    public void setPostoAssegnato(Boolean postoAssegnato) {
        this.postoAssegnato = postoAssegnato;
    }

    public String getNumeroPostoAssegnato() {
        return numeroPostoAssegnato;
    }

    public void setNumeroPostoAssegnato(String numeroPostoAssegnato) {
        this.numeroPostoAssegnato = numeroPostoAssegnato;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<FrequentFlyer> getFrequentFlyer() {
        return frequentFlyer;
    }

    public void setFrequentFlyer(List<FrequentFlyer> frequentFlyer) {
        this.frequentFlyer = frequentFlyer;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getResidence() {
        return residence;
    }

    public void setResidence(String residence) {
        this.residence = residence;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    public List<DocumentRequired> getDocumentRequired() {
        return documentRequired;
    }

    public void setDocumentRequired(List<DocumentRequired> documentRequired) {
        this.documentRequired = documentRequired;
    }

    public String getCodeFFchanged() {
        return codeFFchanged;
    }

    public void setCodeFFchanged(String codeFFchanged) {
        this.codeFFchanged = codeFFchanged;
    }

    public String getNumberFFchanged() {
        return numberFFchanged;
    }

    public void setNumberFFchanged(String numberFFchanged) {
        this.numberFFchanged = numberFFchanged;
    }

    public Boolean getIsChanged() {
        return isChanged;
    }

    public void setIsChanged(Boolean isChanged) {
        this.isChanged = isChanged;
    }

    public String getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(String ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public String getPassengerType() {
        return passengerType;
    }

    public void setPassengerType(String passengerType) {
        this.passengerType = passengerType;
    }

    public Boolean getAdultDocumentsSRequired() {
        return adultDocumentsSRequired;
    }

    public void setAdultDocumentsSRequired(Boolean adultDocumentsSRequired) {
        this.adultDocumentsSRequired = adultDocumentsSRequired;
    }

    public List<AdultDocuments> getAdultDocumentsS() {
        return adultDocumentsS;
    }

    public void setAdultDocumentsS(List<AdultDocuments> adultDocumentsS) {
        this.adultDocumentsS = adultDocumentsS;
    }

    public Boolean getInfantDocumentsSRequired() {
        return infantDocumentsSRequired;
    }

    public void setInfantDocumentsSRequired(Boolean infantDocumentsSRequired) {
        this.infantDocumentsSRequired = infantDocumentsSRequired;
    }

    public List<InfantDocuments> getInfantDocumentsS() {
        return infantDocumentsS;
    }

    public void setInfantDocumentsS(List<InfantDocuments> infantDocumentsS) {
        this.infantDocumentsS = infantDocumentsS;
    }

    public Boolean getAdultDocumentOtherRequired() {
        return adultDocumentOtherRequired;
    }

    public void setAdultDocumentOtherRequired(Boolean adultDocumentOtherRequired) {
        this.adultDocumentOtherRequired = adultDocumentOtherRequired;
    }

    public AdultDocumentOther getAdultDocumentOther() {
        return adultDocumentOther;
    }

    public void setAdultDocumentOther(AdultDocumentOther adultDocumentOther) {
        this.adultDocumentOther = adultDocumentOther;
    }

    public Boolean getInfantDocumentOtherRequired() {
        return infantDocumentOtherRequired;
    }

    public void setInfantDocumentOtherRequired(Boolean infantDocumentOtherRequired) {
        this.infantDocumentOtherRequired = infantDocumentOtherRequired;
    }

    public InfantDocumentOther getInfantDocumentOther() {
        return infantDocumentOther;
    }

    public void setInfantDocumentOther(InfantDocumentOther infantDocumentOther) {
        this.infantDocumentOther = infantDocumentOther;
    }

    public Boolean getAdultResidentDocumentRequired() {
        return adultResidentDocumentRequired;
    }

    public void setAdultResidentDocumentRequired(Boolean adultResidentDocumentRequired) {
        this.adultResidentDocumentRequired = adultResidentDocumentRequired;
    }

    public AdultResidentDocument getAdultResidentDocument() {
        return adultResidentDocument;
    }

    public void setAdultResidentDocument(AdultResidentDocument adultResidentDocument) {
        this.adultResidentDocument = adultResidentDocument;
    }

    public Boolean getInfantResidentDocumentRequired() {
        return infantResidentDocumentRequired;
    }

    public void setInfantResidentDocumentRequired(Boolean infantResidentDocumentRequired) {
        this.infantResidentDocumentRequired = infantResidentDocumentRequired;
    }

    public InfantResidentDocument getInfantResidentDocument() {
        return infantResidentDocument;
    }

    public void setInfantResidentDocument(InfantResidentDocument infantResidentDocument) {
        this.infantResidentDocument = infantResidentDocument;
    }

    public Boolean getAdultDestinationDocumentRequired() {
        return adultDestinationDocumentRequired;
    }

    public void setAdultDestinationDocumentRequired(Boolean adultDestinationDocumentRequired) {
        this.adultDestinationDocumentRequired = adultDestinationDocumentRequired;
    }

    public AdultDestinationDocument getAdultDestinationDocument() {
        return adultDestinationDocument;
    }

    public void setAdultDestinationDocument(AdultDestinationDocument adultDestinationDocument) {
        this.adultDestinationDocument = adultDestinationDocument;
    }

    public Boolean getInfantDestinationDocumentRequired() {
        return infantDestinationDocumentRequired;
    }

    public void setInfantDestinationDocumentRequired(Boolean infantDestinationDocumentRequired) {
        this.infantDestinationDocumentRequired = infantDestinationDocumentRequired;
    }

    public InfantDestinationDocument getInfantDestinationDocument() {
        return infantDestinationDocument;
    }

    public void setInfantDestinationDocument(InfantDestinationDocument infantDestinationDocument) {
        this.infantDestinationDocument = infantDestinationDocument;
    }

    public Boolean getAdultDocVRequired() {
        return adultDocVRequired;
    }

    public void setAdultDocVRequired(Boolean adultDocVRequired) {
        this.adultDocVRequired = adultDocVRequired;
    }

    public Boolean getInfantDocVRequired() {
        return infantDocVRequired;
    }

    public void setInfantDocVRequired(Boolean infantDocVRequired) {
        this.infantDocVRequired = infantDocVRequired;
    }

}
