package com.alitalia.aem.common.data.home.enumerations;

public enum BestPriceTypeEnum {

	DAY("Day"),
    MONTH("Month");
    
	private final String value;

	BestPriceTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BestPriceTypeEnum fromValue(String v) {
        for (BestPriceTypeEnum c: BestPriceTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}