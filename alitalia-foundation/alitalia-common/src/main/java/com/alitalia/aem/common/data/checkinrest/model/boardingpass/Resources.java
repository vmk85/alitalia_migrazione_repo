
package com.alitalia.aem.common.data.checkinrest.model.boardingpass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Resources {

    @SerializedName("flightNumber")
    @Expose
    private String flightNumber;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("subTitleairport")
    @Expose
    private String subTitleairport;
    @SerializedName("subTitlePax")
    @Expose
    private String subTitlePax;
    @SerializedName("skyPriority")
    @Expose
    private String skyPriority;
    @SerializedName("printName")
    @Expose
    private String printName;
    @SerializedName("printFrequentFlyer")
    @Expose
    private String printFrequentFlyer;
    @SerializedName("printTicketNumber")
    @Expose
    private String printTicketNumber;
    @SerializedName("priorityCode")
    @Expose
    private String priorityCode;
    @SerializedName("seniorityDate")
    @Expose
    private String seniorityDate;
    @SerializedName("printSSRCode")
    @Expose
    private String printSSRCode;
    @SerializedName("specialFare")
    @Expose
    private String specialFare;
    @SerializedName("printSec")
    @Expose
    private String printSec;
    @SerializedName("printFlight")
    @Expose
    private String printFlight;
    @SerializedName("terminal")
    @Expose
    private String terminal;
    @SerializedName("printDate")
    @Expose
    private String printDate;
    @SerializedName("printFromCity")
    @Expose
    private String printFromCity;
    @SerializedName("printToCity")
    @Expose
    private String printToCity;
    @SerializedName("printDeparture")
    @Expose
    private String printDeparture;
    @SerializedName("printGate")
    @Expose
    private String printGate;
    @SerializedName("printBoarding")
    @Expose
    private String printBoarding;
    @SerializedName("printClass")
    @Expose
    private String printClass;
    @SerializedName("printSeat")
    @Expose
    private String printSeat;
    @SerializedName("serviceOperatingText")
    @Expose
    private String serviceOperatingText;
    @SerializedName("boardingZoneText")
    @Expose
    private String boardingZoneText;
    @SerializedName("printBoardingLuggage")
    @Expose
    private String printBoardingLuggage;
    @SerializedName("printMsgBoardingLuggage")
    @Expose
    private String printMsgBoardingLuggage;
    @SerializedName("printMoreInfoBoardingLuggage")
    @Expose
    private String printMoreInfoBoardingLuggage;
    @SerializedName("printMsgLastCall")
    @Expose
    private String printMsgLastCall;
    @SerializedName("printMoreInfoBoarding")
    @Expose
    private String printMoreInfoBoarding;
    @SerializedName("printMoreInfoLastCall")
    @Expose
    private String printMoreInfoLastCall;
    @SerializedName("cutText")
    @Expose
    private String cutText;
    @SerializedName("printImportantInfo")
    @Expose
    private String printImportantInfo;
    @SerializedName("printInfoBody")
    @Expose
    private String printInfoBody;

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitleairport() {
        return subTitleairport;
    }

    public void setSubTitleairport(String subTitleairport) {
        this.subTitleairport = subTitleairport;
    }

    public String getSubTitlePax() {
        return subTitlePax;
    }

    public void setSubTitlePax(String subTitlePax) {
        this.subTitlePax = subTitlePax;
    }

    public String getSkyPriority() {
        return skyPriority;
    }

    public void setSkyPriority(String skyPriority) {
        this.skyPriority = skyPriority;
    }

    public String getPrintName() {
        return printName;
    }

    public void setPrintName(String printName) {
        this.printName = printName;
    }

    public String getPrintFrequentFlyer() {
        return printFrequentFlyer;
    }

    public void setPrintFrequentFlyer(String printFrequentFlyer) {
        this.printFrequentFlyer = printFrequentFlyer;
    }

    public String getPrintTicketNumber() {
        return printTicketNumber;
    }

    public void setPrintTicketNumber(String printTicketNumber) {
        this.printTicketNumber = printTicketNumber;
    }

    public String getPriorityCode() {
        return priorityCode;
    }

    public void setPriorityCode(String priorityCode) {
        this.priorityCode = priorityCode;
    }

    public String getSeniorityDate() {
        return seniorityDate;
    }

    public void setSeniorityDate(String seniorityDate) {
        this.seniorityDate = seniorityDate;
    }

    public String getPrintSSRCode() {
        return printSSRCode;
    }

    public void setPrintSSRCode(String printSSRCode) {
        this.printSSRCode = printSSRCode;
    }

    public String getSpecialFare() {
        return specialFare;
    }

    public void setSpecialFare(String specialFare) {
        this.specialFare = specialFare;
    }

    public String getPrintSec() {
        return printSec;
    }

    public void setPrintSec(String printSec) {
        this.printSec = printSec;
    }

    public String getPrintFlight() {
        return printFlight;
    }

    public void setPrintFlight(String printFlight) {
        this.printFlight = printFlight;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getPrintDate() {
        return printDate;
    }

    public void setPrintDate(String printDate) {
        this.printDate = printDate;
    }

    public String getPrintFromCity() {
        return printFromCity;
    }

    public void setPrintFromCity(String printFromCity) {
        this.printFromCity = printFromCity;
    }

    public String getPrintToCity() {
        return printToCity;
    }

    public void setPrintToCity(String printToCity) {
        this.printToCity = printToCity;
    }

    public String getPrintDeparture() {
        return printDeparture;
    }

    public void setPrintDeparture(String printDeparture) {
        this.printDeparture = printDeparture;
    }

    public String getPrintGate() {
        return printGate;
    }

    public void setPrintGate(String printGate) {
        this.printGate = printGate;
    }

    public String getPrintBoarding() {
        return printBoarding;
    }

    public void setPrintBoarding(String printBoarding) {
        this.printBoarding = printBoarding;
    }

    public String getPrintClass() {
        return printClass;
    }

    public void setPrintClass(String printClass) {
        this.printClass = printClass;
    }

    public String getPrintSeat() {
        return printSeat;
    }

    public void setPrintSeat(String printSeat) {
        this.printSeat = printSeat;
    }

    public String getServiceOperatingText() {
        return serviceOperatingText;
    }

    public void setServiceOperatingText(String serviceOperatingText) {
        this.serviceOperatingText = serviceOperatingText;
    }

    public String getBoardingZoneText() {
        return boardingZoneText;
    }

    public void setBoardingZoneText(String boardingZoneText) {
        this.boardingZoneText = boardingZoneText;
    }

    public String getPrintBoardingLuggage() {
        return printBoardingLuggage;
    }

    public void setPrintBoardingLuggage(String printBoardingLuggage) {
        this.printBoardingLuggage = printBoardingLuggage;
    }

    public String getPrintMsgBoardingLuggage() {
        return printMsgBoardingLuggage;
    }

    public void setPrintMsgBoardingLuggage(String printMsgBoardingLuggage) {
        this.printMsgBoardingLuggage = printMsgBoardingLuggage;
    }

    public String getPrintMoreInfoBoardingLuggage() {
        return printMoreInfoBoardingLuggage;
    }

    public void setPrintMoreInfoBoardingLuggage(String printMoreInfoBoardingLuggage) {
        this.printMoreInfoBoardingLuggage = printMoreInfoBoardingLuggage;
    }

    public String getPrintMsgLastCall() {
        return printMsgLastCall;
    }

    public void setPrintMsgLastCall(String printMsgLastCall) {
        this.printMsgLastCall = printMsgLastCall;
    }

    public String getPrintMoreInfoBoarding() {
        return printMoreInfoBoarding;
    }

    public void setPrintMoreInfoBoarding(String printMoreInfoBoarding) {
        this.printMoreInfoBoarding = printMoreInfoBoarding;
    }

    public String getPrintMoreInfoLastCall() {
        return printMoreInfoLastCall;
    }

    public void setPrintMoreInfoLastCall(String printMoreInfoLastCall) {
        this.printMoreInfoLastCall = printMoreInfoLastCall;
    }

    public String getCutText() {
        return cutText;
    }

    public void setCutText(String cutText) {
        this.cutText = cutText;
    }

    public String getPrintImportantInfo() {
        return printImportantInfo;
    }

    public void setPrintImportantInfo(String printImportantInfo) {
        this.printImportantInfo = printImportantInfo;
    }

    public String getPrintInfoBody() {
        return printInfoBody;
    }

    public void setPrintInfoBody(String printInfoBody) {
        this.printInfoBody = printInfoBody;
    }

}
