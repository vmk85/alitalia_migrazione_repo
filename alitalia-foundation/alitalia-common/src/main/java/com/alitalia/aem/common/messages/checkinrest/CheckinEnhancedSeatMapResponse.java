package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.enhancedseatmap.response.EnhancedSeatMapResp;
import com.alitalia.aem.common.messages.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckinEnhancedSeatMapResponse extends BaseResponse{

	@SerializedName("seatMAp")
	@Expose
	private EnhancedSeatMapResp _enhancedseatmapResp;
	@SerializedName("conversationID")
	@Expose
	private String conversationID;

	public EnhancedSeatMapResp get_enhancedseatmapResp() {
		return _enhancedseatmapResp;
	}

	public void set_enhancedseatmapResp(EnhancedSeatMapResp _enhancedseatmapResp) {
		this._enhancedseatmapResp = _enhancedseatmapResp;
	}

	public CheckinEnhancedSeatMapResponse(String tid, String sid) {
		super(tid, sid);
	}

	public CheckinEnhancedSeatMapResponse() {}



}
