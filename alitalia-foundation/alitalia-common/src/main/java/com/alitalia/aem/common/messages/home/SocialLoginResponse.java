package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class SocialLoginResponse extends BaseResponse {
	
	private String userCode;
	private String pin;
	private Boolean loginSuccessful;
	private String errorCode;
	private String errorDescription;
	private String ssoToken;

	public SocialLoginResponse() {
		super();
		this.loginSuccessful = false;
	}

	public SocialLoginResponse(Boolean loginSuccessful, String userCode, String pin,
			String errorCode, String errorDescription, String ssoToken) {
		super();
		this.loginSuccessful = loginSuccessful;
		this.userCode = userCode;
		this.pin = pin;
		this.errorCode = errorCode;
		this.errorDescription = errorDescription;
		this.ssoToken = ssoToken;
	}

	public SocialLoginResponse(String tid, String sid,
			Boolean loginSuccessful, String userCode, String pin,
			String errorCode, String errorDescription, String ssoToken) {
		super(tid, sid);
		this.loginSuccessful = loginSuccessful;
		this.userCode = userCode;
		this.pin = pin;
		this.errorCode = errorCode;
		this.errorDescription = errorDescription;
		this.ssoToken = ssoToken;
	}
	
	public Boolean isLoginSuccessful() {
		return loginSuccessful;
	}
	
	public void setLoginSuccessful(Boolean loginSuccessful) {
		this.loginSuccessful = loginSuccessful;
	}
	
	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getErrorCode() {
		return errorCode;
	}
	
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	
	public String getErrorDescription() {
		return errorDescription;
	}
	
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public String getSsoToken() {
		return ssoToken;
	}

	public void setSsoToken(String ssoToken) {
		this.ssoToken = ssoToken;
	}

	public Boolean getLoginSuccessful() {
		return loginSuccessful;
	}

	@Override
	public String toString() {
		return "SocialLoginResponse [userCode=" + userCode + ", pin=" + pin + ", loginSuccessful=" + loginSuccessful
				+ ", errorCode=" + errorCode + ", errorDescription=" + errorDescription + ", ssoToken=" + ssoToken
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((errorCode == null) ? 0 : errorCode.hashCode());
		result = prime * result + ((errorDescription == null) ? 0 : errorDescription.hashCode());
		result = prime * result + ((loginSuccessful == null) ? 0 : loginSuccessful.hashCode());
		result = prime * result + ((pin == null) ? 0 : pin.hashCode());
		result = prime * result + ((ssoToken == null) ? 0 : ssoToken.hashCode());
		result = prime * result + ((userCode == null) ? 0 : userCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SocialLoginResponse other = (SocialLoginResponse) obj;
		if (errorCode == null) {
			if (other.errorCode != null)
				return false;
		} else if (!errorCode.equals(other.errorCode))
			return false;
		if (errorDescription == null) {
			if (other.errorDescription != null)
				return false;
		} else if (!errorDescription.equals(other.errorDescription))
			return false;
		if (loginSuccessful == null) {
			if (other.loginSuccessful != null)
				return false;
		} else if (!loginSuccessful.equals(other.loginSuccessful))
			return false;
		if (pin == null) {
			if (other.pin != null)
				return false;
		} else if (!pin.equals(other.pin))
			return false;
		if (ssoToken == null) {
			if (other.ssoToken != null)
				return false;
		} else if (!ssoToken.equals(other.ssoToken))
			return false;
		if (userCode == null) {
			if (other.userCode != null)
				return false;
		} else if (!userCode.equals(other.userCode))
			return false;
		return true;
	}

	
	
}
