package com.alitalia.aem.common.data.home;

import java.util.Calendar;
import java.util.List;

import com.alitalia.aem.common.data.home.enumerations.MMAddressTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMGenderTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMMaritalStatusTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMRegistrationTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMSystemChannelTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMTierCodeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMTitleTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMWorkPositionTypeEnum;

public class MMCustomerProfileData {

	public static final String NAME = "MMCustomerProfileData";
	private Calendar birthDate;
	private Calendar passportDate;
	private Calendar tierDate;

	private Boolean contractAgree;

	private String contratcType;
	private String customerName;
	private String customerNickName;
	private String customerNumber;
	private String customerPinCode;
	private String customerSurname;
	private String defaultAddressCountry;
	private String defaultAddressMunicipalityName;
	private String defaultAddressPostalCode;
	private String defaultAddressStateCode;
	private String defaultAddressStreetFreeText;
	private String email;
	private String fiscalCode;
	private String language;
	private String mailingType;
	private String market;
	private String professionalName;
	private String statusCode;
	private String userIdRequest;
	private String webPageRequest;

	private Long pointsEarnedPartial;
	private Long pointsEarnedTotal;
	private Long pointsEarnedTotalQualified;
	private Long pointsRemainingPartial;
	private Long pointsRemainingTotal;
	private Long pointsSpentPartial;
	private Long pointsSpentTotal;
	private Long qualifyingSegment;
	private String qualifiedFlights;
	private Calendar expirationDate;

	private Boolean smsAuthorization;
	private Boolean profiling;

	private MMTitleTypeEnum customerTitle;
	private MMWorkPositionTypeEnum customerWorkPosition;
	private MMAddressTypeEnum defaultAddressType;
	private MMGenderTypeEnum gender;
	private MMMaritalStatusTypeEnum maritalStatus;
	private MMRegistrationTypeEnum registrationRequired;
	private MMSystemChannelTypeEnum systemChannel;
	private MMTierCodeEnum tierCode;
	private MMCreditCardData storedCreditCard;

	private List<MMTelephoneData> telephones;
	private List<MMActivityData> activities;
	private List<MMAddressData> addresses;
	private List<MMContractData> contracts;
	private List<MMCreditCardData> creditCards;
	private List<MMFlightData> flight;
	private List<MMLifestyleData> lifestyles;
	private List<MMMessageData> messages;
	private List<MMPreferenceData> preferences;
	private List<MMProgramData> programs;


	public Calendar getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Calendar birthDate) {
		this.birthDate = birthDate;
	}

	public Calendar getPassportDate() {
		return passportDate;
	}

	public void setPassportDate(Calendar passportDate) {
		this.passportDate = passportDate;
	}

	public Calendar getTierDate() {
		return tierDate;
	}

	public void setTierDate(Calendar tierDate) {
		this.tierDate = tierDate;
	}

	public Boolean getContractAgree() {
		return contractAgree;
	}

	public void setContractAgree(Boolean contractAgree) {
		this.contractAgree = contractAgree;
	}

	public String getContratcType() {
		return contratcType;
	}

	public void setContratcType(String contratcType) {
		this.contratcType = contratcType;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerNickName() {
		return customerNickName;
	}

	public void setCustomerNickName(String customerNickName) {
		this.customerNickName = customerNickName;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getCustomerPinCode() {
		return customerPinCode;
	}

	public void setCustomerPinCode(String customerPinCode) {
		this.customerPinCode = customerPinCode;
	}

	public String getCustomerSurname() {
		return customerSurname;
	}

	public void setCustomerSurname(String customerSurname) {
		this.customerSurname = customerSurname;
	}

	public String getDefaultAddressCountry() {
		return defaultAddressCountry;
	}

	public void setDefaultAddressCountry(String defaultAddressCountry) {
		this.defaultAddressCountry = defaultAddressCountry;
	}

	public String getDefaultAddressMunicipalityName() {
		return defaultAddressMunicipalityName;
	}

	public void setDefaultAddressMunicipalityName(
			String defaultAddressMunicipalityName) {
		this.defaultAddressMunicipalityName = defaultAddressMunicipalityName;
	}

	public String getDefaultAddressPostalCode() {
		return defaultAddressPostalCode;
	}

	public void setDefaultAddressPostalCode(String defaultAddressPostalCode) {
		this.defaultAddressPostalCode = defaultAddressPostalCode;
	}

	public String getDefaultAddressStateCode() {
		return defaultAddressStateCode;
	}

	public void setDefaultAddressStateCode(String defaultAddressStateCode) {
		this.defaultAddressStateCode = defaultAddressStateCode;
	}

	public String getDefaultAddressStreetFreeText() {
		return defaultAddressStreetFreeText;
	}

	public void setDefaultAddressStreetFreeText(
			String defaultAddressStreetFreeText) {
		this.defaultAddressStreetFreeText = defaultAddressStreetFreeText;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFiscalCode() {
		return fiscalCode;
	}

	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getMailingType() {
		return mailingType;
	}

	public void setMailingType(String mailingType) {
		this.mailingType = mailingType;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public String getProfessionalName() {
		return professionalName;
	}

	public void setProfessionalName(String professionalName) {
		this.professionalName = professionalName;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getUserIdRequest() {
		return userIdRequest;
	}

	public void setUserIdRequest(String userIdRequest) {
		this.userIdRequest = userIdRequest;
	}

	public String getWebPageRequest() {
		return webPageRequest;
	}

	public void setWebPageRequest(String webPageRequest) {
		this.webPageRequest = webPageRequest;
	}

	public Long getPointsEarnedPartial() {
		return pointsEarnedPartial;
	}

	public void setPointsEarnedPartial(Long pointsEarnedPartial) {
		this.pointsEarnedPartial = pointsEarnedPartial;
	}

	public Long getPointsEarnedTotal() {
		return pointsEarnedTotal;
	}

	public void setPointsEarnedTotal(Long pointsEarnedTotal) {
		this.pointsEarnedTotal = pointsEarnedTotal;
	}

	public Long getPointsEarnedTotalQualified() {
		return pointsEarnedTotalQualified;
	}

	public void setPointsEarnedTotalQualified(Long pointsEarnedTotalQualified) {
		this.pointsEarnedTotalQualified = pointsEarnedTotalQualified;
	}

	public Long getPointsRemainingPartial() {
		return pointsRemainingPartial;
	}

	public void setPointsRemainingPartial(Long pointsRemainingPartial) {
		this.pointsRemainingPartial = pointsRemainingPartial;
	}

	public Long getPointsRemainingTotal() {
		return pointsRemainingTotal;
	}

	public void setPointsRemainingTotal(Long pointsRemainingTotal) {
		this.pointsRemainingTotal = pointsRemainingTotal;
	}

	public Long getPointsSpentPartial() {
		return pointsSpentPartial;
	}

	public void setPointsSpentPartial(Long pointsSpentPartial) {
		this.pointsSpentPartial = pointsSpentPartial;
	}

	public Long getPointsSpentTotal() {
		return pointsSpentTotal;
	}

	public void setPointsSpentTotal(Long pointsSpentTotal) {
		this.pointsSpentTotal = pointsSpentTotal;
	}

	public Long getQualifyingSegment() {
		return qualifyingSegment;
	}

	public void setQualifyingSegment(Long qualifyingSegment) {
		this.qualifyingSegment = qualifyingSegment;
	}

	public String getQualifiedFlights() {
		return qualifiedFlights;
	}

	public void setQualifiedFlights(String qualifiedFlights) {
		this.qualifiedFlights = qualifiedFlights;
	}

	public Calendar getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Calendar expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Boolean getSmsAuthorization() {
		return smsAuthorization;
	}

	public void setSmsAuthorization(Boolean smsAuthorization) {
		this.smsAuthorization = smsAuthorization;
	}

	public Boolean getProfiling() {
		return profiling;
	}

	public void setProfiling(Boolean profiling) {
		this.profiling = profiling;
	}

	public MMTitleTypeEnum getCustomerTitle() {
		return customerTitle;
	}

	public void setCustomerTitle(MMTitleTypeEnum customerTitle) {
		this.customerTitle = customerTitle;
	}

	public MMWorkPositionTypeEnum getCustomerWorkPosition() {
		return customerWorkPosition;
	}

	public void setCustomerWorkPosition(
			MMWorkPositionTypeEnum customerWorkPosition) {
		this.customerWorkPosition = customerWorkPosition;
	}

	public MMAddressTypeEnum getDefaultAddressType() {
		return defaultAddressType;
	}

	public void setDefaultAddressType(MMAddressTypeEnum defaultAddressType) {
		this.defaultAddressType = defaultAddressType;
	}

	public MMGenderTypeEnum getGender() {
		return gender;
	}

	public void setGender(MMGenderTypeEnum gender) {
		this.gender = gender;
	}

	public MMMaritalStatusTypeEnum getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(MMMaritalStatusTypeEnum maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public MMRegistrationTypeEnum getRegistrationRequired() {
		return registrationRequired;
	}

	public void setRegistrationRequired(
			MMRegistrationTypeEnum registrationRequired) {
		this.registrationRequired = registrationRequired;
	}

	public MMSystemChannelTypeEnum getSystemChannel() {
		return systemChannel;
	}

	public void setSystemChannel(MMSystemChannelTypeEnum systemChannel) {
		this.systemChannel = systemChannel;
	}

	public MMTierCodeEnum getTierCode() {
		return tierCode;
	}

	public void setTierCode(MMTierCodeEnum tierCode) {
		this.tierCode = tierCode;
	}

	public List<MMTelephoneData> getTelephones() {
		return telephones;
	}

	public void setTelephones(List<MMTelephoneData> telephones) {
		this.telephones = telephones;
	}

	public List<MMActivityData> getActivities() {
		return activities;
	}

	public void setActivities(List<MMActivityData> activities) {
		this.activities = activities;
	}

	public List<MMAddressData> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<MMAddressData> addresses) {
		this.addresses = addresses;
	}

	public List<MMContractData> getContracts() {
		return contracts;
	}

	public void setContracts(List<MMContractData> contracts) {
		this.contracts = contracts;
	}

	public List<MMCreditCardData> getCreditCards() {
		return creditCards;
	}

	public void setCreditCards(List<MMCreditCardData> creditCards) {
		this.creditCards = creditCards;
	}

	public List<MMFlightData> getFlight() {
		return flight;
	}

	public void setFlight(List<MMFlightData> flight) {
		this.flight = flight;
	}

	public List<MMLifestyleData> getLifestyles() {
		return lifestyles;
	}

	public void setLifestyles(List<MMLifestyleData> lifestyles) {
		this.lifestyles = lifestyles;
	}

	public List<MMMessageData> getMessages() {
		return messages;
	}

	public void setMessages(List<MMMessageData> messages) {
		this.messages = messages;
	}

	public List<MMPreferenceData> getPreferences() {
		return preferences;
	}

	public void setPreferences(List<MMPreferenceData> preferences) {
		this.preferences = preferences;
	}

	public List<MMProgramData> getPrograms() {
		return programs;
	}

	public void setPrograms(List<MMProgramData> programs) {
		this.programs = programs;
	}

	public MMCreditCardData getStoredCreditCard() {
		return storedCreditCard;
	}

	public void setStoredCreditCard(MMCreditCardData storedCreditCard) {
		this.storedCreditCard = storedCreditCard;
	}

	//SA
	private String username;
	private String password;
	private Integer accessFailureCount;
	private String certifiedCountryNumber;
	private String certifiedEmailAccount;
	private String certifiedPhoneAccount;
	private Boolean emailCertificateFlag;
	private Boolean flagLockedAccount;
	private Boolean flagOldCustomer;
	private Boolean flagRememberPassword;
	private Boolean flagSocialCertified;
	private Boolean flagVerificaPassword;
	private Boolean flagVerificaRisposta;
	private Boolean flagVerificaUserName;
	private Integer idDomandaSegreta;
	private Boolean phoneCertificateFlag;
	private String rispostaSegreta;
	private String socialToken;
	private String socialUserId;

	public String getUsername() {
		return username;
	}
	public void setUserName(String username) {		this.username = username;	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getAccessFailureCount() {
		return accessFailureCount;
	}
	public void setAccessFailureCount(Integer accessFailureCount) {
		this.accessFailureCount = accessFailureCount;
	}

	public String getCertifiedCountryNumber() {
		return certifiedCountryNumber;
	}
	public void setCertifiedCountryNumber(String certifiedCountryNumber) { this.certifiedCountryNumber = certifiedCountryNumber; }

	public String getCertifiedEmailAccount() {
		return certifiedEmailAccount;
	}

	public void setCertifiedEmailAccount(String certifiedEmailAccount) {		this.certifiedEmailAccount = certifiedEmailAccount;	}

	public String getCertifiedPhoneAccount() {
		return certifiedPhoneAccount;
	}

	public void setCertifiedPhoneAccount(String certifiedPhoneAccount) {		this.certifiedPhoneAccount = certifiedPhoneAccount;	}

	public Boolean getEmailCertificateFlag() {
		return emailCertificateFlag;
	}

	public void setEmailCertificateFlag(Boolean emailCertificateFlag) {
		this.emailCertificateFlag = emailCertificateFlag;
	}

	public Boolean getFlagLockedAccount() {
		return flagLockedAccount;
	}

	public void setFlagLockedAccount(Boolean flagLockedAccount) {
		this.flagLockedAccount = flagLockedAccount;
	}

	public Boolean getFlagOldCustomer() {
		return flagOldCustomer;
	}

	public void setFlagOldCustomer(Boolean flagOldCustomer) {
		this.flagOldCustomer = flagOldCustomer;
	}

	public Boolean getFlagRememberPassword() {
		return flagRememberPassword;
	}

	public void setFlagRememberPassword(Boolean flagRememberPassword) {
		this.flagRememberPassword = flagRememberPassword;
	}

	public Boolean getFlagSocialCertified() {
		return flagSocialCertified;
	}

	public void setFlagSocialCertified(Boolean flagSocialCertified) {
		this.flagSocialCertified = flagSocialCertified;
	}

	public Boolean getFlagVerificaPassword() {
		return flagVerificaPassword;
	}

	public void setFlagVerificaPassword(Boolean flagVerificaPassword) {
		this.flagVerificaPassword = flagVerificaPassword;
	}

	public Boolean getFlagVerificaRisposta() {
		return flagVerificaRisposta;
	}

	public void setFlagVerificaRisposta(Boolean flagVerificaRisposta) {
		this.flagVerificaRisposta = flagVerificaRisposta;
	}

	public Boolean getFlagVerificaUserName() {
		return flagVerificaUserName;
	}

	public void setFlagVerificaUserName(Boolean flagVerificaUserName) {
		this.flagVerificaUserName = flagVerificaUserName;
	}

	public Integer getIdDomandaSegreta() {
		return idDomandaSegreta;
	}

	public void setIdDomandaSegreta(Integer idDomandaSegreta) {
		this.idDomandaSegreta = idDomandaSegreta;
	}

	public Boolean getPhoneCertificateFlag() {
		return phoneCertificateFlag;
	}

	public void setPhoneCertificateFlag(Boolean phoneCertificateFlag) {
		this.phoneCertificateFlag = phoneCertificateFlag;
	}

	public String getRispostaSegreta() {
		return rispostaSegreta;
	}

	public void setRispostaSegreta(String rispostaSegreta) {
		this.rispostaSegreta = rispostaSegreta;
	}

	public String getSocialToken() {
		return socialToken;
	}

	public void setSocialToken(String socialToken) {
		this.socialToken = socialToken;
	}

	public String getSocialUserId() {
		return socialUserId;
	}

	public void setSocialUserId(String socialUserId) {
		this.socialUserId = socialUserId;
	}

	//SA

	public static String getNAME() {
		return NAME;
	}

	@Override
	public String toString() {
		return "MMCustomerProfileData{" +
				"birthDate=" + birthDate +
				", passportDate=" + passportDate +
				", tierDate=" + tierDate +
				", contractAgree=" + contractAgree +
				", contratcType='" + contratcType + '\'' +
				", customerName='" + customerName + '\'' +
				", customerNickName='" + customerNickName + '\'' +
				", customerNumber='" + customerNumber + '\'' +
				", customerPinCode='" + customerPinCode + '\'' +
				", customerSurname='" + customerSurname + '\'' +
				", defaultAddressCountry='" + defaultAddressCountry + '\'' +
				", defaultAddressMunicipalityName='" + defaultAddressMunicipalityName + '\'' +
				", defaultAddressPostalCode='" + defaultAddressPostalCode + '\'' +
				", defaultAddressStateCode='" + defaultAddressStateCode + '\'' +
				", defaultAddressStreetFreeText='" + defaultAddressStreetFreeText + '\'' +
				", email='" + email + '\'' +
				", fiscalCode='" + fiscalCode + '\'' +
				", language='" + language + '\'' +
				", mailingType='" + mailingType + '\'' +
				", market='" + market + '\'' +
				", professionalName='" + professionalName + '\'' +
				", statusCode='" + statusCode + '\'' +
				", userIdRequest='" + userIdRequest + '\'' +
				", webPageRequest='" + webPageRequest + '\'' +
				", pointsEarnedPartial=" + pointsEarnedPartial +
				", pointsEarnedTotal=" + pointsEarnedTotal +
				", pointsEarnedTotalQualified=" + pointsEarnedTotalQualified +
				", pointsRemainingPartial=" + pointsRemainingPartial +
				", pointsRemainingTotal=" + pointsRemainingTotal +
				", pointsSpentPartial=" + pointsSpentPartial +
				", pointsSpentTotal=" + pointsSpentTotal +
				", qualifyingSegment=" + qualifyingSegment +
				", qualifiedFlights='" + qualifiedFlights + '\'' +
				", expirationDate=" + expirationDate +
				", smsAuthorization=" + smsAuthorization +
				", profiling=" + profiling +
				", customerTitle=" + customerTitle +
				", customerWorkPosition=" + customerWorkPosition +
				", defaultAddressType=" + defaultAddressType +
				", gender=" + gender +
				", maritalStatus=" + maritalStatus +
				", registrationRequired=" + registrationRequired +
				", systemChannel=" + systemChannel +
				", tierCode=" + tierCode +
				", storedCreditCard=" + storedCreditCard +
				", telephones=" + telephones +
				", activities=" + activities +
				", addresses=" + addresses +
				", contracts=" + contracts +
				", creditCards=" + creditCards +
				", flight=" + flight +
				", lifestyles=" + lifestyles +
				", messages=" + messages +
				", preferences=" + preferences +
				", programs=" + programs +
				", username='" + username + '\'' +
				", password='" + password + '\'' +
				", accessFailureCount=" + accessFailureCount +
				", certifiedCountryNumber='" + certifiedCountryNumber + '\'' +
				", certifiedEmailAccount='" + certifiedEmailAccount + '\'' +
				", certifiedPhoneAccount='" + certifiedPhoneAccount + '\'' +
				", emailCertificateFlag=" + emailCertificateFlag +
				", flagLockedAccount=" + flagLockedAccount +
				", flagOldCustomer=" + flagOldCustomer +
				", flagRememberPassword=" + flagRememberPassword +
				", flagSocialCertified=" + flagSocialCertified +
				", flagVerificaPassword=" + flagVerificaPassword +
				", flagVerificaRisposta=" + flagVerificaRisposta +
				", flagVerificaUserName=" + flagVerificaUserName +
				", idDomandaSegreta=" + idDomandaSegreta +
				", phoneCertificateFlag=" + phoneCertificateFlag +
				", rispostaSegreta='" + rispostaSegreta + '\'' +
				", socialToken='" + socialToken + '\'' +
				", socialUserId='" + socialUserId + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		MMCustomerProfileData that = (MMCustomerProfileData) o;

		if (birthDate != null ? !birthDate.equals(that.birthDate) : that.birthDate != null) return false;
		if (passportDate != null ? !passportDate.equals(that.passportDate) : that.passportDate != null) return false;
		if (tierDate != null ? !tierDate.equals(that.tierDate) : that.tierDate != null) return false;
		if (contractAgree != null ? !contractAgree.equals(that.contractAgree) : that.contractAgree != null)
			return false;
		if (contratcType != null ? !contratcType.equals(that.contratcType) : that.contratcType != null) return false;
		if (customerName != null ? !customerName.equals(that.customerName) : that.customerName != null) return false;
		if (customerNickName != null ? !customerNickName.equals(that.customerNickName) : that.customerNickName != null)
			return false;
		if (customerNumber != null ? !customerNumber.equals(that.customerNumber) : that.customerNumber != null)
			return false;
		if (customerPinCode != null ? !customerPinCode.equals(that.customerPinCode) : that.customerPinCode != null)
			return false;
		if (customerSurname != null ? !customerSurname.equals(that.customerSurname) : that.customerSurname != null)
			return false;
		if (defaultAddressCountry != null ? !defaultAddressCountry.equals(that.defaultAddressCountry) : that.defaultAddressCountry != null)
			return false;
		if (defaultAddressMunicipalityName != null ? !defaultAddressMunicipalityName.equals(that.defaultAddressMunicipalityName) : that.defaultAddressMunicipalityName != null)
			return false;
		if (defaultAddressPostalCode != null ? !defaultAddressPostalCode.equals(that.defaultAddressPostalCode) : that.defaultAddressPostalCode != null)
			return false;
		if (defaultAddressStateCode != null ? !defaultAddressStateCode.equals(that.defaultAddressStateCode) : that.defaultAddressStateCode != null)
			return false;
		if (defaultAddressStreetFreeText != null ? !defaultAddressStreetFreeText.equals(that.defaultAddressStreetFreeText) : that.defaultAddressStreetFreeText != null)
			return false;
		if (email != null ? !email.equals(that.email) : that.email != null) return false;
		if (fiscalCode != null ? !fiscalCode.equals(that.fiscalCode) : that.fiscalCode != null) return false;
		if (language != null ? !language.equals(that.language) : that.language != null) return false;
		if (mailingType != null ? !mailingType.equals(that.mailingType) : that.mailingType != null) return false;
		if (market != null ? !market.equals(that.market) : that.market != null) return false;
		if (professionalName != null ? !professionalName.equals(that.professionalName) : that.professionalName != null)
			return false;
		if (statusCode != null ? !statusCode.equals(that.statusCode) : that.statusCode != null) return false;
		if (userIdRequest != null ? !userIdRequest.equals(that.userIdRequest) : that.userIdRequest != null)
			return false;
		if (webPageRequest != null ? !webPageRequest.equals(that.webPageRequest) : that.webPageRequest != null)
			return false;
		if (pointsEarnedPartial != null ? !pointsEarnedPartial.equals(that.pointsEarnedPartial) : that.pointsEarnedPartial != null)
			return false;
		if (pointsEarnedTotal != null ? !pointsEarnedTotal.equals(that.pointsEarnedTotal) : that.pointsEarnedTotal != null)
			return false;
		if (pointsEarnedTotalQualified != null ? !pointsEarnedTotalQualified.equals(that.pointsEarnedTotalQualified) : that.pointsEarnedTotalQualified != null)
			return false;
		if (pointsRemainingPartial != null ? !pointsRemainingPartial.equals(that.pointsRemainingPartial) : that.pointsRemainingPartial != null)
			return false;
		if (pointsRemainingTotal != null ? !pointsRemainingTotal.equals(that.pointsRemainingTotal) : that.pointsRemainingTotal != null)
			return false;
		if (pointsSpentPartial != null ? !pointsSpentPartial.equals(that.pointsSpentPartial) : that.pointsSpentPartial != null)
			return false;
		if (pointsSpentTotal != null ? !pointsSpentTotal.equals(that.pointsSpentTotal) : that.pointsSpentTotal != null)
			return false;
		if (qualifyingSegment != null ? !qualifyingSegment.equals(that.qualifyingSegment) : that.qualifyingSegment != null)
			return false;
		if (qualifiedFlights != null ? !qualifiedFlights.equals(that.qualifiedFlights) : that.qualifiedFlights != null)
			return false;
		if (expirationDate != null ? !expirationDate.equals(that.expirationDate) : that.expirationDate != null)
			return false;
		if (smsAuthorization != null ? !smsAuthorization.equals(that.smsAuthorization) : that.smsAuthorization != null)
			return false;
		if (profiling != null ? !profiling.equals(that.profiling) : that.profiling != null) return false;
		if (customerTitle != that.customerTitle) return false;
		if (customerWorkPosition != that.customerWorkPosition) return false;
		if (defaultAddressType != that.defaultAddressType) return false;
		if (gender != that.gender) return false;
		if (maritalStatus != that.maritalStatus) return false;
		if (registrationRequired != that.registrationRequired) return false;
		if (systemChannel != that.systemChannel) return false;
		if (tierCode != that.tierCode) return false;
		if (storedCreditCard != null ? !storedCreditCard.equals(that.storedCreditCard) : that.storedCreditCard != null)
			return false;
		if (telephones != null ? !telephones.equals(that.telephones) : that.telephones != null) return false;
		if (activities != null ? !activities.equals(that.activities) : that.activities != null) return false;
		if (addresses != null ? !addresses.equals(that.addresses) : that.addresses != null) return false;
		if (contracts != null ? !contracts.equals(that.contracts) : that.contracts != null) return false;
		if (creditCards != null ? !creditCards.equals(that.creditCards) : that.creditCards != null) return false;
		if (flight != null ? !flight.equals(that.flight) : that.flight != null) return false;
		if (lifestyles != null ? !lifestyles.equals(that.lifestyles) : that.lifestyles != null) return false;
		if (messages != null ? !messages.equals(that.messages) : that.messages != null) return false;
		if (preferences != null ? !preferences.equals(that.preferences) : that.preferences != null) return false;
		if (programs != null ? !programs.equals(that.programs) : that.programs != null) return false;
		if (username != null ? !username.equals(that.username) : that.username != null) return false;
		if (password != null ? !password.equals(that.password) : that.password != null) return false;
		if (accessFailureCount != null ? !accessFailureCount.equals(that.accessFailureCount) : that.accessFailureCount != null)
			return false;
		if (certifiedCountryNumber != null ? !certifiedCountryNumber.equals(that.certifiedCountryNumber) : that.certifiedCountryNumber != null)
			return false;
		if (certifiedEmailAccount != null ? !certifiedEmailAccount.equals(that.certifiedEmailAccount) : that.certifiedEmailAccount != null)
			return false;
		if (certifiedPhoneAccount != null ? !certifiedPhoneAccount.equals(that.certifiedPhoneAccount) : that.certifiedPhoneAccount != null)
			return false;
		if (emailCertificateFlag != null ? !emailCertificateFlag.equals(that.emailCertificateFlag) : that.emailCertificateFlag != null)
			return false;
		if (flagLockedAccount != null ? !flagLockedAccount.equals(that.flagLockedAccount) : that.flagLockedAccount != null)
			return false;
		if (flagOldCustomer != null ? !flagOldCustomer.equals(that.flagOldCustomer) : that.flagOldCustomer != null)
			return false;
		if (flagRememberPassword != null ? !flagRememberPassword.equals(that.flagRememberPassword) : that.flagRememberPassword != null)
			return false;
		if (flagSocialCertified != null ? !flagSocialCertified.equals(that.flagSocialCertified) : that.flagSocialCertified != null)
			return false;
		if (flagVerificaPassword != null ? !flagVerificaPassword.equals(that.flagVerificaPassword) : that.flagVerificaPassword != null)
			return false;
		if (flagVerificaRisposta != null ? !flagVerificaRisposta.equals(that.flagVerificaRisposta) : that.flagVerificaRisposta != null)
			return false;
		if (flagVerificaUserName != null ? !flagVerificaUserName.equals(that.flagVerificaUserName) : that.flagVerificaUserName != null)
			return false;
		if (idDomandaSegreta != null ? !idDomandaSegreta.equals(that.idDomandaSegreta) : that.idDomandaSegreta != null)
			return false;
		if (phoneCertificateFlag != null ? !phoneCertificateFlag.equals(that.phoneCertificateFlag) : that.phoneCertificateFlag != null)
			return false;
		if (rispostaSegreta != null ? !rispostaSegreta.equals(that.rispostaSegreta) : that.rispostaSegreta != null)
			return false;
		if (socialToken != null ? !socialToken.equals(that.socialToken) : that.socialToken != null) return false;
		return socialUserId != null ? socialUserId.equals(that.socialUserId) : that.socialUserId == null;
	}

	@Override
	public int hashCode() {
		int result = birthDate != null ? birthDate.hashCode() : 0;
		result = 31 * result + (passportDate != null ? passportDate.hashCode() : 0);
		result = 31 * result + (tierDate != null ? tierDate.hashCode() : 0);
		result = 31 * result + (contractAgree != null ? contractAgree.hashCode() : 0);
		result = 31 * result + (contratcType != null ? contratcType.hashCode() : 0);
		result = 31 * result + (customerName != null ? customerName.hashCode() : 0);
		result = 31 * result + (customerNickName != null ? customerNickName.hashCode() : 0);
		result = 31 * result + (customerNumber != null ? customerNumber.hashCode() : 0);
		result = 31 * result + (customerPinCode != null ? customerPinCode.hashCode() : 0);
		result = 31 * result + (customerSurname != null ? customerSurname.hashCode() : 0);
		result = 31 * result + (defaultAddressCountry != null ? defaultAddressCountry.hashCode() : 0);
		result = 31 * result + (defaultAddressMunicipalityName != null ? defaultAddressMunicipalityName.hashCode() : 0);
		result = 31 * result + (defaultAddressPostalCode != null ? defaultAddressPostalCode.hashCode() : 0);
		result = 31 * result + (defaultAddressStateCode != null ? defaultAddressStateCode.hashCode() : 0);
		result = 31 * result + (defaultAddressStreetFreeText != null ? defaultAddressStreetFreeText.hashCode() : 0);
		result = 31 * result + (email != null ? email.hashCode() : 0);
		result = 31 * result + (fiscalCode != null ? fiscalCode.hashCode() : 0);
		result = 31 * result + (language != null ? language.hashCode() : 0);
		result = 31 * result + (mailingType != null ? mailingType.hashCode() : 0);
		result = 31 * result + (market != null ? market.hashCode() : 0);
		result = 31 * result + (professionalName != null ? professionalName.hashCode() : 0);
		result = 31 * result + (statusCode != null ? statusCode.hashCode() : 0);
		result = 31 * result + (userIdRequest != null ? userIdRequest.hashCode() : 0);
		result = 31 * result + (webPageRequest != null ? webPageRequest.hashCode() : 0);
		result = 31 * result + (pointsEarnedPartial != null ? pointsEarnedPartial.hashCode() : 0);
		result = 31 * result + (pointsEarnedTotal != null ? pointsEarnedTotal.hashCode() : 0);
		result = 31 * result + (pointsEarnedTotalQualified != null ? pointsEarnedTotalQualified.hashCode() : 0);
		result = 31 * result + (pointsRemainingPartial != null ? pointsRemainingPartial.hashCode() : 0);
		result = 31 * result + (pointsRemainingTotal != null ? pointsRemainingTotal.hashCode() : 0);
		result = 31 * result + (pointsSpentPartial != null ? pointsSpentPartial.hashCode() : 0);
		result = 31 * result + (pointsSpentTotal != null ? pointsSpentTotal.hashCode() : 0);
		result = 31 * result + (qualifyingSegment != null ? qualifyingSegment.hashCode() : 0);
		result = 31 * result + (qualifiedFlights != null ? qualifiedFlights.hashCode() : 0);
		result = 31 * result + (expirationDate != null ? expirationDate.hashCode() : 0);
		result = 31 * result + (smsAuthorization != null ? smsAuthorization.hashCode() : 0);
		result = 31 * result + (profiling != null ? profiling.hashCode() : 0);
		result = 31 * result + (customerTitle != null ? customerTitle.hashCode() : 0);
		result = 31 * result + (customerWorkPosition != null ? customerWorkPosition.hashCode() : 0);
		result = 31 * result + (defaultAddressType != null ? defaultAddressType.hashCode() : 0);
		result = 31 * result + (gender != null ? gender.hashCode() : 0);
		result = 31 * result + (maritalStatus != null ? maritalStatus.hashCode() : 0);
		result = 31 * result + (registrationRequired != null ? registrationRequired.hashCode() : 0);
		result = 31 * result + (systemChannel != null ? systemChannel.hashCode() : 0);
		result = 31 * result + (tierCode != null ? tierCode.hashCode() : 0);
		result = 31 * result + (storedCreditCard != null ? storedCreditCard.hashCode() : 0);
		result = 31 * result + (telephones != null ? telephones.hashCode() : 0);
		result = 31 * result + (activities != null ? activities.hashCode() : 0);
		result = 31 * result + (addresses != null ? addresses.hashCode() : 0);
		result = 31 * result + (contracts != null ? contracts.hashCode() : 0);
		result = 31 * result + (creditCards != null ? creditCards.hashCode() : 0);
		result = 31 * result + (flight != null ? flight.hashCode() : 0);
		result = 31 * result + (lifestyles != null ? lifestyles.hashCode() : 0);
		result = 31 * result + (messages != null ? messages.hashCode() : 0);
		result = 31 * result + (preferences != null ? preferences.hashCode() : 0);
		result = 31 * result + (programs != null ? programs.hashCode() : 0);
		result = 31 * result + (username != null ? username.hashCode() : 0);
		result = 31 * result + (password != null ? password.hashCode() : 0);
		result = 31 * result + (accessFailureCount != null ? accessFailureCount.hashCode() : 0);
		result = 31 * result + (certifiedCountryNumber != null ? certifiedCountryNumber.hashCode() : 0);
		result = 31 * result + (certifiedEmailAccount != null ? certifiedEmailAccount.hashCode() : 0);
		result = 31 * result + (certifiedPhoneAccount != null ? certifiedPhoneAccount.hashCode() : 0);
		result = 31 * result + (emailCertificateFlag != null ? emailCertificateFlag.hashCode() : 0);
		result = 31 * result + (flagLockedAccount != null ? flagLockedAccount.hashCode() : 0);
		result = 31 * result + (flagOldCustomer != null ? flagOldCustomer.hashCode() : 0);
		result = 31 * result + (flagRememberPassword != null ? flagRememberPassword.hashCode() : 0);
		result = 31 * result + (flagSocialCertified != null ? flagSocialCertified.hashCode() : 0);
		result = 31 * result + (flagVerificaPassword != null ? flagVerificaPassword.hashCode() : 0);
		result = 31 * result + (flagVerificaRisposta != null ? flagVerificaRisposta.hashCode() : 0);
		result = 31 * result + (flagVerificaUserName != null ? flagVerificaUserName.hashCode() : 0);
		result = 31 * result + (idDomandaSegreta != null ? idDomandaSegreta.hashCode() : 0);
		result = 31 * result + (phoneCertificateFlag != null ? phoneCertificateFlag.hashCode() : 0);
		result = 31 * result + (rispostaSegreta != null ? rispostaSegreta.hashCode() : 0);
		result = 31 * result + (socialToken != null ? socialToken.hashCode() : 0);
		result = 31 * result + (socialUserId != null ? socialUserId.hashCode() : 0);
		return result;
	}
}