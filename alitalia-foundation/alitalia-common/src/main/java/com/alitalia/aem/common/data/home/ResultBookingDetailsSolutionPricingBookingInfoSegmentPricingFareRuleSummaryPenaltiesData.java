package com.alitalia.aem.common.data.home;

import java.util.List;

public class ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesData {
	
	private List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData> cancellationAndRefundPenaltyField;
	private List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData> changePenaltyField;
	private boolean nonrefundableField;
	private boolean nonrefundableFieldSpecified;
	
	public List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData> getCancellationAndRefundPenaltyField() {
		return cancellationAndRefundPenaltyField;
	}
	
	public void setCancellationAndRefundPenaltyField(
			List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData> cancellationAndRefundPenaltyField) {
		this.cancellationAndRefundPenaltyField = cancellationAndRefundPenaltyField;
	}
	
	public List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData> getChangePenaltyField() {
		return changePenaltyField;
	}
	
	public void setChangePenaltyField(
			List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData> changePenaltyField) {
		this.changePenaltyField = changePenaltyField;
	}
	
	public boolean isNonrefundableField() {
		return nonrefundableField;
	}
	
	public void setNonrefundableField(boolean nonrefundableField) {
		this.nonrefundableField = nonrefundableField;
	}
	
	public boolean isNonrefundableFieldSpecified() {
		return nonrefundableFieldSpecified;
	}
	
	public void setNonrefundableFieldSpecified(
			boolean nonrefundableFieldSpecified) {
		this.nonrefundableFieldSpecified = nonrefundableFieldSpecified;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cancellationAndRefundPenaltyField == null) ? 0
						: cancellationAndRefundPenaltyField.hashCode());
		result = prime
				* result
				+ ((changePenaltyField == null) ? 0 : changePenaltyField
						.hashCode());
		result = prime * result + (nonrefundableField ? 1231 : 1237);
		result = prime * result + (nonrefundableFieldSpecified ? 1231 : 1237);
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesData other = (ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesData) obj;
		if (cancellationAndRefundPenaltyField == null) {
			if (other.cancellationAndRefundPenaltyField != null)
				return false;
		} else if (!cancellationAndRefundPenaltyField
				.equals(other.cancellationAndRefundPenaltyField))
			return false;
		if (changePenaltyField == null) {
			if (other.changePenaltyField != null)
				return false;
		} else if (!changePenaltyField.equals(other.changePenaltyField))
			return false;
		if (nonrefundableField != other.nonrefundableField)
			return false;
		if (nonrefundableFieldSpecified != other.nonrefundableFieldSpecified)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesData [cancellationAndRefundPenaltyField="
				+ cancellationAndRefundPenaltyField
				+ ", changePenaltyField="
				+ changePenaltyField
				+ ", nonrefundableField="
				+ nonrefundableField
				+ ", nonrefundableFieldSpecified="
				+ nonrefundableFieldSpecified + "]";
	}
	
}
