package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.mmb.MmbAmericanStatesData;
import com.alitalia.aem.common.messages.BaseResponse;

public class WebCheckinStatesResponse extends BaseResponse {

	private List<MmbAmericanStatesData> americanStates;

	public List<MmbAmericanStatesData> getAmericanStates() {
		return americanStates;
	}

	public void setAmericanStates(List<MmbAmericanStatesData> americanStates) {
		this.americanStates = americanStates;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((americanStates == null) ? 0 : americanStates.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebCheckinStatesResponse other = (WebCheckinStatesResponse) obj;
		if (americanStates == null) {
			if (other.americanStates != null)
				return false;
		} else if (!americanStates.equals(other.americanStates))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WebCheckinStatesResponse [americanStates=" + americanStates
				+ "]";
	}
}