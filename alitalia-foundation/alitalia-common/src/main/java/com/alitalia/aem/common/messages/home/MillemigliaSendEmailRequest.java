package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.BaseRequest;

public class MillemigliaSendEmailRequest extends BaseRequest {

	private String mailBody;
    private boolean mailSent;
    private String mailSubject;
    private MMCustomerProfileData  customerProfile;
	
    public String getMailBody() {
		return mailBody;
	}
    
	public void setMailBody(String mailBody) {
		this.mailBody = mailBody;
	}
	
	public boolean isMailSent() {
		return mailSent;
	}
	
	public void setMailSent(boolean mailSent) {
		this.mailSent = mailSent;
	}
	
	public String getMailSubject() {
		return mailSubject;
	}
	
	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}

	public MMCustomerProfileData getCustomerProfile() {
		return customerProfile;
	}

	public void setCustomerProfile(MMCustomerProfileData customerProfile) {
		this.customerProfile = customerProfile;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((customerProfile == null) ? 0 : customerProfile.hashCode());
		result = prime * result
				+ ((mailBody == null) ? 0 : mailBody.hashCode());
		result = prime * result + (mailSent ? 1231 : 1237);
		result = prime * result
				+ ((mailSubject == null) ? 0 : mailSubject.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MillemigliaSendEmailRequest other = (MillemigliaSendEmailRequest) obj;
		if (customerProfile == null) {
			if (other.customerProfile != null)
				return false;
		} else if (!customerProfile.equals(other.customerProfile))
			return false;
		if (mailBody == null) {
			if (other.mailBody != null)
				return false;
		} else if (!mailBody.equals(other.mailBody))
			return false;
		if (mailSent != other.mailSent)
			return false;
		if (mailSubject == null) {
			if (other.mailSubject != null)
				return false;
		} else if (!mailSubject.equals(other.mailSubject))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MillemigliaSendEmailRequest [mailBody=" + mailBody
				+ ", mailSent=" + mailSent + ", mailSubject=" + mailSubject
				+ ", customerProfile=" + customerProfile + ", getTid()="
				+ getTid() + ", getSid()=" + getSid() + "]";
	}
}