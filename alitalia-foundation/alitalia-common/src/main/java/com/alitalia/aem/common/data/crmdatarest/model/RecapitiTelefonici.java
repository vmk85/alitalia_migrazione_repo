package com.alitalia.aem.common.data.crmdatarest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecapitiTelefonici {

    @SerializedName("prefissioNazionale")
    @Expose
    private String prefissioNazionale;

    @SerializedName("prefissoArea")
    @Expose
    private String prefissoArea;

    @SerializedName("numero")
    @Expose
    private String numero;

    @SerializedName("tipo")
    @Expose
    private String tipo;

    @SerializedName("cellulare")
    @Expose
    private String cellulare;

    public String getPrefissioNazionale() {
        return prefissioNazionale;
    }

    public void setPrefissioNazionale(String prefissioNazionale) {
        this.prefissioNazionale = prefissioNazionale;
    }

    public String getPrefissoArea() {
        return prefissoArea;
    }

    public void setPrefissoArea(String prefissoArea) {
        this.prefissoArea = prefissoArea;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCellulare() {
        return cellulare;
    }

    public void setCellulare(String cellulare) {
        this.cellulare = cellulare;
    }

    @Override
    public String toString() {
        return "RecapitiTelefonici{" +
                "prefissioNazionale='" + prefissioNazionale + '\'' +
                ", prefissoArea='" + prefissoArea + '\'' +
                ", numero='" + numero + '\'' +
                ", tipo='" + tipo + '\'' +
                ", cellulare='" + cellulare + '\'' +
                '}';
    }
}
