package com.alitalia.aem.common.data.checkinrest.model.passenger;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PassengerDetailList {
	
	@SerializedName("thruIndicator")
	@Expose
	private String thruIndicator;
	
	@SerializedName("lineNumber")
	@Expose
	private Integer lineNumber;
	
	@SerializedName("lineNumberSpecified")
	@Expose
	private Boolean lineNumberSpecified;
	
	@SerializedName("lastName")
	@Expose
	private String lastName;
	
	@SerializedName("firstName")
	@Expose
	private String firstName;
	
	@SerializedName("fullName")
	@Expose
	private String fullName;
	
	@SerializedName("passengerID")
	@Expose
	private String passengerID;
	
	@SerializedName("bookingClass")
	@Expose
	private String bookingClass;
	
	@SerializedName("cabin")
	@Expose
	private String cabin;
	
	@SerializedName("origin")
	@Expose
	private String origin;
	
	@SerializedName("destination")
	@Expose
	private String destination;
	
	@SerializedName("departureGate")
	@Expose
	private String departureGate;
	
	@SerializedName("seat")
	@Expose
	private String seat;
	
	@SerializedName("smokingRowFlag")
	@Expose
	private String smokingRowFlag;
	
	@SerializedName("boardingPassFlag")
	@Expose
	private String boardingPassFlag;
	
	@SerializedName("numberInParty")
	@Expose
	private Integer numberInParty;
	
	@SerializedName("numberInPartySpecified")
	@Expose
	private Boolean numberInPartySpecified;
	
	@SerializedName("groupCode")
	@Expose
	private String groupCode;
	
	@SerializedName("groupCount")
	@Expose
	private Integer groupCount;
	
	@SerializedName("groupCountSpecified")
	@Expose
	private Boolean groupCountSpecified;
	
	@SerializedName("corpIndicator")
	@Expose
	private String corpIndicator;
	
	@SerializedName("passengerType")
	@Expose
	private String passengerType;
	
	@SerializedName("fqtvCode")
	@Expose
	private String fqtvCode;
	
	@SerializedName("priorityCode")
	@Expose
	private String priorityCode;
	
	@SerializedName("upgradeCode")
	@Expose
	private String upgradeCode;
	
	@SerializedName("seniorityDate")
	@Expose
	private String seniorityDate;
	
	@SerializedName("checkInNumber")
	@Expose
	private Integer checkInNumber;
	
	@SerializedName("checkInNumberSpecified")
	@Expose
	private Boolean checkInNumberSpecified;
	
	@SerializedName("inventoryBookingCode")
	@Expose
	private String inventoryBookingCode;
	
	@SerializedName("desiredBookingCode")
	@Expose
	private String desiredBookingCode;
	
	@SerializedName("outboundFlight")
	@Expose
	private String outboundFlight;
	
	@SerializedName("bagCount")
	@Expose
	private String bagCount;
	
	@SerializedName("editCodeList")
	@Expose
	private EditCodeListPassenger editCodeList;
	
	@SerializedName("vcrNumberList")
	@Expose
	private List<VcrNumberList> vcrNumberList = null;
	
	@SerializedName("freeTextInfoList")
	@Expose
	private List<FreeTextInfoList_Passenger> freeTextInfoList = null;
	
	@SerializedName("travelDocDataList")
	@Expose
	private List<TravelDocDataList> travelDocDataList = null;
	
	@SerializedName("aeDetailsList")
	@Expose
	private List<AeDetailsListPassenger> aeDetailsList = null;
	
	@SerializedName("seatDetails")
	@Expose
	private SeatDetails seatDetails;
	
	@SerializedName("errors")
	@Expose
	private List<Error> errors = null;
	
	@SerializedName("warnings")
	@Expose
	private List<WarningPassenger> warnings = null;

	public String getThruIndicator() {
		return thruIndicator;
	}

	public void setThruIndicator(String thruIndicator) {
		this.thruIndicator = thruIndicator;
	}

	public Integer getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(Integer lineNumber) {
		this.lineNumber = lineNumber;
	}

	public Boolean getLineNumberSpecified() {
		return lineNumberSpecified;
	}

	public void setLineNumberSpecified(Boolean lineNumberSpecified) {
		this.lineNumberSpecified = lineNumberSpecified;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getPassengerID() {
		return passengerID;
	}

	public void setPassengerID(String passengerID) {
		this.passengerID = passengerID;
	}

	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	public String getCabin() {
		return cabin;
	}

	public void setCabin(String cabin) {
		this.cabin = cabin;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDepartureGate() {
		return departureGate;
	}

	public void setDepartureGate(String departureGate) {
		this.departureGate = departureGate;
	}

	public String getSeat() {
		return seat;
	}

	public void setSeat(String seat) {
		this.seat = seat;
	}

	public String getSmokingRowFlag() {
		return smokingRowFlag;
	}

	public void setSmokingRowFlag(String smokingRowFlag) {
		this.smokingRowFlag = smokingRowFlag;
	}

	public String getBoardingPassFlag() {
		return boardingPassFlag;
	}

	public void setBoardingPassFlag(String boardingPassFlag) {
		this.boardingPassFlag = boardingPassFlag;
	}

	public Integer getNumberInParty() {
		return numberInParty;
	}

	public void setNumberInParty(Integer numberInParty) {
		this.numberInParty = numberInParty;
	}

	public Boolean getNumberInPartySpecified() {
		return numberInPartySpecified;
	}

	public void setNumberInPartySpecified(Boolean numberInPartySpecified) {
		this.numberInPartySpecified = numberInPartySpecified;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public Integer getGroupCount() {
		return groupCount;
	}

	public void setGroupCount(Integer groupCount) {
		this.groupCount = groupCount;
	}

	public Boolean getGroupCountSpecified() {
		return groupCountSpecified;
	}

	public void setGroupCountSpecified(Boolean groupCountSpecified) {
		this.groupCountSpecified = groupCountSpecified;
	}

	public String getCorpIndicator() {
		return corpIndicator;
	}

	public void setCorpIndicator(String corpIndicator) {
		this.corpIndicator = corpIndicator;
	}

	public String getPassengerType() {
		return passengerType;
	}

	public void setPassengerType(String passengerType) {
		this.passengerType = passengerType;
	}

	public String getFqtvCode() {
		return fqtvCode;
	}

	public void setFqtvCode(String fqtvCode) {
		this.fqtvCode = fqtvCode;
	}

	public String getPriorityCode() {
		return priorityCode;
	}

	public void setPriorityCode(String priorityCode) {
		this.priorityCode = priorityCode;
	}

	public String getUpgradeCode() {
		return upgradeCode;
	}

	public void setUpgradeCode(String upgradeCode) {
		this.upgradeCode = upgradeCode;
	}

	public String getSeniorityDate() {
		return seniorityDate;
	}

	public void setSeniorityDate(String seniorityDate) {
		this.seniorityDate = seniorityDate;
	}

	public Integer getCheckInNumber() {
		return checkInNumber;
	}

	public void setCheckInNumber(Integer checkInNumber) {
		this.checkInNumber = checkInNumber;
	}

	public Boolean getCheckInNumberSpecified() {
		return checkInNumberSpecified;
	}

	public void setCheckInNumberSpecified(Boolean checkInNumberSpecified) {
		this.checkInNumberSpecified = checkInNumberSpecified;
	}

	public String getInventoryBookingCode() {
		return inventoryBookingCode;
	}

	public void setInventoryBookingCode(String inventoryBookingCode) {
		this.inventoryBookingCode = inventoryBookingCode;
	}

	public String getDesiredBookingCode() {
		return desiredBookingCode;
	}

	public void setDesiredBookingCode(String desiredBookingCode) {
		this.desiredBookingCode = desiredBookingCode;
	}

	public String getOutboundFlight() {
		return outboundFlight;
	}

	public void setOutboundFlight(String outboundFlight) {
		this.outboundFlight = outboundFlight;
	}

	public String getBagCount() {
		return bagCount;
	}

	public void setBagCount(String bagCount) {
		this.bagCount = bagCount;
	}

	public EditCodeListPassenger getEditCodeList() {
		return editCodeList;
	}

	public void setEditCodeList(EditCodeListPassenger editCodeList) {
		this.editCodeList = editCodeList;
	}

	public List<VcrNumberList> getVcrNumberList() {
		return vcrNumberList;
	}

	public void setVcrNumberList(List<VcrNumberList> vcrNumberList) {
		this.vcrNumberList = vcrNumberList;
	}

	public List<FreeTextInfoList_Passenger> getFreeTextInfoList() {
		return freeTextInfoList;
	}

	public void setFreeTextInfoList(List<FreeTextInfoList_Passenger> freeTextInfoList) {
		this.freeTextInfoList = freeTextInfoList;
	}

	public List<TravelDocDataList> getTravelDocDataList() {
		return travelDocDataList;
	}

	public void setTravelDocDataList(List<TravelDocDataList> travelDocDataList) {
		this.travelDocDataList = travelDocDataList;
	}

	public List<AeDetailsListPassenger> getAeDetailsList() {
		return aeDetailsList;
	}

	public void setAeDetailsList(List<AeDetailsListPassenger> aeDetailsList) {
		this.aeDetailsList = aeDetailsList;
	}

	public SeatDetails getSeatDetails() {
		return seatDetails;
	}

	public void setSeatDetails(SeatDetails seatDetails) {
		this.seatDetails = seatDetails;
	}

	public List<Error> getErrors() {
		return errors;
	}

	public void setErrors(List<Error> errors) {
		this.errors = errors;
	}

	public List<WarningPassenger> getWarnings() {
		return warnings;
	}

	public void setWarnings(List<WarningPassenger> warnings) {
		this.warnings = warnings;
	}

}
