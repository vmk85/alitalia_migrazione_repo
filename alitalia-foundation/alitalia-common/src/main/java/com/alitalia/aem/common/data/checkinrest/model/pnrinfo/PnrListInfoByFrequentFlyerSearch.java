
package com.alitalia.aem.common.data.checkinrest.model.pnrinfo;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PnrListInfoByFrequentFlyerSearch {

    @SerializedName("pnr")
    @Expose
    private List<Pnr> pnr = null;
    @SerializedName("conversationID")
    @Expose
    private String conversationID;

    private String error;
    
    public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public List<Pnr> getPnr() {
        if (pnr == null) return new ArrayList();
        return pnr;
    }

    public void setPnr(List<Pnr> pnr) {
        this.pnr = pnr;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

}
