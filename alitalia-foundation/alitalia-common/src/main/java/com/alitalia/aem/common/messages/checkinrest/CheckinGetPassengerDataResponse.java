package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.passengerdata.PassengerData;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinGetPassengerDataResponse extends BaseResponse{
	private PassengerData passengerData;
	
	public CheckinGetPassengerDataResponse(){}
	
	public CheckinGetPassengerDataResponse(String tid, String sid) {
		super(tid, sid);
	}
	public PassengerData getPassengerData() {
		return passengerData;
	}
	public void setPassengerData(PassengerData passengerData) {
		this.passengerData = passengerData;
	}
	
}
