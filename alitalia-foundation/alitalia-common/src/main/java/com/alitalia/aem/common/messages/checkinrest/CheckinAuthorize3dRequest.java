package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.messages.BaseRequest;

public class CheckinAuthorize3dRequest extends BaseRequest {

    public CheckinAuthorize3dRequest(String tid, String sid) { super(tid, sid); }

    private String merchantData;
    private String paymentResponse;
    private String iP_Address;
    private String language;
    private String market;
    private String conversationID;
    private String caller;

    public String getMerchantData() {
        return merchantData;
    }

    public void setMerchantData(String merchantData) {
        this.merchantData = merchantData;
    }

    public String getPaymentResponse() {
        return paymentResponse;
    }

    public void setPaymentResponse(String paymentResponse) {
        this.paymentResponse = paymentResponse;
    }

    public String getiP_Address() {
        return iP_Address;
    }

    public void setiP_Address(String iP_Address) {
        this.iP_Address = iP_Address;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

    public String getCaller() {
        return caller;
    }

    public void setCaller(String caller) {
        this.caller = caller;
    }
}
