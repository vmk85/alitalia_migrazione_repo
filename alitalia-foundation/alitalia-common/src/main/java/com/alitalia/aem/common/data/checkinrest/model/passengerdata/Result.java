
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("errorSource")
    @Expose
    private ErrorSource errorSource;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("completionStatus")
    @Expose
    private String completionStatus;
    @SerializedName("completionStatusSpecified")
    @Expose
    private Boolean completionStatusSpecified;
    @SerializedName("system")
    @Expose
    private System system;
    @SerializedName("systemSpecificResults")
    @Expose
    private List<SystemSpecificResult> systemSpecificResults = null;
    @SerializedName("conversationID")
    @Expose
    private String conversationID;
    @SerializedName("messageId")
    @Expose
    private String messageId;
    @SerializedName("version")
    @Expose
    private String version;
    @SerializedName("timeStamp")
    @Expose
    private String timeStamp;
    @SerializedName("timeStampSpecified")
    @Expose
    private Boolean timeStampSpecified;

    public ErrorSource getErrorSource() {
        return errorSource;
    }

    public void setErrorSource(ErrorSource errorSource) {
        this.errorSource = errorSource;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCompletionStatus() {
        return completionStatus;
    }

    public void setCompletionStatus(String completionStatus) {
        this.completionStatus = completionStatus;
    }

    public Boolean getCompletionStatusSpecified() {
        return completionStatusSpecified;
    }

    public void setCompletionStatusSpecified(Boolean completionStatusSpecified) {
        this.completionStatusSpecified = completionStatusSpecified;
    }

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }

    public List<SystemSpecificResult> getSystemSpecificResults() {
        return systemSpecificResults;
    }

    public void setSystemSpecificResults(List<SystemSpecificResult> systemSpecificResults) {
        this.systemSpecificResults = systemSpecificResults;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Boolean getTimeStampSpecified() {
        return timeStampSpecified;
    }

    public void setTimeStampSpecified(Boolean timeStampSpecified) {
        this.timeStampSpecified = timeStampSpecified;
    }

}
