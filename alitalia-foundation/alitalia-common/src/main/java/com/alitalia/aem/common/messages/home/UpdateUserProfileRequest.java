package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.data.home.NewInformationData;
import com.alitalia.aem.common.messages.BaseRequest;

public class UpdateUserProfileRequest extends BaseRequest {

	private MMCustomerProfileData customerProfile;
	private NewInformationData newInformation;

	public MMCustomerProfileData getCustomerProfile() {
		return customerProfile;
	}

	public void setCustomerProfile(MMCustomerProfileData customerProfile) {
		this.customerProfile = customerProfile;
	}

	public NewInformationData getNewInformation() {
		return newInformation;
	}

	public void setNewInformation(NewInformationData newInformation) {
		this.newInformation = newInformation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((customerProfile == null) ? 0 : customerProfile.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		UpdateUserProfileRequest other = (UpdateUserProfileRequest) obj;
		if (customerProfile == null) {
			if (other.customerProfile != null)
				return false;
		} else if (!customerProfile.equals(other.customerProfile))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UpdateUserProfileRequest [customerProfile=" + customerProfile
				+ ", newInformation=" + newInformation + "]";
	}
}