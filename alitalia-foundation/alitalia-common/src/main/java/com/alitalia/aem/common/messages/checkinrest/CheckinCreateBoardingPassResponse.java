package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.boardingpass.BoardingPass;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinCreateBoardingPassResponse extends BaseResponse{
	private BoardingPass boardingPass;
	
	public CheckinCreateBoardingPassResponse() {}

	public CheckinCreateBoardingPassResponse(String tid, String sid) {
		super(tid, sid);
	}

	public BoardingPass getBoardingPass() {
		return boardingPass;
	}

	public void setBoardingPass(BoardingPass boardingPass) {
		this.boardingPass = boardingPass;
	}
}
