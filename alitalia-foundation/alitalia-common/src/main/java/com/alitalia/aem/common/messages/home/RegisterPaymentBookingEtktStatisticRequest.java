package com.alitalia.aem.common.messages.home;


public class RegisterPaymentBookingEtktStatisticRequest extends RegisterStatisticsRequest {

	private String cartID;
	private String companyCode;
	private String discountName;
	private String eticket;
	private String importo;
	private String pnr;
	
	public RegisterPaymentBookingEtktStatisticRequest(String tid, String sid) {
		super(tid,sid);
	}
	public String getCartID() {
		return cartID;
	}
	public void setCartID(String cartID) {
		this.cartID = cartID;
	}
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public String getDiscountName() {
		return discountName;
	}
	public void setDiscountName(String discountName) {
		this.discountName = discountName;
	}
	public String getEticket() {
		return eticket;
	}
	public void setEticket(String eticket) {
		this.eticket = eticket;
	}
	public String getImporto() {
		return importo;
	}
	public void setImporto(String importo) {
		this.importo = importo;
	}
	public String getPnr() {
		return pnr;
	}
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	
	@Override
	public String toString() {
		return "RegisterPaymentBookingEtktStatisticRequest [cartID=" + cartID
				+ ", companyCode=" + companyCode + ", discountName="
				+ discountName + ", eticket=" + eticket + ", importo="
				+ importo + ", pnr=" + pnr + ", getCartID()=" + getCartID()
				+ ", getCompanyCode()=" + getCompanyCode()
				+ ", getDiscountName()=" + getDiscountName()
				+ ", getEticket()=" + getEticket() + ", getImporto()="
				+ getImporto() + ", getPnr()=" + getPnr() + ", getClientIP()="
				+ getClientIP() + ", getErrorDescr()=" + getErrorDescr()
				+ ", getSessionId()=" + getSessionId() + ", getSiteCode()="
				+ getSiteCode() + ", isSuccess()=" + isSuccess()
				+ ", getType()=" + getType() + ", getUserId()=" + getUserId()
				+ ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + ", getTid()=" + getTid() + ", getSid()="
				+ getSid() + ", getClass()=" + getClass() + "]";
	}
}