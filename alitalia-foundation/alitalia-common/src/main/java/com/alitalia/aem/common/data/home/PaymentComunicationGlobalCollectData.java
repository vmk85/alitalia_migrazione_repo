package com.alitalia.aem.common.data.home;

import java.util.Map;

public class PaymentComunicationGlobalCollectData extends
		PaymentComunicationBaseData {
	
	private String currency;
	private String customerId;
	private String errorUrl;
	private String ipAddress;
	private Boolean isPost;
	private String orderId;
	private Map<String, String> requestData;
	
	public String getCurrency() {
		return currency;
	}
	
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public String getCustomerId() {
		return customerId;
	}
	
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	public String getIpAddress() {
		return ipAddress;
	}
	
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	public Boolean getIsPost() {
		return isPost;
	}
	
	public void setIsPost(Boolean isPost) {
		this.isPost = isPost;
	}
	
	public String getOrderId() {
		return orderId;
	}
	
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	
	public Map<String, String> getRequestData() {
		return requestData;
	}
	
	public void setRequestData(Map<String, String> requestData) {
		this.requestData = requestData;
	}

	public String getErrorUrl() {
		return errorUrl;
	}

	public void setErrorUrl(String errorUrl) {
		this.errorUrl = errorUrl;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((currency == null) ? 0 : currency.hashCode());
		result = prime * result
				+ ((customerId == null) ? 0 : customerId.hashCode());
		result = prime * result
				+ ((errorUrl == null) ? 0 : errorUrl.hashCode());
		result = prime * result
				+ ((ipAddress == null) ? 0 : ipAddress.hashCode());
		result = prime * result + ((isPost == null) ? 0 : isPost.hashCode());
		result = prime * result + ((orderId == null) ? 0 : orderId.hashCode());
		result = prime * result
				+ ((requestData == null) ? 0 : requestData.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentComunicationGlobalCollectData other = (PaymentComunicationGlobalCollectData) obj;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (customerId == null) {
			if (other.customerId != null)
				return false;
		} else if (!customerId.equals(other.customerId))
			return false;
		if (errorUrl == null) {
			if (other.errorUrl != null)
				return false;
		} else if (!errorUrl.equals(other.errorUrl))
			return false;
		if (ipAddress == null) {
			if (other.ipAddress != null)
				return false;
		} else if (!ipAddress.equals(other.ipAddress))
			return false;
		if (isPost == null) {
			if (other.isPost != null)
				return false;
		} else if (!isPost.equals(other.isPost))
			return false;
		if (orderId == null) {
			if (other.orderId != null)
				return false;
		} else if (!orderId.equals(other.orderId))
			return false;
		if (requestData == null) {
			if (other.requestData != null)
				return false;
		} else if (!requestData.equals(other.requestData))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PaymentComunicationGlobalCollectData [currency=" + currency
				+ ", customerId=" + customerId + ", errorUrl=" + errorUrl
				+ ", ipAddress=" + ipAddress + ", isPost=" + isPost
				+ ", orderId=" + orderId + ", requestData=" + requestData
				+ ", getDescription()=" + getDescription()
				+ ", getLanguageCode()=" + getLanguageCode()
				+ ", getReturnUrl()=" + getReturnUrl() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass() + "]";
	}
}