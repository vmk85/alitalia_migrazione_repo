
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WeightInfo {

    @SerializedName("bagWeightLimit")
    @Expose
    private BagWeightLimit bagWeightLimit;
    @SerializedName("bagWeightRestriction")
    @Expose
    private String bagWeightRestriction;
    @SerializedName("bagWeightRestrictionSpecified")
    @Expose
    private Boolean bagWeightRestrictionSpecified;

    public BagWeightLimit getBagWeightLimit() {
        return bagWeightLimit;
    }

    public void setBagWeightLimit(BagWeightLimit bagWeightLimit) {
        this.bagWeightLimit = bagWeightLimit;
    }

    public String getBagWeightRestriction() {
        return bagWeightRestriction;
    }

    public void setBagWeightRestriction(String bagWeightRestriction) {
        this.bagWeightRestriction = bagWeightRestriction;
    }

    public Boolean getBagWeightRestrictionSpecified() {
        return bagWeightRestrictionSpecified;
    }

    public void setBagWeightRestrictionSpecified(Boolean bagWeightRestrictionSpecified) {
        this.bagWeightRestrictionSpecified = bagWeightRestrictionSpecified;
    }

}
