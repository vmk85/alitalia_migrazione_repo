package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.AirportDestinationsData;
import com.alitalia.aem.common.messages.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetDestinationsResponse extends BaseResponse {

	@SerializedName("airports")
	@Expose
	private List<AirportDestinationsData> destinations;
	@SerializedName("conversationID")
	@Expose
	private String conversationID;
	
	public GetDestinationsResponse() {}
	
	public GetDestinationsResponse(String tid, String sid) {
		super(tid, sid);
	}
	
	public List<AirportDestinationsData> getDestinations() {
		return destinations;
	}
	
	public void setDestinations(List<AirportDestinationsData> destinations) {
		this.destinations = destinations;
	}

	public String getConversationID() {
		return conversationID;
	}

	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}

}
