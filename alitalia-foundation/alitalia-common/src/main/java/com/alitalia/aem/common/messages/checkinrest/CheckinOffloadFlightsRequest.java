
package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.offload.request.FlightPas;
import com.alitalia.aem.common.data.checkinrest.model.offload.request.PnrFlightInfo;
import com.alitalia.aem.common.data.checkinrest.model.offload.request.SeatList;
import com.alitalia.aem.common.messages.BaseRequest;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CheckinOffloadFlightsRequest extends BaseRequest {

    @SerializedName("pnrFlightInfo")
    @Expose
    private List<PnrFlightInfo> pnrFlightInfo = null;
    @SerializedName("flightPass")
    @Expose
    private List<FlightPas> flightPass;
    @SerializedName("seatList")
    @Expose
    private List<SeatList> seatList;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("market")
    @Expose
    private String market;
    @SerializedName("conversationID")
    @Expose
    private String conversationID;

    public List<PnrFlightInfo> getPnrFlightInfo() {
        return pnrFlightInfo;
    }

    public void setPnrFlightInfo(List<PnrFlightInfo> pnrFlightInfo) {
        this.pnrFlightInfo = pnrFlightInfo;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

    public void setFlightPass(List<FlightPas> flightPass) {
        this.flightPass = flightPass;
    }

    public List<FlightPas> getFlightPass() {
        return flightPass;
    }

    public List<SeatList> getSeatList() {
        return seatList;
    }

    public void setSeatList(List<SeatList> seatList) {
        this.seatList = seatList;
    }
}
