package com.alitalia.aem.common.data.checkinrest.model.endpayment.response;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AZPayedTPResponse {
	
	@SerializedName("azPaxPayedTP")
	@Expose
	private List<AzPaxPayedTP> azPaxPayedTP = null;

	public List<AzPaxPayedTP> getAzPaxPayedTP() {
		return azPaxPayedTP;
	}

	public void setAzPaxPayedTP(List<AzPaxPayedTP> azPaxPayedTP) {
		this.azPaxPayedTP = azPaxPayedTP;
	}


}
