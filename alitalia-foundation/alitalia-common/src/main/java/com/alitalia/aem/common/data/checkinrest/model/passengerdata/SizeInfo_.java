
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SizeInfo_ {

    @SerializedName("bagSizeInfo")
    @Expose
    private BagSizeInfo_ bagSizeInfo;
    @SerializedName("bagSizeLimit")
    @Expose
    private String bagSizeLimit;
    @SerializedName("bagSizeLimitSpecified")
    @Expose
    private Boolean bagSizeLimitSpecified;

    public BagSizeInfo_ getBagSizeInfo() {
        return bagSizeInfo;
    }

    public void setBagSizeInfo(BagSizeInfo_ bagSizeInfo) {
        this.bagSizeInfo = bagSizeInfo;
    }

    public String getBagSizeLimit() {
        return bagSizeLimit;
    }

    public void setBagSizeLimit(String bagSizeLimit) {
        this.bagSizeLimit = bagSizeLimit;
    }

    public Boolean getBagSizeLimitSpecified() {
        return bagSizeLimitSpecified;
    }

    public void setBagSizeLimitSpecified(Boolean bagSizeLimitSpecified) {
        this.bagSizeLimitSpecified = bagSizeLimitSpecified;
    }

}
