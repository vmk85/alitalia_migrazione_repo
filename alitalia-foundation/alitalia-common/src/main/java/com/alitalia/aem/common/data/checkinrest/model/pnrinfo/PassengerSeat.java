package com.alitalia.aem.common.data.checkinrest.model.pnrinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PassengerSeat
{

    @SerializedName("bookingClass")
    @Expose
    private String bookingClass;
    @SerializedName("postoAssegnato")
    @Expose
    private Boolean postoAssegnato;
    @SerializedName("numeroPostoAssegnato")
    @Expose
    private String numeroPostoAssegnato;
    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("departureDate")
    @Expose
    private String departureDate;

    public String getBookingClass() {
        return bookingClass;
    }

    public void setBookingClass(String bookingClass) {
        this.bookingClass = bookingClass;
    }

    public Boolean getPostoAssegnato() {
        return postoAssegnato;
    }

    public void setPostoAssegnato(Boolean postoAssegnato) {
        this.postoAssegnato = postoAssegnato;
    }

    public String getNumeroPostoAssegnato() {
        return numeroPostoAssegnato;
    }

    public void setNumeroPostoAssegnato(String numeroPostoAssegnato) {
        this.numeroPostoAssegnato = numeroPostoAssegnato;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

}
