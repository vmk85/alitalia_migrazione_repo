package com.alitalia.aem.common.data.home.carnet;

public class CarnetPrenotationData {

    private CarnetBuyerData buyer;
    private CarnetData carnet;
    private String carnetCode;
    private String password;
    private CarnetPaymentData payment;
    
	public CarnetBuyerData getBuyer() {
		return buyer;
	}
	public void setBuyer(CarnetBuyerData buyer) {
		this.buyer = buyer;
	}
	public CarnetData getCarnet() {
		return carnet;
	}
	public void setCarnet(CarnetData carnet) {
		this.carnet = carnet;
	}
	public String getCarnetCode() {
		return carnetCode;
	}
	public void setCarnetCode(String carnetCode) {
		this.carnetCode = carnetCode;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public CarnetPaymentData getPayment() {
		return payment;
	}
	public void setPayment(CarnetPaymentData payment) {
		this.payment = payment;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((buyer == null) ? 0 : buyer.hashCode());
		result = prime * result + ((carnet == null) ? 0 : carnet.hashCode());
		result = prime * result
				+ ((carnetCode == null) ? 0 : carnetCode.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((payment == null) ? 0 : payment.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarnetPrenotationData other = (CarnetPrenotationData) obj;
		if (buyer == null) {
			if (other.buyer != null)
				return false;
		} else if (!buyer.equals(other.buyer))
			return false;
		if (carnet == null) {
			if (other.carnet != null)
				return false;
		} else if (!carnet.equals(other.carnet))
			return false;
		if (carnetCode == null) {
			if (other.carnetCode != null)
				return false;
		} else if (!carnetCode.equals(other.carnetCode))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (payment == null) {
			if (other.payment != null)
				return false;
		} else if (!payment.equals(other.payment))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CarnetPrenotationData [buyer=" + buyer + ", carnet=" + carnet
				+ ", carnetCode=" + carnetCode + ", password=" + password
				+ ", payment=" + payment + "]";
	}
}