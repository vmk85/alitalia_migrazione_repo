package com.alitalia.aem.common.data.home.enumerations;

public enum ARTaxInfoTypesEnum {
    NONE("None"),
    CUIT("CUIT"),
    CUIL("CUIL"),
    NOT_AR("NotAR");
    private final String value;

    ARTaxInfoTypesEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ARTaxInfoTypesEnum fromValue(String v) {
        for (ARTaxInfoTypesEnum c: ARTaxInfoTypesEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }


}
