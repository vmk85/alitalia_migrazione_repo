
package com.alitalia.aem.common.data.checkinrest.model.updatesecurityinfo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PnrLocator {

    @SerializedName("nameAssociationID")
    @Expose
    private String nameAssociationID;
    @SerializedName("nameRefNumber")
    @Expose
    private String nameRefNumber;
    @SerializedName("value")
    @Expose
    private String value;

    public String getNameAssociationID() {
        return nameAssociationID;
    }

    public void setNameAssociationID(String nameAssociationID) {
        this.nameAssociationID = nameAssociationID;
    }

    public String getNameRefNumber() {
        return nameRefNumber;
    }

    public void setNameRefNumber(String nameRefNumber) {
        this.nameRefNumber = nameRefNumber;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
