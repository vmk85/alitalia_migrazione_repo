package com.alitalia.aem.common.data.checkinrest.model.insurance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InsurancePolicy {
	
	@SerializedName("pnr")
	@Expose
	private String pnr;
	@SerializedName("buyable")
	@Expose
	private boolean buyable;
	@SerializedName("bought")
	@Expose
	private boolean bought;
	@SerializedName("currency")
	@Expose
	private String currency;
	@SerializedName("discountedPrice")
	@Expose
	private String discountedPrice;
	@SerializedName("discount")
	@Expose
	private String discount;
	@SerializedName("totalInsuranceCost")
	@Expose
	private String totalInsuranceCost;
	@SerializedName("errorDescription")
	@Expose
	private String errorDescription;
	
	public String getPnr() {
		return pnr;
	}
	
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	
	public boolean isBuyable() {
		return buyable;
	}
	
	public void setBuyable(boolean buyable) {
		this.buyable = buyable;
	}
	
	public boolean isBought() {
		return bought;
	}
	
	public void setBought(boolean bought) {
		this.bought = bought;
	}
	
	public String getCurrency() {
		return currency;
	}
	
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public String getDiscountedPrice() {
		return discountedPrice;
	}
	
	public void setDiscountedPrice(String discountedPrice) {
		this.discountedPrice = discountedPrice;
	}
	
	public String getDiscount() {
		return discount;
	}
	
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	
	public String getTotalInsuranceCost() {
		return totalInsuranceCost;
	}
	
	public void setTotalInsuranceCost(String totalInsuranceCost) {
		this.totalInsuranceCost = totalInsuranceCost;
	}
	
	public String getErrorDescription() {
		return errorDescription;
	}
	
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	

}
