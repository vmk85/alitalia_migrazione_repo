package com.alitalia.aem.common.data.home;

public class ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData {
	
	private ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryData ruleSummaryField;
	
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryData getRuleSummaryField() {
		return ruleSummaryField;
	}
	
	public void setRuleSummaryField(
			ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryData ruleSummaryField) {
		this.ruleSummaryField = ruleSummaryField;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((ruleSummaryField == null) ? 0 : ruleSummaryField.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData other = (ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData) obj;
		if (ruleSummaryField == null) {
			if (other.ruleSummaryField != null)
				return false;
		} else if (!ruleSummaryField.equals(other.ruleSummaryField))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData [ruleSummaryField="
				+ ruleSummaryField + "]";
	}
	
}
