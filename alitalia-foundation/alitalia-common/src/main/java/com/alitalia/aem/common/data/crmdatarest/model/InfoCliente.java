package com.alitalia.aem.common.data.crmdatarest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InfoCliente {

    @SerializedName("nome")
    @Expose
    private String nome;

    @SerializedName("cognome")
    @Expose
    private String cognome;

    @SerializedName("secondoNome")
    @Expose
    private String secondoNome;

    @SerializedName("titolo")
    @Expose
    private String titolo;

    @SerializedName("sesso")
    @Expose
    private String sesso;

    @SerializedName("dataNascita")
    @Expose
    private String dataNascita;

    @SerializedName("professione")
    @Expose
    private String professione;

    @SerializedName("tipoIndirizzo")
    @Expose
    private String tipoIndirizzo;

    @SerializedName("nomeAzienda")
    @Expose
    private String nomeAzienda;

    @SerializedName("indirizzo")
    @Expose
    private String indirizzo;

    @SerializedName("cap")
    @Expose
    private String cap;

    @SerializedName("citta")
    @Expose
    private String citta;

    @SerializedName("nazione")
    @Expose
    private String nazione;

    @SerializedName("stato")
    @Expose
    private String stato;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getSecondoNome() {
        return secondoNome;
    }

    public void setSecondoNome(String secondoNome) {
        this.secondoNome = secondoNome;
    }

    public String getTitolo() {
        return titolo;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public String getSesso() {
        return sesso;
    }

    public void setSesso(String sesso) {
        this.sesso = sesso;
    }

    public String getDataNascita() {
        return dataNascita;
    }

    public void setDataNascita(String dataNascita) {
        this.dataNascita = dataNascita;
    }

    public String getProfessione() {
        return professione;
    }

    public void setProfessione(String professione) {
        this.professione = professione;
    }

    public String getTipoIndirizzo() {
        return tipoIndirizzo;
    }

    public void setTipoIndirizzo(String tipoIndirizzo) {
        this.tipoIndirizzo = tipoIndirizzo;
    }

    public String getNomeAzienda() {
        return nomeAzienda;
    }

    public void setNomeAzienda(String nomeAzienda) {
        this.nomeAzienda = nomeAzienda;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    public String getCap() {
        return cap;
    }

    public void setCap(String cap) {
        this.cap = cap;
    }

    public String getCitta() {
        return citta;
    }

    public void setCitta(String città) {
        this.citta = città;
    }

    public String getNazione() {
        return nazione;
    }

    public void setNazione(String nazione) {
        this.nazione = nazione;
    }

    public String getStato() {
        return stato;
    }

    public void setStato(String stato) {
        this.stato = stato;
    }

    @Override
    public String toString() {
        return "InfoCliente{" +
                "nome='" + nome + '\'' +
                ", cognome='" + cognome + '\'' +
                ", secondoNome='" + secondoNome + '\'' +
                ", titolo='" + titolo + '\'' +
                ", sesso='" + sesso + '\'' +
                ", dataNascita='" + dataNascita + '\'' +
                ", professione='" + professione + '\'' +
                ", tipoIndirizzo='" + tipoIndirizzo + '\'' +
                ", nomeAzienda='" + nomeAzienda + '\'' +
                ", indirizzo='" + indirizzo + '\'' +
                ", cap='" + cap + '\'' +
                ", città='" + citta + '\'' +
                ", nazione='" + nazione + '\'' +
                ", stato='" + stato + '\'' +
                '}';
    }
}
