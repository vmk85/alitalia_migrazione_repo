package com.alitalia.aem.common.data.home.enumerations;

public enum DiscountTypeEnum {

    PRICE_AMOUNT("PriceAmount"),
    PRICE_PERCENTAGE("PricePercentage"),
    SCALAR_AMOUNT("ScalarAmount");
    
    private final String value;

    DiscountTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DiscountTypeEnum fromValue(String v) {
        for (DiscountTypeEnum c: DiscountTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
