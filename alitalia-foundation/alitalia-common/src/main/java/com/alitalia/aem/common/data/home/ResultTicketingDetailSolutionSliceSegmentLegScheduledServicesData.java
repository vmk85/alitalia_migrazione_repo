package com.alitalia.aem.common.data.home;

import java.util.List;

public class ResultTicketingDetailSolutionSliceSegmentLegScheduledServicesData {

    private String cabinField;
    private List<String> mealField;

    public String getCabinField() {
        return cabinField;
    }

    public void setCabinField(String value) {
        this.cabinField = value;
    }

    public List<String> getMealField() {
        return mealField;
    }

    public void setMealField(List<String> value) {
        this.mealField = value;
    }

}
