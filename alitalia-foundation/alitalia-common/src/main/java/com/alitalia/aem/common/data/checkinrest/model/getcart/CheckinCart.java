
package com.alitalia.aem.common.data.checkinrest.model.getcart;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckinCart {

    @SerializedName("pnr")
    @Expose
    private String pnr;
    @SerializedName("cartPriceToBeIssued")
    @Expose
    private Float cartPriceToBeIssued;
    @SerializedName("cartPriceIssued")
    @Expose
    private Float cartPriceIssued;
    @SerializedName("aZFlightCart")
    @Expose
    private List<AZFlightCart> aZFlightCart = null;

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public Float getCartPriceToBeIssued() {
        return cartPriceToBeIssued;
    }

    public void setCartPriceToBeIssued(Float cartPriceToBeIssued) {
        this.cartPriceToBeIssued = cartPriceToBeIssued;
    }

    public Float getCartPriceIssued() {
        return cartPriceIssued;
    }

    public void setCartPriceIssued(Float cartPriceIssued) {
        this.cartPriceIssued = cartPriceIssued;
    }

    public List<AZFlightCart> getAZFlightCart() {
        return aZFlightCart;
    }

    public void setAZFlightCart(List<AZFlightCart> aZFlightCart) {
        this.aZFlightCart = aZFlightCart;
    }

}
