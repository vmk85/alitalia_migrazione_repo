
package com.alitalia.aem.common.data.checkinrest.model.updatepassengerdetails.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BagTagPrintData {

    @SerializedName("passengerNameInfo")
    @Expose
    private PassengerNameInfo passengerNameInfo;
    @SerializedName("reservationInfo")
    @Expose
    private ReservationInfo reservationInfo;
    @SerializedName("agentInfo")
    @Expose
    private AgentInfo agentInfo;
    @SerializedName("flightInfo")
    @Expose
    private FlightInfo flightInfo;
    @SerializedName("bagInfoList")
    @Expose
    private BagInfoList bagInfoList;

    public PassengerNameInfo getPassengerNameInfo() {
        return passengerNameInfo;
    }

    public void setPassengerNameInfo(PassengerNameInfo passengerNameInfo) {
        this.passengerNameInfo = passengerNameInfo;
    }

    public ReservationInfo getReservationInfo() {
        return reservationInfo;
    }

    public void setReservationInfo(ReservationInfo reservationInfo) {
        this.reservationInfo = reservationInfo;
    }

    public AgentInfo getAgentInfo() {
        return agentInfo;
    }

    public void setAgentInfo(AgentInfo agentInfo) {
        this.agentInfo = agentInfo;
    }

    public FlightInfo getFlightInfo() {
        return flightInfo;
    }

    public void setFlightInfo(FlightInfo flightInfo) {
        this.flightInfo = flightInfo;
    }

    public BagInfoList getBagInfoList() {
        return bagInfoList;
    }

    public void setBagInfoList(BagInfoList bagInfoList) {
        this.bagInfoList = bagInfoList;
    }

}
