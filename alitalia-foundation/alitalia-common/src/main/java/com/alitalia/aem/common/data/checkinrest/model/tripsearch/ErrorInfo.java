
package com.alitalia.aem.common.data.checkinrest.model.tripsearch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ErrorInfo {

    @SerializedName("message")
    @Expose
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
