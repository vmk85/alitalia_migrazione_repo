
package com.alitalia.aem.common.data.checkinrest.model.boardingpass;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BoardingPassInfo {

    @SerializedName("models")
    @Expose
    private List<Model> models = null;
    @SerializedName("conversationID")
    @Expose
    private String conversationID;

    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getConversationID() {
		return conversationID;
	}

	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}

    public List<Model> getModels() {
        return models;
    }

    public void setModels(List<Model> models) {
        this.models = models;
    }

    public boolean addModel(Model model) {

        this.models.add(model);

        return true;
    }

}
