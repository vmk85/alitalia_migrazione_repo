package com.alitalia.aem.common.data.checkinrest.model.passenger;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GroupBoardingInfo {
	
	@SerializedName("boardingMsg")
	@Expose
	private String boardingMsg;
	
	@SerializedName("boardingRows")
	@Expose
	private List<BoardingRow> boardingRows = null;

	public String getBoardingMsg() {
		return boardingMsg;
	}

	public void setBoardingMsg(String boardingMsg) {
		this.boardingMsg = boardingMsg;
	}

	public List<BoardingRow> getBoardingRows() {
		return boardingRows;
	}

	public void setBoardingRows(List<BoardingRow> boardingRows) {
		this.boardingRows = boardingRows;
	}

}
