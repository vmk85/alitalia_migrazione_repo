
package com.alitalia.aem.common.data.checkinrest.model.reprintbp;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AncillaryList {

    @SerializedName("ancillaryDescriptions")
    @Expose
    private List<AncillaryDescription> ancillaryDescriptions = null;
    @SerializedName("groupCode")
    @Expose
    private String groupCode;
    @SerializedName("subGroupCode")
    @Expose
    private String subGroupCode;

    public List<AncillaryDescription> getAncillaryDescriptions() {
        return ancillaryDescriptions;
    }

    public void setAncillaryDescriptions(List<AncillaryDescription> ancillaryDescriptions) {
        this.ancillaryDescriptions = ancillaryDescriptions;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getSubGroupCode() {
        return subGroupCode;
    }

    public void setSubGroupCode(String subGroupCode) {
        this.subGroupCode = subGroupCode;
    }

}
