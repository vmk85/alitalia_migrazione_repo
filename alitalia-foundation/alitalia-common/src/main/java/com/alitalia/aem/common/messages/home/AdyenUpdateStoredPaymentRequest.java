package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.AdyenCardData;
import com.alitalia.aem.common.data.home.AdyenRecurringData;
import com.alitalia.aem.common.messages.AdyenBaseRequest;

public class AdyenUpdateStoredPaymentRequest  extends AdyenBaseRequest {
	
	/*
	{
	  "card": {
	    "cvc": "737",
	    "expiryMonth": "08",
	    "expiryYear": 2018,
	    "holderName": "Test Uno",
	    "number": "4444333322221111",
	    "type": "Visa"
	  },
	  "shopperEmail": "test@test.com",
	  "shopperReference": "39fbe13b0ffd46ef9ee08d09ec577034",
	  "recurring": {
	    "contract": "ONECLICK"
	  },
	  "shopperInteraction":  ??????????, <-- presente nella doc swagger
	  "selectedRecurringDetailReference": "8315362438809277",
	  "language": "IT",
	  "market": "IT",
	  "conversationID": "987654321",
  	  "caller": "WebCheckin"
	}
	*/

	private AdyenCardData card;
//	private AdyenAddressData address;
	private String shopperEmail;
	private AdyenRecurringData recurring = new AdyenRecurringData();
	private String shopperInteraction;
	private String selectedRecurringDetailReference;
	
	public AdyenCardData getCard() {
		return card;
	}

	public void setCard(AdyenCardData card) {
		this.card = card;
	}

//	public AdyenAddressData getAddress() {
//		return address;
//	}
//
//	public void setAddress(AdyenAddressData address) {
//		this.address = address;
//	}

	public String getShopperEmail() {
		return shopperEmail;
	}

	public void setShopperEmail(String shopperEmail) {
		this.shopperEmail = shopperEmail;
	}

	public AdyenRecurringData getRecurring() {
		return recurring;
	}

	public void setRecurring(AdyenRecurringData recurring) {
		this.recurring = recurring;
	}
	
	public String getShopperInteraction() {
		return shopperInteraction;
	}

	public void setShopperInteraction(String shopperInteraction) {
		this.shopperInteraction = shopperInteraction;
	}

	public String getSelectedRecurringDetailReference() {
		return selectedRecurringDetailReference;
	}

	public void setSelectedRecurringDetailReference(String selectedRecurringDetailReference) {
		this.selectedRecurringDetailReference = selectedRecurringDetailReference;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result	+ ((card == null) ? 0 : card.hashCode());
//		result = prime * result	+ ((address == null) ? 0 : address.hashCode());
		result = prime * result	+ ((shopperEmail == null) ? 0 : shopperEmail.hashCode());
		result = prime * result	+ ((recurring == null) ? 0 : recurring.hashCode());
		result = prime * result	+ ((shopperInteraction == null) ? 0 : shopperInteraction.hashCode());
		result = prime * result	+ ((selectedRecurringDetailReference == null) ? 0 : selectedRecurringDetailReference.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdyenUpdateStoredPaymentRequest other = (AdyenUpdateStoredPaymentRequest) obj;
		if (card == null) {
			if (other.card != null)
				return false;
		} else if (!card.equals(other.card))
			return false;
//		if (address == null) {
//			if (other.address != null)
//				return false;
//		} else if (!address.equals(other.address))
//			return false;
		if (shopperEmail == null) {
			if (other.shopperEmail != null)
				return false;
		} else if (!shopperEmail.equals(other.shopperEmail))
			return false;
		if (recurring == null) {
			if (other.recurring != null)
				return false;
		} else if (!recurring.equals(other.recurring))
			return false;
		if (shopperInteraction == null) {
			if (other.shopperInteraction != null)
				return false;
		} else if (!shopperInteraction.equals(other.shopperInteraction))
			return false;
		if (selectedRecurringDetailReference == null) {
			if (other.selectedRecurringDetailReference != null)
				return false;
		} else if (!selectedRecurringDetailReference.equals(other.selectedRecurringDetailReference))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "RegisterCreditCardRequest ["
				+ "card=" + card
//				+ "address=" + address
				+ "shopperEmail=" + shopperEmail
				+ "shopperReference=" + super.getShopperReference()
				+ "recurring=" + recurring
				+ "shopperInteraction=" + shopperInteraction
				+ "selectedRecurringDetailReference=" + selectedRecurringDetailReference
				+ "language=" + super.getLanguage()
				+ "market=" + super.getMarket()
				+ "conversationID=" + super.getConversationID()
				+ "caller=" + super.getCaller()
				+ "]";
	}
	
}