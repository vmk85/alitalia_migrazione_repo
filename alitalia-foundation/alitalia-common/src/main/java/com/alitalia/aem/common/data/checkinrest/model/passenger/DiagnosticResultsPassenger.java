package com.alitalia.aem.common.data.checkinrest.model.passenger;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DiagnosticResultsPassenger {
	
	@SerializedName("any")
	@Expose
	private List<AnyPassenger> any = null;
	
	@SerializedName("lineNumber")
	@Expose
	private Integer lineNumber;
	
	@SerializedName("lineNumberSpecified")
	@Expose
	private Boolean lineNumberSpecified;
	
	@SerializedName("programName")
	@Expose
	private String programName;

	public List<AnyPassenger> getAny() {
		return any;
	}

	public void setAny(List<AnyPassenger> any) {
		this.any = any;
	}

	public Integer getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(Integer lineNumber) {
		this.lineNumber = lineNumber;
	}

	public Boolean getLineNumberSpecified() {
		return lineNumberSpecified;
	}

	public void setLineNumberSpecified(Boolean lineNumberSpecified) {
		this.lineNumberSpecified = lineNumberSpecified;
	}

	public String getProgramName() {
		return programName;
	}

	public void setProgramName(String programName) {
		this.programName = programName;
	}
	

}
