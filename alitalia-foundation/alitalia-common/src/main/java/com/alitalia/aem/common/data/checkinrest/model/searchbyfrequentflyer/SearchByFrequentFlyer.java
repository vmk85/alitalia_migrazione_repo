
package com.alitalia.aem.common.data.checkinrest.model.searchbyfrequentflyer;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchByFrequentFlyer {

    @SerializedName("pnr")
    @Expose
    private List<Pnr> pnr = null;
    @SerializedName("conversationID")
    @Expose
    private String conversationID;

    public List<Pnr> getPnr() {
        return pnr;
    }

    public void setPnr(List<Pnr> pnr) {
        this.pnr = pnr;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

}
