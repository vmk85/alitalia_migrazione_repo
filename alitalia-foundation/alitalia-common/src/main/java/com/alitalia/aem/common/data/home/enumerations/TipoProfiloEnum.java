package com.alitalia.aem.common.data.home.enumerations;

public enum TipoProfiloEnum {

    MM_KIDS("MMKids"),
    MM_YOUNG("MMYoung"),
    MM_PERSONE_FISICHE("MMPersoneFisiche"),
    MM_BUSINESS("MMBusiness"),
    MM_NON_ASSEGNATO("MMNonAssegnato"),
    MM_PROFILE_ERROR("MMProfileError");


    private final String value;

    TipoProfiloEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TipoProfiloEnum fromValue(String v) {
        for (TipoProfiloEnum c: TipoProfiloEnum.values()) {
            if (c.value.equalsIgnoreCase(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}