package com.alitalia.aem.common.data.home.enumerations;

public enum MMGenderTypeEnum {

    UNKNOWN("UnKnown"),
    MALE("Male"),
    FEMALE("Female");

    private final String value;
    
    MMGenderTypeEnum(String v) {
		value = v;
	}
	
	public String value() {
		return value;
	}

	public static MMGenderTypeEnum fromValue(String v) {
		for (MMGenderTypeEnum c: MMGenderTypeEnum.values()) {
			if (c.value.equalsIgnoreCase(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
