
package com.alitalia.aem.common.data.home.carnet;

import com.alitalia.aem.common.data.home.UserInfoBaseData;
import com.alitalia.aem.common.data.home.enumerations.CreditCardTypeEnum;

public class CarnetCreditCardProviderData extends CarnetProviderData <CarnetComunicationCreditCardData> {

    private String cvv;
    private String creditCardNumber;
    private Short expiryMonth;
    private Short expiryYear;
    private Boolean is3DSecure;
    private CreditCardTypeEnum type;
    private UserInfoBaseData userInfo;
    
	public String getCvv() {
		return cvv;
	}
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	public String getCreditCardNumber() {
		return creditCardNumber;
	}
	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}
	public Short getExpiryMonth() {
		return expiryMonth;
	}
	public void setExpiryMonth(Short expiryMonth) {
		this.expiryMonth = expiryMonth;
	}
	public Short getExpiryYear() {
		return expiryYear;
	}
	public void setExpiryYear(Short expiryYear) {
		this.expiryYear = expiryYear;
	}
	public Boolean getIs3DSecure() {
		return is3DSecure;
	}
	public void setIs3DSecure(Boolean is3dSecure) {
		is3DSecure = is3dSecure;
	}
	public CreditCardTypeEnum getType() {
		return type;
	}
	public void setType(CreditCardTypeEnum type) {
		this.type = type;
	}
	public UserInfoBaseData getUserInfo() {
		return userInfo;
	}
	public void setUserInfo(UserInfoBaseData userInfo) {
		this.userInfo = userInfo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((creditCardNumber == null) ? 0 : creditCardNumber.hashCode());
		result = prime * result + ((cvv == null) ? 0 : cvv.hashCode());
		result = prime * result
				+ ((expiryMonth == null) ? 0 : expiryMonth.hashCode());
		result = prime * result
				+ ((expiryYear == null) ? 0 : expiryYear.hashCode());
		result = prime * result
				+ ((is3DSecure == null) ? 0 : is3DSecure.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result
				+ ((userInfo == null) ? 0 : userInfo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarnetCreditCardProviderData other = (CarnetCreditCardProviderData) obj;
		if (creditCardNumber == null) {
			if (other.creditCardNumber != null)
				return false;
		} else if (!creditCardNumber.equals(other.creditCardNumber))
			return false;
		if (cvv == null) {
			if (other.cvv != null)
				return false;
		} else if (!cvv.equals(other.cvv))
			return false;
		if (expiryMonth == null) {
			if (other.expiryMonth != null)
				return false;
		} else if (!expiryMonth.equals(other.expiryMonth))
			return false;
		if (expiryYear == null) {
			if (other.expiryYear != null)
				return false;
		} else if (!expiryYear.equals(other.expiryYear))
			return false;
		if (is3DSecure == null) {
			if (other.is3DSecure != null)
				return false;
		} else if (!is3DSecure.equals(other.is3DSecure))
			return false;
		if (type != other.type)
			return false;
		if (userInfo == null) {
			if (other.userInfo != null)
				return false;
		} else if (!userInfo.equals(other.userInfo))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CarnetCreditCardProviderData [cvv=" + cvv
				+ ", creditCardNumber=" + creditCardNumber + ", expiryMonth="
				+ expiryMonth + ", expiryYear=" + expiryYear + ", is3DSecure="
				+ is3DSecure + ", type=" + type + ", userInfo=" + userInfo
				+ ", comunication=" + comunication + "]";
	}
}