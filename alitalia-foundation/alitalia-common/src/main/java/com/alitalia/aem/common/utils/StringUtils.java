package com.alitalia.aem.common.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StringUtils {

    public static final String EMPTY = "";

    public static String nullToEmpty(String value) {
        if (value == null) {
            return EMPTY;
        } else {
            return value;
        }
    }

    public static String capitalize(final String words) {
        if (words == null) return EMPTY;
        String[] split = words.trim().split(" ");
        StringBuffer buf =  new StringBuffer();
        for (String word : split) {
            buf.append( capitalizeSingleWord(word)).append(" ");
        }
        return buf.toString().trim();
    }
    private static String capitalizeSingleWord(final String str) {
        int strLen;
        if (str == null || (strLen = str.length()) == 0) {
            return str;
        }

        String myStr = str.toLowerCase().trim();

        final int firstCodepoint = myStr.codePointAt(0);
        final int newCodePoint = Character.toTitleCase(firstCodepoint);
        if (firstCodepoint == newCodePoint) {
            // already capitalized
            return myStr;
        }

        final int newCodePoints[] = new int[strLen]; // cannot be longer than the char array
        int outOffset = 0;
        newCodePoints[outOffset++] = newCodePoint; // copy the first codepoint
        for (int inOffset = Character.charCount(firstCodepoint); inOffset < strLen; ) {
            final int codepoint = myStr.codePointAt(inOffset);
            newCodePoints[outOffset++] = codepoint; // copy the remaining ones
            inOffset += Character.charCount(codepoint);
        }
        return new String(newCodePoints, 0, outOffset);
    }
}
