package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.checkin.CheckinCountryData;
import com.alitalia.aem.common.messages.BaseResponse;

public class GetCountryResponse extends BaseResponse{

	private List<CheckinCountryData> countries;
	
	public GetCountryResponse(){}
	
	public GetCountryResponse(String tid, String sid) {
		super(tid, sid);
	}

	public List<CheckinCountryData> getCountries() {
		return countries;
	}

	public void setCountries(List<CheckinCountryData> countries) {
		this.countries = countries;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((countries == null) ? 0 : countries.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		GetCountryResponse other = (GetCountryResponse) obj;
		if (countries == null) {
			if (other.countries != null)
				return false;
		} else if (!countries.equals(other.countries))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveCountriesResponse [countries=" + countries + "]";
	}

	

}