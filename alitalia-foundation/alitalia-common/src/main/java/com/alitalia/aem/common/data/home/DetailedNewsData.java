package com.alitalia.aem.common.data.home;

import java.util.Calendar;

public class DetailedNewsData {

	private Calendar expiryDate;
	private Calendar lastEditDate;
	private boolean isDeleted;
	private String owner;
	private boolean isPubLive;
	private boolean isPubTest;
	private String resume;
	private String text;
	
	public Calendar getExpiryDate() {
		return expiryDate;
	}
	
	public void setExpiryDate(Calendar expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	public Calendar getLastEditDate() {
		return lastEditDate;
	}
	
	public void setLastEditDate(Calendar lastEditDate) {
		this.lastEditDate = lastEditDate;
	}
	
	public boolean isDeleted() {
		return isDeleted;
	}
	
	public void setIsDeleted(boolean deleted) {
		this.isDeleted = deleted;
	}
	
	public String getOwner() {
		return owner;
	}
	
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	public boolean isPubLive() {
		return isPubLive;
	}
	
	public void setIsPubLive(boolean pubLive) {
		this.isPubLive = pubLive;
	}
	
	public boolean isPubTest() {
		return isPubTest;
	}
	
	public void setIsPubTest(boolean pubTest) {
		this.isPubTest = pubTest;
	}
	
	public String getResume() {
		return resume;
	}
	
	public void setResume(String resume) {
		this.resume = resume;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (isDeleted ? 1231 : 1237);
		result = prime * result
				+ ((expiryDate == null) ? 0 : expiryDate.hashCode());
		result = prime * result
				+ ((lastEditDate == null) ? 0 : lastEditDate.hashCode());
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		result = prime * result + (isPubLive ? 1231 : 1237);
		result = prime * result + (isPubTest ? 1231 : 1237);
		result = prime * result + ((resume == null) ? 0 : resume.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DetailedNewsData other = (DetailedNewsData) obj;
		if (isDeleted != other.isDeleted)
			return false;
		if (expiryDate == null) {
			if (other.expiryDate != null)
				return false;
		} else if (!expiryDate.equals(other.expiryDate))
			return false;
		if (lastEditDate == null) {
			if (other.lastEditDate != null)
				return false;
		} else if (!lastEditDate.equals(other.lastEditDate))
			return false;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner))
			return false;
		if (isPubLive != other.isPubLive)
			return false;
		if (isPubTest != other.isPubTest)
			return false;
		if (resume == null) {
			if (other.resume != null)
				return false;
		} else if (!resume.equals(other.resume))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DetailedNewsData [expiryDate=" + expiryDate + ", lastEditDate="
				+ lastEditDate + ", deleted=" + isDeleted + ", owner=" + owner
				+ ", pubLive=" + isPubLive + ", pubTest=" + isPubTest + ", resume="
				+ resume + ", text=" + text + "]";
	}
}