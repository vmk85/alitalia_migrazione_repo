package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.enumerations.CanAddPassengerReasonEnum;
import com.alitalia.aem.common.messages.BaseResponse;

public class CanAddPassengerCheckinResponse extends BaseResponse {

	private CanAddPassengerReasonEnum canAddPassenger;

	public CanAddPassengerReasonEnum getCanAddPassenger() {
		return canAddPassenger;
	}

	public void setCanAddPassenger(CanAddPassengerReasonEnum canAddPassenger) {
		this.canAddPassenger = canAddPassenger;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((canAddPassenger == null) ? 0 : canAddPassenger.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CanAddPassengerCheckinResponse other = (CanAddPassengerCheckinResponse) obj;
		if (canAddPassenger != other.canAddPassenger)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CanAddPassengerCheckinResponse [canAddPassenger=" + canAddPassenger
				+ ", toString()=" + super.toString() + "]";
	}

}
