
package com.alitalia.aem.common.data.checkinrest.model.updatesecurityinfo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OperatingTypeAirlineInfo {

    @SerializedName("wetLeaseAirlineInfo")
    @Expose
    private WetLeaseAirlineInfo wetLeaseAirlineInfo;
    @SerializedName("commuterAirlineInfo")
    @Expose
    private CommuterAirlineInfo commuterAirlineInfo;

    public WetLeaseAirlineInfo getWetLeaseAirlineInfo() {
        return wetLeaseAirlineInfo;
    }

    public void setWetLeaseAirlineInfo(WetLeaseAirlineInfo wetLeaseAirlineInfo) {
        this.wetLeaseAirlineInfo = wetLeaseAirlineInfo;
    }

    public CommuterAirlineInfo getCommuterAirlineInfo() {
        return commuterAirlineInfo;
    }

    public void setCommuterAirlineInfo(CommuterAirlineInfo commuterAirlineInfo) {
        this.commuterAirlineInfo = commuterAirlineInfo;
    }

}
