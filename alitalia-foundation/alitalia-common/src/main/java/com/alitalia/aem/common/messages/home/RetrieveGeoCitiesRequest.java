package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class RetrieveGeoCitiesRequest extends BaseRequest {

    private String languageCode;
    private String marketCode;
    private String paxType;
    private Integer rngpryDwn;
    private Integer rngpryUp;
    private String target;
	
    public String getLanguageCode() {
		return languageCode;
	}
	
    public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	
    public String getMarketCode() {
		return marketCode;
	}
	
    public void setMarketCode(String marketCode) {
		this.marketCode = marketCode;
	}
	
    public String getPaxType() {
		return paxType;
	}
	
    public void setPaxType(String paxType) {
		this.paxType = paxType;
	}
	
    public Integer getRngpryDwn() {
		return rngpryDwn;
	}
	
    public void setRngpryDwn(Integer rngpryDwn) {
		this.rngpryDwn = rngpryDwn;
	}
	
    public Integer getRngpryUp() {
		return rngpryUp;
	}
	
    public void setRngpryUp(Integer rngpryUp) {
		this.rngpryUp = rngpryUp;
	}
	
    public String getTarget() {
		return target;
	}
	
    public void setTarget(String target) {
		this.target = target;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((languageCode == null) ? 0 : languageCode.hashCode());
		result = prime * result
				+ ((marketCode == null) ? 0 : marketCode.hashCode());
		result = prime * result + ((paxType == null) ? 0 : paxType.hashCode());
		result = prime * result
				+ ((rngpryDwn == null) ? 0 : rngpryDwn.hashCode());
		result = prime * result
				+ ((rngpryUp == null) ? 0 : rngpryUp.hashCode());
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveGeoCitiesRequest other = (RetrieveGeoCitiesRequest) obj;
		if (languageCode == null) {
			if (other.languageCode != null)
				return false;
		} else if (!languageCode.equals(other.languageCode))
			return false;
		if (marketCode == null) {
			if (other.marketCode != null)
				return false;
		} else if (!marketCode.equals(other.marketCode))
			return false;
		if (paxType == null) {
			if (other.paxType != null)
				return false;
		} else if (!paxType.equals(other.paxType))
			return false;
		if (rngpryDwn == null) {
			if (other.rngpryDwn != null)
				return false;
		} else if (!rngpryDwn.equals(other.rngpryDwn))
			return false;
		if (rngpryUp == null) {
			if (other.rngpryUp != null)
				return false;
		} else if (!rngpryUp.equals(other.rngpryUp))
			return false;
		if (target == null) {
			if (other.target != null)
				return false;
		} else if (!target.equals(other.target))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveGeoOffersRequest [languageCode=" + languageCode + ", marketCode="
				+ marketCode + ", paxType=" + paxType + ", rngpryDwn="
				+ rngpryDwn + ", rngpryUp=" + rngpryUp + ", target=" + target
				+ "]";
	}
}