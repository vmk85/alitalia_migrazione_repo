
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SectionInfo {

    @SerializedName("sectionName")
    @Expose
    private String sectionName;
    @SerializedName("sectionType")
    @Expose
    private String sectionType;
    @SerializedName("sectionText")
    @Expose
    private String sectionText;
    @SerializedName("subSectionInfo")
    @Expose
    private List<SubSectionInfo> subSectionInfo = null;

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public String getSectionType() {
        return sectionType;
    }

    public void setSectionType(String sectionType) {
        this.sectionType = sectionType;
    }

    public String getSectionText() {
        return sectionText;
    }

    public void setSectionText(String sectionText) {
        this.sectionText = sectionText;
    }

    public List<SubSectionInfo> getSubSectionInfo() {
        return subSectionInfo;
    }

    public void setSubSectionInfo(List<SubSectionInfo> subSectionInfo) {
        this.subSectionInfo = subSectionInfo;
    }

}
