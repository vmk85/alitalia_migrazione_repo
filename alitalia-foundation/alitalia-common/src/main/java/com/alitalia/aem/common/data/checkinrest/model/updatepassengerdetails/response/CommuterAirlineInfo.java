
package com.alitalia.aem.common.data.checkinrest.model.updatepassengerdetails.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommuterAirlineInfo {

    @SerializedName("commuterAirlineCode")
    @Expose
    private String commuterAirlineCode;
    @SerializedName("commuterAirlineName")
    @Expose
    private String commuterAirlineName;

    public String getCommuterAirlineCode() {
        return commuterAirlineCode;
    }

    public void setCommuterAirlineCode(String commuterAirlineCode) {
        this.commuterAirlineCode = commuterAirlineCode;
    }

    public String getCommuterAirlineName() {
        return commuterAirlineName;
    }

    public void setCommuterAirlineName(String commuterAirlineName) {
        this.commuterAirlineName = commuterAirlineName;
    }

}
