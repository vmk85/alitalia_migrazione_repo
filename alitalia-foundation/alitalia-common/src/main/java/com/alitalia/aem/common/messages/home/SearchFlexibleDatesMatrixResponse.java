package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.FlexibleDatesMatrixData;
import com.alitalia.aem.common.messages.BaseResponse;

public class SearchFlexibleDatesMatrixResponse extends BaseResponse {

	/**
	 * Holds possible flight solutions that satisfy input search criteria
	 * 
	 * @see FlexibleDatesMatrixData
	 */
	private List<FlexibleDatesMatrixData> flexibleDatesMatrixDataList;

	public List<FlexibleDatesMatrixData> getFlexibleDatesMatrixDataList() {
		return flexibleDatesMatrixDataList;
	}

	public void setFlexibleDatesMatrixDataList(
			List<FlexibleDatesMatrixData> flexibleDatesMatrixDataList) {
		this.flexibleDatesMatrixDataList = flexibleDatesMatrixDataList;
	}

	@Override
	public String toString() {
		return "SearchFlexibleDatesMatrixResponse [flexibleDatesMatrixDataList="
				+ flexibleDatesMatrixDataList + "]";
	}
}
