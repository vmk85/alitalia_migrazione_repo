
package com.alitalia.aem.common.data.checkinrest.model.cheangeseats.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SeatMapSector {

    @SerializedName("startingRow")
    @Expose
    private String startingRow;
    @SerializedName("endingRow")
    @Expose
    private String endingRow;
    @SerializedName("seatMapRows")
    @Expose
    private List<SeatMapRow> seatMapRows = null;

    public String getStartingRow() {
        return startingRow;
    }

    public void setStartingRow(String startingRow) {
        this.startingRow = startingRow;
    }

    public String getEndingRow() {
        return endingRow;
    }

    public void setEndingRow(String endingRow) {
        this.endingRow = endingRow;
    }

    public List<SeatMapRow> getSeatMapRows() {
        return seatMapRows;
    }

    public void setSeatMapRows(List<SeatMapRow> seatMapRows) {
        this.seatMapRows = seatMapRows;
    }

}
