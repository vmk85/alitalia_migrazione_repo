package com.alitalia.aem.common.data.home;

import java.util.Arrays;
import java.util.List;

public class MACustomerData {

    private List<MyFlightsDataModel> flights;

    private String address1_Address;
    private String address1_CompanyName;
    private String address1_Type ;
    private String address1_Zip;
    private String address1_CountryCode;
    private String address1_StateCode;
    private String address1_City;

    private String phone1AreaCode;
    private Long mobilePhone;
    private String phone1CountryCode;
    private String phone1Number;
    private String phone1Type;

    private String[] flightPref_FF;
    private String flightPref_Airport;
    private String flightPref_Meal;
    private String flightPref_Seat;
    private String flightPref_Family;

    private String generalPref_Channel;
    private String generalPref_Lang01;

    private String identityCard_CountryCode;
    private Long identityCard_ExpireOn;
    private String identityCard_Number;
    private Long identityCard_ValidFrom;

    private String passport_CountryCode;
    private Long passport_ExpireOn;
    private String passport_Number;
    private Long passport_ValidFrom;

    private String green_Number;
    private String green_CountryCode;
    private String green_NationalityCode;
    private Long green_ExpireOn;
    private Long green_BornDate;

    private String visa_Number;
    private Long visa_ValidFrom;
    private String visa_CountryCode;

    private boolean auth_Com;
    private boolean auth_Data;
    private boolean auth_Profile;
    private boolean smsAuthorization;
    private String newsletterType;
    private Boolean resetPwdSms;

    private String id_mm;

    private String secondoName;

    private boolean isFirstAccess;

    private String isStrong;

    public boolean isFirstAccess() {
        return isFirstAccess;
    }

    public void setFirstAccess(boolean firstAccess) {
        isFirstAccess = firstAccess;
    }

    public String getIsStrong() {
        return isStrong;
    }

    public void setIsStrong(String isStrong) {
        this.isStrong = isStrong;
    }

    //Tolentino - Inizio
    private MACustomerDataPayment payment;

    public MACustomerDataPayment getPayment() {
		return payment;
	}

	public void setPayment(MACustomerDataPayment payment) {
		this.payment = payment;
	}
    //Tolentino - Fine

    public Long getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(Long mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getPhone1CountryCode() {
        return phone1CountryCode;
    }

    public void setPhone1CountryCode(String phone1CountryCode) {
        this.phone1CountryCode = phone1CountryCode;
    }

    public String getPhone1Number() {
        return phone1Number;
    }

    public void setPhone1Number(String phone1Number) {
        this.phone1Number = phone1Number;
    }

    public String getPhone1Type() {
        return phone1Type;
    }

    public void setPhone1Type(String phone1Type) {
        this.phone1Type = phone1Type;
    }

    public void setIdentityCard_ExpireOn(Long identityCard_ExpireOn) {
        this.identityCard_ExpireOn = identityCard_ExpireOn;
    }

    public String getIdentityCard_Number() {
        return identityCard_Number;
    }

    public void setIdentityCard_Number(String identityCard_Number) {
        this.identityCard_Number = identityCard_Number;
    }

    public Long getIdentityCard_ValidFrom() {
        return identityCard_ValidFrom;
    }

    public void setIdentityCard_ValidFrom(Long identityCard_ValidFrom) {
        this.identityCard_ValidFrom = identityCard_ValidFrom;
    }

    public String getPassport_CountryCode() {
        return passport_CountryCode;
    }

    public void setPassport_CountryCode(String passport_CountryCode) {
        this.passport_CountryCode = passport_CountryCode;
    }

    public Long getPassport_ExpireOn() {
        return passport_ExpireOn;
    }

    public void setPassport_ExpireOn(Long passport_ExpireOn) {
        this.passport_ExpireOn = passport_ExpireOn;
    }

    public String getPassport_Number() {
        return passport_Number;
    }

    public void setPassport_Number(String passport_Number) {
        this.passport_Number = passport_Number;
    }

    public Long getPassport_ValidFrom() {
        return passport_ValidFrom;
    }

    public void setPassport_ValidFrom(Long passport_ValidFrom) {
        this.passport_ValidFrom = passport_ValidFrom;
    }

    public String getGreen_Number() {
        return green_Number;
    }

    public void setGreen_Number(String green_Number) {
        this.green_Number = green_Number;
    }

    public String getGreen_CountryCode() {
        return green_CountryCode;
    }

    public void setGreen_CountryCode(String green_CountryCode) {
        this.green_CountryCode = green_CountryCode;
    }

    public String getGreen_NationalityCode() {
        return green_NationalityCode;
    }

    public void setGreen_NationalityCode(String green_NationalityCode) {
        this.green_NationalityCode = green_NationalityCode;
    }

    public Long getGreen_ExpireOn() {
        return green_ExpireOn;
    }

    public void setGreen_ExpireOn(Long green_ExpireOn) {
        this.green_ExpireOn = green_ExpireOn;
    }

    public Long getGreen_BornDate() {
        return green_BornDate;
    }

    public void setGreen_BornDate(Long green_BornDate) {
        this.green_BornDate = green_BornDate;
    }

    public String getVisa_Number() {
        return visa_Number;
    }

    public void setVisa_Number(String visa_Number) {
        this.visa_Number = visa_Number;
    }

    public Long getVisa_ValidFrom() {
        return visa_ValidFrom;
    }

    public void setVisa_ValidFrom(Long visa_ValidFrom) {
        this.visa_ValidFrom = visa_ValidFrom;
    }

    public String getVisa_CountryCode() {
        return visa_CountryCode;
    }

    public void setVisa_CountryCode(String visa_CountryCode) {
        this.visa_CountryCode = visa_CountryCode;
    }

    public String getGeneralPref_Channel() {
        return generalPref_Channel;
    }

    public void setGeneralPref_Channel(String generalPref_Channel) {
        this.generalPref_Channel = generalPref_Channel;
    }

    public String getGeneralPref_Lang01() {
        return generalPref_Lang01;
    }

    public void setGeneralPref_Lang01(String generalPref_Lang01) {
        this.generalPref_Lang01 = generalPref_Lang01;
    }

    public String getIdentityCard_CountryCode() {
        return identityCard_CountryCode;
    }

    public void setIdentityCard_CountryCode(String identityCard_CountryCode) {
        this.identityCard_CountryCode = identityCard_CountryCode;
    }

    public Long getIdentityCard_ExpireOn() {
        return identityCard_ExpireOn;
    }

    public String[] getFlightPref_FF() {
        return flightPref_FF;
    }

    public void setFlightPref_FF(String[] flightPref_FF) {
        this.flightPref_FF = flightPref_FF;
    }

    public String getFlightPref_Airport() {
        return flightPref_Airport;
    }

    public void setFlightPref_Airport(String flightPref_Airport) {
        this.flightPref_Airport = flightPref_Airport;
    }

    public String getFlightPref_Meal() {
        return flightPref_Meal;
    }

    public void setFlightPref_Meal(String flightPref_Meal) {
        this.flightPref_Meal = flightPref_Meal;
    }

    public String getFlightPref_Seat() {
        return flightPref_Seat;
    }

    public void setFlightPref_Seat(String flightPref_Seat) {
        this.flightPref_Seat = flightPref_Seat;
    }

    public String getFlightPref_Family() {
        return flightPref_Family;
    }

    public void setFlightPref_Family(String flightPref_Family) {
        this.flightPref_Family = flightPref_Family;
    }

    public boolean isAuth_Data() {
        return auth_Data;
    }

    public void setAuth_Data(boolean auth_Data) {
        this.auth_Data = auth_Data;
    }

    public boolean isAuth_Profile() {
        return auth_Profile;
    }

    public void setAuth_Profile(boolean auth_Profile) {
        this.auth_Profile = auth_Profile;
    }

    public boolean getSmsAuthorization() {
        return smsAuthorization;
    }

    public void setSmsAuthorization(boolean smsAuthorization) {
        this.smsAuthorization = smsAuthorization;
    }

    public String getNewsletterType() {
        return newsletterType;
    }

    public void setNewsletterType(String newsletterType) {
        this.newsletterType = newsletterType;
    }

    public String getAddress1_Address() {
        return address1_Address;
    }

    public void setAddress1_Address(String address1_Address) {
        this.address1_Address = address1_Address;
    }

    public String getAddress1_CompanyName() {
        return address1_CompanyName;
    }

    public void setAddress1_CompanyName(String address1_CompanyName) {
        this.address1_CompanyName = address1_CompanyName;
    }

    public String getAddress1_Type() {
        return address1_Type;
    }

    public void setAddress1_Type(String address1_Type) {
        this.address1_Type = address1_Type;
    }

    public String getAddress1_Zip() {
        return address1_Zip;
    }

    public void setAddress1_Zip(String address1_Zip) {
        this.address1_Zip = address1_Zip;
    }

    public boolean isAuth_Com() {
        return auth_Com;
    }

    public void setAuth_Com(boolean auth_Com) {
        this.auth_Com = auth_Com;
    }

    public String getPhone1AreaCode() {
        return phone1AreaCode;
    }

    public void setPhone1AreaCode(String phone1AreaCode) {
        this.phone1AreaCode = phone1AreaCode;
    }

    public String getAddress1_CountryCode() {
        return address1_CountryCode;
    }

    public void setAddress1_CountryCode(String address1_CountryCode) {
        this.address1_CountryCode = address1_CountryCode;
    }

    public String getAddress1_City() {
        return address1_City;
    }

    public void setAddress1_City(String address1_City) {
        this.address1_City = address1_City;
    }

    public String getAddress1_StateCode() {
        return address1_StateCode;
    }

    public void setAddress1_StateCode(String address1_StateCode) {
        this.address1_StateCode = address1_StateCode;
    }

    public boolean isSmsAuthorization() {
        return smsAuthorization;
    }

    public Boolean getResetPwdSms() {
        return resetPwdSms;
    }

    public void setResetPwdSms(Boolean resetPwdSms) {
        this.resetPwdSms = resetPwdSms;
    }

    public String getId_mm() {
        return id_mm;
    }

    public void setId_mm(String id_mm) {
        this.id_mm = id_mm;
    }

    public String getSecondoName() {
        return secondoName;
    }

    public void setSecondoName(String secondoName) {
        this.secondoName = secondoName;
    }

    public List<MyFlightsDataModel> getFlights() {
        return flights;
    }

    public void setFlights(List<MyFlightsDataModel> flights) {
        this.flights = flights;
    }

    @Override
    public String toString() {
        return "MACustomerData{" +
                "flights=" + flights +
                ", address1_Address='" + address1_Address + '\'' +
                ", address1_CompanyName='" + address1_CompanyName + '\'' +
                ", address1_Type='" + address1_Type + '\'' +
                ", address1_Zip='" + address1_Zip + '\'' +
                ", address1_CountryCode='" + address1_CountryCode + '\'' +
                ", address1_StateCode='" + address1_StateCode + '\'' +
                ", address1_City='" + address1_City + '\'' +
                ", phone1AreaCode='" + phone1AreaCode + '\'' +
                ", mobilePhone=" + mobilePhone +
                ", phone1CountryCode='" + phone1CountryCode + '\'' +
                ", phone1Number='" + phone1Number + '\'' +
                ", phone1Type='" + phone1Type + '\'' +
                ", flightPref_FF=" + Arrays.toString(flightPref_FF) +
                ", flightPref_Airport='" + flightPref_Airport + '\'' +
                ", flightPref_Meal='" + flightPref_Meal + '\'' +
                ", flightPref_Seat='" + flightPref_Seat + '\'' +
                ", flightPref_Family='" + flightPref_Family + '\'' +
                ", generalPref_Channel='" + generalPref_Channel + '\'' +
                ", generalPref_Lang01='" + generalPref_Lang01 + '\'' +
                ", identityCard_CountryCode='" + identityCard_CountryCode + '\'' +
                ", identityCard_ExpireOn=" + identityCard_ExpireOn +
                ", identityCard_Number='" + identityCard_Number + '\'' +
                ", identityCard_ValidFrom=" + identityCard_ValidFrom +
                ", passport_CountryCode='" + passport_CountryCode + '\'' +
                ", passport_ExpireOn=" + passport_ExpireOn +
                ", passport_Number='" + passport_Number + '\'' +
                ", passport_ValidFrom=" + passport_ValidFrom +
                ", green_Number='" + green_Number + '\'' +
                ", green_CountryCode='" + green_CountryCode + '\'' +
                ", green_NationalityCode='" + green_NationalityCode + '\'' +
                ", green_ExpireOn=" + green_ExpireOn +
                ", green_BornDate=" + green_BornDate +
                ", visa_Number='" + visa_Number + '\'' +
                ", visa_ValidFrom=" + visa_ValidFrom +
                ", visa_CountryCode='" + visa_CountryCode + '\'' +
                ", auth_Com=" + auth_Com +
                ", auth_Data=" + auth_Data +
                ", auth_Profile=" + auth_Profile +
                ", smsAuthorization=" + smsAuthorization +
                ", newsletterType='" + newsletterType + '\'' +
                ", resetPwdSms=" + resetPwdSms +
                ", id_mm='" + id_mm + '\'' +
                ", secondoName='" + secondoName + '\'' +
                ", isFirstAccess=" + isFirstAccess +
                ", isStrong='" + isStrong + '\'' +
			    //Tolentino - Inizio
                ", payment='" + payment + '\'' +
			    //Tolentino - Fine
                '}';
    }
}
