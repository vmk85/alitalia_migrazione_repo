package com.alitalia.aem.common.data.home.checkin;

import java.util.List;

import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;

public class CheckinAncillaryCouponData {
	
    private Integer baggageAllowance;
    private String fareClass;
    private Integer flightId;
    private Integer flightNumber;
    private String inboundCouponNumber;
    private Boolean isChild;
    private Boolean isInfantAccompanist;
    private Boolean isInfant;
    private Boolean isInterline;
    private Boolean isSeatAssignDisabled;
    private Boolean isSeatAssignPayment;
    private Boolean isSpecialPassenger;
    private String number;
    private Integer passengerNumber;
    private String seatNumber;
    private List<MmbCompartimentalClassEnum> upgradeCompartimentalClasses;
    
	public Integer getBaggageAllowance() {
		return baggageAllowance;
	}
	public void setBaggageAllowance(Integer baggageAllowance) {
		this.baggageAllowance = baggageAllowance;
	}
	public String getFareClass() {
		return fareClass;
	}
	public void setFareClass(String fareClass) {
		this.fareClass = fareClass;
	}
	public Integer getFlightId() {
		return flightId;
	}
	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}
	public Integer getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(Integer flightNumber) {
		this.flightNumber = flightNumber;
	}
	public String getInboundCouponNumber() {
		return inboundCouponNumber;
	}
	public void setInboundCouponNumber(String inboundCouponNumber) {
		this.inboundCouponNumber = inboundCouponNumber;
	}
	public Boolean getIsChild() {
		return isChild;
	}
	public void setIsChild(Boolean isChild) {
		this.isChild = isChild;
	}
	public Boolean getIsInfantAccompanist() {
		return isInfantAccompanist;
	}
	public void setIsInfantAccompanist(Boolean isInfantAccompanist) {
		this.isInfantAccompanist = isInfantAccompanist;
	}
	public Boolean getIsInfant() {
		return isInfant;
	}
	public void setIsInfant(Boolean isInfant) {
		this.isInfant = isInfant;
	}
	public Boolean getIsInterline() {
		return isInterline;
	}
	public void setIsInterline(Boolean isInterline) {
		this.isInterline = isInterline;
	}
	public Boolean getIsSeatAssignDisabled() {
		return isSeatAssignDisabled;
	}
	public void setIsSeatAssignDisabled(Boolean isSeatAssignDisabled) {
		this.isSeatAssignDisabled = isSeatAssignDisabled;
	}
	public Boolean getIsSeatAssignPayment() {
		return isSeatAssignPayment;
	}
	public void setIsSeatAssignPayment(Boolean isSeatAssignPayment) {
		this.isSeatAssignPayment = isSeatAssignPayment;
	}
	public Boolean getIsSpecialPassenger() {
		return isSpecialPassenger;
	}
	public void setIsSpecialPassenger(Boolean isSpecialPassenger) {
		this.isSpecialPassenger = isSpecialPassenger;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public Integer getPassengerNumber() {
		return passengerNumber;
	}
	public void setPassengerNumber(Integer passengerNumber) {
		this.passengerNumber = passengerNumber;
	}
	public String getSeatNumber() {
		return seatNumber;
	}
	public void setSeatNumber(String seatNumber) {
		this.seatNumber = seatNumber;
	}
	public List<MmbCompartimentalClassEnum> getUpgradeCompartimentalClasses() {
		return upgradeCompartimentalClasses;
	}
	public void setUpgradeCompartimentalClasses(
			List<MmbCompartimentalClassEnum> upgradeCompartimentalClasses) {
		this.upgradeCompartimentalClasses = upgradeCompartimentalClasses;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((baggageAllowance == null) ? 0 : baggageAllowance.hashCode());
		result = prime * result
				+ ((fareClass == null) ? 0 : fareClass.hashCode());
		result = prime * result
				+ ((flightId == null) ? 0 : flightId.hashCode());
		result = prime * result
				+ ((flightNumber == null) ? 0 : flightNumber.hashCode());
		result = prime
				* result
				+ ((inboundCouponNumber == null) ? 0 : inboundCouponNumber
						.hashCode());
		result = prime * result + ((isChild == null) ? 0 : isChild.hashCode());
		result = prime * result
				+ ((isInfant == null) ? 0 : isInfant.hashCode());
		result = prime
				* result
				+ ((isInfantAccompanist == null) ? 0 : isInfantAccompanist
						.hashCode());
		result = prime * result
				+ ((isInterline == null) ? 0 : isInterline.hashCode());
		result = prime
				* result
				+ ((isSeatAssignDisabled == null) ? 0 : isSeatAssignDisabled
						.hashCode());
		result = prime
				* result
				+ ((isSeatAssignPayment == null) ? 0 : isSeatAssignPayment
						.hashCode());
		result = prime
				* result
				+ ((isSpecialPassenger == null) ? 0 : isSpecialPassenger
						.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result
				+ ((passengerNumber == null) ? 0 : passengerNumber.hashCode());
		result = prime * result
				+ ((seatNumber == null) ? 0 : seatNumber.hashCode());
		result = prime
				* result
				+ ((upgradeCompartimentalClasses == null) ? 0
						: upgradeCompartimentalClasses.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinAncillaryCouponData other = (CheckinAncillaryCouponData) obj;
		if (baggageAllowance == null) {
			if (other.baggageAllowance != null)
				return false;
		} else if (!baggageAllowance.equals(other.baggageAllowance))
			return false;
		if (fareClass == null) {
			if (other.fareClass != null)
				return false;
		} else if (!fareClass.equals(other.fareClass))
			return false;
		if (flightId == null) {
			if (other.flightId != null)
				return false;
		} else if (!flightId.equals(other.flightId))
			return false;
		if (flightNumber == null) {
			if (other.flightNumber != null)
				return false;
		} else if (!flightNumber.equals(other.flightNumber))
			return false;
		if (inboundCouponNumber == null) {
			if (other.inboundCouponNumber != null)
				return false;
		} else if (!inboundCouponNumber.equals(other.inboundCouponNumber))
			return false;
		if (isChild == null) {
			if (other.isChild != null)
				return false;
		} else if (!isChild.equals(other.isChild))
			return false;
		if (isInfant == null) {
			if (other.isInfant != null)
				return false;
		} else if (!isInfant.equals(other.isInfant))
			return false;
		if (isInfantAccompanist == null) {
			if (other.isInfantAccompanist != null)
				return false;
		} else if (!isInfantAccompanist.equals(other.isInfantAccompanist))
			return false;
		if (isInterline == null) {
			if (other.isInterline != null)
				return false;
		} else if (!isInterline.equals(other.isInterline))
			return false;
		if (isSeatAssignDisabled == null) {
			if (other.isSeatAssignDisabled != null)
				return false;
		} else if (!isSeatAssignDisabled.equals(other.isSeatAssignDisabled))
			return false;
		if (isSeatAssignPayment == null) {
			if (other.isSeatAssignPayment != null)
				return false;
		} else if (!isSeatAssignPayment.equals(other.isSeatAssignPayment))
			return false;
		if (isSpecialPassenger == null) {
			if (other.isSpecialPassenger != null)
				return false;
		} else if (!isSpecialPassenger.equals(other.isSpecialPassenger))
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (passengerNumber == null) {
			if (other.passengerNumber != null)
				return false;
		} else if (!passengerNumber.equals(other.passengerNumber))
			return false;
		if (seatNumber == null) {
			if (other.seatNumber != null)
				return false;
		} else if (!seatNumber.equals(other.seatNumber))
			return false;
		if (upgradeCompartimentalClasses == null) {
			if (other.upgradeCompartimentalClasses != null)
				return false;
		} else if (!upgradeCompartimentalClasses
				.equals(other.upgradeCompartimentalClasses))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CheckinAncillaryCouponData [baggageAllowance="
				+ baggageAllowance + ", fareClass=" + fareClass + ", flightId="
				+ flightId + ", flightNumber=" + flightNumber
				+ ", inboundCouponNumber=" + inboundCouponNumber + ", isChild="
				+ isChild + ", isInfantAccompanist=" + isInfantAccompanist
				+ ", isInfant=" + isInfant + ", isInterline=" + isInterline
				+ ", isSeatAssignDisabled=" + isSeatAssignDisabled
				+ ", isSeatAssignPayment=" + isSeatAssignPayment
				+ ", isSpecialPassenger=" + isSpecialPassenger + ", number="
				+ number + ", passengerNumber=" + passengerNumber
				+ ", seatNumber=" + seatNumber
				+ ", upgradeCompartimentalClasses="
				+ upgradeCompartimentalClasses + "]";
	}

}