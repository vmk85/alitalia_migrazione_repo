package com.alitalia.aem.common.data.home;

import java.util.Calendar;

public class NewsData {

	private Calendar dataPub;
	private String tipologia;
	private String titolo;
	private Integer id;
	
	public Calendar getDataPub() {
		return dataPub;
	}
	
	public void setDataPub(Calendar dataPub) {
		this.dataPub = dataPub;
	}
	
	public String getTipologia() {
		return tipologia;
	}
	
	public void setTipologia(String tipologia) {
		this.tipologia = tipologia;
	}
	
	public String getTitolo() {
		return titolo;
	}
	
	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsData other = (NewsData) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NewsData [dataPub=" + dataPub + ", tipologia=" + tipologia
				+ ", titolo=" + titolo + ", id=" + id + "]";
	}
}