package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.PhonePrefixData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetrievePhonePrefixResponse extends BaseResponse {

	private List<PhonePrefixData> phonePrefix;

	public List<PhonePrefixData> getPhonePrefix() {
		return phonePrefix;
	}

	public void setPhonePrefix(List<PhonePrefixData> phonePrefix) {
		this.phonePrefix = phonePrefix;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((phonePrefix == null) ? 0 : phonePrefix.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrievePhonePrefixResponse other = (RetrievePhonePrefixResponse) obj;
		if (phonePrefix == null) {
			if (other.phonePrefix != null)
				return false;
		} else if (!phonePrefix.equals(other.phonePrefix))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrievePhonePrefixResponse [phonePrefix=" + phonePrefix + "]";
	}
	
}
