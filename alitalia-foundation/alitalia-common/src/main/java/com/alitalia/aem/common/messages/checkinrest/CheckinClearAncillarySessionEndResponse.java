package com.alitalia.aem.common.messages.checkinrest;

import java.util.List;

import com.alitalia.aem.common.messages.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckinClearAncillarySessionEndResponse extends BaseResponse {

	@SerializedName("outcome")
	@Expose
	private List<String> outcome = null;
	@SerializedName("conversationID")
	@Expose
	private String conversationID;
	
	private String error;
	
	public CheckinClearAncillarySessionEndResponse() {}
	
	public CheckinClearAncillarySessionEndResponse(String tid, String sid) {
		super(tid, sid);
	}

	public List<String> getOutcome() {
		return outcome;
	}

	public void setOutcome(List<String> outcome) {
		this.outcome = outcome;
	}

	public String getConversationID() {
		return conversationID;
	}

	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
}
