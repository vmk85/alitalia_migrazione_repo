package com.alitalia.aem.common.data.home;

import java.math.BigDecimal;
import java.util.Calendar;

public class FlexibleDatesMatrixData {
 
	private String idField;
	private Calendar departureDateTime;
    private Calendar returnDateTime;
	private BigDecimal amount;
    private String currency;
    
	public String getIdField() {
		return idField;
	}
	public void setIdField(String idField) {
		this.idField = idField;
	}
	public Calendar getDepartureDateTime() {
		return departureDateTime;
	}
	public void setDepartureDateTime(Calendar departureDateTime) {
		this.departureDateTime = departureDateTime;
	}
	public Calendar getReturnDateTime() {
		return returnDateTime;
	}
	public void setReturnDateTime(Calendar returnDateTime) {
		this.returnDateTime = returnDateTime;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result
				+ ((currency == null) ? 0 : currency.hashCode());
		result = prime
				* result
				+ ((departureDateTime == null) ? 0 : departureDateTime
						.hashCode());
		result = prime * result + ((idField == null) ? 0 : idField.hashCode());
		result = prime * result
				+ ((returnDateTime == null) ? 0 : returnDateTime.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FlexibleDatesMatrixData other = (FlexibleDatesMatrixData) obj;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (departureDateTime == null) {
			if (other.departureDateTime != null)
				return false;
		} else if (!departureDateTime.equals(other.departureDateTime))
			return false;
		if (idField == null) {
			if (other.idField != null)
				return false;
		} else if (!idField.equals(other.idField))
			return false;
		if (returnDateTime == null) {
			if (other.returnDateTime != null)
				return false;
		} else if (!returnDateTime.equals(other.returnDateTime))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "FlexibleDatesMatrixData [idField=" + idField
				+ ", departureDateTime=" + departureDateTime
				+ ", returnDateTime=" + returnDateTime + ", amount=" + amount
				+ ", currency=" + currency + "]";
	}
}