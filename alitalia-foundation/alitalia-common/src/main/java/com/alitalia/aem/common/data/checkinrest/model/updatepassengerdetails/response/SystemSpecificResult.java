
package com.alitalia.aem.common.data.checkinrest.model.updatepassengerdetails.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SystemSpecificResult {

    @SerializedName("errorMessage")
    @Expose
    private ErrorMessage errorMessage;
    @SerializedName("shortText")
    @Expose
    private String shortText;
    @SerializedName("element")
    @Expose
    private String element;
    @SerializedName("recordID")
    @Expose
    private String recordID;
    @SerializedName("docURL")
    @Expose
    private String docURL;
    @SerializedName("diagnosticResults")
    @Expose
    private DiagnosticResults diagnosticResults;
    @SerializedName("serviceName")
    @Expose
    private String serviceName;

    public ErrorMessage getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(ErrorMessage errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public String getRecordID() {
        return recordID;
    }

    public void setRecordID(String recordID) {
        this.recordID = recordID;
    }

    public String getDocURL() {
        return docURL;
    }

    public void setDocURL(String docURL) {
        this.docURL = docURL;
    }

    public DiagnosticResults getDiagnosticResults() {
        return diagnosticResults;
    }

    public void setDiagnosticResults(DiagnosticResults diagnosticResults) {
        this.diagnosticResults = diagnosticResults;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

}
