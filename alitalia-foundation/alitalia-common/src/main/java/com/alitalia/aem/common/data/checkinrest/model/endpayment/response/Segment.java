
package com.alitalia.aem.common.data.checkinrest.model.endpayment.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Segment {

    @SerializedName("segmentID")
    @Expose
    private String segmentID;
    @SerializedName("sequence")
    @Expose
    private String sequence;
    @SerializedName("airlineCode")
    @Expose
    private String airlineCode;
    @SerializedName("flightNumber")
    @Expose
    private String flightNumber;
    @SerializedName("classOfService")
    @Expose
    private String classOfService;
    @SerializedName("departureDate")
    @Expose
    private String departureDate;
    @SerializedName("arrivalDate")
    @Expose
    private String arrivalDate;
    @SerializedName("boardPoint")
    @Expose
    private String boardPoint;
    @SerializedName("offPoint")
    @Expose
    private String offPoint;
    @SerializedName("carrierCode")
    @Expose
    private String carrierCode;
    @SerializedName("emdNumber")
    @Expose
    private String emdNumber;
    @SerializedName("emdCoupon")
    @Expose
    private String emdCoupon;
    @SerializedName("eTicketNumber")
    @Expose
    private String eTicketNumber;
    @SerializedName("eTicketCoupon")
    @Expose
    private String eTicketCoupon;
    @SerializedName("marketingCarrier")
    @Expose
    private String marketingCarrier;
    @SerializedName("operatingCarrier")
    @Expose
    private String operatingCarrier;
    @SerializedName("aZmarriageGrp")
    @Expose
    private AZmarriageGrp aZmarriageGrp;

    public String getSegmentID() {
        return segmentID;
    }

    public void setSegmentID(String segmentID) {
        this.segmentID = segmentID;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getClassOfService() {
        return classOfService;
    }

    public void setClassOfService(String classOfService) {
        this.classOfService = classOfService;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getBoardPoint() {
        return boardPoint;
    }

    public void setBoardPoint(String boardPoint) {
        this.boardPoint = boardPoint;
    }

    public String getOffPoint() {
        return offPoint;
    }

    public void setOffPoint(String offPoint) {
        this.offPoint = offPoint;
    }

    public String getCarrierCode() {
        return carrierCode;
    }

    public void setCarrierCode(String carrierCode) {
        this.carrierCode = carrierCode;
    }

    public String getEmdNumber() {
        return emdNumber;
    }

    public void setEmdNumber(String emdNumber) {
        this.emdNumber = emdNumber;
    }

    public String getEmdCoupon() {
        return emdCoupon;
    }

    public void setEmdCoupon(String emdCoupon) {
        this.emdCoupon = emdCoupon;
    }

    public String getETicketNumber() {
        return eTicketNumber;
    }

    public void setETicketNumber(String eTicketNumber) {
        this.eTicketNumber = eTicketNumber;
    }

    public String getETicketCoupon() {
        return eTicketCoupon;
    }

    public void setETicketCoupon(String eTicketCoupon) {
        this.eTicketCoupon = eTicketCoupon;
    }

    public String getMarketingCarrier() {
        return marketingCarrier;
    }

    public void setMarketingCarrier(String marketingCarrier) {
        this.marketingCarrier = marketingCarrier;
    }

    public String getOperatingCarrier() {
        return operatingCarrier;
    }

    public void setOperatingCarrier(String operatingCarrier) {
        this.operatingCarrier = operatingCarrier;
    }

    public AZmarriageGrp getAZmarriageGrp() {
        return aZmarriageGrp;
    }

    public void setAZmarriageGrp(AZmarriageGrp aZmarriageGrp) {
        this.aZmarriageGrp = aZmarriageGrp;
    }

}
