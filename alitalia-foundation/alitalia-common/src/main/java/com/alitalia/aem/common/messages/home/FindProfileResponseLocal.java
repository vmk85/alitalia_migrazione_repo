package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class FindProfileResponseLocal extends BaseResponse {

	private Boolean flagIDProfiloExists;
	private String idProfilo;


	public Boolean getFlagIDProfiloExists() {
		return flagIDProfiloExists;
	}

	public void setFlagIDProfiloExists(Boolean flagIDProfiloExists) {
		this.flagIDProfiloExists = flagIDProfiloExists;
	}

	public String getIdProfilo() {
		return idProfilo;
	}

	public void setIdProfilo(String idProfilo) {
		this.idProfilo = idProfilo;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;

		FindProfileResponseLocal that = (FindProfileResponseLocal) o;

		if (flagIDProfiloExists != null ? !flagIDProfiloExists.equals(that.flagIDProfiloExists) : that.flagIDProfiloExists != null)
			return false;
		return idProfilo != null ? idProfilo.equals(that.idProfilo) : that.idProfilo == null;
	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (flagIDProfiloExists != null ? flagIDProfiloExists.hashCode() : 0);
		result = 31 * result + (idProfilo != null ? idProfilo.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "FindProfileResponseLocal{" +
				"flagIDProfiloExists=" + flagIDProfiloExists +
				", idProfilo='" + idProfilo + '\'' +
				'}';
	}
}