package com.alitalia.aem.common.data.home.mmb.ancillary;

import com.alitalia.aem.common.data.MmbAncillaryErrorData;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryShoppingCartServiceErrorEnum;

public class MmbAncillaryServiceClientErrorData extends MmbAncillaryErrorData {

	public MmbAncillaryServiceClientErrorData() {
		super();
		this.type = MmbAncillaryShoppingCartServiceErrorEnum.SERVICE_CLIENT_ERROR;
	}

}
