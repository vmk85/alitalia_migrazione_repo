package com.alitalia.aem.common.data.home.enumerations;

public enum ResidencyTypeEnum {

    NONE("None"),
    SARDINIA("Sardinia"),
    CROTONE("Crotone"),
    SICILY("Sicily");
    private final String value;

    ResidencyTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ResidencyTypeEnum fromValue(String v) {
        for (ResidencyTypeEnum c: ResidencyTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
