package com.alitalia.aem.common.data.home.enumerations;

public enum AddressFamilyEnum {

	UNKNOWN("Unknown"),
	UNSPECIFIED("Unspecified"),
	UNIX("Unix"),
	INTER_NETWORK("InterNetwork"),
	IMP_LINK("ImpLink"),
	PUP("Pup"),
	CHAOS("Chaos"),
	NS("NS"),
	IPX("Ipx"),
	ISO("Iso"),
	OSI("Osi"),
	ECMA("Ecma"),
	DATA_KIT("DataKit"),
	CCITT("Ccitt"),
	SNA("Sna"),
	DEC_NET("DecNet"),
	DATA_LINK("DataLink"),
	LAT("Lat"),
	HYPER_CHANNEL("HyperChannel"),
	APPLE_TALK("AppleTalk"),
	NET_BIOS("NetBios"),
	VOICE_VIEW("VoiceView"),
	FIRE_FOX("FireFox"),
	BANYAN("Banyan"),
	ATM("Atm"),
	INTER_NETWORK_V_6("InterNetworkV6"),
	CLUSTER("Cluster"),
	IEEE_12844("Ieee12844"),
	IRDA("Irda"),
	NETWORK_DESIGNERS("NetworkDesigners"),
	MAX("Max");

	private final String value;

	AddressFamilyEnum(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	public static AddressFamilyEnum fromValue(String v) {
		for (AddressFamilyEnum c: AddressFamilyEnum.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}
