package com.alitalia.aem.common.data.home;

import java.util.List;

public class ResultTicketingDetailSolutionPricingData {

    private List<ResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowanceData> carryOnBaggageAllowanceField;
    private List<ResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowanceData> checkedBaggageAllowanceField;
    private ResultTicketingDetailSolutionPricingDisplayFareTotalData displayFareTotalField;
    private ResultTicketingDetailSolutionPricingDisplayPriceData displayPriceField;
    private ResultTicketingDetailSolutionPricingDisplayTaxTotalData displayTaxTotalField;
    private ResultTicketingDetailSolutionPricingExtData extField;
    private List<ResultTicketingDetailSolutionPricingFareCalculationData> fareCalculationField;
    private List<ResultTicketingDetailSolutionPricingFareData> fareField;
    private List<ResultTicketingDetailSolutionPricingPassengerData> passengerField;
    private String ptcField;
    private ResultTicketingDetailSolutionPricingSaleFareTotalData saleFareTotalField;
    private ResultTicketingDetailSolutionPricingSalePriceData salePriceField;
    private ResultTicketingDetailSolutionPricingSaleTaxTotalData saleTaxTotalField;
    private List<ResultTicketingDetailSolutionPricingTaxData> taxField;

    public List<ResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowanceData> getCarryOnBaggageAllowanceField() {
        return carryOnBaggageAllowanceField;
    }

    public void setCarryOnBaggageAllowanceField(List<ResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowanceData> value) {
        this.carryOnBaggageAllowanceField = value;
    }

    public List<ResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowanceData> getCheckedBaggageAllowanceField() {
        return checkedBaggageAllowanceField;
    }

    public void setCheckedBaggageAllowanceField(List<ResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowanceData> value) {
        this.checkedBaggageAllowanceField = value;
    }

    public ResultTicketingDetailSolutionPricingDisplayFareTotalData getDisplayFareTotalField() {
        return displayFareTotalField;
    }

    public void setDisplayFareTotalField(ResultTicketingDetailSolutionPricingDisplayFareTotalData value) {
        this.displayFareTotalField = value;
    }

    public ResultTicketingDetailSolutionPricingDisplayPriceData getDisplayPriceField() {
        return displayPriceField;
    }

    public void setDisplayPriceField(ResultTicketingDetailSolutionPricingDisplayPriceData value) {
        this.displayPriceField = value;
    }

    public ResultTicketingDetailSolutionPricingDisplayTaxTotalData getDisplayTaxTotalField() {
        return displayTaxTotalField;
    }

    public void setDisplayTaxTotalField(ResultTicketingDetailSolutionPricingDisplayTaxTotalData value) {
        this.displayTaxTotalField = value;
    }

    public ResultTicketingDetailSolutionPricingExtData getExtField() {
        return extField;
    }

    public void setExtField(ResultTicketingDetailSolutionPricingExtData value) {
        this.extField = value;
    }

    public List<ResultTicketingDetailSolutionPricingFareCalculationData> getFareCalculationField() {
        return fareCalculationField;
    }

    public void setFareCalculationField(List<ResultTicketingDetailSolutionPricingFareCalculationData> value) {
        this.fareCalculationField = value;
    }

    public List<ResultTicketingDetailSolutionPricingFareData> getFareField() {
        return fareField;
    }

    public void setFareField(List<ResultTicketingDetailSolutionPricingFareData> value) {
        this.fareField = value;
    }

    public List<ResultTicketingDetailSolutionPricingPassengerData> getPassengerField() {
        return passengerField;
    }

    public void setPassengerField(List<ResultTicketingDetailSolutionPricingPassengerData> value) {
        this.passengerField = value;
    }

    public String getPtcField() {
        return ptcField;
    }

    public void setPtcField(String value) {
        this.ptcField = value;
    }

    public ResultTicketingDetailSolutionPricingSaleFareTotalData getSaleFareTotalField() {
        return saleFareTotalField;
    }

    public void setSaleFareTotalField(ResultTicketingDetailSolutionPricingSaleFareTotalData value) {
        this.saleFareTotalField = value;
    }

    public ResultTicketingDetailSolutionPricingSalePriceData getSalePriceField() {
        return salePriceField;
    }

    public void setSalePriceField(ResultTicketingDetailSolutionPricingSalePriceData value) {
        this.salePriceField = value;
    }

    public ResultTicketingDetailSolutionPricingSaleTaxTotalData getSaleTaxTotalField() {
        return saleTaxTotalField;
    }

    public void setSaleTaxTotalField(ResultTicketingDetailSolutionPricingSaleTaxTotalData value) {
        this.saleTaxTotalField = value;
    }

    public List<ResultTicketingDetailSolutionPricingTaxData> getTaxField() {
        return taxField;
    }

    public void setTaxField(List<ResultTicketingDetailSolutionPricingTaxData> value) {
        this.taxField = value;
    }

}
