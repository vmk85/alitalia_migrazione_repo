package com.alitalia.aem.common.data.home;

import com.alitalia.aem.common.data.home.enumerations.ContactTypeEnum;

public class ContactData {

	private ContactTypeEnum contactType;
    private String phoneNumber;
    private PhonePrefixData prefix;
	
    public ContactData() {
		super();
	}

    public ContactData(ContactData clone) {
		super();
		this.contactType = clone.getContactType();
		this.phoneNumber = clone.getPhoneNumber();
		this.prefix = clone.getPrefix();
	}

	public ContactTypeEnum getContactType() {
		return contactType;
	}
    
	public void setContactType(ContactTypeEnum contactType) {
		this.contactType = contactType;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public PhonePrefixData getPrefix() {
		return prefix;
	}
	
	public void setPrefix(PhonePrefixData prefix) {
		this.prefix = prefix;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((contactType == null) ? 0 : contactType.hashCode());
		result = prime * result
				+ ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		result = prime * result + ((prefix == null) ? 0 : prefix.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContactData other = (ContactData) obj;
		if (contactType != other.contactType)
			return false;
		if (phoneNumber == null) {
			if (other.phoneNumber != null)
				return false;
		} else if (!phoneNumber.equals(other.phoneNumber))
			return false;
		if (prefix == null) {
			if (other.prefix != null)
				return false;
		} else if (!prefix.equals(other.prefix))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ContactData [contactType=" + contactType + ", phoneNumber="
				+ phoneNumber + ", prefix=" + prefix + "]";
	}
}