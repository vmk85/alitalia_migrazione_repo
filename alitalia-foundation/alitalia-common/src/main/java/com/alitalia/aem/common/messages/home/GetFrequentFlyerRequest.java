package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class GetFrequentFlyerRequest extends BaseRequest {

	private String language;
	private String market;
	private String conversationID;
	
	public GetFrequentFlyerRequest(String tid, String sid) {
		super(tid, sid);
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public String getConversationID() {
		return conversationID;
	}

	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}

	
}
