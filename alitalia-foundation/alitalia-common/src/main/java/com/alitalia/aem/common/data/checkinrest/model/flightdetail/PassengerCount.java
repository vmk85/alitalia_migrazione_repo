
package com.alitalia.aem.common.data.checkinrest.model.flightdetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PassengerCount {

    @SerializedName("classOfService")
    @Expose
    private String classOfService;
    @SerializedName("authorized")
    @Expose
    private Long authorized;
    @SerializedName("authorizedSpecified")
    @Expose
    private Boolean authorizedSpecified;
    @SerializedName("booked")
    @Expose
    private Long booked;
    @SerializedName("bookedSpecified")
    @Expose
    private Boolean bookedSpecified;
    @SerializedName("available")
    @Expose
    private Long available;
    @SerializedName("availableSpecified")
    @Expose
    private Boolean availableSpecified;
    @SerializedName("thru")
    @Expose
    private Long thru;
    @SerializedName("thruSpecified")
    @Expose
    private Boolean thruSpecified;
    @SerializedName("local")
    @Expose
    private Long local;
    @SerializedName("localSpecified")
    @Expose
    private Boolean localSpecified;
    @SerializedName("nonRevenueLocal")
    @Expose
    private Long nonRevenueLocal;
    @SerializedName("nonRevenueLocalSpecified")
    @Expose
    private Boolean nonRevenueLocalSpecified;
    @SerializedName("nonRevenueThru")
    @Expose
    private Long nonRevenueThru;
    @SerializedName("nonRevenueThruSpecified")
    @Expose
    private Boolean nonRevenueThruSpecified;
    @SerializedName("paperTicket")
    @Expose
    private Long paperTicket;
    @SerializedName("paperTicketSpecified")
    @Expose
    private Boolean paperTicketSpecified;
    @SerializedName("electronicTicket")
    @Expose
    private Long electronicTicket;
    @SerializedName("electronicTicketSpecified")
    @Expose
    private Boolean electronicTicketSpecified;
    @SerializedName("kiosk")
    @Expose
    private Long kiosk;
    @SerializedName("kioskSpecified")
    @Expose
    private Boolean kioskSpecified;
    @SerializedName("standbyRestriction")
    @Expose
    private String standbyRestriction;
    @SerializedName("meals")
    @Expose
    private Long meals;
    @SerializedName("mealsSpecified")
    @Expose
    private Boolean mealsSpecified;
    @SerializedName("specialMeals")
    @Expose
    private Long specialMeals;
    @SerializedName("specialMealsSpecified")
    @Expose
    private Boolean specialMealsSpecified;
    @SerializedName("setups")
    @Expose
    private Long setups;
    @SerializedName("setupsSpecified")
    @Expose
    private Boolean setupsSpecified;
    @SerializedName("localOnBoard")
    @Expose
    private Long localOnBoard;
    @SerializedName("localOnBoardSpecified")
    @Expose
    private Boolean localOnBoardSpecified;
    @SerializedName("totalOnBoard")
    @Expose
    private Long totalOnBoard;
    @SerializedName("totalOnBoardSpecified")
    @Expose
    private Boolean totalOnBoardSpecified;
    @SerializedName("totalBoardingPassIssued")
    @Expose
    private Long totalBoardingPassIssued;
    @SerializedName("totalBoardingPassIssuedSpecified")
    @Expose
    private Boolean totalBoardingPassIssuedSpecified;

    public String getClassOfService() {
        return classOfService;
    }

    public void setClassOfService(String classOfService) {
        this.classOfService = classOfService;
    }

    public Long getAuthorized() {
        return authorized;
    }

    public void setAuthorized(Long authorized) {
        this.authorized = authorized;
    }

    public Boolean getAuthorizedSpecified() {
        return authorizedSpecified;
    }

    public void setAuthorizedSpecified(Boolean authorizedSpecified) {
        this.authorizedSpecified = authorizedSpecified;
    }

    public Long getBooked() {
        return booked;
    }

    public void setBooked(Long booked) {
        this.booked = booked;
    }

    public Boolean getBookedSpecified() {
        return bookedSpecified;
    }

    public void setBookedSpecified(Boolean bookedSpecified) {
        this.bookedSpecified = bookedSpecified;
    }

    public Long getAvailable() {
        return available;
    }

    public void setAvailable(Long available) {
        this.available = available;
    }

    public Boolean getAvailableSpecified() {
        return availableSpecified;
    }

    public void setAvailableSpecified(Boolean availableSpecified) {
        this.availableSpecified = availableSpecified;
    }

    public Long getThru() {
        return thru;
    }

    public void setThru(Long thru) {
        this.thru = thru;
    }

    public Boolean getThruSpecified() {
        return thruSpecified;
    }

    public void setThruSpecified(Boolean thruSpecified) {
        this.thruSpecified = thruSpecified;
    }

    public Long getLocal() {
        return local;
    }

    public void setLocal(Long local) {
        this.local = local;
    }

    public Boolean getLocalSpecified() {
        return localSpecified;
    }

    public void setLocalSpecified(Boolean localSpecified) {
        this.localSpecified = localSpecified;
    }

    public Long getNonRevenueLocal() {
        return nonRevenueLocal;
    }

    public void setNonRevenueLocal(Long nonRevenueLocal) {
        this.nonRevenueLocal = nonRevenueLocal;
    }

    public Boolean getNonRevenueLocalSpecified() {
        return nonRevenueLocalSpecified;
    }

    public void setNonRevenueLocalSpecified(Boolean nonRevenueLocalSpecified) {
        this.nonRevenueLocalSpecified = nonRevenueLocalSpecified;
    }

    public Long getNonRevenueThru() {
        return nonRevenueThru;
    }

    public void setNonRevenueThru(Long nonRevenueThru) {
        this.nonRevenueThru = nonRevenueThru;
    }

    public Boolean getNonRevenueThruSpecified() {
        return nonRevenueThruSpecified;
    }

    public void setNonRevenueThruSpecified(Boolean nonRevenueThruSpecified) {
        this.nonRevenueThruSpecified = nonRevenueThruSpecified;
    }

    public Long getPaperTicket() {
        return paperTicket;
    }

    public void setPaperTicket(Long paperTicket) {
        this.paperTicket = paperTicket;
    }

    public Boolean getPaperTicketSpecified() {
        return paperTicketSpecified;
    }

    public void setPaperTicketSpecified(Boolean paperTicketSpecified) {
        this.paperTicketSpecified = paperTicketSpecified;
    }

    public Long getElectronicTicket() {
        return electronicTicket;
    }

    public void setElectronicTicket(Long electronicTicket) {
        this.electronicTicket = electronicTicket;
    }

    public Boolean getElectronicTicketSpecified() {
        return electronicTicketSpecified;
    }

    public void setElectronicTicketSpecified(Boolean electronicTicketSpecified) {
        this.electronicTicketSpecified = electronicTicketSpecified;
    }

    public Long getKiosk() {
        return kiosk;
    }

    public void setKiosk(Long kiosk) {
        this.kiosk = kiosk;
    }

    public Boolean getKioskSpecified() {
        return kioskSpecified;
    }

    public void setKioskSpecified(Boolean kioskSpecified) {
        this.kioskSpecified = kioskSpecified;
    }

    public String getStandbyRestriction() {
        return standbyRestriction;
    }

    public void setStandbyRestriction(String standbyRestriction) {
        this.standbyRestriction = standbyRestriction;
    }

    public Long getMeals() {
        return meals;
    }

    public void setMeals(Long meals) {
        this.meals = meals;
    }

    public Boolean getMealsSpecified() {
        return mealsSpecified;
    }

    public void setMealsSpecified(Boolean mealsSpecified) {
        this.mealsSpecified = mealsSpecified;
    }

    public Long getSpecialMeals() {
        return specialMeals;
    }

    public void setSpecialMeals(Long specialMeals) {
        this.specialMeals = specialMeals;
    }

    public Boolean getSpecialMealsSpecified() {
        return specialMealsSpecified;
    }

    public void setSpecialMealsSpecified(Boolean specialMealsSpecified) {
        this.specialMealsSpecified = specialMealsSpecified;
    }

    public Long getSetups() {
        return setups;
    }

    public void setSetups(Long setups) {
        this.setups = setups;
    }

    public Boolean getSetupsSpecified() {
        return setupsSpecified;
    }

    public void setSetupsSpecified(Boolean setupsSpecified) {
        this.setupsSpecified = setupsSpecified;
    }

    public Long getLocalOnBoard() {
        return localOnBoard;
    }

    public void setLocalOnBoard(Long localOnBoard) {
        this.localOnBoard = localOnBoard;
    }

    public Boolean getLocalOnBoardSpecified() {
        return localOnBoardSpecified;
    }

    public void setLocalOnBoardSpecified(Boolean localOnBoardSpecified) {
        this.localOnBoardSpecified = localOnBoardSpecified;
    }

    public Long getTotalOnBoard() {
        return totalOnBoard;
    }

    public void setTotalOnBoard(Long totalOnBoard) {
        this.totalOnBoard = totalOnBoard;
    }

    public Boolean getTotalOnBoardSpecified() {
        return totalOnBoardSpecified;
    }

    public void setTotalOnBoardSpecified(Boolean totalOnBoardSpecified) {
        this.totalOnBoardSpecified = totalOnBoardSpecified;
    }

    public Long getTotalBoardingPassIssued() {
        return totalBoardingPassIssued;
    }

    public void setTotalBoardingPassIssued(Long totalBoardingPassIssued) {
        this.totalBoardingPassIssued = totalBoardingPassIssued;
    }

    public Boolean getTotalBoardingPassIssuedSpecified() {
        return totalBoardingPassIssuedSpecified;
    }

    public void setTotalBoardingPassIssuedSpecified(Boolean totalBoardingPassIssuedSpecified) {
        this.totalBoardingPassIssuedSpecified = totalBoardingPassIssuedSpecified;
    }

}
