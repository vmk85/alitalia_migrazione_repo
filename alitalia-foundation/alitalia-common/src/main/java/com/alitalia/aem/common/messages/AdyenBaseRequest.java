package com.alitalia.aem.common.messages;

public class AdyenBaseRequest extends BaseRequest {
	
	private String shopperReference;
	private String language;
	private String market;
	private String conversationID;
	private String caller = "WebCheckin";
    private String iP_Address;
	
	public String getShopperReference() {
		return shopperReference;
	}

	public void setShopperReference(String shopperReference) {
		this.shopperReference = shopperReference;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public String getConversationID() {
		return conversationID;
	}

	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}

	public String getCaller() {
		return caller;
	}

	public void setCaller(String caller) {
		this.caller = caller;
	}

	public String getiP_Address() {
		return iP_Address;
	}

	public void setiP_Address(String iP_Address) {
		this.iP_Address = iP_Address;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result	+ ((shopperReference == null) ? 0 : shopperReference.hashCode());
		result = prime * result	+ ((language == null) ? 0 : language.hashCode());
		result = prime * result	+ ((market == null) ? 0 : market.hashCode());
		result = prime * result	+ ((conversationID == null) ? 0 : conversationID.hashCode());
		result = prime * result	+ ((caller == null) ? 0 : caller.hashCode());
		result = prime * result	+ ((iP_Address == null) ? 0 : iP_Address.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdyenBaseRequest other = (AdyenBaseRequest) obj;
		if (shopperReference == null) {
			if (other.shopperReference != null)
				return false;
		} else if (!shopperReference.equals(other.shopperReference))
			return false;
		if (language == null) {
			if (other.language != null)
				return false;
		} else if (!language.equals(other.language))
			return false;
		if (market == null) {
			if (other.market != null)
				return false;
		} else if (!market.equals(other.market))
			return false;
		if (conversationID == null) {
			if (other.conversationID != null)
				return false;
		} else if (!conversationID.equals(other.conversationID))
			return false;
		if (caller == null) {
			if (other.caller != null)
				return false;
		} else if (!caller.equals(other.caller))
			return false;
		if (iP_Address == null) {
			if (other.iP_Address != null)
				return false;
		} else if (!iP_Address.equals(other.iP_Address))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "AdyenBaseRequest ["
				+ "shopperReference=" + shopperReference
				+ "language=" + language
				+ "market=" + market
				+ "conversationID=" + conversationID
				+ "caller=" + caller
				+ "iP_Address=" + iP_Address
				+ "]";
	}
	
}