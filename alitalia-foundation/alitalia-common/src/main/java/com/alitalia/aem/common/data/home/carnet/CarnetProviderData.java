package com.alitalia.aem.common.data.home.carnet;

public class CarnetProviderData <T extends CarnetComunicationData> {

	protected T comunication;

	public T getComunication() {
		return comunication;
	}

	public void setComunication(T comunication) {
		this.comunication = comunication;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((comunication == null) ? 0 : comunication.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarnetProviderData other = (CarnetProviderData) obj;
		if (comunication == null) {
			if (other.comunication != null)
				return false;
		} else if (!comunication.equals(other.comunication))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CarnetProviderData [comunication=" + comunication + "]";
	}
}