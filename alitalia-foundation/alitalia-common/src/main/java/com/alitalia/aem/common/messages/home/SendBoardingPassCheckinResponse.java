package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class SendBoardingPassCheckinResponse extends BaseResponse{

	private Boolean sendBoardingPassResult;
	
	public SendBoardingPassCheckinResponse(){}
	
	public SendBoardingPassCheckinResponse(String tid, String sid) {
		super(tid, sid);
	}

	public Boolean getSendBoardingPassResult() {
		return sendBoardingPassResult;
	}

	public void setSendBoardingPassResult(Boolean sendBoardingPassResult) {
		this.sendBoardingPassResult = sendBoardingPassResult;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((sendBoardingPassResult == null) ? 0
						: sendBoardingPassResult.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SendBoardingPassCheckinResponse other = (SendBoardingPassCheckinResponse) obj;
		if (sendBoardingPassResult == null) {
			if (other.sendBoardingPassResult != null)
				return false;
		} else if (!sendBoardingPassResult.equals(other.sendBoardingPassResult))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SendBoardingPassCheckinResponse [sendBoardingPassResult="
				+ sendBoardingPassResult + ", toString()=" + super.toString()
				+ "]";
	}
}