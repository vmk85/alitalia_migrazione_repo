package com.alitalia.aem.common.messages.checkinrest;

import java.util.List;

import com.alitalia.aem.common.data.checkinrest.model.enhancedseatmap.request.ListItemsFareAvailQualifier;
import com.alitalia.aem.common.messages.BaseRequest;

public class CheckinEnhancedSeatMapRequest extends BaseRequest{

	public CheckinEnhancedSeatMapRequest() {}

	public CheckinEnhancedSeatMapRequest(String tid, String sid) {
		super(tid, sid);
	}

	private List<ListItemsFareAvailQualifier> listItemsFareAvailQualifiers = null;

	private String flight;

	private String bookingClass;

	private String origin;

	private String destination;

	private String departureDate;

	private String airline;
	
	private String pnr;

	private String language;

	private String market;

	private String conversationID;

	  public List<ListItemsFareAvailQualifier> getListItemsFareAvailQualifiers() {
	      return listItemsFareAvailQualifiers;
	  }
	
	  public void setListItemsFareAvailQualifiers(List<ListItemsFareAvailQualifier> listItemsFareAvailQualifiers) {
	      this.listItemsFareAvailQualifiers = listItemsFareAvailQualifiers;
	  }

	public String getFlight() {
		return flight;
	}

	public void setFlight(String flight) {
		this.flight = flight;
	}

	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getAirline() {
		return airline;
	}

	public void setAirline(String airline) {
		this.airline = airline;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public String getConversationID() {
		return conversationID;
	}

	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

}
