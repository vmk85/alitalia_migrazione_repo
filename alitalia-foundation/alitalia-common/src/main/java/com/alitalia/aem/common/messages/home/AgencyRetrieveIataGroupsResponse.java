package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class AgencyRetrieveIataGroupsResponse extends BaseRequest {

	private String codiceAgenzia;
	private Short returnCode;
	private Boolean validResponse;
	private Boolean iataEnrolled;
	private Boolean groupsEnabled;
	
	public AgencyRetrieveIataGroupsResponse() {
		super();
		this.returnCode = -1;
		this.groupsEnabled = false;
		this.iataEnrolled = false;
	}

	public AgencyRetrieveIataGroupsResponse(String tid, String sid) {
		super(tid, sid);
		this.returnCode = -1;
		this.groupsEnabled = false;
		this.iataEnrolled = false;
	}

	public AgencyRetrieveIataGroupsResponse(String tid, String sid, String codiceAgenzia) {
		super(tid, sid);
		this.codiceAgenzia = codiceAgenzia;
		this.returnCode = -1;
		this.groupsEnabled = false;
		this.iataEnrolled = false;
	}

	public String getCodiceAgenzia() {
		return codiceAgenzia;
	}
	
	public void setCodiceAgenzia(String codiceAgenzia) {
		this.codiceAgenzia = codiceAgenzia;
	}

	public Short getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(Short returnCode) {
		this.returnCode = returnCode;
	}

	public Boolean isValidResponse() {
		return validResponse;
	}

	public void setValidResponse(Boolean validResponse) {
		this.validResponse = validResponse;
	}

	public Boolean isIataEnrolled() {
		return iataEnrolled;
	}

	public void setIataEnrolled(Boolean iataEnrolled) {
		this.iataEnrolled = iataEnrolled;
	}

	public Boolean isGroupsEnabled() {
		return groupsEnabled;
	}

	public void setGroupsEnabled(Boolean groupsEnabled) {
		this.groupsEnabled = groupsEnabled;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((codiceAgenzia == null) ? 0 : codiceAgenzia.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgencyRetrieveIataGroupsResponse other = (AgencyRetrieveIataGroupsResponse) obj;
		if (codiceAgenzia == null) {
			if (other.codiceAgenzia != null)
				return false;
		} else if (!codiceAgenzia.equals(other.codiceAgenzia))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AgencyRetrieveIataGroupsResponse [codiceAgenzia="
				+ codiceAgenzia + ", returnCode=" + returnCode
				+ ", validResponse=" + validResponse + ", iataEnrolled="
				+ iataEnrolled + ", groupsEnabled=" + groupsEnabled + "]";
	}
}
