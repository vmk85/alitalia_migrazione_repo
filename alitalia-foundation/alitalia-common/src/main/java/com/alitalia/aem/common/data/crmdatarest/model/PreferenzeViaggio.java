package com.alitalia.aem.common.data.crmdatarest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PreferenzeViaggio {

    @SerializedName("viaggiInFamiglia")
    @Expose
    boolean viaggiInFamiglia = false;

    @SerializedName("aeroportoPreferito")
    @Expose
    private String aeroportoPreferito;

    @SerializedName("cabin")
    @Expose
    private String cabin;

    @SerializedName("posto")
    @Expose
    private String posto;

    @SerializedName("pasto")
    @Expose
    private String pasto;

    @SerializedName("bagaglioStiva")
    @Expose
    private String bagaglioStiva;

    @SerializedName("assicurazione")
    @Expose
    private String assicurazione;

    @SerializedName("motivoDiViaggio")
    @Expose
    private String motivoDiViaggio;

    public boolean isViaggiInFamiglia() {
        return viaggiInFamiglia;
    }

    public void setViaggiInFamiglia(boolean viaggiInFamiglia) {
        this.viaggiInFamiglia = viaggiInFamiglia;
    }

    public String getAeroportoPreferito() {
        return aeroportoPreferito;
    }

    public void setAeroportoPreferito(String aeroportoPreferito) {
        this.aeroportoPreferito = aeroportoPreferito;
    }

    public String getCabin() {
        return cabin;
    }

    public void setCabin(String cabin) {
        this.cabin = cabin;
    }

    public String getPosto() {
        return posto;
    }

    public void setPosto(String posto) {
        this.posto = posto;
    }

    public String getPasto() {
        return pasto;
    }

    public void setPasto(String pasto) {
        this.pasto = pasto;
    }

    public String getBagaglioStiva() {
        return bagaglioStiva;
    }

    public void setBagaglioStiva(String bagaglioStiva) {
        this.bagaglioStiva = bagaglioStiva;
    }

    public String getAssicurazione() {
        return assicurazione;
    }

    public void setAssicurazione(String assicurazione) {
        this.assicurazione = assicurazione;
    }

    public String getMotivoDiViaggio() {
        return motivoDiViaggio;
    }

    public void setMotivoDiViaggio(String motivoDiViaggio) {
        this.motivoDiViaggio = motivoDiViaggio;
    }

    @Override
    public String toString() {
        return "PreferenzeViaggio{" +
                "viaggiInFamiglia=" + viaggiInFamiglia +
                ", aeroportoPreferito='" + aeroportoPreferito + '\'' +
                ", cabin='" + cabin + '\'' +
                ", posto='" + posto + '\'' +
                ", pasto='" + pasto + '\'' +
                ", bagaglioStiva='" + bagaglioStiva + '\'' +
                ", assicurazione='" + assicurazione + '\'' +
                ", motivoDiViaggio='" + motivoDiViaggio + '\'' +
                '}';
    }
}
