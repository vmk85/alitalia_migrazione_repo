
package com.alitalia.aem.common.data.checkinrest.model.cheangeseats.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Seat {

    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("availability")
    @Expose
    private String availability;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("price")
    @Expose
    private Long price;
    @SerializedName("confortSeat")
    @Expose
    private Boolean confortSeat;
    @SerializedName("passengerId")
    @Expose
    private String passengerId;
    @SerializedName("occupatoDa")
    @Expose
    private String occupatoDa;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Boolean getConfortSeat() {
        return confortSeat;
    }

    public void setConfortSeat(Boolean confortSeat) {
        this.confortSeat = confortSeat;
    }

    public String getPassengerId() {
        return passengerId;
    }

    public void setPassengerId(String passengerId) {
        this.passengerId = passengerId;
    }

    public String getOccupatoDa() {
        return occupatoDa;
    }

    public void setOccupatoDa(String occupatoDa) {
        this.occupatoDa = occupatoDa;
    }

}
