package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.common.messages.BaseRequest;

public class CrossSellingsRequest extends BaseRequest {

	private String ipAddress;
	private String languageCode;
	private String marketCode;
	private RoutesData prenotation;
	private String userAgent;

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getMarketCode() {
		return marketCode;
	}

	public void setMarketCode(String marketCode) {
		this.marketCode = marketCode;
	}

	public RoutesData getPrenotation() {
		return prenotation;
	}

	public void setPrenotation(RoutesData prenotation) {
		this.prenotation = prenotation;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ipAddress == null) ? 0 : ipAddress.hashCode());
		result = prime * result + ((languageCode == null) ? 0 : languageCode.hashCode());
		result = prime * result + ((marketCode == null) ? 0 : marketCode.hashCode());
		result = prime * result + ((prenotation == null) ? 0 : prenotation.hashCode());
		result = prime * result + ((userAgent == null) ? 0 : userAgent.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CrossSellingsRequest other = (CrossSellingsRequest) obj;
		if (ipAddress == null) {
			if (other.ipAddress != null)
				return false;
		} else if (!ipAddress.equals(other.ipAddress))
			return false;
		if (languageCode == null) {
			if (other.languageCode != null)
				return false;
		} else if (!languageCode.equals(other.languageCode))
			return false;
		if (marketCode == null) {
			if (other.marketCode != null)
				return false;
		} else if (!marketCode.equals(other.marketCode))
			return false;
		if (prenotation == null) {
			if (other.prenotation != null)
				return false;
		} else if (!prenotation.equals(other.prenotation))
			return false;
		if (userAgent == null) {
			if (other.userAgent != null)
				return false;
		} else if (!userAgent.equals(other.userAgent))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CrossSellingsRequest [ipAddress=" + ipAddress + ", languageCode=" + languageCode + ", marketCode="
				+ marketCode + ", prenotation=" + prenotation + ", userAgent=" + userAgent + "]";
	}

}
