package com.alitalia.aem.common.messages.home.enumeration;

public enum SearchExecuteResponseType {

    MODEL("Model"),
    BOOM_BOX("BoomBox"),
    XML("Xml");
    private final String value;

    SearchExecuteResponseType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SearchExecuteResponseType fromValue(String v) {
        for (SearchExecuteResponseType c: SearchExecuteResponseType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
