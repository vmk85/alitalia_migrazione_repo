package com.alitalia.aem.common.data.home.mmb;

import java.nio.ByteBuffer;
import java.util.Calendar;

import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbGdsTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbSeatStatusEnum;

public class MmbCouponData {

	private Integer id;;
	// Il campo Barcode dovrebbe contenere l'immagine del codice a barre ma non e' usato
	private ByteBuffer barcode;
	private String boardingPassColor;
	private Calendar boardingTime;
	private Boolean changeSeatEnabled;
	private String eticket;
	private Integer flightId;
	private MmbFlightData flight;
	private String gate;
	private MmbGdsTypeEnum gdsCouponType;
	private Boolean comfortSeatPaid;
	private Boolean infantCoupon;
	private Boolean skyPriority;
	private String oldSeat;
	private String orderNumber;
	private Integer passengerId;
	private String previousSeat;
	private String ssrCode;
	private String seatClassName;
	private MmbCompartimentalClassEnum seatClass;
	private String seat;
	private String sequenceNumber;
	private String socialCode;
	private String specialFare;
	private MmbSeatStatusEnum status;
	private String terminal;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ByteBuffer getBarcode() {
		return barcode;
	}

	public void setBarcode(ByteBuffer barcode) {
		this.barcode = barcode;
	}

	public String getBoardingPassColor() {
		return boardingPassColor;
	}

	public void setBoardingPassColor(String boardingPassColor) {
		this.boardingPassColor = boardingPassColor;
	}

	public Calendar getBoardingTime() {
		return boardingTime;
	}

	public void setBoardingTime(Calendar boardingTime) {
		this.boardingTime = boardingTime;
	}

	public Boolean isChangeSeatEnabled() {
		return changeSeatEnabled;
	}

	public void setChangeSeatEnabled(Boolean changeSeatEnabled) {
		this.changeSeatEnabled = changeSeatEnabled;
	}

	public String getEticket() {
		return eticket;
	}

	public void setEticket(String eticket) {
		this.eticket = eticket;
	}

	public Integer getFlightId() {
		return flightId;
	}

	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	public MmbFlightData getFlight() {
		return flight;
	}

	public void setFlight(MmbFlightData flight) {
		this.flight = flight;
	}

	public String getGate() {
		return gate;
	}

	public void setGate(String gate) {
		this.gate = gate;
	}

	public MmbGdsTypeEnum getGdsCouponType() {
		return gdsCouponType;
	}

	public void setGdsCouponType(MmbGdsTypeEnum gdsCouponType) {
		this.gdsCouponType = gdsCouponType;
	}

	public Boolean isComfortSeatPaid() {
		return comfortSeatPaid;
	}

	public void setComfortSeatPaid(Boolean isComfortSeatPaid) {
		this.comfortSeatPaid = isComfortSeatPaid;
	}

	public Boolean iInfantCoupon() {
		return infantCoupon;
	}

	public void setInfantCoupon(Boolean isInfantCoupon) {
		this.infantCoupon = isInfantCoupon;
	}

	public Boolean isSkyPriority() {
		return skyPriority;
	}

	public void setSkyPriority(Boolean isSkyPriority) {
		this.skyPriority = isSkyPriority;
	}

	public String getOldSeat() {
		return oldSeat;
	}

	public void setOldSeat(String oldSeat) {
		this.oldSeat = oldSeat;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public Integer getPassengerId() {
		return passengerId;
	}

	public void setPassengerId(Integer passengerId) {
		this.passengerId = passengerId;
	}

	public String getPreviousSeat() {
		return previousSeat;
	}

	public void setPreviousSeat(String previousSeat) {
		this.previousSeat = previousSeat;
	}

	public String getSsrCode() {
		return ssrCode;
	}

	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	public String getSeatClassName() {
		return seatClassName;
	}

	public void setSeatClassName(String seatClassName) {
		this.seatClassName = seatClassName;
	}

	public MmbCompartimentalClassEnum getSeatClass() {
		return seatClass;
	}

	public void setSeatClass(MmbCompartimentalClassEnum seatClass) {
		this.seatClass = seatClass;
	}

	public String getSeat() {
		return seat;
	}

	public void setSeat(String seat) {
		this.seat = seat;
	}

	public String getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getSocialCode() {
		return socialCode;
	}

	public void setSocialCode(String socialCode) {
		this.socialCode = socialCode;
	}

	public String getSpecialFare() {
		return specialFare;
	}

	public void setSpecialFare(String specialFare) {
		this.specialFare = specialFare;
	}

	public MmbSeatStatusEnum getStatus() {
		return status;
	}

	public void setStatus(MmbSeatStatusEnum status) {
		this.status = status;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbCouponData other = (MmbCouponData) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbCouponData [id=" + id + ", barcode=" + barcode
				+ ", boardingPassColor=" + boardingPassColor
				+ ", boardingTime=" + boardingTime + ", changeSeatEnabled="
				+ changeSeatEnabled + ", eticket=" + eticket + ", flightId="
				+ flightId + ", flight=" + flight + ", gate=" + gate
				+ ", gdsCouponType=" + gdsCouponType + ", comfortSeatPaid="
				+ comfortSeatPaid + ", infantCoupon=" + infantCoupon
				+ ", skyPriority=" + skyPriority + ", oldSeat=" + oldSeat
				+ ", orderNumber=" + orderNumber + ", passengerId="
				+ passengerId + ", previousSeat=" + previousSeat + ", ssrCode="
				+ ssrCode + ", seatClassName=" + seatClassName + ", seatClass="
				+ seatClass + ", seat=" + seat + ", sequenceNumber="
				+ sequenceNumber + ", socialCode=" + socialCode
				+ ", specialFare=" + specialFare + ", status=" + status
				+ ", terminal=" + terminal + "]";
	}

}
