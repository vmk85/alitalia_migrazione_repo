package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PnrListInfoByFrequentFlyerSearch;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinFrequentFlyerSearchResponse extends BaseResponse {
	
	private PnrListInfoByFrequentFlyerSearch pnrData = null;

	public CheckinFrequentFlyerSearchResponse() {}

	public CheckinFrequentFlyerSearchResponse(String tid, String sid) {
		super(tid, sid);
	}

	public PnrListInfoByFrequentFlyerSearch getPnrData() {
		return pnrData;
	}

	public void setPnrData(PnrListInfoByFrequentFlyerSearch pnrData) {
		this.pnrData = pnrData;
	}


}
