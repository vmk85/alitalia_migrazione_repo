
package com.alitalia.aem.common.data.checkinrest.model.updatesecurityinfo.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateSecurityInfoResp {

    @SerializedName("freeTextInfoList")
    @Expose
    private List<FreeTextInfoList> freeTextInfoList = null;
    @SerializedName("itineraryResponseList")
    @Expose
    private List<ItineraryResponseList> itineraryResponseList = null;
    @SerializedName("passengerInfoResponseList")
    @Expose
    private List<PassengerInfoResponseList> passengerInfoResponseList = null;
    @SerializedName("result")
    @Expose
    private Result result;
    @SerializedName("timeStamp")
    @Expose
    private String timeStamp;
    @SerializedName("timeStampSpecified")
    @Expose
    private Boolean timeStampSpecified;
    @SerializedName("trackingId")
    @Expose
    private String trackingId;
    @SerializedName("version")
    @Expose
    private String version;
    @SerializedName("versionSpecified")
    @Expose
    private Boolean versionSpecified;

    public List<FreeTextInfoList> getFreeTextInfoList() {
        return freeTextInfoList;
    }

    public void setFreeTextInfoList(List<FreeTextInfoList> freeTextInfoList) {
        this.freeTextInfoList = freeTextInfoList;
    }

    public List<ItineraryResponseList> getItineraryResponseList() {
        return itineraryResponseList;
    }

    public void setItineraryResponseList(List<ItineraryResponseList> itineraryResponseList) {
        this.itineraryResponseList = itineraryResponseList;
    }

    public List<PassengerInfoResponseList> getPassengerInfoResponseList() {
        return passengerInfoResponseList;
    }

    public void setPassengerInfoResponseList(List<PassengerInfoResponseList> passengerInfoResponseList) {
        this.passengerInfoResponseList = passengerInfoResponseList;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Boolean getTimeStampSpecified() {
        return timeStampSpecified;
    }

    public void setTimeStampSpecified(Boolean timeStampSpecified) {
        this.timeStampSpecified = timeStampSpecified;
    }

    public String getTrackingId() {
        return trackingId;
    }

    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Boolean getVersionSpecified() {
        return versionSpecified;
    }

    public void setVersionSpecified(Boolean versionSpecified) {
        this.versionSpecified = versionSpecified;
    }

}
