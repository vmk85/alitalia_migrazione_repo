package com.alitalia.aem.common.data.home;

import java.util.Calendar;

import com.alitalia.aem.common.data.home.enumerations.MMAddressTypeEnum;

public class MMAddressData {

	private String companyName = "";
	private String country = "";
	private String invalidIndicator = "";
	private String istruction = "";
	private String mailingIndicator = "";
	private String municipalityName = "";
	private String postalCode = "";
	private String stateCode = "";
	private String streetFreeText = "";

	private Calendar startDate;
	private Calendar endDate;

	private MMAddressTypeEnum addressType;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getInvalidIndicator() {
		return invalidIndicator;
	}

	public void setInvalidIndicator(String invalidIndicator) {
		this.invalidIndicator = invalidIndicator;
	}

	public String getIstruction() {
		return istruction;
	}

	public void setIstruction(String istruction) {
		this.istruction = istruction;
	}

	public String getMailingIndicator() {
		return mailingIndicator;
	}

	public void setMailingIndicator(String mailingIndicator) {
		this.mailingIndicator = mailingIndicator;
	}

	public String getMunicipalityName() {
		return municipalityName;
	}

	public void setMunicipalityName(String municipalityName) {
		this.municipalityName = municipalityName;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getStreetFreeText() {
		return streetFreeText;
	}

	public void setStreetFreeText(String streetFreeText) {
		this.streetFreeText = streetFreeText;
	}

	public Calendar getStartDate() {
		return startDate;
	}

	public void setStartDate(Calendar startDate) {
		this.startDate = startDate;
	}

	public Calendar getEndDate() {
		return endDate;
	}

	public void setEndDate(Calendar endDate) {
		this.endDate = endDate;
	}

	public MMAddressTypeEnum getAddressType() {
		return addressType;
	}

	public void setAddressType(MMAddressTypeEnum addressType) {
		this.addressType = addressType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((addressType == null) ? 0 : addressType.hashCode());
		result = prime * result
				+ ((companyName == null) ? 0 : companyName.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime
				* result
				+ ((invalidIndicator == null) ? 0 : invalidIndicator.hashCode());
		result = prime * result
				+ ((istruction == null) ? 0 : istruction.hashCode());
		result = prime
				* result
				+ ((mailingIndicator == null) ? 0 : mailingIndicator.hashCode());
		result = prime
				* result
				+ ((municipalityName == null) ? 0 : municipalityName.hashCode());
		result = prime * result
				+ ((postalCode == null) ? 0 : postalCode.hashCode());
		result = prime * result
				+ ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result
				+ ((stateCode == null) ? 0 : stateCode.hashCode());
		result = prime * result
				+ ((streetFreeText == null) ? 0 : streetFreeText.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MMAddressData other = (MMAddressData) obj;
		if (addressType != other.addressType)
			return false;
		if (companyName == null) {
			if (other.companyName != null)
				return false;
		} else if (!companyName.equals(other.companyName))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (invalidIndicator == null) {
			if (other.invalidIndicator != null)
				return false;
		} else if (!invalidIndicator.equals(other.invalidIndicator))
			return false;
		if (istruction == null) {
			if (other.istruction != null)
				return false;
		} else if (!istruction.equals(other.istruction))
			return false;
		if (mailingIndicator == null) {
			if (other.mailingIndicator != null)
				return false;
		} else if (!mailingIndicator.equals(other.mailingIndicator))
			return false;
		if (municipalityName == null) {
			if (other.municipalityName != null)
				return false;
		} else if (!municipalityName.equals(other.municipalityName))
			return false;
		if (postalCode == null) {
			if (other.postalCode != null)
				return false;
		} else if (!postalCode.equals(other.postalCode))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		if (stateCode == null) {
			if (other.stateCode != null)
				return false;
		} else if (!stateCode.equals(other.stateCode))
			return false;
		if (streetFreeText == null) {
			if (other.streetFreeText != null)
				return false;
		} else if (!streetFreeText.equals(other.streetFreeText))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MMAddressData [companyName=" + companyName + ", country="
				+ country + ", invalidIndicator=" + invalidIndicator
				+ ", istruction=" + istruction + ", mailingIndicator="
				+ mailingIndicator + ", municipalityName=" + municipalityName
				+ ", postalCode=" + postalCode + ", stateCode=" + stateCode
				+ ", streetFreeText=" + streetFreeText + ", startDate="
				+ startDate + ", endDate=" + endDate + ", addressType="
				+ addressType + "]";
	}

}
