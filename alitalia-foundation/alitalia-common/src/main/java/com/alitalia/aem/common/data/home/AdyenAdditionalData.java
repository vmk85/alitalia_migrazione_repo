package com.alitalia.aem.common.data.home;

public class AdyenAdditionalData {

	/*
		{
			"key": "cardBin",
			"cardBin": "444433"
		}
	*/
	
	private String key;
	private String cardBin;

	public AdyenAdditionalData() {
	}

	public AdyenAdditionalData(String key, String cardBin) {
		this.key = key;
		this.cardBin = cardBin;
	}

	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getCardBin() {
		return cardBin;
	}
	public void setCardBin(String cardBin) {
		this.cardBin = cardBin;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result	+ ((key == null) ? 0 : key.hashCode());
		result = prime * result	+ ((cardBin == null) ? 0 : cardBin.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdyenAdditionalData other = (AdyenAdditionalData) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (cardBin == null) {
			if (other.cardBin != null)
				return false;
		} else if (!cardBin.equals(other.cardBin))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "AdditionalData ["
				+ "key=" + key
				+ ", cardBin=" + cardBin
				+ "]";
	}

}

