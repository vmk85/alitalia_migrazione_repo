package com.alitalia.aem.common.messages.home;

public class RegisterECouponStatisticRequest extends RegisterStatisticsRequest {

	private String discountName;

	public RegisterECouponStatisticRequest(String tid, String sid) {
		super(tid, sid);
	}
	
	public String getDiscountName() {
		return discountName;
	}
	public void setDiscountName(String discountName) {
		this.discountName = discountName;
	}

	@Override
	public String toString() {
		return "RegisterECouponStatisticRequest [discountName=" + discountName
				+ ", getDiscountName()=" + getDiscountName()
				+ ", getClientIP()=" + getClientIP() + ", getErrorDescr()="
				+ getErrorDescr() + ", getSessionId()=" + getSessionId()
				+ ", getSiteCode()=" + getSiteCode() + ", isSuccess()="
				+ isSuccess() + ", getType()=" + getType() + ", getUserId()="
				+ getUserId() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + ", getTid()=" + getTid() + ", getSid()="
				+ getSid() + ", getClass()=" + getClass() + "]";
	}
}