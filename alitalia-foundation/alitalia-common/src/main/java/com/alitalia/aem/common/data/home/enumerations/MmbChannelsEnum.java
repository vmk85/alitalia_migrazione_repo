package com.alitalia.aem.common.data.home.enumerations;

public enum MmbChannelsEnum {

	WEB("Web"),
	AGENT("Agent"),
	CALL_CENTER("CallCenter"),
	UNKNOW("UnKnow");
    private final String value;

    MmbChannelsEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MmbChannelsEnum fromValue(String v) {
        for (MmbChannelsEnum c: MmbChannelsEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
