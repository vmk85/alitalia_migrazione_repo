package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.enumerations.AlitaliaTradeUserType;
import com.alitalia.aem.common.messages.BaseRequest;

public class AgencyRetrievePasswordByVATRequest extends BaseRequest {

	private String partitaIva;
	private AlitaliaTradeUserType ruolo;
	
	public AgencyRetrievePasswordByVATRequest(String tid, String sid) {
		super(tid, sid);
	}

	public AgencyRetrievePasswordByVATRequest(String tid, String sid,
			String partitaIva, AlitaliaTradeUserType ruolo) {
		super(tid, sid);
		this.partitaIva = partitaIva;
		this.ruolo = ruolo;
	}

	public String getPartitaIva() {
		return partitaIva;
	}
	
	public void setPartitaIva(String partitaIva) {
		this.partitaIva = partitaIva;
	}
	
	public AlitaliaTradeUserType getRuolo() {
		return ruolo;
	}
	
	public void setRuolo(AlitaliaTradeUserType ruolo) {
		this.ruolo = ruolo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((partitaIva == null) ? 0 : partitaIva.hashCode());
		result = prime * result + ((ruolo == null) ? 0 : ruolo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgencyRetrievePasswordByVATRequest other = (AgencyRetrievePasswordByVATRequest) obj;
		if (partitaIva == null) {
			if (other.partitaIva != null)
				return false;
		} else if (!partitaIva.equals(other.partitaIva))
			return false;
		if (ruolo != other.ruolo)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AgencyPasswordChangeRequest [partitaIva=" + partitaIva
				+ ", ruolo=" + ruolo + "]";
	}
}
