package com.alitalia.aem.common.data.checkinrest.model.passenger;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FlightSeatMapInfoList {
	
	@SerializedName("flightInfo")
	@Expose
	private FlightInfo flightInfo;
	
	@SerializedName("flightSeatMapDetail")
	@Expose
	private List<FlightSeatMapDetail> flightSeatMapDetail = null;

	public FlightInfo getFlightInfo() {
		return flightInfo;
	}

	public void setFlightInfo(FlightInfo flightInfo) {
		this.flightInfo = flightInfo;
	}

	public List<FlightSeatMapDetail> getFlightSeatMapDetail() {
		return flightSeatMapDetail;
	}

	public void setFlightSeatMapDetail(List<FlightSeatMapDetail> flightSeatMapDetail) {
		this.flightSeatMapDetail = flightSeatMapDetail;
	}
	
}
