package com.alitalia.aem.common.data.home.enumerations;


public enum CarnetPaymentTypeEnum {

	BANCA_INTESA("BancaIntesa"),
    BANCO_POSTA("BancoPosta"),
    CREDIT_CARD("CreditCard"),
    PAY_PAL("PayPal"),
    UNICREDIT("Unicredit");
    
    private final String value;

    CarnetPaymentTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CarnetPaymentTypeEnum fromValue(String v) {
        for (CarnetPaymentTypeEnum c: CarnetPaymentTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
