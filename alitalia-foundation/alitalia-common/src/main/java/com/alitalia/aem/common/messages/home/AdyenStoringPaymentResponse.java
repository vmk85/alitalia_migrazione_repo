package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.AdyenBaseResponse;

public class AdyenStoringPaymentResponse  extends AdyenBaseResponse {
	
	/*
	{
	  "pspReference": "8535362442791929",
	  "resultCode": "Authorised",
	  "authCode": "64069",
	  "conversationID": "987654321"
	}
	*/

	private String pspReference;
	private String resultCode;
	private String authCode;
	
	public String getPspReference() {
		return pspReference;
	}

	public void setPspReference(String pspReference) {
		this.pspReference = pspReference;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result	+ ((pspReference == null) ? 0 : pspReference.hashCode());
		result = prime * result	+ ((resultCode == null) ? 0 : resultCode.hashCode());
		result = prime * result	+ ((authCode == null) ? 0 : authCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdyenStoringPaymentResponse other = (AdyenStoringPaymentResponse) obj;
		if (pspReference == null) {
			if (other.pspReference != null)
				return false;
		} else if (!pspReference.equals(other.pspReference))
			return false;
		if (resultCode == null) {
			if (other.resultCode != null)
				return false;
		} else if (!resultCode.equals(other.resultCode))
			return false;
		if (authCode == null) {
			if (other.authCode != null)
				return false;
		} else if (!authCode.equals(other.authCode))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "RegisterCreditCardRequest ["
				+ "pspReference=" + pspReference
				+ "resultCode=" + resultCode
				+ "authCode=" + authCode
				+ "conversationID=" + super.getConversationID()
				+ "]";
	}
	
}