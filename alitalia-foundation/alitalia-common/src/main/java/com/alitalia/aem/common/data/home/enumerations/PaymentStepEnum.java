package com.alitalia.aem.common.data.home.enumerations;

public enum PaymentStepEnum {

    INITIALIZE("Initialize"),
    AUTHORIZE("Authorize"),
    CHECK("Check"),
    RETRIEVE("Retrieve"),
    CLOSE("Close");
    
    private final String value;

    PaymentStepEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PaymentStepEnum fromValue(String v) {
        for (PaymentStepEnum c: PaymentStepEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
