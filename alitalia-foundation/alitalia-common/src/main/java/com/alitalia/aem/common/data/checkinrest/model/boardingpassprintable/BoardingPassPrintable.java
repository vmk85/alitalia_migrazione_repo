package com.alitalia.aem.common.data.checkinrest.model.boardingpassprintable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BoardingPassPrintable {

	@SerializedName("isPrintable")
	@Expose
	private Boolean isPrintable;

	public Boolean getIsPrintable() {
		return isPrintable;
	}

	public void setIsPrintable(Boolean isPrintable) {
		this.isPrintable = isPrintable;
	}

};

