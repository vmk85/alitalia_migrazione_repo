
package com.alitalia.aem.common.data.checkinrest.model.enhancedseatmap.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SeatMapColumn {

    @SerializedName("column")
    @Expose
    private String column;
    @SerializedName("typeCharacteristics")
    @Expose
    private String typeCharacteristics;

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getTypeCharacteristics() {
        return typeCharacteristics;
    }

    public void setTypeCharacteristics(String typeCharacteristics) {
        this.typeCharacteristics = typeCharacteristics;
    }

}
