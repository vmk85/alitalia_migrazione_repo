package com.alitalia.aem.common.data.home.enumerations;

public enum MMTitleTypeEnum {

    UNKNOWN("UnKnown"),
    INGEGNERE("INGEGNERE"),
    MISSIS("MISSIS"),
    RAGIONIERE("RAGIONIERE"),
    SENATORE("SENATORE"),
    REVERENDO("REVERENDO"),
    CAVALIERE("CAVALIERE"),
    MISS("MISS"),
    DOTTORE("DOTTORE"),
    ONOREVOLE("ONOREVOLE"),
    GEOMETRA("GEOMETRA"),
    PROFESSORE("PROFESSORE"),
    COMMENDATORE("COMMENDATORE"),
    MISTER("MISTER"),
    ARCHITETTO("ARCHITETTO"),
    AVVOCATO("AVVOCATO"),
    DOTTORESSA("DOTTORESSA"),
    HERR("HERR"),
    FRAU("FRAU"),
    DOKTOR("DOKTOR"),
    CARDINALE("CARDINALE"),
    AMBASCIATORE("AMBASCIATORE"),
    MINISTRO("MINISTRO"),
    MONSIGNORE("MONSIGNORE"),
    PRINCIPE("PRINCIPE"),
    GENERALE("GENERALE"),
    MS("MS");

    private final String value;
    
    MMTitleTypeEnum(String v) {
		value = v;
	}
	
	public String value() {
		return value;
	}

	public static MMTitleTypeEnum fromValue(String v) {
		for (MMTitleTypeEnum c: MMTitleTypeEnum.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
