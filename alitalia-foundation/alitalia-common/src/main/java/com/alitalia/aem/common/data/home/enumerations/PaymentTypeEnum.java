package com.alitalia.aem.common.data.home.enumerations;

public enum PaymentTypeEnum {

    BANCA_INTESA("BancaIntesa"),
    BANCO_POSTA("BancoPosta"),
    CREDIT_CARD("CreditCard"),
    PAY_PAL("PayPal"),
    UNICREDIT("Unicredit"),
    LOTTOMATICA("Lottomatica"),
    FINDOMESTIC("Findomestic"),
    INSTALMENTS_BRAZIL("InstalmentsBrazil"),
    PAY_AT_TO("PayAtTO"),
    GLOBAL_COLLECT("GlobalCollect"),
    POSTE_ID("PosteID"),
    MASTER_PASS("MasterPass"),
    DISCOVER("Discover"),
    JCB("JCB"),
    CARTE_BLEUE("CarteBleue"),
    YANDEX("Yandex"),
    WEB_MONEY("WebMoney"),
    IDEAL("Ideal"),
    MAESTRO("Maestro"),
    STS("STS");
    
    private final String value;

    PaymentTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PaymentTypeEnum fromValue(String v) {
        for (PaymentTypeEnum c: PaymentTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
