package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.checkin.CheckinRouteData;
import com.alitalia.aem.common.messages.MmbBaseRequest;

public class WebCheckinUndoCheckinRequest extends MmbBaseRequest {
	
    protected String eticket;
    protected String lastname;
    protected String name;
    private CheckinRouteData route;

	public WebCheckinUndoCheckinRequest(String tid, String sid) {
		super(tid, sid);
	}

	public WebCheckinUndoCheckinRequest() {
		super();
	}

	public String getEticket() {
		return eticket;
	}

	public void setEticket(String eticket) {
		this.eticket = eticket;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CheckinRouteData getRoute() {
		return route;
	}

	public void setRoute(CheckinRouteData route) {
		this.route = route;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((eticket == null) ? 0 : eticket.hashCode());
		result = prime * result
				+ ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((route == null) ? 0 : route.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebCheckinUndoCheckinRequest other = (WebCheckinUndoCheckinRequest) obj;
		if (eticket == null) {
			if (other.eticket != null)
				return false;
		} else if (!eticket.equals(other.eticket))
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (route == null) {
			if (other.route != null)
				return false;
		} else if (!route.equals(other.route))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WebCheckinUndoCheckinRequest [eticket=" + eticket + ", lastname="
				+ lastname + ", name=" + name + ", route=" + route
				+ ", toString()=" + super.toString() + "]";
	}
}
