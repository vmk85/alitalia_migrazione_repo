
package com.alitalia.aem.common.data.checkinrest.model.updatesecurityinfo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BagTagInfo_ {

    @SerializedName("bagTagNumber")
    @Expose
    private String bagTagNumber;
    @SerializedName("issuedAirlineCode")
    @Expose
    private String issuedAirlineCode;
    @SerializedName("issueDate")
    @Expose
    private String issueDate;
    @SerializedName("issueTime")
    @Expose
    private String issueTime;
    @SerializedName("parseBagTagNumber")
    @Expose
    private ParseBagTagNumber parseBagTagNumber;

    public String getBagTagNumber() {
        return bagTagNumber;
    }

    public void setBagTagNumber(String bagTagNumber) {
        this.bagTagNumber = bagTagNumber;
    }

    public String getIssuedAirlineCode() {
        return issuedAirlineCode;
    }

    public void setIssuedAirlineCode(String issuedAirlineCode) {
        this.issuedAirlineCode = issuedAirlineCode;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getIssueTime() {
        return issueTime;
    }

    public void setIssueTime(String issueTime) {
        this.issueTime = issueTime;
    }

    public ParseBagTagNumber getParseBagTagNumber() {
        return parseBagTagNumber;
    }

    public void setParseBagTagNumber(ParseBagTagNumber parseBagTagNumber) {
        this.parseBagTagNumber = parseBagTagNumber;
    }

}
