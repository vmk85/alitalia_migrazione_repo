package com.alitalia.aem.common.component.behaviour.exc;

public class BehaviourException extends RuntimeException {
	
	private static final long serialVersionUID = -3801533085508542642L;

	public BehaviourException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BehaviourException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public BehaviourException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public BehaviourException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public BehaviourException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
	

}
