package com.alitalia.aem.common.data.home.checkin;

import java.math.BigDecimal;
import java.util.Calendar;

import com.alitalia.aem.common.data.home.enumerations.CheckinAncillaryServiceEnum;
import com.alitalia.aem.common.data.home.enumerations.CheckinSaleChannelEnum;

public class CheckinPaymentData {
	
	private Integer id;
	private BigDecimal amount;
	private String applicationSpecificOrderID;
	private String authUserFrequentFlyerCode;
	private String cartID;
	private CheckinCreditCardData creditCardData;
	private String currency;
	private String description;
	private String idAuthEvent;
	private String email;
	private String methodId;
	private String orderCode;
	private Integer paymentAttemptCount;
	private String persistenceId;
	private CheckinSaleChannelEnum saleChannel;
	private CheckinAncillaryServiceEnum soldGood;
	private Calendar transactionDate;
	private CheckinPaymentTransactionInfoData transactionInfo;
	private String vbvParam;
	private CheckinVerifiedByVisaData verifiedByVisa;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getApplicationSpecificOrderID() {
		return applicationSpecificOrderID;
	}
	public void setApplicationSpecificOrderID(String applicationSpecificOrderID) {
		this.applicationSpecificOrderID = applicationSpecificOrderID;
	}
	public String getAuthUserFrequentFlyerCode() {
		return authUserFrequentFlyerCode;
	}
	public void setAuthUserFrequentFlyerCode(String authUserFrequentFlyerCode) {
		this.authUserFrequentFlyerCode = authUserFrequentFlyerCode;
	}
	public String getCartID() {
		return cartID;
	}
	public void setCartID(String cartID) {
		this.cartID = cartID;
	}
	public CheckinCreditCardData getCreditCardData() {
		return creditCardData;
	}
	public void setCreditCardData(CheckinCreditCardData creditCardData) {
		this.creditCardData = creditCardData;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIdAuthEvent() {
		return idAuthEvent;
	}
	public void setIdAuthEvent(String idAuthEvent) {
		this.idAuthEvent = idAuthEvent;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMethodId() {
		return methodId;
	}
	public void setMethodId(String methodId) {
		this.methodId = methodId;
	}
	public String getOrderCode() {
		return orderCode;
	}
	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}
	public Integer getPaymentAttemptCount() {
		return paymentAttemptCount;
	}
	public void setPaymentAttemptCount(Integer paymentAttemptCount) {
		this.paymentAttemptCount = paymentAttemptCount;
	}
	public String getPersistenceId() {
		return persistenceId;
	}
	public void setPersistenceId(String persistenceId) {
		this.persistenceId = persistenceId;
	}
	public CheckinSaleChannelEnum getSaleChannel() {
		return saleChannel;
	}
	public void setSaleChannel(CheckinSaleChannelEnum saleChannel) {
		this.saleChannel = saleChannel;
	}
	public CheckinAncillaryServiceEnum getSoldGood() {
		return soldGood;
	}
	public void setSoldGood(CheckinAncillaryServiceEnum soldGood) {
		this.soldGood = soldGood;
	}
	public Calendar getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Calendar transactionDate) {
		this.transactionDate = transactionDate;
	}
	public CheckinPaymentTransactionInfoData getTransactionInfo() {
		return transactionInfo;
	}
	public void setTransactionInfo(CheckinPaymentTransactionInfoData transactionInfo) {
		this.transactionInfo = transactionInfo;
	}
	public String getVbvParam() {
		return vbvParam;
	}
	public void setVbvParam(String vbvParam) {
		this.vbvParam = vbvParam;
	}
	public CheckinVerifiedByVisaData getVerifiedByVisa() {
		return verifiedByVisa;
	}
	public void setVerifiedByVisa(CheckinVerifiedByVisaData verifiedByVisa) {
		this.verifiedByVisa = verifiedByVisa;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime
				* result
				+ ((applicationSpecificOrderID == null) ? 0
						: applicationSpecificOrderID.hashCode());
		result = prime
				* result
				+ ((authUserFrequentFlyerCode == null) ? 0
						: authUserFrequentFlyerCode.hashCode());
		result = prime * result + ((cartID == null) ? 0 : cartID.hashCode());
		result = prime * result
				+ ((creditCardData == null) ? 0 : creditCardData.hashCode());
		result = prime * result
				+ ((currency == null) ? 0 : currency.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((idAuthEvent == null) ? 0 : idAuthEvent.hashCode());
		result = prime * result
				+ ((methodId == null) ? 0 : methodId.hashCode());
		result = prime * result
				+ ((orderCode == null) ? 0 : orderCode.hashCode());
		result = prime
				* result
				+ ((paymentAttemptCount == null) ? 0 : paymentAttemptCount
						.hashCode());
		result = prime * result
				+ ((persistenceId == null) ? 0 : persistenceId.hashCode());
		result = prime * result
				+ ((saleChannel == null) ? 0 : saleChannel.hashCode());
		result = prime * result
				+ ((soldGood == null) ? 0 : soldGood.hashCode());
		result = prime * result
				+ ((transactionDate == null) ? 0 : transactionDate.hashCode());
		result = prime * result
				+ ((transactionInfo == null) ? 0 : transactionInfo.hashCode());
		result = prime * result
				+ ((vbvParam == null) ? 0 : vbvParam.hashCode());
		result = prime * result
				+ ((verifiedByVisa == null) ? 0 : verifiedByVisa.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinPaymentData other = (CheckinPaymentData) obj;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (applicationSpecificOrderID == null) {
			if (other.applicationSpecificOrderID != null)
				return false;
		} else if (!applicationSpecificOrderID
				.equals(other.applicationSpecificOrderID))
			return false;
		if (authUserFrequentFlyerCode == null) {
			if (other.authUserFrequentFlyerCode != null)
				return false;
		} else if (!authUserFrequentFlyerCode
				.equals(other.authUserFrequentFlyerCode))
			return false;
		if (cartID == null) {
			if (other.cartID != null)
				return false;
		} else if (!cartID.equals(other.cartID))
			return false;
		if (creditCardData == null) {
			if (other.creditCardData != null)
				return false;
		} else if (!creditCardData.equals(other.creditCardData))
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (idAuthEvent == null) {
			if (other.idAuthEvent != null)
				return false;
		} else if (!idAuthEvent.equals(other.idAuthEvent))
			return false;
		if (methodId == null) {
			if (other.methodId != null)
				return false;
		} else if (!methodId.equals(other.methodId))
			return false;
		if (orderCode == null) {
			if (other.orderCode != null)
				return false;
		} else if (!orderCode.equals(other.orderCode))
			return false;
		if (paymentAttemptCount == null) {
			if (other.paymentAttemptCount != null)
				return false;
		} else if (!paymentAttemptCount.equals(other.paymentAttemptCount))
			return false;
		if (persistenceId == null) {
			if (other.persistenceId != null)
				return false;
		} else if (!persistenceId.equals(other.persistenceId))
			return false;
		if (saleChannel != other.saleChannel)
			return false;
		if (soldGood != other.soldGood)
			return false;
		if (transactionDate == null) {
			if (other.transactionDate != null)
				return false;
		} else if (!transactionDate.equals(other.transactionDate))
			return false;
		if (transactionInfo == null) {
			if (other.transactionInfo != null)
				return false;
		} else if (!transactionInfo.equals(other.transactionInfo))
			return false;
		if (vbvParam == null) {
			if (other.vbvParam != null)
				return false;
		} else if (!vbvParam.equals(other.vbvParam))
			return false;
		if (verifiedByVisa == null) {
			if (other.verifiedByVisa != null)
				return false;
		} else if (!verifiedByVisa.equals(other.verifiedByVisa))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CheckinPaymentData [id=" + id + ", amount=" + amount
				+ ", applicationSpecificOrderID=" + applicationSpecificOrderID
				+ ", authUserFrequentFlyerCode=" + authUserFrequentFlyerCode
				+ ", cartID=" + cartID + ", creditCardData=" + creditCardData
				+ ", currency=" + currency + ", description=" + description
				+ ", idAuthEvent=" + idAuthEvent + ", email=" + email
				+ ", methodId=" + methodId + ", orderCode=" + orderCode
				+ ", paymentAttemptCount=" + paymentAttemptCount
				+ ", persistenceId=" + persistenceId + ", saleChannel="
				+ saleChannel + ", soldGood=" + soldGood + ", transactionDate="
				+ transactionDate + ", transactionInfo=" + transactionInfo
				+ ", vbvParam=" + vbvParam + ", verifiedByVisa="
				+ verifiedByVisa + "]";
	}
	
}
