package com.alitalia.aem.common.data.home.enumerations;

public enum MMTierCodeEnum {

    UNKNOWN("UnKnown"),
    BASIC("Basic"),
    ULISSE("Ulisse"),
    FRECCIA_ALATA("FrecciaAlata"),
    PLUS("Plus"),
    ELITE("Elite"),
    ELITEPLUS("Elite Plus");

    private final String value;
    
    MMTierCodeEnum(String v) {
		value = v;
	}
	
	public String value() {
		return value;
	}

	public static MMTierCodeEnum fromValue(String v) {
		for (MMTierCodeEnum c: MMTierCodeEnum.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
