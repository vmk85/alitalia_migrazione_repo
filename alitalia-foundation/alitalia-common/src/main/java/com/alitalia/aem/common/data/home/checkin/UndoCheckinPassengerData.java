package com.alitalia.aem.common.data.home.checkin;

import java.util.List;

public class UndoCheckinPassengerData {	

	private String eticket;
	private String lastname;
	private String name;
	private List<String> messages;
	
	public String getEticket() {
		return eticket;
	}
	public void setEticket(String eticket) {
		this.eticket = eticket;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public List<String> getMessages() {
		return messages;
	}
	public void setMessages(List<String> messages) {
		this.messages = messages;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((eticket == null) ? 0 : eticket.hashCode());
		result = prime * result
				+ ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result
				+ ((messages == null) ? 0 : messages.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UndoCheckinPassengerData other = (UndoCheckinPassengerData) obj;
		if (eticket == null) {
			if (other.eticket != null)
				return false;
		} else if (!eticket.equals(other.eticket))
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		if (messages == null) {
			if (other.messages != null)
				return false;
		} else if (!messages.equals(other.messages))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "UndoCheckinPassengerData [eticket=" + eticket + ", lastname="
				+ lastname + ", messages=" + messages + ", name=" + name + "]";
	}
}