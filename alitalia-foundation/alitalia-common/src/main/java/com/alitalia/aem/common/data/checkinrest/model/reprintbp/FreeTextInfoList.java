
package com.alitalia.aem.common.data.checkinrest.model.reprintbp;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FreeTextInfoList {

    @SerializedName("freeTextInfoList")
    @Expose
    private List<FreeTextInfoList_> freeTextInfoList = null;
    @SerializedName("itineraryPassengerList")
    @Expose
    private List<ItineraryPassengerList> itineraryPassengerList = null;
    @SerializedName("pectabDataList")
    @Expose
    private List<PectabDataList> pectabDataList = null;
    @SerializedName("result")
    @Expose
    private Result result;

    public List<FreeTextInfoList_> getFreeTextInfoList() {
        return freeTextInfoList;
    }

    public void setFreeTextInfoList(List<FreeTextInfoList_> freeTextInfoList) {
        this.freeTextInfoList = freeTextInfoList;
    }

    public List<ItineraryPassengerList> getItineraryPassengerList() {
        return itineraryPassengerList;
    }

    public void setItineraryPassengerList(List<ItineraryPassengerList> itineraryPassengerList) {
        this.itineraryPassengerList = itineraryPassengerList;
    }

    public List<PectabDataList> getPectabDataList() {
        return pectabDataList;
    }

    public void setPectabDataList(List<PectabDataList> pectabDataList) {
        this.pectabDataList = pectabDataList;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

}
