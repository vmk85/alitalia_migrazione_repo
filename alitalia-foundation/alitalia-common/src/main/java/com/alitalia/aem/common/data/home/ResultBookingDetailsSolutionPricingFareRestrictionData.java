package com.alitalia.aem.common.data.home;

public class ResultBookingDetailsSolutionPricingFareRestrictionData {
	
	private ResultBookingDetailsSolutionPricingFareRestrictionEarliestReturnData earliestReturnField;
	private ResultBookingDetailsSolutionPricingFareRestrictionLatestReturnData latestReturnField;
	
	public ResultBookingDetailsSolutionPricingFareRestrictionEarliestReturnData getEarliestReturnField() {
		return earliestReturnField;
	}
	
	public void setEarliestReturnField(
			ResultBookingDetailsSolutionPricingFareRestrictionEarliestReturnData earliestReturnField) {
		this.earliestReturnField = earliestReturnField;
	}
	
	public ResultBookingDetailsSolutionPricingFareRestrictionLatestReturnData getLatestReturnField() {
		return latestReturnField;
	}
	
	public void setLatestReturnField(
			ResultBookingDetailsSolutionPricingFareRestrictionLatestReturnData latestReturnField) {
		this.latestReturnField = latestReturnField;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((earliestReturnField == null) ? 0 : earliestReturnField
						.hashCode());
		result = prime
				* result
				+ ((latestReturnField == null) ? 0 : latestReturnField
						.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingDetailsSolutionPricingFareRestrictionData other = (ResultBookingDetailsSolutionPricingFareRestrictionData) obj;
		if (earliestReturnField == null) {
			if (other.earliestReturnField != null)
				return false;
		} else if (!earliestReturnField.equals(other.earliestReturnField))
			return false;
		if (latestReturnField == null) {
			if (other.latestReturnField != null)
				return false;
		} else if (!latestReturnField.equals(other.latestReturnField))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingDetailsSolutionPricingFareRestrictionData [earliestReturnField="
				+ earliestReturnField
				+ ", latestReturnField="
				+ latestReturnField + "]";
	}
	
}
