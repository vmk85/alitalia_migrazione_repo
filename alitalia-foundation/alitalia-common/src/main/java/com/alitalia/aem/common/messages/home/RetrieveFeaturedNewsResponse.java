package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.NewsData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetrieveFeaturedNewsResponse extends BaseResponse{

	private List<NewsData> news;

	public List<NewsData> getNews() {
		return news;
	}

	public void setNews(List<NewsData> news) {
		this.news = news;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveFeaturedNewsResponse other = (RetrieveFeaturedNewsResponse) obj;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveNewsResponse [news=" + news + "]";
	}
}