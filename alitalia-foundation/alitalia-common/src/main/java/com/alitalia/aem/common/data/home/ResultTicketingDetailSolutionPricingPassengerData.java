package com.alitalia.aem.common.data.home;

import java.util.List;

public class ResultTicketingDetailSolutionPricingPassengerData {

    private String idField;
    private List<String> ptcField;

    public String getIdField() {
        return idField;
    }

    public void setIdField(String value) {
        this.idField = value;
    }

    public List<String> getPtcField() {
        return ptcField;
    }

    public void setPtcField(List<String> value) {
        this.ptcField = value;
    }

}
