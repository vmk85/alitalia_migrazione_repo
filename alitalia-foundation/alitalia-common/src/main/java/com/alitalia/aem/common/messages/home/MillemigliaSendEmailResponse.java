package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.BaseResponse;

public class MillemigliaSendEmailResponse extends BaseResponse {

	private boolean mailSent;
	private MMCustomerProfileData customerProfile;
	
	public boolean isMailSent() {
		return mailSent;
	}
	
	public void setMailSent(boolean mailSent) {
		this.mailSent = mailSent;
	}
	
	public MMCustomerProfileData getCustomerProfile() {
		return customerProfile;
	}
	
	public void setCustomerProfile(MMCustomerProfileData customerProfile) {
		this.customerProfile = customerProfile;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((customerProfile == null) ? 0 : customerProfile.hashCode());
		result = prime * result + (mailSent ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MillemigliaSendEmailResponse other = (MillemigliaSendEmailResponse) obj;
		if (customerProfile == null) {
			if (other.customerProfile != null)
				return false;
		} else if (!customerProfile.equals(other.customerProfile))
			return false;
		if (mailSent != other.mailSent)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SendEmailResponse [mailSent=" + mailSent + ", customerProfile="
				+ customerProfile + "]";
	}
}