package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class SetDatiSicurezzaProfiloResponse extends BaseResponse {


	private Boolean flagIDProfiloSicurezzaSaved;
	private Integer returnCode;
	private String errorMessage;

	public Integer getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(Integer returnCode) {
		this.returnCode = returnCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}


	public Boolean getFlagIDProfiloSicurezzaSaved() {
		return flagIDProfiloSicurezzaSaved;
	}

	public void setflagIDProfiloSicurezzaSaved(Boolean flagIDProfiloSicurezzaSaved) {
		this.flagIDProfiloSicurezzaSaved = flagIDProfiloSicurezzaSaved;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
//		result = prime * result
//				+ ((customerProfile == null) ? 0 : customerProfile.hashCode());
		result = prime * result
				+ ((flagIDProfiloSicurezzaSaved == null) ? 0 : flagIDProfiloSicurezzaSaved.hashCode());

		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SetDatiSicurezzaProfiloResponse other = (SetDatiSicurezzaProfiloResponse) obj;

//		if (customerProfile == null) {
//			if (other.customerProfile != null)
//				return false;
//		} else if (!customerProfile.equals(other.customerProfile))
//			return false;

		if (flagIDProfiloSicurezzaSaved == null) {
			if (other.flagIDProfiloSicurezzaSaved != null)
				return false;
		} else if (!flagIDProfiloSicurezzaSaved.equals(other.flagIDProfiloSicurezzaSaved))
			return false;

		return true;
	}

	@Override
	public String toString() {
//		return "SubscribeResponse [customerProfile=" + customerProfile
//				+ ", returnCode=" + returnCode + ", errorMessage="
//				+ errorMessage + ", getTid()=" + getTid() + ", getSid()="
//				+ getSid() + "]";

		return "SubscribeSACheckUserNameDuplicateResponse [flagIDProfiloSicurezzaSaved=" + flagIDProfiloSicurezzaSaved
				+ ", getTid()=" + getTid() + ", getSid()="
				+ getSid() + "]";
	}

}