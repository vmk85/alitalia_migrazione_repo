package com.alitalia.aem.common.data.home;

public class MMCustomerSA {

    private MMCustomerProfileData mmCustomerProfileData;
    private int accessFailureCount;
    private String certifiedCountryNumber;
    private String certifiedEmailAccount;
    private String certifiedPhoneAccount;
    private boolean emailCertificateFlag;
    private boolean flagOldCustomer;
    private boolean flagRememberPassword;
    private boolean flagVerificaPassword;
    private boolean flagVerificaRisposta;
    private boolean flagVerificaUsername;
    private boolean lockedout;
    private String password;
    private boolean phoneCertificateFlag;
    private String secureAnswer;
    private int secureQuestionID;
    private String username;


    public MMCustomerProfileData getMmCustomerProfileData() {
        return mmCustomerProfileData;
    }

    public void setMmCustomerProfileData(MMCustomerProfileData mmCustomerProfileData) {
        this.mmCustomerProfileData = mmCustomerProfileData;
    }

    public int getAccessFailureCount() {
        return accessFailureCount;
    }

    public void setAccessFailureCount(int accessFailureCount) {
        this.accessFailureCount = accessFailureCount;
    }

    public String getCertifiedCountryNumber() {
        return certifiedCountryNumber;
    }

    public void setCertifiedCountryNumber(String certifiedCountryNumber) {
        this.certifiedCountryNumber = certifiedCountryNumber;
    }

    public String getCertifiedEmailAccount() {
        return certifiedEmailAccount;
    }

    public void setCertifiedEmailAccount(String certifiedEmailAccount) {
        this.certifiedEmailAccount = certifiedEmailAccount;
    }

    public String getCertifiedPhoneAccount() {
        return certifiedPhoneAccount;
    }

    public void setCertifiedPhoneAccount(String certifiedPhoneAccount) {
        this.certifiedPhoneAccount = certifiedPhoneAccount;
    }

    public boolean isEmailCertificateFlag() {
        return emailCertificateFlag;
    }

    public void setEmailCertificateFlag(boolean emailCertificateFlag) {
        this.emailCertificateFlag = emailCertificateFlag;
    }

    public boolean isFlagOldCustomer() {
        return flagOldCustomer;
    }

    public void setFlagOldCustomer(boolean flagOldCustomer) {
        this.flagOldCustomer = flagOldCustomer;
    }

    public boolean isFlagRememberPassword() {
        return flagRememberPassword;
    }

    public void setFlagRememberPassword(boolean flagRememberPassword) {
        this.flagRememberPassword = flagRememberPassword;
    }

    public boolean isFlagVerificaPassword() {
        return flagVerificaPassword;
    }

    public void setFlagVerificaPassword(boolean flagVerificaPassword) {
        this.flagVerificaPassword = flagVerificaPassword;
    }

    public boolean isFlagVerificaRisposta() {
        return flagVerificaRisposta;
    }

    public void setFlagVerificaRisposta(boolean flagVerificaRisposta) {
        this.flagVerificaRisposta = flagVerificaRisposta;
    }

    public boolean isFlagVerificaUsername() {
        return flagVerificaUsername;
    }

    public void setFlagVerificaUsername(boolean flagVerificaUsername) {
        this.flagVerificaUsername = flagVerificaUsername;
    }

    public boolean isLockedout() {
        return lockedout;
    }

    public void setLockedout(boolean lockedout) {
        this.lockedout = lockedout;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isPhoneCertificateFlag() {
        return phoneCertificateFlag;
    }

    public void setPhoneCertificateFlag(boolean phoneCertificateFlag) {
        this.phoneCertificateFlag = phoneCertificateFlag;
    }

    public String getSecureAnswer() {
        return secureAnswer;
    }

    public void setSecureAnswer(String secureAnswer) {
        this.secureAnswer = secureAnswer;
    }

    public int getSecureQuestionID() {
        return secureQuestionID;
    }

    public void setSecureQuestionID(int secureQuestionID) {
        this.secureQuestionID = secureQuestionID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
