package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class CheckinSendSmsRequest extends BaseRequest {

   private String phoneNumber;
   private String link;
   private String fromPhone;
   private String applicationName;
   private Boolean notificationSend;
   private Boolean notificationLocalSend;
   private Boolean notificationReceive;
   private Boolean notificationNotReceive;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getFromPhone() {
        return fromPhone;
    }

    public void setFromPhone(String fromPhone) {
        this.fromPhone = fromPhone;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public Boolean getNotificationSend() {
        return notificationSend;
    }

    public void setNotificationSend(Boolean notificationSend) {
        this.notificationSend = notificationSend;
    }

    public Boolean getNotificationLocalSend() {
        return notificationLocalSend;
    }

    public void setNotificationLocalSend(Boolean notificationLocalSend) {
        this.notificationLocalSend = notificationLocalSend;
    }

    public Boolean getNotificationReceive() {
        return notificationReceive;
    }

    public void setNotificationReceive(Boolean notificationReceive) {
        this.notificationReceive = notificationReceive;
    }

    public Boolean getNotificationNotReceive() {
        return notificationNotReceive;
    }

    public void setNotificationNotReceive(Boolean notificationNotReceive) {
        this.notificationNotReceive = notificationNotReceive;
    }
}


