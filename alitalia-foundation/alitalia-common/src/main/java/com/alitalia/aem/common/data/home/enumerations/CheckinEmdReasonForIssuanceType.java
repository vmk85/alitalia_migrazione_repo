package com.alitalia.aem.common.data.home.enumerations;

public enum CheckinEmdReasonForIssuanceType {

	FIRST_BAGGAGE("FirstBaggage"),
    EXTRA_BAGGAGE("ExtraBaggage"),
    CONFORT_SEAT("ConfortSeat"),
    UPGRADE("Upgrade");

	private final String value;

    CheckinEmdReasonForIssuanceType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CheckinEmdReasonForIssuanceType fromValue(String v) {
        for (CheckinEmdReasonForIssuanceType c: CheckinEmdReasonForIssuanceType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
