package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.MmbBaseRequest;

public class WebCheckinStatesRequest extends MmbBaseRequest {

	private String countryCode;

	public WebCheckinStatesRequest(String tid, String sid) {
		super(tid, sid);
	}

	public WebCheckinStatesRequest() {
		super();
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((countryCode == null) ? 0 : countryCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebCheckinStatesRequest other = (WebCheckinStatesRequest) obj;
		if (countryCode == null) {
			if (other.countryCode != null)
				return false;
		} else if (!countryCode.equals(other.countryCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WebCheckinStatesRequest [countryCode=" + countryCode
				+ ", toString()=" + super.toString() + "]";
	}
}