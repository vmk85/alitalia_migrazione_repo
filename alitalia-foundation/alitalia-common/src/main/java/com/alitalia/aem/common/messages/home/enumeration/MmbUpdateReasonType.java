package com.alitalia.aem.common.messages.home.enumeration;

public enum MmbUpdateReasonType {

	CHANGE_SEAT("ChangeSeat"),
	MANAGE_MY_BOOKING("ManageMyBooking");
    private final String value;

    MmbUpdateReasonType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MmbUpdateReasonType fromValue(String v) {
        for (MmbUpdateReasonType c: MmbUpdateReasonType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
