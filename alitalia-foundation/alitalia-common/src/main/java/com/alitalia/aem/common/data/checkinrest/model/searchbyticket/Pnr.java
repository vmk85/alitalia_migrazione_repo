
package com.alitalia.aem.common.data.checkinrest.model.searchbyticket;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pnr {

    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("flights")
    @Expose
    private List<Flight> flights = null;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public List<Flight> getFlights() {
        return flights;
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
    }

}
