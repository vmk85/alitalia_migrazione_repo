
package com.alitalia.aem.common.data.checkinrest.model.endpayment.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AuthorizationResult {

    @SerializedName("approvalCode")
    @Expose
    private String approvalCode;
    @SerializedName("supplierID")
    @Expose
    private String supplierID;
    @SerializedName("supplierTransID")
    @Expose
    private String supplierTransID;
    @SerializedName("supplierReferenceID")
    @Expose
    private String supplierReferenceID;
    @SerializedName("authRemarks1")
    @Expose
    private String authRemarks1;
    @SerializedName("authRemarks2")
    @Expose
    private String authRemarks2;
    @SerializedName("paymentRef")
    @Expose
    private String paymentRef;
    @SerializedName("insurancePayment")
    @Expose
    private InsurancePayment insurancePayment;

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public String getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(String supplierID) {
        this.supplierID = supplierID;
    }

    public String getSupplierTransID() {
        return supplierTransID;
    }

    public void setSupplierTransID(String supplierTransID) {
        this.supplierTransID = supplierTransID;
    }

    public String getSupplierReferenceID() {
        return supplierReferenceID;
    }

    public void setSupplierReferenceID(String supplierReferenceID) {
        this.supplierReferenceID = supplierReferenceID;
    }

    public String getAuthRemarks1() {
        return authRemarks1;
    }

    public void setAuthRemarks1(String authRemarks1) {
        this.authRemarks1 = authRemarks1;
    }

    public String getAuthRemarks2() {
        return authRemarks2;
    }

    public void setAuthRemarks2(String authRemarks2) {
        this.authRemarks2 = authRemarks2;
    }

    public String getPaymentRef() {
        return paymentRef;
    }

    public void setPaymentRef(String paymentRef) {
        this.paymentRef = paymentRef;
    }

    public InsurancePayment getInsurancePayment() {
        return insurancePayment;
    }

    public void setInsurancePayment(InsurancePayment insurancePayment) {
        this.insurancePayment = insurancePayment;
    }

}
