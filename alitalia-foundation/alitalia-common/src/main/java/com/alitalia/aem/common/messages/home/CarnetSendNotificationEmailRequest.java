package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.InfoCarnetData;
import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.common.messages.BaseRequest;

public class CarnetSendNotificationEmailRequest extends BaseRequest {

    private InfoCarnetData carnetInfo;
    private String languageCode;
    private String marketCode;
    private String notificationMailHtml;
    private RoutesData prenotation;

	public CarnetSendNotificationEmailRequest(String tid, String sid) {
		super(tid, sid);
	}

	public CarnetSendNotificationEmailRequest() {
		super();
	}

	public InfoCarnetData getCarnetInfo() {
		return carnetInfo;
	}

	public void setCarnetInfo(InfoCarnetData carnetInfo) {
		this.carnetInfo = carnetInfo;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getMarketCode() {
		return marketCode;
	}

	public void setMarketCode(String marketCode) {
		this.marketCode = marketCode;
	}

	public String getNotificationMailHtml() {
		return notificationMailHtml;
	}

	public void setNotificationMailHtml(String notificationMailHtml) {
		this.notificationMailHtml = notificationMailHtml;
	}

	public RoutesData getPrenotation() {
		return prenotation;
	}

	public void setPrenotation(RoutesData prenotation) {
		this.prenotation = prenotation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((carnetInfo == null) ? 0 : carnetInfo.hashCode());
		result = prime * result
				+ ((languageCode == null) ? 0 : languageCode.hashCode());
		result = prime * result
				+ ((marketCode == null) ? 0 : marketCode.hashCode());
		result = prime
				* result
				+ ((notificationMailHtml == null) ? 0 : notificationMailHtml
						.hashCode());
		result = prime * result
				+ ((prenotation == null) ? 0 : prenotation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarnetSendNotificationEmailRequest other = (CarnetSendNotificationEmailRequest) obj;
		if (carnetInfo == null) {
			if (other.carnetInfo != null)
				return false;
		} else if (!carnetInfo.equals(other.carnetInfo))
			return false;
		if (languageCode == null) {
			if (other.languageCode != null)
				return false;
		} else if (!languageCode.equals(other.languageCode))
			return false;
		if (marketCode == null) {
			if (other.marketCode != null)
				return false;
		} else if (!marketCode.equals(other.marketCode))
			return false;
		if (notificationMailHtml == null) {
			if (other.notificationMailHtml != null)
				return false;
		} else if (!notificationMailHtml.equals(other.notificationMailHtml))
			return false;
		if (prenotation == null) {
			if (other.prenotation != null)
				return false;
		} else if (!prenotation.equals(other.prenotation))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CarnetSendNotificationEmailRequest [carnetInfo=" + carnetInfo
				+ ", languageCode=" + languageCode + ", marketCode="
				+ marketCode + ", notificationMailHtml=" + notificationMailHtml
				+ ", prenotation=" + prenotation + "]";
	}
}