package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class SendReminderCheckinResponse extends BaseResponse{

	private Boolean sendReminderResult;
	
	public SendReminderCheckinResponse(){}
	
	public SendReminderCheckinResponse(String tid, String sid) {
		super(tid, sid);
	}

	public Boolean getSendReminderResult() {
		return sendReminderResult;
	}

	public void setSendReminderResult(Boolean sendReminderResult) {
		this.sendReminderResult = sendReminderResult;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((sendReminderResult == null) ? 0 : sendReminderResult
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SendReminderCheckinResponse other = (SendReminderCheckinResponse) obj;
		if (sendReminderResult == null) {
			if (other.sendReminderResult != null)
				return false;
		} else if (!sendReminderResult.equals(other.sendReminderResult))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SendReminderCheckinResponse [sendReminderResult="
				+ sendReminderResult + ", toString()=" + super.toString() + "]";
	}
}