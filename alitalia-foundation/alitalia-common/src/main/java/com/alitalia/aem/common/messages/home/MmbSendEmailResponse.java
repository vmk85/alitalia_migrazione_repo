package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class MmbSendEmailResponse extends BaseResponse {

	private boolean mailSent;
	
	public boolean isMailSent() {
		return mailSent;
	}
	
	public void setMailSent(boolean mailSent) {
		this.mailSent = mailSent;
	}

	@Override
	public String toString() {
		return "MmbSendEmailResponse [mailSent=" + mailSent + ", getTid()="
				+ getTid() + ", getSid()=" + getSid() + "]";
	}
	
}