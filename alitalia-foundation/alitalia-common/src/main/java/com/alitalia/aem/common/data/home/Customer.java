package com.alitalia.aem.common.data.home;

import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PnrListInfoByFrequentFlyerSearch;
import com.alitalia.aem.common.data.home.enumerations.MMAddressTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMGenderTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMPhoneTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMWorkPositionTypeEnum;
import com.alitalia.aem.common.data.home.mmb.ProfileComplete;
import com.alitalia.aem.common.utils.StringUtils;
import com.google.common.collect.Lists;
import com.day.cq.i18n.I18n;

import java.text.SimpleDateFormat;
import java.util.*;
import java.text.DateFormatSymbols;

import static com.alitalia.aem.common.utils.StringUtils.EMPTY;

public class Customer {

    public static final String MEAL_CATEGORY = "002";
    public static final String SEAT_CATEGORY = "003";

    public static String getMealCategory() {
        return MEAL_CATEGORY;
    }

    public static String getSeatCategory() {
        return SEAT_CATEGORY;
    }

    public I18n getI18n() {
        return i18n;
    }

    public void setI18n(I18n i18n) {
        this.i18n = i18n;
    }

    public MMCustomerProfileData getMmCustomer() {
        return mmCustomer;
    }

    enum Tipo {MA, MM, BOTH, NONE}
    ;

    private I18n i18n;

    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");

    private Map<String, Boolean> frequentFlyerCompaniesSelected;

    MMCustomerProfileData mmCustomer;

    MACustomer maCustomer;

    private PnrListInfoByFrequentFlyerSearch pnrData;
    public void seti18n(I18n i18n) {this.i18n = i18n; }

    public void setMaCustomer(MACustomer maCustomer) {
        this.maCustomer = maCustomer;
    }

    public void setMmCustomer(MMCustomerProfileData mmCustomer) {
        this.mmCustomer = mmCustomer;
    }

    public void setPnrData(PnrListInfoByFrequentFlyerSearch pnrData) {
        this.pnrData = pnrData;
    }

    public PnrListInfoByFrequentFlyerSearch getPnrData() {
        return pnrData;
    }

    public void setFrequentFlyerCompaniesSelected(Map<String, Boolean> frequentFlyerCompaniesSelected) {
        this.frequentFlyerCompaniesSelected = frequentFlyerCompaniesSelected;
    }

    public Map<String, Boolean> getFrequentFlyerCompaniesSelected() {
        return frequentFlyerCompaniesSelected;
    }

    public MACustomer getMaCustomer() {
        return maCustomer;
    }

    public Tipo getTipo() {
        if (mmCustomer != null && maCustomer != null) {
            return Tipo.BOTH;
        } else if (mmCustomer != null) {
            return Tipo.MM;
        } else if (maCustomer != null) {
            return Tipo.MA;
        }
        return Tipo.NONE;
    }

    public boolean isMM() {
        return getTipo() == Tipo.MM || getTipo() == Tipo.BOTH;
    }

    public String getNome() {
        if (mmCustomer != null) {
            return StringUtils.capitalize(mmCustomer.getCustomerName());
        }

        if (maCustomer != null) {
            return StringUtils.capitalize(maCustomer.getProfile().getFirstName());
        }
        return EMPTY;
    }

    public String getSecondoNome() {
        // TODO
        // if (mmCustomer != null) {
        //    return StringUtils.capitalize(mmCustomer.getCustomerName());
        //}

        if (maCustomer != null) {
            return StringUtils.capitalize(maCustomer.getData().getSecondoName());
        }
        return EMPTY;
    }

    public String getCognome() {
        if (mmCustomer != null) {
            return StringUtils.capitalize(mmCustomer.getCustomerSurname());
        }

        if (maCustomer != null) {
            return StringUtils.capitalize(maCustomer.getProfile().getLastName());
        }
        return EMPTY;
    }


    public String getSesso() {
        if (mmCustomer != null) {
            return mmCustomer.getGender().toString();
        }
        if (maCustomer != null) {
            String value = maCustomer.getProfile().getGender();
            return value;
        }
        return MMGenderTypeEnum.UNKNOWN.toString();
    }

    public boolean isIndirizzoBusiness() {

        MMAddressTypeEnum result = MMAddressTypeEnum.UNKNOWN;

        if (mmCustomer != null) {
            if (mmCustomer.getAddresses().isEmpty()) {
                result = MMAddressTypeEnum.UNKNOWN ;
            }
            result = mmCustomer.getAddresses().get(0).getAddressType();
        }

        if (maCustomer != null) {
            if ("company".equalsIgnoreCase(maCustomer.getData().getAddress1_Type())) {
                result = MMAddressTypeEnum.BUSINESS;
            } else {
                result = MMAddressTypeEnum.HOME;
            }
        }
        return MMAddressTypeEnum.BUSINESS == result;
    }

    public MMAddressData getHomeAddress() {
        MMAddressData result = null;

        if (mmCustomer != null) {
            result = getMMAddress(MMAddressTypeEnum.HOME);
        } else if (maCustomer != null && maCustomer.getData() != null) {
            if ("home".equals(maCustomer.getData().getAddress1_Type())) {
                result = new MMAddressData();
                result.setAddressType(MMAddressTypeEnum.HOME);
                result.setPostalCode(maCustomer.getData().getAddress1_Zip());
                result.setStreetFreeText(maCustomer.getData().getAddress1_Address());
                result.setCountry(maCustomer.getData().getAddress1_CountryCode());
                result.setStateCode(maCustomer.getData().getAddress1_StateCode());
                result.setMunicipalityName(maCustomer.getData().getAddress1_City());
            }
        } else {
            result = new MMAddressData();
            result.setAddressType(MMAddressTypeEnum.HOME);
        }
        return result;
    }

    public String getEmail() {
        if (mmCustomer != null) {
            return mmCustomer.getEmail();
        }
        if (maCustomer != null) {
            return maCustomer.getProfile().getEmail();
        }
        return "";
    }

    public MMTelephoneData getTelephone() {
        if (mmCustomer != null) {
            if (mmCustomer.getTelephones() != null) {
                for (MMTelephoneData telephoneData : mmCustomer.getTelephones()) {
                    if (!telephoneData.getTelephoneType().equals(MMPhoneTypeEnum.EMAIL)) {
                        return telephoneData;
                    }
                }
            }
        }
        if (maCustomer != null) {
            MMTelephoneData telData = new MMTelephoneData();
            if (maCustomer.getData().getPhone1Number() != null) {
                telData.setNumber(maCustomer.getData().getPhone1Number());
            }
            if (maCustomer.getData().getPhone1AreaCode()!= null) {
                telData.setPhoneZone(maCustomer.getData().getPhone1AreaCode());
            }
            if (maCustomer.getData().getPhone1CountryCode() != null) {
                telData.setCountryNumber(maCustomer.getData().getPhone1CountryCode());
            }
            if (maCustomer.getData().getPhone1Type() != null) {
                if(!maCustomer.getData().getPhone1Type().equals("")){
                    telData.setTelephoneType(MMPhoneTypeEnum.fromValue(maCustomer.getData().getPhone1Type()));
                }
            }
            return telData;
        }
        return null;

    }

    public String getSaldoMiglia(){
        if(mmCustomer != null){
            return String.format("%,8d",mmCustomer.getPointsEarnedTotal()).replace(',','.');
        }else{
            return "";
        }
    }

    public String getMigliaAccumulate(){
        if(mmCustomer != null){
            return String.format("%,8d",mmCustomer.getPointsEarnedTotalQualified()).replace(',','.');
        }else{
            return "";
        }
    }


    public String getMigliaSpese(){
        if(mmCustomer != null){
            return String.format("%,8d",mmCustomer.getPointsSpentTotal()).replace(',','.');
        }else{
            return "";
        }
    }


    public String getMigliaRimanenti(){
        if(mmCustomer != null){
            return String.format("%,8d",mmCustomer.getPointsRemainingTotal()).replace(',','.');
        }else{
            return "";
        }
    }


    public String getTierCode(){
        if(mmCustomer != null){
            return mmCustomer.getTierCode().toString();
        }else{
            return "";
        }
    }

    public String getQualifiedFlights(){
        if(mmCustomer != null){
            return mmCustomer.getQualifiedFlights().toString();
        }else{
            return "";
        }
    }

    public String getBirthDate(){
        if(mmCustomer != null){
            if(mmCustomer.getBirthDate()!=null){
                return dateFormatter.format(mmCustomer.getBirthDate().getTime());
            }else{
                return "";
            }
        }
        if(maCustomer!=null){
            try {
                int year = Integer.parseInt(maCustomer.getProfile().getBirthYear());
                int month = Integer.parseInt(maCustomer.getProfile().getBirthMonth());
                int day = Integer.parseInt(maCustomer.getProfile().getBirthDay());

                Date birthDate = new GregorianCalendar(year, month - 1, day).getTime();
                return dateFormatter.format(birthDate);
            }catch(Exception e){
                return "";
            }
        }
        return "";
    }

    public String getBirthDateDay(){
        if(maCustomer != null){
            if(maCustomer.getProfile().getBirthDay()!=null){
                String day = maCustomer.getProfile().getBirthDay();
                if(Integer.parseInt(day)<10){
                    day = "0"+day;
                }
                return day;
            }else{
                return "";
            }
        }else{
            return "";
        }
    }
    public String getBirthDateMonth(){
        if(maCustomer != null){
            if(maCustomer.getProfile().getBirthMonth()!=null){
                String month = maCustomer.getProfile().getBirthMonth();
                if(Integer.parseInt(month)<10){
                    month = "0"+month;
                }
                return month;
            }else{
                return "";
            }
        }else{
            return "";
        }
    }
    public String getBirthDateYear(){
        if(maCustomer != null){
            if(maCustomer.getProfile().getBirthYear()!=null){
                return maCustomer.getProfile().getBirthYear();
            }else{
                return "";
            }
        }else{
            return "";
        }
    }

    public String getExpirationDate(){
        if(mmCustomer != null){
            Calendar expDate = mmCustomer.getExpirationDate();
            int day = expDate.get(Calendar.DAY_OF_MONTH);
            int month = expDate.get(Calendar.MONTH);
            int year = expDate.get(Calendar.YEAR);
            String monthString = new DateFormatSymbols().getMonths()[month].toLowerCase();
            String finalDate = String.valueOf(day) + " " + i18n.get("common.monthsOfYear."+monthString) + " " + String.valueOf(year);
            return finalDate;
        }else{
            return "";
        }
    }


    public MMAddressData getBusinessAddress() {
        MMAddressData result = null;

        if (mmCustomer != null) {
            result = getMMAddress(MMAddressTypeEnum.BUSINESS);
        } else if (maCustomer != null && maCustomer.getData() != null) {
            if ("company".equalsIgnoreCase(maCustomer.getData().getAddress1_Type())) {
                result = new MMAddressData();
                result.setAddressType(MMAddressTypeEnum.BUSINESS);
                result.setCompanyName(maCustomer.getData().getAddress1_CompanyName());
                result.setPostalCode(maCustomer.getData().getAddress1_Zip());
                result.setStreetFreeText(maCustomer.getData().getAddress1_Address());

                result.setMunicipalityName(maCustomer.getData().getAddress1_City() );
                result.setStateCode(maCustomer.getData().getAddress1_StateCode());
                result.setCountry( maCustomer.getData().getAddress1_CountryCode());
            }
        } else {
            result = new MMAddressData();
            result.setAddressType(MMAddressTypeEnum.BUSINESS);
        }
        return result;
    }


    private MMAddressData getMMAddress(MMAddressTypeEnum type) {
        for (MMAddressData addressData : mmCustomer.getAddresses()) {
            if (type == addressData.getAddressType()) {
                return addressData;
            }
        }

        MMAddressData result = new MMAddressData();
        result.setAddressType(type);
        return result;
    }

    /**
     * @return elenco delle professioni. Sembra che sly non accetti un array, quindi torno una lista. Utlizzato per popolare la combo
     */
    public List<MMWorkPositionTypeEnum> getProfessioni() {
        return Lists.newArrayList(MMWorkPositionTypeEnum.values());
    }

    public List<String> getSessi() {
        if(getTipo().toString().equalsIgnoreCase(("MA"))) {
            List<String> sessi = new ArrayList<>();
            sessi.add("u"); sessi.add("m"); sessi.add("f");
            return sessi;
        }
        else return Lists.newArrayList(MMGenderTypeEnum.values().toString());
    }

    public MMWorkPositionTypeEnum getProfessione() {
        if (mmCustomer != null) {
            return mmCustomer.getCustomerWorkPosition();
        }

        if (maCustomer != null && maCustomer.getProfile().getProfessionalHeadline() != null ) {
            try {
                return MMWorkPositionTypeEnum.fromValue(maCustomer.getProfile().getProfessionalHeadline());
            } catch (IllegalArgumentException ex) {
                return MMWorkPositionTypeEnum.UNKNOWN;
            }
        }
        return MMWorkPositionTypeEnum.UNKNOWN;
    }

    public MMTier getMMLevel(){
        String tier = getTierCode();
        switch(tier.toLowerCase()) {
            case "basic":
                MMTier tierObj = new MMTier(1, "Basic","/etc/designs/alitalia/clientlibs-myalitalia/images/ico-freccia-alata.svg");
                return tierObj;
            case "ulisse":
                tierObj = new MMTier(2, "Basic","/etc/designs/alitalia/clientlibs-myalitalia/images/ico-freccia-alata.svg");
                return tierObj;
            case "frecciaalata":
                tierObj = new MMTier(3, "Freccia Alata","/etc/designs/alitalia/clientlibs-myalitalia/images/ico-freccia-alata.svg");
                return tierObj;
            case "plus":
                tierObj = new MMTier(4, "Plus","/etc/designs/alitalia/clientlibs-myalitalia/images/ico-freccia-alata.svg");
                return tierObj;
            default:
                return null;
        }
    }

    public MMPreferenceData getMealPreferences() {
        if (this.mmCustomer.getPreferences() != null) {
            for (MMPreferenceData preferenceData : this.mmCustomer.getPreferences()) {
                if (preferenceData.getIdCategory().equals(MEAL_CATEGORY)) {
                    return preferenceData;
                }
            }
        }
        return null;
    }

    public MMPreferenceData getSeatPreferences() {
        if (this.mmCustomer.getPreferences() != null) {
            for (MMPreferenceData preferenceData : this.mmCustomer.getPreferences()) {
                if (preferenceData.getIdCategory().equals(SEAT_CATEGORY)) {
                    return preferenceData;
                }
            }
        }
        return null;
    }

    public boolean getProfiling() {
        try {
            if(getTipo().toString().equals("MA")) {
                return maCustomer.getData().isAuth_Profile();
            } else if(getTipo().toString().equals("MM") || getTipo().toString().equals("BOTH")) {
                return mmCustomer.getProfiling();
            } else return false;
        } catch(Exception e) {
            return false;
        }
    }

    public String getMailingTypeCode() {
        try {
            if (this.getTipo().toString().equals("MA")){
                return maCustomer.getData().getNewsletterType().concat(":").concat(maCustomer.getData().getGeneralPref_Channel());
            }
            else if (this.getTipo().toString().equals(("MM")) || getTipo().equals("BOTH"))
                return mmCustomer.getMailingType();
            return "";
        } catch (Exception e) {
            return "";
        }
    }

    public boolean getNewsletter() {
        try {
            if (this.getTipo().toString().equals("MA")) return maCustomer.getData().isAuth_Com();
            else if (this.getTipo().toString().equals(("MM")) || getTipo().equals("BOTH"))
                return (mmCustomer.getMailingType() != null || mmCustomer.getMailingType() != "005");
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean getConsensoDatiPersonali() {
        try {
            if (this.getTipo().toString().equals("MA")) return maCustomer.getData().isAuth_Data();
            else if (this.getTipo().toString().equals(("MM")) || getTipo().equals("BOTH"))
                return (mmCustomer.getMailingType() != null || mmCustomer.getMailingType() != "005");
            return false;
        } catch (Exception e) {
            return false;
        }
    }
//
//    public String getChannel() {
//        try {
//            switch (getTipo().toString()) {
//                case "MA": return maCustomer.getData().getGeneralPref_Channel();
//                case "BOTH":
//                case "MM":
//                    switch(mmCustomer.getMailingType()) {
//                        case "002": case "004": return "1";
//                        case "006": return "2";
//                        default: return "0";
//                    }
//                default: return "0";
//            }
//        } catch (Exception e) {
//            return "none";
//        }
//    }

    public boolean getSmsAuthorization() {
        try {
            if (this.getTipo().toString().equals("MA")) return maCustomer.getData().getSmsAuthorization();
            else if (this.getTipo().toString().equals(("MM")) || getTipo().equals("BOTH"))
                return mmCustomer.getSmsAuthorization();
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public String getLanguage() {
        try {
            if(getTipo().toString().equals("MA")) return maCustomer.getData().getGeneralPref_Lang01();
            else if(getTipo().toString().equals("MM") || getTipo().equals("MM")) return mmCustomer.getLanguage();
            return "Italiano";
        } catch (Exception e) {
            return "Italiano";
        }
    }
    public ProfileComplete getProfileComplete() {
        try {
            int step = 0;
            String text = "";
            String URL = "";
            int errorCode = 0;
            Boolean skip = false;
            ProfileComplete response = new ProfileComplete();
            if(getTipo().toString().equals("MA")) {
                if(response.checkDatiPersonaliMA(maCustomer)>0){
                    step += response.checkDatiPersonaliMA(maCustomer);
                    if(response.checkDatiPersonaliMA(maCustomer)<20){
                        text = i18n.get("myalitalia.info.completaDatiPersonali");
                        URL = "./myalitalia-dati-personali.html?page=personalData";
                        skip = true;
                    }
                }else{
                    text = i18n.get("myalitalia.info.completaDatiPersonali");
                    URL = "./myalitalia-dati-personali.html?page=personalData";
                    skip = true;
                }

                if(response.checkDatiViaggioMA(maCustomer)>0){
                    step += response.checkDatiViaggioMA(maCustomer);
                    if(response.checkDatiViaggioMA(maCustomer)<20){
                        if(!skip){
                            text = i18n.get("myalitalia.info.completaDatiViaggio");
                            URL = "./myalitalia-dati-di-viaggio.html?page=dataTravel";
                            skip = true;
                        }
                    }
                }else{
                    if(!skip){
                        text = i18n.get("myalitalia.info.completaDatiViaggio");
                        URL = "./myalitalia-dati-di-viaggio.html?page=dataTravel";
                        skip = true;
                    }
                }

                if(response.checkPreferenzeViaggioMA(maCustomer)>0){
                    step += response.checkPreferenzeViaggioMA(maCustomer);
                    if(response.checkPreferenzeViaggioMA(maCustomer)<30){
                        if(!skip){
                            text = i18n.get("myalitalia.info.completaPreferenza");
                            URL = "./myalitalia-millemiglia-preferenze-viaggio.html?page=preferences";
                            skip = true;
                        }
                    }
                }else{
                    if(!skip){
                        text = i18n.get("myalitalia.info.completaPreferenza");
                        URL = "./myalitalia-millemiglia-preferenze-viaggio.html?page=preferences";
                        skip = true;
                    }
                }

                if(response.checkDatiPagamentoFatturazione(maCustomer)>0){
                    step += response.checkDatiPagamentoFatturazione(maCustomer);
                }else{
                    if(!skip){
                        text = "";
                        URL = "#";
                        //text = i18n.get("myalitalia.info.completaDatiPagamento");
                        //URL = "./myalitalia-dati-di-pagamento.html";
                    }
                }

                //DA TOGLIERE UNA VOLTA IMPLEMENTATA LA PARTE DEI DATI DI PAGAMENTO E FATTURAZIONE
                if(step == 70){
                    step = 100;
                }

                if(step==100){
                    text = i18n.get("myalitalia.info.allCompleted");
                    URL = null;
                }


//                if(response.checkDatiPersonaliMA(maCustomer)) {
//                    step += 20;
//                    if(response.checkDatiViaggioMA(maCustomer)) {
//                        step += 20;
//                        if(response.checkDatiPagamentoFatturazione(maCustomer)) {
//                            step += 20;
//                            if (response.checkPreferenzeViaggioMA(maCustomer)) {
//                                step += 20;
//                                if(response.checkMMConnection(maCustomer)) {
//                                    step += 20;
//                                    text = i18n.get("myalitalia.info.allCompleted");
//                                    URL = null;
//                                } else {
//                                    text = i18n.get("myalitalia.info.iscrizioneMillemiglia");
//                                    URL = "#";
//                                }
//                            } else {
//                                text = i18n.get("myalitalia.info.completaPreferenza");
//                                URL = "./myalitalia-millemiglia-preferenze-viaggio.html?page=preferences";
//                            }
//                        } else {
//                            text = i18n.get("myalitalia.info.completaDatiPagamento");
//                            URL = "./myalitalia-dati-di-pagamento.html";
//                        }
//                    } else {
//                        text = i18n.get("myalitalia.info.completaDatiViaggio");
//                        URL = "./myalitalia-dati-di-viaggio.html?page=dataTravel";
//                    }
//                } else {
//                    text = i18n.get("myalitalia.info.completaDatiPersonali");
//                    URL = "./myalitalia-dati-personali.html?page=personalData";
//                }
                response.setErrorCode(errorCode);
                response.setText(text);
                response.setURL(URL);
                response.setCompleted(step);
                return response;
            } else return new ProfileComplete();
        } catch (Exception e) {
            return new ProfileComplete("#", "-", 0, 1, "Eccezione", e.getStackTrace().toString());
        }
    }

    public String getProfileImage() {
        try {
            if(getTipo().toString().equals("MA"))
                return maCustomer.getProfile().getPhotoURL();
            else return null;
        } catch (Exception e) {
            return null;
        }
    }

    public String getSocialProviders() {
        try {
            return maCustomer.getSocialProviders();
        } catch (Exception e) {
            return "none";
        }
    }

    public MAProvider[] getIdentities() {
        try {
            return maCustomer.getIdentities();
        } catch (Exception e) {
            return new MAProvider[0];
        }
    }

    public String getPassportNumber(){
        if (mmCustomer != null) {
            // TODO: recuperare le informazioni da utente millemiglia
            return "";
        }
        if (maCustomer != null) {
            return maCustomer.getData().getPassport_Number();
        }
        return "";
    }

    public String getPassportEmissionCountry(){
        if (mmCustomer != null) {
            // TODO: recuperare le informazioni da utente millemiglia
            return "";
        }
        if (maCustomer != null) {
            return maCustomer.getData().getPassport_CountryCode();
        }
        return "";
    }

    public String getPassportExpireDate(){
        Calendar cal = Calendar.getInstance();
        if (mmCustomer != null) {
            // TODO: recuperare le informazioni da utente millemiglia
            return "";
        }
        if (maCustomer != null) {
            cal.setTimeInMillis(maCustomer.getData().getPassport_ExpireOn());
            return dateFormatter.format(cal.getTime());
        }
        return "";
    }

    public String getICNumber(){
        if (mmCustomer != null) {
            // TODO: recuperare le informazioni da utente millemiglia
            return "";
        }
        if (maCustomer != null) {
            return maCustomer.getData().getIdentityCard_Number();
        }
        return "";
    }

    public String getICEmissionCountry(){
        if (mmCustomer != null) {
            // TODO: recuperare le informazioni da utente millemiglia
            return "";
        }
        if (maCustomer != null) {
            return maCustomer.getData().getIdentityCard_CountryCode();
        }
        return "";
    }

    public String getICExpireDate(){
        Calendar cal = Calendar.getInstance();
        if (mmCustomer != null) {
            // TODO: recuperare le informazioni da utente millemiglia
            return "";
        }
        if (maCustomer != null) {
            cal.setTimeInMillis(maCustomer.getData().getIdentityCard_ExpireOn());
            return dateFormatter.format(cal.getTime());
        }
        return "";
    }

    public String getNumberGC(){
        if (mmCustomer != null) {
            // TODO: recuperare le informazioni da utente millemiglia
            return "";
        }
        if (maCustomer != null) {
            return maCustomer.getData().getGreen_Number();
        }
        return "";
    }

    public String getCountryCodeGC(){
        if (mmCustomer != null) {
            // TODO: recuperare le informazioni da utente millemiglia
            return "";
        }
        if (maCustomer != null) {
            return maCustomer.getData().getGreen_CountryCode();
        }
        return "";
    }

    public String getNationalityCodeGC(){
        Calendar cal = Calendar.getInstance();
        if (mmCustomer != null) {
            // TODO: recuperare le informazioni da utente millemiglia
            return "";
        }
        if (maCustomer != null) {
            return maCustomer.getData().getGreen_NationalityCode();
        }
        return "";
    }

    public String getExpireOnGC(){
        Calendar cal = Calendar.getInstance();
        if (mmCustomer != null) {
            // TODO: recuperare le informazioni da utente millemiglia
            return "";
        }
        if (maCustomer != null) {
            cal.setTimeInMillis(maCustomer.getData().getGreen_ExpireOn());
            return dateFormatter.format(cal.getTime());
        }
        return "";
    }

    public String getBornDateGC(){
        Calendar cal = Calendar.getInstance();
        if (mmCustomer != null) {
            // TODO: recuperare le informazioni da utente millemiglia
            return "";
        }
        if (maCustomer != null) {
            cal.setTimeInMillis(maCustomer.getData().getGreen_BornDate());
            return dateFormatter.format(cal.getTime());
        }
        return "";
    }

    public String getNumberVisto(){
        if (mmCustomer != null) {
            // TODO: recuperare le informazioni da utente millemiglia
            return "";
        }
        if (maCustomer != null) {
            return maCustomer.getData().getVisa_Number();
        }
        return "";
    }

    public String getCountryCodeVisto(){
        if (mmCustomer != null) {
            // TODO: recuperare le informazioni da utente millemiglia
            return "";
        }
        if (maCustomer != null) {
            return maCustomer.getData().getVisa_CountryCode();
        }
        return "";
    }

    public String getValidFromVisto(){
        Calendar cal = Calendar.getInstance();
        if (mmCustomer != null) {
            // TODO: recuperare le informazioni da utente millemiglia
            return "";
        }
        if (maCustomer != null) {
            cal.setTimeInMillis(maCustomer.getData().getVisa_ValidFrom());
            return dateFormatter.format(cal.getTime());
        }
        return "";
    }

    public Boolean getResetPwdSms() {
        if(maCustomer != null) return maCustomer.getData().getResetPwdSms();
        else return null;
    }

}