
package com.alitalia.aem.common.data.checkinrest.model.searchbyticket;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Flight {

    @SerializedName("origin")
    @Expose
    private Origin origin;
    @SerializedName("destination")
    @Expose
    private Destination destination;
    @SerializedName("arrivalDate")
    @Expose
    private String arrivalDate;
    @SerializedName("departureDate")
    @Expose
    private String departureDate;
    @SerializedName("segments")
    @Expose
    private List<Segment> segments = null;
    @SerializedName("isWebCheckinPermitted")
    @Expose
    private Boolean isWebCheckinPermitted;
    @SerializedName("isBpPrintPermitted")
    @Expose
    private Boolean isBpPrintPermitted;
    @SerializedName("webCheckInDeepLink")
    @Expose
    private String webCheckInDeepLink;

    public Origin getOrigin() {
        return origin;
    }

    public void setOrigin(Origin origin) {
        this.origin = origin;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public List<Segment> getSegments() {
        return segments;
    }

    public void setSegments(List<Segment> segments) {
        this.segments = segments;
    }

    public Boolean getIsWebCheckinPermitted() {
        return isWebCheckinPermitted;
    }

    public void setIsWebCheckinPermitted(Boolean isWebCheckinPermitted) {
        this.isWebCheckinPermitted = isWebCheckinPermitted;
    }

    public Boolean getIsBpPrintPermitted() {
        return isBpPrintPermitted;
    }

    public void setIsBpPrintPermitted(Boolean isBpPrintPermitted) {
        this.isBpPrintPermitted = isBpPrintPermitted;
    }

    public String getWebCheckInDeepLink() {
        return webCheckInDeepLink;
    }

    public void setWebCheckInDeepLink(String webCheckInDeepLink) {
        this.webCheckInDeepLink = webCheckInDeepLink;
    }

}
