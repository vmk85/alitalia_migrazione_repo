package com.alitalia.aem.common.messages.home;

import java.util.Calendar;

import com.alitalia.aem.common.messages.BaseRequest;

public class RetrieveNewsRequest extends BaseRequest{

	private String category;
	private Calendar dateFrom;
	private Calendar dateTo;
	private boolean pubTest;
	private String title;
	
	public String getCategory() {
		return category;
	}
	
	public void setCategory(String category) {
		this.category = category;
	}
	
	public Calendar getDateFrom() {
		return dateFrom;
	}
	
	public void setDateFrom(Calendar dateFrom) {
		this.dateFrom = dateFrom;
	}
	
	public Calendar getDateTo() {
		return dateTo;
	}
	
	public void setDateTo(Calendar dateTo) {
		this.dateTo = dateTo;
	}
	
	public boolean isPubTest() {
		return pubTest;
	}
	
	public void setPubTest(boolean pubTest) {
		this.pubTest = pubTest;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((category == null) ? 0 : category.hashCode());
		result = prime * result
				+ ((dateFrom == null) ? 0 : dateFrom.hashCode());
		result = prime * result + ((dateTo == null) ? 0 : dateTo.hashCode());
		result = prime * result + (pubTest ? 1231 : 1237);
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveNewsRequest other = (RetrieveNewsRequest) obj;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (dateFrom == null) {
			if (other.dateFrom != null)
				return false;
		} else if (!dateFrom.equals(other.dateFrom))
			return false;
		if (dateTo == null) {
			if (other.dateTo != null)
				return false;
		} else if (!dateTo.equals(other.dateTo))
			return false;
		if (pubTest != other.pubTest)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveNewsRequest [category=" + category + ", dateFrom="
				+ dateFrom + ", dateTo=" + dateTo + ", pubTest=" + pubTest
				+ ", title=" + title + "]";
	}
}