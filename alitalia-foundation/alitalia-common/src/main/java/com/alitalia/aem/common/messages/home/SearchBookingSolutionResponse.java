package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.AvailableFlightsData;
import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.common.messages.BaseResponse;

public class SearchBookingSolutionResponse extends BaseResponse {

	private RoutesData routesData;
	private AvailableFlightsData availableFlightsTaxes;

	public RoutesData getRoutesData() {
		return routesData;
	}

	public void setRoutesData(RoutesData routesData) {
		this.routesData = routesData;
	}

	public AvailableFlightsData getAvailableFlightsTaxes() {
		return availableFlightsTaxes;
	}

	public void setAvailableFlightsTaxes(AvailableFlightsData availableFlightsTaxes) {
		this.availableFlightsTaxes = availableFlightsTaxes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((availableFlightsTaxes == null) ? 0 : availableFlightsTaxes
						.hashCode());
		result = prime * result
				+ ((routesData == null) ? 0 : routesData.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SearchBookingSolutionResponse other = (SearchBookingSolutionResponse) obj;
		if (availableFlightsTaxes == null) {
			if (other.availableFlightsTaxes != null)
				return false;
		} else if (!availableFlightsTaxes.equals(other.availableFlightsTaxes))
			return false;
		if (routesData == null) {
			if (other.routesData != null)
				return false;
		} else if (!routesData.equals(other.routesData))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SearchBookingSolutionResponse [routesData=" + routesData
				+ ", availableFlightsTaxes=" + availableFlightsTaxes
				+ ", getTid()=" + getTid() + ", getSid()=" + getSid()
				+ ", getCookie()=" + getCookie() + ", getExecute()="
				+ getExecute() + "]";
	}
}