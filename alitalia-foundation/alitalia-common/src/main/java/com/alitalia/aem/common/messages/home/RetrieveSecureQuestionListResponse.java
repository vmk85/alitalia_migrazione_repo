package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.SecureQuestionData;
import com.alitalia.aem.common.messages.BaseResponse;

import java.util.List;

public class RetrieveSecureQuestionListResponse extends BaseResponse {

	private List<SecureQuestionData> secureQuestions;

	public List<SecureQuestionData> getSecureQuestions() {
		return secureQuestions;
	}

	public void setSecureQuestions(List<SecureQuestionData> secureQuestions) {
		this.secureQuestions = secureQuestions;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((secureQuestions == null) ? 0 : secureQuestions.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveSecureQuestionListResponse other = (RetrieveSecureQuestionListResponse) obj;
		if (secureQuestions == null) {
			if (other.secureQuestions != null)
				return false;
		} else if (!secureQuestions.equals(other.secureQuestions))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveSecureQuestionListResponse [secureQuestions=" + secureQuestions + "]";
	}
}