
package com.alitalia.aem.common.data.checkinrest.model.searchbypnr;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FrequentFlyer {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("regularExpressionValidation")
    @Expose
    private String regularExpressionValidation;
    @SerializedName("lenght")
    @Expose
    private Integer lenght;
    @SerializedName("preFillChar")
    @Expose
    private String preFillChar;
    @SerializedName("number")
    @Expose
    private String number;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRegularExpressionValidation() {
        return regularExpressionValidation;
    }

    public void setRegularExpressionValidation(String regularExpressionValidation) {
        this.regularExpressionValidation = regularExpressionValidation;
    }

    public Integer getLenght() {
        return lenght;
    }

    public void setLenght(Integer lenght) {
        this.lenght = lenght;
    }

    public String getPreFillChar() {
        return preFillChar;
    }

    public void setPreFillChar(String preFillChar) {
        this.preFillChar = preFillChar;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

}
