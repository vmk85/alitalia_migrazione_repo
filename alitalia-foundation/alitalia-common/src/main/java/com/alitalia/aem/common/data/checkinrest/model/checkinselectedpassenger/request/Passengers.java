
package com.alitalia.aem.common.data.checkinrest.model.checkinselectedpassenger.request;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Passengers {

    @SerializedName("pnr")
    @Expose
    private String pnr;
    @SerializedName("flight")
    @Expose
    private String flight;
    @SerializedName("passengers")
    @Expose
    private List<Passenger> passengers = null;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("market")
    @Expose
    private String market;
    @SerializedName("conversationID")
    @Expose
    private String conversationID;

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public String getFlight() {
        return flight;
    }

    public void setFlight(String flight) {
        this.flight = flight;
    }

    public List<Passenger> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<Passenger> passengers) {
        this.passengers = passengers;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

}
