package com.alitalia.aem.common.data.checkinrest.model.flightdetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BoardingTime {

    @SerializedName("boardingIndicator")
    @Expose
    private String boardingIndicator;
    @SerializedName("value")
    @Expose
    private String value;

    public String getBoardingIndicator() {
        return boardingIndicator;
    }

    public void setBoardingIndicator(String boardingIndicator) {
        this.boardingIndicator = boardingIndicator;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
