package com.alitalia.aem.common.data.checkinrest.model.searchbyfrequentflyer;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class SegmentDateRender {
	
	String country;
	String date;

	public SegmentDateRender(String date, String country) {
		this.date = date;
		this.country = country == null ? "": country;
	}
	
	public String getStandardDateCheckin() {
		String standardDateCheckin = "";
		
		try {
			Date parsedDate = toDate(date);
			SimpleDateFormat formatter = new  SimpleDateFormat("EEE dd MMM YYYY");
			standardDateCheckin = formatter.format(parsedDate);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return standardDateCheckin;
	}
	
	public String getStandardHoursCheckin() {
		String standardHoursCheckin = "";
		try {
			Date parseTime = toDate(date);

			SimpleDateFormat formatter = new SimpleDateFormat("H:mm");
			standardHoursCheckin = formatter.format(parseTime);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return standardHoursCheckin;
	}



	public static Date toDate(final String dateString1) throws ParseException {

		if (dateString1 == null) return null;

		// ENRICO - 2018-04-05
        // arriva una stringa con Z finale, ma non è in formato GMT!
        // E' necessario ignorare Z lo Z, le date sono già locali alla destinazione e all'arrivo
		String dateString = dateString1.replaceAll("Z$", "");
		DateFormat fmt;

		// Gestione formato con e senza millisecondi
		if (dateString.indexOf(".") > 0) {
			fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		} else {
			fmt =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		}

		if (dateString.contains("T")) {
			dateString = dateString.replace('T', ' ');
		}

		if (dateString.contains("Z")) {
			dateString = dateString.replace("Z", "+0000");
		}
		return fmt.parse(dateString);
	}

}
