package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PnrListInfoByPnrSearch;
import com.alitalia.aem.common.data.checkinrest.model.searchbypnr.SearchByPnr;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinPnrSearchResponse extends BaseResponse {
	
	private PnrListInfoByPnrSearch pnrData;
	
	public CheckinPnrSearchResponse() {}

	public CheckinPnrSearchResponse(String tid, String sid) {
		super(tid, sid);
	}

	public PnrListInfoByPnrSearch getPnrData() {
		return pnrData;
	}

	public void setPnrData(PnrListInfoByPnrSearch pnrData) {
		this.pnrData = pnrData;
	}

}
