package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class GetDatiSicurezzaProfiloOLDRequest extends BaseRequest {

    private String IdProfilo;

    public String getIdProfilo() {
        return IdProfilo;
    }

    public void setIdProfilo(String idProfilo) {
        IdProfilo = idProfilo;
    }

    @Override
    public String toString() {
        return "GetDatiSicurezzaProfiloOLDResponse{" +
                "IdProfilo='" + IdProfilo + '\'' +
                '}';
    }
}
