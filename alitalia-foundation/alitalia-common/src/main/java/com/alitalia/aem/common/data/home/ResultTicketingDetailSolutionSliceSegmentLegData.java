package com.alitalia.aem.common.data.home;

import java.util.List;

public class ResultTicketingDetailSolutionSliceSegmentLegData {

    private ResultTicketingDetailSolutionSliceSegmentLegAircraftData aircraftField;
    private String arrivalField;
    private ResultTicketingDetailSolutionSliceSegmentLegConnectionData connectionField;
    private String departureField;
    private String destinationField;
    private String destinationTerminalField;
    private ResultTicketingDetailSolutionSliceSegmentLegDistanceData distanceField;
    private Integer durationField;
    private Boolean durationFieldSpecified;
    private ResultTicketingDetailSolutionSliceSegmentLegExtData extField;
    private ResultTicketingDetailSolutionSliceSegmentLegOperationalDisclosureData operationalDisclosureField;
    private ResultTicketingDetailSolutionSliceSegmentLegOperationalFlightData operationalFlightField;
    private String originField;
    private String originTerminalField;
    private List<ResultTicketingDetailSolutionSliceSegmentLegScheduledServicesData> scheduledServicesField;

    public ResultTicketingDetailSolutionSliceSegmentLegAircraftData getAircraftField() {
        return aircraftField;
    }

    public void setAircraftField(ResultTicketingDetailSolutionSliceSegmentLegAircraftData value) {
        this.aircraftField = value;
    }

    public String getArrivalField() {
        return arrivalField;
    }

    public void setArrivalField(String value) {
        this.arrivalField = value;
    }

    public ResultTicketingDetailSolutionSliceSegmentLegConnectionData getConnectionField() {
        return connectionField;
    }

    public void setConnectionField(ResultTicketingDetailSolutionSliceSegmentLegConnectionData value) {
        this.connectionField = value;
    }

    public String getDepartureField() {
        return departureField;
    }

    public void setDepartureField(String value) {
        this.departureField = value;
    }

    public String getDestinationField() {
        return destinationField;
    }

    public void setDestinationField(String value) {
        this.destinationField = value;
    }

    public String getDestinationTerminalField() {
        return destinationTerminalField;
    }

    public void setDestinationTerminalField(String value) {
        this.destinationTerminalField = value;
    }

    public ResultTicketingDetailSolutionSliceSegmentLegDistanceData getDistanceField() {
        return distanceField;
    }

    public void setDistanceField(ResultTicketingDetailSolutionSliceSegmentLegDistanceData value) {
        this.distanceField = value;
    }

    public Integer getDurationField() {
        return durationField;
    }

    public void setDurationField(int value) {
        this.durationField = value;
    }

    public Boolean isDurationFieldSpecified() {
        return durationFieldSpecified;
    }

    public void setDurationFieldSpecified(Boolean value) {
        this.durationFieldSpecified = value;
    }

    public ResultTicketingDetailSolutionSliceSegmentLegExtData getExtField() {
        return extField;
    }

    public void setExtField(ResultTicketingDetailSolutionSliceSegmentLegExtData value) {
        this.extField = value;
    }

    public ResultTicketingDetailSolutionSliceSegmentLegOperationalDisclosureData getOperationalDisclosureField() {
        return operationalDisclosureField;
    }

    public void setOperationalDisclosureField(ResultTicketingDetailSolutionSliceSegmentLegOperationalDisclosureData value) {
        this.operationalDisclosureField = value;
    }

    public ResultTicketingDetailSolutionSliceSegmentLegOperationalFlightData getOperationalFlightField() {
        return operationalFlightField;
    }

    public void setOperationalFlightField(ResultTicketingDetailSolutionSliceSegmentLegOperationalFlightData value) {
        this.operationalFlightField = value;
    }

    public String getOriginField() {
        return originField;
    }

    public void setOriginField(String value) {
        this.originField = value;
    }

    public String getOriginTerminalField() {
        return originTerminalField;
    }

    public void setOriginTerminalField(String value) {
        this.originTerminalField = value;
    }

    public List<ResultTicketingDetailSolutionSliceSegmentLegScheduledServicesData> getScheduledServicesField() {
        return scheduledServicesField;
    }

    public void setScheduledServicesField(List<ResultTicketingDetailSolutionSliceSegmentLegScheduledServicesData> value) {
        this.scheduledServicesField = value;
    }

}
