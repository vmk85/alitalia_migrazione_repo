
package com.alitalia.aem.common.data.checkinrest.model.reprintbp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TravelDocDataList {

    @SerializedName("travelDoc")
    @Expose
    private String travelDoc;
    @SerializedName("pngImage")
    @Expose
    private String pngImage;
    @SerializedName("xmlDigitalSignatureData")
    @Expose
    private XmlDigitalSignatureData xmlDigitalSignatureData;
    @SerializedName("type")
    @Expose
    private String type;

    public String getTravelDoc() {
        return travelDoc;
    }

    public void setTravelDoc(String travelDoc) {
        this.travelDoc = travelDoc;
    }

    public String getPngImage() {
        return pngImage;
    }

    public void setPngImage(String pngImage) {
        this.pngImage = pngImage;
    }

    public XmlDigitalSignatureData getXmlDigitalSignatureData() {
        return xmlDigitalSignatureData;
    }

    public void setXmlDigitalSignatureData(XmlDigitalSignatureData xmlDigitalSignatureData) {
        this.xmlDigitalSignatureData = xmlDigitalSignatureData;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
