
package com.alitalia.aem.common.data.checkinrest.model.boardingpass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Model {

    @SerializedName("pnr")
    @Expose
    private String pnr;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("passengerName")
    @Expose
    private String passengerName;
    @SerializedName("frequentFlyerCode")
    @Expose
    private String frequentFlyerCode;
    @SerializedName("ticketNumber")
    @Expose
    private String ticketNumber;
    @SerializedName("flightNumber")
    @Expose
    private String flightNumber;
    @SerializedName("boardingTerminal")
    @Expose
    private String boardingTerminal;
    @SerializedName("departureDate")
    @Expose
    private String departureDate;
    @SerializedName("departureTime")
    @Expose
    private String departureTime;
    @SerializedName("fromAirport")
    @Expose
    private String fromAirport;
    @SerializedName("toAirport")
    @Expose
    private String toAirport;
    @SerializedName("boardingGate")
    @Expose
    private String boardingGate;
    @SerializedName("boardingTime")
    @Expose
    private String boardingTime;
    @SerializedName("seatClassCode")
    @Expose
    private String seatClassCode;
    @SerializedName("seatClassName")
    @Expose
    private String seatClassName;
    @SerializedName("boardingZone")
    @Expose
    private String boardingZone;
    @SerializedName("seat")
    @Expose
    private String seat;
    @SerializedName("sequenceNumber")
    @Expose
    private String sequenceNumber;
    @SerializedName("isInfantCoupon")
    @Expose
    private Boolean isInfantCoupon;
    @SerializedName("isSkyPriority")
    @Expose
    private Boolean isSkyPriority;
    @SerializedName("barCode")
    @Expose
    private String barCode;
    @SerializedName("imgSourceBarCode")
    @Expose
    private String imgSourceBarCode;
    @SerializedName("hasNonRevenueInfo")
    @Expose
    private Boolean hasNonRevenueInfo;
    @SerializedName("nonRevenuePriorityCode")
    @Expose
    private String nonRevenuePriorityCode;
    @SerializedName("nonRevenueSeniorityDate")
    @Expose
    private String nonRevenueSeniorityDate;
    @SerializedName("specialFare")
    @Expose
    private String specialFare;
    @SerializedName("serviceOperatingDetails")
    @Expose
    private String serviceOperatingDetails;
    @SerializedName("ssrCode")
    @Expose
    private String ssrCode;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("resources")
    @Expose
    private Resources resources;
    @SerializedName("skyTeam")
    @Expose
    private String skyTeam;

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public String getFrequentFlyerCode() {
        return frequentFlyerCode;
    }

    public void setFrequentFlyerCode(String frequentFlyerCode) {
        this.frequentFlyerCode = frequentFlyerCode;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getBoardingTerminal() {
        return boardingTerminal;
    }

    public void setBoardingTerminal(String boardingTerminal) {
        this.boardingTerminal = boardingTerminal;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getFromAirport() {
        return fromAirport;
    }

    public void setFromAirport(String fromAirport) {
        this.fromAirport = fromAirport;
    }

    public String getToAirport() {
        return toAirport;
    }

    public void setToAirport(String toAirport) {
        this.toAirport = toAirport;
    }

    public String getBoardingGate() {
        return boardingGate;
    }

    public void setBoardingGate(String boardingGate) {
        this.boardingGate = boardingGate;
    }

    public String getBoardingTime() {
        return boardingTime;
    }

    public void setBoardingTime(String boardingTime) {
        this.boardingTime = boardingTime;
    }

    public String getSeatClassCode() {
        return seatClassCode;
    }

    public void setSeatClassCode(String seatClassCode) {
        this.seatClassCode = seatClassCode;
    }

    public String getSeatClassName() {
        return seatClassName;
    }

    public void setSeatClassName(String seatClassName) {
        this.seatClassName = seatClassName;
    }

    public String getBoardingZone() {
        return boardingZone;
    }

    public void setBoardingZone(String boardingZone) {
        this.boardingZone = boardingZone;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public String getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(String sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public Boolean getIsInfantCoupon() {
        return isInfantCoupon;
    }

    public void setIsInfantCoupon(Boolean isInfantCoupon) {
        this.isInfantCoupon = isInfantCoupon;
    }

    public Boolean getIsSkyPriority() {
        return isSkyPriority;
    }

    public void setIsSkyPriority(Boolean isSkyPriority) {
        this.isSkyPriority = isSkyPriority;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getImgSourceBarCode() {
        return imgSourceBarCode;
    }

    public void setImgSourceBarCode(String imgSourceBarCode) {
        this.imgSourceBarCode = imgSourceBarCode;
    }

    public Boolean getHasNonRevenueInfo() {
        return hasNonRevenueInfo;
    }

    public void setHasNonRevenueInfo(Boolean hasNonRevenueInfo) {
        this.hasNonRevenueInfo = hasNonRevenueInfo;
    }

    public String getNonRevenuePriorityCode() {
        return nonRevenuePriorityCode;
    }

    public void setNonRevenuePriorityCode(String nonRevenuePriorityCode) {
        this.nonRevenuePriorityCode = nonRevenuePriorityCode;
    }

    public String getNonRevenueSeniorityDate() {
        return nonRevenueSeniorityDate;
    }

    public void setNonRevenueSeniorityDate(String nonRevenueSeniorityDate) {
        this.nonRevenueSeniorityDate = nonRevenueSeniorityDate;
    }

    public String getSpecialFare() {
        return specialFare;
    }

    public void setSpecialFare(String specialFare) {
        this.specialFare = specialFare;
    }

    public String getServiceOperatingDetails() {
        return serviceOperatingDetails;
    }

    public void setServiceOperatingDetails(String serviceOperatingDetails) {
        this.serviceOperatingDetails = serviceOperatingDetails;
    }

    public String getSsrCode() {
        return ssrCode;
    }

    public void setSsrCode(String ssrCode) {
        this.ssrCode = ssrCode;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Resources getResources() {
        return resources;
    }

    public void setResources(Resources resources) {
        this.resources = resources;
    }

    public String getSkyTeam() {
        return skyTeam;
    }

    public void setSkyTeam(String skyTeam) {
        this.skyTeam = skyTeam;
    }

}
