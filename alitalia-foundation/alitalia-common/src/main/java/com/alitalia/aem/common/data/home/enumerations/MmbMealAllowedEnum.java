package com.alitalia.aem.common.data.home.enumerations;

public enum MmbMealAllowedEnum {

    TRUE("True"),
    FALSE("False"),
    PARTIAL("Partial");
    private final String value;

    MmbMealAllowedEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MmbMealAllowedEnum fromValue(String v) {
        for (MmbMealAllowedEnum c: MmbMealAllowedEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
