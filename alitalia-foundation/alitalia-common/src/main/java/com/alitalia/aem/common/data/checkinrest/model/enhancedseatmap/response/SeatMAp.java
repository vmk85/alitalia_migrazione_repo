
package com.alitalia.aem.common.data.checkinrest.model.enhancedseatmap.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SeatMAp {

//    @SerializedName("segmentSeat")
//    @Expose
//    private SegmentSeat segmentSeat;
	
    @SerializedName("seatMaps")
    @Expose
    private SeatMaps seatMaps;

//    public SegmentSeat getSegmentSeat() {
//        return segmentSeat;
//    }
//
//    public void setSegmentSeat(SegmentSeat segmentSeat) {
//        this.segmentSeat = segmentSeat;
//    }

    public SeatMaps getSeatMaps() {
        return seatMaps;
    }

    public void setSeatMaps(SeatMaps seatMaps) {
        this.seatMaps = seatMaps;
    }

}
