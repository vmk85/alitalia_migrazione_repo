
package com.alitalia.aem.common.data.checkinrest.model.endpayment.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AncillaryDetail {

    @SerializedName("refundIndicator")
    @Expose
    private String refundIndicator;
    @SerializedName("boardPoint")
    @Expose
    private String boardPoint;
    @SerializedName("offPoint")
    @Expose
    private String offPoint;
    @SerializedName("actionCode")
    @Expose
    private String actionCode;
    @SerializedName("pdcSeat")
    @Expose
    private String pdcSeat;
    @SerializedName("segmentIndicator")
    @Expose
    private String segmentIndicator;
    @SerializedName("segments")
    @Expose
    private List<Segment> segments = null;

    public String getRefundIndicator() {
        return refundIndicator;
    }

    public void setRefundIndicator(String refundIndicator) {
        this.refundIndicator = refundIndicator;
    }

    public String getBoardPoint() {
        return boardPoint;
    }

    public void setBoardPoint(String boardPoint) {
        this.boardPoint = boardPoint;
    }

    public String getOffPoint() {
        return offPoint;
    }

    public void setOffPoint(String offPoint) {
        this.offPoint = offPoint;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public String getPdcSeat() {
        return pdcSeat;
    }

    public void setPdcSeat(String pdcSeat) {
        this.pdcSeat = pdcSeat;
    }

    public String getSegmentIndicator() {
        return segmentIndicator;
    }

    public void setSegmentIndicator(String segmentIndicator) {
        this.segmentIndicator = segmentIndicator;
    }

    public List<Segment> getSegments() {
        return segments;
    }

    public void setSegments(List<Segment> segments) {
        this.segments = segments;
    }

}
