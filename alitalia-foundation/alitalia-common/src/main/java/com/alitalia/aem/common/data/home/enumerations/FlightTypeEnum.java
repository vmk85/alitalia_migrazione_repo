package com.alitalia.aem.common.data.home.enumerations;

public enum FlightTypeEnum {

    CONNECTING("Connecting"),
    DIRECT("Direct");
    private final String value;

    FlightTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FlightTypeEnum fromValue(String v) {
        for (FlightTypeEnum c: FlightTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
