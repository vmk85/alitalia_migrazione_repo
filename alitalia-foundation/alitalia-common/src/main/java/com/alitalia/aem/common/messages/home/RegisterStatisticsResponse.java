package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.RegisterStatisticsResult;
import com.alitalia.aem.common.messages.BaseResponse;

public class RegisterStatisticsResponse extends BaseResponse {
	
	private RegisterStatisticsResult result;

	public RegisterStatisticsResult getResult() {
		return result;
	}

	public void setResult(RegisterStatisticsResult result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "RegisterStatisticsResponse [result=" + result
				+ ", getResult()=" + getResult() + ", getTid()=" + getTid()
				+ ", getSid()=" + getSid() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + ", getClass()="
				+ getClass() + "]";
	}
}
