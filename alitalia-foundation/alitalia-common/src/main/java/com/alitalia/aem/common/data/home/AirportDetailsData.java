package com.alitalia.aem.common.data.home;

import java.io.Serializable;

public class AirportDetailsData implements Serializable{

	private String azTerminal;
	private Integer checkInTimeDomestic;
	private Integer checkInTimeIntercontinental;
	private Integer checkInTimeInternational;
	private Double cityDistance;
	private Integer cityTransferTime;
	private Integer fusoOrarioGMT;
	private Integer notesCode;

	public AirportDetailsData(){}
	
	public AirportDetailsData(AirportDetailsData clone) {
		super();
		this.azTerminal = clone.azTerminal;
		this.checkInTimeDomestic = clone.checkInTimeDomestic;
		this.checkInTimeIntercontinental = clone.checkInTimeIntercontinental;
		this.checkInTimeInternational = clone.checkInTimeInternational;
		this.cityDistance = clone.cityDistance;
		this.cityTransferTime = clone.cityTransferTime;
		this.fusoOrarioGMT = clone.fusoOrarioGMT;
		this.notesCode = clone.notesCode;
	}

	public String getAzTerminal() {
		return azTerminal;
	}
	
	public void setAzTerminal(String azTerminal) {
		this.azTerminal = azTerminal;
	}
	
	public Integer getCheckInTimeDomestic() {
		return checkInTimeDomestic;
	}
	
	public void setCheckInTimeDomestic(Integer checkInTimeDomestic) {
		this.checkInTimeDomestic = checkInTimeDomestic;
	}
	
	public Integer getCheckInTimeIntercontinental() {
		return checkInTimeIntercontinental;
	}
	
	public void setCheckInTimeIntercontinental(Integer checkInTimeIntercontinental) {
		this.checkInTimeIntercontinental = checkInTimeIntercontinental;
	}
	
	public Integer getCheckInTimeInternational() {
		return checkInTimeInternational;
	}
	
	public void setCheckInTimeInternational(Integer checkInTimeInternational) {
		this.checkInTimeInternational = checkInTimeInternational;
	}
	
	public Double getCityDistance() {
		return cityDistance;
	}
	
	public void setCityDistance(Double cityDistance) {
		this.cityDistance = cityDistance;
	}
	
	public Integer getCityTransferTime() {
		return cityTransferTime;
	}
	
	public void setCityTransferTime(Integer cityTransferTime) {
		this.cityTransferTime = cityTransferTime;
	}
	
	public Integer getFusoOrarioGMT() {
		return fusoOrarioGMT;
	}
	
	public void setFusoOrarioGMT(Integer fusoOrarioGMT) {
		this.fusoOrarioGMT = fusoOrarioGMT;
	}
	
	public Integer getNotesCode() {
		return notesCode;
	}
	
	public void setNotesCode(Integer notesCode) {
		this.notesCode = notesCode;
	}

	@Override
	public String toString() {
		return "AirportDetailsData [azTerminal=" + azTerminal
				+ ", checkInTimeDomestic=" + checkInTimeDomestic
				+ ", checkInTimeIntercontinental="
				+ checkInTimeIntercontinental + ", checkInTimeInternational="
				+ checkInTimeInternational + ", cityDistance=" + cityDistance
				+ ", cityTransferTime=" + cityTransferTime + ", fusoOrarioGMT="
				+ fusoOrarioGMT + ", notesCode=" + notesCode + "]";
	}

}
