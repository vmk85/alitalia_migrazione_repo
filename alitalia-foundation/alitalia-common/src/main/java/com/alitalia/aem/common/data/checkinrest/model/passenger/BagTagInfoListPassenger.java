package com.alitalia.aem.common.data.checkinrest.model.passenger;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BagTagInfoListPassenger {
	
	@SerializedName("bagTagInfo")
	@Expose
	private List<BagTagInfoPassenger> bagTagInfo = null;
	
	@SerializedName("totalWeightAndUnit")
	@Expose
	private TotalWeightAndUnitPassenger totalWeightAndUnit;

	public List<BagTagInfoPassenger> getBagTagInfo() {
		return bagTagInfo;
	}

	public void setBagTagInfo(List<BagTagInfoPassenger> bagTagInfo) {
		this.bagTagInfo = bagTagInfo;
	}

	public TotalWeightAndUnitPassenger getTotalWeightAndUnit() {
		return totalWeightAndUnit;
	}

	public void setTotalWeightAndUnit(TotalWeightAndUnitPassenger totalWeightAndUnit) {
		this.totalWeightAndUnit = totalWeightAndUnit;
	}

}
