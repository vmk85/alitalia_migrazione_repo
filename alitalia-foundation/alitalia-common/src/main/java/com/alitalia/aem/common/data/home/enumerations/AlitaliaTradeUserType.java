package com.alitalia.aem.common.data.home.enumerations;

public enum AlitaliaTradeUserType {
    TITOLARE("0"),
    BANCONISTA("1");
    private final String value;

    AlitaliaTradeUserType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AlitaliaTradeUserType fromValue(String v) {
        for (AlitaliaTradeUserType c: AlitaliaTradeUserType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }


}
