
package com.alitalia.aem.common.data.checkinrest.model.reprintbp;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItineraryPassengerList {

    @SerializedName("itineraryDetail")
    @Expose
    private ItineraryDetail itineraryDetail;
    @SerializedName("passengerDetailList")
    @Expose
    private List<PassengerDetailList> passengerDetailList = null;
    @SerializedName("freeTextInfoList")
    @Expose
    private List<FreeTextInfoList___> freeTextInfoList = null;

    public ItineraryDetail getItineraryDetail() {
        return itineraryDetail;
    }

    public void setItineraryDetail(ItineraryDetail itineraryDetail) {
        this.itineraryDetail = itineraryDetail;
    }

    public List<PassengerDetailList> getPassengerDetailList() {
        return passengerDetailList;
    }

    public void setPassengerDetailList(List<PassengerDetailList> passengerDetailList) {
        this.passengerDetailList = passengerDetailList;
    }

    public List<FreeTextInfoList___> getFreeTextInfoList() {
        return freeTextInfoList;
    }

    public void setFreeTextInfoList(List<FreeTextInfoList___> freeTextInfoList) {
        this.freeTextInfoList = freeTextInfoList;
    }

}
