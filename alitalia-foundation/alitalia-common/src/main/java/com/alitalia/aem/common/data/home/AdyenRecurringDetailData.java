package com.alitalia.aem.common.data.home;

import java.util.List;

public class AdyenRecurringDetailData {
	/*
  {
    "additionalDataList": [
      {
        "key": "cardBin",
        "cardBin": "444433"
      }
    ],
    "card": {
      "cvc": null,
      "expiryMonth": "8",
      "expiryYear": "2018",
      "holderName": "Test Uno",
      "number": "1111",
      "type": "visa"
    },
    "alias": "P106674586198742",
    "aliasType": "Default",
    "creationDate": "2018-09-06T14:24:40Z",
    "firstPspReference": "8835362438795089",
    "recurringDetailReference": "8315362438809277",
    "recurringList": [
      {
        "contract": "RECURRING"
      }
    ],
    "paymentMethodVariant": "visacorporatecredit",
    "variant": "visa",
    "address": {
      "city": "Roma",
      "country": "IT",
      "postalCode": "00100",
      "province": "RM",
      "street": "via roma 100"
    },
    "bank": null,
    "shopperName": null
  } 
	 */
	
	private List<AdyenAdditionalData> additionalDataList;
	private AdyenCardData card;
	private String alias;
	private String aliasType;
	private String creationDate;
	private String firstPspReference;
	private String recurringDetailReference;
	private List<AdyenRecurringData> recurringList;
	private String paymentMethodVariant;
	private String variant;
	private AdyenAddressData address;
	private String bank;
	private String shopperName;
	
	public List<AdyenAdditionalData> getAdditionalDataList() {
		return additionalDataList;
	}
	public void setAdditionalDataList(List<AdyenAdditionalData> additionalDataList) {
		this.additionalDataList = additionalDataList;
	}
	public AdyenCardData getCard() {
		return card;
	}
	public void setCard(AdyenCardData card) {
		this.card = card;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getAliasType() {
		return aliasType;
	}
	public void setAliasType(String aliasType) {
		this.aliasType = aliasType;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public String getFirstPspReference() {
		return firstPspReference;
	}
	public void setFirstPspReference(String firstPspReference) {
		this.firstPspReference = firstPspReference;
	}
	public String getRecurringDetailReference() {
		return recurringDetailReference;
	}
	public void setRecurringDetailReference(String recurringDetailReference) {
		this.recurringDetailReference = recurringDetailReference;
	}
	public List<AdyenRecurringData> getRecurringList() {
		return recurringList;
	}
	public void setRecurringList(List<AdyenRecurringData> recurringList) {
		this.recurringList = recurringList;
	}
	public String getPaymentMethodVariant() {
		return paymentMethodVariant;
	}
	public void setPaymentMethodVariant(String paymentMethodVariant) {
		this.paymentMethodVariant = paymentMethodVariant;
	}
	public String getVariant() {
		return variant;
	}
	public void setVariant(String variant) {
		this.variant = variant;
	}
	public AdyenAddressData getAddress() {
		return address;
	}
	public void setAddress(AdyenAddressData address) {
		this.address = address;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public String getShopperName() {
		return shopperName;
	}
	public void setShopperName(String shopperName) {
		this.shopperName = shopperName;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result	+ ((additionalDataList == null) ? 0 : additionalDataList.hashCode());
		result = prime * result	+ ((card == null) ? 0 : card.hashCode());
		result = prime * result	+ ((alias == null) ? 0 : alias.hashCode());
		result = prime * result	+ ((aliasType == null) ? 0 : aliasType.hashCode());
		result = prime * result	+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result	+ ((firstPspReference == null) ? 0 : firstPspReference.hashCode());
		result = prime * result	+ ((recurringDetailReference == null) ? 0 : recurringDetailReference.hashCode());
		result = prime * result	+ ((recurringList == null) ? 0 : recurringList.hashCode());
		result = prime * result	+ ((paymentMethodVariant == null) ? 0 : paymentMethodVariant.hashCode());
		result = prime * result	+ ((variant == null) ? 0 : variant.hashCode());
		result = prime * result	+ ((bank == null) ? 0 : bank.hashCode());
		result = prime * result	+ ((shopperName == null) ? 0 : shopperName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdyenRecurringDetailData other = (AdyenRecurringDetailData) obj;
		if (additionalDataList == null) {
			if (other.additionalDataList != null)
				return false;
		} else if (!additionalDataList.equals(other.additionalDataList))
			return false;
		if (card == null) {
			if (other.card != null)
				return false;
		} else if (!card.equals(other.card))
			return false;
		if (alias == null) {
			if (other.alias != null)
				return false;
		} else if (!alias.equals(other.alias))
			return false;

		if (aliasType == null) {
			if (other.aliasType != null)
				return false;
		} else if (!aliasType.equals(other.aliasType))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (firstPspReference == null) {
			if (other.firstPspReference != null)
				return false;
		} else if (!firstPspReference.equals(other.firstPspReference))
			return false;
		if (recurringDetailReference == null) {
			if (other.recurringDetailReference != null)
				return false;
		} else if (!recurringDetailReference.equals(other.recurringDetailReference))
			return false;
		if (recurringList == null) {
			if (other.recurringList != null)
				return false;
		} else if (!recurringList.equals(other.recurringList))
			return false;
		if (paymentMethodVariant == null) {
			if (other.paymentMethodVariant != null)
				return false;
		} else if (!paymentMethodVariant.equals(other.paymentMethodVariant))
			return false;
		if (variant == null) {
			if (other.variant != null)
				return false;
		} else if (!variant.equals(other.variant))
			return false;
		if (bank == null) {
			if (other.bank != null)
				return false;
		} else if (!bank.equals(other.bank))
			return false;
		if (shopperName == null) {
			if (other.shopperName != null)
				return false;
		} else if (!shopperName.equals(other.shopperName))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "RegisterCreditCardRequest ["
				+ "additionalDataList=" + additionalDataList
				+ ", card=" + card
				+ ", alias=" + alias
				+ ", aliasType=" + aliasType
				+ ", creationDate=" + creationDate
				+ ", firstPspReference=" + firstPspReference
				+ ", recurringDetailReference=" + recurringDetailReference
				+ ", recurringList=" + recurringList
				+ ", paymentMethodVariant=" + paymentMethodVariant
				+ ", variant=" + variant
				+ ", bank=" + bank
				+ ", shopperName=" + shopperName
				+ "]";
	}
	
}
