package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class MilleMigliaLoginSARequest extends BaseRequest {

    private String codiceMM;
    private String password;
    private String username;

    public MilleMigliaLoginSARequest() {
        super();
    }

    public MilleMigliaLoginSARequest(String tid, String sid) {
        super(tid, sid);
    }

    public MilleMigliaLoginSARequest(String tid, String sid, String codiceMM, String username, String password) {
        super(tid, sid);
        this.codiceMM = codiceMM;
        this.username = username;
        this.password = password;
    }


    public String getCodiceMM() {
        return codiceMM;
    }

    public void setCodiceMM(String codiceMM) {
        this.codiceMM = codiceMM;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
