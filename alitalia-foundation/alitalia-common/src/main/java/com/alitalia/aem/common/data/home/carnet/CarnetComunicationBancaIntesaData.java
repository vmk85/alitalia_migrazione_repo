package com.alitalia.aem.common.data.home.carnet;

public class CarnetComunicationBancaIntesaData extends CarnetComunicationData {

	private String errorUrl;
    private String cancelUrl;
    private String shopperId;
    
	public String getErrorUrl() {
		return errorUrl;
	}
	public void setErrorUrl(String errorUrl) {
		this.errorUrl = errorUrl;
	}
	public String getCancelUrl() {
		return cancelUrl;
	}
	public void setCancelUrl(String cancelUrl) {
		this.cancelUrl = cancelUrl;
	}
	public String getShopperId() {
		return shopperId;
	}
	public void setShopperId(String shopperId) {
		this.shopperId = shopperId;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((cancelUrl == null) ? 0 : cancelUrl.hashCode());
		result = prime * result
				+ ((errorUrl == null) ? 0 : errorUrl.hashCode());
		result = prime * result
				+ ((shopperId == null) ? 0 : shopperId.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarnetComunicationBancaIntesaData other = (CarnetComunicationBancaIntesaData) obj;
		if (cancelUrl == null) {
			if (other.cancelUrl != null)
				return false;
		} else if (!cancelUrl.equals(other.cancelUrl))
			return false;
		if (errorUrl == null) {
			if (other.errorUrl != null)
				return false;
		} else if (!errorUrl.equals(other.errorUrl))
			return false;
		if (shopperId == null) {
			if (other.shopperId != null)
				return false;
		} else if (!shopperId.equals(other.shopperId))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CarnetComunicationBancaIntesaData [errorUrl=" + errorUrl
				+ ", cancelUrl=" + cancelUrl + ", shopperId=" + shopperId + "]";
	}
}