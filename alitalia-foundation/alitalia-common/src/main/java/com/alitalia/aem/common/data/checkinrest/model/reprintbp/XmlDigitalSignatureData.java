
package com.alitalia.aem.common.data.checkinrest.model.reprintbp;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class XmlDigitalSignatureData {

    @SerializedName("boardingPassSource")
    @Expose
    private String boardingPassSource;
    @SerializedName("boardingPassType")
    @Expose
    private String boardingPassType;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("agentInfo")
    @Expose
    private AgentInfo agentInfo;
    @SerializedName("pnrLocator")
    @Expose
    private String pnrLocator;
    @SerializedName("barcodeInfo")
    @Expose
    private BarcodeInfo barcodeInfo;
    @SerializedName("boardingPassItineraryDetail")
    @Expose
    private List<BoardingPassItineraryDetail> boardingPassItineraryDetail = null;

    public String getBoardingPassSource() {
        return boardingPassSource;
    }

    public void setBoardingPassSource(String boardingPassSource) {
        this.boardingPassSource = boardingPassSource;
    }

    public String getBoardingPassType() {
        return boardingPassType;
    }

    public void setBoardingPassType(String boardingPassType) {
        this.boardingPassType = boardingPassType;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public AgentInfo getAgentInfo() {
        return agentInfo;
    }

    public void setAgentInfo(AgentInfo agentInfo) {
        this.agentInfo = agentInfo;
    }

    public String getPnrLocator() {
        return pnrLocator;
    }

    public void setPnrLocator(String pnrLocator) {
        this.pnrLocator = pnrLocator;
    }

    public BarcodeInfo getBarcodeInfo() {
        return barcodeInfo;
    }

    public void setBarcodeInfo(BarcodeInfo barcodeInfo) {
        this.barcodeInfo = barcodeInfo;
    }

    public List<BoardingPassItineraryDetail> getBoardingPassItineraryDetail() {
        return boardingPassItineraryDetail;
    }

    public void setBoardingPassItineraryDetail(List<BoardingPassItineraryDetail> boardingPassItineraryDetail) {
        this.boardingPassItineraryDetail = boardingPassItineraryDetail;
    }

}
