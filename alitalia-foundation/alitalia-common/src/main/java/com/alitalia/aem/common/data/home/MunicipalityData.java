package com.alitalia.aem.common.data.home;

public class MunicipalityData {

	private boolean active;
    private String code;
    private String description;
    private String germanDescription;
    private String provinceCode;
	
    public boolean isActive() {
		return active;
	}
    
	public void setActive(boolean active) {
		this.active = active;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getGermanDescription() {
		return germanDescription;
	}
	
	public void setGermanDescription(String germanDescription) {
		this.germanDescription = germanDescription;
	}
	
	public String getProvinceCode() {
		return provinceCode;
	}
	
	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result
				+ ((provinceCode == null) ? 0 : provinceCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MunicipalityData other = (MunicipalityData) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (provinceCode == null) {
			if (other.provinceCode != null)
				return false;
		} else if (!provinceCode.equals(other.provinceCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MunicipalityData [active=" + active + ", code=" + code
				+ ", description=" + description + ", germanDescription="
				+ germanDescription + ", provinceCode=" + provinceCode + "]";
	}
}