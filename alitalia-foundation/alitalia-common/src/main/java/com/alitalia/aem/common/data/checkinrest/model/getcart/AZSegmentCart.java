
package com.alitalia.aem.common.data.checkinrest.model.getcart;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AZSegmentCart {

    @SerializedName("flightNumber")
    @Expose
    private String flightNumber;
    @SerializedName("boardPoint")
    @Expose
    private String boardPoint;
    @SerializedName("offPoint")
    @Expose
    private String offPoint;
    @SerializedName("classOfService")
    @Expose
    private String classOfService;
    @SerializedName("departureDate")
    @Expose
    private String departureDate;
    @SerializedName("arrivalDate")
    @Expose
    private String arrivalDate;
    @SerializedName("aZCartPassenger")
    @Expose
    private List<AZCartPassenger> aZCartPassenger = null;

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getBoardPoint() {
        return boardPoint;
    }

    public void setBoardPoint(String boardPoint) {
        this.boardPoint = boardPoint;
    }

    public String getOffPoint() {
        return offPoint;
    }

    public void setOffPoint(String offPoint) {
        this.offPoint = offPoint;
    }

    public String getClassOfService() {
        return classOfService;
    }

    public void setClassOfService(String classOfService) {
        this.classOfService = classOfService;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public List<AZCartPassenger> getAZCartPassenger() {
        return aZCartPassenger;
    }

    public void setAZCartPassenger(List<AZCartPassenger> aZCartPassenger) {
        this.aZCartPassenger = aZCartPassenger;
    }

}
