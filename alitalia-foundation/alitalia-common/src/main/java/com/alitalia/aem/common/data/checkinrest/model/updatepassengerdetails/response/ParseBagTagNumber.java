
package com.alitalia.aem.common.data.checkinrest.model.updatepassengerdetails.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ParseBagTagNumber {

    @SerializedName("bagTagSixDigitNumber")
    @Expose
    private String bagTagSixDigitNumber;
    @SerializedName("indexNumber")
    @Expose
    private String indexNumber;
    @SerializedName("airlineCode")
    @Expose
    private String airlineCode;

    public String getBagTagSixDigitNumber() {
        return bagTagSixDigitNumber;
    }

    public void setBagTagSixDigitNumber(String bagTagSixDigitNumber) {
        this.bagTagSixDigitNumber = bagTagSixDigitNumber;
    }

    public String getIndexNumber() {
        return indexNumber;
    }

    public void setIndexNumber(String indexNumber) {
        this.indexNumber = indexNumber;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

}
