package com.alitalia.aem.common.data.home;

import java.util.List;

public class ResultBookingDetailsSolutionPricingFareCalculationData {
	
	private List<ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData> boxedTaxField;
	private List<String> lineField;
	
	public List<ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData> getBoxedTaxField() {
		return boxedTaxField;
	}

	public void setBoxedTaxField(
			List<ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData> boxedTaxField) {
		this.boxedTaxField = boxedTaxField;
	}

	public List<String> getLineField() {
		return lineField;
	}

	public void setLineField(List<String> lineField) {
		this.lineField = lineField;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((boxedTaxField == null) ? 0 : boxedTaxField.hashCode());
		result = prime * result
				+ ((lineField == null) ? 0 : lineField.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingDetailsSolutionPricingFareCalculationData other = (ResultBookingDetailsSolutionPricingFareCalculationData) obj;
		if (boxedTaxField == null) {
			if (other.boxedTaxField != null)
				return false;
		} else if (!boxedTaxField.equals(other.boxedTaxField))
			return false;
		if (lineField == null) {
			if (other.lineField != null)
				return false;
		} else if (!lineField.equals(other.lineField))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingDetailsSolutionPricingFareCalculationData [boxedTaxField="
				+ boxedTaxField + ", lineField=" + lineField + "]";
	}
	
}
