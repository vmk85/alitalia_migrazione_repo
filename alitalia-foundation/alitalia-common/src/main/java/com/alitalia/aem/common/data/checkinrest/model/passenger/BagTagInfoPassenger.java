package com.alitalia.aem.common.data.checkinrest.model.passenger;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BagTagInfoPassenger {
	
	@SerializedName("bagTagNumber")
	@Expose
	private String bagTagNumber;
	
	@SerializedName("weightAndSize")
	@Expose
	private WeightAndSizePassenger weightAndSize;
	
	@SerializedName("bagStatus")
	@Expose
	private BagStatusPassenger bagStatus;

	public String getBagTagNumber() {
		return bagTagNumber;
	}

	public void setBagTagNumber(String bagTagNumber) {
		this.bagTagNumber = bagTagNumber;
	}

	public WeightAndSizePassenger getWeightAndSize() {
		return weightAndSize;
	}

	public void setWeightAndSize(WeightAndSizePassenger weightAndSize) {
		this.weightAndSize = weightAndSize;
	}

	public BagStatusPassenger getBagStatus() {
		return bagStatus;
	}

	public void setBagStatus(BagStatusPassenger bagStatus) {
		this.bagStatus = bagStatus;
	}

}
