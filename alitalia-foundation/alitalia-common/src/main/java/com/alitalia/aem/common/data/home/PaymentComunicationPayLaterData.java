package com.alitalia.aem.common.data.home;

import java.util.List;

public class PaymentComunicationPayLaterData extends
		PaymentComunicationBaseData {
	
	private List<String> osis;
	private List<ArcoQueueData> queues;
	private List<String> remarks;
	
	public List<String> getOsis() {
		return osis;
	}
	
	public void setOsis(List<String> osis) {
		this.osis = osis;
	}
	
	public List<ArcoQueueData> getQueues() {
		return queues;
	}
	
	public void setQueues(List<ArcoQueueData> queues) {
		this.queues = queues;
	}
	
	public List<String> getRemarks() {
		return remarks;
	}
	
	public void setRemarks(List<String> remarks) {
		this.remarks = remarks;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((osis == null) ? 0 : osis.hashCode());
		result = prime * result + ((queues == null) ? 0 : queues.hashCode());
		result = prime * result + ((remarks == null) ? 0 : remarks.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentComunicationPayLaterData other = (PaymentComunicationPayLaterData) obj;
		if (osis == null) {
			if (other.osis != null)
				return false;
		} else if (!osis.equals(other.osis))
			return false;
		if (queues == null) {
			if (other.queues != null)
				return false;
		} else if (!queues.equals(other.queues))
			return false;
		if (remarks == null) {
			if (other.remarks != null)
				return false;
		} else if (!remarks.equals(other.remarks))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PaymentComunicationPayLaterData [osis=" + osis + ", queues="
				+ queues + ", remarks=" + remarks + "]";
	}
	
}
