package com.alitalia.aem.common.data.home.enumerations;

public enum DistanceUnitsEnum {

    MI("MI"),
    KN("KN");
    
    private final String value;

    DistanceUnitsEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DistanceUnitsEnum fromValue(String v) {
        for (DistanceUnitsEnum c: DistanceUnitsEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
