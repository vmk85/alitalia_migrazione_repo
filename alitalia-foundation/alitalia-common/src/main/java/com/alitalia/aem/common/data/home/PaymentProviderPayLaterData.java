package com.alitalia.aem.common.data.home;

import java.util.List;

public class PaymentProviderPayLaterData extends PaymentProviderData {
	
	private List<String> emailForMail;
	private String emailForPnr;
	private ContactData otherContact;
	
	public List<String> getEmailForMail() {
		return emailForMail;
	}
	
	public void setEmailForMail(List<String> emailForMail) {
		this.emailForMail = emailForMail;
	}
	
	public String getEmailForPnr() {
		return emailForPnr;
	}
	
	public void setEmailForPnr(String emailForPnr) {
		this.emailForPnr = emailForPnr;
	}
	
	public ContactData getOtherContact() {
		return otherContact;
	}
	
	public void setOtherContact(ContactData otherContact) {
		this.otherContact = otherContact;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((emailForMail == null) ? 0 : emailForMail.hashCode());
		result = prime * result
				+ ((emailForPnr == null) ? 0 : emailForPnr.hashCode());
		result = prime * result
				+ ((otherContact == null) ? 0 : otherContact.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentProviderPayLaterData other = (PaymentProviderPayLaterData) obj;
		if (emailForMail == null) {
			if (other.emailForMail != null)
				return false;
		} else if (!emailForMail.equals(other.emailForMail))
			return false;
		if (emailForPnr == null) {
			if (other.emailForPnr != null)
				return false;
		} else if (!emailForPnr.equals(other.emailForPnr))
			return false;
		if (otherContact == null) {
			if (other.otherContact != null)
				return false;
		} else if (!otherContact.equals(other.otherContact))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PaymentProviderPayLaterData [emailForMail=" + emailForMail
				+ ", emailForPnr=" + emailForPnr + ", otherContact="
				+ otherContact + "]";
	}
	
}
