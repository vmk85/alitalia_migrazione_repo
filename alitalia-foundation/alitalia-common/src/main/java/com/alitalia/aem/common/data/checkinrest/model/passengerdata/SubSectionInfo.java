
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubSectionInfo {

    @SerializedName("subSectionName")
    @Expose
    private String subSectionName;
    @SerializedName("subSectionType")
    @Expose
    private String subSectionType;
    @SerializedName("subSectionText")
    @Expose
    private String subSectionText;

    public String getSubSectionName() {
        return subSectionName;
    }

    public void setSubSectionName(String subSectionName) {
        this.subSectionName = subSectionName;
    }

    public String getSubSectionType() {
        return subSectionType;
    }

    public void setSubSectionType(String subSectionType) {
        this.subSectionType = subSectionType;
    }

    public String getSubSectionText() {
        return subSectionText;
    }

    public void setSubSectionText(String subSectionText) {
        this.subSectionText = subSectionText;
    }

}
