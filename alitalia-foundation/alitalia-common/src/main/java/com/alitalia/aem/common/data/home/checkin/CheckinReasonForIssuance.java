package com.alitalia.aem.common.data.home.checkin;


public class CheckinReasonForIssuance {

	private String reasonCode;
	private String reasonDescription;
	private String reasonSubCode;
	
	public String getReasonCode() {
		return reasonCode;
	}
	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}
	public String getReasonDescription() {
		return reasonDescription;
	}
	public void setReasonDescription(String reasonDescription) {
		this.reasonDescription = reasonDescription;
	}
	public String getReasonSubCode() {
		return reasonSubCode;
	}
	public void setReasonSubCode(String reasonSubCode) {
		this.reasonSubCode = reasonSubCode;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((reasonCode == null) ? 0 : reasonCode.hashCode());
		result = prime
				* result
				+ ((reasonDescription == null) ? 0 : reasonDescription
						.hashCode());
		result = prime * result
				+ ((reasonSubCode == null) ? 0 : reasonSubCode.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinReasonForIssuance other = (CheckinReasonForIssuance) obj;
		if (reasonCode == null) {
			if (other.reasonCode != null)
				return false;
		} else if (!reasonCode.equals(other.reasonCode))
			return false;
		if (reasonDescription == null) {
			if (other.reasonDescription != null)
				return false;
		} else if (!reasonDescription.equals(other.reasonDescription))
			return false;
		if (reasonSubCode == null) {
			if (other.reasonSubCode != null)
				return false;
		} else if (!reasonSubCode.equals(other.reasonSubCode))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "CheckinReasonForIssuance [reasonCode=" + reasonCode
				+ ", reasonDescription=" + reasonDescription
				+ ", reasonSubCode=" + reasonSubCode + "]";
	}
}