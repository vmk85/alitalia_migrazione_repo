
package com.alitalia.aem.common.data.checkinrest.model.passenger;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WeightAndSizePassenger {

    @SerializedName("weightAndUnit")
    @Expose
    private WeightAndUnitPassenger weightAndUnit;
    @SerializedName("lengthAndUnit")
    @Expose
    private LengthAndUnitPassenger lengthAndUnit;

    public WeightAndUnitPassenger getWeightAndUnit() {
        return weightAndUnit;
    }

    public void setWeightAndUnit(WeightAndUnitPassenger weightAndUnit) {
        this.weightAndUnit = weightAndUnit;
    }

    public LengthAndUnitPassenger getLengthAndUnit() {
        return lengthAndUnit;
    }

    public void setLengthAndUnit(LengthAndUnitPassenger lengthAndUnit) {
        this.lengthAndUnit = lengthAndUnit;
    }

}
