
package com.alitalia.aem.common.data.checkinrest.model.updatepassengerdetails.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PassengerInfoResponseList {

    @SerializedName("lineNumber")
    @Expose
    private Integer lineNumber;
    @SerializedName("lineNumberSpecified")
    @Expose
    private Boolean lineNumberSpecified;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("passengerID")
    @Expose
    private String passengerID;
    @SerializedName("groupCode")
    @Expose
    private String groupCode;
    @SerializedName("classOfService")
    @Expose
    private String classOfService;
    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("fqtvCode")
    @Expose
    private String fqtvCode;
    @SerializedName("seat")
    @Expose
    private String seat;
    @SerializedName("departureGate")
    @Expose
    private String departureGate;
    @SerializedName("onBoardFlag")
    @Expose
    private String onBoardFlag;
    @SerializedName("priorityCode")
    @Expose
    private String priorityCode;
    @SerializedName("checkInNumber")
    @Expose
    private Integer checkInNumber;
    @SerializedName("checkInNumberSpecified")
    @Expose
    private Boolean checkInNumberSpecified;
    @SerializedName("collectFee")
    @Expose
    private String collectFee;
    @SerializedName("bagCount")
    @Expose
    private String bagCount;
    @SerializedName("bagTagInfoList")
    @Expose
    private BagTagInfoList bagTagInfoList;
    @SerializedName("bagTagPrintData")
    @Expose
    private BagTagPrintData bagTagPrintData;
    @SerializedName("aeDetailsList")
    @Expose
    private List<AeDetailsList> aeDetailsList = null;
    @SerializedName("editCodeList")
    @Expose
    private EditCodeList editCodeList;
    @SerializedName("freeText")
    @Expose
    private String freeText;

    public Integer getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(Integer lineNumber) {
        this.lineNumber = lineNumber;
    }

    public Boolean getLineNumberSpecified() {
        return lineNumberSpecified;
    }

    public void setLineNumberSpecified(Boolean lineNumberSpecified) {
        this.lineNumberSpecified = lineNumberSpecified;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPassengerID() {
        return passengerID;
    }

    public void setPassengerID(String passengerID) {
        this.passengerID = passengerID;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getClassOfService() {
        return classOfService;
    }

    public void setClassOfService(String classOfService) {
        this.classOfService = classOfService;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getFqtvCode() {
        return fqtvCode;
    }

    public void setFqtvCode(String fqtvCode) {
        this.fqtvCode = fqtvCode;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public String getDepartureGate() {
        return departureGate;
    }

    public void setDepartureGate(String departureGate) {
        this.departureGate = departureGate;
    }

    public String getOnBoardFlag() {
        return onBoardFlag;
    }

    public void setOnBoardFlag(String onBoardFlag) {
        this.onBoardFlag = onBoardFlag;
    }

    public String getPriorityCode() {
        return priorityCode;
    }

    public void setPriorityCode(String priorityCode) {
        this.priorityCode = priorityCode;
    }

    public Integer getCheckInNumber() {
        return checkInNumber;
    }

    public void setCheckInNumber(Integer checkInNumber) {
        this.checkInNumber = checkInNumber;
    }

    public Boolean getCheckInNumberSpecified() {
        return checkInNumberSpecified;
    }

    public void setCheckInNumberSpecified(Boolean checkInNumberSpecified) {
        this.checkInNumberSpecified = checkInNumberSpecified;
    }

    public String getCollectFee() {
        return collectFee;
    }

    public void setCollectFee(String collectFee) {
        this.collectFee = collectFee;
    }

    public String getBagCount() {
        return bagCount;
    }

    public void setBagCount(String bagCount) {
        this.bagCount = bagCount;
    }

    public BagTagInfoList getBagTagInfoList() {
        return bagTagInfoList;
    }

    public void setBagTagInfoList(BagTagInfoList bagTagInfoList) {
        this.bagTagInfoList = bagTagInfoList;
    }

    public BagTagPrintData getBagTagPrintData() {
        return bagTagPrintData;
    }

    public void setBagTagPrintData(BagTagPrintData bagTagPrintData) {
        this.bagTagPrintData = bagTagPrintData;
    }

    public List<AeDetailsList> getAeDetailsList() {
        return aeDetailsList;
    }

    public void setAeDetailsList(List<AeDetailsList> aeDetailsList) {
        this.aeDetailsList = aeDetailsList;
    }

    public EditCodeList getEditCodeList() {
        return editCodeList;
    }

    public void setEditCodeList(EditCodeList editCodeList) {
        this.editCodeList = editCodeList;
    }

    public String getFreeText() {
        return freeText;
    }

    public void setFreeText(String freeText) {
        this.freeText = freeText;
    }

}
