package com.alitalia.aem.common.data.home.enumerations;

public enum MMRegistrationTypeEnum {

    NOT_REQUIRED("NotRequired"),
    UPDATED("Updated"),
    TEMP("Temp"),
    DISABLED("Disabled"),
    EFFECTIVE("Effective");

    private final String value;
    
    MMRegistrationTypeEnum(String v) {
		value = v;
	}
	
	public String value() {
		return value;
	}

	public static MMRegistrationTypeEnum fromValue(String v) {
		for (MMRegistrationTypeEnum c: MMRegistrationTypeEnum.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
