package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class SubscribeSASendMailRequest extends BaseRequest {

	private String profileID;
	public String getProfileID() {
		return profileID;
	}
	public void setProfileID(String errorMessage) {
		this.profileID = profileID;
	}

	private String username;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((profileID == null) ? 0 : profileID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubscribeSASendMailRequest other = (SubscribeSASendMailRequest) obj;
		if (profileID == null) {
			if (other.profileID != null)
				return false;
		} else if (!profileID.equals(other.profileID))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SubscribeSACheckEmailUserNameDuplicateRequest [profileID=" + profileID + "]";
	}



}