package com.alitalia.aem.common.data.home.enumerations;

public enum CarnetResultTypeEnum {

    OK,
    KO;
    
    public String value() {
        return name();
    }
    
    public static CarnetResultTypeEnum fromValue(String v) {
        return valueOf(v);
    }
}
