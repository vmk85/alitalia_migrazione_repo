package com.alitalia.aem.common.data.crmdatarest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PreferenzePersonali {

    @SerializedName("linguaComunicazioniCommerciali")
    @Expose
    private String linguaComunicazioniCommerciali;

    @SerializedName("linguaComunicazioniServizio")
    @Expose
    private String linguaComunicazioniServizio;

    public String getLinguaComunicazioniCommerciali() {
        return linguaComunicazioniCommerciali;
    }

    public void setLinguaComunicazioniCommerciali(String linguaComunicazioniCommerciali) {
        this.linguaComunicazioniCommerciali = linguaComunicazioniCommerciali;
    }

    public String getLinguaComunicazioniServizio() {
        return linguaComunicazioniServizio;
    }

    public void setLinguaComunicazioniServizio(String linguaComunicazioniServizio) {
        this.linguaComunicazioniServizio = linguaComunicazioniServizio;
    }

    @Override
    public String toString() {
        return "PreferenzePersonali{" +
                "linguaComunicazioniCommerciali='" + linguaComunicazioniCommerciali + '\'' +
                ", linguaComunicazioniServizio='" + linguaComunicazioniServizio + '\'' +
                '}';
    }
}
