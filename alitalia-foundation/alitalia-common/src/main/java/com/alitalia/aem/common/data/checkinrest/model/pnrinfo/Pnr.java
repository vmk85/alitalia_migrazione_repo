
package com.alitalia.aem.common.data.checkinrest.model.pnrinfo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pnr {

    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("flights")
    @Expose
    private List<Flight> flights = null;
    @SerializedName("roundTrip")
    @Expose
    private Boolean roundTrip;
    @SerializedName("insurance")
    @Expose
    private Boolean insurance;
    @SerializedName("isVoloAlitalia")
    @Expose
    private Boolean isVoloAlitalia;

    public Boolean getVoloAlitalia() {
        return isVoloAlitalia;
    }

    public void setVoloAlitalia(Boolean voloAlitalia) {
        isVoloAlitalia = voloAlitalia;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public List<Flight> getFlights() {
        return flights;
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
    }

    public Boolean getRoundTrip() {
        return roundTrip;
    }

    public void setRoundTrip(Boolean roundTrip) {
        this.roundTrip = roundTrip;
    }

    public Boolean getInsurance() {
        return insurance;
    }
    public void setInsurance(Boolean insurance) {
        this.insurance = insurance;
    }

}
