package com.alitalia.aem.common.data.home;

import java.io.Serializable;

import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;

public class PassengerNumbersData implements Serializable{

	private PassengerTypeEnum passengerType;
	private Integer number;

	public PassengerNumbersData(){}
			
	public PassengerNumbersData(PassengerNumbersData pnElement) {
		this.number = pnElement.getNumber();
		this.passengerType= pnElement.passengerType; 
	}

	public PassengerTypeEnum getPassengerType() {
		return passengerType;
	}

	public void setPassengerType(PassengerTypeEnum passengerType) {
		this.passengerType = passengerType;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	@Override
	public String toString() {
		return "PassengerNumbersData [passengerType=" + passengerType
				+ ", number=" + number + "]";
	}
}