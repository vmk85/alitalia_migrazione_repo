package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;


public class GetSabrePNRResponse extends BaseResponse {

	private String pnr;
	private String pnrSabre;
	
	public String getPnr() {
		return pnr;
	}
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	public String getPnrSabre() {
		return pnrSabre;
	}
	public void setPnrSabre(String pnrSabre) {
		this.pnrSabre = pnrSabre;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((pnr == null) ? 0 : pnr.hashCode());
		result = prime * result
				+ ((pnrSabre == null) ? 0 : pnrSabre.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		GetSabrePNRResponse other = (GetSabrePNRResponse) obj;
		if (pnr == null) {
			if (other.pnr != null)
				return false;
		} else if (!pnr.equals(other.pnr))
			return false;
		if (pnrSabre == null) {
			if (other.pnrSabre != null)
				return false;
		} else if (!pnrSabre.equals(other.pnrSabre))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "GetSabrePNRResponse [pnr=" + pnr + ", pnrSabre=" + pnrSabre
				+ ", getCookie()=" + getCookie() + ", getExecute()="
				+ getExecute() + "]";
	}
}