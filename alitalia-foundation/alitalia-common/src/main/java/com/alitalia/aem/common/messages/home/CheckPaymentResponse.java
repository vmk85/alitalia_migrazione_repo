package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.PaymentProcessInfoData;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckPaymentResponse extends BaseResponse {
	private PaymentProcessInfoData process;

	public PaymentProcessInfoData getProcess() {
		return process;
	}

	public void setProcess(PaymentProcessInfoData process) {
		this.process = process;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((process == null) ? 0 : process.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckPaymentResponse other = (CheckPaymentResponse) obj;
		if (process == null) {
			if (other.process != null)
				return false;
		} else if (!process.equals(other.process))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CheckPaymentResponse [process=" + process + "]";
	}
	
}
