package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.updatesecurityinfo.request.UpdateSecurityInfoReq;
import com.alitalia.aem.common.messages.BaseRequest;

public class CheckinUpdateSecurityInfoRequest extends BaseRequest{
	
	private UpdateSecurityInfoReq _updatesecurityinfoReq;
	
	public UpdateSecurityInfoReq get_updatesecurityinfoReq() {
		return _updatesecurityinfoReq;
	}

	public void set_updatesecurityinfoReq(UpdateSecurityInfoReq _updatesecurityinfoReq) {
		this._updatesecurityinfoReq = _updatesecurityinfoReq;
	}

	public CheckinUpdateSecurityInfoRequest(String tid, String sid) {
		super(tid, sid);
	}
	
}
