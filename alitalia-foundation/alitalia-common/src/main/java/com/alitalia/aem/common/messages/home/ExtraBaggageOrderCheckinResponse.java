package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.mmb.MmbBaggageOrderData;
import com.alitalia.aem.common.messages.BaseResponse;

public class ExtraBaggageOrderCheckinResponse extends BaseResponse {

	private MmbBaggageOrderData order;

	public MmbBaggageOrderData getOrder() {
		return order;
	}

	public void setOrder(MmbBaggageOrderData order) {
		this.order = order;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((order == null) ? 0 : order.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExtraBaggageOrderCheckinResponse other = (ExtraBaggageOrderCheckinResponse) obj;
		if (order == null) {
			if (other.order != null)
				return false;
		} else if (!order.equals(other.order))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ExtraBaggageOrderCheckinResponse [order=" + order
				+ ", toString()=" + super.toString() + "]";
	}

}
