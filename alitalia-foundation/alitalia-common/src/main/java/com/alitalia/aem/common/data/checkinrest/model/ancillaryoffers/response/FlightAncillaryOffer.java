package com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FlightAncillaryOffer {

    @SerializedName("fastTrackEnabled")
    @Expose
    private Boolean fastTrackEnabled;
    @SerializedName("loungeEnabled")
    @Expose
    private Boolean loungeEnabled;
    @SerializedName("bagsMax")
    @Expose
    private Long bagsMax;
    @SerializedName("flightNumber")
    @Expose
    private String flightNumber;
    @SerializedName("ancillaryOffers")
    @Expose

    private List<AncillaryOffer> ancillaryOffers = null;

    public Boolean getFastTrackEnabled() {
        return fastTrackEnabled;
    }

    public void setFastTrackEnabled(Boolean fastTrackEnabled) {
        this.fastTrackEnabled = fastTrackEnabled;
    }

    public Boolean getLoungeEnabled() {
        return loungeEnabled;
    }

    public void setLoungeEnabled(Boolean loungeEnabled) {
        this.loungeEnabled = loungeEnabled;
    }

    public Long getBagsMax() {
        return bagsMax;
    }

    public void setBagsMax(Long bagsMax) {
        this.bagsMax = bagsMax;
    }

    public List<AncillaryOffer> getAncillaryOffers() {
        return ancillaryOffers;
    }

    public void setAncillaryOffers(List<AncillaryOffer> ancillaryOffers) {
        this.ancillaryOffers = ancillaryOffers;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

}
