package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.IPAddressData;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.data.home.enumerations.OwRtEnum;
import com.alitalia.aem.common.messages.BaseRequest;

public class RetrieveGeoOffersRequest extends BaseRequest {

	private String departureAirportCode;
    private IPAddressData ipAddress;
    private String languageCode;
    private String marketCode;
    private String paxType;
    private Integer rngpryDwn;
    private Integer rngpryUp;
    private String target;
    private String fx;
    private String arrivalAirportCode;
    private OwRtEnum owRt;
    private AreaValueEnum areaValue;
    private Boolean monthDailyPrices;
	
    public String getDepartureAirportCode() {
		return departureAirportCode;
	}
	
    public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}
	
    public IPAddressData getIpAddress() {
		return ipAddress;
	}
	
    public void setIpAddress(IPAddressData ipAddress) {
		this.ipAddress = ipAddress;
	}
	
    public String getLanguageCode() {
		return languageCode;
	}
	
    public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	
    public String getMarketCode() {
		return marketCode;
	}
	
    public void setMarketCode(String marketCode) {
		this.marketCode = marketCode;
	}
	
    public String getPaxType() {
		return paxType;
	}
	
    public void setPaxType(String paxType) {
		this.paxType = paxType;
	}
	
    public Integer getRngpryDwn() {
		return rngpryDwn;
	}
	
    public void setRngpryDwn(Integer rngpryDwn) {
		this.rngpryDwn = rngpryDwn;
	}
	
    public Integer getRngpryUp() {
		return rngpryUp;
	}
	
    public void setRngpryUp(Integer rngpryUp) {
		this.rngpryUp = rngpryUp;
	}
	
    public String getTarget() {
		return target;
	}
	
    public void setTarget(String target) {
		this.target = target;
	}
	
    public String getFx() {
		return fx;
	}
	
    public void setFx(String fx) {
		this.fx = fx;
	}

	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}

	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode;
	}

	public OwRtEnum getOwRt() {
		return owRt;
	}

	public void setOwRt(OwRtEnum owRt) {
		this.owRt = owRt;
	}

	public AreaValueEnum getAreaValue() {
		return areaValue;
	}

	public void setAreaValue(AreaValueEnum areaValue) {
		this.areaValue = areaValue;
	}

	public Boolean getMonthDailyPrices() {
		return monthDailyPrices;
	}

	public void setMonthDailyPrices(Boolean monthDailyPrices) {
		this.monthDailyPrices = monthDailyPrices;
	}
}