package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.passenger.Passenger;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinPassengerResponse extends BaseResponse {
	
	private Passenger passenger;

	public CheckinPassengerResponse() {}

	public CheckinPassengerResponse(String tid, String sid) {
		super(tid, sid);
	}

	public Passenger getPassenger() {
		return passenger;
	}

	public void setPassenger(Passenger passenger) {
		this.passenger = passenger;
	}

}
