package com.alitalia.aem.common.data.home;

public class ArcoQueueData {
	
	private String pseudoCityCode;
	private Short queueNumber;
	
	public String getPseudoCityCode() {
		return pseudoCityCode;
	}
	
	public void setPseudoCityCode(String pseudoCityCode) {
		this.pseudoCityCode = pseudoCityCode;
	}
	
	public Short getQueueNumber() {
		return queueNumber;
	}
	
	public void setQueueNumber(Short queueNumber) {
		this.queueNumber = queueNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((pseudoCityCode == null) ? 0 : pseudoCityCode.hashCode());
		result = prime * result
				+ ((queueNumber == null) ? 0 : queueNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArcoQueueData other = (ArcoQueueData) obj;
		if (pseudoCityCode == null) {
			if (other.pseudoCityCode != null)
				return false;
		} else if (!pseudoCityCode.equals(other.pseudoCityCode))
			return false;
		if (queueNumber == null) {
			if (other.queueNumber != null)
				return false;
		} else if (!queueNumber.equals(other.queueNumber))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ArcoQueueData [pseudoCityCode=" + pseudoCityCode
				+ ", queueNumber=" + queueNumber + "]";
	}
	
}
