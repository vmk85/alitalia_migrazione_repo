package com.alitalia.aem.common.data.home;

import java.util.List;

public class CrossSellingData extends CrossSellingDetailData {

	private List<String> otherImages;

	public List<String> getOtherImages() {
		return otherImages;
	}

	public void setOtherImages(List<String> otherImages) {
		this.otherImages = otherImages;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((otherImages == null) ? 0 : otherImages.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CrossSellingData other = (CrossSellingData) obj;
		if (otherImages == null) {
			if (other.otherImages != null)
				return false;
		} else if (!otherImages.equals(other.otherImages))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CrossSellingData [otherImages=" + otherImages + "]";
	}

}
