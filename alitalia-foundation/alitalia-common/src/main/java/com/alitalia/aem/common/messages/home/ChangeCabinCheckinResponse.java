package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.messages.BaseResponse;

public class ChangeCabinCheckinResponse extends BaseResponse {

	private List<String> messages;
	private String seat;
	private MmbCompartimentalClassEnum seatClass;
	private String seatClassName;
	private Boolean succeeded;
	
	public List<String> getMessages() {
		return messages;
	}
	public void setMessages(List<String> messages) {
		this.messages = messages;
	}
	public String getSeat() {
		return seat;
	}
	public void setSeat(String seat) {
		this.seat = seat;
	}
	public MmbCompartimentalClassEnum getSeatClass() {
		return seatClass;
	}
	public void setSeatClass(MmbCompartimentalClassEnum seatClass) {
		this.seatClass = seatClass;
	}
	public String getSeatClassName() {
		return seatClassName;
	}
	public void setSeatClassName(String seatClassName) {
		this.seatClassName = seatClassName;
	}
	public Boolean getSucceeded() {
		return succeeded;
	}
	public void setSucceeded(Boolean succeeded) {
		this.succeeded = succeeded;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((messages == null) ? 0 : messages.hashCode());
		result = prime * result + ((seat == null) ? 0 : seat.hashCode());
		result = prime * result
				+ ((seatClass == null) ? 0 : seatClass.hashCode());
		result = prime * result
				+ ((seatClassName == null) ? 0 : seatClassName.hashCode());
		result = prime * result
				+ ((succeeded == null) ? 0 : succeeded.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChangeCabinCheckinResponse other = (ChangeCabinCheckinResponse) obj;
		if (messages == null) {
			if (other.messages != null)
				return false;
		} else if (!messages.equals(other.messages))
			return false;
		if (seat == null) {
			if (other.seat != null)
				return false;
		} else if (!seat.equals(other.seat))
			return false;
		if (seatClass != other.seatClass)
			return false;
		if (seatClassName == null) {
			if (other.seatClassName != null)
				return false;
		} else if (!seatClassName.equals(other.seatClassName))
			return false;
		if (succeeded == null) {
			if (other.succeeded != null)
				return false;
		} else if (!succeeded.equals(other.succeeded))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ChangeCabinCheckinResponse [messages=" + messages + ", seat="
				+ seat + ", seatClass=" + seatClass + ", seatClassName="
				+ seatClassName + ", succeeded=" + succeeded + ", toString()="
				+ super.toString() + "]";
	}

}
