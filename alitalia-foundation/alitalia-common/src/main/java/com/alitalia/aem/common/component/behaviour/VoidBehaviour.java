package com.alitalia.aem.common.component.behaviour;

import com.alitalia.aem.common.messages.BaseRequest;
import com.alitalia.aem.common.messages.BaseResponse;

public class VoidBehaviour extends Behaviour<BaseRequest, BaseResponse> {

	@Override
	public BaseResponse executeOrchestration(BaseRequest request) {
		if(request == null) throw new IllegalArgumentException("Base request is null.");
		return new BaseResponse(request.getTid(),request.getSid());
	}	
}