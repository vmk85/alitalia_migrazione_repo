package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.messages.BaseRequest;

public class CheckinReprintBPRequest extends BaseRequest{
	private String airline;
	private String flight;
	private String origin;
	private String departureDate;
	private String lastName;
	private String passengerID;
	
	public CheckinReprintBPRequest(String tid, String sid) {
		super(tid, sid);
	}
	
	public String getAirline() {
		return airline;
	}
	public void setAirline(String airline) {
		this.airline = airline;
	}
	public String getFlight() {
		return flight;
	}
	public void setFlight(String flight) {
		this.flight = flight;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPassengerID() {
		return passengerID;
	}
	public void setPassengerID(String passengerID) {
		this.passengerID = passengerID;
	}
		  
}
