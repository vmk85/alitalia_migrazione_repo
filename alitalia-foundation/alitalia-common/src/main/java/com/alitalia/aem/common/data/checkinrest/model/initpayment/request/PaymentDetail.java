
package com.alitalia.aem.common.data.checkinrest.model.initpayment.request;

public class PaymentDetail {

    private String cardCode;//VISA
 
    private String cardNumber;//444433322221111
 
    private String expireMonth;//08 (Agosto)

    private String expireYear;//2018
 
    private String cardSecurityCode;//CVC 737

    private String cardHolderFirstName;//NOME
 
    private String cardHolderLastName;//COGNOME

    private String amount;//Es.: 23.00
 
    private String currencyCode;//EUR, GBP, USD

    private String cardBin;

    private String approvalCode;//"" non viene valorizzato nella Init
 
    private String transactionID;//"" non viene valorizzato nella Init
 
    private String approvedURL;//

    //private String declinedURL;
 
    private String errorURL;
 
    private AzAddressPayment azAddressPayment; //valorizzato Obbligatoriamente solo in caso di apgamento con AMERICANEXPRESS

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpireMonth() {
        return expireMonth;
    }

    public void setExpireMonth(String expireMonth) {
        this.expireMonth = expireMonth;
    }

    public String getExpireYear() {
        return expireYear;
    }

    public void setExpireYear(String expireYear) {
        this.expireYear = expireYear;
    }

    public String getCardSecurityCode() {
        return cardSecurityCode;
    }

    public void setCardSecurityCode(String cardSecurityCode) {
        this.cardSecurityCode = cardSecurityCode;
    }

    public String getCardHolderFirstName() {
        return cardHolderFirstName;
    }

    public void setCardHolderFirstName(String cardHolderFirstName) {
        this.cardHolderFirstName = cardHolderFirstName;
    }

    public String getCardHolderLastName() {
        return cardHolderLastName;
    }

    public void setCardHolderLastName(String cardHolderLastName) {
        this.cardHolderLastName = cardHolderLastName;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getApprovedURL() {
        return approvedURL;
    }

	public void setApprovedURL(String approvedURL) {
        this.approvedURL = approvedURL;
    }
	
	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	
//    public String getDeclinedURL() {
//        return declinedURL;
//    }

//    public void setDeclinedURL(String declinedURL) {
//        this.declinedURL = declinedURL;
//    }

    public String getErrorURL() {
        return errorURL;
    }

    public void setErrorURL(String errorURL) {
        this.errorURL = errorURL;
    }

    public AzAddressPayment getAzAddressPayment() {
        return azAddressPayment;
    }

    public void setAzAddressPayment(AzAddressPayment azAddressPayment) {
        this.azAddressPayment = azAddressPayment;
    }

    public String getCardBin() {
        return cardBin;
    }

    public void setCardBin(String cardBin) {
        this.cardBin = cardBin;
    }
}
