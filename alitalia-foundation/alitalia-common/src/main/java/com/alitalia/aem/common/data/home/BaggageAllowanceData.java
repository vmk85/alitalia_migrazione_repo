package com.alitalia.aem.common.data.home;

public class BaggageAllowanceData {

	private String code;
	private Integer count;
	
	public BaggageAllowanceData() {
		super();
	}
	
	public BaggageAllowanceData(BaggageAllowanceData clone) {
		super();
		this.code = clone.code;
		this.count = clone.count != null ? new Integer(clone.count) : null;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaggageAllowanceData other = (BaggageAllowanceData) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BaggageAllowance [code=" + code + ", count=" + count + "]";
	}

}
