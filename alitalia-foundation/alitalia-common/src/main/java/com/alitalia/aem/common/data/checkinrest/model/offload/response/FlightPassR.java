package com.alitalia.aem.common.data.checkinrest.model.offload.response;

/**
 * Created by ggadaleta on 25/02/2018.
 */

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FlightPassR {

    @SerializedName("airline")
    @Expose
    private String airline;
    @SerializedName("departureDate")
    @Expose
    private String departureDate;
    @SerializedName("flight")
    @Expose
    private String flight;
    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("seatListRS")
    @Expose
    private List<SeatListR> seatListRS = null;

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getFlight() {
        return flight;
    }

    public void setFlight(String flight) {
        this.flight = flight;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public List<SeatListR> getSeatListRS() {
        return seatListRS;
    }

    public void setSeatListRS(List<SeatListR> seatListRS) {
        this.seatListRS = seatListRS;
    }

}
