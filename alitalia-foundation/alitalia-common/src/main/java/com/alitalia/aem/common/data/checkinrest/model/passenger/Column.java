package com.alitalia.aem.common.data.checkinrest.model.passenger;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Column {
	
	@SerializedName("columnCharacteristic")
	@Expose
	private List<String> columnCharacteristic = null;
	
	@SerializedName("seatCharacteristics")
	@Expose
	private List<String> seatCharacteristics = null;
	
	@SerializedName("padisCode")
	@Expose
	private List<String> padisCode = null;
	
	@SerializedName("facilityList")
	@Expose
	private List<FacilityList> facilityList = null;
	
	@SerializedName("letter")
	@Expose
	private String letter;
	
	@SerializedName("blockCode")
	@Expose
	private String blockCode;

	public List<String> getColumnCharacteristic() {
		return columnCharacteristic;
	}

	public void setColumnCharacteristic(List<String> columnCharacteristic) {
		this.columnCharacteristic = columnCharacteristic;
	}

	public List<String> getSeatCharacteristics() {
		return seatCharacteristics;
	}

	public void setSeatCharacteristics(List<String> seatCharacteristics) {
		this.seatCharacteristics = seatCharacteristics;
	}

	public List<String> getPadisCode() {
		return padisCode;
	}

	public void setPadisCode(List<String> padisCode) {
		this.padisCode = padisCode;
	}

	public List<FacilityList> getFacilityList() {
		return facilityList;
	}

	public void setFacilityList(List<FacilityList> facilityList) {
		this.facilityList = facilityList;
	}

	public String getLetter() {
		return letter;
	}

	public void setLetter(String letter) {
		this.letter = letter;
	}

	public String getBlockCode() {
		return blockCode;
	}

	public void setBlockCode(String blockCode) {
		this.blockCode = blockCode;
	}

}
