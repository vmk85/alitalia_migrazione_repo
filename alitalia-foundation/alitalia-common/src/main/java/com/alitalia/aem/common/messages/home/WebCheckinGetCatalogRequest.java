package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryPassengerData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryFlightData;
import com.alitalia.aem.common.messages.MmbBaseRequest;

public class WebCheckinGetCatalogRequest extends MmbBaseRequest {

    private String client;
    private List<MmbAncillaryFlightData> flights;
    private String language;
    private String machineName;
    private String market;
    private List<CheckinAncillaryPassengerData> passengers;
    private String sessionId;

	public WebCheckinGetCatalogRequest(String tid, String sid) {
		super(tid, sid);
	}

	public WebCheckinGetCatalogRequest() {
		super();
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public List<MmbAncillaryFlightData> getFlights() {
		return flights;
	}

	public void setFlights(List<MmbAncillaryFlightData> flights) {
		this.flights = flights;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getMachineName() {
		return machineName;
	}

	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public List<CheckinAncillaryPassengerData> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<CheckinAncillaryPassengerData> passengers) {
		this.passengers = passengers;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((client == null) ? 0 : client.hashCode());
		result = prime * result + ((flights == null) ? 0 : flights.hashCode());
		result = prime * result
				+ ((language == null) ? 0 : language.hashCode());
		result = prime * result
				+ ((machineName == null) ? 0 : machineName.hashCode());
		result = prime * result + ((market == null) ? 0 : market.hashCode());
		result = prime * result
				+ ((passengers == null) ? 0 : passengers.hashCode());
		result = prime * result
				+ ((sessionId == null) ? 0 : sessionId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebCheckinGetCatalogRequest other = (WebCheckinGetCatalogRequest) obj;
		if (client == null) {
			if (other.client != null)
				return false;
		} else if (!client.equals(other.client))
			return false;
		if (flights == null) {
			if (other.flights != null)
				return false;
		} else if (!flights.equals(other.flights))
			return false;
		if (language == null) {
			if (other.language != null)
				return false;
		} else if (!language.equals(other.language))
			return false;
		if (machineName == null) {
			if (other.machineName != null)
				return false;
		} else if (!machineName.equals(other.machineName))
			return false;
		if (market == null) {
			if (other.market != null)
				return false;
		} else if (!market.equals(other.market))
			return false;
		if (passengers == null) {
			if (other.passengers != null)
				return false;
		} else if (!passengers.equals(other.passengers))
			return false;
		if (sessionId == null) {
			if (other.sessionId != null)
				return false;
		} else if (!sessionId.equals(other.sessionId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WebCheckinGetCatalogRequest [client=" + client + ", flights="
				+ flights + ", language=" + language + ", machineName="
				+ machineName + ", market=" + market + ", passengers="
				+ passengers + ", sessionId=" + sessionId + "]";
	}
}