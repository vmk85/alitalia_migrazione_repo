package com.alitalia.aem.common.data.home;

public class FrequentFlyerTypeData {

	private String code;
	private String description;
	private Integer lenght;
	private String preFillChar;
	private String regularExpressionValidation;
	private String errorCodeCardFrequentFlyer;
	
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Integer getLenght() {
		return lenght;
	}
	
	public void setLenght(Integer lenght) {
		this.lenght = lenght;
	}
	
	public String getPreFillChar() {
		return preFillChar;
	}
	
	public void setPreFillChar(String preFillChar) {
		this.preFillChar = preFillChar;
	}
	
	public String getRegularExpressionValidation() {
		return regularExpressionValidation;
	}
	
	public void setRegularExpressionValidation(String regularExpressionValidation) {
		this.regularExpressionValidation = regularExpressionValidation;
	}
	
	public String getErrorCodeCardFrequentFlyer() {
		return errorCodeCardFrequentFlyer;
	}

	public void setErrorCodeCardFrequentFlyer(String errorCodeCardFrequentFlyer) {
		this.errorCodeCardFrequentFlyer = errorCodeCardFrequentFlyer;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FrequentFlyerTypeData other = (FrequentFlyerTypeData) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FrequentFlyerTypeData [code=" + code + ", description="
				+ description + ", lenght=" + lenght + ", preFillChar="
				+ preFillChar + ", regularExpressionValidation="
				+ regularExpressionValidation + ", errorCodeCardFrequentFlyer=" + errorCodeCardFrequentFlyer +"]";
	}

	
}
