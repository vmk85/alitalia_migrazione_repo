
package com.alitalia.aem.common.data.checkinrest.model.pnrinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DocumentRequired {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("detailStatus")
    @Expose
    private String detailStatus;
    @SerializedName("freeText")
    @Expose
    private String freeText;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDetailStatus() {
        return detailStatus;
    }

    public void setDetailStatus(String detailStatus) {
        this.detailStatus = detailStatus;
    }

    public String getFreeText() {
        return freeText;
    }

    public void setFreeText(String freeText) {
        this.freeText = freeText;
    }

}
