package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.messages.BaseRequest;

public class InfoPassengerSubmitPassengerRequest extends BaseRequest {
	
	private String cug;
	private List<PassengerBaseData> passengers;
	
	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	String market;

	public InfoPassengerSubmitPassengerRequest() {
	}

	public List<PassengerBaseData> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<PassengerBaseData> passengers) {
		this.passengers = passengers;
	}

	public String getCug() {
		return cug;
	}

	public void setCug(String cug) {
		this.cug = cug;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((cug == null) ? 0 : cug.hashCode());
		result = prime * result
				+ ((passengers == null) ? 0 : passengers.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		InfoPassengerSubmitPassengerRequest other = (InfoPassengerSubmitPassengerRequest) obj;
		if (cug == null) {
			if (other.cug != null)
				return false;
		} else if (!cug.equals(other.cug))
			return false;
		if (passengers == null) {
			if (other.passengers != null)
				return false;
		} else if (!passengers.equals(other.passengers))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "InfoPassengerSubmitPassengerRequest [cug=" + cug
				+ ", passengers=" + passengers + ", getTid()=" + getTid()
				+ ", getSid()=" + getSid() + ", getCookie()=" + getCookie()
				+ ", getExecution()=" + getExecution()
				+ ", getSabreGateWayAuthToken()=" + getSabreGateWayAuthToken()
				+ ", toString()=" + super.toString() + ", getClass()="
				+ getClass() + "]";
	}
}