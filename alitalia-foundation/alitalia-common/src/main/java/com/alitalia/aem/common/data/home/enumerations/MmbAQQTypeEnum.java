package com.alitalia.aem.common.data.home.enumerations;

public enum MmbAQQTypeEnum {

	CLEARED("Cleared"),
	NOT_CLEARED("NotCleared");
	
    private final String value;

    MmbAQQTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MmbAQQTypeEnum fromValue(String v) {
        for (MmbAQQTypeEnum c: MmbAQQTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
