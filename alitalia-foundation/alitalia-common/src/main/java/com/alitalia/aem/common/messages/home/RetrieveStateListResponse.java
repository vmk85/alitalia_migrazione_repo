package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.StateData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetrieveStateListResponse extends BaseResponse {

	private List<StateData> states;

	public List<StateData> getStates() {
		return states;
	}

	public void setStates(List<StateData> states) {
		this.states = states;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((states == null) ? 0 : states.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveStateListResponse other = (RetrieveStateListResponse) obj;
		if (states == null) {
			if (other.states != null)
				return false;
		} else if (!states.equals(other.states))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveStateListResponse [states=" + states + "]";
	}
}