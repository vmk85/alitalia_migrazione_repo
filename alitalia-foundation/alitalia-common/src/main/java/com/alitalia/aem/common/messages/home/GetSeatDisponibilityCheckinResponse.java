package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.checkin.CheckinKeyValueOfCompartimentalClassData;
import com.alitalia.aem.common.messages.BaseResponse;

public class GetSeatDisponibilityCheckinResponse extends BaseResponse {

	private List<CheckinKeyValueOfCompartimentalClassData> keyValueOfCompartimentalClassint0HzHfQkm;

	public List<CheckinKeyValueOfCompartimentalClassData> getKeyValueOfCompartimentalClassint0HzHfQkm() {
		return keyValueOfCompartimentalClassint0HzHfQkm;
	}

	public void setKeyValueOfCompartimentalClassint0HzHfQkm(
			List<CheckinKeyValueOfCompartimentalClassData> keyValueOfCompartimentalClassint0HzHfQkm) {
		this.keyValueOfCompartimentalClassint0HzHfQkm = keyValueOfCompartimentalClassint0HzHfQkm;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((keyValueOfCompartimentalClassint0HzHfQkm == null) ? 0
						: keyValueOfCompartimentalClassint0HzHfQkm.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		GetSeatDisponibilityCheckinResponse other = (GetSeatDisponibilityCheckinResponse) obj;
		if (keyValueOfCompartimentalClassint0HzHfQkm == null) {
			if (other.keyValueOfCompartimentalClassint0HzHfQkm != null)
				return false;
		} else if (!keyValueOfCompartimentalClassint0HzHfQkm
				.equals(other.keyValueOfCompartimentalClassint0HzHfQkm))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "GetSeatDisponibilityCheckinResponse [keyValueOfCompartimentalClassint0HzHfQkm="
				+ keyValueOfCompartimentalClassint0HzHfQkm
				+ ", toString()="
				+ super.toString() + "]";
	}

}
