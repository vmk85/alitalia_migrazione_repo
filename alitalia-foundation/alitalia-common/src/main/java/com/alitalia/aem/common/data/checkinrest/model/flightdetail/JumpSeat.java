
package com.alitalia.aem.common.data.checkinrest.model.flightdetail;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JumpSeat {

    @SerializedName("cockpit")
    @Expose
    private List<Cockpit> cockpit = null;
    @SerializedName("cabin")
    @Expose
    private List<Cabin> cabin = null;

    public List<Cockpit> getCockpit() {
        return cockpit;
    }

    public void setCockpit(List<Cockpit> cockpit) {
        this.cockpit = cockpit;
    }

    public List<Cabin> getCabin() {
        return cabin;
    }

    public void setCabin(List<Cabin> cabin) {
        this.cabin = cabin;
    }

}
