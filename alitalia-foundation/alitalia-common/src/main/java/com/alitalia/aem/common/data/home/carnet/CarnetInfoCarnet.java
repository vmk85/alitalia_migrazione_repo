package com.alitalia.aem.common.data.home.carnet;

import java.math.BigDecimal;
import java.util.Calendar;

public class CarnetInfoCarnet {
	
	private String buyerEmail;
    private String buyerLastname;
    private String buyerName;
    private String carnetCode;
    private Calendar creationDate;
    private Calendar expiryDate;
    private Integer id;
    private String password;
    private String purchasedCarnetCode;
    private BigDecimal residualFare;
    private Short residualRoutes;
    private BigDecimal totalFare;
    private Short totalRoutes;
    private BigDecimal usedFare;
    private Short usedRoutes;
    private String eCouponCode;
    private String emdCode;
    
	public String getBuyerEmail() {
		return buyerEmail;
	}
	public void setBuyerEmail(String buyerEmail) {
		this.buyerEmail = buyerEmail;
	}
	public String getBuyerLastname() {
		return buyerLastname;
	}
	public void setBuyerLastname(String buyerLastname) {
		this.buyerLastname = buyerLastname;
	}
	public String getBuyerName() {
		return buyerName;
	}
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}
	public String getCarnetCode() {
		return carnetCode;
	}
	public void setCarnetCode(String carnetCode) {
		this.carnetCode = carnetCode;
	}
	public Calendar getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}
	public Calendar getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Calendar expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPurchasedCarnetCode() {
		return purchasedCarnetCode;
	}
	public void setPurchasedCarnetCode(String purchasedCarnetCode) {
		this.purchasedCarnetCode = purchasedCarnetCode;
	}
	public BigDecimal getResidualFare() {
		return residualFare;
	}
	public void setResidualFare(BigDecimal residualFare) {
		this.residualFare = residualFare;
	}
	public Short getResidualRoutes() {
		return residualRoutes;
	}
	public void setResidualRoutes(Short residualRoutes) {
		this.residualRoutes = residualRoutes;
	}
	public BigDecimal getTotalFare() {
		return totalFare;
	}
	public void setTotalFare(BigDecimal totalFare) {
		this.totalFare = totalFare;
	}
	public Short getTotalRoutes() {
		return totalRoutes;
	}
	public void setTotalRoutes(Short totalRoutes) {
		this.totalRoutes = totalRoutes;
	}
	public BigDecimal getUsedFare() {
		return usedFare;
	}
	public void setUsedFare(BigDecimal usedFare) {
		this.usedFare = usedFare;
	}
	public Short getUsedRoutes() {
		return usedRoutes;
	}
	public void setUsedRoutes(Short usedRoutes) {
		this.usedRoutes = usedRoutes;
	}
	public String geteCouponCode() {
		return eCouponCode;
	}
	public void seteCouponCode(String eCouponCode) {
		this.eCouponCode = eCouponCode;
	}
	public String getEmdCode() {
		return emdCode;
	}
	public void setEmdCode(String emdCode) {
		this.emdCode = emdCode;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((buyerEmail == null) ? 0 : buyerEmail.hashCode());
		result = prime * result
				+ ((buyerLastname == null) ? 0 : buyerLastname.hashCode());
		result = prime * result
				+ ((buyerName == null) ? 0 : buyerName.hashCode());
		result = prime * result
				+ ((carnetCode == null) ? 0 : carnetCode.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result
				+ ((eCouponCode == null) ? 0 : eCouponCode.hashCode());
		result = prime * result + ((emdCode == null) ? 0 : emdCode.hashCode());
		result = prime * result
				+ ((expiryDate == null) ? 0 : expiryDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		result = prime
				* result
				+ ((purchasedCarnetCode == null) ? 0 : purchasedCarnetCode
						.hashCode());
		result = prime * result
				+ ((residualFare == null) ? 0 : residualFare.hashCode());
		result = prime * result
				+ ((residualRoutes == null) ? 0 : residualRoutes.hashCode());
		result = prime * result
				+ ((totalFare == null) ? 0 : totalFare.hashCode());
		result = prime * result
				+ ((totalRoutes == null) ? 0 : totalRoutes.hashCode());
		result = prime * result
				+ ((usedFare == null) ? 0 : usedFare.hashCode());
		result = prime * result
				+ ((usedRoutes == null) ? 0 : usedRoutes.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarnetInfoCarnet other = (CarnetInfoCarnet) obj;
		if (buyerEmail == null) {
			if (other.buyerEmail != null)
				return false;
		} else if (!buyerEmail.equals(other.buyerEmail))
			return false;
		if (buyerLastname == null) {
			if (other.buyerLastname != null)
				return false;
		} else if (!buyerLastname.equals(other.buyerLastname))
			return false;
		if (buyerName == null) {
			if (other.buyerName != null)
				return false;
		} else if (!buyerName.equals(other.buyerName))
			return false;
		if (carnetCode == null) {
			if (other.carnetCode != null)
				return false;
		} else if (!carnetCode.equals(other.carnetCode))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (eCouponCode == null) {
			if (other.eCouponCode != null)
				return false;
		} else if (!eCouponCode.equals(other.eCouponCode))
			return false;
		if (emdCode == null) {
			if (other.emdCode != null)
				return false;
		} else if (!emdCode.equals(other.emdCode))
			return false;
		if (expiryDate == null) {
			if (other.expiryDate != null)
				return false;
		} else if (!expiryDate.equals(other.expiryDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (purchasedCarnetCode == null) {
			if (other.purchasedCarnetCode != null)
				return false;
		} else if (!purchasedCarnetCode.equals(other.purchasedCarnetCode))
			return false;
		if (residualFare == null) {
			if (other.residualFare != null)
				return false;
		} else if (!residualFare.equals(other.residualFare))
			return false;
		if (residualRoutes == null) {
			if (other.residualRoutes != null)
				return false;
		} else if (!residualRoutes.equals(other.residualRoutes))
			return false;
		if (totalFare == null) {
			if (other.totalFare != null)
				return false;
		} else if (!totalFare.equals(other.totalFare))
			return false;
		if (totalRoutes == null) {
			if (other.totalRoutes != null)
				return false;
		} else if (!totalRoutes.equals(other.totalRoutes))
			return false;
		if (usedFare == null) {
			if (other.usedFare != null)
				return false;
		} else if (!usedFare.equals(other.usedFare))
			return false;
		if (usedRoutes == null) {
			if (other.usedRoutes != null)
				return false;
		} else if (!usedRoutes.equals(other.usedRoutes))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "CarnetInfoCarnet [buyerEmail=" + buyerEmail
				+ ", buyerLastname=" + buyerLastname + ", buyerName="
				+ buyerName + ", carnetCode=" + carnetCode + ", creationDate="
				+ creationDate + ", expiryDate=" + expiryDate + ", id=" + id
				+ ", password=" + password + ", purchasedCarnetCode="
				+ purchasedCarnetCode + ", residualFare=" + residualFare
				+ ", residualRoutes=" + residualRoutes + ", totalFare="
				+ totalFare + ", totalRoutes=" + totalRoutes + ", usedFare="
				+ usedFare + ", usedRoutes=" + usedRoutes + ", eCouponCode="
				+ eCouponCode + ", emdCode=" + emdCode + "]";
	}
}