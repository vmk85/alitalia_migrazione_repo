package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.PaymentProcessInfoData;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;
import com.alitalia.aem.common.messages.BaseRequest;

public class CheckPaymentRequest extends BaseRequest {
	private PaymentProcessInfoData process;
	private PaymentTypeEnum type;
	
	public PaymentProcessInfoData getProcess() {
		return process;
	}
	public void setProcess(PaymentProcessInfoData process) {
		this.process = process;
	}
	public PaymentTypeEnum getType() {
		return type;
	}
	public void setType(PaymentTypeEnum type) {
		this.type = type;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((process == null) ? 0 : process.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckPaymentRequest other = (CheckPaymentRequest) obj;
		if (process == null) {
			if (other.process != null)
				return false;
		} else if (!process.equals(other.process))
			return false;
		if (type != other.type)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CheckPaymentRequest [process=" + process + ", type=" + type
				+ "]";
	}
	
}
