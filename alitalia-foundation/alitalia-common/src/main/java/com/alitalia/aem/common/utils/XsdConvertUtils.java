package com.alitalia.aem.common.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.xml.bind.DatatypeConverter;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility conversion methods used to manually decode string-encoded
 * values from SOAP responses.
 * 
 * <p>In some cases, the implemented behavior may be more lenient 
 * than the strict encoding described in XSD specifications.</p>
 */
public class XsdConvertUtils {

	private static Logger logger = LoggerFactory.getLogger(XsdConvertUtils.class);

	/**
	 * Converts the string argument into a boolean value.
	 * 
	 * <p>In addition to standard decoding, force check on lower-case string
	 * and returns a Boolean (instead of boolean).</p>
	 * 
	 * @see DatatypeConverter.parseBoolean
	 * 
	 * @param lexicalXSDBoolean A string containing lexical representation of xsd:boolean.
	 * @return A Boolean value represented by the string argument. 
	 */
	public static Boolean parseBoolean(String lexicalXSDBoolean) {
		Boolean returnValue = Boolean.FALSE;
		if (lexicalXSDBoolean != null && !"".equals(lexicalXSDBoolean)) {
			returnValue = DatatypeConverter.parseBoolean(lexicalXSDBoolean.toLowerCase());
		}
		return returnValue;
	}
	
	/**
	 * Converts the XMLGregorianCalendar argument into a Calendar value.
	 * 
	 * 
	 * @param xmlCalendar An object from XMLGregorianCalendar class.
	 * @return A Calendar value represented by the XMLGregorianCalendar argument.
	 */
	public static Calendar parseCalendar(XMLGregorianCalendar xmlCalendar) {
		Calendar returnValue = null;
		if (xmlCalendar != null) {
			returnValue = xmlCalendar.toGregorianCalendar();
			if (xmlCalendar.getTimezone() == DatatypeConstants.FIELD_UNDEFINED) {
				returnValue.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
			}
		}
		return returnValue;
	}
	
	/**
	 * Converts the string argument into a Date value.
	 * 
	 * <p>In addition to standard decoding, try to determine automatically how to
	 * parse the string (date or datetime) and returns a Date (instead of Calendar).</p>
	 * 
	 * @param lexicalXSDDate A string containing lexical representation of xsd:date or xsd:datetime.
	 * @return A Date value represented by the string argument.
	 */
	public static Date parseDate(String lexicalXSDDate) {
		Date returnValue = null;
		if (lexicalXSDDate != null && !"".equals(lexicalXSDDate)) {
			Calendar parsedCalendar = null;
			if (lexicalXSDDate.indexOf("T") >= 0) {
				parsedCalendar = DatatypeConverter.parseDateTime(lexicalXSDDate);
			} else {
				parsedCalendar = DatatypeConverter.parseDate(lexicalXSDDate);
			}
			if (parsedCalendar != null) {
				returnValue = parsedCalendar.getTime();
			}
		}
		return returnValue;
	}
	
	/**
	 * Converts the XMLGregorianCalendar argument into a Date value.
	 * 
	 * 
	 * @param xmlCalendar An object from XMLGregorianCalendar class.
	 * @return A Date value represented by the XMLGregorianCalendar argument.
	 */
	public static Date parseDate(XMLGregorianCalendar xmlCalendar) {
		Date returnValue = null;
		if (xmlCalendar != null) {
			returnValue = xmlCalendar.toGregorianCalendar().getTime();
		}
		return returnValue;
	}
	
	/**
	 * Converts the string argument into a Calendar value.
	 * 
	 * <p>In addition to standard decoding, try to determine automatically how to
	 * parse the string (date or datetime) and returns a Calendar.</p>
	 * 
	 * @param lexicalXSDDate A string containing lexical representation of xsd:date or xsd:datetime.
	 * @return A Calendar value represented by the string argument.
	 */
	public static Calendar parseCalendar(String lexicalXSDDate) {
		Calendar returnValue = null;
		if (lexicalXSDDate != null && !"".equals(lexicalXSDDate)) {
			if (lexicalXSDDate.indexOf("T") >= 0) {
				returnValue = DatatypeConverter.parseDateTime(lexicalXSDDate);
			} else {
				returnValue = DatatypeConverter.parseDate(lexicalXSDDate);
			}
		}
		return returnValue;
	}
	
	/**
	 * Converts a Date value into a string.
	 * 
	 * <p>In addition to standard encoding, accepts a Date (instead of Calendar) and
	 * convert a null Date instance into an empty String (instead of throwing an exception).</p>
	 * 
	 * @param val A Date value.
	 * @return A string containing a lexical representation of xsd:dateTime.
	 */
	public static String printDateTime(Date val) {
		String returnValue = "";
		if (val != null) {
			Calendar calendarVal = Calendar.getInstance();
			calendarVal.setTime(val);
			returnValue = DatatypeConverter.printDateTime(calendarVal);
		}
		return returnValue;
	}
	
	/**
	 * Converts a Calendar value into a string.
	 * 
	 * 
	 * @param val A Calendar value.
	 * @return A string containing a lexical representation of xsd:dateTime.
	 * @param val
	 * @return
	 */
	public static String printDateTime(Calendar val) {
		String returnValue = "";
		if (val != null) {
			returnValue = DatatypeConverter.printDateTime(val);
		}
		return returnValue;
	}
	
	/**
	 * Converts the Date argument into a XMLGregorianCalendar value.
	 * 
	 * 
	 * @param inputDate An object from Date class.
	 * @return An XMLGregorianCalendar value represented by the Date argument.
	 */
	public static XMLGregorianCalendar toXMLGregorianCalendar(Date inputDate) {
		XMLGregorianCalendar returnValue = null;
		if (inputDate != null) {
			try {
				DatatypeFactory dtf = DatatypeFactory.newInstance();
				GregorianCalendar cal = new GregorianCalendar();

				cal.setTime(inputDate);
				returnValue = dtf.newXMLGregorianCalendar(cal);
			} catch (DatatypeConfigurationException e) {
				logger.error("Error while creating DatatypeFactory object: {}", e);
			}
		}
		return returnValue;
	}
	
	/**
	 * Converts the Calendar argument into a XMLGregorianCalendar value.
	 * 
	 * 
	 * @param inputCalendar An object from Calendar class.
	 * @return An XMLGregorianCalendar value represented by the Date argument.
	 */
	public static XMLGregorianCalendar toXMLGregorianCalendar(Calendar inputCalendar) {
		logger.debug("Converting Calendar {} to XMLGregorianCalendar", inputCalendar);
		XMLGregorianCalendar returnValue = null;
		if (inputCalendar != null) {
			try {
				if (inputCalendar instanceof GregorianCalendar){
					returnValue = DatatypeFactory.newInstance().newXMLGregorianCalendar((GregorianCalendar)inputCalendar);
				} else {
					DatatypeFactory dtf = DatatypeFactory.newInstance();
					GregorianCalendar cal = new GregorianCalendar();
					if (inputCalendar.get(Calendar.YEAR) < 1582 ) //&& !(inputCalendar instanceof GregorianCalendar))
						cal.setGregorianChange(new Date(Long.MIN_VALUE));
					cal.setTimeInMillis(inputCalendar.getTimeInMillis());
					cal.setTimeZone(inputCalendar.getTimeZone());
					logger.debug("Gregorian Calendar: {}", cal);
					returnValue = dtf.newXMLGregorianCalendar(cal);
				}
				logger.debug("XML Gregorian Calendar: {}", returnValue);
			} catch (DatatypeConfigurationException e) {
				logger.error("Error while creating DatatypeFactory object: {}", e);
			}
		}
		return returnValue;
	}
	
}
