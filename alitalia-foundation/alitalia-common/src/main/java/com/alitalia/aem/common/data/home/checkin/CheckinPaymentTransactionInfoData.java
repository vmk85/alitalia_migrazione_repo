package com.alitalia.aem.common.data.home.checkin;

import com.alitalia.aem.common.data.home.enumerations.CheckinPaymentTransactionInfoResponseTypeEnum;

public class CheckinPaymentTransactionInfoData {

	private Integer id;
	private String acceptHeader;
	private String authorizationID;
	private Integer errorCode;
	private String errorDescription;
	private String marketCode;
	private String opaqueParameters;
	private String orderDescription;
	private String orderCode;
	private String requestData;
	private String sessionId;
	private String siteCode;
	private String threeDSecureIssueURL;
	private CheckinPaymentTransactionInfoResponseTypeEnum type;
	private String userAgentHeader;
	private String userHostAddress;
	private String vbvPaRequest;
	private String vbvPaResponse;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAcceptHeader() {
		return acceptHeader;
	}
	public void setAcceptHeader(String acceptHeader) {
		this.acceptHeader = acceptHeader;
	}
	public String getAuthorizationID() {
		return authorizationID;
	}
	public void setAuthorizationID(String authorizationID) {
		this.authorizationID = authorizationID;
	}
	public Integer getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorDescription() {
		return errorDescription;
	}
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	public String getMarketCode() {
		return marketCode;
	}
	public void setMarketCode(String marketCode) {
		this.marketCode = marketCode;
	}
	public String getOpaqueParameters() {
		return opaqueParameters;
	}
	public void setOpaqueParameters(String opaqueParameters) {
		this.opaqueParameters = opaqueParameters;
	}
	public String getOrderDescription() {
		return orderDescription;
	}
	public void setOrderDescription(String orderDescription) {
		this.orderDescription = orderDescription;
	}
	public String getOrderCode() {
		return orderCode;
	}
	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}
	public String getRequestData() {
		return requestData;
	}
	public void setRequestData(String requestData) {
		this.requestData = requestData;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getSiteCode() {
		return siteCode;
	}
	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}
	public String getThreeDSecureIssueURL() {
		return threeDSecureIssueURL;
	}
	public void setThreeDSecureIssueURL(String threeDSecureIssueURL) {
		this.threeDSecureIssueURL = threeDSecureIssueURL;
	}
	public CheckinPaymentTransactionInfoResponseTypeEnum getType() {
		return type;
	}
	public void setType(CheckinPaymentTransactionInfoResponseTypeEnum type) {
		this.type = type;
	}
	public String getUserAgentHeader() {
		return userAgentHeader;
	}
	public void setUserAgentHeader(String userAgentHeader) {
		this.userAgentHeader = userAgentHeader;
	}
	public String getUserHostAddress() {
		return userHostAddress;
	}
	public void setUserHostAddress(String userHostAddress) {
		this.userHostAddress = userHostAddress;
	}
	public String getVbvPaRequest() {
		return vbvPaRequest;
	}
	public void setVbvPaRequest(String vbvPaRequest) {
		this.vbvPaRequest = vbvPaRequest;
	}
	public String getVbvPaResponse() {
		return vbvPaResponse;
	}
	public void setVbvPaResponse(String vbvPaResponse) {
		this.vbvPaResponse = vbvPaResponse;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((acceptHeader == null) ? 0 : acceptHeader.hashCode());
		result = prime * result
				+ ((authorizationID == null) ? 0 : authorizationID.hashCode());
		result = prime * result
				+ ((errorCode == null) ? 0 : errorCode.hashCode());
		result = prime
				* result
				+ ((errorDescription == null) ? 0 : errorDescription.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((marketCode == null) ? 0 : marketCode.hashCode());
		result = prime
				* result
				+ ((opaqueParameters == null) ? 0 : opaqueParameters.hashCode());
		result = prime * result
				+ ((orderCode == null) ? 0 : orderCode.hashCode());
		result = prime
				* result
				+ ((orderDescription == null) ? 0 : orderDescription.hashCode());
		result = prime * result
				+ ((requestData == null) ? 0 : requestData.hashCode());
		result = prime * result
				+ ((sessionId == null) ? 0 : sessionId.hashCode());
		result = prime * result
				+ ((siteCode == null) ? 0 : siteCode.hashCode());
		result = prime
				* result
				+ ((threeDSecureIssueURL == null) ? 0 : threeDSecureIssueURL
						.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result
				+ ((userAgentHeader == null) ? 0 : userAgentHeader.hashCode());
		result = prime * result
				+ ((userHostAddress == null) ? 0 : userHostAddress.hashCode());
		result = prime * result
				+ ((vbvPaRequest == null) ? 0 : vbvPaRequest.hashCode());
		result = prime * result
				+ ((vbvPaResponse == null) ? 0 : vbvPaResponse.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinPaymentTransactionInfoData other = (CheckinPaymentTransactionInfoData) obj;
		if (acceptHeader == null) {
			if (other.acceptHeader != null)
				return false;
		} else if (!acceptHeader.equals(other.acceptHeader))
			return false;
		if (authorizationID == null) {
			if (other.authorizationID != null)
				return false;
		} else if (!authorizationID.equals(other.authorizationID))
			return false;
		if (errorCode == null) {
			if (other.errorCode != null)
				return false;
		} else if (!errorCode.equals(other.errorCode))
			return false;
		if (errorDescription == null) {
			if (other.errorDescription != null)
				return false;
		} else if (!errorDescription.equals(other.errorDescription))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (marketCode == null) {
			if (other.marketCode != null)
				return false;
		} else if (!marketCode.equals(other.marketCode))
			return false;
		if (opaqueParameters == null) {
			if (other.opaqueParameters != null)
				return false;
		} else if (!opaqueParameters.equals(other.opaqueParameters))
			return false;
		if (orderCode == null) {
			if (other.orderCode != null)
				return false;
		} else if (!orderCode.equals(other.orderCode))
			return false;
		if (orderDescription == null) {
			if (other.orderDescription != null)
				return false;
		} else if (!orderDescription.equals(other.orderDescription))
			return false;
		if (requestData == null) {
			if (other.requestData != null)
				return false;
		} else if (!requestData.equals(other.requestData))
			return false;
		if (sessionId == null) {
			if (other.sessionId != null)
				return false;
		} else if (!sessionId.equals(other.sessionId))
			return false;
		if (siteCode == null) {
			if (other.siteCode != null)
				return false;
		} else if (!siteCode.equals(other.siteCode))
			return false;
		if (threeDSecureIssueURL == null) {
			if (other.threeDSecureIssueURL != null)
				return false;
		} else if (!threeDSecureIssueURL.equals(other.threeDSecureIssueURL))
			return false;
		if (type != other.type)
			return false;
		if (userAgentHeader == null) {
			if (other.userAgentHeader != null)
				return false;
		} else if (!userAgentHeader.equals(other.userAgentHeader))
			return false;
		if (userHostAddress == null) {
			if (other.userHostAddress != null)
				return false;
		} else if (!userHostAddress.equals(other.userHostAddress))
			return false;
		if (vbvPaRequest == null) {
			if (other.vbvPaRequest != null)
				return false;
		} else if (!vbvPaRequest.equals(other.vbvPaRequest))
			return false;
		if (vbvPaResponse == null) {
			if (other.vbvPaResponse != null)
				return false;
		} else if (!vbvPaResponse.equals(other.vbvPaResponse))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CheckinPaymentTransactionInfoData [id=" + id
				+ ", acceptHeader=" + acceptHeader + ", authorizationID="
				+ authorizationID + ", errorCode=" + errorCode
				+ ", errorDescription=" + errorDescription + ", marketCode="
				+ marketCode + ", opaqueParameters=" + opaqueParameters
				+ ", orderDescription=" + orderDescription + ", orderCode="
				+ orderCode + ", requestData=" + requestData + ", sessionId="
				+ sessionId + ", siteCode=" + siteCode
				+ ", threeDSecureIssueURL=" + threeDSecureIssueURL + ", type="
				+ type + ", userAgentHeader=" + userAgentHeader
				+ ", userHostAddress=" + userHostAddress + ", vbvPaRequest="
				+ vbvPaRequest + ", vbvPaResponse=" + vbvPaResponse + "]";
	}
	
}