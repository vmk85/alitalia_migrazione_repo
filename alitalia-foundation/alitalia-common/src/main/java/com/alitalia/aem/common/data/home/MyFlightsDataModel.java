package com.alitalia.aem.common.data.home;

public class MyFlightsDataModel {

    private String flight_departureDate;
    private String flight_firstName;
    private String flight_lastName;
    private String flight_nationDestination;
    private String flight_pnr;

    public String getFlight_departureDate() {
        return flight_departureDate;
    }

    public void setFlight_departureDate(String flight_departureDate) {
        this.flight_departureDate = flight_departureDate;
    }

    public String getFlight_firstName() {
        return flight_firstName;
    }

    public void setFlight_firstName(String flight_firstName) {
        this.flight_firstName = flight_firstName;
    }

    public String getFlight_lastName() {
        return flight_lastName;
    }

    public void setFlight_lastName(String flight_lastName) {
        this.flight_lastName = flight_lastName;
    }

    public String getFlight_nationDestination() {
        return flight_nationDestination;
    }

    public void setFlight_nationDestination(String flight_nationDestination) {
        this.flight_nationDestination = flight_nationDestination;
    }

    public String getFlight_pnr() {
        return flight_pnr;
    }

    public void setFlight_pnr(String flight_pnr) {
        this.flight_pnr = flight_pnr;
    }

    @Override
    public String toString() {
        return "MyFlightsDataModel{" +
                "flight_departureDate='" + flight_departureDate + '\'' +
                ", flight_firstName='" + flight_firstName + '\'' +
                ", flight_lastName='" + flight_lastName + '\'' +
                ", flight_nationDestination='" + flight_nationDestination + '\'' +
                ", flight_pnr='" + flight_pnr + '\'' +
                '}';
    }
}
