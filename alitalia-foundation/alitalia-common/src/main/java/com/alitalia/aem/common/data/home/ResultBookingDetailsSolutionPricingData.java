package com.alitalia.aem.common.data.home;

import java.util.List;

public class ResultBookingDetailsSolutionPricingData {
	
	private ResultBookingDetailsSolutionPricingAddCollectPriceData addCollectPriceField;
	private List<ResultBookingDetailsSolutionPricingBookingInfoData> bookingInfoField;
	private ResultBookingDetailsSolutionPricingCocFareDifferenceData cocFareDifferenceField;
	private ResultBookingDetailsSolutionPricingCocFareTotalData cocFareTotalField;
	private ResultBookingDetailsSolutionPricingExtData extField;
	private List<ResultBookingDetailsSolutionPricingFareCalculationData> fareCalculationField;
	private List<ResultBookingDetailsSolutionPricingFareData> fareField;
	private List<ResultBookingDetailsSolutionPricingFareRestrictionData> fareRestrictionField;
	private List<ResultBookingDetailsSolutionPricingNoteData> noteField;
	private int paxCountField;
	private List<ResultBookingDetailsSolutionPricingPenaltyPriceData> penaltyPriceField;
	private ResultBookingDetailsSolutionPricingPreviousCocFareTotalData previousCocFareTotalField;
	private List<ResultBookingDetailsSolutionPricingRefundPriceData> refundPriceField;
	private List<ResultBookingDetailsSolutionPricingReissueInfoData> reissueInfoField;
	private ResultBookingDetailsSolutionPricingReissueTaxTotalDifferenceData reissueTaxTotalDifferenceField;
	private ResultBookingDetailsSolutionPricingSaleFareTotalData saleFareTotalField;
	private ResultBookingDetailsSolutionPricingSalePriceData salePriceField;
	private ResultBookingDetailsSolutionPricingSaleTaxTotalData saleTaxTotalField;
	private List<ResultBookingDetailsSolutionPricingTaxData> taxField;
	
	public ResultBookingDetailsSolutionPricingAddCollectPriceData getAddCollectPriceField() {
		return addCollectPriceField;
	}
	
	public void setAddCollectPriceField(
			ResultBookingDetailsSolutionPricingAddCollectPriceData addCollectPriceField) {
		this.addCollectPriceField = addCollectPriceField;
	}
	
	public List<ResultBookingDetailsSolutionPricingBookingInfoData> getBookingInfoField() {
		return bookingInfoField;
	}
	
	public void setBookingInfoField(
			List<ResultBookingDetailsSolutionPricingBookingInfoData> bookingInfoField) {
		this.bookingInfoField = bookingInfoField;
	}
	
	public ResultBookingDetailsSolutionPricingCocFareDifferenceData getCocFareDifferenceField() {
		return cocFareDifferenceField;
	}
	
	public void setCocFareDifferenceField(
			ResultBookingDetailsSolutionPricingCocFareDifferenceData cocFareDifferenceField) {
		this.cocFareDifferenceField = cocFareDifferenceField;
	}
	
	public ResultBookingDetailsSolutionPricingCocFareTotalData getCocFareTotalField() {
		return cocFareTotalField;
	}
	
	public void setCocFareTotalField(
			ResultBookingDetailsSolutionPricingCocFareTotalData cocFareTotalField) {
		this.cocFareTotalField = cocFareTotalField;
	}
	
	public ResultBookingDetailsSolutionPricingExtData getExtField() {
		return extField;
	}
	
	public void setExtField(ResultBookingDetailsSolutionPricingExtData extField) {
		this.extField = extField;
	}
	
	public List<ResultBookingDetailsSolutionPricingFareCalculationData> getFareCalculationField() {
		return fareCalculationField;
	}
	
	public void setFareCalculationField(
			List<ResultBookingDetailsSolutionPricingFareCalculationData> fareCalculationField) {
		this.fareCalculationField = fareCalculationField;
	}
	
	public List<ResultBookingDetailsSolutionPricingFareData> getFareField() {
		return fareField;
	}
	
	public void setFareField(
			List<ResultBookingDetailsSolutionPricingFareData> fareField) {
		this.fareField = fareField;
	}
	
	public List<ResultBookingDetailsSolutionPricingFareRestrictionData> getFareRestrictionField() {
		return fareRestrictionField;
	}
	
	public void setFareRestrictionField(
			List<ResultBookingDetailsSolutionPricingFareRestrictionData> fareRestrictionField) {
		this.fareRestrictionField = fareRestrictionField;
	}
	
	public List<ResultBookingDetailsSolutionPricingNoteData> getNoteField() {
		return noteField;
	}
	
	public void setNoteField(
			List<ResultBookingDetailsSolutionPricingNoteData> noteField) {
		this.noteField = noteField;
	}
	
	public int getPaxCountField() {
		return paxCountField;
	}
	
	public void setPaxCountField(int paxCountField) {
		this.paxCountField = paxCountField;
	}
	
	public List<ResultBookingDetailsSolutionPricingPenaltyPriceData> getPenaltyPriceField() {
		return penaltyPriceField;
	}
	
	public void setPenaltyPriceField(
			List<ResultBookingDetailsSolutionPricingPenaltyPriceData> penaltyPriceField) {
		this.penaltyPriceField = penaltyPriceField;
	}
	
	public ResultBookingDetailsSolutionPricingPreviousCocFareTotalData getPreviousCocFareTotalField() {
		return previousCocFareTotalField;
	}
	
	public void setPreviousCocFareTotalField(
			ResultBookingDetailsSolutionPricingPreviousCocFareTotalData previousCocFareTotalField) {
		this.previousCocFareTotalField = previousCocFareTotalField;
	}
	
	public List<ResultBookingDetailsSolutionPricingRefundPriceData> getRefundPriceField() {
		return refundPriceField;
	}
	
	public void setRefundPriceField(
			List<ResultBookingDetailsSolutionPricingRefundPriceData> refundPriceField) {
		this.refundPriceField = refundPriceField;
	}
	
	public List<ResultBookingDetailsSolutionPricingReissueInfoData> getReissueInfoField() {
		return reissueInfoField;
	}
	
	public void setReissueInfoField(
			List<ResultBookingDetailsSolutionPricingReissueInfoData> reissueInfoField) {
		this.reissueInfoField = reissueInfoField;
	}
	
	public ResultBookingDetailsSolutionPricingReissueTaxTotalDifferenceData getReissueTaxTotalDifferenceField() {
		return reissueTaxTotalDifferenceField;
	}
	
	public void setReissueTaxTotalDifferenceField(
			ResultBookingDetailsSolutionPricingReissueTaxTotalDifferenceData reissueTaxTotalDifferenceField) {
		this.reissueTaxTotalDifferenceField = reissueTaxTotalDifferenceField;
	}
	
	public ResultBookingDetailsSolutionPricingSaleFareTotalData getSaleFareTotalField() {
		return saleFareTotalField;
	}
	
	public void setSaleFareTotalField(
			ResultBookingDetailsSolutionPricingSaleFareTotalData saleFareTotalField) {
		this.saleFareTotalField = saleFareTotalField;
	}
	
	public ResultBookingDetailsSolutionPricingSalePriceData getSalePriceField() {
		return salePriceField;
	}
	
	public void setSalePriceField(
			ResultBookingDetailsSolutionPricingSalePriceData salePriceField) {
		this.salePriceField = salePriceField;
	}
	
	public ResultBookingDetailsSolutionPricingSaleTaxTotalData getSaleTaxTotalField() {
		return saleTaxTotalField;
	}
	
	public void setSaleTaxTotalField(
			ResultBookingDetailsSolutionPricingSaleTaxTotalData saleTaxTotalField) {
		this.saleTaxTotalField = saleTaxTotalField;
	}
	
	public List<ResultBookingDetailsSolutionPricingTaxData> getTaxField() {
		return taxField;
	}
	
	public void setTaxField(
			List<ResultBookingDetailsSolutionPricingTaxData> taxField) {
		this.taxField = taxField;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((addCollectPriceField == null) ? 0 : addCollectPriceField
						.hashCode());
		result = prime
				* result
				+ ((bookingInfoField == null) ? 0 : bookingInfoField.hashCode());
		result = prime
				* result
				+ ((cocFareDifferenceField == null) ? 0
						: cocFareDifferenceField.hashCode());
		result = prime
				* result
				+ ((cocFareTotalField == null) ? 0 : cocFareTotalField
						.hashCode());
		result = prime * result
				+ ((extField == null) ? 0 : extField.hashCode());
		result = prime
				* result
				+ ((fareCalculationField == null) ? 0 : fareCalculationField
						.hashCode());
		result = prime * result
				+ ((fareField == null) ? 0 : fareField.hashCode());
		result = prime
				* result
				+ ((fareRestrictionField == null) ? 0 : fareRestrictionField
						.hashCode());
		result = prime * result
				+ ((noteField == null) ? 0 : noteField.hashCode());
		result = prime * result + paxCountField;
		result = prime
				* result
				+ ((penaltyPriceField == null) ? 0 : penaltyPriceField
						.hashCode());
		result = prime
				* result
				+ ((previousCocFareTotalField == null) ? 0
						: previousCocFareTotalField.hashCode());
		result = prime
				* result
				+ ((refundPriceField == null) ? 0 : refundPriceField.hashCode());
		result = prime
				* result
				+ ((reissueInfoField == null) ? 0 : reissueInfoField.hashCode());
		result = prime
				* result
				+ ((reissueTaxTotalDifferenceField == null) ? 0
						: reissueTaxTotalDifferenceField.hashCode());
		result = prime
				* result
				+ ((saleFareTotalField == null) ? 0 : saleFareTotalField
						.hashCode());
		result = prime * result
				+ ((salePriceField == null) ? 0 : salePriceField.hashCode());
		result = prime
				* result
				+ ((saleTaxTotalField == null) ? 0 : saleTaxTotalField
						.hashCode());
		result = prime * result
				+ ((taxField == null) ? 0 : taxField.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingDetailsSolutionPricingData other = (ResultBookingDetailsSolutionPricingData) obj;
		if (addCollectPriceField == null) {
			if (other.addCollectPriceField != null)
				return false;
		} else if (!addCollectPriceField.equals(other.addCollectPriceField))
			return false;
		if (bookingInfoField == null) {
			if (other.bookingInfoField != null)
				return false;
		} else if (!bookingInfoField.equals(other.bookingInfoField))
			return false;
		if (cocFareDifferenceField == null) {
			if (other.cocFareDifferenceField != null)
				return false;
		} else if (!cocFareDifferenceField.equals(other.cocFareDifferenceField))
			return false;
		if (cocFareTotalField == null) {
			if (other.cocFareTotalField != null)
				return false;
		} else if (!cocFareTotalField.equals(other.cocFareTotalField))
			return false;
		if (extField == null) {
			if (other.extField != null)
				return false;
		} else if (!extField.equals(other.extField))
			return false;
		if (fareCalculationField == null) {
			if (other.fareCalculationField != null)
				return false;
		} else if (!fareCalculationField.equals(other.fareCalculationField))
			return false;
		if (fareField == null) {
			if (other.fareField != null)
				return false;
		} else if (!fareField.equals(other.fareField))
			return false;
		if (fareRestrictionField == null) {
			if (other.fareRestrictionField != null)
				return false;
		} else if (!fareRestrictionField.equals(other.fareRestrictionField))
			return false;
		if (noteField == null) {
			if (other.noteField != null)
				return false;
		} else if (!noteField.equals(other.noteField))
			return false;
		if (paxCountField != other.paxCountField)
			return false;
		if (penaltyPriceField == null) {
			if (other.penaltyPriceField != null)
				return false;
		} else if (!penaltyPriceField.equals(other.penaltyPriceField))
			return false;
		if (previousCocFareTotalField == null) {
			if (other.previousCocFareTotalField != null)
				return false;
		} else if (!previousCocFareTotalField
				.equals(other.previousCocFareTotalField))
			return false;
		if (refundPriceField == null) {
			if (other.refundPriceField != null)
				return false;
		} else if (!refundPriceField.equals(other.refundPriceField))
			return false;
		if (reissueInfoField == null) {
			if (other.reissueInfoField != null)
				return false;
		} else if (!reissueInfoField.equals(other.reissueInfoField))
			return false;
		if (reissueTaxTotalDifferenceField == null) {
			if (other.reissueTaxTotalDifferenceField != null)
				return false;
		} else if (!reissueTaxTotalDifferenceField
				.equals(other.reissueTaxTotalDifferenceField))
			return false;
		if (saleFareTotalField == null) {
			if (other.saleFareTotalField != null)
				return false;
		} else if (!saleFareTotalField.equals(other.saleFareTotalField))
			return false;
		if (salePriceField == null) {
			if (other.salePriceField != null)
				return false;
		} else if (!salePriceField.equals(other.salePriceField))
			return false;
		if (saleTaxTotalField == null) {
			if (other.saleTaxTotalField != null)
				return false;
		} else if (!saleTaxTotalField.equals(other.saleTaxTotalField))
			return false;
		if (taxField == null) {
			if (other.taxField != null)
				return false;
		} else if (!taxField.equals(other.taxField))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingDetailsSolutionPricingData [addCollectPriceField="
				+ addCollectPriceField + ", bookingInfoField="
				+ bookingInfoField + ", cocFareDifferenceField="
				+ cocFareDifferenceField + ", cocFareTotalField="
				+ cocFareTotalField + ", extField=" + extField
				+ ", fareCalculationField=" + fareCalculationField
				+ ", fareField=" + fareField + ", fareRestrictionField="
				+ fareRestrictionField + ", noteField=" + noteField
				+ ", paxCountField=" + paxCountField + ", penaltyPriceField="
				+ penaltyPriceField + ", previousCocFareTotalField="
				+ previousCocFareTotalField + ", refundPriceField="
				+ refundPriceField + ", reissueInfoField=" + reissueInfoField
				+ ", reissueTaxTotalDifferenceField="
				+ reissueTaxTotalDifferenceField + ", saleFareTotalField="
				+ saleFareTotalField + ", salePriceField=" + salePriceField
				+ ", saleTaxTotalField=" + saleTaxTotalField + ", taxField="
				+ taxField + "]";
	}
	
}
