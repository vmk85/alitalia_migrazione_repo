package com.alitalia.aem.common.messages.checkinrest;

import java.util.List;

import com.alitalia.aem.common.data.checkinrest.model.cheangeseats.response.ChangeSeatsResp;
import com.alitalia.aem.common.data.checkinrest.model.cheangeseats.response.EsitoCambioPosto;
import com.alitalia.aem.common.messages.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckinChangeSeatsResponse extends BaseResponse {
	
	@SerializedName("esitoCambioPosto")
	@Expose
	private List<EsitoCambioPosto> esitoCambioPosto;
	@SerializedName("seatMAp")
	@Expose
	private ChangeSeatsResp _changeseatsResp;
	@SerializedName("conversationID")
	@Expose
	private String conversationID;
	
	private String error;
	
	public ChangeSeatsResp get_changeseatsResp() {
		return _changeseatsResp;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public void set_changeseatsResp(ChangeSeatsResp _changeseatsResp) {
		this._changeseatsResp = _changeseatsResp;
	}

	public CheckinChangeSeatsResponse(String tid, String sid) {
		super(tid, sid);
	}
	
	public CheckinChangeSeatsResponse() {
		// TODO Auto-generated constructor stub
	}

	public List<EsitoCambioPosto> getEsitoCambioPosto() {
		return esitoCambioPosto;
	}

	public void setEsitoCambioPosto(List<EsitoCambioPosto> esitoCambioPosto) {
		this.esitoCambioPosto = esitoCambioPosto;
	}

	public String getConversationID() {
		return conversationID;
	}

	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}

}
