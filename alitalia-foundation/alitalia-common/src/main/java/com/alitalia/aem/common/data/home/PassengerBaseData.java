package com.alitalia.aem.common.data.home;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.alitalia.aem.common.data.PassengerBase;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;

public class PassengerBaseData implements PassengerBase {

    private BigDecimal awardPrice;
    private BigDecimal ccFee;
    private BigDecimal couponPrice;
    private BigDecimal extraCharge;
    private BigDecimal fee;
    private BigDecimal grossFare;
    private PassengerBaseInfoData info;
    private String lastName;
    private String name;
    private BigDecimal netFare;
    private List<TicketInfoData> tickets;
    private PassengerTypeEnum type;
	
    public PassengerBaseData() {
		super();
	}

    public PassengerBaseData(PassengerBaseData clone) {
		super();
		this.awardPrice = clone.getAwardPrice();
		this.ccFee = clone.getCcFee();
		this.couponPrice = clone.getCouponPrice();
		this.extraCharge = clone.getExtraCharge();
		this.fee = clone.getFee();
		this.grossFare = clone.getGrossFare();
		this.lastName = clone.getLastName();
		this.name = clone.getName();
		this.netFare = clone.getNetFare();
		this.type = clone.getType();

		PassengerBaseInfoData cloneBaseInfo = clone.getInfo();
		if (cloneBaseInfo != null)
			this.info = new PassengerBaseInfoData(cloneBaseInfo);
		else
			this.info = cloneBaseInfo;

		ArrayList<TicketInfoData> tickets = null;
		List<TicketInfoData> cloneTickets = clone.getTickets();
		if (cloneTickets != null) {
			tickets = new ArrayList<TicketInfoData>();
			for (TicketInfoData cloneTicketInfoData : cloneTickets) {
				TicketInfoData ticketInfoData = new TicketInfoData(cloneTicketInfoData);
				tickets.add(ticketInfoData);
			}
		}
		this.tickets = tickets;
    }

	public BigDecimal getAwardPrice() {
		return awardPrice;
	}

	public void setAwardPrice(BigDecimal awardPrice) {
		this.awardPrice = awardPrice;
	}

	public BigDecimal getCouponPrice() {
		return couponPrice;
	}
	
    public void setCouponPrice(BigDecimal couponPrice) {
		this.couponPrice = couponPrice;
	}
	
    public BigDecimal getExtraCharge() {
		return extraCharge;
	}
	
    public void setExtraCharge(BigDecimal extraCharge) {
		this.extraCharge = extraCharge;
	}
	
    public BigDecimal getFee() {
		return fee;
	}
	
    public void setFee(BigDecimal fee) {
		this.fee = fee;
	}
	
    public BigDecimal getGrossFare() {
		return grossFare;
	}
	
    public void setGrossFare(BigDecimal grossFare) {
		this.grossFare = grossFare;
	}
	
    public PassengerBaseInfoData getInfo() {
		return info;
	}
	
    public void setInfo(PassengerBaseInfoData info) {
		this.info = info;
	}
	
    public String getLastName() {
		return lastName;
	}
	
    public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
    public String getName() {
		return name;
	}
	
    public void setName(String name) {
		this.name = name;
	}
	
    public BigDecimal getNetFare() {
		return netFare;
	}
	
    public void setNetFare(BigDecimal netFare) {
		this.netFare = netFare;
	}
	
    public List<TicketInfoData> getTickets() {
		return tickets;
	}
	
    public void setTickets(List<TicketInfoData> tickets) {
		this.tickets = tickets;
	}

    public void addTicket(TicketInfoData ticket) {
    	if (this.tickets == null)
    		this.tickets = new ArrayList<TicketInfoData>();

    	this.tickets.add(ticket);
    }
    
    public PassengerTypeEnum getType() {
		return type;
	}
	
    public void setType(PassengerTypeEnum type) {
		this.type = type;
	}

	public BigDecimal getCcFee() {
		return ccFee;
	}

	public void setCcFee(BigDecimal ccFee) {
		this.ccFee = ccFee;
	}

	@Override
	public String toString() {
		return "PassengerBaseData [awardPrice=" + awardPrice + ", ccFee="
				+ ccFee + ", couponPrice=" + couponPrice + ", extraCharge="
				+ extraCharge + ", fee=" + fee + ", grossFare=" + grossFare
				+ ", info=" + info + ", lastName=" + lastName + ", name="
				+ name + ", netFare=" + netFare + ", tickets=" + tickets
				+ ", type=" + type + "]";
	}
}