package com.alitalia.aem.common.messages.checkinrest;


import com.alitalia.aem.common.messages.BaseRequest;

import java.util.List;

public class CheckinClearAncillariesCartRequest extends BaseRequest
{

    private List<String> listAncillaryId = null;
    private String pnr;
    private String language;
    private String market;
    private String conversationID;

    public CheckinClearAncillariesCartRequest(String tid, String sid) {
        super(tid, sid);
    }

    public List<String> getListAncillaryId() {
        return listAncillaryId;
    }

    public void setListAncillaryId(List<String> clearAncillariesCart) {
        this.listAncillaryId = clearAncillariesCart;
    }

    public String getPnr() {
        return pnr;
    }


    public void setPnr(String pnr) {
        this.pnr = pnr;
    }


    public String getLanguage() {
        return language;
    }


    public void setLanguage(String language) {
        this.language = language;
    }


    public String getMarket() {
        return market;
    }


    public void setMarket(String market) {
        this.market = market;
    }


    public String getConversationID() {
        return conversationID;
    }


    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }


}
