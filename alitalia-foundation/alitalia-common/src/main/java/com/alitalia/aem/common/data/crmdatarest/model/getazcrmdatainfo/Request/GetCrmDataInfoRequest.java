package com.alitalia.aem.common.data.crmdatarest.model.getazcrmdatainfo.Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCrmDataInfoRequest {

    @SerializedName("idMyAlitalia")
    @Expose
    private String idMyAlitalia;

    @SerializedName("canale")
    @Expose
    private String canale;

    @SerializedName("language")
    @Expose
    private String language;

    @SerializedName("market")
    @Expose
    private String market;

    @SerializedName("conversationID")
    @Expose
    private String conversationID;

    public String getIdMyAlitalia() {
        return idMyAlitalia;
    }

    public void setIdMyAlitalia(String idMyAlitalia) {
        this.idMyAlitalia = idMyAlitalia;
    }

    public String getCanale() {
        return canale;
    }

    public void setCanale(String canale) {
        this.canale = canale;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

    @Override
    public String toString() {
        return "GetCrmDataInfoRequest{" +
                "idMyAlitalia='" + idMyAlitalia + '\'' +
                ", canale='" + canale + '\'' +
                ", language='" + language + '\'' +
                ", market='" + market + '\'' +
                ", conversationID='" + conversationID + '\'' +
                '}';
    }
}
