package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class SetSocialAccountRequest extends BaseRequest {
	
	private String gigyaId;
	private String gigyaSignature;
	private String userCode;
	private String pin;
	
	public SetSocialAccountRequest(String tid, String sid) {
		super(tid, sid);
	}
	
	public String getGigyaId() {
		return gigyaId;
	}
	
	public void setGigyaId(String gigyaId) {
		this.gigyaId = gigyaId;
	}
	
	public String getGigyaSignature() {
		return gigyaSignature;
	}

	public void setGigyaSignature(String gigyaSignature) {
		this.gigyaSignature = gigyaSignature;
	}

	public String getUserCode() {
		return userCode;
	}
	
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	
	public String getPin() {
		return pin;
	}
	
	public void setPin(String pin) {
		this.pin = pin;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((gigyaId == null) ? 0 : gigyaId.hashCode());
		result = prime * result + ((gigyaSignature == null) ? 0 : gigyaSignature.hashCode());
		result = prime * result + ((pin == null) ? 0 : pin.hashCode());
		result = prime * result + ((userCode == null) ? 0 : userCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SetSocialAccountRequest other = (SetSocialAccountRequest) obj;
		if (gigyaId == null) {
			if (other.gigyaId != null)
				return false;
		} else if (!gigyaId.equals(other.gigyaId))
			return false;
		if (gigyaSignature == null) {
			if (other.gigyaSignature != null)
				return false;
		} else if (!gigyaSignature.equals(other.gigyaSignature))
			return false;
		if (pin == null) {
			if (other.pin != null)
				return false;
		} else if (!pin.equals(other.pin))
			return false;
		if (userCode == null) {
			if (other.userCode != null)
				return false;
		} else if (!userCode.equals(other.userCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SetSocialAccountRequest [gigyaId=" + gigyaId + ", gigyaSignature=" + gigyaSignature + ", userCode="
				+ userCode + ", pin=" + pin + "]";
	}
	
}
