package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.BaseRequest;

public class SubscribeSARequest extends BaseRequest {

	private MMCustomerProfileData customerProfile;
	
	private String bodyWelcomeMail;

	private String objectMail;

	public String getBodyWelcomeMail() {
		return bodyWelcomeMail;
	}

	public void setBodyWelcomeMail(String bodyWelcomeMail) {
		this.bodyWelcomeMail = bodyWelcomeMail;
	}

	public MMCustomerProfileData getCustomerProfile() {
		return customerProfile;
	}

	public void setCustomerProfile(MMCustomerProfileData customerProfile) {
		this.customerProfile = customerProfile;
	}


	private String username;
	private String password;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((customerProfile == null) ? 0 : customerProfile.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubscribeSARequest other = (SubscribeSARequest) obj;
		if (customerProfile == null) {
			if (other.customerProfile != null)
				return false;
		} else if (!customerProfile.equals(other.customerProfile))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SubscribeRequest [customerProfile=" + customerProfile + "]";
	}

	public String getObjectMail() {
		return objectMail;
	}

	public void setObjectMail(String objectMail) {
		this.objectMail = objectMail;
	}


	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

}