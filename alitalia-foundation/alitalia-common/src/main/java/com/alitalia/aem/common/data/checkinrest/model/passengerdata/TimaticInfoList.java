
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TimaticInfoList {

    @SerializedName("timaticStatus")
    @Expose
    private String timaticStatus;
    @SerializedName("infant")
    @Expose
    private Boolean infant;
    @SerializedName("infantSpecified")
    @Expose
    private Boolean infantSpecified;
    @SerializedName("timaticCountryInfo")
    @Expose
    private List<TimaticCountryInfo> timaticCountryInfo = null;

    public String getTimaticStatus() {
        return timaticStatus;
    }

    public void setTimaticStatus(String timaticStatus) {
        this.timaticStatus = timaticStatus;
    }

    public Boolean getInfant() {
        return infant;
    }

    public void setInfant(Boolean infant) {
        this.infant = infant;
    }

    public Boolean getInfantSpecified() {
        return infantSpecified;
    }

    public void setInfantSpecified(Boolean infantSpecified) {
        this.infantSpecified = infantSpecified;
    }

    public List<TimaticCountryInfo> getTimaticCountryInfo() {
        return timaticCountryInfo;
    }

    public void setTimaticCountryInfo(List<TimaticCountryInfo> timaticCountryInfo) {
        this.timaticCountryInfo = timaticCountryInfo;
    }

}
