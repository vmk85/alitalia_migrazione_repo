package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.mmb.MmbPassengerData;
import com.alitalia.aem.common.messages.MmbBaseRequest;

public class MmbModifyPnrApisInfoRequest extends MmbBaseRequest {

	private MmbPassengerData passenger;
	private String pnr;

	public MmbPassengerData getPassenger() {
		return passenger;
	}

	public void setPassenger(MmbPassengerData passenger) {
		this.passenger = passenger;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((passenger == null) ? 0 : passenger.hashCode());
		result = prime * result + ((pnr == null) ? 0 : pnr.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbModifyPnrApisInfoRequest other = (MmbModifyPnrApisInfoRequest) obj;
		if (passenger == null) {
			if (other.passenger != null)
				return false;
		} else if (!passenger.equals(other.passenger))
			return false;
		if (pnr == null) {
			if (other.pnr != null)
				return false;
		} else if (!pnr.equals(other.pnr))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ModifyMmbPnrApisInfoRequest [passenger=" + passenger + ", pnr="
				+ pnr + ", getBaseInfo()=" + getBaseInfo() + ", getClient()="
				+ getClient() + "]";
	}

}
