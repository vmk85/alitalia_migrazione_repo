package com.alitalia.aem.common.data.home;

public class TicketInfoData {

	private String segmentId;
	private String ticketNumber;

	public TicketInfoData() {
		super();
	}

	public TicketInfoData(TicketInfoData clone) {
		this.segmentId = clone.getSegmentId();
		this.ticketNumber = clone.getTicketNumber();
	}

	public String getSegmentId() {
		return segmentId;
	}
	
	public void setSegmentId(String segmentId) {
		this.segmentId = segmentId;
	}
	
	public String getTicketNumber() {
		return ticketNumber;
	}
	
	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	@Override
	public String toString() {
		return "TicketInfoData [segmentId=" + segmentId + ", ticketNumber="
				+ ticketNumber + "]";
	}
}
