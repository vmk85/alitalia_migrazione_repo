package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.PaymentProcessInfoData;
import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.common.data.home.TaxData;
import com.alitalia.aem.common.messages.BaseRequest;

public class RetrieveTicketRequest extends BaseRequest {
	private PaymentProcessInfoData process;
	private RoutesData routes;
	private List<TaxData> taxs;
	private Boolean invoiceRequired;
	
	public PaymentProcessInfoData getProcess() {
		return process;
	}
	
	public void setProcess(PaymentProcessInfoData process) {
		this.process = process;
	}
	
	public RoutesData getRoutes() {
		return routes;
	}
	
	public void setRoutes(RoutesData routes) {
		this.routes = routes;
	}
	
	public List<TaxData> getTaxs() {
		return taxs;
	}
	
	public void setTaxs(List<TaxData> taxs) {
		this.taxs = taxs;
	}
	
	public Boolean isInvoiceRequired() {
		return invoiceRequired;
	}

	public void setInvoiceRequired(Boolean invoiceRequired) {
		this.invoiceRequired = invoiceRequired;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((process == null) ? 0 : process.hashCode());
		result = prime * result + ((routes == null) ? 0 : routes.hashCode());
		result = prime * result + ((taxs == null) ? 0 : taxs.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveTicketRequest other = (RetrieveTicketRequest) obj;
		if (process == null) {
			if (other.process != null)
				return false;
		} else if (!process.equals(other.process))
			return false;
		if (routes == null) {
			if (other.routes != null)
				return false;
		} else if (!routes.equals(other.routes))
			return false;
		if (taxs == null) {
			if (other.taxs != null)
				return false;
		} else if (!taxs.equals(other.taxs))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveTicketRequest [process=" + process + ", routes="
				+ routes + ", taxs=" + taxs + ", invoiceRequired="
				+ invoiceRequired + ", getTid()=" + getTid() + ", getSid()="
				+ getSid() + "]";
	}
	
}
