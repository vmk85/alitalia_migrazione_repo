package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.initpayment.response.InitPaymentResponse;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinInitPaymentResponse extends BaseResponse{
	
	public InitPaymentResponse initPaymentResponse;

	public InitPaymentResponse getInitPaymentResponse() {
		return initPaymentResponse;
	}

	public void setInitPaymentResponse(InitPaymentResponse initPaymentResponse) {
		this.initPaymentResponse = initPaymentResponse;
	}

	public CheckinInitPaymentResponse(){}
	
	public CheckinInitPaymentResponse(String tid, String sid) {
		super(tid, sid);
	}

}
