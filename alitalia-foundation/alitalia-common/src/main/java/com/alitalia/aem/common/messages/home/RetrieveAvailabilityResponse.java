package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.Availability;
import com.alitalia.aem.common.messages.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RetrieveAvailabilityResponse extends BaseResponse {

	@SerializedName("availability")
	@Expose
	private Availability availability;



	@SerializedName("availabilityRTN")
	@Expose
	private Availability availability_ret;

	@SerializedName("conversationID")
	@Expose
	private String conversationID;

	public RetrieveAvailabilityResponse() {}

	public RetrieveAvailabilityResponse(String tid, String sid) {
		super(tid, sid);	
	}


	public Availability getAvailability_ret() {
		return availability_ret;
	}

	public void setAvailability_ret(Availability availability_ret) {
		this.availability_ret = availability_ret;
	}
	public Availability getAvailability() {
		return availability;
	}

	public void setAvailability(Availability availability) {
		this.availability = availability;
	}

	public String getConversationID() {
		return conversationID;
	}

	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}

	
}
