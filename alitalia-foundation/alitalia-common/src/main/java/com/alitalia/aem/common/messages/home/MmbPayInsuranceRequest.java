package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.InsurancePolicyData;
import com.alitalia.aem.common.data.home.mmb.MmbPaymentData;
import com.alitalia.aem.common.data.home.mmb.MmbRouteData;
import com.alitalia.aem.common.messages.BaseRequest;

public class MmbPayInsuranceRequest extends BaseRequest {

	private InsurancePolicyData insurancePolicy;
	private MmbPaymentData paymentData;
	private MmbRouteData route;

	public InsurancePolicyData getInsurancePolicy() {
		return insurancePolicy;
	}

	public void setInsurancePolicy(InsurancePolicyData insurancePolicy) {
		this.insurancePolicy = insurancePolicy;
	}

	public MmbPaymentData getPaymentData() {
		return paymentData;
	}

	public void setPaymentData(MmbPaymentData paymentData) {
		this.paymentData = paymentData;
	}

	public MmbRouteData getRoute() {
		return route;
	}

	public void setRoute(MmbRouteData route) {
		this.route = route;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((insurancePolicy == null) ? 0 : insurancePolicy.hashCode());
		result = prime * result
				+ ((paymentData == null) ? 0 : paymentData.hashCode());
		result = prime * result + ((route == null) ? 0 : route.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbPayInsuranceRequest other = (MmbPayInsuranceRequest) obj;
		if (insurancePolicy == null) {
			if (other.insurancePolicy != null)
				return false;
		} else if (!insurancePolicy.equals(other.insurancePolicy))
			return false;
		if (paymentData == null) {
			if (other.paymentData != null)
				return false;
		} else if (!paymentData.equals(other.paymentData))
			return false;
		if (route == null) {
			if (other.route != null)
				return false;
		} else if (!route.equals(other.route))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbPayInsuranceRequest [insurancePolicy=" + insurancePolicy
				+ ", route=" + route + ", getTid()=" + getTid() + ", getSid()="
				+ getSid() + "]";
	}

}