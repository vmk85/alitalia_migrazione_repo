package com.alitalia.aem.common.errorhandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUse;

public class ErrorHandlerResponse extends WCMUse {
    
	private boolean trade;
	private boolean consumer;
    
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
    public void activate() throws Exception {
    	
        try {
			getResponse().setStatus(404);
			trade = false;
	    	consumer = false;
	    	
	        String contextPath = getRequest().getRequestPathInfo().getResourcePath();
	        if (contextPath.contains("alitaliatrade")) {
	        	trade = true;
	        } else if (contextPath.contains("alitalia")){
	        	consumer = true;
	        }
        }catch(Exception e){
        	logger.error("Unexpected error: ", e);
        	throw e;
        }
    }
	
	public boolean isConsumer(){
		return consumer;
	}
	
	public boolean isTrade(){
		return trade;
	}
}
