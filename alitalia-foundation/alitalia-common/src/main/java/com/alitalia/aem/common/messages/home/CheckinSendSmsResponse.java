package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

import java.util.Objects;

public class CheckinSendSmsResponse extends BaseResponse {

    private String msgId;
    private String description;
    private String responseCode;

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CheckinSendSmsResponse)) return false;
        if (!super.equals(o)) return false;
        CheckinSendSmsResponse that = (CheckinSendSmsResponse) o;
        return Objects.equals(getMsgId(), that.getMsgId()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getResponseCode(), that.getResponseCode());
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), getMsgId(), getDescription(), getResponseCode());
    }

    @Override
    public String toString() {
        return "CheckinSendSmsResponse{" +
                "msgId='" + msgId + '\'' +
                ", description='" + description + '\'' +
                ", responseCode='" + responseCode + '\'' +
                '}';
    }
}
