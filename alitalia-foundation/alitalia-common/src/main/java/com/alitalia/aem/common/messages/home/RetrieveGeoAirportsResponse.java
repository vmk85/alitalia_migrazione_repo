package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.CodeDescriptionData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetrieveGeoAirportsResponse extends BaseResponse {

	private List<CodeDescriptionData> codesDescriptions;

	public List<CodeDescriptionData> getCodesDescriptions() {
		return codesDescriptions;
	}

	public void setCodesDescriptions(List<CodeDescriptionData> codesDescriptions) {
		this.codesDescriptions = codesDescriptions;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((codesDescriptions == null) ? 0 : codesDescriptions
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveGeoAirportsResponse other = (RetrieveGeoAirportsResponse) obj;
		if (codesDescriptions == null) {
			if (other.codesDescriptions != null)
				return false;
		} else if (!codesDescriptions.equals(other.codesDescriptions))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveGeoAirportsResponse [codesDescriptions="
				+ codesDescriptions + "]";
	}
}