package com.alitalia.aem.common.data.checkinrest.model.passenger;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RowDetailList {
	
	@SerializedName("rowCharacteristic")
	@Expose
	private List<String> rowCharacteristic = null;
	
	@SerializedName("column")
	@Expose
	private List<Column> column = null;
	
	@SerializedName("number")
	@Expose
	private int number;

	public List<String> getRowCharacteristic() {
		return rowCharacteristic;
	}

	public void setRowCharacteristic(List<String> rowCharacteristic) {
		this.rowCharacteristic = rowCharacteristic;
	}

	public List<Column> getColumn() {
		return column;
	}

	public void setColumn(List<Column> column) {
		this.column = column;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

}
