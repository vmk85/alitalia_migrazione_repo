
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomePrintedBagTag {

    @SerializedName("reasonList")
    @Expose
    private List<ReasonList> reasonList = null;
    @SerializedName("restriction")
    @Expose
    private String restriction;

    public List<ReasonList> getReasonList() {
        return reasonList;
    }

    public void setReasonList(List<ReasonList> reasonList) {
        this.reasonList = reasonList;
    }

    public String getRestriction() {
        return restriction;
    }

    public void setRestriction(String restriction) {
        this.restriction = restriction;
    }

}
