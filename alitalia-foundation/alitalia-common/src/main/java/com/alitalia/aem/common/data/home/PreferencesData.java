package com.alitalia.aem.common.data.home;

import java.util.ArrayList;
import java.util.List;

public class PreferencesData {

	private MealData mealType;
	private List<SeatPreferencesData> seatPreferences;
	private SeatTypeData seatType;

	public PreferencesData() {
		super();
	}

	public PreferencesData(PreferencesData clone) {
		super();
		this.mealType = clone.getMealType();
		this.seatType = clone.getSeatType();
		
		ArrayList<SeatPreferencesData> seatPrefs = new ArrayList<SeatPreferencesData>();
		for (SeatPreferencesData cloneSeatPreferencesData : clone.getSeatPreferences()) {
			SeatPreferencesData seatPreferencesData = new SeatPreferencesData(cloneSeatPreferencesData);
			seatPrefs.add(seatPreferencesData);
		}
		this.seatPreferences = seatPrefs;
	}

	public MealData getMealType() {
		return mealType;
	}
	
	public void setMealType(MealData mealType) {
		this.mealType = mealType;
	}
	
	public List<SeatPreferencesData> getSeatPreferences() {
		return seatPreferences;
	}
	
	public void setSeatPreferences(List<SeatPreferencesData> seatPreferences) {
		this.seatPreferences = seatPreferences;
	}
	
	public SeatTypeData getSeatType() {
		return seatType;
	}
	
	public void setSeatType(SeatTypeData seatType) {
		this.seatType = seatType;
	}

	@Override
	public String toString() {
		return "PreferencesData [mealType=" + mealType + ", seatPreferences="
				+ seatPreferences + ", seatType=" + seatType + "]";
	}
}
