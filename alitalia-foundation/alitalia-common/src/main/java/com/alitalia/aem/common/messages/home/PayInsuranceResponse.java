package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class PayInsuranceResponse extends BaseResponse {

	private boolean success;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (success ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PayInsuranceResponse other = (PayInsuranceResponse) obj;
		if (success != other.success)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PayInsuranceResponse [success=" + success + "]";
	}
}