package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetrieveCreditCardCountriesResponse extends BaseResponse{

	private List<CountryData> countries;
	
	public RetrieveCreditCardCountriesResponse(){}
	
	public RetrieveCreditCardCountriesResponse(String tid, String sid) {
		super(tid, sid);
	}

	public List<CountryData> getCountries() {
		return countries;
	}

	public void setCountries(List<CountryData> countries) {
		this.countries = countries;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((countries == null) ? 0 : countries.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveCreditCardCountriesResponse other = (RetrieveCreditCardCountriesResponse) obj;
		if (countries == null) {
			if (other.countries != null)
				return false;
		} else if (!countries.equals(other.countries))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveCreditCardCountriesResponse [countries=" + countries + "]";
	}

	

}