package com.alitalia.aem.common.converter;

public interface Converter<S, D> {
	D convert(S source);
}