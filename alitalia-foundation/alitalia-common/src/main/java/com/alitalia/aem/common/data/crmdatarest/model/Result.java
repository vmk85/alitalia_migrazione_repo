package com.alitalia.aem.common.data.crmdatarest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Result {

    @Override
    public String toString() {
        return "Result [errorSourceDetails=" + errorSourceDetails + ", " +
                ", code=" + code +
                ", description=" + description +
                ", status=" + status + "]";
    }


    @SerializedName("errorSourceDetails")
    @Expose
    private List<ErrorSourceDetail> errorSourceDetails;

    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("status")
    @Expose
    private String status;

    public List<ErrorSourceDetail> getErrorSourceDetails() {
        return errorSourceDetails;
    }

    public void setErrorSourceDetails(List<ErrorSourceDetail> errorSourceDetails) {
        this.errorSourceDetails = errorSourceDetails;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
