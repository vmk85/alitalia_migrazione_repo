package com.alitalia.aem.common.data.home.countrystates;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CountryStates {
	
	@SerializedName("countryStates")
	@Expose
	private List<CountryState> countryState = null;
	@SerializedName("language")
	@Expose
	private String language;
	@SerializedName("market")
	@Expose
	private String market;
	@SerializedName("conversationID")
	@Expose
	private String conversationID;
	
	public List<CountryState> getCountryState() {
		return countryState;
	}
	
	public void setCountryState(List<CountryState> countryState) {
		this.countryState = countryState;
	}
	
	public String getLanguage() {
		return language;
	}
	
	public void setLanguage(String language) {
		this.language = language;
	}
	
	public String getMarket() {
		return market;
	}
	
	public void setMarket(String market) {
		this.market = market;
	}
	
	public String getConversationID() {
		return conversationID;
	}
	
	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}
	
}
