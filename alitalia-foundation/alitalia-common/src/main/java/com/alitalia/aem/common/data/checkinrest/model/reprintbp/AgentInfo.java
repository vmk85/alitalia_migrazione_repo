
package com.alitalia.aem.common.data.checkinrest.model.reprintbp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AgentInfo {

    @SerializedName("sine")
    @Expose
    private String sine;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("cityName")
    @Expose
    private String cityName;
    @SerializedName("country")
    @Expose
    private String country;

    public String getSine() {
        return sine;
    }

    public void setSine(String sine) {
        this.sine = sine;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

}
