package com.alitalia.aem.common.data.home;

import java.math.BigDecimal;

public class BrandPenaltyData {

	private BigDecimal maxPrice;
	private String maxPriceCurrency;
	private BigDecimal minPrice;
	private String minPriceCurrency;
	private Boolean permitted;
	
	public BrandPenaltyData() {
		super();
	}
	
	public BrandPenaltyData(BrandPenaltyData clone) {
		super();
		this.maxPrice = clone.getMaxPrice() != null ? new BigDecimal(clone.getMaxPrice().doubleValue()) : null;
		this.maxPriceCurrency = clone.maxPriceCurrency;
		this.minPrice = clone.getMinPrice() != null ? new BigDecimal(clone.getMinPrice().doubleValue()) : null;
		this.minPriceCurrency = clone.minPriceCurrency;
		this.permitted = clone.permitted != null ? new Boolean(clone.permitted) : null;
	}

	public BigDecimal getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(BigDecimal maxPrice) {
		this.maxPrice = maxPrice;
	}

	public String getMaxPriceCurrency() {
		return maxPriceCurrency;
	}

	public void setMaxPriceCurrency(String maxPriceCurrency) {
		this.maxPriceCurrency = maxPriceCurrency;
	}

	public BigDecimal getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(BigDecimal minPrice) {
		this.minPrice = minPrice;
	}

	public String getMinPriceCurrency() {
		return minPriceCurrency;
	}

	public void setMinPriceCurrency(String minPriceCurrency) {
		this.minPriceCurrency = minPriceCurrency;
	}

	public Boolean isPermitted() {
		return permitted;
	}

	public void setPermitted(Boolean permitted) {
		this.permitted = permitted;
	}

	@Override
	public String toString() {
		return "BrandPenaltyData [maxPrice=" + maxPrice + ", maxPriceCurrency="
				+ maxPriceCurrency + ", minPrice=" + minPrice
				+ ", minPriceCurrency=" + minPriceCurrency + ", permitted="
				+ permitted + "]";
	}
}
