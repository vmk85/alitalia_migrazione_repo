package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.offload.response.FlightPassR;
import com.alitalia.aem.common.data.checkinrest.model.offload.response.PnrFlightInfoR;
import com.alitalia.aem.common.data.checkinrest.model.offload.response.SeatListR;
import com.alitalia.aem.common.messages.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CheckinOffloadFlightsResponse extends BaseResponse {

    @SerializedName("pnrFlightInfoRS")
    @Expose
    private List<PnrFlightInfoR> pnrFlightInfoRS = null;
    @SerializedName("flightPassRS")
    @Expose
    private List<FlightPassR> flightPassRS;
    @SerializedName("seatListRS")
    @Expose
    private List<SeatListR> seatListRS;
    @SerializedName("conversationID")
    @Expose
    private String conversationID;

    private String error;

    public List<PnrFlightInfoR> getPnrFlightInfoRS() {
        return pnrFlightInfoRS;
    }

    public void setPnrFlightInfoRS(List<PnrFlightInfoR> pnrFlightInfoRS) {
        this.pnrFlightInfoRS = pnrFlightInfoRS;
    }

    public List<FlightPassR> getFlightPassRS() {
        return flightPassRS;
    }

    public void setFlightPassRS(List<FlightPassR> flightPassRS) {
        this.flightPassRS = flightPassRS;
    }

    public List<SeatListR> getSeatListRS() {
        return seatListRS;
    }

    public void setSeatListRS(List<SeatListR> seatListRS) {
        this.seatListRS = seatListRS;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
