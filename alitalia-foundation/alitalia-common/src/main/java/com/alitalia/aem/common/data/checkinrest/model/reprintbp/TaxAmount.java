
package com.alitalia.aem.common.data.checkinrest.model.reprintbp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TaxAmount {

    @SerializedName("exempt")
    @Expose
    private Boolean exempt;
    @SerializedName("taxCode")
    @Expose
    private String taxCode;
    @SerializedName("currencyCode")
    @Expose
    private String currencyCode;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("value")
    @Expose
    private Long value;

    public Boolean getExempt() {
        return exempt;
    }

    public void setExempt(Boolean exempt) {
        this.exempt = exempt;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

}
