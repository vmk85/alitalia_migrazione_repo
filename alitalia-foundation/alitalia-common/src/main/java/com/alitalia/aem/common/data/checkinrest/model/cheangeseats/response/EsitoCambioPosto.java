package com.alitalia.aem.common.data.checkinrest.model.cheangeseats.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EsitoCambioPosto {
	
	@SerializedName("esito")
	@Expose
	private boolean esito;
	@SerializedName("newSeat")
	@Expose
	private String newSeat;
	@SerializedName("cognome")
	@Expose
	private String cognome;
	@SerializedName("nome")
	@Expose
	private String nome;
	
	public boolean isEsito() {
		return esito;
	}
	
	public void setEsito(boolean esito) {
		this.esito = esito;
	}
	
	public String getNewSeat() {
		return newSeat;
	}
	
	public void setNewSeat(String newSeat) {
		this.newSeat = newSeat;
	}
	
	public String getCognome() {
		return cognome;
	}
	
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	

}
