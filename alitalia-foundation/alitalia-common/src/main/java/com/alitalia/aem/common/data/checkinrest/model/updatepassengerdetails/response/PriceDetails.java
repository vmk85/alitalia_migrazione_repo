
package com.alitalia.aem.common.data.checkinrest.model.updatepassengerdetails.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PriceDetails {

    @SerializedName("totalPrice")
    @Expose
    private TotalPrice totalPrice;
    @SerializedName("basePrice")
    @Expose
    private BasePrice basePrice;
    @SerializedName("equivalentPrice")
    @Expose
    private EquivalentPrice equivalentPrice;
    @SerializedName("taxDetails")
    @Expose
    private TaxDetails taxDetails;

    public TotalPrice getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(TotalPrice totalPrice) {
        this.totalPrice = totalPrice;
    }

    public BasePrice getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BasePrice basePrice) {
        this.basePrice = basePrice;
    }

    public EquivalentPrice getEquivalentPrice() {
        return equivalentPrice;
    }

    public void setEquivalentPrice(EquivalentPrice equivalentPrice) {
        this.equivalentPrice = equivalentPrice;
    }

    public TaxDetails getTaxDetails() {
        return taxDetails;
    }

    public void setTaxDetails(TaxDetails taxDetails) {
        this.taxDetails = taxDetails;
    }

}
