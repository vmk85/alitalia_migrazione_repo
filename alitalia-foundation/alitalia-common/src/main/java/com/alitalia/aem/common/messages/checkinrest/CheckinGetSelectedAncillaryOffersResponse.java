package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.response.AncillaryOffers;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinGetSelectedAncillaryOffersResponse extends BaseResponse{
	
	private AncillaryOffers ancillaryOffers;
	
	public CheckinGetSelectedAncillaryOffersResponse() {}

	public CheckinGetSelectedAncillaryOffersResponse(String tid, String sid) {
		super(tid, sid);
	}

	public AncillaryOffers getAncillaryOffers() {
		return ancillaryOffers;
	}

	public void setAncillaryOffers(AncillaryOffers ancillaryOffers) {
		this.ancillaryOffers = ancillaryOffers;
	}
}
