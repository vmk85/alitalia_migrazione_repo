package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.reprintbp.FreeTextInfoList;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinReprintBPResponse extends BaseResponse {
	private FreeTextInfoList bpData = null;
	
	public CheckinReprintBPResponse() {}

	public CheckinReprintBPResponse(String tid, String sid) {
		super(tid, sid);
	}

	public FreeTextInfoList getBpData() {
		return bpData;
	}

	public void setBpReprintedData(FreeTextInfoList bpData) {
		this.bpData = bpData;
	}

}
