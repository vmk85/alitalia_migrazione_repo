package com.alitalia.aem.common.data.home;

public class AdyenRecurringData {
	
	/*
		{
			"contract": "ONECLICK"
		}
	 */
	
	private String contract = "ONECLICK";//ONECLICK o RECURRING o ONECLICK, RECURRING

	public String getContract() {
		return contract;
	}

	public void setContract(String contract) {
		this.contract = contract;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result	+ ((contract == null) ? 0 : contract.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdyenRecurringData other = (AdyenRecurringData) obj;
		if (contract == null) {
			if (other.contract != null)
				return false;
		} else if (!contract.equals(other.contract))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "Recurring ["
				+ "contract=" + contract
				+ "]";
	}
}