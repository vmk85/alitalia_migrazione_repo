
package com.alitalia.aem.common.data.checkinrest.model.cheangeseats.request;

public class SeatPassenger {

    private String newSeat;
    private String cognome;
    private String nome;

    public String getNewSeat() {
        return newSeat;
    }

    public void setNewSeat(String newSeat) {
        this.newSeat = newSeat;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
