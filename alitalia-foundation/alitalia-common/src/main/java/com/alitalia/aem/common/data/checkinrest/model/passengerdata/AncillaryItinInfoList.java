
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AncillaryItinInfoList {

    @SerializedName("airline")
    @Expose
    private String airline;
    @SerializedName("owningCarrier")
    @Expose
    private String owningCarrier;
    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("ancillaryInfo")
    @Expose
    private List<AncillaryInfo> ancillaryInfo = null;
    @SerializedName("atpcoPassengerType")
    @Expose
    private String atpcoPassengerType;

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getOwningCarrier() {
        return owningCarrier;
    }

    public void setOwningCarrier(String owningCarrier) {
        this.owningCarrier = owningCarrier;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public List<AncillaryInfo> getAncillaryInfo() {
        return ancillaryInfo;
    }

    public void setAncillaryInfo(List<AncillaryInfo> ancillaryInfo) {
        this.ancillaryInfo = ancillaryInfo;
    }

    public String getAtpcoPassengerType() {
        return atpcoPassengerType;
    }

    public void setAtpcoPassengerType(String atpcoPassengerType) {
        this.atpcoPassengerType = atpcoPassengerType;
    }

}
