package com.alitalia.aem.common.data.home;

public class DestinationData {

	private String codeContext;
	private String locationCode;
	
	public String getCodeContext() {
		return codeContext;
	}
	
	public void setCodeContext(String codeContext) {
		this.codeContext = codeContext;
	}
	
	public String getLocationCode() {
		return locationCode;
	}
	
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((codeContext == null) ? 0 : codeContext.hashCode());
		result = prime * result
				+ ((locationCode == null) ? 0 : locationCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DestinationData other = (DestinationData) obj;
		if (codeContext == null) {
			if (other.codeContext != null)
				return false;
		} else if (!codeContext.equals(other.codeContext))
			return false;
		if (locationCode == null) {
			if (other.locationCode != null)
				return false;
		} else if (!locationCode.equals(other.locationCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DestinationData [codeContext=" + codeContext
				+ ", locationCode=" + locationCode + "]";
	}
}