package com.alitalia.aem.common.data.checkinrest.model.pnrinfo;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PassengerExtra
{

    @SerializedName("index")
    @Expose
    private Integer index;
    @SerializedName("pnr")
    @Expose
    private String pnr;
    @SerializedName("numero")
    @Expose
    private String numero;
    @SerializedName("airline")
    @Expose
    private String airline;
    @SerializedName("origin_code")
    @Expose
    private String origin_code;
    @SerializedName("destination_code")
    @Expose
    private String destination_code;
    @SerializedName("flight_number")
    @Expose
    private String flight_number;
    @SerializedName("departure_date")
    @Expose
    private String departure_date;
    @SerializedName("passenger")
    @Expose
    private Passenger passenger = null;
    @SerializedName("passengerSeats")
    @Expose
    private List<PassengerSeat> passengerSeats = null;

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getPrn() {
        return pnr;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNumero() {
        return numero;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getOriginCode() {
        return origin_code;
    }

    public void setDestinationCode(String destination_code) {
        this.destination_code = destination_code;
    }

    public String getDestinationCode() {
        return destination_code;
    }

    public void setOriginCode(String origin_code) {
        this.origin_code = origin_code;
    }

    public String getFlightNumber() {
        return flight_number;
    }

    public void setFlightNumber(String flight_number) {
        this.flight_number = flight_number;
    }

    public String getDepartureDate() {
        return departure_date;
    }

    public void setDepartureDate(String departure_date) {
        this.departure_date = departure_date;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

    public List<PassengerSeat> getPassengerSeats() {
        return passengerSeats;
    }

    public void setPassengerSeats(List<PassengerSeat> passengerSeats) {
        this.passengerSeats = passengerSeats;
    }

}
