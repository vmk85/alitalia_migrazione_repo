package com.alitalia.aem.common.data.home.checkin;

import java.util.List;

import com.alitalia.aem.common.data.home.enumerations.CheckinPassengerStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.CheckinPassengerTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.FrequentFlyerCategoryEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAQQTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbLinkTypeEnum;
import com.alitalia.aem.common.data.home.mmb.MmbBaggageOrderData;
import com.alitalia.aem.common.data.home.mmb.MmbFrequentFlyerCarrierData;

public class CheckinPassengerData {	

	private Integer id;
	private CheckinApisInfoData apisData;
	private MmbAQQTypeEnum authorityPermission;
	private Integer baggageAllowanceQuantity;
	private String baggageAllowance;
	private String boardingPassRetrieveEmail;
	private String boardingPassRetrieveSms;
	private Boolean boardingPassViaEmail;
	private Boolean boardingPassViaSms;
	private Boolean comfortSeatFree;
	private Boolean corporate;
	private List<CheckinCouponData> coupons;
	private String email;
	private String eticketClass;
	private String eticket;
	private MmbFrequentFlyerCarrierData frequentFlyerCarrier;
	private String frequentFlyerCode;
	private FrequentFlyerCategoryEnum frequentFlyerType;
	private Boolean isGroupPnr;
	private Boolean specialFare;
	private String lastName;
	private MmbLinkTypeEnum linkType;
	private String name;
	private MmbCompartimentalClassEnum oldSeatClass;
	private MmbBaggageOrderData baggageOrder;
	private String paymentRetrieveEmail;
	private Boolean paymentViaEmail;
	private String pnr;
	private String ReservationRPH;
	private Integer routeId;
	private MmbCompartimentalClassEnum seatClass;
	private Boolean selected;
	private String statusNote;
	private CheckinPassengerStatusEnum status;
	private CheckinPassengerTypeEnum type;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public CheckinApisInfoData getApisData() {
		return apisData;
	}
	public void setApisData(CheckinApisInfoData apisData) {
		this.apisData = apisData;
	}
	public MmbAQQTypeEnum getAuthorityPermission() {
		return authorityPermission;
	}
	public void setAuthorityPermission(MmbAQQTypeEnum authorityPermission) {
		this.authorityPermission = authorityPermission;
	}
	public Integer getBaggageAllowanceQuantity() {
		return baggageAllowanceQuantity;
	}
	public void setBaggageAllowanceQuantity(Integer baggageAllowanceQuantity) {
		this.baggageAllowanceQuantity = baggageAllowanceQuantity;
	}
	public String getBaggageAllowance() {
		return baggageAllowance;
	}
	public void setBaggageAllowance(String baggageAllowance) {
		this.baggageAllowance = baggageAllowance;
	}
	public String getBoardingPassRetrieveEmail() {
		return boardingPassRetrieveEmail;
	}
	public void setBoardingPassRetrieveEmail(String boardingPassRetrieveEmail) {
		this.boardingPassRetrieveEmail = boardingPassRetrieveEmail;
	}
	public String getBoardingPassRetrieveSms() {
		return boardingPassRetrieveSms;
	}
	public void setBoardingPassRetrieveSms(String boardingPassRetrieveSms) {
		this.boardingPassRetrieveSms = boardingPassRetrieveSms;
	}
	public Boolean getBoardingPassViaEmail() {
		return boardingPassViaEmail;
	}
	public void setBoardingPassViaEmail(Boolean boardingPassViaEmail) {
		this.boardingPassViaEmail = boardingPassViaEmail;
	}
	public Boolean getBoardingPassViaSms() {
		return boardingPassViaSms;
	}
	public void setBoardingPassViaSms(Boolean boardingPassViaSms) {
		this.boardingPassViaSms = boardingPassViaSms;
	}
	public Boolean getComfortSeatFree() {
		return comfortSeatFree;
	}
	public void setComfortSeatFree(Boolean comfortSeatFree) {
		this.comfortSeatFree = comfortSeatFree;
	}
	public Boolean getCorporate() {
		return corporate;
	}
	public void setCorporate(Boolean corporate) {
		this.corporate = corporate;
	}
	public List<CheckinCouponData> getCoupons() {
		return coupons;
	}
	public void setCoupons(List<CheckinCouponData> coupons) {
		this.coupons = coupons;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEticketClass() {
		return eticketClass;
	}
	public void setEticketClass(String eticketClass) {
		this.eticketClass = eticketClass;
	}
	public String getEticket() {
		return eticket;
	}
	public void setEticket(String eticket) {
		this.eticket = eticket;
	}
	public MmbFrequentFlyerCarrierData getFrequentFlyerCarrier() {
		return frequentFlyerCarrier;
	}
	public void setFrequentFlyerCarrier(
			MmbFrequentFlyerCarrierData frequentFlyerCarrier) {
		this.frequentFlyerCarrier = frequentFlyerCarrier;
	}
	public String getFrequentFlyerCode() {
		return frequentFlyerCode;
	}
	public void setFrequentFlyerCode(String frequentFlyerCode) {
		this.frequentFlyerCode = frequentFlyerCode;
	}
	public FrequentFlyerCategoryEnum getFrequentFlyerType() {
		return frequentFlyerType;
	}
	public void setFrequentFlyerType(FrequentFlyerCategoryEnum frequentFlyerType) {
		this.frequentFlyerType = frequentFlyerType;
	}
	public Boolean getIsGroupPnr() {
		return isGroupPnr;
	}
	public void setIsGroupPnr(Boolean isGroupPnr) {
		this.isGroupPnr = isGroupPnr;
	}
	public Boolean getSpecialFare() {
		return specialFare;
	}
	public void setSpecialFare(Boolean specialFare) {
		this.specialFare = specialFare;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public MmbLinkTypeEnum getLinkType() {
		return linkType;
	}
	public void setLinkType(MmbLinkTypeEnum linkType) {
		this.linkType = linkType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public MmbCompartimentalClassEnum getOldSeatClass() {
		return oldSeatClass;
	}
	public void setOldSeatClass(MmbCompartimentalClassEnum oldSeatClass) {
		this.oldSeatClass = oldSeatClass;
	}
	public MmbBaggageOrderData getBaggageOrder() {
		return baggageOrder;
	}
	public void setBaggageOrder(MmbBaggageOrderData baggageOrder) {
		this.baggageOrder = baggageOrder;
	}
	public String getPaymentRetrieveEmail() {
		return paymentRetrieveEmail;
	}
	public void setPaymentRetrieveEmail(String paymentRetrieveEmail) {
		this.paymentRetrieveEmail = paymentRetrieveEmail;
	}
	public Boolean getPaymentViaEmail() {
		return paymentViaEmail;
	}
	public void setPaymentViaEmail(Boolean paymentViaEmail) {
		this.paymentViaEmail = paymentViaEmail;
	}
	public String getPnr() {
		return pnr;
	}
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	public String getReservationRPH() {
		return ReservationRPH;
	}
	public void setReservationRPH(String reservationRPH) {
		ReservationRPH = reservationRPH;
	}
	public Integer getRouteId() {
		return routeId;
	}
	public void setRouteId(Integer routeId) {
		this.routeId = routeId;
	}
	public MmbCompartimentalClassEnum getSeatClass() {
		return seatClass;
	}
	public void setSeatClass(MmbCompartimentalClassEnum seatClass) {
		this.seatClass = seatClass;
	}
	public Boolean getSelected() {
		return selected;
	}
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
	public String getStatusNote() {
		return statusNote;
	}
	public void setStatusNote(String statusNote) {
		this.statusNote = statusNote;
	}
	public CheckinPassengerStatusEnum getStatus() {
		return status;
	}
	public void setStatus(CheckinPassengerStatusEnum status) {
		this.status = status;
	}
	public CheckinPassengerTypeEnum getType() {
		return type;
	}
	public void setType(CheckinPassengerTypeEnum type) {
		this.type = type;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((ReservationRPH == null) ? 0 : ReservationRPH.hashCode());
		result = prime * result
				+ ((apisData == null) ? 0 : apisData.hashCode());
		result = prime
				* result
				+ ((authorityPermission == null) ? 0 : authorityPermission
						.hashCode());
		result = prime
				* result
				+ ((baggageAllowance == null) ? 0 : baggageAllowance.hashCode());
		result = prime
				* result
				+ ((baggageAllowanceQuantity == null) ? 0
						: baggageAllowanceQuantity.hashCode());
		result = prime * result
				+ ((baggageOrder == null) ? 0 : baggageOrder.hashCode());
		result = prime
				* result
				+ ((boardingPassRetrieveEmail == null) ? 0
						: boardingPassRetrieveEmail.hashCode());
		result = prime
				* result
				+ ((boardingPassRetrieveSms == null) ? 0
						: boardingPassRetrieveSms.hashCode());
		result = prime
				* result
				+ ((boardingPassViaEmail == null) ? 0 : boardingPassViaEmail
						.hashCode());
		result = prime
				* result
				+ ((boardingPassViaSms == null) ? 0 : boardingPassViaSms
						.hashCode());
		result = prime * result
				+ ((comfortSeatFree == null) ? 0 : comfortSeatFree.hashCode());
		result = prime * result
				+ ((corporate == null) ? 0 : corporate.hashCode());
		result = prime * result + ((coupons == null) ? 0 : coupons.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((eticket == null) ? 0 : eticket.hashCode());
		result = prime * result
				+ ((eticketClass == null) ? 0 : eticketClass.hashCode());
		result = prime
				* result
				+ ((frequentFlyerCarrier == null) ? 0 : frequentFlyerCarrier
						.hashCode());
		result = prime
				* result
				+ ((frequentFlyerCode == null) ? 0 : frequentFlyerCode
						.hashCode());
		result = prime
				* result
				+ ((frequentFlyerType == null) ? 0 : frequentFlyerType
						.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((isGroupPnr == null) ? 0 : isGroupPnr.hashCode());
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result
				+ ((linkType == null) ? 0 : linkType.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((oldSeatClass == null) ? 0 : oldSeatClass.hashCode());
		result = prime
				* result
				+ ((paymentRetrieveEmail == null) ? 0 : paymentRetrieveEmail
						.hashCode());
		result = prime * result
				+ ((paymentViaEmail == null) ? 0 : paymentViaEmail.hashCode());
		result = prime * result + ((pnr == null) ? 0 : pnr.hashCode());
		result = prime * result + ((routeId == null) ? 0 : routeId.hashCode());
		result = prime * result
				+ ((seatClass == null) ? 0 : seatClass.hashCode());
		result = prime * result
				+ ((selected == null) ? 0 : selected.hashCode());
		result = prime * result
				+ ((specialFare == null) ? 0 : specialFare.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result
				+ ((statusNote == null) ? 0 : statusNote.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinPassengerData other = (CheckinPassengerData) obj;
		if (ReservationRPH == null) {
			if (other.ReservationRPH != null)
				return false;
		} else if (!ReservationRPH.equals(other.ReservationRPH))
			return false;
		if (apisData == null) {
			if (other.apisData != null)
				return false;
		} else if (!apisData.equals(other.apisData))
			return false;
		if (authorityPermission != other.authorityPermission)
			return false;
		if (baggageAllowance == null) {
			if (other.baggageAllowance != null)
				return false;
		} else if (!baggageAllowance.equals(other.baggageAllowance))
			return false;
		if (baggageAllowanceQuantity == null) {
			if (other.baggageAllowanceQuantity != null)
				return false;
		} else if (!baggageAllowanceQuantity
				.equals(other.baggageAllowanceQuantity))
			return false;
		if (baggageOrder == null) {
			if (other.baggageOrder != null)
				return false;
		} else if (!baggageOrder.equals(other.baggageOrder))
			return false;
		if (boardingPassRetrieveEmail == null) {
			if (other.boardingPassRetrieveEmail != null)
				return false;
		} else if (!boardingPassRetrieveEmail
				.equals(other.boardingPassRetrieveEmail))
			return false;
		if (boardingPassRetrieveSms == null) {
			if (other.boardingPassRetrieveSms != null)
				return false;
		} else if (!boardingPassRetrieveSms
				.equals(other.boardingPassRetrieveSms))
			return false;
		if (boardingPassViaEmail == null) {
			if (other.boardingPassViaEmail != null)
				return false;
		} else if (!boardingPassViaEmail.equals(other.boardingPassViaEmail))
			return false;
		if (boardingPassViaSms == null) {
			if (other.boardingPassViaSms != null)
				return false;
		} else if (!boardingPassViaSms.equals(other.boardingPassViaSms))
			return false;
		if (comfortSeatFree == null) {
			if (other.comfortSeatFree != null)
				return false;
		} else if (!comfortSeatFree.equals(other.comfortSeatFree))
			return false;
		if (corporate == null) {
			if (other.corporate != null)
				return false;
		} else if (!corporate.equals(other.corporate))
			return false;
		if (coupons == null) {
			if (other.coupons != null)
				return false;
		} else if (!coupons.equals(other.coupons))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (eticket == null) {
			if (other.eticket != null)
				return false;
		} else if (!eticket.equals(other.eticket))
			return false;
		if (eticketClass == null) {
			if (other.eticketClass != null)
				return false;
		} else if (!eticketClass.equals(other.eticketClass))
			return false;
		if (frequentFlyerCarrier == null) {
			if (other.frequentFlyerCarrier != null)
				return false;
		} else if (!frequentFlyerCarrier.equals(other.frequentFlyerCarrier))
			return false;
		if (frequentFlyerCode == null) {
			if (other.frequentFlyerCode != null)
				return false;
		} else if (!frequentFlyerCode.equals(other.frequentFlyerCode))
			return false;
		if (frequentFlyerType != other.frequentFlyerType)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (isGroupPnr == null) {
			if (other.isGroupPnr != null)
				return false;
		} else if (!isGroupPnr.equals(other.isGroupPnr))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (linkType != other.linkType)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (oldSeatClass != other.oldSeatClass)
			return false;
		if (paymentRetrieveEmail == null) {
			if (other.paymentRetrieveEmail != null)
				return false;
		} else if (!paymentRetrieveEmail.equals(other.paymentRetrieveEmail))
			return false;
		if (paymentViaEmail == null) {
			if (other.paymentViaEmail != null)
				return false;
		} else if (!paymentViaEmail.equals(other.paymentViaEmail))
			return false;
		if (pnr == null) {
			if (other.pnr != null)
				return false;
		} else if (!pnr.equals(other.pnr))
			return false;
		if (routeId == null) {
			if (other.routeId != null)
				return false;
		} else if (!routeId.equals(other.routeId))
			return false;
		if (seatClass != other.seatClass)
			return false;
		if (selected == null) {
			if (other.selected != null)
				return false;
		} else if (!selected.equals(other.selected))
			return false;
		if (specialFare == null) {
			if (other.specialFare != null)
				return false;
		} else if (!specialFare.equals(other.specialFare))
			return false;
		if (status != other.status)
			return false;
		if (statusNote == null) {
			if (other.statusNote != null)
				return false;
		} else if (!statusNote.equals(other.statusNote))
			return false;
		if (type != other.type)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CheckinPassengerData [id=" + id + ", apisData=" + apisData
				+ ", authorityPermission=" + authorityPermission
				+ ", baggageAllowanceQuantity=" + baggageAllowanceQuantity
				+ ", baggageAllowance=" + baggageAllowance
				+ ", boardingPassRetrieveEmail=" + boardingPassRetrieveEmail
				+ ", boardingPassRetrieveSms=" + boardingPassRetrieveSms
				+ ", boardingPassViaEmail=" + boardingPassViaEmail
				+ ", boardingPassViaSms=" + boardingPassViaSms
				+ ", comfortSeatFree=" + comfortSeatFree + ", corporate="
				+ corporate + ", coupons=" + coupons + ", email=" + email
				+ ", eticketClass=" + eticketClass + ", eticket=" + eticket
				+ ", frequentFlyerCarrier=" + frequentFlyerCarrier
				+ ", frequentFlyerCode=" + frequentFlyerCode
				+ ", frequentFlyerType=" + frequentFlyerType + ", isGroupPnr="
				+ isGroupPnr + ", specialFare=" + specialFare + ", lastName="
				+ lastName + ", linkType=" + linkType + ", name=" + name
				+ ", oldSeatClass=" + oldSeatClass + ", baggageOrder="
				+ baggageOrder + ", paymentRetrieveEmail="
				+ paymentRetrieveEmail + ", paymentViaEmail=" + paymentViaEmail
				+ ", pnr=" + pnr + ", ReservationRPH=" + ReservationRPH
				+ ", routeId=" + routeId + ", seatClass=" + seatClass
				+ ", selected=" + selected + ", statusNote=" + statusNote
				+ ", status=" + status + ", type=" + type + ", toString()="
				+ super.toString() + "]";
	}

}
