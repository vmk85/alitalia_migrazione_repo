package com.alitalia.aem.common.messages.crmdatarest;

import com.alitalia.aem.common.data.crmdatarest.model.*;
import com.alitalia.aem.common.messages.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SetCrmDataInfoResponse extends BaseResponse {

    @SerializedName("result")
    @Expose
    private Result result;

    @SerializedName("conversationID")
    @Expose
    private String conversationID;

    private String error;

    public SetCrmDataInfoResponse() {
    }

    public SetCrmDataInfoResponse(String tid, String sid) {
        super(tid, sid);
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "SetCrmDataInfoResponse{" +
                "result=" + result +
                ", conversationID='" + conversationID + '\'' +
                ", error='" + error + '\'' +
                '}';
    }
}
