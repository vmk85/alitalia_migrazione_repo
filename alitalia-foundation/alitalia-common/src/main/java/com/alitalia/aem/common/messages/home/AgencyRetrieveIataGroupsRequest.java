package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class AgencyRetrieveIataGroupsRequest extends BaseRequest {

	private String codiceAgenzia;
	
	public AgencyRetrieveIataGroupsRequest(String tid, String sid) {
		super(tid, sid);
	}

	public AgencyRetrieveIataGroupsRequest(String tid, String sid, String codiceAgenzia) {
		super(tid, sid);
		this.codiceAgenzia = codiceAgenzia;
	}

	public String getCodiceAgenzia() {
		return codiceAgenzia;
	}
	
	public void setCodiceAgenzia(String codiceAgenzia) {
		this.codiceAgenzia = codiceAgenzia;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((codiceAgenzia == null) ? 0 : codiceAgenzia.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgencyRetrieveIataGroupsRequest other = (AgencyRetrieveIataGroupsRequest) obj;
		if (codiceAgenzia == null) {
			if (other.codiceAgenzia != null)
				return false;
		} else if (!codiceAgenzia.equals(other.codiceAgenzia))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AgencyRetrieveIataGroupsRequest [codiceAgenzia="
				+ codiceAgenzia + "]";
	}
	
}
