package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.PaymentProcessInfoData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetrieveTicketResponse extends BaseResponse {
	private List<PassengerBaseData> passenger;
	private PaymentProcessInfoData process;
	private Boolean invoiceRequestSuccessful;
	
	public List<PassengerBaseData> getPassenger() {
		return passenger;
	}
	
	public void setPassenger(List<PassengerBaseData> passenger) {
		this.passenger = passenger;
	}
	
	public PaymentProcessInfoData getProcess() {
		return process;
	}
	
	public void setProcess(PaymentProcessInfoData process) {
		this.process = process;
	}
	
	public Boolean isInvoiceRequestSuccessful() {
		return invoiceRequestSuccessful;
	}

	public void setInvoiceRequestSuccessful(Boolean invoiceRequestSuccessful) {
		this.invoiceRequestSuccessful = invoiceRequestSuccessful;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((passenger == null) ? 0 : passenger.hashCode());
		result = prime * result + ((process == null) ? 0 : process.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveTicketResponse other = (RetrieveTicketResponse) obj;
		if (passenger == null) {
			if (other.passenger != null)
				return false;
		} else if (!passenger.equals(other.passenger))
			return false;
		if (process == null) {
			if (other.process != null)
				return false;
		} else if (!process.equals(other.process))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveTicketResponse [passenger=" + passenger + ", process="
				+ process + ", invoiceRequestSuccessful=" + invoiceRequestSuccessful
				+ ", getTid()=" + getTid() + ", getSid()=" + getSid() + "]";
	}
	
}
