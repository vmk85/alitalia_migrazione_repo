package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.messages.BaseResponse;

public class RetrieveNewsCategoriesResponse extends BaseResponse{

	private int count;
	private List<String> categories;
	
	public int getCount() {
		return count;
	}
	
	public void setCount(int count) {
		this.count = count;
	}
	
	public List<String> getCategories() {
		return categories;
	}
	
	public void setCategories(List<String> categories) {
		this.categories = categories;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((categories == null) ? 0 : categories.hashCode());
		result = prime * result + count;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveNewsCategoriesResponse other = (RetrieveNewsCategoriesResponse) obj;
		if (categories == null) {
			if (other.categories != null)
				return false;
		} else if (!categories.equals(other.categories))
			return false;
		if (count != other.count)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveNewsCategoriesResponse [count=" + count
				+ ", categories=" + categories + "]";
	}
}