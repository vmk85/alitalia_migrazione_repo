
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WeightAndSize {

    @SerializedName("weightAndUnit")
    @Expose
    private WeightAndUnit weightAndUnit;
    @SerializedName("lengthAndUnit")
    @Expose
    private LengthAndUnit lengthAndUnit;

    public WeightAndUnit getWeightAndUnit() {
        return weightAndUnit;
    }

    public void setWeightAndUnit(WeightAndUnit weightAndUnit) {
        this.weightAndUnit = weightAndUnit;
    }

    public LengthAndUnit getLengthAndUnit() {
        return lengthAndUnit;
    }

    public void setLengthAndUnit(LengthAndUnit lengthAndUnit) {
        this.lengthAndUnit = lengthAndUnit;
    }

}
