package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class ValidateMmUserCheckinResponse extends BaseResponse {

	private boolean isValid;
	private String lastname;
	private String name;
	
	public ValidateMmUserCheckinResponse(String tid, String sid) {
		super(tid, sid);
	}

	public ValidateMmUserCheckinResponse() {
		super();
	}
	
	public boolean isValid() {
		return isValid;
	}
	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (isValid ? 1231 : 1237);
		result = prime * result
				+ ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValidateMmUserCheckinResponse other = (ValidateMmUserCheckinResponse) obj;
		if (isValid != other.isValid)
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "ValidateMmUserResponse [isValid=" + isValid + ", lastname="
				+ lastname + ", name=" + name + ", isValid()=" + isValid()
				+ ", getLastname()=" + getLastname() + ", getName()="
				+ getName() + ", hashCode()=" + hashCode() + ", getTid()="
				+ getTid() + ", getSid()=" + getSid() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass() + "]";
	}
}
