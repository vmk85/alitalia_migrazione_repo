package com.alitalia.aem.common.data.checkinrest.model.passenger;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VcrNumberList {
	
	@SerializedName("couponNumber")
	@Expose
	private Integer couponNumber;
	
	@SerializedName("couponNumberSpecified")
	@Expose
	private Boolean couponNumberSpecified;
	
	@SerializedName("couponStatus")
	@Expose
	private String couponStatus;
	
	@SerializedName("origin")
	@Expose
	private String origin;
	
	@SerializedName("destination")
	@Expose
	private String destination;
	
	@SerializedName("class")
	@Expose
	private String _class;
	
	@SerializedName("isDisassociated")
	@Expose
	private Boolean isDisassociated;
	
	@SerializedName("isDisassociatedSpecified")
	@Expose
	private Boolean isDisassociatedSpecified;
	
	@SerializedName("date")
	@Expose
	private String date;
	
	@SerializedName("value")
	@Expose
	private String value;

	public Integer getCouponNumber() {
		return couponNumber;
	}

	public void setCouponNumber(Integer couponNumber) {
		this.couponNumber = couponNumber;
	}

	public Boolean getCouponNumberSpecified() {
		return couponNumberSpecified;
	}

	public void setCouponNumberSpecified(Boolean couponNumberSpecified) {
		this.couponNumberSpecified = couponNumberSpecified;
	}

	public String getCouponStatus() {
		return couponStatus;
	}

	public void setCouponStatus(String couponStatus) {
		this.couponStatus = couponStatus;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String get_class() {
		return _class;
	}

	public void set_class(String _class) {
		this._class = _class;
	}

	public Boolean getIsDisassociated() {
		return isDisassociated;
	}

	public void setIsDisassociated(Boolean isDisassociated) {
		this.isDisassociated = isDisassociated;
	}

	public Boolean getIsDisassociatedSpecified() {
		return isDisassociatedSpecified;
	}

	public void setIsDisassociatedSpecified(Boolean isDisassociatedSpecified) {
		this.isDisassociatedSpecified = isDisassociatedSpecified;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}


}
