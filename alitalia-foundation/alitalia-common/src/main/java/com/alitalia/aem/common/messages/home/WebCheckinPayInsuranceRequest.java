package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.checkin.CheckinInsurancePolicyData;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.checkin.CheckinRouteData;
import com.alitalia.aem.common.data.home.enumerations.MmbLegTypeEnum;
import com.alitalia.aem.common.messages.MmbBaseRequest;

public class WebCheckinPayInsuranceRequest extends MmbBaseRequest {
    
    private MmbLegTypeEnum area;
    private Boolean hasReturn;
    private CheckinInsurancePolicyData insurancePolicy;
	private List<CheckinPassengerData> passengersList;
	private CheckinRouteData selectedRoute;

	public WebCheckinPayInsuranceRequest(String tid, String sid) {
		super(tid, sid);
	}

	public WebCheckinPayInsuranceRequest() {
		super();
	}

	public MmbLegTypeEnum getArea() {
		return area;
	}

	public void setArea(MmbLegTypeEnum area) {
		this.area = area;
	}

	public Boolean getHasReturn() {
		return hasReturn;
	}

	public void setHasReturn(Boolean hasReturn) {
		this.hasReturn = hasReturn;
	}

	public CheckinInsurancePolicyData getInsurancePolicy() {
		return insurancePolicy;
	}

	public void setInsurancePolicy(CheckinInsurancePolicyData insurancePolicy) {
		this.insurancePolicy = insurancePolicy;
	}

	public List<CheckinPassengerData> getPassengersList() {
		return passengersList;
	}

	public void setPassengersList(List<CheckinPassengerData> passengersList) {
		this.passengersList = passengersList;
	}

	public CheckinRouteData getSelectedRoute() {
		return selectedRoute;
	}

	public void setSelectedRoute(CheckinRouteData selectedRoute) {
		this.selectedRoute = selectedRoute;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((area == null) ? 0 : area.hashCode());
		result = prime * result
				+ ((hasReturn == null) ? 0 : hasReturn.hashCode());
		result = prime * result
				+ ((insurancePolicy == null) ? 0 : insurancePolicy.hashCode());
		result = prime * result
				+ ((passengersList == null) ? 0 : passengersList.hashCode());
		result = prime * result
				+ ((selectedRoute == null) ? 0 : selectedRoute.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebCheckinPayInsuranceRequest other = (WebCheckinPayInsuranceRequest) obj;
		if (area != other.area)
			return false;
		if (hasReturn == null) {
			if (other.hasReturn != null)
				return false;
		} else if (!hasReturn.equals(other.hasReturn))
			return false;
		if (insurancePolicy == null) {
			if (other.insurancePolicy != null)
				return false;
		} else if (!insurancePolicy.equals(other.insurancePolicy))
			return false;
		if (passengersList == null) {
			if (other.passengersList != null)
				return false;
		} else if (!passengersList.equals(other.passengersList))
			return false;
		if (selectedRoute == null) {
			if (other.selectedRoute != null)
				return false;
		} else if (!selectedRoute.equals(other.selectedRoute))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WebCheckinPayInsuranceRequest [area=" + area + ", hasReturn="
				+ hasReturn + ", insurancePolicy=" + insurancePolicy
				+ ", passengersList=" + passengersList + ", selectedRoute="
				+ selectedRoute + ", toString()=" + super.toString() + "]";
	}
}
