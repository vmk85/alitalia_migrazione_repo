package com.alitalia.aem.common.data.checkinrest.model.passenger;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FacilityList {


	@SerializedName("facilityCode")
	@Expose
	private String facilityCode;
	
	@SerializedName("facilityDirection")
	@Expose
	private String facilityDirection;
	
	@SerializedName("padisCode")
	@Expose
	private String padisCode;

	public String getFacilityCode() {
		return facilityCode;
	}

	public void setFacilityCode(String facilityCode) {
		this.facilityCode = facilityCode;
	}

	public String getFacilityDirection() {
		return facilityDirection;
	}

	public void setFacilityDirection(String facilityDirection) {
		this.facilityDirection = facilityDirection;
	}

	public String getPadisCode() {
		return padisCode;
	}

	public void setPadisCode(String padisCode) {
		this.padisCode = padisCode;
	}

}
