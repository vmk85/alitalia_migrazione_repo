package com.alitalia.aem.common.logging.CrmData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CrmServiceLog implements CrmDataLogger {

    private static Logger logger = LoggerFactory.getLogger(CrmServiceLog.class);

    CrmErrorLog crmErrorLog = new CrmErrorLog();

    @Override
    public void error(String log) {
        logger.error(log);
        crmErrorLog.error(log);
    }

    @Override
    public void debug(String log) {
        logger.debug(log);
    }

    @Override
    public void info(String log) {
        logger.info(log);
    }

    @Override
    public void trace(String log) {
        logger.trace(log);
    }
}
