package com.alitalia.aem.common.data.home.enumerations;

public enum MmbPaymentGroupMaskEnum {

	NONE("None"),
	CREDIT_CARDS("CreditCards"),
	OTHER_PAYMENT_METHODS("OtherPaymentMethods"),
	CREDIT_CARDS_BRAZIL("CreditCardsBrazil"),
	CREDIT_CARDS_INSTALLMENT_BRAZIL("CreditCardsInstallmentBrazil"),
	CREDIT_CARDS_ARGENTINA("CreditCardsArgentina"),
	BANKING_ONLINE_METHODS("BankingOnlineMethods"),
	MY_CREDIT_CARDS("MyCreditCards"),
	BOOKING_SAVED_CARD("BookingSavedCard");

	private final String value;

    MmbPaymentGroupMaskEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MmbPaymentGroupMaskEnum fromValue(String v) {
        for (MmbPaymentGroupMaskEnum c: MmbPaymentGroupMaskEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
