package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.messages.MmbBaseRequest;

public class ChangeSeatCheckinRequest extends MmbBaseRequest {

	private MmbCompartimentalClassEnum compartimentalClass;
	private String eticket;
	private CheckinFlightData flight;
	private String lastname;
	private String name;
	private String seatNumber;
	
	public MmbCompartimentalClassEnum getCompartimentalClass() {
		return compartimentalClass;
	}
	public void setCompartimentalClass(
			MmbCompartimentalClassEnum compartimentalClass) {
		this.compartimentalClass = compartimentalClass;
	}
	public String getEticket() {
		return eticket;
	}
	public void setEticket(String eticket) {
		this.eticket = eticket;
	}
	public CheckinFlightData getFlight() {
		return flight;
	}
	public void setFlight(CheckinFlightData flight) {
		this.flight = flight;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSeatNumber() {
		return seatNumber;
	}
	public void setSeatNumber(String seatNumber) {
		this.seatNumber = seatNumber;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((compartimentalClass == null) ? 0 : compartimentalClass
						.hashCode());
		result = prime * result + ((eticket == null) ? 0 : eticket.hashCode());
		result = prime * result + ((flight == null) ? 0 : flight.hashCode());
		result = prime * result
				+ ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((seatNumber == null) ? 0 : seatNumber.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChangeSeatCheckinRequest other = (ChangeSeatCheckinRequest) obj;
		if (compartimentalClass != other.compartimentalClass)
			return false;
		if (eticket == null) {
			if (other.eticket != null)
				return false;
		} else if (!eticket.equals(other.eticket))
			return false;
		if (flight == null) {
			if (other.flight != null)
				return false;
		} else if (!flight.equals(other.flight))
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (seatNumber == null) {
			if (other.seatNumber != null)
				return false;
		} else if (!seatNumber.equals(other.seatNumber))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ChangeSeatCheckinRequest [compartimentalClass="
				+ compartimentalClass + ", eticket=" + eticket + ", flight="
				+ flight + ", lastname=" + lastname + ", name=" + name
				+ ", seatNumber=" + seatNumber + ", toString()="
				+ super.toString() + "]";
	}

}
