package com.alitalia.aem.common.messages.checkinrest;

import java.util.List;

import com.alitalia.aem.common.data.checkinrest.model.updatepassenger.request.Passenger;
import com.alitalia.aem.common.messages.BaseRequest;

public class CheckinUpdatePassengerRequest extends BaseRequest{

	private String airline;
	private String departureDate;
	private String flight;
	private String origin;
	private List<Passenger> passengers = null;
	private String language;
	private String market;
	private String conversationID;

	public String getAirline() {
		return airline;
	}

	public void setAirline(String airline) {
		this.airline = airline;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getFlight() {
		return flight;
	}

	public void setFlight(String flight) {
		this.flight = flight;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public List<Passenger> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<Passenger> passengers) {
		this.passengers = passengers;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public String getConversationID() {
		return conversationID;
	}

	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}

//	private UpdatePassengerReq _updatepassengerReq;
//
//
//	public UpdatePassengerReq get_updatepassengerReq() {
//		return _updatepassengerReq;
//	}

//
//	public void set_updatepassengerReq(UpdatePassengerReq _updatepassengerReq) {
//		this._updatepassengerReq = _updatepassengerReq;
//	}


	public CheckinUpdatePassengerRequest(String tid, String sid) {
		super(tid, sid);
	}


}
