package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.fasttrack.response.FastTrack;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinSetFastTrackResponse extends BaseResponse{
	private FastTrack fastTrack;

	public CheckinSetFastTrackResponse() {}

	public CheckinSetFastTrackResponse(String tid, String sid) {
		super(tid, sid);
	}

	public FastTrack getFastTrack() {
		return fastTrack;
	}

	public void setFastTrack(FastTrack fastTrack) {
		this.fastTrack = fastTrack;
	}
}
