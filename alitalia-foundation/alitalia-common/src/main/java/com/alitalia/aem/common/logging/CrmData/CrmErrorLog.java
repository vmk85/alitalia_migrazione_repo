package com.alitalia.aem.common.logging.CrmData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CrmErrorLog {

    private static Logger logger = LoggerFactory.getLogger(CrmErrorLog.class);

    public void error(String log) {
        logger.error(log);
    }

}
