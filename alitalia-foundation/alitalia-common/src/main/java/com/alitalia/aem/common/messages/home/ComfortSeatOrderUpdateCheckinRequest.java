package com.alitalia.aem.common.messages.home;


public class ComfortSeatOrderUpdateCheckinRequest extends ComfortSeatOrderCheckinRequest {

    private String newSeat;

	public String getNewSeat() {
		return newSeat;
	}

	public void setNewSeat(String newSeat) {
		this.newSeat = newSeat;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((newSeat == null) ? 0 : newSeat.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComfortSeatOrderUpdateCheckinRequest other = (ComfortSeatOrderUpdateCheckinRequest) obj;
		if (newSeat == null) {
			if (other.newSeat != null)
				return false;
		} else if (!newSeat.equals(other.newSeat))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ComfortSeatOrderUpdateCheckinRequest [newSeat=" + newSeat
				+ ", toString()=" + super.toString() + "]";
	}

}
