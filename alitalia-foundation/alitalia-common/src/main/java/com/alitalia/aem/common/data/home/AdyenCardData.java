package com.alitalia.aem.common.data.home;

public class AdyenCardData {
	
	/*
		{
			"cvc": null,
			"expiryMonth": "8",
			"expiryYear": "2018",
			"holderName": "Test Uno",
			"number": "1111",
			"type": "visa"
		}
	 */
	
	private String cvc;
	private String expiryMonth;
	private String expiryYear;
	private String holderName;
	private String number;
	private String type;
	
	public String getCvc() {
		return cvc;
	}
	public void setCvc(String cvc) {
		this.cvc = cvc;
	}
	public String getExpiryMonth() {
		return expiryMonth;
	}
	public void setExpiryMonth(String expiryMonth) {
		this.expiryMonth = expiryMonth;
	}
	public String getExpiryYear() {
		return expiryYear;
	}
	public void setExpiryYear(String expiryYear) {
		this.expiryYear = expiryYear;
	}
	public String getHolderName() {
		return holderName;
	}
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public AdyenCardData(String cvc, String expiryMonth, String expiryYear, String holderName, String number, String type) {
		super();
		this.cvc = cvc;
		this.expiryMonth = expiryMonth;
		this.expiryYear = expiryYear;
		this.holderName = holderName;
		this.number = number;
		this.type = type;
	}

	public AdyenCardData(String type) {
		super();
		this.type = type;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result	+ ((cvc == null) ? 0 : cvc.hashCode());
		result = prime * result	+ ((expiryMonth == null) ? 0 : expiryMonth.hashCode());
		result = prime * result	+ ((expiryYear == null) ? 0 : expiryYear.hashCode());
		result = prime * result	+ ((holderName == null) ? 0 : holderName.hashCode());
		result = prime * result	+ ((number == null) ? 0 : number.hashCode());
		result = prime * result	+ ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdyenCardData other = (AdyenCardData) obj;
		if (cvc == null) {
			if (other.cvc != null)
				return false;
		} else if (!cvc.equals(other.cvc))
			return false;
		if (expiryMonth == null) {
			if (other.expiryMonth != null)
				return false;
		} else if (!expiryMonth.equals(other.expiryMonth))
			return false;
		if (expiryYear == null) {
			if (other.expiryYear != null)
				return false;
		} else if (!expiryYear.equals(other.expiryYear))
			return false;
		if (holderName == null) {
			if (other.holderName != null)
				return false;
		} else if (!holderName.equals(other.holderName))
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "Card ["
				+ "cvc=" + cvc
				+ ", expiryMonth=" + expiryMonth
				+ ", expiryYear=" + expiryYear
				+ ", holderName=" + holderName
				+ ", number=" + number
				+ ", type=" + type
				+ "]";
	}

}
