package com.alitalia.aem.common.data.home.enumerations;


public enum CheckinEmdTicketType {

	E_TICKET("eTicket"),
    UNKNOW("Unknow"),
    PAPER("Paper"),
    MCO("MCO");

	private final String value;

    CheckinEmdTicketType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CheckinEmdTicketType fromValue(String v) {
        for (CheckinEmdTicketType c: CheckinEmdTicketType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
