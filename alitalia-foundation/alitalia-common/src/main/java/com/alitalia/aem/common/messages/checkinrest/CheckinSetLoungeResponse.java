package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.lounge.response.Lounge;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinSetLoungeResponse extends BaseResponse{
	private Lounge lounge;

	public CheckinSetLoungeResponse() {}

	public CheckinSetLoungeResponse(String tid, String sid) {
		super(tid, sid);
	}

	public Lounge getLounge() {
		return lounge;
	}

	public void setLounge(Lounge lounge) {
		this.lounge = lounge;
	}
}
