
package com.alitalia.aem.common.data.checkinrest.model.passenger;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TaxDetailsPassenger {

    @SerializedName("totalTaxAmount")
    @Expose
    private TotalTaxAmountPassenger totalTaxAmount;
    @SerializedName("taxAmount")
    @Expose
    private List<TaxAmountPassenger> taxAmount = null;

    public TotalTaxAmountPassenger getTotalTaxAmount() {
        return totalTaxAmount;
    }

    public void setTotalTaxAmount(TotalTaxAmountPassenger totalTaxAmount) {
        this.totalTaxAmount = totalTaxAmount;
    }

    public List<TaxAmountPassenger> getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(List<TaxAmountPassenger> taxAmount) {
        this.taxAmount = taxAmount;
    }

}
