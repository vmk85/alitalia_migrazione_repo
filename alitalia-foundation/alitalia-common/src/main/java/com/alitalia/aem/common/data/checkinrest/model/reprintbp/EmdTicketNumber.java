
package com.alitalia.aem.common.data.checkinrest.model.reprintbp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EmdTicketNumber {

    @SerializedName("couponNumber")
    @Expose
    private Long couponNumber;
    @SerializedName("couponNumberSpecified")
    @Expose
    private Boolean couponNumberSpecified;
    @SerializedName("value")
    @Expose
    private String value;

    public Long getCouponNumber() {
        return couponNumber;
    }

    public void setCouponNumber(Long couponNumber) {
        this.couponNumber = couponNumber;
    }

    public Boolean getCouponNumberSpecified() {
        return couponNumberSpecified;
    }

    public void setCouponNumberSpecified(Boolean couponNumberSpecified) {
        this.couponNumberSpecified = couponNumberSpecified;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
