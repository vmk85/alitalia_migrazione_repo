package com.alitalia.aem.common.data.home.enumerations;

public enum CheckinPassengerTypeEnum {

	NORMAL("Normal"),
    CHILD("Child"),
    INFANT("Infant");
	
    private final String value;

    CheckinPassengerTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CheckinPassengerTypeEnum fromValue(String v) {
        for (CheckinPassengerTypeEnum c: CheckinPassengerTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
