package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class AwardValueResponse extends BaseResponse {

	private Long awardValue;

	public Long getAwardValue() {
		return awardValue;
	}

	public void setAwardValue(Long awardValue) {
		this.awardValue = awardValue;
	}

	@Override
	public String toString() {
		return "AwardValueResponse [awardValue=" + awardValue + ", getTid()="
				+ getTid() + ", getSid()=" + getSid() + "]";
	}

}
