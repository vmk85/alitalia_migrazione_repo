package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.BaseRequest;

public class DeletePanRequest extends BaseRequest {

	private String panCode;
	private MMCustomerProfileData customerProfile;

	public String getPanCode() {
		return panCode;
	}

	public void setPanCode(String panCode) {
		this.panCode = panCode;
	}

	public MMCustomerProfileData getCustomerProfile() {
		return customerProfile;
	}

	public void setCustomerProfile(MMCustomerProfileData customerProfile) {
		this.customerProfile = customerProfile;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((customerProfile == null) ? 0 : customerProfile.hashCode());
		result = prime * result + ((panCode == null) ? 0 : panCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeletePanRequest other = (DeletePanRequest) obj;
		if (customerProfile == null) {
			if (other.customerProfile != null)
				return false;
		} else if (!customerProfile.equals(other.customerProfile))
			return false;
		if (panCode == null) {
			if (other.panCode != null)
				return false;
		} else if (!panCode.equals(other.panCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DeletePanRequest [panCode=" + panCode + ", customerProfile="
				+ customerProfile + "]";
	}
}