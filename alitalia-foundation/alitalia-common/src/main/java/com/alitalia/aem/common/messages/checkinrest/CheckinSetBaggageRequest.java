package com.alitalia.aem.common.messages.checkinrest;

import java.util.List;

import com.alitalia.aem.common.data.checkinrest.model.setbaggage.request.FlightBaggage;
import com.alitalia.aem.common.messages.BaseRequest;

public class CheckinSetBaggageRequest extends BaseRequest{
	
    private String pnr;
    private List<FlightBaggage> flightBaggage = null;
    private String language;
    private String market;
    private String conversationID;

	public CheckinSetBaggageRequest(String tid, String sid) {
		super(tid, sid);
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public List<FlightBaggage> getFlightBaggage() {
		return flightBaggage;
	}

	public void setFlightBaggage(List<FlightBaggage> flightBaggage) {
		this.flightBaggage = flightBaggage;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public String getConversationID() {
		return conversationID;
	}

	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}
	
}
