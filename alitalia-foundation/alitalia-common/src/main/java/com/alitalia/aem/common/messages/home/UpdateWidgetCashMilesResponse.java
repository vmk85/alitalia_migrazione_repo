package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class UpdateWidgetCashMilesResponse extends BaseResponse {

	private Boolean succeded;
	private CashAndMilesData cashMiles;

	public UpdateWidgetCashMilesResponse() {
		super();
	}
	
	public UpdateWidgetCashMilesResponse(String tid, String sid) {
		super(tid, sid);
	}

	public Boolean isSucceded() {
		return succeded;
	}

	public void setSucceded(Boolean isSucceded) {
		this.succeded = isSucceded;
	}

	public CashAndMilesData getCashMiles() {
		return cashMiles;
	}

	public void setCashMiles(CashAndMilesData cashMiles) {
		this.cashMiles = cashMiles;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((cashMiles == null) ? 0 : cashMiles.hashCode());
		result = prime * result
				+ ((succeded == null) ? 0 : succeded.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		UpdateWidgetCashMilesResponse other = (UpdateWidgetCashMilesResponse) obj;
		if (cashMiles == null) {
			if (other.cashMiles != null)
				return false;
		} else if (!cashMiles.equals(other.cashMiles))
			return false;
		if (succeded == null) {
			if (other.succeded != null)
				return false;
		} else if (!succeded.equals(other.succeded))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UpdateWidgetCashMilesResponse [succeded=" + succeded
				+ ", cashMiles=" + cashMiles + ", getTid()=" + getTid()
				+ ", getSid()=" + getSid() + "]";
	}

}