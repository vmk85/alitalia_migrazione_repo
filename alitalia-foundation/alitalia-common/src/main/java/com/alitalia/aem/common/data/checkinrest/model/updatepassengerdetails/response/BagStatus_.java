
package com.alitalia.aem.common.data.checkinrest.model.updatepassengerdetails.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BagStatus_ {

    @SerializedName("aeStatus")
    @Expose
    private String aeStatus;
    @SerializedName("aeStatusSpecified")
    @Expose
    private Boolean aeStatusSpecified;
    @SerializedName("value")
    @Expose
    private String value;

    public String getAeStatus() {
        return aeStatus;
    }

    public void setAeStatus(String aeStatus) {
        this.aeStatus = aeStatus;
    }

    public Boolean getAeStatusSpecified() {
        return aeStatusSpecified;
    }

    public void setAeStatusSpecified(Boolean aeStatusSpecified) {
        this.aeStatusSpecified = aeStatusSpecified;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
