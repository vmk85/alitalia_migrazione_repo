package com.alitalia.aem.common.data.home;


public class AdultPassengerData extends PassengerBaseData {

	private PreferencesData preferences;
	private String frequentFlyerCode;
	private String frequentFlyerTier;
	private FrequentFlyerTypeData frequentFlyerType;

	public AdultPassengerData() {
		super();
	}

	public AdultPassengerData(AdultPassengerData clone) {
		super(clone);
		this.frequentFlyerCode = clone.getFrequentFlyerCode();
		this.frequentFlyerTier = clone.getFrequentFlyerTier();
		this.frequentFlyerType = clone.getFrequentFlyerType();
		PreferencesData clonePreferences = clone.getPreferences();
		if (clonePreferences != null)
			this.preferences = new PreferencesData(clonePreferences);
	}

	public AdultPassengerData(PassengerBaseData clone) {
		super(clone);
		this.frequentFlyerCode = null;
		this.frequentFlyerTier = null;
		this.frequentFlyerType = null;
		this.preferences = null;
	}

	public PreferencesData getPreferences() {
		return preferences;
	}
	
	public void setPreferences(PreferencesData preferences) {
		this.preferences = preferences;
	}
	
	public String getFrequentFlyerCode() {
		return frequentFlyerCode;
	}
	
	public void setFrequentFlyerCode(String frequentFlyerCode) {
		this.frequentFlyerCode = frequentFlyerCode;
	}
	
	public String getFrequentFlyerTier() {
		return frequentFlyerTier;
	}
	
	public void setFrequentFlyerTier(String frequentFlyerTier) {
		this.frequentFlyerTier = frequentFlyerTier;
	}
	
	public FrequentFlyerTypeData getFrequentFlyerType() {
		return frequentFlyerType;
	}
	
	public void setFrequentFlyerType(FrequentFlyerTypeData frequentFlyerType) {
		this.frequentFlyerType = frequentFlyerType;
	}

	@Override
	public String toString() {
		return "AdultPassengerData [preferences=" + preferences
				+ ", frequentFlyerCode=" + frequentFlyerCode
				+ ", frequentFlyerTier=" + frequentFlyerTier
				+ ", frequentFlyerType=" + frequentFlyerType + "]";
	}

}
