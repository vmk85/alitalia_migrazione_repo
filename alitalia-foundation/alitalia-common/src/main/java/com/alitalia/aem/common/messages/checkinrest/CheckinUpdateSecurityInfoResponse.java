package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.updatesecurityinfo.response.UpdateSecurityInfoResp;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinUpdateSecurityInfoResponse extends BaseResponse{
	
	private UpdateSecurityInfoResp _updatesecurityinfoResp;
	
	public UpdateSecurityInfoResp get_updatesecurityinfoResp() {
		return _updatesecurityinfoResp;
	}

	public void set_updatesecurityinfoResp(UpdateSecurityInfoResp _updatesecurityinfoResp) {
		this._updatesecurityinfoResp = _updatesecurityinfoResp;
	}

	public CheckinUpdateSecurityInfoResponse(String tid, String sid) {
		super(tid, sid);
	}

	public CheckinUpdateSecurityInfoResponse() {}

}
