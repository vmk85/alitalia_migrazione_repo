package com.alitalia.aem.common.data.home;

import java.util.ArrayList;
import java.util.List;

import com.alitalia.aem.common.data.home.enumerations.AddressFamilyEnum;

public class IPAddressData {

	private long mAddress;
    private AddressFamilyEnum mFamily;
    private int mHashCode;
    private List<Integer> mNumbers;
    private long mScopeId;
	
    public long getmAddress() {
		return mAddress;
	}
	
    public void setmAddress(long mAddress) {
		this.mAddress = mAddress;
	}
	
    public AddressFamilyEnum getmFamily() {
		return mFamily;
	}
	
    public void setmFamily(AddressFamilyEnum mFamily) {
		this.mFamily = mFamily;
	}
	
    public int getmHashCode() {
		return mHashCode;
	}
	
    public void setmHashCode(int mHashCode) {
		this.mHashCode = mHashCode;
	}
	
    public List<Integer> getmNumbers() {
    	if(mNumbers == null){
    		mNumbers = new ArrayList<Integer>();
    	}
		return mNumbers;
	}
	
    public void setmNumbers(List<Integer> mNumbers) {
		this.mNumbers = mNumbers;
	}
	
    public long getmScopeId() {
		return mScopeId;
	}
	
    public void setmScopeId(long mScopeId) {
		this.mScopeId = mScopeId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (mAddress ^ (mAddress >>> 32));
		result = prime * result + ((mFamily == null) ? 0 : mFamily.hashCode());
		result = prime * result + mHashCode;
		result = prime * result
				+ ((mNumbers == null) ? 0 : mNumbers.hashCode());
		result = prime * result + (int) (mScopeId ^ (mScopeId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IPAddressData other = (IPAddressData) obj;
		if (mAddress != other.mAddress)
			return false;
		if (mFamily != other.mFamily)
			return false;
		if (mHashCode != other.mHashCode)
			return false;
		if (mNumbers == null) {
			if (other.mNumbers != null)
				return false;
		} else if (!mNumbers.equals(other.mNumbers))
			return false;
		if (mScopeId != other.mScopeId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "IPAddressData [mAddress=" + mAddress + ", mFamily=" + mFamily
				+ ", mHashCode=" + mHashCode + ", mNumbers=" + mNumbers
				+ ", mScopeId=" + mScopeId + "]";
	}
}