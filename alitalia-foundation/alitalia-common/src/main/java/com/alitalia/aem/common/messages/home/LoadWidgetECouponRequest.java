package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.TaxData;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.messages.BaseRequest;

public class LoadWidgetECouponRequest extends BaseRequest {
	
	private String site;
	private CabinEnum cabinClass;
	private List<CabinEnum> cabinClassList;
    private String eCouponCode;
    private Boolean isRoundTrip;
    private List<FlightData> itineraries;
    private List<PassengerBaseData> passengers;
    private List<TaxData> taxes;
	
	public LoadWidgetECouponRequest(String tid, String sid) {
		super(tid, sid);
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}
	
	public String geteCouponCode() {
		return eCouponCode;
	}

	public void seteCouponCode(String eCouponCode) {
		this.eCouponCode = eCouponCode;
	}

	public Boolean getIsRoundTrip() {
		return isRoundTrip;
	}

	public void setIsRoundTrip(Boolean isRoundTrip) {
		this.isRoundTrip = isRoundTrip;
	}

	public List<FlightData> getItineraries() {
		return itineraries;
	}

	public void setItineraries(List<FlightData> itineraries) {
		this.itineraries = itineraries;
	}

	public List<PassengerBaseData> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<PassengerBaseData> passengers) {
		this.passengers = passengers;
	}

	public List<TaxData> getTaxes() {
		return taxes;
	}

	public void setTaxes(List<TaxData> taxes) {
		this.taxes = taxes;
	}

	public List<CabinEnum> getCabinClassList() {
		return cabinClassList;
	}

	public void setCabinClassList(List<CabinEnum> cabinClassList) {
		this.cabinClassList = cabinClassList;
	}

	public CabinEnum getCabinClass() {
		return cabinClass;
	}

	public void setCabinClass(CabinEnum cabinClass) {
		this.cabinClass = cabinClass;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((cabinClass == null) ? 0 : cabinClass.hashCode());
		result = prime * result
				+ ((cabinClassList == null) ? 0 : cabinClassList.hashCode());
		result = prime * result
				+ ((eCouponCode == null) ? 0 : eCouponCode.hashCode());
		result = prime * result
				+ ((isRoundTrip == null) ? 0 : isRoundTrip.hashCode());
		result = prime * result
				+ ((itineraries == null) ? 0 : itineraries.hashCode());
		result = prime * result
				+ ((passengers == null) ? 0 : passengers.hashCode());
		result = prime * result + ((site == null) ? 0 : site.hashCode());
		result = prime * result + ((taxes == null) ? 0 : taxes.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoadWidgetECouponRequest other = (LoadWidgetECouponRequest) obj;
		if (cabinClass != other.cabinClass)
			return false;
		if (cabinClassList == null) {
			if (other.cabinClassList != null)
				return false;
		} else if (!cabinClassList.equals(other.cabinClassList))
			return false;
		if (eCouponCode == null) {
			if (other.eCouponCode != null)
				return false;
		} else if (!eCouponCode.equals(other.eCouponCode))
			return false;
		if (isRoundTrip == null) {
			if (other.isRoundTrip != null)
				return false;
		} else if (!isRoundTrip.equals(other.isRoundTrip))
			return false;
		if (itineraries == null) {
			if (other.itineraries != null)
				return false;
		} else if (!itineraries.equals(other.itineraries))
			return false;
		if (passengers == null) {
			if (other.passengers != null)
				return false;
		} else if (!passengers.equals(other.passengers))
			return false;
		if (site == null) {
			if (other.site != null)
				return false;
		} else if (!site.equals(other.site))
			return false;
		if (taxes == null) {
			if (other.taxes != null)
				return false;
		} else if (!taxes.equals(other.taxes))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "LoadWidgetECouponRequest [site=" + site + ", cabinClass="
				+ cabinClass + ", cabinClassList=" + cabinClassList
				+ ", eCouponCode=" + eCouponCode + ", isRoundTrip="
				+ isRoundTrip + ", itineraries=" + itineraries
				+ ", passengers=" + passengers + ", taxes=" + taxes + "]";
	}
	
//	@Override
//	public String toString() {
//		return "LoadWidgetECouponRequest [site=" + site + ", cabinClass="
//				+ cabinClass + ", eCouponCode=" + eCouponCode
//				+ ", isRoundTrip=" + isRoundTrip + ", itineraries="
//				+ itineraries + ", passengers=" + passengers + ", taxes="
//				+ taxes + "]";
//	}
}
