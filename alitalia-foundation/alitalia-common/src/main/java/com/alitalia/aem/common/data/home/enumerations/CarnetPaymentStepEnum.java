package com.alitalia.aem.common.data.home.enumerations;

public enum CarnetPaymentStepEnum {

	INITIALIZE("Initialize"),
    AUTHORIZE("Authorize"),
    CHECK("Check"),
    RETRIEVE("Retrieve"),
    CLOSE("Close");
    
    private final String value;

    CarnetPaymentStepEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CarnetPaymentStepEnum fromValue(String v) {
        for (CarnetPaymentStepEnum c: CarnetPaymentStepEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
