
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequiredInfoSumList {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("detailStatus")
    @Expose
    private List<String> detailStatus = null;
    @SerializedName("freeText")
    @Expose
    private String freeText;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<String> getDetailStatus() {
        return detailStatus;
    }

    public void setDetailStatus(List<String> detailStatus) {
        this.detailStatus = detailStatus;
    }

    public String getFreeText() {
        return freeText;
    }

    public void setFreeText(String freeText) {
        this.freeText = freeText;
    }

}
