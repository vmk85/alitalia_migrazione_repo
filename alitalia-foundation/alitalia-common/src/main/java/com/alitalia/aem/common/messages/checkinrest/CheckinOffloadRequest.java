
package com.alitalia.aem.common.messages.checkinrest;

import java.util.List;

import com.alitalia.aem.common.data.checkinrest.model.offload.request.PnrFlightInfo;
import com.alitalia.aem.common.messages.BaseRequest;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckinOffloadRequest extends BaseRequest {

    @SerializedName("pnrFlightInfo")
    @Expose
    private List<PnrFlightInfo> pnrFlightInfo = null;
    @SerializedName("airline")
    @Expose
    private String airline;
    @SerializedName("departureDate")
    @Expose
    private String departureDate;
    @SerializedName("flight")
    @Expose
    private String flight;
    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("seat")
    @Expose
    private String[] seat;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("market")
    @Expose
    private String market;
    @SerializedName("conversationID")
    @Expose
    private String conversationID;

    public List<PnrFlightInfo> getPnrFlightInfo() {
        return pnrFlightInfo;
    }

    public void setPnrFlightInfo(List<PnrFlightInfo> pnrFlightInfo) {
        this.pnrFlightInfo = pnrFlightInfo;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getFlight() {
        return flight;
    }

    public void setFlight(String flight) {
        this.flight = flight;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String[] getSeat() {
        return seat;
    }

    public void setSeat(String[] seat) {
        this.seat = seat;
    }
}
