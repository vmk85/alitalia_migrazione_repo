package com.alitalia.aem.common.data.home.mmb.ancillary;

import java.util.List;

public class MmbAncillaryPassengerData {

	private List<MmbAncillaryEticketData> etickets;
	private MmbAncillaryFrequentFlyerData frequentFlyerInfo;
	private Integer id;
	private String lastName;
	private String name;
	private Integer referenceNumber;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<MmbAncillaryEticketData> getEtickets() {
		return etickets;
	}

	public void setEtickets(List<MmbAncillaryEticketData> etickets) {
		this.etickets = etickets;
	}

	public MmbAncillaryFrequentFlyerData getFrequentFlyerInfo() {
		return frequentFlyerInfo;
	}

	public void setFrequentFlyerInfo(MmbAncillaryFrequentFlyerData frequentFlyerInfo) {
		this.frequentFlyerInfo = frequentFlyerInfo;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(Integer referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((referenceNumber == null) ? 0 : referenceNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbAncillaryPassengerData other = (MmbAncillaryPassengerData) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (referenceNumber == null) {
			if (other.referenceNumber != null)
				return false;
		} else if (!referenceNumber.equals(other.referenceNumber))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbAncillaryPassengerData [eTickets=" + etickets
				+ ", frequentFlyerInfo=" + frequentFlyerInfo + ", id=" + id
				+ ", lastName=" + lastName + ", name=" + name
				+ ", referenceNumber=" + referenceNumber + "]";
	}

}
