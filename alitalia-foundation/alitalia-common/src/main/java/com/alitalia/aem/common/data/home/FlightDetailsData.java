package com.alitalia.aem.common.data.home;

public class FlightDetailsData {

	private String aeromobile;
	private String arrivalTerminal;
	private String departureTerminal;
	private String distance;
	private String otherInformation;
	
	public FlightDetailsData() {
		super();
	}
	
	public FlightDetailsData(FlightDetailsData clone) {
		super();
		this.aeromobile = clone.aeromobile;
		this.arrivalTerminal = clone.arrivalTerminal;
		this.departureTerminal = clone.departureTerminal;
		this.distance = clone.distance;
		this.otherInformation = clone.otherInformation;
	}

	public String getAeromobile() {
		return aeromobile;
	}

	public void setAeromobile(String aeromobile) {
		this.aeromobile = aeromobile;
	}

	public String getArrivalTerminal() {
		return arrivalTerminal;
	}

	public void setArrivalTerminal(String arrivalTerminal) {
		this.arrivalTerminal = arrivalTerminal;
	}

	public String getDepartureTerminal() {
		return departureTerminal;
	}

	public void setDepartureTerminal(String departureTerminal) {
		this.departureTerminal = departureTerminal;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getOtherInformation() {
		return otherInformation;
	}

	public void setOtherInformation(String otherInformation) {
		this.otherInformation = otherInformation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((aeromobile == null) ? 0 : aeromobile.hashCode());
		result = prime * result
				+ ((arrivalTerminal == null) ? 0 : arrivalTerminal.hashCode());
		result = prime
				* result
				+ ((departureTerminal == null) ? 0 : departureTerminal
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FlightDetailsData other = (FlightDetailsData) obj;
		if (aeromobile == null) {
			if (other.aeromobile != null)
				return false;
		} else if (!aeromobile.equals(other.aeromobile))
			return false;
		if (arrivalTerminal == null) {
			if (other.arrivalTerminal != null)
				return false;
		} else if (!arrivalTerminal.equals(other.arrivalTerminal))
			return false;
		if (departureTerminal == null) {
			if (other.departureTerminal != null)
				return false;
		} else if (!departureTerminal.equals(other.departureTerminal))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FlightDetailsData [aeromobile=" + aeromobile
				+ ", arrivalTerminal=" + arrivalTerminal
				+ ", departureTerminal=" + departureTerminal + ", distance="
				+ distance + ", otherInformation=" + otherInformation + "]";
	}

}
