package com.alitalia.aem.common.data.home;

import java.util.List;

public class AdyenRetrievalStoredPaymentData {
	/*
  {
    "creationDate": "2018-09-06T14:24:40Z",
    "recurringDetailList": [
      {
        "additionalDataList": [
          {
            "key": "cardBin",
            "cardBin": "444433"
          }
        ],
        "card": {
          "cvc": null,
          "expiryMonth": "8",
          "expiryYear": "2018",
          "holderName": "Test Uno",
          "number": "1111",
          "type": "visa"
        },
        "alias": "P106674586198742",
        "aliasType": "Default",
        "creationDate": "2018-09-06T14:24:40Z",
        "firstPspReference": "8835362438795089",
        "recurringDetailReference": "8315362438809277",
        "recurringList": [
          {
            "contract": "RECURRING"
          }
        ],
        "paymentMethodVariant": "visacorporatecredit",
        "variant": "visa",
        "address": {
          "city": "Roma",
          "country": "IT",
          "postalCode": "00100",
          "province": "RM",
          "street": "via roma 100"
        },
        "bank": null,
        "shopperName": null
      }
    ],
    "lastKnownShopperEmail": "test@test.com",
    "shopperReference": "39fbe13b0ffd46ef9ee08d09ec577034",
    "conversationID": "987654321"
  }
	*/
	
	private String creationDate;
	private List<AdyenRecurringDetailData> recurringDetailList;
	private String lastKnownShopperEmail;
	private String shopperReference;
	private String conversationID;
	
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public List<AdyenRecurringDetailData> getRecurringDetailList() {
		return recurringDetailList;
	}
	public void setRecurringDetailList(List<AdyenRecurringDetailData> recurringDetailList) {
		this.recurringDetailList = recurringDetailList;
	}
	public String getLastKnownShopperEmail() {
		return lastKnownShopperEmail;
	}
	public void setLastKnownShopperEmail(String lastKnownShopperEmail) {
		this.lastKnownShopperEmail = lastKnownShopperEmail;
	}
	public String getShopperReference() {
		return shopperReference;
	}
	public void setShopperReference(String shopperReference) {
		this.shopperReference = shopperReference;
	}
	public String getConversationID() {
		return conversationID;
	}
	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result	+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result	+ ((recurringDetailList == null) ? 0 : recurringDetailList.hashCode());
		result = prime * result	+ ((lastKnownShopperEmail == null) ? 0 : lastKnownShopperEmail.hashCode());
		result = prime * result	+ ((shopperReference == null) ? 0 : shopperReference.hashCode());
		result = prime * result	+ ((conversationID == null) ? 0 : conversationID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdyenRetrievalStoredPaymentData other = (AdyenRetrievalStoredPaymentData) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (recurringDetailList == null) {
			if (other.recurringDetailList != null)
				return false;
		} else if (!recurringDetailList.equals(other.recurringDetailList))
			return false;
		if (lastKnownShopperEmail == null) {
			if (other.lastKnownShopperEmail != null)
				return false;
		} else if (!lastKnownShopperEmail.equals(other.lastKnownShopperEmail))
			return false;
		if (shopperReference == null) {
			if (other.shopperReference != null)
				return false;
		} else if (!shopperReference.equals(other.shopperReference))
			return false;
		if (shopperReference == null) {
			if (other.shopperReference != null)
				return false;
		} else if (!shopperReference.equals(other.shopperReference))
			return false;
		if (conversationID == null) {
			if (other.conversationID != null)
				return false;
		} else if (!conversationID.equals(other.conversationID))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "AdyenRetrievalStoredPayment ["
				+ "creationDate=" + creationDate
				+ ", recurringDetailList=" + recurringDetailList
				+ ", lastKnownShopperEmail=" + lastKnownShopperEmail
				+ ", shopperReference=" + shopperReference
				+ ", conversationID=" + conversationID
				+ "]";
	}
	
}