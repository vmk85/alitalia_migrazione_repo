package com.alitalia.aem.common.data.home.enumerations;

public enum MmbFaultExceptionMessageCodeEnum {

	NO_ERROR("noError"),
	ERROR_GET_CATALOG("errorGetCatalog"),
	ERROR_ADD_TO_CART("errorAddToCart");

	private final String value;

    MmbFaultExceptionMessageCodeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MmbFaultExceptionMessageCodeEnum fromValue(String v) {
        for (MmbFaultExceptionMessageCodeEnum c: MmbFaultExceptionMessageCodeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
