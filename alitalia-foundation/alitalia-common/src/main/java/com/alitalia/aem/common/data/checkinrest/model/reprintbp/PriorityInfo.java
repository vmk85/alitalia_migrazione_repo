
package com.alitalia.aem.common.data.checkinrest.model.reprintbp;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PriorityInfo {

    @SerializedName("priorityCode")
    @Expose
    private String priorityCode;
    @SerializedName("upgradePriorityCode")
    @Expose
    private String upgradePriorityCode;
    @SerializedName("skyPriorityText")
    @Expose
    private String skyPriorityText;
    @SerializedName("priorityText")
    @Expose
    private String priorityText;
    @SerializedName("priorityAaccessText")
    @Expose
    private String priorityAaccessText;
    @SerializedName("priorityBoardingText")
    @Expose
    private List<String> priorityBoardingText = null;

    public String getPriorityCode() {
        return priorityCode;
    }

    public void setPriorityCode(String priorityCode) {
        this.priorityCode = priorityCode;
    }

    public String getUpgradePriorityCode() {
        return upgradePriorityCode;
    }

    public void setUpgradePriorityCode(String upgradePriorityCode) {
        this.upgradePriorityCode = upgradePriorityCode;
    }

    public String getSkyPriorityText() {
        return skyPriorityText;
    }

    public void setSkyPriorityText(String skyPriorityText) {
        this.skyPriorityText = skyPriorityText;
    }

    public String getPriorityText() {
        return priorityText;
    }

    public void setPriorityText(String priorityText) {
        this.priorityText = priorityText;
    }

    public String getPriorityAaccessText() {
        return priorityAaccessText;
    }

    public void setPriorityAaccessText(String priorityAaccessText) {
        this.priorityAaccessText = priorityAaccessText;
    }

    public List<String> getPriorityBoardingText() {
        return priorityBoardingText;
    }

    public void setPriorityBoardingText(List<String> priorityBoardingText) {
        this.priorityBoardingText = priorityBoardingText;
    }

}
