package com.alitalia.aem.common.data.home.checkin;

import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;

public class CheckinKeyValueOfCompartimentalClassData {
	
	private MmbCompartimentalClassEnum key;
	private Integer value;
	
	public MmbCompartimentalClassEnum getKey() {
		return key;
	}
	public void setKey(MmbCompartimentalClassEnum key) {
		this.key = key;
	}
	public Integer getValue() {
		return value;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinKeyValueOfCompartimentalClassData other = (CheckinKeyValueOfCompartimentalClassData) obj;
		if (key != other.key)
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CheckinKeyValueOfCompartimentalClassData [key=" + key
				+ ", value=" + value + "]";
	}

}
