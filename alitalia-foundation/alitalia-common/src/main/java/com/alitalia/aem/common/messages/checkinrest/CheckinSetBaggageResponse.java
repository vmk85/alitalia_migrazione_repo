package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.setbaggage.response.Baggage;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinSetBaggageResponse extends BaseResponse {
	
	private Baggage baggage;

	public CheckinSetBaggageResponse() {}

	public CheckinSetBaggageResponse(String tid, String sid) {
		super(tid, sid);
	}

	public Baggage getBaggage() {
		return baggage;
	}

	public void setBaggage(Baggage baggage) {
		this.baggage = baggage;
	}
}
