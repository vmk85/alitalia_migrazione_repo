package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class RetrieveGeoNearestAirportByCoordsResponse extends BaseResponse {

    private String returnCode;
    private String returnDescription;
    private String airportCode;
    private String airportDesc;
    private String airportDistanceKM;
    private String airportLatitude;
    private String airportLongitude;
    private String cityCode;
    private String cityDesc;
    private String countryCode;
	
    public String getReturnCode() {
		return returnCode;
	}
	
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
	
	public String getReturnDescription() {
		return returnDescription;
	}
	
	public void setReturnDescription(String returnDescription) {
		this.returnDescription = returnDescription;
	}
	
	public String getAirportCode() {
		return airportCode;
	}
	
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}
	
	public String getAirportDesc() {
		return airportDesc;
	}
	
	public void setAirportDesc(String airportDesc) {
		this.airportDesc = airportDesc;
	}
	
	public String getAirportDistanceKM() {
		return airportDistanceKM;
	}
	
	public void setAirportDistanceKM(String airportDistanceKM) {
		this.airportDistanceKM = airportDistanceKM;
	}
	
	public String getAirportLatitude() {
		return airportLatitude;
	}
	
	public void setAirportLatitude(String airportLatitude) {
		this.airportLatitude = airportLatitude;
	}
	
	public String getAirportLongitude() {
		return airportLongitude;
	}
	
	public void setAirportLongitude(String airportLongitude) {
		this.airportLongitude = airportLongitude;
	}
	
	public String getCityCode() {
		return cityCode;
	}
	
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	
	public String getCityDesc() {
		return cityDesc;
	}
	
	public void setCityDesc(String cityDesc) {
		this.cityDesc = cityDesc;
	}
	
	public String getCountryCode() {
		return countryCode;
	}
	
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((airportCode == null) ? 0 : airportCode.hashCode());
		result = prime * result + ((airportDesc == null) ? 0 : airportDesc.hashCode());
		result = prime * result + ((airportDistanceKM == null) ? 0 : airportDistanceKM.hashCode());
		result = prime * result + ((airportLatitude == null) ? 0 : airportLatitude.hashCode());
		result = prime * result + ((airportLongitude == null) ? 0 : airportLongitude.hashCode());
		result = prime * result + ((cityCode == null) ? 0 : cityCode.hashCode());
		result = prime * result + ((cityDesc == null) ? 0 : cityDesc.hashCode());
		result = prime * result + ((countryCode == null) ? 0 : countryCode.hashCode());
		result = prime * result + ((returnCode == null) ? 0 : returnCode.hashCode());
		result = prime * result + ((returnDescription == null) ? 0 : returnDescription.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveGeoNearestAirportByCoordsResponse other = (RetrieveGeoNearestAirportByCoordsResponse) obj;
		if (airportCode == null) {
			if (other.airportCode != null)
				return false;
		} else if (!airportCode.equals(other.airportCode))
			return false;
		if (airportDesc == null) {
			if (other.airportDesc != null)
				return false;
		} else if (!airportDesc.equals(other.airportDesc))
			return false;
		if (airportDistanceKM == null) {
			if (other.airportDistanceKM != null)
				return false;
		} else if (!airportDistanceKM.equals(other.airportDistanceKM))
			return false;
		if (airportLatitude == null) {
			if (other.airportLatitude != null)
				return false;
		} else if (!airportLatitude.equals(other.airportLatitude))
			return false;
		if (airportLongitude == null) {
			if (other.airportLongitude != null)
				return false;
		} else if (!airportLongitude.equals(other.airportLongitude))
			return false;
		if (cityCode == null) {
			if (other.cityCode != null)
				return false;
		} else if (!cityCode.equals(other.cityCode))
			return false;
		if (cityDesc == null) {
			if (other.cityDesc != null)
				return false;
		} else if (!cityDesc.equals(other.cityDesc))
			return false;
		if (countryCode == null) {
			if (other.countryCode != null)
				return false;
		} else if (!countryCode.equals(other.countryCode))
			return false;
		if (returnCode == null) {
			if (other.returnCode != null)
				return false;
		} else if (!returnCode.equals(other.returnCode))
			return false;
		if (returnDescription == null) {
			if (other.returnDescription != null)
				return false;
		} else if (!returnDescription.equals(other.returnDescription))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveGeoNearestAirportByCoordsResponse [returnCode=" + returnCode + ", returnDescription="
				+ returnDescription + ", airportCode=" + airportCode + ", airportDesc=" + airportDesc
				+ ", airportDistanceKM=" + airportDistanceKM + ", airportLatitude=" + airportLatitude
				+ ", airportLongitude=" + airportLongitude + ", cityCode=" + cityCode + ", cityDesc=" + cityDesc
				+ ", countryCode=" + countryCode + "]";
	}
	
}