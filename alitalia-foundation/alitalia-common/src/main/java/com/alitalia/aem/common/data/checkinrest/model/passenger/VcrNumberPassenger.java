
package com.alitalia.aem.common.data.checkinrest.model.passenger;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VcrNumberPassenger {

    @SerializedName("couponNumber")
    @Expose
    private String couponNumber;
    @SerializedName("validatingCarrier")
    @Expose
    private String validatingCarrier;
    @SerializedName("checkDigit")
    @Expose
    private String checkDigit;
    @SerializedName("value")
    @Expose
    private String value;

    public String getCouponNumber() {
        return couponNumber;
    }

    public void setCouponNumber(String couponNumber) {
        this.couponNumber = couponNumber;
    }

    public String getValidatingCarrier() {
        return validatingCarrier;
    }

    public void setValidatingCarrier(String validatingCarrier) {
        this.validatingCarrier = validatingCarrier;
    }

    public String getCheckDigit() {
        return checkDigit;
    }

    public void setCheckDigit(String checkDigit) {
        this.checkDigit = checkDigit;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
