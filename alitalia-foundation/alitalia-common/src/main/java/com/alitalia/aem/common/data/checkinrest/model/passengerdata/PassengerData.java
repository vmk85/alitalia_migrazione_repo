
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PassengerData {

    @SerializedName("passengerDataResponseList")
    @Expose
    private PassengerDataResponseList passengerDataResponseList;
    @SerializedName("ancillaryItinInfoList")
    @Expose
    private List<AncillaryItinInfoList> ancillaryItinInfoList = null;
    @SerializedName("result")
    @Expose
    private Result result;
    @SerializedName("timeStamp")
    @Expose
    private String timeStamp;
    @SerializedName("timeStampSpecified")
    @Expose
    private Boolean timeStampSpecified;
    @SerializedName("trackingId")
    @Expose
    private String trackingId;
    @SerializedName("version")
    @Expose
    private String version;

    public PassengerDataResponseList getPassengerDataResponseList() {
        return passengerDataResponseList;
    }

    public void setPassengerDataResponseList(PassengerDataResponseList passengerDataResponseList) {
        this.passengerDataResponseList = passengerDataResponseList;
    }

    public List<AncillaryItinInfoList> getAncillaryItinInfoList() {
        return ancillaryItinInfoList;
    }

    public void setAncillaryItinInfoList(List<AncillaryItinInfoList> ancillaryItinInfoList) {
        this.ancillaryItinInfoList = ancillaryItinInfoList;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Boolean getTimeStampSpecified() {
        return timeStampSpecified;
    }

    public void setTimeStampSpecified(Boolean timeStampSpecified) {
        this.timeStampSpecified = timeStampSpecified;
    }

    public String getTrackingId() {
        return trackingId;
    }

    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

}
