
package com.alitalia.aem.common.data.checkinrest.model.updatepassengerdetails.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdatePassengerDetailsReq {

    @SerializedName("airline")
    @Expose
    private String airline;
    @SerializedName("flight")
    @Expose
    private String flight;
    @SerializedName("departureDate")
    @Expose
    private String departureDate;
    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("bookingClass")
    @Expose
    private String bookingClass;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("passengerID")
    @Expose
    private String passengerID;
    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("airlineFQTV")
    @Expose
    private String airlineFQTV;
    @SerializedName("opFQTV")
    @Expose
    private String opFQTV;

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getFlight() {
        return flight;
    }

    public void setFlight(String flight) {
        this.flight = flight;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getBookingClass() {
        return bookingClass;
    }

    public void setBookingClass(String bookingClass) {
        this.bookingClass = bookingClass;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPassengerID() {
        return passengerID;
    }

    public void setPassengerID(String passengerID) {
        this.passengerID = passengerID;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAirlineFQTV() {
        return airlineFQTV;
    }

    public void setAirlineFQTV(String airlineFQTV) {
        this.airlineFQTV = airlineFQTV;
    }

    public String getOpFQTV() {
        return opFQTV;
    }

    public void setOpFQTV(String opFQTV) {
        this.opFQTV = opFQTV;
    }

}
