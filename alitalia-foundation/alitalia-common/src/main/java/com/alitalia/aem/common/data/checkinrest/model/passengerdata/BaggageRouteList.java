
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaggageRouteList {

    @SerializedName("segmentID")
    @Expose
    private Long segmentID;
    @SerializedName("airline")
    @Expose
    private String airline;
    @SerializedName("flight")
    @Expose
    private String flight;
    @SerializedName("bookingClass")
    @Expose
    private String bookingClass;
    @SerializedName("operatingAirline")
    @Expose
    private String operatingAirline;
    @SerializedName("operatingFlight")
    @Expose
    private String operatingFlight;
    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("departureDate")
    @Expose
    private String departureDate;
    @SerializedName("departureTime")
    @Expose
    private String departureTime;
    @SerializedName("arrivalDate")
    @Expose
    private String arrivalDate;
    @SerializedName("arrivalTime")
    @Expose
    private String arrivalTime;
    @SerializedName("segmentStatus")
    @Expose
    private String segmentStatus;
    @SerializedName("passengerID")
    @Expose
    private String passengerID;
    @SerializedName("bagEmbargo")
    @Expose
    private Long bagEmbargo;
    @SerializedName("bagEmbargoSpecified")
    @Expose
    private Boolean bagEmbargoSpecified;
    @SerializedName("petRestriction")
    @Expose
    private String petRestriction;
    @SerializedName("freeText")
    @Expose
    private String freeText;
    @SerializedName("baggageRestrictionLimit")
    @Expose
    private String baggageRestrictionLimit;
    @SerializedName("lateCheckin")
    @Expose
    private String lateCheckin;
    @SerializedName("shortCheckinReason")
    @Expose
    private String shortCheckinReason;
    @SerializedName("homePrintedBagTag")
    @Expose
    private HomePrintedBagTag homePrintedBagTag;

    public Long getSegmentID() {
        return segmentID;
    }

    public void setSegmentID(Long segmentID) {
        this.segmentID = segmentID;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getFlight() {
        return flight;
    }

    public void setFlight(String flight) {
        this.flight = flight;
    }

    public String getBookingClass() {
        return bookingClass;
    }

    public void setBookingClass(String bookingClass) {
        this.bookingClass = bookingClass;
    }

    public String getOperatingAirline() {
        return operatingAirline;
    }

    public void setOperatingAirline(String operatingAirline) {
        this.operatingAirline = operatingAirline;
    }

    public String getOperatingFlight() {
        return operatingFlight;
    }

    public void setOperatingFlight(String operatingFlight) {
        this.operatingFlight = operatingFlight;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getSegmentStatus() {
        return segmentStatus;
    }

    public void setSegmentStatus(String segmentStatus) {
        this.segmentStatus = segmentStatus;
    }

    public String getPassengerID() {
        return passengerID;
    }

    public void setPassengerID(String passengerID) {
        this.passengerID = passengerID;
    }

    public Long getBagEmbargo() {
        return bagEmbargo;
    }

    public void setBagEmbargo(Long bagEmbargo) {
        this.bagEmbargo = bagEmbargo;
    }

    public Boolean getBagEmbargoSpecified() {
        return bagEmbargoSpecified;
    }

    public void setBagEmbargoSpecified(Boolean bagEmbargoSpecified) {
        this.bagEmbargoSpecified = bagEmbargoSpecified;
    }

    public String getPetRestriction() {
        return petRestriction;
    }

    public void setPetRestriction(String petRestriction) {
        this.petRestriction = petRestriction;
    }

    public String getFreeText() {
        return freeText;
    }

    public void setFreeText(String freeText) {
        this.freeText = freeText;
    }

    public String getBaggageRestrictionLimit() {
        return baggageRestrictionLimit;
    }

    public void setBaggageRestrictionLimit(String baggageRestrictionLimit) {
        this.baggageRestrictionLimit = baggageRestrictionLimit;
    }

    public String getLateCheckin() {
        return lateCheckin;
    }

    public void setLateCheckin(String lateCheckin) {
        this.lateCheckin = lateCheckin;
    }

    public String getShortCheckinReason() {
        return shortCheckinReason;
    }

    public void setShortCheckinReason(String shortCheckinReason) {
        this.shortCheckinReason = shortCheckinReason;
    }

    public HomePrintedBagTag getHomePrintedBagTag() {
        return homePrintedBagTag;
    }

    public void setHomePrintedBagTag(HomePrintedBagTag homePrintedBagTag) {
        this.homePrintedBagTag = homePrintedBagTag;
    }

}
