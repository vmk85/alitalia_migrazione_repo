package com.alitalia.aem.common.data.checkinrest.model.fasttrack.request;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FlightFastTrack {

    @SerializedName("flight")
    @Expose
    private String flight;
    @SerializedName("passengers")
    @Expose
    private List<Passenger> passengers = null;

    public String getFlight() {
        return flight;
    }

    public void setFlight(String flight) {
        this.flight = flight;
    }

    public List<Passenger> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<Passenger> passengers) {
        this.passengers = passengers;
    }

}
