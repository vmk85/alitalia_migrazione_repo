package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.ItineraryModelData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetrieveItinerariesResponse extends BaseResponse {

	private List<ItineraryModelData> itineraries;

	public List<ItineraryModelData> getItineraries() {
		return itineraries;
	}

	public void setItineraries(List<ItineraryModelData> itineraries) {
		this.itineraries = itineraries;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((itineraries == null) ? 0 : itineraries.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveItinerariesResponse other = (RetrieveItinerariesResponse) obj;
		if (itineraries == null) {
			if (other.itineraries != null)
				return false;
		} else if (!itineraries.equals(other.itineraries))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveItinerariesResponse [itineraries=" + itineraries + "]";
	}
}