package com.alitalia.aem.common.messages.home;

public class RegisterBillingStatisticRequest extends RegisterStatisticsRequest {

	protected String otherParameters;
    protected String parameter1;
    protected String parameter2;
    protected String parameter3;
    protected String parameter4;
    protected String parameter5;
    
    public RegisterBillingStatisticRequest(String tid, String sid) {
		super(tid, sid);
	}
    
	public String getOtherParameters() {
		return otherParameters;
	}
	public void setOtherParameters(String otherParameters) {
		this.otherParameters = otherParameters;
	}
	public String getParameter1() {
		return parameter1;
	}
	public void setParameter1(String parameter1) {
		this.parameter1 = parameter1;
	}
	public String getParameter2() {
		return parameter2;
	}
	public void setParameter2(String parameter2) {
		this.parameter2 = parameter2;
	}
	public String getParameter3() {
		return parameter3;
	}
	public void setParameter3(String parameter3) {
		this.parameter3 = parameter3;
	}
	public String getParameter4() {
		return parameter4;
	}
	public void setParameter4(String parameter4) {
		this.parameter4 = parameter4;
	}
	public String getParameter5() {
		return parameter5;
	}
	public void setParameter5(String parameter5) {
		this.parameter5 = parameter5;
	}
	
	@Override
	public String toString() {
		return "RegisterBillingStatisticRequest [otherParameters="
				+ otherParameters + ", parameter1=" + parameter1
				+ ", parameter2=" + parameter2 + ", parameter3=" + parameter3
				+ ", parameter4=" + parameter4 + ", parameter5=" + parameter5
				+ ", getOtherParameters()=" + getOtherParameters()
				+ ", getParameter1()=" + getParameter1() + ", getParameter2()="
				+ getParameter2() + ", getParameter3()=" + getParameter3()
				+ ", getParameter4()=" + getParameter4() + ", getParameter5()="
				+ getParameter5() + ", getClientIP()=" + getClientIP()
				+ ", getErrorDescr()=" + getErrorDescr() + ", getSessionId()="
				+ getSessionId() + ", getSiteCode()=" + getSiteCode()
				+ ", isSuccess()=" + isSuccess() + ", getType()=" + getType()
				+ ", getUserId()=" + getUserId() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + ", getTid()=" + getTid()
				+ ", getSid()=" + getSid() + ", getClass()=" + getClass() + "]";
	}
}