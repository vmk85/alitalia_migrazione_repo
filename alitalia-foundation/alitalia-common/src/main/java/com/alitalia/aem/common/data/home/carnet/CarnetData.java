package com.alitalia.aem.common.data.home.carnet;

import java.math.BigDecimal;

public class CarnetData {

	private String changeDescription;
	private String code;
	private String codeEMD;
	private String currency;
	private BigDecimal incrementPrice;
	private String prenotationClass;
	private String prenotationClassDescription;
	private BigDecimal price;
	private Short quantity;
	private String redemption;
	private Short validity;
	
	public String getChangeDescription() {
		return changeDescription;
	}
	public void setChangeDescription(String changeDescription) {
		this.changeDescription = changeDescription;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCodeEMD() {
		return codeEMD;
	}
	public void setCodeEMD(String codeEMD) {
		this.codeEMD = codeEMD;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public BigDecimal getIncrementPrice() {
		return incrementPrice;
	}
	public void setIncrementPrice(BigDecimal incrementPrice) {
		this.incrementPrice = incrementPrice;
	}
	public String getPrenotationClass() {
		return prenotationClass;
	}
	public void setPrenotationClass(String prenotationClass) {
		this.prenotationClass = prenotationClass;
	}
	public String getPrenotationClassDescription() {
		return prenotationClassDescription;
	}
	public void setPrenotationClassDescription(String prenotationClassDescription) {
		this.prenotationClassDescription = prenotationClassDescription;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public Short getQuantity() {
		return quantity;
	}
	public void setQuantity(Short quantity) {
		this.quantity = quantity;
	}
	public String getRedemption() {
		return redemption;
	}
	public void setRedemption(String redemption) {
		this.redemption = redemption;
	}
	public Short getValidity() {
		return validity;
	}
	public void setValidity(Short validity) {
		this.validity = validity;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((changeDescription == null) ? 0 : changeDescription
						.hashCode());
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((codeEMD == null) ? 0 : codeEMD.hashCode());
		result = prime * result
				+ ((currency == null) ? 0 : currency.hashCode());
		result = prime * result
				+ ((incrementPrice == null) ? 0 : incrementPrice.hashCode());
		result = prime
				* result
				+ ((prenotationClass == null) ? 0 : prenotationClass.hashCode());
		result = prime
				* result
				+ ((prenotationClassDescription == null) ? 0
						: prenotationClassDescription.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result
				+ ((quantity == null) ? 0 : quantity.hashCode());
		result = prime * result
				+ ((redemption == null) ? 0 : redemption.hashCode());
		result = prime * result
				+ ((validity == null) ? 0 : validity.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarnetData other = (CarnetData) obj;
		if (changeDescription == null) {
			if (other.changeDescription != null)
				return false;
		} else if (!changeDescription.equals(other.changeDescription))
			return false;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (codeEMD == null) {
			if (other.codeEMD != null)
				return false;
		} else if (!codeEMD.equals(other.codeEMD))
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (incrementPrice == null) {
			if (other.incrementPrice != null)
				return false;
		} else if (!incrementPrice.equals(other.incrementPrice))
			return false;
		if (prenotationClass == null) {
			if (other.prenotationClass != null)
				return false;
		} else if (!prenotationClass.equals(other.prenotationClass))
			return false;
		if (prenotationClassDescription == null) {
			if (other.prenotationClassDescription != null)
				return false;
		} else if (!prenotationClassDescription
				.equals(other.prenotationClassDescription))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (quantity == null) {
			if (other.quantity != null)
				return false;
		} else if (!quantity.equals(other.quantity))
			return false;
		if (redemption == null) {
			if (other.redemption != null)
				return false;
		} else if (!redemption.equals(other.redemption))
			return false;
		if (validity == null) {
			if (other.validity != null)
				return false;
		} else if (!validity.equals(other.validity))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "CarnetData [changeDescription=" + changeDescription + ", code="
				+ code + ", codeEMD=" + codeEMD + ", currency=" + currency
				+ ", incrementPrice=" + incrementPrice + ", prenotationClass="
				+ prenotationClass + ", prenotationClassDescription="
				+ prenotationClassDescription + ", price=" + price
				+ ", quantity=" + quantity + ", redemption=" + redemption
				+ ", validity=" + validity + "]";
	}
}