
package com.alitalia.aem.common.data.checkinrest.model.reprintbp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BagTagInfo {

    @SerializedName("bagTagNumber")
    @Expose
    private String bagTagNumber;
    @SerializedName("weightAndSize")
    @Expose
    private WeightAndSize weightAndSize;
    @SerializedName("bagStatus")
    @Expose
    private BagStatus bagStatus;

    public String getBagTagNumber() {
        return bagTagNumber;
    }

    public void setBagTagNumber(String bagTagNumber) {
        this.bagTagNumber = bagTagNumber;
    }

    public WeightAndSize getWeightAndSize() {
        return weightAndSize;
    }

    public void setWeightAndSize(WeightAndSize weightAndSize) {
        this.weightAndSize = weightAndSize;
    }

    public BagStatus getBagStatus() {
        return bagStatus;
    }

    public void setBagStatus(BagStatus bagStatus) {
        this.bagStatus = bagStatus;
    }

}
