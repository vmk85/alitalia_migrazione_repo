
package com.alitalia.aem.common.data.checkinrest.model.flightdetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cockpit {

    @SerializedName("inUse")
    @Expose
    private Boolean inUse;
    @SerializedName("inUseSpecified")
    @Expose
    private Boolean inUseSpecified;
    @SerializedName("value")
    @Expose
    private Long value;

    public Boolean getInUse() {
        return inUse;
    }

    public void setInUse(Boolean inUse) {
        this.inUse = inUse;
    }

    public Boolean getInUseSpecified() {
        return inUseSpecified;
    }

    public void setInUseSpecified(Boolean inUseSpecified) {
        this.inUseSpecified = inUseSpecified;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

}
