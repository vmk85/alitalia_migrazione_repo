package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.getcart.Cart;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinGetCartResponse extends BaseResponse{

	public Cart cart;
	
	public Cart getCart() {
		return cart;
	}

	public void setCart(Cart cart) {
		this.cart = cart;
	}

	public CheckinGetCartResponse(){}
	
	public CheckinGetCartResponse(String tid, String sid) {
		super(tid, sid);
	}
	
}
