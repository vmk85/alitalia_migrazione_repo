
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VcrInfoList {

    @SerializedName("vcrNumber")
    @Expose
    private VcrNumber_ vcrNumber;
    @SerializedName("createDate")
    @Expose
    private String createDate;
    @SerializedName("fareBasisCode")
    @Expose
    private String fareBasisCode;
    @SerializedName("fareCalculation")
    @Expose
    private String fareCalculation;
    @SerializedName("bagAllowance")
    @Expose
    private BagAllowance bagAllowance;

    public VcrNumber_ getVcrNumber() {
        return vcrNumber;
    }

    public void setVcrNumber(VcrNumber_ vcrNumber) {
        this.vcrNumber = vcrNumber;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getFareBasisCode() {
        return fareBasisCode;
    }

    public void setFareBasisCode(String fareBasisCode) {
        this.fareBasisCode = fareBasisCode;
    }

    public String getFareCalculation() {
        return fareCalculation;
    }

    public void setFareCalculation(String fareCalculation) {
        this.fareCalculation = fareCalculation;
    }

    public BagAllowance getBagAllowance() {
        return bagAllowance;
    }

    public void setBagAllowance(BagAllowance bagAllowance) {
        this.bagAllowance = bagAllowance;
    }

}
