package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PnrListInfoByTicketSearch;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinTicketSearchResponse extends BaseResponse {

	private PnrListInfoByTicketSearch pnrData = null;

	public CheckinTicketSearchResponse() {}

	public CheckinTicketSearchResponse(String tid, String sid) {
		super(tid, sid);
	}

	public PnrListInfoByTicketSearch getPnrData() {
		return pnrData;
	}

	public void setPnrData(PnrListInfoByTicketSearch pnrData) {
		this.pnrData = pnrData;
	}

}
