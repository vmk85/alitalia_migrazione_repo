package com.alitalia.aem.common.data.home;

import java.util.Map;

public class PaymentComunicationFindomesticData extends
		PaymentComunicationBaseData {
	
	private Boolean isPost;
	private Map<String, String> requestData;
	private String urlCallBack;
	private String urlRedirect;
	public Boolean getIsPost() {
		return isPost;
	}
	public void setIsPost(Boolean isPost) {
		this.isPost = isPost;
	}
	public Map<String, String> getRequestData() {
		return requestData;
	}
	public void setRequestData(Map<String, String> requestData) {
		this.requestData = requestData;
	}
	public String getUrlCallBack() {
		return urlCallBack;
	}
	public void setUrlCallBack(String urlCallBack) {
		this.urlCallBack = urlCallBack;
	}
	public String getUrlRedirect() {
		return urlRedirect;
	}
	public void setUrlRedirect(String urlRedirect) {
		this.urlRedirect = urlRedirect;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((isPost == null) ? 0 : isPost.hashCode());
		result = prime * result
				+ ((requestData == null) ? 0 : requestData.hashCode());
		result = prime * result
				+ ((urlCallBack == null) ? 0 : urlCallBack.hashCode());
		result = prime * result
				+ ((urlRedirect == null) ? 0 : urlRedirect.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentComunicationFindomesticData other = (PaymentComunicationFindomesticData) obj;
		if (isPost == null) {
			if (other.isPost != null)
				return false;
		} else if (!isPost.equals(other.isPost))
			return false;
		if (requestData == null) {
			if (other.requestData != null)
				return false;
		} else if (!requestData.equals(other.requestData))
			return false;
		if (urlCallBack == null) {
			if (other.urlCallBack != null)
				return false;
		} else if (!urlCallBack.equals(other.urlCallBack))
			return false;
		if (urlRedirect == null) {
			if (other.urlRedirect != null)
				return false;
		} else if (!urlRedirect.equals(other.urlRedirect))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "PaymentComunicationFindomesticData [isPost=" + isPost
				+ ", requestData=" + requestData + ", urlCallBack="
				+ urlCallBack + ", urlRedirect=" + urlRedirect + "]";
	}
	
}
