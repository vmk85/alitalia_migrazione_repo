
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AncillaryInfo {

    @SerializedName("atpcoPassengerType")
    @Expose
    private String atpcoPassengerType;
    @SerializedName("portion")
    @Expose
    private Boolean portion;
    @SerializedName("portionSpecified")
    @Expose
    private Boolean portionSpecified;
    @SerializedName("hardMatch")
    @Expose
    private Boolean hardMatch;
    @SerializedName("hardMatchSpecified")
    @Expose
    private Boolean hardMatchSpecified;
    @SerializedName("groupCode")
    @Expose
    private String groupCode;
    @SerializedName("subCode")
    @Expose
    private String subCode;
    @SerializedName("descriptions")
    @Expose
    private Descriptions descriptions;
    @SerializedName("subGroup")
    @Expose
    private SubGroup subGroup;
    @SerializedName("ancillaryServiceType")
    @Expose
    private String ancillaryServiceType;
    @SerializedName("rficCode")
    @Expose
    private String rficCode;
    @SerializedName("baggageWeightAndSize")
    @Expose
    private BaggageWeightAndSize baggageWeightAndSize;
    @SerializedName("baggageData")
    @Expose
    private BaggageData baggageData;
    @SerializedName("actionCode")
    @Expose
    private String actionCode;
    @SerializedName("vendor")
    @Expose
    private String vendor;
    @SerializedName("feeIndicator")
    @Expose
    private String feeIndicator;
    @SerializedName("chargeIndicator")
    @Expose
    private String chargeIndicator;
    @SerializedName("taxesIncluded")
    @Expose
    private Boolean taxesIncluded;
    @SerializedName("taxesIncludedSpecified")
    @Expose
    private Boolean taxesIncludedSpecified;
    @SerializedName("emdType")
    @Expose
    private String emdType;
    @SerializedName("refund")
    @Expose
    private String refund;
    @SerializedName("baggageAllowance")
    @Expose
    private BaggageAllowance baggageAllowance;
    @SerializedName("priceDetails")
    @Expose
    private PriceDetails_ priceDetails;
    @SerializedName("ancillaryFreeTextInfoList")
    @Expose
    private List<AncillaryFreeTextInfoList> ancillaryFreeTextInfoList = null;
    @SerializedName("passengerID")
    @Expose
    private String passengerID;
    @SerializedName("bagID")
    @Expose
    private Long bagID;
    @SerializedName("bagIDSpecified")
    @Expose
    private Boolean bagIDSpecified;
    @SerializedName("itemID")
    @Expose
    private String itemID;

    public String getAtpcoPassengerType() {
        return atpcoPassengerType;
    }

    public void setAtpcoPassengerType(String atpcoPassengerType) {
        this.atpcoPassengerType = atpcoPassengerType;
    }

    public Boolean getPortion() {
        return portion;
    }

    public void setPortion(Boolean portion) {
        this.portion = portion;
    }

    public Boolean getPortionSpecified() {
        return portionSpecified;
    }

    public void setPortionSpecified(Boolean portionSpecified) {
        this.portionSpecified = portionSpecified;
    }

    public Boolean getHardMatch() {
        return hardMatch;
    }

    public void setHardMatch(Boolean hardMatch) {
        this.hardMatch = hardMatch;
    }

    public Boolean getHardMatchSpecified() {
        return hardMatchSpecified;
    }

    public void setHardMatchSpecified(Boolean hardMatchSpecified) {
        this.hardMatchSpecified = hardMatchSpecified;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getSubCode() {
        return subCode;
    }

    public void setSubCode(String subCode) {
        this.subCode = subCode;
    }

    public Descriptions getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(Descriptions descriptions) {
        this.descriptions = descriptions;
    }

    public SubGroup getSubGroup() {
        return subGroup;
    }

    public void setSubGroup(SubGroup subGroup) {
        this.subGroup = subGroup;
    }

    public String getAncillaryServiceType() {
        return ancillaryServiceType;
    }

    public void setAncillaryServiceType(String ancillaryServiceType) {
        this.ancillaryServiceType = ancillaryServiceType;
    }

    public String getRficCode() {
        return rficCode;
    }

    public void setRficCode(String rficCode) {
        this.rficCode = rficCode;
    }

    public BaggageWeightAndSize getBaggageWeightAndSize() {
        return baggageWeightAndSize;
    }

    public void setBaggageWeightAndSize(BaggageWeightAndSize baggageWeightAndSize) {
        this.baggageWeightAndSize = baggageWeightAndSize;
    }

    public BaggageData getBaggageData() {
        return baggageData;
    }

    public void setBaggageData(BaggageData baggageData) {
        this.baggageData = baggageData;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getFeeIndicator() {
        return feeIndicator;
    }

    public void setFeeIndicator(String feeIndicator) {
        this.feeIndicator = feeIndicator;
    }

    public String getChargeIndicator() {
        return chargeIndicator;
    }

    public void setChargeIndicator(String chargeIndicator) {
        this.chargeIndicator = chargeIndicator;
    }

    public Boolean getTaxesIncluded() {
        return taxesIncluded;
    }

    public void setTaxesIncluded(Boolean taxesIncluded) {
        this.taxesIncluded = taxesIncluded;
    }

    public Boolean getTaxesIncludedSpecified() {
        return taxesIncludedSpecified;
    }

    public void setTaxesIncludedSpecified(Boolean taxesIncludedSpecified) {
        this.taxesIncludedSpecified = taxesIncludedSpecified;
    }

    public String getEmdType() {
        return emdType;
    }

    public void setEmdType(String emdType) {
        this.emdType = emdType;
    }

    public String getRefund() {
        return refund;
    }

    public void setRefund(String refund) {
        this.refund = refund;
    }

    public BaggageAllowance getBaggageAllowance() {
        return baggageAllowance;
    }

    public void setBaggageAllowance(BaggageAllowance baggageAllowance) {
        this.baggageAllowance = baggageAllowance;
    }

    public PriceDetails_ getPriceDetails() {
        return priceDetails;
    }

    public void setPriceDetails(PriceDetails_ priceDetails) {
        this.priceDetails = priceDetails;
    }

    public List<AncillaryFreeTextInfoList> getAncillaryFreeTextInfoList() {
        return ancillaryFreeTextInfoList;
    }

    public void setAncillaryFreeTextInfoList(List<AncillaryFreeTextInfoList> ancillaryFreeTextInfoList) {
        this.ancillaryFreeTextInfoList = ancillaryFreeTextInfoList;
    }

    public String getPassengerID() {
        return passengerID;
    }

    public void setPassengerID(String passengerID) {
        this.passengerID = passengerID;
    }

    public Long getBagID() {
        return bagID;
    }

    public void setBagID(Long bagID) {
        this.bagID = bagID;
    }

    public Boolean getBagIDSpecified() {
        return bagIDSpecified;
    }

    public void setBagIDSpecified(Boolean bagIDSpecified) {
        this.bagIDSpecified = bagIDSpecified;
    }

    public String getItemID() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }

}
