package com.alitalia.aem.common.data.checkinrest.model.pnrinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Error {

	@SerializedName("isError")
	@Expose
	private Boolean isError;
	@SerializedName("errorMessage")
	@Expose
	private String errorMessage;
	@SerializedName("sabreStatusCode")
	@Expose
	private String sabreStatusCode;
	@SerializedName("conversationID")
	@Expose
	private String conversationID;
	@SerializedName("callerMethod")
	@Expose
	private String callerMethod;

	public String getCallerMethod() {
		return callerMethod;
	}

	public void setCallerMethod(String callerMethod) {
		this.callerMethod = callerMethod;
	}

	public Boolean getIsError() {
		return isError;
	}

	public void setIsError(Boolean isError) {
		this.isError = isError;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Object getSabreStatusCode() {
		return sabreStatusCode;
	}

	public void setSabreStatusCode(String sabreStatusCode) {
		this.sabreStatusCode = sabreStatusCode;
	}

	public Object getConversationID() {
		return conversationID;
	}

	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}

}
