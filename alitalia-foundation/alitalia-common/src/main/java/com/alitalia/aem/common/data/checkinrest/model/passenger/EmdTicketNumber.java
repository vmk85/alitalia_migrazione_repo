package com.alitalia.aem.common.data.checkinrest.model.passenger;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EmdTicketNumber {
	
	@SerializedName("couponNumber")
	@Expose
	private int couponNumber;
	
	@SerializedName("couponNumberSpecified")
	@Expose
	private Boolean couponNumberSpecified;
	
	@SerializedName("value")
	@Expose
	private String value;

	public int getCouponNumber() {
		return couponNumber;
	}

	public void setCouponNumber(int couponNumber) {
		this.couponNumber = couponNumber;
	}

	public Boolean getCouponNumberSpecified() {
		return couponNumberSpecified;
	}

	public void setCouponNumberSpecified(Boolean couponNumberSpecified) {
		this.couponNumberSpecified = couponNumberSpecified;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
