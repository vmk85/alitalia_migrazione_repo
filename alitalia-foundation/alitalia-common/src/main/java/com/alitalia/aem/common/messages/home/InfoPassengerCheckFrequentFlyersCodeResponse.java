package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.messages.BaseResponse;

public class InfoPassengerCheckFrequentFlyersCodeResponse extends BaseResponse {
	
private List<AdultPassengerData> adultPassengersWithWrongFrequentFlyer;
	
	public InfoPassengerCheckFrequentFlyersCodeResponse() {
	}
	
	public InfoPassengerCheckFrequentFlyersCodeResponse(List<AdultPassengerData> adultPassengers) {
		super();
		this.adultPassengersWithWrongFrequentFlyer = adultPassengers;
	}
	
	public InfoPassengerCheckFrequentFlyersCodeResponse(String tid, String sid) {
		super(tid, sid);
	}
	
	public InfoPassengerCheckFrequentFlyersCodeResponse(String tid, String sid, 
			List<AdultPassengerData> adultPassengersWithWrongFrequentFlyer) {
		super(tid, sid);
		this.adultPassengersWithWrongFrequentFlyer = adultPassengersWithWrongFrequentFlyer;
	}
	
	public List<AdultPassengerData> getAdultPassengersWithWrongFrequentFlyer() {
		return adultPassengersWithWrongFrequentFlyer;
	}

	public void setAdultPassengersWithWrongFrequentFlyer(List<AdultPassengerData> adultPassengersWithWrongFrequentFlyer) {
		this.adultPassengersWithWrongFrequentFlyer = adultPassengersWithWrongFrequentFlyer;
	}

	@Override
	public String toString() {
		return "InfoPassengerCheckFrequentFlyersCodeRequest "
				+ "[adultPassengersWithWrongFrequentFlyer=" + adultPassengersWithWrongFrequentFlyer + "]";
	}
	
}
