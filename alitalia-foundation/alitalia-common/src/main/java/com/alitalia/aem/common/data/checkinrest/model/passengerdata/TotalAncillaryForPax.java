package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import com.alitalia.aem.common.data.checkinrest.model.cheangeseats.response.Seat;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerSeat;

import java.util.List;

public class TotalAncillaryForPax {

    private Passenger passenger;

    private int totalBaggage;

    private int totalLounge;

    private int totalFast;

    public List<PassengerSeat> getConfortSeat() {
        return confortSeat;
    }

    public void setConfortSeat(List<PassengerSeat> confortSeat) {
        this.confortSeat = confortSeat;
    }

    private List<PassengerSeat> confortSeat;


    public int getTotalBaggage() {
        return totalBaggage;
    }

    public void setTotalBaggage(int totalBaggage) {
        this.totalBaggage = totalBaggage;
    }

    public int getTotalLounge() {
        return totalLounge;
    }

    public void setTotalLounge(int totalLounge) {
        this.totalLounge = totalLounge;
    }

    public int getTotalFast() {
        return totalFast;
    }

    public void setTotalFast(int totalFast) {
        this.totalFast = totalFast;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

}
