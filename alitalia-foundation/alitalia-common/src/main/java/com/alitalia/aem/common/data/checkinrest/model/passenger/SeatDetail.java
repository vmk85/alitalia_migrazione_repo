package com.alitalia.aem.common.data.checkinrest.model.passenger;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SeatDetail {
	
	@SerializedName("columnDetailList")
	@Expose
	private List<ColumnDetailList> columnDetailList = null;
	
	@SerializedName("rowDetailList")
	@Expose
	private List<RowDetailList> rowDetailList = null;

	public List<ColumnDetailList> getColumnDetailList() {
		return columnDetailList;
	}

	public void setColumnDetailList(List<ColumnDetailList> columnDetailList) {
		this.columnDetailList = columnDetailList;
	}

	public List<RowDetailList> getRowDetailList() {
		return rowDetailList;
	}

	public void setRowDetailList(List<RowDetailList> rowDetailList) {
		this.rowDetailList = rowDetailList;
	}

}
