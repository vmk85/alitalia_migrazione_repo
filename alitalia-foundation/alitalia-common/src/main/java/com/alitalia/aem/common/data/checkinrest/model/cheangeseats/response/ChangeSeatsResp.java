
package com.alitalia.aem.common.data.checkinrest.model.cheangeseats.response;

import com.alitalia.aem.common.data.checkinrest.model.enhancedseatmap.response.SeatMaps;
import com.alitalia.aem.common.data.checkinrest.model.enhancedseatmap.response.SegmentSeat;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChangeSeatsResp {

	@SerializedName("segmentSeat")
	@Expose
	private SegmentSeat segmentSeat;
    @SerializedName("seatMaps")
    @Expose
    private SeatMaps seatMaps;
    
	public SegmentSeat getSegmentSeat() {
		return segmentSeat;
	}
	
	public void setSegmentSeat(SegmentSeat segmentSeat) {
		this.segmentSeat = segmentSeat;
	}
	
	public SeatMaps getSeatMaps() {
		return seatMaps;
	}
	
	public void setSeatMaps(SeatMaps seatMaps) {
		this.seatMaps = seatMaps;
	}
    
    

}
