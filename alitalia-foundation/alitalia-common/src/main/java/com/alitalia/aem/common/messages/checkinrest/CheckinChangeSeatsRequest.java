package com.alitalia.aem.common.messages.checkinrest;

import java.util.List;

import com.alitalia.aem.common.data.checkinrest.model.cheangeseats.request.SeatPassenger;
import com.alitalia.aem.common.messages.BaseRequest;

public class CheckinChangeSeatsRequest extends BaseRequest {

	    private String bookingClass;
	    private String airline;
	    private String flight;
	    private String departureDate;
	    private String origin;
	    private String destination;
	    private String pnr;
	    private List<SeatPassenger> seatPassenger = null;
	    private String language;
	    private String market;
	    private String conversationID;
	
	public CheckinChangeSeatsRequest() {}

	public CheckinChangeSeatsRequest(String tid, String sid) {
		super(tid, sid);
	}

	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	public String getAirline() {
		return airline;
	}

	public void setAirline(String airline) {
		this.airline = airline;
	}

	public String getFlight() {
		return flight;
	}

	public void setFlight(String flight) {
		this.flight = flight;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public List<SeatPassenger> getSeatPassenger() {
		return seatPassenger;
	}

	public void setSeatPassenger(List<SeatPassenger> seatPassenger) {
		this.seatPassenger = seatPassenger;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public String getConversationID() {
		return conversationID;
	}

	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}


}
