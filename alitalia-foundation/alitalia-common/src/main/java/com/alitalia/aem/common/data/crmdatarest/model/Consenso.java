package com.alitalia.aem.common.data.crmdatarest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Consenso {
    @Override
    public String toString() {
        return "ErrorSourceDetails [tipoConsenso=" + tipoConsenso + ", " +
                                    ", flagConsenso=" + flagConsenso +
                                    ", dataUltimoAggiornamento=" + dataUltimoAggiornamento + "]";
    }
    
    @SerializedName("tipoConsenso")
    @Expose
    private String tipoConsenso;
    
    @SerializedName("flagConsenso")
    @Expose
    private Boolean flagConsenso;
    
    @SerializedName("dataUltimoAggiornamento")
    @Expose
    private String dataUltimoAggiornamento;
    
    
    public String getTipoConsenso() {
        return tipoConsenso;
    }
    
    public void setTipoConsenso(String tipoConsenso) {
        this.tipoConsenso = tipoConsenso;
    }
    
    public Boolean getFlagConsenso() {
        return flagConsenso;
    }
    
    public void setFlagConsenso(Boolean flagConsenso) {
        this.flagConsenso = flagConsenso;
    }
    
    public String getDataUltimoAggiornamento() {
        return dataUltimoAggiornamento;
    }
    
    public void setDataUltimoAggiornamento(String dataUltimoAggiornamento) {
        this.dataUltimoAggiornamento = dataUltimoAggiornamento;
    }
    

}
