
package com.alitalia.aem.common.data.checkinrest.model.enhancedseatmap.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SeatMapRow {

    @SerializedName("rowNumber")
    @Expose
    private String rowNumber;
    @SerializedName("seats")
    @Expose
    private List<Seat> seats = null;

    public String getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(String rowNumber) {
        this.rowNumber = rowNumber;
    }

    public List<Seat> getSeats() {
        return seats;
    }

    public void setSeats(List<Seat> seats) {
        this.seats = seats;
    }

}
