
package com.alitalia.aem.common.data.checkinrest.model.updatepassengerdetails.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BagInfo {

    @SerializedName("individualWeightAndUnit")
    @Expose
    private IndividualWeightAndUnit individualWeightAndUnit;
    @SerializedName("bagTagInfo")
    @Expose
    private BagTagInfo_ bagTagInfo;
    @SerializedName("bagTexts")
    @Expose
    private List<BagText> bagTexts = null;
    @SerializedName("specialServiceTypeList")
    @Expose
    private List<SpecialServiceTypeList> specialServiceTypeList = null;
    @SerializedName("relativeBagSequenceNo")
    @Expose
    private String relativeBagSequenceNo;
    @SerializedName("bagTypeInfoList")
    @Expose
    private List<BagTypeInfoList> bagTypeInfoList = null;
    @SerializedName("bagRouteInfoList")
    @Expose
    private List<BagRouteInfoList> bagRouteInfoList = null;

    public IndividualWeightAndUnit getIndividualWeightAndUnit() {
        return individualWeightAndUnit;
    }

    public void setIndividualWeightAndUnit(IndividualWeightAndUnit individualWeightAndUnit) {
        this.individualWeightAndUnit = individualWeightAndUnit;
    }

    public BagTagInfo_ getBagTagInfo() {
        return bagTagInfo;
    }

    public void setBagTagInfo(BagTagInfo_ bagTagInfo) {
        this.bagTagInfo = bagTagInfo;
    }

    public List<BagText> getBagTexts() {
        return bagTexts;
    }

    public void setBagTexts(List<BagText> bagTexts) {
        this.bagTexts = bagTexts;
    }

    public List<SpecialServiceTypeList> getSpecialServiceTypeList() {
        return specialServiceTypeList;
    }

    public void setSpecialServiceTypeList(List<SpecialServiceTypeList> specialServiceTypeList) {
        this.specialServiceTypeList = specialServiceTypeList;
    }

    public String getRelativeBagSequenceNo() {
        return relativeBagSequenceNo;
    }

    public void setRelativeBagSequenceNo(String relativeBagSequenceNo) {
        this.relativeBagSequenceNo = relativeBagSequenceNo;
    }

    public List<BagTypeInfoList> getBagTypeInfoList() {
        return bagTypeInfoList;
    }

    public void setBagTypeInfoList(List<BagTypeInfoList> bagTypeInfoList) {
        this.bagTypeInfoList = bagTypeInfoList;
    }

    public List<BagRouteInfoList> getBagRouteInfoList() {
        return bagRouteInfoList;
    }

    public void setBagRouteInfoList(List<BagRouteInfoList> bagRouteInfoList) {
        this.bagRouteInfoList = bagRouteInfoList;
    }

}
