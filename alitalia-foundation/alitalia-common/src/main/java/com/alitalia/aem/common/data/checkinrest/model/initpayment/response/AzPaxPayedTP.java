package com.alitalia.aem.common.data.checkinrest.model.initpayment.response;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AzPaxPayedTP {
	
	@SerializedName("passengerID")
	@Expose
	private String passengerID;
	@SerializedName("nameId")
	@Expose
	private String nameId;
	@SerializedName("firstName")
	@Expose
	private String firstName;
	@SerializedName("lastName")
	@Expose
	private String lastName;
	@SerializedName("azAncillaryPayedTP")
	@Expose
	private List<AzAncillaryPayedTP> azAncillaryPayedTP = null;
	
	public String getPassengerID() {
		return passengerID;
	}
	
	public void setPassengerID(String passengerID) {
		this.passengerID = passengerID;
	}
	
	public String getNameId() {
		return nameId;
	}
	
	public void setNameId(String nameId) {
		this.nameId = nameId;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public List<AzAncillaryPayedTP> getAzAncillaryPayedTP() {
		return azAncillaryPayedTP;
	}
	
	public void setAzAncillaryPayedTP(List<AzAncillaryPayedTP> azAncillaryPayedTP) {
		this.azAncillaryPayedTP = azAncillaryPayedTP;
	}
	

}
