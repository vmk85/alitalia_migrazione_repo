package com.alitalia.aem.common.data.home.mmb;

import java.util.Calendar;
import java.util.List;

import com.alitalia.aem.common.data.home.enumerations.MmbApisTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbChannelsEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbMealAllowedEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbRouteStatusEnum;

public class MmbRouteData {

	private Integer id;
	private MmbApisTypeEnum apisTypeRequired;
	private MmbPassengerData applicantPassenger;
	private Boolean cartFastTrackBox;
	private Boolean cartVipLoungeBox;
	private MmbChannelsEnum channel;
	private Boolean checkinEnabled;
	private Boolean confirmed;
	private String createDateTime;
	private Calendar dateTimePaymentLimit;
	private MmbDeepLinkData deepLink;
	private List<MmbFlightData> flights;
	private Boolean mealAvailable;
	private MmbMealAllowedEnum mealAllowed;
	private String pnr;
	private Boolean selected;
	private MmbRouteStatusEnum status;
	private List<MmbTaxData> taxes;
	private Boolean undoEnabled;
	private Boolean visibleApis;

	public MmbApisTypeEnum getApisTypeRequired() {
		return apisTypeRequired;
	}

	public void setApisTypeRequired(MmbApisTypeEnum apisTypeRequired) {
		this.apisTypeRequired = apisTypeRequired;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public MmbPassengerData getApplicantPassenger() {
		return applicantPassenger;
	}

	public void setApplicantPassenger(MmbPassengerData applicantPassenger) {
		this.applicantPassenger = applicantPassenger;
	}

	public Boolean isCartFastTrackBox() {
		return cartFastTrackBox;
	}

	public void setCartFastTrackBox(Boolean cartFastTrackBox) {
		this.cartFastTrackBox = cartFastTrackBox;
	}

	public Boolean isCartVipLoungeBox() {
		return cartVipLoungeBox;
	}

	public void setCartVipLoungeBox(Boolean cartVipLoungeBox) {
		this.cartVipLoungeBox = cartVipLoungeBox;
	}

	public MmbChannelsEnum getChannel() {
		return channel;
	}

	public void setChannel(MmbChannelsEnum channel) {
		this.channel = channel;
	}

	public Boolean isCheckinEnabled() {
		return checkinEnabled;
	}

	public void setCheckinEnabled(Boolean checkinEnabled) {
		this.checkinEnabled = checkinEnabled;
	}

	public Boolean isConfirmed() {
		return confirmed;
	}

	public void setConfirmed(Boolean confirmed) {
		this.confirmed = confirmed;
	}

	public String getCreateDateTime() {
		return createDateTime;
	}

	public void setCreateDateTime(String createDateTime) {
		this.createDateTime = createDateTime;
	}

	public Calendar getDateTimePaymentLimit() {
		return dateTimePaymentLimit;
	}

	public void setDateTimePaymentLimit(Calendar dateTimePaymentLimit) {
		this.dateTimePaymentLimit = dateTimePaymentLimit;
	}

	public MmbDeepLinkData getDeepLink() {
		return deepLink;
	}

	public void setDeepLink(MmbDeepLinkData deepLink) {
		this.deepLink = deepLink;
	}

	public List<MmbFlightData> getFlights() {
		return flights;
	}

	public void setFlights(List<MmbFlightData> flights) {
		this.flights = flights;
	}

	public Boolean isMealAvailable() {
		return mealAvailable;
	}

	public void setMealAvailable(Boolean mealAvailable) {
		this.mealAvailable = mealAvailable;
	}

	public MmbMealAllowedEnum getMealAllowed() {
		return mealAllowed;
	}

	public void setMealAllowed(MmbMealAllowedEnum mealAllowed) {
		this.mealAllowed = mealAllowed;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Boolean isSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public MmbRouteStatusEnum getStatus() {
		return status;
	}

	public void setStatus(MmbRouteStatusEnum status) {
		this.status = status;
	}

	public List<MmbTaxData> getTaxes() {
		return taxes;
	}

	public void setTaxes(List<MmbTaxData> taxes) {
		this.taxes = taxes;
	}

	public Boolean isUndoEnabled() {
		return undoEnabled;
	}

	public void setUndoEnabled(Boolean undoEnabled) {
		this.undoEnabled = undoEnabled;
	}

	public Boolean isVisibleApis() {
		return visibleApis;
	}

	public void setVisibleApis(Boolean visibleApis) {
		this.visibleApis = visibleApis;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbRouteData other = (MmbRouteData) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbRouteData [id=" + id + ", apisTypeRequired="
				+ apisTypeRequired + ", applicantPassenger="
				+ applicantPassenger + ", cartFastTrackBox=" + cartFastTrackBox
				+ ", cartVipLoungeBox=" + cartVipLoungeBox + ", channel="
				+ channel + ", checkinEnabled=" + checkinEnabled
				+ ", confirmed=" + confirmed + ", createDateTime="
				+ createDateTime + ", dateTimePaymentLimit="
				+ dateTimePaymentLimit + ", deepLink=" + deepLink
				+ ", flights=" + flights + ", mealAvailable=" + mealAvailable
				+ ", mealAllowed=" + mealAllowed
				+ ", pnr=" + pnr + ", selected=" + selected + ", status="
				+ status + ", taxes=" + taxes + ", undoEnabled=" + undoEnabled
				+ ", visibleApis=" + visibleApis + "]";
	}

}
