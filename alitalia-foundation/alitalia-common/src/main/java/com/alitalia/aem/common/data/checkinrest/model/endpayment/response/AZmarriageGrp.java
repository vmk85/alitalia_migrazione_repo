
package com.alitalia.aem.common.data.checkinrest.model.endpayment.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AZmarriageGrp {

    @SerializedName("ind")
    @Expose
    private String ind;
    @SerializedName("group")
    @Expose
    private String group;
    @SerializedName("sequence")
    @Expose
    private String sequence;

    public String getInd() {
        return ind;
    }

    public void setInd(String ind) {
        this.ind = ind;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

}
