package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.MmbBaseRequest;

public class WebCheckinAirportNameRequest extends MmbBaseRequest {

	private String languageCode;

	public WebCheckinAirportNameRequest(String tid, String sid) {
		super(tid, sid);
	}

	public WebCheckinAirportNameRequest() {
		super();
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((languageCode == null) ? 0 : languageCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebCheckinAirportNameRequest other = (WebCheckinAirportNameRequest) obj;
		if (languageCode == null) {
			if (other.languageCode != null)
				return false;
		} else if (!languageCode.equals(other.languageCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WebCheckinStatesRequest [languageCode=" + languageCode
				+ ", toString()=" + super.toString() + "]";
	}
}