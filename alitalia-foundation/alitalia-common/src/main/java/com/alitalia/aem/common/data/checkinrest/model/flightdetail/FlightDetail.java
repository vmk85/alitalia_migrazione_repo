
package com.alitalia.aem.common.data.checkinrest.model.flightdetail;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FlightDetail {

    @SerializedName("itineraryResponseList")
    @Expose
    private List<ItineraryResponseList> itineraryResponseList = null;
    @SerializedName("legInfoList")
    @Expose
    private List<LegInfoList> legInfoList = null;
    @SerializedName("passengerCounts")
    @Expose
    private List<PassengerCount> passengerCounts = null;
    @SerializedName("jumpSeat")
    @Expose
    private JumpSeat jumpSeat;
    @SerializedName("totalPlan")
    @Expose
    private Long totalPlan;
    @SerializedName("totalPlanSpecified")
    @Expose
    private Boolean totalPlanSpecified;
    @SerializedName("apisInfoList")
    @Expose
    private List<ApisInfoList_> apisInfoList = null;
    @SerializedName("freeTextInfoList")
    @Expose
    private List<FreeTextInfoList_> freeTextInfoList = null;
    @SerializedName("result")
    @Expose
    private Result result;

    public List<ItineraryResponseList> getItineraryResponseList() {
        return itineraryResponseList;
    }

    public void setItineraryResponseList(List<ItineraryResponseList> itineraryResponseList) {
        this.itineraryResponseList = itineraryResponseList;
    }

    public List<LegInfoList> getLegInfoList() {
        return legInfoList;
    }

    public void setLegInfoList(List<LegInfoList> legInfoList) {
        this.legInfoList = legInfoList;
    }

    public List<PassengerCount> getPassengerCounts() {
        return passengerCounts;
    }

    public void setPassengerCounts(List<PassengerCount> passengerCounts) {
        this.passengerCounts = passengerCounts;
    }

    public JumpSeat getJumpSeat() {
        return jumpSeat;
    }

    public void setJumpSeat(JumpSeat jumpSeat) {
        this.jumpSeat = jumpSeat;
    }

    public Long getTotalPlan() {
        return totalPlan;
    }

    public void setTotalPlan(Long totalPlan) {
        this.totalPlan = totalPlan;
    }

    public Boolean getTotalPlanSpecified() {
        return totalPlanSpecified;
    }

    public void setTotalPlanSpecified(Boolean totalPlanSpecified) {
        this.totalPlanSpecified = totalPlanSpecified;
    }

    public List<ApisInfoList_> getApisInfoList() {
        return apisInfoList;
    }

    public void setApisInfoList(List<ApisInfoList_> apisInfoList) {
        this.apisInfoList = apisInfoList;
    }

    public List<FreeTextInfoList_> getFreeTextInfoList() {
        return freeTextInfoList;
    }

    public void setFreeTextInfoList(List<FreeTextInfoList_> freeTextInfoList) {
        this.freeTextInfoList = freeTextInfoList;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

}
