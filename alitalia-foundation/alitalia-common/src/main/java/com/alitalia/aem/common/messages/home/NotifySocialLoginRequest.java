package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class NotifySocialLoginRequest extends BaseRequest {
	
	private Boolean newUser;
	private String siteUID;
	
	public Boolean getNewUser() {
		return newUser;
	}

	public void setNewUser(Boolean newUser) {
		this.newUser = newUser;
	}

	public String getSiteUID() {
		return siteUID;
	}

	public void setSiteUID(String siteUID) {
		this.siteUID = siteUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((newUser == null) ? 0 : newUser.hashCode());
		result = prime * result + ((siteUID == null) ? 0 : siteUID.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		NotifySocialLoginRequest other = (NotifySocialLoginRequest) obj;
		if (newUser == null) {
			if (other.newUser != null)
				return false;
		} else if (!newUser.equals(other.newUser))
			return false;
		if (siteUID == null) {
			if (other.siteUID != null)
				return false;
		} else if (!siteUID.equals(other.siteUID))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "NotifySocialLoginRequest [newUser=" + newUser + ", siteUID=" + siteUID + "]";
	}
	
}
