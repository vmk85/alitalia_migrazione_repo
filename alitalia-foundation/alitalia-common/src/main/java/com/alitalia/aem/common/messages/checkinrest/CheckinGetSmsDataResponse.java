package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.boardingpass.BoardingPassSms;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinGetSmsDataResponse extends BaseResponse {

    private BoardingPassSms boardingPassData;
    private String conversationID;
    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public CheckinGetSmsDataResponse() {}

    public CheckinGetSmsDataResponse(String tid, String sid) {
        super(tid, sid);
    }

    public BoardingPassSms getBoardingPassData() {
        return boardingPassData;
    }

    public void setBoardingPassData(BoardingPassSms boardingPassData) {
        this.boardingPassData = boardingPassData;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }
}
