package com.alitalia.aem.common.data.home;

import java.util.List;

public class ResultBookingPriceSolutionSliceSegmentPricingData {
	
	private List<ResultBookingPriceSolutionSliceSegmentPricingBaggageAllowanceOptionFreeBaggageAllowanceData> baggageAllowanceOptionField;
	private ResultBookingPriceSolutionSliceSegmentPricingExtData extField;
	
	public List<ResultBookingPriceSolutionSliceSegmentPricingBaggageAllowanceOptionFreeBaggageAllowanceData> getBaggageAllowanceOptionField() {
		return baggageAllowanceOptionField;
	}
	
	public void setBaggageAllowanceOptionField(
			List<ResultBookingPriceSolutionSliceSegmentPricingBaggageAllowanceOptionFreeBaggageAllowanceData> baggageAllowanceOptionField) {
		this.baggageAllowanceOptionField = baggageAllowanceOptionField;
	}
	
	public ResultBookingPriceSolutionSliceSegmentPricingExtData getExtField() {
		return extField;
	}
	
	public void setExtField(
			ResultBookingPriceSolutionSliceSegmentPricingExtData extField) {
		this.extField = extField;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((baggageAllowanceOptionField == null) ? 0
						: baggageAllowanceOptionField.hashCode());
		result = prime * result
				+ ((extField == null) ? 0 : extField.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingPriceSolutionSliceSegmentPricingData other = (ResultBookingPriceSolutionSliceSegmentPricingData) obj;
		if (baggageAllowanceOptionField == null) {
			if (other.baggageAllowanceOptionField != null)
				return false;
		} else if (!baggageAllowanceOptionField
				.equals(other.baggageAllowanceOptionField))
			return false;
		if (extField == null) {
			if (other.extField != null)
				return false;
		} else if (!extField.equals(other.extField))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingPriceSolutionSliceSegmentPricingData [baggageAllowanceOptionField="
				+ baggageAllowanceOptionField + ", extField=" + extField + "]";
	}
	
	
}
