package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.NLUserData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RegisterUserToAgencyNewsletterResponse extends BaseResponse {

	private String agencyId;
	private List<NLUserData> users;
	
	public String getAgencyId() {
		return agencyId;
	}
	
	public void setAgencyId(String agencyId) {
		this.agencyId = agencyId;
	}
	
	public List<NLUserData> getUsers() {
		return users;
	}
	
	public void setUsers(List<NLUserData> users) {
		this.users = users;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((agencyId == null) ? 0 : agencyId.hashCode());
		result = prime * result + ((users == null) ? 0 : users.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegisterUserToAgencyNewsletterResponse other = (RegisterUserToAgencyNewsletterResponse) obj;
		if (agencyId == null) {
			if (other.agencyId != null)
				return false;
		} else if (!agencyId.equals(other.agencyId))
			return false;
		if (users == null) {
			if (other.users != null)
				return false;
		} else if (!users.equals(other.users))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RegisterUserToAgencyNewsletterResponse [agencyId=" + agencyId
				+ ", users=" + users + "]";
	}
}