
package com.alitalia.aem.common.data.checkinrest.model.initpayment.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AncillariesPassenger {

    @SerializedName("passengerID")
    @Expose
    private String passengerID;
    @SerializedName("nameId")
    @Expose
    private String nameId;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("priceToBeIssued")
    @Expose
    private Integer priceToBeIssued;
    @SerializedName("priceIssued")
    @Expose
    private Integer priceIssued;
    @SerializedName("ancillaries")
    @Expose
    private List<Ancillary> ancillaries = null;

    public String getPassengerID() {
        return passengerID;
    }

    public void setPassengerID(String passengerID) {
        this.passengerID = passengerID;
    }

    public String getNameId() {
        return nameId;
    }

    public void setNameId(String nameId) {
        this.nameId = nameId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getPriceToBeIssued() {
        return priceToBeIssued;
    }

    public void setPriceToBeIssued(Integer priceToBeIssued) {
        this.priceToBeIssued = priceToBeIssued;
    }

    public Integer getPriceIssued() {
        return priceIssued;
    }

    public void setPriceIssued(Integer priceIssued) {
        this.priceIssued = priceIssued;
    }

    public List<Ancillary> getAncillaries() {
        return ancillaries;
    }

    public void setAncillaries(List<Ancillary> ancillaries) {
        this.ancillaries = ancillaries;
    }

}
