
package com.alitalia.aem.common.data.checkinrest.model.enhancedseatmap.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SegmentSeat {

    @SerializedName("airline")
    @Expose
    private String airline;
    @SerializedName("flight")
    @Expose
    private String flight;
    @SerializedName("origin")
    @Expose
    private Origin origin;
    @SerializedName("destination")
    @Expose
    private Destination destination;
    @SerializedName("arrivalDate")
    @Expose
    private String arrivalDate;
    @SerializedName("departureDate")
    @Expose
    private String departureDate;
    @SerializedName("openCI")
    @Expose
    private Boolean openCI;
    @SerializedName("checkInGate")
    @Expose
    private String checkInGate;
    @SerializedName("airlineName")
    @Expose
    private String airlineName;
    @SerializedName("terminal")
    @Expose
    private String terminal;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("waiting")
    @Expose
    private String waiting;
    @SerializedName("aircraft")
    @Expose
    private String aircraft;

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getFlight() {
        return flight;
    }

    public void setFlight(String flight) {
        this.flight = flight;
    }

    public Origin getOrigin() {
        return origin;
    }

    public void setOrigin(Origin origin) {
        this.origin = origin;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public Boolean getOpenCI() {
        return openCI;
    }

    public void setOpenCI(Boolean openCI) {
        this.openCI = openCI;
    }

    public String getCheckInGate() {
        return checkInGate;
    }

    public void setCheckInGate(String checkInGate) {
        this.checkInGate = checkInGate;
    }

    public String getAirlineName() {
        return airlineName;
    }

    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getWaiting() {
        return waiting;
    }

    public void setWaiting(String waiting) {
        this.waiting = waiting;
    }

    public String getAircraft() {
        return aircraft;
    }

    public void setAircraft(String aircraft) {
        this.aircraft = aircraft;
    }

}
