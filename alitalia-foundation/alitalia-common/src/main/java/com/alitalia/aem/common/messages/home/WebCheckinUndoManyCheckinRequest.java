package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.checkin.CheckinRouteData;
import com.alitalia.aem.common.data.home.checkin.UndoCheckinPassengerData;
import com.alitalia.aem.common.messages.MmbBaseRequest;

public class WebCheckinUndoManyCheckinRequest extends MmbBaseRequest {
	
    private List<UndoCheckinPassengerData> passengers;
    private CheckinRouteData route;

	public WebCheckinUndoManyCheckinRequest(String tid, String sid) {
		super(tid, sid);
	}

	public WebCheckinUndoManyCheckinRequest() {
		super();
	}

	public List<UndoCheckinPassengerData> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<UndoCheckinPassengerData> passengers) {
		this.passengers = passengers;
	}

	public CheckinRouteData getRoute() {
		return route;
	}

	public void setRoute(CheckinRouteData route) {
		this.route = route;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((passengers == null) ? 0 : passengers.hashCode());
		result = prime * result + ((route == null) ? 0 : route.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebCheckinUndoManyCheckinRequest other = (WebCheckinUndoManyCheckinRequest) obj;
		if (passengers == null) {
			if (other.passengers != null)
				return false;
		} else if (!passengers.equals(other.passengers))
			return false;
		if (route == null) {
			if (other.route != null)
				return false;
		} else if (!route.equals(other.route))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WebCheckinUndoManyCheckinRequest [passengers=" + passengers
				+ ", route=" + route + "]";
	}
}