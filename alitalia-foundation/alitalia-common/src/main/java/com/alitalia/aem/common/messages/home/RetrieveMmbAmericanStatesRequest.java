package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class RetrieveMmbAmericanStatesRequest extends BaseRequest {
	
	private String itemCache;
	private String languageCode;
	private String market;
	
	public RetrieveMmbAmericanStatesRequest(String tid, String sid) {
		super(tid, sid);
	}

	public String getItemCache() {
		return itemCache;
	}
	
	public void setItemCache(String itemCache) {
		this.itemCache = itemCache;
	}
	
	public String getLanguageCode() {
		return languageCode;
	}
	
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	
	public String getMarket() {
		return market;
	}
	
	public void setMarket(String market) {
		this.market = market;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((itemCache == null) ? 0 : itemCache.hashCode());
		result = prime * result
				+ ((languageCode == null) ? 0 : languageCode.hashCode());
		result = prime * result + ((market == null) ? 0 : market.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveMmbAmericanStatesRequest other = (RetrieveMmbAmericanStatesRequest) obj;
		if (itemCache == null) {
			if (other.itemCache != null)
				return false;
		} else if (!itemCache.equals(other.itemCache))
			return false;
		if (languageCode == null) {
			if (other.languageCode != null)
				return false;
		} else if (!languageCode.equals(other.languageCode))
			return false;
		if (market == null) {
			if (other.market != null)
				return false;
		} else if (!market.equals(other.market))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveMealsRequest [itemCache=" + itemCache
				+ ", languageCode=" + languageCode + ", market=" + market + "]";
	}

}
