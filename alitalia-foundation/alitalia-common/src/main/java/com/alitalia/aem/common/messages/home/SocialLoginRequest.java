package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class SocialLoginRequest extends BaseRequest {

	private String gigyaId;
	
	private String gigyaSignature;
	
	public SocialLoginRequest() {
		super();
	}
	
	public SocialLoginRequest(String tid, String sid) {
		super(tid, sid);
	}
	
	public SocialLoginRequest(String tid, String sid, String gigyaId, String gigyaSignature) {
		super(tid, sid);
		this.gigyaId = gigyaId;
		this.gigyaSignature = gigyaSignature;
	}
	
	public String getGigyaId() {
		return gigyaId;
	}

	public void setGigyaId(String gigyaId) {
		this.gigyaId = gigyaId;
	}
	
	public String getGigyaSignature() {
		return gigyaSignature;
	}

	public void setGigyaSignature(String gigyaSignature) {
		this.gigyaSignature = gigyaSignature;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((gigyaId == null) ? 0 : gigyaId.hashCode());
		result = prime * result + ((gigyaSignature == null) ? 0 : gigyaSignature.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SocialLoginRequest other = (SocialLoginRequest) obj;
		if (gigyaId == null) {
			if (other.gigyaId != null)
				return false;
		} else if (!gigyaId.equals(other.gigyaId))
			return false;
		if (gigyaSignature == null) {
			if (other.gigyaSignature != null)
				return false;
		} else if (!gigyaSignature.equals(other.gigyaSignature))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SocialLoginRequest [gigyaId=" + gigyaId + ", gigyaSignature=" + gigyaSignature + "]";
	}
	
}
