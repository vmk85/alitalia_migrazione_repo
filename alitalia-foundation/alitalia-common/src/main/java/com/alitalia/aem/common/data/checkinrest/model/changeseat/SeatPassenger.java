package com.alitalia.aem.common.data.checkinrest.model.changeseat;

public class SeatPassenger {

	private String newSeat;
	private String passengerID;
	private String cognome;
	private String nome;

	public String getNewSeat() {
		return newSeat;
	}
	public void setNewSeat(String newSeat) {
		this.newSeat = newSeat;
	}
	public String getPassengerID() {
		return passengerID;
	}
	public void setPassengerID(String passengerID) {
		this.passengerID = passengerID;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

}
