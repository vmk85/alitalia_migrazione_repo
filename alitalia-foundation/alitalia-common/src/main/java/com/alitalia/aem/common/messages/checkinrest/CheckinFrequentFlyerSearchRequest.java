package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.messages.BaseRequest;

public class CheckinFrequentFlyerSearchRequest extends BaseRequest {
	
	private String frequentFlyer;
	private String lastName;
	private String market;
	private String language;
	private String conversationId;
	
	public CheckinFrequentFlyerSearchRequest() {}
	
	public CheckinFrequentFlyerSearchRequest(String tid, String sid) {
		super(tid, sid);
	}

	public String getFrequentFlyer() {
		return frequentFlyer;
	}

	public void setFrequentFlyer(String frequentFlyer) {
		this.frequentFlyer = frequentFlyer;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getConversationId() {
		return conversationId;
	}

	public void setConversationId(String conversationId) {
		this.conversationId = conversationId;
	}

}
