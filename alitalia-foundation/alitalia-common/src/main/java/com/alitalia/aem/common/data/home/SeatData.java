package com.alitalia.aem.common.data.home;

import com.alitalia.aem.common.data.home.enumerations.AvailabilitySeatEnum;
import com.alitalia.aem.common.data.home.enumerations.TypeSeatEnum;

public class SeatData {

	private AvailabilitySeatEnum availability;
    private String number;
    private Integer passengerIndex;
    private TypeSeatEnum typeSeat;
	
    public AvailabilitySeatEnum getAvailability() {
		return availability;
	}
    
	public void setAvailability(AvailabilitySeatEnum availability) {
		this.availability = availability;
	}
	
	public String getNumber() {
		return number;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	
	public Integer getPassengerIndex() {
		return passengerIndex;
	}
	
	public void setPassengerIndex(Integer passengerIndex) {
		this.passengerIndex = passengerIndex;
	}
	
	public TypeSeatEnum getTypeSeat() {
		return typeSeat;
	}
	
	public void setTypeSeat(TypeSeatEnum typeSeat) {
		this.typeSeat = typeSeat;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((availability == null) ? 0 : availability.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result
				+ ((passengerIndex == null) ? 0 : passengerIndex.hashCode());
		result = prime * result
				+ ((typeSeat == null) ? 0 : typeSeat.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SeatData other = (SeatData) obj;
		if (availability != other.availability)
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (passengerIndex == null) {
			if (other.passengerIndex != null)
				return false;
		} else if (!passengerIndex.equals(other.passengerIndex))
			return false;
		if (typeSeat != other.typeSeat)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SeatData [availability=" + availability + ", number=" + number
				+ ", passengerIndex=" + passengerIndex + ", typeSeat="
				+ typeSeat + "]";
	}
}