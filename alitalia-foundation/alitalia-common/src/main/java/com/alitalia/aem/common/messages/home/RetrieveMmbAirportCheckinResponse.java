package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.FrequentFlyerTypeData;
import com.alitalia.aem.common.data.home.mmb.MmbAirportCheckinData;
import com.alitalia.aem.common.data.home.mmb.MmbFrequentFlyerCarrierData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetrieveMmbAirportCheckinResponse extends BaseResponse{

	private List<MmbAirportCheckinData> airportCheckinList;
	
	public RetrieveMmbAirportCheckinResponse(){}
	
	public RetrieveMmbAirportCheckinResponse(String tid, String sid) {
		super(tid, sid);
	}

	public List<MmbAirportCheckinData> getAirportCheckinList() {
		return airportCheckinList;
	}

	public void setAirportCheckinList(List<MmbAirportCheckinData> airportCheckinList) {
		this.airportCheckinList = airportCheckinList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((airportCheckinList == null) ? 0 : airportCheckinList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveMmbAirportCheckinResponse other = (RetrieveMmbAirportCheckinResponse) obj;
		if (airportCheckinList == null) {
			if (other.airportCheckinList != null)
				return false;
		} else if (!airportCheckinList.equals(other.airportCheckinList))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveMmbAirportCheckinResponse [airportCheckinList="
				+ airportCheckinList + ", getTid()=" + getTid() + ", getSid()="
				+ getSid() + "]";
	}
}