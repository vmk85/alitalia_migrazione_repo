package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.messages.BaseResponse;

public class SearchFlightsResponse extends BaseResponse{

	private List<AirportData> airports;
	
	public SearchFlightsResponse(){}
	
	public SearchFlightsResponse(String tid, String sid) {
		super(tid, sid);
	}

	public List<AirportData> getAirports() {
		return airports;
	}

	public void setAirports(List<AirportData> airports) {
		this.airports = airports;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((airports == null) ? 0 : airports.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SearchFlightsResponse other = (SearchFlightsResponse) obj;
		if (airports == null) {
			if (other.airports != null)
				return false;
		} else if (!airports.equals(other.airports))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveAirportsResponse [airports=" + airports + "]";
	}
}