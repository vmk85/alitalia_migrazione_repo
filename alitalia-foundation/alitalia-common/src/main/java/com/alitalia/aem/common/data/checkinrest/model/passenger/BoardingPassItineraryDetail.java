package com.alitalia.aem.common.data.checkinrest.model.passenger;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BoardingPassItineraryDetail {

	@SerializedName("operatingAirline")
	@Expose
	private OperatingAirline operatingAirline;
	@SerializedName("marketingAirline")
	@Expose
	private MarketingAirline marketingAirline;
	@SerializedName("gate")
	@Expose
	private String gate;
	@SerializedName("terminal")
	@Expose
	private String terminal;
	@SerializedName("boardingTime")
	@Expose
	private String boardingTime;
	@SerializedName("doorCloseTime")
	@Expose
	private String doorCloseTime;
	@SerializedName("origin")
	@Expose
	private String origin;
	@SerializedName("originCity")
	@Expose
	private String originCity;
	@SerializedName("originCountry")
	@Expose
	private String originCountry;
	@SerializedName("thru")
	@Expose
	private String thru;
	@SerializedName("thruCity")
	@Expose
	private String thruCity;
	@SerializedName("thruCountry")
	@Expose
	private String thruCountry;
	@SerializedName("scheduledDepartureDate")
	@Expose
	private String scheduledDepartureDate;
	@SerializedName("scheduledDepartureTime")
	@Expose
	private String scheduledDepartureTime;
	@SerializedName("estimatedDepartureDate")
	@Expose
	private String estimatedDepartureDate;
	@SerializedName("estimatedDepartureTime")
	@Expose
	private String estimatedDepartureTime;
	@SerializedName("julianDepartureDate")
	@Expose
	private String julianDepartureDate;
	@SerializedName("clockType")
	@Expose
	private String clockType;
	@SerializedName("destination")
	@Expose
	private String destination;
	@SerializedName("destinationCity")
	@Expose
	private String destinationCity;
	@SerializedName("destinationCountry")
	@Expose
	private String destinationCountry;
	@SerializedName("scheduledArrivalTime")
	@Expose
	private String scheduledArrivalTime;
	@SerializedName("estimatedArrivalTime")
	@Expose
	private String estimatedArrivalTime;
	@SerializedName("cabin")
	@Expose
	private String cabin;
	@SerializedName("deck")
	@Expose
	private String deck;
	@SerializedName("bookingCode")
	@Expose
	private String bookingCode;
	@SerializedName("checkInSequence")
	@Expose
	private String checkInSequence;
	@SerializedName("seatInfoList")
	@Expose
	private List<SeatInfoList> seatInfoList = null;
	@SerializedName("extraSeatInfo")
	@Expose
	private String extraSeatInfo;
	@SerializedName("codeshare")
	@Expose
	private String codeshare;
	@SerializedName("wetleaseOperatedBy")
	@Expose
	private String wetleaseOperatedBy;
	@SerializedName("wetleaseCarrier")
	@Expose
	private String wetleaseCarrier;
	@SerializedName("wetleaseFlightConfig")
	@Expose
	private String wetleaseFlightConfig;
	@SerializedName("commuterOperatedByText")
	@Expose
	private String commuterOperatedByText;
	@SerializedName("commuterCarrier")
	@Expose
	private String commuterCarrier;
	@SerializedName("frequentTravel")
	@Expose
	private FrequentTravel frequentTravel;
	@SerializedName("totalBaggageWeight")
	@Expose
	private TotalBaggageWeight totalBaggageWeight;
	@SerializedName("totalCarryOnWeight")
	@Expose
	private TotalCarryOnWeight totalCarryOnWeight;
	@SerializedName("bagTagNumber")
	@Expose
	private List<String> bagTagNumber = null;
	@SerializedName("piecesOfBaggage")
	@Expose
	private String piecesOfBaggage;
	@SerializedName("priorityInfo")
	@Expose
	private PriorityInfo priorityInfo;
	@SerializedName("dhsStatusCode")
	@Expose
	private String dhsStatusCode;
	@SerializedName("selectee")
	@Expose
	private String selectee;
	@SerializedName("tsaPreCheckText")
	@Expose
	private String tsaPreCheckText;
	@SerializedName("etciapiText")
	@Expose
	private String etciapiText;
	@SerializedName("groupZone")
	@Expose
	private String groupZone;
	@SerializedName("brandedFare")
	@Expose
	private String brandedFare;
	@SerializedName("carryOnText")
	@Expose
	private String carryOnText;
	@SerializedName("flightCouponText")
	@Expose
	private String flightCouponText;
	@SerializedName("ticketText")
	@Expose
	private String ticketText;
	@SerializedName("vcrNumber")
	@Expose
	private VcrNumberPassenger vcrNumber;
	@SerializedName("documentType")
	@Expose
	private String documentType;
	@SerializedName("ssrCode")
	@Expose
	private List<String> ssrCode = null;
	@SerializedName("pingTip")
	@Expose
	private String pingTip;
	@SerializedName("speedText")
	@Expose
	private String speedText;
	@SerializedName("evenMoreSpeedText")
	@Expose
	private String evenMoreSpeedText;
	@SerializedName("boardingPassOnlyText")
	@Expose
	private String boardingPassOnlyText;
	@SerializedName("plusInfantText")
	@Expose
	private String plusInfantText;
	@SerializedName("childText")
	@Expose
	private String childText;
	@SerializedName("thruText")
	@Expose
	private String thruText;
	@SerializedName("premiumText")
	@Expose
	private String premiumText;
	@SerializedName("exitText")
	@Expose
	private String exitText;
	@SerializedName("tboText")
	@Expose
	private String tboText;
	@SerializedName("tboCityCode")
	@Expose
	private String tboCityCode;
	@SerializedName("cabinText")
	@Expose
	private String cabinText;
	@SerializedName("exclusiveWaitingArea")
	@Expose
	private String exclusiveWaitingArea;
	@SerializedName("loungeAccess")
	@Expose
	private String loungeAccess;
	@SerializedName("spanishResident")
	@Expose
	private String spanishResident;
	@SerializedName("spanishLargeFamily")
	@Expose
	private String spanishLargeFamily;
	@SerializedName("ancillaryList")
	@Expose
	private List<AncillaryList> ancillaryList = null;
	
	public OperatingAirline getOperatingAirline() {
		return operatingAirline;
	}
	public void setOperatingAirline(OperatingAirline operatingAirline) {
		this.operatingAirline = operatingAirline;
	}
	public MarketingAirline getMarketingAirline() {
		return marketingAirline;
	}
	public void setMarketingAirline(MarketingAirline marketingAirline) {
		this.marketingAirline = marketingAirline;
	}
	public String getGate() {
		return gate;
	}
	public void setGate(String gate) {
		this.gate = gate;
	}
	public String getTerminal() {
		return terminal;
	}
	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}
	public String getBoardingTime() {
		return boardingTime;
	}
	public void setBoardingTime(String boardingTime) {
		this.boardingTime = boardingTime;
	}
	public String getDoorCloseTime() {
		return doorCloseTime;
	}
	public void setDoorCloseTime(String doorCloseTime) {
		this.doorCloseTime = doorCloseTime;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getOriginCity() {
		return originCity;
	}
	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}
	public String getOriginCountry() {
		return originCountry;
	}
	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}
	public String getThru() {
		return thru;
	}
	public void setThru(String thru) {
		this.thru = thru;
	}
	public String getThruCity() {
		return thruCity;
	}
	public void setThruCity(String thruCity) {
		this.thruCity = thruCity;
	}
	public String getThruCountry() {
		return thruCountry;
	}
	public void setThruCountry(String thruCountry) {
		this.thruCountry = thruCountry;
	}
	public String getScheduledDepartureDate() {
		return scheduledDepartureDate;
	}
	public void setScheduledDepartureDate(String scheduledDepartureDate) {
		this.scheduledDepartureDate = scheduledDepartureDate;
	}
	public String getScheduledDepartureTime() {
		return scheduledDepartureTime;
	}
	public void setScheduledDepartureTime(String scheduledDepartureTime) {
		this.scheduledDepartureTime = scheduledDepartureTime;
	}
	public String getEstimatedDepartureDate() {
		return estimatedDepartureDate;
	}
	public void setEstimatedDepartureDate(String estimatedDepartureDate) {
		this.estimatedDepartureDate = estimatedDepartureDate;
	}
	public String getEstimatedDepartureTime() {
		return estimatedDepartureTime;
	}
	public void setEstimatedDepartureTime(String estimatedDepartureTime) {
		this.estimatedDepartureTime = estimatedDepartureTime;
	}
	public String getJulianDepartureDate() {
		return julianDepartureDate;
	}
	public void setJulianDepartureDate(String julianDepartureDate) {
		this.julianDepartureDate = julianDepartureDate;
	}
	public String getClockType() {
		return clockType;
	}
	public void setClockType(String clockType) {
		this.clockType = clockType;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getDestinationCity() {
		return destinationCity;
	}
	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}
	public String getDestinationCountry() {
		return destinationCountry;
	}
	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}
	public String getScheduledArrivalTime() {
		return scheduledArrivalTime;
	}
	public void setScheduledArrivalTime(String scheduledArrivalTime) {
		this.scheduledArrivalTime = scheduledArrivalTime;
	}
	public String getEstimatedArrivalTime() {
		return estimatedArrivalTime;
	}
	public void setEstimatedArrivalTime(String estimatedArrivalTime) {
		this.estimatedArrivalTime = estimatedArrivalTime;
	}
	public String getCabin() {
		return cabin;
	}
	public void setCabin(String cabin) {
		this.cabin = cabin;
	}
	public String getDeck() {
		return deck;
	}
	public void setDeck(String deck) {
		this.deck = deck;
	}
	public String getBookingCode() {
		return bookingCode;
	}
	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}
	public String getCheckInSequence() {
		return checkInSequence;
	}
	public void setCheckInSequence(String checkInSequence) {
		this.checkInSequence = checkInSequence;
	}
	public List<SeatInfoList> getSeatInfoList() {
		return seatInfoList;
	}
	public void setSeatInfoList(List<SeatInfoList> seatInfoList) {
		this.seatInfoList = seatInfoList;
	}
	public String getExtraSeatInfo() {
		return extraSeatInfo;
	}
	public void setExtraSeatInfo(String extraSeatInfo) {
		this.extraSeatInfo = extraSeatInfo;
	}
	public String getCodeshare() {
		return codeshare;
	}
	public void setCodeshare(String codeshare) {
		this.codeshare = codeshare;
	}
	public String getWetleaseOperatedBy() {
		return wetleaseOperatedBy;
	}
	public void setWetleaseOperatedBy(String wetleaseOperatedBy) {
		this.wetleaseOperatedBy = wetleaseOperatedBy;
	}
	public String getWetleaseCarrier() {
		return wetleaseCarrier;
	}
	public void setWetleaseCarrier(String wetleaseCarrier) {
		this.wetleaseCarrier = wetleaseCarrier;
	}
	public String getWetleaseFlightConfig() {
		return wetleaseFlightConfig;
	}
	public void setWetleaseFlightConfig(String wetleaseFlightConfig) {
		this.wetleaseFlightConfig = wetleaseFlightConfig;
	}
	public String getCommuterOperatedByText() {
		return commuterOperatedByText;
	}
	public void setCommuterOperatedByText(String commuterOperatedByText) {
		this.commuterOperatedByText = commuterOperatedByText;
	}
	public String getCommuterCarrier() {
		return commuterCarrier;
	}
	public void setCommuterCarrier(String commuterCarrier) {
		this.commuterCarrier = commuterCarrier;
	}
	public FrequentTravel getFrequentTravel() {
		return frequentTravel;
	}
	public void setFrequentTravel(FrequentTravel frequentTravel) {
		this.frequentTravel = frequentTravel;
	}
	public TotalBaggageWeight getTotalBaggageWeight() {
		return totalBaggageWeight;
	}
	public void setTotalBaggageWeight(TotalBaggageWeight totalBaggageWeight) {
		this.totalBaggageWeight = totalBaggageWeight;
	}
	public TotalCarryOnWeight getTotalCarryOnWeight() {
		return totalCarryOnWeight;
	}
	public void setTotalCarryOnWeight(TotalCarryOnWeight totalCarryOnWeight) {
		this.totalCarryOnWeight = totalCarryOnWeight;
	}
	public List<String> getBagTagNumber() {
		return bagTagNumber;
	}
	public void setBagTagNumber(List<String> bagTagNumber) {
		this.bagTagNumber = bagTagNumber;
	}
	public String getPiecesOfBaggage() {
		return piecesOfBaggage;
	}
	public void setPiecesOfBaggage(String piecesOfBaggage) {
		this.piecesOfBaggage = piecesOfBaggage;
	}
	public PriorityInfo getPriorityInfo() {
		return priorityInfo;
	}
	public void setPriorityInfo(PriorityInfo priorityInfo) {
		this.priorityInfo = priorityInfo;
	}
	public String getDhsStatusCode() {
		return dhsStatusCode;
	}
	public void setDhsStatusCode(String dhsStatusCode) {
		this.dhsStatusCode = dhsStatusCode;
	}
	public String getSelectee() {
		return selectee;
	}
	public void setSelectee(String selectee) {
		this.selectee = selectee;
	}
	public String getTsaPreCheckText() {
		return tsaPreCheckText;
	}
	public void setTsaPreCheckText(String tsaPreCheckText) {
		this.tsaPreCheckText = tsaPreCheckText;
	}
	public String getEtciapiText() {
		return etciapiText;
	}
	public void setEtciapiText(String etciapiText) {
		this.etciapiText = etciapiText;
	}
	public String getGroupZone() {
		return groupZone;
	}
	public void setGroupZone(String groupZone) {
		this.groupZone = groupZone;
	}
	public String getBrandedFare() {
		return brandedFare;
	}
	public void setBrandedFare(String brandedFare) {
		this.brandedFare = brandedFare;
	}
	public String getCarryOnText() {
		return carryOnText;
	}
	public void setCarryOnText(String carryOnText) {
		this.carryOnText = carryOnText;
	}
	public String getFlightCouponText() {
		return flightCouponText;
	}
	public void setFlightCouponText(String flightCouponText) {
		this.flightCouponText = flightCouponText;
	}
	public String getTicketText() {
		return ticketText;
	}
	public void setTicketText(String ticketText) {
		this.ticketText = ticketText;
	}
	public VcrNumberPassenger getVcrNumber() {
		return vcrNumber;
	}
	public void setVcrNumber(VcrNumberPassenger vcrNumber) {
		this.vcrNumber = vcrNumber;
	}
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public List<String> getSsrCode() {
		return ssrCode;
	}
	public void setSsrCode(List<String> ssrCode) {
		this.ssrCode = ssrCode;
	}
	public String getPingTip() {
		return pingTip;
	}
	public void setPingTip(String pingTip) {
		this.pingTip = pingTip;
	}
	public String getSpeedText() {
		return speedText;
	}
	public void setSpeedText(String speedText) {
		this.speedText = speedText;
	}
	public String getEvenMoreSpeedText() {
		return evenMoreSpeedText;
	}
	public void setEvenMoreSpeedText(String evenMoreSpeedText) {
		this.evenMoreSpeedText = evenMoreSpeedText;
	}
	public String getBoardingPassOnlyText() {
		return boardingPassOnlyText;
	}
	public void setBoardingPassOnlyText(String boardingPassOnlyText) {
		this.boardingPassOnlyText = boardingPassOnlyText;
	}
	public String getPlusInfantText() {
		return plusInfantText;
	}
	public void setPlusInfantText(String plusInfantText) {
		this.plusInfantText = plusInfantText;
	}
	public String getChildText() {
		return childText;
	}
	public void setChildText(String childText) {
		this.childText = childText;
	}
	public String getThruText() {
		return thruText;
	}
	public void setThruText(String thruText) {
		this.thruText = thruText;
	}
	public String getPremiumText() {
		return premiumText;
	}
	public void setPremiumText(String premiumText) {
		this.premiumText = premiumText;
	}
	public String getExitText() {
		return exitText;
	}
	public void setExitText(String exitText) {
		this.exitText = exitText;
	}
	public String getTboText() {
		return tboText;
	}
	public void setTboText(String tboText) {
		this.tboText = tboText;
	}
	public String getTboCityCode() {
		return tboCityCode;
	}
	public void setTboCityCode(String tboCityCode) {
		this.tboCityCode = tboCityCode;
	}
	public String getCabinText() {
		return cabinText;
	}
	public void setCabinText(String cabinText) {
		this.cabinText = cabinText;
	}
	public String getExclusiveWaitingArea() {
		return exclusiveWaitingArea;
	}
	public void setExclusiveWaitingArea(String exclusiveWaitingArea) {
		this.exclusiveWaitingArea = exclusiveWaitingArea;
	}
	public String getLoungeAccess() {
		return loungeAccess;
	}
	public void setLoungeAccess(String loungeAccess) {
		this.loungeAccess = loungeAccess;
	}
	public String getSpanishResident() {
		return spanishResident;
	}
	public void setSpanishResident(String spanishResident) {
		this.spanishResident = spanishResident;
	}
	public String getSpanishLargeFamily() {
		return spanishLargeFamily;
	}
	public void setSpanishLargeFamily(String spanishLargeFamily) {
		this.spanishLargeFamily = spanishLargeFamily;
	}
	public List<AncillaryList> getAncillaryList() {
		return ancillaryList;
	}
	public void setAncillaryList(List<AncillaryList> ancillaryList) {
		this.ancillaryList = ancillaryList;
	}

}
