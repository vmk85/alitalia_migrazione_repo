
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubCodeOfBaggageItemList {

    @SerializedName("maxCount")
    @Expose
    private Long maxCount;
    @SerializedName("maxCountSpecified")
    @Expose
    private Boolean maxCountSpecified;
    @SerializedName("subCode")
    @Expose
    private String subCode;
    @SerializedName("descriptions")
    @Expose
    private Descriptions_ descriptions;
    @SerializedName("subGroup")
    @Expose
    private SubGroup_ subGroup;
    @SerializedName("vendor")
    @Expose
    private String vendor;
    @SerializedName("owningCarrier")
    @Expose
    private String owningCarrier;
    @SerializedName("airline")
    @Expose
    private String airline;
    @SerializedName("emdType")
    @Expose
    private String emdType;
    @SerializedName("rficCode")
    @Expose
    private String rficCode;
    @SerializedName("ancillaryServiceType")
    @Expose
    private String ancillaryServiceType;
    @SerializedName("baggageWeightAndSize")
    @Expose
    private BaggageWeightAndSize_ baggageWeightAndSize;
    @SerializedName("freeTextInfoList")
    @Expose
    private List<FreeTextInfoList__> freeTextInfoList = null;

    public Long getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(Long maxCount) {
        this.maxCount = maxCount;
    }

    public Boolean getMaxCountSpecified() {
        return maxCountSpecified;
    }

    public void setMaxCountSpecified(Boolean maxCountSpecified) {
        this.maxCountSpecified = maxCountSpecified;
    }

    public String getSubCode() {
        return subCode;
    }

    public void setSubCode(String subCode) {
        this.subCode = subCode;
    }

    public Descriptions_ getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(Descriptions_ descriptions) {
        this.descriptions = descriptions;
    }

    public SubGroup_ getSubGroup() {
        return subGroup;
    }

    public void setSubGroup(SubGroup_ subGroup) {
        this.subGroup = subGroup;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getOwningCarrier() {
        return owningCarrier;
    }

    public void setOwningCarrier(String owningCarrier) {
        this.owningCarrier = owningCarrier;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getEmdType() {
        return emdType;
    }

    public void setEmdType(String emdType) {
        this.emdType = emdType;
    }

    public String getRficCode() {
        return rficCode;
    }

    public void setRficCode(String rficCode) {
        this.rficCode = rficCode;
    }

    public String getAncillaryServiceType() {
        return ancillaryServiceType;
    }

    public void setAncillaryServiceType(String ancillaryServiceType) {
        this.ancillaryServiceType = ancillaryServiceType;
    }

    public BaggageWeightAndSize_ getBaggageWeightAndSize() {
        return baggageWeightAndSize;
    }

    public void setBaggageWeightAndSize(BaggageWeightAndSize_ baggageWeightAndSize) {
        this.baggageWeightAndSize = baggageWeightAndSize;
    }

    public List<FreeTextInfoList__> getFreeTextInfoList() {
        return freeTextInfoList;
    }

    public void setFreeTextInfoList(List<FreeTextInfoList__> freeTextInfoList) {
        this.freeTextInfoList = freeTextInfoList;
    }

}
