package com.alitalia.aem.common.data.home;

import java.util.List;

public class ResultTicketingDetailSolutionSliceData {

    private Integer durationField;
    private Boolean durationFieldSpecified;
    private ResultTicketingDetailSolutionSliceExtData extField;
    private List<ResultTicketingDetailSolutionSliceSegmentData> segmentField;
    private Integer stopCountField;

    public Integer getDurationField() {
        return durationField;
    }

    public void setDurationField(int value) {
        this.durationField = value;
    }

    public Boolean isDurationFieldSpecified() {
        return durationFieldSpecified;
    }

    public void setDurationFieldSpecified(Boolean value) {
        this.durationFieldSpecified = value;
    }

    public ResultTicketingDetailSolutionSliceExtData getExtField() {
        return extField;
    }

    public void setExtField(ResultTicketingDetailSolutionSliceExtData value) {
        this.extField = value;
    }

    public List<ResultTicketingDetailSolutionSliceSegmentData> getSegmentField() {
        return segmentField;
    }

    public void setSegmentField(List<ResultTicketingDetailSolutionSliceSegmentData> value) {
        this.segmentField = value;
    }

    public Integer getStopCountField() {
        return stopCountField;
    }

    public void setStopCountField(int value) {
        this.stopCountField = value;
    }

}
