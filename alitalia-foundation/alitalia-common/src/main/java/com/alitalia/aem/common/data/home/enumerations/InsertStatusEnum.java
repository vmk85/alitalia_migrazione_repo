package com.alitalia.aem.common.data.home.enumerations;

public enum InsertStatusEnum {

	NOT_INSERTED("NotInserted"),
	INSERTED("Inserted"),
	UPDATED("Updated"),
	NOT_UPDATED("NotUpdated"),
	EXISTING("Existing"),
	NOT_EXISTING("NotExisting"),
	INVALID_USER("InvalidUser");

	private final String value;

	InsertStatusEnum(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	public static InsertStatusEnum fromValue(String v) {
		for (InsertStatusEnum c: InsertStatusEnum.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}