
package com.alitalia.aem.common.data.checkinrest.model.updatesecurityinfo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ApisInfoList {

    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("aqq")
    @Expose
    private String aqq;
    @SerializedName("esta")
    @Expose
    private String esta;
    @SerializedName("sf")
    @Expose
    private String sf;
    @SerializedName("dcar")
    @Expose
    private String dcar;
    @SerializedName("dcad")
    @Expose
    private String dcad;
    @SerializedName("doco")
    @Expose
    private String doco;
    @SerializedName("docv")
    @Expose
    private String docv;
    @SerializedName("pctc")
    @Expose
    private String pctc;
    @SerializedName("docs")
    @Expose
    private String docs;
    @SerializedName("docType")
    @Expose
    private String docType;
    @SerializedName("api")
    @Expose
    private String api;
    @SerializedName("iapp")
    @Expose
    private String iapp;
    @SerializedName("tim")
    @Expose
    private String tim;
    @SerializedName("twapp")
    @Expose
    private String twapp;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAqq() {
        return aqq;
    }

    public void setAqq(String aqq) {
        this.aqq = aqq;
    }

    public String getEsta() {
        return esta;
    }

    public void setEsta(String esta) {
        this.esta = esta;
    }

    public String getSf() {
        return sf;
    }

    public void setSf(String sf) {
        this.sf = sf;
    }

    public String getDcar() {
        return dcar;
    }

    public void setDcar(String dcar) {
        this.dcar = dcar;
    }

    public String getDcad() {
        return dcad;
    }

    public void setDcad(String dcad) {
        this.dcad = dcad;
    }

    public String getDoco() {
        return doco;
    }

    public void setDoco(String doco) {
        this.doco = doco;
    }

    public String getDocv() {
        return docv;
    }

    public void setDocv(String docv) {
        this.docv = docv;
    }

    public String getPctc() {
        return pctc;
    }

    public void setPctc(String pctc) {
        this.pctc = pctc;
    }

    public String getDocs() {
        return docs;
    }

    public void setDocs(String docs) {
        this.docs = docs;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }

    public String getIapp() {
        return iapp;
    }

    public void setIapp(String iapp) {
        this.iapp = iapp;
    }

    public String getTim() {
        return tim;
    }

    public void setTim(String tim) {
        this.tim = tim;
    }

    public String getTwapp() {
        return twapp;
    }

    public void setTwapp(String twapp) {
        this.twapp = twapp;
    }

}
