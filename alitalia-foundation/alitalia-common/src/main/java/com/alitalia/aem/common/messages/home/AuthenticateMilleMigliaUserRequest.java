package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class AuthenticateMilleMigliaUserRequest extends BaseRequest {

	private String languageCode;
    private String marketCode;
    private String mmCode;
    private String mmPin;
	
    public String getLanguageCode() {
		return languageCode;
	}
	
    public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	
    public String getMarketCode() {
		return marketCode;
	}
	
    public void setMarketCode(String marketCode) {
		this.marketCode = marketCode;
	}
	
    public String getMmCode() {
		return mmCode;
	}
	
    public void setMmCode(String mmCode) {
		this.mmCode = mmCode;
	}
	
    public String getMmPin() {
		return mmPin;
	}
	
    public void setMmPin(String mmPin) {
		this.mmPin = mmPin;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((languageCode == null) ? 0 : languageCode.hashCode());
		result = prime * result
				+ ((marketCode == null) ? 0 : marketCode.hashCode());
		result = prime * result + ((mmCode == null) ? 0 : mmCode.hashCode());
		result = prime * result + ((mmPin == null) ? 0 : mmPin.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthenticateMilleMigliaUserRequest other = (AuthenticateMilleMigliaUserRequest) obj;
		if (languageCode == null) {
			if (other.languageCode != null)
				return false;
		} else if (!languageCode.equals(other.languageCode))
			return false;
		if (marketCode == null) {
			if (other.marketCode != null)
				return false;
		} else if (!marketCode.equals(other.marketCode))
			return false;
		if (mmCode == null) {
			if (other.mmCode != null)
				return false;
		} else if (!mmCode.equals(other.mmCode))
			return false;
		if (mmPin == null) {
			if (other.mmPin != null)
				return false;
		} else if (!mmPin.equals(other.mmPin))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AuthenticateMilleMigliaUserRequest [languageCode="
				+ languageCode + ", marketCode=" + marketCode + ", mmCode="
				+ mmCode + ", mmPin=" + mmPin + "]";
	}	
}