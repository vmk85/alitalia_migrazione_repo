package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class AgencySubscriptionRequest extends BaseRequest {

	private String nomeCompagnia;
	private String partitaIva;
	private String codiceFiscale;
	private String indirizzo;
	private String citta;
	private String provincia;
	private String cap;
	private String stato;
	private String numeroTelefono;
	private String numeroFax;
	private String emailTitolare;
	private String emailOperatore;
	
	public String getNomeCompagnia() {
		return nomeCompagnia;
	}
	
	public void setNomeCompagnia(String nomeCompagnia) {
		this.nomeCompagnia = nomeCompagnia;
	}
	
	public String getPartitaIva() {
		return partitaIva;
	}
	
	public void setPartitaIva(String partitaIva) {
		this.partitaIva = partitaIva;
	}
	
	public String getCodiceFiscale() {
		return codiceFiscale;
	}
	
	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}
	
	public String getIndirizzo() {
		return indirizzo;
	}
	
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	
	public String getCitta() {
		return citta;
	}
	
	public void setCitta(String citta) {
		this.citta = citta;
	}
	
	public String getProvincia() {
		return provincia;
	}
	
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	
	public String getCap() {
		return cap;
	}
	
	public void setCap(String cap) {
		this.cap = cap;
	}
	
	public String getStato() {
		return stato;
	}
	
	public void setStato(String stato) {
		this.stato = stato;
	}
	
	public String getNumeroTelefono() {
		return numeroTelefono;
	}
	
	public void setNumeroTelefono(String numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}
	
	public String getNumeroFax() {
		return numeroFax;
	}
	
	public void setNumeroFax(String numeroFax) {
		this.numeroFax = numeroFax;
	}
	
	public String getEmailTitolare() {
		return emailTitolare;
	}
	
	public void setEmailTitolare(String emailTitolare) {
		this.emailTitolare = emailTitolare;
	}
	
	public String getEmailOperatore() {
		return emailOperatore;
	}
	
	public void setEmailOperatore(String emailOperatore) {
		this.emailOperatore = emailOperatore;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((codiceFiscale == null) ? 0 : codiceFiscale.hashCode());
		result = prime * result + ((partitaIva == null) ? 0 : partitaIva.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgencySubscriptionRequest other = (AgencySubscriptionRequest) obj;
		if (codiceFiscale == null) {
			if (other.codiceFiscale != null)
				return false;
		} else if (!codiceFiscale.equals(other.codiceFiscale))
			return false;
		if (partitaIva == null) {
			if (other.partitaIva != null)
				return false;
		} else if (!partitaIva.equals(other.partitaIva))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AgencySubscriptionRequest [companyname=" + nomeCompagnia
				+ ", piva=" + partitaIva + ", codicefiscale=" + codiceFiscale
				+ ", address=" + indirizzo + ", city=" + citta + ", provincia="
				+ provincia + ", postalcode=" + cap + ", state=" + stato
				+ ", telephonenumber=" + numeroTelefono + ", faxnumber="
				+ numeroFax + ", emailtitolare=" + emailTitolare
				+ ", emailoperatore=" + emailOperatore + "]";
	}
}