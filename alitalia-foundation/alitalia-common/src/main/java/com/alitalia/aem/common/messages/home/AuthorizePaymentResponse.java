package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.InsurancePolicyData;
import com.alitalia.aem.common.data.home.PaymentProcessInfoData;
import com.alitalia.aem.common.messages.BaseResponse;

public class AuthorizePaymentResponse extends BaseResponse {
	
	private InsurancePolicyData insurance;
	private PaymentProcessInfoData process;
	private String pnr;
	private String errorStatisticMessage;

	public InsurancePolicyData getInsurance() {
		return insurance;
	}
	public void setInsurance(InsurancePolicyData insurance) {
		this.insurance = insurance;
	}
	public PaymentProcessInfoData getProcess() {
		return process;
	}
	public void setProcess(PaymentProcessInfoData process) {
		this.process = process;
	}
	public String getPnr() {
		return pnr;
	}
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	public String getErrorStatisticMessage() {
		return errorStatisticMessage;
	}
	public void setErrorStatisticMessage(String errorStatisticMessage) {
		this.errorStatisticMessage = errorStatisticMessage;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((errorStatisticMessage == null) ? 0 : errorStatisticMessage
						.hashCode());
		result = prime * result
				+ ((insurance == null) ? 0 : insurance.hashCode());
		result = prime * result + ((pnr == null) ? 0 : pnr.hashCode());
		result = prime * result + ((process == null) ? 0 : process.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthorizePaymentResponse other = (AuthorizePaymentResponse) obj;
		if (errorStatisticMessage == null) {
			if (other.errorStatisticMessage != null)
				return false;
		} else if (!errorStatisticMessage.equals(other.errorStatisticMessage))
			return false;
		if (insurance == null) {
			if (other.insurance != null)
				return false;
		} else if (!insurance.equals(other.insurance))
			return false;
		if (pnr == null) {
			if (other.pnr != null)
				return false;
		} else if (!pnr.equals(other.pnr))
			return false;
		if (process == null) {
			if (other.process != null)
				return false;
		} else if (!process.equals(other.process))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "AuthorizePaymentResponse [insurance=" + insurance
				+ ", process=" + process + ", pnr=" + pnr
				+ ", errorStatisticMessage=" + errorStatisticMessage + "]";
	}
}