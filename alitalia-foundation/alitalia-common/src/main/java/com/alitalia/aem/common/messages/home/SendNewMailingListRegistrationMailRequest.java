package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.BaseRequest;

public class SendNewMailingListRegistrationMailRequest extends BaseRequest {

	private MMCustomerProfileData customer;
	private String mailBody;
	private Boolean mailSent;
	private String mailSubject;

	public MMCustomerProfileData getCustomer() {
		return customer;
	}

	public void setCustomer(MMCustomerProfileData customer) {
		this.customer = customer;
	}

	public String getMailBody() {
		return mailBody;
	}

	public void setMailBody(String mailBody) {
		this.mailBody = mailBody;
	}

	public Boolean getMailSent() {
		return mailSent;
	}

	public void setMailSent(Boolean mailSent) {
		this.mailSent = mailSent;
	}

	public String getMailSubject() {
		return mailSubject;
	}

	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((customer == null) ? 0 : customer.hashCode());
		result = prime * result
				+ ((mailBody == null) ? 0 : mailBody.hashCode());
		result = prime * result
				+ ((mailSent == null) ? 0 : mailSent.hashCode());
		result = prime * result
				+ ((mailSubject == null) ? 0 : mailSubject.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SendNewMailingListRegistrationMailRequest other = (SendNewMailingListRegistrationMailRequest) obj;
		if (customer == null) {
			if (other.customer != null)
				return false;
		} else if (!customer.equals(other.customer))
			return false;
		if (mailBody == null) {
			if (other.mailBody != null)
				return false;
		} else if (!mailBody.equals(other.mailBody))
			return false;
		if (mailSent == null) {
			if (other.mailSent != null)
				return false;
		} else if (!mailSent.equals(other.mailSent))
			return false;
		if (mailSubject == null) {
			if (other.mailSubject != null)
				return false;
		} else if (!mailSubject.equals(other.mailSubject))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SendNewMailingListRegistrationMailRequest [customer="
				+ customer + ", mailBody=" + mailBody + ", mailSent="
				+ mailSent + ", mailSubject=" + mailSubject + "]";
	}

}
