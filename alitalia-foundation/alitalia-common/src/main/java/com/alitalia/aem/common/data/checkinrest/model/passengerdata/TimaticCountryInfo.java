
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TimaticCountryInfo {

    @SerializedName("timaticStatus")
    @Expose
    private String timaticStatus;
    @SerializedName("country")
    @Expose
    private Country country;
    @SerializedName("sectionInfo")
    @Expose
    private List<SectionInfo> sectionInfo = null;

    public String getTimaticStatus() {
        return timaticStatus;
    }

    public void setTimaticStatus(String timaticStatus) {
        this.timaticStatus = timaticStatus;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public List<SectionInfo> getSectionInfo() {
        return sectionInfo;
    }

    public void setSectionInfo(List<SectionInfo> sectionInfo) {
        this.sectionInfo = sectionInfo;
    }

}
