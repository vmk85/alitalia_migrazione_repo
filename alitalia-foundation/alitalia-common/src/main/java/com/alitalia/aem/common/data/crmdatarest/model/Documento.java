package com.alitalia.aem.common.data.crmdatarest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Documento {

    @SerializedName("tipo")
    @Expose
    private String tipo;

    @SerializedName("numero")
    @Expose
    private String numero;

    @SerializedName("dataEmissione")
    @Expose
    private String dataEmissione;

    @SerializedName("dataScadenza")
    @Expose
    private String dataScadenza;

    @SerializedName("paeseEmissione")
    @Expose
    private String paeseEmissione;

    @SerializedName("nazionalita")
    @Expose
    private String nazionalita;

    @SerializedName("dataNascitaPasseggero")
    @Expose
    private String dataNascitaPasseggero;

    @SerializedName("paeseValiditaVisto")
    @Expose
    private String paeseValiditaVisto;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getDataEmissione() {
        return dataEmissione;
    }

    public void setDataEmissione(String dataEmissione) {
        this.dataEmissione = dataEmissione;
    }

    public String getDataScadenza() {
        return dataScadenza;
    }

    public void setDataScadenza(String dataScadenza) {
        this.dataScadenza = dataScadenza;
    }

    public String getPaeseEmissione() {
        return paeseEmissione;
    }

    public void setPaeseEmissione(String paeseEmissione) {
        this.paeseEmissione = paeseEmissione;
    }

    public String getNazionalita() {
        return nazionalita;
    }

    public void setNazionalita(String nazionalità) {
        this.nazionalita = nazionalità;
    }

    public String getDataNascitaPasseggero() {
        return dataNascitaPasseggero;
    }

    public void setDataNascitaPasseggero(String dataNascitaPasseggero) {
        this.dataNascitaPasseggero = dataNascitaPasseggero;
    }

    public String getPaeseValiditaVisto() {
        return paeseValiditaVisto;
    }

    public void setPaeseValiditaVisto(String paeseValiditaVisto) {
        this.paeseValiditaVisto = paeseValiditaVisto;
    }

    @Override
    public String toString() {
        return "ElencoDocumenti{" +
                "tipo='" + tipo + '\'' +
                ", numero='" + numero + '\'' +
                ", dataEmissione='" + dataEmissione + '\'' +
                ", dataScadenza='" + dataScadenza + '\'' +
                ", paeseEmissione='" + paeseEmissione + '\'' +
                ", nazionalità='" + nazionalita + '\'' +
                ", dataNascitaPasseggero='" + dataNascitaPasseggero + '\'' +
                ", paeseValiditaVisto='" + paeseValiditaVisto + '\'' +
                '}';
    }
}
