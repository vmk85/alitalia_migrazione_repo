package com.alitalia.aem.common.data.home;

import com.alitalia.aem.common.data.home.enumerations.CabinEnum;

public class SeatPreferencesData {

	private CabinEnum cabinSeat;
	private String flightCarrier;
	private String flightNumber;
	private String number;
	private String row;
	
	public SeatPreferencesData() {
		super();
	}

	public SeatPreferencesData(SeatPreferencesData clone) {
		this.cabinSeat = clone.getCabinSeat();
		this.flightCarrier = clone.getFlightCarrier();
		this.flightNumber = clone.getFlightNumber();
		this.number = clone.getNumber();
		this.row = clone.getRow();
	}

	public CabinEnum getCabinSeat() {
		return cabinSeat;
	}
	
	public void setCabinSeat(CabinEnum cabinSeat) {
		this.cabinSeat = cabinSeat;
	}
	
	public String getFlightCarrier() {
		return flightCarrier;
	}
	
	public void setFlightCarrier(String flightCarrier) {
		this.flightCarrier = flightCarrier;
	}
	
	public String getFlightNumber() {
		return flightNumber;
	}
	
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	
	public String getNumber() {
		return number;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	
	public String getRow() {
		return row;
	}
	
	public void setRow(String row) {
		this.row = row;
	}

	@Override
	public String toString() {
		return "SeatPreferencesData [cabinSeat=" + cabinSeat
				+ ", flightCarrier=" + flightCarrier + ", flightNumber="
				+ flightNumber + ", number=" + number + ", row=" + row + "]";
	}
}
