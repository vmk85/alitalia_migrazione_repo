package com.alitalia.aem.common.data.checkinrest.model.passenger;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItineraryDetail {
	
	@SerializedName("operatingAirline")
	@Expose
	private OperatingAirline operatingAirline;
	@SerializedName("marketingAirline")
	@Expose
	private MarketingAirline marketingAirline;
	@SerializedName("gate")
	@Expose
	private String gate;
	@SerializedName("terminal")
	@Expose
	private String terminal;
	@SerializedName("boardingTime")
	@Expose
	private String boardingTime;
	@SerializedName("doorCloseTime")
	@Expose
	private String doorCloseTime;
	@SerializedName("origin")
	@Expose
	private String origin;
	@SerializedName("originCity")
	@Expose
	private String originCity;
	@SerializedName("originCountry")
	@Expose
	private String originCountry;
	@SerializedName("thru")
	@Expose
	private String thru;
	@SerializedName("thruCity")
	@Expose
	private String thruCity;
	@SerializedName("thruCountry")
	@Expose
	private String thruCountry;
	@SerializedName("scheduledDepartureDate")
	@Expose
	private String scheduledDepartureDate;
	@SerializedName("scheduledDepartureTime")
	@Expose
	private String scheduledDepartureTime;
	@SerializedName("estimatedDepartureDate")
	@Expose
	private String estimatedDepartureDate;
	@SerializedName("estimatedDepartureTime")
	@Expose
	private String estimatedDepartureTime;
	@SerializedName("julianDepartureDate")
	@Expose
	private String julianDepartureDate;
	@SerializedName("clockType")
	@Expose
	private String clockType;
	@SerializedName("destination")
	@Expose
	private String destination;
	@SerializedName("destinationCity")
	@Expose
	private String destinationCity;
	@SerializedName("destinationCountry")
	@Expose
	private String destinationCountry;
	@SerializedName("scheduledArrivalTime")
	@Expose
	private String scheduledArrivalTime;
	@SerializedName("estimatedArrivalTime")
	@Expose
	private String estimatedArrivalTime;
	@SerializedName("cabin")
	@Expose
	private String cabin;
	@SerializedName("deck")
	@Expose
	private String deck;
	@SerializedName("bookingCode")
	@Expose
	private String bookingCode;
	@SerializedName("checkInSequence")
	@Expose
	private String checkInSequence;
	@SerializedName("seatInfoList")
	@Expose
	private List<SeatInfoList> seatInfoList = null;
	@SerializedName("extraSeatInfo")
	@Expose
	private String extraSeatInfo;
	@SerializedName("codeshare")
	@Expose
	private String codeshare;
	@SerializedName("wetleaseOperatedBy")
	@Expose
	private String wetleaseOperatedBy;
	@SerializedName("wetleaseCarrier")
	@Expose
	private String wetleaseCarrier;
	@SerializedName("wetleaseFlightConfig")
	@Expose
	private String wetleaseFlightConfig;
	@SerializedName("commuterOperatedByText")
	@Expose
	private String commuterOperatedByText;
	@SerializedName("commuterCarrier")
	@Expose
	private String commuterCarrier;
	@SerializedName("frequentTravel")
	@Expose
	private FrequentTravel frequentTravel;
	@SerializedName("totalBaggageWeight")
	@Expose
	private TotalBaggageWeight totalBaggageWeight;
	@SerializedName("totalCarryOnWeight")
	@Expose
	private TotalCarryOnWeight totalCarryOnWeight;
	@SerializedName("bagTagNumber")
	@Expose
	private List<String> bagTagNumber = null;
	@SerializedName("piecesOfBaggage")
	@Expose
	private String piecesOfBaggage;
	@SerializedName("priorityInfo")
	@Expose
	private PriorityInfo priorityInfo;
	@SerializedName("dhsStatusCode")
	@Expose
	private String dhsStatusCode;
	@SerializedName("selectee")
	@Expose
	private String selectee;
	@SerializedName("tsaPreCheckText")
	@Expose
	private String tsaPreCheckText;
	@SerializedName("etciapiText")
	@Expose
	private String etciapiText;
	@SerializedName("groupZone")
	@Expose
	private String groupZone;
	@SerializedName("brandedFare")
	@Expose
	private String brandedFare;
	@SerializedName("carryOnText")
	@Expose
	private String carryOnText;
	@SerializedName("flightCouponText")
	@Expose
	private String flightCouponText;
	@SerializedName("ticketText")
	@Expose
	private String ticketText;
	@SerializedName("vcrNumber")
	@Expose
	private VcrNumberPassenger vcrNumber;
	@SerializedName("documentType")
	@Expose
	private String documentType;
	@SerializedName("ssrCode")
	@Expose
	private List<String> ssrCode = null;
	@SerializedName("pingTip")
	@Expose
	private String pingTip;
	@SerializedName("speedText")
	@Expose
	private String speedText;
	@SerializedName("evenMoreSpeedText")
	@Expose
	private String evenMoreSpeedText;
	@SerializedName("boardingPassOnlyText")
	@Expose
	private String boardingPassOnlyText;
	@SerializedName("plusInfantText")
	@Expose
	private String plusInfantText;
	@SerializedName("childText")
	@Expose
	private String childText;
	@SerializedName("thruText")
	@Expose
	private String thruText;
	@SerializedName("premiumText")
	@Expose
	private String premiumText;
	@SerializedName("exitText")
	@Expose
	private String exitText;
	@SerializedName("tboText")
	@Expose
	private String tboText;
	@SerializedName("tboCityCode")
	@Expose
	private String tboCityCode;
	@SerializedName("cabinText")
	@Expose
	private String cabinText;
	@SerializedName("exclusiveWaitingArea")
	@Expose
	private String exclusiveWaitingArea;
	@SerializedName("loungeAccess")
	@Expose
	private String loungeAccess;
	@SerializedName("spanishResident")
	@Expose
	private String spanishResident;
	@SerializedName("spanishLargeFamily")
	@Expose
	private String spanishLargeFamily;
	@SerializedName("ancillaryList")
	@Expose
	private List<AncillaryList> ancillaryList = null;

}
