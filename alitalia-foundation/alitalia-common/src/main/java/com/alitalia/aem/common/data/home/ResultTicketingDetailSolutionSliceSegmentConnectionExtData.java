package com.alitalia.aem.common.data.home;

public class ResultTicketingDetailSolutionSliceSegmentConnectionExtData {

    private ResultTicketingDetailSolutionSliceSegmentConnectionExtWarningData warningField;

    public ResultTicketingDetailSolutionSliceSegmentConnectionExtWarningData getWarningField() {
        return warningField;
    }

    public void setWarningField(ResultTicketingDetailSolutionSliceSegmentConnectionExtWarningData value) {
        this.warningField = value;
    }

}
