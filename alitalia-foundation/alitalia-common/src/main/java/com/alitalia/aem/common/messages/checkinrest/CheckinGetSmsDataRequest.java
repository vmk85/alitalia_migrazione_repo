package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.messages.BaseRequest;

public class CheckinGetSmsDataRequest extends BaseRequest {

    private String key;
    private String language;
    private String market;
    private String conversationID;

    public CheckinGetSmsDataRequest() {}

    public CheckinGetSmsDataRequest(String tid, String sid) {
        super(tid, sid);
    }


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }
}
