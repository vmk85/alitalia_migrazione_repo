package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.clearancillariescart.response.ClearAncillariesCart;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinClearAncillariesCartResponse extends BaseResponse{
    private ClearAncillariesCart clearAncillariesCartResponse;

    public CheckinClearAncillariesCartResponse() {}

    public CheckinClearAncillariesCartResponse(String tid, String sid) {
        super(tid, sid);
    }

    public ClearAncillariesCart getClearAncillariesCart() {
        return clearAncillariesCartResponse;
    }

    public void setClearAncillariesCart(ClearAncillariesCart clearAncillariesCartResponse) {
        this.clearAncillariesCartResponse = clearAncillariesCartResponse;
    }
}
