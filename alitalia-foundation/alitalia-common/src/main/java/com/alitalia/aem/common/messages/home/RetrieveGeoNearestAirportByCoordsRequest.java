package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class RetrieveGeoNearestAirportByCoordsRequest extends BaseRequest {

    private String latitude;
    private String longitude;
    private String countryCode;
    private Integer maxDistanceKM;
	
    public String getLatitude() {
		return latitude;
	}
    
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	
	public String getLongitude() {
		return longitude;
	}
	
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
	public String getCountryCode() {
		return countryCode;
	}
	
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
	public Integer getMaxDistanceKM() {
		return maxDistanceKM;
	}
	
	public void setMaxDistanceKM(Integer maxDistanceKM) {
		this.maxDistanceKM = maxDistanceKM;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((countryCode == null) ? 0 : countryCode.hashCode());
		result = prime * result + ((latitude == null) ? 0 : latitude.hashCode());
		result = prime * result + ((longitude == null) ? 0 : longitude.hashCode());
		result = prime * result + ((maxDistanceKM == null) ? 0 : maxDistanceKM.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveGeoNearestAirportByCoordsRequest other = (RetrieveGeoNearestAirportByCoordsRequest) obj;
		if (countryCode == null) {
			if (other.countryCode != null)
				return false;
		} else if (!countryCode.equals(other.countryCode))
			return false;
		if (latitude == null) {
			if (other.latitude != null)
				return false;
		} else if (!latitude.equals(other.latitude))
			return false;
		if (longitude == null) {
			if (other.longitude != null)
				return false;
		} else if (!longitude.equals(other.longitude))
			return false;
		if (maxDistanceKM == null) {
			if (other.maxDistanceKM != null)
				return false;
		} else if (!maxDistanceKM.equals(other.maxDistanceKM))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveGeoNearestAirportByCoordsRequest [latitude=" + latitude + ", longitude=" + longitude
				+ ", countryCode=" + countryCode + ", maxDistanceKM=" + maxDistanceKM + "]";
	}
    
}