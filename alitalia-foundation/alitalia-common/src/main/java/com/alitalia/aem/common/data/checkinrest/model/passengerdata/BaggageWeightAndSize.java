
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaggageWeightAndSize {

    @SerializedName("weightInfo")
    @Expose
    private List<WeightInfo> weightInfo = null;
    @SerializedName("sizeInfo")
    @Expose
    private List<SizeInfo> sizeInfo = null;

    public List<WeightInfo> getWeightInfo() {
        return weightInfo;
    }

    public void setWeightInfo(List<WeightInfo> weightInfo) {
        this.weightInfo = weightInfo;
    }

    public List<SizeInfo> getSizeInfo() {
        return sizeInfo;
    }

    public void setSizeInfo(List<SizeInfo> sizeInfo) {
        this.sizeInfo = sizeInfo;
    }

}
