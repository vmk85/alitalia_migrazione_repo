
package com.alitalia.aem.common.data.checkinrest.model.updatepassengerdetails.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdatePassengerDetailsResp {

    @SerializedName("freeTextInfoList")
    @Expose
    private List<FreeTextInfoList> freeTextInfoList = null;
    @SerializedName("itineraryResponseList")
    @Expose
    private List<ItineraryResponseList> itineraryResponseList = null;
    @SerializedName("passengerInfoResponseList")
    @Expose
    private List<PassengerInfoResponseList> passengerInfoResponseList = null;
    @SerializedName("result")
    @Expose
    private Result result;

    public List<FreeTextInfoList> getFreeTextInfoList() {
        return freeTextInfoList;
    }

    public void setFreeTextInfoList(List<FreeTextInfoList> freeTextInfoList) {
        this.freeTextInfoList = freeTextInfoList;
    }

    public List<ItineraryResponseList> getItineraryResponseList() {
        return itineraryResponseList;
    }

    public void setItineraryResponseList(List<ItineraryResponseList> itineraryResponseList) {
        this.itineraryResponseList = itineraryResponseList;
    }

    public List<PassengerInfoResponseList> getPassengerInfoResponseList() {
        return passengerInfoResponseList;
    }

    public void setPassengerInfoResponseList(List<PassengerInfoResponseList> passengerInfoResponseList) {
        this.passengerInfoResponseList = passengerInfoResponseList;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

}
