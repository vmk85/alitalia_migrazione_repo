package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.checkin.CheckinPaymentTransactionInfoData;
import com.alitalia.aem.common.messages.BaseResponse;

public class PaymentCheckinResponse extends BaseResponse {

	private CheckinPaymentTransactionInfoData transactionInfo;

	public CheckinPaymentTransactionInfoData getTransactionInfo() {
		return transactionInfo;
	}

	public void setTransactionInfo(CheckinPaymentTransactionInfoData transactionInfo) {
		this.transactionInfo = transactionInfo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((transactionInfo == null) ? 0 : transactionInfo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentCheckinResponse other = (PaymentCheckinResponse) obj;
		if (transactionInfo == null) {
			if (other.transactionInfo != null)
				return false;
		} else if (!transactionInfo.equals(other.transactionInfo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PaymentCheckinResponse [transactionInfo=" + transactionInfo
				+ ", toString()=" + super.toString() + "]";
	}

}
