package com.alitalia.aem.common.data.home.countries;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCountry {
	
	@SerializedName("countryID")
	@Expose
	private String countryID;
	@SerializedName("countryCode")
	@Expose
	private String countryCode;
	@SerializedName("countryName")
	@Expose
	private String countryName;
	@SerializedName("countryISOCode")
	@Expose
	private String countryISOCode;
	
	public String getCountryID() {
		return countryID;
	}
	
	public void setCountryID(String countryID) {
		this.countryID = countryID;
	}
	
	public String getCountryCode() {
		return countryCode;
	}
	
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
	public String getCountryName() {
		return countryName;
	}
	
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	
	public String getCountryISOCode() {
		return countryISOCode;
	}
	
	public void setCountryISOCode(String countryISOCode) {
		this.countryISOCode = countryISOCode;
	}

}
