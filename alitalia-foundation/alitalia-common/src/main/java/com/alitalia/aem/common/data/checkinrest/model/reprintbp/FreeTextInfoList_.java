
package com.alitalia.aem.common.data.checkinrest.model.reprintbp;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FreeTextInfoList_ {

    @SerializedName("lineID")
    @Expose
    private String lineID;
    @SerializedName("editCode")
    @Expose
    private String editCode;
    @SerializedName("textLine")
    @Expose
    private List<String> textLine = null;

    public String getLineID() {
        return lineID;
    }

    public void setLineID(String lineID) {
        this.lineID = lineID;
    }

    public String getEditCode() {
        return editCode;
    }

    public void setEditCode(String editCode) {
        this.editCode = editCode;
    }

    public List<String> getTextLine() {
        return textLine;
    }

    public void setTextLine(List<String> textLine) {
        this.textLine = textLine;
    }

}
