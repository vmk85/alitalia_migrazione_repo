package com.alitalia.aem.common.data.checkinrest.model.passenger;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FlightInfo {
	
	@SerializedName("airline")
	@Expose
	private String airline;
	
	@SerializedName("flight")
	@Expose
	private String flight;
	
	@SerializedName("origin")
	@Expose
	private String origin;
	
	@SerializedName("destination")
	@Expose
	private String destination;
	
	@SerializedName("departureDate")
	@Expose
	private String departureDate;
	
	@SerializedName("departureTime")
	@Expose
	private String departureTime;
	
	@SerializedName("departureGate")
	@Expose
	private String departureGate;
	
	@SerializedName("arrivalTime")
	@Expose
	private String arrivalTime;
	
	@SerializedName("status")
	@Expose
	private String status;
	
	@SerializedName("aircraftType")
	@Expose
	private AircraftType aircraftType;
	
	@SerializedName("aircraftConfigNumber")
	@Expose
	private String aircraftConfigNumber;
	
	@SerializedName("flightLegList")
	@Expose
	private List<FlightLegList> flightLegList = null;
	
	@SerializedName("groupBoardingInfo")
	@Expose
	private GroupBoardingInfo groupBoardingInfo;

	public String getAirline() {
		return airline;
	}

	public void setAirline(String airline) {
		this.airline = airline;
	}

	public String getFlight() {
		return flight;
	}

	public void setFlight(String flight) {
		this.flight = flight;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public String getDepartureGate() {
		return departureGate;
	}

	public void setDepartureGate(String departureGate) {
		this.departureGate = departureGate;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public AircraftType getAircraftType() {
		return aircraftType;
	}

	public void setAircraftType(AircraftType aircraftType) {
		this.aircraftType = aircraftType;
	}

	public String getAircraftConfigNumber() {
		return aircraftConfigNumber;
	}

	public void setAircraftConfigNumber(String aircraftConfigNumber) {
		this.aircraftConfigNumber = aircraftConfigNumber;
	}

	public List<FlightLegList> getFlightLegList() {
		return flightLegList;
	}

	public void setFlightLegList(List<FlightLegList> flightLegList) {
		this.flightLegList = flightLegList;
	}

	public GroupBoardingInfo getGroupBoardingInfo() {
		return groupBoardingInfo;
	}

	public void setGroupBoardingInfo(GroupBoardingInfo groupBoardingInfo) {
		this.groupBoardingInfo = groupBoardingInfo;
	}

}
