package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.BaseResponse;

public class MilleMigliaLoginResponse extends BaseResponse {

	private MMCustomerProfileData customerProfileData;
	private Boolean loginSuccessful;
	private String errorCode;
	private String errorDescription;
	private String ssoToken;

	public MilleMigliaLoginResponse() {
		super();
		this.loginSuccessful = false;
	}

	public MilleMigliaLoginResponse(MMCustomerProfileData customerProfileData, Boolean loginSuccessful,
			String errorCode, String errorDescription, String ssoToken) {
		super();
		this.customerProfileData = customerProfileData;
		this.loginSuccessful = loginSuccessful;
		this.errorCode = errorCode;
		this.errorDescription = errorDescription;
		this.setSsoToken(ssoToken);
	}

	public MilleMigliaLoginResponse(String tid, String sid,
			MMCustomerProfileData customerProfileData, Boolean loginSuccessful,
			String errorCode, String errorDescription) {
		super(tid, sid);
		this.customerProfileData = customerProfileData;
		this.loginSuccessful = loginSuccessful;
		this.errorCode = errorCode;
		this.errorDescription = errorDescription;
	}

	public MMCustomerProfileData getCustomerProfileData() {
		return customerProfileData;
	}

	public void setCustomerProfileData(MMCustomerProfileData customerProfileData) {
		this.customerProfileData = customerProfileData;
	}

	public Boolean isLoginSuccessful() {
		return loginSuccessful;
	}
	
	public void setLoginSuccessful(Boolean loginSuccessful) {
		this.loginSuccessful = loginSuccessful;
	}

	public String getErrorCode() {
		return errorCode;
	}
	
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	
	public String getErrorDescription() {
		return errorDescription;
	}
	
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public String getSsoToken() {
		return ssoToken;
	}

	public void setSsoToken(String ssoToken) {
		this.ssoToken = ssoToken;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((customerProfileData == null) ? 0 : customerProfileData
						.hashCode());
		result = prime * result
				+ ((errorCode == null) ? 0 : errorCode.hashCode());
		result = prime
				* result
				+ ((errorDescription == null) ? 0 : errorDescription.hashCode());
		result = prime * result
				+ ((loginSuccessful == null) ? 0 : loginSuccessful.hashCode());
		result = prime * result
				+ ((ssoToken == null) ? 0 : ssoToken.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MilleMigliaLoginResponse other = (MilleMigliaLoginResponse) obj;
		if (customerProfileData == null) {
			if (other.customerProfileData != null)
				return false;
		} else if (!customerProfileData.equals(other.customerProfileData))
			return false;
		if (errorCode == null) {
			if (other.errorCode != null)
				return false;
		} else if (!errorCode.equals(other.errorCode))
			return false;
		if (errorDescription == null) {
			if (other.errorDescription != null)
				return false;
		} else if (!errorDescription.equals(other.errorDescription))
			return false;
		if (loginSuccessful == null) {
			if (other.loginSuccessful != null)
				return false;
		} else if (!loginSuccessful.equals(other.loginSuccessful))
			return false;
		if (ssoToken == null) {
			if (other.ssoToken != null)
				return false;
		} else if (!ssoToken.equals(other.ssoToken))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MilleMigliaLoginResponse [customerProfileData="
				+ customerProfileData + ", loginSuccessful=" + loginSuccessful
				+ ", errorCode=" + errorCode + ", errorDescription="
				+ errorDescription + ", ssoToken=" + ssoToken + "]";
	}
}
