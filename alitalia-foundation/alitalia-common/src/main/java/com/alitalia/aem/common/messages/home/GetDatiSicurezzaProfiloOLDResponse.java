package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.enumerations.TipoProfiloEnum;
import com.alitalia.aem.common.messages.BaseResponse;

public class GetDatiSicurezzaProfiloOLDResponse extends BaseResponse {

    private String alias;
    private String codiceMM;
    private String pinMM;
    private boolean flagIDProfiloExists;
    private TipoProfiloEnum tipoProfilo;

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getCodiceMM() {
        return codiceMM;
    }

    public void setCodiceMM(String codiceMM) {
        this.codiceMM = codiceMM;
    }

    public String getPinMM() {
        return pinMM;
    }

    public void setPinMM(String pinMM) {
        this.pinMM = pinMM;
    }

    public boolean isFlagIDProfiloExists() {
        return flagIDProfiloExists;
    }

    public void setFlagIDProfiloExists(boolean flagIDProfiloExists) {
        this.flagIDProfiloExists = flagIDProfiloExists;
    }

    public TipoProfiloEnum getTipoProfilo() {
        return tipoProfilo;
    }

    public void setTipoProfilo(TipoProfiloEnum tipoProfilo) {
        this.tipoProfilo = tipoProfilo;
    }

    @Override
    public String toString() {
        return "GetDatiSicurezzaProfiloOLDResponse{" +
                "alias='" + alias + '\'' +
                ", codiceMM='" + codiceMM + '\'' +
                ", pinMM='" + pinMM + '\'' +
                ", flagIDProfiloExists=" + flagIDProfiloExists +
                ", tipoProfilo=" + tipoProfilo +
                '}';
    }
}
