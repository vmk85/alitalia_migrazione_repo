package com.alitalia.aem.common.data.checkinrest.model.passenger;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BoardingRow {
	
	@SerializedName("groupNumber")
	@Expose
	private String groupNumber;
	
	@SerializedName("startingRow")
	@Expose
	private String startingRow;
	
	@SerializedName("endingRow")
	@Expose
	private String endingRow;

	public String getGroupNumber() {
		return groupNumber;
	}

	public void setGroupNumber(String groupNumber) {
		this.groupNumber = groupNumber;
	}

	public String getStartingRow() {
		return startingRow;
	}

	public void setStartingRow(String startingRow) {
		this.startingRow = startingRow;
	}

	public String getEndingRow() {
		return endingRow;
	}

	public void setEndingRow(String endingRow) {
		this.endingRow = endingRow;
	}
	
}
