package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class LocateResponse extends BaseResponse {

	private String dsxobjcny;
    private String dsxisp;
    private String cdxare;
    private String dsxobjplc;
    private String cdxiso;
    private String coolat;
    private String coolon;
    private String cdxmtr;
    private String cdxzip;
    private String cdxreg;
    private String dsxorg;
    private String dsxreg;
	
	private long wp9STC;
    private String dsxerr;
    
	public String getDsxobjcny() {
		return dsxobjcny;
	}
	
	public void setDsxobjcny(String dsxobjcny) {
		this.dsxobjcny = dsxobjcny;
	}
	
	public String getDsxisp() {
		return dsxisp;
	}
	
	public void setDsxisp(String dsxisp) {
		this.dsxisp = dsxisp;
	}
	
	public String getCdxare() {
		return cdxare;
	}
	
	public void setCdxare(String cdxare) {
		this.cdxare = cdxare;
	}
	
	public String getDsxobjplc() {
		return dsxobjplc;
	}
	
	public void setDsxobjplc(String dsxobjplc) {
		this.dsxobjplc = dsxobjplc;
	}
	
	public String getCdxiso() {
		return cdxiso;
	}
	
	public void setCdxiso(String cdxiso) {
		this.cdxiso = cdxiso;
	}
	
	public String getCoolat() {
		return coolat;
	}
	
	public void setCoolat(String coolat) {
		this.coolat = coolat;
	}
	
	public String getCoolon() {
		return coolon;
	}
	
	public void setCoolon(String coolon) {
		this.coolon = coolon;
	}
	
	public String getCdxmtr() {
		return cdxmtr;
	}
	
	public void setCdxmtr(String cdxmtr) {
		this.cdxmtr = cdxmtr;
	}
	
	public String getCdxzip() {
		return cdxzip;
	}
	
	public void setCdxzip(String cdxzip) {
		this.cdxzip = cdxzip;
	}
	
	public String getCdxreg() {
		return cdxreg;
	}
	
	public void setCdxreg(String cdxreg) {
		this.cdxreg = cdxreg;
	}
	
	public String getDsxorg() {
		return dsxorg;
	}
	
	public void setDsxorg(String dsxorg) {
		this.dsxorg = dsxorg;
	}
	
	public String getDsxreg() {
		return dsxreg;
	}
	
	public void setDsxreg(String dsxreg) {
		this.dsxreg = dsxreg;
	}
	
	public long getWp9STC() {
		return wp9STC;
	}
	
	public void setWp9STC(long wp9stc) {
		wp9STC = wp9stc;
	}
	
	public String getDsxerr() {
		return dsxerr;
	}
	
	public void setDsxerr(String dsxerr) {
		this.dsxerr = dsxerr;
	}
    
    
}