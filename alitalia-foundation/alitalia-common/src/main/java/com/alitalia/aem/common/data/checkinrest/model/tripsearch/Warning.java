
package com.alitalia.aem.common.data.checkinrest.model.tripsearch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Warning {

    @SerializedName("shortText")
    @Expose
    private String shortText;
    @SerializedName("value")
    @Expose
    private String value;

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
