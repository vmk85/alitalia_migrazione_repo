package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.BaseRequest;

public class ProfileRequest extends BaseRequest {

	private MMCustomerProfileData customerProfile;
	
	public ProfileRequest() {
		super();
	}
	
	public ProfileRequest(String tid, String sid) {
		super(tid, sid);
	}
	
	public MMCustomerProfileData getCustomerProfile() {
		return customerProfile;
	}

	public void setCustomerProfile(MMCustomerProfileData customerProfile) {
		this.customerProfile = customerProfile;
	}

}
