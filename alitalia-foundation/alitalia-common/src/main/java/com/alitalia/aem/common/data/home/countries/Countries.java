package com.alitalia.aem.common.data.home.countries;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Countries {
	
	@SerializedName("countries")
	@Expose
	private List<GetCountry> country = null;
	@SerializedName("language")
	@Expose
	private String language;
	@SerializedName("market")
	@Expose
	private String market;
	@SerializedName("conversationID")
	@Expose
	private String conversationID;
	
	public List<GetCountry> getCountry() {
		return country;
	}
	
	public void setCountry(List<GetCountry> country) {
		this.country = country;
	}
	
	public String getLanguage() {
		return language;
	}
	
	public void setLanguage(String language) {
		this.language = language;
	}
	
	public String getMarket() {
		return market;
	}
	
	public void setMarket(String market) {
		this.market = market;
	}
	
	public String getConversationID() {
		return conversationID;
	}
	
	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}

}
