package com.alitalia.aem.common.data.home;

import java.util.Calendar;

import com.alitalia.aem.common.data.home.enumerations.MMCardTypeEnum;
import com.alitalia.aem.common.messages.BaseResponse;

public class MMCreditCardData extends BaseResponse {

	public MMCreditCardData(){}
	
	public MMCreditCardData(String circuitCode, String number, String token,
			Calendar expireDate, MMCardTypeEnum cardType) {
		super();
		this.circuitCode = circuitCode;
		this.number = number;
		this.token = token;
		this.expireDate = expireDate;
		this.cardType = cardType;
	}

	private String circuitCode;
	private String number;
	private String token;
	private Calendar expireDate;
	
	private String obfuscatedNumber;
	private String expirationDate;

	private MMCardTypeEnum cardType;

	public String getCircuitCode() {
		return circuitCode;
	}

	public void setCircuitCode(String circuitCode) {
		this.circuitCode = circuitCode;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Calendar getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Calendar expireDate) {
		this.expireDate = expireDate;
	}


	public MMCardTypeEnum getCardType() {
		return cardType;
	}

	public void setCardType(MMCardTypeEnum cardType) {
		this.cardType = cardType;
	}

	public String getObfuscatedNumber() {
		return obfuscatedNumber;
	}

	public void setObfuscatedNumber(String obfuscatedNumber) {
		this.obfuscatedNumber = obfuscatedNumber;
	}
	
	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cardType == null) ? 0 : cardType.hashCode());
		result = prime * result
				+ ((circuitCode == null) ? 0 : circuitCode.hashCode());
		result = prime * result
				+ ((expirationDate == null) ? 0 : expirationDate.hashCode());
		result = prime * result
				+ ((expireDate == null) ? 0 : expireDate.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime
				* result
				+ ((obfuscatedNumber == null) ? 0 : obfuscatedNumber.hashCode());
		result = prime * result + ((token == null) ? 0 : token.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MMCreditCardData other = (MMCreditCardData) obj;
		if (cardType != other.cardType)
			return false;
		if (circuitCode == null) {
			if (other.circuitCode != null)
				return false;
		} else if (!circuitCode.equals(other.circuitCode))
			return false;
		if (expirationDate == null) {
			if (other.expirationDate != null)
				return false;
		} else if (!expirationDate.equals(other.expirationDate))
			return false;
		if (expireDate == null) {
			if (other.expireDate != null)
				return false;
		} else if (!expireDate.equals(other.expireDate))
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (obfuscatedNumber == null) {
			if (other.obfuscatedNumber != null)
				return false;
		} else if (!obfuscatedNumber.equals(other.obfuscatedNumber))
			return false;
		if (token == null) {
			if (other.token != null)
				return false;
		} else if (!token.equals(other.token))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MMCreditCardData [circuitCode=" + circuitCode + ", number="
				+ number + ", token=" + token + ", expireDate=" + expireDate
				+ ", obfuscatedNumber=" + obfuscatedNumber
				+ ", expirationDate=" + expirationDate + ", cardType="
				+ cardType + ", getCircuitCode()=" + getCircuitCode()
				+ ", getNumber()=" + getNumber() + ", getToken()=" + getToken()
				+ ", getExpireDate()=" + getExpireDate() + ", getCardType()="
				+ getCardType() + ", getObfuscatedNumber()="
				+ getObfuscatedNumber() + ", hashCode()=" + hashCode()
				+ ", getClass()=" + getClass() + ", toString()="
				+ super.toString() + "]";
	}
}
