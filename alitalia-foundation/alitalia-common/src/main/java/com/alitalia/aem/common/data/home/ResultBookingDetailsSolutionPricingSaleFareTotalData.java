package com.alitalia.aem.common.data.home;

import java.math.BigDecimal;

public class ResultBookingDetailsSolutionPricingSaleFareTotalData {
	
	private BigDecimal amountField;
	private String currencyField;
	
	public BigDecimal getAmountField() {
		return amountField;
	}
	
	public void setAmountField(BigDecimal amountField) {
		this.amountField = amountField;
	}
	
	public String getCurrencyField() {
		return currencyField;
	}
	
	public void setCurrencyField(String currencyField) {
		this.currencyField = currencyField;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((amountField == null) ? 0 : amountField.hashCode());
		result = prime * result
				+ ((currencyField == null) ? 0 : currencyField.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingDetailsSolutionPricingSaleFareTotalData other = (ResultBookingDetailsSolutionPricingSaleFareTotalData) obj;
		if (amountField == null) {
			if (other.amountField != null)
				return false;
		} else if (!amountField.equals(other.amountField))
			return false;
		if (currencyField == null) {
			if (other.currencyField != null)
				return false;
		} else if (!currencyField.equals(other.currencyField))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingDetailsSolutionPricingSaleFareTotalData [amountField="
				+ amountField + ", currencyField=" + currencyField + "]";
	}
	
}
