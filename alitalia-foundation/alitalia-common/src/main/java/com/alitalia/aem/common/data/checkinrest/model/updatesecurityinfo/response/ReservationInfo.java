
package com.alitalia.aem.common.data.checkinrest.model.updatesecurityinfo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReservationInfo {

    @SerializedName("cabinClass")
    @Expose
    private String cabinClass;
    @SerializedName("bookingClass")
    @Expose
    private String bookingClass;
    @SerializedName("pnrLocator")
    @Expose
    private PnrLocator pnrLocator;

    public String getCabinClass() {
        return cabinClass;
    }

    public void setCabinClass(String cabinClass) {
        this.cabinClass = cabinClass;
    }

    public String getBookingClass() {
        return bookingClass;
    }

    public void setBookingClass(String bookingClass) {
        this.bookingClass = bookingClass;
    }

    public PnrLocator getPnrLocator() {
        return pnrLocator;
    }

    public void setPnrLocator(PnrLocator pnrLocator) {
        this.pnrLocator = pnrLocator;
    }

}
