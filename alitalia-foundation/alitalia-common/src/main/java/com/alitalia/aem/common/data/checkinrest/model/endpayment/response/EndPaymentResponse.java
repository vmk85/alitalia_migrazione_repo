
package com.alitalia.aem.common.data.checkinrest.model.endpayment.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EndPaymentResponse {

    @SerializedName("systemDateTime")
    @Expose
    private String systemDateTime;
    @SerializedName("sabreTransactionID")
    @Expose
    private String sabreTransactionID;
    @SerializedName("totalPriceToBeIssued")
    @Expose
    private String totalPriceToBeIssued;
    @SerializedName("totalPriceIssued")
    @Expose
    private String totalPriceIssued;
    @SerializedName("result")
    @Expose
    private Result result;
    @SerializedName("authorizationResult")
    @Expose
    private AuthorizationResult authorizationResult;
    @SerializedName("aZPayedTPResponse")
    @Expose
    private AZPayedTPResponse aZPayedTPResponse;
    @SerializedName("redirectInfoFor3D")
    @Expose
    private RedirectInfoFor3D redirectInfoFor3D;
    @SerializedName("conversationID")
    @Expose
    private String conversationID;
    
    private String error;

    public String getSystemDateTime() {
        return systemDateTime;
    }

    public void setSystemDateTime(String systemDateTime) {
        this.systemDateTime = systemDateTime;
    }

    public String getSabreTransactionID() {
        return sabreTransactionID;
    }

    public void setSabreTransactionID(String sabreTransactionID) {
        this.sabreTransactionID = sabreTransactionID;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public AuthorizationResult getAuthorizationResult() {
        return authorizationResult;
    }

    public void setAuthorizationResult(AuthorizationResult authorizationResult) {
        this.authorizationResult = authorizationResult;
    }

    public RedirectInfoFor3D getRedirectInfoFor3D() {
        return redirectInfoFor3D;
    }

    public void setRedirectInfoFor3D(RedirectInfoFor3D redirectInfoFor3D) {
        this.redirectInfoFor3D = redirectInfoFor3D;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

	public String getTotalPriceToBeIssued() {
		return totalPriceToBeIssued;
	}

	public void setTotalPriceToBeIssued(String totalPriceToBeIssued) {
		this.totalPriceToBeIssued = totalPriceToBeIssued;
	}

	public String getTotalPriceIssued() {
		return totalPriceIssued;
	}

	public void setTotalPriceIssued(String totalPriceIssued) {
		this.totalPriceIssued = totalPriceIssued;
	}

	public AZPayedTPResponse getaZPayedTPResponse() {
		return aZPayedTPResponse;
	}

	public void setaZPayedTPResponse(AZPayedTPResponse aZPayedTPResponse) {
		this.aZPayedTPResponse = aZPayedTPResponse;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

}
