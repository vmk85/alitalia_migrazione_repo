package com.alitalia.aem.common.data.home.enumerations;


public enum GatewayTypeEnum {

	GATEWAY_SEARCH("GatewaySearch"),
    MULTI_SLICE_NEXT_SEARCH("MultiSliceNextSearch"),
    BOOKING_SELLUP("BookingSellup"),
    BOOKING_TAX_SEARCH("BookingTaxSearch"),
    META_SEARCH("MetaSearch"),
    FULL_TEXT_RULES_SEARCH("FullTextRulesSearch"),
    AWARD_SELLUP("AwardSellup"),
    AWARD_TAXES("AwardTaxes");

	private final String value;

    GatewayTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GatewayTypeEnum fromValue(String v) {
        for (GatewayTypeEnum c: GatewayTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
