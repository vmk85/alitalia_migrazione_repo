package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.FlightItinerayData;
import com.alitalia.aem.common.messages.BaseRequest;

public class RetrieveItinerariesRequest extends BaseRequest {

	private FlightItinerayData flight;

	public FlightItinerayData getFlight() {
		return flight;
	}

	public void setFlight(FlightItinerayData flight) {
		this.flight = flight;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((flight == null) ? 0 : flight.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveItinerariesRequest other = (RetrieveItinerariesRequest) obj;
		if (flight == null) {
			if (other.flight != null)
				return false;
		} else if (!flight.equals(other.flight))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveItinerariesRequest [flight=" + flight + "]";
	}
}