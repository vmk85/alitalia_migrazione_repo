package com.alitalia.aem.common.data.crmdatarest.model.setazcrmdatainfonewsletter.request;

import java.util.List;

import com.alitalia.aem.common.data.crmdatarest.model.Consenso;
import com.alitalia.aem.common.data.crmdatarest.model.PreferenzePersonali;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SetAZCRMDataInfoNewsLetterRequest {
    
    @Override
    public String toString() {
        return "ErrorSourceDetails [listaConsensi=" + listaConsensi + ", " +
                ", email=" + email +
                ", unsubscribeNewsletter=" + unsubscribeNewsletter +
                ", language=" + language +
                ", market=" + market +
                ", conversationID=" + conversationID + "]";
    }
    
    @SerializedName("listaConsensi")
    @Expose
    private List<Consenso> listaConsensi;

    @SerializedName("preferenzePersonali")
    @Expose
    private PreferenzePersonali preferenzePersonali;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("unsubscribeNewsletter")
    @Expose
    private Boolean unsubscribeNewsletter;

    @SerializedName("language")
    @Expose
    private String language;

    @SerializedName("market")
    @Expose
    private String market;

    @SerializedName("conversationID")
    @Expose
    private String conversationID;

    public List<Consenso> getListaConsensi() {
        return listaConsensi;
    }

    public void setListaConsensi(List<Consenso> listaConsensi) {
        this.listaConsensi = listaConsensi;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getUnsubscribeNewsletter() {
        return unsubscribeNewsletter;
    }

    public void setUnsubscribeNewsletter(Boolean unsubscribeNewsletter) {
        this.unsubscribeNewsletter = unsubscribeNewsletter;
    }

    public PreferenzePersonali getPreferenzePersonali() {
        return preferenzePersonali;
    }

    public void setPreferenzePersonali(PreferenzePersonali preferenzePersonali) {
        this.preferenzePersonali = preferenzePersonali;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
    
    public String getMarket() {
        return market;
    }
    
    public void setMarket(String market) {
        this.market = market;
    }
    
    public String getConversationID() {
        return conversationID;
    }
    
    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }
    
    
  
    
}
