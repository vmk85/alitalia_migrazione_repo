
package com.alitalia.aem.common.data.checkinrest.model.flightdetail;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItineraryResponseList {

    @SerializedName("airline")
    @Expose
    private String airline;
    @SerializedName("flight")
    @Expose
    private String flight;
    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("departureDate")
    @Expose
    private String departureDate;
    @SerializedName("departureTime")
    @Expose
    private String departureTime;
    @SerializedName("scheduledDepartureDate")
    @Expose
    private String scheduledDepartureDate;
    @SerializedName("scheduledDepartureTime")
    @Expose
    private String scheduledDepartureTime;
    @SerializedName("estimatedDepartureDate")
    @Expose
    private String estimatedDepartureDate;
    @SerializedName("estimatedDepartureTime")
    @Expose
    private String estimatedDepartureTime;
    @SerializedName("departureGate")
    @Expose
    private String departureGate;
    @SerializedName("boardingTime")
    @Expose
    private BoardingTime boardingTime;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("finalDestination")
    @Expose
    private String finalDestination;
    @SerializedName("arrivalDate")
    @Expose
    private String arrivalDate;
    @SerializedName("arrivalTime")
    @Expose
    private String arrivalTime;
    @SerializedName("scheduledArrivalDate")
    @Expose
    private String scheduledArrivalDate;
    @SerializedName("scheduledArrivalTime")
    @Expose
    private String scheduledArrivalTime;
    @SerializedName("estimatedArrivalDate")
    @Expose
    private String estimatedArrivalDate;
    @SerializedName("estimatedArrivalTime")
    @Expose
    private String estimatedArrivalTime;
    @SerializedName("baggageClaim")
    @Expose
    private String baggageClaim;
    @SerializedName("aircraftType")
    @Expose
    private AircraftType aircraftType;
    @SerializedName("flightStatus")
    @Expose
    private String flightStatus;
    @SerializedName("aircraftConfigNumber")
    @Expose
    private String aircraftConfigNumber;
    @SerializedName("aircraftRegistration")
    @Expose
    private String aircraftRegistration;
    @SerializedName("checkinRuleNumber")
    @Expose
    private String checkinRuleNumber;
    @SerializedName("seatConfig")
    @Expose
    private String seatConfig;
    @SerializedName("bagCheckInOption")
    @Expose
    private String bagCheckInOption;
    @SerializedName("autoOn")
    @Expose
    private String autoOn;
    @SerializedName("heldSeat")
    @Expose
    private String heldSeat;
    @SerializedName("freeTextInfoList")
    @Expose
    private List<FreeTextInfoList> freeTextInfoList = null;
    @SerializedName("apisInfoList")
    @Expose
    private List<ApisInfoList> apisInfoList = null;

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getFlight() {
        return flight;
    }

    public void setFlight(String flight) {
        this.flight = flight;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getScheduledDepartureDate() {
        return scheduledDepartureDate;
    }

    public void setScheduledDepartureDate(String scheduledDepartureDate) {
        this.scheduledDepartureDate = scheduledDepartureDate;
    }

    public String getScheduledDepartureTime() {
        return scheduledDepartureTime;
    }

    public void setScheduledDepartureTime(String scheduledDepartureTime) {
        this.scheduledDepartureTime = scheduledDepartureTime;
    }

    public String getEstimatedDepartureDate() {
        return estimatedDepartureDate;
    }

    public void setEstimatedDepartureDate(String estimatedDepartureDate) {
        this.estimatedDepartureDate = estimatedDepartureDate;
    }

    public String getEstimatedDepartureTime() {
        return estimatedDepartureTime;
    }

    public void setEstimatedDepartureTime(String estimatedDepartureTime) {
        this.estimatedDepartureTime = estimatedDepartureTime;
    }

    public String getDepartureGate() {
        return departureGate;
    }

    public void setDepartureGate(String departureGate) {
        this.departureGate = departureGate;
    }

    public BoardingTime getBoardingTime() {
        return boardingTime;
    }

    public void setBoardingTime(BoardingTime boardingTime) {
        this.boardingTime = boardingTime;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getFinalDestination() {
        return finalDestination;
    }

    public void setFinalDestination(String finalDestination) {
        this.finalDestination = finalDestination;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getScheduledArrivalDate() {
        return scheduledArrivalDate;
    }

    public void setScheduledArrivalDate(String scheduledArrivalDate) {
        this.scheduledArrivalDate = scheduledArrivalDate;
    }

    public String getScheduledArrivalTime() {
        return scheduledArrivalTime;
    }

    public void setScheduledArrivalTime(String scheduledArrivalTime) {
        this.scheduledArrivalTime = scheduledArrivalTime;
    }

    public String getEstimatedArrivalDate() {
        return estimatedArrivalDate;
    }

    public void setEstimatedArrivalDate(String estimatedArrivalDate) {
        this.estimatedArrivalDate = estimatedArrivalDate;
    }

    public String getEstimatedArrivalTime() {
        return estimatedArrivalTime;
    }

    public void setEstimatedArrivalTime(String estimatedArrivalTime) {
        this.estimatedArrivalTime = estimatedArrivalTime;
    }

    public String getBaggageClaim() {
        return baggageClaim;
    }

    public void setBaggageClaim(String baggageClaim) {
        this.baggageClaim = baggageClaim;
    }

    public AircraftType getAircraftType() {
        return aircraftType;
    }

    public void setAircraftType(AircraftType aircraftType) {
        this.aircraftType = aircraftType;
    }

    public String getFlightStatus() {
        return flightStatus;
    }

    public void setFlightStatus(String flightStatus) {
        this.flightStatus = flightStatus;
    }

    public String getAircraftConfigNumber() {
        return aircraftConfigNumber;
    }

    public void setAircraftConfigNumber(String aircraftConfigNumber) {
        this.aircraftConfigNumber = aircraftConfigNumber;
    }

    public String getAircraftRegistration() {
        return aircraftRegistration;
    }

    public void setAircraftRegistration(String aircraftRegistration) {
        this.aircraftRegistration = aircraftRegistration;
    }

    public String getCheckinRuleNumber() {
        return checkinRuleNumber;
    }

    public void setCheckinRuleNumber(String checkinRuleNumber) {
        this.checkinRuleNumber = checkinRuleNumber;
    }

    public String getSeatConfig() {
        return seatConfig;
    }

    public void setSeatConfig(String seatConfig) {
        this.seatConfig = seatConfig;
    }

    public String getBagCheckInOption() {
        return bagCheckInOption;
    }

    public void setBagCheckInOption(String bagCheckInOption) {
        this.bagCheckInOption = bagCheckInOption;
    }

    public String getAutoOn() {
        return autoOn;
    }

    public void setAutoOn(String autoOn) {
        this.autoOn = autoOn;
    }

    public String getHeldSeat() {
        return heldSeat;
    }

    public void setHeldSeat(String heldSeat) {
        this.heldSeat = heldSeat;
    }

    public List<FreeTextInfoList> getFreeTextInfoList() {
        return freeTextInfoList;
    }

    public void setFreeTextInfoList(List<FreeTextInfoList> freeTextInfoList) {
        this.freeTextInfoList = freeTextInfoList;
    }

    public List<ApisInfoList> getApisInfoList() {
        return apisInfoList;
    }

    public void setApisInfoList(List<ApisInfoList> apisInfoList) {
        this.apisInfoList = apisInfoList;
    }

}
