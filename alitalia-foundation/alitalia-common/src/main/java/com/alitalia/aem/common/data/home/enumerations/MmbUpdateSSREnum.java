package com.alitalia.aem.common.data.home.enumerations;

public enum MmbUpdateSSREnum {

	NONE("None"),
	UPDATE("Update"),
	INSERT("Insert"),
	DELETE("Delete");
    private final String value;

    MmbUpdateSSREnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MmbUpdateSSREnum fromValue(String v) {
        for (MmbUpdateSSREnum c: MmbUpdateSSREnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
