package com.alitalia.aem.common.data.checkinrest.model.initpayment.response;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AzAncillaryPayedTP {
	
	@SerializedName("code")
	@Expose
	private String code;
	@SerializedName("emdType")
	@Expose
	private String emdType;
	@SerializedName("commercialName")
	@Expose
	private String commercialName;
	@SerializedName("rficCode")
	@Expose
	private String rficCode;
	@SerializedName("numberOfItems")
	@Expose
	private String numberOfItems;
	@SerializedName("price")
	@Expose
	private String price;
	@SerializedName("currency")
	@Expose
	private String currency;
	@SerializedName("group")
	@Expose
	private String group;
	@SerializedName("rficSubcode")
	@Expose
	private String rficSubcode;
	@SerializedName("pdcSeat")
	@Expose
	private String pdcSeat;
	@SerializedName("azSegmentPayedTP")
	@Expose
	private List<AzSegmentPayedTP> azSegmentPayedTP = null;
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getEmdType() {
		return emdType;
	}
	
	public void setEmdType(String emdType) {
		this.emdType = emdType;
	}
	
	public String getCommercialName() {
		return commercialName;
	}
	
	public void setCommercialName(String commercialName) {
		this.commercialName = commercialName;
	}
	
	public String getRficCode() {
		return rficCode;
	}
	
	public void setRficCode(String rficCode) {
		this.rficCode = rficCode;
	}
	
	public String getNumberOfItems() {
		return numberOfItems;
	}
	
	public void setNumberOfItems(String numberOfItems) {
		this.numberOfItems = numberOfItems;
	}
	
	public String getPrice() {
		return price;
	}
	
	public void setPrice(String price) {
		this.price = price;
	}
	
	public String getCurrency() {
		return currency;
	}
	
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public String getGroup() {
		return group;
	}
	
	public void setGroup(String group) {
		this.group = group;
	}
	
	public String getRficSubcode() {
		return rficSubcode;
	}
	
	public void setRficSubcode(String rficSubcode) {
		this.rficSubcode = rficSubcode;
	}
	
	public String getPdcSeat() {
		return pdcSeat;
	}
	
	public void setPdcSeat(String pdcSeat) {
		this.pdcSeat = pdcSeat;
	}
	
	public List<AzSegmentPayedTP> getAzSegmentPayedTP() {
		return azSegmentPayedTP;
	}
	
	public void setAzSegmentPayedTP(List<AzSegmentPayedTP> azSegmentPayedTP) {
		this.azSegmentPayedTP = azSegmentPayedTP;
	}


}
