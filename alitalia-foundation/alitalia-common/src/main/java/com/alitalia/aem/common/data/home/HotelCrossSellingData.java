package com.alitalia.aem.common.data.home;

import java.util.List;

public class HotelCrossSellingData extends CrossSellingDetailData {

	private List<String> otherImages;
	private String tripAdvisorRatingUrl;
	private Double hotelRating;
	private Double tripAdvisorRating;

	public List<String> getOtherImages() {
		return otherImages;
	}

	public void setOtherImages(List<String> otherImages) {
		this.otherImages = otherImages;
	}

	public String getTripAdvisorRatingUrl() {
		return tripAdvisorRatingUrl;
	}

	public void setTripAdvisorRatingUrl(String tripAdvisorRatingUrl) {
		this.tripAdvisorRatingUrl = tripAdvisorRatingUrl;
	}

	public Double getHotelRating() {
		return hotelRating;
	}

	public void setHotelRating(Double hotelRating) {
		this.hotelRating = hotelRating;
	}

	public Double getTripAdvisorRating() {
		return tripAdvisorRating;
	}

	public void setTripAdvisorRating(Double tripAdvisorRating) {
		this.tripAdvisorRating = tripAdvisorRating;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((hotelRating == null) ? 0 : hotelRating.hashCode());
		result = prime * result + ((otherImages == null) ? 0 : otherImages.hashCode());
		result = prime * result + ((tripAdvisorRating == null) ? 0 : tripAdvisorRating.hashCode());
		result = prime * result + ((tripAdvisorRatingUrl == null) ? 0 : tripAdvisorRatingUrl.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		HotelCrossSellingData other = (HotelCrossSellingData) obj;
		if (hotelRating == null) {
			if (other.hotelRating != null)
				return false;
		} else if (!hotelRating.equals(other.hotelRating))
			return false;
		if (otherImages == null) {
			if (other.otherImages != null)
				return false;
		} else if (!otherImages.equals(other.otherImages))
			return false;
		if (tripAdvisorRating == null) {
			if (other.tripAdvisorRating != null)
				return false;
		} else if (!tripAdvisorRating.equals(other.tripAdvisorRating))
			return false;
		if (tripAdvisorRatingUrl == null) {
			if (other.tripAdvisorRatingUrl != null)
				return false;
		} else if (!tripAdvisorRatingUrl.equals(other.tripAdvisorRatingUrl))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HotelCrossSellingData [otherImages=" + otherImages + ", tripAdvisorRatingUrl=" + tripAdvisorRatingUrl
				+ ", hotelRating=" + hotelRating + ", tripAdvisorRating=" + tripAdvisorRating + "]";
	}

}
