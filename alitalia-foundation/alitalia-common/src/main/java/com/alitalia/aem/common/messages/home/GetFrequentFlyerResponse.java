package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.frequentflyer.FrequentFlyers;
import com.alitalia.aem.common.messages.BaseResponse;

public class GetFrequentFlyerResponse extends BaseResponse{
	
	private FrequentFlyers frequentFlyers;
	
	public GetFrequentFlyerResponse() {}

	public GetFrequentFlyerResponse(String tid, String sid) {
		super(tid, sid);
	}

	public FrequentFlyers getFrequentFlyers() {
		return frequentFlyers;
	}

	public void setFrequentFlyers(FrequentFlyers frequentFlyers) {
		this.frequentFlyers = frequentFlyers;
	}
}
