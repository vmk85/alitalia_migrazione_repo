package com.alitalia.aem.common.data.crmdatarest.model.setazcrmdatainfonewsletter.response;

import com.alitalia.aem.common.data.crmdatarest.model.*;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SetAZCRMDataInfoNewsLetterResponse {
    
    @Override
    public String toString() {
        return "GetAZCRMDataInfo [result=" + result + ", " +
                                    "erroreSearch=" + erroreSearch + "]";
    }
    
    
    @SerializedName("result")
    @Expose
    private Result result;
    
    private String erroreSearch;
    
    
    public Result getResult() {
        return result;
    }
    
    public void setResult(Result result) {
        this.result = result;
    }
    
    public String getErroreSearch() {
        return erroreSearch;
    }
    
    public void setErroreSearch(String erroreSearch) {
        this.erroreSearch = erroreSearch;
    }
}
