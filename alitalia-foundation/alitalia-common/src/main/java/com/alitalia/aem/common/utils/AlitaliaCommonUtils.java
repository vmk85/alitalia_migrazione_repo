package com.alitalia.aem.common.utils;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.NotSerializableException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.common.data.home.checkin.CheckinRouteData;

public class AlitaliaCommonUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(AlitaliaCommonUtils.class);

	/**
	 * Return the correct flight list for a checkin
	 * @param routeData
	 * @return
	 */
	public static List<CheckinFlightData> getFlightsList (
			CheckinRouteData routeData) {
		if (routeData == null) {
			return new ArrayList<CheckinFlightData>();
		}
		if (routeData.getInterlineFlights() != null
				&& routeData.getInterlineFlights().size() > 0) {
			return routeData.getInterlineFlights();
		} else {
			return routeData.getFlights();
		}
	}
	
	/**
	 * It perform a deep clone of an Object using Stream
	 * @param object
	 * @return
	 * @throws IOException 
	 */
	public static Object deepClone(Object object) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = null;
		ObjectInputStream ois = null;
		try {
			oos = new ObjectOutputStream(baos);
			oos.writeObject(object);
			ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
			ois = new ObjectInputStream(bais);
			return ois.readObject();
		} catch (NotSerializableException e) {
			logger.error("Error cloning object: The object {} must implements Serializable interface.", e.getMessage());
			return null;
		} catch (Exception e) {
			logger.error("Generic Error cloning object", e);
			return null;
		} finally {
			oos.close();
			ois.close();
		}
	}
	/**
	 * Method to convert an object to Map (introspection)
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public static Map<String, Object> introspect(Object obj) throws Exception {
	    Map<String, Object> result = new HashMap<String, Object>();
	    BeanInfo info = Introspector.getBeanInfo(obj.getClass());
	    for (PropertyDescriptor pd : info.getPropertyDescriptors()) {
	        Method reader = pd.getReadMethod();
	        if (reader != null)
	            result.put(pd.getName(), reader.invoke(obj));
	    }
	    return result;
	}
	
}
