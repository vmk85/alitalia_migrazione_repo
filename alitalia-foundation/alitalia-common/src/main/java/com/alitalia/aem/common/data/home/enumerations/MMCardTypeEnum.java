package com.alitalia.aem.common.data.home.enumerations;

public enum MMCardTypeEnum {

    PERSONAL("Personal"),
    BUSINESS("Business"),
    COBRANDED("Cobranded");

    private final String value;
    
    MMCardTypeEnum(String v) {
		value = v;
	}
	
	public String value() {
		return value;
	}

	public static MMCardTypeEnum fromValue(String v) {
		for (MMCardTypeEnum c: MMCardTypeEnum.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
