package com.alitalia.aem.common.data.home;

public class ResultBookingDetailsSolutionPricingReissueInfoData {
	
	private String reissueActionField;
	
	public String getReissueActionField() {
		return reissueActionField;
	}
	
	public void setReissueActionField(String reissueActionField) {
		this.reissueActionField = reissueActionField;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((reissueActionField == null) ? 0 : reissueActionField
						.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultBookingDetailsSolutionPricingReissueInfoData other = (ResultBookingDetailsSolutionPricingReissueInfoData) obj;
		if (reissueActionField == null) {
			if (other.reissueActionField != null)
				return false;
		} else if (!reissueActionField.equals(other.reissueActionField))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ResultBookingDetailsSolutionPricingReissueInfoData [reissueActionField="
				+ reissueActionField + "]";
	}
	
}
