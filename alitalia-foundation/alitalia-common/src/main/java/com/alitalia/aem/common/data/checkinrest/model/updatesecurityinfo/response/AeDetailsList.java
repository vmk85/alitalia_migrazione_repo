
package com.alitalia.aem.common.data.checkinrest.model.updatesecurityinfo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AeDetailsList {

    @SerializedName("itemID")
    @Expose
    private String itemID;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("atpcoGroupCode")
    @Expose
    private String atpcoGroupCode;
    @SerializedName("atpcoSubCode")
    @Expose
    private String atpcoSubCode;
    @SerializedName("statusCode")
    @Expose
    private String statusCode;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("usedEMD")
    @Expose
    private Boolean usedEMD;
    @SerializedName("usedEMDSpecified")
    @Expose
    private Boolean usedEMDSpecified;
    @SerializedName("quantityUsed")
    @Expose
    private Integer quantityUsed;
    @SerializedName("quantityUsedSpecified")
    @Expose
    private Boolean quantityUsedSpecified;
    @SerializedName("quantityBought")
    @Expose
    private Integer quantityBought;
    @SerializedName("quantityBoughtSpecified")
    @Expose
    private Boolean quantityBoughtSpecified;
    @SerializedName("purchaseDate")
    @Expose
    private String purchaseDate;
    @SerializedName("purchaseTime")
    @Expose
    private String purchaseTime;
    @SerializedName("disassociated")
    @Expose
    private Boolean disassociated;
    @SerializedName("disassociatedSpecified")
    @Expose
    private Boolean disassociatedSpecified;
    @SerializedName("marketingAirline")
    @Expose
    private String marketingAirline;
    @SerializedName("marketingFlight")
    @Expose
    private String marketingFlight;
    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("operatingAirline")
    @Expose
    private String operatingAirline;
    @SerializedName("operatingFlight")
    @Expose
    private String operatingFlight;
    @SerializedName("emdTicketNumber")
    @Expose
    private EmdTicketNumber emdTicketNumber;
    @SerializedName("priceDetails")
    @Expose
    private PriceDetails priceDetails;
    @SerializedName("bagTagInfoList")
    @Expose
    private BagTagInfoList_ bagTagInfoList;

    public String getItemID() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAtpcoGroupCode() {
        return atpcoGroupCode;
    }

    public void setAtpcoGroupCode(String atpcoGroupCode) {
        this.atpcoGroupCode = atpcoGroupCode;
    }

    public String getAtpcoSubCode() {
        return atpcoSubCode;
    }

    public void setAtpcoSubCode(String atpcoSubCode) {
        this.atpcoSubCode = atpcoSubCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getUsedEMD() {
        return usedEMD;
    }

    public void setUsedEMD(Boolean usedEMD) {
        this.usedEMD = usedEMD;
    }

    public Boolean getUsedEMDSpecified() {
        return usedEMDSpecified;
    }

    public void setUsedEMDSpecified(Boolean usedEMDSpecified) {
        this.usedEMDSpecified = usedEMDSpecified;
    }

    public Integer getQuantityUsed() {
        return quantityUsed;
    }

    public void setQuantityUsed(Integer quantityUsed) {
        this.quantityUsed = quantityUsed;
    }

    public Boolean getQuantityUsedSpecified() {
        return quantityUsedSpecified;
    }

    public void setQuantityUsedSpecified(Boolean quantityUsedSpecified) {
        this.quantityUsedSpecified = quantityUsedSpecified;
    }

    public Integer getQuantityBought() {
        return quantityBought;
    }

    public void setQuantityBought(Integer quantityBought) {
        this.quantityBought = quantityBought;
    }

    public Boolean getQuantityBoughtSpecified() {
        return quantityBoughtSpecified;
    }

    public void setQuantityBoughtSpecified(Boolean quantityBoughtSpecified) {
        this.quantityBoughtSpecified = quantityBoughtSpecified;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public String getPurchaseTime() {
        return purchaseTime;
    }

    public void setPurchaseTime(String purchaseTime) {
        this.purchaseTime = purchaseTime;
    }

    public Boolean getDisassociated() {
        return disassociated;
    }

    public void setDisassociated(Boolean disassociated) {
        this.disassociated = disassociated;
    }

    public Boolean getDisassociatedSpecified() {
        return disassociatedSpecified;
    }

    public void setDisassociatedSpecified(Boolean disassociatedSpecified) {
        this.disassociatedSpecified = disassociatedSpecified;
    }

    public String getMarketingAirline() {
        return marketingAirline;
    }

    public void setMarketingAirline(String marketingAirline) {
        this.marketingAirline = marketingAirline;
    }

    public String getMarketingFlight() {
        return marketingFlight;
    }

    public void setMarketingFlight(String marketingFlight) {
        this.marketingFlight = marketingFlight;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getOperatingAirline() {
        return operatingAirline;
    }

    public void setOperatingAirline(String operatingAirline) {
        this.operatingAirline = operatingAirline;
    }

    public String getOperatingFlight() {
        return operatingFlight;
    }

    public void setOperatingFlight(String operatingFlight) {
        this.operatingFlight = operatingFlight;
    }

    public EmdTicketNumber getEmdTicketNumber() {
        return emdTicketNumber;
    }

    public void setEmdTicketNumber(EmdTicketNumber emdTicketNumber) {
        this.emdTicketNumber = emdTicketNumber;
    }

    public PriceDetails getPriceDetails() {
        return priceDetails;
    }

    public void setPriceDetails(PriceDetails priceDetails) {
        this.priceDetails = priceDetails;
    }

    public BagTagInfoList_ getBagTagInfoList() {
        return bagTagInfoList;
    }

    public void setBagTagInfoList(BagTagInfoList_ bagTagInfoList) {
        this.bagTagInfoList = bagTagInfoList;
    }

}
