package com.alitalia.aem.common.data.checkinrest.model.passenger;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FrequentTravel {
	
	@SerializedName("airline")
	@Expose
	private String airline;
	
	@SerializedName("number")
	@Expose
	private String number;
	
	@SerializedName("MMTier")
	@Expose
	private String tier;
	
	@SerializedName("tierBannerName")
	@Expose
	private String tierBannerName;

	public String getAirline() {
		return airline;
	}

	public void setAirline(String airline) {
		this.airline = airline;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getTier() {
		return tier;
	}

	public void setTier(String tier) {
		this.tier = tier;
	}

	public String getTierBannerName() {
		return tierBannerName;
	}

	public void setTierBannerName(String tierBannerName) {
		this.tierBannerName = tierBannerName;
	}

}
