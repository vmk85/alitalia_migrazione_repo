package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.messages.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SetInsuranceResponse extends BaseResponse {
	
	@SerializedName("outcome")
	@Expose
	private String outcome;
	@SerializedName("pnr")
	@Expose
	private String pnr;
	@SerializedName("price")
	@Expose
	private String price;
	@SerializedName("conversationID")
	@Expose
	private String conversationID;
	
	private String error;

	public SetInsuranceResponse() {}

	public SetInsuranceResponse(String tid, String sid) {
		super(tid, sid);
	}

	public String getOutcome() {
		return outcome;
	}

	public void setOutcome(String outcome) {
		this.outcome = outcome;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getConversationID() {
		return conversationID;
	}

	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

}
