package com.alitalia.aem.common.data.home.carnet;

public class CarnetComunicationCreditCardData extends CarnetComunicationData {

	private String errorUrl;
    private String acceptHeader;
    private String ipAddress;
    private String userAgent;
    
	public String getErrorUrl() {
		return errorUrl;
	}
	public void setErrorUrl(String errorUrl) {
		this.errorUrl = errorUrl;
	}
	public String getAcceptHeader() {
		return acceptHeader;
	}
	public void setAcceptHeader(String acceptHeader) {
		this.acceptHeader = acceptHeader;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getUserAgent() {
		return userAgent;
	}
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((acceptHeader == null) ? 0 : acceptHeader.hashCode());
		result = prime * result
				+ ((errorUrl == null) ? 0 : errorUrl.hashCode());
		result = prime * result
				+ ((ipAddress == null) ? 0 : ipAddress.hashCode());
		result = prime * result
				+ ((userAgent == null) ? 0 : userAgent.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarnetComunicationCreditCardData other = (CarnetComunicationCreditCardData) obj;
		if (acceptHeader == null) {
			if (other.acceptHeader != null)
				return false;
		} else if (!acceptHeader.equals(other.acceptHeader))
			return false;
		if (errorUrl == null) {
			if (other.errorUrl != null)
				return false;
		} else if (!errorUrl.equals(other.errorUrl))
			return false;
		if (ipAddress == null) {
			if (other.ipAddress != null)
				return false;
		} else if (!ipAddress.equals(other.ipAddress))
			return false;
		if (userAgent == null) {
			if (other.userAgent != null)
				return false;
		} else if (!userAgent.equals(other.userAgent))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CarnetComunicationCreditCardData [errorUrl=" + errorUrl
				+ ", acceptHeader=" + acceptHeader + ", ipAddress=" + ipAddress
				+ ", userAgent=" + userAgent + ", description=" + description
				+ ", languageCode=" + languageCode + ", returnUrl=" + returnUrl
				+ "]";
	}
}