package com.alitalia.aem.common.data.home;

import java.util.List;

public class ResultTicketingDetailSolutionData {

    private List<ResultTicketingDetailSolutionCurrencyConversionData> currencyConversionField;
    private ResultTicketingDetailSolutionDisplayFareTotalData displayFareTotalField;
    private ResultTicketingDetailSolutionDisplayTaxTotalData displayTaxTotalField;
    private ResultTicketingDetailSolutionDisplayTotalData displayTotalField;
    private ResultTicketingDetailSolutionExtData extField;
    private String idField;
    private List<ResultTicketingDetailSolutionPricingData> pricingField;
    private ResultTicketingDetailSolutionSaleFareTotalData saleFareTotalField;
    private ResultTicketingDetailSolutionSaleTaxTotalData saleTaxTotalField;
    private ResultTicketingDetailSolutionSaleTotalData saleTotalField;
    private List<ResultTicketingDetailSolutionSliceData> sliceField;

    public List<ResultTicketingDetailSolutionCurrencyConversionData> getCurrencyConversionField() {
        return currencyConversionField;
    }

    public void setCurrencyConversionField(List<ResultTicketingDetailSolutionCurrencyConversionData> value) {
        this.currencyConversionField = value;
    }

    public ResultTicketingDetailSolutionDisplayFareTotalData getDisplayFareTotalField() {
        return displayFareTotalField;
    }

    public void setDisplayFareTotalField(ResultTicketingDetailSolutionDisplayFareTotalData value) {
        this.displayFareTotalField = value;
    }

    public ResultTicketingDetailSolutionDisplayTaxTotalData getDisplayTaxTotalField() {
        return displayTaxTotalField;
    }

    public void setDisplayTaxTotalField(ResultTicketingDetailSolutionDisplayTaxTotalData value) {
        this.displayTaxTotalField = value;
    }

    public ResultTicketingDetailSolutionDisplayTotalData getDisplayTotalField() {
        return displayTotalField;
    }

    public void setDisplayTotalField(ResultTicketingDetailSolutionDisplayTotalData value) {
        this.displayTotalField = value;
    }

    public ResultTicketingDetailSolutionExtData getExtField() {
        return extField;
    }

    public void setExtField(ResultTicketingDetailSolutionExtData value) {
        this.extField = value;
    }

    public String getIdField() {
        return idField;
    }

    public void setIdField(String value) {
        this.idField = value;
    }

    public List<ResultTicketingDetailSolutionPricingData> getPricingField() {
        return pricingField;
    }

    public void setPricingField(List<ResultTicketingDetailSolutionPricingData> value) {
        this.pricingField = value;
    }

    public ResultTicketingDetailSolutionSaleFareTotalData getSaleFareTotalField() {
        return saleFareTotalField;
    }

    public void setSaleFareTotalField(ResultTicketingDetailSolutionSaleFareTotalData value) {
        this.saleFareTotalField = value;
    }

    public ResultTicketingDetailSolutionSaleTaxTotalData getSaleTaxTotalField() {
        return saleTaxTotalField;
    }

    public void setSaleTaxTotalField(ResultTicketingDetailSolutionSaleTaxTotalData value) {
        this.saleTaxTotalField = value;
    }

    public ResultTicketingDetailSolutionSaleTotalData getSaleTotalField() {
        return saleTotalField;
    }

    public void setSaleTotalField(ResultTicketingDetailSolutionSaleTotalData value) {
        this.saleTotalField = value;
    }

    public List<ResultTicketingDetailSolutionSliceData> getSliceField() {
        return sliceField;
    }

    public void setSliceField(List<ResultTicketingDetailSolutionSliceData> value) {
        this.sliceField = value;
    }

}
