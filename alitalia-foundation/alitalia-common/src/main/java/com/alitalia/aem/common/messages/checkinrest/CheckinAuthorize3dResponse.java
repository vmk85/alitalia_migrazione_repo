package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.initpayment.response.Authorize3dResponse;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinAuthorize3dResponse extends BaseResponse {

    public Authorize3dResponse authorize3dResponse;

    public Authorize3dResponse getAuthorize3dResponse() {
        return authorize3dResponse;
    }

    public void setAuthorize3dResponse(Authorize3dResponse authorize3dResponse) {
        this.authorize3dResponse = authorize3dResponse;
    }

    public CheckinAuthorize3dResponse() {}

    public CheckinAuthorize3dResponse(String tid, String sid) { super(tid, sid); }
}
