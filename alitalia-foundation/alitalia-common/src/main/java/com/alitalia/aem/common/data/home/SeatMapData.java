package com.alitalia.aem.common.data.home;

import java.util.List;

import com.alitalia.aem.common.data.home.enumerations.CabinEnum;

public class SeatMapData {

	private CabinEnum cabinType;
	private String startingRow;
	private String endingRow;
    private List<SeatMapSectorData> seatMapSectors;
	
    public CabinEnum getCabinType() {
		return cabinType;
	}
    
	public void setCabinType(CabinEnum cabinType) {
		this.cabinType = cabinType;
	}
	
	public String getStartingRow() {
		return startingRow;
	}
	
	public void setStartingRow(String startingRow) {
		this.startingRow = startingRow;
	}
	
	public String getEndingRow() {
		return endingRow;
	}
	
	public void setEndingRow(String endingRow) {
		this.endingRow = endingRow;
	}
	
	public List<SeatMapSectorData> getSeatMapSectors() {
		return seatMapSectors;
	}
	
	public void setSeatMapSectors(List<SeatMapSectorData> seatMapSectors) {
		this.seatMapSectors = seatMapSectors;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cabinType == null) ? 0 : cabinType.hashCode());
		result = prime * result
				+ ((endingRow == null) ? 0 : endingRow.hashCode());
		result = prime * result
				+ ((seatMapSectors == null) ? 0 : seatMapSectors.hashCode());
		result = prime * result
				+ ((startingRow == null) ? 0 : startingRow.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SeatMapData other = (SeatMapData) obj;
		if (cabinType != other.cabinType)
			return false;
		if (endingRow == null) {
			if (other.endingRow != null)
				return false;
		} else if (!endingRow.equals(other.endingRow))
			return false;
		if (seatMapSectors == null) {
			if (other.seatMapSectors != null)
				return false;
		} else if (!seatMapSectors.equals(other.seatMapSectors))
			return false;
		if (startingRow == null) {
			if (other.startingRow != null)
				return false;
		} else if (!startingRow.equals(other.startingRow))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SeatMapData [cabinType=" + cabinType + ", startingRow="
				+ startingRow + ", endingRow=" + endingRow
				+ ", seatMapSectors=" + seatMapSectors + "]";
	}
}