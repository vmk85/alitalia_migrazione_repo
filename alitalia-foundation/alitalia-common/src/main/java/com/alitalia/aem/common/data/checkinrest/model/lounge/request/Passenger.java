package com.alitalia.aem.common.data.checkinrest.model.lounge.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Passenger {

    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("passengerID")
    @Expose
    private String passengerID;
    @SerializedName("lounge")
    @Expose
    private Boolean lounge;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassengerID() {
        return passengerID;
    }

    public void setPassengerID(String passengerID) {
        this.passengerID = passengerID;
    }

    public Boolean getLounge() {
        return lounge;
    }

    public void setLounge(Boolean lounge) {
        this.lounge = lounge;
    }

}
