
package com.alitalia.aem.common.data.checkinrest.model.endpayment.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RedirectInfoFor3D {

    @SerializedName("paRequest")
    @Expose
    private String paRequest;
    @SerializedName("redirectUrl")
    @Expose
    private String redirectUrl;
    @SerializedName("merchantData")
    @Expose
    private String merchantData;
    @SerializedName("redirectHTML")
    @Expose
    private String redirectHTML;

    public String getPaRequest() {
        return paRequest;
    }

    public void setPaRequest(String paRequest) {
        this.paRequest = paRequest;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public String getMerchantData() {
        return merchantData;
    }

    public void setMerchantData(String merchantData) {
        this.merchantData = merchantData;
    }

    public String getRedirectHTML() {
        return redirectHTML;
    }

    public void setRedirectHTML(String redirectHTML) {
        this.redirectHTML = redirectHTML;
    }

}
