
package com.alitalia.aem.common.data.checkinrest.model.updatesecurityinfo.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItineraryResponseList {

    @SerializedName("airline")
    @Expose
    private String airline;
    @SerializedName("flight")
    @Expose
    private String flight;
    @SerializedName("bookingClass")
    @Expose
    private String bookingClass;
    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("departureDate")
    @Expose
    private String departureDate;
    @SerializedName("departureTimeSched")
    @Expose
    private String departureTimeSched;
    @SerializedName("departureDateEstimated")
    @Expose
    private String departureDateEstimated;
    @SerializedName("departureTime")
    @Expose
    private String departureTime;
    @SerializedName("departureGate")
    @Expose
    private String departureGate;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("arrivalDate")
    @Expose
    private String arrivalDate;
    @SerializedName("arrivalTimeSched")
    @Expose
    private String arrivalTimeSched;
    @SerializedName("arrivalTime")
    @Expose
    private String arrivalTime;
    @SerializedName("arrivalGate")
    @Expose
    private String arrivalGate;
    @SerializedName("baggageClaim")
    @Expose
    private String baggageClaim;
    @SerializedName("destinationFinal")
    @Expose
    private String destinationFinal;
    @SerializedName("aircraftType")
    @Expose
    private AircraftType aircraftType;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("indicator")
    @Expose
    private String indicator;
    @SerializedName("cityPair")
    @Expose
    private String cityPair;
    @SerializedName("aircraftConfigNumber")
    @Expose
    private String aircraftConfigNumber;
    @SerializedName("aircraftRegistration")
    @Expose
    private String aircraftRegistration;
    @SerializedName("checkinRuleNumber")
    @Expose
    private String checkinRuleNumber;
    @SerializedName("seatConfig")
    @Expose
    private String seatConfig;
    @SerializedName("bag")
    @Expose
    private String bag;
    @SerializedName("autoOn")
    @Expose
    private String autoOn;
    @SerializedName("heldSeat")
    @Expose
    private String heldSeat;
    @SerializedName("freeTextInfoList")
    @Expose
    private List<FreeTextInfoList_> freeTextInfoList = null;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("apisInfoList")
    @Expose
    private List<ApisInfoList> apisInfoList = null;

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getFlight() {
        return flight;
    }

    public void setFlight(String flight) {
        this.flight = flight;
    }

    public String getBookingClass() {
        return bookingClass;
    }

    public void setBookingClass(String bookingClass) {
        this.bookingClass = bookingClass;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getDepartureTimeSched() {
        return departureTimeSched;
    }

    public void setDepartureTimeSched(String departureTimeSched) {
        this.departureTimeSched = departureTimeSched;
    }

    public String getDepartureDateEstimated() {
        return departureDateEstimated;
    }

    public void setDepartureDateEstimated(String departureDateEstimated) {
        this.departureDateEstimated = departureDateEstimated;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getDepartureGate() {
        return departureGate;
    }

    public void setDepartureGate(String departureGate) {
        this.departureGate = departureGate;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getArrivalTimeSched() {
        return arrivalTimeSched;
    }

    public void setArrivalTimeSched(String arrivalTimeSched) {
        this.arrivalTimeSched = arrivalTimeSched;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getArrivalGate() {
        return arrivalGate;
    }

    public void setArrivalGate(String arrivalGate) {
        this.arrivalGate = arrivalGate;
    }

    public String getBaggageClaim() {
        return baggageClaim;
    }

    public void setBaggageClaim(String baggageClaim) {
        this.baggageClaim = baggageClaim;
    }

    public String getDestinationFinal() {
        return destinationFinal;
    }

    public void setDestinationFinal(String destinationFinal) {
        this.destinationFinal = destinationFinal;
    }

    public AircraftType getAircraftType() {
        return aircraftType;
    }

    public void setAircraftType(AircraftType aircraftType) {
        this.aircraftType = aircraftType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIndicator() {
        return indicator;
    }

    public void setIndicator(String indicator) {
        this.indicator = indicator;
    }

    public String getCityPair() {
        return cityPair;
    }

    public void setCityPair(String cityPair) {
        this.cityPair = cityPair;
    }

    public String getAircraftConfigNumber() {
        return aircraftConfigNumber;
    }

    public void setAircraftConfigNumber(String aircraftConfigNumber) {
        this.aircraftConfigNumber = aircraftConfigNumber;
    }

    public String getAircraftRegistration() {
        return aircraftRegistration;
    }

    public void setAircraftRegistration(String aircraftRegistration) {
        this.aircraftRegistration = aircraftRegistration;
    }

    public String getCheckinRuleNumber() {
        return checkinRuleNumber;
    }

    public void setCheckinRuleNumber(String checkinRuleNumber) {
        this.checkinRuleNumber = checkinRuleNumber;
    }

    public String getSeatConfig() {
        return seatConfig;
    }

    public void setSeatConfig(String seatConfig) {
        this.seatConfig = seatConfig;
    }

    public String getBag() {
        return bag;
    }

    public void setBag(String bag) {
        this.bag = bag;
    }

    public String getAutoOn() {
        return autoOn;
    }

    public void setAutoOn(String autoOn) {
        this.autoOn = autoOn;
    }

    public String getHeldSeat() {
        return heldSeat;
    }

    public void setHeldSeat(String heldSeat) {
        this.heldSeat = heldSeat;
    }

    public List<FreeTextInfoList_> getFreeTextInfoList() {
        return freeTextInfoList;
    }

    public void setFreeTextInfoList(List<FreeTextInfoList_> freeTextInfoList) {
        this.freeTextInfoList = freeTextInfoList;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<ApisInfoList> getApisInfoList() {
        return apisInfoList;
    }

    public void setApisInfoList(List<ApisInfoList> apisInfoList) {
        this.apisInfoList = apisInfoList;
    }

}
