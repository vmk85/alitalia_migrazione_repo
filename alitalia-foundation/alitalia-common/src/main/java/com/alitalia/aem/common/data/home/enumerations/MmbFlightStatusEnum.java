package com.alitalia.aem.common.data.home.enumerations;

public enum MmbFlightStatusEnum {

	UNKNOWN("Unknown"),
	REGULAR("Regular"),
	AVAILABLE("Available"),
	CHECKEDIN("CheckedIn"),
	FLOWN("Flown"),
	INVALID("Invalid");
    private final String value;

    MmbFlightStatusEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MmbFlightStatusEnum fromValue(String v) {
        for (MmbFlightStatusEnum c: MmbFlightStatusEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
