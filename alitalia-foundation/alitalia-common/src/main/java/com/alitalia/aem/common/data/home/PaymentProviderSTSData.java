package com.alitalia.aem.common.data.home;

import java.util.Calendar;

public class PaymentProviderSTSData extends PaymentProviderData {

	private String clientIP;
	private String clientInfoRef;
	private String clientUserAccount;
	private String failurePageUrl;
	private String merchantShopRef;
	private Calendar stsAdvanceDate;
	private Calendar stsBalanceDate;
	private String stsClientID;
	private Calendar stsDepartureDate;
	private String stsMerchantID;
	private String stsPayType;
	private String successPageUrl;
	
	public String getClientIP() {
		return clientIP;
	}
	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}
	public String getClientInfoRef() {
		return clientInfoRef;
	}
	public void setClientInfoRef(String clientInfoRef) {
		this.clientInfoRef = clientInfoRef;
	}
	public String getClientUserAccount() {
		return clientUserAccount;
	}
	public void setClientUserAccount(String clientUserAccount) {
		this.clientUserAccount = clientUserAccount;
	}
	public String getFailurePageUrl() {
		return failurePageUrl;
	}
	public void setFailurePageUrl(String failurePageUrl) {
		this.failurePageUrl = failurePageUrl;
	}
	public String getMerchantShopRef() {
		return merchantShopRef;
	}
	public void setMerchantShopRef(String merchantShopRef) {
		this.merchantShopRef = merchantShopRef;
	}
	public Calendar getStsAdvanceDate() {
		return stsAdvanceDate;
	}
	public void setStsAdvanceDate(Calendar stsAdvanceDate) {
		this.stsAdvanceDate = stsAdvanceDate;
	}
	public Calendar getStsBalanceDate() {
		return stsBalanceDate;
	}
	public void setStsBalanceDate(Calendar stsBalanceDate) {
		this.stsBalanceDate = stsBalanceDate;
	}
	public String getStsClientID() {
		return stsClientID;
	}
	public void setStsClientID(String stsClientID) {
		this.stsClientID = stsClientID;
	}
	public Calendar getStsDepartureDate() {
		return stsDepartureDate;
	}
	public void setStsDepartureDate(Calendar stsDepartureDate) {
		this.stsDepartureDate = stsDepartureDate;
	}
	public String getStsMerchantID() {
		return stsMerchantID;
	}
	public void setStsMerchantID(String stsMerchantID) {
		this.stsMerchantID = stsMerchantID;
	}
	public String getStsPayType() {
		return stsPayType;
	}
	public void setStsPayType(String stsPayType) {
		this.stsPayType = stsPayType;
	}
	public String getSuccessPageUrl() {
		return successPageUrl;
	}
	public void setSuccessPageUrl(String successPageUrl) {
		this.successPageUrl = successPageUrl;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((clientIP == null) ? 0 : clientIP.hashCode());
		result = prime * result
				+ ((clientInfoRef == null) ? 0 : clientInfoRef.hashCode());
		result = prime
				* result
				+ ((clientUserAccount == null) ? 0 : clientUserAccount
						.hashCode());
		result = prime * result
				+ ((failurePageUrl == null) ? 0 : failurePageUrl.hashCode());
		result = prime * result
				+ ((merchantShopRef == null) ? 0 : merchantShopRef.hashCode());
		result = prime * result
				+ ((stsAdvanceDate == null) ? 0 : stsAdvanceDate.hashCode());
		result = prime * result
				+ ((stsBalanceDate == null) ? 0 : stsBalanceDate.hashCode());
		result = prime * result
				+ ((stsClientID == null) ? 0 : stsClientID.hashCode());
		result = prime
				* result
				+ ((stsDepartureDate == null) ? 0 : stsDepartureDate.hashCode());
		result = prime * result
				+ ((stsMerchantID == null) ? 0 : stsMerchantID.hashCode());
		result = prime * result
				+ ((stsPayType == null) ? 0 : stsPayType.hashCode());
		result = prime * result
				+ ((successPageUrl == null) ? 0 : successPageUrl.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentProviderSTSData other = (PaymentProviderSTSData) obj;
		if (clientIP == null) {
			if (other.clientIP != null)
				return false;
		} else if (!clientIP.equals(other.clientIP))
			return false;
		if (clientInfoRef == null) {
			if (other.clientInfoRef != null)
				return false;
		} else if (!clientInfoRef.equals(other.clientInfoRef))
			return false;
		if (clientUserAccount == null) {
			if (other.clientUserAccount != null)
				return false;
		} else if (!clientUserAccount.equals(other.clientUserAccount))
			return false;
		if (failurePageUrl == null) {
			if (other.failurePageUrl != null)
				return false;
		} else if (!failurePageUrl.equals(other.failurePageUrl))
			return false;
		if (merchantShopRef == null) {
			if (other.merchantShopRef != null)
				return false;
		} else if (!merchantShopRef.equals(other.merchantShopRef))
			return false;
		if (stsAdvanceDate == null) {
			if (other.stsAdvanceDate != null)
				return false;
		} else if (!stsAdvanceDate.equals(other.stsAdvanceDate))
			return false;
		if (stsBalanceDate == null) {
			if (other.stsBalanceDate != null)
				return false;
		} else if (!stsBalanceDate.equals(other.stsBalanceDate))
			return false;
		if (stsClientID == null) {
			if (other.stsClientID != null)
				return false;
		} else if (!stsClientID.equals(other.stsClientID))
			return false;
		if (stsDepartureDate == null) {
			if (other.stsDepartureDate != null)
				return false;
		} else if (!stsDepartureDate.equals(other.stsDepartureDate))
			return false;
		if (stsMerchantID == null) {
			if (other.stsMerchantID != null)
				return false;
		} else if (!stsMerchantID.equals(other.stsMerchantID))
			return false;
		if (stsPayType == null) {
			if (other.stsPayType != null)
				return false;
		} else if (!stsPayType.equals(other.stsPayType))
			return false;
		if (successPageUrl == null) {
			if (other.successPageUrl != null)
				return false;
		} else if (!successPageUrl.equals(other.successPageUrl))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "PaymentProviderSTSData [clientIP=" + clientIP
				+ ", clientInfoRef=" + clientInfoRef + ", clientUserAccount="
				+ clientUserAccount + ", failurePageUrl=" + failurePageUrl
				+ ", merchantShopRef=" + merchantShopRef + ", stsAdvanceDate="
				+ stsAdvanceDate + ", stsBalanceDate=" + stsBalanceDate
				+ ", stsClientID=" + stsClientID + ", stsDepartureDate="
				+ stsDepartureDate + ", stsMerchantID=" + stsMerchantID
				+ ", stsPayType=" + stsPayType + ", successPageUrl="
				+ successPageUrl + ", getComunication()=" + getComunication()
				+ "]";
	}
}