package com.alitalia.aem.common.data.home.checkin;

import com.alitalia.aem.common.data.MmbAncillaryDetail;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;

public class CheckinAncillaryUpgradeDetailData implements MmbAncillaryDetail {

    private MmbCompartimentalClassEnum compartimentalClass;
    private String seat;
    
	public MmbCompartimentalClassEnum getCompartimentalClass() {
		return compartimentalClass;
	}
	public void setCompartimentalClass(
			MmbCompartimentalClassEnum compartimentalClass) {
		this.compartimentalClass = compartimentalClass;
	}
	public String getSeat() {
		return seat;
	}
	public void setSeat(String seat) {
		this.seat = seat;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((compartimentalClass == null) ? 0 : compartimentalClass
						.hashCode());
		result = prime * result + ((seat == null) ? 0 : seat.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinAncillaryUpgradeDetailData other = (CheckinAncillaryUpgradeDetailData) obj;
		if (compartimentalClass != other.compartimentalClass)
			return false;
		if (seat == null) {
			if (other.seat != null)
				return false;
		} else if (!seat.equals(other.seat))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CheckinAncillaryUpgradeDetailData [compartimentalClass="
				+ compartimentalClass + ", seat=" + seat + "]";
	}
}