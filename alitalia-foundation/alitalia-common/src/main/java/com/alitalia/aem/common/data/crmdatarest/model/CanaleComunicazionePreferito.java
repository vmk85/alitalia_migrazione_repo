package com.alitalia.aem.common.data.crmdatarest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CanaleComunicazionePreferito {

    @SerializedName("tipo")
    @Expose
    private String tipo;

    @SerializedName("selezionato")
    @Expose
    private boolean selezionato = true;

    @SerializedName("dataUltimoAggiornamento")
    @Expose
    private String dataUltimoAggiornamento;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public boolean isSelezionato() {
        return selezionato;
    }

    public void setSelezionato(boolean selezionato) {
        this.selezionato = selezionato;
    }

    public String getDataUltimoAggiornamento() {
        return dataUltimoAggiornamento;
    }

    public void setDataUltimoAggiornamento(String dataUltimoAggiornamento) {
        this.dataUltimoAggiornamento = dataUltimoAggiornamento;
    }

    @Override
    public String toString() {
        return "CanaleComunicazionePreferito{" +
                "tipo='" + tipo + '\'' +
                ", selezionato=" + selezionato +
                ", dataUltimoAggiornamento='" + dataUltimoAggiornamento + '\'' +
                '}';
    }
}
