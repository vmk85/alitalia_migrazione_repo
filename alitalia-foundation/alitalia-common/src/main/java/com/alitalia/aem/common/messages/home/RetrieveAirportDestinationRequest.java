package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseRequest;

public class RetrieveAirportDestinationRequest extends BaseRequest {
	
	private String iata;
	private String market;
	private String language;
	private String conversationID;
	
	public RetrieveAirportDestinationRequest() {}

	public RetrieveAirportDestinationRequest(String tid, String sid) {
		super(tid, sid);
	}

	public String getIata() {
		return iata;
	}
	
	public void setIata(String iata) {
		this.iata = iata;
	}
	
	public String getMarket() {
		return market;
	}
	
	public void setMarket(String market) {
		this.market = market;
	}
	
	public String getLanguage() {
		return language;
	}
	
	public void setLanguage(String language) {
		this.language = language;
	}

	public String getConversationID() {
		return conversationID;
	}

	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}
	
}
