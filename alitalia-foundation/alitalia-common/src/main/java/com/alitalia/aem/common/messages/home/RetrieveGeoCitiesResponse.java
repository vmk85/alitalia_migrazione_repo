package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.CodeDescriptionData;
import com.alitalia.aem.common.messages.BaseResponse;

public class RetrieveGeoCitiesResponse extends BaseResponse {

	private List<CodeDescriptionData> cities;

	public List<CodeDescriptionData> getCities() {
		return cities;
	}

	public void setCities(List<CodeDescriptionData> cities) {
		this.cities = cities;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((cities == null) ? 0 : cities.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveGeoCitiesResponse other = (RetrieveGeoCitiesResponse) obj;
		if (cities == null) {
			if (other.cities != null)
				return false;
		} else if (!cities.equals(other.cities))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveGeoCitiesResponse [cities=" + cities + "]";
	}
}