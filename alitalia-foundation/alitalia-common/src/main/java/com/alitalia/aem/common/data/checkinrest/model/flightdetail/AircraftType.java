
package com.alitalia.aem.common.data.checkinrest.model.flightdetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AircraftType {

    @SerializedName("change")
    @Expose
    private String change;
    @SerializedName("value")
    @Expose
    private String value;

    public String getChange() {
        return change;
    }

    public void setChange(String change) {
        this.change = change;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
