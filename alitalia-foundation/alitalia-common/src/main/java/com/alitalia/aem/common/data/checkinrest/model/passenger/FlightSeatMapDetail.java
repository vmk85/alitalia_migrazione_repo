package com.alitalia.aem.common.data.checkinrest.model.passenger;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FlightSeatMapDetail {
	
	@SerializedName("seatDetail")
	@Expose
	private List<SeatDetail> seatDetail = null;
	
	@SerializedName("classOfService")
	@Expose
	private String classOfService;

	public List<SeatDetail> getSeatDetail() {
		return seatDetail;
	}

	public void setSeatDetail(List<SeatDetail> seatDetail) {
		this.seatDetail = seatDetail;
	}

	public String getClassOfService() {
		return classOfService;
	}

	public void setClassOfService(String classOfService) {
		this.classOfService = classOfService;
	}

}
