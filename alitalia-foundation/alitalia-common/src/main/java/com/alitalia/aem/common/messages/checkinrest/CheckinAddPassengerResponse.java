package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PnrListInfoByAddPassenger;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PnrListInfoByTicketSearch;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinAddPassengerResponse extends BaseResponse {

    private PnrListInfoByAddPassenger pnrData = null;

    public CheckinAddPassengerResponse() {}

    public CheckinAddPassengerResponse(String tid, String sid) {
        super(tid, sid);
    }

    public PnrListInfoByAddPassenger getPnrData() {
        return pnrData;
    }

    public void setPnrData(PnrListInfoByAddPassenger pnrData) {
        this.pnrData = pnrData;
    }

}
