package com.alitalia.aem.common.messages;

public class BaseResponse implements Response {

	private String tid;
	private String sid;
	private String cookie;
	private String execute;
	private String sabreGateWayAuthToken;
	
	public BaseResponse(){};
			
	public BaseResponse(String tid, String sid){
		this.tid = tid;
		this.sid = sid;
	}
	
	public String getTid() {
		return tid;
	}
	
	public void setTid(String tid) {
		this.tid = tid;
	}
	
	public String getSid() {
		return sid;
	}
	
	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getCookie() {
		return cookie;
	}

	public void setCookie(String cookie) {
		this.cookie = cookie;
	}

	public String getExecute() {
		return execute;
	}

	public void setExecute(String execute) {
		this.execute = execute;
	}

	public String getSabreGateWayAuthToken() {
		return sabreGateWayAuthToken;
	}

	public void setSabreGateWayAuthToken(String sabreGateWayAuthToken) {
		this.sabreGateWayAuthToken = sabreGateWayAuthToken;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cookie == null) ? 0 : cookie.hashCode());
		result = prime * result + ((execute == null) ? 0 : execute.hashCode());
		result = prime
				* result
				+ ((sabreGateWayAuthToken == null) ? 0 : sabreGateWayAuthToken
						.hashCode());
		result = prime * result + ((sid == null) ? 0 : sid.hashCode());
		result = prime * result + ((tid == null) ? 0 : tid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseResponse other = (BaseResponse) obj;
		if (cookie == null) {
			if (other.cookie != null)
				return false;
		} else if (!cookie.equals(other.cookie))
			return false;
		if (execute == null) {
			if (other.execute != null)
				return false;
		} else if (!execute.equals(other.execute))
			return false;
		if (sabreGateWayAuthToken == null) {
			if (other.sabreGateWayAuthToken != null)
				return false;
		} else if (!sabreGateWayAuthToken.equals(other.sabreGateWayAuthToken))
			return false;
		if (sid == null) {
			if (other.sid != null)
				return false;
		} else if (!sid.equals(other.sid))
			return false;
		if (tid == null) {
			if (other.tid != null)
				return false;
		} else if (!tid.equals(other.tid))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BaseResponse [tid=" + tid + ", sid=" + sid + ", cookie="
				+ cookie + ", execute=" + execute + ", sabreGateWayAuthToken="
				+ sabreGateWayAuthToken + "]";
	}
}