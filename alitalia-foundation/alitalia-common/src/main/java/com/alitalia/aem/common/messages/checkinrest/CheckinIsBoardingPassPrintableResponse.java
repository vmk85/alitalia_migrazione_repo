package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.boardingpassprintable.BoardingPassPrintable;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinIsBoardingPassPrintableResponse extends BaseResponse{

	private BoardingPassPrintable isPrintable = null;
	
	public CheckinIsBoardingPassPrintableResponse() {}

	public CheckinIsBoardingPassPrintableResponse(String tid, String sid) {
		super(tid, sid);
	}

	public BoardingPassPrintable getIsPrintable() {
		return isPrintable;
	}

	public void setIsPrintable(BoardingPassPrintable isPrintable) {
		this.isPrintable = isPrintable;
	}
	
	
}
