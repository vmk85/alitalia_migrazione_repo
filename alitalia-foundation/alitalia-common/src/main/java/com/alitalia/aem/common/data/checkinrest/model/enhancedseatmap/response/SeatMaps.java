
package com.alitalia.aem.common.data.checkinrest.model.enhancedseatmap.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SeatMaps {

    @SerializedName("cabinType")
    @Expose
    private String cabinType;
    @SerializedName("startingRow")
    @Expose
    private String startingRow;
    @SerializedName("endingRow")
    @Expose
    private String endingRow;
    @SerializedName("seatMapSectors")
    @Expose
    private List<SeatMapSector> seatMapSectors = null;
    @SerializedName("seatMapColumns")
    @Expose
    private List<SeatMapColumn> seatMapColumns = null;

    public String getCabinType() {
        return cabinType;
    }

    public void setCabinType(String cabinType) {
        this.cabinType = cabinType;
    }

    public String getStartingRow() {
        return startingRow;
    }

    public void setStartingRow(String startingRow) {
        this.startingRow = startingRow;
    }

    public String getEndingRow() {
        return endingRow;
    }

    public void setEndingRow(String endingRow) {
        this.endingRow = endingRow;
    }

    public List<SeatMapSector> getSeatMapSectors() {
        return seatMapSectors;
    }

    public void setSeatMapSectors(List<SeatMapSector> seatMapSectors) {
        this.seatMapSectors = seatMapSectors;
    }

    public List<SeatMapColumn> getSeatMapColumns() {
        return seatMapColumns;
    }

    public void setSeatMapColumns(List<SeatMapColumn> seatMapColumns) {
        this.seatMapColumns = seatMapColumns;
    }

}
