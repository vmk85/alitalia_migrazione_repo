package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.messages.BaseResponse;

public class SubscribeSACheckUserNameDuplicateResponse extends BaseResponse {


	private Integer returnCode;
	private String errorMessage;
	private String profileID;
	private boolean FlagNoUserNameDuplicate =false;

	public Integer getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(Integer returnCode) {
		this.returnCode = returnCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}



	public String getProfileID() {
		return profileID;
	}

	public void setProfileID(String errorMessage) {
		this.profileID = profileID;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
//		result = prime * result
//				+ ((customerProfile == null) ? 0 : customerProfile.hashCode());
		result = prime * result
				+ ((errorMessage == null) ? 0 : errorMessage.hashCode());
		result = prime * result
				+ ((returnCode == null) ? 0 : returnCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubscribeSACheckUserNameDuplicateResponse other = (SubscribeSACheckUserNameDuplicateResponse) obj;

//		if (customerProfile == null) {
//			if (other.customerProfile != null)
//				return false;
//		} else if (!customerProfile.equals(other.customerProfile))
//			return false;

		if (profileID == null) {
			if (other.profileID != null)
				return false;
		} else if (!profileID.equals(other.profileID))
			return false;

		if (errorMessage == null) {
			if (other.errorMessage != null)
				return false;
		} else if (!errorMessage.equals(other.errorMessage))
			return false;

		if (returnCode == null) {
			if (other.returnCode != null)
				return false;
		} else if (!returnCode.equals(other.returnCode))
			return false;

		return true;
	}

	@Override
	public String toString() {
//		return "SubscribeResponse [customerProfile=" + customerProfile
//				+ ", returnCode=" + returnCode + ", errorMessage="
//				+ errorMessage + ", getTid()=" + getTid() + ", getSid()="
//				+ getSid() + "]";

		return "SubscribeSACheckUserNameDuplicateResponse [profileID=" + profileID
				+ ", returnCode=" + returnCode + ", errorMessage="
				+ errorMessage + ", getTid()=" + getTid() + ", getSid()="
				+ getSid() + "]";
	}

	public boolean isFlagNoUserNameDuplicate() {
		return FlagNoUserNameDuplicate;
	}

	public void setFlagNoUserNameDuplicate(boolean flagNoUserNameDuplicate) {
		FlagNoUserNameDuplicate = flagNoUserNameDuplicate;
	}
}