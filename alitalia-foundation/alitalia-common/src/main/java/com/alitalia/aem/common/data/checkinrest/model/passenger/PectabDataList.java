package com.alitalia.aem.common.data.checkinrest.model.passenger;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PectabDataList {
	
	@SerializedName("version")
	@Expose
	private String version;
	
	@SerializedName("value")
	@Expose
	private String value;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
