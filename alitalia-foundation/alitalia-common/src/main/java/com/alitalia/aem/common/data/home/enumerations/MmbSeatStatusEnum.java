package com.alitalia.aem.common.data.home.enumerations;

public enum MmbSeatStatusEnum {

	RESERVED("Reserved"),
    CHECKED_IN("CheckedIn");
    private final String value;

    MmbSeatStatusEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MmbSeatStatusEnum fromValue(String v) {
        for (MmbSeatStatusEnum c: MmbSeatStatusEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
