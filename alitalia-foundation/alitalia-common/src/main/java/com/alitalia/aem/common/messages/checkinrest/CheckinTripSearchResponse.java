package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.tripsearch.Trip;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinTripSearchResponse extends BaseResponse{
	
	private Trip tripData = null;
	
	public CheckinTripSearchResponse(){}
	
	public CheckinTripSearchResponse(String tid, String sid) {
		super(tid, sid);
	}

	public Trip getTripData() {
		return tripData;
	}

	public void setTripData(Trip tripData) {
		this.tripData = tripData;
	}
	
	
}
