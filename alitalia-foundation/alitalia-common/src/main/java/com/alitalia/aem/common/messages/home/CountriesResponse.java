package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.countries.Countries;
import com.alitalia.aem.common.messages.BaseResponse;

public class CountriesResponse extends BaseResponse {
	
	private Countries countries;

	public CountriesResponse() {}

	public CountriesResponse(String tid, String sid) {
		super(tid, sid);
	}

	public Countries getCountries() {
		return countries;
	}

	public void setCountries(Countries countries) {
		this.countries = countries;
	}

}
