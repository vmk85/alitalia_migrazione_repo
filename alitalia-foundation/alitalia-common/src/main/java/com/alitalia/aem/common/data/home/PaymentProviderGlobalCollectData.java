package com.alitalia.aem.common.data.home;

import com.alitalia.aem.common.data.home.enumerations.GlobalCollectPaymentTypeEnum;

public class PaymentProviderGlobalCollectData extends PaymentProviderData {
	
	private Integer installementsNumber;
	private GlobalCollectPaymentTypeEnum type;
	private UserInfoBaseData userInfo;
	
	public Integer getInstallementsNumber() {
		return installementsNumber;
	}

	public void setInstallementsNumber(Integer installementsNumber) {
		this.installementsNumber = installementsNumber;
	}

	public GlobalCollectPaymentTypeEnum getType() {
		return type;
	}

	public void setType(GlobalCollectPaymentTypeEnum type) {
		this.type = type;
	}

	public UserInfoBaseData getUserInfo() {
		return userInfo;
	}
	
	public void setUserInfo(UserInfoBaseData userInfo) {
		this.userInfo = userInfo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((installementsNumber == null) ? 0 : installementsNumber
						.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result
				+ ((userInfo == null) ? 0 : userInfo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentProviderGlobalCollectData other = (PaymentProviderGlobalCollectData) obj;
		if (installementsNumber == null) {
			if (other.installementsNumber != null)
				return false;
		} else if (!installementsNumber.equals(other.installementsNumber))
			return false;
		if (type != other.type)
			return false;
		if (userInfo == null) {
			if (other.userInfo != null)
				return false;
		} else if (!userInfo.equals(other.userInfo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PaymentProviderGlobalCollectData [installementsNumber="
				+ installementsNumber + ", type=" + type + ", userInfo="
				+ userInfo + "]";
	}
	
}
