package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.AdyenRecurringData;
import com.alitalia.aem.common.messages.AdyenBaseRequest;

public class AdyenRetrievalStoredPaymentRequest  extends AdyenBaseRequest {
	
	/*
	{
	  "shopperReference": "39fbe13b0ffd46ef9ee08d09ec577034",
	  "recurring": {
	    "contract": "ONECLICK"
	  },
	  "language": "IT",
	  "market": "IT",
	  "conversationID": "987654327",
	  "caller": "WebCheckin"
	}	
	*/

	private AdyenRecurringData recurring = new AdyenRecurringData();
	
	public AdyenRecurringData getRecurring() {
		return recurring;
	}

	public void setRecurring(AdyenRecurringData recurring) {
		this.recurring = recurring;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result	+ ((recurring == null) ? 0 : recurring.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdyenRetrievalStoredPaymentRequest other = (AdyenRetrievalStoredPaymentRequest) obj;
		if (recurring == null) {
			if (other.recurring != null)
				return false;
		} else if (!recurring.equals(other.recurring))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "AdyenRetrievalStoredPaymentRequest ["
				+ "shopperReference=" + super.getShopperReference()
				+ "recurring.contract=" + recurring!=null?recurring.getContract():null
				+ "language=" + super.getLanguage()
				+ "market=" + super.getMarket()
				+ "conversationID=" + super.getConversationID()
				+ "caller=" + super.getCaller()
				+ "]";
	}
	
}