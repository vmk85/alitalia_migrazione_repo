
package com.alitalia.aem.common.data.checkinrest.model.updatesecurityinfo.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BagInfoList {

    @SerializedName("bagInfo")
    @Expose
    private List<BagInfo> bagInfo = null;
    @SerializedName("totalBagCount")
    @Expose
    private String totalBagCount;
    @SerializedName("totalBagWeight")
    @Expose
    private String totalBagWeight;
    @SerializedName("totalBagWeightUnit")
    @Expose
    private String totalBagWeightUnit;

    public List<BagInfo> getBagInfo() {
        return bagInfo;
    }

    public void setBagInfo(List<BagInfo> bagInfo) {
        this.bagInfo = bagInfo;
    }

    public String getTotalBagCount() {
        return totalBagCount;
    }

    public void setTotalBagCount(String totalBagCount) {
        this.totalBagCount = totalBagCount;
    }

    public String getTotalBagWeight() {
        return totalBagWeight;
    }

    public void setTotalBagWeight(String totalBagWeight) {
        this.totalBagWeight = totalBagWeight;
    }

    public String getTotalBagWeightUnit() {
        return totalBagWeightUnit;
    }

    public void setTotalBagWeightUnit(String totalBagWeightUnit) {
        this.totalBagWeightUnit = totalBagWeightUnit;
    }

}
