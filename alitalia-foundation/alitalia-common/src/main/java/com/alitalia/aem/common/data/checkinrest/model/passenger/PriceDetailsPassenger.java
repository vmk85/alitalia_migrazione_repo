package com.alitalia.aem.common.data.checkinrest.model.passenger;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PriceDetailsPassenger {
	
	@SerializedName("totalPrice")
	@Expose
	private TotalPricePassenger totalPrice;
	
	@SerializedName("basePrice")
	@Expose
	private BasePricePassenger basePrice;
	
	@SerializedName("equivalentPrice")
	@Expose
	private EquivalentPricePassenger equivalentPrice;
	
	@SerializedName("taxDetails")
	@Expose
	private TaxDetailsPassenger taxDetails;

	public TotalPricePassenger getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(TotalPricePassenger totalPrice) {
		this.totalPrice = totalPrice;
	}

	public BasePricePassenger getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(BasePricePassenger basePrice) {
		this.basePrice = basePrice;
	}

	public EquivalentPricePassenger getEquivalentPrice() {
		return equivalentPrice;
	}

	public void setEquivalentPrice(EquivalentPricePassenger equivalentPrice) {
		this.equivalentPrice = equivalentPrice;
	}

	public TaxDetailsPassenger getTaxDetails() {
		return taxDetails;
	}

	public void setTaxDetails(TaxDetailsPassenger taxDetails) {
		this.taxDetails = taxDetails;
	}
	
}
