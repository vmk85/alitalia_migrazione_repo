package com.alitalia.aem.common.logging.CrmData;

public interface CrmDataLogger {

    public void error(String log);

    public void debug(String log);

    public void info(String log);

    public void trace(String log);

}
