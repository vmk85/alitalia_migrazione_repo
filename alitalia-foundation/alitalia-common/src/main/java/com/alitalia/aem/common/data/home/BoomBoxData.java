package com.alitalia.aem.common.data.home;

public abstract class BoomBoxData {

	private String id;
	private String sessionId;
	private String solutionSet;
	private String solutionId;
	private String refreshSolutionId;
	private String farmId;
	private PropertiesData properties;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getSolutionSet() {
		return solutionSet;
	}

	public void setSolutionSet(String solutionSet) {
		this.solutionSet = solutionSet;
	}

	public String getSolutionId() {
		return solutionId;
	}

	public void setSolutionId(String solutionId) {
		this.solutionId = solutionId;
	}

	public String getRefreshSolutionId() {
		return refreshSolutionId;
	}

	public void setRefreshSolutionId(String refreshSolutionId) {
		this.refreshSolutionId = refreshSolutionId;
	}

	public String getFarmId() {
		return farmId;
	}

	public void setFarmId(String farmId) {
		this.farmId = farmId;
	}

	public PropertiesData getProperties() {
		return properties;
	}

	public void setProperties(PropertiesData properties) {
		this.properties = properties;
	}

	public void addProperty(String key, Object value) {
		if (this.properties == null)
			this.properties = new PropertiesData();
		
		this.properties.addElement(key, value);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((solutionId == null) ? 0 : solutionId.hashCode());
		result = prime * result
				+ ((solutionSet == null) ? 0 : solutionSet.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BoomBoxData other = (BoomBoxData) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (solutionId == null) {
			if (other.solutionId != null)
				return false;
		} else if (!solutionId.equals(other.solutionId))
			return false;
		if (solutionSet == null) {
			if (other.solutionSet != null)
				return false;
		} else if (!solutionSet.equals(other.solutionSet))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BoomBoxData [id=" + id + ", sessionId=" + sessionId
				+ ", solutionSet=" + solutionSet + ", solutionId=" + solutionId
				+ ", refreshSolutionId=" + refreshSolutionId + ", farmId="
				+ farmId + ", properties=" + properties + "]";
	}

}
