
package com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AncillaryOffers {

    @SerializedName("flightAncillaryOffers")
    @Expose
    private List<FlightAncillaryOffer> flightAncillaryOffers = null;
    @SerializedName("conversationID")
    @Expose
    private String conversationID;

    public List<FlightAncillaryOffer> getFlightAncillaryOffers() {
        return flightAncillaryOffers;
    }

    public void setFlightAncillaryOffers(List<FlightAncillaryOffer> flightAncillaryOffers) {
        this.flightAncillaryOffers = flightAncillaryOffers;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

}
