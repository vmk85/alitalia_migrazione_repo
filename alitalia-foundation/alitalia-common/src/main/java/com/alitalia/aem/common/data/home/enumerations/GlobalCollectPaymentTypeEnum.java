package com.alitalia.aem.common.data.home.enumerations;

public enum GlobalCollectPaymentTypeEnum {

    VISA("Visa"),
    AMERICAN_EXPRESS("AmericanExpress"),
    MASTER_CARD("MasterCard"),
    VISA_DELTA("VisaDelta"),
    VISA_DEBIT("VisaDebit"),
    MAESTRO("Maestro"),
    SOLO("Solo"),
    MASTER_CARD_DEBIT("MasterCardDebit"),
    VISA_ELECTRON("VisaElectron"),
    DANKORT("Dankort"),
    LASER("Laser"),
    JCB("JCB"),
    DISCOVER("Discover"),
    CARTE_BLEUE("CarteBleue"),
    IDEAL("Ideal"),
    YANDEX("Yandex"),
    SOFORT("Sofort"),
    WEB_MONEY("WebMoney"),
    DINERS("Diners");
    
    private final String value;

    GlobalCollectPaymentTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GlobalCollectPaymentTypeEnum fromValue(String v) {
        for (GlobalCollectPaymentTypeEnum c: GlobalCollectPaymentTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
