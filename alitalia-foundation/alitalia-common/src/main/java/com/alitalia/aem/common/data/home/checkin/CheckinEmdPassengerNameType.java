package com.alitalia.aem.common.data.home.checkin;

import java.util.List;

public class CheckinEmdPassengerNameType {

	private List<String> givenName;
    private String surname;
    
	public List<String> getGivenName() {
		return givenName;
	}
	public void setGivenName(List<String> givenName) {
		this.givenName = givenName;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((givenName == null) ? 0 : givenName.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinEmdPassengerNameType other = (CheckinEmdPassengerNameType) obj;
		if (givenName == null) {
			if (other.givenName != null)
				return false;
		} else if (!givenName.equals(other.givenName))
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CheckinEmdPassengerNameType [givenName=" + givenName
				+ ", surname=" + surname + "]";
	}
}