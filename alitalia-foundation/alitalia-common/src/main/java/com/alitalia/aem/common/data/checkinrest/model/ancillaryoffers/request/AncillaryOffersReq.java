
package com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.request;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AncillaryOffersReq {

    @SerializedName("pnr")
    @Expose
    private String pnr;
    @SerializedName("flightPassengers")
    @Expose
    private List<FlightPassenger> flightPassengers = null;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("market")
    @Expose
    private String market;
    @SerializedName("conversationID")
    @Expose
    private String conversationID;

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public List<FlightPassenger> getFlightPassengers() {
        return flightPassengers;
    }

    public void setFlightPassengers(List<FlightPassenger> flightPassengers) {
        this.flightPassengers = flightPassengers;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

}
