package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.IPAddressData;
import com.alitalia.aem.common.messages.BaseRequest;

public class RetrieveGeoCountryRequest extends BaseRequest {

    private IPAddressData ipAddress;
	
    public IPAddressData getIpAddress() {
		return ipAddress;
	}
	
    public void setIpAddress(IPAddressData ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((ipAddress == null) ? 0 : ipAddress.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrieveGeoCountryRequest other = (RetrieveGeoCountryRequest) obj;
		if (ipAddress == null) {
			if (other.ipAddress != null)
				return false;
		} else if (!ipAddress.equals(other.ipAddress))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RetrieveGeoCountriesRequest [ipAddress=" + ipAddress + "]";
	}
}