package com.alitalia.aem.common.messages.crmdatarest;

import java.util.List;

import com.alitalia.aem.common.data.crmdatarest.model.CanaleComunicazionePreferito;
import com.alitalia.aem.common.data.crmdatarest.model.Consenso;
import com.alitalia.aem.common.data.crmdatarest.model.PreferenzePersonali;
import com.alitalia.aem.common.messages.BaseRequest;

public class SetCRMDataNewsLetterRequest extends BaseRequest {
    
    
    private String email;
    private Boolean unsubscribeNewsletter;
    private List<CanaleComunicazionePreferito> canaleComunicazionePreferito;
    private List<Consenso> listaConsensi;
    private PreferenzePersonali preferenzePersonali;
    private String idMyAlitalia;
    private String canale;
    private String language;
    private String market;
    private String conversationID;

    public List<CanaleComunicazionePreferito> getCanaleComunicazionePreferito() {
        return canaleComunicazionePreferito;
    }

    public void setCanaleComunicazionePreferito(List<CanaleComunicazionePreferito> canaleComunicazionePreferito) {
        this.canaleComunicazionePreferito = canaleComunicazionePreferito;
    }

    public PreferenzePersonali getPreferenzePersonali() {
        return preferenzePersonali;
    }

    public void setPreferenzePersonali(PreferenzePersonali preferenzePersonali) {
        this.preferenzePersonali = preferenzePersonali;
    }

    public SetCRMDataNewsLetterRequest() {}

    public SetCRMDataNewsLetterRequest(String tid, String sid) {
        super(tid, sid);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getUnsubscribeNewsletter() {
        return unsubscribeNewsletter;
    }
    
    public void setUnsubscribeNewsletter(Boolean unsubscribeNewsletter) {
        this.unsubscribeNewsletter = unsubscribeNewsletter;
    }
    
    public List<Consenso> getListaConsensi() {
        return listaConsensi;
    }
    
    public String getIdMyAlitalia() {
        return idMyAlitalia;
    }
    
    public void setIdMyAlitalia(String idMyAlitalia) {
        this.idMyAlitalia = idMyAlitalia;
    }
    
    public String getCanale() {
        return canale;
    }
    
    public void setCanale(String canale) {
        this.canale = canale;
    }
    
    public void setListaConsensi(List<Consenso> listaConsensi) {
        this.listaConsensi = listaConsensi;
    }
    
    public String getLanguage() {
        return language;
    }
    
    public void setLanguage(String language) {
        this.language = language;
    }
    
    public String getMarket() {
        return market;
    }
    
    public void setMarket(String market) {
        this.market = market;
    }
    
    public String getConversationID() {
        return conversationID;
    }
    
    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

    @Override
    public String toString() {
        return "SetCRMDataNewsLetterRequest{" +
                "email='" + email + '\'' +
                ", unsubscribeNewsletter=" + unsubscribeNewsletter +
                ", listaConsensi=" + listaConsensi +
                ", idMyAlitalia='" + idMyAlitalia + '\'' +
                ", canale='" + canale + '\'' +
                ", language='" + language + '\'' +
                ", market='" + market + '\'' +
                ", conversationID='" + conversationID + '\'' +
                '}';
    }
}
