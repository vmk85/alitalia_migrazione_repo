
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BagTagInfo {

    @SerializedName("bagTagNumber")
    @Expose
    private String bagTagNumber;
    @SerializedName("weightAndSize")
    @Expose
    private WeightAndSize_ weightAndSize;
    @SerializedName("bagStatus")
    @Expose
    private BagStatus bagStatus;

    public String getBagTagNumber() {
        return bagTagNumber;
    }

    public void setBagTagNumber(String bagTagNumber) {
        this.bagTagNumber = bagTagNumber;
    }

    public WeightAndSize_ getWeightAndSize() {
        return weightAndSize;
    }

    public void setWeightAndSize(WeightAndSize_ weightAndSize) {
        this.weightAndSize = weightAndSize;
    }

    public BagStatus getBagStatus() {
        return bagStatus;
    }

    public void setBagStatus(BagStatus bagStatus) {
        this.bagStatus = bagStatus;
    }

}
