
package com.alitalia.aem.common.data.checkinrest.model.endpayment.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ancillary {

    @SerializedName("ancillaryID")
    @Expose
    private String ancillaryID;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("emdType")
    @Expose
    private String emdType;
    @SerializedName("commercialName")
    @Expose
    private String commercialName;
    @SerializedName("rficCode")
    @Expose
    private String rficCode;
    @SerializedName("numberOfItems")
    @Expose
    private Integer numberOfItems;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("group")
    @Expose
    private String group;
    @SerializedName("rficSubcode")
    @Expose
    private String rficSubcode;
    @SerializedName("carrierCode")
    @Expose
    private String carrierCode;
    @SerializedName("passengerTypeCode")
    @Expose
    private String passengerTypeCode;
    @SerializedName("ancillaryDetail")
    @Expose
    private AncillaryDetail ancillaryDetail;

    public String getAncillaryID() {
        return ancillaryID;
    }

    public void setAncillaryID(String ancillaryID) {
        this.ancillaryID = ancillaryID;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getEmdType() {
        return emdType;
    }

    public void setEmdType(String emdType) {
        this.emdType = emdType;
    }

    public String getCommercialName() {
        return commercialName;
    }

    public void setCommercialName(String commercialName) {
        this.commercialName = commercialName;
    }

    public String getRficCode() {
        return rficCode;
    }

    public void setRficCode(String rficCode) {
        this.rficCode = rficCode;
    }

    public Integer getNumberOfItems() {
        return numberOfItems;
    }

    public void setNumberOfItems(Integer numberOfItems) {
        this.numberOfItems = numberOfItems;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getRficSubcode() {
        return rficSubcode;
    }

    public void setRficSubcode(String rficSubcode) {
        this.rficSubcode = rficSubcode;
    }

    public String getCarrierCode() {
        return carrierCode;
    }

    public void setCarrierCode(String carrierCode) {
        this.carrierCode = carrierCode;
    }

    public String getPassengerTypeCode() {
        return passengerTypeCode;
    }

    public void setPassengerTypeCode(String passengerTypeCode) {
        this.passengerTypeCode = passengerTypeCode;
    }

    public AncillaryDetail getAncillaryDetail() {
        return ancillaryDetail;
    }

    public void setAncillaryDetail(AncillaryDetail ancillaryDetail) {
        this.ancillaryDetail = ancillaryDetail;
    }

}
