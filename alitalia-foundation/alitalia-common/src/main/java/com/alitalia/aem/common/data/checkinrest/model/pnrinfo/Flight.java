
package com.alitalia.aem.common.data.checkinrest.model.pnrinfo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Flight {

    @SerializedName("origin")
    @Expose
    private Origin origin;
    @SerializedName("destination")
    @Expose
    private Destination destination;
    @SerializedName("arrivalDate")
    @Expose
    private String arrivalDate;
    @SerializedName("departureDate")
    @Expose
    private String departureDate;
    @SerializedName("segments")
    @Expose
    private List<Segment> segments = null;
    @SerializedName("isWebCheckinPermitted")
    @Expose
    private Boolean isWebCheckinPermitted;
    @SerializedName("isBpPrintPermitted")
    @Expose
    private Boolean isBpPrintPermitted;
    @SerializedName("tooLate")
    @Expose
    private Boolean tooLate;
    @SerializedName("isVoloAlitalia")
    @Expose
    private Boolean isVoloAlitalia;

    public Boolean getVoloAlitalia() {
        return isVoloAlitalia;
    }

    public void setVoloAlitalia(Boolean voloAlitalia) {
        isVoloAlitalia = voloAlitalia;
    }

    public Origin getOrigin() {
        return origin;
    }

    public void setOrigin(Origin origin) {
        this.origin = origin;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public List<Segment> getSegments() {
        return segments;
    }

    public void setSegments(List<Segment> segments) {
        this.segments = segments;
    }

    public Boolean getIsWebCheckinPermitted() {
        return isWebCheckinPermitted;
    }

    public void setIsWebCheckinPermitted(Boolean isWebCheckinPermitted) {
        this.isWebCheckinPermitted = isWebCheckinPermitted;
    }

    public Boolean getIsBpPrintPermitted() {
        return isBpPrintPermitted;
    }

    public void setIsBpPrintPermitted(Boolean isBpPrintPermitted) {
        this.isBpPrintPermitted = isBpPrintPermitted;
    }

    public Boolean getTooLate() {
        return tooLate;
    }

    public void setTooLate(Boolean tooLate) {
        this.tooLate = tooLate;
    }

    // nrPassengers
    public int getNrPassengers() {
        if (getSegments() == null) return 0;
        if(getSegments().get(0).getOpenCI()){ //il primo segmento detiene le informazioni dei passeggeri per tutto l'itinerario/flight/volo
            if(getSegments().get(0).getPassengers() != null) {//i passeggeri possono essere null nel caso in cui la tratta sia operata da un altro operatore
                return getSegments().get(0).getPassengers().size();
            }
        }
        return 0;
    }

    public int getNrCheckinNotAllowed() {
        int total =0;
        int executed = 0;
        int notAllowed = 0;
        if(getSegments().get(0).getOpenCI()){ //il primo segmento detiene le informazioni dei passeggeri per tutto l'itinerario/flight/volo
            if(getSegments().get(0).getPassengers() != null){//i passeggeri possono essere null nel caso in cui la tratta sia operata da un altro operatore
                total = getSegments().get(0).getPassengers().size();
                for(int p = 0; p < total; p++){
                    if(getSegments().get(0).getPassengers().get(p).getCheckInComplete()){
                        executed++;
                    }
                }
                notAllowed = total - executed;
            }
        }
        return notAllowed;
    }

    // nrCheckinExecuted
    public int getNrCheckinExecuted() {
        int executed = 0;
        if(getSegments().get(0).getOpenCI()){ //il primo segmento detiene le informazioni dei passeggeri per tutto l'itinerario/flight/volo
            if(getSegments().get(0).getPassengers() != null){//i passeggeri possono essere null nel caso in cui la tratta sia operata da un altro operatore
                for(int p = 0; p < getSegments().get(0).getPassengers().size(); p++){
                    if(getSegments().get(0).getPassengers().get(p).getCheckInComplete()){
                        executed++;
                    }
                }
            }
        }
        return executed;
    }

}
