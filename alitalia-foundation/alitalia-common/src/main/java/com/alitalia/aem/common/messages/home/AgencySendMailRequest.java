package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.EmailMessage;
import com.alitalia.aem.common.messages.BaseRequest;

public class AgencySendMailRequest extends BaseRequest {

	private EmailMessage emailMessage;

	public EmailMessage getEmailMessage() {
		return emailMessage;
	}

	public void setEmailMessage(EmailMessage emailMessage) {
		this.emailMessage = emailMessage;
	}

	@Override
	public String toString() {
		return "SendMailRequest [emailMessage=" + emailMessage + ", getTid()="
				+ getTid() + ", getSid()=" + getSid() + "]";
	}
}
