
package com.alitalia.aem.common.data.checkinrest.model.pnrinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Emd {

    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("ancillaryID")
    @Expose
    private Object ancillaryID;
    @SerializedName("actionCode")
    @Expose
    private String actionCode;
    @SerializedName("code")
    @Expose
    private Object code;
    @SerializedName("group")
    @Expose
    private Object group;
    @SerializedName("rficCode")
    @Expose
    private String rficCode;
    @SerializedName("rficSubcode")
    @Expose
    private String rficSubcode;
    @SerializedName("ancillaryType")
    @Expose
    private Integer ancillaryType;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("currency")
    @Expose
    private Object currency;
    @SerializedName("pdcSeat")
    @Expose
    private Object pdcSeat;
    @SerializedName("commercialName")
    @Expose
    private String commercialName;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Object getAncillaryID() {
        return ancillaryID;
    }

    public void setAncillaryID(Object ancillaryID) {
        this.ancillaryID = ancillaryID;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public Object getCode() {
        return code;
    }

    public void setCode(Object code) {
        this.code = code;
    }

    public Object getGroup() {
        return group;
    }

    public void setGroup(Object group) {
        this.group = group;
    }

    public String getRficCode() {
        return rficCode;
    }

    public void setRficCode(String rficCode) {
        this.rficCode = rficCode;
    }

    public String getRficSubcode() {
        return rficSubcode;
    }

    public void setRficSubcode(String rficSubcode) {
        this.rficSubcode = rficSubcode;
    }

    public Integer getAncillaryType() {
        return ancillaryType;
    }

    public void setAncillaryType(Integer ancillaryType) {
        this.ancillaryType = ancillaryType;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Object getCurrency() {
        return currency;
    }

    public void setCurrency(Object currency) {
        this.currency = currency;
    }

    public Object getPdcSeat() {
        return pdcSeat;
    }

    public void setPdcSeat(Object pdcSeat) {
        this.pdcSeat = pdcSeat;
    }

    public String getCommercialName() {
        return commercialName;
    }

    public void setCommercialName(String commercialName) {
        this.commercialName = commercialName;
    }

}
/*
public class Emd {

    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("ancillaryID")
    @Expose
    private String ancillaryID;
    @SerializedName("actionCode")
    @Expose
    private String actionCode;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("group")
    @Expose
    private String group;
    @SerializedName("rficCode")
    @Expose
    private String rficCode;
    @SerializedName("ancillaryType")
    @Expose
    private String ancillaryType;
    @SerializedName("price")
    @Expose
    private Float price;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("pdcSeat")
    @Expose
    private String pdcSeat;
    @SerializedName("commercialName")
    @Expose
    private String commercialName;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAncillaryID() {
        return ancillaryID;
    }

    public void setAncillaryID(String ancillaryID) {
        this.ancillaryID = ancillaryID;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getRficCode() {
        return rficCode;
    }

    public void setRficCode(String rficCode) {
        this.rficCode = rficCode;
    }

    public String getAncillaryType() {
        return ancillaryType;
    }

    public void setAncillaryType(String ancillaryType) {
        this.ancillaryType = ancillaryType;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPdcSeat() {
        return pdcSeat;
    }

    public void setPdcSeat(String pdcSeat) {
        this.pdcSeat = pdcSeat;
    }

    public String getCommercialName() {
        return commercialName;
    }

    public void setCommercialName(String commercialName) {
        this.commercialName = commercialName;
    }

}*/
