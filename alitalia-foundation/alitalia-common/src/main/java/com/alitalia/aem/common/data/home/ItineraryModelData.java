package com.alitalia.aem.common.data.home;

import java.util.Calendar;
import java.util.List;

public class ItineraryModelData {

	private Calendar dateFrom;
    private Calendar dateTo;
    private List<FlightItinerayData> flights;
    private DaysOfWeekData frequency;
    private String key;
    private Integer numberOfConnection;
	
    public Calendar getDateFrom() {
		return dateFrom;
	}
    
	public void setDateFrom(Calendar dateFrom) {
		this.dateFrom = dateFrom;
	}
	
	public Calendar getDateTo() {
		return dateTo;
	}
	
	public void setDateTo(Calendar dateTo) {
		this.dateTo = dateTo;
	}
	
	public List<FlightItinerayData> getFlights() {
		return flights;
	}
	
	public void setFlights(List<FlightItinerayData> flights) {
		this.flights = flights;
	}
	
	public DaysOfWeekData getFrequency() {
		return frequency;
	}
	
	public void setFrequency(DaysOfWeekData frequency) {
		this.frequency = frequency;
	}
	
	public String getKey() {
		return key;
	}
	
	public void setKey(String key) {
		this.key = key;
	}
	
	public Integer getNumberOfConnection() {
		return numberOfConnection;
	}
	
	public void setNumberOfConnection(Integer numberOfConnection) {
		this.numberOfConnection = numberOfConnection;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dateFrom == null) ? 0 : dateFrom.hashCode());
		result = prime * result + ((dateTo == null) ? 0 : dateTo.hashCode());
		result = prime * result + ((flights == null) ? 0 : flights.hashCode());
		result = prime * result
				+ ((frequency == null) ? 0 : frequency.hashCode());
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime
				* result
				+ ((numberOfConnection == null) ? 0 : numberOfConnection
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItineraryModelData other = (ItineraryModelData) obj;
		if (dateFrom == null) {
			if (other.dateFrom != null)
				return false;
		} else if (!dateFrom.equals(other.dateFrom))
			return false;
		if (dateTo == null) {
			if (other.dateTo != null)
				return false;
		} else if (!dateTo.equals(other.dateTo))
			return false;
		if (flights == null) {
			if (other.flights != null)
				return false;
		} else if (!flights.equals(other.flights))
			return false;
		if (frequency == null) {
			if (other.frequency != null)
				return false;
		} else if (!frequency.equals(other.frequency))
			return false;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (numberOfConnection == null) {
			if (other.numberOfConnection != null)
				return false;
		} else if (!numberOfConnection.equals(other.numberOfConnection))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ItineraryModelData [dateFrom=" + dateFrom + ", dateTo="
				+ dateTo + ", flights=" + flights + ", frequency=" + frequency
				+ ", key=" + key + ", numberOfConnection=" + numberOfConnection
				+ "]";
	}
}