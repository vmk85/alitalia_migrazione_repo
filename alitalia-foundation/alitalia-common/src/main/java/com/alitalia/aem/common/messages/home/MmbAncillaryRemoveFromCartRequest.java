package com.alitalia.aem.common.messages.home;

import java.util.List;

import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryCartItemData;
import com.alitalia.aem.common.messages.MmbBaseRequest;

public class MmbAncillaryRemoveFromCartRequest extends MmbBaseRequest {

	private String sessionId;
	private String machineName;
	private Long cartId;
	private List<Integer> itemIdsToRemove;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getMachineName() {
		return machineName;
	}

	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}

	public Long getCartId() {
		return cartId;
	}

	public void setCartId(Long cartId) {
		this.cartId = cartId;
	}

	public List<Integer> getItemIdsToRemove() {
		return itemIdsToRemove;
	}

	public void setItemIdsToRemove(List<Integer> itemIdsToRemove) {
		this.itemIdsToRemove = itemIdsToRemove;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((cartId == null) ? 0 : cartId.hashCode());
		result = prime * result
				+ ((itemIdsToRemove == null) ? 0 : itemIdsToRemove.hashCode());
		result = prime * result
				+ ((machineName == null) ? 0 : machineName.hashCode());
		result = prime * result
				+ ((sessionId == null) ? 0 : sessionId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MmbAncillaryRemoveFromCartRequest other = (MmbAncillaryRemoveFromCartRequest) obj;
		if (cartId == null) {
			if (other.cartId != null)
				return false;
		} else if (!cartId.equals(other.cartId))
			return false;
		if (itemIdsToRemove == null) {
			if (other.itemIdsToRemove != null)
				return false;
		} else if (!itemIdsToRemove.equals(other.itemIdsToRemove))
			return false;
		if (machineName == null) {
			if (other.machineName != null)
				return false;
		} else if (!machineName.equals(other.machineName))
			return false;
		if (sessionId == null) {
			if (other.sessionId != null)
				return false;
		} else if (!sessionId.equals(other.sessionId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MmbAncillaryRemoveFromCartRequest [sessionId=" + sessionId
				+ ", machineName=" + machineName + ", cartId=" + cartId
				+ ", itemIdsToRemove=" + itemIdsToRemove + ", getBaseInfo()="
				+ getBaseInfo() + ", getClient()=" + getClient()
				+ ", getTid()=" + getTid() + ", getSid()=" + getSid() + "]";
	}

}
