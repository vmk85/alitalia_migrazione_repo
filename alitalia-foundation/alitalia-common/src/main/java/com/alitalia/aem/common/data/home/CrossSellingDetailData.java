package com.alitalia.aem.common.data.home;

public class CrossSellingDetailData extends CrossSellingBaseData {

	private Double price;
	private String priceDesription;

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getPriceDesription() {
		return priceDesription;
	}

	public void setPriceDesription(String priceDesription) {
		this.priceDesription = priceDesription;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((priceDesription == null) ? 0 : priceDesription.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CrossSellingDetailData other = (CrossSellingDetailData) obj;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (priceDesription == null) {
			if (other.priceDesription != null)
				return false;
		} else if (!priceDesription.equals(other.priceDesription))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CrossSellingDetailData [price=" + price + ", priceDesription=" + priceDesription + "]";
	}

}
