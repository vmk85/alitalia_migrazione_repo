package com.alitalia.aem.common.data.home;

public class BrandPenaltiesData {

	private BrandPenaltyData changeAfterDepature;
	private BrandPenaltyData changeBeforeDepature;
	private BrandPenaltyData refundAfterDepature;
	private BrandPenaltyData refundBeforeDepature;
	
	public BrandPenaltiesData() {
		super();
	}
	
	public BrandPenaltiesData(BrandPenaltiesData clone) {
		super();
		this.changeAfterDepature = new BrandPenaltyData(clone.getChangeAfterDepature());
		this.changeBeforeDepature = new BrandPenaltyData(clone.getChangeBeforeDepature());
		this.refundAfterDepature = new BrandPenaltyData(clone.getRefundAfterDepature());
		this.refundBeforeDepature = new BrandPenaltyData(clone.getRefundBeforeDepature());
	}

	public BrandPenaltyData getChangeAfterDepature() {
		return changeAfterDepature;
	}

	public void setChangeAfterDepature(BrandPenaltyData changeAfterDepature) {
		this.changeAfterDepature = changeAfterDepature;
	}

	public BrandPenaltyData getChangeBeforeDepature() {
		return changeBeforeDepature;
	}

	public void setChangeBeforeDepature(BrandPenaltyData changeBeforeDepature) {
		this.changeBeforeDepature = changeBeforeDepature;
	}

	public BrandPenaltyData getRefundAfterDepature() {
		return refundAfterDepature;
	}

	public void setRefundAfterDepature(BrandPenaltyData refundAfterDepature) {
		this.refundAfterDepature = refundAfterDepature;
	}

	public BrandPenaltyData getRefundBeforeDepature() {
		return refundBeforeDepature;
	}

	public void setRefundBeforeDepature(BrandPenaltyData refundBeforeDepature) {
		this.refundBeforeDepature = refundBeforeDepature;
	}

	@Override
	public String toString() {
		return "BrandPenatiesData [changeAfterDepature=" + changeAfterDepature
				+ ", changeBeforeDepature=" + changeBeforeDepature
				+ ", refundAfterDepature=" + refundAfterDepature
				+ ", refundBeforeDepature=" + refundBeforeDepature + "]";
	}

}
