package com.alitalia.aem.common.messages.checkinrest;

import com.alitalia.aem.common.data.checkinrest.model.flightdetail.FlightDetail;
import com.alitalia.aem.common.messages.BaseResponse;

public class CheckinFlightDetailResponse extends BaseResponse{
	
	private FlightDetail flightDetail;
	
	public CheckinFlightDetailResponse(){}
	
	public CheckinFlightDetailResponse(String tid, String sid) {
		super(tid, sid);
	}
	
	public FlightDetail getFlightDetail() {
		return flightDetail;
	}

	public void setFlightDetail(FlightDetail flightDetail) {
		this.flightDetail = flightDetail;
	}
}
