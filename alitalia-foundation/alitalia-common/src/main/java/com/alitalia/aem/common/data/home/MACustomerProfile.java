package com.alitalia.aem.common.data.home;

import com.alitalia.aem.common.data.home.mmb.ProfileComplete;

public class MACustomerProfile {

    private String email;
    private String firstName;
    private String gender;
    private String lastName;
    private String professionalHeadline;
    private String photoURL;
    private String birthDay;
    private String birthMonth;
    private String birthYear;
    private ProfileComplete profileComplete;

    public ProfileComplete getProfileComplete() {
        return profileComplete;
    }

    public void setProfileComplete(ProfileComplete profileComplete) {
        this.profileComplete = profileComplete;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfessionalHeadline() {
        return professionalHeadline;
    }

    public void setProfessionalHeadline(String professionalHeadline) {
        this.professionalHeadline = professionalHeadline;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getBirthMonth() {
        return birthMonth;
    }

    public void setBirthMonth(String birthMonth) {
        this.birthMonth = birthMonth;
    }

    public String getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
    }

    @Override
    public String toString() {
        return "MACustomerProfile{" +
                "email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", gender='" + gender + '\'' +
                ", lastName='" + lastName + '\'' +
                ", professionalHeadline='" + professionalHeadline + '\'' +
                ", photoURL='" + photoURL + '\'' +
                ", birthDay='" + birthDay + '\'' +
                ", birthMonth='" + birthMonth + '\'' +
                ", birthYear='" + birthYear + '\'' +
                ", profileComplete=" + profileComplete +
                '}';
    }
}
