
package com.alitalia.aem.common.data.checkinrest.model.initpayment.request;

public class InsurancePayment {


    private Boolean buyInsurance;

    private String insuranceAmount;
 
    private String policyNumber;

    public Boolean getBuyInsurance() {
        return buyInsurance;
    }

    public void setBuyInsurance(Boolean buyInsurance) {
        this.buyInsurance = buyInsurance;
    }

    public String getInsuranceAmount() {
        return insuranceAmount;
    }

    public void setInsuranceAmount(String insuranceAmount) {
        this.insuranceAmount = insuranceAmount;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

}
