
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TaxDetails {

    @SerializedName("totalTaxAmount")
    @Expose
    private TotalTaxAmount totalTaxAmount;
    @SerializedName("taxAmount")
    @Expose
    private List<TaxAmount> taxAmount = null;

    public TotalTaxAmount getTotalTaxAmount() {
        return totalTaxAmount;
    }

    public void setTotalTaxAmount(TotalTaxAmount totalTaxAmount) {
        this.totalTaxAmount = totalTaxAmount;
    }

    public List<TaxAmount> getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(List<TaxAmount> taxAmount) {
        this.taxAmount = taxAmount;
    }

}
