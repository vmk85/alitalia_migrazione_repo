package com.alitalia.aem.common.messages.home;

import com.alitalia.aem.common.data.home.MMCustomerSA;
import com.alitalia.aem.common.messages.BaseResponse;

public class MilleMigliaLoginSAResponse extends BaseResponse {

    private MMCustomerSA customerSA;
    private String SSO_token;
    private String errorCode;
    private String errorDescription;
    private boolean loginSuccessful;

    public boolean isLoginSuccessful() {
        return loginSuccessful;
    }

    public void setLoginSuccessful(boolean loginSuccessful) {
        this.loginSuccessful = loginSuccessful;
    }

    public MilleMigliaLoginSAResponse() {
        super();
    }

    public MMCustomerSA getCustomerSA() {
        return customerSA;
    }

    public void setCustomerSA(MMCustomerSA customerSA) {
        this.customerSA = customerSA;
    }

    public String getSSO_token() {
        return SSO_token;
    }

    public void setSSO_token(String SSO_token) {
        this.SSO_token = SSO_token;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }
}
