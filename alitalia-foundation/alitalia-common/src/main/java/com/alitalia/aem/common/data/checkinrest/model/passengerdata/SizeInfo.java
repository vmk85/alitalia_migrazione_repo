
package com.alitalia.aem.common.data.checkinrest.model.passengerdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SizeInfo {

    @SerializedName("bagSizeInfo")
    @Expose
    private BagSizeInfo bagSizeInfo;
    @SerializedName("bagSizeLimit")
    @Expose
    private String bagSizeLimit;
    @SerializedName("bagSizeLimitSpecified")
    @Expose
    private Boolean bagSizeLimitSpecified;

    public BagSizeInfo getBagSizeInfo() {
        return bagSizeInfo;
    }

    public void setBagSizeInfo(BagSizeInfo bagSizeInfo) {
        this.bagSizeInfo = bagSizeInfo;
    }

    public String getBagSizeLimit() {
        return bagSizeLimit;
    }

    public void setBagSizeLimit(String bagSizeLimit) {
        this.bagSizeLimit = bagSizeLimit;
    }

    public Boolean getBagSizeLimitSpecified() {
        return bagSizeLimitSpecified;
    }

    public void setBagSizeLimitSpecified(Boolean bagSizeLimitSpecified) {
        this.bagSizeLimitSpecified = bagSizeLimitSpecified;
    }

}
