package com.alitalia.aem.common.data.checkinrest.model.offload.response;

/**
 * Created by ggadaleta on 25/02/2018.
 */

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PnrFlightInfoR {

    @SerializedName("pnr")
    @Expose
    private String pnr;
    @SerializedName("flightPassRS")
    @Expose
    private List<FlightPassR> flightPassRS = null;

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public List<FlightPassR> getFlightPassRS() {
        return flightPassRS;
    }

    public void setFlightPassRS(List<FlightPassR> flightPassRS) {
        this.flightPassRS = flightPassRS;
    }

}
