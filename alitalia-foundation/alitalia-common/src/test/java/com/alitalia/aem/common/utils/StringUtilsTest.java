package com.alitalia.aem.common.utils;

import org.junit.Test;

import static org.junit.Assert.*;

public class StringUtilsTest {

    @Test
    public void capitalize() {

        assertEquals("Test Enrico", StringUtils.capitalize("TEST ENRICO"));
        assertEquals("Test", StringUtils.capitalize("TEST"));
        assertEquals("Test", StringUtils.capitalize("test"));
        assertEquals("", StringUtils.capitalize(null));
        assertEquals("", StringUtils.capitalize(""));
        assertEquals("Test", StringUtils.capitalize("tEst "));
    }
}