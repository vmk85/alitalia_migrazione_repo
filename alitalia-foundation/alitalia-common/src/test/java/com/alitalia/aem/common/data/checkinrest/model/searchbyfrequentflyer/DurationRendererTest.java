package com.alitalia.aem.common.data.checkinrest.model.searchbyfrequentflyer;

import org.junit.Test;

import static org.junit.Assert.*;

public class DurationRendererTest {


    @Test
    public void testFormattazione() {
        assertEquals("12h 43m",  new DurationRenderer("12:43:00").toString());
        assertEquals("11h", new DurationRenderer("11:00:00").toString());
        assertEquals("5h 9m", new DurationRenderer("05:09:00").toString());
        assertEquals("", new DurationRenderer(null).toString());
    }

}