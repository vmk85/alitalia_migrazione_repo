package com.alitalia.aem.common.data.checkinrest.model.searchbyfrequentflyer;

import org.junit.Test;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.*;

public class CheckinDateRenderTest {

    @Test
    public void testToLocalDate() {
        String departureDate = "2018-04-04T21:50:00Z";
        Date localDate = null;
        try {
            localDate = SegmentDateRender.toDate(departureDate);
            Calendar cal = Calendar.getInstance();
            cal.setTime(localDate);
            assertEquals(3,  cal.get(Calendar.MONTH));
            assertEquals(2018, cal.get(Calendar.YEAR));
            assertEquals(4, cal.get(Calendar.DAY_OF_MONTH));
        } catch (ParseException e) {
            fail(e.getMessage());
        }




    }
}