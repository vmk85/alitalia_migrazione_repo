package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.LoadWidgetCashMilesRequest;
import com.alitalia.aem.common.messages.home.LoadWidgetCashMilesResponse;
import com.alitalia.aem.common.messages.home.LoadWidgetECouponRequest;
import com.alitalia.aem.common.messages.home.LoadWidgetECouponResponse;
import com.alitalia.aem.common.messages.home.LoadWidgetOneClickRequest;
import com.alitalia.aem.common.messages.home.LoadWidgetOneClickResponse;
import com.alitalia.aem.common.messages.home.UpdateWidgetCashMilesRequest;
import com.alitalia.aem.common.messages.home.UpdateWidgetCashMilesResponse;

public interface BookingWidgetService {

	LoadWidgetECouponResponse loadWidgetECoupon(LoadWidgetECouponRequest request);
	LoadWidgetCashMilesResponse loadWidgetCashMiles(LoadWidgetCashMilesRequest request);
	UpdateWidgetCashMilesResponse updateWidgetCashMiles(UpdateWidgetCashMilesRequest request);
	LoadWidgetOneClickResponse loadWidgetOneClick(LoadWidgetOneClickRequest request);
}