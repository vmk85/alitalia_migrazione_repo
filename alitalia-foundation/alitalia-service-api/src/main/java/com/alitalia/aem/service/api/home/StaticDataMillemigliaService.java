package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.RetrieveSecureQuestionListRequest;
import com.alitalia.aem.common.messages.home.RetrieveSecureQuestionListResponse;
import com.alitalia.aem.common.messages.home.RetrieveStateListRequest;
import com.alitalia.aem.common.messages.home.RetrieveStateListResponse;

import com.alitalia.aem.common.messages.home.RetrieveSecureQuestionListRequest;
import com.alitalia.aem.common.messages.home.RetrieveSecureQuestionListResponse;

public interface StaticDataMillemigliaService {
	
	RetrieveStateListResponse retrieveStatesList(RetrieveStateListRequest request);

	RetrieveSecureQuestionListResponse retrieveSecureQuestionList(RetrieveSecureQuestionListRequest request);
}