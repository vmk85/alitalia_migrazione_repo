package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.CarnetCountryStaticDataResponse;
import com.alitalia.aem.common.messages.home.CarnetPaymentTypeResponse;
import com.alitalia.aem.common.messages.home.CarnetStaticDataRequest;


public interface CarnetStaticDataService {

	CarnetPaymentTypeResponse getPaymentList(CarnetStaticDataRequest request);
	CarnetCountryStaticDataResponse getCreditCardCountryList(CarnetStaticDataRequest request);
	CarnetCountryStaticDataResponse getStateList(CarnetStaticDataRequest request);
	
}
