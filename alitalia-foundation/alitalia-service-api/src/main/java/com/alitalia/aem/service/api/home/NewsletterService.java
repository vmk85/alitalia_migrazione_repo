package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.RegisterUserToAgencyNewsletterRequest;
import com.alitalia.aem.common.messages.home.RegisterUserToAgencyNewsletterResponse;

public interface NewsletterService {
	
	RegisterUserToAgencyNewsletterResponse registerUserToAgencyNewsletter(RegisterUserToAgencyNewsletterRequest request);
}