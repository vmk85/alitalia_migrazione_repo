package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.CarnetSendNotificationEmailRequest;
import com.alitalia.aem.common.messages.home.CarnetSendNotificationEmailResponse;
import com.alitalia.aem.common.messages.home.CrossSellingsRequest;
import com.alitalia.aem.common.messages.home.CrossSellingsResponse;
import com.alitalia.aem.common.messages.home.SapInvoiceRequest;
import com.alitalia.aem.common.messages.home.SapInvoiceResponse;

public interface BookingTicketService {

	CrossSellingsResponse getCrossSellings(CrossSellingsRequest request);
	SapInvoiceResponse callSAPService(SapInvoiceRequest request);
	CarnetSendNotificationEmailResponse sendCarnetNotificationEmail(CarnetSendNotificationEmailRequest request);
}