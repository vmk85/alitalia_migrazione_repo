package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.WebCheckinEmdRequest;
import com.alitalia.aem.common.messages.home.WebCheckinEmdResponse;
import com.alitalia.aem.common.messages.home.WebCheckinReasonForIssuanceRequest;
import com.alitalia.aem.common.messages.home.WebCheckinReasonForIssuanceResponse;


public interface WebCheckinEmdService {

	WebCheckinEmdResponse extraBaggageEmd(WebCheckinEmdRequest request);
	WebCheckinEmdResponse comfortSeatEmd(WebCheckinEmdRequest request);
	WebCheckinEmdResponse cabinUpgradeEmd(WebCheckinEmdRequest request);
	WebCheckinReasonForIssuanceResponse getReasonForIssuance(WebCheckinReasonForIssuanceRequest request);

}
