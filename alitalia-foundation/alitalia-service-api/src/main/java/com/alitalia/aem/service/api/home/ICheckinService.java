package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.checkinrest.*;

public interface ICheckinService {
	CheckinTripSearchResponse retrieveCheckinTrip(CheckinTripSearchRequest request);
	CheckinPnrSearchResponse retrieveCheckinPnr(CheckinPnrSearchRequest request);
	CheckinGetPassengerDataResponse retrievePassengerData(CheckinGetPassengerDataRequest request);
	CheckinTicketSearchResponse retrieveCheckinTicket(CheckinTicketSearchRequest request);
	CheckinAddPassengerResponse retrieveCheckinAddPassenger(CheckinAddPassengerRequest request);
	CheckinFrequentFlyerSearchResponse retrieveCheckinFrequentFlyer(CheckinFrequentFlyerSearchRequest request);
	CheckinSelectedPassengerResponse checkinSelectedPassenger(CheckinSelectedPassengerRequest request);
	CheckinFlightDetailResponse retrieveFlightDetail(CheckinFlightDetailRequest request);
	CheckinPassengerResponse retrieveCheckinPassenger(CheckinPassengerRequest request);
	CheckinReprintBPResponse retrieveCheckinNewBP(CheckinReprintBPRequest request);
	CheckinIsBoardingPassPrintableResponse retrieveBoardingPassPrintable(CheckinIsBoardingPassPrintableRequest request);
	CheckinCreateBoardingPassResponse retrieveBoardingPass(CheckinCreateBoardingPassRequest request);
	CheckinGetSelectedAncillaryOffersResponse retrieveSelectedAncillaryOffers(CheckinGetSelectedAncillaryOffersRequest request);
	CheckinClearAncillariesCartResponse clearAncillariesCart(CheckinClearAncillariesCartRequest request);
	CheckinSetFastTrackResponse setFastTrack(CheckinSetFastTrackRequest request);
	CheckinSetLoungeResponse setLounge(CheckinSetLoungeRequest request);
	CheckinGetBoardingPassInfoResponse getBoardingPassInfo(CheckinGetBoardingPassInfoRequest request);
	CheckinGetBoardingPassInfoByNameResponse getBoardingPassInfoByName(CheckinGetBoardingPassInfoByNameRequest request);

	//Last 03/01/2018 INIZIO
	CheckinSetBaggageResponse setBaggage(CheckinSetBaggageRequest request);
	CheckinEnhancedSeatMapResponse enhancedSeatMap (CheckinEnhancedSeatMapRequest request);
	CheckinChangeSeatsResponse changeSeats (CheckinChangeSeatsRequest request);
	CheckinUpdateSecurityInfoResponse updateSecurityInfo(CheckinUpdateSecurityInfoRequest request);
	CheckinUpdatePassengerDetailsResponse updatePassengerDetails (CheckinUpdatePassengerDetailsRequest request);
	CheckinUpdatePassengerResponse updatePassenger (CheckinUpdatePassengerRequest request);
	CheckinClearAncillarySessionEndResponse clearAcillayEndSession (CheckinClearAncillarySessionEndRequest request);
	CheckinOffloadResponse offload(CheckinOffloadRequest request);
	CheckinOffloadFlightsResponse offloadFlights(CheckinOffloadFlightsRequest request);
	//FINE


	CheckinSetSmsDataResponse setSmsData(CheckinSetSmsDataRequest request);
	CheckinGetSmsDataResponse getSmsData(CheckinGetSmsDataRequest request);
}
