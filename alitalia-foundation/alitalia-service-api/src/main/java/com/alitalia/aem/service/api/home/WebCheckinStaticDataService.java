package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.BaseRequest;
import com.alitalia.aem.common.messages.home.GetCountryRequest;
import com.alitalia.aem.common.messages.home.GetCountryResponse;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixResponse;
import com.alitalia.aem.common.messages.home.WebCheckinAirportNameRequest;
import com.alitalia.aem.common.messages.home.WebCheckinAirportNameResponse;
import com.alitalia.aem.common.messages.home.WebCheckinBPPermissionRequest;
import com.alitalia.aem.common.messages.home.WebCheckinBPPermissionResponse;
import com.alitalia.aem.common.messages.home.WebCheckinFrequentFlyerResponse;
import com.alitalia.aem.common.messages.home.WebCheckinStatesRequest;
import com.alitalia.aem.common.messages.home.WebCheckinStatesResponse;

public interface WebCheckinStaticDataService {

	GetCountryResponse getCountries(GetCountryRequest request);
	RetrievePhonePrefixResponse getPrefixes(GetCountryRequest request);
	WebCheckinBPPermissionResponse getBoardingPassPermissions(WebCheckinBPPermissionRequest request);
	WebCheckinFrequentFlyerResponse getFrequentFlyers(GetCountryRequest request);
	WebCheckinStatesResponse getAmericanStates(BaseRequest request);
	WebCheckinStatesResponse getStates(WebCheckinStatesRequest request);
	WebCheckinAirportNameResponse getAirportNames(WebCheckinAirportNameRequest request);
}