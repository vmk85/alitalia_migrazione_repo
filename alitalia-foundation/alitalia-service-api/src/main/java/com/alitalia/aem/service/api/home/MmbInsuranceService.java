package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.InsuranceResponse;
import com.alitalia.aem.common.messages.home.MmbInsuranceInfoRequest;
import com.alitalia.aem.common.messages.home.MmbPayInsuranceRequest;

public interface MmbInsuranceService {

	InsuranceResponse getInsurance(MmbInsuranceInfoRequest request);
	InsuranceResponse payInsurance(MmbPayInsuranceRequest request);

}