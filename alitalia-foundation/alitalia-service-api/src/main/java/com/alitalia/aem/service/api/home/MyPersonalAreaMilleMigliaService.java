package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.*;

public interface MyPersonalAreaMilleMigliaService {

	MilleMigliaLoginResponse login(MilleMigliaLoginRequest request);
	MilleMigliaLoginSAResponse loginSA(MilleMigliaLoginSARequest request);
	UserFromSocialResponse getMMCode(UserFromSocialRequest request);
	SetSocialAccountResponse setSocialAccount(SetSocialAccountRequest request);
	NotifySocialLoginResponse notifyLogin(NotifySocialLoginRequest request);
	ProfileResponse getProfile(ProfileRequest request);
	AddressesResponse getAddresses(AddressesRequest request);
	MessagesResponse getMessages(MessagesRequest request);
	BalanceResponse getBalance(BalanceRequest request);
	UpdateUserProfileResponse updateProfile(UpdateUserProfileRequest request);
	StatementByDateResponse getStatementByDate(StatementByDateRequest request);
	AddFlightActivityResponse addFlightActivity(AddFlightActivityRequest request);
	SocialAccountsResponse getSocialAccounts(SocialAccountsRequest request);
	UpdatePinResponse updatePin(UpdatePinRequest request);
	SubscribeResponse subscribeByCustomer(SubscribeRequest request);
	UpdateNicknameResponse updateNickname(UpdateNicknameRequest request);
	RetrievePinResponse retrievePinByCustomerNumber(RetrievePinRequest request);
	RetrievePinResponse retrievePinByNickname(RetrievePinRequest request);
	MillemigliaSendEmailResponse sendNewRegistrationEmail(MillemigliaSendEmailRequest request);
	MillemigliaSendEmailResponse sendNewMailingListRegistrationMail(SendNewMailingListRegistrationMailRequest request);
	MailinglistRegistrationResponse registerToMailinglist(MailinglistRegistrationRequest request);
	CreditCardsListResponse getCreditCardsList(CreditCardsListRequest request);
	DeletePanResponse deletePan(DeletePanRequest request);
	RemoveSocialAccountsResponse removeUidGigya(RemoveSocialAccountsRequest request);
	MMPaybackAccrualResponse paybackAccrual(MMPaybackAccrualRequest request);

	SubscribeSAResponse subscribeSA(SubscribeSARequest request);
	SubscribeSACheckEmailUserNameDuplicateResponse CheckEmailUserNameDuplicate (SubscribeSACheckEmailUserNameDuplicateRequest request);
	SubscribeSACheckUserNameDuplicateResponse CheckUserNameDuplicate (SubscribeSACheckUserNameDuplicateRequest request);
	SubscribeSASendMailResponse SubscribeSASendMail(SubscribeSASendMailRequest request);

	SetDatiSicurezzaProfiloResponse SetDatiSicurezzaProfilo(SetDatiSicurezzaProfiloRequest request);

	GetDatiSicurezzaProfiloResponseLocal GetDatiSicurezzaProfilo (GetDatiSicurezzaProfiloRequestLocal request);

	FindProfileResponseLocal FindProfile (FindProfileRequestLocal request);

//	FindProfileResponse FindProfile (FindProfileRequest request);
//	GetDatiSicurezzaProfiloResponse GetDatiSicurezzaProfilo (GetDatiSicurezzaProfiloRequest request);
	GetDatiSicurezzaProfiloOLDResponse GetDatiSicurezzaProfiloOLD (GetDatiSicurezzaProfiloOLDRequest request);
//	SetDatiProfiloResponse SetDatiProfilo (SetDatiProfiloRequest request);
//	SetDatiSicurezzaProfiloResponse SetDatiSicurezzaProfilo (SetDatiSicurezzaProfiloRequest request);
//	SetDatiSicurezzaProfiloOLDResponse SetDatiSicurezzaProfiloOLD (SetDatiSicurezzaProfiloOLDRequest request);
//	UpdateSmsAuthorizationResponse UpdateSmsAuthorization (UpdateSmsAuthorizationRequest request);
}