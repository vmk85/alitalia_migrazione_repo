package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.AwardValueRequest;
import com.alitalia.aem.common.messages.home.AwardValueResponse;
import com.alitalia.aem.common.messages.home.SearchBookingSolutionResponse;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionResponse;

public interface BookingAwardSearchService {

	SearchFlightSolutionResponse executeAwardSearch(SearchFlightSolutionRequest request);
	SearchFlightSolutionResponse executeAwardCalendarSearch(SearchFlightSolutionRequest request);
	SearchFlightSolutionResponse executeAwardFlightSearch(SearchFlightSolutionRequest request);
	SearchBookingSolutionResponse executeAwardSellupSearch(SearchFlightSolutionRequest request);
	SearchFlightSolutionResponse executeAwardTaxSearch(SearchFlightSolutionRequest request);
	AwardValueResponse retrieveAwardValue(AwardValueRequest request);

}
