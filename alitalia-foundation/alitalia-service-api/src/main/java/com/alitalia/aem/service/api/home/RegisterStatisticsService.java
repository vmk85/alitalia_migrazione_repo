package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.RegisterStatisticsRequest;
import com.alitalia.aem.common.messages.home.RegisterStatisticsResponse;

public interface RegisterStatisticsService {

	RegisterStatisticsResponse registerStatistics(RegisterStatisticsRequest request);
}