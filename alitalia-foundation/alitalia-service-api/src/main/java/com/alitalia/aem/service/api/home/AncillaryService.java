package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.InsuranceRequest;
import com.alitalia.aem.common.messages.home.InsuranceResponse;
import com.alitalia.aem.common.messages.home.PayInsuranceRequest;
import com.alitalia.aem.common.messages.home.PayInsuranceResponse;

public interface AncillaryService {

	InsuranceResponse getInsurance(InsuranceRequest request);
	PayInsuranceResponse payInsurance(PayInsuranceRequest request);
}