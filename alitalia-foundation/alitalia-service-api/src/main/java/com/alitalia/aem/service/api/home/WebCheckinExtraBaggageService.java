package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.ExtraBaggageOrderCheckinRequest;
import com.alitalia.aem.common.messages.home.ExtraBaggageOrderCheckinResponse;
import com.alitalia.aem.common.messages.home.ExtraBaggagePolicyCheckinRequest;
import com.alitalia.aem.common.messages.home.ExtraBaggagePolicyCheckinResponse;


public interface WebCheckinExtraBaggageService {

	ExtraBaggageOrderCheckinResponse getExtraBaggageOrder(ExtraBaggageOrderCheckinRequest request);
	ExtraBaggagePolicyCheckinResponse getBaggagePolicy(ExtraBaggagePolicyCheckinRequest request);

}
