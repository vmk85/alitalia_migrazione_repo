package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.RegisterCreditCardRequest;
import com.alitalia.aem.common.messages.home.RegisterCreditCardResponse;

public interface IMyAlitaliaManageCreditCardService {
	RegisterCreditCardResponse storeCreditCardData(RegisterCreditCardRequest request);
}
