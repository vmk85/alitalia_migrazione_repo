package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.crmdatarest.*;

public interface IServiceCRMData {
	
	SetCRMDataNewsLetterResponse setCRMDataNewsLetter(SetCRMDataNewsLetterRequest request);

	SetCrmDataInfoResponse setCRMDataInfo(SetCrmDataInfoRequest request);

	GetCrmDataInfoResponse getCRMDataInfo(GetCrmDataInfoRequest request);

}
