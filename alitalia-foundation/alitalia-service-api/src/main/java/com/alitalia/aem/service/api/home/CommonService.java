package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.AuthenticateMilleMigliaUserRequest;
import com.alitalia.aem.common.messages.home.AuthenticateMilleMigliaUserResponse;
import com.alitalia.aem.common.messages.home.RetrieveAirportDetailsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportDetailsResponse;
import com.alitalia.aem.common.messages.home.RetrieveFlightMileageRequest;
import com.alitalia.aem.common.messages.home.RetrieveFlightMileageResponse;
import com.alitalia.aem.common.messages.home.RetrieveFlightSeatMapRequest;
import com.alitalia.aem.common.messages.home.RetrieveFlightSeatMapResponse;

public interface CommonService {

	RetrieveAirportDetailsResponse retrieveAirportDetails(RetrieveAirportDetailsRequest request);
	RetrieveFlightMileageResponse retrieveFlightMileage(RetrieveFlightMileageRequest request);
	RetrieveFlightSeatMapResponse retrieveFlightSeatMap(RetrieveFlightSeatMapRequest request);
	AuthenticateMilleMigliaUserResponse authenticateMilleMigliaUser(AuthenticateMilleMigliaUserRequest request);
}
