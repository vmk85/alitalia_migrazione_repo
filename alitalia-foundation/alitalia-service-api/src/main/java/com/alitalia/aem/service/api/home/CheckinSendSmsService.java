package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.CheckinSendSmsRequest;
import com.alitalia.aem.common.messages.home.CheckinSendSmsResponse;

public interface CheckinSendSmsService {
    CheckinSendSmsResponse sendSms(CheckinSendSmsRequest request);
}
