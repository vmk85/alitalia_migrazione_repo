package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.SearchBookingSolutionResponse;
import com.alitalia.aem.common.messages.home.SearchExtraChargeRequest;
import com.alitalia.aem.common.messages.home.SearchExtraChargeResponse;
import com.alitalia.aem.common.messages.home.SearchFlexibleDatesMatrixResponse;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionResponse;

public interface BookingSearchService {

	SearchFlightSolutionResponse executeBrandSearch(SearchFlightSolutionRequest request);
	SearchFlightSolutionResponse executeRibbonSearch(SearchFlightSolutionRequest request);
	SearchFlightSolutionResponse executeRefreshSearch(SearchFlightSolutionRequest request);
	SearchFlightSolutionResponse executeYouthSearch(SearchFlightSolutionRequest request);
	SearchBookingSolutionResponse executeBookingSellupSearch(SearchFlightSolutionRequest request);
	SearchFlightSolutionResponse executeBookingTaxSearchSearch(SearchFlightSolutionRequest request);
	SearchExtraChargeResponse executeExtraChargeSearch(SearchExtraChargeRequest request);
	SearchFlightSolutionResponse executeMultiSliceSearch(SearchFlightSolutionRequest request);
	SearchFlightSolutionResponse executeNextSliceSearch(SearchFlightSolutionRequest request);
	SearchFlexibleDatesMatrixResponse executeFlexibleDatesSearch(SearchFlightSolutionRequest request);
	SearchFlightSolutionResponse executeMetaSearch(SearchFlightSolutionRequest request);
	SearchFlightSolutionResponse executeFullTextRulesSearch(SearchFlightSolutionRequest request);
	//SearchYouthSolutionResponse executeJumpUpSearch(SearchFlightSolutionRequest request);
}
