package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.AdyenDisableStoredPaymentRequest;
import com.alitalia.aem.common.messages.home.AdyenDisableStoredPaymentResponse;
import com.alitalia.aem.common.messages.home.AdyenRetrievalStoredPaymentRequest;
import com.alitalia.aem.common.messages.home.AdyenRetrievalStoredPaymentResponse;
import com.alitalia.aem.common.messages.home.AdyenStoringPaymentRequest;
import com.alitalia.aem.common.messages.home.AdyenStoringPaymentResponse;
import com.alitalia.aem.common.messages.home.AdyenUpdateStoredPaymentRequest;
import com.alitalia.aem.common.messages.home.AdyenUpdateStoredPaymentResponse;


public interface IAdyenRecurringPaymentService {

	AdyenStoringPaymentResponse storingPayment(AdyenStoringPaymentRequest request);
	AdyenRetrievalStoredPaymentResponse retrievalStoredPayment(AdyenRetrievalStoredPaymentRequest request);
	AdyenUpdateStoredPaymentResponse updateStoredPayment(AdyenUpdateStoredPaymentRequest request);
	AdyenDisableStoredPaymentResponse disableStoredPayment(AdyenDisableStoredPaymentRequest request);

}
