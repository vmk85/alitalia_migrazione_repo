package com.alitalia.aem.service.api.beanrest;

import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;

public interface StaticDataServiceRest {
	
	RetrieveAirportsResponse getAllAirports(RetrieveAirportsRequest request);
	RetrieveAirportsResponse getAirportDestination(RetrieveAirportsRequest request);
	//RetrieveAvailabilityResponse getAvailabilty(RetrieveAvailabilityRequest request);
	
}
