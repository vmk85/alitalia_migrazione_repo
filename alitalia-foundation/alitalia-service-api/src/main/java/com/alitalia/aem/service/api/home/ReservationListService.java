package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.GetSabrePNRRequest;
import com.alitalia.aem.common.messages.home.GetSabrePNRResponse;

public interface ReservationListService {

	GetSabrePNRResponse getSabrePNR(GetSabrePNRRequest request);

}
