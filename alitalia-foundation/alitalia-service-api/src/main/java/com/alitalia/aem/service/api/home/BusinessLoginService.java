package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.*;

public interface BusinessLoginService {

	AgencyLoginResponse agencyLogin(AgencyLoginRequest request);
	AgencyPasswordChangeResponse changePassword(AgencyPasswordChangeRequest request);
	AgencyRetrievePasswordResponse retrievePasswordByID(AgencyRetrievePasswordByIDRequest request);
	AgencyRetrievePasswordResponse retrievePasswordByVAT(AgencyRetrievePasswordByVATRequest request);
	AgencyRetrieveDataResponse retrieveAgencyData(AgencyRetrieveDataRequest request);
	AgencyUpdateDataResponse updateAgencyData(AgencyUpdateDataRequest request);
	AgencySubscriptionResponse subscribeAgency(AgencySubscriptionRequest request);
	AgencySendMailResponse sendMail(AgencySendMailRequest request);
	AgencySendMailResponse sendMailWithAttachment(AgencySendMailRequest request);
	AgencyInsertConsentResponse insertConsent(AgencyInsertConsentRequest request);
	AgencyUnsubscribeNewsLetterIataResponse unsubscribeNewsletterForeignAgency(AgencyUnsubscribeNewsLetterIataRequest request);
}
