package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.LocateRequest;
import com.alitalia.aem.common.messages.home.LocateResponse;

public interface EgonService {

	LocateResponse locate(LocateRequest request);
}