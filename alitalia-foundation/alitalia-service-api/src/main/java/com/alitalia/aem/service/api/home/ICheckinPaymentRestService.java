package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.checkinrest.*;

public interface ICheckinPaymentRestService {
	
	// Payment 25/01/2018
	CheckinGetCartResponse getCart(CheckinGetCartRequest request);
	CheckinInitPaymentResponse initPayment(CheckinInitPaymentRequest request);
	CheckinEndPaymentResponse endPayment(CheckinEndPaymentRequest request);
	//Tolentino - Inizio
	CheckinInitPaymentResponse initPaymentRecurringAdyen(CheckinInitPaymentRequest request);
	//Tolentino - Fine
	CheckinAuthorize3dResponse authorize3d(CheckinAuthorize3dRequest request);

}
