package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.AgencyRetrieveIataGroupsRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrieveIataGroupsResponse;

public interface WebAgencyService {

	AgencyRetrieveIataGroupsResponse retrieveIataAgencyInfo(AgencyRetrieveIataGroupsRequest request);
	AgencyRetrieveIataGroupsResponse retrieveNonIataAgencyInfo(AgencyRetrieveIataGroupsRequest request);
}