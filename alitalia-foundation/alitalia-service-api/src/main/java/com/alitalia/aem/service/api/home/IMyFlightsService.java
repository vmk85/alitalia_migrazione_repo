package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.checkinrest.*;

public interface IMyFlightsService {
	CheckinPnrSearchResponse retrieveCheckinPnr(CheckinPnrSearchRequest request);

}
