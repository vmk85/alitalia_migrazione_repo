package com.alitalia.aem.service.api.homerest;

import com.alitalia.aem.common.messages.home.CountriesRequest;
import com.alitalia.aem.common.messages.home.CountriesResponse;
import com.alitalia.aem.common.messages.home.CountryStatesRequest;
import com.alitalia.aem.common.messages.home.CountryStatesResponse;
import com.alitalia.aem.common.messages.home.GetDestinationsResponse;
import com.alitalia.aem.common.messages.home.GetFrequentFlyerRequest;
import com.alitalia.aem.common.messages.home.GetFrequentFlyerResponse;
import com.alitalia.aem.common.messages.home.RetrieveAirportDestinationRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.common.messages.home.RetrieveAvailabilityRequest;
import com.alitalia.aem.common.messages.home.RetrieveAvailabilityResponse;

public interface StaticDataServiceRest {
	
	public RetrieveAirportsResponse getAllAirports(RetrieveAirportsRequest request);
	public GetDestinationsResponse getAirportDestination(RetrieveAirportDestinationRequest request);
	public RetrieveAvailabilityResponse getAvailability(RetrieveAvailabilityRequest request);
	public RetrieveAvailabilityResponse getAvailabilityRTN(RetrieveAvailabilityRequest request);
	public GetFrequentFlyerResponse getFrequentFlyers(GetFrequentFlyerRequest request);
	public CountryStatesResponse getCountryStates(CountryStatesRequest request);
	public CountriesResponse getCountries(CountriesRequest request);
}
