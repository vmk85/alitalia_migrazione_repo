package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.ChangeSeatCheckinRequest;
import com.alitalia.aem.common.messages.home.ChangeSeatCheckinResponse;
import com.alitalia.aem.common.messages.home.GetSeatDisponibilityCheckinRequest;
import com.alitalia.aem.common.messages.home.GetSeatDisponibilityCheckinResponse;
import com.alitalia.aem.common.messages.home.GetSeatMapCheckinRequest;
import com.alitalia.aem.common.messages.home.GetSeatMapCheckinResponse;


public interface WebCheckinSeatMapService {

	GetSeatMapCheckinResponse getSeatMap(GetSeatMapCheckinRequest request);
	GetSeatDisponibilityCheckinResponse getSeatDisponibility(GetSeatDisponibilityCheckinRequest request);
	ChangeSeatCheckinResponse changeSeat(ChangeSeatCheckinRequest request);

}
