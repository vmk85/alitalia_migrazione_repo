package com.alitalia.aem.service.api.home;

import com.alitalia.aem.common.messages.home.GetSabrePNRRequest;
import com.alitalia.aem.common.messages.home.GetSabrePNRResponse;
import com.alitalia.aem.common.messages.home.MmbModifyPnrApisInfoRequest;
import com.alitalia.aem.common.messages.home.MmbModifyPnrApisInfoResponse;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationRequest;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationResponse;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrContactsRequest;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrContactsResponse;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrFrequentFlyerRequest;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrFrequentFlyerResponse;

public interface MmbSearchService {

	RetriveMmbPnrInformationResponse retrivePnrInformation(RetriveMmbPnrInformationRequest request);
	UpdateMmbPnrContactsResponse updatePnrContacts(UpdateMmbPnrContactsRequest request);
	UpdateMmbPnrFrequentFlyerResponse updatePnrFrequentFlyer(UpdateMmbPnrFrequentFlyerRequest request);
	MmbModifyPnrApisInfoResponse modifyPnrPassengerApisInfo(MmbModifyPnrApisInfoRequest request);
	GetSabrePNRResponse getSabrePNR(GetSabrePNRRequest request);
	
}
