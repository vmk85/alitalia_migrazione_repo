package com.alitalia.aem.service.impl.converter.home.commonservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveFlightMileageRequest;
import com.alitalia.aem.ws.booking.commonservice.xsd1.FlightMileageRequest;
import com.alitalia.aem.ws.booking.commonservice.xsd1.ObjectFactory;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveFlightMileageRequestToFlightMileageRequest.class)
public class RetrieveFlightMileageRequestToFlightMileageRequest 
		implements Converter<RetrieveFlightMileageRequest, FlightMileageRequest> {
	
	@Reference
	private DirectFlightDataToDirectFlight directFlightConverter;
	
	@Override
	public FlightMileageRequest convert(RetrieveFlightMileageRequest source) {
		
		FlightMileageRequest destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			destination = objectFactory.createFlightMileageRequest();
			
			destination.setFlight(
					objectFactory.createFlightMileageRequestFlight(
						directFlightConverter.convert(
								source.getDirectFlight())));
			
		}
		
		return destination;
	}
	
}