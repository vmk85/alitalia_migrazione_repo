package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.InsuranceResponse;
import com.alitalia.aem.common.messages.home.MmbInsuranceInfoRequest;
import com.alitalia.aem.common.messages.home.MmbPayInsuranceRequest;
import com.alitalia.aem.service.api.home.MmbInsuranceService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.mmbinsuranceservice.InsuranceInfoResponseToInsuranceResponse;
import com.alitalia.aem.service.impl.converter.home.mmbinsuranceservice.InsurancePaymentResponseToInsuranceResponse;
import com.alitalia.aem.service.impl.converter.home.mmbinsuranceservice.MmbInsuranceInfoRequestToInsuranceInfoRequest;
import com.alitalia.aem.service.impl.converter.home.mmbinsuranceservice.MmbPayInsuranceRequestToInsurancePaymentRequest;
import com.alitalia.aem.ws.mmb.insuranceservice.MmbInsuranceServiceClient;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd1.InsuranceInfoRequest;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd1.InsuranceInfoResponse;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd1.InsurancePaymentRequest;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd1.InsurancePaymentResponse;

@Component(immediate=true, metatype=false)
@Service
public class SimpleMmbInsuranceService implements MmbInsuranceService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleMmbInsuranceService.class);

	@Reference
	private MmbInsuranceServiceClient mmbInsuranceServiceClient;

	@Reference
	private MmbInsuranceInfoRequestToInsuranceInfoRequest mmbInsuranceInfoRequestConverter;

	@Reference
	private InsuranceInfoResponseToInsuranceResponse insuranceInfoResponseConverter;

	@Reference
	private MmbPayInsuranceRequestToInsurancePaymentRequest mmbPayInsuranceRequestConverter;

	@Reference
	private InsurancePaymentResponseToInsuranceResponse insurancePaymentResponseConverter;

	@Override
	public InsuranceResponse getInsurance(MmbInsuranceInfoRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method getInsurance(). The request is {}", request);
		}
		

		try {
			InsuranceResponse response = new InsuranceResponse();
			InsuranceInfoRequest serviceRequest = mmbInsuranceInfoRequestConverter.convert(request);
			InsuranceInfoResponse serviceResponse = mmbInsuranceServiceClient.getInsurance(serviceRequest);
			response = insuranceInfoResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully getInsurance(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getInsurance().", e);
			throw new AlitaliaServiceException("Exception executing getInsurance: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public InsuranceResponse payInsurance(MmbPayInsuranceRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method payInsurance(). The request is {}", request);
		}
		

		try {
			InsuranceResponse response = new InsuranceResponse();
			InsurancePaymentRequest serviceRequest = mmbPayInsuranceRequestConverter.convert(request);
			InsurancePaymentResponse serviceResponse = mmbInsuranceServiceClient.payInsurance(serviceRequest);
			response = insurancePaymentResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully getInsurance(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method payInsurance().", e);
			throw new AlitaliaServiceException("Exception executing payInsurance: Sid ["+request.getSid()+"]" , e);
		}
	}
}
