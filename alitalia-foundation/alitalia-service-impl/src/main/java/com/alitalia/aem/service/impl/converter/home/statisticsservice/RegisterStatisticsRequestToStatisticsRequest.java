package com.alitalia.aem.service.impl.converter.home.statisticsservice;

import javax.xml.datatype.DatatypeFactory;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.BrandSearchData;
import com.alitalia.aem.common.data.home.RibbonSearchData;
import com.alitalia.aem.common.data.home.enumerations.CreditCardTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.SearchTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.StatisticsDataTypeEnum;
import com.alitalia.aem.common.messages.SearchExecuteRequestFilter;
import com.alitalia.aem.common.messages.home.RegisterBillingStatisticRequest;
import com.alitalia.aem.common.messages.home.RegisterECouponStatisticRequest;
import com.alitalia.aem.common.messages.home.RegisterFlightSearchStatisticRequest;
import com.alitalia.aem.common.messages.home.RegisterPNRStatisticRequest;
import com.alitalia.aem.common.messages.home.RegisterPaymentAuthAttemptStatisticRequest;
import com.alitalia.aem.common.messages.home.RegisterPaymentBookingEtktStatisticRequest;
import com.alitalia.aem.common.messages.home.RegisterPaymentBookingInsuranceStatisticRequest;
import com.alitalia.aem.common.messages.home.RegisterPaymentFromAuthToTicketStatisticRequest;
import com.alitalia.aem.common.messages.home.RegisterStatisticsRequest;
import com.alitalia.aem.service.impl.home.SimpleCommonService;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd1.AvailStatisticsRequest;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd1.BillingStatisticsRequest;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd1.EcouponStatisticsRequest;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd1.PNRStatisticsRequest;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd1.PaymentAuthorizationAttemptPayPalStatisticsRequest;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd1.PaymentAuthorizationAttemptStatisticsRequest;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd1.PaymentBookingEtktStatisticsRequest;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd1.PaymentBookingInsuranceRequest;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd1.PaymentFromAuthorizationToTicketStatisticsRequest;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd1.StatisticsRequest;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd2.SearchType;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd2.StatisticDataType;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd3.ASearch;

@Component(immediate = true, metatype = false)
@Service(value = RegisterStatisticsRequestToStatisticsRequest.class)
public class RegisterStatisticsRequestToStatisticsRequest implements Converter<RegisterStatisticsRequest, StatisticsRequest> {

	private static final Logger logger = LoggerFactory.getLogger(SimpleCommonService.class);
	private final long DEFAULT_DURATION_MILLIS = 0;
	
	
	@Reference
	private BrandSearchDataToBrandSearchStatistics brandSearchDataConverter;
	
	@Reference
	private RibbonSearchDataToRibbonSearchStatistics ribbonSearchDataConverter;

	@Override
	public StatisticsRequest convert(RegisterStatisticsRequest source) {
		
		logger.info("Executing conversion. The request is {}", source);

		ObjectFactory objectFactory = new ObjectFactory();
		StatisticsRequest request = null;

		try {

			if (source instanceof RegisterFlightSearchStatisticRequest) {
				SearchExecuteRequestFilter searchData = ((RegisterFlightSearchStatisticRequest) source).getSearch();
				SearchTypeEnum searchType = ((RegisterFlightSearchStatisticRequest) source).getSearchType();
				request = new AvailStatisticsRequest();
				ASearch search = null;
				if (searchData instanceof BrandSearchData){
					search = brandSearchDataConverter.convert((BrandSearchData) searchData);
				} else if (searchData instanceof RibbonSearchData){
					search = ribbonSearchDataConverter.convert((RibbonSearchData) searchData);
				}
				((AvailStatisticsRequest) request).setSearch(objectFactory.createAvailStatisticsRequestSearch(search));
				((AvailStatisticsRequest) request).setSearchType(SearchType.fromValue(searchType.value()));
			} else if (source instanceof RegisterECouponStatisticRequest) {
				String discountName = ((RegisterECouponStatisticRequest) source).getDiscountName();
				request = new EcouponStatisticsRequest();
				((EcouponStatisticsRequest) request).setDiscountName(objectFactory.createEcouponStatisticsRequestDiscountName(discountName));
			} else if (source instanceof RegisterPNRStatisticRequest) {
				String pnr = ((RegisterPNRStatisticRequest) source).getPnr();
				String travelInfo = ((RegisterPNRStatisticRequest) source).getTravelInfo();
				Boolean isAward = ((RegisterPNRStatisticRequest) source).isAward();
				request = new PNRStatisticsRequest();
				((PNRStatisticsRequest) request).setPNR(objectFactory.createPNRStatisticsRequestPNR(pnr));
				((PNRStatisticsRequest) request).setTravelInfo(objectFactory.createPNRStatisticsRequestTravelInfo(travelInfo));
				((PNRStatisticsRequest) request).setIsAward(isAward);
			} else if (source instanceof RegisterPaymentAuthAttemptStatisticRequest) {
				CreditCardTypeEnum ccName = ((RegisterPaymentAuthAttemptStatisticRequest) source).getCcName();
				String email = ((RegisterPaymentAuthAttemptStatisticRequest) source).getEmail();
				PaymentTypeEnum paymentType = ((RegisterPaymentAuthAttemptStatisticRequest) source).getPaymentType();
				String threeDSCheck = ((RegisterPaymentAuthAttemptStatisticRequest) source).getThreeDSCheck();
				if (!StatisticsDataTypeEnum.PAYMENT_AUTHORIZATION_PAY_PAL.equals(source.getType())) {
					request = objectFactory.createPaymentAuthorizationAttemptStatisticsRequest();
					((PaymentAuthorizationAttemptStatisticsRequest) request).setCCName(objectFactory.createPaymentAuthorizationAttemptStatisticsRequestCCName(ccName.value()));
					((PaymentAuthorizationAttemptStatisticsRequest) request).setEmail(objectFactory.createPaymentAuthorizationAttemptStatisticsRequestEmail(email));
					((PaymentAuthorizationAttemptStatisticsRequest) request).setPaymentType(objectFactory.createPaymentAuthorizationAttemptStatisticsRequestPaymentType(paymentType.value()));
					((PaymentAuthorizationAttemptStatisticsRequest) request).setThreeDSCheck(objectFactory.createPaymentAuthorizationAttemptStatisticsRequestThreeDSCheck(threeDSCheck));
				}
				else {
					request = objectFactory.createPaymentAuthorizationAttemptPayPalStatisticsRequest();
					((PaymentAuthorizationAttemptPayPalStatisticsRequest) request).setEmail(objectFactory.createPaymentAuthorizationAttemptStatisticsRequestEmail(email));
				}
			} else if (source instanceof RegisterPaymentFromAuthToTicketStatisticRequest) {
				String backOfficeId = ((RegisterPaymentFromAuthToTicketStatisticRequest) source).getBackofficeID();
				Long backOfficeTimeMillis = ((RegisterPaymentFromAuthToTicketStatisticRequest) source).getBackofficeTimeMillis();
				String cartId = ((RegisterPaymentFromAuthToTicketStatisticRequest) source).getCartID();
				request = new PaymentFromAuthorizationToTicketStatisticsRequest();
				((PaymentFromAuthorizationToTicketStatisticsRequest) request).setBackofficeID(objectFactory.createPaymentFromAuthorizationToTicketStatisticsRequestBackofficeID(backOfficeId));
				if (backOfficeTimeMillis==null)
					logger.error("RegisterStatisticsRequestToStatisticsRequest conversion error: duration null");
				((PaymentFromAuthorizationToTicketStatisticsRequest) request).setBackofficeTime(DatatypeFactory.newInstance().newDurationDayTime((backOfficeTimeMillis!=null ? backOfficeTimeMillis : DEFAULT_DURATION_MILLIS)));
				((PaymentFromAuthorizationToTicketStatisticsRequest) request).setCartID(objectFactory.createPaymentFromAuthorizationToTicketStatisticsRequestCartID(cartId));
			} else if (source instanceof RegisterPaymentBookingEtktStatisticRequest) {
				String cartID = ((RegisterPaymentBookingEtktStatisticRequest) source).getCartID();
				String companyCode = ((RegisterPaymentBookingEtktStatisticRequest) source).getCompanyCode();
				String discountName = ((RegisterPaymentBookingEtktStatisticRequest) source).getDiscountName();
				String eticket = ((RegisterPaymentBookingEtktStatisticRequest) source).getEticket();
				String importo = ((RegisterPaymentBookingEtktStatisticRequest) source).getImporto();
				String pnr = ((RegisterPaymentBookingEtktStatisticRequest) source).getPnr();
				request = new PaymentBookingEtktStatisticsRequest();
				((PaymentBookingEtktStatisticsRequest) request).setCartID(objectFactory.createPaymentBookingEtktStatisticsRequestCartID(cartID));
				((PaymentBookingEtktStatisticsRequest) request).setCompanyCode(objectFactory.createPaymentBookingEtktStatisticsRequestCompanyCode(companyCode));
				((PaymentBookingEtktStatisticsRequest) request).setDiscountName(objectFactory.createPaymentBookingEtktStatisticsRequestDiscountName(discountName));
				((PaymentBookingEtktStatisticsRequest) request).setEticket(objectFactory.createPaymentBookingEtktStatisticsRequestEticket(eticket));
				((PaymentBookingEtktStatisticsRequest) request).setImporto(objectFactory.createPaymentBookingEtktStatisticsRequestImporto(importo));
				((PaymentBookingEtktStatisticsRequest) request).setPNR(objectFactory.createPaymentBookingEtktStatisticsRequestPNR(pnr));
			} else if (source instanceof RegisterBillingStatisticRequest) {
				String otherParameters = ((RegisterBillingStatisticRequest) source).getOtherParameters();
				String parameter1 = ((RegisterBillingStatisticRequest) source).getParameter1();
				String parameter2 = ((RegisterBillingStatisticRequest) source).getParameter2();
				String parameter3 = ((RegisterBillingStatisticRequest) source).getParameter3();
				String parameter4 = ((RegisterBillingStatisticRequest) source).getParameter4();
				String parameter5 = ((RegisterBillingStatisticRequest) source).getParameter5();
				request = new BillingStatisticsRequest();
				((BillingStatisticsRequest) request).setOtherParameters(objectFactory.createBillingStatisticsRequestOtherParameters(otherParameters));
				((BillingStatisticsRequest) request).setParameter1(objectFactory.createBillingStatisticsRequestParameter1(parameter1));
				((BillingStatisticsRequest) request).setParameter2(objectFactory.createBillingStatisticsRequestParameter2(parameter2));
				((BillingStatisticsRequest) request).setParameter3(objectFactory.createBillingStatisticsRequestParameter3(parameter3));
				((BillingStatisticsRequest) request).setParameter4(objectFactory.createBillingStatisticsRequestParameter4(parameter4));
				((BillingStatisticsRequest) request).setParameter5(objectFactory.createBillingStatisticsRequestParameter5(parameter5));
			} else if (source instanceof RegisterPaymentBookingInsuranceStatisticRequest) {
				String cardType = ((RegisterPaymentBookingInsuranceStatisticRequest) source).getCardType();
				String policyNumber = ((RegisterPaymentBookingInsuranceStatisticRequest) source).getPolicyNumber();
				String typeOfTrip = ((RegisterPaymentBookingInsuranceStatisticRequest) source).getTypeOfTrip();
				request = new PaymentBookingInsuranceRequest();
				((PaymentBookingInsuranceRequest) request).setCardType(objectFactory.createPaymentBookingInsuranceRequestCardType(cardType));
				((PaymentBookingInsuranceRequest) request).setPolicyNumber(objectFactory.createPaymentBookingInsuranceRequestPolicyNumber(policyNumber));
				((PaymentBookingInsuranceRequest) request).setTypeOfTrip(objectFactory.createPaymentBookingInsuranceRequestTypeOfTrip(typeOfTrip));
			} else if (!(source instanceof RegisterPaymentAuthAttemptStatisticRequest) &&
					StatisticsDataTypeEnum.PAYMENT_AUTHORIZATION_PAY_PAL.equals(source.getType())) {
				request = objectFactory.createPaymentAuthorizationPayPalStatisticsRequest();
			} else {
				request = new StatisticsRequest();
			}

			String clientIP = source.getClientIP();
			String errorDescription = source.getErrorDescr();
			String sessionId = source.getSessionId();
			String siteCode = source.getSiteCode();
			boolean success = source.isSuccess();
			StatisticsDataTypeEnum statisticsDataTypeEnum = source.getType();

			request.setClientIP(objectFactory.createStatisticsRequestClientIP(clientIP));
			request.setErrorDescr(objectFactory.createStatisticsRequestErrorDescr(errorDescription));
			request.setSessionID(objectFactory.createStatisticsRequestSessionID(sessionId));
			request.setSiteCode(objectFactory.createStatisticsRequestSiteCode(siteCode));
			request.setSuccess(success);
			request.setType(StatisticDataType.fromValue(statisticsDataTypeEnum.value()));

		} catch(Exception e){
			logger.error("Exception while executing conversion.", e);
		}

		return request;
	}
}