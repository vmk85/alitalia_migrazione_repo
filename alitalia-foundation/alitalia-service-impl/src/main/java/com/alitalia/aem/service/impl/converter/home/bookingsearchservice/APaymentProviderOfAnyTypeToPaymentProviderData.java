package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PaymentProviderBancaIntesaData;
import com.alitalia.aem.common.data.home.PaymentProviderBancoPostaData;
import com.alitalia.aem.common.data.home.PaymentProviderCreditCardData;
import com.alitalia.aem.common.data.home.PaymentProviderData;
import com.alitalia.aem.common.data.home.PaymentProviderFindomesticData;
import com.alitalia.aem.common.data.home.PaymentProviderGlobalCollectData;
import com.alitalia.aem.common.data.home.PaymentProviderInstalmentsBrazilData;
import com.alitalia.aem.common.data.home.PaymentProviderLottomaticaData;
import com.alitalia.aem.common.data.home.PaymentProviderMasterPassData;
import com.alitalia.aem.common.data.home.PaymentProviderPayAtTOData;
import com.alitalia.aem.common.data.home.PaymentProviderPayLaterData;
import com.alitalia.aem.common.data.home.PaymentProviderPayPalData;
import com.alitalia.aem.common.data.home.PaymentProviderPosteIDData;
import com.alitalia.aem.common.data.home.PaymentProviderZeroPaymentData;
import com.alitalia.aem.common.data.home.enumerations.CreditCardTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.GlobalCollectPaymentTypeEnum;
import com.alitalia.aem.ws.booking.searchservice.xsd3.AComunicationBase;
import com.alitalia.aem.ws.booking.searchservice.xsd3.APaymentProviderOfanyType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.AUserInfoBase;
import com.alitalia.aem.ws.booking.searchservice.xsd3.BancaIntesaOfanyType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.BancoPostaOfanyType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.Contact;
import com.alitalia.aem.ws.booking.searchservice.xsd3.CreditCardOfanyTypeanyType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.FindomesticOfanyTypeanyType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.GlobalCollectOfanyTypeanyType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.InstalmentsBrazilOfanyTypeanyType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.LottomaticaOfanyType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.MasterPassOfanyType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.PayAtTOOfanyTypeanyType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.PayLaterOfanyTypeanyType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.PayPalOfanyType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.PosteIDOfanyType;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ZeroPaymentProviderOfanyTypeanyType;

@Component(immediate=true, metatype=false)
@Service(value=APaymentProviderOfAnyTypeToPaymentProviderData.class)
public class APaymentProviderOfAnyTypeToPaymentProviderData 
		implements Converter<APaymentProviderOfanyType, PaymentProviderData> {
	
	@Reference
	private AComunicationBaseToPaymentComunicationBaseData comunicationOfAnyTypeToPaymentComunicationConverter;
	
	@Reference
	private AUserInfoToUserInfoBaseData aUserInfoToUserInfoBaseDataConverter;
	
	@Reference
	private ContactToContactData contactToContactDataConverter;
	
	@Override
	public PaymentProviderData convert(APaymentProviderOfanyType source) {
		
		PaymentProviderData destination = null;
		
		if (source != null) {
			
			if (source instanceof BancaIntesaOfanyType) {
				destination = new PaymentProviderBancaIntesaData();
				
			} else if (source instanceof BancoPostaOfanyType) {
				destination = new PaymentProviderBancoPostaData();
				
			} else if (source instanceof ZeroPaymentProviderOfanyTypeanyType) {
				destination = new PaymentProviderZeroPaymentData();
				
			} else if (source instanceof PosteIDOfanyType) {
				destination = new PaymentProviderPosteIDData();
				
			} else if (source instanceof CreditCardOfanyTypeanyType) {
				CreditCardOfanyTypeanyType typedSource = (CreditCardOfanyTypeanyType) source;
				PaymentProviderCreditCardData typedDestination = new PaymentProviderCreditCardData();
				destination = typedDestination;
				
				if (typedSource.getCreditCardNumber() != null) {
					typedDestination.setCreditCardNumber(
							typedSource.getCreditCardNumber().getValue());
				}
				
				if (typedSource.getCVV() != null) {
					typedDestination.setCreditCardNumber(
							typedSource.getCVV().getValue());
				}
				
				typedDestination.setExpiryMonth(
						typedSource.getExpiryMonth());
				
				typedDestination.setExpiryYear(
						typedSource.getExpiryYear());
				
				if (typedSource.getToken() != null) {
					typedDestination.setToken(
							typedSource.getToken().getValue());
				}
				
				if (typedSource.getType() != null) {
					typedDestination.setType(
							CreditCardTypeEnum.fromValue(typedSource.getType().value()));
				}
				
				if (typedSource.getUserInfo() != null) {
					typedDestination.setUserInfo(
							aUserInfoToUserInfoBaseDataConverter.convert(
									(AUserInfoBase) typedSource.getUserInfo().getValue()));
				}
				
			} else if (source instanceof FindomesticOfanyTypeanyType) {
				FindomesticOfanyTypeanyType typedSource = (FindomesticOfanyTypeanyType) source;
				PaymentProviderFindomesticData typedDestination = new PaymentProviderFindomesticData();
				destination = typedDestination;	
				
				if (typedSource.getUserInfo() != null) {
					typedDestination.setUserInfo(
							aUserInfoToUserInfoBaseDataConverter.convert(
									(AUserInfoBase) typedSource.getUserInfo().getValue()));
				}
				
				
			} else if (source instanceof GlobalCollectOfanyTypeanyType) {
				GlobalCollectOfanyTypeanyType typedSource = (GlobalCollectOfanyTypeanyType) source;
				PaymentProviderGlobalCollectData typedDestination = new PaymentProviderGlobalCollectData();
				destination = typedDestination;	
				
				if (typedSource.getUserInfo() != null) {
					typedDestination.setUserInfo(
							aUserInfoToUserInfoBaseDataConverter.convert(
									(AUserInfoBase) typedSource.getUserInfo().getValue()));
				}
				
				if (typedSource.getType() != null) {
					typedDestination.setType(
							GlobalCollectPaymentTypeEnum.fromValue(typedSource.getType().value()));
				}
				
				typedDestination.setInstallementsNumber(
						typedSource.getInstallementsNumber());
				
			} else if (source instanceof InstalmentsBrazilOfanyTypeanyType) {
				InstalmentsBrazilOfanyTypeanyType typedSource = (InstalmentsBrazilOfanyTypeanyType) source;
				PaymentProviderInstalmentsBrazilData typedDestination = new PaymentProviderInstalmentsBrazilData();
				destination = typedDestination;	
				
				List<String> emailForMail = new ArrayList<String>();
				if (typedSource.getEmailForMail() != null && typedSource.getEmailForMail().getValue() != null) {
					for (String email : typedSource.getEmailForMail().getValue().getString()) {
						emailForMail.add(email);
					}
				}
				typedDestination.setEmailForMail(
						emailForMail);
				
				if (typedSource.getEmailForPnr() != null) {
					typedDestination.setEmailForPnr(
							typedSource.getEmailForPnr().getValue());
				}
				
				if (typedSource.getOtherContact() != null) {
					typedDestination.setOtherContact(
							contactToContactDataConverter.convert(
									(Contact) typedSource.getOtherContact().getValue()));
				}
				
				if (typedSource.getType() != null) {
					typedDestination.setType(
							CreditCardTypeEnum.fromValue(typedSource.getType().value()));
				}
				
			} else if (source instanceof LottomaticaOfanyType) {
				PaymentProviderLottomaticaData typedDestination = new PaymentProviderLottomaticaData();
				destination = typedDestination;	
				
			} else if (source instanceof MasterPassOfanyType) {
				PaymentProviderMasterPassData typedDestination = new PaymentProviderMasterPassData();
				destination = typedDestination;	
				
			} else if (source instanceof PayAtTOOfanyTypeanyType) {
				PayAtTOOfanyTypeanyType typedSource = (PayAtTOOfanyTypeanyType) source;
				PaymentProviderPayAtTOData typedDestination = new PaymentProviderPayAtTOData();
				destination = typedDestination;	
				
				List<String> emailForMail = new ArrayList<String>();
				if (typedSource.getEmailForMail() != null && typedSource.getEmailForMail().getValue() != null) {
					for (String email : typedSource.getEmailForMail().getValue().getString()) {
						emailForMail.add(email);
					}
				}
				typedDestination.setEmailForMail(
						emailForMail);
				
				if (typedSource.getEmailForPnr() != null) {
					typedDestination.setEmailForPnr(
							typedSource.getEmailForPnr().getValue());
				}
				
				if (typedSource.getOtherContact() != null) {
					typedDestination.setOtherContact(
							contactToContactDataConverter.convert(
									(Contact) typedSource.getOtherContact().getValue()));
				}
				
			} else if (source instanceof PayLaterOfanyTypeanyType) {
				PayLaterOfanyTypeanyType typedSource = (PayLaterOfanyTypeanyType) source;
				PaymentProviderPayLaterData typedDestination = new PaymentProviderPayLaterData();
				destination = typedDestination;	
				
				List<String> emailForMail = new ArrayList<String>();
				if (typedSource.getEmailForMail() != null && typedSource.getEmailForMail().getValue() != null) {
					for (String email : typedSource.getEmailForMail().getValue().getString()) {
						emailForMail.add(email);
					}
				}
				typedDestination.setEmailForMail(
						emailForMail);
				
				if (typedSource.getEmailForPnr() != null) {
					typedDestination.setEmailForPnr(
							typedSource.getEmailForPnr().getValue());
				}
				
				if (typedSource.getOtherContact() != null) {
					typedDestination.setOtherContact(
							contactToContactDataConverter.convert(
									(Contact) typedSource.getOtherContact().getValue()));
				}
				
			} else if (source instanceof PayPalOfanyType) {
				PaymentProviderPayPalData typedDestination = new PaymentProviderPayPalData();
				destination = typedDestination;	
				
			} else {
				destination = new PaymentProviderData();
				
				
			}
			
			if (source.getComunication() != null) {
				destination.setComunication(
						comunicationOfAnyTypeToPaymentComunicationConverter.convert(
								(AComunicationBase) source.getComunication().getValue()));
			}
		
		}
				
		return destination;
	}

}
