package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import java.util.Map;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ArcoQueueData;
import com.alitalia.aem.common.data.home.PaymentComunicationBancaIntesaData;
import com.alitalia.aem.common.data.home.PaymentComunicationBancoPostaData;
import com.alitalia.aem.common.data.home.PaymentComunicationBaseData;
import com.alitalia.aem.common.data.home.PaymentComunicationCreditCardData;
import com.alitalia.aem.common.data.home.PaymentComunicationFindomesticData;
import com.alitalia.aem.common.data.home.PaymentComunicationGlobalCollectData;
import com.alitalia.aem.common.data.home.PaymentComunicationInfoData;
import com.alitalia.aem.common.data.home.PaymentComunicationLottomaticaData;
import com.alitalia.aem.common.data.home.PaymentComunicationMasterPassData;
import com.alitalia.aem.common.data.home.PaymentComunicationPayLaterData;
import com.alitalia.aem.common.data.home.PaymentComunicationPayPalData;
import com.alitalia.aem.common.data.home.PaymentComunicationPosteIDData;
import com.alitalia.aem.common.data.home.PaymentComunicationSTSData;
import com.alitalia.aem.common.data.home.PaymentComunicationUnicreditData;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.AComunicationBase;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.AComunicationInfo;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.BancoPostaType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ComunicationBancaIntesa;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ComunicationBancoPosta;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ComunicationCreditCard;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ComunicationFindomestic;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ComunicationGlobalCollect;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ComunicationLottomatica;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ComunicationMasterPass;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ComunicationPayLater;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ComunicationPayPal;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ComunicationPosteID;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ComunicationSTS;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ComunicationUnicredit;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd5.ArrayOfanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd5.ArrayOfstring;
import com.alitalia.aem.ws.booking.paymentservice.xsd9.ArrayOfKeyValuePairOfstringstring;
import com.alitalia.aem.ws.booking.paymentservice.xsd9.KeyValuePairOfstringstring;

@Component(immediate=true, metatype=false)
@Service(value=PaymentComunicationBaseDataToAComunicationBase.class)
public class PaymentComunicationBaseDataToAComunicationBase 
		implements Converter<PaymentComunicationBaseData, AComunicationBase> {
	
	@Reference
	private ArcoQueueDataToArcoQueue arcoQueueDataToArcoQueueConverter;
	
	private static final Logger logger = LoggerFactory.getLogger(PaymentComunicationBaseDataToAComunicationBase.class);

	
	@Override
	public AComunicationBase convert(PaymentComunicationBaseData source) {
		
		AComunicationBase destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			com.alitalia.aem.ws.booking.paymentservice.xsd5.ObjectFactory xsd5ObjectFactory = 
					new com.alitalia.aem.ws.booking.paymentservice.xsd5.ObjectFactory();
			
			com.alitalia.aem.ws.booking.paymentservice.xsd9.ObjectFactory xsd9ObjectFactory = 
					new com.alitalia.aem.ws.booking.paymentservice.xsd9.ObjectFactory();
			
			if (source instanceof PaymentComunicationBancaIntesaData) {
				logger.debug("PaymentComunicationBaseData instanceof PaymentComunicationBancaIntesaData");
				PaymentComunicationBancaIntesaData typedSource = (PaymentComunicationBancaIntesaData) source;
				ComunicationBancaIntesa typedDestination = new ComunicationBancaIntesa();
				destination = typedDestination;
				
				typedDestination.setErrorUrl(
						objectFactory.createAComunicationInfoErrorUrl(
								typedSource.getErrorUrl()));
				
				typedDestination.setCancelUrl(
						objectFactory.createComunicationBancaIntesaCancelUrl(
								typedSource.getCancelUrl()));
				
				typedDestination.setShopperId(
						objectFactory.createComunicationBancaIntesaShopperId(
								typedSource.getShopperId()));
				
				ArrayOfKeyValuePairOfstringstring requestData = 
						xsd9ObjectFactory.createArrayOfKeyValuePairOfstringstring();
				if (typedSource.getRequestData() != null) {
					for (Map.Entry<String, String> entry : typedSource.getRequestData().entrySet()) {
						KeyValuePairOfstringstring requestDataEntry = 
								xsd9ObjectFactory.createKeyValuePairOfstringstring();
						requestDataEntry.setKey(entry.getKey());
						requestDataEntry.setValue(entry.getValue());
						requestData.getKeyValuePairOfstringstring().add(requestDataEntry);
					}
				}
				typedDestination.setRequestData(
						objectFactory.createComunicationBancaIntesaRequestData(
								requestData));
				
			} else if (source instanceof PaymentComunicationBancoPostaData) {
				logger.debug("PaymentComunicationBaseData instanceof PaymentComunicationBancoPostaData");
				PaymentComunicationBancoPostaData typedSource = (PaymentComunicationBancoPostaData) source;
				ComunicationBancoPosta typedDestination = new ComunicationBancoPosta();
				destination = typedDestination;
				
				typedDestination.setErrorUrl(
						objectFactory.createAComunicationInfoErrorUrl(
								typedSource.getErrorUrl()));
				
				if (typedSource.getType() != null) {
					typedDestination.setType(
							BancoPostaType.fromValue(typedSource.getType().value()));
				}
				
			} else if (source instanceof PaymentComunicationPosteIDData) {
				logger.debug("PaymentComunicationBaseData instanceof PaymentComunicationPosteIDData");
				PaymentComunicationPosteIDData typedSource = (PaymentComunicationPosteIDData) source;
				ComunicationPosteID typedDestination = new ComunicationPosteID();
				destination = typedDestination;
				
				typedDestination.setErrorUrl(
						objectFactory.createAComunicationInfoErrorUrl(
								typedSource.getErrorUrl()));
				
			} else if (source instanceof PaymentComunicationCreditCardData) {
				logger.debug("PaymentComunicationBaseData instanceof PaymentComunicationCreditCardData");
				PaymentComunicationCreditCardData typedSource = (PaymentComunicationCreditCardData) source;
				ComunicationCreditCard typedDestination = new ComunicationCreditCard();
				destination = typedDestination;
				
				typedDestination.setErrorUrl(
						objectFactory.createAComunicationInfoErrorUrl(
								typedSource.getErrorUrl()));
				
				typedDestination.setAcceptHeader(
						objectFactory.createComunicationCreditCardAcceptHeader(
								typedSource.getAcceptHeader()));
				
				typedDestination.setIPAddress(
						objectFactory.createComunicationCreditCardIPAddress(
								typedSource.getIpAddress()));
				
				typedDestination.setUserAgent(
						objectFactory.createComunicationCreditCardUserAgent(
								typedSource.getUserAgent()));
				
			} else if (source instanceof PaymentComunicationFindomesticData) {
				logger.debug("PaymentComunicationBaseData instanceof PaymentComunicationFindomesticData");

				PaymentComunicationFindomesticData typedSource = (PaymentComunicationFindomesticData) source;
				ComunicationFindomestic typedDestination = new ComunicationFindomestic();
				destination = typedDestination;
				
				typedDestination.setUrlCallBack(
						objectFactory.createComunicationFindomesticUrlCallBack(
								typedSource.getUrlCallBack()));

				typedDestination.setUrlRedirect(
						objectFactory.createComunicationFindomesticUrlRedirect(
								typedSource.getUrlRedirect()));
				
				ArrayOfKeyValuePairOfstringstring requestData = 
						xsd9ObjectFactory.createArrayOfKeyValuePairOfstringstring();
				if (typedSource.getRequestData() != null) {
					for (Map.Entry<String, String> entry : typedSource.getRequestData().entrySet()) {
						KeyValuePairOfstringstring requestDataEntry = 
								xsd9ObjectFactory.createKeyValuePairOfstringstring();
						requestDataEntry.setKey(entry.getKey());
						requestDataEntry.setValue(entry.getValue());
						requestData.getKeyValuePairOfstringstring().add(requestDataEntry);
					}
				}
				typedDestination.setRequestData(
						objectFactory.createComunicationFindomesticRequestData(
								requestData));
				
			} else if (source instanceof PaymentComunicationGlobalCollectData) {
				logger.debug("PaymentComunicationBaseData instanceof PaymentComunicationGlobalCollectData");

				PaymentComunicationGlobalCollectData typedSource = (PaymentComunicationGlobalCollectData) source;
				ComunicationGlobalCollect typedDestination = new ComunicationGlobalCollect();
				destination = typedDestination;
				
				typedDestination.setCurrency(
						objectFactory.createComunicationGlobalCollectCurrency(
								typedSource.getCurrency()));
				
				typedDestination.setCustomerId(
						objectFactory.createComunicationGlobalCollectCustomerId(
								typedSource.getCustomerId()));
				
				typedDestination.setErrorUrl(
						objectFactory.createComunicationGlobalCollectErrorUrl(
								typedSource.getErrorUrl()));
				
				typedDestination.setIPAddress(
						objectFactory.createComunicationGlobalCollectIPAddress(
								typedSource.getIpAddress()));
				
				typedDestination.setIsPost(
						typedSource.getIsPost());
				
				typedDestination.setOrderId(
						objectFactory.createComunicationGlobalCollectOrderId(
								typedSource.getOrderId()));
				
				ArrayOfKeyValuePairOfstringstring requestData = 
						xsd9ObjectFactory.createArrayOfKeyValuePairOfstringstring();
				if (typedSource.getRequestData() != null) {
					for (Map.Entry<String, String> entry : typedSource.getRequestData().entrySet()) {
						KeyValuePairOfstringstring requestDataEntry = 
								xsd9ObjectFactory.createKeyValuePairOfstringstring();
						requestDataEntry.setKey(entry.getKey());
						requestDataEntry.setValue(entry.getValue());
						requestData.getKeyValuePairOfstringstring().add(requestDataEntry);
					}
				}
				typedDestination.setRequestData(
						objectFactory.createComunicationGlobalCollectRequestData(
								requestData));
				
			} else if (source instanceof PaymentComunicationLottomaticaData) {
				logger.debug("PaymentComunicationBaseData instanceof PaymentComunicationLottomaticaData");

				ComunicationLottomatica typedDestination = new ComunicationLottomatica();
				destination = typedDestination; 
				
			} else if (source instanceof PaymentComunicationMasterPassData) {
				logger.debug("PaymentComunicationBaseData instanceof PaymentComunicationMasterPassData");

				PaymentComunicationMasterPassData typedSource = (PaymentComunicationMasterPassData) source;
				
				ComunicationMasterPass typedDestination = new ComunicationMasterPass();
				destination = typedDestination;	
				
				typedDestination.setCancelUrl(
						objectFactory.createComunicationPayPalCancelUrl(
								typedSource.getCancelUrl()));
				
				typedDestination.setShippingEnabled(
						typedSource.getShippingEnabled());
				
			} else if (source instanceof PaymentComunicationPayLaterData) {
				logger.debug("PaymentComunicationBaseData instanceof PaymentComunicationPayLaterData");

				PaymentComunicationPayLaterData typedSource = (PaymentComunicationPayLaterData) source;
				ComunicationPayLater typedDestination = new ComunicationPayLater();
				destination = typedDestination;	
				
				ArrayOfstring osis = xsd5ObjectFactory.createArrayOfstring();
				if (typedSource.getOsis() != null) {
					for (String osisItem : typedSource.getOsis()) {
						osis.getString().add(osisItem);
					}
				}
				typedDestination.setOsis(
						objectFactory.createComunicationPayLaterOsis(osis));
				
				ArrayOfstring remarks = xsd5ObjectFactory.createArrayOfstring();
				if (typedSource.getRemarks() != null) {
					for (String remark : typedSource.getRemarks()) {
						remarks.getString().add(remark);
					}
				}
				typedDestination.setRemarks(
						objectFactory.createComunicationPayLaterRemarks(remarks));
				
				ArrayOfanyType queues = xsd5ObjectFactory.createArrayOfanyType();
				if (typedSource.getQueues() != null) {
					for (ArcoQueueData queue : typedSource.getQueues()) {
						queues.getAnyType().add(
								arcoQueueDataToArcoQueueConverter.convert(
										queue));
					}
				}
				typedDestination.setQueues(
						objectFactory.createComunicationPayLaterQueues(queues));
				
			} else if (source instanceof PaymentComunicationPayPalData) {
				logger.debug("PaymentComunicationBaseData instanceof PaymentComunicationPayPalData");

				PaymentComunicationPayPalData typedSource = (PaymentComunicationPayPalData) source;
				ComunicationPayPal typedDestination = new ComunicationPayPal();
				destination = typedDestination;	
				
				typedDestination.setCancelUrl(
						objectFactory.createComunicationPayPalCancelUrl(
								typedSource.getCancelUrl()));
				
				typedDestination.setShippingEnabled(
						typedSource.getShippingEnabled());
				
			} else if (source instanceof PaymentComunicationPosteIDData) {
				logger.debug("PaymentComunicationBaseData instanceof PaymentComunicationPosteIDData");

				PaymentComunicationPosteIDData typedSource = (PaymentComunicationPosteIDData) source;
				ComunicationPosteID typedDestination = new ComunicationPosteID();
				destination = typedDestination;	
				
				typedDestination.setErrorUrl(
						objectFactory.createAComunicationInfoErrorUrl(
								typedSource.getErrorUrl()));
				
			} else if (source instanceof PaymentComunicationUnicreditData) {
				logger.debug("PaymentComunicationBaseData instanceof PaymentComunicationUnicreditData");

				PaymentComunicationUnicreditData typedSource = (PaymentComunicationUnicreditData) source;
				ComunicationUnicredit typedDestination = new ComunicationUnicredit();
				destination = typedDestination;	
				
				typedDestination.setErrorUrl(
						objectFactory.createAComunicationInfoErrorUrl(
								typedSource.getErrorUrl()));
			
			} else if (source instanceof PaymentComunicationInfoData) {
				logger.debug("PaymentComunicationBaseData instanceof PaymentComunicationInfoData");

				PaymentComunicationInfoData typedSource = (PaymentComunicationInfoData) source;
				AComunicationInfo typedDestination = new AComunicationInfo();
				destination = typedDestination;
				
				typedDestination.setErrorUrl(
						objectFactory.createAComunicationInfoErrorUrl(
								typedSource.getErrorUrl()));
				
			} else if (source instanceof PaymentComunicationSTSData) {
				logger.debug("PaymentComunicationBaseData instanceof PaymentComunicationSTSData");

				PaymentComunicationSTSData typedSource = (PaymentComunicationSTSData) source;
				ComunicationSTS typedDestination = new ComunicationSTS();
				destination = typedDestination;	
				
				typedDestination.setAcceptHeader(
						objectFactory.createComunicationSTSAcceptHeader(
								typedSource.getAcceptHeader()));
				
				typedDestination.setIPAddress(
						objectFactory.createComunicationSTSIPAddress(
								typedSource.getIpAddress()));
				
				typedDestination.setUserAgent(
						objectFactory.createComunicationSTSUserAgent(
								typedSource.getUserAgent()));
				
				typedDestination.setShippingEnabled(
						typedSource.getShippingEnabled());
				
			} else {
				logger.debug("PaymentComunicationBaseData instanceof AComunicationBase");

				destination = new AComunicationBase();
				
			}
			
			destination.setDescription(
					objectFactory.createAComunicationBaseDescription(
							source.getDescription()));
			
			destination.setLanguageCode(
					objectFactory.createAComunicationBaseLanguageCode(
							source.getLanguageCode()));
			
			destination.setReturnUrl(
					objectFactory.createAComunicationBaseReturnUrl(
							source.getReturnUrl()));
			
		}
				
		return destination;
	}

}
