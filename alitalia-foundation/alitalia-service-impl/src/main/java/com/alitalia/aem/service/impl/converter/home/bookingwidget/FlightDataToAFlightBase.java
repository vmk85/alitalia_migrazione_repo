package com.alitalia.aem.service.impl.converter.home.bookingwidget;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.AFlightBase;

@Component(immediate=true, metatype=false)
@Service(value=FlightDataToAFlightBase.class)
public class FlightDataToAFlightBase implements Converter<FlightData, AFlightBase> {

	@Reference
	private DirectFlightDataToDirectFlight directFlightDataConverter;
	
	@Reference
	private ConnectingFlightDataToConnectingFlight connectingFlightDataConverter;
	
	@Override
	public AFlightBase convert(FlightData source) {
		AFlightBase destination = null;
		if (source != null) {
			if (source instanceof DirectFlightData) {
				destination = directFlightDataConverter.convert((DirectFlightData) source);
			} else if (source instanceof ConnectingFlightData) {
				destination = connectingFlightDataConverter.convert((ConnectingFlightData) source);
			}
			
		}
		return destination;
	}

}
