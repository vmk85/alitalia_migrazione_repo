package com.alitalia.aem.service.impl.converter.home.flightstatusservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.FlightInfoData;
import com.alitalia.aem.common.messages.home.RetrieveFlightInfoResponse;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetFlightInfoModelResponse;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.FlightInfoModel;

@Component(immediate = true, metatype = false)
@Service(value = GetFlightInfoModelResponseToRetrieveFlightInfoResponseConverter.class)
public class GetFlightInfoModelResponseToRetrieveFlightInfoResponseConverter implements Converter<GetFlightInfoModelResponse, RetrieveFlightInfoResponse> {
	
	@Reference
	private FlightInfoModelToFlightInfoData flightInfoModelConverter;
	
	@Override
	public RetrieveFlightInfoResponse convert(GetFlightInfoModelResponse source) {
		RetrieveFlightInfoResponse response = new RetrieveFlightInfoResponse();
		
		JAXBElement<FlightInfoModel> jaxbFlightInfoModel = source.getFlightInfo();
		FlightInfoModel flightInfoModel = jaxbFlightInfoModel.getValue();
		FlightInfoData flightInfoData = flightInfoModelConverter.convert(flightInfoModel);
		
		response.setFlightInfoData(flightInfoData);
		return response;
	}
}