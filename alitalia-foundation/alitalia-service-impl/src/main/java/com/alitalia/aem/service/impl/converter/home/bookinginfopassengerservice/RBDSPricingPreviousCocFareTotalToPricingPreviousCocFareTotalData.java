package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingPreviousCocFareTotalData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ResultBookingDetailsSolutionPricingPreviousCocFareTotal;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingPreviousCocFareTotalToPricingPreviousCocFareTotalData.class)
public class RBDSPricingPreviousCocFareTotalToPricingPreviousCocFareTotalData
		implements Converter<ResultBookingDetailsSolutionPricingPreviousCocFareTotal, ResultBookingDetailsSolutionPricingPreviousCocFareTotalData> {
	
	@Override
	public ResultBookingDetailsSolutionPricingPreviousCocFareTotalData convert(
			ResultBookingDetailsSolutionPricingPreviousCocFareTotal source) {
		ResultBookingDetailsSolutionPricingPreviousCocFareTotalData destination = null;
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingPreviousCocFareTotalData();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}

}
