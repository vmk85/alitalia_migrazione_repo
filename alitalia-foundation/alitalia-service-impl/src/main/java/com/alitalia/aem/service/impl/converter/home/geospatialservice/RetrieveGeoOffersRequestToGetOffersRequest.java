package com.alitalia.aem.service.impl.converter.home.geospatialservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.IPAddressData;
import com.alitalia.aem.common.messages.home.RetrieveGeoOffersRequest;
import com.alitalia.aem.ws.geospatial.service.xsd1.GetOffersRequest;
import com.alitalia.aem.ws.geospatial.service.xsd1.ObjectFactory;
import com.alitalia.aem.ws.geospatial.service.xsd2.IPAddress;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveGeoOffersRequestToGetOffersRequest.class)
public class RetrieveGeoOffersRequestToGetOffersRequest implements Converter<RetrieveGeoOffersRequest, GetOffersRequest> {

	@Reference
	private IPAddressDataToIPAddress ipAddressDataConverter;
	
	@Override
	public GetOffersRequest convert(RetrieveGeoOffersRequest source) {
		
		GetOffersRequest request = new GetOffersRequest();
		
		String departureAirpordCode = source.getDepartureAirportCode();
		
		IPAddressData ipAddressData = source.getIpAddress();
		IPAddress ipAddress = (ipAddressData != null) ? ipAddressDataConverter.convert(ipAddressData) : null;		
		String languageCode = source.getLanguageCode();
		String marketCode = source.getMarketCode();
		String paxType = source.getPaxType();
		
		ObjectFactory objectFactory = new ObjectFactory();
		
		JAXBElement<String> jaxbDepartureAirport = objectFactory.createGetOffersRequestDepartureAirportCode(departureAirpordCode);
		JAXBElement<IPAddress> jaxbIPAddress = objectFactory.createGetOffersRequestIPAddress(ipAddress);
		JAXBElement<String> jaxbLanguageCode = objectFactory.createGetOffersRequestLanguageCode(languageCode);
		JAXBElement<String> jaxbMarketCode = objectFactory.createGetOffersRequestMarketCode(marketCode);
		JAXBElement<String> jaxbPaxType = objectFactory.createGetOffersRequestPaxType(paxType);
		
		request.setDepartureAirportCode(jaxbDepartureAirport);
		request.setIPAddress(jaxbIPAddress);
		request.setLanguageCode(jaxbLanguageCode);
		request.setMarketCode(jaxbMarketCode);
		request.setPaxType(jaxbPaxType);
		
		return request;
	}
}