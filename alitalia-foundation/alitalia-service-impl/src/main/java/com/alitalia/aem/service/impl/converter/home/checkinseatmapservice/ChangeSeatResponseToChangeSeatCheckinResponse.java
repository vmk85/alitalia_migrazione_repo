package com.alitalia.aem.service.impl.converter.home.checkinseatmapservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.ChangeSeatCheckinResponse;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd1.ChangeSeatResponse;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd4.ArrayOfstring;

@Component(immediate=true, metatype=false)
@Service(value=ChangeSeatResponseToChangeSeatCheckinResponse.class)
public class ChangeSeatResponseToChangeSeatCheckinResponse implements Converter<ChangeSeatResponse, ChangeSeatCheckinResponse> {
	

	@Reference
	private SeatMapToCheckinSeatMapData seatMapConverter;


	@Override
	public ChangeSeatCheckinResponse convert(ChangeSeatResponse source) {
		ChangeSeatCheckinResponse destination = null;

		if (source != null) {
			destination = new ChangeSeatCheckinResponse();
			
			JAXBElement<ArrayOfstring> sourceMessages = source.getMessages();
			if(sourceMessages != null &&
					sourceMessages.getValue() != null &&
					sourceMessages.getValue().getString() != null &&
					!sourceMessages.getValue().getString().isEmpty()){
				List<String> messagesList = new ArrayList<String>();
				for(String sourceMessage : sourceMessages.getValue().getString()){
					messagesList.add(sourceMessage);
				}
				destination.setMessages(messagesList);
			}
			
			if(source.getNewSeat() != null){
				destination.setNewSeat(source.getNewSeat().getValue());
			}
		}

		return destination;
	}

}
