package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoData;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ResultBookingDetailsSolutionPricingBookingInfo;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingBookingInfoDataToPricingBookingInfo.class)
public class RBDSPricingBookingInfoDataToPricingBookingInfo implements
		Converter<ResultBookingDetailsSolutionPricingBookingInfoData, ResultBookingDetailsSolutionPricingBookingInfo> {

	@Reference
	RBDSPricingBookingInfoFareDataToPricingBookingInfoFare rbdsPricingBookingInfoFareConverter;
	
	@Reference
	RBDSPricingBookingInfoSegmentDataToPricingBookingInfoSegment rbdsPricingBookingInfoSegmentConverter;
	
	@Override
	public ResultBookingDetailsSolutionPricingBookingInfo convert(
			ResultBookingDetailsSolutionPricingBookingInfoData source) {
		
		ResultBookingDetailsSolutionPricingBookingInfo destination = null;
		if(source != null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionPricingBookingInfo();
			destination.setBookingCodeCountField(source.getBookingCodeCountField());
			destination.setBookingCodeField(source.getBookingCodeField());
			destination.setCabinField(source.getCabinField());
			destination.setFareField(rbdsPricingBookingInfoFareConverter.convert(source.getFareField()));
			destination.setSegmentField(rbdsPricingBookingInfoSegmentConverter.convert(source.getSegmentField()));
			destination.setMarriedSegmentIndexField(source.getMarriedSegmentIndexField());
			destination.setMarriedSegmentIndexFieldSpecified(source.isMarriedSegmentIndexFieldSpecified());
			
			
		}
		
		return destination;
		
	}

}
