package com.alitalia.aem.service.impl.checkinrest.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.checkinrest.DeleteInsuranceResponse;
import com.alitalia.aem.common.messages.checkinrest.GetInsuranceResponse;
import com.alitalia.aem.common.messages.checkinrest.SetInsuranceResponse;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class InsuranceMessageUnmarshal {

	private static final Logger logger = LoggerFactory.getLogger(InsuranceMessageUnmarshal.class);

	public static GetInsuranceResponse getInsuranceUnmarshal(String jsonString) {
		GetInsuranceResponse getInsuranceResponse = null;
		String errorObj = "";
		Gson gson = new Gson();
		try {
			if (jsonString.contains("isError")) {
				errorObj = jsonString;
				getInsuranceResponse = new GetInsuranceResponse();
				getInsuranceResponse.setError(errorObj);
			}
			else {
//				logger.info("getInsuranceUnmarshal jsonString : " + jsonString);
				getInsuranceResponse = gson.fromJson(jsonString, GetInsuranceResponse.class);
			}
		}
		catch (JsonSyntaxException e) {
			logger.error(e.getMessage());
		}

		return getInsuranceResponse;
	}

	public static SetInsuranceResponse setInsuranceUnmarshal(String jsonString) {
		SetInsuranceResponse setInsuranceResponse = null;
		String errorObj = "";
		Gson gson = new Gson();
		try {
			if (jsonString.contains("isError")) {
				errorObj = jsonString;
				setInsuranceResponse = new SetInsuranceResponse();
				setInsuranceResponse.setError(errorObj);
			}
			else {
//				logger.info("setInsuranceUnmarshal jsonString : " + jsonString);
				setInsuranceResponse = gson.fromJson(jsonString, SetInsuranceResponse.class);
			}
		}
		catch (JsonSyntaxException e) {
			logger.error(e.getMessage());
		}

		return setInsuranceResponse;
	}

	public static DeleteInsuranceResponse deleteInsuranceUnmarshal(String jsonString) {
		DeleteInsuranceResponse deleteInsuranceResponse = null;
		String errorObj = "";
		Gson gson = new Gson();
		try {
			if (jsonString.contains("isError")) {
				errorObj = jsonString;
				deleteInsuranceResponse = new DeleteInsuranceResponse();
				deleteInsuranceResponse.setError(errorObj);
			}
			else {
//				logger.info("deleteInsuranceUnmarshal jsonString : " + jsonString);
				deleteInsuranceResponse = gson.fromJson(jsonString, DeleteInsuranceResponse.class);
			}
		}
		catch (JsonSyntaxException e) {
			logger.error(e.getMessage());
		}

		return deleteInsuranceResponse;
	}

}
