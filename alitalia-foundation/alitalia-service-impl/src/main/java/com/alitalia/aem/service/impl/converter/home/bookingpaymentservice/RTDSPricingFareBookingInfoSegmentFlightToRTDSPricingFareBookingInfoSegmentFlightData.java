package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareBookingInfoSegmentFlightData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingFareBookingInfoSegmentFlight;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareBookingInfoSegmentFlightToRTDSPricingFareBookingInfoSegmentFlightData.class)
public class RTDSPricingFareBookingInfoSegmentFlightToRTDSPricingFareBookingInfoSegmentFlightData implements
		Converter<ResultTicketingDetailSolutionPricingFareBookingInfoSegmentFlight, 
					ResultTicketingDetailSolutionPricingFareBookingInfoSegmentFlightData> {

	@Override
	public ResultTicketingDetailSolutionPricingFareBookingInfoSegmentFlightData convert(
			ResultTicketingDetailSolutionPricingFareBookingInfoSegmentFlight source) {
		ResultTicketingDetailSolutionPricingFareBookingInfoSegmentFlightData destintion = null;

		if (source != null) {
			destintion = new ResultTicketingDetailSolutionPricingFareBookingInfoSegmentFlightData();

			destintion.setCarrierField(source.getCarrierField());
			destintion.setNumberField(source.getNumberField());
			destintion.setNumberFieldSpecified(source.isNumberFieldSpecified());
		}

		return destintion;
	}

}
