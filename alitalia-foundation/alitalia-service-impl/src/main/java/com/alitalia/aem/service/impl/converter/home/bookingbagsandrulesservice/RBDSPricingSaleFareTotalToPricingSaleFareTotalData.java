package com.alitalia.aem.service.impl.converter.home.bookingbagsandrulesservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingSaleFareTotalData;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetailsSolutionPricingSaleFareTotal;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingSaleFareTotalToPricingSaleFareTotalData.class)
public class RBDSPricingSaleFareTotalToPricingSaleFareTotalData implements
		Converter<ResultBookingDetailsSolutionPricingSaleFareTotal, ResultBookingDetailsSolutionPricingSaleFareTotalData> {

	@Override
	public ResultBookingDetailsSolutionPricingSaleFareTotalData convert(
			ResultBookingDetailsSolutionPricingSaleFareTotal source) {
		ResultBookingDetailsSolutionPricingSaleFareTotalData destination = null;
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingSaleFareTotalData();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}

	public ResultBookingDetailsSolutionPricingSaleFareTotalData convert(
			com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetailsSolutionPricingSaleFareTotal source) {
		ResultBookingDetailsSolutionPricingSaleFareTotalData destination = null;
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingSaleFareTotalData();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}
}
