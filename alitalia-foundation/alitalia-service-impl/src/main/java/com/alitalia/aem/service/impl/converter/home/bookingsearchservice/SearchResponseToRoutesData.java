package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.ws.booking.searchservice.xsd1.SearchResponse;
import com.alitalia.aem.ws.booking.searchservice.xsd2.ModelResponse;
import com.alitalia.aem.ws.booking.searchservice.xsd3.Routes;

@Component(immediate=true, metatype=false)
@Service(value=SearchResponseToRoutesData.class)
public class SearchResponseToRoutesData implements Converter<SearchResponse, RoutesData> {

	private static final Logger logger = LoggerFactory.getLogger(SearchResponseToRoutesData.class);

	@Reference
	private RoutesToRoutesData routesConveter;

	@Reference
	private BookingSearchServiceJAXBContextFactory jaxbContextFactory;

	@SuppressWarnings("unchecked")
	@Override
	public RoutesData convert(SearchResponse source) {
		RoutesData destination = null;

		if (source != null) {
			destination = new RoutesData();
			ModelResponse result = (ModelResponse) source.getResult().getValue();
			Object model = result.getModel().getValue();
			if (model instanceof Routes) {
				Routes routes = (Routes) result.getModel().getValue();
				destination = routesConveter.convert(routes);
			}
			else
			{
				try {
					Unmarshaller routesUnmashaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
					JAXBElement<Routes> unmarshalledObj = (JAXBElement<Routes>) routesUnmashaller
																.unmarshal((Node) model);
					destination = routesConveter
							.convert(unmarshalledObj.getValue());
				} catch (JAXBException e) {
					logger.error(
							"Error instantiating JAXBContext/Unmarshaller: {}", e);
				}
			}
		}

		return destination;
	}

}
