package com.alitalia.aem.service.impl.converter.homerest.staticdataservice;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyStore;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.BaseRequest;
import com.alitalia.aem.ws.common.exception.WSClientException;
import com.alitalia.aem.ws.common.utils.WSClientConstants;
import com.google.gson.Gson;


@Service(value = StaticDataServiceClientRest.class)
@Component(immediate = true, metatype = true) 
@Properties({  
	@Property(name = "service.vendor", value = "Reply") })
public class StaticDataServiceClientRest {

	private static Logger logger = LoggerFactory.getLogger(StaticDataServiceClientRest.class);

	@Property(description = "URL of the 'Static Data Web Service REST' used to invoke the Web Service.")
	private static final String WS_ENDPOINT_PROPERTY = "ws.endpoint";

	@Property(description = "Flag used to invoke Web Service in HTTPS.")
	private static final String WS_HTTPS_MODE_PROPERTY = "ws.https_mode";

	@Property(description = "Flag used to load identity store.")
	private static final String WS_LOAD_IDENTITY_STORE_PROPERTY = "ws.load_identity_store";

	@Property(description = "Flag used to enable hostname verifier.")
	private static final String WS_HOSTNAME_VERIFIER_PROPERTY = "ws.hostname_verifier";

	@Property(description = "Property used to set identity store path.")
	private static final String WS_IDENTITY_STORE_PATH_PROPERTY = "ws.identity_store_path";

	@Property(description = "Property used to set identity store password.")
	private static final String WS_IDENTITY_STORE_PASSWORD_PROPERTY = "ws.identity_store_password";

	@Property(description = "Property used to set identity key password.")
	private static final String WS_IDENTITY_KEY_PASSWORD_PROPERTY = "ws.identity_key_password";

	@Property(description = "Property used to set trust store path.")
	private static final String WS_TRUST_STORE_PATH_PROPERTY = "ws.trust_store_path";

	@Property(description = "Property used to set trust store password.")
	private static final String WS_TRUST_STORE_PASSWORD_PROPERTY = "ws.trust_store_password";

	@Property(description = "Property used to set connection timout")
	private static final String WS_CONNECTION_TIMEOUT_PROPERTY = "ws.connection_timeout";

	@Property(description = "Property used to set request timout")
	private static final String WS_REQUEST_TIMEOUT_PROPERTY = "ws.request_timeout";

	@Property(description = "Property used to set request timout")
	private static final String WS_SSL_PROTOCOL = "ws.ssl_protocol";

	@Property(description = "Property used to print the request")
	private static final String WS_PRINT_REQUET = "ws.print_request";

	@Property(description = "Property used to print the response")
	private static final String WS_PRINT_RESPONSE = "ws.print_response";

	public String serviceEndpoint;
//	private String serviceQNameNamespace;
//	private String serviceQNameLocalPart;
	private Integer connectionTimeout;
	private Integer requestTimeout;
	private String identityStorePath;
	private String identityStorePassword;
	private String identityKeyPassword;
	private String trustStorePath;
	private String trustStorePassword;
	private String sslProtocol;
	private boolean hostnameVerifier;
	private boolean loadIdentityStore;
	private boolean httpsMode;
	private boolean printRequest;
	private boolean printResponse;

	private boolean initRequired = true;


	private void init() throws WSClientException {
		FileInputStream fisTruststore = null;
		FileInputStream fisIdentitystore = null;
		try {
//			URL classURL = StaticDataServiceClientRest.class.getClassLoader().getResource(this.serviceEndpoint);
//			QName serviceQName = new QName(serviceQNameNamespace, serviceQNameLocalPart);

			logger.debug("Https mode [" + httpsMode + "]");

			if (httpsMode) {
				logger.debug("Loaded identity store path [" + identityStorePath + "], trust store path [" + trustStorePath + "], " 
						+ "hostname verifier [" + hostnameVerifier + "] and loading identity store flag [" + loadIdentityStore + "]");

				SSLContext sslContext = SSLContext.getInstance(sslProtocol);

				TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
				KeyStore trustKeystore = KeyStore.getInstance(KeyStore.getDefaultType());
				fisTruststore = new FileInputStream(trustStorePath);
				trustKeystore.load(fisTruststore, trustStorePassword.toCharArray());
				tmf.init(trustKeystore);

				if (loadIdentityStore) {
					KeyStore identityKeystore = KeyStore.getInstance(WSClientConstants.keystoreTypeDefault);
					fisIdentitystore = new FileInputStream(identityStorePath);
					identityKeystore.load(fisIdentitystore, identityStorePassword.toCharArray());

					KeyManagerFactory kmf = KeyManagerFactory.getInstance(WSClientConstants.keymanagerFactoryAlgorithmDefault);
					kmf.init(identityKeystore, identityKeyPassword.toCharArray());
					sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

					logger.debug("Loaded identity and trust store");
				} else {
					sslContext.init(null, tmf.getTrustManagers(), null);
					logger.debug("Loaded trust store only");
				}

				SSLContext.setDefault(sslContext);
			}

		} catch (java.net.MalformedURLException e) {
			logger.error("Error to initialize StaticDataServiceClientRest, invalid endpoint = {}", this.serviceEndpoint);
			throw new WSClientException("Error to initialize StaticDataServiceClientRest", e);
		} catch (Exception e) {
			logger.error("Error to initialize StaticDataServiceClientRest: ", e);
			throw new WSClientException("Error to initialize StaticDataServiceClientRest", e);
		} finally {
			if (fisIdentitystore != null) {
				try {
					fisIdentitystore.close();
				} catch (IOException e) {
					logger.error("Error to close input stream", e);
				}
			}

			if (fisTruststore != null) {
				try {
					fisTruststore.close();
				} catch (IOException e) {
					logger.error("Error to close input stream", e);
				}
			}
		}
	}

	@Activate
	protected void activate(final Map<String, Object> config) {
		String serviceEndpoint = null;
		String identityStorePath = null;
		String identityStorePassword = null;
		String identityKeyPassword = null;
		String trustStorePath = null;
		String trustStorePassword = null;
		Integer connectionTimeout = null;
		Integer requestTimeout = null;
		String sslProtocol = null;
		boolean httpsMode;
		boolean hostnameVerifier;
		boolean loadIdentityStore;
		boolean printRequest;
		boolean printResponse;

		try {			
			httpsMode = Boolean.valueOf(String.valueOf(config.get(WS_HTTPS_MODE_PROPERTY)));
			this.httpsMode = httpsMode;

			hostnameVerifier = Boolean.valueOf(String.valueOf(config.get(WS_HOSTNAME_VERIFIER_PROPERTY)));
			this.hostnameVerifier = hostnameVerifier;

			loadIdentityStore = Boolean.valueOf(String.valueOf(config.get(WS_LOAD_IDENTITY_STORE_PROPERTY)));
			this.loadIdentityStore = loadIdentityStore;

			identityStorePath = String.valueOf(config.get(WS_IDENTITY_STORE_PATH_PROPERTY));
			this.identityStorePath = identityStorePath;

			identityStorePassword = String.valueOf(config.get(WS_IDENTITY_STORE_PASSWORD_PROPERTY));
			this.identityStorePassword = identityStorePassword;

			identityKeyPassword = String.valueOf(config.get(WS_IDENTITY_KEY_PASSWORD_PROPERTY));
			this.identityKeyPassword = identityKeyPassword;

			trustStorePath = String.valueOf(config.get(WS_TRUST_STORE_PATH_PROPERTY));
			this.trustStorePath = trustStorePath;

			trustStorePassword = String.valueOf(config.get(WS_TRUST_STORE_PASSWORD_PROPERTY));
			this.trustStorePassword = trustStorePassword;

			connectionTimeout = Integer.valueOf(String.valueOf(config.get(WS_CONNECTION_TIMEOUT_PROPERTY)));
			this.connectionTimeout = connectionTimeout;	

			requestTimeout = Integer.valueOf(String.valueOf(config.get(WS_REQUEST_TIMEOUT_PROPERTY)));
			this.requestTimeout = requestTimeout;	

			sslProtocol = String.valueOf(config.get(WS_SSL_PROTOCOL));
			this.sslProtocol = sslProtocol;	

			printRequest = Boolean.valueOf(String.valueOf(config.get(WS_PRINT_REQUET)));
			this.printRequest = printRequest;	

			printResponse = Boolean.valueOf(String.valueOf(config.get(WS_PRINT_RESPONSE)));
			this.printResponse = printResponse;			
		} catch (Exception e) {
			logger.error("Error to read custom configuration to activate StaticDataServiceClientRest - default configuration used", e);
			setDefault();
		}

		try {			
			serviceEndpoint = String.valueOf(config.get(WS_ENDPOINT_PROPERTY));
			this.serviceEndpoint = serviceEndpoint;

			logger.info("Activated StaticDataServiceClientRest: "
					+ "WS_ENDPOINT_PROPERTY=[" + this.serviceEndpoint + "], "
					+ "WS_HTTPS_MODE_PROPERTY=[" + this.httpsMode + "], "
					+ "WS_HOSTNAME_VERIFIER_PROPERTY=[" + this.hostnameVerifier +"], "
					+ "WS_LOAD_IDENTITY_STORE_PROPERTY=[" + this.loadIdentityStore + "], "
					+ "WS_IDENTITY_STORE_PATH_PROPERTY=[" + this.identityStorePath + "], "
					+ "WS_TRUST_STORE_PATH_PROPERTY=[" + this.trustStorePath + "], "
					+ "WS_CONNECTION_TIMEOUT_PROPERTY=[" + this.connectionTimeout + "], "
					+ "WS_REQUEST_TIMEOUT_PROPERTY=[" + this.requestTimeout + "], "
					+ "WS_SSL_PROTOCOL=[" + this.sslProtocol + "], "
					+ "WS_PRINT_RESPONSE=[" + this.printRequest + "], "
					+ "WS_PRINT_REQUET=[" + this.printResponse + "]");
		} catch (Exception e) {
			logger.error("Error to read WS_ENDPOINT_PROPERTY to activate StaticDataServiceClientRest", e);
			throw new WSClientException("Error to initialize StaticDataServiceClientRest", e);
		}

		try {
			init();
			this.initRequired = false;
		} catch (Exception e) {
			this.initRequired = true;
			logger.error("Error initializing StaticDataServiceClientRest", e);
		}
	}

	@Modified
	protected void modified(final Map<String, Object> config) {
		String serviceEndpoint = null;
		boolean httpsMode;
		boolean hostnameVerifier;
		boolean loadIdentityStore;
		boolean printRequest;
		boolean printResponse;
		String identityStorePath = null;
		String identityStorePassword = null;
		String identityKeyPassword = null;
		String trustStorePath = null;
		String trustStorePassword = null;
		Integer connectionTimeout = null;
		Integer requestTimeout = null;
		String sslProtocol = null;

		try {			
			httpsMode = Boolean.valueOf(String.valueOf(config.get(WS_HTTPS_MODE_PROPERTY)));
			this.httpsMode = httpsMode;

			hostnameVerifier = Boolean.valueOf(String.valueOf(config.get(WS_HOSTNAME_VERIFIER_PROPERTY)));
			this.hostnameVerifier = hostnameVerifier;

			loadIdentityStore = Boolean.valueOf(String.valueOf(config.get(WS_LOAD_IDENTITY_STORE_PROPERTY)));
			this.loadIdentityStore = loadIdentityStore;

			identityStorePath = String.valueOf(config.get(WS_IDENTITY_STORE_PATH_PROPERTY));
			this.identityStorePath = identityStorePath;

			identityStorePassword = String.valueOf(config.get(WS_IDENTITY_STORE_PASSWORD_PROPERTY));
			this.identityStorePassword = identityStorePassword;

			identityKeyPassword = String.valueOf(config.get(WS_IDENTITY_KEY_PASSWORD_PROPERTY));
			this.identityKeyPassword = identityKeyPassword;

			trustStorePath = String.valueOf(config.get(WS_TRUST_STORE_PATH_PROPERTY));
			this.trustStorePath = trustStorePath;

			trustStorePassword = String.valueOf(config.get(WS_TRUST_STORE_PASSWORD_PROPERTY));
			this.trustStorePassword = trustStorePassword;

			connectionTimeout = Integer.valueOf(String.valueOf(config.get(WS_CONNECTION_TIMEOUT_PROPERTY)));
			this.connectionTimeout = connectionTimeout;	

			requestTimeout = Integer.valueOf(String.valueOf(config.get(WS_REQUEST_TIMEOUT_PROPERTY)));
			this.requestTimeout = requestTimeout;	

			sslProtocol = String.valueOf(config.get(WS_SSL_PROTOCOL));
			this.sslProtocol = sslProtocol;	

			printRequest = Boolean.valueOf(String.valueOf(config.get(WS_PRINT_REQUET)));
			this.printRequest = printRequest;	

			printResponse = Boolean.valueOf(String.valueOf(config.get(WS_PRINT_RESPONSE)));
			this.printResponse = printResponse;	
		} catch (Exception e) {
			logger.error("Error to read custom configuration to modify StaticDataServiceClientRest - default configuration used", e);
			setDefault();
		}

		try {			
			serviceEndpoint = String.valueOf(config.get(WS_ENDPOINT_PROPERTY));
			this.serviceEndpoint = serviceEndpoint;

			logger.info("Modified StaticDataServiceClientRest: "
					+ "WS_ENDPOINT_PROPERTY=[" + this.serviceEndpoint + "], "
					+ "WS_HTTPS_MODE_PROPERTY=[" + this.httpsMode + "], "
					+ "WS_HOSTNAME_VERIFIER_PROPERTY=[" + this.hostnameVerifier +"], "
					+ "WS_LOAD_IDENTITY_STORE_PROPERTY=[" + this.loadIdentityStore + "], "
					+ "WS_IDENTITY_STORE_PATH_PROPERTY=[" + this.identityStorePath + "], "
					+ "WS_TRUST_STORE_PATH_PROPERTY=[" + this.trustStorePath + "], "
					+ "WS_CONNECTION_TIMEOUT_PROPERTY=[" + this.connectionTimeout + "], "
					+ "WS_REQUEST_TIMEOUT_PROPERTY=[" + this.requestTimeout + "], "
					+ "WS_SSL_PROTOCOL=[" + this.sslProtocol + "], "
					+ "WS_PRINT_RESPONSE=[" + this.printRequest + "], "
					+ "WS_PRINT_REQUET=[" + this.printResponse + "]");
		} catch (Exception e) {
			logger.error("Error to read WS_ENDPOINT_PROPERTY to modify StaticDataServiceClientRest", e);
			throw new WSClientException("Error to initialize StaticDataServiceClientRest", e);
		}


		try {
			init();
			this.initRequired = false;
		} catch (Exception e) {
			this.initRequired = true;
			logger.error("Error initializing StaticDataServiceClientRest", e);
		}
	}

	private void setDefault() {
		this.httpsMode = WSClientConstants.httpsModeDefault;
		this.hostnameVerifier = WSClientConstants.hostnameVerifierDefault;
		this.loadIdentityStore = WSClientConstants.loadIdentityStoreDefault;
		this.connectionTimeout = WSClientConstants.connectionTimeoutDefault;
		this.requestTimeout = WSClientConstants.requestTimeoutDefault;
		this.sslProtocol = WSClientConstants.sslProtocolDefault;
		this.printRequest = WSClientConstants.printRequest;
		this.printResponse = WSClientConstants.printResponse;
	}

//	public StaticDataResponse getAirportsAllLanguages(StaticDataRequest request) {
//		if (this.initRequired) {
//			init();
//			this.initRequired = false;
//		}
//
//		try {
//			logger.info("Calling WS-StaticDataService Operation-getAirportsAllLanguages...");
//			long before = System.currentTimeMillis();
//			StaticDataResponse response = iStaticDataService.getAirportsAllLanguages(request);
//			long after = System.currentTimeMillis();
//			long execTime = after - before;
//
//			logger.info("Calling WS-StaticDataService Operation-getAirportsAllLanguages...done execution time="
//					+ execTime);
//			return response;
//		} catch (Exception e) {
//			throw new WSClientException(
//					"Exception while executing WS-StaticDataService Operation-getAirportsAllLanguages", e);
//		}
//	}

//	public Boolean deleteItemCache(StaticDataRequest request) {
//		if (this.initRequired) {
//			init();
//			this.initRequired = false;
//		}
//
//		try {
//			logger.info("Calling WS-StaticDataService Operation-deleteItemCache...");
//			long before = System.currentTimeMillis();
//			Boolean response = iStaticDataService.deleteItemCache(request);
//			long after = System.currentTimeMillis();
//			long execTime = after - before;
//
//			logger.info("Calling WS-StaticDataService Operation-deleteItemCache...done execution time=" + execTime);
//			return response;
//		} catch (Exception e) {
//			throw new WSClientException("Exception while executing WS-StaticDataService Operation-deleteItemCache", e);
//		}
//	}

	public Integer getConnectionTimeout() {
		return connectionTimeout;
	}

	public Integer getRequestTimeout() {
		return requestTimeout;
	}

	public String getIdentityStorePath() {
		return identityStorePath;
	}

	public String getIdentityStorePassword() {
		return identityStorePassword;
	}

	public String getTrustStorePath() {
		return trustStorePath;
	}

	public String getTrustStorePassword() {
		return trustStorePassword;
	}

	public boolean getHostnameVerifier() {
		return hostnameVerifier;
	}

	public boolean getLoadIdentityStore() {
		return loadIdentityStore;
	}

	public boolean getHttpsMode() {
		return httpsMode;
	}

	public String getSslProtocol() {
		return sslProtocol;
	}

	public String getIdentityKeyPassword() {
		return identityKeyPassword;
	}

	public boolean isPrintRequest() {
		return printRequest;
	}

	public boolean isPrintResponse() {
		return printResponse;
	}	

	public String getData(BaseRequest request, String relativePath){

		String jsonString = "";
		
		FileInputStream fisTruststore = null;
		FileInputStream fisIdentitystore = null;
		
		InputStream is = null;
		OutputStream os = null;
		URLConnection urlConnection = null;
		Gson gson = new Gson();		
		String input = "";
		input = gson.toJson(request);
		
		String serviceEndPoint = this.serviceEndpoint;
		boolean httpsMode = this.httpsMode; 
		
		try {
//			URL classURL = StaticDataServiceClientRest.class.getClassLoader().getResource(this.serviceEndpoint);
//			QName serviceQName = new QName(serviceQNameNamespace, serviceQNameLocalPart);

			logger.debug("Https mode [" + httpsMode + "]");

			if (httpsMode) {
				logger.debug("Loaded identity store path [" + identityStorePath + "], trust store path [" + trustStorePath + "], " 
						+ "hostname verifier [" + hostnameVerifier + "] and loading identity store flag [" + loadIdentityStore + "]");

				SSLContext sslContext = SSLContext.getInstance(sslProtocol);

				TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
				KeyStore trustKeystore = KeyStore.getInstance(KeyStore.getDefaultType());
				fisTruststore = new FileInputStream(trustStorePath);
				trustKeystore.load(fisTruststore, trustStorePassword.toCharArray());
				tmf.init(trustKeystore);

				if (loadIdentityStore) {
					KeyStore identityKeystore = KeyStore.getInstance(WSClientConstants.keystoreTypeDefault);
					fisIdentitystore = new FileInputStream(identityStorePath);
					identityKeystore.load(fisIdentitystore, identityStorePassword.toCharArray());

					KeyManagerFactory kmf = KeyManagerFactory.getInstance(WSClientConstants.keymanagerFactoryAlgorithmDefault);
					kmf.init(identityKeystore, identityKeyPassword.toCharArray());
					sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

					logger.debug("Loaded identity and trust store");
				} else {
					sslContext.init(null, tmf.getTrustManagers(), null);
					logger.debug("Loaded trust store only");
				}

				SSLContext.setDefault(sslContext);
				
				SSLSocketFactory sslSf = sslContext.getSocketFactory();
				
				// We prepare a URLConnection 
				String urlPath = serviceEndPoint.concat(relativePath);
				URL url = new URL(urlPath);
				urlConnection = url.openConnection();
				// Before actually opening the sockets, we affect the SSLSocketFactory
				HttpsURLConnection httpsUrlConnection = (HttpsURLConnection) urlConnection;
				httpsUrlConnection.setSSLSocketFactory(sslSf);
				
				httpsUrlConnection.setDoOutput(true);
				httpsUrlConnection.setRequestProperty("Accept-Charset", "application/json");
				httpsUrlConnection.setRequestProperty("Content-Type", "application/json");
				
				logger.info("StaticDataServiceClient request a {} : {}", url, input);
				os = httpsUrlConnection.getOutputStream();
				os.write(input.getBytes());
				is = httpsUrlConnection.getInputStream();
				
				jsonString = ResponseReaderService.retrieveResponse(is);
//				System.out.println("jsonString è = " + jsonString);
				logger.info("StaticDataServiceClient response: " + jsonString);
				
			}else{
				String urlPath = serviceEndPoint.concat(relativePath);
//				logger.info("UrlPath chiamata servizio : " + urlPath);
				
				URL url = new URL(urlPath);
				urlConnection = url.openConnection();
				urlConnection.setDoOutput(true);
				urlConnection.setRequestProperty("Accept-Charset", "application/json");
				urlConnection.setRequestProperty("Content-Type", "application/json");
	
				logger.info("StaticDataServiceClient request a {} : {}", url, input);
				os = urlConnection.getOutputStream();
				os.write(input.getBytes());
				is = urlConnection.getInputStream();
	
				jsonString = ResponseReaderService.retrieveResponse(is);
//				System.out.println("jsonString è = " + jsonString);
				logger.info("StaticDataServiceClient response: " + jsonString);
				
			}

		} catch (java.net.MalformedURLException e) {
			logger.error("Error to initialize StaticDataServiceClientRest, invalid endpoint = {}", this.serviceEndpoint);
			throw new WSClientException("Error to initialize StaticDataServiceClientRest", e);
		} catch (Exception e) {
			logger.error("Error to initialize StaticDataServiceClientRest: ", e);
			throw new WSClientException("Error to initialize StaticDataServiceClientRest", e);
		} finally {
			if (fisIdentitystore != null) {
				try {
					fisIdentitystore.close();
				} catch (IOException e) {
					logger.error("Error to close input stream", e);
				}
			}

			if (fisTruststore != null) {
				try {
					fisTruststore.close();
				} catch (IOException e) {
					logger.error("Error to close input stream", e);
				}
			}
		}

		return jsonString;
	}

}
