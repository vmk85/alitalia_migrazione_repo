package com.alitalia.aem.service.impl.converter.home.mmbancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryCouponData;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.Cabin;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.Coupons;

@Component(immediate=true, metatype=false)
@Service(value=CouponsToMmbAncillaryCouponData.class)
public class CouponsToMmbAncillaryCouponData implements Converter<Coupons, MmbAncillaryCouponData> {

	@Override
	public MmbAncillaryCouponData convert(Coupons source) {
		MmbAncillaryCouponData destination = null;

		if (source != null) {
			destination = new MmbAncillaryCouponData();

			destination.setBaggageAllowance(source.getX003CBaggageAllowanceX003EKBackingField());

			Cabin sourceCabin = source.getX003CCabinX003EKBackingField();
			if (sourceCabin != null)
				destination.setCabin(CabinEnum.fromValue(sourceCabin.value()));

			destination.setChild(source.isX003CIsChildX003EKBackingField());
			destination.setFareClass(source.getX003CFareClassX003EKBackingField());
			destination.setFlightId(source.getX003CFlightIdX003EKBackingField());
			destination.setFlightNumber(source.getX003CFlightNumberX003EKBackingField());
			destination.setInboundCouponNumber(source.getX003CInboundCouponNumberX003EKBackingField());
			destination.setInfant(source.isX003CIsInfantX003EKBackingField());
			destination.setInfantAccompanist(source.isX003CIsInfantAccompanistX003EKBackingField());
			destination.setNumber(source.getX003CNumberX003EKBackingField());
			destination.setPassengerNumber(source.getX003CPassengerNumberX003EKBackingField());
			destination.setSpecialPassenger(source.isX003CIsSpecialPassengerX003EKBackingField());
		}

		return destination;
	}

}
