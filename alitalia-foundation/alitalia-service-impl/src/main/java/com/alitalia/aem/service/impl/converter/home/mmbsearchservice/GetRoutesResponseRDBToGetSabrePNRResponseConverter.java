package com.alitalia.aem.service.impl.converter.home.mmbsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.GetSabrePNRResponse;
import com.alitalia.aem.ws.mmb.searchservice.xsd1.GetRoutesResponseRDB;

@Component(immediate=true, metatype=false)
@Service(value=GetRoutesResponseRDBToGetSabrePNRResponseConverter.class)
public class GetRoutesResponseRDBToGetSabrePNRResponseConverter implements Converter<GetRoutesResponseRDB, GetSabrePNRResponse> {

	@Override
	public GetSabrePNRResponse convert(GetRoutesResponseRDB source) {
		GetSabrePNRResponse destination = null;

		if (source != null) {
			destination = new GetSabrePNRResponse();

			destination.setPnr(source.getPnr().getValue());
			destination.setPnrSabre(source.getPnrSabre().getValue());
		}

		return destination;
	}

}
