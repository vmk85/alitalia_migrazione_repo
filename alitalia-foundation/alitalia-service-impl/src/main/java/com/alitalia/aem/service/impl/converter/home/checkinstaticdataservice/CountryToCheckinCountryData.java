package com.alitalia.aem.service.impl.converter.home.checkinstaticdataservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinCountryData;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd2.Country;

@Component(immediate=true, metatype=false)
@Service(value=CountryToCheckinCountryData.class)
public class CountryToCheckinCountryData implements Converter<Country, CheckinCountryData> {

	@Override
	public CheckinCountryData convert(Country source) {
		
		CheckinCountryData destination = new CheckinCountryData();
		destination.setId(source.getX003CIdX003EKBackingField());
		destination.setCountryName(source.getX003CCountryNameX003EKBackingField());
		destination.setCountryCode(source.getX003CCountryCodeX003EKBackingField());
		destination.setCountryISOCode(source.getX003CCountryISOCodeX003EKBackingField());
		
		return destination;
	}
}