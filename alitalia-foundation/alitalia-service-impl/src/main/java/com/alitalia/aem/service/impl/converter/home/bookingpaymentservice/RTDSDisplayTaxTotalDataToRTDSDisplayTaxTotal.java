package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionDisplayTaxTotalData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionDisplayTaxTotal;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=RTDSDisplayTaxTotalDataToRTDSDisplayTaxTotal.class)
public class RTDSDisplayTaxTotalDataToRTDSDisplayTaxTotal implements
		Converter<ResultTicketingDetailSolutionDisplayTaxTotalData, 
					ResultTicketingDetailSolutionDisplayTaxTotal> {

	@Override
	public ResultTicketingDetailSolutionDisplayTaxTotal convert(ResultTicketingDetailSolutionDisplayTaxTotalData source) {
		ResultTicketingDetailSolutionDisplayTaxTotal destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionDisplayTaxTotal();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
