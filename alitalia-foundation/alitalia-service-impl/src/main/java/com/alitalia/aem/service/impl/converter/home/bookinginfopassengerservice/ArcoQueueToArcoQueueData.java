package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ArcoQueueData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ArcoQueue;

@Component(immediate=true, metatype=false)
@Service(value=ArcoQueueToArcoQueueData.class)
public class ArcoQueueToArcoQueueData 
		implements Converter<ArcoQueue, ArcoQueueData> {
	
	@Override
	public ArcoQueueData convert(ArcoQueue source) {
		
		ArcoQueueData destination = null;
		
		if (source != null) {
			
			destination = new ArcoQueueData();
			
			if (source.getPseudoCityCode() != null) {
				destination.setPseudoCityCode(
						source.getPseudoCityCode().getValue());
			}
			
			destination.setQueueNumber(
					source.getQueueNumber());
			
		}
		
		return destination;
	}

}
