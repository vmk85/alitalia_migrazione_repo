package com.alitalia.aem.service.impl.home;

import com.alitalia.aem.ws.statistics.statisticsservice.SabreDcStatisticsServiceClient;
import com.day.cq.wcm.api.Page;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.RegisterStatisticsRequest;
import com.alitalia.aem.common.messages.home.RegisterStatisticsResponse;
import com.alitalia.aem.service.api.home.RegisterStatisticsService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.statisticsservice.RegisterStatisticsRequestToStatisticsRequest;
import com.alitalia.aem.service.impl.converter.home.statisticsservice.StatisticsResponseToRegisterStatisticsResponse;
import com.alitalia.aem.ws.statistics.statisticsservice.StatisticsServiceClient;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd1.StatisticsRequest;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd1.StatisticsResponse;

@Service
@Component(immediate=true, metatype=false)
public class SimpleRegisterStatisticsService implements RegisterStatisticsService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleRegisterStatisticsService.class);

	@Reference
	private StatisticsServiceClient statisticsServiceClient;
	
	@Reference
	private RegisterStatisticsRequestToStatisticsRequest registerStatisticsRequestConverter;
	
	@Reference
	private StatisticsResponseToRegisterStatisticsResponse registerStatisticsResponseConverter;

	/** Migrazione 3.6 inizio*/

	@Reference
	private ResourceResolverFactory resolverFactory;

	@Reference
	private SabreDcStatisticsServiceClient sabreDcStatisticsServiceClient;

	/** Migrazione 3.6 fine*/

	@Override
	public RegisterStatisticsResponse registerStatistics(RegisterStatisticsRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method registerStatistics. The request is {}", request);
		}
		

		try {
			
			RegisterStatisticsResponse response = new RegisterStatisticsResponse();
			StatisticsRequest statisticsRequest = registerStatisticsRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			StatisticsResponse statisticsResponse = null;
			if (newSabreEnable(statisticsRequest.getSiteCode().getValue().toLowerCase())){
				statisticsResponse = sabreDcStatisticsServiceClient.registerStatistic(statisticsRequest);
			} else {
				statisticsResponse = statisticsServiceClient.registerStatistic(statisticsRequest);
			}

			/** Migrazione 3.6 fine*/

//			StatisticsResponse statisticsResponse = statisticsServiceClient.registerStatistic(statisticsRequest);
			response = registerStatisticsResponseConverter.convert(statisticsResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
			
			return response;
			
		} catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing registerStatistics: Sid ["+request.getSid()+"]" , e);
		}
	}

	/** Migrazione 3.6 inizio*/

	private boolean newSabreEnable(String market) throws LoginException {

		ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
		Resource res = resourceResolver.getResource("/content/alitalia/alitalia-" + market);
		Page page = res.adaptTo(Page.class);
		ValueMap props = page.getProperties();
		String title = props.get("isNewService", "default value");
		logger.info("dato preso dalla pagina: " + title);
		return Boolean.parseBoolean(title);

	}

	/** Migrazione 3.6 fine*/

}