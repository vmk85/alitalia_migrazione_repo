package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.MilleMigliaLoginSAResponse;
import com.alitalia.aem.common.messages.home.SubscribeSARequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfileSA;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

//import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;

@Component(immediate = true, metatype = false)
@Service(value = SubscribeSAResponseToCustomerResponse.class)
public class SubscribeSAResponseToCustomerResponse implements Converter<
		com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SubscribeSAResponse,
		com.alitalia.aem.common.messages.home.SubscribeSAResponse> {

	@Reference
	private MMCustomerProfileDataToCustomerProfileSA mmCustomerProfileDataConverter;

	@Override
	public com.alitalia.aem.common.messages.home.SubscribeSAResponse convert(com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SubscribeSAResponse source) {
		com.alitalia.aem.common.messages.home.SubscribeSAResponse  destination = null;

		if (source != null) {

			destination = new com.alitalia.aem.common.messages.home.SubscribeSAResponse();

			if (source.getIDProfilo() != null) {
				destination.setProfileID(
					source.getIDProfilo().getValue()
				);
			}
		}

		return destination;
	}
}