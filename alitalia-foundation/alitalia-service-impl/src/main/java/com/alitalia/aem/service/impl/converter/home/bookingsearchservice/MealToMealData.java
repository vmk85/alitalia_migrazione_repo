package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MealData;
import com.alitalia.aem.ws.booking.searchservice.xsd3.Meal;

@Component(immediate=true, metatype=false)
@Service(value=MealToMealData.class)
public class MealToMealData implements Converter<Meal, MealData> {

	@Override
	public MealData convert(Meal source) {
		
		MealData destination = null;
		
		if (source != null) {
			
			destination = new MealData();
			
			if (source.getCode() != null) {
				destination.setCode(
						source.getCode().getValue());
			}
			
			if (source.getDescription() != null) {
				destination.setDescription(
						source.getDescription().getValue());
			}
			
		}
		
		return destination;
	}

}
