package com.alitalia.aem.service.impl.converter.home.flightstatusservice;

import java.util.Calendar;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.DestinationData;
import com.alitalia.aem.common.data.home.DestinationDetailsData;
import com.alitalia.aem.common.data.home.FlightInfoData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.ArrivalAirport;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.ArrivalDetails;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.DepartureAirport;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.DepartureDetails;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.FlightInfoModel;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd4.ObjectFactory;

@Component(immediate = true, metatype = false)
@Service(value = FlightInfoDataToFlightInfoModel.class)
public class FlightInfoDataToFlightInfoModel implements Converter<FlightInfoData, FlightInfoModel> {

	@Override
	public FlightInfoModel convert(FlightInfoData source) {
		
	FlightInfoModel flightInfoModel = null;
	if(source!=null){
		flightInfoModel = new FlightInfoModel();
		
		Calendar sourceArrivalDateTime = source.getArrivalDateTime();
		XMLGregorianCalendar arrivalTime = null;
		if (sourceArrivalDateTime != null)
			arrivalTime = XsdConvertUtils.toXMLGregorianCalendar(sourceArrivalDateTime);

		DestinationData arrivalAirportData = null;
		if(source.getArrivalAirport()!=null)
			 arrivalAirportData = source.getArrivalAirport();
		
		String arrivalCodeContext = null;
		String arrivalLocationCode = null;
		if(arrivalAirportData!=null){
			arrivalCodeContext = arrivalAirportData.getCodeContext();
			arrivalLocationCode = arrivalAirportData.getLocationCode();
		}
		
		DestinationDetailsData arrivalDetailsData = null;
		if(source.getArrivalDetails()!=null)
			arrivalDetailsData = source.getArrivalDetails();
		
		String arrivalCodeStatus = null;
		String arrivalOpertiveInfo = null;
		if(arrivalDetailsData!=null){
			arrivalCodeStatus = arrivalDetailsData.getCodeStatus();
			arrivalOpertiveInfo = arrivalDetailsData.getOpertiveInfo();
		}
		
		Calendar sourceArrivalGapTime = source.getArrivalGapTime();
		XMLGregorianCalendar arrivalGapTime = null;
		if (sourceArrivalGapTime != null)
			arrivalGapTime = XsdConvertUtils.toXMLGregorianCalendar(sourceArrivalGapTime);
		
		Calendar sourceDepartureDateTime = source.getDepartureDateTime();
		XMLGregorianCalendar departureTime = null;
		if (sourceDepartureDateTime != null)
			departureTime = XsdConvertUtils.toXMLGregorianCalendar(sourceDepartureDateTime);

		String departureCodeContext = null;
		String departureLocationCode = null;
		if(source.getDepartureAirport() != null) {
			DestinationData departureAirportData = source.getDepartureAirport();
				
			if(departureAirportData.getCodeContext()!=null)
				departureCodeContext = departureAirportData.getCodeContext();
		
			if(departureAirportData.getLocationCode()!=null)
				departureLocationCode = departureAirportData.getLocationCode();
		}
		
		String departureCodeStatus = null;
		String departureOpertiveInfo = null;
		if(source.getDepartureDetails() != null) {
			DestinationDetailsData departureDetailsData = source.getDepartureDetails();

			if (departureDetailsData.getCodeStatus() != null)
				departureCodeStatus = departureDetailsData.getCodeStatus();

			if (departureDetailsData.getOpertiveInfo() != null)
				departureOpertiveInfo = departureDetailsData.getOpertiveInfo();
		}
		
		Calendar sourceDepartureGapTime = source.getDepartureGapTime();
		XMLGregorianCalendar departureGapTime = null;
		if (sourceDepartureGapTime != null)
			departureGapTime = XsdConvertUtils.toXMLGregorianCalendar(sourceDepartureGapTime);

		String errorDescription = null;
		if(source.getErrorDescription()!=null)
			errorDescription = source.getErrorDescription();
		
		String flightNumber = null;
		if(source.getFlightNumber()!=null)
			flightNumber = source.getFlightNumber();
		
		String rescheduled = null;
		if(source.getRescheduled()!=null)
			rescheduled = source.getRescheduled();
		
		Integer sequenceNumber = null;
		if(source.getSequenceNumber()!=null)
			sequenceNumber = source.getSequenceNumber();
		
		String vector = null;
		if(source.getVector()!=null)
			vector = source.getVector();
		
		ObjectFactory objectFactory = new ObjectFactory();
		ArrivalAirport arrivalAirport = new ArrivalAirport();
		arrivalAirport.setCodeContext(objectFactory.createAirportCodeContext(arrivalCodeContext));
		arrivalAirport.setLocationCode(objectFactory.createAirportLocationCode(arrivalLocationCode));
		com.alitalia.aem.ws.booking.flightstatusservice.xsd3.ObjectFactory objectFactory2 = new com.alitalia.aem.ws.booking.flightstatusservice.xsd3.ObjectFactory();
		JAXBElement<ArrivalAirport> jaxbArrivalAirport = objectFactory2.createFlightInfoModelArrivalAirport(arrivalAirport);
		
		ArrivalDetails arrivalDetails = new ArrivalDetails();
		arrivalDetails.setCodeStatus(objectFactory2.createDetailsCodeStatus(arrivalCodeStatus));
		arrivalDetails.setOpertiveInfo(objectFactory2.createDetailsOpertiveInfo(arrivalOpertiveInfo));
		JAXBElement<ArrivalDetails> jaxbArrivalDetails = objectFactory2.createFlightInfoModelArrivalDetails(arrivalDetails);
		
		DepartureAirport departureAirport = new DepartureAirport();
		departureAirport.setCodeContext(objectFactory.createAirportCodeContext(departureCodeContext));
		departureAirport.setLocationCode(objectFactory.createAirportLocationCode(departureLocationCode));
		JAXBElement<DepartureAirport> jaxbDepartureAirport = objectFactory2.createFlightInfoModelDepartureAirport(departureAirport);
		
		DepartureDetails departureDetails = new DepartureDetails();
		departureDetails.setCodeStatus(objectFactory2.createDetailsCodeStatus(departureCodeStatus));
		departureDetails.setOpertiveInfo(objectFactory2.createDetailsOpertiveInfo(departureOpertiveInfo));
		JAXBElement<DepartureDetails> jaxbDepartureDetails = objectFactory2.createFlightInfoModelDepartureDetails(departureDetails);

		
		flightInfoModel.setArrivalDateTime(arrivalTime);
		flightInfoModel.setArrivalAirport(jaxbArrivalAirport);
		flightInfoModel.setArrivalDetails(jaxbArrivalDetails);
		flightInfoModel.setArrivalGapTime(arrivalGapTime);
		
		flightInfoModel.setDepartureDateTime(departureTime);
		flightInfoModel.setDepartureAirport(jaxbDepartureAirport);
		flightInfoModel.setDepartureDetails(jaxbDepartureDetails);
		flightInfoModel.setDepartureGapTime(departureGapTime);
		
		flightInfoModel.setErrorDescription(objectFactory2.createFlightInfoModelErrorDescription(errorDescription));
		flightInfoModel.setFlightNumber(objectFactory2.createFlightInfoModelFlightNumber(flightNumber));
		flightInfoModel.setRescheduled(objectFactory2.createFlightInfoModelRescheduled(rescheduled));
		flightInfoModel.setSequenceNumber(sequenceNumber);
		flightInfoModel.setVector(objectFactory2.createFlightInfoModelVector(vector));
		}
		return flightInfoModel;
	}
}