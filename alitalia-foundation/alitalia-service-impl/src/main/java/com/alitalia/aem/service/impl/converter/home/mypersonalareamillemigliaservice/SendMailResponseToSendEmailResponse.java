package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.home.MillemigliaSendEmailResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SendMailResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;

@Component(immediate = true, metatype = false)
@Service(value = SendMailResponseToSendEmailResponse.class)
public class SendMailResponseToSendEmailResponse implements Converter<SendMailResponse, MillemigliaSendEmailResponse> {

	@Reference
	private CustomerProfileToMMCustomerProfileData customerProfileConverter;
	
	@Override
	public MillemigliaSendEmailResponse convert(SendMailResponse source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		MillemigliaSendEmailResponse destination = new MillemigliaSendEmailResponse(); 
		
		JAXBElement<CustomerProfile> jaxbElement = source.getCustomer();
		CustomerProfile customerProfile = jaxbElement.getValue();
		if(customerProfile != null){
			MMCustomerProfileData mmCustomerProfile = customerProfileConverter.convert(customerProfile);
			destination.setCustomerProfile(mmCustomerProfile);
		}
		destination.setMailSent(source.isMailSent());
		
		return destination;
	}
}