package com.alitalia.aem.service.impl.converter.home.commonservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.SeatMapRowData;
import com.alitalia.aem.common.data.home.SeatMapSectorData;
import com.alitalia.aem.ws.booking.commonservice.xsd7.SeatMapRow;
import com.alitalia.aem.ws.booking.commonservice.xsd7.SeatMapSector;

@Component(immediate=true, metatype=false)
@Service(value=SeatMapSectorToSeatMapSectorData.class)
public class SeatMapSectorToSeatMapSectorData 
		implements Converter<SeatMapSector, SeatMapSectorData> {

	@Reference
	private SeatMapRowToSeatMapRowData seatMapRowConverter;
	
	@Override
	public SeatMapSectorData convert(SeatMapSector source) {
		
		SeatMapSectorData destination = null;
		
		if (source != null) {
		
			destination = new SeatMapSectorData();
			
			destination.setStartingRow(
					source.getX003CStartingRowX003EKBackingField());
			
			destination.setEndingRow(
					source.getX003CEndingRowX003EKBackingField());
			
			List<SeatMapRowData> seatMapRows = new ArrayList<SeatMapRowData>();
			if (source.getX003CSeatMapRowsX003EKBackingField() != null &&
					source.getX003CSeatMapRowsX003EKBackingField().getSeatMapRow() != null) {
				for (SeatMapRow seatMapRow : source.getX003CSeatMapRowsX003EKBackingField().getSeatMapRow()) {
					seatMapRows.add(seatMapRowConverter.convert(seatMapRow));
				}
			}
			destination.setSeatMapRows(seatMapRows);
			
		}
		
		return destination;
		
	}

}
