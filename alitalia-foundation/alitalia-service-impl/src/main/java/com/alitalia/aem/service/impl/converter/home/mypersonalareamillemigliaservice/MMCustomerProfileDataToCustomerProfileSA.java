package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.*;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.*;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd3.ArrayOfanyType;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import java.util.List;

@Component(immediate = true, metatype = false)
@Service(value = MMCustomerProfileDataToCustomerProfileSA.class)
public class MMCustomerProfileDataToCustomerProfileSA implements Converter<MMCustomerProfileData, CustomerProfileSA> {

	@Reference
	private MMActivityDataToActivity activityDataToActivityConverter;

	@Reference
	private MMProgramDataToProgram programDataToProgramConverter;

	@Reference
	private MMTelephoneDataToTelephone telephoneDataToTelephoneConverter;

	@Reference
	private MMPreferenceDataToPreference preferenceDataToPreferenceConverter;

	@Reference
	private MMMessageDataToMessage messageDataToMessageConverter;

	@Reference
	private MMLifestyleDataToLifestyle lifestyleDataToLifestyleConverter;

	@Reference
	private MMFlightDataToFlight flightDataToFlightConverter;

	@Reference
	private MMAddressDataToAddress addressDataToAddressConverter;

	@Reference
	private MMCreditCardDataToCreditCard creditCardDataToCreditCardConverter;

	@Reference
	private MMContractDataToContract contractDataToContractConverter;

	@Override
	public CustomerProfileSA convert(MMCustomerProfileData source) {
		CustomerProfileSA destination = null;
		
		if (source != null) {
			com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ObjectFactory objectFactory2 = new com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ObjectFactory();
			com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd3.ObjectFactory objectFactory3 = new com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd3.ObjectFactory();

			CustomerProfile customerprofile = objectFactory2.createCustomerProfile();
			destination = objectFactory2.createCustomerProfileSA();
			destination.setCustomerProfile(objectFactory2.createCustomerProfile(customerprofile));
			destination.getCustomerProfile().getValue().setContratcType(objectFactory2.createCustomerProfileContratcType(source.getContratcType()));
			destination.getCustomerProfile().getValue().setEmail(objectFactory2.createCustomerProfileEmail(source.getEmail()));
			destination.getCustomerProfile().getValue().setFiscalCode(objectFactory2.createCustomerProfileFiscalCode(source.getFiscalCode()));
			destination.getCustomerProfile().getValue().setLanguage(objectFactory2.createCustomerProfileLanguage(source.getLanguage()));
			destination.getCustomerProfile().getValue().setMailingType(objectFactory2.createCustomerProfileMailingType(source.getMailingType()));
			destination.getCustomerProfile().getValue().setMarket(objectFactory2.createCustomerProfileMarket(source.getMarket()));
			destination.getCustomerProfile().getValue().setDefaultAddressCountry(objectFactory2.createCustomerProfileDefaultAddressCountry(source
					.getDefaultAddressCountry()));
			destination.getCustomerProfile().getValue().setDefaultAddressMunicipalityName(objectFactory2
					.createCustomerProfileDefaultAddressMunicipalityName(source.getDefaultAddressMunicipalityName()));
			destination.getCustomerProfile().getValue().setDefaultAddressPostalCode(objectFactory2.createCustomerProfileDefaultAddressPostalCode(source
					.getDefaultAddressPostalCode()));
			destination.getCustomerProfile().getValue().setDefaultAddressStateCode(objectFactory2.createCustomerProfileDefaultAddressStateCode(source
					.getDefaultAddressStateCode()));
			destination.getCustomerProfile().getValue().setDefaultAddressStreetFreeText(objectFactory2
					.createCustomerProfileDefaultAddressStreetFreeText(source.getDefaultAddressStreetFreeText()));
			destination.getCustomerProfile().getValue().setCustomerName(objectFactory2.createCustomerProfileCustomerName(source.getCustomerName()));
			destination.getCustomerProfile().getValue().setCustomerNickName(objectFactory2.createCustomerProfileCustomerNickName(source
					.getCustomerNickName()));
			destination
					.getCustomerProfile().getValue().setCustomerNumber(objectFactory2.createCustomerProfileCustomerNumber(source.getCustomerNumber()));
			destination.getCustomerProfile().getValue().setCustomerPinCode(objectFactory2.createCustomerProfileCustomerPinCode(source
					.getCustomerPinCode()));
			destination.getCustomerProfile().getValue().setCustomerSurname(objectFactory2.createCustomerProfileCustomerSurname(source
					.getCustomerSurname()));

			///////////////////////
			destination.setPassword(objectFactory2.createCustomerProfileSAPassword(source.getPassword()));
			destination.setUsername(objectFactory2.createCustomerProfileSAUsername(source.getUsername()));
			destination.setSecureAnswer(objectFactory2.createCustomerProfileSASecureAnswer(source.getRispostaSegreta()));
			destination.setSecureQuestionID(source.getIdDomandaSegreta());

			destination.setLockedout(source.getFlagLockedAccount());
			destination.setFlagOldCustomer(source.getFlagOldCustomer());
			destination.setFlagRememberPassword(source.getFlagRememberPassword()); //FlagRememberPassword
//			destination.setFlagSocialCertified(source.getFlagSocialCertified()); //FlagSocialCertified
			destination.setFlagVerificaPassword(source.getFlagVerificaPassword()); //FlagSocialCertified
			destination.setFlagVerificaRisposta(source.getFlagVerificaRisposta()); //FlagSocialCertified
			destination.setFlagVerificaUsername(source.getFlagVerificaUserName()); //FlagSocialCertified

			destination.setEmailCertificateFlag(source.getEmailCertificateFlag()); //FlagSocialCertified
			destination.setCertifiedCountryNumber(objectFactory2.createCustomerProfileSACertifiedCountryNumber(source.getCertifiedCountryNumber())); //FlagSocialCertified
			destination.setCertifiedEmailAccount(objectFactory2.createCustomerProfileSACertifiedEmailAccount(source.getCertifiedEmailAccount()));
			destination.setCertifiedPhoneAccount(objectFactory2.createCustomerProfileSACertifiedPhoneAccount(source.getCertifiedPhoneAccount()));
			destination.setEmailCertificateFlag(source.getEmailCertificateFlag());
			destination.setAccessFailureCount(source.getAccessFailureCount());

			destination.setPhoneCertificateFlag(source.getPhoneCertificateFlag());
//			destination.setSocialToken(objectFactory2.createCustomerProfileSocialToken(source.getSocialToken()));
//			destination.setSocialUserId(objectFactory2.createCustomerProfileSocialUserId(source.getSocialUserId()));


			////// verificare qui /////////////////////


			//////////////////////

			destination.getCustomerProfile().getValue().setContractAgree(source.getContractAgree());
			destination.getCustomerProfile().getValue().setPointsEarnedPartial(source.getPointsEarnedPartial());
			destination.getCustomerProfile().getValue().setPointsEarnedTotal(source.getPointsEarnedTotal());
			destination.getCustomerProfile().getValue().setPointsEarnedTotalQualified(source.getPointsEarnedTotalQualified());
			destination.getCustomerProfile().getValue().setPointsRemainingPartial(source.getPointsRemainingPartial());
			destination.getCustomerProfile().getValue().setPointsRemainingTotal(source.getPointsRemainingTotal());
			destination.getCustomerProfile().getValue().setPointsSpentPartial(source.getPointsSpentPartial());
			destination.getCustomerProfile().getValue().setPointsSpentTotal(source.getPointsSpentTotal());
			destination.getCustomerProfile().getValue().setProfiling(source.getProfiling());
			destination.getCustomerProfile().getValue().setQualifyingSegment(source.getQualifyingSegment());
			destination.getCustomerProfile().getValue().setSmsAuthorization(source.getSmsAuthorization());

			destination.getCustomerProfile().getValue().setUserIdRequest(objectFactory2.createCustomerProfileUserIdRequest(source.getUserIdRequest()));
			destination
					.getCustomerProfile().getValue().setWebPageRequest(objectFactory2.createCustomerProfileWebPageRequest(source.getWebPageRequest()));
			destination.getCustomerProfile().getValue().setStatusCode(objectFactory2.createCustomerProfileStatusCode(source.getStatusCode()));
			destination.getCustomerProfile().getValue().setProfessionalName(objectFactory2.createCustomerProfileProfessionalName(source
					.getProfessionalName()));

			if (source.getPassportDate() != null)
				destination.getCustomerProfile().getValue().setPassportDate(XsdConvertUtils.toXMLGregorianCalendar(source.getPassportDate()));
			if (source.getBirthDate() != null)
				destination.getCustomerProfile().getValue().setBirthDate(XsdConvertUtils.toXMLGregorianCalendar(source.getBirthDate()));
			if (source.getTierDate() != null)
				destination.getCustomerProfile().getValue().setTierDate(XsdConvertUtils.toXMLGregorianCalendar(source.getTierDate()));

			destination.getCustomerProfile().getValue().setGender(source.getGender() != null ? GenderTypes.fromValue(source.getGender().value()) : null);
			destination.getCustomerProfile().getValue().setSystemChannel(source.getSystemChannel() != null ? SystemChannelTypes.fromValue(source.getSystemChannel().value()) : null);
			destination.getCustomerProfile().getValue().setTierCode(source.getTierCode() != null ? TierCodeTypes.fromValue(source.getTierCode().value()) : null);
			destination.getCustomerProfile().getValue().setRegistrationRequired(source.getRegistrationRequired() != null ? RegistrationTypes.fromValue(source.getRegistrationRequired().value()) : null);
			destination.getCustomerProfile().getValue().setMaritalStatus(source.getMaritalStatus() != null ? MaritalStatusTypes.fromValue(source.getMaritalStatus().value()) : null);
			destination.getCustomerProfile().getValue().setCustomerTitle(source.getCustomerTitle() != null ? TitleTypes.fromValue(source.getCustomerTitle().value()) : null);
			destination.getCustomerProfile().getValue().setCustomerWorkPosition(source.getCustomerWorkPosition() != null ? WorkPositionTypes.fromValue(source.getCustomerWorkPosition().value()) : null);
			destination.getCustomerProfile().getValue().setDefaultAddressType(source.getDefaultAddressType() != null ? AddressTypes.fromValue(source.getDefaultAddressType().value()) : null);

			List<MMActivityData> sourceActivityData = source.getActivities();
			ArrayOfanyType activitiesList = objectFactory3.createArrayOfanyType();
			if (sourceActivityData != null) {
				for (MMActivityData mmActivityData : sourceActivityData) {
					Activity activity = activityDataToActivityConverter.convert(mmActivityData);
					activitiesList.getAnyType().add(activity);
				}
			}
			destination.getCustomerProfile().getValue().setActivities(objectFactory2.createCustomerProfileActivities(activitiesList));

			List<MMProgramData> sourceProgramData = source.getPrograms();
			ArrayOfanyType programsList = objectFactory3.createArrayOfanyType();
			if (sourceProgramData != null) {
				for (MMProgramData mmProgramData : sourceProgramData) {
					Program program = programDataToProgramConverter.convert(mmProgramData);
					programsList.getAnyType().add(program);
				}
			}
			destination.getCustomerProfile().getValue().setPrograms(objectFactory2.createCustomerProfilePrograms(programsList));

			List<MMTelephoneData> sourceTelephoneData = source.getTelephones();
			ArrayOfanyType telephonesList = objectFactory3.createArrayOfanyType();
			if (sourceTelephoneData != null) {
				for (MMTelephoneData mmTelephoneData : sourceTelephoneData) {
					if(mmTelephoneData != null){
						Telephone telephone = telephoneDataToTelephoneConverter.convert(mmTelephoneData);
						telephonesList.getAnyType().add(telephone);
					}
				}
			}
			destination.getCustomerProfile().getValue().setTelephones(objectFactory2.createCustomerProfileTelephones(telephonesList));

			List<MMPreferenceData> sourcePreferenceData = source.getPreferences();
			ArrayOfanyType preferencesList = objectFactory3.createArrayOfanyType();
			if (sourcePreferenceData != null) {
				for (MMPreferenceData mmPreferenceData : sourcePreferenceData) {
					Preference preference = preferenceDataToPreferenceConverter.convert(mmPreferenceData);
					preferencesList.getAnyType().add(preference);
				}
			}
			destination.getCustomerProfile().getValue().setPreferences(objectFactory2.createCustomerProfilePreferences(preferencesList));

			List<MMMessageData> sourceMessageData = source.getMessages();
			ArrayOfanyType messagesList = objectFactory3.createArrayOfanyType();
			if (sourceMessageData != null) {
				for (MMMessageData mmMessageData : sourceMessageData) {
					Message message = messageDataToMessageConverter.convert(mmMessageData);
					messagesList.getAnyType().add(message);
				}
			}
			destination.getCustomerProfile().getValue().setMessages(objectFactory2.createCustomerProfileMessages(messagesList));

			List<MMLifestyleData> sourceLifestyleData = source.getLifestyles();
			ArrayOfanyType lifestylesList = objectFactory3.createArrayOfanyType();
			if (sourceLifestyleData != null) {
				for (MMLifestyleData mmLifestyleData : sourceLifestyleData) {
					Lifestyle lifestyle = lifestyleDataToLifestyleConverter.convert(mmLifestyleData);
					lifestylesList.getAnyType().add(lifestyle);
				}
			}
			destination.getCustomerProfile().getValue().setLifestyles(objectFactory2.createCustomerProfileLifestyles(lifestylesList));

			List<MMFlightData> sourceFlightData = source.getFlight();
			ArrayOfanyType flightsList = objectFactory3.createArrayOfanyType();
			if (sourceFlightData != null) {
				for (MMFlightData mmFlightData : sourceFlightData) {
					Flight flight = flightDataToFlightConverter.convert(mmFlightData);
					flightsList.getAnyType().add(flight);
				}
			}
			destination.getCustomerProfile().getValue().setFlights(objectFactory2.createCustomerProfileFlights(flightsList));

			List<MMAddressData> sourceAddressData = source.getAddresses();
			ArrayOfanyType addressesList = objectFactory3.createArrayOfanyType();
			if (sourceAddressData != null) {
				for (MMAddressData mmAddressData : sourceAddressData) {
					Address address = addressDataToAddressConverter.convert(mmAddressData);
					addressesList.getAnyType().add(address);
				}
			}
			destination.getCustomerProfile().getValue().setAddresses(objectFactory2.createCustomerProfileAddresses(addressesList));

			List<MMCreditCardData> sourceCreditCardData = source.getCreditCards();
			ArrayOfanyType creditCardsList = objectFactory3.createArrayOfanyType();
			if (sourceCreditCardData != null) {
				for (MMCreditCardData mmCreditCardData : sourceCreditCardData) {
					CreditCard creditCard = creditCardDataToCreditCardConverter.convert(mmCreditCardData);
					creditCardsList.getAnyType().add(creditCard);
				}
			}
			destination.getCustomerProfile().getValue().setCreditCards(objectFactory2.createCustomerProfileCreditCards(creditCardsList));

			List<MMContractData> sourceContractData = source.getContracts();
			ArrayOfanyType contractsList = objectFactory3.createArrayOfanyType();
			if (sourceContractData != null) {
				for (MMContractData mmContractData : sourceContractData) {
					Contract contract = contractDataToContractConverter.convert(mmContractData);
					contractsList.getAnyType().add(contract);
				}
			}
			destination.getCustomerProfile().getValue().setContracts(objectFactory2.createCustomerProfileContracts(contractsList));

		}

		return destination;
	}

}
