package com.alitalia.aem.service.impl.crmdatarest.utils;

import com.alitalia.aem.common.messages.crmdatarest.GetCrmDataInfoResponse;
import com.alitalia.aem.common.messages.crmdatarest.SetCrmDataInfoResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.crmdatarest.SetCRMDataNewsLetterResponse;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class CRMDataMessageUnmarshal {
    
    private static final Logger logger = LoggerFactory.getLogger(CRMDataMessageUnmarshal.class);
    
    public static SetCRMDataNewsLetterResponse setCRMDataUnmarshal(String jsonString) {
        SetCRMDataNewsLetterResponse setCRMDataResponse = null;
        String errorObj = "";
        Gson gson = new Gson();
        try {
            if (jsonString.contains("isError")) {
                errorObj = jsonString;
                setCRMDataResponse = new SetCRMDataNewsLetterResponse();
                setCRMDataResponse.setError(errorObj);
            }
            else {
				logger.debug("CRMDataMessageUnmarshal jsonString : " + jsonString);
                setCRMDataResponse = gson.fromJson(jsonString, SetCRMDataNewsLetterResponse.class);
            }
        }
        catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        
        return setCRMDataResponse;
    }



    public static SetCrmDataInfoResponse setCRMDataInfoUnmarshal(String jsonString) {
        SetCrmDataInfoResponse setCRMDataResponse = null;
        String errorObj = "";
        Gson gson = new Gson();
        try {
            if (jsonString.contains("isError")) {
                errorObj = jsonString;
                setCRMDataResponse = new SetCrmDataInfoResponse();
                setCRMDataResponse.setError(errorObj);
            }
            else {
				logger.debug("CRMDataMessageUnmarshal jsonString : " + jsonString);
                setCRMDataResponse = gson.fromJson(jsonString, SetCrmDataInfoResponse.class);
            }
        }
        catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }

        return setCRMDataResponse;
    }

    public static GetCrmDataInfoResponse getCRMDataInfoUnmarshal(String jsonString) {
        GetCrmDataInfoResponse getCRMDataResponse = null;
        Gson gson = new Gson();
        try {
            if (jsonString.contains("isError")) {
                getCRMDataResponse = new GetCrmDataInfoResponse();
            }
            else {
                logger.debug("CRMDataMessageUnmarshal jsonString : " + jsonString);
                getCRMDataResponse = gson.fromJson(jsonString, GetCrmDataInfoResponse.class);
            }
        }
        catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }

        return getCRMDataResponse;
    }
}
