package com.alitalia.aem.service.impl.converter.home.mmbcommonservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.MmbFrequentFlyerTypeData;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.FrequentFlyerType;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=MmbFrequentFlyerTypeDataToFrequentFlyerType.class)
public class MmbFrequentFlyerTypeDataToFrequentFlyerType implements Converter<MmbFrequentFlyerTypeData, FrequentFlyerType> {

	@Override
	public FrequentFlyerType convert(MmbFrequentFlyerTypeData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		FrequentFlyerType destination = null;

		if (source != null) {
			destination = objectFactory.createFrequentFlyerType();

			destination.setX003CCodeX003EKBackingField(source.getCode());
			destination.setX003CDescriptionX003EKBackingField(source.getDescription());
			destination.setX003CIdX003EKBackingField(source.getId());
			destination.setX003CLenghtX003EKBackingField(source.getLenght());
			destination.setX003CPreFillCharX003EKBackingField(source.getPreFillChar());
			destination.setX003CRegularExpressionValidationX003EKBackingField(source.getRegularExpressionValidation());
		}

		return destination;
	}

}
