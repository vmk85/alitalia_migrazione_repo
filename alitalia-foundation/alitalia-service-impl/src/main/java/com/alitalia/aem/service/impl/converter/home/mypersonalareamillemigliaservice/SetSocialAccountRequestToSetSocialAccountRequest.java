package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SetSocialAccountRequest;

@Component(immediate = true, metatype = false)
@Service(value = SetSocialAccountRequestToSetSocialAccountRequest.class)
public class SetSocialAccountRequestToSetSocialAccountRequest 
		implements Converter<com.alitalia.aem.common.messages.home.SetSocialAccountRequest, SetSocialAccountRequest> {

	@Override
	public SetSocialAccountRequest convert(com.alitalia.aem.common.messages.home.SetSocialAccountRequest source) {
		SetSocialAccountRequest destination = null;

		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			destination = objectFactory.createSetSocialAccountRequest();
			
			destination.setGigyaId(
					objectFactory.createSetSocialAccountRequestGigyaId(
							source.getGigyaId()));
			
			destination.setUserCode(
					objectFactory.createSetSocialAccountRequestUserCode(
							source.getUserCode()));
			
			destination.setPin(
					objectFactory.createSetSocialAccountRequestPin(
							source.getPin()));
			
		}

		return destination;
	}

}
