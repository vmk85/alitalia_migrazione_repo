package com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.messages.home.RetrieveCreditCardCountriesResponse;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.Country;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd6.StaticDataResponse;

@Component(immediate = true, metatype = false)
@Service(value = StaticDataResponseToRetrieveCreditCardCountriesResponseConverter.class)
public class StaticDataResponseToRetrieveCreditCardCountriesResponseConverter implements Converter<StaticDataResponse, RetrieveCreditCardCountriesResponse> {

	private static final Logger logger = LoggerFactory.getLogger(StaticDataResponseToRetrieveCreditCardCountriesResponseConverter.class);

	@Reference
	private CountryToCountryData countryConverter;
	
	@Reference
	private BookingMMBStaticDataServiceJAXBContextFactory jaxbContextFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public RetrieveCreditCardCountriesResponse convert(StaticDataResponse source) {
		RetrieveCreditCardCountriesResponse destination = new RetrieveCreditCardCountriesResponse();
		try {
			List<CountryData> countriesData = new ArrayList<>();
			
			Unmarshaller countryUnmashaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			JAXBElement<Country> unmarshalledObj = null;

			List<Object> countries = source.getEntities().getValue().getAnyType();
			for(Object country: countries){
				Country castedCountry = null;
				if (country instanceof Country) {
					castedCountry = (Country) country;
				} else {
					unmarshalledObj = (JAXBElement<Country>) countryUnmashaller.unmarshal((Node) country);
					castedCountry = unmarshalledObj.getValue();
				}
				CountryData countryData = countryConverter.convert(castedCountry);
				countriesData.add(countryData);
			}
			
			destination.setCountries(countriesData);
			
		} catch (JAXBException e) {
			logger.error("Error instantiating JAXBContext/Unmarshaller: {}", e);
		} finally {
			logger.info("Conversion executed successfully");
		}
		return destination;			
	}
}