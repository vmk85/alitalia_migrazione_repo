package com.alitalia.aem.service.impl.converter.home.checkinpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinComfortSeatInfoData;
import com.alitalia.aem.ws.checkin.paymentservice.xsd2.ComfortSeatInfo;
import com.alitalia.aem.ws.checkin.paymentservice.xsd2.ObjectFactory;


@Component(immediate=true, metatype=false)
@Service(value=CheckinComfortSeatInfoDataToComfortSeatInfo.class)
public class CheckinComfortSeatInfoDataToComfortSeatInfo implements Converter<CheckinComfortSeatInfoData, ComfortSeatInfo> {
	
	@Reference
	private CheckinComfortSeatOrderDataToComfortSeatOrder checkinComfortSeatOrderDataConverter;
	
	@Reference
	private CheckinPassengerDataToPassenger checkinPassengerDataConverter;
	
	@Reference
	private CheckinPaymentDataToPayment checkinPaymentDataConverter;
	
	@Reference
	private CheckinPaymentReceiptDataToPaymentReceipt checkinPaymentReceiptDataConverter;

	@Override
	public ComfortSeatInfo convert(CheckinComfortSeatInfoData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		ComfortSeatInfo destination = null;

		if (source != null) {
			destination = objectFactory.createComfortSeatInfo();
			
			destination.setX003COrderX003EKBackingField(checkinComfortSeatOrderDataConverter.convert(source.getOrder()));
			destination.setX003CPassengerX003EKBackingField(checkinPassengerDataConverter.convert(source.getPassenger()));
			destination.setX003CPaymentInfoX003EKBackingField(checkinPaymentDataConverter.convert(source.getPaymentInfo()));
			destination.setX003CReceiptInfoX003EKBackingField(checkinPaymentReceiptDataConverter.convert(source.getReceiptInfo()));
			
		}

		return destination;
	}

}
