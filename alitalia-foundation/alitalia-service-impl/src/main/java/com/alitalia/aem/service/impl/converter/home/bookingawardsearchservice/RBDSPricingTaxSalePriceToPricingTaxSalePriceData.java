//2
package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingTaxSalePriceData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetailsSolutionPricingTaxSalePrice;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingTaxSalePriceToPricingTaxSalePriceData.class)
public class RBDSPricingTaxSalePriceToPricingTaxSalePriceData implements
		Converter<ResultBookingDetailsSolutionPricingTaxSalePrice, ResultBookingDetailsSolutionPricingTaxSalePriceData> {

	@Override
	public ResultBookingDetailsSolutionPricingTaxSalePriceData convert(
			ResultBookingDetailsSolutionPricingTaxSalePrice source) {
		ResultBookingDetailsSolutionPricingTaxSalePriceData destination = null;
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingTaxSalePriceData();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}
	
	public ResultBookingDetailsSolutionPricingTaxSalePriceData convert(
			com.alitalia.aem.ws.bookingaward.searchservice.xsd13.ResultBookingDetailsSolutionPricingTaxSalePrice source) {
		ResultBookingDetailsSolutionPricingTaxSalePriceData destination = null;
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingTaxSalePriceData();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}

}
