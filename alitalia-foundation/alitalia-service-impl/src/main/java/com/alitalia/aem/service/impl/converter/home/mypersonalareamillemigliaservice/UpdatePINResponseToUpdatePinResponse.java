package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.home.UpdatePinResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.UpdatePINResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;

@Component(immediate = true, metatype = false)
@Service(value = UpdatePINResponseToUpdatePinResponse.class)
public class UpdatePINResponseToUpdatePinResponse implements Converter<UpdatePINResponse, UpdatePinResponse> {

	@Reference
	private CustomerProfileToMMCustomerProfileData customerProfileConverter;
	
	@Override
	public UpdatePinResponse convert(UpdatePINResponse source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		UpdatePinResponse destination = new UpdatePinResponse(); 
		
		JAXBElement<CustomerProfile> jaxbElement = source.getCustomer();
		CustomerProfile customerProfile = jaxbElement.getValue();
		MMCustomerProfileData mmCustomerProfile = customerProfileConverter.convert(customerProfile);
		destination.setCustomerProfile(mmCustomerProfile);
		
		List<String> updateErrors = source.getCustomerUpdateErrors();
		destination.setCustomerUpdateErrors(updateErrors);
		
		return destination;
	}
}