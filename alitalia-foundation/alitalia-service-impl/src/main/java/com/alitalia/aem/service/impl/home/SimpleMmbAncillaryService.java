package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.MmbAncillaryAddToCartRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryAddToCartResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutEndRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutEndResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutInitRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutInitResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutStatusRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutStatusResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryRemoveFromCartRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryRemoveFromCartResponse;
import com.alitalia.aem.common.messages.home.MmbAncillarySendEmailRequest;
import com.alitalia.aem.common.messages.home.MmbAncillarySendEmailResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryUpdateCartRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryUpdateCartResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCartRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCartResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCatalogRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCatalogResponse;
import com.alitalia.aem.service.api.home.MmbAncillaryService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.mmbancillaryservice.AddToCartResponseToMmbAncillaryAddToCartResponse;
import com.alitalia.aem.service.impl.converter.home.mmbancillaryservice.CheckOutEndResponseToMmbAncillaryCheckOutEndResponse;
import com.alitalia.aem.service.impl.converter.home.mmbancillaryservice.CheckOutInitResponseToMmbAncillaryCheckOutInitResponse;
import com.alitalia.aem.service.impl.converter.home.mmbancillaryservice.GetCartResponseToRetrieveMmbAncillaryCartResponse;
import com.alitalia.aem.service.impl.converter.home.mmbancillaryservice.GetCatalogResponseToRetrieveMmbAncillaryCatalogResponse;
import com.alitalia.aem.service.impl.converter.home.mmbancillaryservice.GetCheckOutStatusResponseToMmbAncillaryCheckOutStatusResponse;
import com.alitalia.aem.service.impl.converter.home.mmbancillaryservice.MmbAncillaryAddToCartRequestToAddToCartRequest;
import com.alitalia.aem.service.impl.converter.home.mmbancillaryservice.MmbAncillaryCheckOutEndRequestToCheckOutEndRequest;
import com.alitalia.aem.service.impl.converter.home.mmbancillaryservice.MmbAncillaryCheckOutInitRequestToCheckOutInitRequest;
import com.alitalia.aem.service.impl.converter.home.mmbancillaryservice.MmbAncillaryCheckOutStatusRequestToGetCheckOutStatusRequest;
import com.alitalia.aem.service.impl.converter.home.mmbancillaryservice.MmbAncillaryRemoveFromCartRequestToRemoveFromCartRequest;
import com.alitalia.aem.service.impl.converter.home.mmbancillaryservice.MmbAncillarySendEmailRequestToSendEmailRequest;
import com.alitalia.aem.service.impl.converter.home.mmbancillaryservice.MmbAncillaryUpdateCartRequestToUpdateCartRequest;
import com.alitalia.aem.service.impl.converter.home.mmbancillaryservice.RemoveFromCartResponseToMmbAncillaryRemoveFromCartResponse;
import com.alitalia.aem.service.impl.converter.home.mmbancillaryservice.RetrieveMmbAncillaryCartRequestToGetCartRequest;
import com.alitalia.aem.service.impl.converter.home.mmbancillaryservice.RetrieveMmbAncillaryCatalogRequestToGetCatalogRequest;
import com.alitalia.aem.service.impl.converter.home.mmbancillaryservice.SendEmailResponseToMmbAncillarySendEmailResponse;
import com.alitalia.aem.service.impl.converter.home.mmbancillaryservice.UpdateCartResponseToMmbAncillaryUpdateCartResponse;
import com.alitalia.aem.ws.mmb.ancillaryservice.MmbAncillaryServiceClient;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.AddToCartRequest;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.AddToCartResponse;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.CheckOutEndRequest;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.CheckOutEndResponse;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.CheckOutInitRequest;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.CheckOutInitResponse;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.GetCartRequest;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.GetCartResponse;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.GetCatalogRequest;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.GetCatalogResponse;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.GetCheckOutStatusRequest;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.GetCheckOutStatusResponse;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.RemoveFromCartRequest;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.RemoveFromCartResponse;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.SendEmailRequest;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.SendEmailResponse;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.UpdateCartRequest;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.UpdateCartResponse;

@Component(immediate=true, metatype=false)
@Service
public class SimpleMmbAncillaryService implements MmbAncillaryService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleMmbAncillaryService.class);

	@Reference
	private MmbAncillaryServiceClient mmbAncillaryService;

	@Reference
	private RetrieveMmbAncillaryCatalogRequestToGetCatalogRequest retrieveMmbAncillaryCatalogRequestConverter;

	@Reference
	private GetCatalogResponseToRetrieveMmbAncillaryCatalogResponse getCatalogResponseConverter;

	@Reference
	private RetrieveMmbAncillaryCartRequestToGetCartRequest retrieveMmbAncillaryCartRequestConverter;

	@Reference
	private GetCartResponseToRetrieveMmbAncillaryCartResponse getCartResponseConverter;

	@Reference
	private MmbAncillaryAddToCartRequestToAddToCartRequest mmbAncillaryAddToCartRequestConverter;

	@Reference
	private AddToCartResponseToMmbAncillaryAddToCartResponse addToCartResponseConverter;

	@Reference
	private MmbAncillaryUpdateCartRequestToUpdateCartRequest mmbAncillaryUpdateCartRequestConverter;

	@Reference
	private UpdateCartResponseToMmbAncillaryUpdateCartResponse updateCartResponseConverter;

	@Reference
	private MmbAncillaryRemoveFromCartRequestToRemoveFromCartRequest mmbAncillaryRemoveFromCartRequestConverter;

	@Reference
	private RemoveFromCartResponseToMmbAncillaryRemoveFromCartResponse removeFromCartResponseConverter;

	@Reference
	private MmbAncillaryCheckOutInitRequestToCheckOutInitRequest mmbAncillaryCheckOutInitRequestConverter;

	@Reference
	private CheckOutInitResponseToMmbAncillaryCheckOutInitResponse checkOutInitResponseConverter;

	@Reference
	private MmbAncillaryCheckOutEndRequestToCheckOutEndRequest mmbAncillaryCheckOutEndRequestConverter;

	@Reference
	private CheckOutEndResponseToMmbAncillaryCheckOutEndResponse checkOutEndResponseConverter;

	@Reference
	private MmbAncillaryCheckOutStatusRequestToGetCheckOutStatusRequest mmbAncillaryCheckOutStatusRequestConverter;

	@Reference
	private GetCheckOutStatusResponseToMmbAncillaryCheckOutStatusResponse getCheckOutStatusResponseConverter;

	@Reference
	private MmbAncillarySendEmailRequestToSendEmailRequest mmbAncillarySendEmailRequestConverter;

	@Reference
	private SendEmailResponseToMmbAncillarySendEmailResponse sendEmailResponseConverter;

	@Override
	public RetrieveMmbAncillaryCatalogResponse getCatalog(RetrieveMmbAncillaryCatalogRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method getCatalog(). The request is {}", request);
		}
		

		try {
			
			RetrieveMmbAncillaryCatalogResponse response = new RetrieveMmbAncillaryCatalogResponse();
			GetCatalogRequest serviceRequest = retrieveMmbAncillaryCatalogRequestConverter.convert(request);
			GetCatalogResponse serviceResponse = mmbAncillaryService.getCatalog(serviceRequest);
			response = getCatalogResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully getCatalog(). The response is {}", response);
			}
			
			return response;
			
		} catch (Exception e) {
			logger.error("Exception while executing method getCatalog().", e);
			throw new AlitaliaServiceException("Exception executing getCatalog: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public RetrieveMmbAncillaryCartResponse getCart(RetrieveMmbAncillaryCartRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method getCart(). The request is {}", request);
		}
		

		try {
			
			RetrieveMmbAncillaryCartResponse response = new RetrieveMmbAncillaryCartResponse();
			GetCartRequest serviceRequest = retrieveMmbAncillaryCartRequestConverter.convert(request);
			GetCartResponse serviceResponse = mmbAncillaryService.getCart(serviceRequest);
			response = getCartResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully getCart(). The response is {}", response);
			}
			
			return response;
			
		} catch (Exception e) {
			logger.error("Exception while executing method getCart().", e);
			throw new AlitaliaServiceException("Exception executing getCart: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public MmbAncillaryAddToCartResponse addToCart(MmbAncillaryAddToCartRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method addToCart(). The request is {}", request);
		}
		

		try {
			MmbAncillaryAddToCartResponse response = new MmbAncillaryAddToCartResponse();
			AddToCartRequest serviceRequest = mmbAncillaryAddToCartRequestConverter.convert(request);
			AddToCartResponse serviceResponse = mmbAncillaryService.addToCart(serviceRequest);
			response = addToCartResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully addToCart(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method addToCart().", e);
			throw new AlitaliaServiceException("Exception executing addToCart: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public MmbAncillaryUpdateCartResponse updateCart(MmbAncillaryUpdateCartRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method updateCart(). The request is {}", request);
		}
		

		try {
			MmbAncillaryUpdateCartResponse response = new MmbAncillaryUpdateCartResponse();
			UpdateCartRequest serviceRequest = mmbAncillaryUpdateCartRequestConverter.convert(request);
			UpdateCartResponse serviceResponse = mmbAncillaryService.updateCart(serviceRequest);
			response = updateCartResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully updateCart(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method updateCart().", e);
			throw new AlitaliaServiceException("Exception executing updateCart: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public MmbAncillaryRemoveFromCartResponse removeFromCart(MmbAncillaryRemoveFromCartRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method removeFromCart(). The request is {}", request);
		}
		

		try {
			MmbAncillaryRemoveFromCartResponse response = new MmbAncillaryRemoveFromCartResponse();
			RemoveFromCartRequest serviceRequest = mmbAncillaryRemoveFromCartRequestConverter.convert(request);
			RemoveFromCartResponse serviceResponse = mmbAncillaryService.removeFromCart(serviceRequest);
			response = removeFromCartResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully removeFromCart(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method removeFromCart().", e);
			throw new AlitaliaServiceException("Exception executing removeFromCart: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public MmbAncillaryCheckOutInitResponse checkOutInit(MmbAncillaryCheckOutInitRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method checkOutInit(). The request is {}", request);
		}
		

		try {
			MmbAncillaryCheckOutInitResponse response = new MmbAncillaryCheckOutInitResponse();
			CheckOutInitRequest serviceRequest = mmbAncillaryCheckOutInitRequestConverter.convert(request);
			CheckOutInitResponse serviceResponse = mmbAncillaryService.checkOutInit(serviceRequest);
			response = checkOutInitResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully checkOutInit(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method checkOutInit().", e);
			throw new AlitaliaServiceException("Exception executing checkOutInit: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public MmbAncillaryCheckOutEndResponse checkOutEnd(MmbAncillaryCheckOutEndRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method checkOutEnd(). The request is {}", request);
		}
		

		try {
			MmbAncillaryCheckOutEndResponse response = new MmbAncillaryCheckOutEndResponse();
			CheckOutEndRequest serviceRequest = mmbAncillaryCheckOutEndRequestConverter.convert(request);
			CheckOutEndResponse serviceResponse = mmbAncillaryService.checkOutEnd(serviceRequest);
			response = checkOutEndResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully checkOutEnd(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method checkOutEnd().", e);
			throw new AlitaliaServiceException("Exception executing checkOutEnd: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public MmbAncillaryCheckOutStatusResponse getCheckOutStatus(MmbAncillaryCheckOutStatusRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method getCheckOutStatus(). The request is {}", request);
		}
		
		try {
			MmbAncillaryCheckOutStatusResponse response = new MmbAncillaryCheckOutStatusResponse();
			GetCheckOutStatusRequest serviceRequest = mmbAncillaryCheckOutStatusRequestConverter.convert(request);
			GetCheckOutStatusResponse serviceResponse = mmbAncillaryService.getCheckOutStatus(serviceRequest);
			response = getCheckOutStatusResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully getCheckOutStatus(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getCheckOutStatus().", e);
			throw new AlitaliaServiceException("Exception executing getCheckOutStatus: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public MmbAncillarySendEmailResponse sendEmail(MmbAncillarySendEmailRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method sendEmail(). The request is {}", request);
		}
		
		try {
			MmbAncillarySendEmailResponse response = new MmbAncillarySendEmailResponse();
			SendEmailRequest serviceRequest = mmbAncillarySendEmailRequestConverter.convert(request);
			SendEmailResponse serviceResponse = mmbAncillaryService.sendEmail(serviceRequest);
			response = sendEmailResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully sendEmail(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method sendEmail().", e);
			throw new AlitaliaServiceException("Exception executing sendEmail: Sid ["+request.getSid()+"]" , e);
		}
	}

}
