package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.home.DeletePanResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.DeletePanSIAResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;

@Component(immediate = true, metatype = false)
@Service(value = DeletePanSIAResponseToDeletePanResponse.class)
public class DeletePanSIAResponseToDeletePanResponse implements Converter<DeletePanSIAResponse, DeletePanResponse> {

	@Reference
	private CustomerProfileToMMCustomerProfileData customerProfileConverter;
	
	@Override
	public DeletePanResponse convert(DeletePanSIAResponse source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		DeletePanResponse destination = new DeletePanResponse(); 
		
		JAXBElement<CustomerProfile> jaxbElement = source.getCustomer();
		CustomerProfile customerProfile = jaxbElement.getValue();
		MMCustomerProfileData mmCustomerProfile = customerProfileConverter.convert(customerProfile);
		destination.setCustomerProfile(mmCustomerProfile);
		destination.setSucceeded(source.isSucceeded());
		
		return destination;
	}
}