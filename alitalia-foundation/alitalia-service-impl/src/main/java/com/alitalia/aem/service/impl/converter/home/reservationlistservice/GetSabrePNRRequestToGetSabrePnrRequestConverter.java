package com.alitalia.aem.service.impl.converter.home.reservationlistservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.GetSabrePNRRequest;
import com.alitalia.aem.ws.reservationlist.reservationlistservice.xsd1.GetSabrePnrRequest;
import com.alitalia.aem.ws.reservationlist.reservationlistservice.xsd1.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=GetSabrePNRRequestToGetSabrePnrRequestConverter.class)
public class GetSabrePNRRequestToGetSabrePnrRequestConverter implements
		Converter<GetSabrePNRRequest, GetSabrePnrRequest> {
	
	@Reference
	private MmbRequestBaseInfoDataToRequestBaseInfo mmbRequestBaseInfoDataConverter;

	@Override
	public GetSabrePnrRequest convert(GetSabrePNRRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		GetSabrePnrRequest destination = null;

		if (source != null) {
			destination = objectFactory.createGetSabrePnrRequest();

			destination.setLocale(objectFactory.createGetSabrePnrRequestLocale(source.getLocale()));
			destination.setPnr(objectFactory.createGetSabrePnrRequestPnr(source.getPnr()));

			destination.setX003CPNRX003EKBackingField(source.getPnr());
			destination.setX003CInfoRequestX003EKBackingField(mmbRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
		}

		return destination;
	}

}
