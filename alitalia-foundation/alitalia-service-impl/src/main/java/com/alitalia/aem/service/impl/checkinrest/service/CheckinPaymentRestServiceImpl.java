package com.alitalia.aem.service.impl.checkinrest.service;

import com.alitalia.aem.common.data.checkinrest.model.initpayment.response.Authorize3dResponse;
import com.alitalia.aem.common.messages.checkinrest.*;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.checkinrest.model.endpayment.response.EndPaymentResponse;
import com.alitalia.aem.common.data.checkinrest.model.getcart.Cart;
import com.alitalia.aem.common.data.checkinrest.model.initpayment.response.InitPaymentResponse;
import com.alitalia.aem.service.api.home.ICheckinPaymentRestService;
import com.alitalia.aem.service.impl.checkinrest.client.CheckinPaymentRestServiceClient;
import com.alitalia.aem.service.impl.checkinrest.utils.CheckinMessageUnmarshal;

@Service
@Component(immediate = true, metatype = false)
public class CheckinPaymentRestServiceImpl implements ICheckinPaymentRestService{
	
	private static final Logger logger = LoggerFactory.getLogger(CheckinServiceImpl.class);
	
	@Reference
	private CheckinPaymentRestServiceClient checkinPaymentRestServiceClient;
	
	@Override
	public CheckinGetCartResponse getCart(CheckinGetCartRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method getCart. the request is {}", request);
		}
		CheckinGetCartResponse response = null;
		try {
			response = new CheckinGetCartResponse();
			String url = "/getcart";
			
			Cart getCart = new Cart();
			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinPaymentRestServiceClient.getData(request,  url);
			getCart = CheckinMessageUnmarshal.checkinGetCartUnmarshal(responseClient, request.getSeparatore());
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.setCart(getCart);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}
	
	@Override
	public CheckinInitPaymentResponse initPayment(CheckinInitPaymentRequest request) {
//		if (logger.isDebugEnabled()) {
//			logger.debug("Executing method initPayment. the request is {}", request);
//		}
		CheckinInitPaymentResponse response = null;
		try {
			response = new CheckinInitPaymentResponse();
			String url = "/initpayment";
			
			InitPaymentResponse initPaymentResponse = new InitPaymentResponse();
			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinPaymentRestServiceClient.getData(request,  url);
			initPaymentResponse = CheckinMessageUnmarshal.checkinInitPaymentUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.setInitPaymentResponse(initPaymentResponse);

		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}

	@Override
	public CheckinEndPaymentResponse endPayment(CheckinEndPaymentRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method initPayment. the request is {}", request);
		}
		CheckinEndPaymentResponse response = null;
		try {
			response = new CheckinEndPaymentResponse();
			String url = "/endpayment";
			
			EndPaymentResponse endPaymentResponse = new EndPaymentResponse();
			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinPaymentRestServiceClient.getData(request,  url);
			endPaymentResponse = CheckinMessageUnmarshal.checkinEndPaymentUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.setEndPaymentResponse(endPaymentResponse);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}

	//Tolentino - Inizio
	@Override
	public CheckinInitPaymentResponse initPaymentRecurringAdyen(CheckinInitPaymentRequest request) {
		logger.debug("MAPayment - Executing service initPaymentRecurringAdyen, the request is {}", request);

		CheckinInitPaymentResponse response = null;
		try {
			response = new CheckinInitPaymentResponse();
			String url = "/initpaymentrecurringadyen";

			InitPaymentResponse initPaymentResponse = new InitPaymentResponse();
			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinPaymentRestServiceClient.getDataForAdyenRecurringPayment(request,  url);
			logger.debug("MAPayment - Executed service initPaymentRecurringAdyen, the response is {}", responseClient);
			initPaymentResponse = CheckinMessageUnmarshal.checkinInitPaymentUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.setInitPaymentResponse(initPaymentResponse);

		}
		catch (Exception e) {
			logger.error("MAPayment - Error executing service initPaymentRecurringAdyen: "+e.getMessage());
		}
		return response;
	}
	//Tolentino - Fine

	public CheckinAuthorize3dResponse authorize3d(CheckinAuthorize3dRequest request) {
		logger.debug("MAPayment - Executing service authorize3d, the request is {}", request);

		CheckinAuthorize3dResponse response = null;
		try {
			Authorize3dResponse authorize3dResponse = new Authorize3dResponse();
			String url = "/authorize3d";

			response = new CheckinAuthorize3dResponse();
			String responseClient = checkinPaymentRestServiceClient.getData(request, url);
			logger.debug("MAPayment - Executed service authorize3d, the response is {}", responseClient);
			authorize3dResponse = CheckinMessageUnmarshal.getAuthorize3dUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.setAuthorize3dResponse(authorize3dResponse);

		} catch (Exception e) {
			logger.error("MAPayment - Error executing service authorize3d: "+e.getMessage());
		}
		return response;
	}

}
