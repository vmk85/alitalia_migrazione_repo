package com.alitalia.aem.service.impl.converter.home.statisticsservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.RegisterStatisticsResult;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd1.StatisticsResponse;

@Component(immediate = true, metatype = false)
@Service(value = StatisticsResponseToRegisterStatisticsResult.class)
public class StatisticsResponseToRegisterStatisticsResult implements Converter<StatisticsResponse, RegisterStatisticsResult> {

	@Override
	public RegisterStatisticsResult convert(StatisticsResponse source) {
		
		RegisterStatisticsResult result = new RegisterStatisticsResult();
		return result;
		
	}
}