package com.alitalia.aem.service.impl.converter.home.flightstatusservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.FlightDetailsModelData;
import com.alitalia.aem.common.data.home.FlySegmentData;
import com.alitalia.aem.common.messages.home.RetrieveFlightDetailsResponse;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetFlightDetailsModelResponse;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.ArrayOfFlySegment;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.FlightDetailsModel;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.FlySegment;

@Component(immediate = true, metatype = false)
@Service(value = GetFlightDetailsModelResponseToRetrieveFlightDetailsResponse.class)
public class GetFlightDetailsModelResponseToRetrieveFlightDetailsResponse implements Converter<GetFlightDetailsModelResponse, RetrieveFlightDetailsResponse> {

	@Override
	public RetrieveFlightDetailsResponse convert(GetFlightDetailsModelResponse source) {
		RetrieveFlightDetailsResponse response = new RetrieveFlightDetailsResponse();
		
		JAXBElement<FlightDetailsModel> jaxbFlightDetails = source.getFlightDetails();
		FlightDetailsModel flightDetailsModel = jaxbFlightDetails.getValue();
		String airCraft = flightDetailsModel.getAirCraft().getValue();
		XMLGregorianCalendar flightDate = flightDetailsModel.getFlightDate();
		String flightNumber = flightDetailsModel.getFlightNumber().getValue();
		ArrayOfFlySegment flightSegments = flightDetailsModel.getFlySegment().getValue();
		
		List<FlySegment> flySegments = (flightSegments != null) ? flightSegments.getFlySegment() : new ArrayList<FlySegment>();
		String flightVector = flightDetailsModel.getVector().getValue();
		
		FlightDetailsModelData flightDetailsModelData = new FlightDetailsModelData();
		flightDetailsModelData.setAirCraft(airCraft);
		if (flightDate != null)
			flightDetailsModelData.setFlightDate(XsdConvertUtils.parseCalendar(flightDate));
		flightDetailsModelData.setFlightNumber(flightNumber);
		
		List<FlySegmentData> flySegmentDatas = new ArrayList<>();
		for(FlySegment flySegment: flySegments){
			FlySegmentData flySegmentData = new FlySegmentData();
			flySegmentData.setBoardPoint(flySegment.getBoardPoint().getValue());
			flySegmentData.setBoardPointDepTime(flySegment.getBoardPointDepTime().getValue());
			flySegmentData.setDaysFromStart(flySegment.getDaysFromStart());
			flySegmentData.setMilesFromStart(flySegment.getMilesFromStart());
			flySegmentData.setOffPoint(flySegment.getOffPoint().getValue());
			flySegmentData.setOffPointArrTime(flySegment.getOffPointArrTime().getValue());
			flySegmentData.setTimeFromStart(flySegment.getTimeFromStart().getValue());
			flySegmentDatas.add(flySegmentData);
		}
		
		flightDetailsModelData.setFlySegments(flySegmentDatas);
		flightDetailsModelData.setVector(flightVector);
		
		response.setFlightDetailsModelData(flightDetailsModelData);
		
		return response;
	}
}