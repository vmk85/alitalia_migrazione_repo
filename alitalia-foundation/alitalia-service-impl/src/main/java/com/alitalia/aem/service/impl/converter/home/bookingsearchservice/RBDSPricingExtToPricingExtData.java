//2
package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingExtData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingExtEndorsementData;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ResultBookingDetailsSolutionPricingExt;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ResultBookingDetailsSolutionPricingExtEndorsement;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingExtToPricingExtData.class)
public class RBDSPricingExtToPricingExtData implements Converter<ResultBookingDetailsSolutionPricingExt, ResultBookingDetailsSolutionPricingExtData> {

	@Reference
	private RBDSPricingExtEndorsementToPricingExtEndorsementData endorsementFieldConverter;
	
	@Reference
	private RBDSPricingExtPaxToPricingExtPaxData paxFieldConverter;
	
	@Override
	public ResultBookingDetailsSolutionPricingExtData convert(
			ResultBookingDetailsSolutionPricingExt source) {
		ResultBookingDetailsSolutionPricingExtData destination = null;
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingExtData();
			List<ResultBookingDetailsSolutionPricingExtEndorsementData> array = new ArrayList<ResultBookingDetailsSolutionPricingExtEndorsementData>();
			if(source.getEndorsementField()!=null)
			for(ResultBookingDetailsSolutionPricingExtEndorsement s : source.getEndorsementField().getResultBookingDetailsSolutionPricingExtEndorsement()){
				array.add(endorsementFieldConverter.convert(s));
			}
			
			destination.setEndorsementField(array);
			destination.setPaxField(paxFieldConverter.convert(source.getPaxField()));
		}
		return destination;
	}
	
	public ResultBookingDetailsSolutionPricingExtData convert(
			com.alitalia.aem.ws.booking.searchservice.xsd14.ResultBookingDetailsSolutionPricingExt source) {
		ResultBookingDetailsSolutionPricingExtData destination = null;
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingExtData();
			List<ResultBookingDetailsSolutionPricingExtEndorsementData> array = new ArrayList<ResultBookingDetailsSolutionPricingExtEndorsementData>();
			if(source.getEndorsementField()!=null)
			for(com.alitalia.aem.ws.booking.searchservice.xsd14.ResultBookingDetailsSolutionPricingExtEndorsement s : source.getEndorsementField().getResultBookingDetailsSolutionPricingExtEndorsement()){
				array.add(endorsementFieldConverter.convert(s));
			}
			
			destination.setEndorsementField(array);
			destination.setPaxField(paxFieldConverter.convert(source.getPaxField()));
		}
		return destination;
	}

}
