package com.alitalia.aem.service.impl.converter.home.staticdataservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.FrequentFlyerTypeData;
import com.alitalia.aem.common.messages.home.RetrieveFrequentFlyersResponse;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.StaticDataResponse;
import com.alitalia.aem.ws.booking.staticdataservice.xsd8.FrequentFlyerType;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveFrequentFlyersResponseToStaticDataResponseConverter.class)
public class RetrieveFrequentFlyersResponseToStaticDataResponseConverter implements Converter<StaticDataResponse, RetrieveFrequentFlyersResponse> {

	private static final Logger logger = LoggerFactory.getLogger(RetrieveFrequentFlyersResponseToStaticDataResponseConverter.class);
	
	@Reference
	private FrequentFlyerTypeToFrequentFlyerData frequentFlayerConverter;
	
	@Reference
	private StaticDataServiceJAXBContextFactory jaxbContextFactory;

	@SuppressWarnings("unchecked")
	@Override
	public RetrieveFrequentFlyersResponse convert(StaticDataResponse source) {
		RetrieveFrequentFlyersResponse destination = new RetrieveFrequentFlyersResponse();
		try{
			List<FrequentFlyerTypeData> frequentFlyersData = new ArrayList<>();
			
			Unmarshaller frequentFlyerUnmashaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			JAXBElement<FrequentFlyerType> unmarshalledObj = null;

			List<Object> freqFlayers = source.getEntities().getValue().getAnyType();
			for (Object flyer: freqFlayers) {
				FrequentFlyerType castedflyer = null;
				if (flyer instanceof FrequentFlyerType) {
					castedflyer = (FrequentFlyerType) flyer;
				} else {
					unmarshalledObj = (JAXBElement<FrequentFlyerType>) 
							frequentFlyerUnmashaller.unmarshal((Node) flyer);
					castedflyer = unmarshalledObj.getValue();
				}
				FrequentFlyerTypeData freqFlayerTypeData = frequentFlayerConverter.convert(castedflyer);
				frequentFlyersData.add(freqFlayerTypeData);
			}
			
			destination.setFrequentFlyers(frequentFlyersData);
		} catch (JAXBException e) {
			logger.error("Error instantiating JAXBContext/Unmarshaller: {}", e);
		} finally {
			logger.info("Conversion executed successfully");
		}
		
		
		return destination;
	}
}