package com.alitalia.aem.service.impl.converter.home.carnetpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.carnet.CarnetComunicationBancaIntesaData;
import com.alitalia.aem.common.data.home.carnet.CarnetComunicationBancoPostaData;
import com.alitalia.aem.common.data.home.carnet.CarnetComunicationCreditCardData;
import com.alitalia.aem.common.data.home.carnet.CarnetComunicationData;
import com.alitalia.aem.common.data.home.carnet.CarnetComunicationPayPalData;
import com.alitalia.aem.common.data.home.carnet.CarnetComunicationUnicreditData;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.AComunicationBase;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.BancoPostaType;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.ComunicationBancaIntesa;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.ComunicationBancoPosta;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.ComunicationCreditCard;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.ComunicationPayPal;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.ComunicationUnicredit;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.ObjectFactory;



@Component(immediate=true, metatype=false)
@Service(value=CarnetComunicationDataToAComunicationBase.class)
public class CarnetComunicationDataToAComunicationBase 
		implements Converter<CarnetComunicationData, AComunicationBase> {
	
	private static final Logger logger = LoggerFactory.getLogger(CarnetComunicationDataToAComunicationBase.class);

	
	@Override
	public AComunicationBase convert(CarnetComunicationData source) {
		
		AComunicationBase destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			if (source instanceof CarnetComunicationBancaIntesaData) {
				logger.debug("CarnetComunicationData instanceof CarnetComunicationBancaIntesaData");
				CarnetComunicationBancaIntesaData typedSource = (CarnetComunicationBancaIntesaData) source;
				ComunicationBancaIntesa typedDestination = new ComunicationBancaIntesa();
				destination = typedDestination;
				
				typedDestination.setErrorUrl(
						objectFactory.createAComunicationInfoErrorUrl(
								typedSource.getErrorUrl()));
				
				typedDestination.setCancelUrl(
						objectFactory.createComunicationBancaIntesaCancelUrl(
								typedSource.getCancelUrl()));
				
				typedDestination.setShopperId(
						objectFactory.createComunicationBancaIntesaShopperId(
								typedSource.getShopperId()));
				
			} else if (source instanceof CarnetComunicationBancoPostaData) {
				logger.debug("CarnetComunicationData instanceof CarnetComunicationBancoPostaData");
				CarnetComunicationBancoPostaData typedSource = (CarnetComunicationBancoPostaData) source;
				ComunicationBancoPosta typedDestination = new ComunicationBancoPosta();
				destination = typedDestination;
				
				typedDestination.setErrorUrl(
						objectFactory.createAComunicationInfoErrorUrl(
								typedSource.getErrorUrl()));
				
				if (typedSource.getType() != null) {
					typedDestination.setType(
							BancoPostaType.fromValue(typedSource.getType().value()));
				}
				
			} else if (source instanceof CarnetComunicationCreditCardData) {
				logger.debug("CarnetComunicationData instanceof CarnetComunicationCreditCardData");
				CarnetComunicationCreditCardData typedSource = (CarnetComunicationCreditCardData) source;
				ComunicationCreditCard typedDestination = new ComunicationCreditCard();
				destination = typedDestination;
				
				typedDestination.setErrorUrl(
						objectFactory.createAComunicationInfoErrorUrl(
								typedSource.getErrorUrl()));
				
				typedDestination.setAcceptHeader(
						objectFactory.createComunicationCreditCardAcceptHeader(
								typedSource.getAcceptHeader()));
				
				typedDestination.setIPAddress(
						objectFactory.createComunicationCreditCardIPAddress(
								typedSource.getIpAddress()));
				
				typedDestination.setUserAgent(
						objectFactory.createComunicationCreditCardUserAgent(
								typedSource.getUserAgent()));
				
			} else if (source instanceof CarnetComunicationPayPalData) {
				logger.debug("CarnetComunicationData instanceof CarnetComunicationPayPalData");

				CarnetComunicationPayPalData typedSource = (CarnetComunicationPayPalData) source;
				ComunicationPayPal typedDestination = new ComunicationPayPal();
				destination = typedDestination;	
				
				typedDestination.setCancelUrl(
						objectFactory.createComunicationPayPalCancelUrl(
								typedSource.getCancelUrl()));
				
				typedDestination.setShippingEnabled(
						typedSource.getShippingEnabled());
				

				
			} else if (source instanceof CarnetComunicationUnicreditData) {
				logger.debug("CarnetComunicationData instanceof CarnetComunicationUnicreditData");

				CarnetComunicationUnicreditData typedSource = (CarnetComunicationUnicreditData) source;
				ComunicationUnicredit typedDestination = new ComunicationUnicredit();
				destination = typedDestination;	
				
				typedDestination.setErrorUrl(
						objectFactory.createAComunicationInfoErrorUrl(
								typedSource.getErrorUrl()));
				
			} else {
				logger.debug("CarnetComunicationData instanceof AComunicationBase");

				destination = new AComunicationBase();
				
			}
			
			destination.setDescription(
					objectFactory.createAComunicationBaseDescription(
							source.getDescription()));
			
			destination.setLanguageCode(
					objectFactory.createAComunicationBaseLanguageCode(
							source.getLanguageCode()));
			
			destination.setReturnUrl(
					objectFactory.createAComunicationBaseReturnUrl(
							source.getReturnUrl()));
			
		}
				
		return destination;
	}

}
