package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.Brand;
import com.alitalia.aem.ws.booking.ticketservice.xsd2.ArrayOfanyType;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ConnectingFlight;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.DirectFlight;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.FlightType;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ObjectFactory;

@Component(immediate = true, metatype = false)
@Service(value = ConnectingFlightDataToConnectingFlight.class)
public class ConnectingFlightDataToConnectingFlight implements Converter<ConnectingFlightData, ConnectingFlight> {

	@Reference
	private DirectFlightDataToDirectFlight directFlightDataConverter;

	@Reference
	private BrandDataToBrand brandDataConverter;

	@Override
	public ConnectingFlight convert(ConnectingFlightData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		com.alitalia.aem.ws.booking.ticketservice.xsd2.ObjectFactory objectFactory2 = 
				new com.alitalia.aem.ws.booking.ticketservice.xsd2.ObjectFactory();

		ConnectingFlight destination = objectFactory.createConnectingFlight();

		destination.setType(FlightType.CONNECTING);

		ArrayOfanyType flightsArray = objectFactory2.createArrayOfanyType();
		for (FlightData flightData : source.getFlights()) {
			if (flightData instanceof DirectFlightData) {
				DirectFlight directFlight = directFlightDataConverter.convert((DirectFlightData) flightData);
				flightsArray.getAnyType().add(directFlight);
			} else {
				ConnectingFlight connectingFlight = this.convert((ConnectingFlightData) flightData);
				flightsArray.getAnyType().add(connectingFlight);
			}
		}
		destination.setFlights(objectFactory.createConnectingFlightFlights(flightsArray));

		ArrayOfanyType brandsArray = objectFactory2.createArrayOfanyType();
		for (BrandData brandData : source.getBrands()) {
			Brand brand = brandDataConverter.convert(brandData);
			brandsArray.getAnyType().add(brand);
		}
		destination.setBrands(objectFactory.createAFlightBaseBrands(brandsArray));

		// Dato che non e' presente l'XSD che consente di definire i campi delle properties
		// il servizio non ha necessita' dell'informazione
		destination.setProperties(null);

		return destination;
	}

}
