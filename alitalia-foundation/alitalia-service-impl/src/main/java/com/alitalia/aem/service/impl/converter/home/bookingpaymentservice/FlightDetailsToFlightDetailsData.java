package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.FlightDetailsData;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.FlightDetails;

@Component(immediate=true, metatype=false)
@Service(value=FlightDetailsToFlightDetailsData.class)
public class FlightDetailsToFlightDetailsData implements Converter<FlightDetails, FlightDetailsData> {

	@Override
	public FlightDetailsData convert(FlightDetails source) {
		FlightDetailsData destiantion = new FlightDetailsData();

		destiantion.setAeromobile(source.getAeromobile().getValue());
		destiantion.setArrivalTerminal(source.getArrivalTerminal().getValue());
		destiantion.setDepartureTerminal(source.getDepartureTerminal().getValue());
		destiantion.setDistance(source.getDistance().getValue());
		destiantion.setOtherInformation(source.getOtherInformation().getValue());

		return destiantion;
	}

}
