package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareEndorsementData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingFareEndorsement;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareEndorsementDataToRTDSPricingFareEndorsement.class)
public class RTDSPricingFareEndorsementDataToRTDSPricingFareEndorsement implements
		Converter<ResultTicketingDetailSolutionPricingFareEndorsementData, 
					ResultTicketingDetailSolutionPricingFareEndorsement> {

	@Override
	public ResultTicketingDetailSolutionPricingFareEndorsement convert(
			ResultTicketingDetailSolutionPricingFareEndorsementData source) {
		ResultTicketingDetailSolutionPricingFareEndorsement destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingFareEndorsement();

			destination.setBoxesField(source.getBoxesField());
		}

		return destination;
	}

}
