package com.alitalia.aem.service.impl.beanrest.staticdataservicerest.beans;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetAirports implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@SerializedName("airport")
    @Expose
    private List<Airports> airport = null;

    public List<Airports> getAirport() {
        return airport;
    }

    public void setAirport(List<Airports> airport) {
        this.airport = airport;
    }

}
