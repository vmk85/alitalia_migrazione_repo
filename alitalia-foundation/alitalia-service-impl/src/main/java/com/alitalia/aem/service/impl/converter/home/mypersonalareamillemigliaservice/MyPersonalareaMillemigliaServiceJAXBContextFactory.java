package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Activity;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Address;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Contract;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CreditCard;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Flight;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Lifestyle;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Message;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Preference;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Program;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Telephone;

@Component(immediate=true, metatype=false)
@Service(value=MyPersonalareaMillemigliaServiceJAXBContextFactory.class)
public class MyPersonalareaMillemigliaServiceJAXBContextFactory {

	private static final Logger logger = LoggerFactory.getLogger(MyPersonalareaMillemigliaServiceJAXBContextFactory.class);

	private JAXBContext jaxbContext = null;

	public synchronized JAXBContext getJaxbContext() throws JAXBException {

		if (jaxbContext == null) {
			jaxbContext = JAXBContext.newInstance(Activity.class,
					Address.class,
					Contract.class,
					CreditCard.class,
					Flight.class,
					Lifestyle.class,
					Message.class,
					Preference.class,
					Program.class,
					Telephone.class
								);
		}

		return jaxbContext;
	}

	@Activate
	private void activate() {
		try {
			jaxbContext = JAXBContext.newInstance(Activity.class,
					Address.class,
					Contract.class,
					CreditCard.class,
					Flight.class,
					Lifestyle.class,
					Message.class,
					Preference.class,
					Program.class,
					Telephone.class
					);
		} catch (JAXBException e) {
			logger.warn("MyPersonalareaMillemigliaServiceJAXBContextFactory - cannot instantiate JAXBContext: {}", e.getMessage());
		}
	}
}
