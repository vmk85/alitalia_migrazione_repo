package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.BrandPenaltiesData;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.BrandPenalties;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.BrandPenalty;

@Component(immediate=true, metatype=false)
@Service(value=BrandPenaltiesToBrandPenaltiesData.class)
public class BrandPenaltiesToBrandPenaltiesData implements Converter<BrandPenalties, BrandPenaltiesData> {

	@Reference
	private BrandPenaltyToBrandPenaltyData brandPenaltyConveter;

	@Override
	public BrandPenaltiesData convert(BrandPenalties source) {
		BrandPenaltiesData destination = new BrandPenaltiesData();
		BrandPenalty penalty = null;

		JAXBElement<Object> penaltyElement = source.getChangeAfterDepature();
		if (penaltyElement != null) {
			penalty = (BrandPenalty) penaltyElement.getValue();
			destination.setChangeAfterDepature(brandPenaltyConveter.convert(penalty));
		}
		else
			destination.setChangeAfterDepature(null);

		penaltyElement = source.getChangeBeforeDepature();
		if (penaltyElement != null) {
			penalty = (BrandPenalty) penaltyElement.getValue();
			destination.setChangeBeforeDepature(brandPenaltyConveter.convert(penalty));
		}
		else
			destination.setChangeBeforeDepature(null);

		penaltyElement = source.getRefundAfterDepature();
		if (penaltyElement != null) {
			penalty = (BrandPenalty) penaltyElement.getValue();
			destination.setRefundAfterDepature(brandPenaltyConveter.convert(penalty));
		}
		else
			destination.setRefundAfterDepature(null);

		penaltyElement = source.getRefundBeforeDepature();
		if (penaltyElement != null) {
			penalty = (BrandPenalty) penaltyElement.getValue();
			destination.setRefundBeforeDepature(brandPenaltyConveter.convert(penalty));
		}
		else
			destination.setRefundBeforeDepature(null);

		return destination;
	}

}
