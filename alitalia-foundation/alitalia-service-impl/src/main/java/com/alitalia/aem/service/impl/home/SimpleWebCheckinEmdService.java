package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.WebCheckinEmdRequest;
import com.alitalia.aem.common.messages.home.WebCheckinEmdResponse;
import com.alitalia.aem.common.messages.home.WebCheckinReasonForIssuanceRequest;
import com.alitalia.aem.common.messages.home.WebCheckinReasonForIssuanceResponse;
import com.alitalia.aem.service.api.home.WebCheckinEmdService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.checkinemdservice.EMDResponseToWebCheckinEmdResponse;
import com.alitalia.aem.service.impl.converter.home.checkinemdservice.ReasonForIssuanceTypeToWebCheckinReasonForIssuanceResponse;
import com.alitalia.aem.service.impl.converter.home.checkinemdservice.WebCheckinEmdRequestToEMDRequestConverter;
import com.alitalia.aem.ws.checkin.emdservice.CheckinEmdServiceClient;
import com.alitalia.aem.ws.checkin.emdservice.xsd1.EMDReasonForIssuanceEnumType;
import com.alitalia.aem.ws.checkin.emdservice.xsd1.EMDRequest;
import com.alitalia.aem.ws.checkin.emdservice.xsd1.EMDResponse;
import com.alitalia.aem.ws.checkin.emdservice.xsd2.ReasonForIssuanceType;


@Component(immediate=true, metatype=false)
@Service
public class SimpleWebCheckinEmdService implements WebCheckinEmdService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleWebCheckinEmdService.class);

	@Reference
	private CheckinEmdServiceClient checkinEmdClient;
	
	@Reference
	private WebCheckinEmdRequestToEMDRequestConverter checkinEmdRequestConverter;
	
	@Reference
	private EMDResponseToWebCheckinEmdResponse checkinEmdResponseConverter;
	
	@Reference
	private ReasonForIssuanceTypeToWebCheckinReasonForIssuanceResponse reasonForIssuanceResponseConverter;
	
	@Override
	public WebCheckinEmdResponse extraBaggageEmd(WebCheckinEmdRequest request) {
		logger.debug("Executing method extraBaggageEmd(). The request is {}", request);

		try {
			WebCheckinEmdResponse response = new WebCheckinEmdResponse();
			EMDRequest serviceRequest = checkinEmdRequestConverter.convert(request);
			EMDResponse serviceResponse = checkinEmdClient.extraBaggageEMD(serviceRequest);
			response = checkinEmdResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully extraBaggageEmd(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method extraBaggageEmd().", e);
			throw new AlitaliaServiceException("Exception executing extraBaggageEmd: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public WebCheckinEmdResponse comfortSeatEmd(WebCheckinEmdRequest request) {
		logger.debug("Executing method comfortSeatEmd(). The request is {}", request);

		try {
			WebCheckinEmdResponse response = new WebCheckinEmdResponse();
			EMDRequest serviceRequest = checkinEmdRequestConverter.convert(request);
			EMDResponse serviceResponse = checkinEmdClient.comfortSeatEMD(serviceRequest);
			response = checkinEmdResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully comfortSeatEmd(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method comfortSeatEmd().", e);
			throw new AlitaliaServiceException("Exception executing comfortSeatEmd: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public WebCheckinEmdResponse cabinUpgradeEmd(WebCheckinEmdRequest request) {
		logger.debug("Executing method cabinUpgradeEmd(). The request is {}", request);

		try {
			WebCheckinEmdResponse response = new WebCheckinEmdResponse();
			EMDRequest serviceRequest = checkinEmdRequestConverter.convert(request);
			EMDResponse serviceResponse = checkinEmdClient.cabinUpgradeEMD(serviceRequest);
			response = checkinEmdResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully cabinUpgradeEmd(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method cabinUpgradeEmd().", e);
			throw new AlitaliaServiceException("Exception executing cabinUpgradeEmd: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public WebCheckinReasonForIssuanceResponse getReasonForIssuance(WebCheckinReasonForIssuanceRequest request) {
		logger.debug("Executing method getReasonForIssuance(). The request is {}", request);

		try {
			WebCheckinReasonForIssuanceResponse response = new WebCheckinReasonForIssuanceResponse();
			EMDReasonForIssuanceEnumType serviceRequest = EMDReasonForIssuanceEnumType.fromValue(request.getReasonForIssuanceType().value());
			ReasonForIssuanceType serviceResponse = checkinEmdClient.getReasonForIssuance(serviceRequest);
			response = reasonForIssuanceResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully getReasonForIssuance(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getReasonForIssuance().", e);
			throw new AlitaliaServiceException("Exception executing getReasonForIssuance: Sid ["+request.getSid()+"]" , e);
		}
	}
}