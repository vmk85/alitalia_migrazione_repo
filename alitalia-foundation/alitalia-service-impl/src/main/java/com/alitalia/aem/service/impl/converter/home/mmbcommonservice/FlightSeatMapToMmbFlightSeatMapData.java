package com.alitalia.aem.service.impl.converter.home.mmbcommonservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.SeatMapData;
import com.alitalia.aem.common.data.home.mmb.MmbFlightSeatMapData;
import com.alitalia.aem.ws.mmb.commonservice.xsd6.FlightSeatMap;
import com.alitalia.aem.ws.mmb.commonservice.xsd6.SeatMap;

@Component(immediate=true, metatype=false)
@Service(value=FlightSeatMapToMmbFlightSeatMapData.class)
public class FlightSeatMapToMmbFlightSeatMapData implements Converter<FlightSeatMap, MmbFlightSeatMapData> {

	@Reference
	private SeatMapToSeatMapData seatMapConverter;

	@Reference
	private FlightToMmbFlightData flightConverter;	

	@Override
	public MmbFlightSeatMapData convert(FlightSeatMap source) {
		MmbFlightSeatMapData destination = null;

		if (source != null) {
			destination = new MmbFlightSeatMapData();

			destination.setFlight(
					flightConverter.convert(source.getX003CFlightX003EKBackingField()));
			
			List<SeatMapData> seatMaps = new ArrayList<SeatMapData>();
			if (source.getX003CSeatMapsX003EKBackingField() != null &&
					source.getX003CSeatMapsX003EKBackingField().getSeatMap() != null) {
				for (SeatMap seatMap : source.getX003CSeatMapsX003EKBackingField().getSeatMap()) {
					seatMaps.add(seatMapConverter.convert(seatMap));
				}
			}
			destination.setSeatMaps(seatMaps);
			
		}
	
		return destination;
	}

}
