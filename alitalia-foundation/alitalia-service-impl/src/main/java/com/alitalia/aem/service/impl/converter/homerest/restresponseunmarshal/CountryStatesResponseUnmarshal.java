package com.alitalia.aem.service.impl.converter.homerest.restresponseunmarshal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.countrystates.CountryStates;
import com.alitalia.aem.common.messages.home.CountryStatesResponse;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class CountryStatesResponseUnmarshal {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public CountryStatesResponse countryStatesUnmarshal (String jsonString) {
		CountryStatesResponse response = new CountryStatesResponse();
		CountryStates cs = new CountryStates();
		Gson gson = new Gson();
		
		try {
			cs = gson.fromJson(jsonString, CountryStates.class);
			response.setCountryStates(cs);
		}
		catch (JsonSyntaxException e) {
			e.printStackTrace();
			logger.debug("JsonSyntaxException in countryStatesUnmarshal");
		}
		
		return response;
	}

}
