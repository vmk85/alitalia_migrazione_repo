package com.alitalia.aem.service.impl.converter.home.checkinservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinAirport;
import com.alitalia.aem.ws.checkin.checkinservice.xsd2.Airport;


@Component(immediate=true, metatype=false)
@Service(value=AirportToCheckinAirport.class)
public class AirportToCheckinAirport implements Converter<Airport, CheckinAirport> {

	@Override
	public CheckinAirport convert(Airport source) {
		CheckinAirport destination = null;

		if (source != null) {
			
			destination = new CheckinAirport();
			destination.setId(source.getX003CIdX003EKBackingField());
			destination.setAirportName(source.getX003CAirportNameX003EKBackingField());
			destination.setCode(source.getX003CCodeX003EKBackingField());
			destination.setCity(source.getX003CCityNameX003EKBackingField());
			destination.setCountryCode(source.getX003CCountryCodeX003EKBackingField());
			destination.setName(source.getX003CNameX003EKBackingField());
			
		}
		
		return destination;
	}

}
