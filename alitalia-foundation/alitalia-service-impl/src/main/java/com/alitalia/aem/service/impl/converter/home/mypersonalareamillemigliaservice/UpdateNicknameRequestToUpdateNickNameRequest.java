package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.UpdateNicknameRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.UpdateNickNameRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;

@Component(immediate = true, metatype = false)
@Service(value = UpdateNicknameRequestToUpdateNickNameRequest.class)
public class UpdateNicknameRequestToUpdateNickNameRequest implements Converter<UpdateNicknameRequest, UpdateNickNameRequest> {

	@Reference
	private MMCustomerProfileDataToCustomerProfile mmCustomerProfileDataConverter;
	
	@Override
	public UpdateNickNameRequest convert(UpdateNicknameRequest source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		
		ObjectFactory objectFactory = new ObjectFactory();
		
		UpdateNickNameRequest destination = objectFactory.createUpdateNickNameRequest();
		CustomerProfile customerProfile = mmCustomerProfileDataConverter.convert(source.getCustomerProfile());
		
		JAXBElement<CustomerProfile> jaxbCustomerProfile = objectFactory.createCustomerRequestCustomer(customerProfile);
		JAXBElement<String> jaxbNewNickName = objectFactory.createUpdateNickNameRequestNewNickName(source.getNewNickname());
		destination.setCustomer(jaxbCustomerProfile);
		destination.setNewNickName(jaxbNewNickName);
		
		return destination;
	}
}