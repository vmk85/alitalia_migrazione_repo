package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.TipoProfilo;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd5.ActivityTypes;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import javax.xml.bind.JAXBElement;

@Component(immediate = true, metatype = false)
@Service(value = SetDatiSicurezzaProfiloRequesToWsSetDatiSicurezzaProfiloRequest.class)
public class SetDatiSicurezzaProfiloRequesToWsSetDatiSicurezzaProfiloRequest
		implements Converter<
			com.alitalia.aem.common.messages.home.SetDatiSicurezzaProfiloRequest,
			com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SetDatiSicurezzaProfiloRequest
		> {

	@Reference
	private MMCustomerProfileDataToCustomerProfile mmCustomerProfileDataConverter;

	@Override
	public com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SetDatiSicurezzaProfiloRequest convert(
			com.alitalia.aem.common.messages.home.SetDatiSicurezzaProfiloRequest source) {
		com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SetDatiSicurezzaProfiloRequest destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createSetDatiSicurezzaProfiloRequest();

			destination.setUsername(
					objectFactory.createSetDatiSicurezzaProfiloRequestUsername(
							source.getUserName()));

			destination.setSecureQuestionID(source.getSecureQuestionID());

			destination.setSecureAnswer(
					objectFactory.createSetDatiSicurezzaProfiloRequestSecureAnswer(
							source.getSecureAnswer()));

			destination.setPassword(
					objectFactory.createSetDatiSicurezzaProfiloRequestPassword(
							source.getPassword()));

			destination.setLockedout(source.getLockedout());

			destination.setIDProfilo(
					objectFactory.createSetDatiSicurezzaProfiloRequestIDProfilo(
							source.getIdProfilo()));

			destination.setFlagVerificaUsername(source.getFlagVerificaUserNameOK());

			destination.setFlagVerificaRisposta(source.getFlagVerificaRispostaOK());

			destination.setFlagVerificaPassword(source.getFlagVerificaPasswordOK());

			destination.setFlagVerificaEmailAccount(source.getFlagVerificaEmailAccountOK());

			destination.setFlagVerificaCellulare(source.getFlagVerificaCellulareOK());

			destination.setFlagRememberPassword(source.getFlagRememberPassword());

			destination.setCertifiedEmailAccount(
					objectFactory.createSetDatiSicurezzaProfiloRequestCertifiedEmailAccount(
							source.getEmailAccount()));

			destination.setAccessFailedCount(
					objectFactory.createSetDatiSicurezzaProfiloRequestAccessFailedCount(
							source.getAccessFailedCount()));

			destination.setCertifiedPhoneAccount(
					objectFactory.createSetDatiSicurezzaProfiloRequestCertifiedPhoneAccount(
							source.getCellulare()));

			destination.setCertifiedCountryNumber(
					objectFactory.createSetDatiSicurezzaProfiloRequestCertifiedCountryNumber(
							source.getPrefissoNazioneCellulare()));
		}

		return destination;
	}
}