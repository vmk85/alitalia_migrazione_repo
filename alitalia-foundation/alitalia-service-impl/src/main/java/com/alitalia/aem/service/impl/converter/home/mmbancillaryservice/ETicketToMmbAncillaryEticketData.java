package com.alitalia.aem.service.impl.converter.home.mmbancillaryservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryCouponData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryEticketData;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.Coupons;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.ETicket;

@Component(immediate=true, metatype=false)
@Service(value=ETicketToMmbAncillaryEticketData.class)
public class ETicketToMmbAncillaryEticketData implements Converter<ETicket, MmbAncillaryEticketData> {

	@Reference
	private CouponsToMmbAncillaryCouponData couponConverter;

	@Override
	public MmbAncillaryEticketData convert(ETicket source) {
		MmbAncillaryEticketData destination = null;

		if (source != null) {
			destination = new MmbAncillaryEticketData();

			destination.setNumber(source.getX003CNumberX003EKBackingField());
			destination.setPnr(source.getX003CPnrX003EKBackingField());

			List<MmbAncillaryCouponData> coupons = null;
			if (source.getX003CCouponsX003EKBackingField() != null &&
					source.getX003CCouponsX003EKBackingField().getCoupons()!= null &&
					!source.getX003CCouponsX003EKBackingField().getCoupons().isEmpty()) {
				coupons = new ArrayList<MmbAncillaryCouponData>();
				List<Coupons> sourceCouponsList = source.getX003CCouponsX003EKBackingField().getCoupons();
				for (Coupons sourceCoupons : sourceCouponsList)
					coupons.add(couponConverter.convert(sourceCoupons));
				
			}
			destination.setCoupons(coupons);
		}

		return destination;
	}

}
