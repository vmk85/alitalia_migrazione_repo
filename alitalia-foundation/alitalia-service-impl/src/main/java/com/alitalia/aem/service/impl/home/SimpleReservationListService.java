package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.GetSabrePNRRequest;
import com.alitalia.aem.common.messages.home.GetSabrePNRResponse;
import com.alitalia.aem.service.api.home.ReservationListService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.reservationlistservice.GetSabrePNRRequestToGetSabrePnrRequestConverter;
import com.alitalia.aem.service.impl.converter.home.reservationlistservice.GetSabrePnrResponseToGetSabrePNRResponseConverter;
import com.alitalia.aem.ws.reservationlist.reservationlistservice.ReservationListClient;
import com.alitalia.aem.ws.reservationlist.reservationlistservice.xsd1.GetSabrePnrRequest;
import com.alitalia.aem.ws.reservationlist.reservationlistservice.xsd1.GetSabrePnrResponse;


@Component(immediate=true, metatype=false)
@Service
public class SimpleReservationListService implements ReservationListService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleReservationListService.class);

	@Reference
	private ReservationListClient reservationListClient;
	
	@Reference
	private GetSabrePNRRequestToGetSabrePnrRequestConverter getSabrePNRRequestConverter;
	
	@Reference
	private GetSabrePnrResponseToGetSabrePNRResponseConverter getSabrePNRResponseConverter;
	
	@Override
	public GetSabrePNRResponse getSabrePNR(GetSabrePNRRequest request){
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method getSabrePNR(). The request is {}", request);
		}
		
		try {
			GetSabrePNRResponse response = new GetSabrePNRResponse();
			GetSabrePnrRequest serviceRequest = getSabrePNRRequestConverter.convert(request);
			GetSabrePnrResponse serviceResponse = reservationListClient.getSabrePnr(serviceRequest);
			response = getSabrePNRResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully getSabrePNR(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getSabrePNR().", e);
			throw new AlitaliaServiceException("Exception executing getSabrePNR: Sid ["+request.getSid()+"]" , e);
		}
	}

}
