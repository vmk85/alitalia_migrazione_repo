package com.alitalia.aem.service.impl.converter.homerest.beans;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetAllAirports implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SerializedName("airport")
    @Expose
	private List<AllAirports> airports;
	private String conversationID;

	public List<AllAirports> getAirports() {
		return airports;
	}

	public void setAirports(List<AllAirports> airports) {
		this.airports = airports;
	}

	public String getConversationID() {
		return conversationID;
	}

	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}

}
