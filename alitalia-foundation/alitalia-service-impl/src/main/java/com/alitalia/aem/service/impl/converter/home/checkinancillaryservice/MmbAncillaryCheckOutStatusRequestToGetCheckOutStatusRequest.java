package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutStatusRequest;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.GetCheckOutStatusRequest;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=MmbAncillaryCheckOutStatusRequestToGetCheckOutStatusRequest.class)
public class MmbAncillaryCheckOutStatusRequestToGetCheckOutStatusRequest implements
		Converter<MmbAncillaryCheckOutStatusRequest, GetCheckOutStatusRequest> {

	@Reference
	private MmbAncillaryPaymentDataToPayment mmbAncillaryPaymentDataConverter;

	@Override
	public GetCheckOutStatusRequest convert(MmbAncillaryCheckOutStatusRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		GetCheckOutStatusRequest destination = null;

		if (source != null) {
			destination = objectFactory.createGetCheckOutStatusRequest();

			destination.setCartID(source.getCartId());
			destination.setMachineName(objectFactory.createAddToCartRequestMachineName(source.getMachineName()));
			destination.setSessionId(objectFactory.createAddToCartRequestSessionId(source.getSessionId()));
			destination.setClient(objectFactory.createCheckOutEndRequestClient(source.getClient()));

		}

		return destination;
	}

}
