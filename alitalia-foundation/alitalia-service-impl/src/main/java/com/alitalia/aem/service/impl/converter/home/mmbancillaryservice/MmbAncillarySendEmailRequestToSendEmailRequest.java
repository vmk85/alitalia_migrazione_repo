package com.alitalia.aem.service.impl.converter.home.mmbancillaryservice;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.MmbAncillarySendEmailRequest;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.SendEmailRequest;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd3.ArrayOfstring;

@Component(immediate=true, metatype=false)
@Service(value=MmbAncillarySendEmailRequestToSendEmailRequest.class)
public class MmbAncillarySendEmailRequestToSendEmailRequest implements Converter<MmbAncillarySendEmailRequest, SendEmailRequest> {

	@Reference
	private MmbRequestBaseInfoDataToRequestBaseInfo mmbRequestBaseInfoDataConverter;
	
	@Override
	public SendEmailRequest convert(MmbAncillarySendEmailRequest source) {
		SendEmailRequest destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createSendEmailRequest();

			destination.setCartId(source.getCartId());
			destination.setClient(source.getClient());
			destination.setSessionId(objectFactory.createSendEmailRequestSessionId(source.getSessionId()));
			destination.setMachineName(objectFactory.createSendEmailRequestMachineName(source.getClient()));

			String sourcePaymentType = source.getPaymentType();
			if (sourcePaymentType != null)
				destination.setPaymentType(objectFactory.createSendEmailRequestPaymentType(sourcePaymentType));
			else
				destination.setPaymentType(objectFactory.createSendEmailRequestPaymentType(""));

			destination.setX003CPNRX003EKBackingField(source.getPnr());
			destination.setX003CInfoRequestX003EKBackingField(
					mmbRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			
			List<String> sourceRecipients = source.getRecipients();
			if (sourceRecipients != null && !sourceRecipients.isEmpty()) {
				com.alitalia.aem.ws.mmb.ancillaryservice.xsd3.ObjectFactory objectFactory3 = 
						new com.alitalia.aem.ws.mmb.ancillaryservice.xsd3.ObjectFactory();
				ArrayOfstring recipients = objectFactory3.createArrayOfstring();
				for (String sourceRecipient : sourceRecipients)
					recipients.getString().add(sourceRecipient);
				destination.setEmailAddresses(objectFactory.createSendEmailRequestEmailAddresses(recipients));
			}
		}

		return destination;
	}

}
