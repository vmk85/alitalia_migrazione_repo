package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingSaleTaxTotalData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionPricingSaleTaxTotal;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingSaleTaxTotalToRTDSPricingSaleTaxTotalData.class)
public class RTDSPricingSaleTaxTotalToRTDSPricingSaleTaxTotalData implements
		Converter<ResultTicketingDetailSolutionPricingSaleTaxTotal, 
					ResultTicketingDetailSolutionPricingSaleTaxTotalData> {

	@Override
	public ResultTicketingDetailSolutionPricingSaleTaxTotalData convert(
			ResultTicketingDetailSolutionPricingSaleTaxTotal source) {
		ResultTicketingDetailSolutionPricingSaleTaxTotalData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingSaleTaxTotalData();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
