package com.alitalia.aem.service.impl.converter.home.commonservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.SeatData;
import com.alitalia.aem.common.data.home.SeatMapRowData;
import com.alitalia.aem.ws.booking.commonservice.xsd7.Seat;
import com.alitalia.aem.ws.booking.commonservice.xsd7.SeatMapRow;

@Component(immediate=true, metatype=false)
@Service(value=SeatMapRowToSeatMapRowData.class)
public class SeatMapRowToSeatMapRowData 
		implements Converter<SeatMapRow, SeatMapRowData> {

	@Reference
	private SeatToSeatData seatConverter;
	
	@Override
	public SeatMapRowData convert(SeatMapRow source) {
		
		SeatMapRowData destination = null;
		
		if (source != null) {
		
			destination = new SeatMapRowData();
			
			destination.setNumber(
					source.getX003CNumberX003EKBackingField());
			
			List<SeatData> seats = new ArrayList<SeatData>();
			if (source.getX003CSeatsX003EKBackingField() != null &&
					source.getX003CSeatsX003EKBackingField().getSeat() != null) {
				for (Seat seat : source.getX003CSeatsX003EKBackingField().getSeat()) {
					seats.add(seatConverter.convert(seat));
				}
			}
			destination.setSeats(seats);
			
		}
		
		return destination;
		
	}

}
