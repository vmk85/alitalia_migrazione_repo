package com.alitalia.aem.service.impl.converter.home.staticdataservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PhonePrefixData;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixResponse;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.StaticDataResponse;
import com.alitalia.aem.ws.booking.staticdataservice.xsd5.PhonePrefix;

@Component(immediate=true, metatype=false)
@Service(value=StaticDataResponseToRetrievePhonePrefixResponseConverter.class)
public class StaticDataResponseToRetrievePhonePrefixResponseConverter implements Converter<StaticDataResponse, RetrievePhonePrefixResponse> {

	private static final Logger logger = LoggerFactory.getLogger(StaticDataResponseToRetrievePhonePrefixResponseConverter.class);

	@Reference
	private PhonePrefixToPhonePrefixData phonePrefixConverter;

	@Reference
	private StaticDataServiceJAXBContextFactory jaxbContextFactory;

	@SuppressWarnings("unchecked")
	@Override
	public RetrievePhonePrefixResponse convert(StaticDataResponse source) {
		RetrievePhonePrefixResponse response = new RetrievePhonePrefixResponse();
		try {
			List<PhonePrefixData> phonePrefixsData = new ArrayList<>();
			
			Unmarshaller phonePrefixUnmashaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			JAXBElement<PhonePrefix> unmarshalledObj = null;

			List<Object> phonePrefixs = source.getEntities().getValue().getAnyType();
			for(Object phonePrefix: phonePrefixs){
				unmarshalledObj = (JAXBElement<PhonePrefix>) phonePrefixUnmashaller.unmarshal((Node) phonePrefix);
				PhonePrefix castedPhonePrefix = unmarshalledObj.getValue();
				PhonePrefixData phonePrefixData = phonePrefixConverter.convert(castedPhonePrefix);
				phonePrefixsData.add(phonePrefixData);
			}
			
			response.setPhonePrefix(phonePrefixsData);
			
		} catch (JAXBException e) {
			logger.error("Error instantiating JAXBContext/Unmarshaller: {}", e);
		} finally {
			logger.info("Conversion executed successfully");
		}
		return response;			
		
	}
}