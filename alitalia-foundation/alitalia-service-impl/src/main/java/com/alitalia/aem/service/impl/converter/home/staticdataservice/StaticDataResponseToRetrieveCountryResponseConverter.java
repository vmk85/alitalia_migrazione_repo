package com.alitalia.aem.service.impl.converter.home.staticdataservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.messages.home.RetrieveCountriesResponse;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.StaticDataResponse;
import com.alitalia.aem.ws.booking.staticdataservice.xsd2.Country;

@Component(immediate = true, metatype = false)
@Service(value = StaticDataResponseToRetrieveCountryResponseConverter.class)
public class StaticDataResponseToRetrieveCountryResponseConverter implements Converter<StaticDataResponse, RetrieveCountriesResponse> {

	private static final Logger logger = LoggerFactory.getLogger(StaticDataResponseToRetrieveCountryResponseConverter.class);

	@Reference
	private CountryToCountryData countryConverter;
	
	@Reference
	private StaticDataServiceJAXBContextFactory jaxbContextFactory;

	@SuppressWarnings("unchecked")
	@Override
	public RetrieveCountriesResponse convert(StaticDataResponse source) {
		RetrieveCountriesResponse destination = new RetrieveCountriesResponse();
		try {
			List<CountryData> countriesData = new ArrayList<>();
			
			Unmarshaller countryUnmashaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			JAXBElement<Country> unmarshalledObj = null;

			List<Object> countries = source.getEntities().getValue().getAnyType();
			for (Object country : countries) {
				Country countrySource = null;
				if (country instanceof Country) {
					countrySource = (Country) country;
				} else {
					unmarshalledObj = (JAXBElement<Country>) countryUnmashaller.unmarshal((Node) country);
					countrySource = unmarshalledObj.getValue();
				}
				CountryData countryData = countryConverter.convert(countrySource);
				countriesData.add(countryData);
			}
			
			destination.setCountries(countriesData);
			
		} catch (JAXBException e) {
			logger.error("Error instantiating JAXBContext/Unmarshaller: {}", e);
		} finally {
			logger.info("Conversion executed successfully");
		}
		return destination;			
	}
}