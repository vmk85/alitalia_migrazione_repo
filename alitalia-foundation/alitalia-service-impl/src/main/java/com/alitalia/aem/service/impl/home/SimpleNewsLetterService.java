package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.RegisterUserToAgencyNewsletterRequest;
import com.alitalia.aem.common.messages.home.RegisterUserToAgencyNewsletterResponse;
import com.alitalia.aem.service.api.home.NewsletterService;
import com.alitalia.aem.service.api.home.RetrieveNewsService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.newsletterservice.RegisterUserResponseToRegisterUserToAgencyNewsletterResponse;
import com.alitalia.aem.service.impl.converter.home.newsletterservice.RegisterUserToAgencyNewsletterRequestToRegisterUserRequest;
import com.alitalia.aem.ws.news.newsletterservice.NewsletterServiceClient;
import com.alitalia.aem.ws.news.newsletterservice.xsd1.RegisterUserRequest;
import com.alitalia.aem.ws.news.newsletterservice.xsd1.RegisterUserResponse;

@Service
@Component(immediate = true, metatype = false)
public class SimpleNewsLetterService implements NewsletterService {

	private static final Logger logger = LoggerFactory.getLogger(RetrieveNewsService.class);

	@Reference
	private NewsletterServiceClient newsletterServiceClientClient;
	
	@Reference
	private RegisterUserToAgencyNewsletterRequestToRegisterUserRequest registerUserRequestConverter;
	
	@Reference
	private RegisterUserResponseToRegisterUserToAgencyNewsletterResponse registerUserResponseConverter;

	@Override
	public RegisterUserToAgencyNewsletterResponse registerUserToAgencyNewsletter(RegisterUserToAgencyNewsletterRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method registerUserToAgencyNewsletter. The request is {}", request);
		}
		

		try {

			RegisterUserToAgencyNewsletterResponse response = new RegisterUserToAgencyNewsletterResponse();
			RegisterUserRequest registerUserRequest = registerUserRequestConverter.convert(request);
			RegisterUserResponse registerUserResponse = newsletterServiceClientClient.registerUser(registerUserRequest);
			response = registerUserResponseConverter.convert(registerUserResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
			
			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing registerUserToAgencyNewsletter: Sid ["+request.getSid()+"]" , e);
		}
	}
}