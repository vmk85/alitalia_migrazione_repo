package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryUpgradeDetailData;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.UpgradeDetail;


@Component(immediate=true, metatype=false)
@Service(value=UpgradeDetailToCheckinAncillaryUpgradeDetailData.class)
public class UpgradeDetailToCheckinAncillaryUpgradeDetailData implements 
			Converter<UpgradeDetail, CheckinAncillaryUpgradeDetailData> {

	@Override
	public CheckinAncillaryUpgradeDetailData convert(UpgradeDetail source) {
		CheckinAncillaryUpgradeDetailData destination = null;

		if (source != null) {
			destination = new CheckinAncillaryUpgradeDetailData();

			if (source.getX003CCompartimentalClassX003EKBackingField() != null)
				destination.setCompartimentalClass(MmbCompartimentalClassEnum.fromValue(source.getX003CCompartimentalClassX003EKBackingField().value()));
			destination.setSeat(source.getX003CSeatX003EKBackingField());
		}

		return destination;
	}
}