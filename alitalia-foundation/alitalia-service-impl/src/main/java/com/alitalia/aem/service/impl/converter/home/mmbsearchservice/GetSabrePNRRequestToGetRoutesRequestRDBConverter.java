package com.alitalia.aem.service.impl.converter.home.mmbsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.GetSabrePNRRequest;
import com.alitalia.aem.ws.mmb.searchservice.xsd1.GetRoutesRequestRDB;
import com.alitalia.aem.ws.mmb.searchservice.xsd1.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=GetSabrePNRRequestToGetRoutesRequestRDBConverter.class)
public class GetSabrePNRRequestToGetRoutesRequestRDBConverter implements
		Converter<GetSabrePNRRequest, GetRoutesRequestRDB> {

	@Reference
	private MmbRequestBaseInfoDataToRequestBaseInfo mmbRequestBaseInfoDataConverter;

	@Override
	public GetRoutesRequestRDB convert(GetSabrePNRRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		GetRoutesRequestRDB destination = null;

		if (source != null) {
			destination = objectFactory.createGetRoutesRequestRDB();

			destination.setLocale(objectFactory.createGetRoutesRequestRDBLocale(source.getLocale()));
			destination.setPnr(objectFactory.createGetRoutesRequestRDBPnr(source.getPnr()));

			destination.setX003CPNRX003EKBackingField(source.getPnr());
			destination.setX003CInfoRequestX003EKBackingField(mmbRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
		}

		return destination;
	}

}
