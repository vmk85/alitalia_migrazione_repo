package com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveCountriesRequest;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.DictionaryItem;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.ArrayOfDictionaryItem;

@Component(immediate=true, metatype=false)
@Service(value=RetrieveCountriesRequestToArrayOfDictionaryItemConverter.class)
public class RetrieveCountriesRequestToArrayOfDictionaryItemConverter implements Converter<RetrieveCountriesRequest, ArrayOfDictionaryItem> {

	@Override
	public ArrayOfDictionaryItem convert(RetrieveCountriesRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();		
		ArrayOfDictionaryItem destination = objectFactory.createArrayOfDictionaryItem();
		
		DictionaryItem parameterMarket = objectFactory.createDictionaryItem();
		parameterMarket.setKey(objectFactory.createDictionaryItemKey("Market"));
		parameterMarket.setValue(objectFactory.createDictionaryItemValue(source.getMarket()));
		destination.getDictionaryItem().add(parameterMarket);

		DictionaryItem parameterLanguageCode = objectFactory.createDictionaryItem();
		parameterLanguageCode.setKey(objectFactory.createDictionaryItemKey("LanguageCode"));
		parameterLanguageCode.setValue(objectFactory.createDictionaryItemValue(source.getLanguageCode()));
		destination.getDictionaryItem().add(parameterLanguageCode);
		
		return destination;
	}

}