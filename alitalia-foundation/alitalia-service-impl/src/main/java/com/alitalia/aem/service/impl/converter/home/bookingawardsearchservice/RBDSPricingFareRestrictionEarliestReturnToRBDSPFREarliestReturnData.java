//2
package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareRestrictionEarliestReturnData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetailsSolutionPricingFareRestrictionEarliestReturn;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingFareRestrictionEarliestReturnToRBDSPFREarliestReturnData.class)
public class RBDSPricingFareRestrictionEarliestReturnToRBDSPFREarliestReturnData
		implements Converter<ResultBookingDetailsSolutionPricingFareRestrictionEarliestReturn, ResultBookingDetailsSolutionPricingFareRestrictionEarliestReturnData> {

	@Override
	public ResultBookingDetailsSolutionPricingFareRestrictionEarliestReturnData convert(
			ResultBookingDetailsSolutionPricingFareRestrictionEarliestReturn source) {
		ResultBookingDetailsSolutionPricingFareRestrictionEarliestReturnData destination = null;
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingFareRestrictionEarliestReturnData();
			destination.setDateField(source.getDateField());
			destination.setTimeField(source.getTimeField());
		}
		return destination;
	}
	
	public ResultBookingDetailsSolutionPricingFareRestrictionEarliestReturnData convert(
			com.alitalia.aem.ws.bookingaward.searchservice.xsd13.ResultBookingDetailsSolutionPricingFareRestrictionEarliestReturn source) {
		ResultBookingDetailsSolutionPricingFareRestrictionEarliestReturnData destination = null;
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingFareRestrictionEarliestReturnData();
			destination.setDateField(source.getDateField());
			destination.setTimeField(source.getTimeField());
		}
		return destination;
	}

}
