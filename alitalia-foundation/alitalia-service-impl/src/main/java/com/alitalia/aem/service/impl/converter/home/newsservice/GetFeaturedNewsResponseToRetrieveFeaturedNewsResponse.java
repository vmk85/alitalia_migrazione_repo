package com.alitalia.aem.service.impl.converter.home.newsservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.NewsData;
import com.alitalia.aem.common.messages.home.RetrieveFeaturedNewsResponse;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.news.newsservice.xsd2.GetFeaturedNewsResponse;
import com.alitalia.aem.ws.news.newsservice.xsd5.ArrayOfNews;
import com.alitalia.aem.ws.news.newsservice.xsd5.News;

@Component(immediate = true, metatype = false)
@Service(value = GetFeaturedNewsResponseToRetrieveFeaturedNewsResponse.class)
public class GetFeaturedNewsResponseToRetrieveFeaturedNewsResponse implements Converter<GetFeaturedNewsResponse, RetrieveFeaturedNewsResponse> {

	@Override
	public RetrieveFeaturedNewsResponse convert(GetFeaturedNewsResponse source) {
		RetrieveFeaturedNewsResponse response = new RetrieveFeaturedNewsResponse();
		
		JAXBElement<ArrayOfNews> arrayOfNews = source.getFeaturedNews();
		List<News> news = arrayOfNews.getValue().getNews();
		
		List<NewsData> newsData = new ArrayList<>();
		for(News singleNews: news){
			NewsData singleNewsData = new NewsData();

			XMLGregorianCalendar dataPub = singleNews.getDataPub();
			if (dataPub != null)
				singleNewsData.setDataPub(XsdConvertUtils.parseCalendar(dataPub));

			singleNewsData.setId(singleNews.getId());
			singleNewsData.setTipologia(singleNews.getTipologia().getValue());
			singleNewsData.setTitolo(singleNews.getTitolo().getValue());
			
			newsData.add(singleNewsData);
		}
		
		response.setNews(newsData);
		
		return response;
	}
}