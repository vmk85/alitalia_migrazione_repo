package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMLifestyleData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Lifestyle;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd3.ArrayOfanyType;

@Component(immediate = true, metatype = false)
@Service(value = LifestylesToMMLifestylesData.class)
public class LifestylesToMMLifestylesData implements Converter<ArrayOfanyType, List<MMLifestyleData>> {

	private static Logger logger = LoggerFactory.getLogger(LifestylesToMMLifestylesData.class);
	
	@Reference
	private MyPersonalareaMillemigliaServiceJAXBContextFactory jaxbContextFactory;
	
	@Override
	public List<MMLifestyleData> convert(ArrayOfanyType source) {
		
		if (source == null) {
			return null;
		}

		List<MMLifestyleData> mmLifestylesData = new ArrayList<MMLifestyleData>();

		for (Object object: source.getAnyType()) {
			try {
				Unmarshaller unmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
				
				JAXBElement<Lifestyle> jaxbElement = (JAXBElement<Lifestyle>) unmarshaller.unmarshal((Node) object);
				Lifestyle lifestyle = jaxbElement.getValue();
				
				MMLifestyleData mmLifestyleData = new MMLifestyleData();
				mmLifestyleData.setCategory(lifestyle.getCategory().getValue());
				mmLifestyleData.setDescription(lifestyle.getDescription().getValue());
				mmLifestyleData.setLifestyle(lifestyle.getLifeStyle().getValue());

				if (lifestyle.getCreateDate() != null)
					mmLifestyleData.setCreateDate(XsdConvertUtils.parseCalendar(lifestyle.getCreateDate()));
				
				mmLifestylesData.add(mmLifestyleData);
				
			} catch (JAXBException e) {
				logger.error("Error while creating JAXBConext/Unmarshller for List<MMLifestyleData>: {}", e);
			}
		}
		return mmLifestylesData;
	}
}