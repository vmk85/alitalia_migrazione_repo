package com.alitalia.aem.service.impl.converter.home.mmbcommonservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveMmbFlightSeatMapRequest;
import com.alitalia.aem.ws.mmb.commonservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mmb.commonservice.xsd1.SeatMapRequest;
import com.alitalia.aem.ws.mmb.commonservice.xsd2.ArrayOfstring;

@Component(immediate=true, metatype=false)
@Service(value=RetrieveMmbFlightSeatMapRequestToSeatMapRequest.class)
public class RetrieveMmbFlightSeatMapRequestToSeatMapRequest implements Converter<RetrieveMmbFlightSeatMapRequest, SeatMapRequest> {

	@Reference
	private MmbFlightDataToFlight mmbFlightDataConverter;

	@Override
	public SeatMapRequest convert(RetrieveMmbFlightSeatMapRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		com.alitalia.aem.ws.mmb.commonservice.xsd2.ObjectFactory objectFactory2 =
				new com.alitalia.aem.ws.mmb.commonservice.xsd2.ObjectFactory();
		SeatMapRequest destination = objectFactory.createSeatMapRequest();

		
		ArrayOfstring compartimentalClassList = objectFactory2.createArrayOfstring();
		for(String sourceCompartimentalClass : source.getCompartimentalClass()) {
			compartimentalClassList.getString().add(sourceCompartimentalClass);
		}
		destination.setCompartimentalClass(objectFactory.createSeatMapRequestCompartimentalClass(compartimentalClassList ));

		destination.setFlightInfo(objectFactory.createSeatMapRequestFlightInfo(
				mmbFlightDataConverter.convert(source.getFlightInfo())));

		return destination;
	}

}
