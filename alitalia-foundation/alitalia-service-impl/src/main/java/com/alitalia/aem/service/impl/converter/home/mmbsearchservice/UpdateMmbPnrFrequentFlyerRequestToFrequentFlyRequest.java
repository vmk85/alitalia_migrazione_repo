package com.alitalia.aem.service.impl.converter.home.mmbsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrFrequentFlyerRequest;
import com.alitalia.aem.ws.mmb.searchservice.xsd1.FrequentFlyRequest;
import com.alitalia.aem.ws.mmb.searchservice.xsd1.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=UpdateMmbPnrFrequentFlyerRequestToFrequentFlyRequest.class)
public class UpdateMmbPnrFrequentFlyerRequestToFrequentFlyRequest implements
		Converter<UpdateMmbPnrFrequentFlyerRequest, FrequentFlyRequest> {

	@Reference
	private MmbRouteDataToRoute mmbRouteDataConverter;

	@Reference
	private MmbRequestBaseInfoDataToRequestBaseInfo mmbRequestBaseInfoDataConverter;

	@Override
	public FrequentFlyRequest convert(UpdateMmbPnrFrequentFlyerRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		FrequentFlyRequest destination = null;

		if (source != null) {
			destination = objectFactory.createFrequentFlyRequest();

			destination.setLastName(objectFactory.createPassengerListRequestLastName(source.getLastName()));
			destination.setName(objectFactory.createPassengerListRequestName(source.getName()));
			destination.setFrequentFlyerCarrier(
					objectFactory.createFrequentFlyRequestFrequentFlyerCarrier(source.getFrequentFlyerCarrier()));
			destination.setFrequentFlyerCode(
					objectFactory.createFrequentFlyRequestFrequentFlyerCode(source.getFrequentFlyerCode()));
			destination.setPnr(objectFactory.createFrequentFlyRequestPnr(source.getPnr()));
			destination.setTravelerRefNumber(
					objectFactory.createFrequentFlyRequestTravelerRefNumber(source.getTravelerRefNumber()));

			destination.setX003CPNRX003EKBackingField(null);
			destination.setX003CInfoRequestX003EKBackingField(mmbRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
		}

		return destination;
	}

}
