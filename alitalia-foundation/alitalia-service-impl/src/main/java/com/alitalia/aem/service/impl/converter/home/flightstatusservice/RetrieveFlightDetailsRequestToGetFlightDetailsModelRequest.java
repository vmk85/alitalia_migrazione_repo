package com.alitalia.aem.service.impl.converter.home.flightstatusservice;

import java.util.Calendar;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.FlightDetailsModelData;
import com.alitalia.aem.common.data.home.FlySegmentData;
import com.alitalia.aem.common.messages.home.RetrieveFlightDetailsRequest;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetFlightDetailsModelRequest;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.ArrayOfFlySegment;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.FlightDetailsModel;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.FlySegment;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.ObjectFactory;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveFlightDetailsRequestToGetFlightDetailsModelRequest.class)
public class RetrieveFlightDetailsRequestToGetFlightDetailsModelRequest implements Converter<RetrieveFlightDetailsRequest, GetFlightDetailsModelRequest> {

	@Override
	public GetFlightDetailsModelRequest convert(RetrieveFlightDetailsRequest source) {
		ObjectFactory requestFactory = new ObjectFactory();
		
		GetFlightDetailsModelRequest request = requestFactory.createGetFlightDetailsModelRequest();
		
		FlightDetailsModelData flightDetailsModelData = source.getFlightDetailsModelData();
		String airCraft = flightDetailsModelData.getAirCraft();
		Calendar flightData = flightDetailsModelData.getFlightDate();
		String flightNumber = flightDetailsModelData.getFlightNumber();
		List<FlySegmentData> flySegmentDatas = flightDetailsModelData.getFlySegments();
		String vector = flightDetailsModelData.getVector();
		
		com.alitalia.aem.ws.booking.flightstatusservice.xsd3.ObjectFactory elementFactory = new com.alitalia.aem.ws.booking.flightstatusservice.xsd3.ObjectFactory();
		
		FlightDetailsModel flighDetailsModel = elementFactory.createFlightDetailsModel();
		flighDetailsModel.setAirCraft(elementFactory.createFlightDetailsModelAirCraft(airCraft));
		if (flightData != null)
			flighDetailsModel.setFlightDate(XsdConvertUtils.toXMLGregorianCalendar(flightData));
		flighDetailsModel.setFlightNumber(elementFactory.createFlightDetailsModelFlightNumber(flightNumber));
		
		ArrayOfFlySegment flySegments = elementFactory.createArrayOfFlySegment();
		List<FlySegment> flySegmentList = flySegments.getFlySegment();
		if (flySegmentDatas != null) {
			for(FlySegmentData flySegmentData: flySegmentDatas){
				FlySegment flySegment = new FlySegment();
				flySegment.setBoardPoint(elementFactory.createFlySegmentBoardPoint(flySegmentData.getBoardPoint()));
				flySegment.setBoardPointDepTime(elementFactory.createFlySegmentBoardPointDepTime(flySegmentData.getBoardPointDepTime()));
				flySegment.setDaysFromStart(flySegmentData.getDaysFromStart());
				flySegment.setMilesFromStart(flySegmentData.getMilesFromStart());
				flySegment.setOffPoint(elementFactory.createFlySegmentOffPoint(flySegmentData.getOffPoint()));
				flySegment.setOffPointArrTime(elementFactory.createFlySegmentTimeFromStart(flySegmentData.getTimeFromStart()));
				flySegmentList.add(flySegment);
			}
		}
		JAXBElement<ArrayOfFlySegment>  jaxbFlySegments = elementFactory.createFlightDetailsModelFlySegment(flySegments);
		flighDetailsModel.setVector(elementFactory.createFlightDetailsModelVector(vector));
		flighDetailsModel.setFlySegment(jaxbFlySegments);		
		
		JAXBElement<FlightDetailsModel> jaxbFlightDetailsModel = requestFactory.createGetFlightDetailsModelRequestFlightDetails(flighDetailsModel);
		request.setFlightDetails(jaxbFlightDetailsModel);
		
		return request;
	}
}