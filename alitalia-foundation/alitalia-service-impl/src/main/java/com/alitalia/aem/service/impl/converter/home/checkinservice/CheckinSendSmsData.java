package com.alitalia.aem.service.impl.converter.home.checkinservice;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.CheckinSendSmsRequest;
import com.alitalia.aem.ws.sendsms.xsd.ObjectFactory;
import com.alitalia.aem.ws.sendsms.xsd.SmsMsg;
import com.alitalia.aem.ws.sendsms.xsd.SmsParametersType;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;


@Component(immediate = true, metatype = false)
@Service(value = CheckinSendSmsData.class)
public class CheckinSendSmsData implements Converter<CheckinSendSmsRequest, SmsMsg> {

    @Override
    public SmsMsg convert(CheckinSendSmsRequest source)
    {
        ObjectFactory factory = new ObjectFactory();
        SmsMsg sms = factory.createSmsMsg();
        sms.setFromPhoneNumber(source.getFromPhone());
        sms.setMessage(source.getLink());
        sms.setToPhoneNumber(source.getPhoneNumber());
        sms.setApplicationName(source.getApplicationName());
        SmsParametersType parameter = factory.createSmsParametersType();
        parameter.setNotificationSend(source.getNotificationSend());
        parameter.setNotificationLocalSend(source.getNotificationLocalSend());
        parameter.setNotificationReceive(source.getNotificationReceive());
        parameter.setNotificationNotReceive(source.getNotificationNotReceive());
        sms.setSmsParameters(parameter);
        return sms;
    }
}