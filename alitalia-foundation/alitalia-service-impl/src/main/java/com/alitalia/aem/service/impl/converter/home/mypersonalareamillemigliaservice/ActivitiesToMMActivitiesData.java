package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMActivityData;
import com.alitalia.aem.common.data.home.enumerations.MMActivityTypeEnum;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Activity;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd3.ArrayOfanyType;

@Component(immediate = true, metatype = false)
@Service(value = ActivitiesToMMActivitiesData.class)
public class ActivitiesToMMActivitiesData implements Converter<ArrayOfanyType, List<MMActivityData>> {

	private static Logger logger = LoggerFactory.getLogger(ActivitiesToMMActivitiesData.class);
	
	@Reference
	private MyPersonalareaMillemigliaServiceJAXBContextFactory jaxbContextFactory;

	@Override
	public List<MMActivityData> convert(ArrayOfanyType source) {
		
		if (source == null) {
			return null;
		}

		List<MMActivityData> mmActivities = new ArrayList<MMActivityData>();

		for (Object object: source.getAnyType()) {
			if(object != null){
				try {
					Unmarshaller unmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();

					JAXBElement<Activity> jaxbElement = (JAXBElement<Activity>) unmarshaller.unmarshal((Node) object);
					Activity activity = jaxbElement.getValue();

					MMActivityData mmActivityData = new MMActivityData();
					mmActivityData.setAmount(activity.getAmount());
					mmActivityData.setBasic(activity.getBasic());
					mmActivityData.setBonus(activity.getBonus());

					if(activity.getCustomerNumber() != null)
						mmActivityData.setCustomerNumber(activity.getCustomerNumber().getValue());

					if(activity.getDescription() != null)
						mmActivityData.setDescription(activity.getDescription().getValue());
					mmActivityData.setPointsTotal(activity.getPointsTotal());
					mmActivityData.setQualified(activity.isQualified());

					if(activity.getReferenceNumber() != null)
						mmActivityData.setReferenceNumber(activity.getReferenceNumber().getValue());

					if(activity.getRowID() != null)
						mmActivityData.setRowId(activity.getRowID().getValue());

					if(activity.getSequenceNumber() != null)
						mmActivityData.setSequenceNumber(activity.getSequenceNumber().getValue());

					if (activity.getStatementDate() != null)
						mmActivityData.setStatementDate(XsdConvertUtils.parseCalendar(activity.getStatementDate()));
					if (activity.getActivityDate() != null)
						mmActivityData.setActivityDate(XsdConvertUtils.parseCalendar(activity.getActivityDate()));
					if (activity.getActivityType() != null)
						mmActivityData.setActivityType(MMActivityTypeEnum.fromValue(activity.getActivityType().value()));

					mmActivities.add(mmActivityData);

				} catch (JAXBException e) {
					logger.error("Error while creating JAXBConext/Unmarshller for List<MMActivityData>: {}", e);
				}
			}
		}

		return mmActivities;
	}
}