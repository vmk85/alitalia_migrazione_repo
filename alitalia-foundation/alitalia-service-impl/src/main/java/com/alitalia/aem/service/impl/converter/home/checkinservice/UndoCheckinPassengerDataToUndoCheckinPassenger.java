package com.alitalia.aem.service.impl.converter.home.checkinservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.UndoCheckinPassengerData;
import com.alitalia.aem.ws.checkin.checkinservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.checkinservice.xsd1.UndoRequestPassenger;
import com.alitalia.aem.ws.checkin.checkinservice.xsd4.ArrayOfstring;



@Component(immediate=true, metatype=false)
@Service(value=UndoCheckinPassengerDataToUndoCheckinPassenger.class)
public class UndoCheckinPassengerDataToUndoCheckinPassenger implements Converter<UndoCheckinPassengerData, UndoRequestPassenger> {

	@Override
	public UndoRequestPassenger convert(UndoCheckinPassengerData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		UndoRequestPassenger destination = null;

		if (source != null) {
			destination = objectFactory.createUndoRequestPassenger();
			destination.setEticket(objectFactory.createUndoRequestPassengerEticket(source.getEticket()));
			destination.setLastname(objectFactory.createUndoRequestPassengerLastname(source.getLastname()));
			destination.setName(objectFactory.createUndoRequestPassengerName(source.getName()));
			
			com.alitalia.aem.ws.checkin.checkinservice.xsd4.ObjectFactory objectFactory4 = new com.alitalia.aem.ws.checkin.checkinservice.xsd4.ObjectFactory();
			ArrayOfstring messagesList = objectFactory4.createArrayOfstring();
			if (source.getMessages() != null){
				for (String mess : source.getMessages()) {
					messagesList.getString().add(mess);
				}
			}
			destination.setMessages(objectFactory.createUndoRequestPassengerMessages(messagesList));
		}
		return destination;
	}
}