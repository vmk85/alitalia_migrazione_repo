package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.BaseResponse;
import com.alitalia.aem.ws.booking.paymentservice.xsd1.InitializePaymentResponse;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.PaymentOfanyTypeanyType;

@Component(metatype=false, immediate=true)
@Service(value=InitializePaymentResponseConverter.class)
public class InitializePaymentResponseConverter extends BaseResponse implements Converter<InitializePaymentResponse, com.alitalia.aem.common.messages.home.InitializePaymentResponse>  {

	private final static Logger logger = LoggerFactory.getLogger(InitializePaymentResponseConverter.class);
	
	@Reference
	private PaymentToPaymentData paymentConverter;
	
	@Reference
	private BookingPaymentServiceJAXBContextFactory jaxbContextFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public com.alitalia.aem.common.messages.home.InitializePaymentResponse convert(InitializePaymentResponse source) {
	  com.alitalia.aem.common.messages.home.InitializePaymentResponse response = new com.alitalia.aem.common.messages.home.InitializePaymentResponse();
		try{
			Unmarshaller paymentUnmashaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			JAXBElement<PaymentOfanyTypeanyType> unmarshalledObj = null;
			Object payment = source.getPayment().getValue();
			unmarshalledObj = (JAXBElement<PaymentOfanyTypeanyType>) paymentUnmashaller.unmarshal((Node) payment);
			PaymentOfanyTypeanyType castedPayment = unmarshalledObj.getValue();
			response.setPaymentData(paymentConverter.convert(castedPayment));
			response.setCookie(castedPayment.getCookie().getValue());
			response.setExecute(castedPayment.getExecution().getValue());
			response.setSabreGateWayAuthToken(castedPayment.getSabreGateWayAuthToken().getValue());

		} catch (JAXBException e) {
			logger.error("Error instantiating JAXBContext/Unmarshaller: {}", e);
		} finally {
			logger.info("Conversion executed successfully");
		}
		
		return response;
		
		
	} 

}
