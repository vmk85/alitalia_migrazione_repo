package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowanceData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowance;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingCheckedBaggageAllowanceFreeBaggageAllowanceToRTDSPricingCheckedBaggageAllowanceFreeBaggageAllowanceData.class)
public class RTDSPricingCheckedBaggageAllowanceFreeBaggageAllowanceToRTDSPricingCheckedBaggageAllowanceFreeBaggageAllowanceData implements
		Converter<ResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowance, 
					ResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowanceData> {

	@Override
	public ResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowanceData convert(
			ResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowance source) {
		ResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowanceData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingCheckedBaggageAllowanceFreeBaggageAllowanceData();

			destination.setBagDescriptorField(null);

			destination.setKilosField(source.getKilosField());
			destination.setKilosFieldSpecified(source.isKilosFieldSpecified());
			destination.setKilosPerPieceField(source.getKilosPerPieceField());
			destination.setKilosPerPieceFieldSpecified(source.isKilosPerPieceFieldSpecified());
			destination.setPiecesField(source.getPiecesField());
			destination.setPiecesFieldSpecified(source.isPiecesFieldSpecified());
			destination.setPoundsField(source.getPoundsField());
			destination.setPoundsFieldSpecified(source.isPoundsFieldSpecified());
		}

		return destination;
	}

}
