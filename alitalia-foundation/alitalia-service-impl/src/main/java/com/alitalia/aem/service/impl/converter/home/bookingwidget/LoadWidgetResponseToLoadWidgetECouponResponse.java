package com.alitalia.aem.service.impl.converter.home.bookingwidget;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.messages.home.LoadWidgetECouponResponse;
import com.alitalia.aem.ws.booking.widgetservice.xsd1.LoadECouponResponse;
import com.alitalia.aem.ws.booking.widgetservice.xsd1.LoadWidgetResponse;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.APassengerBase;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.ECoupon;

@Component(immediate=true, metatype=false)
@Service(value=LoadWidgetResponseToLoadWidgetECouponResponse.class)
public class LoadWidgetResponseToLoadWidgetECouponResponse implements Converter<LoadWidgetResponse, LoadWidgetECouponResponse> {

	private static final Logger logger = LoggerFactory.getLogger(LoadWidgetResponseToLoadWidgetECouponResponse.class);

	@Reference
	private PassengerBaseToAPassengerBaseData passengerBaseConverter;

	@Reference
	private ECouponToECouponData eCouponConverter;

	@Reference
	private BookingWidgetServiceJAXBContextFactory jaxbContextFactory;

	@Override
	public LoadWidgetECouponResponse convert(LoadWidgetResponse source) {
		LoadWidgetECouponResponse destination = new LoadWidgetECouponResponse();

		JAXBElement<Object> sourceLoadECouponResponseElement = source.getWidget();
		if (sourceLoadECouponResponseElement != null && sourceLoadECouponResponseElement.getValue() != null) {
			Object sourceLoadECouponResponse = sourceLoadECouponResponseElement.getValue();
			LoadECouponResponse loadECouponResponse = new LoadECouponResponse();
			if (sourceLoadECouponResponseElement.getValue() instanceof LoadECouponResponse || sourceLoadECouponResponseElement.getValue() instanceof LoadWidgetResponse) {
				logger.debug("sourceLoadECouponResponseElement.getValue() is instance of {}", sourceLoadECouponResponse);
				loadECouponResponse = (LoadECouponResponse) sourceLoadECouponResponse;
			} else {
				logger.debug("sourceLoadECouponResponseElement.getValue() is not instance of {}", sourceLoadECouponResponse);
//				try {
//					Unmarshaller unmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
//					@SuppressWarnings("unchecked")
//					JAXBElement<ECoupon> loadECouponResponseElement = 
//							(JAXBElement<ECoupon>) unmarshaller.unmarshal((Node) sourceLoadECouponResponse);
					loadECouponResponse.setWidget(sourceLoadECouponResponseElement);
//				} catch (JAXBException e) {
//					logger.error("Error creating JAXBContext/Unmarshaller for LoadECouponResponse class: {}", e);
//				}
			}
			if (loadECouponResponse != null) {
				JAXBElement<Object> sourceWidget = loadECouponResponse.getWidget();
				if (sourceWidget != null && sourceWidget.getValue() != null) {
					ECoupon eCouponSource = null;
					if (sourceWidget.getValue() instanceof ECoupon) {
						eCouponSource = (ECoupon) sourceWidget.getValue();
					} else {
						try {
							Unmarshaller unmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
							@SuppressWarnings("unchecked")
							JAXBElement<ECoupon> eCouponElement = 
									(JAXBElement<ECoupon>) unmarshaller.unmarshal((Node) sourceWidget.getValue());
							eCouponSource = eCouponElement.getValue();
						} catch (JAXBException e) {
							logger.error("Error creating JAXBContext/Unmarshaller for LoadECouponResponse class: {}", e);
						}
					}
					destination.seteCoupon(eCouponConverter.convert(eCouponSource));
				}
				
				if (source instanceof LoadECouponResponse) {
					LoadECouponResponse eCouponResponse = (LoadECouponResponse) source;
					List<PassengerBaseData> passengers = null;
					if (eCouponResponse.getPassengers() != null && eCouponResponse.getPassengers().getValue() != null)
					{
						passengers = new ArrayList<PassengerBaseData>();
						for (APassengerBase sourcePassenger : eCouponResponse.getPassengers().getValue().getAPassengerBase()) {
							passengers.add(passengerBaseConverter.convert(sourcePassenger));
						}
					}
					destination.setPassengers(passengers);
					destination.setSabreGateWayAuthToken(eCouponResponse.getSabreGateWayAuthToken().getValue());
					destination.setCookie(eCouponResponse.getCookie().getValue());
					destination.setExecute(eCouponResponse.getExecution().getValue());
				}
			}
		}

		return destination;
	}
}
