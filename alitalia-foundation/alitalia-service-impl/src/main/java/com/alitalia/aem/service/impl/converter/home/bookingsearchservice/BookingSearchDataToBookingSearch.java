package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.PassengerBase;
import com.alitalia.aem.common.data.home.BookingSearchData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.PropertiesData;
import com.alitalia.aem.ws.booking.searchservice.xsd2.BookingSearch;
import com.alitalia.aem.ws.booking.searchservice.xsd3.APassengerBase;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ArrayOfAPassengerBase;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ArrayOfDictionaryItem;
import com.alitalia.aem.ws.booking.searchservice.xsd3.GatewayType;

@Component(immediate=true, metatype=false)
@Service(value=BookingSearchDataToBookingSearch.class)
public class BookingSearchDataToBookingSearch implements Converter<BookingSearchData, BookingSearch> {

	@Reference
	private PassengerBaseDataToAPassengerBase passengerBaseDataConverter;

	@Reference
	private PropertiesDataToArrayOfDictionaryItem propertiesDataConverter; 

	@Override
	public BookingSearch convert(BookingSearchData source) {
		com.alitalia.aem.ws.booking.searchservice.xsd2.ObjectFactory factory2 = 
				new com.alitalia.aem.ws.booking.searchservice.xsd2.ObjectFactory();
		com.alitalia.aem.ws.booking.searchservice.xsd3.ObjectFactory factory3 =
				new com.alitalia.aem.ws.booking.searchservice.xsd3.ObjectFactory();
		BookingSearch destination = factory2.createBookingSearch();

		PropertiesData sourceProperties = source.getProperties();
		if (sourceProperties != null) {
			ArrayOfDictionaryItem properties = propertiesDataConverter.convert(sourceProperties);
			destination.setProperties(factory3.createABoomBoxGenericInfoProperties(properties));
		}
		else
			destination.setProperties(factory3.createABoomBoxGenericInfoProperties(null));

		destination.setFarmId(factory3.createABoomBoxInfoFarmId(source.getFarmId()));
		destination.setId(factory3.createABoomBoxInfoId(source.getId()));
		destination.setSessionId(factory3.createABoomBoxInfoSessionId(source.getSessionId()));
		destination.setSolutionSet(factory3.createABoomBoxInfoSolutionSet(source.getSolutionSet()));
		destination.setSolutionId(factory2.createGateWayBrandSearchSolutionId(source.getSolutionId()));
		destination.setType(GatewayType.fromValue(source.getType().value()));
		destination.setBookingSolutionId(factory2.createBookingSearchBookingSolutionId(source.getBookingSolutionId()));
		destination.setIsMultiSlice(source.isMultiSlice());
		destination.setIsNeutral(source.isNeutral());

		ArrayOfAPassengerBase passengers = new ArrayOfAPassengerBase();
		for (PassengerBase sourcePassengerBase : source.getPassengers()) {
			APassengerBase aPassengerBase = passengerBaseDataConverter.convert((PassengerBaseData) sourcePassengerBase);
			passengers.getAPassengerBase().add(aPassengerBase);
		}
		destination.setPassengers(factory2.createGateWayBrandSearchPassengers(passengers));

		return destination;
	}

}
