package com.alitalia.aem.service.impl.converter.home.statisticsservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PassengerNumbersData;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd3.ObjectFactory;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd3.PassengerNumbers;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd3.PassengerType;


@Component(immediate=true, metatype=false)
@Service(value=PassengerNumbersDataToPassengerNumbersStatistics.class)
public class PassengerNumbersDataToPassengerNumbersStatistics implements Converter<PassengerNumbersData, PassengerNumbers> {

	@Override
	public PassengerNumbers convert(PassengerNumbersData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		PassengerNumbers destination = objectFactory.createPassengerNumbers();

		destination.setNumber(source.getNumber());
		destination.setType(PassengerType.fromValue(source.getPassengerType().value()));

		return destination;
	}
}
