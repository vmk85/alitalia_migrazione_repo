package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PaymentProviderBancaIntesaData;
import com.alitalia.aem.common.data.home.PaymentProviderBancoPostaData;
import com.alitalia.aem.common.data.home.PaymentProviderCreditCardData;
import com.alitalia.aem.common.data.home.PaymentProviderData;
import com.alitalia.aem.common.data.home.PaymentProviderFindomesticData;
import com.alitalia.aem.common.data.home.PaymentProviderGlobalCollectData;
import com.alitalia.aem.common.data.home.PaymentProviderInstalmentsBrazilData;
import com.alitalia.aem.common.data.home.PaymentProviderLottomaticaData;
import com.alitalia.aem.common.data.home.PaymentProviderMasterPassData;
import com.alitalia.aem.common.data.home.PaymentProviderPayAtTOData;
import com.alitalia.aem.common.data.home.PaymentProviderPayLaterData;
import com.alitalia.aem.common.data.home.PaymentProviderPayPalData;
import com.alitalia.aem.common.data.home.PaymentProviderPosteIDData;
import com.alitalia.aem.common.data.home.PaymentProviderZeroPaymentData;
import com.alitalia.aem.ws.booking.ticketservice.xsd2.ArrayOfstring;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.APaymentProviderOfanyType;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.BancaIntesaOfanyType;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.BancoPostaOfanyType;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.CreditCardOfanyTypeanyType;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.CreditCardType;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.FindomesticOfanyTypeanyType;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.GlobalCollectOfanyTypeanyType;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.GlobalCollectPaymentType;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.InstalmentsBrazilOfanyTypeanyType;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.LottomaticaOfanyType;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.MasterPassOfanyType;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ObjectFactory;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.PayAtTOOfanyTypeanyType;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.PayLaterOfanyTypeanyType;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.PayPalOfanyType;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.PosteIDOfanyType;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ZeroPaymentProviderOfanyTypeanyType;

@Component(immediate = true, metatype = false)
@Service(value = PaymentProviderDataToAPaymentProviderOfAnyType.class)
public class PaymentProviderDataToAPaymentProviderOfAnyType implements
		Converter<PaymentProviderData, APaymentProviderOfanyType> {

	@Reference
	private PaymentComunicationBaseDataToAComunicationBase paymentComunicationBaseDataToAComunicationBase;

	@Reference
	private UserInfoBaseToAUserInfo userInfoBaseToAUserInfoConverter;

	@Reference
	private ContactDataToContact contactDataToContactConverter;

	@Override
	public APaymentProviderOfanyType convert(PaymentProviderData source) {
		APaymentProviderOfanyType destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			com.alitalia.aem.ws.booking.ticketservice.xsd2.ObjectFactory objectFactory2 = new com.alitalia.aem.ws.booking.ticketservice.xsd2.ObjectFactory();

			if (source instanceof PaymentProviderBancaIntesaData) {
				destination = new BancaIntesaOfanyType();

			} else if (source instanceof PaymentProviderBancoPostaData) {
				destination = new BancoPostaOfanyType();
				
			} else if (source instanceof PaymentProviderZeroPaymentData) {
				destination = new ZeroPaymentProviderOfanyTypeanyType();
				
			} else if (source instanceof PaymentProviderPosteIDData) {
				destination = new PosteIDOfanyType();

			} else if (source instanceof PaymentProviderCreditCardData) {
				PaymentProviderCreditCardData typedSource = (PaymentProviderCreditCardData) source;
				CreditCardOfanyTypeanyType typedDestination = new CreditCardOfanyTypeanyType();
				destination = typedDestination;

				typedDestination.setCreditCardNumber(objectFactory
						.createCreditCardOfanyTypeanyTypeCreditCardNumber(typedSource.getCreditCardNumber()));
				typedDestination.setCVV(objectFactory.createCreditCardOfanyTypeanyTypeCVV(typedSource.getCvv()));
				typedDestination.setExpiryMonth(typedSource.getExpiryMonth());
				typedDestination.setExpiryYear(typedSource.getExpiryYear());
				typedDestination.setToken(objectFactory.createCreditCardOfanyTypeanyTypeToken(typedSource.getToken()));

				if (typedSource.getType() != null) {
					typedDestination.setType(CreditCardType.fromValue(typedSource.getType().value()));
				}

				if (typedSource.getUserInfo() != null) {
					typedDestination.setUserInfo(objectFactory
							.createCreditCardOfanyTypeanyTypeUserInfo(userInfoBaseToAUserInfoConverter
									.convert(typedSource.getUserInfo())));
				}

			} else if (source instanceof PaymentProviderFindomesticData) {
				PaymentProviderFindomesticData typedSource = (PaymentProviderFindomesticData) source;
				FindomesticOfanyTypeanyType typedDestination = new FindomesticOfanyTypeanyType();
				destination = typedDestination;

				if (typedSource.getUserInfo() != null) {
					typedDestination.setUserInfo(objectFactory
							.createFindomesticOfanyTypeanyTypeUserInfo(userInfoBaseToAUserInfoConverter
									.convert(typedSource.getUserInfo())));
				}
			} else if (source instanceof PaymentProviderGlobalCollectData) {
				PaymentProviderGlobalCollectData typedSource = (PaymentProviderGlobalCollectData) source;
				GlobalCollectOfanyTypeanyType typedDestination = new GlobalCollectOfanyTypeanyType();
				destination = typedDestination;

				if (typedSource.getUserInfo() != null) {
					typedDestination.setUserInfo(objectFactory
							.createGlobalCollectOfanyTypeanyTypeUserInfo(userInfoBaseToAUserInfoConverter
									.convert(typedSource.getUserInfo())));
				}

				if (typedSource.getType() != null) {
					typedDestination.setType(GlobalCollectPaymentType.fromValue(typedSource.getType().value()));
				}

				typedDestination.setInstallementsNumber(typedSource.getInstallementsNumber());
			} else if (source instanceof PaymentProviderInstalmentsBrazilData) {
				PaymentProviderInstalmentsBrazilData typedSource = (PaymentProviderInstalmentsBrazilData) source;
				InstalmentsBrazilOfanyTypeanyType typedDestination = new InstalmentsBrazilOfanyTypeanyType();
				destination = typedDestination;

				ArrayOfstring emailForMail = objectFactory2.createArrayOfstring();
				if (typedSource.getEmailForMail() != null) {
					for (String email : typedSource.getEmailForMail()) {
						emailForMail.getString().add(email);
					}
				}
				typedDestination
						.setEmailForMail(objectFactory.createPayLaterOfanyTypeanyTypeEmailForMail(emailForMail));
				typedDestination.setEmailForPnr(objectFactory.createPayLaterOfanyTypeanyTypeEmailForPnr(typedSource
						.getEmailForPnr()));

				if (typedSource.getOtherContact() != null) {
					typedDestination.setOtherContact(objectFactory
							.createPayLaterOfanyTypeanyTypeOtherContact(contactDataToContactConverter
									.convert(typedSource.getOtherContact())));
				}

				if (typedSource.getType() != null) {
					typedDestination.setType(CreditCardType.fromValue(typedSource.getType().value()));
				}
			} else if (source instanceof PaymentProviderLottomaticaData) {
				LottomaticaOfanyType typedDestination = new LottomaticaOfanyType();
				destination = typedDestination;
			} else if (source instanceof PaymentProviderMasterPassData) {
				MasterPassOfanyType typedDestination = new MasterPassOfanyType();
				destination = typedDestination;
			} else if (source instanceof PaymentProviderPayAtTOData) {
				PaymentProviderPayAtTOData typedSource = (PaymentProviderPayAtTOData) source;
				PayAtTOOfanyTypeanyType typedDestination = new PayAtTOOfanyTypeanyType();
				destination = typedDestination;

				ArrayOfstring emailForMail = objectFactory2.createArrayOfstring();
				if (typedSource.getEmailForMail() != null) {
					for (String email : typedSource.getEmailForMail()) {
						emailForMail.getString().add(email);
					}
				}
				typedDestination
						.setEmailForMail(objectFactory.createPayLaterOfanyTypeanyTypeEmailForMail(emailForMail));

				typedDestination.setEmailForPnr(objectFactory.createPayLaterOfanyTypeanyTypeEmailForPnr(typedSource
						.getEmailForPnr()));

				if (typedSource.getOtherContact() != null) {
					typedDestination.setOtherContact(objectFactory
							.createPayLaterOfanyTypeanyTypeOtherContact(contactDataToContactConverter
									.convert(typedSource.getOtherContact())));
				}
			} else if (source instanceof PaymentProviderPayLaterData) {
				PaymentProviderPayLaterData typedSource = (PaymentProviderPayLaterData) source;
				PayLaterOfanyTypeanyType typedDestination = new PayLaterOfanyTypeanyType();
				destination = typedDestination;

				ArrayOfstring emailForMail = objectFactory2.createArrayOfstring();
				if (typedSource.getEmailForMail() != null) {
					for (String email : typedSource.getEmailForMail()) {
						emailForMail.getString().add(email);
					}
				}
				typedDestination
						.setEmailForMail(objectFactory.createPayLaterOfanyTypeanyTypeEmailForMail(emailForMail));

				typedDestination.setEmailForPnr(objectFactory.createPayLaterOfanyTypeanyTypeEmailForPnr(typedSource
						.getEmailForPnr()));

				if (typedSource.getOtherContact() != null) {
					typedDestination.setOtherContact(objectFactory
							.createPayLaterOfanyTypeanyTypeOtherContact(contactDataToContactConverter
									.convert(typedSource.getOtherContact())));
				}
			} else if (source instanceof PaymentProviderPayPalData) {
				PayPalOfanyType typedDestination = new PayPalOfanyType();
				destination = typedDestination;
			} else {
				destination = new APaymentProviderOfanyType();
			}

			if (source.getComunication() != null) {
				destination.setComunication(objectFactory
						.createAPaymentProviderOfanyTypeComunication(paymentComunicationBaseDataToAComunicationBase
								.convert(source.getComunication())));
			}
		}

		return destination;
	}

}
