package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionDisplayFareTotalData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionDisplayFareTotal;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=RTDSDisplayFareTotalDataToRTDSDisplayFareTotal.class)
public class RTDSDisplayFareTotalDataToRTDSDisplayFareTotal implements
		Converter<ResultTicketingDetailSolutionDisplayFareTotalData, 
					ResultTicketingDetailSolutionDisplayFareTotal> {

	@Override
	public ResultTicketingDetailSolutionDisplayFareTotal convert(ResultTicketingDetailSolutionDisplayFareTotalData source) {
		ResultTicketingDetailSolutionDisplayFareTotal destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionDisplayFareTotal();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
