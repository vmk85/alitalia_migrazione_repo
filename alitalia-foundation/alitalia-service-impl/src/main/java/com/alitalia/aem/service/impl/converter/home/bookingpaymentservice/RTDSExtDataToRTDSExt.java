package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionExtData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionExt;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=RTDSExtDataToRTDSExt.class)
public class RTDSExtDataToRTDSExt implements
		Converter<ResultTicketingDetailSolutionExtData, ResultTicketingDetailSolutionExt> {

	@Override
	public ResultTicketingDetailSolutionExt convert(ResultTicketingDetailSolutionExtData source) {
		ResultTicketingDetailSolutionExt destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionExt();

			destination.setPrivateFareField(
					source.isPrivateFareField()!=null && source.isPrivateFareField());

			destination.setPrivateFareFieldSpecified(
					source.isPrivateFareFieldSpecified() != null &&
						source.isPrivateFareFieldSpecified());
		}

		return destination;
	}

}
