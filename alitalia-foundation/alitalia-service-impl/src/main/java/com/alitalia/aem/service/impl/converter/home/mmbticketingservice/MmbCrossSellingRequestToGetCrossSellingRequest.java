package com.alitalia.aem.service.impl.converter.home.mmbticketingservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.MmbCrossSellingsRequest;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd1.GetCrossSellingsRequest;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd2.Route;

@Component(immediate = true, metatype = false)
@Service(value = MmbCrossSellingRequestToGetCrossSellingRequest.class)
public class MmbCrossSellingRequestToGetCrossSellingRequest implements
		Converter<MmbCrossSellingsRequest, GetCrossSellingsRequest> {

	@Reference
	private MmbRouteDataToRoute mmbRouteDataConverter;

	@Override
	public GetCrossSellingsRequest convert(MmbCrossSellingsRequest source) {
		GetCrossSellingsRequest destination = null;

		if (source != null) {
			ObjectFactory objectFactory1 = new ObjectFactory();

			destination = objectFactory1.createGetCrossSellingsRequest();
			destination.setIpAddress(objectFactory1.createGetCrossSellingsRequestIpAddress(source.getIpAddress()));
			destination.setLanguageCode(objectFactory1.createGetCrossSellingsRequestLanguageCode(source
					.getLanguageCode()));
			destination.setMarketCode(objectFactory1.createGetCrossSellingsRequestMarketCode(source.getMarketCode()));
			destination.setUserAgent(objectFactory1.createGetCrossSellingsRequestUserAgent(source.getUserAgent()));

			JAXBElement<Route> value = null;
			if (source.getPrenotation() != null) {
				Route routes = mmbRouteDataConverter.convert(source.getPrenotation());
				value = objectFactory1.createGetCrossSellingsRequestPrenotation(routes);
			}
			destination.setPrenotation(value);
		}

		return destination;
	}

}
