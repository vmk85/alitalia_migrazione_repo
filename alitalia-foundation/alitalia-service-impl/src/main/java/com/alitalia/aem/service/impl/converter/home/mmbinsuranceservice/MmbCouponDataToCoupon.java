package com.alitalia.aem.service.impl.converter.home.mmbinsuranceservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbGdsTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbSeatStatusEnum;
import com.alitalia.aem.common.data.home.mmb.MmbCouponData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd3.CompartimentalClass;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd3.Coupon;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd3.GdsType;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd3.ObjectFactory;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd3.SeatStatus;

@Component(immediate=true, metatype=false)
@Service(value=MmbCouponDataToCoupon.class)
public class MmbCouponDataToCoupon implements Converter<MmbCouponData, Coupon> {

	@Reference
	private MmbFlightDataToFlight mmbFlightDataConverter;

	@Override
	public Coupon convert(MmbCouponData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		Coupon destination = null;

		if (source != null) {
			destination = objectFactory.createCoupon();

			// Il campo Barcode dovrebbe contenere l'immagine del codice a barre ma non e' usato
			destination.setX003CBarcodeX003EKBackingField(null);
			destination.setX003CBoardingPassColorX003EKBackingField(source.getBoardingPassColor());
			destination.setX003CBoardingTimeX003EKBackingField(
					XsdConvertUtils.toXMLGregorianCalendar(source.getBoardingTime()));
			destination.setX003CEticketX003EKBackingField(source.getEticket());
			destination.setX003CFlightIdX003EKBackingField(source.getFlightId());
			destination.setX003CGateX003EKBackingField(source.getGate());

			MmbGdsTypeEnum sourceCouponType = source.getGdsCouponType();
			if (sourceCouponType != null)
				destination.setX003CGdsCouponTypeX003EKBackingField(GdsType.fromValue(sourceCouponType.value()));

			destination.setX003CIdX003EKBackingField(source.getId());
			destination.setX003CIsComfortSeatPaidX003EKBackingField(source.isComfortSeatPaid());
			destination.setX003CIsInfantCouponX003EKBackingField(source.iInfantCoupon());
			destination.setX003CIsSkyPriorityX003EKBackingField(source.isSkyPriority());
			destination.setX003COldSeatX003EKBackingField(source.getOldSeat());
			destination.setX003COrderNumberX003EKBackingField(source.getOrderNumber());
			destination.setX003CPassengerIdX003EKBackingField(source.getPassengerId());
			destination.setX003CPreviousSeatX003EKBackingField(source.getPreviousSeat());
			destination.setX003CSeatClassNameX003EKBackingField(source.getSeatClassName());

			MmbCompartimentalClassEnum sourceSeatClass = source.getSeatClass();
			if (sourceSeatClass != null)
				destination.setX003CSeatClassX003EKBackingField(CompartimentalClass.fromValue(sourceSeatClass.value()));

			destination.setX003CSeatX003EKBackingField(source.getSeat());
			destination.setX003CSequenceNumberX003EKBackingField(source.getSequenceNumber());
			destination.setX003CSocialCodeX003EKBackingField(source.getSocialCode());
			destination.setX003CSpecialFareX003EKBackingField(source.getSpecialFare());
			destination.setX003CSSRCodeX003EKBackingField(source.getSsrCode());

			MmbSeatStatusEnum sourceStatus = source.getStatus();
			if (sourceStatus != null)
				destination.setX003CStatusX003EKBackingField(SeatStatus.fromValue(sourceStatus.value()));

			destination.setX003CTerminalX003EKBackingField(source.getTerminal());

			destination.setX003CFlightX003EKBackingField(mmbFlightDataConverter.convert(source.getFlight()));
		}

		return destination;
	}

}
