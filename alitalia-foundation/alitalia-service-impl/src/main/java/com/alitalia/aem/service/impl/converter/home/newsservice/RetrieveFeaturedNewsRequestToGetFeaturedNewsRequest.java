package com.alitalia.aem.service.impl.converter.home.newsservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveFeaturedNewsRequest;
import com.alitalia.aem.ws.news.newsservice.xsd2.GetFeaturedNewsRequest;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveFeaturedNewsRequestToGetFeaturedNewsRequest.class)
public class RetrieveFeaturedNewsRequestToGetFeaturedNewsRequest implements Converter<RetrieveFeaturedNewsRequest, GetFeaturedNewsRequest> {

	@Override
	public GetFeaturedNewsRequest convert(RetrieveFeaturedNewsRequest source) {
		
		GetFeaturedNewsRequest request = new GetFeaturedNewsRequest();
		request.setPubTest(source.isPubTest());
		
		return request;
	}
}