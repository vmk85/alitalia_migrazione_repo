package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareEndorsementData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionPricingFareEndorsement;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareEndorsementToRTDSPricingFareEndorsementData.class)
public class RTDSPricingFareEndorsementToRTDSPricingFareEndorsementData implements
		Converter<ResultTicketingDetailSolutionPricingFareEndorsement, 
					ResultTicketingDetailSolutionPricingFareEndorsementData> {

	@Override
	public ResultTicketingDetailSolutionPricingFareEndorsementData convert(
			ResultTicketingDetailSolutionPricingFareEndorsement source) {
		ResultTicketingDetailSolutionPricingFareEndorsementData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingFareEndorsementData();

			destination.setBoxesField(source.getBoxesField());
		}

		return destination;
	}

}
