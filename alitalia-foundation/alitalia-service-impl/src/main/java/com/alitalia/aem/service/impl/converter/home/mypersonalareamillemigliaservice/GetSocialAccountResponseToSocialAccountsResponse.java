package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.SocialAccountData;
import com.alitalia.aem.common.messages.home.SocialAccountsResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.GetSocialAccountResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ArrayOfSocialAccount;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.SocialAccount;

@Component(immediate = true, metatype = false)
@Service(value = GetSocialAccountResponseToSocialAccountsResponse.class)
public class GetSocialAccountResponseToSocialAccountsResponse 
implements Converter<GetSocialAccountResponse, SocialAccountsResponse> {

	@Override
	public SocialAccountsResponse convert(GetSocialAccountResponse source) {
		SocialAccountsResponse  destination = new SocialAccountsResponse();

		if (source != null) {

			List<SocialAccountData> socialAccountsData = new ArrayList<SocialAccountData>();

			JAXBElement<ArrayOfSocialAccount> jaxbAccounts = source.getSocialAccounts();
			if(jaxbAccounts != null && jaxbAccounts.getValue() != null){
				
				ArrayOfSocialAccount arrayOfSocialAccounts = jaxbAccounts.getValue();
				for(SocialAccount account: arrayOfSocialAccounts.getSocialAccount()){
					SocialAccountData accountData = new SocialAccountData();
					accountData.setEmail(account.getEmail().getValue());
					accountData.setGigyaUID(account.getGigyaUID().getValue());
					accountData.setLogicalDelete(account.isLogicalDelete());
					accountData.setLoginProvider(account.getLoginProvider().getValue());
					socialAccountsData.add(accountData);
				}
			}

			destination.setSocialAccounts(socialAccountsData);
		}
		return destination;
	}
}