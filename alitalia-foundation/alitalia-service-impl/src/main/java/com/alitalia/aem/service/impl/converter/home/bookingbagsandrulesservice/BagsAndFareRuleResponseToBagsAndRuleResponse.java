package com.alitalia.aem.service.impl.converter.home.bookingbagsandrulesservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.messages.home.BagsAndRuleResponse;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd4.BagsAndFareRuleResponse;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd4.Brand;

@Component(immediate = true, metatype = false)
@Service(value = BagsAndFareRuleResponseToBagsAndRuleResponse.class)
public class BagsAndFareRuleResponseToBagsAndRuleResponse implements Converter<BagsAndFareRuleResponse, BagsAndRuleResponse> {

	@Reference
	private BrandToBrandData brandDataConverter;
	
	@Reference
	private BookingBagsAndFareRulesServiceJAXBContextFactory jaxbContextFactory;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public BagsAndRuleResponse convert(BagsAndFareRuleResponse source) {
		BagsAndRuleResponse destination = null;

		if (source != null) {
			destination = new BagsAndRuleResponse();
			destination.setCookie(source.getCookie().getValue());
			destination.setExecute(source.getExecution().getValue());
			List<BrandData> brands = null;
			if (source.getBrands() != null && source.getBrands().getValue() != null && source.getBrands().getValue().getAnyType() != null
					&& source.getBrands().getValue().getAnyType().size() > 0){
				brands = new ArrayList<>();
				for (Object brand : source.getBrands().getValue().getAnyType()){
					if (brand instanceof Brand)
						brands.add(brandDataConverter.convert((Brand) brand));
					else {
						// Unmarshalling
						try {
							Unmarshaller brandUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
							@SuppressWarnings("rawtypes")
							JAXBElement jaxbBrand = (JAXBElement) brandUnmarshaller.unmarshal((Node) brand);
							brands.add(brandDataConverter.convert((Brand) (jaxbBrand.getValue())));
						} catch (JAXBException e) {
							logger.error("Error while creating JAXBConext/Unmarshller for Brand: {}", e);
						}
					}
				}
			}
			destination.setBrands(brands);
		}
		return destination;
	}

}
