package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareRestrictionEarliestReturnData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ResultBookingDetailsSolutionPricingFareRestrictionEarliestReturn;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingFareRestrictionEarliestReturnDataToRBDSPFREarliestReturn.class)
public class RBDSPricingFareRestrictionEarliestReturnDataToRBDSPFREarliestReturn
		implements Converter<ResultBookingDetailsSolutionPricingFareRestrictionEarliestReturnData, ResultBookingDetailsSolutionPricingFareRestrictionEarliestReturn> {

	@Override
	public ResultBookingDetailsSolutionPricingFareRestrictionEarliestReturn convert(
			ResultBookingDetailsSolutionPricingFareRestrictionEarliestReturnData source) {
		ResultBookingDetailsSolutionPricingFareRestrictionEarliestReturn destination = null;
		if(source!=null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionPricingFareRestrictionEarliestReturn();
			destination.setDateField(source.getDateField());
			destination.setTimeField(source.getTimeField());
		}
		return destination;
	}

}
