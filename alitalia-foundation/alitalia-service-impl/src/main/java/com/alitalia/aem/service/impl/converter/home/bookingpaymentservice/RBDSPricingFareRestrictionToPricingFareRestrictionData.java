//2
package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareRestrictionData;
import com.alitalia.aem.ws.booking.paymentservice.xsd11.ResultBookingDetailsSolutionPricingFareRestriction;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingFareRestrictionToPricingFareRestrictionData.class)
public class RBDSPricingFareRestrictionToPricingFareRestrictionData implements
		Converter<ResultBookingDetailsSolutionPricingFareRestriction, ResultBookingDetailsSolutionPricingFareRestrictionData> {

	@Reference
	RBDSPricingFareRestrictionEarliestReturnToRBDSPFREarliestReturnData earliestReturnFieldConverter;
	
	@Reference
	RBDSPricingFareRestrictionLatestReturnToRBDSPFRestrictionLatestReturnData latestReturnFieldConverter;
	
	@Override
	public ResultBookingDetailsSolutionPricingFareRestrictionData convert(
			ResultBookingDetailsSolutionPricingFareRestriction source) {
		ResultBookingDetailsSolutionPricingFareRestrictionData destination = null;
		
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingFareRestrictionData();
			destination.setEarliestReturnField(earliestReturnFieldConverter.convert(source.getEarliestReturnField()));
			destination.setLatestReturnField(latestReturnFieldConverter.convert(source.getLatestReturnField()));
		}
		return destination;
	}
		
}
