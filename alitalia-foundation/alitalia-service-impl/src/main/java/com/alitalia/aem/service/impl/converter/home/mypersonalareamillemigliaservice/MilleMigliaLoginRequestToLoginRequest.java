package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.MilleMigliaLoginRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.LoginRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;

@Component(immediate = true, metatype = false)
@Service(value = MilleMigliaLoginRequestToLoginRequest.class)
public class MilleMigliaLoginRequestToLoginRequest 
		implements Converter<MilleMigliaLoginRequest, LoginRequest> {

	@Override
	public LoginRequest convert(MilleMigliaLoginRequest source) {
		LoginRequest destination = null;

		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			destination = objectFactory.createLoginRequest();
			
			destination.setCustomerCode(
					objectFactory.createLoginRequestCustomerCode(
							source.getCustomerCode()));
			
			destination.setCustomerPIN(
					objectFactory.createLoginRequestCustomerPIN(
							source.getCustomerPin()));
		}

		return destination;
	}

}
