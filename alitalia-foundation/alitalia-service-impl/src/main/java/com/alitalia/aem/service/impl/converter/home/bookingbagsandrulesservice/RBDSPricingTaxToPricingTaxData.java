package com.alitalia.aem.service.impl.converter.home.bookingbagsandrulesservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingTaxData;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetailsSolutionPricingTax;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingTaxToPricingTaxData.class)
public class RBDSPricingTaxToPricingTaxData implements Converter<ResultBookingDetailsSolutionPricingTax, ResultBookingDetailsSolutionPricingTaxData> {

	@Reference
	private RBDSPricingTaxPreviousSalePriceToPricingTaxPreviousSalePriceData pricingTaxPreviousSalePriceConverter;
	
	@Reference
	private RBDSPricingTaxSalePriceToPricingTaxSalePriceData pricingTaxSalePriceConverter;

	@Override
	public ResultBookingDetailsSolutionPricingTaxData convert(
			ResultBookingDetailsSolutionPricingTax source) {
		ResultBookingDetailsSolutionPricingTaxData destination = null;
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingTaxData();
			destination.setCodeField(source.getCodeField());
			destination.setPreviousSalePriceField(pricingTaxPreviousSalePriceConverter.convert(source.getPreviousSalePriceField()));
			destination.setSalePriceField(pricingTaxSalePriceConverter.convert(source.getSalePriceField()));
		}
		return destination;
	}
	
	public ResultBookingDetailsSolutionPricingTaxData convert(
			com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetailsSolutionPricingTax source) {
		ResultBookingDetailsSolutionPricingTaxData destination = null;
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingTaxData();
			destination.setCodeField(source.getCodeField());
			destination.setPreviousSalePriceField(pricingTaxPreviousSalePriceConverter.convert(source.getPreviousSalePriceField()));
			destination.setSalePriceField(pricingTaxSalePriceConverter.convert(source.getSalePriceField()));
		}
		return destination;
	}

}
