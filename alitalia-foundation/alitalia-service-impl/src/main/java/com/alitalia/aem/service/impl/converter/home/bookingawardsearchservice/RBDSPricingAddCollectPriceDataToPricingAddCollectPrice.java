package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingAddCollectPriceData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetailsSolutionPricingAddCollectPrice;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingAddCollectPriceDataToPricingAddCollectPrice.class)
public class RBDSPricingAddCollectPriceDataToPricingAddCollectPrice implements
		Converter<ResultBookingDetailsSolutionPricingAddCollectPriceData, ResultBookingDetailsSolutionPricingAddCollectPrice> {

	@Override
	public ResultBookingDetailsSolutionPricingAddCollectPrice convert(
			ResultBookingDetailsSolutionPricingAddCollectPriceData source) {
		ResultBookingDetailsSolutionPricingAddCollectPrice destination = null;
		if(source!=null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionPricingAddCollectPrice();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}
	
}
