package com.alitalia.aem.service.impl.converter.home.staticdataservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemRequest;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.StaticDataRequest;
import com.alitalia.aem.ws.booking.staticdataservice.xsd2.StaticDataType;


@Component(immediate = true, metatype = false)
@Service(value = RetrievePaymentTypeItemsRequestToStaticDataRequestConverter.class)
public class RetrievePaymentTypeItemsRequestToStaticDataRequestConverter implements Converter<RetrievePaymentTypeItemRequest, StaticDataRequest> {
	
	@Override
	public StaticDataRequest convert(RetrievePaymentTypeItemRequest source) {
		StaticDataRequest request = new StaticDataRequest();
		
		String itemCache = source.getItemCache();
		String languageCode = source.getLanguageCode();
		String market = source.getMarket();
		
		ObjectFactory objectFactory = new ObjectFactory();
		request.setLanguageCode(objectFactory.createStaticDataRequestLanguageCode(languageCode));
		request.setMarket(objectFactory.createStaticDataRequestMarket(market));
		request.setItemCache(objectFactory.createStaticDataRequestItemCache(itemCache));
		request.setType(StaticDataType.PAYMENT_TYPE_ITEM);
		
			
		return request;
	}
}