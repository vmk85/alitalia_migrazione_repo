package com.alitalia.aem.service.impl.converter.home.checkincomfortseatservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.ChangeCabinCheckinRequest;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.ChangeCabinRequest;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.Resource;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd2.CompartimentalClass;


@Component(immediate=true, metatype=false)
@Service(value=ChangeCabinCheckinRequestToChangeCabinRequest.class)
public class ChangeCabinCheckinRequestToChangeCabinRequest implements Converter<ChangeCabinCheckinRequest, ChangeCabinRequest> {

	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;
	
	@Reference
	private CheckinFlightDataToFlight checkinFlightDataConverter;
	
	@Reference
	private CheckinPassengerDataToPassenger checkinPassengerDataConverter;
	

	@Override
	public ChangeCabinRequest convert(ChangeCabinCheckinRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		ChangeCabinRequest destination = null;

		if (source != null) {
			destination = objectFactory.createChangeCabinRequest();
			
			destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			destination.setX003CResourceX003EKBackingField(Resource.WEB);
				
			if(source.getCabin() != null){
				destination.setCabin(CompartimentalClass.fromValue(source.getCabin().value()));
			}
			destination.setFlight(objectFactory.createChangeCabinRequestFlight(checkinFlightDataConverter.convert(source.getFlight())));
			destination.setPassenger(objectFactory.createChangeCabinRequestPassenger(checkinPassengerDataConverter.convert(source.getPassenger())));
			destination.setSeat(objectFactory.createChangeCabinRequestSeat(source.getSeat()));

		}

		return destination;
	}


}
