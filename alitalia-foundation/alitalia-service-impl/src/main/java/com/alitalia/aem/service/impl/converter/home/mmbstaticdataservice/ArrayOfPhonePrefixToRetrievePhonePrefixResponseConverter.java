package com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PhonePrefixData;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixResponse;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd3.ArrayOfPhonePrefix;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd3.PhonePrefix;

@Component(immediate = true, metatype = false)
@Service(value = ArrayOfPhonePrefixToRetrievePhonePrefixResponseConverter.class)
public class ArrayOfPhonePrefixToRetrievePhonePrefixResponseConverter implements Converter<ArrayOfPhonePrefix, RetrievePhonePrefixResponse> {

	private static final Logger logger = LoggerFactory.getLogger(ArrayOfPhonePrefixToRetrievePhonePrefixResponseConverter.class);

	@Reference
	private PhonePrefixToPhonePrefixData phonePrefixConverter;
	
	@Reference
	private BookingMMBStaticDataServiceJAXBContextFactory jaxbContextFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public RetrievePhonePrefixResponse convert(ArrayOfPhonePrefix source) {
		RetrievePhonePrefixResponse destination = new RetrievePhonePrefixResponse();
		try {
			List<PhonePrefixData> phonePrefixesData = new ArrayList<>();
			
			Unmarshaller phonePrefixUnmashaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			JAXBElement<PhonePrefix> unmarshalledObj = null;

			List<PhonePrefix> countries = source.getPhonePrefix();
			for (Object phonePrefix : countries) {
				PhonePrefix phonePrefixSource = null;
				if (phonePrefix instanceof PhonePrefix) {
					phonePrefixSource = (PhonePrefix) phonePrefix;
				} else {
					unmarshalledObj = (JAXBElement<PhonePrefix>) phonePrefixUnmashaller.unmarshal((Node) phonePrefix);
					phonePrefixSource = unmarshalledObj.getValue();
				}
				PhonePrefixData phonePrefixData = phonePrefixConverter.convert(phonePrefixSource);
				phonePrefixesData.add(phonePrefixData);
			}
			
			destination.setPhonePrefix(phonePrefixesData);
			
		} catch (JAXBException e) {
			logger.error("Error instantiating JAXBContext/Unmarshaller: {}", e);
		} finally {
			logger.info("Conversion executed successfully");
		}
		return destination;			
	}
}