package com.alitalia.aem.service.impl.converter.home.mmbancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryMealDetailData;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.MealDetail;

@Component(immediate=true, metatype=false)
@Service(value=MealDetailToMmbAncillaryMealDetailData.class)
public class MealDetailToMmbAncillaryMealDetailData implements Converter<MealDetail, MmbAncillaryMealDetailData> {

	@Override
	public MmbAncillaryMealDetailData convert(MealDetail source) {
		MmbAncillaryMealDetailData destination = null;

		if (source != null) {
			destination = new MmbAncillaryMealDetailData();

			destination.setMeal(source.getX003CMealX003EKBackingField());
		}

		return destination;
	}

}
