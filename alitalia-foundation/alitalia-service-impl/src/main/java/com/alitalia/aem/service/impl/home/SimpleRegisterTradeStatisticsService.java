package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.RegisterStatisticsResponse;
import com.alitalia.aem.common.messages.home.TradeRegisterStatisticRequest;
import com.alitalia.aem.service.api.home.RegisterTradeStatisticsService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.statisticsservice.RegisterStatisticsRequestToTradeStatisticsRequest;
import com.alitalia.aem.service.impl.converter.home.statisticsservice.TradeStatisticsResponseToRegisterStatisticsResponse;
import com.alitalia.aem.ws.tradestatistics.statisticsservice.TradeStatisticsServiceClient;
import com.alitalia.aem.ws.tradestatistics.statisticsservice.xsd1.StatisticsRequest;
import com.alitalia.aem.ws.tradestatistics.statisticsservice.xsd1.StatisticsResponse;

@Service
@Component(immediate=true, metatype=false)
public class SimpleRegisterTradeStatisticsService implements RegisterTradeStatisticsService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleRegisterTradeStatisticsService.class);

	@Reference
	private TradeStatisticsServiceClient tradeStatisticsServiceClient;
	
	@Reference
	private RegisterStatisticsRequestToTradeStatisticsRequest registerStatisticsRequestConverter;
	
	@Reference
	private TradeStatisticsResponseToRegisterStatisticsResponse registerStatisticsResponseConverter;

	@Override
	public RegisterStatisticsResponse registerStatistics(TradeRegisterStatisticRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method registerStatistics. The request is {}", request);
		}
		

		try {
			
			RegisterStatisticsResponse response = new RegisterStatisticsResponse();
			StatisticsRequest statisticsRequest = registerStatisticsRequestConverter.convert(request);
			StatisticsResponse statisticsResponse = tradeStatisticsServiceClient.registerStatistic(statisticsRequest);
			response = registerStatisticsResponseConverter.convert(statisticsResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
			
			return response;
			
		} catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing registerStatistics: Sid ["+request.getSid()+"]" , e);
		}
	}
}