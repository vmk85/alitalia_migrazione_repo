package com.alitalia.aem.service.impl.converter.home.commonservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.AuthenticateMilleMigliaUserRequest;
import com.alitalia.aem.ws.booking.commonservice.xsd1.AuthenticateMmUserRequest;
import com.alitalia.aem.ws.booking.commonservice.xsd1.ObjectFactory;

@Component(immediate = true, metatype = false)
@Service(value = AuthenticateMilleMigliaUserRequestToAuthenticateMmUserRequest.class)
public class AuthenticateMilleMigliaUserRequestToAuthenticateMmUserRequest 
		implements Converter<AuthenticateMilleMigliaUserRequest, AuthenticateMmUserRequest> {

	@Override
	public AuthenticateMmUserRequest convert(AuthenticateMilleMigliaUserRequest source) {
		
		AuthenticateMmUserRequest destination = null;
		
		if (source != null) {
		
			ObjectFactory objectFactory = new ObjectFactory();
		
			destination = objectFactory.createAuthenticateMmUserRequest();
		
			destination.setLanguageCode(
					objectFactory.createAuthenticateMmUserRequestLanguageCode(
							source.getLanguageCode()));
			
			destination.setMarketCode(
					objectFactory.createAuthenticateMmUserRequestMarketCode(
							source.getMarketCode()));
			
			destination.setMmCode(
					objectFactory.createAuthenticateMmUserRequestMmCode(
							source.getMmCode()));
			
			destination.setMmPin(
					objectFactory.createAuthenticateMmUserRequestMmPin(
							source.getMmPin()));
		
		}
		
		return destination;
		
	}
	
}
