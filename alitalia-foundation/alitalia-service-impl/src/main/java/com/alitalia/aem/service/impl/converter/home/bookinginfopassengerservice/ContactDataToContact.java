package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ContactData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.Contact;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ContactType;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=ContactDataToContact.class)
public class ContactDataToContact 
		implements Converter<ContactData, Contact> {
	
	@Reference
	private PhonePrefixDataToPhonePrefix phonePrefixDataToPhonePrefixConverter;
	
	@Override
	public Contact convert(ContactData source) {
		
		Contact destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			destination = objectFactory.createContact();
			
			if (source.getContactType() != null) {
				destination.setContactType(
						ContactType.fromValue(
								source.getContactType().value()));	
			}
			
			destination.setPhoneNumber(
					objectFactory.createContactPhoneNumber(
							source.getPhoneNumber()));
			
			destination.setPrefix(
					objectFactory.createContactPrefix(
							phonePrefixDataToPhonePrefixConverter.convert(
									source.getPrefix())));
			
		}
		
		return destination;
	}

}
