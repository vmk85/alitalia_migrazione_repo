package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryCartItemData;
import com.alitalia.aem.common.messages.home.WebCheckinAddToCartRequest;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.AddToCartRequest;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.Resource;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ArrayOfAddToCartItem;

@Component(immediate=true, metatype=false)
@Service(value=WebCheckinAddToCartRequestToAddToCartRequest.class)
public class WebCheckinAddToCartRequestToAddToCartRequest implements
		Converter<WebCheckinAddToCartRequest, AddToCartRequest> {

	@Reference
	private MmbRequestBaseInfoDataToRequestBaseInfo mmbRequestBaseInfoDataConverter;

	@Reference
	private MmbAncillaryCartItemDataToAddToCartItem mmbAncillaryCartItemDataConverter;

	@Override
	public AddToCartRequest convert(WebCheckinAddToCartRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ObjectFactory objectFactory2 =
				new com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ObjectFactory();
		AddToCartRequest destination = null;

		if (source != null) {
			destination = objectFactory.createAddToCartRequest();

			destination.setCartId(source.getCartId());
			destination.setClient(objectFactory.createAddToCartRequestClient(source.getClient()));
			destination.setMachineName(objectFactory.createAddToCartRequestMachineName(source.getMachineName()));
			destination.setSessionId(objectFactory.createAddToCartRequestSessionId(source.getSessionId()));

			destination.setX003CInfoRequestX003EKBackingField(mmbRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			destination.setX003CResourceX003EKBackingField(Resource.WEB);

			ArrayOfAddToCartItem cartItems = objectFactory2.createArrayOfAddToCartItem();
			for(MmbAncillaryCartItemData sourceItem: source.getItems()) {
				cartItems.getAddToCartItem().add(mmbAncillaryCartItemDataConverter.convert(sourceItem));
			}
			destination.setItems(objectFactory.createAddToCartRequestItems(cartItems));
		}

		return destination;
	}
}