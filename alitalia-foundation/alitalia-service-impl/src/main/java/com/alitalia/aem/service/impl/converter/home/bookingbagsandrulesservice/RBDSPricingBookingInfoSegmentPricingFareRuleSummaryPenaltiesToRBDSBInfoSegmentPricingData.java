package com.alitalia.aem.service.impl.converter.home.bookingbagsandrulesservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesData;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenalties;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenalty;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenalty;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesToRBDSBInfoSegmentPricingData.class)
public class RBDSPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesToRBDSBInfoSegmentPricingData
		implements Converter<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenalties, ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesData> {

	@Reference
	private RBDSPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyToRBDSPBISPFRSPenaltiesCancellationAndRefundPenaltyData penaltiesCancellationAndRefundPenaltyConverter;
	
	@Reference
	private RBDSPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyToRBDSPFRSPenaltiesChangePenaltyData penaltiesChangePenaltyFieldConverter;
	
	@Override
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesData convert(
			ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenalties source) {
		ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesData destination = null;
		
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesData();
			List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData> array_1 = new ArrayList<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData>();
			if(source.getCancellationAndRefundPenaltyField()!=null)
			for(ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenalty s : source.getCancellationAndRefundPenaltyField().getResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenalty())
				array_1.add(penaltiesCancellationAndRefundPenaltyConverter.convert(s));
			
			List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData> array_2 = new ArrayList<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData>();
			if(source.getChangePenaltyField()!=null)
			for(ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenalty s : source.getChangePenaltyField().getResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenalty())
				array_2.add(penaltiesChangePenaltyFieldConverter.convert(s));
			
			destination.setCancellationAndRefundPenaltyField(array_1);
			destination.setChangePenaltyField(array_2);
			destination.setNonrefundableField(source.isNonrefundableField());
			destination.setNonrefundableFieldSpecified(source.isNonrefundableFieldSpecified());
		}
		
		return destination;
	}
	
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesData convert(
			com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenalties source) {
		ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesData destination = null;
		
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesData();
			List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData> array_1 = new ArrayList<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData>();
			if(source.getCancellationAndRefundPenaltyField()!=null)
			for(com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenalty s : source.getCancellationAndRefundPenaltyField().getResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenalty())
				array_1.add(penaltiesCancellationAndRefundPenaltyConverter.convert(s));
			
			List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData> array_2 = new ArrayList<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenaltyData>();
			if(source.getChangePenaltyField()!=null)
			for(com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenalty s : source.getChangePenaltyField().getResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesChangePenalty())
				array_2.add(penaltiesChangePenaltyFieldConverter.convert(s));
			
			destination.setCancellationAndRefundPenaltyField(array_1);
			destination.setChangePenaltyField(array_2);
			destination.setNonrefundableField(source.isNonrefundableField());
			destination.setNonrefundableFieldSpecified(source.isNonrefundableFieldSpecified());
		}
		
		return destination;
	}
	

}
