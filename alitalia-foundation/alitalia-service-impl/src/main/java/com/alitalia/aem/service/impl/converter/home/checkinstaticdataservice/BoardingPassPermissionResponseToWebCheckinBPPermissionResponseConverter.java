package com.alitalia.aem.service.impl.converter.home.checkinstaticdataservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.WebCheckinBPPermissionResponse;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.BoardingPassPermissionResponse;


@Component(immediate=true, metatype=false)
@Service(value=BoardingPassPermissionResponseToWebCheckinBPPermissionResponseConverter.class)
public class BoardingPassPermissionResponseToWebCheckinBPPermissionResponseConverter implements Converter<BoardingPassPermissionResponse, WebCheckinBPPermissionResponse> {

	@Override
	public WebCheckinBPPermissionResponse convert(BoardingPassPermissionResponse source) {
		
		WebCheckinBPPermissionResponse response = new WebCheckinBPPermissionResponse();
		response.setIsBPPdfEnabled(source.isIsBoardingPassPdfEnabled());
		response.setIsBPViaEmailEnabled(source.isIsBoardingPassViaEmailEnabled());
		response.setIsBPViaSmsEnabled(source.isIsBoardingPassViaSmsEnabled());
		response.setIsDelayedDeliveryTypeEnabled(source.isIsDelayedDeliveryTypeEnabled());

		return response;			
	}
}