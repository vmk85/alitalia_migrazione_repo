package com.alitalia.aem.service.impl.converter.home.checkinsearchservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.ValidateMmUserCheckinResponse;
import com.alitalia.aem.ws.checkin.searchservice.xsd1.ValidateMmUserResponse;


@Component(immediate=true, metatype=false)
@Service(value=ValidateMmUserResponseToValidateMmUserCheckinResponse.class)
public class ValidateMmUserResponseToValidateMmUserCheckinResponse implements
		Converter<ValidateMmUserResponse, ValidateMmUserCheckinResponse> {

	@Override
	public ValidateMmUserCheckinResponse convert(ValidateMmUserResponse source) {
		ValidateMmUserCheckinResponse destination = null;

		if (source != null) {
			destination = new ValidateMmUserCheckinResponse();			

			JAXBElement<String> sourceName = source.getName();
			if (sourceName != null)
				destination.setName(sourceName.getValue());

			JAXBElement<String> sourceLastname = source.getLastname();
			if (sourceLastname != null)
				destination.setLastname(sourceLastname.getValue());

			destination.setValid(source.isIsValid());
			
		}

		return destination;
	}

}
