/**
 * 
 */
package com.alitalia.aem.service.impl.converter.home.carnetservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.CarnetGetListRequest;
import com.alitalia.aem.ws.carnet.carnetservice.xsd1.GetCarnetListRequest;
import com.alitalia.aem.ws.carnet.carnetservice.xsd1.ObjectFactory;



@Component(immediate=true, metatype=false)
@Service(value=CarnetGetListRequestToGetCarnetListRequest.class)
public class CarnetGetListRequestToGetCarnetListRequest implements Converter<CarnetGetListRequest, GetCarnetListRequest> {

	@Override
	public GetCarnetListRequest convert(CarnetGetListRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		GetCarnetListRequest destination = null;

		if (source != null) {
			
			destination = objectFactory.createGetCarnetListRequest();
			destination.setLanguageCode(objectFactory.createGetCarnetListRequestLanguageCode(source.getLanguageCode()));
			destination.setMarketCode(objectFactory.createGetCarnetListRequestMarketCode(source.getMarketCode()));

		}
		return destination;
	}
}
