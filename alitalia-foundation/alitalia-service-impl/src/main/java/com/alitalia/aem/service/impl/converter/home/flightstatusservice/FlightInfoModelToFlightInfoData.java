package com.alitalia.aem.service.impl.converter.home.flightstatusservice;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.DestinationData;
import com.alitalia.aem.common.data.home.DestinationDetailsData;
import com.alitalia.aem.common.data.home.FlightInfoData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.ArrivalAirport;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.ArrivalDetails;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.DepartureAirport;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.DepartureDetails;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.FlightInfoModel;

@Component(immediate = true, metatype = false)
@Service(value = FlightInfoModelToFlightInfoData.class)
public class FlightInfoModelToFlightInfoData implements Converter<FlightInfoModel, FlightInfoData> {

	@Override
	public FlightInfoData convert(FlightInfoModel flightInfoModel) {
		FlightInfoData flightInfoData = null;

		if (flightInfoModel != null) {
			flightInfoData = new FlightInfoData();

			XMLGregorianCalendar arrivalTime = flightInfoModel.getArrivalDateTime();
			if (arrivalTime != null)
				flightInfoData.setArrivalDateTime(XsdConvertUtils.parseCalendar(arrivalTime));

			JAXBElement<ArrivalAirport> jaxbArrivalAirport = flightInfoModel.getArrivalAirport();
			DestinationData arrivalAirportData = new DestinationData();
			if (jaxbArrivalAirport != null) {
				ArrivalAirport arrivalAirport = jaxbArrivalAirport.getValue();

				if (arrivalAirport != null) {
					if (arrivalAirport.getCodeContext() != null)
						arrivalAirportData.setCodeContext(arrivalAirport.getCodeContext().getValue());
					if (arrivalAirport.getLocationCode() != null)
						arrivalAirportData.setLocationCode(arrivalAirport.getLocationCode().getValue());
				}
			}

			XMLGregorianCalendar arrivalGapTime = flightInfoModel.getArrivalGapTime();
			if (arrivalGapTime != null)
				flightInfoData.setArrivalGapTime(XsdConvertUtils.parseCalendar(arrivalGapTime));

			DestinationDetailsData arrivalDetailsData = new DestinationDetailsData();
			JAXBElement<ArrivalDetails> jaxbArrivalDetails = flightInfoModel.getArrivalDetails();
			if (jaxbArrivalDetails != null) {
				ArrivalDetails arrivalDetails = jaxbArrivalDetails.getValue();

				if (arrivalDetails != null) {
					if (arrivalDetails.getCodeStatus() != null)
						arrivalDetailsData.setCodeStatus(arrivalDetails.getCodeStatus().getValue());
					if (arrivalDetails.getOpertiveInfo() != null)
						arrivalDetailsData.setOpertiveInfo(arrivalDetails.getOpertiveInfo().getValue());
				}
			}

			XMLGregorianCalendar departureTime = flightInfoModel.getDepartureDateTime();
			if (departureTime != null)
				flightInfoData.setDepartureDateTime(XsdConvertUtils.parseCalendar(departureTime));

			JAXBElement<DepartureAirport> jaxbDepartureAirport = flightInfoModel.getDepartureAirport();

			DestinationData departureAirportData = new DestinationData();
			if (jaxbDepartureAirport != null) {
				DepartureAirport departureAirport = jaxbDepartureAirport.getValue();

				if (departureAirport != null) {
					if (departureAirport.getCodeContext() != null)
						departureAirportData.setCodeContext(departureAirport.getCodeContext().getValue());
					if (departureAirport.getLocationCode() != null)
						departureAirportData.setLocationCode(departureAirport.getLocationCode().getValue());
				}
			}

			XMLGregorianCalendar departureGapTime = flightInfoModel.getDepartureGapTime();
			if (departureGapTime != null)
				flightInfoData.setDepartureGapTime(XsdConvertUtils.parseCalendar(departureGapTime));

			JAXBElement<DepartureDetails> jaxbDepartureDetails = flightInfoModel.getDepartureDetails();
			DestinationDetailsData departureDetailsData = new DestinationDetailsData();
			if (jaxbDepartureDetails != null) {
				DepartureDetails departureDetails = jaxbDepartureDetails.getValue();

				if (departureDetails != null) {
					if (departureDetails.getCodeStatus() != null)
						departureDetailsData.setCodeStatus(departureDetails.getCodeStatus().getValue());
					if (departureDetails.getOpertiveInfo() != null)
						departureDetailsData.setOpertiveInfo(departureDetails.getOpertiveInfo().getValue());
				}
			}

			String errorDescription = null, flightNumber = null, rescheduled = null, vector = null;
			if (flightInfoModel.getErrorDescription() != null)
				errorDescription = flightInfoModel.getErrorDescription().getValue();

			if (flightInfoModel.getFlightNumber() != null)
				flightNumber = flightInfoModel.getFlightNumber().getValue();

			if (flightInfoModel.getRescheduled() != null)
				rescheduled = flightInfoModel.getRescheduled().getValue();

			Integer sequenceNumber = flightInfoModel.getSequenceNumber();

			if (flightInfoModel.getVector() != null)
				vector = flightInfoModel.getVector().getValue();

			flightInfoData.setArrivalAirport(arrivalAirportData);
			flightInfoData.setArrivalDetails(arrivalDetailsData);

			flightInfoData.setDepartureAirport(departureAirportData);
			flightInfoData.setDepartureDetails(departureDetailsData);

			flightInfoData.setErrorDescription(errorDescription);
			flightInfoData.setFlightNumber(flightNumber);
			flightInfoData.setRescheduled(rescheduled);
			flightInfoData.setSequenceNumber(sequenceNumber);
			flightInfoData.setVector(vector);
		}

		return flightInfoData;
	}

}