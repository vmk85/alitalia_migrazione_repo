package com.alitalia.aem.service.impl.converter.home.mmbancillaryservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.MmbAncillarySendEmailResponse;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.SendEmailResponse;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd3.ArrayOfanyType;

@Component(immediate=true, metatype=false)
@Service(value=SendEmailResponseToMmbAncillarySendEmailResponse.class)
public class SendEmailResponseToMmbAncillarySendEmailResponse implements
		Converter<SendEmailResponse, MmbAncillarySendEmailResponse> {

	@Override
	public MmbAncillarySendEmailResponse convert(SendEmailResponse source) {
		MmbAncillarySendEmailResponse destination = null;

		if (source != null) {
			destination = new MmbAncillarySendEmailResponse();

			JAXBElement<String> sourceMachineName = source.getMachineName();
			if (sourceMachineName != null)
				destination.setMachineName(sourceMachineName.getValue());

			JAXBElement<ArrayOfanyType> sourceErrors = source.getErrors();
			if (sourceErrors != null &&
					sourceErrors.getValue() != null &&
					sourceErrors.getValue().getAnyType() != null &&
					!sourceErrors.getValue().getAnyType().isEmpty()) {

				destination.setSuccessful(false);
				List<String> destinationErrors = new ArrayList<String>();
				List<Object> sourceErrorsList = sourceErrors.getValue().getAnyType();

				for(Object sourceError : sourceErrorsList) 
					destinationErrors.add((String) sourceError);

				destination.setErrors(destinationErrors);
			}
			else
				destination.setSuccessful(true);
		}

		return destination;
	}

}
