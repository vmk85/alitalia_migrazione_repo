package com.alitalia.aem.service.impl.converter.home.mmbticketingservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.SeatTypeData;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd4.SeatType;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd4.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=SeatTypeDataToSeatType.class)
public class SeatTypeDataToSeatType implements Converter<SeatTypeData, SeatType> {

	@Override
	public SeatType convert(SeatTypeData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		SeatType destination = null;

		if (source != null) {
			destination = objectFactory.createSeatType();

			destination.setCode(objectFactory.createSeatTypeCode(source.getCode()));
			destination.setDescription(objectFactory.createSeatTypeDescription(source.getDescription()));
			destination.setText(objectFactory.createSeatTypeText(source.getText()));
		}

		return destination;
	}

}
