package com.alitalia.aem.service.impl.converter.home.checkinservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinCouponData;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbGdsTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbSeatStatusEnum;
import com.alitalia.aem.ws.checkin.checkinservice.xsd2.CompartimentalClass;
import com.alitalia.aem.ws.checkin.checkinservice.xsd2.Coupon;
import com.alitalia.aem.ws.checkin.checkinservice.xsd2.GdsType;
import com.alitalia.aem.ws.checkin.checkinservice.xsd2.SeatStatus;


@Component(immediate=true, metatype=false)
@Service(value=CouponToCheckinCouponData.class)
public class CouponToCheckinCouponData implements Converter<Coupon, CheckinCouponData> {

	@Reference
	private FlightToCheckinFlightData flightConverter;	

	@Override
	public CheckinCouponData convert(Coupon source) {
		CheckinCouponData destination = null;

		if (source != null) {
			destination = new CheckinCouponData();

			destination.setBarcode(source.getX003CBarcodeX003EKBackingField());
			destination.setBoardingPassColor(source.getX003CBoardingPassColorX003EKBackingField());
			destination.setBoardingPassLoaded(source.isX003CBoardingPassLoadedX003EKBackingField());
			destination.setBoardingTime(source.getX003CBoardingTimeX003EKBackingField().toGregorianCalendar());
			destination.setBoardingZone(source.getX003CBoardingZoneX003EKBackingField());
			destination.setChangeSeatEnabled(source.isX003CChangeSeatEnabledX003EKBackingField());
			destination.setChangeSeatPayment(source.isX003CChangeSeatPaymentX003EKBackingField());
			destination.setComfortSeatPaid(source.isX003CIsComfortSeatPaidX003EKBackingField());
			destination.setEticket(source.getX003CEticketX003EKBackingField());
			destination.setFlightId(source.getX003CFlightIdX003EKBackingField());
			destination.setGate(source.getX003CGateX003EKBackingField());

			GdsType sourceCdsCouponType = source.getX003CGdsCouponTypeX003EKBackingField();
			if (sourceCdsCouponType != null)
				destination.setGdsCouponType(MmbGdsTypeEnum.fromValue(sourceCdsCouponType.value()));

			destination.setId(source.getX003CIdX003EKBackingField());
			destination.setInfantCoupon(source.isX003CIsInfantCouponX003EKBackingField());
			destination.setOldSeat(source.getX003COldSeatX003EKBackingField());
			
			if(source.getX003COldSeatClassX003EKBackingField() != null){
				destination.setOldSeatClass(MmbCompartimentalClassEnum.fromValue(source.getX003COldSeatClassX003EKBackingField().value()));
			}
			
			destination.setOrderNumber(source.getX003COrderNumberX003EKBackingField());
			destination.setPassengerId(source.getX003CPassengerIdX003EKBackingField());
			destination.setPreviousSeat(source.getX003CPreviousSeatX003EKBackingField());
			destination.setSeat(source.getX003CSeatX003EKBackingField());

			CompartimentalClass sourceSeatClass = source.getX003CSeatClassX003EKBackingField();
			if (sourceSeatClass != null)
				destination.setSeatClass(MmbCompartimentalClassEnum.fromValue(sourceSeatClass.value()));

			destination.setSeatClassName(source.getX003CSeatClassNameX003EKBackingField());
			destination.setSequenceNumber(source.getX003CSequenceNumberX003EKBackingField());
			destination.setServiceCooperationDetails(source.getX003CServiceCooperatinDetailsX003EKBackingField());
			destination.setServiceOperatingDetails(source.getX003CServiceOperatingDetailsX003EKBackingField());
			destination.setSkyPriority(source.isX003CIsSkyPriorityX003EKBackingField());
			destination.setSocialCode(source.getX003CSocialCodeX003EKBackingField());
			destination.setSpecialFare(source.getX003CSpecialFareX003EKBackingField());
			destination.setSsrCode(source.getX003CSSRCodeX003EKBackingField());

			SeatStatus sourceStatus = source.getX003CStatusX003EKBackingField();
			if (sourceStatus != null)
				destination.setStatus(MmbSeatStatusEnum.fromValue(sourceStatus.value()));

			destination.setTerminal(source.getX003CTerminalX003EKBackingField());
			
			List<CompartimentalClass> sourceUpgradeEnabledClasses = source.getX003CUpgradeEnabledClassesX003EKBackingField()!=null ? source.getX003CUpgradeEnabledClassesX003EKBackingField().getCompartimentalClass() : null;			
			if (sourceUpgradeEnabledClasses != null && !sourceUpgradeEnabledClasses.isEmpty()){
				List<MmbCompartimentalClassEnum> upgradeEnabledClassList = new ArrayList<MmbCompartimentalClassEnum>();
				for (CompartimentalClass sourceUpgradeEnabledClassItem : sourceUpgradeEnabledClasses){
					upgradeEnabledClassList.add(MmbCompartimentalClassEnum.fromValue(sourceUpgradeEnabledClassItem.value()));
				}
				destination.setUpgradeEnabledClasses(upgradeEnabledClassList);
			}
			destination.setUpgradeEnabled(source.isX003CUpgradeEnabledX003EKBackingField());
			destination.setUpgradedAlready(source.isX003CUpgradedAlreadyX003EKBackingField());

			destination.setFlight(flightConverter.convert(source.getX003CFlightX003EKBackingField()));
		}

		return destination;
	}

}
