package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ObjectFactory;
import com.alitalia.aem.ws.booking.searchservice.xsd3.Routes;
import com.alitalia.aem.ws.booking.searchservice.xsd4.Cabin;
import com.alitalia.aem.ws.booking.searchservice.xsd5.ArrayOfanyType;

@Component(immediate=true, metatype=false)
@Service(value=RoutesDataToRoutes.class)
public class RoutesDataToRoutes implements Converter<RoutesData, Routes> {

	@Reference
	private BillingDataToBilling billingDataToBillingConverter;
	
	@Reference
	private CashAndMilesDataToCashAndMiles cashAndMilesDataToCashAndMilesConverter;
	
	@Reference
	private MmCustomerDataToMmCustomer mmCustomerDataToMmCustomerConverter;
	
	@Reference
	private ECouponDataToECoupon eCouponDataToECouponConverter;
	
	@Reference
	private InsurancePolicyDataToInsurancePolicy insurancePolicyDataToInsurancePolicyConverter;
	
	@Reference
	private PassengerBaseDataToAPassengerBase passengerBaseDataToAPassengerBaseConverter;
	
	@Reference
	private PaymentDataToPayment paymentDataToPaymentConverter;
	
	@Reference
	private PropertiesDataToArrayOfDictionaryItem propertiesDataToArrayOfDictionaryItemConverter;

	@Reference
	private RouteDataToRoute routeDataConverter;

	@Override
	public Routes convert(RoutesData source) {
		
		Routes destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			com.alitalia.aem.ws.booking.searchservice.xsd5.ObjectFactory xsd5ObjectFactory = 
					new com.alitalia.aem.ws.booking.searchservice.xsd5.ObjectFactory();
		
			destination = objectFactory.createRoutes();
			
			destination.setFarmId(objectFactory.createABoomBoxInfoFarmId(source.getFarmId()));

			destination.setBilling(
					objectFactory.createRoutesBilling(
							billingDataToBillingConverter.convert(
									source.getBilling())));
			
			if (source.getCabin() != null) {
				destination.setCabin(
						Cabin.fromValue(
								source.getCabin().value()));
			}
			
			destination.setCashAndMiles(
					objectFactory.createRoutesCashAndMiles(
							cashAndMilesDataToCashAndMilesConverter.convert(
									source.getCashAndMiles())));
					
			destination.setCoupon(
					objectFactory.createRoutesCoupon(
							eCouponDataToECouponConverter.convert(
									source.getCoupon())));
			
			destination.setMilleMigliaCustomer(
					objectFactory.createRoutesMilleMigliaCustomer(
							mmCustomerDataToMmCustomerConverter.convert(
									source.getMilleMigliaCustomer())));
			
			destination.setMseType(
					objectFactory.createRoutesMseType(
							source.getMseType()));
					
			destination.setNationalInsuranceNumber(
					objectFactory.createRoutesNationalInsuranceNumber(
							source.getNationalInsuranceNumber()));
			
			ArrayOfanyType passengers = xsd5ObjectFactory.createArrayOfanyType();
			if (source.getPassengers() != null) {
				for (PassengerBaseData passenger : source.getPassengers()) {
					passengers.getAnyType().add(
							passengerBaseDataToAPassengerBaseConverter.convert(
									passenger));
				}
			}
			destination.setPassengers(
					objectFactory.createRoutesPassengers(
							passengers));
			
			destination.setPayment(
					objectFactory.createRoutesPayment(
							paymentDataToPaymentConverter.convert(
									source.getPayment())));
			
			destination.setPNR(
					objectFactory.createRoutesPNR(
							source.getPnr()));
			
			ArrayOfanyType routesList = xsd5ObjectFactory.createArrayOfanyType();
			if (source.getRoutesList() != null) {
				for (RouteData routeData : source.getRoutesList()) {
					routesList.getAnyType().add(routeDataConverter.convert(routeData));
				}
			}
			destination.setRoutesList(
					objectFactory.createRoutesRoutesList(
							routesList));
			
			destination.setSliceCount(
					source.getSliceCount());
		
			destination.setProperties(
					objectFactory.createABoomBoxGenericInfoProperties(
							propertiesDataToArrayOfDictionaryItemConverter.convert(
								source.getProperties())));
			
			destination.setTradeAgencyCode(
					objectFactory.createRoutesTradeAgencyCode(
							source.getTradeAgencyCode()));
			
			destination.setFarmId(
					objectFactory.createABoomBoxInfoFarmId(
							source.getFarmId()));
			
			destination.setFarmId(
					objectFactory.createABoomBoxInfoId(
							source.getId()));
			
			destination.setSessionId(
					objectFactory.createABoomBoxInfoSessionId(
							source.getSessionId()));
			
			destination.setSolutionSet(
					objectFactory.createABoomBoxInfoSolutionSet(
							source.getSolutionSet()));
			
			destination.setInsurance(
					objectFactory.createRoutesInsurance(
							insurancePolicyDataToInsurancePolicyConverter.convert(
									source.getInsurance())));
		}
		
		return destination;
	}

}
