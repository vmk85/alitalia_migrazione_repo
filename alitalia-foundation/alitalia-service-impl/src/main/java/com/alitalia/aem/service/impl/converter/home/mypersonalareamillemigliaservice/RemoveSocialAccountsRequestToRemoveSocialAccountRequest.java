package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.SocialAccountData;
import com.alitalia.aem.common.messages.home.RemoveSocialAccountsRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.RemoveSocialAccountRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ArrayOfSocialAccount;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.SocialAccount;

@Component(immediate = true, metatype = false)
@Service(value = RemoveSocialAccountsRequestToRemoveSocialAccountRequest.class)
public class RemoveSocialAccountsRequestToRemoveSocialAccountRequest implements Converter<RemoveSocialAccountsRequest, RemoveSocialAccountRequest> {

	@Reference
	private MMCustomerProfileDataToCustomerProfile mmCustomerProfileDataConverter;
	
	@Override
	public RemoveSocialAccountRequest convert(RemoveSocialAccountsRequest source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		
		ObjectFactory objectFactory = new ObjectFactory();
		com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ObjectFactory objectFactory2 = new com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ObjectFactory();
		
		RemoveSocialAccountRequest destination = objectFactory.createRemoveSocialAccountRequest();
		
		ArrayOfSocialAccount arrayOfSocialAccounts = objectFactory2.createArrayOfSocialAccount();
		List<SocialAccount> socialAccounts = arrayOfSocialAccounts.getSocialAccount();
		
		for(SocialAccountData socialAccountData: source.getSocialAccounts()){
			SocialAccount socialAccount = objectFactory2.createSocialAccount();
			socialAccount.setEmail(objectFactory2.createSocialAccountEmail(socialAccountData.getEmail()));
			socialAccount.setGigyaUID(objectFactory2.createSocialAccountGigyaUID(socialAccountData.getGigyaUID()));
			socialAccount.setLogicalDelete(socialAccountData.getLogicalDelete());
			socialAccount.setLoginProvider(objectFactory2.createSocialAccountLoginProvider(socialAccountData.getLoginProvider()));
			
			socialAccounts.add(socialAccount);
		}
		
		JAXBElement<ArrayOfSocialAccount> jaxbArrayOfSocialAccounts = objectFactory.createRemoveSocialAccountRequestSocialAccounts(arrayOfSocialAccounts);
		JAXBElement<String> jaxbPin = objectFactory.createRemoveSocialAccountRequestPin(source.getPin());
		JAXBElement<String> jaxbUserCode = objectFactory.createRemoveSocialAccountRequestUserCode(source.getUserCode()); 
		destination.setSocialAccounts(jaxbArrayOfSocialAccounts);
		destination.setPin(jaxbPin);
		destination.setUserCode(jaxbUserCode);
				
		return destination;
	}
}