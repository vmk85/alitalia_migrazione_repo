package com.alitalia.aem.service.impl.converter.home.checkinemdservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.EmdItemData;
import com.alitalia.aem.common.data.home.enumerations.CheckinEmdTicketType;
import com.alitalia.aem.ws.checkin.emdservice.xsd2.EMDItemType;


@Component(immediate=true, metatype=false)
@Service(value=EMDItemTypeToEmdItemData.class)
public class EMDItemTypeToEmdItemData implements
		Converter<EMDItemType, EmdItemData> {
	
	@Reference
	private EMDPassengerNameTypeToCheckinEmdPassengerNameType passengerNameConverter;


	@Override
	public EmdItemData convert(EMDItemType source) {
		EmdItemData destination = null;

		if (source != null) {
			
			destination = new EmdItemData();	
			destination.setCommissionAmount(source.getX003CCommissionAmountX003EKBackingField());
			destination.setInvoiceNumber(source.getX003CInvoiceNumberX003EKBackingField());
			destination.setItemNumber(source.getX003CItemNumberX003EKBackingField());
			destination.setNetAmount(source.getX003CNetAmountX003EKBackingField());
			destination.setPassengerName(passengerNameConverter.convert(source.getX003CPassengerNameX003EKBackingField()));
			destination.setPaymentType(source.getX003CPaymentTypeX003EKBackingField());
			destination.setTicketingStatus(source.getX003CTicketingStatusX003EKBackingField());
			destination.setTicketNumber(source.getX003CTicketNumberX003EKBackingField());
			destination.setTotalAmount(source.getX003CTotalAmountX003EKBackingField());
			if (source.getX003CTypeX003EKBackingField() != null)
				destination.setType(CheckinEmdTicketType.fromValue(source.getX003CTypeX003EKBackingField().value()));
			destination.setTypeSpecified(source.isX003CTypeSpecifiedX003EKBackingField());

		}
		return destination;
	}

}
