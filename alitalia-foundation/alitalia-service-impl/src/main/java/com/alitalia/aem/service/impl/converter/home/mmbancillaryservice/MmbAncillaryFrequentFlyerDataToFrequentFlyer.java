package com.alitalia.aem.service.impl.converter.home.mmbancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryFrequentFlyerData;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.FrequentFlyer;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=MmbAncillaryFrequentFlyerDataToFrequentFlyer.class)
public class MmbAncillaryFrequentFlyerDataToFrequentFlyer implements Converter<MmbAncillaryFrequentFlyerData, FrequentFlyer> {

	@Override
	public FrequentFlyer convert(MmbAncillaryFrequentFlyerData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		FrequentFlyer destination = null;

		if (source != null) {
			destination = objectFactory.createFrequentFlyer();

			destination.setX003CCodeX003EKBackingField(source.getCode());
			destination.setX003CFFTypeX003EKBackingField(source.getFfType());
			destination.setX003CTierX003EKBackingField(source.getTier());
		}

		return destination;
	}

}
