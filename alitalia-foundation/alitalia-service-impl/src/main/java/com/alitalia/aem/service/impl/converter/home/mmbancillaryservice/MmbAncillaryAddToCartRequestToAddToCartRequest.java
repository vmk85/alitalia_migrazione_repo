package com.alitalia.aem.service.impl.converter.home.mmbancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryCartItemData;
import com.alitalia.aem.common.messages.home.MmbAncillaryAddToCartRequest;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.AddToCartRequest;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.AddToCartItem;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.ArrayOfAddToCartItem;

@Component(immediate=true, metatype=false)
@Service(value=MmbAncillaryAddToCartRequestToAddToCartRequest.class)
public class MmbAncillaryAddToCartRequestToAddToCartRequest implements
		Converter<MmbAncillaryAddToCartRequest, AddToCartRequest> {

	@Reference
	private MmbRequestBaseInfoDataToRequestBaseInfo mmbRequestBaseInfoDataConverter;

	@Reference
	private MmbAncillaryCartItemDataToAddToCartItem mmbAncillaryCartItemDataConverter;

	@Override
	public AddToCartRequest convert(MmbAncillaryAddToCartRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.ObjectFactory objectFactory2 =
				new com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.ObjectFactory();
		AddToCartRequest destination = null;

		if (source != null) {
			destination = objectFactory.createAddToCartRequest();

			destination.setCartId(source.getCartId());
			destination.setClient(source.getClient());
			destination.setMachineName(objectFactory.createAddToCartRequestMachineName(source.getMachineName()));
			destination.setSessionId(objectFactory.createAddToCartRequestSessionId(source.getSessionId()));
			destination.setX003CPNRX003EKBackingField(null);

			destination.setX003CInfoRequestX003EKBackingField(mmbRequestBaseInfoDataConverter.convert(source.getBaseInfo()));

			ArrayOfAddToCartItem cartItems = objectFactory2.createArrayOfAddToCartItem();
			for(MmbAncillaryCartItemData sourceItem: source.getItemsToAdd()) {
				cartItems.getAddToCartItem().add(mmbAncillaryCartItemDataConverter.convert(sourceItem));
			}
			destination.setItems(objectFactory.createAddToCartRequestItems(cartItems));
		}

		return destination;
	}

}
