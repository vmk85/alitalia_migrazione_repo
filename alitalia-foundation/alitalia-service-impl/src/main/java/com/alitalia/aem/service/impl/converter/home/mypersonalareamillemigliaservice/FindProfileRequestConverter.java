package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.FindProfileRequestLocal;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.FindProfileRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

@Component(immediate = true, metatype = false)
@Service(value = FindProfileRequestConverter.class)
public class FindProfileRequestConverter implements Converter<FindProfileRequestLocal, FindProfileRequest> {

	@Reference
	private MMCustomerProfileDataToCustomerProfile mmCustomerProfileDataConverter;

	@Override
	public FindProfileRequest convert(FindProfileRequestLocal source) {
		FindProfileRequest destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = new FindProfileRequest();

			destination.setAlias(
					objectFactory.createFindProfileRequestAlias(
							source.getAlias()));

			destination.setCodiceMM(
					objectFactory.createFindProfileRequestCodiceMM(
							source.getCodiceMM()));

			destination.setCognome(
					objectFactory.createFindProfileRequestCognome(
							source.getCognome()));

			destination.setUsername(
					objectFactory.createFindProfileRequestUsername(
							source.getUsername()));

		}

		return destination;
	}
}