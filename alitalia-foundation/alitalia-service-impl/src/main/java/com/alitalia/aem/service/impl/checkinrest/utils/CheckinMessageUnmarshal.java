package com.alitalia.aem.service.impl.checkinrest.utils;

import com.alitalia.aem.common.data.checkinrest.model.clearancillariescart.response.ClearAncillariesCart;
import com.alitalia.aem.common.data.checkinrest.model.getcart.CheckinCart;
import com.alitalia.aem.common.data.checkinrest.model.initpayment.response.Authorize3dResponse;
import com.alitalia.aem.common.messages.checkinrest.*;

import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PnrListInfoByAddPassenger;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.response.AncillaryOffers;
import com.alitalia.aem.common.data.checkinrest.model.boardingpass.BoardingPass;
import com.alitalia.aem.common.data.checkinrest.model.boardingpass.BoardingPassInfo;
import com.alitalia.aem.common.data.checkinrest.model.boardingpassprintable.BoardingPassPrintable;
import com.alitalia.aem.common.data.checkinrest.model.checkinselectedpassenger.response.Outcome;
import com.alitalia.aem.common.data.checkinrest.model.endpayment.response.EndPaymentResponse;
import com.alitalia.aem.common.data.checkinrest.model.enhancedseatmap.response.EnhancedSeatMapResp;
import com.alitalia.aem.common.data.checkinrest.model.fasttrack.response.FastTrack;
import com.alitalia.aem.common.data.checkinrest.model.flightdetail.FlightDetail;
import com.alitalia.aem.common.data.checkinrest.model.getcart.Cart;
import com.alitalia.aem.common.data.checkinrest.model.initpayment.response.InitPaymentResponse;
import com.alitalia.aem.common.data.checkinrest.model.lounge.response.Lounge;
import com.alitalia.aem.common.data.checkinrest.model.passenger.Passenger;
import com.alitalia.aem.common.data.checkinrest.model.passengerdata.PassengerData;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PnrListInfoByFrequentFlyerSearch;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PnrListInfoByPnrSearch;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PnrListInfoByTicketSearch;
import com.alitalia.aem.common.data.checkinrest.model.reprintbp.FreeTextInfoList;
import com.alitalia.aem.common.data.checkinrest.model.setbaggage.response.Baggage;
import com.alitalia.aem.common.data.checkinrest.model.tripsearch.Trip;
import com.alitalia.aem.common.data.checkinrest.model.updatepassenger.response.UpdatePassengerResp;
import com.alitalia.aem.common.data.checkinrest.model.updatepassengerdetails.response.UpdatePassengerDetailsResp;
import com.alitalia.aem.common.data.checkinrest.model.updatesecurityinfo.response.UpdateSecurityInfoResp;
import com.alitalia.aem.common.messages.checkinrest.CheckinChangeSeatsResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinClearAncillarySessionEndResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinEnhancedSeatMapResponse;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class CheckinMessageUnmarshal {

    private static final Logger logger = LoggerFactory.getLogger(CheckinMessageUnmarshal.class);

    public static Trip checkinTripSearchUnmarshal(String jsonString) {
        Trip tripObj = null;
        Gson gson = new Gson();
        try {
            tripObj = gson.fromJson(jsonString, Trip.class);
        } catch (JsonSyntaxException e) {
            logger.error("[CheckinMessageUnmarshal] Error: " + e.getMessage());
        }
        return tripObj;
    }

    public static PnrListInfoByPnrSearch checkinPnrSearchUnmarshal(String jsonString) {
        PnrListInfoByPnrSearch pnrObj = null;
        String errorObj = "";

        Gson gson = new Gson();
        try {
            if (jsonString.contains("isError")) {
                errorObj = jsonString;
                pnrObj = new PnrListInfoByPnrSearch();
                pnrObj.setError(errorObj);
            } else {
//				logger.info("checkinPnrSearchUnmarshal jsonString : " + jsonString);
                pnrObj = gson.fromJson(jsonString, PnrListInfoByPnrSearch.class);
            }
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return pnrObj;
    }

    public static PassengerData checkinGetPassengerDataUnmarshal(String jsonString) {
        PassengerData passengerDataObj = null;
        Gson gson = new Gson();
        try {
            passengerDataObj = gson.fromJson(jsonString, PassengerData.class);
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return passengerDataObj;
    }

    public static PnrListInfoByTicketSearch checkinTicketSearchUnmarshal(String jsonString) {
        PnrListInfoByTicketSearch pnrObj = null;
        String errorObj = "";

        Gson gson = new Gson();
        try {
            if (jsonString.contains("isError")) {
                errorObj = jsonString;
                pnrObj = new PnrListInfoByTicketSearch();
                pnrObj.setError(errorObj);
            } else {
                pnrObj = gson.fromJson(jsonString, PnrListInfoByTicketSearch.class);
            }
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return pnrObj;
    }

    public static PnrListInfoByAddPassenger checkinAddPassengerUnmarshal(String jsonString) {
        PnrListInfoByAddPassenger pnrObj = null;
        String errorObj = "";

        Gson gson = new Gson();
        try {
            if (jsonString.contains("isError")) {
                errorObj = jsonString;
                pnrObj = new PnrListInfoByAddPassenger();
                pnrObj.setError(errorObj);
            } else {
                pnrObj = gson.fromJson(jsonString, PnrListInfoByAddPassenger.class);
            }
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return pnrObj;
    }

    public static PnrListInfoByFrequentFlyerSearch checkinFrequentFlyerSearchUnmarshal(String jsonString) {
        PnrListInfoByFrequentFlyerSearch pnrObj = null;
        String errorObj = "";

        Gson gson = new Gson();
        try {
            if (jsonString.contains("isError")) {
                errorObj = jsonString;
                pnrObj = new PnrListInfoByFrequentFlyerSearch();
                pnrObj.setError(errorObj);
            } else {
//				logger.info("checkinFrequentFlyerSearchUnmarshal jsonString : " + jsonString);
                pnrObj = gson.fromJson(jsonString, PnrListInfoByFrequentFlyerSearch.class);
            }
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return pnrObj;
    }

    public static Outcome checkinSelectedPassengerUnmarshal(String jsonString) {
        Outcome outObj = null;
        String errorObj = "";

        Gson gson = new Gson();
        try {
            if (jsonString.contains("isError")) {
                errorObj = jsonString;
                outObj = new Outcome();
                outObj.setError(errorObj);
            } else {
                outObj = gson.fromJson(jsonString, Outcome.class);
            }
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return outObj;
    }

    public static FlightDetail checkinFlightDetailUnmarshal(String jsonString) {
        FlightDetail flightDetailObj = null;
        Gson gson = new Gson();
        try {
            flightDetailObj = gson.fromJson(jsonString, FlightDetail.class);
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return flightDetailObj;
    }

    public static Passenger checkinPassengerUnmarshal(String jsonString) {
        Passenger passengerObj = null;
        Gson gson = new Gson();
        try {
            passengerObj = gson.fromJson(jsonString, Passenger.class);
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return passengerObj;
    }

    public static FreeTextInfoList checkinnewBPUnmarshal(String jsonString) {
        FreeTextInfoList freeTextInfoListObj = null;
        Gson gson = new Gson();
        try {
            freeTextInfoListObj = gson.fromJson(jsonString, FreeTextInfoList.class);
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return freeTextInfoListObj;
    }

    public static BoardingPassPrintable checkinBoardingPassPrintableUnmarshal(String jsonString) {
        BoardingPassPrintable boardingPassPrintable = null;
        Gson gson = new Gson();
        try {
            boardingPassPrintable = gson.fromJson(jsonString, BoardingPassPrintable.class);
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return boardingPassPrintable;
    }

    public static BoardingPass checkinBoardingPassUnmarshal(String jsonString) {
        BoardingPass boardingPass = null;
        Gson gson = new Gson();
        try {
            boardingPass = gson.fromJson(jsonString, BoardingPass.class);
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return boardingPass;
    }

    public static AncillaryOffers checkinGetSelectedAncillayOffersUnmarshal(String jsonString) {
        AncillaryOffers ancillaryOffers = null;
        Gson gson = new Gson();
        try {
            ancillaryOffers = gson.fromJson(jsonString, AncillaryOffers.class);
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return ancillaryOffers;
    }


    public static Baggage checkinSetBaggageUnmarshal(String jsonString) {
        Baggage baggage = null;
        Gson gson = new Gson();
        try {
            baggage = gson.fromJson(jsonString, Baggage.class);
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return baggage;
    }

    public static EnhancedSeatMapResp CheckinEnhancedSeatMapUnmarshal(String jsonString) {
        EnhancedSeatMapResp enhancedSeatMapResp = null;
        Gson gson = new Gson();
        try {
            if (jsonString.contains("isError")) {
                enhancedSeatMapResp = new EnhancedSeatMapResp();
                enhancedSeatMapResp.setError(jsonString);
            } else {
                enhancedSeatMapResp = gson.fromJson(jsonString, EnhancedSeatMapResp.class);
            }
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return enhancedSeatMapResp;
    }

    public static CheckinChangeSeatsResponse CheckinChangeSeatsUnmarshal(String jsonString) {
        CheckinChangeSeatsResponse changeSeatsResp = null;
        String errorObj = "";
        Gson gson = new Gson();
        try {
            if (jsonString.contains("isError")) {
                errorObj = jsonString;
                changeSeatsResp = new CheckinChangeSeatsResponse();
                changeSeatsResp.setError(errorObj);
            } else {
//				logger.info("checkinChangeSeatsUnmarshal jsonString : " + jsonString);
                changeSeatsResp = gson.fromJson(jsonString, CheckinChangeSeatsResponse.class);
            }
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return changeSeatsResp;
    }

    public static UpdateSecurityInfoResp CheckinUpdateSecurityInfoUnmarshal(String jsonString) {
        UpdateSecurityInfoResp updateSecurityInfoResp = null;
        Gson gson = new Gson();
        try {
            updateSecurityInfoResp = gson.fromJson(jsonString, UpdateSecurityInfoResp.class);
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return updateSecurityInfoResp;
    }

    public static FastTrack checkinSetFastTrackUnmarshal(String jsonString) {
        FastTrack fastTrack = null;
        Gson gson = new Gson();
        try {
            fastTrack = gson.fromJson(jsonString, FastTrack.class);
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return fastTrack;
    }

    public static Lounge checkinSetLoungeUnmarshal(String jsonString) {
        Lounge lounge = null;
        Gson gson = new Gson();
        try {
            lounge = gson.fromJson(jsonString, Lounge.class);
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return lounge;
    }

    public static UpdatePassengerDetailsResp CheckinUpdatePassengerDetailsUnmarshal(String jsonString) {
        UpdatePassengerDetailsResp updatePassengerDetailsResp = null;
        Gson gson = new Gson();
        try {
            updatePassengerDetailsResp = gson.fromJson(jsonString, UpdatePassengerDetailsResp.class);
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return updatePassengerDetailsResp;
    }

    public static UpdatePassengerResp CheckinUpdatePassengerUnmarshal(String jsonString) {
        UpdatePassengerResp updatePassengerResp = null;
        Gson gson = new Gson();
        try {
            updatePassengerResp = gson.fromJson(jsonString, UpdatePassengerResp.class);
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return updatePassengerResp;
    }

    public static BoardingPassInfo checkinGetBoardingPassInfoUnmarshal(String jsonString) {
        BoardingPassInfo boardingPassInfo = null;
        Gson gson = new Gson();
        try {
            if (jsonString.contains("isError")) {
                boardingPassInfo = new BoardingPassInfo();
                boardingPassInfo.setError(jsonString);
            } else {
                boardingPassInfo = gson.fromJson(jsonString, BoardingPassInfo.class);
            }
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return boardingPassInfo;
    }

    public static Cart checkinGetCartUnmarshal(String jsonString,String separatore) {
        Cart cart = null;
        String errorObj = "";

        Gson gson = new Gson();
        try {
            if (jsonString.contains("isError")) {
                errorObj = jsonString;
                cart = new Cart();
                cart.setError(errorObj);
            } else {
                cart = gson.fromJson(jsonString, Cart.class);
                separatore = ",";
                cart = mapCartStringToCartFloat(cart,separatore);
            }

        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return cart;
    }

    public static InitPaymentResponse checkinInitPaymentUnmarshal(String jsonString) {
        InitPaymentResponse initPaymentResponse = null;
        String errorObj = "";

        Gson gson = new Gson();
        try {
            if (jsonString.contains("isError")) {
                errorObj = jsonString;
                initPaymentResponse = new InitPaymentResponse();
                initPaymentResponse.setError(errorObj);
            } else {
                initPaymentResponse = gson.fromJson(jsonString, InitPaymentResponse.class);
            }

        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return initPaymentResponse;
    }

    public static EndPaymentResponse checkinEndPaymentUnmarshal(String jsonString) {
        EndPaymentResponse endPaymentResponse = null;
        String errorObj = "";

        Gson gson = new Gson();
        try {
            if (jsonString.contains("isError")) {
                errorObj = jsonString;
                endPaymentResponse = new EndPaymentResponse();
                endPaymentResponse.setError(errorObj);
            } else {
                endPaymentResponse = gson.fromJson(jsonString, EndPaymentResponse.class);
            }

        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return endPaymentResponse;
    }

    public static CheckinClearAncillarySessionEndResponse ClearAncillaySessionEndUnmarshal(String jsonString) {
        CheckinClearAncillarySessionEndResponse checkinClearAncillarySessionEndResponse = null;
        String errorObj = "";

        Gson gson = new Gson();
        try {
            if (jsonString.contains("isError")) {
                errorObj = jsonString;
                checkinClearAncillarySessionEndResponse = new CheckinClearAncillarySessionEndResponse();
                checkinClearAncillarySessionEndResponse.setError(errorObj);
            } else {
                checkinClearAncillarySessionEndResponse = gson.fromJson(jsonString, CheckinClearAncillarySessionEndResponse.class);
            }
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }

        return checkinClearAncillarySessionEndResponse;
    }

    public static ClearAncillariesCart clearAncillaiesCartUnmarshal(String jsonString) {
        ClearAncillariesCart checkinClearAncillariesCartResponse = null;
        Gson gson = new Gson();
        try {
            checkinClearAncillariesCartResponse = gson.fromJson(jsonString, ClearAncillariesCart.class);
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return checkinClearAncillariesCartResponse;
    }

    public static CheckinOffloadResponse OffloadUnmarshal(String jsonString) {
        CheckinOffloadResponse checkinOffloadResponse = null;
        String errorObj = "";

        Gson gson = new Gson();
        try {
            if (jsonString.contains("isError")) {
                errorObj = jsonString;
                checkinOffloadResponse = new CheckinOffloadResponse();
                checkinOffloadResponse.setError(errorObj);
            } else {
                checkinOffloadResponse = gson.fromJson(jsonString, CheckinOffloadResponse.class);
            }

        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return checkinOffloadResponse;
    }

    public static CheckinOffloadFlightsResponse OffloadFlightsUnmarshal(String jsonString) {
        CheckinOffloadFlightsResponse CheckinOffloadFlightsResponse = null;
        String errorObj = "";

        Gson gson = new Gson();
        try {
            if (jsonString.contains("isError")) {
                errorObj = jsonString;
                CheckinOffloadFlightsResponse = new CheckinOffloadFlightsResponse();
                CheckinOffloadFlightsResponse.setError(errorObj);
            } else {
                CheckinOffloadFlightsResponse = gson.fromJson(jsonString, CheckinOffloadFlightsResponse.class);
            }

        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return CheckinOffloadFlightsResponse;
    }

    public static Cart mapCartStringToCartFloat(Cart cart,String separatore) {
        if (cart.getCheckinCartObj() != null) {
            cart.setCheckinCart(new CheckinCart());
            cart.setTotalPrice(cart.getCheckinCartObj().getCartPriceToBeIssued(null));
            cart.getCheckinCart().setCartPriceToBeIssued(Float.parseFloat(cart.getCheckinCartObj().getCartPriceToBeIssued(separatore)));
//            cart.getCheckinCart().setCartPriceIssued(Float.parseFloat(cart.getCheckinCartObj().getCartPriceIssued().replace(separatore,"")));
            cart.getCheckinCart().setPnr(cart.getCheckinCartObj().getPnr());
            cart.getCheckinCart().setAZFlightCart(cart.getCheckinCartObj().getAZFlightCart());
        }
        return cart;
    }

    public static CheckinSetSmsDataResponse setSmsDataUnmarshal(String jsonString) {
        CheckinSetSmsDataResponse checkinSetSmsDataResponse = null;
        String errorObj = "";

        Gson gson = new Gson();
        try {
            if(jsonString.contains("isError")) {
                errorObj = jsonString;
                checkinSetSmsDataResponse = new CheckinSetSmsDataResponse();
                checkinSetSmsDataResponse.setError(errorObj);
            } else {
                checkinSetSmsDataResponse = gson.fromJson(jsonString, CheckinSetSmsDataResponse.class);
            }

        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return checkinSetSmsDataResponse;
    }

    public static CheckinGetSmsDataResponse getSmsDataUnmarshal(String jsonString) {
        CheckinGetSmsDataResponse checkinGetSmsDataResponse = null;
        String errorObj = "";

        Gson gson = new Gson();
        try {
            if(jsonString.contains("isError")) {
                errorObj = jsonString;
                checkinGetSmsDataResponse = new CheckinGetSmsDataResponse();
                checkinGetSmsDataResponse.setError(errorObj);
            } else {
                checkinGetSmsDataResponse = gson.fromJson(jsonString, CheckinGetSmsDataResponse.class);
            }

        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return checkinGetSmsDataResponse;
    }

    public static Authorize3dResponse getAuthorize3dUnmarshal(String jsonString) {
        Authorize3dResponse authorize3dResponse = null;
        String errorObj = "";

        Gson gson = new Gson();
        try {
            if(jsonString.contains("isError")) {
                errorObj = jsonString;
                authorize3dResponse = new Authorize3dResponse();
                authorize3dResponse.setError(errorObj);
            } else {
                authorize3dResponse = gson.fromJson(jsonString, Authorize3dResponse.class);
            }
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage());
        }
        return authorize3dResponse;
    }
}
