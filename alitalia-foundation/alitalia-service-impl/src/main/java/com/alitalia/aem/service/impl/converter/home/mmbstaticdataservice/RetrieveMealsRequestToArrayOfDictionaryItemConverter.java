package com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveMealsRequest;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.ArrayOfDictionaryItem;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.DictionaryItem;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=RetrieveMealsRequestToArrayOfDictionaryItemConverter.class)
public class RetrieveMealsRequestToArrayOfDictionaryItemConverter implements Converter<RetrieveMealsRequest, ArrayOfDictionaryItem> {

	@Override
	public ArrayOfDictionaryItem convert(RetrieveMealsRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();		
		ArrayOfDictionaryItem destination = objectFactory.createArrayOfDictionaryItem();
		
		DictionaryItem parameterMarket = objectFactory.createDictionaryItem();
		parameterMarket.setKey(objectFactory.createDictionaryItemKey("Market"));
		parameterMarket.setValue(objectFactory.createDictionaryItemValue(source.getMarket()));
		destination.getDictionaryItem().add(parameterMarket);

		DictionaryItem parameterLanguageCode = objectFactory.createDictionaryItem();
		parameterLanguageCode.setKey(objectFactory.createDictionaryItemKey("LanguageCode"));
		parameterLanguageCode.setValue(objectFactory.createDictionaryItemValue(source.getLanguageCode()));
		destination.getDictionaryItem().add(parameterLanguageCode);
		
		return destination;
	}

}