package com.alitalia.aem.service.impl.converter.home.carnetpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.carnet.CarnetBancaIntesaProviderData;
import com.alitalia.aem.common.data.home.carnet.CarnetBancoPostaProviderData;
import com.alitalia.aem.common.data.home.carnet.CarnetComunicationData;
import com.alitalia.aem.common.data.home.carnet.CarnetCreditCardProviderData;
import com.alitalia.aem.common.data.home.carnet.CarnetPayPalProviderData;
import com.alitalia.aem.common.data.home.carnet.CarnetProviderData;
import com.alitalia.aem.common.data.home.carnet.CarnetUnicreditProviderData;
import com.alitalia.aem.common.data.home.carnet.CarnetZeroPaymentProviderData;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.ObjectFactory;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.APaymentProviderOfanyType;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.CreditCardOfanyTypeanyType;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.CreditCardType;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.PayPalOfanyType;



@Component(immediate=true, metatype=false)
@Service(value=CarnetProviderDataToAPaymentProviderOfAnyType.class)
public class CarnetProviderDataToAPaymentProviderOfAnyType 
		implements Converter<CarnetProviderData<? extends CarnetComunicationData>, APaymentProviderOfanyType> {
	
	@Reference
	private CarnetComunicationDataToAComunicationBase carnetComunicationDataConverter;
	
	@Reference
	private UserInfoBaseToAUserInfo userInfoBaseToAUserInfoConverter;
		
	@Override
	public APaymentProviderOfanyType convert(CarnetProviderData<? extends CarnetComunicationData> source) {
		
		APaymentProviderOfanyType destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			if (source instanceof CarnetBancaIntesaProviderData) {
				destination = objectFactory.createBancaIntesaOfanyType();
				
			} else if (source instanceof CarnetUnicreditProviderData){
				destination = objectFactory.createUnicreditOfanyType();
				
			} else if (source instanceof CarnetBancoPostaProviderData) {
				destination = objectFactory.createBancoPostaOfanyType();
				
			} else if (source instanceof CarnetZeroPaymentProviderData) {
				destination = objectFactory.createZeroPaymentProviderOfanyTypeanyType();
				
			} else if (source instanceof CarnetCreditCardProviderData) {
				CarnetCreditCardProviderData typedSource = (CarnetCreditCardProviderData) source;
				CreditCardOfanyTypeanyType typedDestination = objectFactory.createCreditCardOfanyTypeanyType();
				
				typedDestination.setCreditCardNumber(
						objectFactory.createCreditCardOfanyTypeanyTypeCreditCardNumber(
								typedSource.getCreditCardNumber()));
				
				typedDestination.setCVV(
						objectFactory.createCreditCardOfanyTypeanyTypeCVV(
								typedSource.getCvv()));
				
				typedDestination.setExpiryMonth(
						typedSource.getExpiryMonth());
				
				typedDestination.setExpiryYear(
						typedSource.getExpiryYear());
				
				typedDestination.setIs3DSecure(typedSource.getIs3DSecure());
				
				if (typedSource.getType() != null) {
					typedDestination.setType(
							CreditCardType.fromValue(typedSource.getType().value()));
				}
				
				if (typedSource.getUserInfo() != null) {
					typedDestination.setUserInfo(
							objectFactory.createCreditCardOfanyTypeanyTypeUserInfo(
								userInfoBaseToAUserInfoConverter.convert(
										typedSource.getUserInfo())));
				}
				destination = typedDestination;
				
			} else if (source instanceof CarnetPayPalProviderData) {
				PayPalOfanyType typedDestination = objectFactory.createPayPalOfanyType();
				destination = typedDestination;	
				
			} else {
				destination = objectFactory.createAPaymentProviderOfanyType();
				
			}
			
			if (source.getComunication() != null) {
				destination.setComunication(
						objectFactory.createAPaymentProviderOfanyTypeComunication(
								carnetComunicationDataConverter.convert(
										source.getComunication())));
			}
		
		}
				
		return destination;
	}

}
