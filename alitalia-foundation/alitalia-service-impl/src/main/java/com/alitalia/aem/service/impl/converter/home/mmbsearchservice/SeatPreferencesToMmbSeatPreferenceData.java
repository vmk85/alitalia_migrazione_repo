package com.alitalia.aem.service.impl.converter.home.mmbsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbUpdateSSREnum;
import com.alitalia.aem.common.data.home.mmb.MmbSeatPreferenceData;
import com.alitalia.aem.ws.mmb.searchservice.xsd4.SeatPreference;

@Component(immediate=true, metatype=false)
@Service(value=SeatPreferencesToMmbSeatPreferenceData.class)
public class SeatPreferencesToMmbSeatPreferenceData implements Converter<SeatPreference, MmbSeatPreferenceData> {
	
	@Override
	public MmbSeatPreferenceData convert(SeatPreference source) {
		
		MmbSeatPreferenceData destination = null;
		
		if (source != null) {
			
			destination = new MmbSeatPreferenceData();
			
			if (source.getCabinSeat() != null)
				destination.setCabinSeat(CabinEnum.fromValue(source.getCabinSeat().value()));

			destination.setComfort(source.isIsComfort());
			
			if (source.getFlightCarrier() != null)
				destination.setFlightCarrier(source.getFlightCarrier().getValue());
			
			if (source.getFlightNumber() != null)
				destination.setFlightNumber(source.getFlightNumber().getValue());
			
			if (source.getNumber() != null)
				destination.setNumber(source.getNumber().getValue());
			
			if (source.getOldNumber() != null)
				destination.setOldNumber(source.getOldNumber().getValue());

			if (source.getOldRow() != null)
				destination.setOldRow(source.getOldRow().getValue());

			destination.setPending(source.isIsPending());

			if (source.getRow() != null)
				destination.setRow(source.getRow().getValue());

			if (source.getTravelerRefNumber() != null)
				destination.setTravelerRefNumber(source.getTravelerRefNumber().getValue());
			
			if (source.getUpdate() != null)
				destination.setUpdate(MmbUpdateSSREnum.fromValue(source.getUpdate().value()));

			if (source.getUpdateFlightRefNumberRPH() != null)
				destination.setUpdateFlightRefNumberRPH(source.getUpdateFlightRefNumberRPH().getValue());

			destination.setUpdating(source.isUpdating());
		}
		
		return destination;
	}

}
