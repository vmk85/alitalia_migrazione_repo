package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.home.UpdateNicknameResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.UpdateNickNameResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;

@Component(immediate = true, metatype = false)
@Service(value = UpdateNickNameResponseToUpdateNicknameResponse.class)
public class UpdateNickNameResponseToUpdateNicknameResponse implements Converter<UpdateNickNameResponse, UpdateNicknameResponse> {

	@Reference
	private CustomerProfileToMMCustomerProfileData customerProfileConverter;
	
	@Override
	public UpdateNicknameResponse convert(UpdateNickNameResponse source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		UpdateNicknameResponse destination = new UpdateNicknameResponse(); 
		
		JAXBElement<CustomerProfile> jaxbElement = source.getCustomer();
		CustomerProfile customerProfile = jaxbElement.getValue();
		MMCustomerProfileData mmCustomerProfile = customerProfileConverter.convert(customerProfile);
		destination.setCustomerProfile(mmCustomerProfile);
		
		List<String> updateErrors = source.getCustomerUpdateErrors();
		destination.setCustomerUpdateErrors(updateErrors);
		
		return destination;
	}
}