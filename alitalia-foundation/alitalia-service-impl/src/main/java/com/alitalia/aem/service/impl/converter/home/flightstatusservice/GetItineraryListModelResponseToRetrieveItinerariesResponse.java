package com.alitalia.aem.service.impl.converter.home.flightstatusservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.DaysOfWeekData;
import com.alitalia.aem.common.data.home.ItineraryModelData;
import com.alitalia.aem.common.messages.home.RetrieveItinerariesResponse;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetItineraryListModelResponse;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.ArrayOfFlight;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.ArrayOfItineraryModel;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.DaysOfWeek;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.ItineraryListModel;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.ItineraryModel;

@Component(immediate = true, metatype = false)
@Service(value = GetItineraryListModelResponseToRetrieveItinerariesResponse.class)
public class GetItineraryListModelResponseToRetrieveItinerariesResponse implements Converter<GetItineraryListModelResponse, RetrieveItinerariesResponse> {
	
	@Reference
	private ListOfFlightToListOfFlightItineraryData listOfFlightConverter;
	
	@Override
	public RetrieveItinerariesResponse convert(GetItineraryListModelResponse source) {
		RetrieveItinerariesResponse response = new RetrieveItinerariesResponse();
		
		JAXBElement<ItineraryListModel> jaxbItinerayListModel = source.getItineraryList();
		ItineraryListModel itineraryListModel = jaxbItinerayListModel.getValue();
		ArrayOfItineraryModel arrayOfItinerayModel = itineraryListModel.getItineraries().getValue();
		List<ItineraryModel> itineraryModels = arrayOfItinerayModel.getItineraryModel();
		
		List<ItineraryModelData> itineraryModelsData = new ArrayList<ItineraryModelData>();
		for(ItineraryModel model: itineraryModels){
			ItineraryModelData itineraryModelData = new ItineraryModelData();
			itineraryModelData.setDateFrom(model.getDateFrom().toGregorianCalendar());
			itineraryModelData.setDateTo(model.getDateTo().toGregorianCalendar());
			
			
			DaysOfWeek daysOfWeek = model.getFrequency().getValue();
			
			DaysOfWeekData daysOfWeekData = new DaysOfWeekData();
			daysOfWeekData.setMonday(daysOfWeek.isMonday());
			daysOfWeekData.setTuesday(daysOfWeek.isTuesday());
			daysOfWeekData.setWednesday(daysOfWeek.isWednesday());
			daysOfWeekData.setThursday(daysOfWeek.isThursday());
			daysOfWeekData.setFriday(daysOfWeek.isFriday());
			daysOfWeekData.setSaturday(daysOfWeek.isSaturday());
			daysOfWeekData.setSunday(daysOfWeek.isSunday());
			itineraryModelData.setFrequency(daysOfWeekData);
			
			
			itineraryModelData.setNumberOfConnection(model.getNumberOfConnection());
			
			ArrayOfFlight arrayOfFlights = model.getFlights().getValue();
			itineraryModelData.setFlights(listOfFlightConverter.convert(arrayOfFlights.getFlight()));
			
			itineraryModelsData.add(itineraryModelData);
		}
		
		response.setItineraries(itineraryModelsData);
		
		return response;
	}
}