package com.alitalia.aem.service.impl.converter.home.businessloginservice;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.AgencyRetrieveDataResponse;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.b2bv2.xsd.GetAnagraficaAgenziaResponse.GetAnagraficaAgenziaResult;
import com.alitalia.aem.ws.b2bv2.xsd.Table;

@Component
@Service(value = GetAnagraficaAgenziaToAgencyRetrieveDataResponse.class)
public class GetAnagraficaAgenziaToAgencyRetrieveDataResponse implements
		Converter<GetAnagraficaAgenziaResult, AgencyRetrieveDataResponse> {

	private static final Logger logger = LoggerFactory
			.getLogger(GetAnagraficaAgenziaToAgencyRetrieveDataResponse.class);
	
	@Reference
	private BusinessLoginServiceJAXBContextFactory jaxbContextFactory;

	@Override
	public AgencyRetrieveDataResponse convert(GetAnagraficaAgenziaResult source) {

		AgencyRetrieveDataResponse destination = null;
		Table unmarshalledObj = null;

		try {
			Unmarshaller loginUnmashaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			unmarshalledObj = null;
			Node responseElement = (Node) source.getAny();
			Node newDataSetElement = responseElement.getChildNodes().item(0);
			Node tableElement = newDataSetElement.getChildNodes().item(0);
			unmarshalledObj = (Table) loginUnmashaller.unmarshal(tableElement);
		} catch (JAXBException e) {
			logger.error("Error instantiating JAXBContext/Unmarshaller: {}", e);
		}

		if (unmarshalledObj != null) {
			destination = new AgencyRetrieveDataResponse(unmarshalledObj.getCodiceAgenzia(),
					unmarshalledObj.getRagioneSociale(), unmarshalledObj.getPartitaIVA(),
					unmarshalledObj.getCodiceFiscale(), unmarshalledObj.getIndirizzo(), unmarshalledObj.getCitta(),
					unmarshalledObj.getProvincia(), unmarshalledObj.getZIP(), unmarshalledObj.getRegione(),
					unmarshalledObj.getStato(), unmarshalledObj.getTelefono(), unmarshalledObj.getFax(),
					unmarshalledObj.getEmailPubblica(), unmarshalledObj.getEmailTitolare(),
					unmarshalledObj.getCODFAMCON(), unmarshalledObj.getEmailAlitalia(),
					XsdConvertUtils.parseBoolean(unmarshalledObj.getAbModuli()),
					XsdConvertUtils.parseBoolean(unmarshalledObj.getAbReportVenduto()),
					XsdConvertUtils.parseBoolean(unmarshalledObj.getAbAccordi()), unmarshalledObj.getCodiceAccordo(),
					unmarshalledObj.getDataAccordo(), unmarshalledObj.getCodiceSirax(),
					XsdConvertUtils.parseDate(unmarshalledObj.getDataValiditaAccordo()),
					unmarshalledObj.getCittaAccordo(), XsdConvertUtils.parseBoolean(unmarshalledObj
							.getAbTariffaDedicata()));
		}

		return destination;
	}

}
