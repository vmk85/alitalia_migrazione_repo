package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxPriceData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxPrice;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareCalculationBoxedTaxPriceDataToRTDSPricingFareCalculationBoxedTaxPrice.class)
public class RTDSPricingFareCalculationBoxedTaxPriceDataToRTDSPricingFareCalculationBoxedTaxPrice implements
		Converter<ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxPriceData, 
					ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxPrice> {

	@Override
	public ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxPrice convert(
			ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxPriceData source) {
		ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxPrice destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingFareCalculationBoxedTaxPrice();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
