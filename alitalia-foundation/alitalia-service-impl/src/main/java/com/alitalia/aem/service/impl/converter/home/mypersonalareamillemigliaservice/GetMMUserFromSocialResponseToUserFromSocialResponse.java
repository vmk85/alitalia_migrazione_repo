package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.UserFromSocialResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.GetMMUserFromSocialResponse;

@Component(immediate = true, metatype = false)
@Service(value = GetMMUserFromSocialResponseToUserFromSocialResponse.class)
public class GetMMUserFromSocialResponseToUserFromSocialResponse 
		implements Converter<GetMMUserFromSocialResponse, UserFromSocialResponse> {

	@Override
	public UserFromSocialResponse convert(GetMMUserFromSocialResponse source) {
		UserFromSocialResponse destination = null;

		if (source != null) {
			
			destination = new UserFromSocialResponse();
			
			if (source.getUserCode() != null) {
				destination.setUserCode(
						source.getUserCode().getValue());
			}
			
			if (source.getPin() != null) {
				destination.setPin(
						source.getPin().getValue());
			}
			
			if(source.getSSOToken() != null){
				destination.setSsoToken(
						source.getSSOToken().getValue());
			}
			
		}

		return destination;
	}

}
