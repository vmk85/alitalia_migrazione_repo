package com.alitalia.aem.service.impl.converter.home.checkinemdservice;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.common.messages.home.WebCheckinEmdRequest;
import com.alitalia.aem.ws.checkin.emdservice.xsd1.EMDRequest;
import com.alitalia.aem.ws.checkin.emdservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.emdservice.xsd2.ArrayOfFlight;
import com.alitalia.aem.ws.checkin.emdservice.xsd2.CompartimentalClass;
import com.alitalia.aem.ws.checkin.emdservice.xsd2.EMDModel;


@Component(immediate=true, metatype=false)
@Service(value=WebCheckinEmdRequestToEMDRequestConverter.class)
public class WebCheckinEmdRequestToEMDRequestConverter implements Converter<WebCheckinEmdRequest, EMDRequest> {
	
	@Reference
	private CheckinReasonForIssuanceToReasonForIssuanceType reasonForIssuanceRequestConverter;
	
	@Reference
	private CheckinCreditCardDataToCreditCard creditCardRequestConverter;
	
	@Reference
	private CheckinFlightDataToFlight checkinFlightDataConverter;
	
	@Reference CheckinPassengerDataToPassenger checkinPassengerConverter;
	
	@Override
	public EMDRequest convert(WebCheckinEmdRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();		
		EMDRequest destination = objectFactory.createEMDRequest();
		
		com.alitalia.aem.ws.checkin.emdservice.xsd2.ObjectFactory objectFactory2 = 
				new com.alitalia.aem.ws.checkin.emdservice.xsd2.ObjectFactory();

		EMDModel model = objectFactory2.createEMDModel();
		model.setReasonForIssuance(reasonForIssuanceRequestConverter.convert(source.getReasonForIssuance()));
		source.getReasonForIssuance();
		model.setX003CApprovalCodeX003EKBackingField(source.getApprovalCode());
		model.setX003CAuthorizationIDX003EKBackingField(source.getAuthorizationId());
		if (source.getCabin() != null)
			model.setX003CCabinX003EKBackingField(CompartimentalClass.fromValue(source.getCabin().value()));
		model.setX003CCartIDX003EKBackingField(source.getCartId());
		model.setX003CCityX003EKBackingField(source.getCity());
		model.setX003CCountryCodeX003EKBackingField(source.getCountryCode());
		model.setX003CCreditCardDataX003EKBackingField(creditCardRequestConverter.convert(source.getCreditCardData()));
		model.setX003CCuponeAmountX003EKBackingField(source.getCuponeAmount());
		model.setX003CCurrencyCodeX003EKBackingField(source.getCurrencyCode());
		List<CheckinFlightData> sourceFlightsList = source.getFlights();
		if(sourceFlightsList != null){
			ArrayOfFlight flightsList = objectFactory2.createArrayOfFlight();
			for(CheckinFlightData sourceFlightData : sourceFlightsList){
				flightsList.getFlight().add(checkinFlightDataConverter.convert(sourceFlightData));
			}
			model.setX003CFlightsX003EKBackingField(flightsList);
		}
		model.setX003CIndexOfFlightX003EKBackingField(source.getIndexOfFlight());
		model.setX003CLanguageCodeX003EKBackingField(source.getLanguageCode());
		model.setX003COrderCodeX003EKBackingField(source.getOrderCode());
		model.setX003COriginCityX003EKBackingField(source.getOriginCity());
		model.setX003CPassengerX003EKBackingField(checkinPassengerConverter.convert(source.getPassenger()));
		model.setX003CSeatX003EKBackingField(source.getSeat());
		model.setX003CSegmentsX003EKBackingField(source.getSegments());
		model.setX003CTotalAmountX003EKBackingField(source.getTotalAmount());
		model.setX003CUnitOfMeasureQuantityX003EKBackingField(source.getUnitOfMeasureQuantity());
		destination.setEMDIssued(objectFactory.createEMDRequestEMDIssued(model));

		return destination;
	}
}