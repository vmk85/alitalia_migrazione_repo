package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentConnectionData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionSliceSegmentConnection;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentConnectionToRTDSSliceSegmentConnectionData.class)
public class RTDSSliceSegmentConnectionToRTDSSliceSegmentConnectionData implements
		Converter<ResultTicketingDetailSolutionSliceSegmentConnection, ResultTicketingDetailSolutionSliceSegmentConnectionData> {

	@Reference
	private RTDSSliceSegmentConnectionExtToRTDSSliceSegmentConnectionExtData extFieldConverter;

	@Override
	public ResultTicketingDetailSolutionSliceSegmentConnectionData convert(ResultTicketingDetailSolutionSliceSegmentConnection source) {
		ResultTicketingDetailSolutionSliceSegmentConnectionData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionSliceSegmentConnectionData();

			destination.setDurationField(source.getDurationField());

			destination.setExtField(
					extFieldConverter.convert(source.getExtField()));
		}

		return destination;
	}

}
