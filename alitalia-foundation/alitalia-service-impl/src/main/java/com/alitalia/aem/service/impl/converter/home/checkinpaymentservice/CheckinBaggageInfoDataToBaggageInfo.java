package com.alitalia.aem.service.impl.converter.home.checkinpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinBaggageInfoData;
import com.alitalia.aem.ws.checkin.paymentservice.xsd2.BaggageInfo;
import com.alitalia.aem.ws.checkin.paymentservice.xsd2.ObjectFactory;


@Component(immediate=true, metatype=false)
@Service(value=CheckinBaggageInfoDataToBaggageInfo.class)
public class CheckinBaggageInfoDataToBaggageInfo implements Converter<CheckinBaggageInfoData, BaggageInfo> {
	
	@Reference
	private CheckinFlightDataToFlight checkinFlightDataConverter;	
	
	@Reference
	private MmbBaggageOrderDataToBaggageOrder mmbBaggageOrderDataConverter;
	
	@Reference
	private CheckinWebCheckInInfoDataToWebCheckInInfo checkinWebCheckInInfoDataConverter;
	
	@Reference
	private CheckinPaymentDataToPayment checkinPaymentDataConverter;
	
	@Reference
	private CheckinPaymentReceiptDataToPaymentReceipt checkinPaymentReceiptDataConverter;

	@Override
	public BaggageInfo convert(CheckinBaggageInfoData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		BaggageInfo destination = null;

		if (source != null) {
			destination = objectFactory.createBaggageInfo();
			
			destination.setX003CEticketX003EKBackingField(source.getEticket());
			destination.setX003CFlightX003EKBackingField(checkinFlightDataConverter.convert(source.getFlight()));
			destination.setX003COrderX003EKBackingField(mmbBaggageOrderDataConverter.convert(source.getOrder()));
			destination.setX003CWebCheckInInfoX003EKBackingField(checkinWebCheckInInfoDataConverter.convert(source.getWebCheckInInfo()));	
			destination.setX003CPaymentInfoX003EKBackingField(checkinPaymentDataConverter.convert(source.getPaymentInfo()));
			destination.setX003CReceiptInfoX003EKBackingField(checkinPaymentReceiptDataConverter.convert(source.getReceiptInfo()));
			
		}

		return destination;
	}

}
