package com.alitalia.aem.service.impl.home;

import com.alitalia.aem.ws.booking.commonservice.SabreDcCommonServiceClient;
import com.day.cq.wcm.api.Page;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.AuthenticateMilleMigliaUserRequest;
import com.alitalia.aem.common.messages.home.AuthenticateMilleMigliaUserResponse;
import com.alitalia.aem.common.messages.home.RetrieveAirportDetailsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportDetailsResponse;
import com.alitalia.aem.common.messages.home.RetrieveFlightMileageRequest;
import com.alitalia.aem.common.messages.home.RetrieveFlightMileageResponse;
import com.alitalia.aem.common.messages.home.RetrieveFlightSeatMapRequest;
import com.alitalia.aem.common.messages.home.RetrieveFlightSeatMapResponse;
import com.alitalia.aem.service.api.home.CommonService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.commonservice.AirportDetailResponseToRetrieveAirportDetailResponse;
import com.alitalia.aem.service.impl.converter.home.commonservice.AuthenticateMilleMigliaUserRequestToAuthenticateMmUserRequest;
import com.alitalia.aem.service.impl.converter.home.commonservice.AuthenticateMmUserResponseToAuthenticateMilleMigliaUserResponse;
import com.alitalia.aem.service.impl.converter.home.commonservice.FlightMileageResponseToRetrieveFlightMileageResponse;
import com.alitalia.aem.service.impl.converter.home.commonservice.RetrieveAirportDetailRequestToAirportDetailRequest;
import com.alitalia.aem.service.impl.converter.home.commonservice.RetrieveFlightMileageRequestToFlightMileageRequest;
import com.alitalia.aem.service.impl.converter.home.commonservice.RetrieveFlightSeatMapRequestToSeatMapRequest;
import com.alitalia.aem.service.impl.converter.home.commonservice.SeatMapResponseToRetrieveFlightSeatMapResponse;
import com.alitalia.aem.ws.booking.commonservice.CommonServiceClient;
import com.alitalia.aem.ws.booking.commonservice.xsd1.AirportDetailRequest;
import com.alitalia.aem.ws.booking.commonservice.xsd1.AirportDetailResponse;
import com.alitalia.aem.ws.booking.commonservice.xsd1.AuthenticateMmUserRequest;
import com.alitalia.aem.ws.booking.commonservice.xsd1.AuthenticateMmUserResponse;
import com.alitalia.aem.ws.booking.commonservice.xsd1.FlightMileageRequest;
import com.alitalia.aem.ws.booking.commonservice.xsd1.FlightMileageResponse;
import com.alitalia.aem.ws.booking.commonservice.xsd1.SeatMapRequest;
import com.alitalia.aem.ws.booking.commonservice.xsd1.SeatMapResponse;

@Service
@Component(immediate=true, metatype=false)
public class SimpleCommonService implements CommonService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleCommonService.class);

	@Reference
	private CommonServiceClient commonServiceClient;
	
	@Reference
	private RetrieveAirportDetailRequestToAirportDetailRequest retrieveAirportDetailsRequestConverter;
	
	@Reference
	private AirportDetailResponseToRetrieveAirportDetailResponse retrieveAirportDetailsResponseConverter;

	@Reference
	private RetrieveFlightMileageRequestToFlightMileageRequest retrieveFlightMileageRequestConverter;
	
	@Reference
	private FlightMileageResponseToRetrieveFlightMileageResponse retrieveFlightMileageRsponseConverter;
	
	@Reference
	private RetrieveFlightSeatMapRequestToSeatMapRequest retrieveFlightSeatMapRequestConverter;
	
	@Reference
	private SeatMapResponseToRetrieveFlightSeatMapResponse retrieveFlightSeatMapRsponseConverter;
	
	@Reference
	private AuthenticateMilleMigliaUserRequestToAuthenticateMmUserRequest authenticateMilleMigliaRequestConverter;
	
	@Reference
	private AuthenticateMmUserResponseToAuthenticateMilleMigliaUserResponse authenticateMilleMigliaResponseConverter;

	/** Migrazione 3.6 inizio*/

	@Reference
	private ResourceResolverFactory resolverFactory;

	@Reference
	private SabreDcCommonServiceClient sabreDcCommonServiceClient;

	/** Migrazione 3.6 fine*/

	@Override
	public RetrieveAirportDetailsResponse retrieveAirportDetails(RetrieveAirportDetailsRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveAirportDetails. The request is {}", request);
		}
		

		try {
			
			RetrieveAirportDetailsResponse response = new RetrieveAirportDetailsResponse();
			AirportDetailRequest airportDetailRequest = retrieveAirportDetailsRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			AirportDetailResponse airportDetailResponse = null;
			if (newSabreEnable(request.getMarket().toLowerCase())){
				airportDetailResponse = sabreDcCommonServiceClient.loadAirportDetails(airportDetailRequest);
			} else {
				airportDetailResponse = commonServiceClient.loadAirportDetails(airportDetailRequest);
			}

			/** Migrazione 3.6 fine*/

//			AirportDetailResponse airportDetailResponse = commonServiceClient.loadAirportDetails(airportDetailRequest, request.getMarket());
			response = retrieveAirportDetailsResponseConverter.convert(airportDetailResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method retrieveAirportDetails. The response is {}", response);
			}
			
			return response;
			
		} catch (Exception e) {
			logger.error("Exception while executing method retrieveAirportDetails.", e);
			throw new AlitaliaServiceException("Exception executing retrieveAirportDetails: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public RetrieveFlightMileageResponse retrieveFlightMileage(RetrieveFlightMileageRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveFlightMileage. The request is {}", request);
		}
		

		try {
			
			RetrieveFlightMileageResponse response = new RetrieveFlightMileageResponse();
			FlightMileageRequest flightMileageRequest = retrieveFlightMileageRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			FlightMileageResponse flightMileageResponse = null;
			if (newSabreEnable(request.getMarket().toLowerCase())){
				flightMileageResponse = sabreDcCommonServiceClient.loadFlightMileage(flightMileageRequest);
			} else {
				flightMileageResponse = commonServiceClient.loadFlightMileage(flightMileageRequest);
			}

			/** Migrazione 3.6 fine*/

//			FlightMileageResponse flightMileageResponse = commonServiceClient.loadFlightMileage(flightMileageRequest, request.getMarket());
			response = retrieveFlightMileageRsponseConverter.convert(flightMileageResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method retrieveFlightMileage. The response is {}", response);
			}
			
			return response;
			
		} catch (Exception e) {
			logger.error("Exception while executing method retrieveFlightMileage.", e);
			throw new AlitaliaServiceException("Exception executing retrieveFlightMileage: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public RetrieveFlightSeatMapResponse retrieveFlightSeatMap(RetrieveFlightSeatMapRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveFlightSeatMap. The request is {}", request);
		}
		

		try {
			
			RetrieveFlightSeatMapResponse response = new RetrieveFlightSeatMapResponse();
			SeatMapRequest seatMapRequest = retrieveFlightSeatMapRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			SeatMapResponse seatMapResponse = null;
			if (newSabreEnable(request.getMarket().toLowerCase())){
				seatMapResponse = sabreDcCommonServiceClient.getFlightSeatMap(seatMapRequest);
			} else {
				seatMapResponse = commonServiceClient.getFlightSeatMap(seatMapRequest);
			}

			/** Migrazione 3.6 fine*/

//			SeatMapResponse seatMapResponse = commonServiceClient.getFlightSeatMap(seatMapRequest, request.getMarket());
			response = retrieveFlightSeatMapRsponseConverter.convert(seatMapResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method retrieveFlightSeatMap. The response is {}", response);
			}
			
			return response;
			
		} catch (Exception e) {
			logger.error("Exception while executing method retrieveFlightSeatMap.", e);
			throw new AlitaliaServiceException("Exception executing retrieveFlightSeatMap: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public AuthenticateMilleMigliaUserResponse authenticateMilleMigliaUser(AuthenticateMilleMigliaUserRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method authenticateMilleMigliaUser. The request is {}", request);
		}
		

		try {
			
			AuthenticateMilleMigliaUserResponse response = new AuthenticateMilleMigliaUserResponse();
			AuthenticateMmUserRequest authenticateMilleMigliaUserRequest = authenticateMilleMigliaRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			AuthenticateMmUserResponse authenticateMilleMigliaUserResponse = null;
			if (newSabreEnable("it")){
				authenticateMilleMigliaUserResponse = sabreDcCommonServiceClient.mmUserAuthentication(authenticateMilleMigliaUserRequest);
			} else {
				authenticateMilleMigliaUserResponse = commonServiceClient.mmUserAuthentication(authenticateMilleMigliaUserRequest);
			}

			/** Migrazione 3.6 fine*/

//			AuthenticateMmUserResponse authenticateMilleMigliaUserResponse = commonServiceClient.mmUserAuthentication(authenticateMilleMigliaUserRequest);
			response = authenticateMilleMigliaResponseConverter.convert(authenticateMilleMigliaUserResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method authenticateMilleMigliaUser. The response is {}", response);
			}
			
			return response;
			
		} catch (Exception e) {
			logger.error("Exception while executing method authenticateMilleMigliaUser.", e);
			throw new AlitaliaServiceException("Exception executing authenticateMilleMigliaUser: Sid ["+request.getSid()+"]" , e);
		}
	}

	/** Migrazione 3.6 inizio*/

	private boolean newSabreEnable(String market) throws LoginException {

		ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
		Resource res = resourceResolver.getResource("/content/alitalia/alitalia-" + market);
		Page page = res.adaptTo(Page.class);
		ValueMap props = page.getProperties();
		String title = props.get("isNewService", "default value");
		logger.info("dato preso dalla pagina: " + title);
		return Boolean.parseBoolean(title);

	}

	/** Migrazione 3.6 fine*/
}