package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.BrandPenaltyData;
import com.alitalia.aem.ws.booking.searchservice.xsd3.BrandPenalty;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=BrandPenaltyDataToBrandPenalty.class)
public class BrandPenaltyDataToBrandPenalty implements Converter<BrandPenaltyData, BrandPenalty> {

	@Override
	public BrandPenalty convert(BrandPenaltyData source) {
		ObjectFactory factory3 = new ObjectFactory(); 
		BrandPenalty destination = factory3.createBrandPenalty();

		destination.setMaxPrice(factory3.createBrandPenaltyMaxPrice(source.getMaxPrice()));
		destination.setMaxPriceCurrency(factory3.createBrandPenaltyMaxPriceCurrency(source.getMaxPriceCurrency()));
		destination.setMinPrice(factory3.createBrandPenaltyMinPrice(source.getMinPrice()));
		destination.setMinPriceCurrency(factory3.createBrandPenaltyMinPriceCurrency(source.getMinPriceCurrency()));
		destination.setPermitted(source.isPermitted());

		return destination;
	}

}
