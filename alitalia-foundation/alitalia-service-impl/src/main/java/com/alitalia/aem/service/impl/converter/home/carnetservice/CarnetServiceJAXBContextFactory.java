package com.alitalia.aem.service.impl.converter.home.carnetservice;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.carnet.carnetservice.xsd5.Carnet;
import com.alitalia.aem.ws.carnet.carnetservice.xsd5.InfoCarnet;

@Component(immediate=true, metatype=false)
@Service(value=CarnetServiceJAXBContextFactory.class)
public class CarnetServiceJAXBContextFactory {

	private static final Logger logger = LoggerFactory.getLogger(CarnetServiceJAXBContextFactory.class);

	private JAXBContext jaxbContext = null;

	public synchronized JAXBContext getJaxbContext() throws JAXBException {

		if (jaxbContext == null) {
			jaxbContext = JAXBContext.newInstance(Carnet.class,
					InfoCarnet.class);
		}

		return jaxbContext;
	}

	@Activate
	private void activate() {
		try {
			jaxbContext = JAXBContext.newInstance(Carnet.class,
					InfoCarnet.class);
		} catch (JAXBException e) {
			logger.warn("CarnetServiceJAXBContextFactory - cannot instantiate JAXBContext: {}", e.getMessage());
		}
	}
}
