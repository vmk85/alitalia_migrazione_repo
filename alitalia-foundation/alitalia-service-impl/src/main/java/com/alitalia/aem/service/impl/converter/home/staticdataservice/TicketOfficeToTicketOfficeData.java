package com.alitalia.aem.service.impl.converter.home.staticdataservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.TicketOfficeData;
import com.alitalia.aem.ws.booking.staticdataservice.xsd3.ArrayOfstring;
import com.alitalia.aem.ws.booking.staticdataservice.xsd5.TicketOffice;

@Component(immediate=true, metatype=false)
@Service(value=TicketOfficeToTicketOfficeData.class)
public class TicketOfficeToTicketOfficeData implements Converter<TicketOffice, TicketOfficeData> {

	@Override
	public TicketOfficeData convert(TicketOffice source) {
		TicketOfficeData destination = new TicketOfficeData();
		ArrayOfstring arrayOfString = source.getDescriptions().getValue();
		destination.setDescriptions(arrayOfString.getString());
		destination.setLinkUrl(source.getLinkUrl().getValue());
		return destination;
	}
}