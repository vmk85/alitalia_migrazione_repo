package com.alitalia.aem.service.impl.home;

import javax.xml.ws.Holder;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.AgencyRetrieveIataGroupsRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrieveIataGroupsResponse;
import com.alitalia.aem.service.api.home.WebAgencyService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.webagencyservice.ClAgencyDataToAgencyRetrieveIataGroupsResponse;
import com.alitalia.aem.ws.webagency.WsWebAgencyClient;
import com.alitalia.aem.ws.webagency.xsd.ClAgencyData;

@Service
@Component(immediate=true, metatype=false)
public class SimpleWebAgencyService implements WebAgencyService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleStaticDataService.class);

	@Reference
	private WsWebAgencyClient webAgencyClient;
	
	@Reference
	private ClAgencyDataToAgencyRetrieveIataGroupsResponse clAgencyDataConverter;

	@Override
	public AgencyRetrieveIataGroupsResponse retrieveIataAgencyInfo(AgencyRetrieveIataGroupsRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveIataAgencyInfo. The request is {}", request);
		}
		

		try {
			AgencyRetrieveIataGroupsResponse response = new AgencyRetrieveIataGroupsResponse();
			String agnCode = request.getCodiceAgenzia();
			Holder<ClAgencyData> agencyData = new Holder<ClAgencyData>();
			Holder<Short> agyIsEnabledResult = new Holder<Short>();

			webAgencyClient.agyIsEnabled(agnCode, agencyData, agyIsEnabledResult);
			if ((agyIsEnabledResult != null) && (agyIsEnabledResult.value == 0) && (agencyData.value != null))
				response = clAgencyDataConverter.convert(agencyData.value);
			else
				response.setValidResponse(false);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
			
			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing retrieveIataAgencyInfo: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public AgencyRetrieveIataGroupsResponse retrieveNonIataAgencyInfo(AgencyRetrieveIataGroupsRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retieveNonIataAgencyInfo. The request is {}", request);
		}
		

		try {
			
			AgencyRetrieveIataGroupsResponse response = new AgencyRetrieveIataGroupsResponse();
			String agnCode = request.getCodiceAgenzia();
			Holder<ClAgencyData> agencyData = new Holder<ClAgencyData>();
			Holder<Short> agyNoIataIsEnabledResult = new Holder<Short>();

			webAgencyClient.agyNoIataIsEnabled(agnCode, agencyData, agyNoIataIsEnabledResult);
			if ((agyNoIataIsEnabledResult != null) && (agyNoIataIsEnabledResult.value == 0) && (agencyData.value != null))
				response = clAgencyDataConverter.convert(agencyData.value);
			else
				response.setValidResponse(false);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
			
			return response;
		
		} catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing retrieveNonIataAgencyInfo: Sid ["+request.getSid()+"]" , e);
		}
	}
}