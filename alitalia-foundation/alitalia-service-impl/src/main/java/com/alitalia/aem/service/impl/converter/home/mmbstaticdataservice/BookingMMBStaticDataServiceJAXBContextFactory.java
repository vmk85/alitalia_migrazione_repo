package com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.AirportCheckin;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.AmericanStates;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.Country;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.FrequentFlyerCarrier;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd3.Meal;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd3.PaymentTypeItem;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd3.PhonePrefix;


@Component(immediate=true, metatype=false)
@Service(value=BookingMMBStaticDataServiceJAXBContextFactory.class)
public class BookingMMBStaticDataServiceJAXBContextFactory {

	private static final Logger logger = LoggerFactory.getLogger(BookingMMBStaticDataServiceJAXBContextFactory.class);

	private JAXBContext jaxbContext = null;

	public synchronized JAXBContext getJaxbContext() throws JAXBException {

		if (jaxbContext == null) {
			jaxbContext = JAXBContext.newInstance(AirportCheckin.class,
					AmericanStates.class,
					Country.class,
					FrequentFlyerCarrier.class,
					Meal.class,
					PaymentTypeItem.class,
					PhonePrefix.class);
		}

		return jaxbContext;
	}

	@Activate
	private void activate() {
		try {
			jaxbContext = JAXBContext.newInstance(AirportCheckin.class,
					AmericanStates.class,
					Country.class,
					FrequentFlyerCarrier.class,
					Meal.class,
					PaymentTypeItem.class,
					PhonePrefix.class);
		} catch (JAXBException e) {
			logger.warn("BookingSearchServiceJAXBContextFactory - cannot instantiate JAXBContext: {}", e.getMessage());
		}
	}
}
