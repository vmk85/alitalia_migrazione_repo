/**
 * 
 */
package com.alitalia.aem.service.impl.converter.home.checkinsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationRequest;
import com.alitalia.aem.ws.checkin.searchservice.xsd1.GetRoutesRequest;
import com.alitalia.aem.ws.checkin.searchservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.searchservice.xsd1.Resource;


/**
 * @author R.Capitini
 *
 */
@Component(immediate=true, metatype=false)
@Service(value=RetrieveMmbPnrInformationRequestToGetRoutesRequest.class)
public class RetrieveMmbPnrInformationRequestToGetRoutesRequest implements Converter<RetriveMmbPnrInformationRequest, GetRoutesRequest> {

	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;

	@Override
	public GetRoutesRequest convert(RetriveMmbPnrInformationRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		GetRoutesRequest destination = null;

		if (source != null) {
			
			destination = objectFactory.createGetRoutesRequest();
			destination.setLastName(objectFactory.createGetRoutesRequestLastName(source.getLastName()));
			destination.setName(objectFactory.createGetRoutesRequestName(source.getName()));
			destination.setLocale(objectFactory.createGetRoutesRequestLocale(source.getLocale()));
			destination.setTripID(objectFactory.createGetRoutesRequestTripID(source.getTripId()));
			destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			destination.setX003CResourceX003EKBackingField(Resource.WEB);

			// Non sono valorizzati Eticket, FrequentFlyerCode, Pnr. E' gia' presente il TripId che puo' essere il codice MM del cliente,
			// il PNR o il numero di biglietto
		}

		return destination;
	}

}
