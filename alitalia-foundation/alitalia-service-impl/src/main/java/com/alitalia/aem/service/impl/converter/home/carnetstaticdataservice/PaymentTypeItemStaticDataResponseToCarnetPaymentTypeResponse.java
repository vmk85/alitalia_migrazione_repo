package com.alitalia.aem.service.impl.converter.home.carnetstaticdataservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.carnet.CarnetPaymentTypeItemData;
import com.alitalia.aem.common.messages.home.CarnetPaymentTypeResponse;
import com.alitalia.aem.ws.carnet.staticdataservice.xsd1.PaymentTypeItemStaticDataResponse;
import com.alitalia.aem.ws.carnet.staticdataservice.xsd2.PaymentTypeItem;

@Component(immediate=true, metatype=false)
@Service(value=PaymentTypeItemStaticDataResponseToCarnetPaymentTypeResponse.class)
public class PaymentTypeItemStaticDataResponseToCarnetPaymentTypeResponse implements Converter<PaymentTypeItemStaticDataResponse, CarnetPaymentTypeResponse> {

	@Reference
	private PaymentTypeItemToCarnetPaymentTypeItemData paymentTypeDataConverter;
	
	@Override
	public CarnetPaymentTypeResponse convert(PaymentTypeItemStaticDataResponse source) {
		CarnetPaymentTypeResponse destination = null;

		if (source != null) {
			destination = new CarnetPaymentTypeResponse();
			
			List<CarnetPaymentTypeItemData> paymentTypeData = null;
			if (source.getEntities() != null && source.getEntities().getValue() != null
				&& source.getEntities().getValue().getPaymentTypeItem()!=null && source.getEntities().getValue().getPaymentTypeItem().size() > 0) {
				paymentTypeData = new ArrayList<CarnetPaymentTypeItemData>();
				for (PaymentTypeItem payTypeItem : source.getEntities().getValue().getPaymentTypeItem()){
					paymentTypeData.add(paymentTypeDataConverter.convert(payTypeItem));
				}
				destination.setPaymentTypeItem(paymentTypeData);
			}
		}
		return destination;
	}
}