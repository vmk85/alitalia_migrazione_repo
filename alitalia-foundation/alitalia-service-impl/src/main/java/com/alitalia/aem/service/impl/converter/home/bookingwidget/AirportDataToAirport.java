package com.alitalia.aem.service.impl.converter.home.bookingwidget;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.data.home.AirportDetailsData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.Airport;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.AreaValue;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.ObjectFactory;

@Component(immediate = true, metatype = false)
@Service(value = AirportDataToAirport.class)
public class AirportDataToAirport implements Converter<AirportData, Airport> {

	@Reference
	private AirportDetailsDataToAirportDetails airportDetailsDataConverter;

	@Override
	public Airport convert(AirportData source) {
		ObjectFactory factory5 = new ObjectFactory();
		Airport airport = factory5.createAirport();

		airport.setArea(AreaValue.fromValue(source.getArea().value()));
		airport.setAZDeserves(source.getAzDeserves());
		airport.setCity(factory5.createAirportCity(source.getCity()));
		airport.setCityCode(factory5.createAirportCityCode(source.getCityCode()));
		airport.setCityUrl(factory5.createAirportCityUrl(source.getCityUrl()));
		airport.setCode(factory5.createAirportCode(source.getCode()));
		airport.setCountry(factory5.createAirportCountry(source.getCountry()));
		airport.setCountryCode(factory5.createAirportCountryCode(source.getCountryCode()));
		airport.setDescription(factory5.createAirportDescription(source.getDescription()));
		airport.setEligibility(source.getEligibility());
		airport.setEtktAvlblFrom(factory5.createAirportEtktAvlblFrom(XsdConvertUtils.toXMLGregorianCalendar(source.getEtktAvlblFrom())));
		airport.setHiddenInFirstDeparture(source.getHiddenInFirstDeparture());
		airport.setIdMsg(source.getIdMsg());
		airport.setName(factory5.createAirportName(source.getName()));
		airport.setState(factory5.createAirportState(source.getState()));
		airport.setStateCode(factory5.createAirportStateCode(source.getStateCode()));
		airport.setUnavlblFrom(factory5.createAirportUnavlblFrom(XsdConvertUtils.toXMLGregorianCalendar(source.getUnavlblFrom())));
		airport.setUnavlblUntil(factory5.createAirportUnavlblUntil(XsdConvertUtils.toXMLGregorianCalendar(source.getUnavlblUntil())));

		AirportDetailsData sourceDetails = source.getDetails();
		if (sourceDetails != null)
			airport.setDetails(factory5.createAirportDetails(airportDetailsDataConverter.convert(sourceDetails)));
		else
			airport.setDetails(factory5.createAirportDetails(null));
		
		return airport;
	}
}
