package com.alitalia.aem.service.impl.converter.home.checkinboardingpassservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.SendReminderCheckinResponse;


@Component(immediate=true, metatype=false)
@Service(value=SendReminderResponseToSendReminderCheckinResponse.class)
public class SendReminderResponseToSendReminderCheckinResponse implements Converter<Boolean, SendReminderCheckinResponse> {


	@Override
	public SendReminderCheckinResponse convert(Boolean source) {
		
		SendReminderCheckinResponse destination = null;

		if (source != null) {
			
			destination = new SendReminderCheckinResponse();
			
			destination.setSendReminderResult(source);
		}

		return destination;
	}
}
