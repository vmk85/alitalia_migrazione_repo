package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.DeletePanRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.DeletePanSIARequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;

@Component(immediate = true, metatype = false)
@Service(value = DeletePanRequestToDeletePanSIARequest.class)
public class DeletePanRequestToDeletePanSIARequest implements Converter<DeletePanRequest, DeletePanSIARequest> {

	@Reference
	private MMCustomerProfileDataToCustomerProfile mmCustomerProfileDataConverter;
	
	@Override
	public DeletePanSIARequest convert(DeletePanRequest source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		
		ObjectFactory objectFactory = new ObjectFactory();
		
		DeletePanSIARequest destination = objectFactory.createDeletePanSIARequest();
		CustomerProfile customerProfile = mmCustomerProfileDataConverter.convert(source.getCustomerProfile());
		
		JAXBElement<CustomerProfile> jaxbCustomerProfile = objectFactory.createCustomerRequestCustomer(customerProfile);
		JAXBElement<String> jaxbPan = objectFactory.createDeletePanSIARequestPanCode(source.getPanCode());
		destination.setCustomer(jaxbCustomerProfile);
		destination.setPanCode(jaxbPan);
		
		return destination;
	}
}