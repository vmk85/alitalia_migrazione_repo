package com.alitalia.aem.service.impl.converter.home.staticdataservice;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.booking.staticdataservice.xsd2.Country;
import com.alitalia.aem.ws.booking.staticdataservice.xsd2.Municipality;
import com.alitalia.aem.ws.booking.staticdataservice.xsd5.Airport;
import com.alitalia.aem.ws.booking.staticdataservice.xsd5.Meal;
import com.alitalia.aem.ws.booking.staticdataservice.xsd5.PaymentTypeItem;
import com.alitalia.aem.ws.booking.staticdataservice.xsd5.PhonePrefix;
import com.alitalia.aem.ws.booking.staticdataservice.xsd5.SeatType;
import com.alitalia.aem.ws.booking.staticdataservice.xsd5.TicketOffice;
import com.alitalia.aem.ws.booking.staticdataservice.xsd8.FrequentFlyerType;

@Component(immediate=true, metatype=false)
@Service(value=StaticDataServiceJAXBContextFactory.class)
public class StaticDataServiceJAXBContextFactory {

	private static final Logger logger = LoggerFactory.getLogger(StaticDataServiceJAXBContextFactory.class);

	private JAXBContext jaxbContext = null;

	public synchronized JAXBContext getJaxbContext() throws JAXBException {

		if (jaxbContext == null) {
			jaxbContext = JAXBContext.newInstance(PhonePrefix.class,
								PaymentTypeItem.class,
								Country.class,
								TicketOffice.class,
								SeatType.class,
								Meal.class,
								Country.class,
								Municipality.class,
								Airport.class,
								FrequentFlyerType.class
								);
		}

		return jaxbContext;
	}

	@Activate
	private void activate() {
		try {
			jaxbContext = JAXBContext.newInstance(PhonePrefix.class,
					PaymentTypeItem.class,
					Country.class,
					TicketOffice.class,
					SeatType.class,
					Meal.class,
					Country.class,
					Municipality.class,
					Airport.class,
					FrequentFlyerType.class
					);
		} catch (JAXBException e) {
			logger.warn("BookingSearchServiceJAXBContextFactory - cannot instantiate JAXBContext: {}", e.getMessage());
		}
	}
}
