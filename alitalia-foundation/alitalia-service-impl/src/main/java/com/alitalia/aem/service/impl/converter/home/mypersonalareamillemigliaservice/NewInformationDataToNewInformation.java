package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.NewInformationData;
import com.alitalia.aem.common.data.home.enumerations.WorkPositionTypesEnum;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.NewInformation;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.WorkPositionTypes;

@Component(immediate = true, metatype = false)
@Service(value = NewInformationDataToNewInformation.class)
public class NewInformationDataToNewInformation implements Converter<NewInformationData, NewInformation> {

	
	@Override
	public NewInformation convert(NewInformationData source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		
		ObjectFactory objectFactory = new ObjectFactory();
		
		String pin = source.getPin();
		boolean isSmsAuthorization = source.isSmsAuthorization();
		boolean isSmsAuthorizationUpdated = source.isSmsAuthorizationUpdated();
		boolean isWorkPositionUpdated = source.isWorkPositionUpdated();
		WorkPositionTypesEnum workPositionEnum = source.getWorkPosition();
		
		NewInformation newInformation = objectFactory.createNewInformation();
		newInformation.setPIN(objectFactory.createNewInformationPIN(pin));
		newInformation.setSmsAuthorization(isSmsAuthorization);
		newInformation.setSmsAuthorizationUpdated(isSmsAuthorizationUpdated);
		newInformation.setWorkPositionUpdated(isWorkPositionUpdated);
		newInformation.setWorkPosition(
				workPositionEnum != null ? WorkPositionTypes.fromValue(workPositionEnum.value()) : null);
		
		return newInformation;
	}
}