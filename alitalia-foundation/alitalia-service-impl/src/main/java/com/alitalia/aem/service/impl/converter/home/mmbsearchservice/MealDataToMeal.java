package com.alitalia.aem.service.impl.converter.home.mmbsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MealData;
import com.alitalia.aem.ws.mmb.searchservice.xsd4.Meal;
import com.alitalia.aem.ws.mmb.searchservice.xsd4.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=MealDataToMeal.class)
public class MealDataToMeal implements Converter<MealData, Meal> {

	@Override
	public Meal convert(MealData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		Meal destination = null;

		if (source != null) {
			destination = objectFactory.createMeal();

			destination.setCode(objectFactory.createMealCode(source.getCode()));
			destination.setDescription(objectFactory.createMealDescription(source.getDescription()));
			destination.setText(objectFactory.createMealText(source.getText()));
		}

		return destination;
	}

}
