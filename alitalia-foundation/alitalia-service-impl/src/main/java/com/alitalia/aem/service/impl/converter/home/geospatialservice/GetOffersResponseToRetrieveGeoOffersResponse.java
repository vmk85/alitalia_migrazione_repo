package com.alitalia.aem.service.impl.converter.home.geospatialservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.data.home.BestPriceData;
import com.alitalia.aem.common.data.home.OfferData;
import com.alitalia.aem.common.data.home.PassengerNumbersData;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.data.home.enumerations.BestPriceTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.messages.home.RetrieveGeoOffersResponse;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.geospatial.service.xsd1.GetOffersResponse;
import com.alitalia.aem.ws.geospatial.service.xsd6.ArrayOfOffer;
import com.alitalia.aem.ws.geospatial.service.xsd6.BestPrice;
import com.alitalia.aem.ws.geospatial.service.xsd6.Offer;
import com.alitalia.aem.ws.geospatial.service.xsd6.Passenger;

@Component(immediate = true, metatype = false)
@Service(value = GetOffersResponseToRetrieveGeoOffersResponse.class)
public class GetOffersResponseToRetrieveGeoOffersResponse implements Converter<GetOffersResponse, RetrieveGeoOffersResponse> {

	@Reference
	private GeoAirportToAirportData geoAirportConverter;

	@Override
	public RetrieveGeoOffersResponse convert(GetOffersResponse source) {

		RetrieveGeoOffersResponse response = new RetrieveGeoOffersResponse();


		ArrayOfOffer array = source.getOffers().getValue();	
		List<Offer> offers = array.getOffer();

		List<OfferData> offersData = new ArrayList<>();
		for(Offer offer: offers){
			OfferData offerData = new OfferData();

			offerData.setArea(AreaValueEnum.fromValue(offer.getArea().value()));

			AirportData arrivalAirportData = geoAirportConverter.convert(offer.getArrivalAirport().getValue());
			offerData.setArrivalAirport(arrivalAirportData);
			offerData.setBestPrice(offer.getBestPrice());

			List<BestPriceData> bestPricesData = new ArrayList<>();
			if (offer.getBestPrices() != null && offer.getBestPrices().getValue() != null &&
					offer.getBestPrices().getValue().getBestPrice() != null){
				for(BestPrice bestPrice: offer.getBestPrices().getValue().getBestPrice()){
					BestPriceData bestPriceData = new BestPriceData();
					bestPriceData.setDate(XsdConvertUtils.parseCalendar(bestPrice.getDate()));
					bestPriceData.setPrice(bestPrice.getPrice());
					bestPriceData.setType(BestPriceTypeEnum.fromValue(bestPrice.getType().value()));

					bestPricesData.add(bestPriceData);
				}
			}

			offerData.setBestPrices(bestPricesData);
			offerData.setCabin(CabinEnum.fromValue(offer.getCabinType().value()));

			AirportData departureAirportData = geoAirportConverter.convert(offer.getDepartureAirport().getValue());
			offerData.setDepartureAirport(departureAirportData);

			XMLGregorianCalendar expireOfferDate = offer.getExpireOfferDate();
			if (expireOfferDate != null)
				offerData.setExpireOfferData(XsdConvertUtils.parseCalendar(expireOfferDate));

			offerData.setHomePageOffer(offer.isIsHomePageOffer());
			offerData.setNumberOfNights(offer.getNumberOfNights().getValue());

			List<PassengerNumbersData> passengersData = new ArrayList<>();
			List<Passenger> passengers =offer.getPassengersList().getValue().getPassenger();
			for(Passenger p: passengers){
				PassengerNumbersData passengerData = new PassengerNumbersData();
				passengerData.setNumber(p.getPassengerNumber());
				passengerData.setPassengerType(PassengerTypeEnum.fromValue(p.getType().value()));

				passengersData.add(passengerData);
			}

			offerData.setPassengerList(passengersData);
			offerData.setPriority(offer.getODpriority());
			offerData.setRouteType(RouteTypeEnum.fromValue(offer.getType().value()));

			offersData.add(offerData);
		}
		response.setOffers(offersData);
		return response;
	}

}