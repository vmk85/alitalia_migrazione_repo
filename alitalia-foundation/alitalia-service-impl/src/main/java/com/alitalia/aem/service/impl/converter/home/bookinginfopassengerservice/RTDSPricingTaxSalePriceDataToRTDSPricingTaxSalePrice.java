package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingTaxSalePriceData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionPricingTaxSalePrice;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingTaxSalePriceDataToRTDSPricingTaxSalePrice.class)
public class RTDSPricingTaxSalePriceDataToRTDSPricingTaxSalePrice implements
		Converter<ResultTicketingDetailSolutionPricingTaxSalePriceData, 
					ResultTicketingDetailSolutionPricingTaxSalePrice> {

	@Override
	public ResultTicketingDetailSolutionPricingTaxSalePrice convert(
			ResultTicketingDetailSolutionPricingTaxSalePriceData source) {
		ResultTicketingDetailSolutionPricingTaxSalePrice destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingTaxSalePrice();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
