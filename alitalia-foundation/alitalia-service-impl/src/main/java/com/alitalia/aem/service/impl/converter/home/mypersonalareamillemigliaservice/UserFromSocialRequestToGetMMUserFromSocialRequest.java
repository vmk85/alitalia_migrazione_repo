package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.UserFromSocialRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.GetMMUserFromSocialRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;

@Component(immediate = true, metatype = false)
@Service(value = UserFromSocialRequestToGetMMUserFromSocialRequest.class)
public class UserFromSocialRequestToGetMMUserFromSocialRequest 
		implements Converter<UserFromSocialRequest, GetMMUserFromSocialRequest> {

	@Override
	public GetMMUserFromSocialRequest convert(UserFromSocialRequest source) {
		GetMMUserFromSocialRequest destination = null;

		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			destination = objectFactory.createGetMMUserFromSocialRequest();
			
			destination.setGigyaId(
					objectFactory.createGetMMUserFromSocialRequestGigyaId(
							source.getGigyaId()));
		}

		return destination;
	}

}
