package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.RetrieveCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCountriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveCreditCardCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCreditCardCountriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveFrequentFlayerRequest;
import com.alitalia.aem.common.messages.home.RetrieveMealsRequest;
import com.alitalia.aem.common.messages.home.RetrieveMealsResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbAirportCheckinRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAirportCheckinResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbAmericanStatesRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAmericanStatesResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbFrequentFlyerCarriersResponse;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemRequest;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemResponse;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixRequest;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixResponse;
import com.alitalia.aem.common.messages.home.RetrieveProvincesRequest;
import com.alitalia.aem.common.messages.home.RetrieveProvincesResponse;
import com.alitalia.aem.service.api.home.MmbStaticDataService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice.ArrayOfAirportCheckinToRetrieveMmbAirportCheckinResponseConverter;
import com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice.ArrayOfAmericanStatesToRetrieveMmbAmericanStatesResponseConverter;
import com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice.ArrayOfCountryToRetrieveCountryResponseConverter;
import com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice.ArrayOfFrequentFlyerCarrierToRetrieveMmbFrequentFlyerCarriersResponseConverter;
import com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice.ArrayOfMealToRetrieveMealsResponseConverter;
import com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice.ArrayOfPaymentTypeItemToRetrievePaymentTypeItemResponseConverter;
import com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice.ArrayOfPhonePrefixToRetrievePhonePrefixResponseConverter;
import com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice.RetrieveCountriesRequestToArrayOfDictionaryItemConverter;
import com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice.RetrieveCreditCardCountriesRequestToStaticDataRequestConverter;
import com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice.RetrieveFrequentFlyerRequestToArrayOfDictionaryItemConverter;
import com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice.RetrieveMealsRequestToArrayOfDictionaryItemConverter;
import com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice.RetrieveMmbAirportCheckinRequestToArrayOfDictionaryItemConverter;
import com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice.RetrieveMmbAmericanStatesRequestToArrayOfDictionaryItemConverter;
import com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice.RetrievePaymentTypeItemRequestToArrayOfDictionaryItemConverter;
import com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice.RetrievePhonePrefixRequestToArrayOfDictionaryItemConverter;
import com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice.RetrieveProvincesRequestToStaticDataRequestConverter;
import com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice.StaticDataResponseToRetrieveCreditCardCountriesResponseConverter;
import com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice.StaticDataResponseToRetrieveProvincesResponseConverter;
import com.alitalia.aem.ws.mmb.staticdataservice.MmbStaticDataServiceClient;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd1.StaticDataRequest;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.ArrayOfAirportCheckin;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.ArrayOfAmericanStates;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.ArrayOfCountry;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.ArrayOfDictionaryItem;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.ArrayOfFrequentFlyerCarrier;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd3.ArrayOfMeal;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd3.ArrayOfPaymentTypeItem;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd3.ArrayOfPhonePrefix;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd6.StaticDataResponse;

@Component(immediate=true, metatype=false)
@Service
public class SimpleMmbStaticDataService implements MmbStaticDataService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleMmbStaticDataService.class);

	@Reference
	private MmbStaticDataServiceClient mmbStaticDataServiceClient;

	@Reference
	private RetrieveProvincesRequestToStaticDataRequestConverter retrieveProvincesRequestConverter;

	@Reference
	private StaticDataResponseToRetrieveProvincesResponseConverter retrieveProvincesResponseConverter;

	@Reference
	private RetrieveCreditCardCountriesRequestToStaticDataRequestConverter retrieveCreditCardCountriesRequestConverter;

	@Reference
	private StaticDataResponseToRetrieveCreditCardCountriesResponseConverter retrieveCreditCardCountriesResponseConverter;

	@Reference
	private RetrieveCountriesRequestToArrayOfDictionaryItemConverter retrieveCountriesRequestConverter;

	@Reference
	private ArrayOfCountryToRetrieveCountryResponseConverter retrieveCountryResponseConverter;

	@Reference
	private RetrievePhonePrefixRequestToArrayOfDictionaryItemConverter retrievePhonePrefixRequestConverter;

	@Reference
	private ArrayOfPhonePrefixToRetrievePhonePrefixResponseConverter retrievePhonePrefixResponseConverter;

	@Reference
	private RetrievePaymentTypeItemRequestToArrayOfDictionaryItemConverter retrievePaymentTypeItemRequestConverter;

	@Reference
	private ArrayOfPaymentTypeItemToRetrievePaymentTypeItemResponseConverter retrievePaymentTypeItemResponseConverter;

	@Reference
	private RetrieveMealsRequestToArrayOfDictionaryItemConverter retrieveMealsRequestConverter;

	@Reference
	private ArrayOfMealToRetrieveMealsResponseConverter retrieveMealsResponseConverter;

	@Reference
	private RetrieveFrequentFlyerRequestToArrayOfDictionaryItemConverter retrieveFrequentFlyerRequestConverter;

	@Reference
	private ArrayOfFrequentFlyerCarrierToRetrieveMmbFrequentFlyerCarriersResponseConverter retrieveMmbFrequentFlyerCarriersResponseConverter;

	@Reference
	private RetrieveMmbAirportCheckinRequestToArrayOfDictionaryItemConverter retrieveMmbAirportCheckinRequestConverter;

	@Reference
	private ArrayOfAirportCheckinToRetrieveMmbAirportCheckinResponseConverter retrieveMmbAirportCheckinResponseConverter;

	@Reference
	private RetrieveMmbAmericanStatesRequestToArrayOfDictionaryItemConverter retrieveMmbAmericanStatesRequestConverter;

	@Reference
	private ArrayOfAmericanStatesToRetrieveMmbAmericanStatesResponseConverter retrieveMmbAmericanStatesResponseConverter;

	@Override
	public RetrieveProvincesResponse retrieveProvinces(RetrieveProvincesRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveProvinces(). The request is {}", request);
		}
		

		try {
			RetrieveProvincesResponse response = new RetrieveProvincesResponse();
			StaticDataRequest serviceRequest = retrieveProvincesRequestConverter.convert(request);
			StaticDataResponse serviceResponse = mmbStaticDataServiceClient.getData(serviceRequest);
			response = retrieveProvincesResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully retrieveProvinces(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method retrieveProvinces().", e);
			throw new AlitaliaServiceException("Exception executing retrieveProvinces: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public RetrieveCreditCardCountriesResponse retrieveCreditCardCountries(RetrieveCreditCardCountriesRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveCreditCardCountries(). The request is {}", request);
		}
		

		try {
			RetrieveCreditCardCountriesResponse response = new RetrieveCreditCardCountriesResponse();
			StaticDataRequest serviceRequest = retrieveCreditCardCountriesRequestConverter.convert(request);
			StaticDataResponse serviceResponse = mmbStaticDataServiceClient.getData(serviceRequest);
			response = retrieveCreditCardCountriesResponseConverter.convert(serviceResponse);
			
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully retrieveCreditCardCountries(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method retrieveCreditCardCountries().", e);
			throw new AlitaliaServiceException("Exception executing retrieveCreditCardCountries: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public RetrieveCountriesResponse retrieveCountries(RetrieveCountriesRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveCountries(). The request is {}", request);
		}
		

		try {
			RetrieveCountriesResponse response = new RetrieveCountriesResponse();
			ArrayOfDictionaryItem serviceRequest = retrieveCountriesRequestConverter.convert(request);
			ArrayOfCountry serviceResponse = mmbStaticDataServiceClient.loadCountry(serviceRequest);
			response = retrieveCountryResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully retrieveCountries(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method retrieveCountries().", e);
			throw new AlitaliaServiceException("Exception executing retrieveCountries: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public RetrievePhonePrefixResponse retrievePhonePrefix(RetrievePhonePrefixRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrievePhonePrefix(). The request is {}", request);
		}
		

		try {
			RetrievePhonePrefixResponse response = new RetrievePhonePrefixResponse();
			ArrayOfDictionaryItem serviceRequest = retrievePhonePrefixRequestConverter.convert(request);
			ArrayOfPhonePrefix serviceResponse = mmbStaticDataServiceClient.loadPhonePrefix(serviceRequest);
			response = retrievePhonePrefixResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully retrievePhonePrefix(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method retrievePhonePrefix().", e);
			throw new AlitaliaServiceException("Exception executing retrievePhonePrefix: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public RetrievePaymentTypeItemResponse retrievePaymentTypeItem(RetrievePaymentTypeItemRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrievePaymentTypeItem(). The request is {}", request);
		}
		

		try {
			RetrievePaymentTypeItemResponse response = new RetrievePaymentTypeItemResponse();
			ArrayOfDictionaryItem serviceRequest = retrievePaymentTypeItemRequestConverter.convert(request);
			ArrayOfPaymentTypeItem serviceResponse = mmbStaticDataServiceClient.loadPaymentTypeItem(serviceRequest);
			response = retrievePaymentTypeItemResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully retrievePaymentTypeItem(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method retrievePaymentTypeItem().", e);
			throw new AlitaliaServiceException("Exception executing retrievePaymentTypeItem: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public RetrieveMealsResponse retrieveMeals(RetrieveMealsRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveMeals(). The request is {}", request);
		}
	

		try {
			RetrieveMealsResponse response = new RetrieveMealsResponse();
			ArrayOfDictionaryItem serviceRequest = retrieveMealsRequestConverter.convert(request);
			ArrayOfMeal serviceResponse = mmbStaticDataServiceClient.loadMeal(serviceRequest);
			response = retrieveMealsResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully retrieveMeals(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method retrieveMeals().", e);
			throw new AlitaliaServiceException("Exception executing retrieveMeals: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public RetrieveMmbFrequentFlyerCarriersResponse retrieveFrequentFlyers(RetrieveFrequentFlayerRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveFrequentFlyers(). The request is {}", request);
		}
		

		try {
			RetrieveMmbFrequentFlyerCarriersResponse response = new RetrieveMmbFrequentFlyerCarriersResponse();
			ArrayOfDictionaryItem serviceRequest = retrieveFrequentFlyerRequestConverter.convert(request);
			ArrayOfFrequentFlyerCarrier serviceResponse = mmbStaticDataServiceClient.loadFrequentFlyersList(serviceRequest);
			response = retrieveMmbFrequentFlyerCarriersResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully retrieveFrequentFlyers(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method retrieveFrequentFlyers().", e);
			throw new AlitaliaServiceException("Exception executing retrieveFrequentFlyers: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public RetrieveMmbAirportCheckinResponse retrieveAirportCheckIn(RetrieveMmbAirportCheckinRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveAirportCheckIn(). The request is {}", request);
		}
		

		try {
			RetrieveMmbAirportCheckinResponse response = new RetrieveMmbAirportCheckinResponse();
			ArrayOfDictionaryItem serviceRequest = retrieveMmbAirportCheckinRequestConverter.convert(request);
			ArrayOfAirportCheckin serviceResponse = mmbStaticDataServiceClient.loadAirportCheckin(serviceRequest);
			response = retrieveMmbAirportCheckinResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully retrieveAirportCheckIn(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method retrieveAirportCheckIn().", e);
			throw new AlitaliaServiceException("Exception executing retrieveAirportCheckIn: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public RetrieveMmbAmericanStatesResponse retrieveAmericanStates(RetrieveMmbAmericanStatesRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveAmericanStates(). The request is {}", request);
		}
		

		try {
			RetrieveMmbAmericanStatesResponse response = new RetrieveMmbAmericanStatesResponse();
			ArrayOfAmericanStates serviceResponse = mmbStaticDataServiceClient.loadAmericanStates();
			response = retrieveMmbAmericanStatesResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully retrieveAmericanStates(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method retrieveAmericanStates().", e);
			throw new AlitaliaServiceException("Exception executing retrieveAmericanStates: Sid ["+request.getSid()+"]" , e);
		}
	}

}
