package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.CreditCardsListRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.GetCreditCardListRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;

@Component(immediate = true, metatype = false)
@Service(value = CreditCardsListRequestToGetCardListRequest.class)
public class CreditCardsListRequestToGetCardListRequest implements Converter<CreditCardsListRequest, GetCreditCardListRequest> {

	@Reference
	private MMCustomerProfileDataToCustomerProfile mmCustomerProfileDataConverter;
	
	@Override
	public GetCreditCardListRequest convert(CreditCardsListRequest source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		
		ObjectFactory objectFactory = new ObjectFactory();
		
		GetCreditCardListRequest destination = objectFactory.createGetCreditCardListRequest();
		CustomerProfile customerProfile = mmCustomerProfileDataConverter.convert(source.getCustomerProfile());
		
		JAXBElement<CustomerProfile> jaxbCustomerProfile = objectFactory.createCustomerRequestCustomer(customerProfile);
		destination.setCustomer(jaxbCustomerProfile);
				
		return destination;
	}
}