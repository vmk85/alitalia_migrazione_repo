package com.alitalia.aem.service.impl.converter.home.flightstatusservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveFullItinerariesRequest;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetIteneraryListModelFullRequest;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveFullItinerariesRequestToGetItineraryListModelFullRequest.class)
public class RetrieveFullItinerariesRequestToGetItineraryListModelFullRequest implements Converter<RetrieveFullItinerariesRequest, GetIteneraryListModelFullRequest> {

	@Override
	public GetIteneraryListModelFullRequest convert(RetrieveFullItinerariesRequest source) {
		GetIteneraryListModelFullRequest request = new GetIteneraryListModelFullRequest();
		
		
		
		return request;
	}
}