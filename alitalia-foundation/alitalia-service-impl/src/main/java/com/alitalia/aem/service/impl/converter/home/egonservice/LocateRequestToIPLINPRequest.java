package com.alitalia.aem.service.impl.converter.home.egonservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.LocateRequest;
import com.alitalia.aem.ws.egon.xsd.IPLAREAINP;
import com.alitalia.aem.ws.egon.xsd.IPLINP;
import com.alitalia.aem.ws.egon.xsd.ObjectFactory;
import com.alitalia.aem.ws.egon.xsd.PAR;

@Component(immediate = true, metatype = false)
@Service(value = LocateRequestToIPLINPRequest.class)
public class LocateRequestToIPLINPRequest implements Converter<LocateRequest, IPLINP> {

	@Override
	public IPLINP convert(LocateRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		
		IPLINP request = objectFactory.createIPLINP();
		
		String cdxipa = source.getCdxipa();
		String par1 = source.getFlxpar1();
		String par2 = source.getFlxpar2();
		String par3 = source.getFlxpar3();
		String user = source.getWpuser();
		String password = source.getWppasw();
		
		IPLAREAINP iplareainp = objectFactory.createIPLAREAINP();
		JAXBElement<String> jaxbCdxipa = objectFactory.createIPLAREAINPCDXIPA(cdxipa);
		iplareainp.setCDXIPA(jaxbCdxipa);
		
		PAR par = objectFactory.createPAR();
		
		JAXBElement<String> jaxbPar1 = objectFactory.createPARFLXPAR1(par1);
		JAXBElement<String> jaxbPar2 = objectFactory.createPARFLXPAR2(par2);
		JAXBElement<String> jaxbPar3 = objectFactory.createPARFLXPAR2(par3);
		
		JAXBElement<String> jaxbUser = objectFactory.createPARWPUSER(user);
		JAXBElement<String> jaxbPassword = objectFactory.createPARWPPASW(password);
		
		par.setFLXPAR1(jaxbPar1);
		par.setFLXPAR2(jaxbPar2);
		par.setFLXPAR3(jaxbPar3);
		
		par.setWPUSER(jaxbUser);
		par.setWPPASW(jaxbPassword);
		
		JAXBElement<PAR> jaxbPar = objectFactory.createIPLINPPAR(par);
		
		request.setIPLAREAINP(iplareainp);
		request.setPAR(jaxbPar);
		
		return request;
	}
}