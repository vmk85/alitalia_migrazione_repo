package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareBookingInfoSegmentData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingFareBookingInfoSegment;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareBookingInfoSegmentDataToRTDSPricingFareBookingInfoSegment.class)
public class RTDSPricingFareBookingInfoSegmentDataToRTDSPricingFareBookingInfoSegment implements
		Converter<ResultTicketingDetailSolutionPricingFareBookingInfoSegmentData, 
					ResultTicketingDetailSolutionPricingFareBookingInfoSegment> {

	@Reference
	private RTDSPricingFareBookingInfoSegmentFlightDataToRTDSPricingFareBookingInfoSegmentFlight rtdsFlightFieldConverter;

	@Override
	public ResultTicketingDetailSolutionPricingFareBookingInfoSegment convert(
			ResultTicketingDetailSolutionPricingFareBookingInfoSegmentData source) {
		ResultTicketingDetailSolutionPricingFareBookingInfoSegment destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingFareBookingInfoSegment();

			destination.setDepartureField(source.getDepartureField());
			destination.setDestinationField(source.getDestinationField());

			destination.setFlightField(rtdsFlightFieldConverter.convert(source.getFlightField()));

			destination.setHashField(source.getHashField());
			destination.setOriginField(source.getOriginField());
		}

		return destination;
	}

}
