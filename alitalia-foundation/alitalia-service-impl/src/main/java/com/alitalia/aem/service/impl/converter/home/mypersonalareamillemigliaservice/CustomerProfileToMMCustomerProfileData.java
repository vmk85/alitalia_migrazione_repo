package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import java.util.Calendar;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMActivityData;
import com.alitalia.aem.common.data.home.MMAddressData;
import com.alitalia.aem.common.data.home.MMContractData;
import com.alitalia.aem.common.data.home.MMCreditCardData;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.data.home.MMFlightData;
import com.alitalia.aem.common.data.home.MMLifestyleData;
import com.alitalia.aem.common.data.home.MMMessageData;
import com.alitalia.aem.common.data.home.MMPreferenceData;
import com.alitalia.aem.common.data.home.MMProgramData;
import com.alitalia.aem.common.data.home.MMTelephoneData;
import com.alitalia.aem.common.data.home.enumerations.MMAddressTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMGenderTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMMaritalStatusTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMRegistrationTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMSystemChannelTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMTierCodeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMTitleTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMWorkPositionTypeEnum;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.AddressTypes;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.GenderTypes;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.MaritalStatusTypes;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.RegistrationTypes;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.SystemChannelTypes;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.TierCodeTypes;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.TitleTypes;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.WorkPositionTypes;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd3.ArrayOfanyType;

@Component(immediate=true, metatype=false)
@Service(value=CustomerProfileToMMCustomerProfileData.class)
public class CustomerProfileToMMCustomerProfileData implements Converter<CustomerProfile, MMCustomerProfileData> {

	@Reference
	private ActivitiesToMMActivitiesData activitiesConverter;
	
	@Reference
	private AddressesToMMAddresses addressesConverter;
	
	@Reference
	private ContractToMMContractData contractsConverter;
	
	@Reference
	private CreditCardsToMMCreditCardsData creditCardsConverter;
	
	@Reference
	private MessageToMMMessageData messagesConverter;
	
	@Reference
	private PreferencesToMMPreferencesData preferencesConverter;
	
	@Reference
	private ProgramsToMMProgramsData programsConverter;
	
	@Reference
	private TelephonesToMMTelephonesData telephonesConverter;
	
	@Reference
	private FlightsToMMFlightsData flightConverter;
	
	@Reference
	private LifestylesToMMLifestylesData lifestylesConverter;
	
	@Override
	public MMCustomerProfileData convert(CustomerProfile source) {
		if(source == null) throw new IllegalArgumentException("CustomerProfile is null.");

		MMCustomerProfileData destination = null;

		if (source != null) {
			destination = new MMCustomerProfileData();
			XMLGregorianCalendar xmlBirthDate = source.getBirthDate();
			String contractType = source.getContratcType() != null ? source
					.getContratcType().getValue() : "";
			String name = source.getCustomerName() != null ? source
					.getCustomerName().getValue() : "";
			String nickname = source.getCustomerNickName() != null ? source
					.getCustomerNickName().getValue() : "";
			String surname = source.getCustomerSurname() != null ? source
					.getCustomerSurname().getValue() : "";
			String fiscalCode = source.getFiscalCode() != null ? source
					.getFiscalCode().getValue() : "";
			String language = source.getLanguage() != null ? source
					.getLanguage().getValue() : "";
			Boolean isContractAgree = source.isContractAgree();
			GenderTypes gender = source.getGender();
			String number = source.getCustomerNumber() != null ? source
					.getCustomerNumber().getValue() : "";
			String pinCode = source.getCustomerPinCode() != null ? source
					.getCustomerPinCode().getValue() : "";
			String email = source.getEmail() != null ? source.getEmail()
					.getValue() : "";
			TitleTypes titleType = source.getCustomerTitle();
			WorkPositionTypes workPositionType = source
					.getCustomerWorkPosition();
			String defaultAddressCountry = source.getDefaultAddressCountry() != null ? source
					.getDefaultAddressCountry().getValue() : "";
			String defaultAddressMunicipalityName = source
					.getDefaultAddressMunicipalityName() != null ? source
					.getDefaultAddressMunicipalityName().getValue() : "";
			String defaultAddressPostalCode = source
					.getDefaultAddressPostalCode() != null ? source
					.getDefaultAddressPostalCode().getValue() : "";
			String defaultAddressStateCode = source
					.getDefaultAddressStateCode() != null ? source
					.getDefaultAddressStateCode().getValue() : "";
			String defaultAddressStreetFreeText = source
					.getDefaultAddressStreetFreeText() != null ? source
					.getDefaultAddressStreetFreeText().getValue() : "";
			AddressTypes defaultAddressType = source.getDefaultAddressType();
			String mailingType = source.getMailingType() != null ? source
					.getMailingType().getValue() : "";
			MaritalStatusTypes maritalStatusType = source.getMaritalStatus();
			String market = source.getMarket() != null ? source.getMarket()
					.getValue() : "";
			ArrayOfanyType arrayOfActivities = source.getActivities() != null ? source
					.getActivities().getValue() : null;
			ArrayOfanyType arrayOfAddresses = source.getAddresses() != null ? source
					.getAddresses().getValue() : null;
			ArrayOfanyType arrayOfContracts = source.getContracts() != null ? source
					.getContracts().getValue() : null;
			ArrayOfanyType arrayOfCreditCards = source.getCreditCards() != null ? source
					.getCreditCards().getValue() : null;
			ArrayOfanyType arrayOfFlights = source.getFlights() != null ? source
					.getFlights().getValue() : null;
			ArrayOfanyType arrayOfLifestyles = source.getLifestyles() != null ? source
					.getLifestyles().getValue() : null;
			ArrayOfanyType arrayOfMessages = source.getMessages() != null ? source
					.getMessages().getValue() : null;
			ArrayOfanyType arrayOfPreferences = source.getPreferences() != null ? source
					.getPreferences().getValue() : null;
			ArrayOfanyType arrayOfPrograms = source.getPrograms() != null ? source
					.getPrograms().getValue() : null;
			ArrayOfanyType arrayOfTelephones = source.getTelephones() != null ? source
					.getTelephones().getValue() : null;
			XMLGregorianCalendar xmlPassportDate = source.getPassportDate();
			Boolean isProfiling = source.isProfiling();
			String professionalName = source.getProfessionalName() != null ? source
					.getProfessionalName().getValue() : "";
			RegistrationTypes registrationType = source
					.getRegistrationRequired();
			Boolean isSmsAuthorization = source.isSmsAuthorization();
			SystemChannelTypes systemChannelType = source.getSystemChannel();
			String statusCode = source.getStatusCode() != null ? source
					.getStatusCode().getValue() : "";
			XMLGregorianCalendar xmlTierDate = source.getTierDate();
			TierCodeTypes tierCode = source.getTierCode();
			String userIdRequest = source.getUserIdRequest() != null ? source
					.getUserIdRequest().getValue() : "";
			String webPageRequest = source.getWebPageRequest() != null ? source
					.getWebPageRequest().getValue() : "";
			List<MMActivityData> activities = activitiesConverter
					.convert(arrayOfActivities);
			List<MMAddressData> addresses = addressesConverter
					.convert(arrayOfAddresses);
			List<MMContractData> contracts = contractsConverter
					.convert(arrayOfContracts);
			List<MMCreditCardData> creditCards = creditCardsConverter
					.convert(arrayOfCreditCards);
			List<MMMessageData> messages = messagesConverter
					.convert(arrayOfMessages);
			List<MMPreferenceData> preferences = preferencesConverter.convert(arrayOfPreferences);
			List<MMProgramData> programs = programsConverter.convert(arrayOfPrograms);
			List<MMTelephoneData> telephones = telephonesConverter.convert(arrayOfTelephones);
			List<MMFlightData> flights = flightConverter.convert(arrayOfFlights);
			List<MMLifestyleData> lifestyles = lifestylesConverter.convert(arrayOfLifestyles);
			Calendar birthDate = XsdConvertUtils.parseCalendar(xmlBirthDate);
			Calendar tierDate = XsdConvertUtils.parseCalendar(xmlTierDate);
			Calendar passportDate = XsdConvertUtils.parseCalendar(xmlPassportDate);

			destination.setActivities(activities);
			destination.setAddresses(addresses);
			destination.setBirthDate(birthDate);
			destination.setContratcType(contractType);
			destination.setCustomerName(name);
			destination.setCustomerNickName(nickname);
			destination.setCustomerSurname(surname);
			destination.setFiscalCode(fiscalCode);
			destination.setLanguage(language);
			if (gender != null) {
				switch (gender) {
				case FEMALE:
					destination.setGender(MMGenderTypeEnum.FEMALE);
					break;
				case MALE:
					destination.setGender(MMGenderTypeEnum.MALE);
					break;
				case UN_KNOWN:
					destination.setGender(MMGenderTypeEnum.UNKNOWN);
					break;
				default:
					destination.setGender(null);
				}
			}
			destination.setCustomerNumber(number);
			destination.setCustomerPinCode(pinCode);
			destination.setEmail(email);
			if (titleType != null) {
				destination.setCustomerTitle(MMTitleTypeEnum
						.fromValue(titleType.value()));
			}
			if (workPositionType != null) {
				destination.setCustomerWorkPosition(MMWorkPositionTypeEnum
						.fromValue(workPositionType.value()));
			}
			destination.setContractAgree(isContractAgree);
			destination.setContracts(contracts);
			destination.setCreditCards(creditCards);
			destination.setDefaultAddressCountry(defaultAddressCountry);
			destination
					.setDefaultAddressMunicipalityName(defaultAddressMunicipalityName);
			destination.setDefaultAddressPostalCode(defaultAddressPostalCode);
			destination.setDefaultAddressPostalCode(defaultAddressPostalCode);
			destination.setDefaultAddressStateCode(defaultAddressStateCode);
			destination
					.setDefaultAddressStreetFreeText(defaultAddressStreetFreeText);
			if (defaultAddressType != null) {
				destination.setDefaultAddressType(MMAddressTypeEnum
						.fromValue(defaultAddressType.value()));
			}
			destination.setMailingType(mailingType);
			if (maritalStatusType != null) {
				destination.setMaritalStatus(MMMaritalStatusTypeEnum
						.fromValue(maritalStatusType.value()));
			}
			destination.setMarket(market);
			destination.setFlight(flights);
			destination.setLifestyles(lifestyles);
			destination.setMessages(messages);
			destination.setPassportDate(passportDate);
			destination.setPointsEarnedPartial(source.getPointsEarnedPartial());
			destination.setPointsEarnedTotal(source.getPointsEarnedTotal());
			destination.setPointsEarnedTotalQualified(source
					.getPointsEarnedTotalQualified());
			destination.setPointsRemainingPartial(source
					.getPointsRemainingPartial());
			destination.setPointsRemainingTotal(source
					.getPointsRemainingTotal());
			destination.setPointsSpentPartial(source.getPointsSpentPartial());
			destination.setPointsSpentTotal(source.getPointsSpentTotal());
			destination.setPreferences(preferences);
			destination.setProfessionalName(professionalName);
			destination.setProfiling(isProfiling);
			destination.setPrograms(programs);
			destination.setQualifyingSegment(source.getQualifyingSegment());
			if (registrationType != null) {
				destination.setRegistrationRequired(MMRegistrationTypeEnum
						.fromValue(registrationType.value()));
			}
			destination.setSmsAuthorization(isSmsAuthorization);
			if (systemChannelType != null) {
				destination.setSystemChannel(MMSystemChannelTypeEnum
						.fromValue(systemChannelType.value()));
			}
			destination.setStatusCode(statusCode);
			destination.setTelephones(telephones);
			destination.setTierDate(tierDate);
			if (tierCode != null) {
				destination.setTierCode(MMTierCodeEnum.fromValue(tierCode
						.value()));
			}
			destination.setUserIdRequest(userIdRequest);
			destination.setWebPageRequest(webPageRequest);

//			//SA
//			String username = source.getUsername() != null ? source
//					.getUsername().getValue() : "";
//			destination.setUserName(username);
//
//			String password = source.getPassword() != null ? source
//					.getPassword().getValue() : "";
//			destination.setPassword(password);
//
//			Integer accessFailureCount = source.getAccessFailureCount();
//			destination.setAccessFailureCount(accessFailureCount);
//
//			String certifiedCountryNumber = source.getCertifiedCountryNumber() != null ? source
//					.getCertifiedCountryNumber().getValue() : "";
//			destination.setCertifiedCountryNumber(certifiedCountryNumber);
//
//			String certifiedEmailAccount = source.getCertifiedEmailAccount() != null ? source
//					.getCertifiedEmailAccount().getValue() : "";
//			destination.setCertifiedEmailAccount(certifiedEmailAccount);
//
//			String certifiedPhoneAccount = source.getCertifiedPhoneAccount() != null ? source
//					.getCertifiedPhoneAccount().getValue() : "";
//			destination.setCertifiedPhoneAccount(certifiedPhoneAccount);
//
//			Boolean emailCertificateFlag = source.isEmailCertificateFlag();
//			destination.setEmailCertificateFlag(emailCertificateFlag);
//
//			Boolean flagLockedAccount = source.isFlagLockedAccount();
//			destination.setFlagLockedAccount(flagLockedAccount);
//
//			Boolean flagOldCustomer = source.isFlagOldCustomer();
//			destination.setFlagOldCustomer(flagOldCustomer);
//
//			Boolean flagRememberPassword = source.isFlagRememberPassword();
//			destination.setFlagRememberPassword(flagRememberPassword);
//
//			Boolean flagSocialCertified = source.isFlagSocialCertified();
//			destination.setFlagSocialCertified(flagSocialCertified);
//
//			Boolean flagVerificaRisposta = source.isFlagVerificaRisposta();
//			destination.setFlagVerificaRisposta(flagVerificaRisposta);
//
//			Boolean flagVerificaUserName = source.isFlagVerificaUserName();
//			destination.setFlagVerificaUserName(flagVerificaUserName);
//
//			Integer idDomandaSegreta = source.getIdDomandaSegreta();
//			destination.setIdDomandaSegreta(idDomandaSegreta);
//
//			Boolean phoneCertificateFlag = source.isPhoneCertificateFlag();
//			destination.setPhoneCertificateFlag(phoneCertificateFlag);
//
//			String rispostaSegreta = source.getRispostaSegreta() != null ? source
//					.getRispostaSegreta().getValue() : "";
//			destination.setRispostaSegreta(rispostaSegreta);
//
//			String socialToken = source.getSocialToken() != null ? source
//					.getSocialToken().getValue() : "";
//			destination.setSocialToken(socialToken);
//
//			String socialUserId = source.getSocialUserId() != null ? source
//					.getSocialUserId().getValue() : "";
//			destination.setSocialUserId(socialUserId);
//
//
//			//SA
		}
		return destination;
	}
}