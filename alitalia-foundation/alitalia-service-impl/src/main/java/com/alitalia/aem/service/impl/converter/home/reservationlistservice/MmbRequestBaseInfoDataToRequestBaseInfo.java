package com.alitalia.aem.service.impl.converter.home.reservationlistservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.MmbRequestBaseInfoData;
import com.alitalia.aem.ws.reservationlist.reservationlistservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.reservationlist.reservationlistservice.xsd1.RequestBaseInfo;


@Component(immediate=true, metatype=false)
@Service(value=MmbRequestBaseInfoDataToRequestBaseInfo.class)
public class MmbRequestBaseInfoDataToRequestBaseInfo implements Converter<MmbRequestBaseInfoData, RequestBaseInfo> {

	@Override
	public RequestBaseInfo convert(MmbRequestBaseInfoData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		RequestBaseInfo destination = null;

		if (source != null) {
			destination = objectFactory.createRequestBaseInfo();

			destination.setX003CClientIPX003EKBackingField(source.getClientIp());
			destination.setX003CSessionIDX003EKBackingField(source.getSessionId());
			destination.setX003CSiteCodeX003EKBackingField(source.getSiteCode());
		}

		return destination;
	}

}
