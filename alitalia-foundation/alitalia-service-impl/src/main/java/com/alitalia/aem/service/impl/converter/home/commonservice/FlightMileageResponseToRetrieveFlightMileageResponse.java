package com.alitalia.aem.service.impl.converter.home.commonservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveFlightMileageResponse;
import com.alitalia.aem.ws.booking.commonservice.xsd1.FlightMileageResponse;

@Component(immediate = true, metatype = false)
@Service(value = FlightMileageResponseToRetrieveFlightMileageResponse.class)
public class FlightMileageResponseToRetrieveFlightMileageResponse 
		implements Converter<FlightMileageResponse, RetrieveFlightMileageResponse> {
	
	@Override
	public RetrieveFlightMileageResponse convert(FlightMileageResponse source) {
		
		RetrieveFlightMileageResponse destination = null;
		
		if (source != null) {
			
			destination = new RetrieveFlightMileageResponse();
			
			destination.setMileage(
					source.getMileage());
			
		}
		
		return destination;
	}
}