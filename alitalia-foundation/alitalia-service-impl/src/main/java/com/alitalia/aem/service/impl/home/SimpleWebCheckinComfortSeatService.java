package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.BaseResponse;
import com.alitalia.aem.common.messages.home.ChangeCabinCheckinRequest;
import com.alitalia.aem.common.messages.home.ChangeCabinCheckinResponse;
import com.alitalia.aem.common.messages.home.ComfortSeatOrderCheckinRequest;
import com.alitalia.aem.common.messages.home.ComfortSeatOrderUpdateCheckinRequest;
import com.alitalia.aem.common.messages.home.ComfortSeatPriceCheckinRequest;
import com.alitalia.aem.common.messages.home.ComfortSeatPriceCheckinResponse;
import com.alitalia.aem.common.messages.home.ComfortSeatPurchaseCheckinRequest;
import com.alitalia.aem.common.messages.home.ComfortSeatPurchaseCheckinResponse;
import com.alitalia.aem.common.messages.home.GenericServiceResponse;
import com.alitalia.aem.common.messages.home.UpgradePolicyCheckinRequest;
import com.alitalia.aem.common.messages.home.UpgradePolicyCheckinResponse;
import com.alitalia.aem.service.api.home.WebCheckinComfortSeatService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.checkincomfortseatservice.ChangeCabinCheckinRequestToChangeCabinRequest;
import com.alitalia.aem.service.impl.converter.home.checkincomfortseatservice.ChangeCabinResponseToChangeCabinCheckinResponse;
import com.alitalia.aem.service.impl.converter.home.checkincomfortseatservice.ComfortSeatOrderCheckinRequestToComfortSeatOrderRequest;
import com.alitalia.aem.service.impl.converter.home.checkincomfortseatservice.ComfortSeatOrderUpdateCheckinRequestToComfortSeatOrderUpdateRequest;
import com.alitalia.aem.service.impl.converter.home.checkincomfortseatservice.ComfortSeatPriceCheckinRequestToComfortSeatPriceRequest;
import com.alitalia.aem.service.impl.converter.home.checkincomfortseatservice.ComfortSeatPriceResponseToComfortSeatPriceCheckinResponse;
import com.alitalia.aem.service.impl.converter.home.checkincomfortseatservice.ComfortSeatPurchaseCheckinRequestToComfortSeatPurchaseRequest;
import com.alitalia.aem.service.impl.converter.home.checkincomfortseatservice.ComfortSeatPurchaseResponseToComfortSeatPurchaseCheckinResponse;
import com.alitalia.aem.service.impl.converter.home.checkincomfortseatservice.ExistsComfortSeatPurchaseResponseToGenericServiceResponse;
import com.alitalia.aem.service.impl.converter.home.checkincomfortseatservice.UpgradePolicyCheckinRequestToUpgradePolicyRequest;
import com.alitalia.aem.service.impl.converter.home.checkincomfortseatservice.UpgradePolicyResponseToUpgradePolicyCheckinResponse;
import com.alitalia.aem.ws.checkin.comfortseatservice.CheckinComfortSeatServiceClient;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.ChangeCabinRequest;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.ChangeCabinResponse;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.ComfortSeatOrderRequest;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.ComfortSeatOrderUpdateRequest;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.ComfortSeatPriceRequest;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.ComfortSeatPriceResponse;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.ComfortSeatPurchaseRequest;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.ComfortSeatPurchaseResponse;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.UpgradePolicyRequest;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.UpgradePolicyResponse;

@Component(immediate=true, metatype=false)
@Service
public class SimpleWebCheckinComfortSeatService implements WebCheckinComfortSeatService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleWebCheckinComfortSeatService.class);

	@Reference
	private CheckinComfortSeatServiceClient checkinComfortSeatServiceClient;

	@Reference
	private ComfortSeatPurchaseCheckinRequestToComfortSeatPurchaseRequest comfortSeatPurchaseCheckinRequestConverter;

	@Reference
	private ComfortSeatPurchaseResponseToComfortSeatPurchaseCheckinResponse comfortSeatPurchaseResponseConverter;
	
	@Reference
	private ComfortSeatPriceCheckinRequestToComfortSeatPriceRequest comfortSeatPriceCheckinRequestConverter;

	@Reference
	private ComfortSeatPriceResponseToComfortSeatPriceCheckinResponse comfortSeatPriceResponseConverter;

	@Reference
	private ComfortSeatOrderCheckinRequestToComfortSeatOrderRequest comfortSeatOrderRequestConverter;

	@Reference
	private ExistsComfortSeatPurchaseResponseToGenericServiceResponse existsComfortSeatPurchaseResponseConverter;
	
	@Reference
	private ComfortSeatOrderUpdateCheckinRequestToComfortSeatOrderUpdateRequest comfortSeatOrderUpdateRequestConverter;
	
	@Reference
	private ChangeCabinCheckinRequestToChangeCabinRequest changeCabinCheckinRequestConverter;
	
	@Reference
	private ChangeCabinResponseToChangeCabinCheckinResponse changeCabinResponseConverter;
	
	@Reference
	private UpgradePolicyCheckinRequestToUpgradePolicyRequest upgradePolicyCheckinRequestConverter;
	
	@Reference
	private UpgradePolicyResponseToUpgradePolicyCheckinResponse upgradePolicyResponseConverter;
	
	
	@Override
	public ComfortSeatPurchaseCheckinResponse selectComfortSeatPurchase(ComfortSeatPurchaseCheckinRequest request) {

		logger.debug("Executing method selectComfortSeatPurchase(). The request is {}", request);	

		try {
			ComfortSeatPurchaseCheckinResponse response = new ComfortSeatPurchaseCheckinResponse();
			ComfortSeatPurchaseRequest serviceRequest = comfortSeatPurchaseCheckinRequestConverter.convert(request);
			ComfortSeatPurchaseResponse serviceResponse = checkinComfortSeatServiceClient.selectComfortSeatPurchase(serviceRequest);
			response = comfortSeatPurchaseResponseConverter.convert(serviceResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			logger.debug("Executed method successfully selectComfortSeatPurchase(). The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method selectComfortSeatPurchase().", e);
			throw new AlitaliaServiceException("Exception executing selectComfortSeatPurchase: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public ComfortSeatPriceCheckinResponse getComfortSeatPrice(ComfortSeatPriceCheckinRequest request) {

		logger.debug("Executing method getComfortSeatPrice(). The request is {}", request);	

		try {
			ComfortSeatPriceCheckinResponse response = new ComfortSeatPriceCheckinResponse();
			ComfortSeatPriceRequest serviceRequest = comfortSeatPriceCheckinRequestConverter.convert(request);
			ComfortSeatPriceResponse serviceResponse = checkinComfortSeatServiceClient.getComfortSeatPrice(serviceRequest);
			response = comfortSeatPriceResponseConverter.convert(serviceResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			logger.debug("Executed method successfully getComfortSeatPrice(). The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getComfortSeatPrice().", e);
			throw new AlitaliaServiceException("Exception executing getComfortSeatPrice: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public GenericServiceResponse existsComfortSeatPurchaseIntoBO(ComfortSeatOrderCheckinRequest request) {

		logger.debug("Executing method existsComfortSeatPurchaseIntoBO(). The request is {}", request);	

		try {
			GenericServiceResponse response = new GenericServiceResponse();
			ComfortSeatOrderRequest serviceRequest = comfortSeatOrderRequestConverter.convert(request);
			Boolean serviceResponse = checkinComfortSeatServiceClient.existsComfortSeatPurchaseIntoBO(serviceRequest);
			response = existsComfortSeatPurchaseResponseConverter.convert(serviceResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			logger.debug("Executed method successfully existsComfortSeatPurchaseIntoBO(). The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method existsComfortSeatPurchaseIntoBO().", e);
			throw new AlitaliaServiceException("Exception executing existsComfortSeatPurchaseIntoBO: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public BaseResponse updateComfortSeatPurchaseIntoBO(ComfortSeatOrderUpdateCheckinRequest request) {

		logger.debug("Executing method updateComfortSeatPurchaseIntoBO(). The request is {}", request);	

		try {
			BaseResponse response = new BaseResponse();
			ComfortSeatOrderUpdateRequest serviceRequest = comfortSeatOrderUpdateRequestConverter.convert(request);
			checkinComfortSeatServiceClient.updateComfortSeatPurchaseIntoBO(serviceRequest);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			logger.debug("Executed method successfully updateComfortSeatPurchaseIntoBO(). The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method updateComfortSeatPurchaseIntoBO().", e);
			throw new AlitaliaServiceException("Exception executing updateComfortSeatPurchaseIntoBO: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public ChangeCabinCheckinResponse changeCabin(ChangeCabinCheckinRequest request) {

		logger.debug("Executing method changeCabin(). The request is {}", request);	

		try {
			ChangeCabinCheckinResponse response = new ChangeCabinCheckinResponse();
			ChangeCabinRequest serviceRequest = changeCabinCheckinRequestConverter.convert(request);
			ChangeCabinResponse serviceResponse = checkinComfortSeatServiceClient.changeCabin(serviceRequest);
			response = changeCabinResponseConverter.convert(serviceResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			logger.debug("Executed method successfully changeCabin(). The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method changeCabin().", e);
			throw new AlitaliaServiceException("Exception executing changeCabin: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public UpgradePolicyCheckinResponse getUpgradePolicy(UpgradePolicyCheckinRequest request) {

		logger.debug("Executing method getUpgradePolicy(). The request is {}", request);	

		try {
			UpgradePolicyCheckinResponse response = new UpgradePolicyCheckinResponse();
			UpgradePolicyRequest serviceRequest = upgradePolicyCheckinRequestConverter.convert(request);
			UpgradePolicyResponse serviceResponse = checkinComfortSeatServiceClient.getUpgradePolicy(serviceRequest);
			response = upgradePolicyResponseConverter.convert(serviceResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			logger.debug("Executed method successfully getUpgradePolicy(). The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getUpgradePolicy().", e);
			throw new AlitaliaServiceException("Exception executing getUpgradePolicy: Sid ["+request.getSid()+"]" , e);
		}
	}

}
