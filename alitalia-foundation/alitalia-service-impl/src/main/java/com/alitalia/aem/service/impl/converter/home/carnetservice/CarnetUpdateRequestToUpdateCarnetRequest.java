/**
 * 
 */
package com.alitalia.aem.service.impl.converter.home.carnetservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.CarnetUpdateRequest;
import com.alitalia.aem.ws.carnet.carnetservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.carnet.carnetservice.xsd1.UpdateCarnetRequest;



@Component(immediate=true, metatype=false)
@Service(value=CarnetUpdateRequestToUpdateCarnetRequest.class)
public class CarnetUpdateRequestToUpdateCarnetRequest implements Converter<CarnetUpdateRequest, UpdateCarnetRequest> {

	@Reference
	private CarnetInfoCarnetToInfoCarnet infoCarnetConverter;
	
	@Override
	public UpdateCarnetRequest convert(CarnetUpdateRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		UpdateCarnetRequest destination = null;

		if (source != null) {
			
			destination = objectFactory.createUpdateCarnetRequest();
			destination.setAmount(source.getAmount());
			destination.setInfoCarnet(objectFactory.createUpdateCarnetRequestInfoCarnet(infoCarnetConverter.convert(source.getInfoCarnet())));
			destination.setRoutesNumber(source.getRoutesNumber());

		}
		return destination;
	}
}
