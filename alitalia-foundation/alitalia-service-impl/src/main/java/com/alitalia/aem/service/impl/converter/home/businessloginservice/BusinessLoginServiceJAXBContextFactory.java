package com.alitalia.aem.service.impl.converter.home.businessloginservice;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.b2bv2.xsd.Table;
import com.alitalia.aem.ws.b2bv2.xsd1.AlitaliaTrade;

@Component(immediate=true, metatype=false)
@Service(value=BusinessLoginServiceJAXBContextFactory.class)
public class BusinessLoginServiceJAXBContextFactory {

	private static final Logger logger = LoggerFactory.getLogger(BusinessLoginServiceJAXBContextFactory.class);

	private JAXBContext jaxbContext = null;

	public synchronized JAXBContext getJaxbContext() throws JAXBException {

		if (jaxbContext == null) {
			jaxbContext = JAXBContext.newInstance(Table.class,
													AlitaliaTrade.class
								);
		}

		return jaxbContext;
	}

	@Activate
	private void activate() {
		try {
			jaxbContext = JAXBContext.newInstance(Table.class,
													AlitaliaTrade.class
					);
		} catch (JAXBException e) {
			logger.warn("BusinessLoginServiceJAXBContextFactory - cannot instantiate JAXBContext: {}", e.getMessage());
		}
	}
}
