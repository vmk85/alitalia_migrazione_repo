package com.alitalia.aem.service.impl.converter.home.checkinservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.MmbBaggageOrderData;
import com.alitalia.aem.ws.checkin.checkinservice.xsd2.BaggageOrder;


@Component(immediate=true, metatype=false)
@Service(value=BaggageOrderToMmbBaggageOrderData.class)
public class BaggageOrderToMmbBaggageOrderData implements Converter<BaggageOrder, MmbBaggageOrderData> {

	@Override
	public MmbBaggageOrderData convert(BaggageOrder source) {
		MmbBaggageOrderData destination = null;

		if (source != null) {
			destination = new MmbBaggageOrderData();

			destination.setCurrency(source.getX003CCurrencyX003EKBackingField());
			destination.setId(source.getX003CIdX003EKBackingField());
			destination.setQuantity(source.getX003CQuantityX003EKBackingField());
			destination.setSavings(source.getX003CSavingsX003EKBackingField());
			destination.setTotalAirportFare(source.getX003CTotalAirportFareX003EKBackingField());
			destination.setTotalFare(source.getX003CTotalFareX003EKBackingField());
		}

		return destination;
	}

}
