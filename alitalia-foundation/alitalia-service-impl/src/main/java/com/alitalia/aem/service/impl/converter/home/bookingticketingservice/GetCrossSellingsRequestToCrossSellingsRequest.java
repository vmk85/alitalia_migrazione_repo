package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.CrossSellingsRequest;
import com.alitalia.aem.ws.booking.ticketservice.xsd1.GetCrossSellingsRequest;

@Component(immediate = true, metatype = false)
@Service(value = GetCrossSellingsRequestToCrossSellingsRequest.class)
public class GetCrossSellingsRequestToCrossSellingsRequest implements
		Converter<GetCrossSellingsRequest, CrossSellingsRequest> {

	@Reference
	private RoutesToRoutesData routesToRoutesDataConverter;

	@Override
	public CrossSellingsRequest convert(GetCrossSellingsRequest source) {
		CrossSellingsRequest destination = null;

		if (source != null) {
			destination = new CrossSellingsRequest();
			destination.setIpAddress(source.getIpAddress().getValue());
			destination.setLanguageCode(source.getLanguageCode().getValue());
			destination.setMarketCode(source.getMarketCode().getValue());
			destination.setPrenotation(routesToRoutesDataConverter.convert(source.getPrenotation().getValue()));
			destination.setUserAgent(source.getUserAgent().getValue());
		}

		return destination;
	}

}
