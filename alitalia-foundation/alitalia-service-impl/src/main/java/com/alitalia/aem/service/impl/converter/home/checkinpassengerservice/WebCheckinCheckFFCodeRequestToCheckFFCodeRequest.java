package com.alitalia.aem.service.impl.converter.home.checkinpassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesRequest;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.CheckFFCodeRequest;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.Resource;
import com.alitalia.aem.ws.checkin.passengerservice.xsd4.ArrayOfKeyValueOfintArrayOfstringty7Ep6D1;
import com.alitalia.aem.ws.checkin.passengerservice.xsd4.ArrayOfKeyValueOfintArrayOfstringty7Ep6D1.KeyValueOfintArrayOfstringty7Ep6D1;
import com.alitalia.aem.ws.checkin.passengerservice.xsd4.ArrayOfstring;


@Component(immediate=true, metatype=false)
@Service(value=WebCheckinCheckFFCodeRequestToCheckFFCodeRequest.class)
public class WebCheckinCheckFFCodeRequestToCheckFFCodeRequest implements
		Converter<MmbCheckFrequentFlyerCodesRequest, CheckFFCodeRequest> {
	
	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;

	@Override
	public CheckFFCodeRequest convert(MmbCheckFrequentFlyerCodesRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		com.alitalia.aem.ws.checkin.passengerservice.xsd4.ObjectFactory objectFactory2 = 
				new com.alitalia.aem.ws.checkin.passengerservice.xsd4.ObjectFactory();
		CheckFFCodeRequest destination = null;

		if (source != null) {
			destination = objectFactory.createCheckFFCodeRequest();
			destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			destination.setX003CResourceX003EKBackingField(Resource.WEB);

			if (source.getFfCodesToCheck() != null &&
					!source.getFfCodesToCheck().isEmpty()) {
				ArrayOfKeyValueOfintArrayOfstringty7Ep6D1 frequentFlyersCodes = 
						objectFactory2.createArrayOfKeyValueOfintArrayOfstringty7Ep6D1();
				for(Integer sourceKey : source.getFfCodesToCheck().keySet()) {
					KeyValueOfintArrayOfstringty7Ep6D1 destinationElement = 
							objectFactory2.createArrayOfKeyValueOfintArrayOfstringty7Ep6D1KeyValueOfintArrayOfstringty7Ep6D1();
					destinationElement.setKey(sourceKey);
					ArrayOfstring destinationElementValue = objectFactory2.createArrayOfstring();
					destinationElementValue.getString().add(0, source.getFfCodesToCheck().get(sourceKey).get(0));
					destinationElementValue.getString().add(1, source.getFfCodesToCheck().get(sourceKey).get(1));
					destinationElementValue.getString().add(2, source.getFfCodesToCheck().get(sourceKey).get(2));
					destinationElement.setValue(destinationElementValue);
					frequentFlyersCodes.getKeyValueOfintArrayOfstringty7Ep6D1().add(destinationElement);
				}
				destination.setFrequentFlyersCodes(
						objectFactory.createCheckFFCodeRequestFrequentFlyersCodes(frequentFlyersCodes));
			}
		}
		return destination;
	}

}
