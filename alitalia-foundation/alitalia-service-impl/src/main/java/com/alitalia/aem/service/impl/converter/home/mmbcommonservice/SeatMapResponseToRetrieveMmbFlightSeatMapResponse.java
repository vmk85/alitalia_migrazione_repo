package com.alitalia.aem.service.impl.converter.home.mmbcommonservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveMmbFlightSeatMapResponse;
import com.alitalia.aem.ws.mmb.commonservice.xsd1.SeatMapResponse;
import com.alitalia.aem.ws.mmb.commonservice.xsd6.FlightSeatMap;

@Component(immediate=true, metatype=false)
@Service(value=SeatMapResponseToRetrieveMmbFlightSeatMapResponse.class)
public class SeatMapResponseToRetrieveMmbFlightSeatMapResponse implements
		Converter<SeatMapResponse, RetrieveMmbFlightSeatMapResponse> {

	@Reference
	private FlightSeatMapToMmbFlightSeatMapData flightSeatMapConverter;

	@Override
	public RetrieveMmbFlightSeatMapResponse convert(SeatMapResponse source) {
		RetrieveMmbFlightSeatMapResponse destination = null;

		if (source != null) {
			destination = new RetrieveMmbFlightSeatMapResponse();

			JAXBElement<FlightSeatMap> sourceFlightSeatMapMatrix = source.getFlightSeatMapMatrix();
			if (sourceFlightSeatMapMatrix != null)
				destination.setSeatMapMatrix(
						flightSeatMapConverter.convert(sourceFlightSeatMapMatrix.getValue()));
		}

		return destination;
	}

}
