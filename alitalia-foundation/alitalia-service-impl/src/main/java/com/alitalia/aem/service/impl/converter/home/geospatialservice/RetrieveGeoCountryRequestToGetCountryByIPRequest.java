package com.alitalia.aem.service.impl.converter.home.geospatialservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.IPAddressData;
import com.alitalia.aem.common.messages.home.RetrieveGeoCountryRequest;
import com.alitalia.aem.ws.geospatial.service.xsd1.GetCountryByIPRequest;
import com.alitalia.aem.ws.geospatial.service.xsd1.ObjectFactory;
import com.alitalia.aem.ws.geospatial.service.xsd2.IPAddress;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveGeoCountryRequestToGetCountryByIPRequest.class)
public class RetrieveGeoCountryRequestToGetCountryByIPRequest implements Converter<RetrieveGeoCountryRequest, GetCountryByIPRequest> {

	@Reference
	private IPAddressDataToIPAddress ipAddressDataConverter;
	
	@Override
	public GetCountryByIPRequest convert(RetrieveGeoCountryRequest source) {
		
		GetCountryByIPRequest request = new GetCountryByIPRequest();
		
		IPAddressData ipAddressData = source.getIpAddress();
		IPAddress ipAddress = ipAddressDataConverter.convert(ipAddressData);
		
		ObjectFactory objectFactory = new ObjectFactory();
		request.setIPAddress(objectFactory.createGetNearAirportsRequestIPAddress(ipAddress));
		
		return request;
	}
}