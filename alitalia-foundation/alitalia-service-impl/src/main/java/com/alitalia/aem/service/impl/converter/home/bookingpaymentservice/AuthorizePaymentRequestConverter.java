package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;


import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.ws.booking.paymentservice.xsd1.AuthorizePaymentRequest;
import com.alitalia.aem.ws.booking.paymentservice.xsd1.ObjectFactory;

@Component(metatype=false, immediate=true)
@Service(value=AuthorizePaymentRequestConverter.class)
public class AuthorizePaymentRequestConverter implements Converter<com.alitalia.aem.common.messages.home.AuthorizePaymentRequest, AuthorizePaymentRequest> {

	@Reference
	RoutesDataToRoutes routesConverter;
	
	@Override
	public AuthorizePaymentRequest convert(com.alitalia.aem.common.messages.home.AuthorizePaymentRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		AuthorizePaymentRequest destination = objectFactory.createAuthorizePaymentRequest();
		destination.setPrenotation(objectFactory.createAuthorizePaymentRequestPrenotation(routesConverter.convert(source.getPrenotation())));
		destination.setFlgUsaCa(source.getFlgUsaCa());
		destination.setCookie(objectFactory.createAuthorizePaymentRequestCookie(source.getCookie()));
		destination.setExecution(objectFactory.createAuthorizePaymentRequestExecution(source.getExecution()));
		destination.setSabreGateWayAuthToken(objectFactory.createAuthorizePaymentRequestSabreGateWayAuthToken(source.getSabreGateWayAuthToken()));
		return destination;
	}
	
}
