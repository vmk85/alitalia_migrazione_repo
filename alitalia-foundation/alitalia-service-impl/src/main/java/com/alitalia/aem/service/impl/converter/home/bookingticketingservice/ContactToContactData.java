package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ContactData;
import com.alitalia.aem.common.data.home.enumerations.ContactTypeEnum;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.Contact;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.PhonePrefix;

@Component(immediate = true, metatype = false)
@Service(value = ContactToContactData.class)
public class ContactToContactData implements Converter<Contact, ContactData> {

	@Reference
	private PhonePrefixToPhonePrefixData phonePrefixToPhonePrefixDataConverter;

	@Override
	public ContactData convert(Contact source) {
		ContactData destination = null;

		if (source != null) {
			destination = new ContactData();

			if (source.getContactType() != null) {
				destination.setContactType(ContactTypeEnum.fromValue(source.getContactType().value()));
			}

			if (source.getPhoneNumber() != null) {
				destination.setPhoneNumber(source.getPhoneNumber().getValue());
			}

			if (source.getPrefix() != null) {
				destination.setPrefix(phonePrefixToPhonePrefixDataConverter.convert((PhonePrefix)source.getPrefix().getValue()));
			}

		}

		return destination;
	}

}
