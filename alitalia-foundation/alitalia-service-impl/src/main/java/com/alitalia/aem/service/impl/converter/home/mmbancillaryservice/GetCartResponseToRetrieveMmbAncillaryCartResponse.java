package com.alitalia.aem.service.impl.converter.home.mmbancillaryservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.MmbAncillaryErrorData;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCartResponse;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.GetCartResponse;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.Cart;

@Component(immediate=true, metatype=false)
@Service(value=GetCartResponseToRetrieveMmbAncillaryCartResponse.class)
public class GetCartResponseToRetrieveMmbAncillaryCartResponse implements
		Converter<GetCartResponse, RetrieveMmbAncillaryCartResponse> {

	@Reference
	private CartToMmbAncillaryCartData cartConverter;

	@Reference
	private ErrorToMmbAncillaryErrorData errorConverter;

	@Override
	public RetrieveMmbAncillaryCartResponse convert(GetCartResponse source) {
		RetrieveMmbAncillaryCartResponse destination = null;

		if (source != null) {
			destination = new RetrieveMmbAncillaryCartResponse();

			JAXBElement<String> sourceMachineName = source.getMachineName();
			if (sourceMachineName != null)
				destination.setMachine(sourceMachineName.getValue());

			JAXBElement<Cart> sourceCart = source.getCart();
			if (sourceCart != null)
				destination.setCart(cartConverter.convert(sourceCart.getValue()));

			if (source.getErrors() != null &&
					source.getErrors().getValue() != null &&
					source.getErrors().getValue().getAnyType() != null &&
					!source.getErrors().getValue().getAnyType().isEmpty()) {
				List<Object> sourceErrors = source.getErrors().getValue().getAnyType();
				List<MmbAncillaryErrorData> errors = new ArrayList<MmbAncillaryErrorData>();
				for (Object sourceError : sourceErrors) {
					errors.add(errorConverter.convert(sourceError));
				}
				destination.setErrors(errors);
			}
		}

		return destination;
	}

}
