package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.enumerations.TipoProfiloEnum;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.GetDatiSicurezzaProfiloOLDResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.TipoProfilo;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

@Component(immediate = true, metatype = false)
@Service(value = GetDatiSicurezzaProfiloOLDResponseConverter.class)
public class GetDatiSicurezzaProfiloOLDResponseConverter implements Converter<GetDatiSicurezzaProfiloOLDResponse,com.alitalia.aem.common.messages.home.GetDatiSicurezzaProfiloOLDResponse> {
    @Override
    public com.alitalia.aem.common.messages.home.GetDatiSicurezzaProfiloOLDResponse convert(GetDatiSicurezzaProfiloOLDResponse source) {
        if(source == null) throw new IllegalArgumentException("Request is null.");
        ObjectFactory objectFactory=new ObjectFactory();

        com.alitalia.aem.common.messages.home.GetDatiSicurezzaProfiloOLDResponse destination = new com.alitalia.aem.common.messages.home.GetDatiSicurezzaProfiloOLDResponse();
        destination.setAlias(source.getAlias().getValue());
        destination.setCodiceMM(source.getCodiceMM().getValue());
        destination.setFlagIDProfiloExists(source.isFlagIDProfiloExists());
        destination.setPinMM(source.getPinMM().getValue());
        destination.setTipoProfilo(TipoProfiloEnum.fromValue(source.getTipoProfilo().value()));
        return destination;
    }
}
