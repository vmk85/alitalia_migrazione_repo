package com.alitalia.aem.service.impl.converter.home.checkinservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.UndoCheckinPassengerData;
import com.alitalia.aem.common.messages.home.WebCheckinUndoManyCheckinResponse;
import com.alitalia.aem.ws.checkin.checkinservice.xsd1.ArrayOfUndoRequestPassenger;
import com.alitalia.aem.ws.checkin.checkinservice.xsd1.UndoManyCheckInResponse;
import com.alitalia.aem.ws.checkin.checkinservice.xsd1.UndoRequestPassenger;




@Component(immediate=true, metatype=false)
@Service(value=UndoManyCheckInResponseToWebCheckinUndoManyCheckinResponse.class)
public class UndoManyCheckInResponseToWebCheckinUndoManyCheckinResponse implements
		Converter<UndoManyCheckInResponse, WebCheckinUndoManyCheckinResponse> {
	
	@Reference
	private UndoRequestPassengerToUndoCheckinPassengerData undoCheckinPassengerConverter;

	@Override
	public WebCheckinUndoManyCheckinResponse convert(UndoManyCheckInResponse source) {
		WebCheckinUndoManyCheckinResponse destination = null;

		if (source != null) {
			destination = new WebCheckinUndoManyCheckinResponse();			
			
			JAXBElement<ArrayOfUndoRequestPassenger> sourcePassengers = source.getPassengers();
			if (sourcePassengers != null && 
					sourcePassengers.getValue() != null &&
					sourcePassengers.getValue().getUndoRequestPassenger() != null &&
					!sourcePassengers.getValue().getUndoRequestPassenger().isEmpty()) {
				List<UndoCheckinPassengerData> passengersList = new ArrayList<UndoCheckinPassengerData>();
				for(UndoRequestPassenger sourcePassenger : sourcePassengers.getValue().getUndoRequestPassenger()) {
					passengersList.add(undoCheckinPassengerConverter.convert(sourcePassenger));
				}
				destination.setPassengers(passengersList);
			}
		}
		return destination;
	}

}
