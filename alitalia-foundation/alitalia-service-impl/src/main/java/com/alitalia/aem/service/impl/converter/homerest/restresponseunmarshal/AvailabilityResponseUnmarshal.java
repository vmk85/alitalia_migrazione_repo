package com.alitalia.aem.service.impl.converter.homerest.restresponseunmarshal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.RetrieveAvailabilityResponse;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class AvailabilityResponseUnmarshal {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public RetrieveAvailabilityResponse availabilityUnmarshal (String jsonString) {
		
		RetrieveAvailabilityResponse response = new RetrieveAvailabilityResponse();
		Gson gson = new Gson();
		
		try {
			response  = gson.fromJson(jsonString, RetrieveAvailabilityResponse.class);
		}
		catch (JsonSyntaxException e) {
			e.printStackTrace();
			logger.debug("JsonSyntaxException in AvailabilityResponseUnmarshal");
		}
		
		return response;
	}

}
