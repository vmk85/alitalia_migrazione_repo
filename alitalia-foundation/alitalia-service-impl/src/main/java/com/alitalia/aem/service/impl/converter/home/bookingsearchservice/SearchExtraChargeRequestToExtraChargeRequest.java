package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.BrandSearchData;
import com.alitalia.aem.common.data.home.CarnetSearchData;
import com.alitalia.aem.common.data.home.MultiSliceSearchData;
import com.alitalia.aem.common.messages.home.SearchExtraChargeRequest;
import com.alitalia.aem.ws.booking.searchservice.xsd1.ExtraChargeRequest;
import com.alitalia.aem.ws.booking.searchservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.booking.searchservice.xsd2.BrandSearch;
import com.alitalia.aem.ws.booking.searchservice.xsd2.CarnetSearch;
import com.alitalia.aem.ws.booking.searchservice.xsd2.MultiSliceSearch;

@Component(immediate=true, metatype=false)
@Service(value=SearchExtraChargeRequestToExtraChargeRequest.class)
public class SearchExtraChargeRequestToExtraChargeRequest implements Converter<SearchExtraChargeRequest, ExtraChargeRequest> {

	@Reference
	private BrandSearchDataToBrandSearch brandSearchDataConverter;
	
	@Reference
	private CarnetSearchDataToCarnetSearch carnetSearchDataConverter;
	
	@Reference
	private MultiSliceSearchDataToMultiSliceSearch multiSliceSearchConverter;

	@Override
	public ExtraChargeRequest convert(SearchExtraChargeRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		ExtraChargeRequest destination = objectFactory.createExtraChargeRequest();
		Object searchData = source.getFilter();
		if (searchData instanceof BrandSearchData) {
			BrandSearchData brandSearchData = (BrandSearchData) searchData;
			BrandSearch filter = brandSearchDataConverter.convert(brandSearchData);
			destination.setFilter(objectFactory.createExtraChargeRequestFilter(filter));
		} else if (searchData instanceof MultiSliceSearchData) {
			MultiSliceSearchData multiSliceSearchData = (MultiSliceSearchData) searchData;
			MultiSliceSearch filter = multiSliceSearchConverter.convert(multiSliceSearchData);
			destination.setFilter(objectFactory.createExtraChargeRequestFilter(filter));
		} else if (searchData instanceof CarnetSearchData){
			CarnetSearchData carnetSearchData = (CarnetSearchData) searchData;
			CarnetSearch filter = carnetSearchDataConverter.convert(carnetSearchData);
			destination.setFilter(objectFactory.createExtraChargeRequestFilter(filter));
		}
		
		return destination;
	}

}
