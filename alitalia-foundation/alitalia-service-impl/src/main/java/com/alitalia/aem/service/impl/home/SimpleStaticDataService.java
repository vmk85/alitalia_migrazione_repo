package com.alitalia.aem.service.impl.home;

import com.alitalia.aem.ws.booking.ancillaryservice.xsd1.GetInsuranceResponse;
import com.alitalia.aem.ws.booking.staticdataservice.SabreDcStaticDataServiceClient;
import com.day.cq.wcm.api.Page;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.common.messages.home.RetrieveCouncilsRequest;
import com.alitalia.aem.common.messages.home.RetrieveCouncilsResponse;
import com.alitalia.aem.common.messages.home.RetrieveCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCountriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveCreditCardCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCreditCardCountriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveFrequentFlayerRequest;
import com.alitalia.aem.common.messages.home.RetrieveFrequentFlyersResponse;
import com.alitalia.aem.common.messages.home.RetrieveMealsRequest;
import com.alitalia.aem.common.messages.home.RetrieveMealsResponse;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemRequest;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemResponse;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixRequest;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixResponse;
import com.alitalia.aem.common.messages.home.RetrieveProvincesRequest;
import com.alitalia.aem.common.messages.home.RetrieveProvincesResponse;
import com.alitalia.aem.common.messages.home.RetrieveSeatTypeRequest;
import com.alitalia.aem.common.messages.home.RetrieveSeatTypeResponse;
import com.alitalia.aem.common.messages.home.RetrieveTicketOfficeRequest;
import com.alitalia.aem.common.messages.home.RetrieveTicketOfficeResponse;
import com.alitalia.aem.common.messages.home.SearchFlightsRequest;
import com.alitalia.aem.common.messages.home.SearchFlightsResponse;
import com.alitalia.aem.service.api.home.StaticDataService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.staticdataservice.RetrieveAirportRequestToStaticDataRequestConverter;
import com.alitalia.aem.service.impl.converter.home.staticdataservice.RetrieveCouncilsRequestToStaticDataRequestConverter;
import com.alitalia.aem.service.impl.converter.home.staticdataservice.RetrieveCountriesRequestToStaticDataRequestConverter;
import com.alitalia.aem.service.impl.converter.home.staticdataservice.RetrieveCreditCardCountriesRequestToStaticDataRequestConverter;
import com.alitalia.aem.service.impl.converter.home.staticdataservice.RetrieveFrequentFlyersRequestToStaticDataRequestConverter;
import com.alitalia.aem.service.impl.converter.home.staticdataservice.RetrieveFrequentFlyersResponseToStaticDataResponseConverter;
import com.alitalia.aem.service.impl.converter.home.staticdataservice.RetrieveMealsRequestToStaticDataRequestConverter;
import com.alitalia.aem.service.impl.converter.home.staticdataservice.RetrievePaymentTypeItemsRequestToStaticDataRequestConverter;
import com.alitalia.aem.service.impl.converter.home.staticdataservice.RetrievePhonePrefixToStaticDataRequestConverter;
import com.alitalia.aem.service.impl.converter.home.staticdataservice.RetrieveProvincesRequestToStaticDataRequestConverter;
import com.alitalia.aem.service.impl.converter.home.staticdataservice.RetrieveSeatTypeToStaticDataRequestConverter;
import com.alitalia.aem.service.impl.converter.home.staticdataservice.RetrieveTicketOfficeToStaticDataRequestConverter;
import com.alitalia.aem.service.impl.converter.home.staticdataservice.StaticDataResponseToRetrieveAirportResponseConverter;
import com.alitalia.aem.service.impl.converter.home.staticdataservice.StaticDataResponseToRetrieveCouncilsResponseConverter;
import com.alitalia.aem.service.impl.converter.home.staticdataservice.StaticDataResponseToRetrieveCountryResponseConverter;
import com.alitalia.aem.service.impl.converter.home.staticdataservice.StaticDataResponseToRetrieveCreditCardCountriesResponseConverter;
import com.alitalia.aem.service.impl.converter.home.staticdataservice.StaticDataResponseToRetrieveMealsResponseConverter;
import com.alitalia.aem.service.impl.converter.home.staticdataservice.StaticDataResponseToRetrievePaymentTypesItemResponseConverter;
import com.alitalia.aem.service.impl.converter.home.staticdataservice.StaticDataResponseToRetrievePhonePrefixResponseConverter;
import com.alitalia.aem.service.impl.converter.home.staticdataservice.StaticDataResponseToRetrieveProvincesResponseConverter;
import com.alitalia.aem.service.impl.converter.home.staticdataservice.StaticDataResponseToRetrieveSeatTypeResponseConverter;
import com.alitalia.aem.service.impl.converter.home.staticdataservice.StaticDataResponseToRetrieveTicketOfficeResponseConverter;
import com.alitalia.aem.ws.booking.staticdataservice.StaticDataServiceClient;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.StaticDataRequest;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.StaticDataResponse;

@Service
@Component(immediate=true, metatype=false)
public class SimpleStaticDataService implements StaticDataService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleStaticDataService.class);
	
	@Reference
	private StaticDataServiceClient staticDataServiceClient;
	
	@Reference
	private RetrieveAirportRequestToStaticDataRequestConverter retrieveAirportRequestConverter;
	
	@Reference
	private StaticDataResponseToRetrieveAirportResponseConverter retrieveAirportResponseConverter;
	
	@Reference
	private RetrieveCountriesRequestToStaticDataRequestConverter retrieveCountriesRequestRequestConverter;
	
	@Reference
	private StaticDataResponseToRetrieveCountryResponseConverter retrieveCountryResponseConverter;
	
	@Reference
	private RetrieveCreditCardCountriesRequestToStaticDataRequestConverter retrieveCreditCardCountriesRequestToStaticDataRequestConverter;
	
	@Reference
	private StaticDataResponseToRetrieveCreditCardCountriesResponseConverter retrieveCreditCardCountriesResponseToStaticDataResponseConverter;
	
	@Reference
	private RetrieveFrequentFlyersRequestToStaticDataRequestConverter retrieveFrequentFlyersRequestToStaticDataRequestConverter;
	
	@Reference
	private RetrieveFrequentFlyersResponseToStaticDataResponseConverter retrieveFrequentFlyersResponseToStaticDataResponseConverter;
	
	@Reference
	private RetrieveMealsRequestToStaticDataRequestConverter retrieveMealsRequestToStaticDataRequestConverter;
	
	@Reference
	private StaticDataResponseToRetrieveMealsResponseConverter staticDataResponseToRetrieveMealsResponseConverter;
	
	@Reference
	private RetrievePaymentTypeItemsRequestToStaticDataRequestConverter retrievePaymentTypeItemsRequestToStaticDataRequestConverter;
	
	@Reference
	private StaticDataResponseToRetrievePaymentTypesItemResponseConverter staticDataResponseToRetrievePaymentTypesItemResponseConverter;
	
	@Reference
	private StaticDataResponseToRetrievePhonePrefixResponseConverter staticDataResponseToRetrievePhonePrefixResponseConverter;
	
	@Reference
	private RetrievePhonePrefixToStaticDataRequestConverter retrievePhonePrefixToStaticDataRequestConverter;
	
	@Reference
	private RetrieveSeatTypeToStaticDataRequestConverter retrieveSeatTypeToStaticDataRequestConverter;
	
	@Reference
	private StaticDataResponseToRetrieveSeatTypeResponseConverter staticDataResponseToRetrieveSeatTypeResponseConverter;
	
	@Reference
	private RetrieveTicketOfficeToStaticDataRequestConverter retrieveTicketOfficeToStaticDataRequestConverter;
	
	@Reference
	private StaticDataResponseToRetrieveTicketOfficeResponseConverter staticDataResponseToRetrieveTicketOfficeResponseConverter;
	
	@Reference
	private RetrieveCouncilsRequestToStaticDataRequestConverter retrieveCouncilsRequestConverter;
	
	@Reference
	private StaticDataResponseToRetrieveCouncilsResponseConverter staticDataResponseToRetrieveCouncilsResponseConverter;
	
	@Reference
	private RetrieveProvincesRequestToStaticDataRequestConverter retrieveProvincesRequestRequestConverter;
	
	@Reference
	private StaticDataResponseToRetrieveProvincesResponseConverter staticDataResponseToRetrieveProvincesResponseConverter;

	/** Migrazione 3.6 inizio*/

	@Reference
	private ResourceResolverFactory resolverFactory;

	@Reference
	private SabreDcStaticDataServiceClient sabreDcStaticDataServiceClient;

	/** Migrazione 3.6 fine*/
	
	@Override
	public RetrieveAirportsResponse retrieveAirports(RetrieveAirportsRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveAirports. The request is {}", request);
		}
		
		
		try {
			
			RetrieveAirportsResponse response = new RetrieveAirportsResponse();
			StaticDataRequest staticDataRequest = retrieveAirportRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			StaticDataResponse staticDataResponse = null;
			if (newSabreEnable(staticDataRequest.getMarket().getValue().toLowerCase())){
				staticDataResponse = sabreDcStaticDataServiceClient.getData(staticDataRequest);
			} else {
				staticDataResponse = staticDataServiceClient.getData(staticDataRequest);
			}

			/** Migrazione 3.6 fine*/

//			StaticDataResponse staticDataResponse = staticDataServiceClient.getData(staticDataRequest);
			response = retrieveAirportResponseConverter.convert(staticDataResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
			
			return response;
			
		} catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing retrieveAirports: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public SearchFlightsResponse searchFlights(SearchFlightsRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method searchFlights. The request is {}", request);
		}
		
		
		try {
			
			SearchFlightsResponse response = new SearchFlightsResponse();
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
			
			return response;
			
		} catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing searchFlights: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public RetrieveCountriesResponse retrieveCountries(RetrieveCountriesRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveCountries. The request is {}", request);
		}
		
		try {
			
			RetrieveCountriesResponse response = new RetrieveCountriesResponse();
			StaticDataRequest staticDataRequest = retrieveCountriesRequestRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			StaticDataResponse staticDataResponse = null;
			if (newSabreEnable(staticDataRequest.getMarket().getValue().toLowerCase())){
				staticDataResponse = sabreDcStaticDataServiceClient.getData(staticDataRequest);
			} else {
				staticDataResponse = staticDataServiceClient.getData(staticDataRequest);
			}

			/** Migrazione 3.6 fine*/

//			StaticDataResponse staticDataResponse = staticDataServiceClient.getData(staticDataRequest);
			response = retrieveCountryResponseConverter.convert(staticDataResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
			
			return response;	
			
		} catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing retrieveCountries: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public RetrieveCreditCardCountriesResponse retrieveCreditCardCountries(RetrieveCreditCardCountriesRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveCreditCardCountries. The request is {}", request);
		}
		
		try{
			RetrieveCreditCardCountriesResponse response = new RetrieveCreditCardCountriesResponse();
			StaticDataRequest staticDataRequest = retrieveCreditCardCountriesRequestToStaticDataRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			StaticDataResponse staticDataResponse = null;
			if (newSabreEnable(staticDataRequest.getMarket().getValue().toLowerCase())){
				staticDataResponse = sabreDcStaticDataServiceClient.getData(staticDataRequest);
			} else {
				staticDataResponse = staticDataServiceClient.getData(staticDataRequest);
			}

			/** Migrazione 3.6 fine*/

//			StaticDataResponse staticDataResponse = staticDataServiceClient.getData(staticDataRequest);
			response = retrieveCreditCardCountriesResponseToStaticDataResponseConverter.convert(staticDataResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
		
			return response;
		}catch(Exception e){
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing retrieveCreditCardCountries: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public RetrieveFrequentFlyersResponse retrieveFrequentFlyers(RetrieveFrequentFlayerRequest request){
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveFrequentFlyers. The request is {}", request);
		}
		
		try{
			RetrieveFrequentFlyersResponse response = new RetrieveFrequentFlyersResponse();
			StaticDataRequest staticDataRequest = retrieveFrequentFlyersRequestToStaticDataRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			StaticDataResponse staticDataResponse = null;
			if (newSabreEnable(staticDataRequest.getMarket().getValue().toLowerCase())){
				staticDataResponse = sabreDcStaticDataServiceClient.getData(staticDataRequest);
			} else {
				staticDataResponse = staticDataServiceClient.getData(staticDataRequest);
			}

			/** Migrazione 3.6 fine*/

//			StaticDataResponse staticDataResponse = staticDataServiceClient.getData(staticDataRequest);
			response = retrieveFrequentFlyersResponseToStaticDataResponseConverter.convert(staticDataResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
			
			return response;
		}catch(Exception e){
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing retrieveFrequentFlyers: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public RetrieveMealsResponse retrieveMeals(RetrieveMealsRequest request){
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveMeals. The request is {}", request);
		}
		
		try{
			RetrieveMealsResponse response = new RetrieveMealsResponse();
			StaticDataRequest staticDataRequest = retrieveMealsRequestToStaticDataRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			StaticDataResponse staticDataResponse = null;
			if (newSabreEnable(staticDataRequest.getMarket().getValue().toLowerCase())){
				staticDataResponse = sabreDcStaticDataServiceClient.getData(staticDataRequest);
			} else {
				staticDataResponse = staticDataServiceClient.getData(staticDataRequest);
			}

			/** Migrazione 3.6 fine*/

//			StaticDataResponse staticDataResponse = staticDataServiceClient.getData(staticDataRequest);
			response = staticDataResponseToRetrieveMealsResponseConverter.convert(staticDataResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
			
			return response;
		}catch(Exception e){
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing retrieveMeals: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public RetrievePaymentTypeItemResponse retrievePaymentTypeItem(RetrievePaymentTypeItemRequest request){
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrievePaymentTypeItem. The request is {}", request);
		}
		
		try{
			RetrievePaymentTypeItemResponse response = new RetrievePaymentTypeItemResponse();
			StaticDataRequest staticDataRequest = retrievePaymentTypeItemsRequestToStaticDataRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			StaticDataResponse staticDataResponse = null;
			if (newSabreEnable(staticDataRequest.getMarket().getValue().toLowerCase())){
				staticDataResponse = sabreDcStaticDataServiceClient.getData(staticDataRequest);
			} else {
				staticDataResponse = staticDataServiceClient.getData(staticDataRequest);
			}

			/** Migrazione 3.6 fine*/

//			StaticDataResponse staticDataResponse = staticDataServiceClient.getData(staticDataRequest);
			response = staticDataResponseToRetrievePaymentTypesItemResponseConverter.convert(staticDataResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
			
			return response;
		} catch(Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing retrievePaymentTypeItem: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public RetrievePhonePrefixResponse retrievePhonePrefix(RetrievePhonePrefixRequest request){
		if (logger.isDebugEnabled()) {
			logger.debug("Execcuting method retrievePhonePrefix. Thee request is {}", request);
		}
		
		try{
			RetrievePhonePrefixResponse response = new RetrievePhonePrefixResponse();
			StaticDataRequest staticDataRequest = retrievePhonePrefixToStaticDataRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			StaticDataResponse staticDataResponse = null;
			if (newSabreEnable(staticDataRequest.getMarket().getValue().toLowerCase())){
				staticDataResponse = sabreDcStaticDataServiceClient.getData(staticDataRequest);
			} else {
				staticDataResponse = staticDataServiceClient.getData(staticDataRequest);
			}

			/** Migrazione 3.6 fine*/

//			StaticDataResponse staticDataResponse = staticDataServiceClient.getData(staticDataRequest);
			response = staticDataResponseToRetrievePhonePrefixResponseConverter.convert(staticDataResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
			
			return response;
		}catch(Exception e){
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing retrievePhonePrefix: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public RetrieveSeatTypeResponse retrieveSeatTypes(RetrieveSeatTypeRequest request){
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveSeatTypes. The request is {}", request);
		}
		
		try{
			RetrieveSeatTypeResponse response = new RetrieveSeatTypeResponse();
			StaticDataRequest staticDataRequest = retrieveSeatTypeToStaticDataRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			StaticDataResponse staticDataResponse = null;
			if (newSabreEnable(staticDataRequest.getMarket().getValue().toLowerCase())){
				staticDataResponse = sabreDcStaticDataServiceClient.getData(staticDataRequest);
			} else {
				staticDataResponse = staticDataServiceClient.getData(staticDataRequest);
			}

			/** Migrazione 3.6 fine*/

//			StaticDataResponse staticDataResponse = staticDataServiceClient.getData(staticDataRequest);
			response = staticDataResponseToRetrieveSeatTypeResponseConverter.convert(staticDataResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
			
			return response;
		}catch(Exception e){
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing retrieveSeatTypes: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public RetrieveTicketOfficeResponse retrieveTicketsOffice(RetrieveTicketOfficeRequest request){
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveTicketsOffice. The request is {}", request);
		}
		
		try{
			RetrieveTicketOfficeResponse response = new RetrieveTicketOfficeResponse();
			StaticDataRequest staticDataRequest = retrieveTicketOfficeToStaticDataRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			StaticDataResponse staticDataResponse = null;
			if (newSabreEnable(staticDataRequest.getMarket().getValue().toLowerCase())){
				staticDataResponse = sabreDcStaticDataServiceClient.getData(staticDataRequest);
			} else {
				staticDataResponse = staticDataServiceClient.getData(staticDataRequest);
			}

			/** Migrazione 3.6 fine*/

//			StaticDataResponse staticDataResponse = staticDataServiceClient.getData(staticDataRequest);
			response  = staticDataResponseToRetrieveTicketOfficeResponseConverter.convert(staticDataResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
			
			return response;
		}catch(Exception e){
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing retrieveTicketsOffice: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public RetrieveCouncilsResponse retrieveCouncils(RetrieveCouncilsRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveCouncils. The request is {}", request);
		}
		
		try {
			RetrieveCouncilsResponse response = new RetrieveCouncilsResponse();
			StaticDataRequest staticDataRequest = retrieveCouncilsRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			StaticDataResponse staticDataResponse = null;
			if (newSabreEnable(staticDataRequest.getMarket().getValue().toLowerCase())){
				staticDataResponse = sabreDcStaticDataServiceClient.getData(staticDataRequest);
			} else {
				staticDataResponse = staticDataServiceClient.getData(staticDataRequest);
			}

			/** Migrazione 3.6 fine*/

//			StaticDataResponse staticDataResponse = staticDataServiceClient.getData(staticDataRequest);
			response = staticDataResponseToRetrieveCouncilsResponseConverter.convert(staticDataResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
			
			return response;
			
		} catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing retrieveCouncils: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public RetrieveProvincesResponse retrieveProvinces(RetrieveProvincesRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveProvinces. The request is {}", request);
		}
		
		try {
			RetrieveProvincesResponse response = new RetrieveProvincesResponse();
			StaticDataRequest staticDataRequest = retrieveProvincesRequestRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			StaticDataResponse staticDataResponse = null;
			if (newSabreEnable(staticDataRequest.getMarket().getValue().toLowerCase())){
				staticDataResponse = sabreDcStaticDataServiceClient.getData(staticDataRequest);
			} else {
				staticDataResponse = staticDataServiceClient.getData(staticDataRequest);
			}

			/** Migrazione 3.6 fine*/

//			StaticDataResponse staticDataResponse = staticDataServiceClient.getData(staticDataRequest);
			response = staticDataResponseToRetrieveProvincesResponseConverter.convert(staticDataResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
			
			return response;	
		} catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing retrieveProvinces: Sid ["+request.getSid()+"]" , e);
		}
	}

	/** Migrazione 3.6 inizio*/

	private boolean newSabreEnable(String market) throws LoginException {

		ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
		Resource res = resourceResolver.getResource("/content/alitalia/alitalia-" + market);
		Page page = res.adaptTo(Page.class);
		ValueMap props = page.getProperties();
		String title = props.get("isNewService", "default value");
		logger.info("dato preso dalla pagina: " + title);
		return Boolean.parseBoolean(title);

	}

	/** Migrazione 3.6 fine*/
}