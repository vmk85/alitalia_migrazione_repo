package com.alitalia.aem.service.impl.myalitaliarest.service;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.RegisterCreditCardResult;
import com.alitalia.aem.common.messages.home.RegisterCreditCardRequest;
import com.alitalia.aem.common.messages.home.RegisterCreditCardResponse;
import com.alitalia.aem.service.api.home.IMyAlitaliaManageCreditCardService;
import com.gigya.socialize.GSArray;
import com.gigya.socialize.GSKeyNotFoundException;
import com.gigya.socialize.GSObject;
import com.gigya.socialize.GSRequest;
import com.gigya.socialize.GSResponse;

@Service
@Component(immediate = true, metatype = false)
public class MyAlitaliaManageCreditCardService implements IMyAlitaliaManageCreditCardService {
	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public RegisterCreditCardResponse storeCreditCardData(RegisterCreditCardRequest request) {
		logger.debug("[storeCreditCardData] storing credit card in MA user profile");

        try {
    		String apiKey = request.getApiKey();
    	    String secretKey = request.getSecretKey();
    	    String userKey = request.getUserKey();
            String domain = request.getDomain();

            if(userKey==null || userKey.length()==0)
                    userKey = "AI9W/RQHPi9r";

            if(apiKey==null || apiKey.length()==0)
                    apiKey = "3_6cxzgvUvDl5IM7jwQTtPhYYEjtUalJBu9cBG2wo_QIeAnwADfbItRNXs63nMqvvr";

            if(secretKey==null || secretKey.length()==0)
                    secretKey = "7XDjmCjYblwLmP+ejS79/RhqJPGapRbl";
            
            if (domain==null || domain.trim().isEmpty()==true)
            	domain = "eu1.gigya.com";
            
            logger.debug("Gigya parameter: domain [{}]", domain);
            logger.debug("Gigya parameter: apiKey [{}]", apiKey);
            logger.debug("Gigya parameter: secretKey [{}]", secretKey);
            logger.debug("Gigya parameter: userKey [{}]", userKey);

			String creditCardDataToken = request.getCreditCardDataToken();

			GSObject accountInfoData = getAccountInfoData(apiKey, secretKey, userKey, domain, request.getUID());
			
			if (accountInfoData==null) return null;

			GSArray accountInfoDataPaymentTokens = null;
			if (!accountInfoData.containsKey("payment")) {
				accountInfoData.put("payment", new GSObject());
			}
			GSObject accountInfoDataPayment = accountInfoData.getObject("payment");
			if (!accountInfoDataPayment.containsKey("token")) {
				accountInfoDataPayment.put("token", new GSArray());
			}
			accountInfoDataPaymentTokens = accountInfoDataPayment.getArray("token");
			accountInfoDataPaymentTokens.add(creditCardDataToken);
			
			GSObject accountInfoDataPaymentRoot = new GSObject();
			accountInfoDataPaymentRoot.put("payment", accountInfoDataPayment);

			GSObject result = setAccountInfoDataPaymentToken(apiKey, secretKey, userKey, domain, request.getUID(), accountInfoDataPaymentRoot);
			
			if (result==null) return null;

			return new RegisterCreditCardResponse(new RegisterCreditCardResult(creditCardDataToken));
		} catch (Exception e) {
            logger.error("[storeCreditCardData] error during storing credit card in MA user profile: {}", e.getMessage());
		}
        
        return null;
	}
	
	private GSObject getAccountInfoData(String apiKey, String secretKey, String userKey, String domain, String UID) throws GSKeyNotFoundException {
		String method = "accounts.getAccountInfo";
        GSRequest request = new GSRequest(apiKey, secretKey, method, null, true, userKey);
        request.setParam("UID", UID);
        request.setParam("include", "data");
        request.setAPIDomain(domain);
        
        GSResponse response = request.send();

        if (response.getErrorCode() == 0) {
            logger.debug("[getAccountInfoData] account info found");
			GSObject resObj = response.getData();
			return resObj.getObject("data");
        } else {
            logger.error("[getAccountInfoData] error during get account info: {}", response.getErrorMessage());
        }
        
        return null;
	}

	private GSObject setAccountInfoDataPaymentToken(String apiKey, String secretKey, String userKey, String domain, String UID, GSObject payment) {
		String method = "accounts.setAccountInfo";
        GSRequest request = new GSRequest(apiKey, secretKey, method, null, true, userKey);
        request.setParam("UID", UID);
        request.setParam("data", payment);
        request.setAPIDomain(domain);
        GSResponse response = request.send();

        if (response.getErrorCode() == 0) {
            logger.debug("[setAccountInfoDataPaymentToken] account info updated");
			GSObject resObj = response.getData();
			return resObj;
        } else {
            logger.error("[setAccountInfoDataPaymentToken] error during set account info: {}", response.getErrorMessage());
        }
        
        return null;
	}	

}
