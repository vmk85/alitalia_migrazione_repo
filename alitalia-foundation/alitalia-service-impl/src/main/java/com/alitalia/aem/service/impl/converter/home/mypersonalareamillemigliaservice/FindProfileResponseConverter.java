package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.FindProfileResponseLocal;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.FindProfileResponse;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

@Component(immediate = true, metatype = false)
@Service(value = FindProfileResponseConverter.class)
public class FindProfileResponseConverter implements Converter<FindProfileResponse,FindProfileResponseLocal > {

	@Override
	public FindProfileResponseLocal convert( FindProfileResponse source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");

		FindProfileResponseLocal destination = new FindProfileResponseLocal();

		if (source != null) {
			destination.setFlagIDProfiloExists(source.isFlagIDProfiloExists());
			destination.setIdProfilo(source.getIDProfilo().getValue());
		}
		return destination;
	}
}