package com.alitalia.aem.service.impl.converter.home.mmbsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbGdsTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbSeatStatusEnum;
import com.alitalia.aem.common.data.home.mmb.MmbCouponData;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.CompartimentalClass;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.Coupon;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.GdsType;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.SeatStatus;

@Component(immediate=true, metatype=false)
@Service(value=CouponToMmbCouponData.class)
public class CouponToMmbCouponData implements Converter<Coupon, MmbCouponData> {

	@Reference
	private FlightToMmbFlightData flightConverter;	

	@Override
	public MmbCouponData convert(Coupon source) {
		MmbCouponData destination = null;

		if (source != null) {
			destination = new MmbCouponData();

			// Il campo Barcode dovrebbe contenere l'immagine del codice a barre ma non e' usato
			destination.setBarcode(null);
			destination.setBoardingPassColor(source.getX003CBoardingPassColorX003EKBackingField());
			destination.setBoardingTime(source.getX003CBoardingTimeX003EKBackingField().toGregorianCalendar());
			destination.setChangeSeatEnabled(source.isX003CChangeSeatEnabledX003EKBackingField());
			destination.setComfortSeatPaid(source.isX003CIsComfortSeatPaidX003EKBackingField());
			destination.setEticket(source.getX003CEticketX003EKBackingField());
			destination.setFlightId(source.getX003CFlightIdX003EKBackingField());
			destination.setGate(source.getX003CGateX003EKBackingField());

			GdsType sourceCdsCouponType = source.getX003CGdsCouponTypeX003EKBackingField();
			if (sourceCdsCouponType != null)
				destination.setGdsCouponType(MmbGdsTypeEnum.fromValue(sourceCdsCouponType.value()));

			destination.setId(source.getX003CIdX003EKBackingField());
			destination.setInfantCoupon(source.isX003CIsInfantCouponX003EKBackingField());
			destination.setOldSeat(source.getX003COldSeatX003EKBackingField());
			destination.setOrderNumber(source.getX003COrderNumberX003EKBackingField());
			destination.setPassengerId(source.getX003CPassengerIdX003EKBackingField());
			destination.setPreviousSeat(source.getX003CPreviousSeatX003EKBackingField());
			destination.setSeat(source.getX003CSeatX003EKBackingField());

			CompartimentalClass sourceSeatClass = source.getX003CSeatClassX003EKBackingField();
			if (sourceSeatClass != null)
				destination.setSeatClass(MmbCompartimentalClassEnum.fromValue(sourceSeatClass.value()));

			destination.setSeatClassName(source.getX003CSeatClassNameX003EKBackingField());
			destination.setSequenceNumber(source.getX003CSequenceNumberX003EKBackingField());
			destination.setSkyPriority(source.isX003CIsSkyPriorityX003EKBackingField());
			destination.setSocialCode(source.getX003CSocialCodeX003EKBackingField());
			destination.setSpecialFare(source.getX003CSpecialFareX003EKBackingField());
			destination.setSsrCode(source.getX003CSSRCodeX003EKBackingField());

			SeatStatus sourceStatus = source.getX003CStatusX003EKBackingField();
			if (sourceStatus != null)
				destination.setStatus(MmbSeatStatusEnum.fromValue(sourceStatus.value()));

			destination.setTerminal(source.getX003CTerminalX003EKBackingField());

			destination.setFlight(flightConverter.convert(source.getX003CFlightX003EKBackingField()));
		}

		return destination;
	}

}
