package com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.MmbAmericanStatesData;
import com.alitalia.aem.common.messages.home.RetrieveMmbAmericanStatesResponse;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.AmericanStates;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.ArrayOfAmericanStates;

@Component(immediate = true, metatype = false)
@Service(value = ArrayOfAmericanStatesToRetrieveMmbAmericanStatesResponseConverter.class)
public class ArrayOfAmericanStatesToRetrieveMmbAmericanStatesResponseConverter implements Converter<ArrayOfAmericanStates, RetrieveMmbAmericanStatesResponse> {

	private static final Logger logger = LoggerFactory.getLogger(ArrayOfAmericanStatesToRetrieveMmbAmericanStatesResponseConverter.class);

	@Reference
	private AmericanStatesToMmbAmericanStatesData AmericanStatesConverter;
	
	@Reference
	private BookingMMBStaticDataServiceJAXBContextFactory jaxbContextFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public RetrieveMmbAmericanStatesResponse convert(ArrayOfAmericanStates source) {
		RetrieveMmbAmericanStatesResponse destination = new RetrieveMmbAmericanStatesResponse();
		try {
			List<MmbAmericanStatesData> americanStatesListData = new ArrayList<>();
			
			Unmarshaller phonePrefixUnmashaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			JAXBElement<AmericanStates> unmarshalledObj = null;

			List<AmericanStates> countries = source.getAmericanStates();
			for (Object americanStates : countries) {
				AmericanStates americanStatesSource = null;
				if (americanStates instanceof AmericanStates) {
					americanStatesSource = (AmericanStates) americanStates;
				} else {
					unmarshalledObj = (JAXBElement<AmericanStates>) phonePrefixUnmashaller.unmarshal((Node) americanStates);
					americanStatesSource = unmarshalledObj.getValue();
				}
				MmbAmericanStatesData mmbAmericanStatesData = AmericanStatesConverter.convert(americanStatesSource);
				americanStatesListData.add(mmbAmericanStatesData);
			}
			
			destination.setAmericanStatesList(americanStatesListData);
			
		} catch (JAXBException e) {
			logger.error("Error instantiating JAXBContext/Unmarshaller: {}", e);
		} finally {
			logger.info("Conversion executed successfully");
		}
		return destination;			
	}
}