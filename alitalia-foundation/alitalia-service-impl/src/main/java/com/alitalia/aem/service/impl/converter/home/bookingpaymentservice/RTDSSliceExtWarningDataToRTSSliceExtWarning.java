package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceExtWarningData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionSliceExtWarning;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceExtWarningDataToRTSSliceExtWarning.class)
public class RTDSSliceExtWarningDataToRTSSliceExtWarning implements
		Converter<ResultTicketingDetailSolutionSliceExtWarningData, 
					ResultTicketingDetailSolutionSliceExtWarning> {

	@Override
	public ResultTicketingDetailSolutionSliceExtWarning convert(
			ResultTicketingDetailSolutionSliceExtWarningData source) {
		ResultTicketingDetailSolutionSliceExtWarning destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionSliceExtWarning();

			destination.setChangeOfAirportField(source.isChangeOfAirportField());
			destination.setChangeOfAirportFieldSpecified(source.isChangeOfAirportFieldSpecified());
			destination.setChangeOfTerminalField(source.isChangeOfTerminalField());
			destination.setChangeOfTerminalFieldSpecified(source.isChangeOfTerminalFieldSpecified());
			destination.setLongLayoverField(source.isLongLayoverField());
			destination.setLongLayoverFieldSpecified(source.isLongLayoverField());
			destination.setLongLayoverFieldSpecified(source.isLongLayoverFieldSpecified());
			destination.setNonPreferredCabinField(source.isNonPreferredCabinField());
			destination.setNonPreferredCabinFieldSpecified(source.isNonPreferredCabinFieldSpecified());
			destination.setOvernightField(source.isOvernightField());
			destination.setOvernightFieldSpecified(source.isOvernightFieldSpecified());
			destination.setRiskyConnectionField(source.isRiskyConnectionField());
			destination.setRiskyConnectionFieldSpecified(source.isRiskyConnectionFieldSpecified());
			destination.setTightConnectionField(source.isTightConnectionField());
			destination.setTightConnectionFieldSpecified(source.isTightConnectionFieldSpecified());
		}

		return destination;
	}

}
