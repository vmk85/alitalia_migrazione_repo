package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.SubscribeSACheckEmailUserNameDuplicateRequest;
import com.alitalia.aem.common.messages.home.SubscribeSACheckUserNameDuplicateRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.CheckEmailUsernameDuplicateRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.CheckUsernameDuplicateRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

@Component(immediate = true, metatype = false)
@Service(value = CheckUserNameDuplicateRequestConverter.class)
public class CheckUserNameDuplicateRequestConverter implements Converter<SubscribeSACheckUserNameDuplicateRequest, CheckUsernameDuplicateRequest> {
	


	@Override
	public CheckUsernameDuplicateRequest convert(SubscribeSACheckUserNameDuplicateRequest source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");

		CheckUsernameDuplicateRequest destination = new CheckUsernameDuplicateRequest();
		ObjectFactory objectFactory=new ObjectFactory();

		destination.setIDProfilo(objectFactory.createCheckUsernameDuplicateRequestIDProfilo(source.getProfileID()) );
		destination.setUsername(objectFactory.createCheckUsernameDuplicateRequestUsername(source.getUsername()) );

		return destination;
	}
}