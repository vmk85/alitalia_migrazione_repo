package com.alitalia.aem.service.impl.converter.home.checkinservice;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.common.data.home.checkin.CheckinRouteData;
import com.alitalia.aem.ws.checkin.checkinservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.checkin.checkinservice.xsd2.ApisType;
import com.alitalia.aem.ws.checkin.checkinservice.xsd2.ArrayOfFlight;
import com.alitalia.aem.ws.checkin.checkinservice.xsd2.Route;
import com.alitalia.aem.ws.checkin.checkinservice.xsd2.RouteStatus;



@Component(immediate=true, metatype=false)
@Service(value=CheckinRouteDataToRoute.class)
public class CheckinRouteDataToRoute implements Converter<CheckinRouteData, Route> {
	
	@Reference
	private CheckinFlightDataToFlight checkinFlightDataConverter;
	
	@Reference
	private MmbDeepLinkDataToDeepLink  mmbDeepLinkDataConverter;

	@Override
	public Route convert(CheckinRouteData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		Route destination = null;

		if (source != null) {
			destination = objectFactory.createRoute();
			destination.setX003CIdX003EKBackingField(source.getId());
			if (source.getApisTypeRequired() != null)
				destination.setX003CApisTypeRequiredX003EKBackingField(ApisType.fromValue(source.getApisTypeRequired().value()));
			destination.setX003CCheckinEnabledX003EKBackingField(source.getCheckinEnabled());
			destination.setX003CIdIDApisX003EKBackingField(source.getIdApis());
			if(source.getIsOutwardRoute() != null){
				destination.setX003CIsOutwardRouteX003EKBackingField(source.getIsOutwardRoute());
			}
			destination.setX003CPnrX003EKBackingField(source.getPnr());
			destination.setX003CSelectedX003EKBackingField(source.getSelected());
			
			if(source.getStatus() != null){
				destination.setX003CStatusX003EKBackingField(RouteStatus.fromValue(source.getStatus().value()));
			}
			
			destination.setX003CUndoEnabledX003EKBackingField(source.getUndoEnabled());
			
			List<CheckinFlightData> sourceFlightsList = source.getFlights();
			if(sourceFlightsList != null){
				ArrayOfFlight flightsList = objectFactory.createArrayOfFlight();
				for(CheckinFlightData sourceFlightData : sourceFlightsList){
					flightsList.getFlight().add(checkinFlightDataConverter.convert(sourceFlightData));
				}
				destination.setX003CFlightsX003EKBackingField(flightsList);
			}
			
			List<CheckinFlightData> sourceInterlineFlightsList = source.getInterlineFlights();
			if(sourceInterlineFlightsList != null){
				ArrayOfFlight interlineFlightsList = objectFactory.createArrayOfFlight();
				for(CheckinFlightData sourceInterlineFlightData : sourceInterlineFlightsList){
					interlineFlightsList.getFlight().add(checkinFlightDataConverter.convert(sourceInterlineFlightData));
				}
				destination.setX003CInterlineFlightsX003EKBackingField(interlineFlightsList);
			}
			
			destination.setX003CDeepLinkX003EKBackingField(mmbDeepLinkDataConverter.convert(source.getDeepLink()));
			
		}
		return destination;
	}

}
