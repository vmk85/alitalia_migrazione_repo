package com.alitalia.aem.service.impl.converter.home.staticdataservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PaymentTypeItemData;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemResponse;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.StaticDataResponse;
import com.alitalia.aem.ws.booking.staticdataservice.xsd5.PaymentTypeItem;

@Component(immediate=true, metatype=false)
@Service(value=StaticDataResponseToRetrievePaymentTypesItemResponseConverter.class)
public class StaticDataResponseToRetrievePaymentTypesItemResponseConverter implements Converter<StaticDataResponse, RetrievePaymentTypeItemResponse> {

	private static final Logger logger = LoggerFactory.getLogger(StaticDataResponseToRetrievePaymentTypesItemResponseConverter.class);

	@Reference
	private PaymentTypeItemToPaymentTypeItemData paymentTypeItemsConverter;

	@Reference
	private StaticDataServiceJAXBContextFactory jaxbContextFactory;

	@SuppressWarnings("unchecked")
	@Override
	public RetrievePaymentTypeItemResponse convert(StaticDataResponse source) {
		RetrievePaymentTypeItemResponse response = new RetrievePaymentTypeItemResponse();
		
		
		try {
			List<PaymentTypeItem> paymentTypeItems = new ArrayList<>();
			
			List<PaymentTypeItemData> paymentTypeItemsData = new ArrayList<PaymentTypeItemData>();
			
			Unmarshaller payTypeUnmashaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			JAXBElement<PaymentTypeItem> unmarshalledObj = null;
			
			for(Object paymentTItem : source.getEntities().getValue().getAnyType()){
				unmarshalledObj = (JAXBElement<PaymentTypeItem>) payTypeUnmashaller.unmarshal((Node) paymentTItem);
				PaymentTypeItem castedPaymentType = unmarshalledObj.getValue();
				paymentTypeItems.add(castedPaymentType);
			}
			
			
			for(PaymentTypeItem paymentTypeItem : paymentTypeItems){
				PaymentTypeItemData paym = paymentTypeItemsConverter.convert(paymentTypeItem);
				paymentTypeItemsData.add(paym);
			}
			response.setPaymentTypeItemsData(paymentTypeItemsData);
			
			return response;
			
			
	
			
		} catch (JAXBException e) {
			logger.error("Error instantiating JAXBContext/Unmarshaller: {}", e);
		} finally {
			logger.info("Conversion executed successfully");
		}
		return response;			
		
	}
}