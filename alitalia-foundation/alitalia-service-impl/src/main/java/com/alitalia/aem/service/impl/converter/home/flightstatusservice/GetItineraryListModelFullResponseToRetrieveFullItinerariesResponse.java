package com.alitalia.aem.service.impl.converter.home.flightstatusservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveFullItinerariesResponse;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetIteneraryListModelFullResponse;

@Component(immediate = true, metatype = false)
@Service(value = GetItineraryListModelFullResponseToRetrieveFullItinerariesResponse.class)
public class GetItineraryListModelFullResponseToRetrieveFullItinerariesResponse implements Converter<GetIteneraryListModelFullResponse, RetrieveFullItinerariesResponse> {
	
	@Override
	public RetrieveFullItinerariesResponse convert(GetIteneraryListModelFullResponse source) {
		RetrieveFullItinerariesResponse response = new RetrieveFullItinerariesResponse();
		
		return response;
	}
}