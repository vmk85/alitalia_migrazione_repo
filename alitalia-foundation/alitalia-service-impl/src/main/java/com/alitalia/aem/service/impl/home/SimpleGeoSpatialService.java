package com.alitalia.aem.service.impl.home;

import com.alitalia.aem.ws.geospatial.service.SabreDcGeoSpatialServiceClient;
import com.day.cq.wcm.api.Page;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.RetrieveGeoAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoAirportsResponse;
import com.alitalia.aem.common.messages.home.RetrieveGeoCitiesRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoCitiesResponse;
import com.alitalia.aem.common.messages.home.RetrieveGeoCountryRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoCountryResponse;
import com.alitalia.aem.common.messages.home.RetrieveGeoNearestAirportByCoordsRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoNearestAirportByCoordsResponse;
import com.alitalia.aem.common.messages.home.RetrieveGeoOffersRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoOffersResponse;
import com.alitalia.aem.service.api.home.GeoSpatialService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.geospatialservice.GetCitiesResponseToRetrieveGeoCitiesResponse;
import com.alitalia.aem.service.impl.converter.home.geospatialservice.GetCountryByIPResponseToRetrieveGeoCountryResponse;
import com.alitalia.aem.service.impl.converter.home.geospatialservice.GetNearAirportsResponseToRetrieveGeoAirportsResponse;
import com.alitalia.aem.service.impl.converter.home.geospatialservice.GetNearestAirportByCoordsResponseToRetrieveGeoNearestAirportByCoordsResponse;
import com.alitalia.aem.service.impl.converter.home.geospatialservice.GetOffersResponseToRetrieveGeoOffersResponse;
import com.alitalia.aem.service.impl.converter.home.geospatialservice.RetrieveGeoAirportsRequestToGetNearAirportsRequest;
import com.alitalia.aem.service.impl.converter.home.geospatialservice.RetrieveGeoCitiesRequestToGetCitiesByTargetRequest;
import com.alitalia.aem.service.impl.converter.home.geospatialservice.RetrieveGeoCitiesRequestToGetCitiesRequest;
import com.alitalia.aem.service.impl.converter.home.geospatialservice.RetrieveGeoCountryRequestToGetCountryByIPRequest;
import com.alitalia.aem.service.impl.converter.home.geospatialservice.RetrieveGeoNearestAirportByCoordsRequestToGetNearerstAirportByCoordsRequest;
import com.alitalia.aem.service.impl.converter.home.geospatialservice.RetrieveGeoOffersRequestToGetOffersByTargetRequest;
import com.alitalia.aem.service.impl.converter.home.geospatialservice.RetrieveGeoOffersRequestToGetOffersRequest;
import com.alitalia.aem.ws.geospatial.service.GeoSpatialServiceClient;
import com.alitalia.aem.ws.geospatial.service.xsd1.GetCitiesByTargetRequest;
import com.alitalia.aem.ws.geospatial.service.xsd1.GetCitiesRequest;
import com.alitalia.aem.ws.geospatial.service.xsd1.GetCitiesResponse;
import com.alitalia.aem.ws.geospatial.service.xsd1.GetCountryByIPRequest;
import com.alitalia.aem.ws.geospatial.service.xsd1.GetCountryByIPResponse;
import com.alitalia.aem.ws.geospatial.service.xsd1.GetNearAirportsRequest;
import com.alitalia.aem.ws.geospatial.service.xsd1.GetNearAirportsResponse;
import com.alitalia.aem.ws.geospatial.service.xsd1.GetOffersByTargetRequest;
import com.alitalia.aem.ws.geospatial.service.xsd1.GetOffersRequest;
import com.alitalia.aem.ws.geospatial.service.xsd1.GetOffersResponse;
import com.alitalia.aem.ws.geospatial.service.xsd6.GetNearestAirportByCoordsRequest;
import com.alitalia.aem.ws.geospatial.service.xsd6.GetNearestAirportByCoordsResponse;

@Service
@Component(immediate = true, metatype = false)
public class SimpleGeoSpatialService implements GeoSpatialService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleGeoSpatialService.class);

	@Reference
	private GeoSpatialServiceClient geoSpatialServiceClient;

	@Reference
	private RetrieveGeoOffersRequestToGetOffersRequest retrieveGeoOffersRequestConverter;
	
	@Reference
	private RetrieveGeoOffersRequestToGetOffersByTargetRequest retrieveGeoOffersByTargetRequestConverter;
	
	@Reference
	private GetOffersResponseToRetrieveGeoOffersResponse retrieveGeoOffersResponseConverter;
	
	@Reference
	private RetrieveGeoCitiesRequestToGetCitiesRequest retrieveGeoCitiesRequestConverter;
	
	@Reference
	private RetrieveGeoCitiesRequestToGetCitiesByTargetRequest retrieveGeoCitiesByTargetRequestConverter;
	
	@Reference
	private GetCitiesResponseToRetrieveGeoCitiesResponse retrieveGeoCitiesResponseConverter;
	
	@Reference
	private RetrieveGeoAirportsRequestToGetNearAirportsRequest retrieveGeoAirportsRequestConverter;
	
	@Reference
	private GetNearAirportsResponseToRetrieveGeoAirportsResponse retrieveGeoAirportsResponseConverter;
	
	@Reference
	private RetrieveGeoCountryRequestToGetCountryByIPRequest retrieveGeoCountryRequestConverter;
	
	@Reference
	private GetCountryByIPResponseToRetrieveGeoCountryResponse retrieveGeoCountryResponseConverter;
	
	@Reference
	private RetrieveGeoNearestAirportByCoordsRequestToGetNearerstAirportByCoordsRequest retrieveNearestAirportRequestConverter;
	
	@Reference
	private GetNearestAirportByCoordsResponseToRetrieveGeoNearestAirportByCoordsResponse retrieveNearestAirportResponseConverter;

	/** Migrazione 3.6 inizio*/

	@Reference
	private ResourceResolverFactory resolverFactory;

	@Reference
	private SabreDcGeoSpatialServiceClient sabreDcGeoSpatialServiceClient;

	/** Migrazione 3.6 fine*/

	@Override
	public RetrieveGeoOffersResponse retrieveGeoOffers(RetrieveGeoOffersRequest request) {
		if(logger.isDebugEnabled())
			logger.debug("Executing method retrieveGeoOffers. The request is {}", request);
		try {

			RetrieveGeoOffersResponse response = new RetrieveGeoOffersResponse();
			GetOffersRequest offersRequest = retrieveGeoOffersRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			GetOffersResponse getOffersResponse = null;
			if (newSabreEnable(offersRequest.getMarketCode().getValue().toLowerCase())){
				getOffersResponse = sabreDcGeoSpatialServiceClient.getOffers(offersRequest);
			} else {
				getOffersResponse = geoSpatialServiceClient.getOffers(offersRequest);
			}

			/** Migrazione 3.6 fine*/

//			GetOffersResponse getOffersResponse = geoSpatialServiceClient.getOffers(offersRequest);
			response = retrieveGeoOffersResponseConverter.convert(getOffersResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if(logger.isDebugEnabled())
				logger.debug("Executed method. The response is {}", response);
			return response;
			
		} catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing retrieveGeoOffers: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public RetrieveGeoOffersResponse retrieveGeoOffersByTarget(RetrieveGeoOffersRequest request) {
		logger.debug("Executing method retrieveGeoOffersByTarget. The request is {}", request);
		
		try {
			RetrieveGeoOffersResponse response = new RetrieveGeoOffersResponse();
			GetOffersByTargetRequest offersRequest = retrieveGeoOffersByTargetRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			GetOffersResponse getOffersResponse = null;
			if (newSabreEnable(offersRequest.getMarketCode().getValue().toLowerCase())){
				getOffersResponse = sabreDcGeoSpatialServiceClient.getOffersByTarget(offersRequest);
			} else {
				getOffersResponse = geoSpatialServiceClient.getOffersByTarget(offersRequest);
			}

			/** Migrazione 3.6 fine*/

//			GetOffersResponse getOffersResponse = geoSpatialServiceClient.getOffersByTarget(offersRequest);
			response = retrieveGeoOffersResponseConverter.convert(getOffersResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			logger.debug("Executed method retrieveGeoOffersByTarget. The response is {}", response);
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method retrieveGeoOffersByTarget.", e);
			throw new AlitaliaServiceException("Exception executing retrieveGeoOffersByTarget: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public RetrieveGeoCitiesResponse retrieveGeoCities(RetrieveGeoCitiesRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method retrieveGeoCities. The request is {}", request);

		try {

			RetrieveGeoCitiesResponse response = new RetrieveGeoCitiesResponse();
			GetCitiesRequest citiesRequest = retrieveGeoCitiesRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			GetCitiesResponse getCitiesResponse = null;
			if (newSabreEnable(citiesRequest.getMarketCode().getValue().toLowerCase())){
				getCitiesResponse = sabreDcGeoSpatialServiceClient.getCities(citiesRequest);
			} else {
				getCitiesResponse = geoSpatialServiceClient.getCities(citiesRequest);
			}

			/** Migrazione 3.6 fine*/

//			GetCitiesResponse getCitiesResponse = geoSpatialServiceClient.getCities(citiesRequest);
			response = retrieveGeoCitiesResponseConverter.convert(getCitiesResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if(logger.isDebugEnabled())
			logger.debug("Executed method. The response is {}", response);
			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing retrieveGeoCities: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public RetrieveGeoCitiesResponse retrieveGeoCitiesByTarget(RetrieveGeoCitiesRequest request) {
		logger.debug("Executing method retrieveGeoCitiesByTarget. The request is {}", request);

		try {

			RetrieveGeoCitiesResponse response = new RetrieveGeoCitiesResponse();
			GetCitiesByTargetRequest citiesRequest = retrieveGeoCitiesByTargetRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			GetCitiesResponse getCitiesResponse = null;
			if (newSabreEnable(citiesRequest.getMarketCode().getValue().toLowerCase())){
				getCitiesResponse = sabreDcGeoSpatialServiceClient.getCitiesByTarget(citiesRequest);
			} else {
				getCitiesResponse = geoSpatialServiceClient.getCitiesByTarget(citiesRequest);
			}

			/** Migrazione 3.6 fine*/

//			GetCitiesResponse getCitiesResponse = geoSpatialServiceClient.getCitiesByTarget(citiesRequest);
			response = retrieveGeoCitiesResponseConverter.convert(getCitiesResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			logger.debug("Executed method. The response is {}", response);
			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method retrieveGeoCitiesByTarget.", e);
			throw new AlitaliaServiceException("Exception executing retrieveGeoCitiesByTarget: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public RetrieveGeoAirportsResponse retrieveGeoAirports(RetrieveGeoAirportsRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method retrieveGeoAirports. The request is {}", request);
		
		try {

			RetrieveGeoAirportsResponse response = new RetrieveGeoAirportsResponse();
			GetNearAirportsRequest getNearAirportsRequest = retrieveGeoAirportsRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			GetNearAirportsResponse getNearAirportsResponse = null;
			if (newSabreEnable(getNearAirportsRequest.getMarketCode().getValue().toLowerCase())){
				getNearAirportsResponse = sabreDcGeoSpatialServiceClient.getNearAirports(getNearAirportsRequest);
			} else {
				getNearAirportsResponse = geoSpatialServiceClient.getNearAirports(getNearAirportsRequest);
			}

			/** Migrazione 3.6 fine*/

//			GetNearAirportsResponse getNearAirportsResponse = geoSpatialServiceClient.getNearAirports(getNearAirportsRequest);
			response = retrieveGeoAirportsResponseConverter.convert(getNearAirportsResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if(logger.isDebugEnabled())
			logger.debug("Executed method. The response is {}", response);
			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing retrieveGeoAirports: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public RetrieveGeoCountryResponse retrieveGeoCountry(RetrieveGeoCountryRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method retrieveGeoCountry. The request is {}", request);
		
		try {

			RetrieveGeoCountryResponse response = new RetrieveGeoCountryResponse();
			GetCountryByIPRequest getCountryByIPRequest = retrieveGeoCountryRequestConverter.convert(request);
			GetCountryByIPResponse getCountryByIPResponse = geoSpatialServiceClient.getCountryByIP(getCountryByIPRequest);
			response = retrieveGeoCountryResponseConverter.convert(getCountryByIPResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if(logger.isDebugEnabled())
				logger.debug("Executed method. The response is {}", response);
			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing retrieveGeoCountry: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public RetrieveGeoNearestAirportByCoordsResponse retrieveGeoNearestAirportByCoords(RetrieveGeoNearestAirportByCoordsRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method retrieveGeoNearestAirportByCoords. The request is {}", request);

		try {

			RetrieveGeoNearestAirportByCoordsResponse response = new RetrieveGeoNearestAirportByCoordsResponse();
			GetNearestAirportByCoordsRequest serviceRequest = retrieveNearestAirportRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			GetNearestAirportByCoordsResponse serviceResponse = null;
			if (newSabreEnable(serviceRequest.getCountryCode().getValue().toLowerCase())){
				serviceResponse = sabreDcGeoSpatialServiceClient.getNearestAirportByCoords(serviceRequest);
			} else {
				serviceResponse = geoSpatialServiceClient.getNearestAirportByCoords(serviceRequest);
			}

			/** Migrazione 3.6 fine*/

//			GetNearestAirportByCoordsResponse serviceResponse = geoSpatialServiceClient.getNearestAirportByCoords(serviceRequest);
			response = retrieveNearestAirportResponseConverter.convert(serviceResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if(logger.isDebugEnabled())
			logger.debug("Executed method. The response is {}", response);
			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing retrieveGeoNearestAirportByCoords: Sid ["+request.getSid()+"]" , e);
		}
	}

	/** Migrazione 3.6 inizio*/

	private boolean newSabreEnable(String market) throws LoginException {

		ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
		Resource res = resourceResolver.getResource("/content/alitalia/alitalia-" + market);
		Page page = res.adaptTo(Page.class);
		ValueMap props = page.getProperties();
		String title = props.get("isNewService", "default value");
		logger.info("dato preso dalla pagina: " + title);
		return Boolean.parseBoolean(title);

	}

	/** Migrazione 3.6 fine*/

}