package com.alitalia.aem.service.impl.converter.home.mmbinsuranceservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.enumerations.DocumentTypeEnum;
import com.alitalia.aem.common.data.home.mmb.MmbApisInfoData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd3.ApisData;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd3.DocumentType;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd3.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=MmbApisInfoDataToApisData.class)
public class MmbApisInfoDataToApisData implements Converter<MmbApisInfoData, ApisData> {

	@Override
	public ApisData convert(MmbApisInfoData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		ApisData destination = null;

		if (source != null) {
			destination = objectFactory.createApisData();

			destination.setX003CBirthDateX003EKBackingField(
					XsdConvertUtils.toXMLGregorianCalendar(source.getBirthDate()));
			destination.setX003CDestinationAddressX003EKBackingField(source.getDestinationAddress());
			destination.setX003CDestinationCityX003EKBackingField(source.getDestinationCity());
			destination.setX003CDestinationStateX003EKBackingField(source.getDestinationState());
			destination.setX003CDestinationZipCodeX003EKBackingField(source.getDestinationZipCode());
			destination.setX003CDocTypeX003EKBackingField(source.getDocType());
			destination.setX003CExtraDocumentExpirationDateX003EKBackingField(
					XsdConvertUtils.toXMLGregorianCalendar(source.getExtraDocumentExpirationDate()));
			destination.setX003CExtraDocumentIssuingCountryX003EKBackingField(source.getExtraDocumentIssuingCountry());
			destination.setX003CExtraDocumentIssuingStateX003EKBackingField(source.getExtraDocumentIssuingState());
			destination.setX003CExtraDocumentNumberX003EKBackingField(source.getExtraDocumentNumber());

			DocumentTypeEnum sourceExtraDocType = source.getExtraDocumentType();
			if (sourceExtraDocType != null)
				destination.setX003CExtraDocumentTypeX003EKBackingField(DocumentType.fromValue(sourceExtraDocType.value()));
			else
				destination.setX003CExtraDocumentTypeX003EKBackingField(DocumentType.NONE);

			destination.setX003CGenderX003EKBackingField(source.getGender());
			destination.setX003CLastnameX003EKBackingField(source.getLastname());
			destination.setX003CNameX003EKBackingField(source.getName());
			destination.setX003CNationalityX003EKBackingField(source.getNationality());
			destination.setX003CPassportExpirationDateX003EKBackingField(
					XsdConvertUtils.toXMLGregorianCalendar(source.getPassportExpirationDate()));
			destination.setX003CPassportNumberX003EKBackingField(source.getPassportNumber());
			destination.setX003CResidenceCountryX003EKBackingField(source.getResidenceCountry());
			destination.setX003CSecondNameX003EKBackingField(source.getSecondName());
		}

		return destination;
	}

}
