package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingSaleFareTotalData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionPricingSaleFareTotal;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingSaleFareTotalToRTDSPricingSaleFareTotalData.class)
public class RTDSPricingSaleFareTotalToRTDSPricingSaleFareTotalData implements
		Converter<ResultTicketingDetailSolutionPricingSaleFareTotal,
					ResultTicketingDetailSolutionPricingSaleFareTotalData> {

	@Override
	public ResultTicketingDetailSolutionPricingSaleFareTotalData convert(
			ResultTicketingDetailSolutionPricingSaleFareTotal source) {
		ResultTicketingDetailSolutionPricingSaleFareTotalData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingSaleFareTotalData();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
