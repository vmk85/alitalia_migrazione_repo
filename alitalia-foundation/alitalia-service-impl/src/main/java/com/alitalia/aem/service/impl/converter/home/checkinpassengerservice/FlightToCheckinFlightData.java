package com.alitalia.aem.service.impl.converter.home.checkinpassengerservice;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferenceCardinality;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbFlightStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbLegTypeEnum;
import com.alitalia.aem.service.impl.converter.home.mmbcommonservice.PassengerToMmbPassengerData;
import com.alitalia.aem.ws.checkin.passengerservice.xsd2.CompartimentalClass;
import com.alitalia.aem.ws.checkin.passengerservice.xsd2.Flight;
import com.alitalia.aem.ws.checkin.passengerservice.xsd2.FlightStatus;
import com.alitalia.aem.ws.checkin.passengerservice.xsd2.LegType;



@Component(immediate=true, metatype=false)
@Service(value=FlightToCheckinFlightData.class)
public class FlightToCheckinFlightData implements Converter<Flight, CheckinFlightData> {

	@Reference
	private DeepLinkToMmbDeepLinkCheckinData deepLinkConverter;

	@Reference
	private AirportToCheckinAirport airportConverter;

	// E' stata inserita la cardianlita' opzionale perche' c'e' un ciclo
	// di riferimenti fra Passenger, Flight e Coupon che altrimenti impedisce
	// che salgano i service component
	@Reference(policy=ReferencePolicy.DYNAMIC, cardinality=ReferenceCardinality.OPTIONAL_UNARY)
	private volatile PassengerToMmbPassengerData passengerConverter;

	@Override
	public CheckinFlightData convert(Flight source) {
		CheckinFlightData destination = null;

		if (source != null) {
			destination = new CheckinFlightData();

			destination.setId(source.getX003CIdX003EKBackingField());

			Calendar arrivalDateTime = source.getX003CArrivalDateX003EKBackingField().toGregorianCalendar();
			Calendar arrivalTime = source.getX003CArrivalTimeX003EKBackingField().toGregorianCalendar();
			arrivalDateTime.set(Calendar.HOUR_OF_DAY, arrivalTime.get(Calendar.HOUR_OF_DAY));
			arrivalDateTime.set(Calendar.MINUTE, arrivalTime.get(Calendar.MINUTE));
			arrivalDateTime.set(Calendar.SECOND, arrivalTime.get(Calendar.SECOND));
			destination.setArrivalDateTime(arrivalDateTime);

			List<CompartimentalClass> compClasses = source.getX003CCabinsX003EKBackingField()!=null ? source.getX003CCabinsX003EKBackingField().getCompartimentalClass() : null;
			
			if (compClasses != null){
				List<MmbCompartimentalClassEnum> mmbCompClasses = new ArrayList<MmbCompartimentalClassEnum>();
				for (CompartimentalClass compClass : compClasses){
					mmbCompClasses.add(MmbCompartimentalClassEnum.fromValue(compClass.value()));
				}
				destination.setCabins(mmbCompClasses);
			}

			destination.setCarrier(source.getX003CCarrierX003EKBackingField());
			destination.setComfortSeatFare(source.getX003CComfortSeatFareX003EKBackingField());
			destination.setComfortSeatPaid(source.isX003CComfortSeatPaidX003EKBackingField());
			
			destination.setDeepLinkCode(
					deepLinkConverter.convert(source.getX003CDeepLinkCodeX003EKBackingField()));

			Calendar departureDateTime = source.getX003CDepartureDateX003EKBackingField().toGregorianCalendar();
			Calendar departureTime = source.getX003CDepartureTimeX003EKBackingField().toGregorianCalendar();
			departureDateTime.set(Calendar.HOUR_OF_DAY, departureTime.get(Calendar.HOUR_OF_DAY));
			departureDateTime.set(Calendar.MINUTE, departureTime.get(Calendar.MINUTE));
			departureDateTime.set(Calendar.SECOND, departureTime.get(Calendar.SECOND));
			destination.setDepartureDateTime(departureDateTime);

			destination.setEticketClass(source.getX003CEticketClassX003EKBackingField());
			destination.setEticket(source.getX003CEticketX003EKBackingField());
			destination.setFlightNumber(source.getX003CFlightNumberX003EKBackingField());
			
			destination.setFrom(airportConverter.convert(source.getX003CFromX003EKBackingField()));
			
			destination.setHasComfortSeat(source.isX003CHasComfortSeatX003EKBackingField());
			destination.setIndex(source.getX003CIndexX003EKBackingField());

			LegType sourceLegType = source.getX003CLegTypeX003EKBackingField();
			if (sourceLegType != null)
				destination.setLegType(MmbLegTypeEnum.fromValue(sourceLegType.value()));

			destination.setOperatingCarrier(source.getX003COperatingCarrierX003EKBackingField());
			destination.setOperatingFlightNumber(source.getX003COperatingFlightNumberX003EKBackingField());
			destination.setPnr(source.getX003CPnrX003EKBackingField());
			destination.setRph(source.getX003CReservationRPHX003EKBackingField());
			destination.setRouteId(source.getX003CRouteIdX003EKBackingField());
			CompartimentalClass sourceSeatClass = source.getX003CSeatClassX003EKBackingField();
			if (sourceSeatClass != null)
				destination.setSeatClass(MmbCompartimentalClassEnum.fromValue(sourceSeatClass.value()));

			FlightStatus sourceFlightStatus = source.getX003CStatusX003EKBackingField();
			if (sourceFlightStatus != null)
				destination.setStatus(MmbFlightStatusEnum.fromValue(sourceFlightStatus.value()));

			destination.setTo(airportConverter.convert(source.getX003CToX003EKBackingField()));
		}
		
		return destination;
	}

}
