package com.alitalia.aem.service.impl.converter.home.commonservice;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.data.home.BaggageAllowanceData;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightDetailsData;
import com.alitalia.aem.common.data.home.PropertiesData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.commonservice.xsd2.Airport;
import com.alitalia.aem.ws.booking.commonservice.xsd2.BaggageAllowance;
import com.alitalia.aem.ws.booking.commonservice.xsd2.Brand;
import com.alitalia.aem.ws.booking.commonservice.xsd2.DirectFlight;
import com.alitalia.aem.ws.booking.commonservice.xsd2.FlightDetails;
import com.alitalia.aem.ws.booking.commonservice.xsd2.FlightType;
import com.alitalia.aem.ws.booking.commonservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.booking.commonservice.xsd3.ArrayOfanyType;
import com.alitalia.aem.ws.booking.commonservice.xsd3.ArrayOfstring;
import com.alitalia.aem.ws.booking.commonservice.xsd4.Cabin;

@Component(immediate = true, metatype = false)
@Service(value = DirectFlightDataToDirectFlight.class)
public class DirectFlightDataToDirectFlight implements Converter<DirectFlightData, DirectFlight> {

	private static final Logger logger = LoggerFactory.getLogger(DirectFlightDataToDirectFlight.class);
	
	@Reference
	private BaggageAllowanceDataToBaggageAllowance baggageAllowanceDataConverter; 

	@Reference
	private BrandDataToBrand brandDataConverter;
	
	@Reference
	private AirportDataToAirport airportDataConverter;

	@Reference
	private FlightDetailsDataToFlightDetails flightDetailsDataConverter;

	@Reference
	private PropertiesDataToArrayOfDictionaryItem propertiesDataConverter;

	@Override
	public DirectFlight convert(DirectFlightData source) {
		DirectFlight directFlight = new DirectFlight();

		ObjectFactory factory2 = new ObjectFactory();
		
		com.alitalia.aem.ws.booking.commonservice.xsd3.ObjectFactory factory3 = 
				new com.alitalia.aem.ws.booking.commonservice.xsd3.ObjectFactory();

		directFlight.setType(FlightType.DIRECT);
		directFlight.setCabin(Cabin.fromValue(source.getCabin().value()));
		directFlight.setCarrier(factory2.createDirectFlightCarrier(source.getCarrier()));
		directFlight.setCarrierForLogo(factory2.createDirectFlightCarrierForLogo(source.getCarrierForLogo()));

		int days = (int)(TimeUnit.HOURS.toDays(source.getDurationHour()));
		int hours = source.getDurationHour() - (days * 24);
		int minutes = source.getDurationMinutes();
		DatatypeFactory factory;
		try {
			factory = DatatypeFactory.newInstance();
			Duration duration = factory.newDurationDayTime(true, days, hours, minutes, 0);
			directFlight.setDuration(duration);
		} catch (DatatypeConfigurationException e) {
			logger.error("Error while setting Duration field: {}", e);
		}

		Boolean sourceEnabledSeatMap = source.getEnabledSeatMap();
		if (sourceEnabledSeatMap != null)
			directFlight.setEnabledSeatMap(factory2.createDirectFlightEnabledSeatMap(sourceEnabledSeatMap));
		else
			directFlight.setEnabledSeatMap(factory2.createDirectFlightEnabledSeatMap(false));

		directFlight.setFlightNumber(factory2.createDirectFlightFlightNumber(source.getFlightNumber()));
		directFlight.setMarriageGroup(factory2.createDirectFlightMarriageGroup(source.getMarriageGroup()));
		directFlight.setMileage(source.getMileage());
		directFlight.setOperationalCarrier(factory2.createDirectFlightOperationalCarrier(source.getOperationalCarrier()));
		directFlight.setOperationalFlightNumber(factory2.createDirectFlightOperationalFlightNumber(source.getOperationalFlightNumber()));
		directFlight.setOperationalFlightText(factory2.createDirectFlightOperationalFlightText(source.getOperationalFlightText()));
		directFlight.setStopOver(source.getStopOver());
		directFlight.setUrlConditions(factory2.createDirectFlightUrlConditions(source.getUrlConditions()));

		Calendar arrivalDate = source.getArrivalDate();
		if (arrivalDate != null)
			directFlight.setArrivalDate(XsdConvertUtils.toXMLGregorianCalendar(arrivalDate));
		Calendar departureDate = source.getDepartureDate();
		if (departureDate != null)
			directFlight.setDepartureDate(XsdConvertUtils.toXMLGregorianCalendar(departureDate));

		ArrayOfstring arrayOfString = new ArrayOfstring();
		List<String> compartimentalClass = arrayOfString.getString();
		List<String> sourceCompartimentalClass = source.getCompartimentalClass();

		if (sourceCompartimentalClass != null) {
			for (String compClassSource : sourceCompartimentalClass){ 
				compartimentalClass.add(compClassSource);
			}
		}
		directFlight.setCompartimentalClass(factory2.createDirectFlightCompartimentalClass(arrayOfString));

		ArrayOfanyType arrayOfanyTypeBaggagesAllowance = new ArrayOfanyType();
		List<Object> baggagesAllowance = arrayOfanyTypeBaggagesAllowance.getAnyType();
		List<BaggageAllowanceData> sourceBaggageAllowance = source.getBaggageAllowanceList();
		if (sourceBaggageAllowance != null) {
			for (BaggageAllowanceData baggageAllowanceData : sourceBaggageAllowance) {
				BaggageAllowance baggageAllowance = baggageAllowanceDataConverter.convert(baggageAllowanceData);
				baggagesAllowance.add(baggageAllowance);
			}
		}
		directFlight.setBaggageAllowanceList(factory2.createBrandBaggageAllowanceList(arrayOfanyTypeBaggagesAllowance));

		List<BrandData> brandsData = source.getBrands();
		ArrayOfanyType brands = factory3.createArrayOfanyType();
		if (brandsData != null) {
			for (BrandData sourceBrandData : brandsData) {
				Brand brand = brandDataConverter.convert(sourceBrandData);
				brands.getAnyType().add(brand);
			}
		}
		directFlight.setBrands(factory2.createAFlightBaseBrands(brands));

		FlightDetailsData flightDetailsData = source.getDetails();
		if (flightDetailsData != null){
			FlightDetails flightDetails = flightDetailsDataConverter.convert(flightDetailsData);
			directFlight.setDetails(factory2.createDirectFlightDetails(flightDetails));
		}

		AirportData fromAirportData = source.getFrom();
		AirportData toAirportData = source.getTo();
		
		Airport from = airportDataConverter.convert(fromAirportData);
		Airport to = airportDataConverter.convert(toAirportData);
		
		directFlight.setFrom(factory2.createDirectFlightFrom(from));
		directFlight.setTo(factory2.createDirectFlightTo(to));

		PropertiesData sourceProperties = source.getProperties();
		if (sourceProperties != null)
			directFlight.setProperties(factory2.createABoomBoxGenericInfoProperties(
				propertiesDataConverter.convert(sourceProperties)));
		else
			directFlight.setProperties(factory2.createABoomBoxGenericInfoProperties(null));

		return directFlight;
	}
}
