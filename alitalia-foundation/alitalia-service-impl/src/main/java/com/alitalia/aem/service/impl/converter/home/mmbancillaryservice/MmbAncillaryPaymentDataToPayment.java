package com.alitalia.aem.service.impl.converter.home.mmbancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryPaymentData;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.Payment;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.PaymentType;

@Component(immediate=true, metatype=false)
@Service(value=MmbAncillaryPaymentDataToPayment.class)
public class MmbAncillaryPaymentDataToPayment implements Converter<MmbAncillaryPaymentData, Payment> {

	@Override
	public Payment convert(MmbAncillaryPaymentData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		Payment destination = null;

		if (source != null) {
			destination = objectFactory.createPayment();

			destination.setX003CAddressX003EKBackingField(source.getAddress());
			destination.setX003CCancellUrlX003EKBackingField(source.getCancellUrl());
			destination.setX003CCardnumberX003EKBackingField(source.getCardNumber());
			destination.setX003CCityX003EKBackingField(source.getCity());
			destination.setX003CCountryX003EKBackingField(source.getCountry());
			destination.setX003CCreditCardTypeX003EKBackingField(source.getCreditCardType());
			destination.setX003CCvvX003EKBackingField(source.getCvv());
			destination.setX003CEmailX003EKBackingField(source.getEmail());
			destination.setX003CErrorUrlX003EKBackingField(source.getErrorUrl());
			destination.setX003CExpirationMonthX003EKBackingField(source.getExpirationMonth());
			destination.setX003CExpirationYearX003EKBackingField(source.getExpirationYear());
			destination.setX003CIpAddressX003EKBackingField(source.getIpAddress());
			destination.setX003COKUrlX003EKBackingField(source.getOkUrl());
			destination.setX003COwnerLastNameX003EKBackingField(source.getOwnerLastName());
			destination.setX003COwnerNameX003EKBackingField(source.getOwnerName());

			PaymentTypeEnum sourcePaymentType = source.getPaymentType();
			if (sourcePaymentType != null)
				destination.setX003CPaymentTypeX003EKBackingField(PaymentType.fromValue(sourcePaymentType.value()));

			destination.setX003CStareX003EKBackingField(source.getStare());
			destination.setX003CUserAgentX003EKBackingField(source.getUserAgent());
			destination.setX003CZipCodeX003EKBackingField(source.getZipCode());
		}

		return destination;
	}

}
