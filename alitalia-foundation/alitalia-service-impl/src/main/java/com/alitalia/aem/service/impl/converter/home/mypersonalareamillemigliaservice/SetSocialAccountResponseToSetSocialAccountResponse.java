package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SetSocialAccountResponse;

@Component(immediate = true, metatype = false)
@Service(value = SetSocialAccountResponseToSetSocialAccountResponse.class)
public class SetSocialAccountResponseToSetSocialAccountResponse 
		implements Converter<SetSocialAccountResponse, com.alitalia.aem.common.messages.home.SetSocialAccountResponse> {

	@Override
	public com.alitalia.aem.common.messages.home.SetSocialAccountResponse convert(SetSocialAccountResponse source) {
		com.alitalia.aem.common.messages.home.SetSocialAccountResponse destination = null;

		if (source != null) {
			
			destination = new com.alitalia.aem.common.messages.home.SetSocialAccountResponse();
			
			destination.setSucceeded(
					source.isSucceeded());
			
		}

		return destination;
	}

}
