package com.alitalia.aem.service.impl.converter.home.mmbsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.MmbModifyPnrApisInfoResponse;
import com.alitalia.aem.ws.mmb.searchservice.xsd1.ApisDataResponse;

@Component(immediate=true, metatype=false)
@Service(value=ApisDataResponseToMmbModifyPnrApisInfoResponse.class)
public class ApisDataResponseToMmbModifyPnrApisInfoResponse implements
		Converter<ApisDataResponse, MmbModifyPnrApisInfoResponse> {

	@Override
	public MmbModifyPnrApisInfoResponse convert(ApisDataResponse source) {
		MmbModifyPnrApisInfoResponse destination = null;

		if (source != null) {
			destination = new MmbModifyPnrApisInfoResponse();

			destination.setSuccessful(source.isIsUpdated());
		}

		return destination;
	}

}
