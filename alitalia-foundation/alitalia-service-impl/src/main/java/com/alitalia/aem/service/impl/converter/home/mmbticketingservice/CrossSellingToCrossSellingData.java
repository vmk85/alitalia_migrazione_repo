package com.alitalia.aem.service.impl.converter.home.mmbticketingservice;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.CrossSellingData;
import com.alitalia.aem.common.data.home.enumerations.CrossSellingTypeEnum;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd2.CrossSellingType;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd4.CrossSelling;

@Component(immediate = true, metatype = false)
@Service(value = CrossSellingToCrossSellingData.class)
public class CrossSellingToCrossSellingData implements
		Converter<CrossSelling, CrossSellingData> {

	@Override
	public CrossSellingData convert(CrossSelling source) {
		CrossSellingData destination = null;
		if (source != null) {
			destination = new CrossSellingData();

			JAXBElement<String> sourceDescription = source.getDescription();
			if (sourceDescription != null)
				destination.setDescription(sourceDescription.getValue());

			JAXBElement<String> sourceImageName = source.getImageName();
			if (sourceImageName != null)
				destination.setImageName(sourceImageName.getValue());

			JAXBElement<String> sourceName = source.getName();
			if (sourceName != null)
				destination.setName(sourceName.getValue());

			CrossSellingType sourceType = source.getType();
			if (sourceType != null)
				destination.setType(CrossSellingTypeEnum.fromValue(sourceType.value()));

			JAXBElement<String> sourceUrl = source.getUrl();
			if (sourceUrl != null)
				destination.setUrl(sourceUrl.getValue());

			BigDecimal sourcePrice = source.getPrice();
			if (sourcePrice != null)
				destination.setPrice(sourcePrice.doubleValue());

			JAXBElement<String> sourcePriceDescription = source.getPriceDescription();
			if (sourcePriceDescription != null)
				destination.setPriceDesription(sourcePriceDescription.getValue());

			List<String> otherImages = null;
			if (source.getOtherImages() != null &&
					source.getOtherImages().getValue() != null &&
					source.getOtherImages().getValue().getString() != null &&
					!source.getOtherImages().getValue().getString().isEmpty()) {
				List<String> sourceOtherImages = source.getOtherImages().getValue().getString();
				otherImages = new ArrayList<String>();
				for (String sourceImage : sourceOtherImages)
					otherImages.add(sourceImage);
			}
			destination.setOtherImages(otherImages);
		}

		return destination;
	}

}
