package com.alitalia.aem.service.impl.converter.home.staticdataservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MunicipalityData;
import com.alitalia.aem.ws.booking.staticdataservice.xsd2.Municipality;

@Component(immediate = true, metatype = false)
@Service(value = MunicipalityToMunicipalityData.class)
public class MunicipalityToMunicipalityData implements Converter<Municipality, MunicipalityData> {

	@Override
	public MunicipalityData convert(Municipality source) {
		MunicipalityData destination = new MunicipalityData();
		destination.setActive(source.isActive());
		if(source.getCode() !=null)
			destination.setCode(source.getCode().getValue());
		destination.setDescription(source.getDescription().getValue());
		destination.setGermanDescription(source.getGermanDescription().getValue());
		destination.setProvinceCode(source.getProvinceCode().getValue());
		return destination;
	}
}