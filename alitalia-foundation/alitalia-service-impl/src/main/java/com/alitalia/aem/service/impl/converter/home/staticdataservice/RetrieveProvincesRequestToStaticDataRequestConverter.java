package com.alitalia.aem.service.impl.converter.home.staticdataservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveProvincesRequest;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.StaticDataRequest;
import com.alitalia.aem.ws.booking.staticdataservice.xsd2.StaticDataType;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveProvincesRequestToStaticDataRequestConverter.class)
public class RetrieveProvincesRequestToStaticDataRequestConverter implements Converter<RetrieveProvincesRequest, StaticDataRequest> {

	@Override
	public StaticDataRequest convert(RetrieveProvincesRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();		
		StaticDataRequest destination = objectFactory.createStaticDataRequest();
		
		String itemCache = source.getItemCache();
		String languageCode = source.getLanguageCode();
		String market = source.getMarket();
		
		destination.setLanguageCode(objectFactory.createStaticDataRequestLanguageCode(languageCode));
		destination.setMarket(objectFactory.createStaticDataRequestMarket(market));
		destination.setItemCache(objectFactory.createStaticDataRequestItemCache(itemCache));
		destination.setType(StaticDataType.PROVINCES_LIST);

		return destination;
	}
}