package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingDisplayFareTotalData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingDisplayFareTotal;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingDisplayFareTotalToRTDSPricingDisplayFareTotalData.class)
public class RTDSPricingDisplayFareTotalToRTDSPricingDisplayFareTotalData implements
		Converter<ResultTicketingDetailSolutionPricingDisplayFareTotal,
						ResultTicketingDetailSolutionPricingDisplayFareTotalData> {

	@Override
	public ResultTicketingDetailSolutionPricingDisplayFareTotalData convert(
			ResultTicketingDetailSolutionPricingDisplayFareTotal source) {
		ResultTicketingDetailSolutionPricingDisplayFareTotalData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingDisplayFareTotalData();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
