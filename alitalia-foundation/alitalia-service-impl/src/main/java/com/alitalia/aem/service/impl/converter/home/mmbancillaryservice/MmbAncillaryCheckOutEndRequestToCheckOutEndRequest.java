package com.alitalia.aem.service.impl.converter.home.mmbancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutEndRequest;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.CheckOutEndRequest;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=MmbAncillaryCheckOutEndRequestToCheckOutEndRequest.class)
public class MmbAncillaryCheckOutEndRequestToCheckOutEndRequest implements
		Converter<MmbAncillaryCheckOutEndRequest, CheckOutEndRequest> {

	@Reference
	private MmbAncillaryPaymentDataToPayment mmbAncillaryPaymentDataConverter;

	@Override
	public CheckOutEndRequest convert(MmbAncillaryCheckOutEndRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		CheckOutEndRequest destination = null;

		if (source != null) {
			destination = objectFactory.createCheckOutEndRequest();

			destination.setCartID(source.getCartId());
			destination.setMachineName(objectFactory.createAddToCartRequestMachineName(source.getMachineName()));
			destination.setSessionId(objectFactory.createAddToCartRequestSessionId(source.getSessionId()));
			destination.setClient(objectFactory.createCheckOutEndRequestClient(source.getClient()));

		}

		return destination;
	}

}
