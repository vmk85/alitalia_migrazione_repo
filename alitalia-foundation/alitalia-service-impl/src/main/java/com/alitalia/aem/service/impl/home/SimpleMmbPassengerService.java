package com.alitalia.aem.service.impl.home;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesRequest;
import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesResponse;
import com.alitalia.aem.service.api.home.MmbPassengerService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.mmbpassengerservice.MmbCheckFrequentFlyerCodesRequestToCheckFFCodeRequest;
import com.alitalia.aem.ws.mmb.passengerservice.MmbPassengerServiceClient;
import com.alitalia.aem.ws.mmb.passengerservice.xsd1.CheckFFCodeRequest;
import com.alitalia.aem.ws.mmb.passengerservice.xsd1.CheckFFCodeResponse;

@Component(immediate=true, metatype=false)
@Service
public class SimpleMmbPassengerService implements MmbPassengerService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleMmbPassengerService.class);

	@Reference
	private MmbPassengerServiceClient mmbPassengerServiceClient;

	@Reference
	private MmbCheckFrequentFlyerCodesRequestToCheckFFCodeRequest mmbCheckFrequentFlyerCodesRequestConverter;

	@Override
	public MmbCheckFrequentFlyerCodesResponse checkFrequentFlyerCodes(MmbCheckFrequentFlyerCodesRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method checkFrequentFlyerCodes(). The request is {}", request);
		}
		

		try {
			MmbCheckFrequentFlyerCodesResponse response = new MmbCheckFrequentFlyerCodesResponse();
			CheckFFCodeRequest serviceRequest = mmbCheckFrequentFlyerCodesRequestConverter.convert(request);
			CheckFFCodeResponse serviceResponse = mmbPassengerServiceClient.checkFFCode(serviceRequest);
			if (serviceResponse != null &&
					serviceResponse.getWrongFfPassengerIds() != null &&
					serviceResponse.getWrongFfPassengerIds().getValue() != null &&
					serviceResponse.getWrongFfPassengerIds().getValue().getInt() != null &&
					!serviceResponse.getWrongFfPassengerIds().getValue().getInt().isEmpty()) {
				List<String> validFFCodes = new ArrayList<String>(); 
				for (Integer indexValidFFCode : serviceResponse.getWrongFfPassengerIds().getValue().getInt()) {
					validFFCodes.add(request.getFfCodesToCheck().get(indexValidFFCode).get(0));
				}
				response.setInvalidFFCodes(validFFCodes);
			}

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully checkFrequentFlyerCodes(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method checkFrequentFlyerCodes().", e);
			throw new AlitaliaServiceException("Exception executing checkFrequentFlyerCodes: Sid ["+request.getSid()+"]" , e);
		}
	}

}
