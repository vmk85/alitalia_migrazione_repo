package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import java.util.Map;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ArcoQueueData;
import com.alitalia.aem.common.data.home.PaymentComunicationBancaIntesaData;
import com.alitalia.aem.common.data.home.PaymentComunicationBancoPostaData;
import com.alitalia.aem.common.data.home.PaymentComunicationBaseData;
import com.alitalia.aem.common.data.home.PaymentComunicationCreditCardData;
import com.alitalia.aem.common.data.home.PaymentComunicationFindomesticData;
import com.alitalia.aem.common.data.home.PaymentComunicationGlobalCollectData;
import com.alitalia.aem.common.data.home.PaymentComunicationInfoData;
import com.alitalia.aem.common.data.home.PaymentComunicationLottomaticaData;
import com.alitalia.aem.common.data.home.PaymentComunicationMasterPassData;
import com.alitalia.aem.common.data.home.PaymentComunicationPayLaterData;
import com.alitalia.aem.common.data.home.PaymentComunicationPayPalData;
import com.alitalia.aem.common.data.home.PaymentComunicationPosteIDData;
import com.alitalia.aem.common.data.home.PaymentComunicationUnicreditData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.AComunicationBase;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.AComunicationInfo;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.BancoPostaType;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ComunicationBancaIntesa;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ComunicationBancoPosta;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ComunicationCreditCard;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ComunicationFindomestic;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ComunicationGlobalCollect;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ComunicationLottomatica;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ComunicationMasterPass;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ComunicationPayLater;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ComunicationPayPal;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ComunicationPosteID;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ComunicationUnicredit;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd5.ArrayOfanyType;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd5.ArrayOfstring;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd9.ArrayOfKeyValuePairOfstringstring;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd9.KeyValuePairOfstringstring;

@Component(immediate=true, metatype=false)
@Service(value=PaymentComunicationBaseDataToAComunicationBase.class)
public class PaymentComunicationBaseDataToAComunicationBase 
		implements Converter<PaymentComunicationBaseData, AComunicationBase> {
	
	@Reference
	private ArcoQueueDataToArcoQueue arcoQueueDataToArcoQueueConverter;
	
	@Override
	public AComunicationBase convert(PaymentComunicationBaseData source) {
		
		AComunicationBase destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			com.alitalia.aem.ws.booking.infopassengerservice.xsd5.ObjectFactory xsd5ObjectFactory = 
					new com.alitalia.aem.ws.booking.infopassengerservice.xsd5.ObjectFactory();
			
			com.alitalia.aem.ws.booking.infopassengerservice.xsd9.ObjectFactory xsd9ObjectFactory = 
					new com.alitalia.aem.ws.booking.infopassengerservice.xsd9.ObjectFactory();
			
			if (source instanceof PaymentComunicationBancaIntesaData) {
				PaymentComunicationBancaIntesaData typedSource = (PaymentComunicationBancaIntesaData) source;
				ComunicationBancaIntesa typedDestination = new ComunicationBancaIntesa();
				destination = typedDestination;
				
				typedDestination.setErrorUrl(
						objectFactory.createAComunicationInfoErrorUrl(
								typedSource.getErrorUrl()));
				
				typedDestination.setCancelUrl(
						objectFactory.createComunicationBancaIntesaCancelUrl(
								typedSource.getCancelUrl()));
				
				typedDestination.setShopperId(
						objectFactory.createComunicationBancaIntesaShopperId(
								typedSource.getShopperId()));
				
				ArrayOfKeyValuePairOfstringstring requestData = 
						xsd9ObjectFactory.createArrayOfKeyValuePairOfstringstring();
				if (typedSource.getRequestData() != null) {
					for (Map.Entry<String, String> entry : typedSource.getRequestData().entrySet()) {
						KeyValuePairOfstringstring requestDataEntry = 
								xsd9ObjectFactory.createKeyValuePairOfstringstring();
						requestDataEntry.setKey(entry.getKey());
						requestDataEntry.setValue(entry.getValue());
						requestData.getKeyValuePairOfstringstring().add(requestDataEntry);
					}
				}
				typedDestination.setRequestData(
						objectFactory.createComunicationBancaIntesaRequestData(
								requestData));
				
			} else if (source instanceof PaymentComunicationBancoPostaData) {
				PaymentComunicationBancoPostaData typedSource = (PaymentComunicationBancoPostaData) source;
				ComunicationBancoPosta typedDestination = new ComunicationBancoPosta();
				destination = typedDestination;
				
				typedDestination.setErrorUrl(
						objectFactory.createAComunicationInfoErrorUrl(
								typedSource.getErrorUrl()));
				
				if (typedSource.getType() != null) {
					typedDestination.setType(
							BancoPostaType.fromValue(typedSource.getType().value()));
				}
				
			} else if (source instanceof PaymentComunicationPosteIDData) {
				PaymentComunicationPosteIDData typedSource = (PaymentComunicationPosteIDData) source;
				ComunicationPosteID typedDestination = new ComunicationPosteID();
				destination = typedDestination;
				
				typedDestination.setErrorUrl(
						objectFactory.createAComunicationInfoErrorUrl(
								typedSource.getErrorUrl()));
				
			} else if (source instanceof PaymentComunicationCreditCardData) {
				PaymentComunicationCreditCardData typedSource = (PaymentComunicationCreditCardData) source;
				ComunicationCreditCard typedDestination = new ComunicationCreditCard();
				destination = typedDestination;
				
				typedDestination.setErrorUrl(
						objectFactory.createAComunicationInfoErrorUrl(
								typedSource.getErrorUrl()));
				
				typedDestination.setAcceptHeader(
						objectFactory.createComunicationCreditCardAcceptHeader(
								typedSource.getAcceptHeader()));
				
				typedDestination.setIPAddress(
						objectFactory.createComunicationCreditCardIPAddress(
								typedSource.getIpAddress()));
				
				typedDestination.setUserAgent(
						objectFactory.createComunicationCreditCardUserAgent(
								typedSource.getUserAgent()));
				
			} else if (source instanceof PaymentComunicationFindomesticData) {
				PaymentComunicationFindomesticData typedSource = (PaymentComunicationFindomesticData) source;
				ComunicationFindomestic typedDestination = new ComunicationFindomestic();
				destination = typedDestination;
				
				typedDestination.setUrlCallBack(
						objectFactory.createComunicationFindomesticUrlCallBack(
								typedSource.getUrlCallBack()));

				typedDestination.setUrlRedirect(
						objectFactory.createComunicationFindomesticUrlRedirect(
								typedSource.getUrlRedirect()));
				
				ArrayOfKeyValuePairOfstringstring requestData = 
						xsd9ObjectFactory.createArrayOfKeyValuePairOfstringstring();
				if (typedSource.getRequestData() != null) {
					for (Map.Entry<String, String> entry : typedSource.getRequestData().entrySet()) {
						KeyValuePairOfstringstring requestDataEntry = 
								xsd9ObjectFactory.createKeyValuePairOfstringstring();
						requestDataEntry.setKey(entry.getKey());
						requestDataEntry.setValue(entry.getValue());
						requestData.getKeyValuePairOfstringstring().add(requestDataEntry);
					}
				}
				typedDestination.setRequestData(
						objectFactory.createComunicationFindomesticRequestData(
								requestData));
				
			} else if (source instanceof PaymentComunicationGlobalCollectData) {
				PaymentComunicationGlobalCollectData typedSource = (PaymentComunicationGlobalCollectData) source;
				ComunicationGlobalCollect typedDestination = new ComunicationGlobalCollect();
				destination = typedDestination;
				
				typedDestination.setCurrency(
						objectFactory.createComunicationGlobalCollectCurrency(
								typedSource.getCurrency()));
				
				typedDestination.setCustomerId(
						objectFactory.createComunicationGlobalCollectCustomerId(
								typedSource.getCustomerId()));
				
				typedDestination.setIPAddress(
						objectFactory.createComunicationGlobalCollectIPAddress(
								typedSource.getIpAddress()));
				
				typedDestination.setIsPost(
						typedSource.getIsPost());
				
				typedDestination.setOrderId(
						objectFactory.createComunicationGlobalCollectOrderId(
								typedSource.getOrderId()));
				
				ArrayOfKeyValuePairOfstringstring requestData = 
						xsd9ObjectFactory.createArrayOfKeyValuePairOfstringstring();
				if (typedSource.getRequestData() != null) {
					for (Map.Entry<String, String> entry : typedSource.getRequestData().entrySet()) {
						KeyValuePairOfstringstring requestDataEntry = 
								xsd9ObjectFactory.createKeyValuePairOfstringstring();
						requestDataEntry.setKey(entry.getKey());
						requestDataEntry.setValue(entry.getValue());
						requestData.getKeyValuePairOfstringstring().add(requestDataEntry);
					}
				}
				typedDestination.setRequestData(
						objectFactory.createComunicationGlobalCollectRequestData(
								requestData));
				
			} else if (source instanceof PaymentComunicationLottomaticaData) {
				ComunicationLottomatica typedDestination = new ComunicationLottomatica();
				destination = typedDestination; 
				
			} else if (source instanceof PaymentComunicationMasterPassData) {
				ComunicationMasterPass typedDestination = new ComunicationMasterPass();
				destination = typedDestination;	
				
			} else if (source instanceof PaymentComunicationPayLaterData) {
				PaymentComunicationPayLaterData typedSource = (PaymentComunicationPayLaterData) source;
				ComunicationPayLater typedDestination = new ComunicationPayLater();
				destination = typedDestination;	
				
				ArrayOfstring osis = xsd5ObjectFactory.createArrayOfstring();
				if (typedSource.getOsis() != null) {
					for (String osisItem : typedSource.getOsis()) {
						osis.getString().add(osisItem);
					}
				}
				typedDestination.setOsis(
						objectFactory.createComunicationPayLaterOsis(osis));
				
				ArrayOfstring remarks = xsd5ObjectFactory.createArrayOfstring();
				if (typedSource.getRemarks() != null) {
					for (String remark : typedSource.getRemarks()) {
						remarks.getString().add(remark);
					}
				}
				typedDestination.setRemarks(
						objectFactory.createComunicationPayLaterRemarks(remarks));
				
				ArrayOfanyType queues = xsd5ObjectFactory.createArrayOfanyType();
				if (typedSource.getQueues() != null) {
					for (ArcoQueueData queue : typedSource.getQueues()) {
						queues.getAnyType().add(
								arcoQueueDataToArcoQueueConverter.convert(
										queue));
					}
				}
				typedDestination.setQueues(
						objectFactory.createComunicationPayLaterQueues(queues));
				
			} else if (source instanceof PaymentComunicationPayPalData) {
				PaymentComunicationPayPalData typedSource = (PaymentComunicationPayPalData) source;
				ComunicationPayPal typedDestination = new ComunicationPayPal();
				destination = typedDestination;	
				
				typedDestination.setCancelUrl(
						objectFactory.createComunicationPayPalCancelUrl(
								typedSource.getCancelUrl()));
				
				typedDestination.setShippingEnabled(
						typedSource.getShippingEnabled());
				
			} else if (source instanceof PaymentComunicationPosteIDData) {
				PaymentComunicationPosteIDData typedSource = (PaymentComunicationPosteIDData) source;
				ComunicationPosteID typedDestination = new ComunicationPosteID();
				destination = typedDestination;	
				
				typedDestination.setErrorUrl(
						objectFactory.createAComunicationInfoErrorUrl(
								typedSource.getErrorUrl()));
				
			} else if (source instanceof PaymentComunicationUnicreditData) {
				PaymentComunicationUnicreditData typedSource = (PaymentComunicationUnicreditData) source;
				ComunicationUnicredit typedDestination = new ComunicationUnicredit();
				destination = typedDestination;	
				
				typedDestination.setErrorUrl(
						objectFactory.createAComunicationInfoErrorUrl(
								typedSource.getErrorUrl()));
			
			} else if (source instanceof PaymentComunicationInfoData) {
				PaymentComunicationInfoData typedSource = (PaymentComunicationInfoData) source;
				AComunicationInfo typedDestination = new AComunicationInfo();
				destination = typedDestination;
				
				typedDestination.setErrorUrl(
						objectFactory.createAComunicationInfoErrorUrl(
								typedSource.getErrorUrl()));
				
			} else {
				destination = new AComunicationBase();
				
			}
			
			destination.setDescription(
					objectFactory.createAComunicationBaseDescription(
							source.getDescription()));
			
			destination.setLanguageCode(
					objectFactory.createAComunicationBaseLanguageCode(
							source.getLanguageCode()));
			
			destination.setReturnUrl(
					objectFactory.createAComunicationBaseReturnUrl(
							source.getReturnUrl()));
			
		}
				
		return destination;
	}

}
