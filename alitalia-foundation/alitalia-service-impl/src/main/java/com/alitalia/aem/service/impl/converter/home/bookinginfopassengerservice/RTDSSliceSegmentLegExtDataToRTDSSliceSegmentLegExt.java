package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentLegExtData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionSliceSegmentLegExt;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentLegExtDataToRTDSSliceSegmentLegExt.class)
public class RTDSSliceSegmentLegExtDataToRTDSSliceSegmentLegExt implements
		Converter<ResultTicketingDetailSolutionSliceSegmentLegExtData, 
					ResultTicketingDetailSolutionSliceSegmentLegExt> {

	@Reference
	private RTDSSliceSegmentLegExtWarningDataToRTDSSliceSegmentLegExtWarning rtdsWarningFieldConverter;

	@Override
	public ResultTicketingDetailSolutionSliceSegmentLegExt convert(
			ResultTicketingDetailSolutionSliceSegmentLegExtData source) {
		ResultTicketingDetailSolutionSliceSegmentLegExt destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionSliceSegmentLegExt();

			destination.setWarningField(rtdsWarningFieldConverter.convert(source.getWarningField()));
		}

		return destination;
	}

}
