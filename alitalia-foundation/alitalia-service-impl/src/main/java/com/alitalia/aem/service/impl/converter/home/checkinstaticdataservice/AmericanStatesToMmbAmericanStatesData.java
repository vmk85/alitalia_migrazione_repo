package com.alitalia.aem.service.impl.converter.home.checkinstaticdataservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.MmbAmericanStatesData;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd2.AmericanStates;


@Component(immediate=true, metatype=false)
@Service(value=AmericanStatesToMmbAmericanStatesData.class)
public class AmericanStatesToMmbAmericanStatesData implements Converter<AmericanStates, MmbAmericanStatesData> {

	@Override
	public MmbAmericanStatesData convert(AmericanStates source) {
		MmbAmericanStatesData destination = null;

		if (source != null) {
			destination = new MmbAmericanStatesData();

			destination.setCountry(source.getX003CCountryX003EKBackingField());
			destination.setCountryCode(source.getX003CCountryCodeX003EKBackingField());
			destination.setFullStateName(source.getX003CFullStateNameX003EKBackingField());
			destination.setId(source.getX003CIdX003EKBackingField());
			destination.setState(source.getX003CStateX003EKBackingField());
			destination.setStateCode(source.getX003CStateCodeX003EKBackingField());
		}

		return destination;
	}

}
