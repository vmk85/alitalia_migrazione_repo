package com.alitalia.aem.service.impl.converter.home.bookingbagsandrulesservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareCalculationData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareRestrictionData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingNoteData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingPenaltyPriceData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingRefundPriceData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingReissueInfoData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingTaxData;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetailsSolutionPricing;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetailsSolutionPricingBookingInfo;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetailsSolutionPricingFare;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetailsSolutionPricingFareCalculation;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetailsSolutionPricingFareRestriction;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetailsSolutionPricingNote;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetailsSolutionPricingPenaltyPrice;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetailsSolutionPricingRefundPrice;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetailsSolutionPricingReissueInfo;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetailsSolutionPricingTax;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingToPricingData.class)
public class RBDSPricingToPricingData implements Converter<ResultBookingDetailsSolutionPricing, ResultBookingDetailsSolutionPricingData> {

	@Reference
	private RBDSPricingAddCollectPriceToPricingAddCollectPriceData rbdsPricingAddCollectPriceConverter;
	
	@Reference
	private RBDSPricingBookingInfoToPricingBookingInfoData rbdsPricingBookingInfoConverter;
	 
	@Reference
	private RBDSPricingCocFareDifferenceToPricingCocFareDifferenceData rbdsPricingCocFareDifferenceConverter;
	
	@Reference
	private RBDSPricingCocFareTotalToPricingCocFareTotalData cocFareTotalFieldConverter;
	
	@Reference
	private RBDSPricingExtToPricingExtData extFieldConverter;
	
	@Reference
	private RBDSPricingFareCalculationToPricingFareCalculationDataConverter rbdsPricingFareCalculationConverter;
	
	@Reference
	private RBDSPricingFareToPricingFareData rbdsPricingFareFieldConverter;
	
	@Reference
	private RBDSPricingFareRestrictionToPricingFareRestrictionData rbdsPricingFareRestrictionConvert;
	
	@Reference
	private RBDSPricingNoteToPricingNoteData noteFieldConverter;
	
	@Reference
	private RBDSPricingPenaltyPriceToPricingPenaltyPriceData penaltyPriceFieldConverter;
	
	@Reference
	private RBDSPricingPreviousCocFareTotalToPricingPreviousCocFareTotalData previousCocFareTotalFieldConverter;
	
	@Reference
	private RBDSPricingRefundPriceToPricingRefundPriceData refundPriceConverter; 
	 
	@Reference
	private RBDSPricingReissueInfoToPricingReissueInfoData pricingReissueInfoConverter;
	
	@Reference
	private RBDSPricingReissueTaxTotalDifferenceToPricingReissueTaxTotalDifferenceData reissueTaxTotalDifferenceFieldConverter;
	
	@Reference
	private RBDSPricingSaleFareTotalToPricingSaleFareTotalData saleFareTotalFieldConverter;
	
	@Reference
	private RBDSPricingSalePriceToPricingSalePriceData salePriceFieldConverter;
	
	@Reference
	private RBDSPricingSaleTaxTotalToPricingSaleTaxTotalData saleTaxTotalFieldConverter;
	
	@Reference
	private RBDSPricingTaxToPricingTaxData taxFieldConverter;
	
	@Override
	public ResultBookingDetailsSolutionPricingData convert(ResultBookingDetailsSolutionPricing source) {
		ResultBookingDetailsSolutionPricingData destination = null;
		
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingData();
			
		    destination.setAddCollectPriceField(rbdsPricingAddCollectPriceConverter.convert(source.getAddCollectPriceField()));
		    List<ResultBookingDetailsSolutionPricingBookingInfoData> arrayOfRBDSPricingBookingInfo = new ArrayList<ResultBookingDetailsSolutionPricingBookingInfoData>();
		    if(source.getBookingInfoField()!=null)
		    for(ResultBookingDetailsSolutionPricingBookingInfo s : source.getBookingInfoField().getResultBookingDetailsSolutionPricingBookingInfo()){
		    	arrayOfRBDSPricingBookingInfo.add(rbdsPricingBookingInfoConverter.convert(s));
		    }
		    destination.setBookingInfoField(arrayOfRBDSPricingBookingInfo);
		    destination.setCocFareDifferenceField(rbdsPricingCocFareDifferenceConverter.convert(source.getCocFareDifferenceField()));
		    destination.setCocFareTotalField(cocFareTotalFieldConverter.convert(source.getCocFareTotalField()));
		    destination.setExtField(extFieldConverter.convert(source.getExtField()));
		    
		    List<ResultBookingDetailsSolutionPricingFareCalculationData> fareCalculationField = new ArrayList<ResultBookingDetailsSolutionPricingFareCalculationData>();
		    if(source.getFareCalculationField()!=null)
		    for(ResultBookingDetailsSolutionPricingFareCalculation s : source.getFareCalculationField().getResultBookingDetailsSolutionPricingFareCalculation())
		    	 fareCalculationField.add(rbdsPricingFareCalculationConverter.convert(s));
		    destination.setFareCalculationField(fareCalculationField);
		    
		    List<ResultBookingDetailsSolutionPricingFareData> fareField = new ArrayList<ResultBookingDetailsSolutionPricingFareData>();
		    if(source.getFareField()!=null)
		    for(ResultBookingDetailsSolutionPricingFare s : source.getFareField().getResultBookingDetailsSolutionPricingFare())
		    	fareField.add(rbdsPricingFareFieldConverter.convert(s));
		    destination.setFareField(fareField);
		    
		    List<ResultBookingDetailsSolutionPricingFareRestrictionData> fareRestrictionField = new ArrayList<ResultBookingDetailsSolutionPricingFareRestrictionData>();
		    if(source.getFareRestrictionField()!=null)
		    for(ResultBookingDetailsSolutionPricingFareRestriction s : source.getFareRestrictionField().getResultBookingDetailsSolutionPricingFareRestriction())
		    	fareRestrictionField.add(rbdsPricingFareRestrictionConvert.convert(s));
		    destination.setFareRestrictionField(fareRestrictionField);
		    
		    List<ResultBookingDetailsSolutionPricingNoteData> noteField = new ArrayList<ResultBookingDetailsSolutionPricingNoteData>();
		    if(source.getNoteField()!=null)
		    for(ResultBookingDetailsSolutionPricingNote s : source.getNoteField().getResultBookingDetailsSolutionPricingNote())
		    	noteField.add(noteFieldConverter.convert(s));
		    destination.setNoteField(noteField);
		    destination.setPaxCountField(source.getPaxCountField());
		    
		    List<ResultBookingDetailsSolutionPricingPenaltyPriceData> penaltyPriceField = new ArrayList<ResultBookingDetailsSolutionPricingPenaltyPriceData>();
		    if(source.getPenaltyPriceField()!=null)
		    for(ResultBookingDetailsSolutionPricingPenaltyPrice s : source.getPenaltyPriceField().getResultBookingDetailsSolutionPricingPenaltyPrice())
		    	penaltyPriceField.add(penaltyPriceFieldConverter.convert(s));
		    destination.setPenaltyPriceField(penaltyPriceField);
		    
		    destination.setPreviousCocFareTotalField(previousCocFareTotalFieldConverter.convert(source.getPreviousCocFareTotalField()));
		    
		    List<ResultBookingDetailsSolutionPricingRefundPriceData> refundPriceField = new ArrayList<ResultBookingDetailsSolutionPricingRefundPriceData>();
		    if(source.getRefundPriceField()!=null)
		    for(ResultBookingDetailsSolutionPricingRefundPrice s : source.getRefundPriceField().getResultBookingDetailsSolutionPricingRefundPrice())
		    	refundPriceField.add(refundPriceConverter.convert(s));
		    destination.setRefundPriceField(refundPriceField);
		  
		    List<ResultBookingDetailsSolutionPricingReissueInfoData> reissueInfoField = new ArrayList<ResultBookingDetailsSolutionPricingReissueInfoData>();
		    if(source.getReissueInfoField()!=null)
		    for(ResultBookingDetailsSolutionPricingReissueInfo s : source.getReissueInfoField().getResultBookingDetailsSolutionPricingReissueInfo())
		    	reissueInfoField.add(pricingReissueInfoConverter.convert(s));
		    destination.setReissueInfoField(reissueInfoField);
		    
		    
		    destination.setReissueTaxTotalDifferenceField(reissueTaxTotalDifferenceFieldConverter.convert(source.getReissueTaxTotalDifferenceField()));
		    destination.setSaleFareTotalField(saleFareTotalFieldConverter.convert(source.getSaleFareTotalField()));
		    
		    destination.setSalePriceField(salePriceFieldConverter.convert(source.getSalePriceField()));
		    destination.setSaleTaxTotalField(saleTaxTotalFieldConverter.convert(source.getSaleTaxTotalField()));
		    
		    List<ResultBookingDetailsSolutionPricingTaxData> taxField = new ArrayList<ResultBookingDetailsSolutionPricingTaxData>();
		    if(source.getTaxField()!=null)
		    for(ResultBookingDetailsSolutionPricingTax s : source.getTaxField().getResultBookingDetailsSolutionPricingTax())
		    	taxField.add(taxFieldConverter.convert(s));
		    destination.setTaxField(taxField);
			
			
		}
		return destination;
		
	}

	public ResultBookingDetailsSolutionPricingData convert(com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetailsSolutionPricing source) {
		ResultBookingDetailsSolutionPricingData destination = null;
		
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingData();
			
		    destination.setAddCollectPriceField(rbdsPricingAddCollectPriceConverter.convert(source.getAddCollectPriceField()));
		    List<ResultBookingDetailsSolutionPricingBookingInfoData> arrayOfRBDSPricingBookingInfo = new ArrayList<ResultBookingDetailsSolutionPricingBookingInfoData>();
		    if(source.getBookingInfoField()!=null)
		    for(com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetailsSolutionPricingBookingInfo s : source.getBookingInfoField().getResultBookingDetailsSolutionPricingBookingInfo()){
		    	arrayOfRBDSPricingBookingInfo.add(rbdsPricingBookingInfoConverter.convert(s));
		    }
		    destination.setBookingInfoField(arrayOfRBDSPricingBookingInfo);
		    destination.setCocFareDifferenceField(rbdsPricingCocFareDifferenceConverter.convert(source.getCocFareDifferenceField()));
		    destination.setCocFareTotalField(cocFareTotalFieldConverter.convert(source.getCocFareTotalField()));
		    destination.setExtField(extFieldConverter.convert(source.getExtField()));
		    
		    List<ResultBookingDetailsSolutionPricingFareCalculationData> fareCalculationField = new ArrayList<ResultBookingDetailsSolutionPricingFareCalculationData>();
		    if(source.getFareCalculationField()!=null)
		    for(com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetailsSolutionPricingFareCalculation s : source.getFareCalculationField().getResultBookingDetailsSolutionPricingFareCalculation())
		    	 fareCalculationField.add(rbdsPricingFareCalculationConverter.convert(s));
		    destination.setFareCalculationField(fareCalculationField);
		    
		    List<ResultBookingDetailsSolutionPricingFareData> fareField = new ArrayList<ResultBookingDetailsSolutionPricingFareData>();
		    if(source.getFareField()!=null)
		    for(com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetailsSolutionPricingFare s : source.getFareField().getResultBookingDetailsSolutionPricingFare())
		    	fareField.add(rbdsPricingFareFieldConverter.convert(s));
		    destination.setFareField(fareField);
		    
		    List<ResultBookingDetailsSolutionPricingFareRestrictionData> fareRestrictionField = new ArrayList<ResultBookingDetailsSolutionPricingFareRestrictionData>();
		    if(source.getFareRestrictionField()!=null)
		    for(com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetailsSolutionPricingFareRestriction s : source.getFareRestrictionField().getResultBookingDetailsSolutionPricingFareRestriction())
		    	fareRestrictionField.add(rbdsPricingFareRestrictionConvert.convert(s));
		    destination.setFareRestrictionField(fareRestrictionField);
		    
		    List<ResultBookingDetailsSolutionPricingNoteData> noteField = new ArrayList<ResultBookingDetailsSolutionPricingNoteData>();
		    if(source.getNoteField()!=null)
		    for(com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetailsSolutionPricingNote s : source.getNoteField().getResultBookingDetailsSolutionPricingNote())
		    	noteField.add(noteFieldConverter.convert(s));
		    destination.setNoteField(noteField);
		    destination.setPaxCountField(source.getPaxCountField());
		    
		    List<ResultBookingDetailsSolutionPricingPenaltyPriceData> penaltyPriceField = new ArrayList<ResultBookingDetailsSolutionPricingPenaltyPriceData>();
		    if(source.getPenaltyPriceField()!=null)
		    for(com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetailsSolutionPricingPenaltyPrice s : source.getPenaltyPriceField().getResultBookingDetailsSolutionPricingPenaltyPrice())
		    	penaltyPriceField.add(penaltyPriceFieldConverter.convert(s));
		    destination.setPenaltyPriceField(penaltyPriceField);
		    
		    destination.setPreviousCocFareTotalField(previousCocFareTotalFieldConverter.convert(source.getPreviousCocFareTotalField()));
		    
		    List<ResultBookingDetailsSolutionPricingRefundPriceData> refundPriceField = new ArrayList<ResultBookingDetailsSolutionPricingRefundPriceData>();
		    if(source.getRefundPriceField()!=null)
		    for(com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetailsSolutionPricingRefundPrice s : source.getRefundPriceField().getResultBookingDetailsSolutionPricingRefundPrice())
		    	refundPriceField.add(refundPriceConverter.convert(s));
		    destination.setRefundPriceField(refundPriceField);
		  
		    List<ResultBookingDetailsSolutionPricingReissueInfoData> reissueInfoField = new ArrayList<ResultBookingDetailsSolutionPricingReissueInfoData>();
		    if(source.getReissueInfoField()!=null)
		    for(com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetailsSolutionPricingReissueInfo s : source.getReissueInfoField().getResultBookingDetailsSolutionPricingReissueInfo())
		    	reissueInfoField.add(pricingReissueInfoConverter.convert(s));
		    destination.setReissueInfoField(reissueInfoField);
		    
		    
		    destination.setReissueTaxTotalDifferenceField(reissueTaxTotalDifferenceFieldConverter.convert(source.getReissueTaxTotalDifferenceField()));
		    destination.setSaleFareTotalField(saleFareTotalFieldConverter.convert(source.getSaleFareTotalField()));
		    
		    destination.setSalePriceField(salePriceFieldConverter.convert(source.getSalePriceField()));
		    destination.setSaleTaxTotalField(saleTaxTotalFieldConverter.convert(source.getSaleTaxTotalField()));
		    
		    List<ResultBookingDetailsSolutionPricingTaxData> taxField = new ArrayList<ResultBookingDetailsSolutionPricingTaxData>();
		    if(source.getTaxField()!=null)
		    for(com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetailsSolutionPricingTax s : source.getTaxField().getResultBookingDetailsSolutionPricingTax())
		    	taxField.add(taxFieldConverter.convert(s));
		    destination.setTaxField(taxField);
			
			
		}
		return destination;
		
	}
	
}
