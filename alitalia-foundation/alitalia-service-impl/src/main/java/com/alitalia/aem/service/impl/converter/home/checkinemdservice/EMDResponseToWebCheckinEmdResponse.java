package com.alitalia.aem.service.impl.converter.home.checkinemdservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.EmdErrorData;
import com.alitalia.aem.common.data.home.checkin.EmdItemData;
import com.alitalia.aem.common.data.home.checkin.EmdWarningData;
import com.alitalia.aem.common.messages.home.WebCheckinEmdResponse;
import com.alitalia.aem.ws.checkin.emdservice.xsd1.EMDResponse;
import com.alitalia.aem.ws.checkin.emdservice.xsd2.EMDErrorType;
import com.alitalia.aem.ws.checkin.emdservice.xsd2.EMDItemType;
import com.alitalia.aem.ws.checkin.emdservice.xsd2.EMDWarningType;


@Component(immediate=true, metatype=false)
@Service(value=EMDResponseToWebCheckinEmdResponse.class)
public class EMDResponseToWebCheckinEmdResponse implements Converter<EMDResponse, WebCheckinEmdResponse> {

	@Reference
	private EMDWarningTypeToEmdWarningData warningTypeConverter;
	
	@Reference
	private EMDErrorTypeToEmdErrorData errorTypeConverter;
	
	@Reference
	private EMDItemTypeToEmdItemData itemTypeConverter;
	
	@Override
	public WebCheckinEmdResponse convert(EMDResponse source) {
		WebCheckinEmdResponse destination = null;

		if (source != null) {
			destination = new WebCheckinEmdResponse();
			
			destination.setSuccess(source.isSuccess());
			
			List<EmdErrorData> errors = new ArrayList<>();
			if (source.getErrors()!=null && source.getErrors().getValue()!=null 
					&& source.getErrors().getValue().getEMDErrorType()!=null &&
					source.getErrors().getValue().getEMDErrorType().size()>0){
				List<EMDErrorType> sourceErrors = source.getErrors().getValue().getEMDErrorType();
				for (EMDErrorType sourceError : sourceErrors) {
					errors.add(errorTypeConverter.convert(sourceError));
				}
			}
			destination.setErrors(errors);
			
			List<EmdItemData> items = new ArrayList<>();
			if (source.getItems()!=null && source.getItems().getValue()!=null 
					&& source.getItems().getValue().getEMDItemType()!=null &&
					source.getItems().getValue().getEMDItemType().size()>0){
				List<EMDItemType> sourceItems = source.getItems().getValue().getEMDItemType();
				for (EMDItemType sourceItem : sourceItems) {
					items.add(itemTypeConverter.convert(sourceItem));
				}
			}
			destination.setItems(items);
			
			List<EmdWarningData> warnings = new ArrayList<>();
			if (source.getWarnings()!=null && source.getWarnings().getValue()!=null 
					&& source.getWarnings().getValue().getEMDWarningType()!=null &&
					source.getWarnings().getValue().getEMDWarningType().size()>0){
				List<EMDWarningType> sourceWarnings = source.getWarnings().getValue().getEMDWarningType();
				for (EMDWarningType sourceWarning : sourceWarnings) {
					warnings.add(warningTypeConverter.convert(sourceWarning));
				}
			}
			destination.setWarnings(warnings);
		}

		return destination;
	}

}
