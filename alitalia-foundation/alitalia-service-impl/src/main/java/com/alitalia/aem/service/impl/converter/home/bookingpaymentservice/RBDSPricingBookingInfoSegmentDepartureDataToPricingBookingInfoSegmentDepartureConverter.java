package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentDepartureData;
import com.alitalia.aem.ws.booking.paymentservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd11.ResultBookingDetailsSolutionPricingBookingInfoSegmentDeparture;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingBookingInfoSegmentDepartureDataToPricingBookingInfoSegmentDepartureConverter.class)
public class RBDSPricingBookingInfoSegmentDepartureDataToPricingBookingInfoSegmentDepartureConverter
		implements Converter<ResultBookingDetailsSolutionPricingBookingInfoSegmentDepartureData, ResultBookingDetailsSolutionPricingBookingInfoSegmentDeparture> {

	@Override
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentDeparture convert(
			ResultBookingDetailsSolutionPricingBookingInfoSegmentDepartureData source) {
		ResultBookingDetailsSolutionPricingBookingInfoSegmentDeparture destination = null;
		
		if(source != null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionPricingBookingInfoSegmentDeparture();
			destination.setDateField(source.getDateField());
			destination.setTimeField(source.getTimeField());
		}
		
		return destination;
		
	}

}
