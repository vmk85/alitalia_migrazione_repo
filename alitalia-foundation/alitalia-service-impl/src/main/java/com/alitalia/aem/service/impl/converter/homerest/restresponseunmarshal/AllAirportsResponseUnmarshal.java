package com.alitalia.aem.service.impl.converter.homerest.restresponseunmarshal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.service.impl.converter.homerest.beans.GetAllAirports;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class AllAirportsResponseUnmarshal {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public GetAllAirports allAirportsUnmarshal(String jsonString) {
		
		GetAllAirports response = new GetAllAirports();
		Gson gson = new Gson();
		
		try {
			response = gson.fromJson(jsonString, GetAllAirports.class);
		}
		catch (JsonSyntaxException e) {
			e.printStackTrace();
			logger.debug("JsonSyntaxException in AllAirportsResponseUnmarshal");
		}
		
		return response;
	}

}
