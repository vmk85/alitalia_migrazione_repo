package com.alitalia.aem.service.impl.converter.home.bookingbagsandrulesservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareRestrictionLatestReturnData;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetailsSolutionPricingFareRestrictionLatestReturn;


@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingFareRestrictionLatestReturnToRBDSPFRestrictionLatestReturnData.class)
public class RBDSPricingFareRestrictionLatestReturnToRBDSPFRestrictionLatestReturnData
		implements Converter<ResultBookingDetailsSolutionPricingFareRestrictionLatestReturn, ResultBookingDetailsSolutionPricingFareRestrictionLatestReturnData> {

	@Override
	public ResultBookingDetailsSolutionPricingFareRestrictionLatestReturnData convert(
			ResultBookingDetailsSolutionPricingFareRestrictionLatestReturn source) {
		ResultBookingDetailsSolutionPricingFareRestrictionLatestReturnData destination = null;
		
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingFareRestrictionLatestReturnData();
			destination.setDateField(source.getDateField());
			destination.setTimeField(source.getTimeField());
		}
		
		return destination;
	}
	
	public ResultBookingDetailsSolutionPricingFareRestrictionLatestReturnData convert(
			com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetailsSolutionPricingFareRestrictionLatestReturn source) {
		ResultBookingDetailsSolutionPricingFareRestrictionLatestReturnData destination = null;
		
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingFareRestrictionLatestReturnData();
			destination.setDateField(source.getDateField());
			destination.setTimeField(source.getTimeField());
		}
		
		return destination;
	}

}
