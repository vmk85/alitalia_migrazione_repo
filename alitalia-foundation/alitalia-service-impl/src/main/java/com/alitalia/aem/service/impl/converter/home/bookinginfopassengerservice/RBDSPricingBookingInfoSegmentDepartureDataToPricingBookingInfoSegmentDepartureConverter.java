package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentDepartureData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ResultBookingDetailsSolutionPricingBookingInfoSegmentDeparture;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingBookingInfoSegmentDepartureDataToPricingBookingInfoSegmentDepartureConverter.class)
public class RBDSPricingBookingInfoSegmentDepartureDataToPricingBookingInfoSegmentDepartureConverter
		implements Converter<ResultBookingDetailsSolutionPricingBookingInfoSegmentDepartureData, ResultBookingDetailsSolutionPricingBookingInfoSegmentDeparture> {

	@Override
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentDeparture convert(
			ResultBookingDetailsSolutionPricingBookingInfoSegmentDepartureData source) {
		ResultBookingDetailsSolutionPricingBookingInfoSegmentDeparture destination = null;
		
		if(source != null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionPricingBookingInfoSegmentDeparture();
			destination.setDateField(source.getDateField());
			destination.setTimeField(source.getTimeField());
		}
		
		return destination;
		
	}

}
