package com.alitalia.aem.service.impl.converter.home.mmbsearchservice;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.mmb.searchservice.xsd4.Preferences;


@Component(immediate=true, metatype=false)
@Service(value=BookingMMBSearchServiceJAXBContextFactory.class)
public class BookingMMBSearchServiceJAXBContextFactory {

	private static final Logger logger = LoggerFactory.getLogger(BookingMMBSearchServiceJAXBContextFactory.class);

	private JAXBContext jaxbContext = null;

	public synchronized JAXBContext getJaxbContext() throws JAXBException {

		if (jaxbContext == null) {
			jaxbContext = JAXBContext.newInstance(Preferences.class);
		}

		return jaxbContext;
	}

	@Activate
	private void activate() {
		try {
			jaxbContext = JAXBContext.newInstance(Preferences.class);
		} catch (JAXBException e) {
			logger.warn("BookingSearchServiceJAXBContextFactory - cannot instantiate JAXBContext: {}", e.getMessage());
		}
	}
}
