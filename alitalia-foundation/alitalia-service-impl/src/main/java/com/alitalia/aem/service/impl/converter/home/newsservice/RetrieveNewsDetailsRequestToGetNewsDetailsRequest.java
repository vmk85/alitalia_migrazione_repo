package com.alitalia.aem.service.impl.converter.home.newsservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveNewsDetailsRequest;
import com.alitalia.aem.ws.news.newsservice.xsd2.GetNewsDetailsRequest;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveNewsDetailsRequestToGetNewsDetailsRequest.class)
public class RetrieveNewsDetailsRequestToGetNewsDetailsRequest implements Converter<RetrieveNewsDetailsRequest, GetNewsDetailsRequest> {

	@Override
	public GetNewsDetailsRequest convert(RetrieveNewsDetailsRequest source) {
		
		GetNewsDetailsRequest request = new GetNewsDetailsRequest();
		
		int id = source.getId();
		request.setId(id);
		
		return request;
	}
}