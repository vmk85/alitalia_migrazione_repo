package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.AdultPassenger;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ApplicantPassenger;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ChildPassenger;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.InfantPassenger;

@Component(immediate=true, metatype=false)
@Service(value=PassengerBaseToAPassengerBaseData.class)
public class PassengerBaseToAPassengerBaseData implements Converter<Object, PassengerBaseData> {

	private static final Logger logger = LoggerFactory.getLogger(PassengerBaseToAPassengerBaseData.class);

	@Reference
	private ApplicantPassengerToApplicantPassengerData applicantPassengerConverter;
	
	@Reference
	private AdultPassengerToAdultPassengerData adultPassengerConverter;
	
	@Reference
	private ChildPassengerToChildPassengerData childPassengerConverter;
	
	@Reference
	private InfantPassengerToInfantPassengerData infantPassengerConverter;
	
	@Reference
	private BookingTicketingServiceJAXBContextFactory jaxbContextFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public PassengerBaseData convert(Object source) {
		
		PassengerBaseData destination = null;
		
		if (source != null) {
			try {
				XPath xpath = XPathFactory.newInstance().newXPath();
				XPathExpression expr = xpath.compile("/*[local-name()='anyType']/@*[local-name()='type']");

				Unmarshaller applicantUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
				Unmarshaller adultUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
				Unmarshaller childUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
				Unmarshaller infantUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();

				String passengerInstanceType = (String) expr.evaluate((Node) source, XPathConstants.STRING);

				if ("ApplicantPassenger".equals(passengerInstanceType)) {
					ApplicantPassenger castedSource = (ApplicantPassenger) 
							((JAXBElement<ApplicantPassenger>) applicantUnmarshaller.unmarshal((Node) source)).getValue();
					destination = applicantPassengerConverter.convert(castedSource);
				
				} else if ("AdultPassenger".equals(passengerInstanceType)) {
					AdultPassenger castedSource = (AdultPassenger) 
							((JAXBElement<AdultPassenger>) adultUnmarshaller.unmarshal((Node) source)).getValue();
					destination = adultPassengerConverter.convert(castedSource);
				
				} else if ("ChildPassenger".equals(passengerInstanceType)) {
					ChildPassenger castedSource = (ChildPassenger) 
							((JAXBElement<ChildPassenger>) childUnmarshaller.unmarshal((Node) source)).getValue();
					destination = childPassengerConverter.convert(castedSource);
				
				} else if ("InfantPassenger".equals(passengerInstanceType)) {
					InfantPassenger castedSource = (InfantPassenger) 
							((JAXBElement<InfantPassenger>) infantUnmarshaller.unmarshal((Node) source)).getValue();
					destination = infantPassengerConverter.convert(castedSource);
				
				}

			} catch (JAXBException e) {
				logger.error("Error creating JAXBContext/Unmarshaller for a Passenger class: {}", e);
			} catch (XPathExpressionException e) {
				logger.error("Error extracting type attribute value: {}", e);
			}
			
		}
		
		return destination;
	}

}
