package com.alitalia.aem.service.impl.converter.home.commonservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveAirportDetailsRequest;
import com.alitalia.aem.ws.booking.commonservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.booking.commonservice.xsd1.AirportDetailRequest;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveAirportDetailRequestToAirportDetailRequest.class)
public class RetrieveAirportDetailRequestToAirportDetailRequest implements Converter<RetrieveAirportDetailsRequest, AirportDetailRequest> {

	@Override
	public AirportDetailRequest convert(RetrieveAirportDetailsRequest source) {
		AirportDetailRequest request = new AirportDetailRequest();
		
		ObjectFactory objectFactory = new ObjectFactory();
		String airportCode = source.getAirportCode();
		JAXBElement<String> jaxbAirportCode = objectFactory.createAirportDetailRequestAirportCode(airportCode);
		
		request.setAirportCode(jaxbAirportCode);
		
		return request;
	}
}