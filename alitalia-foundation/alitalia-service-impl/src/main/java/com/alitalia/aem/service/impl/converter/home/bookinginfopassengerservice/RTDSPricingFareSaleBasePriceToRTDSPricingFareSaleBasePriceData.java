package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareSaleBasePriceData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionPricingFareSaleBasePrice;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareSaleBasePriceToRTDSPricingFareSaleBasePriceData.class)
public class RTDSPricingFareSaleBasePriceToRTDSPricingFareSaleBasePriceData implements
		Converter<ResultTicketingDetailSolutionPricingFareSaleBasePrice, 
					ResultTicketingDetailSolutionPricingFareSaleBasePriceData> {

	@Override
	public ResultTicketingDetailSolutionPricingFareSaleBasePriceData convert(
			ResultTicketingDetailSolutionPricingFareSaleBasePrice source) {
		ResultTicketingDetailSolutionPricingFareSaleBasePriceData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingFareSaleBasePriceData();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
