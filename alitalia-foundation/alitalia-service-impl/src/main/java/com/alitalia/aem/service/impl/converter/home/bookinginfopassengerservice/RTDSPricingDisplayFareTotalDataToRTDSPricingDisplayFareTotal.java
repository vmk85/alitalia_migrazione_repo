package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingDisplayFareTotalData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionPricingDisplayFareTotal;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingDisplayFareTotalDataToRTDSPricingDisplayFareTotal.class)
public class RTDSPricingDisplayFareTotalDataToRTDSPricingDisplayFareTotal implements
		Converter<ResultTicketingDetailSolutionPricingDisplayFareTotalData, 
					ResultTicketingDetailSolutionPricingDisplayFareTotal> {

	@Override
	public ResultTicketingDetailSolutionPricingDisplayFareTotal convert(
			ResultTicketingDetailSolutionPricingDisplayFareTotalData source) {
		ResultTicketingDetailSolutionPricingDisplayFareTotal destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingDisplayFareTotal();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
