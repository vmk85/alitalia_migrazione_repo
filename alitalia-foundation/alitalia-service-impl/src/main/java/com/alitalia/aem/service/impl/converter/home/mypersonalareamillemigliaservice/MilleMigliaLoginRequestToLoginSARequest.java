package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.MilleMigliaLoginRequest;
import com.alitalia.aem.common.messages.home.MilleMigliaLoginSARequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.LoginRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.LoginSARequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

@Component(immediate = true, metatype = false)
@Service(value = MilleMigliaLoginRequestToLoginSARequest.class)
public class MilleMigliaLoginRequestToLoginSARequest
		implements Converter<MilleMigliaLoginSARequest, LoginSARequest> {

	@Override
	public LoginSARequest convert(MilleMigliaLoginSARequest source) {
		LoginSARequest destination = null;

		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createLoginSARequest();

			destination.setCodiceMM(
					objectFactory.createLoginSARequestCodiceMM(
							source.getCodiceMM()
					)
			);
			destination.setUserName(
					objectFactory.createLoginSARequestUserName(
							source.getUsername()
					)
			);
			destination.setPassword(
					objectFactory.createLoginSARequestPassword(
							source.getPassword()
					)
			);

		}

		return destination;
	}

}
