package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentLegOperationalFlightData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionSliceSegmentLegOperationalFlight;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentLegOperationalFlightToRTDSSliceSegmentLegOperationalFlightData.class)
public class RTDSSliceSegmentLegOperationalFlightToRTDSSliceSegmentLegOperationalFlightData implements
		Converter<ResultTicketingDetailSolutionSliceSegmentLegOperationalFlight, ResultTicketingDetailSolutionSliceSegmentLegOperationalFlightData> {

	@Override
	public ResultTicketingDetailSolutionSliceSegmentLegOperationalFlightData convert(
			ResultTicketingDetailSolutionSliceSegmentLegOperationalFlight source) {
		ResultTicketingDetailSolutionSliceSegmentLegOperationalFlightData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionSliceSegmentLegOperationalFlightData();

			destination.setCarrierField(source.getCarrierField());
			destination.setNumberField(source.getNumberField());
			destination.setNumberFieldSpecified(source.isNumberFieldSpecified());
		}

		return destination;
	}

}
