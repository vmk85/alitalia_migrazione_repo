/**
 * 
 */
package com.alitalia.aem.service.impl.converter.home.checkinservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.messages.home.WebCheckinRequest;
import com.alitalia.aem.ws.checkin.checkinservice.xsd1.CheckInRequest;
import com.alitalia.aem.ws.checkin.checkinservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.checkinservice.xsd1.Resource;
import com.alitalia.aem.ws.checkin.checkinservice.xsd2.ArrayOfPassenger;

@Component(immediate=true, metatype=false)
@Service(value=WebCheckinRequestToCheckInRequest.class)
public class WebCheckinRequestToCheckInRequest implements Converter<WebCheckinRequest, CheckInRequest> {

	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;
	
	@Reference
	private CheckinRouteDataToRoute checkinRouteDataConverter;
	
	@Reference
	private CheckinPassengerDataToPassenger checkinPassengerConverter;

	@Override
	public CheckInRequest convert(WebCheckinRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		CheckInRequest destination = null;

		if (source != null) {
			
			destination = objectFactory.createCheckInRequest();
			destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			destination.setX003CResourceX003EKBackingField(Resource.WEB);
			
			com.alitalia.aem.ws.checkin.checkinservice.xsd2.ObjectFactory objectFactory2 = new com.alitalia.aem.ws.checkin.checkinservice.xsd2.ObjectFactory();
			JAXBElement<ArrayOfPassenger> passengersList = objectFactory.createCheckInRequestPassengers(objectFactory2.createArrayOfPassenger());
			if (source.getPassengersList()!=null){
				for (CheckinPassengerData passengerData : source.getPassengersList()) {
					passengersList.getValue().getPassenger().add(checkinPassengerConverter.convert(passengerData));
				}
			}
			destination.setPassengers(passengersList);
			destination.setPnr(objectFactory.createCheckInRequestPnr(source.getPnr()));
			destination.setSelectedRoute(objectFactory.createCheckInRequestSelectedRoute(checkinRouteDataConverter.convert(source.getSelectedRoute())));

		}
		return destination;
	}
}
