package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareBookingInfoData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareEndorsementData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareSurchargeData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ArrayOfresultTicketingDetailSolutionPricingFareBookingInfo;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ArrayOfresultTicketingDetailSolutionPricingFareEndorsement;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ArrayOfresultTicketingDetailSolutionPricingFareSurcharge;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionPricingFare;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd5.ArrayOfstring;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareDataToRTDSPricingFare.class)
public class RTDSPricingFareDataToRTDSPricingFare implements
		Converter<ResultTicketingDetailSolutionPricingFareData,
					ResultTicketingDetailSolutionPricingFare> {

	@Reference
	private RTDSPricingFareAdjustedPriceDataToRTDSPricingFareAdjustedPrice rtdsAdjustedPriceFieldConverter;

	@Reference
	private RTDSPricingFareBasePriceDataToRTDSPricingFareBasePrice rtdsBasePriceFieldConverter;

	@Reference
	private RTDSPricingFareBookingInfoDataToRTDSPricingFareBookingInfo rtdsBookingInfoFieldConverter;

	@Reference
	private RTDSPricingFareDisplayBasePriceDataToRTDSPricingFareDisplayBasePrice rtdsDisplayBasePriceFieldConverter;

	@Reference
	private RTDSPricingFareEndorsementDataToRTDSPricingFareEndorsement rdtsEndorsementFieldConverter;

	@Reference
	private RTDSPricingFareSaleAdjustedPriceDataToRTDSPricingFareSaleAdjustedPrice rtdsSaleAdjustedPriceFieldConverter;

	@Reference
	private RTDSPricingFareSaleBasePriceDataToRTDSPricingFareSaleBasePrice rtdsSaleBasePriceFieldConverter;

	@Reference
	private RTDSPricingFareSurchargeDataToRTDSPricingFareSurcharge rtdsSurchargeFieldConverter;

	@Override
	public ResultTicketingDetailSolutionPricingFare convert(ResultTicketingDetailSolutionPricingFareData source) {
		ResultTicketingDetailSolutionPricingFare destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			com.alitalia.aem.ws.booking.infopassengerservice.xsd5.ObjectFactory objectFactory5 = 
					new com.alitalia.aem.ws.booking.infopassengerservice.xsd5.ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingFare();

			destination.setAdjustedPriceField(
					rtdsAdjustedPriceFieldConverter.convert(source.getAdjustedPriceField()));

			destination.setBasePriceField(
					rtdsBasePriceFieldConverter.convert(source.getBasePriceField()));

			ArrayOfresultTicketingDetailSolutionPricingFareBookingInfo bookingInfoField = null;
			if (source.getBookingInfoField() != null &&
					!source.getBookingInfoField().isEmpty()) {
				bookingInfoField = objectFactory.createArrayOfresultTicketingDetailSolutionPricingFareBookingInfo();
				for(ResultTicketingDetailSolutionPricingFareBookingInfoData sourceBookingInfoFieldElem : 
						source.getBookingInfoField())
					bookingInfoField.getResultTicketingDetailSolutionPricingFareBookingInfo().add(
							rtdsBookingInfoFieldConverter.convert(sourceBookingInfoFieldElem));
			}
			destination.setBookingInfoField(bookingInfoField);

			destination.setCarrierField(source.getCarrierField());
			destination.setCity1Field(source.getCity1Field());
			destination.setCity2Field(source.getCity2Field());
			destination.setDestinationCityField(source.getDestinationCityField());

			destination.setDisplayBasePriceField(
					rtdsDisplayBasePriceFieldConverter.convert(source.getDisplayBasePriceField()));

			ArrayOfresultTicketingDetailSolutionPricingFareEndorsement endorsementField = null;
			if (source.getEndorsementField() != null &&
					!source.getEndorsementField().isEmpty()) {
				endorsementField = objectFactory.createArrayOfresultTicketingDetailSolutionPricingFareEndorsement();
				for (ResultTicketingDetailSolutionPricingFareEndorsementData sourceEndorsementFieldElem : source.getEndorsementField())
					endorsementField.getResultTicketingDetailSolutionPricingFareEndorsement().add(
							rdtsEndorsementFieldConverter.convert(sourceEndorsementFieldElem));
			}
			destination.setEndorsementField(endorsementField);

			destination.setExtendedFareCodeField(source.getExtendedFareCodeField());
			destination.setGlobalIndicatorField(source.getGlobalIndicatorField());
			destination.setOriginCityField(source.getOriginCityField());

			ArrayOfstring ptcField = null;
			if (source.getPtcField() != null &&
					!source.getPtcField().isEmpty()) {
				ptcField = objectFactory5.createArrayOfstring();
				for(String sourcePtcFieldElem : source.getPtcField())
					ptcField.getString().add(sourcePtcFieldElem);
			}
			destination.setPtcField(ptcField);

			destination.setRkeyField(source.getRkeyField());

			destination.setSaleAdjustedPriceField(
					rtdsSaleAdjustedPriceFieldConverter.convert(source.getSaleAdjustedPriceField()));

			destination.setSaleBasePriceField(
					rtdsSaleBasePriceFieldConverter.convert(source.getSaleBasePriceField()));

			ArrayOfresultTicketingDetailSolutionPricingFareSurcharge surchargeField = null;
			if (source.getSurchargeField() != null &&
					!source.getSurchargeField().isEmpty()) {
				surchargeField = objectFactory.createArrayOfresultTicketingDetailSolutionPricingFareSurcharge();
				for (ResultTicketingDetailSolutionPricingFareSurchargeData sourceSurchargeFieldElem : source.getSurchargeField())
					surchargeField.getResultTicketingDetailSolutionPricingFareSurcharge().add(
							rtdsSurchargeFieldConverter.convert(sourceSurchargeFieldElem));
			}
			destination.setSurchargeField(surchargeField);

			destination.setTagField(source.getTagField());
			destination.setTicketDesignatorField(source.getTicketDesignatorField());
		}

		return destination;
	}
	

}
