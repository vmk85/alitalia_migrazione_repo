package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareCalculationData;
import com.alitalia.aem.ws.booking.paymentservice.xsd11.ArrayOfresultBookingDetailsSolutionPricingFareCalculationBoxedTax;
import com.alitalia.aem.ws.booking.paymentservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd11.ResultBookingDetailsSolutionPricingFareCalculation;
import com.alitalia.aem.ws.booking.paymentservice.xsd5.ArrayOfstring;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingFareCalculationDataToPricingFareCalculationConverter.class)
public class RBDSPricingFareCalculationDataToPricingFareCalculationConverter
		implements Converter<ResultBookingDetailsSolutionPricingFareCalculationData, ResultBookingDetailsSolutionPricingFareCalculation> {

	@Reference
	RBDSPricingFareCalculationBoxedTaxDataToPricingFareCalculationBoxedTax pricingFareCalculationBoxedTaxConverter;
	
	
	@Override
	public ResultBookingDetailsSolutionPricingFareCalculation convert(
			ResultBookingDetailsSolutionPricingFareCalculationData source) {
		ResultBookingDetailsSolutionPricingFareCalculation destination = null;
		if(source != null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionPricingFareCalculation();
			
			
			ArrayOfresultBookingDetailsSolutionPricingFareCalculationBoxedTax boxedTaxField = objfact.createArrayOfresultBookingDetailsSolutionPricingFareCalculationBoxedTax();
			
			if(source.getBoxedTaxField()!=null)
			for(ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData s : source.getBoxedTaxField()){
				boxedTaxField.getResultBookingDetailsSolutionPricingFareCalculationBoxedTax().add(pricingFareCalculationBoxedTaxConverter.convert(s));
			}
			destination.setBoxedTaxField(boxedTaxField);
			
			com.alitalia.aem.ws.booking.paymentservice.xsd5.ObjectFactory obj = new com.alitalia.aem.ws.booking.paymentservice.xsd5.ObjectFactory();
			ArrayOfstring lineField = obj.createArrayOfstring();
			
			if(source.getLineField()!=null)
			for(String s : source.getLineField())
				lineField.getString().add(s);
			destination.setLineField(lineField);
		}
		return destination;
	}

}
