package com.alitalia.aem.service.impl.converter.home.commonservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.SeatMapData;
import com.alitalia.aem.common.data.home.SeatMapSectorData;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.ws.booking.commonservice.xsd7.SeatMap;
import com.alitalia.aem.ws.booking.commonservice.xsd7.SeatMapSector;

@Component(immediate=true, metatype=false)
@Service(value=SeatMapToSeatMapData.class)
public class SeatMapToSeatMapData 
		implements Converter<SeatMap, SeatMapData> {

	@Reference
	private SeatMapSectorToSeatMapSectorData seatMapSectorConverter;
	
	@Override
	public SeatMapData convert(SeatMap source) {
		
		SeatMapData destination = null;
		
		if (source != null) {
		
			destination = new SeatMapData();
			
			if (source.getX003CCabinTypeX003EKBackingField() != null) {
				destination.setCabinType(
						CabinEnum.fromValue(source.getX003CCabinTypeX003EKBackingField().value()));
			}
			
			destination.setStartingRow(
					source.getX003CStartingRowX003EKBackingField());
			
			destination.setEndingRow(
					source.getX003CEndingRowX003EKBackingField());
			
			List<SeatMapSectorData> seatMapSectors = new ArrayList<SeatMapSectorData>();
			if (source.getX003CSeatMapSectorsX003EKBackingField() != null &&
					source.getX003CSeatMapSectorsX003EKBackingField().getSeatMapSector() != null) {
				for (SeatMapSector seatMapSector : source.getX003CSeatMapSectorsX003EKBackingField().getSeatMapSector()) {
					seatMapSectors.add(seatMapSectorConverter.convert(seatMapSector));
				}
			}
			destination.setSeatMapSectors(seatMapSectors);
			
		}
		
		return destination;
		
	}

}
