package com.alitalia.aem.service.impl.converter.home.staticdataservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.TicketOfficeData;
import com.alitalia.aem.common.messages.home.RetrieveTicketOfficeResponse;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.StaticDataResponse;
import com.alitalia.aem.ws.booking.staticdataservice.xsd5.TicketOffice;

@Component(immediate=true, metatype=false)
@Service(value=StaticDataResponseToRetrieveTicketOfficeResponseConverter.class)
public class StaticDataResponseToRetrieveTicketOfficeResponseConverter implements Converter<StaticDataResponse, RetrieveTicketOfficeResponse> {

	private static final Logger logger = LoggerFactory.getLogger(StaticDataResponseToRetrieveTicketOfficeResponseConverter.class);

	@Reference
	private TicketOfficeToTicketOfficeData ticketOfficeCoverter;

	@Reference
	private StaticDataServiceJAXBContextFactory jaxbContextFactory;

	@SuppressWarnings("unchecked")
	@Override
	public RetrieveTicketOfficeResponse convert(StaticDataResponse source) {
		RetrieveTicketOfficeResponse destination = new RetrieveTicketOfficeResponse();
		try {
			List<TicketOfficeData> ticketsOfficeData = new ArrayList<>();
			
			Unmarshaller seatTypeUnmashaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			JAXBElement<TicketOffice> unmarshalledObj = null;

			List<Object> ticketsOffice = source.getEntities().getValue().getAnyType();
			for(Object ticketOffice: ticketsOffice){
				unmarshalledObj = (JAXBElement<TicketOffice>) seatTypeUnmashaller.unmarshal((Node) ticketOffice);
				TicketOffice castedTicketOffice = unmarshalledObj.getValue();
				TicketOfficeData ticketOfficeData = ticketOfficeCoverter.convert(castedTicketOffice);
				ticketsOfficeData.add(ticketOfficeData);
			}
			
			destination.setTicketOfficeData(ticketsOfficeData);
			
		} catch (JAXBException e) {
			logger.error("Error instantiating JAXBContext/Unmarshaller: {}", e);
		} finally {
			logger.info("Conversion executed successfully");
		}
		return destination;			
	}
}