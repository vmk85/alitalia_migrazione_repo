/**
 * 
 */
package com.alitalia.aem.service.impl.converter.home.mmbsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationRequest;
import com.alitalia.aem.ws.mmb.searchservice.xsd1.GetRoutesRequestNew;
import com.alitalia.aem.ws.mmb.searchservice.xsd1.ObjectFactory;

/**
 * @author R.Capitini
 *
 */
@Component(immediate=true, metatype=false)
@Service(value=RetriveMmbPnrInformationRequestToGetRoutesRequestNew.class)
public class RetriveMmbPnrInformationRequestToGetRoutesRequestNew implements Converter<RetriveMmbPnrInformationRequest, GetRoutesRequestNew> {

	@Reference
	private MmbRequestBaseInfoDataToRequestBaseInfo mmbRequestBaseInfoDataConverter;

	@Override
	public GetRoutesRequestNew convert(RetriveMmbPnrInformationRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		GetRoutesRequestNew destination = null;

		if (source != null) {
			destination = objectFactory.createGetRoutesRequestNew();

			destination.setLastName(objectFactory.createGetRoutesRequestNewLastName(source.getLastName()));
			destination.setLocale(objectFactory.createGetRoutesRequestNewLocale(source.getLocale()));
			destination.setName(objectFactory.createGetRoutesRequestNewName(source.getName()));
			destination.setTripID(objectFactory.createGetRoutesRequestNewTripID(source.getTripId()));

			// Non viene valorizzato, e' gia' presente il TripId che puo' essere il codice MM del cliente,
			// il PNR o il numero di biglietto
			destination.setX003CPNRX003EKBackingField(null);

			destination.setX003CInfoRequestX003EKBackingField(
					mmbRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
		}

		return destination;
	}

}
