package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.RetrieveFeaturedNewsRequest;
import com.alitalia.aem.common.messages.home.RetrieveFeaturedNewsResponse;
import com.alitalia.aem.common.messages.home.RetrieveNewsCategoriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveNewsCategoriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveNewsDetailsRequest;
import com.alitalia.aem.common.messages.home.RetrieveNewsDetailsResponse;
import com.alitalia.aem.common.messages.home.RetrieveNewsRequest;
import com.alitalia.aem.common.messages.home.RetrieveNewsResponse;
import com.alitalia.aem.service.api.home.RetrieveNewsService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.newsservice.GetCategoriesResponseToRetrieveNewsCategoriesResponse;
import com.alitalia.aem.service.impl.converter.home.newsservice.GetFeaturedNewsResponseToRetrieveFeaturedNewsResponse;
import com.alitalia.aem.service.impl.converter.home.newsservice.GetNewsDetailsResponseToRetrieveNewsDetailsResponse;
import com.alitalia.aem.service.impl.converter.home.newsservice.RetrieveFeaturedNewsRequestToGetFeaturedNewsRequest;
import com.alitalia.aem.service.impl.converter.home.newsservice.RetrieveNewsCategoriesRequestToGetCategoriesRequest;
import com.alitalia.aem.service.impl.converter.home.newsservice.RetrieveNewsDetailsRequestToGetNewsDetailsRequest;
import com.alitalia.aem.service.impl.converter.home.newsservice.RetrieveNewsRequestToSearchNewsRequest;
import com.alitalia.aem.service.impl.converter.home.newsservice.SearchNewsResponseToRetrieveNewsResponse;
import com.alitalia.aem.ws.news.newsservice.NewsServiceClient;
import com.alitalia.aem.ws.news.newsservice.xsd2.GetCategoriesRequest;
import com.alitalia.aem.ws.news.newsservice.xsd2.GetCategoriesResponse;
import com.alitalia.aem.ws.news.newsservice.xsd2.GetFeaturedNewsRequest;
import com.alitalia.aem.ws.news.newsservice.xsd2.GetFeaturedNewsResponse;
import com.alitalia.aem.ws.news.newsservice.xsd2.GetNewsDetailsRequest;
import com.alitalia.aem.ws.news.newsservice.xsd2.GetNewsDetailsResponse;
import com.alitalia.aem.ws.news.newsservice.xsd2.SearchNewsRequest;
import com.alitalia.aem.ws.news.newsservice.xsd2.SearchNewsResponse;

@Service
@Component(immediate = true, metatype = false)
public class SimpleRetrieveNewsService implements RetrieveNewsService {

	private static final Logger logger = LoggerFactory.getLogger(RetrieveNewsService.class);

	@Reference
	private NewsServiceClient newsServiceClientClient;

	@Reference
	private RetrieveNewsRequestToSearchNewsRequest retrieveNewsRequestConverter;

	@Reference
	private SearchNewsResponseToRetrieveNewsResponse searchNewsResponseConverter;

	@Reference
	private RetrieveFeaturedNewsRequestToGetFeaturedNewsRequest retrieveFeaturedNewsRequestConverter;

	@Reference
	private GetFeaturedNewsResponseToRetrieveFeaturedNewsResponse getFeaturedNewsResponseConverter;

	@Reference
	private RetrieveNewsDetailsRequestToGetNewsDetailsRequest retrieveNewsDetailsRequestConverter;

	@Reference
	private GetNewsDetailsResponseToRetrieveNewsDetailsResponse getNewsDetailsResponseConverter;
	
	@Reference
	private RetrieveNewsCategoriesRequestToGetCategoriesRequest retrieveNewsCategoriesRequestConverter;

	@Reference
	private GetCategoriesResponseToRetrieveNewsCategoriesResponse getCategoriesResponseConverter;

	@Override
	public RetrieveNewsResponse searchNews(RetrieveNewsRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method searchNews. The request is {}", request);
		}
		

		try {

			RetrieveNewsResponse response = new RetrieveNewsResponse();
			SearchNewsRequest searchNewsRequest = retrieveNewsRequestConverter.convert(request);
			SearchNewsResponse searchNewsResponse = newsServiceClientClient.searchNews(searchNewsRequest);
			response = searchNewsResponseConverter.convert(searchNewsResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
			
			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing searchNews: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public RetrieveFeaturedNewsResponse searchFeaturedNews(RetrieveFeaturedNewsRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method. The request is {}", request);
		}
		

		try {

			RetrieveFeaturedNewsResponse response = new RetrieveFeaturedNewsResponse();
			GetFeaturedNewsRequest getFeaturedNewsRequest = retrieveFeaturedNewsRequestConverter.convert(request);
			GetFeaturedNewsResponse getFeaturedNewsResponse = newsServiceClientClient.getFeaturedNews(getFeaturedNewsRequest);
			response = getFeaturedNewsResponseConverter.convert(getFeaturedNewsResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
			
			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing searchFeaturedNews: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public RetrieveNewsDetailsResponse searchNewsDetails(RetrieveNewsDetailsRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method searchNewsDetails. The request is {}", request);
		}
		

		try {

			RetrieveNewsDetailsResponse response = new RetrieveNewsDetailsResponse();
			GetNewsDetailsRequest getNewsDetailsRequest = retrieveNewsDetailsRequestConverter.convert(request);
			GetNewsDetailsResponse getNewsDetailsResponse = newsServiceClientClient.getNewsDetails(getNewsDetailsRequest);
			response = getNewsDetailsResponseConverter.convert(getNewsDetailsResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
			
			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing searchNewsDetails: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public RetrieveNewsCategoriesResponse searchNewsCategories(RetrieveNewsCategoriesRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method searchNewsCategories. The request is {}", request);
		}
		

		try {

			RetrieveNewsCategoriesResponse response = new RetrieveNewsCategoriesResponse();
			GetCategoriesRequest getCategoriesRequest = retrieveNewsCategoriesRequestConverter.convert(request);
			GetCategoriesResponse getCategoriesResponse = newsServiceClientClient.getCategories(getCategoriesRequest);
			response = getCategoriesResponseConverter.convert(getCategoriesResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
			
			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing searchNewsCategories: Sid ["+request.getSid()+"]" , e);
		}
	}
}