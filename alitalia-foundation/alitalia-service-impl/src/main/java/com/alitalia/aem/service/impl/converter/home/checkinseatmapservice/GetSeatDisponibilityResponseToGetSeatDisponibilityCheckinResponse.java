package com.alitalia.aem.service.impl.converter.home.checkinseatmapservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinKeyValueOfCompartimentalClassData;
import com.alitalia.aem.common.messages.home.GetSeatDisponibilityCheckinResponse;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd1.GetSeatDisponibilityResponse;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd4.ArrayOfKeyValueOfCompartimentalClassint0HzHfQkm;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd4.ArrayOfKeyValueOfCompartimentalClassint0HzHfQkm.KeyValueOfCompartimentalClassint0HzHfQkm;

@Component(immediate=true, metatype=false)
@Service(value=GetSeatDisponibilityResponseToGetSeatDisponibilityCheckinResponse.class)
public class GetSeatDisponibilityResponseToGetSeatDisponibilityCheckinResponse implements Converter<GetSeatDisponibilityResponse, GetSeatDisponibilityCheckinResponse> {
	

	@Reference
	private KeyValueOfCompartimentalClassToCheckinKeyValueOfCompartimentalClassData keyValueOfCompartimentalClassConverter;


	@Override
	public GetSeatDisponibilityCheckinResponse convert(GetSeatDisponibilityResponse source) {
		GetSeatDisponibilityCheckinResponse destination = null;

		if (source != null) {
			destination = new GetSeatDisponibilityCheckinResponse();
			
			JAXBElement<ArrayOfKeyValueOfCompartimentalClassint0HzHfQkm> sourceDisponibilities = source.getDisponibilities();
			if(sourceDisponibilities != null &&
					sourceDisponibilities.getValue() != null &&
					sourceDisponibilities.getValue().getKeyValueOfCompartimentalClassint0HzHfQkm() != null &&
					!sourceDisponibilities.getValue().getKeyValueOfCompartimentalClassint0HzHfQkm().isEmpty()){
				List<CheckinKeyValueOfCompartimentalClassData> seatDisponibilityList = new ArrayList<CheckinKeyValueOfCompartimentalClassData>();
				for(KeyValueOfCompartimentalClassint0HzHfQkm sourceDisponibilityItem : sourceDisponibilities.getValue().getKeyValueOfCompartimentalClassint0HzHfQkm()){
					seatDisponibilityList.add(keyValueOfCompartimentalClassConverter.convert(sourceDisponibilityItem));
				}
				destination.setKeyValueOfCompartimentalClassint0HzHfQkm(seatDisponibilityList);
			}
		}

		return destination;
	}

}
