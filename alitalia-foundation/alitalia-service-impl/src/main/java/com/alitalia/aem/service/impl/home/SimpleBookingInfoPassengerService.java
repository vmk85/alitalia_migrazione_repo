package com.alitalia.aem.service.impl.home;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import com.alitalia.aem.ws.booking.infopassengerservice.SabreDcInfoPassengerServiceClient;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd1.StatisticsResponse;
import com.day.cq.wcm.api.Page;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.data.home.FrequentFlyerTypeData;
import com.alitalia.aem.common.messages.home.InfoPassengerCheckFrequentFlyersCodeRequest;
import com.alitalia.aem.common.messages.home.InfoPassengerCheckFrequentFlyersCodeResponse;
import com.alitalia.aem.common.messages.home.InfoPassengerCreatePNRRequest;
import com.alitalia.aem.common.messages.home.InfoPassengerCreatePNRResponse;
import com.alitalia.aem.common.messages.home.InfoPassengerSubmitPassengerRequest;
import com.alitalia.aem.common.messages.home.InfoPassengerSubmitPassengerResponse;
import com.alitalia.aem.service.api.home.BookingInfoPassengerService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice.CheckFrequentFlyersCodeRequestConverter;
import com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice.CheckFrequentFlyersCodeResponseConverter;
import com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice.CreatePNRResponseToInfoPassengerCreatePNRResponse;
import com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice.InfoPassengerCreatePNRRequestToCreatePNRRequest;
import com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice.InfoPassengerSubmitPassengerRequestToPassengerSubmitRequest;
import com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice.InfoPassengerSubmitPassengerResponseConverter;
import com.alitalia.aem.ws.booking.infopassengerservice.InfoPassengerServiceClient;
import com.alitalia.aem.ws.booking.infopassengerservice.wsdl.IInfoPassengerServiceSubmitPassengerDetailsServiceFaultFaultFaultMessage;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd1.CheckFrequentFlyersCodeRequest;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd1.CheckFrequentFlyersCodeResponse;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd1.CreatePNRRequest;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd1.CreatePNRResponse;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd1.PassengerSubmitRequest;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd1.PassengerSubmitResponse;

@Service
@Component(immediate=true, metatype=false)
public class SimpleBookingInfoPassengerService implements BookingInfoPassengerService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleBookingInfoPassengerService.class);
	
	@Reference
	private InfoPassengerServiceClient infoPassengerServiceClient;
	
	@Reference
	private InfoPassengerCreatePNRRequestToCreatePNRRequest createPNRRequestConverter;
	
	@Reference
	private CreatePNRResponseToInfoPassengerCreatePNRResponse createPNRResponseConverter;
	
	@Reference
	private CheckFrequentFlyersCodeRequestConverter checkFrequentFlyersCodeRequestConverter;
	
	@Reference
	private CheckFrequentFlyersCodeResponseConverter checkFrequentFlyersCodeResponseConverter;
	
	@Reference
	private InfoPassengerSubmitPassengerRequestToPassengerSubmitRequest submitPassengerRequestConverter;
	
	@Reference
	private InfoPassengerSubmitPassengerResponseConverter submitPassengerResponseConverter;

	/** Migrazione 3.6 inizio*/

	@Reference
	private ResourceResolverFactory resolverFactory;

	@Reference
	private SabreDcInfoPassengerServiceClient sabreDcInfoPassengerServiceClient;

	/** Migrazione 3.6 fine*/
	
	@Override
	public InfoPassengerCreatePNRResponse executeCreatePNR(InfoPassengerCreatePNRRequest request) {
		
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method executeCreatePNR. The request is {}", request);
		}
		
		
		try {
			
			InfoPassengerCreatePNRResponse response = new InfoPassengerCreatePNRResponse();
			long time_before_req = System.currentTimeMillis();
			CreatePNRRequest createPNRRequest = createPNRRequestConverter.convert(request);
			long time_after_req = System.currentTimeMillis();

			/** Migrazione 3.6 inizio*/

			CreatePNRResponse createPNRResponse = null;
			if (newSabreEnable(createPNRRequest.getMarketCode().getValue().toLowerCase())){
				createPNRResponse = sabreDcInfoPassengerServiceClient.createPNR(createPNRRequest);
			} else {
				createPNRResponse = infoPassengerServiceClient.createPNR(createPNRRequest);
			}

			/** Migrazione 3.6 fine*/

//			CreatePNRResponse createPNRResponse = infoPassengerServiceClient.createPNR(createPNRRequest);
			long time_before_resp = System.currentTimeMillis();
			response = createPNRResponseConverter.convert(createPNRResponse);
			long time_after_resp = System.currentTimeMillis();
			logger.info("ExecuteCreatePNR: request converter execution time: {}. Response converter execution time: {}", (time_after_req - time_before_req), (time_after_resp - time_before_resp));
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			return response;
			
		} catch (Exception e) {
			logger.error("Exception while executing method executeCreatePNR(): {}", e);
			throw new AlitaliaServiceException("Exception executing executeCreatePNR: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public InfoPassengerCheckFrequentFlyersCodeResponse executeCheckFrequentFlyersCode(
			InfoPassengerCheckFrequentFlyersCodeRequest request) {
		
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method executeCheckFrequentFlyersCode. The request is {}", request);
		}
		
		
		try {
			
			InfoPassengerCheckFrequentFlyersCodeResponse response = new InfoPassengerCheckFrequentFlyersCodeResponse();
			long time_before_req = System.currentTimeMillis();
			CheckFrequentFlyersCodeRequest checkFFCodeRequest = 
					checkFrequentFlyersCodeRequestConverter.convert(request);
			long time_after_req = System.currentTimeMillis();
			CheckFrequentFlyersCodeResponse checkFFCodeResponse = 
					infoPassengerServiceClient.checkFrequentFlyersCode(checkFFCodeRequest);
			long time_before_resp = System.currentTimeMillis();
			response = checkFrequentFlyersCodeResponseConverter.convert(checkFFCodeResponse);
			long time_after_resp = System.currentTimeMillis();
			logger.info("ExecuteCheckFrequentFlyersCode: request converter execution time: {}. Response converter execution time: {}", (time_after_req - time_before_req), (time_after_resp - time_before_resp));
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			return response;
			
		} catch (Exception e) {
			logger.error("Exception while executing method executeCheckFrequentFlyersCode(): {}", e);
			throw new AlitaliaServiceException("Exception executing executeCheckFrequentFlyersCode: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public InfoPassengerSubmitPassengerResponse submitPassengerDetails(InfoPassengerSubmitPassengerRequest request){
		
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method submitPassengerDetails. The request is {}", request);
		}
		InfoPassengerSubmitPassengerResponse response = new InfoPassengerSubmitPassengerResponse();
		try {
			
			
			long time_before_req = System.currentTimeMillis();
			PassengerSubmitRequest passengerSubmitRequest = submitPassengerRequestConverter.convert(request);
			long time_after_req = System.currentTimeMillis();

			/** Migrazione 3.6 inizio*/

			PassengerSubmitResponse passengerSubmitResponse = null;
			if (newSabreEnable(request.getMarket().toLowerCase())){
				passengerSubmitResponse = sabreDcInfoPassengerServiceClient.submitPassengerDetails(passengerSubmitRequest);
			} else {
				passengerSubmitResponse = infoPassengerServiceClient.submitPassengerDetails(passengerSubmitRequest);
			}

			/** Migrazione 3.6 fine*/

//			PassengerSubmitResponse passengerSubmitResponse = infoPassengerServiceClient.submitPassengerDetails(passengerSubmitRequest, request.getMarket());
			long time_before_resp = System.currentTimeMillis();
			response = submitPassengerResponseConverter.convert(passengerSubmitResponse);
			long time_after_resp = System.currentTimeMillis();
			logger.info("ExecuteSubmitPassengerDetails: request converter execution time: {}. Response converter execution time: {}", (time_after_req - time_before_req), (time_after_resp - time_before_resp));
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			return response;
		} catch (IInfoPassengerServiceSubmitPassengerDetailsServiceFaultFaultFaultMessage e1){
			
			com.alitalia.aem.ws.booking.infopassengerservice.xsd5.ArrayOfKeyValueOfstringstring messageDictionaries = e1.getFaultInfo().getRemoteSystemMessageDictionary().getValue();
			
			
			if (messageDictionaries.getKeyValueOfstringstring() != null){
				for (com.alitalia.aem.ws.booking.infopassengerservice.xsd5.ArrayOfKeyValueOfstringstring.KeyValueOfstringstring message : messageDictionaries.getKeyValueOfstringstring()){
					
					
					
					
					if (message.getValue()!=null && message.getValue().contains("ERR.SSW.CLIENT.INVALID_REQUEST")){
						
//						if (message.getValue().contains("validation.frequentFlyer.airline.invalid") || message.getValue().contains("validation.text.pattern")){
//						} POSSIBILI ERRORI SPECIFICI PER SINGOLO PASSEGGERO IN CASO DI : "ERR.SSW.CLIENT.INVALID_REQUEST"
						
						List<AdultPassengerData> adultpassengersWithWrongFrequentFlyer = new ArrayList<>();
						for (int i = 0; i < request.getPassengers().size(); i++) {
							
							String errorCodeCardFrequentFlyer = "";
							
							String strPassenger = "passengers[" + i + "]";
														
							AdultPassengerData adultPassengerData = new AdultPassengerData();
							FrequentFlyerTypeData frequentFlyerTypeData = new FrequentFlyerTypeData();
							
							if (message.getValue().contains(strPassenger)) {
								errorCodeCardFrequentFlyer = "VALIDATION.FREQUENTFLYER.INVALID";
								frequentFlyerTypeData.setErrorCodeCardFrequentFlyer(errorCodeCardFrequentFlyer);
								adultPassengerData.setFrequentFlyerType(frequentFlyerTypeData);
								adultpassengersWithWrongFrequentFlyer.add(adultPassengerData);
								
							}else{
								frequentFlyerTypeData.setErrorCodeCardFrequentFlyer(errorCodeCardFrequentFlyer);
								adultPassengerData.setFrequentFlyerType(frequentFlyerTypeData);
								adultpassengersWithWrongFrequentFlyer.add(adultPassengerData);
							}
							
						}						
						response.setAdultPassengersWithWrongFrequentFlyer(adultpassengersWithWrongFrequentFlyer);
						return response;
					}
				}
			} 
			logger.error("Exception1 while executing method executeBrandSearch(): {}", e1);
			throw new AlitaliaServiceException("Exception1 executing executeBrandSearch: Sid ["+request.getSid()+"]" , e1);
		}catch (Exception e) {
			logger.error("Exception while executing method submitPassengerDetails(): {}", e);
			throw new AlitaliaServiceException("Exception executing submitPassengerDetails: Sid ["+request.getSid()+"]" , e);
		}
	}

	/** Migrazione 3.6 inizio*/

	private boolean newSabreEnable(String market) throws LoginException {

		ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
		Resource res = resourceResolver.getResource("/content/alitalia/alitalia-" + market);
		Page page = res.adaptTo(Page.class);
		ValueMap props = page.getProperties();
		String title = props.get("isNewService", "default value");
		logger.info("dato preso dalla pagina: " + title);
		return Boolean.parseBoolean(title);

	}

	/** Migrazione 3.6 fine*/

}