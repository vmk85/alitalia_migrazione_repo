package com.alitalia.aem.service.impl.converter.home.checkinseatmapservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.GetSeatDisponibilityCheckinRequest;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd1.GetSeatDisponibilityRequest;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd1.Resource;


@Component(immediate=true, metatype=false)
@Service(value=GetSeatDisponibilityCheckinRequestToGetSeatDisponibilityRequest.class)
public class GetSeatDisponibilityCheckinRequestToGetSeatDisponibilityRequest implements Converter<GetSeatDisponibilityCheckinRequest, GetSeatDisponibilityRequest> {
	
	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;


	@Override
	public GetSeatDisponibilityRequest convert(GetSeatDisponibilityCheckinRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		GetSeatDisponibilityRequest destination = null;

		if (source != null) {
			destination = objectFactory.createGetSeatDisponibilityRequest();

			destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			destination.setX003CResourceX003EKBackingField(Resource.WEB);
			
			destination.setDepartureDate(XsdConvertUtils.toXMLGregorianCalendar(source.getDepartureDate()));
			destination.setFlightCarrier(objectFactory.createGetSeatDisponibilityRequestFlightCarrier(source.getFlightCarrier()));
			destination.setFlightNumber(objectFactory.createGetSeatDisponibilityRequestFlightNumber(source.getFlightNumber()));
			destination.setFrom(objectFactory.createGetSeatDisponibilityRequestFrom(source.getFrom()));
			destination.setTo(objectFactory.createGetSeatDisponibilityRequestTo(source.getTo()));
			
		}
		return destination;
	}

}
