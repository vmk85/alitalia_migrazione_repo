package com.alitalia.aem.service.impl.converter.home.mmbancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryCartItemData;
import com.alitalia.aem.common.messages.home.MmbAncillaryUpdateCartRequest;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.UpdateCartRequest;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.ArrayOfUpdateCartItem;

@Component(immediate=true, metatype=false)
@Service(value=MmbAncillaryUpdateCartRequestToUpdateCartRequest.class)
public class MmbAncillaryUpdateCartRequestToUpdateCartRequest implements
		Converter<MmbAncillaryUpdateCartRequest, UpdateCartRequest> {

	@Reference
	private MmbRequestBaseInfoDataToRequestBaseInfo mmbRequestBaseInfoDataConverter;

	@Reference
	private MmbAncillaryCartItemDataToUpdateCartItem mmbAncillaryCartItemDataConverter;

	@Override
	public UpdateCartRequest convert(MmbAncillaryUpdateCartRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.ObjectFactory objectFactory2 =
				new com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.ObjectFactory();
		UpdateCartRequest destination = null;

		if (source != null) {
			destination = objectFactory.createUpdateCartRequest();

			destination.setCartId(source.getCartId());
			destination.setClient(source.getClient());
			destination.setMachineName(objectFactory.createAddToCartRequestMachineName(source.getMachineName()));
			destination.setSessionId(objectFactory.createAddToCartRequestSessionId(source.getSessionId()));
			destination.setX003CPNRX003EKBackingField(null);

			destination.setX003CInfoRequestX003EKBackingField(mmbRequestBaseInfoDataConverter.convert(source.getBaseInfo()));

			ArrayOfUpdateCartItem cartItems = objectFactory2.createArrayOfUpdateCartItem();
			for(MmbAncillaryCartItemData sourceItem: source.getItemsToUpdate()) {
				cartItems.getUpdateCartItem().add(mmbAncillaryCartItemDataConverter.convert(sourceItem));
			}
			destination.setItems(objectFactory.createUpdateCartRequestItems(cartItems));
		}

		return destination;
	}

}
