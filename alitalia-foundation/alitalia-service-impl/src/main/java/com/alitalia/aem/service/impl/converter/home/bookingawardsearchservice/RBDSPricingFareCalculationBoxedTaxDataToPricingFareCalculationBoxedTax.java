package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetailsSolutionPricingFareCalculationBoxedTax;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingFareCalculationBoxedTaxDataToPricingFareCalculationBoxedTax.class)
public class RBDSPricingFareCalculationBoxedTaxDataToPricingFareCalculationBoxedTax
		implements Converter<ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData, ResultBookingDetailsSolutionPricingFareCalculationBoxedTax> {

	@Override
	public ResultBookingDetailsSolutionPricingFareCalculationBoxedTax convert(
			ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData source) {
		ResultBookingDetailsSolutionPricingFareCalculationBoxedTax destination = null;
		
		if(source!=null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionPricingFareCalculationBoxedTax();
			destination.setPriceField(source.getPriceField());
			destination.setTaxCodeField(source.getTaxCodeField());
		}
		return destination;
	}

}
