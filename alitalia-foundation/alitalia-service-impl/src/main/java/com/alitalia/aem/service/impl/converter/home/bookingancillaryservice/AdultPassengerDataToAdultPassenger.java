package com.alitalia.aem.service.impl.converter.home.bookingancillaryservice;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.data.home.TicketInfoData;
import com.alitalia.aem.ws.booking.ancillaryservice.xsd2.AdultPassenger;
import com.alitalia.aem.ws.booking.ancillaryservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.booking.ancillaryservice.xsd2.PassengerType;
import com.alitalia.aem.ws.booking.ancillaryservice.xsd3.ArrayOfanyType;

@Component(immediate=true, metatype=false)
@Service(value=AdultPassengerDataToAdultPassenger.class)
public class AdultPassengerDataToAdultPassenger 
		implements Converter<AdultPassengerData, AdultPassenger> {
	
	@Reference
	private PassengerInfoBaseDataToInfoBase infoBaseDataToInfoBaseConverter;
	
	@Reference
	private PreferencesDataToPreferences preferencesDataToPreferencesConverter;
	
	@Reference
	private FrequentFlyerTypeDataToFrequentFlyerType frequentFlyerTypeDataToFrequentFlyerTypeConverter;
	
	@Reference
	private TicketInfoDataToTicketInfo ticketInfoDataToTicketInfoConverter;
	
	@Override
	public AdultPassenger convert(AdultPassengerData source) {
		
		AdultPassenger destination = null;
		
		if (source != null) {
			
			ObjectFactory factory2 = new ObjectFactory();
			com.alitalia.aem.ws.booking.ancillaryservice.xsd3.ObjectFactory factory3 = 
					new com.alitalia.aem.ws.booking.ancillaryservice.xsd3.ObjectFactory();
			
			destination = factory2.createAdultPassenger();
			
			// AdultPassenger data
			
			destination.setFrequentFlyerCode(
					factory2.createAdultPassengerFrequentFlyerCode(
							source.getFrequentFlyerCode()));
			
			destination.setFrequentFlyerTier(
					factory2.createAdultPassengerFrequentFlyerTier(
							source.getFrequentFlyerTier()));
			
			destination.setFrequentFlyerType(
					factory2.createAdultPassengerFrequentFlyerType(
							frequentFlyerTypeDataToFrequentFlyerTypeConverter.convert(
								source.getFrequentFlyerType())));
			
			// ARegularPassenger data
			
			destination.setPreferences(
					factory2.createARegularPassengerPreferences(
							preferencesDataToPreferencesConverter.convert(
									source.getPreferences())));
			
			// APassengerBase data
			
			destination.setLastName(
					factory2.createAPassengerBaseLastName(
							source.getLastName()));
			
			destination.setName(
					factory2.createAPassengerBaseName(
							source.getName()));
			
			destination.setCouponPrice(
					source.getCouponPrice());
			
			destination.setExtraCharge(
					source.getExtraCharge());
			
			destination.setFee(
					source.getFee());
			
			destination.setGrossFare(
					source.getGrossFare());
			
			destination.setNetFare(
					source.getNetFare());
			
			destination.setInfo(
					factory2.createAPassengerBaseInfo(
							infoBaseDataToInfoBaseConverter.convert(
									source.getInfo())));
			
			List<TicketInfoData> sourceTickets = source.getTickets();
			if (sourceTickets != null && !source.getTickets().isEmpty()) {
				ArrayOfanyType tickets = factory3.createArrayOfanyType();
				for (TicketInfoData ticketInfoData : sourceTickets) {
					tickets.getAnyType().add(ticketInfoDataToTicketInfoConverter.convert(ticketInfoData));
				}
				destination.setTickets(factory2.createAPassengerBaseTickets(tickets));
			}
			destination.setType(PassengerType.fromValue(source.getType().value()));
		
		}
		
		return destination;
	}

}
