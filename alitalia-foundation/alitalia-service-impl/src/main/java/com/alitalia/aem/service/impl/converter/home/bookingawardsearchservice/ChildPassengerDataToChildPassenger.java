package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ChildPassengerData;
import com.alitalia.aem.common.data.home.TicketInfoData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.ChildPassenger;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.ObjectFactory;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.PassengerType;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd6.ArrayOfanyType;

@Component(immediate=true, metatype=false)
@Service(value=ChildPassengerDataToChildPassenger.class)
public class ChildPassengerDataToChildPassenger 
		implements Converter<ChildPassengerData, ChildPassenger> {
	
	@Reference
	private PassengerInfoBaseDataToInfoBase infoBaseDataToInfoBaseConverter;
	
	@Reference
	private PreferencesDataToPreferences preferencesDataToPreferencesConverter;
	
	@Reference
	private TicketInfoDataToTicketInfo ticketInfoDataToTicketInfoConverter;
	
	@Override
	public ChildPassenger convert(ChildPassengerData source) {
		
		ChildPassenger destination = null;
		
		if (source != null) {
			
			ObjectFactory factory3 = new ObjectFactory();
			com.alitalia.aem.ws.bookingaward.searchservice.xsd6.ObjectFactory factory6 = 
					new com.alitalia.aem.ws.bookingaward.searchservice.xsd6.ObjectFactory();
			
			destination = factory3.createChildPassenger();
			
			// ARegularPassenger data
			
			destination.setPreferences(
					factory3.createARegularPassengerPreferences(
							preferencesDataToPreferencesConverter.convert(source.getPreferences())));
			
			// APassengerBase data
			
			destination.setLastName(factory3.createAPassengerBaseLastName(source.getLastName()));
			
			destination.setName(factory3.createAPassengerBaseName(source.getName()));
			
			destination.setCouponPrice(source.getCouponPrice());
			
			destination.setExtraCharge(source.getExtraCharge());
			
			destination.setFee(source.getFee());
			
			destination.setGrossFare(source.getGrossFare());
			
			destination.setNetFare(source.getNetFare());
			
			destination.setInfo(factory3.createAPassengerBaseInfo(
							infoBaseDataToInfoBaseConverter.convert(source.getInfo())));
			
			List<TicketInfoData> sourceTickets = source.getTickets();
			if (sourceTickets != null && !source.getTickets().isEmpty()) {
				ArrayOfanyType tickets = factory6.createArrayOfanyType();
				for (TicketInfoData ticketInfoData : sourceTickets) {
					tickets.getAnyType().add(ticketInfoDataToTicketInfoConverter.convert(ticketInfoData));
				}
				destination.setTickets(factory3.createAPassengerBaseTickets(tickets));
			}
			destination.setType(PassengerType.fromValue(source.getType().value()));
		
		}
		
		return destination;
	}

}
