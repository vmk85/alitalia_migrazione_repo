package com.alitalia.aem.service.impl.converter.home.mmbcommonservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.MmbFrequentFlyerTypeData;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.FrequentFlyerType;

@Component(immediate=true, metatype=false)
@Service(value=FrequentFlyerTypeToMmbFrequentFlyerTypeData.class)
public class FrequentFlyerTypeToMmbFrequentFlyerTypeData implements Converter<FrequentFlyerType, MmbFrequentFlyerTypeData> {

	@Override
	public MmbFrequentFlyerTypeData convert(FrequentFlyerType source) {
		MmbFrequentFlyerTypeData destination = null;

		if (source != null) {
			destination = new MmbFrequentFlyerTypeData();

			destination.setCode(source.getX003CCodeX003EKBackingField());
			destination.setDescription(source.getX003CDescriptionX003EKBackingField());
			destination.setId(source.getX003CIdX003EKBackingField());
			destination.setLenght(source.getX003CLenghtX003EKBackingField());
			destination.setPreFillChar(source.getX003CPreFillCharX003EKBackingField());
			destination.setRegularExpressionValidation(source.getX003CRegularExpressionValidationX003EKBackingField());
		}

		return destination;
	}

}
