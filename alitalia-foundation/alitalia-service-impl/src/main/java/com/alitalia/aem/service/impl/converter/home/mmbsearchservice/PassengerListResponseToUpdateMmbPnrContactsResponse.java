package com.alitalia.aem.service.impl.converter.home.mmbsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrContactsResponse;
import com.alitalia.aem.ws.mmb.searchservice.xsd1.PassengerListResponse;

@Component(immediate=true, metatype=false)
@Service(value=PassengerListResponseToUpdateMmbPnrContactsResponse.class)
public class PassengerListResponseToUpdateMmbPnrContactsResponse implements Converter<PassengerListResponse, UpdateMmbPnrContactsResponse> {

	@Override
	public UpdateMmbPnrContactsResponse convert(PassengerListResponse source) {
		UpdateMmbPnrContactsResponse destination = null;

		if (source != null) {
			destination = new UpdateMmbPnrContactsResponse();

			destination.setSuccessful(source.isIsUpdated());
		}

		return destination;
	}

}
