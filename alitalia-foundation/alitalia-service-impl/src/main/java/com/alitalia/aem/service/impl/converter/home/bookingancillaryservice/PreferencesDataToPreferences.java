package com.alitalia.aem.service.impl.converter.home.bookingancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PreferencesData;
import com.alitalia.aem.common.data.home.SeatPreferencesData;
import com.alitalia.aem.ws.booking.ancillaryservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.booking.ancillaryservice.xsd2.Preferences;
import com.alitalia.aem.ws.booking.ancillaryservice.xsd3.ArrayOfanyType;

@Component(immediate=true, metatype=false)
@Service(value=PreferencesDataToPreferences.class)
public class PreferencesDataToPreferences 
		implements Converter<PreferencesData, Preferences> {
	
	@Reference
	private SeatPreferencesDataToSeatPreference seatPreferencesDataToSeatPreferencesConverter;
	
	@Reference
	private MealDataToMeal mealDataToMealConverter;
	
	@Reference
	private SeatTypeDataToSeatType seatTypeDataToSeatTypeConverter;
	
	@Override
	public Preferences convert(PreferencesData source) {
		
		Preferences destination = null;
		
		if (source != null) {
			
			ObjectFactory factory2 = new ObjectFactory();
			com.alitalia.aem.ws.booking.ancillaryservice.xsd3.ObjectFactory factory3 = 
					new com.alitalia.aem.ws.booking.ancillaryservice.xsd3.ObjectFactory();
			
			destination = factory2.createPreferences();
			
			ArrayOfanyType seatPreferences = factory3.createArrayOfanyType();
			if (source.getSeatPreferences() != null) {
				for (SeatPreferencesData seatPreferencesData : source.getSeatPreferences()) {
					seatPreferences.getAnyType().add(
							seatPreferencesDataToSeatPreferencesConverter.convert(
									seatPreferencesData));
				}
			}
			destination.setSeatPreferences(
					factory2.createPreferencesSeatPreferences(
							seatPreferences));
			
			destination.setMealType(
					factory2.createPreferencesMealType(
							mealDataToMealConverter.convert(
									source.getMealType())));
			
			destination.setSeatType(
					factory2.createPreferencesSeatType(
							seatTypeDataToSeatTypeConverter.convert(
									source.getSeatType())));
			
		}
		
		return destination;
	}

}
