package com.alitalia.aem.service.impl.home;

import com.alitalia.aem.common.messages.home.CheckinSendSmsRequest;
import com.alitalia.aem.common.messages.home.CheckinSendSmsResponse;
import com.alitalia.aem.service.api.home.CheckinSendSmsService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.checkinservice.CheckinSendSmsData;
import com.alitalia.aem.service.impl.converter.home.checkinservice.CheckinSendSmsDataConverResponse;
import com.alitalia.aem.ws.sendsms.SendSmsServiceClient;
import com.alitalia.aem.ws.sendsms.xsd.SmsMsg;
import com.alitalia.aem.ws.sendsms.xsd.SmsMsgReturn;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
@Component(immediate=true, metatype=false)
public class SimpleCheckinSendSmsService implements CheckinSendSmsService {

    private static final Logger logger = LoggerFactory.getLogger(SimpleBusinessLoginService.class);

    @Reference
    private SendSmsServiceClient sendSmsServiceClient;

    @Reference
    private CheckinSendSmsDataConverResponse responseConverter;

    @Reference
    private CheckinSendSmsData checkinSendSmsData;

    @Override
    public CheckinSendSmsResponse sendSms(CheckinSendSmsRequest request) {

        if (logger.isDebugEnabled()) {
            logger.debug("Executing method sendSms. The request is {}", request);
        }
        try
        {
            CheckinSendSmsResponse response = new CheckinSendSmsResponse();
            SmsMsg sms = checkinSendSmsData.convert(request);
            SmsMsgReturn smsMsgReturn = sendSmsServiceClient.sendSms(sms);
            response = responseConverter.convert(smsMsgReturn);
            response.setSid(request.getSid());
            response.setTid(request.getTid());
            if (logger.isDebugEnabled()) {
                logger.debug("Executed method successfully sendSms(). The response is {}", response);
            }
            return response;
        }
        catch (Exception e)
        {
            logger.error("Exception while executing method sendSms().", e);
            throw new AlitaliaServiceException("Exception executing sendSms: Sid ["+request.getSid()+"]" , e);
        }
    }
}
