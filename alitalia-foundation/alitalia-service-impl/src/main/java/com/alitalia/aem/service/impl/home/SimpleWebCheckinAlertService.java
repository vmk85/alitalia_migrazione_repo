package com.alitalia.aem.service.impl.home;

import javax.xml.ws.Holder;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.WebCheckinAlertRequest;
import com.alitalia.aem.common.messages.home.WebCheckinAlertResponse;
import com.alitalia.aem.service.api.home.WebCheckinAlertService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.checkinalertservice.WebCheckinAlertRequestToAlertRequest;
import com.alitalia.aem.ws.checkin.alertservice.CheckinAlertServiceClient;
import com.alitalia.aem.ws.checkin.alertservice.xsd2.User;


@Component(immediate=true, metatype=false)
@Service
public class SimpleWebCheckinAlertService implements WebCheckinAlertService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleWebCheckinAlertService.class);

	@Reference
	private CheckinAlertServiceClient checkinAlertClient;
	
	@Reference
	private WebCheckinAlertRequestToAlertRequest webCheckinAlertRequestConverter;
	
	@Override
	public WebCheckinAlertResponse submitUser(WebCheckinAlertRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method submitUser(). The request is {}", request);
		}

		try {
			
			WebCheckinAlertResponse response = new WebCheckinAlertResponse();
			Holder<User> serviceRequest = webCheckinAlertRequestConverter.convert(request);
			checkinAlertClient.submitUser(serviceRequest);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully submitUser(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method submitUser().", e);
			throw new AlitaliaServiceException("Exception executing submitUser: Sid ["+request.getSid()+"]" , e);
		}
	}
}