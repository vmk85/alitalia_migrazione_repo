package com.alitalia.aem.service.impl.converter.home.mmbancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryCouponData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryEticketData;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.ArrayOfCoupons;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.ETicket;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=MmbAncillaryETicketDataToETicket.class)
public class MmbAncillaryETicketDataToETicket implements Converter<MmbAncillaryEticketData, ETicket> {

	@Reference
	private MmbAncillaryCouponDataToCoupons mmbAncillaryCouponDataConverter;

	@Override
	public ETicket convert(MmbAncillaryEticketData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		ETicket destination = null;

		if (source != null) {
			destination = objectFactory.createETicket();

			destination.setX003CNumberX003EKBackingField(source.getNumber());
			destination.setX003CPnrX003EKBackingField(source.getPnr());

			ArrayOfCoupons coupons = objectFactory.createArrayOfCoupons();
			for(MmbAncillaryCouponData sourceCoupon : source.getCoupons()) {
				coupons.getCoupons().add(mmbAncillaryCouponDataConverter.convert(sourceCoupon));
			}
			destination.setX003CCouponsX003EKBackingField(coupons);
		}

		return destination;
	}

}
