package com.alitalia.aem.service.impl.converter.home.staticdataservice;
import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.ws.booking.staticdataservice.xsd5.DictionaryItem;
import com.alitalia.aem.ws.booking.staticdataservice.xsd5.PaymentTypeItem;
import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.DictionaryItemData;
import com.alitalia.aem.common.data.home.PaymentTypeItemData;

@Component(immediate=true, metatype=false)
@Service(value=PaymentTypeItemToPaymentTypeItemData.class)
public class PaymentTypeItemToPaymentTypeItemData implements Converter<PaymentTypeItem, PaymentTypeItemData> {

	@Override
	public PaymentTypeItemData convert(PaymentTypeItem source) {
		PaymentTypeItemData paymentTypeItemData = new PaymentTypeItemData();
		paymentTypeItemData.setCode(source.getCode().getValue());
		paymentTypeItemData.setDescription(source.getCode().getValue());
		List<String> groups = new ArrayList<>();
		for(String group : source.getGroup())
			groups.add(group);
		paymentTypeItemData.setGroup(groups);
		List<DictionaryItemData> otherInfo = new ArrayList<>();
		for(DictionaryItem dictionaryItem : source.getOtherInfo().getValue().getDictionaryItem()){
			DictionaryItemData dictionaryItemData = new DictionaryItemData();
			dictionaryItemData.setKey(dictionaryItem.getKey().getValue());
			dictionaryItemData.setValue(dictionaryItem.getValue().getValue());
			otherInfo.add(dictionaryItemData);
		}
		paymentTypeItemData.setOtherInfo(otherInfo);
		return paymentTypeItemData;
	}

}
