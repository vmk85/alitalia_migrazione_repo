package com.alitalia.aem.service.impl.converter.home.mmbticketingservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.CrossSellingsResponse;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd6.GetCrossSellingsResponse;

@Component(immediate = true, metatype = false)
@Service(value = GetCrossSellingsResponseToCrossSellingsResponse.class)
public class GetCrossSellingsResponseToCrossSellingsResponse implements
		Converter<GetCrossSellingsResponse, CrossSellingsResponse> {

	@Reference
	private CrossSellingsToCrossSellingsData crosseSellingsToCrossSellingsDataConverter;

	@Override
	public CrossSellingsResponse convert(GetCrossSellingsResponse source) {
		CrossSellingsResponse destination = null;

		if (source != null) {
			destination = new CrossSellingsResponse();
			
			if (source.getCrossSellings() != null)
				destination.setCrossSellingsData(crosseSellingsToCrossSellingsDataConverter.convert(source.getCrossSellings().getValue()));
		}

		return destination;
	}

}
