package com.alitalia.aem.service.impl.converter.home.mmbsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.MmbBaggageOrderData;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.BaggageOrder;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=MmbBaggageOrderDataToBaggageOrder.class)
public class MmbBaggageOrderDataToBaggageOrder implements Converter<MmbBaggageOrderData, BaggageOrder> {

	@Override
	public BaggageOrder convert(MmbBaggageOrderData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		BaggageOrder destination = null;

		if (source != null) {
			destination = objectFactory.createBaggageOrder();

			destination.setX003CCurrencyX003EKBackingField(source.getCurrency());
			destination.setX003CIdX003EKBackingField(source.getId());
			destination.setX003CQuantityX003EKBackingField(source.getQuantity());
			destination.setX003CSavingsX003EKBackingField(source.getSavings());
			destination.setX003CTotalAirportFareX003EKBackingField(source.getTotalAirportFare());
			destination.setX003CTotalFareX003EKBackingField(source.getTotalFare());
		}

		return destination;
	}

}
