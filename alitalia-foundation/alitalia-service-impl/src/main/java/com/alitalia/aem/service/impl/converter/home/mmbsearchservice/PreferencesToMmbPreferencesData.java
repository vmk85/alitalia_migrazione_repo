package com.alitalia.aem.service.impl.converter.home.mmbsearchservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.MmbPreferencesData;
import com.alitalia.aem.common.data.home.mmb.MmbSeatPreferenceData;
import com.alitalia.aem.ws.mmb.searchservice.xsd3.ArrayOfanyType;
import com.alitalia.aem.ws.mmb.searchservice.xsd4.SeatPreference;
import com.alitalia.aem.ws.mmb.searchservice.xsd4.SeatType;
import com.alitalia.aem.ws.mmb.searchservice.xsd4.Meal;
import com.alitalia.aem.ws.mmb.searchservice.xsd4.Preferences;

@Component(immediate=true, metatype=false)
@Service(value=PreferencesToMmbPreferencesData.class)
public class PreferencesToMmbPreferencesData implements Converter<Preferences, MmbPreferencesData> {

	@Reference
	private MealToMealData mealConverter;

	@Reference
	private SeatTypeToSeatTypeData seatTypeConverter;

	@Reference
	private SeatPreferencesToMmbSeatPreferenceData seatPreferencesConverter;

	@Override
	public MmbPreferencesData convert(Preferences source) {
		MmbPreferencesData destination = null;

		if (source != null) {
			destination = new MmbPreferencesData();

			JAXBElement<Object> sourceMealType = source.getMealType();
			if (sourceMealType != null)
				destination.setMealType(mealConverter.convert((Meal) sourceMealType.getValue()));

			JAXBElement<ArrayOfanyType> sourceSeatPreferences = source.getSeatPreferences();
			if (sourceSeatPreferences != null &&
					sourceSeatPreferences.getValue() != null &&
					sourceSeatPreferences.getValue().getAnyType() != null &&
					!sourceSeatPreferences.getValue().getAnyType().isEmpty()) {

				List<MmbSeatPreferenceData> seatPreferences = new ArrayList<MmbSeatPreferenceData>();

				for( Object sourceSeatPreference : sourceSeatPreferences.getValue().getAnyType()) {
					seatPreferences.add(seatPreferencesConverter.convert((SeatPreference) sourceSeatPreference));
				}

				destination.setSeatPreferences(seatPreferences);
			}

			JAXBElement<Object> sourceSeatType = source.getSeatType();
			if (sourceSeatType != null)
				destination.setSeatType(seatTypeConverter.convert((SeatType) sourceSeatType.getValue()));
		}

		return destination;
	}

}
