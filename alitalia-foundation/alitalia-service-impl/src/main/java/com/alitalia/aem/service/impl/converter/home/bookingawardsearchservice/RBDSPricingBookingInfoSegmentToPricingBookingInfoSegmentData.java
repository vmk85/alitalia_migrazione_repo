//2
package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetailsSolutionPricingBookingInfoSegment;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricing;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingBookingInfoSegmentToPricingBookingInfoSegmentData.class)
public class RBDSPricingBookingInfoSegmentToPricingBookingInfoSegmentData
		implements Converter<ResultBookingDetailsSolutionPricingBookingInfoSegment, ResultBookingDetailsSolutionPricingBookingInfoSegmentData> {

	@Reference
	private RBDSPricingBookingInfoSegmentDepartureToPricingBookingInfoSegmentDepartureDataConverter rbdsPricingBookingInfoSegmentDepartureConverter;

	@Reference
	private RBDSPricingBookingInfoSegmentFlightConverterToPricingBookingInfoSegmentFlightDataConverter rbdsPricingBookingInfoSegmentFlightConverter;
	
	@Reference
	private RBDSPricingBookingInfoSegmentPricingToPricingBookingInfoSegmentPricingData rbdsPricingBookingInfoSegmentPricingConverter;
	
	@Override
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentData convert(
			ResultBookingDetailsSolutionPricingBookingInfoSegment source) {
		ResultBookingDetailsSolutionPricingBookingInfoSegmentData destination = null;
		
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingBookingInfoSegmentData();
			destination.setDepartureField(rbdsPricingBookingInfoSegmentDepartureConverter.convert(source.getDepartureField()));
			destination.setDestinationField(source.getDestinationField());
			destination.setFlightField(rbdsPricingBookingInfoSegmentFlightConverter.convert(source.getFlightField()));
			destination.setOriginField(source.getOriginField());
			List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData> pricingField = new ArrayList<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData>();
			if(source.getPricingField()!=null)
			for(ResultBookingDetailsSolutionPricingBookingInfoSegmentPricing s : source.getPricingField().getResultBookingDetailsSolutionPricingBookingInfoSegmentPricing()){
				pricingField.add(rbdsPricingBookingInfoSegmentPricingConverter.convert(s));
			}
			destination.setPricingField(pricingField);
			
			
			
		}
		
		return destination;
	}
	
	
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentData convert(
			com.alitalia.aem.ws.bookingaward.searchservice.xsd13.ResultBookingDetailsSolutionPricingBookingInfoSegment source) {
		ResultBookingDetailsSolutionPricingBookingInfoSegmentData destination = null;
		
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingBookingInfoSegmentData();
			destination.setDepartureField(rbdsPricingBookingInfoSegmentDepartureConverter.convert(source.getDepartureField()));
			destination.setDestinationField(source.getDestinationField());
			destination.setFlightField(rbdsPricingBookingInfoSegmentFlightConverter.convert(source.getFlightField()));
			destination.setOriginField(source.getOriginField());
			List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData> pricingField = new ArrayList<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData>();
			if(source.getPricingField()!=null)
			for(com.alitalia.aem.ws.bookingaward.searchservice.xsd13.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricing s : source.getPricingField().getResultBookingDetailsSolutionPricingBookingInfoSegmentPricing()){
				pricingField.add(rbdsPricingBookingInfoSegmentPricingConverter.convert(s));
			}
			destination.setPricingField(pricingField);
			
			
			
		}
		
		return destination;
	}
}
