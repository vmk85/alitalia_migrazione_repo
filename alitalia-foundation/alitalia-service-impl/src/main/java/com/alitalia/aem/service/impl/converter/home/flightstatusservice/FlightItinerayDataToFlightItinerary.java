package com.alitalia.aem.service.impl.converter.home.flightstatusservice;

import java.util.Calendar;

import javax.xml.datatype.Duration;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.FlightItinerayData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.Flight;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.ObjectFactory;

@Component(immediate = true, metatype = false)
@Service(value = FlightItinerayDataToFlightItinerary.class)
public class FlightItinerayDataToFlightItinerary implements Converter<FlightItinerayData, Flight> {

	@Reference
	private FlightItinerayDetailsModelDataToFlightDetailsModel flightItineraryDetailsModelConverter;
	
	@Override
	public Flight convert(FlightItinerayData source) {
		Flight flight = new Flight();

		ObjectFactory objectFactory = new ObjectFactory();
		flight.setArrival(objectFactory.createFlightArrival(source.getArrival()));
		Calendar arrivalDate = source.getArrivalDate();

		if (arrivalDate != null)
			flight.setArrivalDate(XsdConvertUtils.toXMLGregorianCalendar(arrivalDate));

		Duration arrivalTime = source.getArrivalTime();
		if(arrivalTime != null)
			flight.setArrivalTime(arrivalTime);

		flight.setDeparture(objectFactory.createFlightDeparture(source.getDeparture()));

		Calendar departureDate = source.getDepartureDate();
		if (departureDate != null)
			flight.setDepartureDate(XsdConvertUtils.toXMLGregorianCalendar(departureDate));

		Duration departureTime = source.getDepartureTime();
		if(departureTime != null)
			flight.setDepartureTime(departureTime);

//		FlightDetailsModel flightDetails = flightItineraryDetailsModelConverter.convert(source.getFlightDetails());
//		flight.setFlightDetails(objectFactory.createFlightDetailsModelElement(flightDetails));
//		flight.setFlightLength(source.getFlightLength());
//		flight.setFlightNumber(objectFactory.createFlightFlightNumber(source.getFlightNumber()));
//		flight.setVector(objectFactory.createFlightVector(source.getVector()));

		return flight;
	}
}