package com.alitalia.aem.service.impl.converter.home.statisticsservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.TradeRegisterStatisticRequest;
import com.alitalia.aem.ws.tradestatistics.statisticsservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.tradestatistics.statisticsservice.xsd1.StatisticsRequest;
import com.alitalia.aem.ws.tradestatistics.statisticsservice.xsd2.StatisticsItem;

@Component(immediate = true, metatype = false)
@Service(value = RegisterStatisticsRequestToTradeStatisticsRequest.class)
public class RegisterStatisticsRequestToTradeStatisticsRequest implements Converter<TradeRegisterStatisticRequest, StatisticsRequest> {

	@Override
	public StatisticsRequest convert(TradeRegisterStatisticRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		com.alitalia.aem.ws.tradestatistics.statisticsservice.xsd2.ObjectFactory objectFactory2 = new com.alitalia.aem.ws.tradestatistics.statisticsservice.xsd2.ObjectFactory();
		com.alitalia.aem.ws.tradestatistics.statisticsservice.xsd5.ObjectFactory objectFactory5 = new com.alitalia.aem.ws.tradestatistics.statisticsservice.xsd5.ObjectFactory();

		StatisticsRequest statisticsRequest = objectFactory.createStatisticsRequest();
		StatisticsItem statisticsItem;
		
		String clientIP = source.getClientIP();
		String errorDescription = source.getErrorDescr();
		String sessionId = source.getSessionId();
		String siteCode = source.getSiteCode();
		boolean success = source.isSuccess();
		String userId = source.getUserId();
		
		switch(source.getType()) {
			case CHANGE_PSW:{
				statisticsItem = objectFactory5.createChangePswStatisticsRequest();
			}break;
			case GET_PSW:{
				statisticsItem = objectFactory5.createGetPswStatisticsRequest();
			}break;
			case GET_REPORT:{
				statisticsItem = objectFactory5.createGetReportStatisticsRequest();
			}break;
			case LOGIN:{
				statisticsItem = objectFactory5.createLoginStatisticsRequest();
			}break;
			case LOGOUT:{
				statisticsItem = objectFactory5.createLogoutStatisticsRequest();
			}break;
			case STATEMENT:{
				statisticsItem = objectFactory5.createStatementStatisticsRequest();
			}break;
			default:{
				statisticsItem = objectFactory2.createStatisticsItem();
			}break;
		}
		
		statisticsItem.setClientIP(objectFactory2.createStatisticsItemClientIP(clientIP));
		statisticsItem.setErrorDescr(objectFactory2.createStatisticsItemErrorDescr(errorDescription));
		statisticsItem.setSessionID(objectFactory2.createStatisticsItemSessionID(sessionId));
		statisticsItem.setSiteCode(objectFactory2.createStatisticsItemSiteCode(siteCode));
		statisticsItem.setSuccess(success);
		statisticsItem.setUserId(objectFactory2.createStatisticsItemUserId(userId));
		
		statisticsRequest.setItem(objectFactory.createStatisticsRequestItem(statisticsItem));
		
		return statisticsRequest;
	}
}