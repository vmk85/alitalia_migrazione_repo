package com.alitalia.aem.service.impl.converter.homerest.restresponseunmarshal;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.service.impl.beanrest.staticdataservicerest.beans.GetAirports;

public class DataResponseChange {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public RetrieveAirportsResponse dataChange(GetAirports toChange) {
		RetrieveAirportsResponse newResponse = new RetrieveAirportsResponse();
		List<AirportData> airports = new ArrayList<AirportData>();
		AirportData airportData = null;
		logger.info("prima del for loop di DataResponseChange...");

		try {

			for (int i = 0; i < toChange.getAirport().size(); i++) {
				airportData = new AirportData();

				airportData.setCity(toChange.getAirport().get(i).getCity());
				airportData.setCityUrl(toChange.getAirport().get(i).getCityUrl());
				airportData.setCityCode(toChange.getAirport().get(i).getCityCode());
				airportData.setCode(toChange.getAirport().get(i).getCode());
				airportData.setCountry(toChange.getAirport().get(i).getCountry());
				airportData.setCountryCode(toChange.getAirport().get(i).getCountryCode());
				airportData.setState(toChange.getAirport().get(i).getState());
				airportData.setStateCode(toChange.getAirport().get(i).getStateCode());
				airportData.setName(toChange.getAirport().get(i).getName());
				airportData.setEligibility(toChange.getAirport().get(i).getCheckInEligibility());	

				airports.add(airportData);
			}
		}
		catch (IndexOutOfBoundsException e) {
			logger.info(e.getMessage());
		}

		newResponse.setAirports(airports);

		return newResponse;
	}

}
