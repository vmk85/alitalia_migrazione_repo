package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ApplicantPassengerData;
import com.alitalia.aem.common.data.home.ContactData;
import com.alitalia.aem.common.data.home.TicketInfoData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ARTaxInfoTypes;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ApplicantPassenger;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.PassengerType;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd5.ArrayOfanyType;

@Component(immediate=true, metatype=false)
@Service(value=ApplicantPassengerDataToApplicantPassenger.class)
public class ApplicantPassengerDataToApplicantPassenger 
		implements Converter<ApplicantPassengerData, ApplicantPassenger> {
	
	@Reference
	private PassengerInfoBaseDataToInfoBase infoBaseDataToInfoBaseConverter;
	
	@Reference
	private PreferencesDataToPreferences preferencesDataToPreferencesConverter;
	
	@Reference
	private FrequentFlyerTypeDataToFrequentFlyerType frequentFlyerTypeDataToFrequentFlyerTypeConverter;
	
	@Reference
	private TicketInfoDataToTicketInfo ticketInfoDataToTicketInfoConverter;
	
	@Reference
	private ContactDataToContact contactDataToContactConverter;
	
	@Override
	public ApplicantPassenger convert(ApplicantPassengerData source) {
		
		ApplicantPassenger destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			com.alitalia.aem.ws.booking.infopassengerservice.xsd5.ObjectFactory xsd5ObjectFactory = 
					new com.alitalia.aem.ws.booking.infopassengerservice.xsd5.ObjectFactory();
			
			destination = objectFactory.createApplicantPassenger();
			
			// ApplicantPassenger data
			
			if (source.getArTaxInfoType() != null) {
				destination.setARTaxInfoType(
							ARTaxInfoTypes.fromValue(
									source.getArTaxInfoType().value()));
			}
			
			destination.setBlueBizCode(
					objectFactory.createApplicantPassengerBlueBizCode(
							source.getBlueBizCode()));
			
			destination.setSkyBonusCode(
					objectFactory.createApplicantPassengerSkyBonusCode(
							source.getSkyBonusCode()));
			
			destination.setSubscribeToNewsletter(
					objectFactory.createApplicantPassengerSubscribeToNewsletter(
							source.isSubscribeToNewsletter()));
			
			ArrayOfanyType contacts = xsd5ObjectFactory.createArrayOfanyType();
			if (source.getContact() != null) {
				for (ContactData contact : source.getContact()) {
					contacts.getAnyType().add(
							contactDataToContactConverter.convert(
									contact));
				}
			}
			
			destination.setContact(
					objectFactory.createApplicantPassengerContact(
							contacts));
			
			destination.setCountry(
					objectFactory.createApplicantPassengerCountry(
							source.getCountry()));
			
			destination.setCouponPrice(
					source.getCouponPrice());
			
			destination.setCUIT(
					objectFactory.createApplicantPassengerCUIT(
							source.getCuit()));
			
			destination.setEmail(
					objectFactory.createApplicantPassengerEmail(
							source.getEmail()));
			
			destination.setExtraCharge(
					source.getExtraCharge());
			
			destination.setFee(
					source.getFee());
			
			// AdultPassenger data
			
			destination.setFrequentFlyerCode(
					objectFactory.createAdultPassengerFrequentFlyerCode(
							source.getFrequentFlyerCode()));
			
			destination.setFrequentFlyerTier(
					objectFactory.createAdultPassengerFrequentFlyerTier(
							source.getFrequentFlyerTier()));
			
			destination.setFrequentFlyerType(
					objectFactory.createAdultPassengerFrequentFlyerType(
							frequentFlyerTypeDataToFrequentFlyerTypeConverter.convert(
								source.getFrequentFlyerType())));
			
			// ARegularPassenger data
			
			destination.setPreferences(
					objectFactory.createARegularPassengerPreferences(
							preferencesDataToPreferencesConverter.convert(
									source.getPreferences())));
			
			// APassengerBase data
			if (source.getAwardPrice() != null) {
				destination.setAwardPrice(source.getAwardPrice());
			}
			
			if (source.getCcFee() != null) {
				destination.setCCFee(source.getCcFee());
			}

			if (source.getType() != null) {
				destination.setType(
						PassengerType.fromValue(source.getType().value()));
			}
			
			destination.setLastName(
					objectFactory.createAPassengerBaseLastName(
							source.getLastName()));
			
			destination.setName(
					objectFactory.createAPassengerBaseName(
							source.getName()));
			
			destination.setCouponPrice(
					source.getCouponPrice());
			
			destination.setExtraCharge(
					source.getExtraCharge());
			
			destination.setFee(
					source.getFee());
			
			destination.setGrossFare(
					source.getGrossFare());
			
			destination.setNetFare(
					source.getNetFare());
			
			destination.setInfo(
					objectFactory.createAPassengerBaseInfo(
							infoBaseDataToInfoBaseConverter.convert(
									source.getInfo())));
			
			ArrayOfanyType tickets = null;
			if (source.getTickets() != null && !source.getTickets().isEmpty()) {
				tickets = xsd5ObjectFactory.createArrayOfanyType();
				for (TicketInfoData ticketInfoData : source.getTickets()) {
					tickets.getAnyType().add(
							ticketInfoDataToTicketInfoConverter.convert(
								ticketInfoData));
				}
			}
			destination.setTickets(
					objectFactory.createAPassengerBaseTickets(
							tickets));
		
		}
		
		return destination;
	}

}
