package com.alitalia.aem.service.impl.converter.home.checkinseatmapservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.ChangeSeatCheckinRequest;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd1.ChangeSeatRequest;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd1.Resource;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd2.CompartimentalClass;


@Component(immediate=true, metatype=false)
@Service(value=ChangeSeatCheckinRequestToChangeSeatRequest.class)
public class ChangeSeatCheckinRequestToChangeSeatRequest implements Converter<ChangeSeatCheckinRequest, ChangeSeatRequest> {
	
	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;
	
	@Reference
	private CheckinFlightDataToFlight checkinFlightDataConverter;


	@Override
	public ChangeSeatRequest convert(ChangeSeatCheckinRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		ChangeSeatRequest destination = null;

		if (source != null) {
			destination = objectFactory.createChangeSeatRequest();

			destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			destination.setX003CResourceX003EKBackingField(Resource.WEB);
			if (source.getCompartimentalClass() != null)
				destination.setCompartimentalClass(CompartimentalClass.fromValue(source.getCompartimentalClass().value()));
			destination.setEticket(objectFactory.createChangeSeatRequestEticket(source.getEticket()));
			destination.setFlight(objectFactory.createChangeSeatRequestFlight(checkinFlightDataConverter.convert(source.getFlight())));
			destination.setLastname(objectFactory.createChangeSeatRequestLastname(source.getLastname()));
			destination.setName(objectFactory.createChangeSeatRequestName(source.getName()));
			destination.setSeatNumber(objectFactory.createChangeSeatRequestSeatNumber(source.getSeatNumber()));
		}
		return destination;
	}

}
