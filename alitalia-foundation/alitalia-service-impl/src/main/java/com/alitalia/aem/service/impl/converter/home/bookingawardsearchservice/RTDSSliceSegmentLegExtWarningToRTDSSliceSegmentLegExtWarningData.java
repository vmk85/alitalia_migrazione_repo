package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentLegExtWarningData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionSliceSegmentLegExtWarning;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentLegExtWarningToRTDSSliceSegmentLegExtWarningData.class)
public class RTDSSliceSegmentLegExtWarningToRTDSSliceSegmentLegExtWarningData implements
		Converter<ResultTicketingDetailSolutionSliceSegmentLegExtWarning, ResultTicketingDetailSolutionSliceSegmentLegExtWarningData> {

	@Override
	public ResultTicketingDetailSolutionSliceSegmentLegExtWarningData convert(ResultTicketingDetailSolutionSliceSegmentLegExtWarning source) {
		ResultTicketingDetailSolutionSliceSegmentLegExtWarningData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionSliceSegmentLegExtWarningData();

			destination.setOvernightField(source.isOvernightField());
			destination.setOvernightFieldSpecified(source.isOvernightFieldSpecified());
		}

		return destination;
	}

}
