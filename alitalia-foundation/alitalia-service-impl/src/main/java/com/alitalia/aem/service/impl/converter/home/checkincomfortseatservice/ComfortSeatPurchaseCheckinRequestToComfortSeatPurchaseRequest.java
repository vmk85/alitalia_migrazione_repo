package com.alitalia.aem.service.impl.converter.home.checkincomfortseatservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.ComfortSeatPurchaseCheckinRequest;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.ComfortSeatPurchaseRequest;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.Resource;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.SearchReason;


@Component(immediate=true, metatype=false)
@Service(value=ComfortSeatPurchaseCheckinRequestToComfortSeatPurchaseRequest.class)
public class ComfortSeatPurchaseCheckinRequestToComfortSeatPurchaseRequest implements Converter<ComfortSeatPurchaseCheckinRequest, ComfortSeatPurchaseRequest> {

	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;
	

	@Override
	public ComfortSeatPurchaseRequest convert(ComfortSeatPurchaseCheckinRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		ComfortSeatPurchaseRequest destination = null;

		if (source != null) {
			destination = objectFactory.createComfortSeatPurchaseRequest();
			
			destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			destination.setX003CResourceX003EKBackingField(Resource.WEB);
				
			destination.setFlightDepartureDate(XsdConvertUtils.toXMLGregorianCalendar(source.getFlightDepartureDate()));			
			destination.setFlightVectorAndNumber(objectFactory.createComfortSeatPurchaseRequestFlightVectorAndNumber(source.getFlightVectorAndNumber()));
			if(source.getReason() != null){
				destination.setReason(SearchReason.fromValue(source.getReason().value()));
			}
			destination.setTicketNumber(objectFactory.createComfortSeatPurchaseRequestTicketNumber(source.getTicketNumber()));			
		}

		return destination;
	}


}
