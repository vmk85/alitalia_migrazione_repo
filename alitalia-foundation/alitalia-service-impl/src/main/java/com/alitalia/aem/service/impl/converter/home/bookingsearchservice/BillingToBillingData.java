package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.BillingData;
import com.alitalia.aem.ws.booking.searchservice.xsd6.Billing;

@Component(immediate=true, metatype=false)
@Service(value=BillingToBillingData.class)
public class BillingToBillingData 
		implements Converter<Billing, BillingData> {
	
	@Override
	public BillingData convert(Billing source) {
		
		BillingData destination = null;
		
		if (source != null) {
			
			destination = new BillingData();
			
			destination.setBill(
					source.isX003CBillX003EKBackingField());
			
			destination.setCap(
					source.getX003CCapX003EKBackingField());
			
			destination.setCfInt(
					source.getX003CCfIntX003EKBackingField());
			
			destination.setCfPax(
					source.getX003CCfPaxX003EKBackingField());
			
			destination.setCFPIVA(
					source.getX003CCFPIVAX003EKBackingField());
			
			destination.setEmailInt(
					source.getX003CEmailIntX003EKBackingField());
			
			destination.setEnteEmit(
					source.getX003CEnteEmitX003EKBackingField());
			
			destination.setEnteRich(
					source.getX003CEnteRichX003EKBackingField());
			
			destination.setFormaSocietaria(
					source.getX003CFormaSocietariaX003EKBackingField());
			
			destination.setIndSped(
					source.getX003CIndSpedX003EKBackingField());
			
			destination.setIntFattura(
					source.getX003CIntFatturaX003EKBackingField());
			
			destination.setLocSped(
					source.getX003CLocSpedX003EKBackingField());
			
			destination.setName(
					source.getX003CNameX003EKBackingField());
			
			destination.setNome(
					source.getX003CNomeX003EKBackingField());
			
			destination.setPaese(
					source.getX003CPaeseX003EKBackingField());
			
			destination.setPivaInt(
					source.getX003CPivaIntX003EKBackingField());
			
			destination.setProv(
					source.getX003CProvX003EKBackingField());
			
			destination.setSelectedCountry(
					source.getX003CSelectedCountryX003EKBackingField());
			
			destination.setSocieta(
					source.getX003CSocietaX003EKBackingField());
			
			destination.setSurname(
					source.getX003CSurnameXYX003EKBackingField());
			
			destination.setTipoRichiesta(
					source.getX003CTipoRichiestaX003EKBackingField());
			
		}
		
		return destination;
	}

}
