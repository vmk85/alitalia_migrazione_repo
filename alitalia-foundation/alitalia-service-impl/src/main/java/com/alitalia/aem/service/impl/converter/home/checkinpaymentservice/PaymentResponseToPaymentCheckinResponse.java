package com.alitalia.aem.service.impl.converter.home.checkinpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.PaymentCheckinResponse;
import com.alitalia.aem.ws.checkin.paymentservice.xsd1.PaymentResponse;

@Component(immediate=true, metatype=false)
@Service(value=PaymentResponseToPaymentCheckinResponse.class)
public class PaymentResponseToPaymentCheckinResponse implements Converter<PaymentResponse, PaymentCheckinResponse> {
	
	@Reference
	private PaymentTransactionInfoToCheckinPaymentTransactionInfoData paymentTransactionInfoConverter;


	@Override
	public PaymentCheckinResponse convert(PaymentResponse source) {
		PaymentCheckinResponse destination = null;

		if (source != null) {
			destination = new PaymentCheckinResponse();
			
			if(source.getTransactionInfo() != null){
				destination.setTransactionInfo(paymentTransactionInfoConverter.convert(source.getTransactionInfo().getValue()));
			}

		}

		return destination;
	}

}
