package com.alitalia.aem.service.impl.converter.home.statisticsservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RegisterStatisticsResponse;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd1.StatisticsResponse;

@Component(immediate = true, metatype = false)
@Service(value = StatisticsResponseToRegisterStatisticsResponse.class)
public class StatisticsResponseToRegisterStatisticsResponse implements Converter<StatisticsResponse, RegisterStatisticsResponse> {

	@Reference
	private StatisticsResponseToRegisterStatisticsResult statisticsResponseToRegisterStatisticsResult;
	
	@Override
	public RegisterStatisticsResponse convert(StatisticsResponse source) {
		RegisterStatisticsResponse response = new RegisterStatisticsResponse();

		response.setResult(statisticsResponseToRegisterStatisticsResult.convert(source));
		
		return response;
	}
}