package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.CarnetGetListRequest;
import com.alitalia.aem.common.messages.home.CarnetGetListResponse;
import com.alitalia.aem.common.messages.home.CarnetLoginRequest;
import com.alitalia.aem.common.messages.home.CarnetLoginResponse;
import com.alitalia.aem.common.messages.home.CarnetRecoveryCodeRequest;
import com.alitalia.aem.common.messages.home.CarnetRecoveryCodeResponse;
import com.alitalia.aem.common.messages.home.CarnetUpdateRequest;
import com.alitalia.aem.common.messages.home.CarnetUpdateResponse;
import com.alitalia.aem.service.api.home.CarnetService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.carnetservice.CarnetGetListRequestToGetCarnetListRequest;
import com.alitalia.aem.service.impl.converter.home.carnetservice.CarnetLoginRequestToLoginCarnetRequest;
import com.alitalia.aem.service.impl.converter.home.carnetservice.CarnetRecoveryCodeRequestToRecoveryCarnetCodeRequest;
import com.alitalia.aem.service.impl.converter.home.carnetservice.CarnetUpdateRequestToUpdateCarnetRequest;
import com.alitalia.aem.service.impl.converter.home.carnetservice.GetCarnetListResponseToCarnetGetListResponse;
import com.alitalia.aem.service.impl.converter.home.carnetservice.LoginCarnetResponseToCarnetLoginResponse;
import com.alitalia.aem.service.impl.converter.home.carnetservice.RecoveryCarnetCodeResponseToCarnetRecoveryCodeResponse;
import com.alitalia.aem.service.impl.converter.home.carnetservice.UpdateCarnetResponseToCarnetUpdateResponse;
import com.alitalia.aem.ws.carnet.carnetservice.CarnetServiceClient;
import com.alitalia.aem.ws.carnet.carnetservice.xsd1.GetCarnetListRequest;
import com.alitalia.aem.ws.carnet.carnetservice.xsd1.GetCarnetListResponse;
import com.alitalia.aem.ws.carnet.carnetservice.xsd1.LoginCarnetRequest;
import com.alitalia.aem.ws.carnet.carnetservice.xsd1.LoginCarnetResponse;
import com.alitalia.aem.ws.carnet.carnetservice.xsd1.RecoveryCarnetCodeRequest;
import com.alitalia.aem.ws.carnet.carnetservice.xsd1.RecoveryCarnetCodeResponse;
import com.alitalia.aem.ws.carnet.carnetservice.xsd1.UpdateCarnetRequest;
import com.alitalia.aem.ws.carnet.carnetservice.xsd1.UpdateCarnetResponse;

@Component(immediate=true, metatype=false)
@Service
public class SimpleCarnetService implements CarnetService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleCarnetService.class);

	@Reference
	private CarnetServiceClient carnetServiceClient;
	
	@Reference
	private CarnetGetListRequestToGetCarnetListRequest getCarnetListRequestConverter;
	
	@Reference
	private CarnetLoginRequestToLoginCarnetRequest loginCarnetRequestConverter;
	
	@Reference
	private CarnetRecoveryCodeRequestToRecoveryCarnetCodeRequest recoveryCarnetCodeRequestConverter;
	
	@Reference
	private CarnetUpdateRequestToUpdateCarnetRequest updateCarnetRequestConverter;
	
	@Reference
	private GetCarnetListResponseToCarnetGetListResponse getCarnetListResponseConverter;
	
	@Reference
	private LoginCarnetResponseToCarnetLoginResponse loginCarnetResponseConverter;
	
	@Reference
	private RecoveryCarnetCodeResponseToCarnetRecoveryCodeResponse recoveryCarnetCodeResponseConverter;
	
	@Reference
	private UpdateCarnetResponseToCarnetUpdateResponse updateCarnetResponseConverter;
	
	@Override
	public CarnetGetListResponse getCarnetList(CarnetGetListRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method getCarnetList(). The request is {}", request);
		}

		try {
			CarnetGetListResponse response = new CarnetGetListResponse();
			GetCarnetListRequest serviceRequest = getCarnetListRequestConverter.convert(request);
			GetCarnetListResponse serviceResponse = carnetServiceClient.getCarnetList(serviceRequest);
			response = getCarnetListResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully getCarnetList(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getCarnetList().", e);
			throw new AlitaliaServiceException("Exception executing getCarnetList: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public CarnetLoginResponse loginCarnet(CarnetLoginRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method loginCarnet(). The request is {}", request);
		}

		try {
			CarnetLoginResponse response = new CarnetLoginResponse();
			LoginCarnetRequest serviceRequest = loginCarnetRequestConverter.convert(request);
			LoginCarnetResponse serviceResponse = carnetServiceClient.loginCarnet(serviceRequest);
			response = loginCarnetResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully loginCarnet(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method sendEmail().", e);
			throw new AlitaliaServiceException("Exception executing sendEmail: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public CarnetRecoveryCodeResponse recoveryCarnetCode(CarnetRecoveryCodeRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method recoveryCarnetCode(). The request is {}", request);
		}

		try {
			CarnetRecoveryCodeResponse response = new CarnetRecoveryCodeResponse();
			RecoveryCarnetCodeRequest serviceRequest = recoveryCarnetCodeRequestConverter.convert(request);
			RecoveryCarnetCodeResponse serviceResponse = carnetServiceClient.recoveryCarnetCode(serviceRequest);
			response = recoveryCarnetCodeResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully recoveryCarnetCode(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method recoveryCarnetCode().", e);
			throw new AlitaliaServiceException("Exception executing recoveryCarnetCode: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public CarnetUpdateResponse updateCarnet(CarnetUpdateRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method updateCarnet(). The request is {}", request);
		}

		try {
			CarnetUpdateResponse response = new CarnetUpdateResponse();
			UpdateCarnetRequest serviceRequest = updateCarnetRequestConverter.convert(request);
			UpdateCarnetResponse serviceResponse = carnetServiceClient.updateCarnet(serviceRequest);
			response = updateCarnetResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully updateCarnet(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method updateCarnet().", e);
			throw new AlitaliaServiceException("Exception executing updateCarnet: Sid ["+request.getSid()+"]" , e);
		}
	}
}