package com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.MmbAirportCheckinData;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.AirportCheckin;

@Component(immediate=true, metatype=false)
@Service(value=AirportCheckinToMmbAirportCheckinData.class)
public class AirportCheckinToMmbAirportCheckinData implements Converter<AirportCheckin, MmbAirportCheckinData> {

	@Override
	public MmbAirportCheckinData convert(AirportCheckin source) {
		MmbAirportCheckinData destination = null;

		if (source != null) {
			destination = new MmbAirportCheckinData();

			JAXBElement<String> sourceAirportName = source.getAirportName();
			if (sourceAirportName != null)
				destination.setAirportName(sourceAirportName.getValue());

			destination.setArriveEnabled(source.isArriveEnabled());

			JAXBElement<String> sourceCode = source.getCode();
			if (sourceCode != null)
				destination.setCode(sourceCode.getValue());

			JAXBElement<String> sourceDeepLink = source.getDeepLink();
			if (sourceDeepLink!= null)
				destination.setDeepLink(sourceDeepLink.getValue());

			destination.setDepartureEnabled(source.isDepartureEnabled());
			destination.setId(source.getX003CIdX003EKBackingField());
		}

		return destination;
	}

}
