package com.alitalia.aem.service.impl.converter.home.checkinstaticdataservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinAirportName;
import com.alitalia.aem.common.messages.home.WebCheckinAirportNameResponse;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.StaticDataResponseOfAirportNameUGS9ND5Y;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd2.AirportName;


@Component(immediate = true, metatype = false)
@Service(value = StaticDataResponseToWebCheckinAirportNameResponse.class)
public class StaticDataResponseToWebCheckinAirportNameResponse implements Converter<StaticDataResponseOfAirportNameUGS9ND5Y, WebCheckinAirportNameResponse> {

	@Reference
	private AirportNameToCheckinAirportName airportNameConverter;

	@Override
	public WebCheckinAirportNameResponse convert(StaticDataResponseOfAirportNameUGS9ND5Y source) {
		WebCheckinAirportNameResponse destination = new WebCheckinAirportNameResponse();

		List<CheckinAirportName> airports = new ArrayList<>();
		if (source.getEntities()!=null && source.getEntities().getValue()!=null &&
				source.getEntities().getValue().getAirportName()!=null && source.getEntities().getValue().getAirportName().size()>0){
			List<AirportName> airportNames = source.getEntities().getValue().getAirportName();
			for (AirportName airportName : airportNames) {
				CheckinAirportName airport = airportNameConverter.convert(airportName);
				airports.add(airport);
			}
			destination.setAirports(airports);
		}

		return destination;			
	}
}