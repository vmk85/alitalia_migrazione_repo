package com.alitalia.aem.service.impl.converter.home.geospatialservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveGeoNearestAirportByCoordsRequest;
import com.alitalia.aem.ws.geospatial.service.xsd6.GetNearestAirportByCoordsRequest;
import com.alitalia.aem.ws.geospatial.service.xsd6.ObjectFactory;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveGeoNearestAirportByCoordsRequestToGetNearerstAirportByCoordsRequest.class)
public class RetrieveGeoNearestAirportByCoordsRequestToGetNearerstAirportByCoordsRequest 
		implements Converter<RetrieveGeoNearestAirportByCoordsRequest, GetNearestAirportByCoordsRequest> {

	@Reference
	private IPAddressDataToIPAddress ipAddressDataConverter;
	
	@Override
	public GetNearestAirportByCoordsRequest convert(RetrieveGeoNearestAirportByCoordsRequest source) {
		
		GetNearestAirportByCoordsRequest destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			destination = objectFactory.createGetNearestAirportByCoordsRequest();
			
			destination.setCountryCode(
					objectFactory.createGetNearestAirportByCoordsRequestCountryCode(
							source.getCountryCode()));
			
			destination.setLatitude(
					objectFactory.createGetNearestAirportByCoordsRequestLatitude(
							source.getLatitude()));
			
			destination.setLongitude(
					objectFactory.createGetNearestAirportByCoordsRequestLongitude(
							source.getLongitude()));
			
			destination.setMaxDistanceKM(
					objectFactory.createGetNearestAirportByCoordsRequestMaxDistanceKM(
							source.getMaxDistanceKM()));
			
		}
		
		return destination;
	}
	
}