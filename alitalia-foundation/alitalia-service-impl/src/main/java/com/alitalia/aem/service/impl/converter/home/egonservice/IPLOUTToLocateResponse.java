package com.alitalia.aem.service.impl.converter.home.egonservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.LocateResponse;
import com.alitalia.aem.ws.egon.xsd.IPLAREAOUT;
import com.alitalia.aem.ws.egon.xsd.IPLERR;
import com.alitalia.aem.ws.egon.xsd.IPLOUT;

@Component(immediate = true, metatype = false)
@Service(value = IPLOUTToLocateResponse.class)
public class IPLOUTToLocateResponse implements Converter<IPLOUT, LocateResponse> {

	@Override
	public LocateResponse convert(IPLOUT source) {
		
		LocateResponse response = new LocateResponse();
		
		if (source != null) {
			
			if (source.getIPLAREAOUT() != null && source.getIPLAREAOUT().getValue() != null) {
				IPLAREAOUT ipOut = source.getIPLAREAOUT().getValue();
				if (ipOut.getCDXARE() != null) {
					response.setCdxare(ipOut.getCDXARE().getValue());
				}
				if (ipOut.getCDXISO() != null) {
					response.setCdxiso(ipOut.getCDXISO().getValue());
				}
				if (ipOut.getCDXMTR() != null) {
					response.setCdxmtr(ipOut.getCDXMTR().getValue());
				}
				if (ipOut.getCDXREG() != null) {
					response.setCdxreg(ipOut.getCDXREG().getValue());
				}
				if (ipOut.getCDXZIP() != null) {
					response.setCdxzip(ipOut.getCDXZIP().getValue());
				}
				if (ipOut.getCOOLAT() != null) {
					response.setCoolat(ipOut.getCOOLAT().getValue());
				}
				if (ipOut.getCOOLON() != null) {
					response.setCoolon(ipOut.getCOOLON().getValue());
				}
				if (ipOut.getDSXISP() != null) {
					response.setDsxisp(ipOut.getDSXISP().getValue());
				}
				if (ipOut.getDSXOBJCNY() != null) {
					response.setDsxobjcny(ipOut.getDSXOBJCNY().getValue());
				}
				if (ipOut.getDSXOBJPLC() != null) {
					response.setDsxobjplc(ipOut.getDSXOBJPLC().getValue());
				}
				if (ipOut.getDSXORG() != null) {
					response.setDsxorg(ipOut.getDSXORG().getValue());
				}
				if (ipOut.getDSXREG() != null) {
					response.setDsxreg(ipOut.getDSXREG().getValue());
				}
			}
			
			if (source.getERR() != null && source.getERR().getValue() != null) {
				IPLERR ipErr = source.getERR().getValue();
				if (ipErr.getDSXERR() != null) {
					response.setDsxerr(ipErr.getDSXERR().getValue());
				}
				response.setWp9STC(ipErr.getWP9STC());
			}
			
		}
		
		return response;
	}
}