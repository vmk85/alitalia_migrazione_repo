package com.alitalia.aem.service.impl.converter.home.bookingwidget;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMCreditCardData;
import com.alitalia.aem.common.data.home.enumerations.MMCardTypeEnum;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.widgetservice.xsd8.ArrayOfCreditCard;
import com.alitalia.aem.ws.booking.widgetservice.xsd8.CreditCard;

@Component(immediate = true, metatype = false)
@Service(value = CreditCardsToMMCreditCardsData.class)
public class CreditCardsToMMCreditCardsData implements Converter<ArrayOfCreditCard, List<MMCreditCardData>> {

	private static Logger logger = LoggerFactory.getLogger(CreditCardsToMMCreditCardsData.class);
	
	@Override
	public List<MMCreditCardData> convert(ArrayOfCreditCard source) {
		
		if (source == null) {
			return null;
		}
		
		List<MMCreditCardData> mmCards = new ArrayList<MMCreditCardData>();

		for (CreditCard creditCard: source.getCreditCard()) {
			try {
				
				MMCreditCardData mmCreditCardData = new MMCreditCardData();
				mmCreditCardData.setCardType(MMCardTypeEnum.fromValue(creditCard.getCardType().value()));
				if (creditCard.getCircuitCode().getValue() != null)
					mmCreditCardData.setCircuitCode(MMCreditCardEnum.fromValue(creditCard.getCircuitCode().getValue()).toString());
				mmCreditCardData.setNumber(creditCard.getNumber().getValue());
				mmCreditCardData.setToken(creditCard.getToken().getValue());
				
				if (creditCard.getExpireDate() != null)
					mmCreditCardData.setExpireDate(XsdConvertUtils.parseCalendar(creditCard.getExpireDate()));
				
				mmCards.add(mmCreditCardData);
			
			} catch (Exception e) {
				logger.error("Error while executing Credit Card conversion: {}", e);
			}
		}
		
		return mmCards;
	}
}