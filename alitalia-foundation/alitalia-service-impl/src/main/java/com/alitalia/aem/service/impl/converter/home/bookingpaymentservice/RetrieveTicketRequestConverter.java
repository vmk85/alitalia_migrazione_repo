package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.TaxData;
import com.alitalia.aem.common.messages.home.RetrieveTicketRequest;
import com.alitalia.aem.ws.booking.paymentservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd4.ArrayOfTax;

@Component(metatype=false, immediate=true)
@Service(value=RetrieveTicketRequestConverter.class)
public class RetrieveTicketRequestConverter implements Converter<RetrieveTicketRequest, com.alitalia.aem.ws.booking.paymentservice.xsd1.RetrieveTicketRequest> {

	@Reference
	RoutesDataToRoutes routesConverter;
	
	@Reference
	PaymentProcessInfoDataToProcessInfo processInfoConverter;
	
	@Reference
	TaxDataToTax taxConverter;
	
	@Override
	public com.alitalia.aem.ws.booking.paymentservice.xsd1.RetrieveTicketRequest convert(RetrieveTicketRequest source) {
		com.alitalia.aem.ws.booking.paymentservice.xsd4.ObjectFactory objectFactory4 = new com.alitalia.aem.ws.booking.paymentservice.xsd4.ObjectFactory();
		ObjectFactory objectFactory = new ObjectFactory();
		com.alitalia.aem.ws.booking.paymentservice.xsd1.RetrieveTicketRequest request = objectFactory.createRetrieveTicketRequest();
		request.setProcess(objectFactory.createRetrieveTicketRequestProcess(processInfoConverter.convert(source.getProcess())));
		request.setRoutes(objectFactory.createRetrieveTicketRequestRoutes(routesConverter.convert(source.getRoutes())));
		
		
		ArrayOfTax taxes = objectFactory4.createArrayOfTax();
		for(TaxData tax : source.getTaxs())
			taxes.getTax().add(taxConverter.convert(tax));
		
		request.setTaxes(objectFactory.createRetrieveTicketRequestTaxes(taxes));
		request.setCookie(objectFactory.createRetrieveTicketRequestCookie(source.getCookie()));
		request.setExecution(objectFactory.createRetrieveTicketRequestExecution(source.getExecution()));
		request.setSabreGateWayAuthToken(objectFactory.createRetrieveTicketRequestSabreGateWayAuthToken(source.getSabreGateWayAuthToken()));
		return request;
	}
	
}
