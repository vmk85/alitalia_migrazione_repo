package com.alitalia.aem.service.impl.converter.home.commonservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveFlightSeatMapResponse;
import com.alitalia.aem.ws.booking.commonservice.xsd1.SeatMapResponse;

@Component(immediate = true, metatype = false)
@Service(value = SeatMapResponseToRetrieveFlightSeatMapResponse.class)
public class SeatMapResponseToRetrieveFlightSeatMapResponse 
		implements Converter<SeatMapResponse, RetrieveFlightSeatMapResponse> {

	@Reference
	private FlightSeatMapToFlightSeatMapData flightSeatMapDataConverter;
	
	@Override
	public RetrieveFlightSeatMapResponse convert(SeatMapResponse source) {
		
		RetrieveFlightSeatMapResponse destination = null;
		
		if (source != null) {
		
			destination = new RetrieveFlightSeatMapResponse();
			
			if (source.getFlightSeatMapMatrix() != null) {
				destination.setSeatMapMatrix(
						flightSeatMapDataConverter.convert(
								source.getFlightSeatMapMatrix().getValue()));
			}
			
			destination.setCookie(source.getCookie().getValue());
			destination.setExecute(source.getExecution().getValue());
			destination.setSabreGateWayAuthToken(source.getSabreGateWayAuthToken().getValue());
			
		}
		
		return destination;
		
	}
	
}