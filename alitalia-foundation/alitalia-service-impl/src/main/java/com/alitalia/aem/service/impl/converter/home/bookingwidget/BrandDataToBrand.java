package com.alitalia.aem.service.impl.converter.home.bookingwidget;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.BaggageAllowanceData;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.BrandPenaltiesData;
import com.alitalia.aem.common.data.home.PropertiesData;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.BaggageAllowance;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.Brand;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.ObjectFactory;
import com.alitalia.aem.ws.booking.widgetservice.xsd3.ArrayOfanyType;
import com.alitalia.aem.ws.booking.widgetservice.xsd3.ArrayOfstring;

@Component(immediate=true, metatype=false)
@Service(value=BrandDataToBrand.class)
public class BrandDataToBrand implements Converter<BrandData, Brand> {

	@Reference
	private BaggageAllowanceDataToBaggageAllowance baggageAllowanceDataConverter; 

	@Reference
	private BrandPenaltiesDataToBrandPenalties brandPenaltiesDataConverter;

	@Reference
	private PropertiesDataToArrayOfDictionaryItem propertiesDataConverter;
	
	@Override
	public Brand convert(BrandData source) {
		ObjectFactory factory5 = new ObjectFactory();
		com.alitalia.aem.ws.booking.widgetservice.xsd3.ObjectFactory factory3 =
				new com.alitalia.aem.ws.booking.widgetservice.xsd3.ObjectFactory();
		Brand destination = factory5.createBrand();

		destination.setAdvancePurchaseDays(source.getAdvancePurchaseDays());
		destination.setBestFare(source.isBestFare());
		destination.setCode(factory5.createBrandCode(source.getCode()));
		destination.setCompartimentalClass(factory5.createBrandCompartimentalClass(source.getCompartimentalClass()));
		destination.setCurrency(factory5.createBrandCurrency(source.getCurrency()));
		destination.setEnabled(source.isEnabled());
		destination.setGrossFare(source.getGrossFare());
		destination.setIndex(source.getIndex());
		destination.setNetFare(source.getNetFare());
		destination.setRealPrice(source.getRealPrice());
		destination.setRefreshSolutionId(factory5.createABoomBoxRefreshInfoRefreshSolutionId(source.getRefreshSolutionId()));
		destination.setSeatsAvailable(source.getSeatsAvailable());
		destination.setSelected(source.isSelected());
		destination.setSolutionId(factory5.createABoomBoxItemInfoSolutionId(source.getSolutionId()));

		List<BaggageAllowanceData> sourceBaggageAllowance = source.getBaggageAllowanceList();
		ArrayOfanyType baggageAllowanceList = null;
		if (sourceBaggageAllowance != null && !sourceBaggageAllowance.isEmpty()) {
			baggageAllowanceList = factory3.createArrayOfanyType();
			for (BaggageAllowanceData baggageAllowanceData : sourceBaggageAllowance) {
				BaggageAllowance baggageAllowance = baggageAllowanceDataConverter.convert(baggageAllowanceData);
				baggageAllowanceList.getAnyType().add(baggageAllowance);
			}
		}
		destination.setBaggageAllowanceList(factory5.createBrandBaggageAllowanceList(baggageAllowanceList));

		List<String> sourceNotes = source.getNotes();
		ArrayOfstring notes = null;
		if (sourceNotes != null && !sourceNotes.isEmpty()) {
			notes = factory3.createArrayOfstring();
			for (String noteSource : sourceNotes ) 
				notes.getString().add(noteSource);
		}
		destination.setNotes(factory5.createBrandNotes(notes));

		BrandPenaltiesData penalties = source.getPenalties();
		if (penalties == null)
			destination.setPenalties(null);
		else
			destination.setPenalties(factory5.createBrandPenalties(brandPenaltiesDataConverter.convert(penalties)));

		PropertiesData sourceProperties = source.getProperties();
		if (sourceProperties != null)
			destination.setProperties(factory5.createABoomBoxGenericInfoProperties(
				propertiesDataConverter.convert(sourceProperties)));
		else
			destination.setProperties(factory5.createABoomBoxGenericInfoProperties(null));

		return destination;
	}
}