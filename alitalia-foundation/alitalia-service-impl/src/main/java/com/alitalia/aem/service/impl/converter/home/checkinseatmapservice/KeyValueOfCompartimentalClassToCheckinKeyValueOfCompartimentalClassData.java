package com.alitalia.aem.service.impl.converter.home.checkinseatmapservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinKeyValueOfCompartimentalClassData;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd4.ArrayOfKeyValueOfCompartimentalClassint0HzHfQkm.KeyValueOfCompartimentalClassint0HzHfQkm;


@Component(immediate=true, metatype=false)
@Service(value=KeyValueOfCompartimentalClassToCheckinKeyValueOfCompartimentalClassData.class)
public class KeyValueOfCompartimentalClassToCheckinKeyValueOfCompartimentalClassData implements Converter<KeyValueOfCompartimentalClassint0HzHfQkm, CheckinKeyValueOfCompartimentalClassData> {
	

	@Override
	public CheckinKeyValueOfCompartimentalClassData convert(KeyValueOfCompartimentalClassint0HzHfQkm source) {
		CheckinKeyValueOfCompartimentalClassData destination = null;

		if (source != null) {
			destination = new CheckinKeyValueOfCompartimentalClassData();

			if (source.getKey() != null)
				destination.setKey(MmbCompartimentalClassEnum.fromValue(source.getKey().value()));
			destination.setValue(source.getValue());
		}

		return destination;
	}

}
