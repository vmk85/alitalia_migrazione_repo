package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.WebCheckinRequest;
import com.alitalia.aem.common.messages.home.WebCheckinResponse;
import com.alitalia.aem.common.messages.home.WebCheckinUndoCheckinRequest;
import com.alitalia.aem.common.messages.home.WebCheckinUndoCheckinResponse;
import com.alitalia.aem.common.messages.home.WebCheckinUndoManyCheckinRequest;
import com.alitalia.aem.common.messages.home.WebCheckinUndoManyCheckinResponse;
import com.alitalia.aem.service.api.home.WebCheckinService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.checkinservice.CheckinResponseToWebCheckinResponse;
import com.alitalia.aem.service.impl.converter.home.checkinservice.UndoCheckinResponseToWebCheckinUndoCheckinResponse;
import com.alitalia.aem.service.impl.converter.home.checkinservice.UndoManyCheckInResponseToWebCheckinUndoManyCheckinResponse;
import com.alitalia.aem.service.impl.converter.home.checkinservice.WebCheckinRequestToCheckInRequest;
import com.alitalia.aem.service.impl.converter.home.checkinservice.WebCheckinUndoCheckinRequestToUndoCheckInRequest;
import com.alitalia.aem.service.impl.converter.home.checkinservice.WebCheckinUndoManyCheckinRequestToUndoManyCheckInRequest;
import com.alitalia.aem.ws.checkin.checkinservice.CheckinCheckInServiceClient;
import com.alitalia.aem.ws.checkin.checkinservice.xsd1.CheckInRequest;
import com.alitalia.aem.ws.checkin.checkinservice.xsd1.CheckInResponse;
import com.alitalia.aem.ws.checkin.checkinservice.xsd1.UndoCheckInRequest;
import com.alitalia.aem.ws.checkin.checkinservice.xsd1.UndoCheckInResponse;
import com.alitalia.aem.ws.checkin.checkinservice.xsd1.UndoManyCheckInRequest;
import com.alitalia.aem.ws.checkin.checkinservice.xsd1.UndoManyCheckInResponse;

@Component(immediate=true, metatype=false)
@Service
public class SimpleWebCheckinService implements WebCheckinService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleWebCheckinService.class);

	@Reference
	private CheckinCheckInServiceClient checkinServiceClient;

	@Reference
	private WebCheckinRequestToCheckInRequest checkInRequestConverter;
	
	@Reference
	private CheckinResponseToWebCheckinResponse checkinResponseConverter;
	
	@Reference
	private WebCheckinUndoCheckinRequestToUndoCheckInRequest undoCheckinRequestConverter;
	
	@Reference
	private UndoCheckinResponseToWebCheckinUndoCheckinResponse undoCheckinResponseConverter;
	
	@Reference
	private WebCheckinUndoManyCheckinRequestToUndoManyCheckInRequest undoManyCheckinRequestConverter;
	
	@Reference
	private UndoManyCheckInResponseToWebCheckinUndoManyCheckinResponse undoManyCheckinResponseConverter;
	
	@Override
	public WebCheckinResponse checkIn(WebCheckinRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method checkIn(). The request is {}", request);
		}

		try {
			WebCheckinResponse response = new WebCheckinResponse();
			CheckInRequest serviceRequest = checkInRequestConverter.convert(request);
			CheckInResponse serviceResponse = checkinServiceClient.checkIn(serviceRequest);
			response = checkinResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully checkIn(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method checkIn().", e);
			throw new AlitaliaServiceException("Exception executing checkIn: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public WebCheckinUndoCheckinResponse undoCheckin(WebCheckinUndoCheckinRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method undoCheckin(). The request is {}", request);
		}

		try {
			WebCheckinUndoCheckinResponse response = new WebCheckinUndoCheckinResponse();
			UndoCheckInRequest serviceRequest = undoCheckinRequestConverter.convert(request);
			UndoCheckInResponse serviceResponse = checkinServiceClient.undoCheckIn(serviceRequest);
			response = undoCheckinResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully undoCheckin(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method undoCheckin().", e);
			throw new AlitaliaServiceException("Exception executing undoCheckin: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public WebCheckinUndoManyCheckinResponse undoManyCheckin(WebCheckinUndoManyCheckinRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method undoManyCheckin(). The request is {}", request);
		}

		try {
			WebCheckinUndoManyCheckinResponse response = new WebCheckinUndoManyCheckinResponse();
			UndoManyCheckInRequest serviceRequest = undoManyCheckinRequestConverter.convert(request);
			UndoManyCheckInResponse serviceResponse = checkinServiceClient.undoManyCheckIn(serviceRequest);
			response = undoManyCheckinResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully undoManyCheckin(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method undoManyCheckin().", e);
			throw new AlitaliaServiceException("Exception executing undoManyCheckin: Sid ["+request.getSid()+"]" , e);
		}
	}
}