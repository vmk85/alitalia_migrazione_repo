package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ContactData;
import com.alitalia.aem.common.data.home.FFCardData;
import com.alitalia.aem.common.data.home.MmCustomerData;
import com.alitalia.aem.common.data.home.enumerations.GenderTypeEnum;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.Contact;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.FFCard;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.MmCustomer;

@Component(immediate=true, metatype=false)
@Service(value=MmCustomerToMmCustomerData.class)
public class MmCustomerToMmCustomerData 
		implements Converter<MmCustomer, MmCustomerData> {
	
	@Reference
	private FFCardToFFCardData ffCardToFfCardDataConverter;
	
	@Reference
	private ContactToContactData contactToContactDataConverter;
	
	@Reference
	private MealToMealData mealToMealDataConverter;
	
	@Reference
	private SeatTypeToSeatTypeData seatTypeToSeatTypeDataConverter;
	
	@Override
	public MmCustomerData convert(MmCustomer source) {
		
		MmCustomerData destination = null;
		
		if (source != null) {
			
			destination = new MmCustomerData();
			
			destination.setBirthDate(
					XsdConvertUtils.parseCalendar(
							source.getBirthDate()));
			
			List<FFCardData> cards = new ArrayList<FFCardData>();
			if (source.getCards() != null && source.getCards().getValue() != null) {
				for (FFCard card : source.getCards().getValue().getFFCard()) {
					cards.add(
							ffCardToFfCardDataConverter.convert(
									card));
				}
			}
			destination.setCards(cards);
			
			if (source.getCode() != null) {
				destination.setCode(
						source.getCode().getValue());
			}
			
			List<ContactData> contacts = new ArrayList<ContactData>();
			if (source.getCustomerPhones() != null && source.getCustomerPhones().getValue() != null) {
				for (Contact contact : source.getCustomerPhones().getValue().getContact()) {
					contacts.add(
							contactToContactDataConverter.convert(
									contact));
				}
			}
			destination.setCustomerPhones(contacts);
			
			if (source.getEMail() != null) {
				destination.setEmail(
						source.getEMail().getValue());
			}
			
			if (source.getGender() != null) {
				destination.setGender(
						GenderTypeEnum.fromValue(source.getGender().value()));
			}
			
			destination.setIgnore(
					source.isIgnore());
			
			if (source.getLastName() != null) {
				destination.setLastName(
						source.getLastName().getValue());
			}
			
			if (source.getMealPreference() != null) {
				destination.setMealPreference(
						mealToMealDataConverter.convert(
								source.getMealPreference().getValue()));
			}
			
			destination.setMilesBalance(
					source.getMilesBalance());
			
			destination.setMilesEarned(
					source.getMilesEarned());
			
			destination.setMilesQualify(
					source.getMilesQualify());

			if (source.getName() != null) {
				destination.setName(
						source.getName().getValue());
			}
			
			if (source.getPin() != null) {
				destination.setPin(
						source.getPin().getValue());
			}
			
			if (source.getSeatPreference() != null) {
				destination.setSeatPreference(
						seatTypeToSeatTypeDataConverter.convert(
								source.getSeatPreference().getValue()));
			}
			
			if (source.getTierCode() != null) {
				destination.setTierCode(
							source.getTierCode().getValue());
			}
			
			if (source.getTitle() != null) {
				destination.setTitle(
							source.getTitle().getValue());
			}
			
		}
		
		return destination;
	}

}
