package com.alitalia.aem.service.impl.converter.home.checkinstaticdataservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.FrequentFlyerTypeData;
import com.alitalia.aem.common.messages.home.WebCheckinFrequentFlyerResponse;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.StaticDataResponseOfFrequentFlyerCarrierUGS9ND5Y;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd2.FrequentFlyerCarrier;


@Component(immediate = true, metatype = false)
@Service(value = StaticDataResponseToWebCheckinFrequentFlyerResponse.class)
public class StaticDataResponseToWebCheckinFrequentFlyerResponse implements Converter<StaticDataResponseOfFrequentFlyerCarrierUGS9ND5Y, WebCheckinFrequentFlyerResponse> {
	
	@Reference
	private FrequentFlyerCarrierToFrequentFlyerTypeData frequentFlyerConverter;

	@Override
	public WebCheckinFrequentFlyerResponse convert(StaticDataResponseOfFrequentFlyerCarrierUGS9ND5Y source) {
		WebCheckinFrequentFlyerResponse destination = new WebCheckinFrequentFlyerResponse();

		List<FrequentFlyerTypeData> frequentFlyerTypeData = new ArrayList<>();
		if (source.getEntities()!=null && source.getEntities().getValue()!=null &&
				source.getEntities().getValue().getFrequentFlyerCarrier()!=null && source.getEntities().getValue().getFrequentFlyerCarrier().size()>0){
			List<FrequentFlyerCarrier> ffData = source.getEntities().getValue().getFrequentFlyerCarrier();
			for (FrequentFlyerCarrier ffCarrier : ffData) {
				FrequentFlyerTypeData ffTypeData = frequentFlyerConverter.convert(ffCarrier);
				frequentFlyerTypeData.add(ffTypeData);
			}
			destination.setFrequentFlyers(frequentFlyerTypeData);
		}
		return destination;			
	}
}