package com.alitalia.aem.service.impl.converter.home.bookingwidget;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.Brand;
import com.alitalia.aem.ws.booking.widgetservice.xsd3.ArrayOfanyType;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.ConnectingFlight;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.DirectFlight;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.FlightType;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=ConnectingFlightDataToConnectingFlight.class)
public class ConnectingFlightDataToConnectingFlight implements Converter<ConnectingFlightData, ConnectingFlight> {

	@Reference
	private DirectFlightDataToDirectFlight directFlightDataConverter;
	
	@Reference
	private BrandDataToBrand brandDataConverter;

	@Reference
	private PropertiesDataToArrayOfDictionaryItem propertiesDataConverter;

	@Override
	public ConnectingFlight convert(ConnectingFlightData source) {
		ObjectFactory factory5 = new ObjectFactory();
		com.alitalia.aem.ws.booking.widgetservice.xsd3.ObjectFactory factory3 = 
				new com.alitalia.aem.ws.booking.widgetservice.xsd3.ObjectFactory();
		
		ConnectingFlight destination = factory5.createConnectingFlight();
		
		destination.setType(FlightType.CONNECTING);
		
		ArrayOfanyType flightsArray = factory3.createArrayOfanyType();
		for (FlightData flightData : source.getFlights()) {
			if (flightData instanceof DirectFlightData) {
				DirectFlight directFlight = directFlightDataConverter.convert((DirectFlightData) flightData);
				flightsArray.getAnyType().add(directFlight);
			} else {
				ConnectingFlight connectingFlight = this.convert((ConnectingFlightData) flightData);
				flightsArray.getAnyType().add(connectingFlight);
			}
		}
		destination.setFlights(factory5.createConnectingFlightFlights(flightsArray));

		ArrayOfanyType brandsArray = factory3.createArrayOfanyType();
		for (BrandData brandData : source.getBrands()) {
			Brand brand = brandDataConverter.convert(brandData);
			brandsArray.getAnyType().add(brand);
		}
		destination.setBrands(factory5.createAFlightBaseBrands(brandsArray));

		if (source.getProperties() != null)
			destination.setProperties(factory5.createABoomBoxGenericInfoProperties(
					propertiesDataConverter.convert(source.getProperties())));
		else
			destination.setProperties(factory5.createABoomBoxGenericInfoProperties(null));

		return destination;
	}

}
