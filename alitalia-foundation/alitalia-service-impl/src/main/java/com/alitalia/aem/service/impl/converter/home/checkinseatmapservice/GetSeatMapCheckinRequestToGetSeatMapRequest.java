package com.alitalia.aem.service.impl.converter.home.checkinseatmapservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.GetSeatMapCheckinRequest;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd1.GetSeatMapRequest;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd1.Resource;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd2.CompartimentalClass;


@Component(immediate=true, metatype=false)
@Service(value=GetSeatMapCheckinRequestToGetSeatMapRequest.class)
public class GetSeatMapCheckinRequestToGetSeatMapRequest implements Converter<GetSeatMapCheckinRequest, GetSeatMapRequest> {
	
	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;
	
	@Reference
	private CheckinFlightDataToFlight checkinFlightDataConverter;


	@Override
	public GetSeatMapRequest convert(GetSeatMapCheckinRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		GetSeatMapRequest destination = null;

		if (source != null) {
			destination = objectFactory.createGetSeatMapRequest();

			destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			destination.setX003CResourceX003EKBackingField(Resource.WEB);
			
			
			if(source.getCompartimentalClass() != null){
				destination.setCompartimentalClass(CompartimentalClass.fromValue(source.getCompartimentalClass().value()));
			}
			destination.setEticket(objectFactory.createGetSeatMapRequestEticket(source.getEticket()));
			destination.setFlight(objectFactory.createGetSeatMapRequestFlight(checkinFlightDataConverter.convert(source.getFlight())));
			destination.setMarket(objectFactory.createGetSeatMapRequestMarket(source.getMarket()));
		}
		return destination;
	}

}
