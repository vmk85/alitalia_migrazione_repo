package com.alitalia.aem.service.impl.crmdatarest.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ResponseReader {

	public static String retrieveResponse(InputStream is) {
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String received;

		try {
			br = new BufferedReader(new InputStreamReader(is));
			while ((received = br.readLine()) != null) {
				sb.append(received);
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if (br != null) {
				try {
					br.close();
				}
				catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		String jsonString = sb.toString();
		return jsonString;
	}

}
