package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.NotifySocialLoginResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.NotifyLoginResponse;

@Component(immediate = true, metatype = false)
@Service(value = NotifyLoginResponseToNotifySocialLoginResponse.class)
public class NotifyLoginResponseToNotifySocialLoginResponse 
		implements Converter<NotifyLoginResponse, NotifySocialLoginResponse> {

	@Override
	public NotifySocialLoginResponse convert(NotifyLoginResponse source) {
		NotifySocialLoginResponse destination = null;

		if (source != null) {
			
			destination = new NotifySocialLoginResponse();
			
			destination.setResult(
					source.isResult());
			
		}

		return destination;
	}

}
