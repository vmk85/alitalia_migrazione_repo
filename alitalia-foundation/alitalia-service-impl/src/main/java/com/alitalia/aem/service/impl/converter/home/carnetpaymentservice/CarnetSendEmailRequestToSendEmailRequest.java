/**
 * 
 */
package com.alitalia.aem.service.impl.converter.home.carnetpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.CarnetSendEmailRequest;
import com.alitalia.aem.ws.carnet.paymentservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.carnet.paymentservice.xsd1.SendEmailRequest;


@Component(immediate=true, metatype=false)
@Service(value=CarnetSendEmailRequestToSendEmailRequest.class)
public class CarnetSendEmailRequestToSendEmailRequest implements Converter<CarnetSendEmailRequest, SendEmailRequest> {

	@Override
	public SendEmailRequest convert(CarnetSendEmailRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		SendEmailRequest destination = null;

		if (source != null) {
			
			destination = objectFactory.createSendEmailRequest();
			destination.setMailHtml(objectFactory.createSendEmailRequestMailHtml(source.getMailHtml()));
			destination.setMailTo(objectFactory.createSendEmailRequestMailTo(source.getMailTo()));

		}
		return destination;
	}
}
