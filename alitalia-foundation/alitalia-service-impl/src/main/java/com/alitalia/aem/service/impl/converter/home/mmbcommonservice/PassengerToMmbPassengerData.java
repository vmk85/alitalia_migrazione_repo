package com.alitalia.aem.service.impl.converter.home.mmbcommonservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.enumerations.ContactTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAQQTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbLinkTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbPassengerNoteEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbPassengerStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbUpdateSSREnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.common.data.home.mmb.MmbCouponData;
import com.alitalia.aem.common.data.home.mmb.MmbPassengerData;
import com.alitalia.aem.common.data.home.mmb.MmbPreferencesData;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.AQQType;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.ArrayOfCoupon;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.CompartimentalClass;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.Coupon;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.LinkType;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.Passenger;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.PassengerNote;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.PassengerStatus;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.PassengerType;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.UpdateSSR;
import com.alitalia.aem.ws.mmb.commonservice.xsd4.ContactType;

@Component(immediate=true, metatype=false)
@Service(value=PassengerToMmbPassengerData.class)
public class PassengerToMmbPassengerData implements Converter<Passenger, MmbPassengerData> {

	@Reference
	private ApisDataToMmbApisInfoData apisDataConverter;

	@Reference
	private BaggageOrderToMmbBaggageOrderData baggageOrderConverter;

	@Reference
	private FrequentFlyerCarrierToMmbFrequentFlyerCarrierData frequentFlyerCarrierConverter;

	@Reference
	private FrequentFlyerTypeToMmbFrequentFlyerTypeData frequentFlyerTypeConverter;

	@Reference
	private CouponToMmbCouponData couponConverter;

	@Override
	public MmbPassengerData convert(Passenger source) {
		MmbPassengerData destination = null;

		if (source != null) {
			destination = new MmbPassengerData();

			AQQType sourceAQQType = source.getX003CAuthorityPermissionX003EKBackingField();
			if (sourceAQQType != null)
				destination.setAuthorityPermission(MmbAQQTypeEnum.fromValue(sourceAQQType.value()));

			destination.setBaggageAllowance(source.getX003CBaggageAllowanceX003EKBackingField());
			destination.setBaggageAllowanceQuantity(source.getX003CBaggageAllowanceQuantityX003EKBackingField());
			destination.setBoardingPassRetrieveEmail(source.getX003CBoardingPassRetrieveEmailX003EKBackingField());
			destination.setBoardingPassRetrieveSms(source.getX003CBoardingPassRetrieveSmsX003EKBackingField());
			destination.setBoardingPassViaEmail(source.isX003CBoardingPassViaEmailX003EKBackingField());
			destination.setBoardingPassViaSms(source.isX003CBoardingPassViaSmsX003EKBackingField());
			destination.setComfortSeatFree(source.isX003CComfortSeatFreeX003EKBackingField());

			ContactType sourceContactType = source.getX003CContactTypeX003EKBackingField();
			if (sourceContactType != null)
				destination.setContactType(ContactTypeEnum.fromValue(sourceContactType.value()));

			destination.setCorporate(source.isX003CCorporateX003EKBackingField());
			destination.setEmail1(source.getX003CEmail1X003EKBackingField());
			destination.setEmail2(source.getX003CEmail2X003EKBackingField());
			destination.setEticket(source.getX003CEticketX003EKBackingField());
			destination.setEticketClass(source.getX003CEticketClassX003EKBackingField());
			destination.setFrequentFlyerCode(source.getX003CFrequentFlyerCodeX003EKBackingField());
			destination.setFrequentFlyerCustomerValue(source.getX003CFrequentFlyerCustomerValueX003EKBackingField());
			destination.setGender(source.getX003CGenderX003EKBackingField());
			destination.setId(source.getX003CIdX003EKBackingField());
			destination.setLastName(source.getX003CLastNameX003EKBackingField());

			LinkType sourceLinkType = source.getX003CLinkTypeX003EKBackingField();
			if (sourceLinkType != null)
				destination.setLinkType(MmbLinkTypeEnum.fromValue(sourceLinkType.value()));

			PassengerNote sourceNote = source.getX003CNoteX003EKBackingField();
			if (sourceNote != null)
				destination.setNote(MmbPassengerNoteEnum.fromValue(sourceNote.value()));

			destination.setName(source.getX003CNameX003EKBackingField());
			destination.setPaymentRetrieveEmail(source.getX003CPaymentRetrieveEmailX003EKBackingField());
			destination.setPaymentViaEmail(source.isX003CPaymentViaEmailX003EKBackingField());
			destination.setPnr(source.getX003CPnrX003EKBackingField());
			destination.setRouteId(source.getX003CRouteIdX003EKBackingField());
			destination.setSeat(source.getX003CSeatX003EKBackingField());
			destination.setSpecialFare(source.isX003CIsSpecialFareX003EKBackingField());

			CompartimentalClass sourceSeatClass = source.getX003CSeatClassX003EKBackingField();
			if (sourceSeatClass != null)
				destination.setSeatClass(MmbCompartimentalClassEnum.fromValue(sourceSeatClass.value()));

			destination.setSelected(source.isX003CSelectedX003EKBackingField());

			PassengerStatus sourceStatus = source.getX003CStatusX003EKBackingField();
			if (sourceStatus != null)
				destination.setStatus(MmbPassengerStatusEnum.fromValue(sourceStatus.value()));

			destination.setStatusNote(source.getX003CStatusNoteX003EKBackingField());
			destination.setTelephone1(source.getX003CTelephone1X003EKBackingField());
			destination.setTelephone2(source.getX003CTelephone2X003EKBackingField());
			destination.setTickets(null);
			destination.setTravelerRefNumber(source.getX003CTravelerRefNumberX003EKBackingField());

			PassengerType sourcePassengerType = source.getX003CTypeX003EKBackingField();
			if (sourcePassengerType != null)
				destination.setType(PassengerTypeEnum.fromValue(sourcePassengerType.value()));

			UpdateSSR sourceUpdateSsr = source.getX003CUpdateX003EKBackingField();
			if (sourceUpdateSsr != null)
				destination.setUpdate(MmbUpdateSSREnum.fromValue(sourceUpdateSsr.value()));

			destination.setUpdateFlightRefNumberRPH(source.getX003CUpdateFlightRefNumberRPHX003EKBackingField());

			destination.setApisData(apisDataConverter.convert(source.getX003CApisDataX003EKBackingField()));

			destination.setApisDataToUpdate(apisDataConverter.convert(
					source.getX003CApisDataToUpdateX003EKBackingField()));

			destination.setBaggageOrder(baggageOrderConverter.convert(
					source.getX003COrderX003EKBackingField()));

			destination.setFrequentFlyerCarrier(frequentFlyerCarrierConverter.convert(
					source.getX003CFrequentFlyerCarrierX003EKBackingField()));

			destination.setFrequentFlyerType(frequentFlyerTypeConverter.convert(
					source.getX003CFrequentFlyerTypeX003EKBackingField()));

			List<MmbCouponData> couponsList = null;
			ArrayOfCoupon sourceCoupunsList = source.getX003CCouponsX003EKBackingField();
			if (sourceCoupunsList != null &&
					sourceCoupunsList.getCoupon() != null &&
					!sourceCoupunsList.getCoupon().isEmpty()) {
				couponsList = new ArrayList<MmbCouponData>();
				for (Coupon sourceCoupon : sourceCoupunsList.getCoupon()) {
					couponsList.add(couponConverter.convert(sourceCoupon));
				}
				destination.setCoupons(couponsList);
			}

			MmbPreferencesData preferences = null;
			Object sourcePreferencesList = source.getX003CPreferencesX003EKBackingField();
			if (sourcePreferencesList != null) {
				preferences = new MmbPreferencesData();
				destination.setPreferences(preferences);
			}
		}

		return destination;
	}

}
