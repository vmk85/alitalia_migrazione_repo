package com.alitalia.aem.service.impl.converter.home.mmbancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryQuotationData;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.Quotations;

@Component(immediate=true, metatype=false)
@Service(value=QuotationsToMmbAncillaryQuotationData.class)
public class QuotationsToMmbAncillaryQuotationData implements Converter<Quotations, MmbAncillaryQuotationData> {

	@Override
	public MmbAncillaryQuotationData convert(Quotations source) {
		MmbAncillaryQuotationData destination = null;

		if (source != null) {
			destination = new MmbAncillaryQuotationData();

			destination.setMaxQuantity(source.getX003CMaxQuantityX003EKBackingField());
			destination.setMinQuantity(source.getX003CMinQuantityX003EKBackingField());
			destination.setPrice(source.getX003CPriceX003EKBackingField());
		}

		return destination;
	}

}
