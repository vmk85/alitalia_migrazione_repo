package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.*;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.*;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

@Component(immediate = true, metatype = false)
@Service(value = CheckEmailUserNameDuplicateResponseConverter.class)
public class CheckEmailUserNameDuplicateResponseConverter implements Converter<CheckEmailUsernameDuplicateResponse,SubscribeSACheckUserNameDuplicateResponse> {



	@Override
	public SubscribeSACheckUserNameDuplicateResponse convert(CheckEmailUsernameDuplicateResponse source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");

		SubscribeSACheckUserNameDuplicateResponse destination = new SubscribeSACheckUserNameDuplicateResponse();

		if (source != null)
			destination.setFlagNoUserNameDuplicate(source.isFlagNoEmailUsernameDuplicateOK());

		return destination;
	}

	public CheckEmailUsernameDuplicateResponse  convert(SubscribeSACheckUserNameDuplicateResponse source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");

		CheckEmailUsernameDuplicateResponse destination = new CheckEmailUsernameDuplicateResponse();

		if (source != null)
			destination.setFlagNoEmailUsernameDuplicateOK(source.isFlagNoUserNameDuplicate());

		return destination;
	}
}