package com.alitalia.aem.service.impl.converter.home.newsletterservice;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.NLUserData;
import com.alitalia.aem.common.messages.home.RegisterUserToAgencyNewsletterResponse;
import com.alitalia.aem.ws.news.newsletterservice.xsd1.RegisterUserResponse;
import com.alitalia.aem.ws.news.newsletterservice.xsd2.ArrayOfNLUser;

@Component(immediate = true, metatype = false)
@Service(value = RegisterUserResponseToRegisterUserToAgencyNewsletterResponse.class)
public class RegisterUserResponseToRegisterUserToAgencyNewsletterResponse implements Converter<RegisterUserResponse, RegisterUserToAgencyNewsletterResponse> {

	@Reference
	private ArrayOfNLUserToListOfNLUserData arrayOfNLUserConverter;
	
	@Override
	public RegisterUserToAgencyNewsletterResponse convert(RegisterUserResponse source) {
		RegisterUserToAgencyNewsletterResponse response = new RegisterUserToAgencyNewsletterResponse();
		
		String agencyId = source.getAgencyId().getValue();
		ArrayOfNLUser users = source.getUsers().getValue();
		
		List<NLUserData> usersData = arrayOfNLUserConverter.convert(users);
		response.setAgencyId(agencyId);
		response.setUsers(usersData);
		
		return response;
	}
}