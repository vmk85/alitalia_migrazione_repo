package com.alitalia.aem.service.impl.converter.home.checkinstaticdataservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.MmbAmericanStatesData;
import com.alitalia.aem.common.messages.home.WebCheckinStatesResponse;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.StaticDataResponseOfAmericanStatesUGS9ND5Y;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd2.AmericanStates;


@Component(immediate = true, metatype = false)
@Service(value = StaticDataResponseToWebCheckinStatesResponse.class)
public class StaticDataResponseToWebCheckinStatesResponse implements Converter<StaticDataResponseOfAmericanStatesUGS9ND5Y, WebCheckinStatesResponse> {

	@Reference
	private AmericanStatesToMmbAmericanStatesData americanStatesConverter;

	@Override
	public WebCheckinStatesResponse convert(StaticDataResponseOfAmericanStatesUGS9ND5Y source) {
		WebCheckinStatesResponse destination = new WebCheckinStatesResponse();

		List<MmbAmericanStatesData> americanStates = new ArrayList<>();
		if (source.getEntities()!=null && source.getEntities().getValue()!=null &&
				source.getEntities().getValue().getAmericanStates()!=null && source.getEntities().getValue().getAmericanStates().size()>0){
			List<AmericanStates> states = source.getEntities().getValue().getAmericanStates();
			for (AmericanStates state : states) {
				MmbAmericanStatesData mmbState = americanStatesConverter.convert(state);
				americanStates.add(mmbState);
			}
			destination.setAmericanStates(americanStates);
		}

		return destination;			
	}
}