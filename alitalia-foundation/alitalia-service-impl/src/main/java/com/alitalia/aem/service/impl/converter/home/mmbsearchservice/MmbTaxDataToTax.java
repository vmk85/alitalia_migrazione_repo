package com.alitalia.aem.service.impl.converter.home.mmbsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.MmbTaxData;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.Tax;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=MmbTaxDataToTax.class)
public class MmbTaxDataToTax implements Converter<MmbTaxData, Tax> {

	@Override
	public Tax convert(MmbTaxData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		Tax destination = null;

		if (source != null) {
			destination = objectFactory.createTax();

			destination.setX003CAmountX003EKBackingField(source.getAmount());
			destination.setX003CCodeX003EKBackingField(source.getCode());
			destination.setX003CCurrencyX003EKBackingField(source.getCurrency());

			String sourceId = source.getId();
			if (sourceId != null)
				destination.setX003CIdX003EKBackingField(Integer.parseInt(sourceId));
			else
				destination.setX003CIdX003EKBackingField(0);

			destination.setX003CNameX003EKBackingField(source.getName());
			destination.setX003CPassengerTypeQuantityX003EKBackingField(source.getPassengerTypeQuantity());
			destination.setX003CTotalAmountX003EKBackingField(source.getTotalAmount());
		}

		return destination;
	}

}
