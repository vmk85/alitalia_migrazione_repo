package com.alitalia.aem.service.impl.home;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.CanAddPassengerCheckinRequest;
import com.alitalia.aem.common.messages.home.CanAddPassengerCheckinResponse;
import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesRequest;
import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesResponse;
import com.alitalia.aem.common.messages.home.PassengerListCheckinRequest;
import com.alitalia.aem.common.messages.home.PassengerListCheckinResponse;
import com.alitalia.aem.common.messages.home.WebCheckinNewPassengerRequest;
import com.alitalia.aem.common.messages.home.WebCheckinNewPassengerResponse;
import com.alitalia.aem.service.api.home.WebCheckinPassengerService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.checkinpassengerservice.CanAddPassengerCheckinRequestToCanAddPassengerRequest;
import com.alitalia.aem.service.impl.converter.home.checkinpassengerservice.CanAddPassengerResponseToCanAddPassengerCheckinResponse;
import com.alitalia.aem.service.impl.converter.home.checkinpassengerservice.NewPassengerResponseToWebCheckinNewPassengerResponse;
import com.alitalia.aem.service.impl.converter.home.checkinpassengerservice.PassengerListCheckinRequestToPassengerListRequest;
import com.alitalia.aem.service.impl.converter.home.checkinpassengerservice.PassengerListResponseToPassengerListCheckinResponse;
import com.alitalia.aem.service.impl.converter.home.checkinpassengerservice.WebCheckinCheckFFCodeRequestToCheckFFCodeRequest;
import com.alitalia.aem.service.impl.converter.home.checkinpassengerservice.WebCheckinNewPassengerRequestToNewPassengerRequest;
import com.alitalia.aem.ws.checkin.passengerservice.CheckinPassengerServiceClient;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.CanAddPassengerRequest;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.CanAddPassengerResponse;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.CheckFFCodeRequest;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.CheckFFCodeResponse;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.NewPassengerRequest;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.NewPassengerResponse;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.PassengerListRequest;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.PassengerListResponse;

@Component(immediate=true, metatype=false)
@Service
public class SimpleWebCheckinPassengerService implements WebCheckinPassengerService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleWebCheckinPassengerService.class);

	@Reference
	private CheckinPassengerServiceClient checkinPassengerServiceClient;

	@Reference
	private PassengerListCheckinRequestToPassengerListRequest passengerListCheckinRequestConverter;

	@Reference
	private PassengerListResponseToPassengerListCheckinResponse passengerListResponseConverter;

	@Reference
	private CanAddPassengerCheckinRequestToCanAddPassengerRequest canAddPassengerCheckinRequestConverter;

	@Reference
	private CanAddPassengerResponseToCanAddPassengerCheckinResponse canAddPassengerResponseConverter;

	@Reference
	private NewPassengerResponseToWebCheckinNewPassengerResponse getNewPassengerResponseConverter;

	@Reference
	private WebCheckinNewPassengerRequestToNewPassengerRequest getNewPassengerRequestConverter;
	
	@Reference
	private WebCheckinCheckFFCodeRequestToCheckFFCodeRequest checkFFCodeRequestConverter;

	@Override
	public PassengerListCheckinResponse getPassengerList(PassengerListCheckinRequest request) {

		logger.debug("Executing method getPassengerList(). The request is {}", request);	

		try {
			PassengerListCheckinResponse response = new PassengerListCheckinResponse();
			PassengerListRequest serviceRequest = passengerListCheckinRequestConverter.convert(request);
			PassengerListResponse serviceResponse = checkinPassengerServiceClient.getPassengerList(serviceRequest);
			response = passengerListResponseConverter.convert(serviceResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			logger.debug("Executed method successfully getPassengerList(). The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getPassengerList().", e);
			throw new AlitaliaServiceException("Exception executing getPassengerList: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public CanAddPassengerCheckinResponse canAddPassenger(CanAddPassengerCheckinRequest request) {

		logger.debug("Executing method canAddPassenger(). The request is {}", request);	

		try {
			CanAddPassengerCheckinResponse response = new CanAddPassengerCheckinResponse();
			CanAddPassengerRequest serviceRequest = canAddPassengerCheckinRequestConverter.convert(request);
			CanAddPassengerResponse serviceResponse = checkinPassengerServiceClient.canAddPassenger(serviceRequest);
			response = canAddPassengerResponseConverter.convert(serviceResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			logger.debug("Executed method successfully canAddPassenger(). The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method canAddPassenger().", e);
			throw new AlitaliaServiceException("Exception executing canAddPassenger: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public WebCheckinNewPassengerResponse getNewPassenger(WebCheckinNewPassengerRequest request) {

		logger.debug("Executing method getNewPassenger(). The request is {}", request);	

		try {
			WebCheckinNewPassengerResponse response = new WebCheckinNewPassengerResponse();
			NewPassengerRequest serviceRequest = getNewPassengerRequestConverter.convert(request);
			NewPassengerResponse serviceResponse = checkinPassengerServiceClient.getNewPassenger(serviceRequest);
			response = getNewPassengerResponseConverter.convert(serviceResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			logger.debug("Executed method successfully getNewPassenger(). The response is {}", response);
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getNewPassenger().", e);
			throw new AlitaliaServiceException("Exception executing getNewPassenger: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public MmbCheckFrequentFlyerCodesResponse checkFFCode(MmbCheckFrequentFlyerCodesRequest request) {

		logger.debug("Executing method checkFFCode(). The request is {}", request);	
		try {
			MmbCheckFrequentFlyerCodesResponse response = new MmbCheckFrequentFlyerCodesResponse();
			CheckFFCodeRequest serviceRequest = checkFFCodeRequestConverter.convert(request);
			CheckFFCodeResponse serviceResponse = checkinPassengerServiceClient.checkFFCode(serviceRequest);
			if (serviceResponse != null &&
					serviceResponse.getWrongFfPassengerIds() != null &&
					serviceResponse.getWrongFfPassengerIds().getValue() != null &&
					serviceResponse.getWrongFfPassengerIds().getValue().getInt() != null &&
					!serviceResponse.getWrongFfPassengerIds().getValue().getInt().isEmpty()) {
				List<String> validFFCodes = new ArrayList<String>(); 
				for (Integer indexValidFFCode : serviceResponse.getWrongFfPassengerIds().getValue().getInt()) {
					validFFCodes.add(request.getFfCodesToCheck().get(indexValidFFCode).get(0));
				}
				response.setInvalidFFCodes(validFFCodes);
			}
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			logger.debug("Executed method successfully checkFFCode(). The response is {}", response);
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method checkFFCode().", e);
			throw new AlitaliaServiceException("Exception executing checkFFCode: Sid ["+request.getSid()+"]" , e);
		}
	}
}