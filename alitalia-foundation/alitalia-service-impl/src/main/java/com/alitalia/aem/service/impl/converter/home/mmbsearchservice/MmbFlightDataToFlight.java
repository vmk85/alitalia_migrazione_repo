package com.alitalia.aem.service.impl.converter.home.mmbsearchservice;

import java.util.Calendar;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferenceCardinality;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.enumerations.MmbApisTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbFlightStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbLegTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.MmbPassengerData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.ApisType;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.ArrayOfPassenger;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.Cabin;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.CompartimentalClass;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.Flight;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.FlightStatus;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.LegType;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.mmb.searchservice.xsd4.RouteType;

@Component(immediate=true, metatype=false)
@Service(value=MmbFlightDataToFlight.class)
public class MmbFlightDataToFlight implements Converter<MmbFlightData, Flight> {

	@Reference
	private MmbDeepLinkDataToDeepLink mmbDeepLinkDataConverter;

	@Reference
	private MmbAirportDataToAirport mmbAirportDataConverter;

	// E' stata inserita la cardianlita' opzionale perche' c'e' un ciclo
	// di riferimenti fra Passenger, Flight e Coupon che altrimenti impedisce
	// che salgano i service component
	@Reference(policy=ReferencePolicy.DYNAMIC, cardinality=ReferenceCardinality.OPTIONAL_UNARY)
	private volatile MmbPassengerDataToPassenger mmbPassengerDataConverter;

	@Override
	public Flight convert(MmbFlightData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		Flight destination = null;

		if (source != null) {
			destination = objectFactory.createFlight();

			MmbApisTypeEnum sourceApisTypeReq = source.getApisTypeRequired();
			if (sourceApisTypeReq != null)
				destination.setX003CApisTypeRequiredX003EKBackingField(ApisType.fromValue(sourceApisTypeReq.value()));

			Calendar sourceArrivalDate = (Calendar) source.getArrivalDateTime().clone();
			sourceArrivalDate.set(Calendar.HOUR, 0);
			sourceArrivalDate.set(Calendar.MINUTE, 0);
			sourceArrivalDate.set(Calendar.SECOND, 0);
			sourceArrivalDate.set(Calendar.MILLISECOND, 0);
			destination.setX003CArrivalDateX003EKBackingField(XsdConvertUtils.toXMLGregorianCalendar(sourceArrivalDate));

			Calendar sourceArrivalTime = (Calendar) source.getArrivalDateTime().clone();
			sourceArrivalTime.set(Calendar.YEAR, 1);
			sourceArrivalTime.set(Calendar.MONTH, 0);
			sourceArrivalTime.set(Calendar.DAY_OF_MONTH, 1);
			destination.setX003CArrivalTimeX003EKBackingField(XsdConvertUtils.toXMLGregorianCalendar(sourceArrivalTime));

			destination.setX003CBaggageAllowX003EKBackingField(source.getBaggageAllow());

			String sourceCabin = source.getCabin().value();
			if (sourceCabin != null)
				destination.setX003CCabinX003EKBackingField(Cabin.fromValue(sourceCabin));

			destination.setX003CCarrierX003EKBackingField(source.getCarrier());
			destination.setX003CComfortSeatFareX003EKBackingField(source.getComfortSeatFare());
			destination.setX003CComfortSeatPaidX003EKBackingField(source.isComfortSeatPaid());

			Calendar sourceDepartureDate = (Calendar) source.getDepartureDateTime().clone();
			sourceDepartureDate.set(Calendar.HOUR, 0);
			sourceDepartureDate.set(Calendar.MINUTE, 0);
			sourceDepartureDate.set(Calendar.SECOND, 0);
			sourceDepartureDate.set(Calendar.MILLISECOND, 0);
			destination.setX003CDepartureDateX003EKBackingField(XsdConvertUtils.toXMLGregorianCalendar(sourceDepartureDate));

			Calendar sourceDepartureTime = (Calendar) source.getDepartureDateTime().clone();
			sourceDepartureTime.set(Calendar.YEAR, 1);
			sourceDepartureTime.set(Calendar.MONTH, 1);
			sourceDepartureTime.set(Calendar.DAY_OF_MONTH, 1);
			destination.setX003CDepartureTimeX003EKBackingField(XsdConvertUtils.toXMLGregorianCalendar(sourceDepartureTime));

			destination.setX003CEnabledSeatMapX003EKBackingField(source.isEnabledSeatMap());
			destination.setX003CEticketClassX003EKBackingField(source.getEticketClass());
			destination.setX003CEticketX003EKBackingField(source.getEticket());
			destination.setX003CExtraBaggageX003EKBackingField(source.getExtraBaggage());
			destination.setX003CFareClassX003EKBackingField(source.getFareClass());
			destination.setX003CFirstFlightX003EKBackingField(source.isFirstFlight());
			destination.setX003CFlightNumberX003EKBackingField(source.getFlightNumber());
			destination.setX003CFromCityX003EKBackingField(source.getFromCity());
			destination.setX003CFromTerminalX003EKBackingField(source.getFromTerminal());
			destination.setX003CHasComfortSeatX003EKBackingField(source.hasComfortSeat());
			destination.setX003CIdX003EKBackingField(source.getId());
			destination.setX003CIndexX003EKBackingField(source.getIndex());
			destination.setX003CIsMiniFareX003EKBackingField(source.isMiniFare());

			MmbLegTypeEnum sourceLegType = source.getLegType();
			if (sourceLegType != null)
				destination.setX003CLegTypeX003EKBackingField(LegType.fromValue(sourceLegType.value()));

			destination.setX003CMultitrattaX003EKBackingField(source.getMultitratta());
			destination.setX003COperatingCarrierX003EKBackingField(source.getOperatingCarrier());
			destination.setX003COperatingFlightNumberX003EKBackingField(source.getOperatingFlightNumber());
			destination.setX003CPnrX003EKBackingField(source.getPnr());
			destination.setX003CRouteIdX003EKBackingField(source.getRouteId());
			destination.setX003CRPHX003EKBackingField(source.getRph());

			MmbCompartimentalClassEnum sourceSeatClass = source.getSeatClass();
			if (sourceSeatClass != null)
				destination.setX003CSeatClassX003EKBackingField(CompartimentalClass.fromValue(sourceSeatClass.value()));

			MmbFlightStatusEnum sourceStatus = source.getStatus();
			if (sourceStatus != null)
				destination.setX003CStatusX003EKBackingField(FlightStatus.fromValue(sourceStatus.value()));

			destination.setX003CToCityX003EKBackingField(source.getToCity());
			destination.setX003CToTerminalX003EKBackingField(source.getToTerminal());

			RouteTypeEnum sourceType = source.getType();
			if (sourceType != null)
				destination.setX003CTypeX003EKBackingField(RouteType.fromValue(sourceType.value()));

			destination.setX003CDeepLinkCodeX003EKBackingField(mmbDeepLinkDataConverter.convert(source.getDeepLinkCode()));

			destination.setX003CFromX003EKBackingField(mmbAirportDataConverter.convert(source.getFrom()));

			ArrayOfPassenger passengersList = objectFactory.createArrayOfPassenger();
			for (MmbPassengerData passengerData : source.getPassengers()) {
				passengersList.getPassenger().add(mmbPassengerDataConverter.convert(passengerData));
			}
			destination.setX003CPassengersX003EKBackingField(passengersList);

			destination.setX003CToX003EKBackingField(mmbAirportDataConverter.convert(source.getTo()));
		}

		return destination;
	}

}
