package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSaleFareTotalData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionSaleFareTotal;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSaleFareTotalDataToRTDSSaleFareTotal.class)
public class RTDSSaleFareTotalDataToRTDSSaleFareTotal implements
		Converter<ResultTicketingDetailSolutionSaleFareTotalData,
					ResultTicketingDetailSolutionSaleFareTotal> {

	@Override
	public ResultTicketingDetailSolutionSaleFareTotal convert(ResultTicketingDetailSolutionSaleFareTotalData source) {
		ResultTicketingDetailSolutionSaleFareTotal destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionSaleFareTotal();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
