package com.alitalia.aem.service.impl.converter.home.mmbsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.data.home.mmb.MmbAirportData;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.Airport;
import com.alitalia.aem.ws.mmb.searchservice.xsd4.AreaValue;

@Component(immediate=true, metatype=false)
@Service(value=AirportToMmbAirportData.class)
public class AirportToMmbAirportData implements Converter<Airport, MmbAirportData> {

	@Override
	public MmbAirportData convert(Airport source) {
		MmbAirportData destination = null;

		if (source != null) {
			destination = new MmbAirportData();

			destination.setCode(source.getX003CCodeX003EKBackingField());
			destination.setId(source.getX003CIdX003EKBackingField());

			AreaValue sourceArea = source.getX003CAreaX003EKBackingField();
			if (sourceArea != null)
				destination.setArea(AreaValueEnum.fromValue(sourceArea.value()));

			destination.setCityCode(source.getX003CCityNameX003EKBackingField());
			destination.setCity(source.getX003CCityX003EKBackingField());
			destination.setCountryCode(source.getX003CCountryCodeX003EKBackingField());
			destination.setDescription(source.getX003CNameX003EKBackingField());
			destination.setName(source.getX003CAirportNameX003EKBackingField());
		}

		return destination;
	}

}
