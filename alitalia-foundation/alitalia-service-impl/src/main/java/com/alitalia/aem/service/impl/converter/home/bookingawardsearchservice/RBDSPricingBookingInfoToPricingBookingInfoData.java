//2
package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetailsSolutionPricingBookingInfo;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingBookingInfoToPricingBookingInfoData.class)
public class RBDSPricingBookingInfoToPricingBookingInfoData implements
		Converter<ResultBookingDetailsSolutionPricingBookingInfo, ResultBookingDetailsSolutionPricingBookingInfoData> {

	@Reference
	private RBDSPricingBookingInfoFareToPricingBookingInfoFareData rbdsPricingBookingInfoFareConverter;
	
	@Reference
	private RBDSPricingBookingInfoSegmentToPricingBookingInfoSegmentData rbdsPricingBookingInfoSegmentConverter;
	
	@Override
	public ResultBookingDetailsSolutionPricingBookingInfoData convert(
			ResultBookingDetailsSolutionPricingBookingInfo source) {
		
		ResultBookingDetailsSolutionPricingBookingInfoData destination = null;
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingBookingInfoData();
			destination.setBookingCodeCountField(source.getBookingCodeCountField());
			destination.setBookingCodeField(source.getBookingCodeField());
			destination.setCabinField(source.getCabinField());
			destination.setFareField(rbdsPricingBookingInfoFareConverter.convert(source.getFareField()));
			destination.setSegmentField(rbdsPricingBookingInfoSegmentConverter.convert(source.getSegmentField()));
			destination.setMarriedSegmentIndexField(source.getMarriedSegmentIndexField());
			destination.setMarriedSegmentIndexFieldSpecified(source.isMarriedSegmentIndexFieldSpecified());
			
			
		}
		
		return destination;
		
	}

	public ResultBookingDetailsSolutionPricingBookingInfoData convert(
			com.alitalia.aem.ws.bookingaward.searchservice.xsd13.ResultBookingDetailsSolutionPricingBookingInfo source) {
		
		ResultBookingDetailsSolutionPricingBookingInfoData destination = null;
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingBookingInfoData();
			destination.setBookingCodeCountField(source.getBookingCodeCountField());
			destination.setBookingCodeField(source.getBookingCodeField());
			destination.setCabinField(source.getCabinField());
			destination.setFareField(rbdsPricingBookingInfoFareConverter.convert(source.getFareField()));
			destination.setSegmentField(rbdsPricingBookingInfoSegmentConverter.convert(source.getSegmentField()));
			destination.setMarriedSegmentIndexField(source.getMarriedSegmentIndexField());
			destination.setMarriedSegmentIndexFieldSpecified(source.isMarriedSegmentIndexFieldSpecified());
			
			
		}
		
		return destination;
		
	}
}
