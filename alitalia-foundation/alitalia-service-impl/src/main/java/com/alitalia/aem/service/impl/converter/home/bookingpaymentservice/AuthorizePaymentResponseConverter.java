package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.BaseResponse;
import com.alitalia.aem.ws.booking.paymentservice.xsd1.AuthorizePaymentResponse;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.InsurancePolicy;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ProcessInfo;

@Component(metatype=false, immediate=true)
@Service(value=AuthorizePaymentResponseConverter.class)
public class AuthorizePaymentResponseConverter extends BaseResponse implements Converter<AuthorizePaymentResponse, com.alitalia.aem.common.messages.home.AuthorizePaymentResponse> {

	Logger log = LoggerFactory.getLogger(AuthorizePaymentResponseConverter.class);
	
	@Reference
	InsurancePolicyToInsurancePolicyData insuranceConverter;
	
	@Reference
	ProcessInfoToPaymentProcessInfoData processConverter;
	
	@Reference
	private BookingPaymentServiceJAXBContextFactory jaxbContextFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public com.alitalia.aem.common.messages.home.AuthorizePaymentResponse convert(AuthorizePaymentResponse source) {
		com.alitalia.aem.common.messages.home.AuthorizePaymentResponse response = new com.alitalia.aem.common.messages.home.AuthorizePaymentResponse();
		
		InsurancePolicy castedInsurncePolicy = null;
		if (source.getInsurance() != null &&
				source.getInsurance().getValue() != null) {
			try {
				Unmarshaller insurncePolicyUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
				JAXBElement<InsurancePolicy> unmarshalledObj = null;
				unmarshalledObj = (JAXBElement<InsurancePolicy>) insurncePolicyUnmarshaller.unmarshal((Node) source.getInsurance().getValue());
				castedInsurncePolicy = unmarshalledObj.getValue();
			} catch (JAXBException e) {
				log.error("Impossibile convertire il process di tipo InsurancePolicy", e);
			}
		}
		response.setInsurance(insuranceConverter.convert(castedInsurncePolicy));
		
		ProcessInfo castedinfoProcess = null;
		try {
			Unmarshaller infoprocessUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			JAXBElement<ProcessInfo> unmarshalledObj = null;
			unmarshalledObj = (JAXBElement<ProcessInfo>) infoprocessUnmarshaller.unmarshal((Node) source.getProcess().getValue());
			castedinfoProcess = unmarshalledObj.getValue();
		} catch (JAXBException e) {
			log.error("Impossibile convertire il process di tipo ProcessInfo", e);
		}
		response.setProcess(processConverter.convert(castedinfoProcess));
		response.setPnr(source.getPNR().getValue());
		response.setCookie(source.getCookie().getValue());
		response.setExecute(source.getExecution().getValue());
		response.setSabreGateWayAuthToken(source.getSabreGateWayAuthToken().getValue());
		
		return response;
	}

}
