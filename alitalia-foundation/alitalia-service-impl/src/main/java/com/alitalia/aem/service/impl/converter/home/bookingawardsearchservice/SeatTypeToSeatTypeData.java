package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.SeatTypeData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.SeatType;

@Component(immediate=true, metatype=false)
@Service(value=SeatTypeToSeatTypeData.class)
public class SeatTypeToSeatTypeData implements Converter<SeatType, SeatTypeData> {

	@Override
	public SeatTypeData convert(SeatType source) {
		
		SeatTypeData destination = null;
		
		if (source != null) {
		
			destination = new SeatTypeData();
			
			if (source.getCode() != null) {
				destination.setCode(
						source.getCode().getValue());
			}
			
			if (source.getDescription() != null) {
				destination.setDescription(
						source.getDescription().getValue());
			}
			
		}
		
		return destination;
		
	}

}
