package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowanceData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowance;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingCarryOnBaggageAllowanceFreeBaggageAllowanceDataToRTDSPricingCarryOnBaggageAllowanceFreeBaggageAllowance.class)
public class RTDSPricingCarryOnBaggageAllowanceFreeBaggageAllowanceDataToRTDSPricingCarryOnBaggageAllowanceFreeBaggageAllowance implements
		Converter<ResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowanceData, 
					ResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowance> {

	@Override
	public ResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowance convert(
			ResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowanceData source) {
		ResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowance destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowance();

			destination.setBagDescriptorField(null);

			destination.setKilosField(source.getKilosField());
			destination.setKilosFieldSpecified(source.isKilosFieldSpecified());
			destination.setKilosPerPieceField(source.getKilosPerPieceField());
			destination.setKilosPerPieceFieldSpecified(source.isKilosPerPieceFieldSpecified());
			destination.setPiecesField(source.getPiecesField());
			destination.setPiecesFieldSpecified(source.isPiecesFieldSpecified());
			destination.setPoundsField(source.getPoundsField());
			destination.setPoundsFieldSpecified(source.isPoundsFieldSpecified());
		}

		return destination;
	}

}
