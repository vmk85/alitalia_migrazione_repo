package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.FlightDetailsData;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.FlightDetails;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ObjectFactory;

@Component(immediate = true, metatype = false)
@Service(value = FlightDetailsDataToFlightDetails.class)
public class FlightDetailsDataToFlightDetails implements Converter<FlightDetailsData, FlightDetails> {

	@Override
	public FlightDetails convert(FlightDetailsData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		FlightDetails destination = objectFactory.createFlightDetails();

		destination.setAeromobile(objectFactory.createFlightDetailsAeromobile(source.getAeromobile()));
		destination.setArrivalTerminal(objectFactory.createFlightDetailsArrivalTerminal(source.getArrivalTerminal()));
		destination.setDepartureTerminal(objectFactory.createFlightDetailsDepartureTerminal(source.getDepartureTerminal()));
		destination.setDistance(objectFactory.createFlightDetailsDistance(source.getDistance()));
		destination.setOtherInformation(objectFactory.createFlightDetailsOtherInformation(source.getOtherInformation()));

		return destination;
	}

}
