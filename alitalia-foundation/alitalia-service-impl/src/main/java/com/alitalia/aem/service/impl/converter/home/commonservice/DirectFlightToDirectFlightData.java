package com.alitalia.aem.service.impl.converter.home.commonservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.BaggageAllowanceData;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.FlightTypeEnum;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.commonservice.xsd2.ArrayOfDictionaryItem;
import com.alitalia.aem.ws.booking.commonservice.xsd2.BaggageAllowance;
import com.alitalia.aem.ws.booking.commonservice.xsd2.Brand;
import com.alitalia.aem.ws.booking.commonservice.xsd2.DirectFlight;
import com.alitalia.aem.ws.booking.commonservice.xsd2.FlightDetails;
import com.alitalia.aem.ws.booking.commonservice.xsd3.ArrayOfanyType;
import com.alitalia.aem.ws.booking.commonservice.xsd3.ArrayOfstring;

@Component(immediate=true, metatype=false)
@Service(value=DirectFlightToDirectFlightData.class)
public class DirectFlightToDirectFlightData implements Converter<DirectFlight, DirectFlightData> {

	@Reference
	private BookingSearchAirportToAirportData airportConverter;

	@Reference
	private BaggageAllowanceToBaggageAllowanceData baggageAllowanceConverter; 

	@Reference
	private BrandToBrandData brandConverter;

	@Reference
	private FlightDetailsToFlightDetailsData flightDetailsConverter;

	@Reference
	private ArrayOfDictionaryItemToPropertiesData arrayOfDictionaryItemConverter;
	
	@Reference
	private BookingCommonServiceJAXBContextFactory jaxbContextFactory;

	@Override
	public DirectFlightData convert(DirectFlight source) {
		DirectFlightData destination = new DirectFlightData();

		if (source.getCabin() != null)
			destination.setCabin(CabinEnum.fromValue(source.getCabin().value()));
		
		destination.setCarrier(source.getCarrier().getValue());
		destination.setCarrierForLogo(source.getCarrierForLogo().getValue());
		Duration duration = source.getDuration();
		if (duration != null) {
			destination.setDurationHour(duration.getDays() * 24 + duration.getHours());
			destination.setDurationMinutes(duration.getMinutes());
		}
		destination.setEnabledSeatMap(source.getEnabledSeatMap().getValue());
		destination.setFlightNumber(source.getFlightNumber().getValue());
		destination.setFlightType(FlightTypeEnum.fromValue(source.getType().value()));
		destination.setMarriageGroup(source.getMarriageGroup().getValue());
		destination.setMileage(source.getMileage());
		destination.setOperationalCarrier(source.getOperationalCarrier().getValue());
		destination.setOperationalFlightNumber(source.getOperationalFlightNumber().getValue());
		destination.setOperationalFlightText(source.getOperationalFlightText().getValue());
		destination.setStopOver(source.isStopOver());
		destination.setUrlConditions(source.getUrlConditions().getValue());

		XMLGregorianCalendar arrivalDate = source.getArrivalDate();
		if (arrivalDate != null)
			destination.setArrivalDate(XsdConvertUtils.parseCalendar(arrivalDate));
		XMLGregorianCalendar departureDate = source.getDepartureDate();
		if (departureDate != null)
			destination.setDepartureDate(XsdConvertUtils.parseCalendar(departureDate));

		ArrayList<String> compartimentalClass = new ArrayList<String>();
		ArrayOfstring sourceCompartimentalClass = source.getCompartimentalClass().getValue();
		if (sourceCompartimentalClass != null) {
			for (String compClassSource : sourceCompartimentalClass.getString() ) 
				compartimentalClass.add(compClassSource);
		}
		destination.setCompartimentalClass(compartimentalClass);

		ArrayList<BaggageAllowanceData> baggageAllowance = new ArrayList<BaggageAllowanceData>();
		ArrayOfanyType sourceBaggageAllowance = source.getBaggageAllowanceList().getValue();
		if (sourceBaggageAllowance != null) {
			Unmarshaller unmarshaller = null;
			try {
				unmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			} catch (JAXBException e) {
				throw new RuntimeException(e);
			}
			List<Object> sourceBaggageAllow = sourceBaggageAllowance.getAnyType();
			for (Object baggageAllow : sourceBaggageAllow) {
				BaggageAllowance castedBaggageAllowance = null;
				if (baggageAllow instanceof BaggageAllowance) {
					castedBaggageAllowance = (BaggageAllowance) baggageAllow;
					BaggageAllowanceData allowanceData = baggageAllowanceConverter.convert(castedBaggageAllowance);
					baggageAllowance.add(allowanceData);
				} else {
					try {
						@SuppressWarnings("unchecked")
						JAXBElement<BaggageAllowance> unmarshalled = 
								(JAXBElement<BaggageAllowance>) unmarshaller.unmarshal((Node) baggageAllow);
						castedBaggageAllowance = unmarshalled.getValue();
					} catch (JAXBException e) {
						throw new RuntimeException(e);
					}
				}
			}
		}
		destination.setBaggageAllowanceList(baggageAllowance);

		ArrayList<BrandData> brands = new ArrayList<BrandData>();
		ArrayOfanyType sourceBrands = source.getBrands().getValue();
		if (sourceBrands != null) {
			Unmarshaller unmarshaller = null;
			try {
				unmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			} catch (JAXBException e) {
				throw new RuntimeException(e);
			}
			List<Object> brandsSource = sourceBrands.getAnyType();
			for (Object brand : brandsSource) {
				Brand castedBrand = null;
				if (brand instanceof Brand) {
					castedBrand = (Brand) brand;
				} else {
					try {
						@SuppressWarnings("unchecked")
						JAXBElement<Brand> unmarshalled = 
								(JAXBElement<Brand>) unmarshaller.unmarshal((Node) brand);
						castedBrand = unmarshalled.getValue();
					} catch (JAXBException e) {
						throw new RuntimeException(e);
					}
				}
				BrandData brandData = brandConverter.convert(castedBrand);
				brands.add(brandData);
			}
		}
		destination.setBrands(brands);

		FlightDetails sourceFlightDetails = source.getDetails().getValue();
		if (sourceFlightDetails != null)
			destination.setDetails(flightDetailsConverter.convert(sourceFlightDetails));
		else
			destination.setDetails(null);

		destination.setFrom(airportConverter.convert(source.getFrom().getValue()));

		JAXBElement<ArrayOfDictionaryItem> sourceProperties = source.getProperties();
		if (sourceProperties != null && sourceProperties.getValue() != null)
			destination.setProperties(arrayOfDictionaryItemConverter.convert(sourceProperties.getValue()));
		else
			destination.setProperties(null);

		destination.setTo(airportConverter.convert(source.getTo().getValue()));

		return destination;
	}
}
