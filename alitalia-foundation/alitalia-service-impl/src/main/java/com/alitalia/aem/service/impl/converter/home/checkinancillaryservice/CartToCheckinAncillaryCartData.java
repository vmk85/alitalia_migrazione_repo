package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryCartData;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryPassengerData;
import com.alitalia.aem.common.data.home.enumerations.MmbFaultExceptionMessageCodeEnum;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryFlightData;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.FaultExceptionMessageCode;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.Ancillaries;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ArrayOfAncillaries;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ArrayOfFlight;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.Cart;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.Flight;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.Passenger;


@Component(immediate=true, metatype=false)
@Service(value=CartToCheckinAncillaryCartData.class)
public class CartToCheckinAncillaryCartData implements Converter<Cart, CheckinAncillaryCartData> {

	@Reference
	private AncillariesToMmbAncillaryData ancillariesConverter;

	@Reference
	private FlightToMmbAncillaryFlightData flightConverter;

	@Reference
	private PassengerToCheckinAncillaryPassengerData passengerConverter;

	@Override
	public CheckinAncillaryCartData convert(Cart source) {
		CheckinAncillaryCartData destination = null;

		if (source != null) {
			destination = new CheckinAncillaryCartData();

			destination.setId(source.getX003CIdX003EKBackingField());

			FaultExceptionMessageCode sourceMessageCode = source.getX003CMessageCodeX003EKBackingField();
			if (sourceMessageCode != null)
				destination.setMessageCode(MmbFaultExceptionMessageCodeEnum.fromValue(sourceMessageCode.value()));

			destination.setMessageCodeDetail(source.getX003CMessageCodeDetailX003EKBackingField());

			List<MmbAncillaryData> ancillaries = null;
			if (source.getX003CAncillariesX003EKBackingField() != null &&
					source.getX003CAncillariesX003EKBackingField().getAncillaries() != null &&
					!source.getX003CAncillariesX003EKBackingField().getAncillaries().isEmpty()) {
				ancillaries = new ArrayList<MmbAncillaryData>();
				ArrayOfAncillaries sourceAncillaries = source.getX003CAncillariesX003EKBackingField();
				for(Ancillaries sourceAncillary : sourceAncillaries.getAncillaries()) {
					ancillaries.add(ancillariesConverter.convert(sourceAncillary));
				}
			}
			destination.setAncillaries(ancillaries);

			List<MmbAncillaryFlightData> flights = null;
			if (source.getX003CFlightsX003EKBackingField() != null &&
					source.getX003CFlightsX003EKBackingField().getFlight() != null &&
					!source.getX003CFlightsX003EKBackingField().getFlight().isEmpty()) {
				flights = new ArrayList<MmbAncillaryFlightData>();
				ArrayOfFlight sourceFlights = source.getX003CFlightsX003EKBackingField();
				for (Flight sourceFlight : sourceFlights.getFlight()) {
					flights.add(flightConverter.convert(sourceFlight));
				}
			}
			destination.setFlights(flights);

			List<CheckinAncillaryPassengerData> passengers = null;
			if (source.getX003CPassengersX003EKBackingField() != null &&
					source.getX003CPassengersX003EKBackingField().getPassenger() != null &&
					!source.getX003CPassengersX003EKBackingField().getPassenger().isEmpty()) {
				passengers = new ArrayList<CheckinAncillaryPassengerData>();
				List<Passenger> sourcePassengersList = source.getX003CPassengersX003EKBackingField().getPassenger();
				for (Passenger sourcePassenger : sourcePassengersList) 
					passengers.add(passengerConverter.convert(sourcePassenger));
			}
			destination.setPassengers(passengers);
		}

		return destination;
	}

}
