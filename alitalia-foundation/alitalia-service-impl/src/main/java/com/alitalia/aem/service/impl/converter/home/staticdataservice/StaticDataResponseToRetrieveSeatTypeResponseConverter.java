package com.alitalia.aem.service.impl.converter.home.staticdataservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.SeatTypeData;
import com.alitalia.aem.common.messages.home.RetrieveSeatTypeResponse;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.StaticDataResponse;
import com.alitalia.aem.ws.booking.staticdataservice.xsd5.SeatType;

@Component(immediate=true, metatype=false)
@Service(value=StaticDataResponseToRetrieveSeatTypeResponseConverter.class)
public class StaticDataResponseToRetrieveSeatTypeResponseConverter implements Converter<StaticDataResponse, RetrieveSeatTypeResponse> {

	private static final Logger logger = LoggerFactory.getLogger(StaticDataResponseToRetrieveSeatTypeResponseConverter.class);

	@Reference
	private SeatTypeToSeatTypeData seatTypeCoverter;

	@Reference
	private StaticDataServiceJAXBContextFactory jaxbContextFactory;

	@SuppressWarnings("unchecked")
	@Override
	public RetrieveSeatTypeResponse convert(StaticDataResponse source) {
		RetrieveSeatTypeResponse destination = new RetrieveSeatTypeResponse();
		try {
			List<SeatTypeData> seatTypesData = new ArrayList<>();
			
			Unmarshaller seatTypeUnmashaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			JAXBElement<SeatType> unmarshalledObj = null;

			List<Object> seatTypes = source.getEntities().getValue().getAnyType();
			for(Object seatType: seatTypes){
				unmarshalledObj = (JAXBElement<SeatType>) seatTypeUnmashaller.unmarshal((Node) seatType);
				SeatType castedSeatType = unmarshalledObj.getValue();
				SeatTypeData seatTypeData = seatTypeCoverter.convert(castedSeatType);
				seatTypesData.add(seatTypeData);
			}
			
			destination.setSeatTypesData(seatTypesData);
			
		} catch (JAXBException e) {
			logger.error("Error instantiating JAXBContext/Unmarshaller: {}", e);
		} finally {
			logger.info("Conversion executed successfully");
		}
		return destination;			
	}
}