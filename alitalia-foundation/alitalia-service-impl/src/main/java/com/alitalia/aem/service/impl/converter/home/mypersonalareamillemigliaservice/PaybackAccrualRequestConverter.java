package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.MMPaybackAccrualRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.PaybackAccrualRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd5.FirstLastNameVerificationDataType;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd5.MemberAliasAuthenticationType;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd5.MemberIdentificationType;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd5.TicketNumberType;

@Component(immediate = true, metatype = false)
@Service(value = PaybackAccrualRequestConverter.class)
public class PaybackAccrualRequestConverter implements Converter<MMPaybackAccrualRequest, PaybackAccrualRequest> {
	
	@Override
	public PaybackAccrualRequest convert(MMPaybackAccrualRequest source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		
		ObjectFactory objectFactory = new ObjectFactory();
		com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd5.ObjectFactory objectFactory5 = new com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd5.ObjectFactory();
		
		PaybackAccrualRequest destination = objectFactory.createPaybackAccrualRequest();
		
		FirstLastNameVerificationDataType verificationDataType = objectFactory5.createFirstLastNameVerificationDataType();
		verificationDataType.setMemberVerificationFirstName(objectFactory5.createFirstLastNameVerificationDataTypeMemberVerificationFirstName(source.getMemberVerificationFirstName()));
		verificationDataType.setMemberVerificationLastName(objectFactory5.createFirstLastNameVerificationDataTypeMemberVerificationLastName(source.getMemberVerificationLastName()));
		destination.setCustomerFirstLastName(objectFactory.createPaybackAccrualRequestCustomerFirstLastName(verificationDataType));
		
		MemberAliasAuthenticationType authenticationType = objectFactory5.createMemberAliasAuthenticationType();
		MemberIdentificationType identificationType = objectFactory5.createMemberIdentificationType();
		identificationType.setAlias(objectFactory5.createMemberIdentificationTypeAlias(source.getPaybackCardNumber() != null ? source.getPaybackCardNumber().getAlias() : null));
		identificationType.setAliasType(source.getPaybackCardNumber() != null ? source.getPaybackCardNumber().getAliasType() : null);
		authenticationType.setIdentification(objectFactory5.createMemberIdentificationType(identificationType));
		destination.setPaybackCardNumber(objectFactory.createPaybackAccrualRequestPaybackCardNumber(authenticationType));
		
		TicketNumberType ticketNumber = objectFactory5.createTicketNumberType();
		ticketNumber.setAirlineCode(objectFactory5.createTicketNumberTypeAirlineCode(source.getTicketNumber() != null ? source.getTicketNumber().getAirlineCode() : null));
		ticketNumber.setTicketNumberId(objectFactory5.createTicketNumberTypeTicketNumberId(source.getTicketNumber() != null ? source.getTicketNumber().getTicketNumberId() : null));
		destination.setTicketNumber(objectFactory.createPaybackAccrualRequestTicketNumber(ticketNumber));

		destination.setUsername(objectFactory.createPaybackAccrualRequestUsername(source.getUsername()));
				
		return destination;
	}
}