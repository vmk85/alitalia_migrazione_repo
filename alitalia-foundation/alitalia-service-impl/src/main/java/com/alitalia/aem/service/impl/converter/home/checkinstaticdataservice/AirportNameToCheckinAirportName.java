package com.alitalia.aem.service.impl.converter.home.checkinstaticdataservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinAirportName;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd2.AirportName;

@Component(immediate=true, metatype=false)
@Service(value=AirportNameToCheckinAirportName.class)
public class AirportNameToCheckinAirportName implements Converter<AirportName, CheckinAirportName> {

	@Override
	public CheckinAirportName convert(AirportName source) {
		CheckinAirportName destination = null;

		if (source != null) {
			destination = new CheckinAirportName();
			destination.setCountryCode(source.getX003CCountryCodeX003EKBackingField());
			destination.setDisplayName(source.getX003CDisplayNameX003EKBackingField());
			destination.setFqan(source.getX003CFqanX003EKBackingField());
			destination.setHiddenInFirstDeparture(source.getX003CHiddenInFirstDepartureX003EKBackingField());
			destination.setIataCode(source.getX003CIataCodeX003EKBackingField());
		}
		
		return destination;
	}
}