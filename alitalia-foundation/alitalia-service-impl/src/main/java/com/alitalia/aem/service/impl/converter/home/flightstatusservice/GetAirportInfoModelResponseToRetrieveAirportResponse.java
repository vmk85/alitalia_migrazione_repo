package com.alitalia.aem.service.impl.converter.home.flightstatusservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AirportModelData;
import com.alitalia.aem.common.messages.home.RetrieveAirportResponse;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetAirportInfoModelResponse;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.AirportModel;

@Component(immediate = true, metatype = false)
@Service(value = GetAirportInfoModelResponseToRetrieveAirportResponse.class)
public class GetAirportInfoModelResponseToRetrieveAirportResponse implements Converter<GetAirportInfoModelResponse, RetrieveAirportResponse> {
	
	@Override
	public RetrieveAirportResponse convert(GetAirportInfoModelResponse source) {
		RetrieveAirportResponse response = new RetrieveAirportResponse();
		
		AirportModel airportModel = source.getAirport().getValue();
		String azTerminal = airportModel.getAZTerminal().getValue();
		Integer checkTimeDomestic = airportModel.getCheckInTimeDomestic().getValue();
		Integer checkIntercontinental = airportModel.getCheckInTimeIntercontinental().getValue();
		Integer checkInternational = airportModel.getCheckInTimeInternational().getValue();
		Double cityDistance = airportModel.getCityDistance().getValue();
		Integer gmt = airportModel.getGMT().getValue();
		String name = airportModel.getName().getValue();
		
		AirportModelData airportModelData = new AirportModelData();
		airportModelData.setAzTerminal(azTerminal);
		airportModelData.setCheckInTimeDomestic(checkTimeDomestic);
		airportModelData.setCheckInTimeIntercontinental(checkIntercontinental);
		airportModelData.setCheckInTimeInternational(checkInternational);
		airportModelData.setCityDistance(cityDistance);
		airportModelData.setGmt(gmt);
		airportModelData.setName(name);
		
		response.setAirport(airportModelData);
		
		return response;
	}
}