package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.home.RetrievePinResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.RetrievePINResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;

@Component(immediate = true, metatype = false)
@Service(value = RetrievePINResponseToRetrievePinResponse.class)
public class RetrievePINResponseToRetrievePinResponse implements Converter<RetrievePINResponse, RetrievePinResponse> {

	@Reference
	private CustomerProfileToMMCustomerProfileData customerProfileConverter;
	
	@Override
	public RetrievePinResponse convert(RetrievePINResponse source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		RetrievePinResponse destination = new RetrievePinResponse(); 
		
		JAXBElement<CustomerProfile> jaxbElement = source.getCustomer();
		CustomerProfile customerProfile = jaxbElement.getValue();
		MMCustomerProfileData mmCustomerProfile = customerProfileConverter.convert(customerProfile);
		destination.setCustomerProfile(mmCustomerProfile);
		destination.setRetrieved(source.isRetrived());
		
		return destination;
	}
}