package com.alitalia.aem.service.impl.converter.home.carnetservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.carnet.CarnetData;
import com.alitalia.aem.common.messages.home.CarnetGetListResponse;
import com.alitalia.aem.ws.carnet.carnetservice.xsd1.GetCarnetListResponse;
import com.alitalia.aem.ws.carnet.carnetservice.xsd5.Carnet;

@Component(immediate=true, metatype=false)
@Service(value=GetCarnetListResponseToCarnetGetListResponse.class)
public class GetCarnetListResponseToCarnetGetListResponse implements Converter<GetCarnetListResponse, CarnetGetListResponse> {

	@Reference
	private CarnetToCarnetData carnetConverter;

	@Reference
	private CarnetServiceJAXBContextFactory jaxbContextFactory;

	private Logger logger = LoggerFactory.getLogger(GetCarnetListResponseToCarnetGetListResponse.class);


	@Override
	public CarnetGetListResponse convert(GetCarnetListResponse source) {
		CarnetGetListResponse destination = null;

		if (source != null) {
			destination = new CarnetGetListResponse();
			List<CarnetData> carnets = null;
			if (source.getCarnets() != null && source.getCarnets().getValue() != null &&
					source.getCarnets().getValue().getAnyType() != null && source.getCarnets().getValue().getAnyType().size() >0){
				carnets = new ArrayList<>();
				for (Object carnet : source.getCarnets().getValue().getAnyType()) {
					if (carnet instanceof Carnet) {
						carnets.add(carnetConverter.convert((Carnet) carnet));
					} else {
						try {
							Unmarshaller carnetUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
							@SuppressWarnings("unchecked")
							JAXBElement<Carnet> unmarshalledObj = (JAXBElement<Carnet>) carnetUnmarshaller.unmarshal((Node) carnet);
							Carnet carn = (Carnet) unmarshalledObj.getValue();
							carnets.add(carnetConverter.convert(carn));
						} catch (JAXBException e) {
							logger.error("Error while Unmarshalling Carnet: ", e);
						}
					}
				}
			}
			destination.setCarnets(carnets);
		}

		return destination;
	}

}
