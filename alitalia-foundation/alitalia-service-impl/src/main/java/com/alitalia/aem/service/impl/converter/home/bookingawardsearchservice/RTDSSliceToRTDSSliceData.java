package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionSlice;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionSliceSegment;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceToRTDSSliceData.class)
public class RTDSSliceToRTDSSliceData implements
		Converter<ResultTicketingDetailSolutionSlice, ResultTicketingDetailSolutionSliceData> {

	@Reference
	private RTDSSliceExtToRTDSSliceExtData sliceExtFieldConverter;

	@Reference
	private RTDSSliceSegmentToRTDSSliceSegmentData sliceSegmentConverter;

	@Override
	public ResultTicketingDetailSolutionSliceData convert(ResultTicketingDetailSolutionSlice source) {
		ResultTicketingDetailSolutionSliceData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionSliceData();

			destination.setDurationField(source.getDurationField());
			destination.setDurationFieldSpecified(source.isDurationFieldSpecified());
			destination.setStopCountField(source.getStopCountField());

			destination.setExtField(sliceExtFieldConverter.convert(source.getExtField()));

			List<ResultTicketingDetailSolutionSliceSegmentData> segmentField = null;
			if (source.getSegmentField() != null &&
					source.getSegmentField().getResultTicketingDetailSolutionSliceSegment() != null &&
					!source.getSegmentField().getResultTicketingDetailSolutionSliceSegment().isEmpty()) {
				segmentField = new ArrayList<ResultTicketingDetailSolutionSliceSegmentData>();
				for(ResultTicketingDetailSolutionSliceSegment sourceSegmentFieldElem : 
							source.getSegmentField().getResultTicketingDetailSolutionSliceSegment())
					segmentField.add(sliceSegmentConverter.convert(sourceSegmentFieldElem));
			}
			destination.setSegmentField(segmentField);
		}

		return destination;
	}

}
