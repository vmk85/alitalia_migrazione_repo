package com.alitalia.aem.service.impl.converter.home.mmbinsuranceservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.MmbPayInsuranceRequest;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd1.InsurancePaymentRequest;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd2.InsurancePolicy;

@Component(immediate=true, metatype=false)
@Service(value=MmbPayInsuranceRequestToInsurancePaymentRequest.class)
public class MmbPayInsuranceRequestToInsurancePaymentRequest implements Converter<MmbPayInsuranceRequest, InsurancePaymentRequest> {

	@Reference
	private InsurancePolicyDataToInsurancePolicy insurancePolicyDataConverter;

	@Reference
	private MmbRouteDataToRoute mmbRouteDataConverter;

	@Reference
	private MmbPaymentDataToPayment mmbPaymentDataConverter;

	@Override
	public InsurancePaymentRequest convert(MmbPayInsuranceRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		com.alitalia.aem.ws.mmb.insuranceservice.xsd2.ObjectFactory objectFactory2 = 
				new com.alitalia.aem.ws.mmb.insuranceservice.xsd2.ObjectFactory();
		InsurancePaymentRequest destination = null;

		if (source != null) {
			destination = objectFactory.createInsurancePaymentRequest();

			InsurancePolicy insurancePolicy = insurancePolicyDataConverter.convert(source.getInsurancePolicy());
			insurancePolicy.setPayment(objectFactory2.createInsurancePolicyPayment(
					mmbPaymentDataConverter.convert(source.getPaymentData())));
			destination.setInsurancePolicy(objectFactory.createInsuranceInfoResponseInsurancePolicy(insurancePolicy));

			destination.setRoute(objectFactory.createInsurancePaymentRequestRoute(mmbRouteDataConverter.convert(source.getRoute())));
		}

		return destination;
	}

}
