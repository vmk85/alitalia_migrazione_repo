package com.alitalia.aem.service.impl.converter.home.newsservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveNewsCategoriesRequest;
import com.alitalia.aem.ws.news.newsservice.xsd2.GetCategoriesRequest;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveNewsCategoriesRequestToGetCategoriesRequest.class)
public class RetrieveNewsCategoriesRequestToGetCategoriesRequest implements Converter<RetrieveNewsCategoriesRequest, GetCategoriesRequest> {

	@Override
	public GetCategoriesRequest convert(RetrieveNewsCategoriesRequest source) {		
		return new GetCategoriesRequest();
	}
}