package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareDisplayBasePriceData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionPricingFareDisplayBasePrice;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareDisplayBasePriceToRTDSPricingFareDisplayBasePriceData.class)
public class RTDSPricingFareDisplayBasePriceToRTDSPricingFareDisplayBasePriceData implements
		Converter<ResultTicketingDetailSolutionPricingFareDisplayBasePrice, 
					ResultTicketingDetailSolutionPricingFareDisplayBasePriceData> {

	@Override
	public ResultTicketingDetailSolutionPricingFareDisplayBasePriceData convert(
			ResultTicketingDetailSolutionPricingFareDisplayBasePrice source) {
		ResultTicketingDetailSolutionPricingFareDisplayBasePriceData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingFareDisplayBasePriceData();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
