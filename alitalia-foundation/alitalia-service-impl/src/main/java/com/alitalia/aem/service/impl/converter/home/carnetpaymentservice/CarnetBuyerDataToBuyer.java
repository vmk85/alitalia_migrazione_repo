package com.alitalia.aem.service.impl.converter.home.carnetpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.carnet.CarnetBuyerData;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.Buyer;
import com.alitalia.aem.ws.carnet.paymentservice.xsd4.ObjectFactory;



@Component(immediate=true, metatype=false)
@Service(value=CarnetBuyerDataToBuyer.class)
public class CarnetBuyerDataToBuyer implements Converter<CarnetBuyerData, Buyer> {


	@Override
	public Buyer convert(CarnetBuyerData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		Buyer destination = null;

		if (source != null) {
			destination = objectFactory.createBuyer();
			destination.setAddress(objectFactory.createBuyerAddress(source.getAddress()));
			destination.setBusinessName(objectFactory.createBuyerBusinessName(source.getBusinessName()));
			destination.setCity(objectFactory.createBuyerCity(source.getCity()));
			destination.setCountryCode(objectFactory.createBuyerCountryCode(source.getCountryCode()));
			destination.setEmail(objectFactory.createABuyerBaseEmail(source.getEmail()));
			destination.setFax(objectFactory.createBuyerFax(source.getFax()));
			destination.setLastName(objectFactory.createABuyerBaseLastName(source.getLastName()));
			destination.setMobileNumber(objectFactory.createABuyerBaseMobileNumber(source.getMobileNumber()));
			destination.setName(objectFactory.createABuyerBaseName(source.getName()));
			destination.setNationInsuranceNumber(objectFactory.createBuyerNationInsuranceNumber(source.getNationInsuranceNumber()));
			destination.setPrivacyFlag(source.getPrivacyFlag());
			destination.setSellerCode(objectFactory.createBuyerSellerCode(source.getSellerCode()));
			destination.setStateCode(objectFactory.createBuyerStateCode(source.getStateCode()));
			destination.setTelephoneNumber(objectFactory.createBuyerTelephoneNumber(source.getTelephoneNumber()));
			destination.setVatNumber(objectFactory.createBuyerVatNumber(source.getVatNumber()));
			destination.setZipCode(objectFactory.createBuyerZipCode(source.getZipCode()));
		}
		
		return destination;
	}

}
