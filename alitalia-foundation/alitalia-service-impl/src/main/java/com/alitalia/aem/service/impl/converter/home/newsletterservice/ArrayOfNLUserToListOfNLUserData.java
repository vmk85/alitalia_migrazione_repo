package com.alitalia.aem.service.impl.converter.home.newsletterservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.NLUserData;
import com.alitalia.aem.common.data.home.enumerations.InsertStatusEnum;
import com.alitalia.aem.ws.news.newsletterservice.xsd2.ArrayOfNLUser;
import com.alitalia.aem.ws.news.newsletterservice.xsd2.NLUser;

@Component(immediate = true, metatype = false)
@Service(value = ArrayOfNLUserToListOfNLUserData.class)
public class ArrayOfNLUserToListOfNLUserData implements Converter<ArrayOfNLUser, List<NLUserData>> {

	@Override
	public List<NLUserData> convert(ArrayOfNLUser source) {
		
		List<NLUserData> usersData = new ArrayList<NLUserData>();
		
		List<NLUser> users = (List<NLUser>) ((source != null && source.getNLUser() != null) ? source.getNLUser() : new ArrayList<NLUser>());
		for(NLUser user: users){
			NLUserData nlUserData = new NLUserData();
			nlUserData.setEmail(user.getEmail().getValue());
			nlUserData.setLastname(user.getLastname().getValue());
			nlUserData.setName(user.getName().getValue());
			nlUserData.setRole(user.getRole());
			nlUserData.setStatus(InsertStatusEnum.fromValue(user.getSatus().value()));
			
			usersData.add(nlUserData);
		}
				
		return usersData;
	}
}