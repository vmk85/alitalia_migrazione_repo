package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMFlightData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Flight;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd3.ArrayOfanyType;

@Component(immediate = true, metatype = false)
@Service(value = FlightsToMMFlightsData.class)
public class FlightsToMMFlightsData implements Converter<ArrayOfanyType, List<MMFlightData>> {

	private static Logger logger = LoggerFactory.getLogger(FlightsToMMFlightsData.class);
	
	@Reference
	private MyPersonalareaMillemigliaServiceJAXBContextFactory jaxbContextFactory;
			
	@Override
	public List<MMFlightData> convert(ArrayOfanyType source) {
		
		if (source == null) {
			return null;
		}
		
		List<MMFlightData> mmFlightsData = new ArrayList<MMFlightData>();
		
		for (Object object: source.getAnyType()) {
			try {
				Unmarshaller unmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
				
				JAXBElement<Flight> jaxbElement = (JAXBElement<Flight>) unmarshaller.unmarshal((Node) object);
				Flight flight = jaxbElement.getValue();
				
				String arrival = flight.getAirportArrival().getValue();
				String departure = flight.getAirportDeparture().getValue();
				String number = flight.getFlightNumber().getValue();
				int result = flight.getResult();
				String serviceClass = flight.getServiceClass().getValue();
				String ticketNumber = flight.getTicketNumber().getValue();
				String vector = flight.getVector().getValue();
				
				MMFlightData mmFlightData = new MMFlightData();
				mmFlightData.setAirportArrival(arrival);
				mmFlightData.setAirportDeparture(departure);
				mmFlightData.setFlightNumber(number);
				mmFlightData.setResult(result);
				mmFlightData.setServiceClass(serviceClass);
				mmFlightData.setTicketNumber(ticketNumber);
				mmFlightData.setVector(vector);

				if (flight.getFlightDate() != null)
					mmFlightData.setFlightDate(XsdConvertUtils.parseCalendar(flight.getFlightDate()));
				
				mmFlightsData.add(mmFlightData);
			
			} catch (JAXBException e) {
				logger.error("Error while creating JAXBConext/Unmarshller for List<MMFlightData>: {}", e);
			}
		}

		return mmFlightsData;
	}
}