package com.alitalia.aem.service.impl.converter.home.bookingwidget;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.LoadWidgetOneClickRequest;
import com.alitalia.aem.ws.booking.widgetservice.xsd1.LoadOneClickRequest;
import com.alitalia.aem.ws.booking.widgetservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.booking.widgetservice.xsd2.WidgetType;

@Component(immediate = true, metatype = false)
@Service(value = LoadWidgetOneClickRequestToLoadOneClickRequest.class)
public class LoadWidgetOneClickRequestToLoadOneClickRequest implements Converter<LoadWidgetOneClickRequest, LoadOneClickRequest> {

	@Override
	public LoadOneClickRequest convert(LoadWidgetOneClickRequest source) {

		ObjectFactory objectFactory = new ObjectFactory();
		LoadOneClickRequest oneClickRequest = objectFactory.createLoadOneClickRequest();
		oneClickRequest.setLastName(objectFactory.createLoadOneClickRequestLastName(source.getLastName()));
		oneClickRequest.setMMCode(objectFactory.createLoadOneClickRequestMMCode(source.getMmCode()));
		oneClickRequest.setMMPin(objectFactory.createLoadOneClickRequestMMPin(source.getMmPin()));
		oneClickRequest.setType(WidgetType.ONE_CLICK);
		return oneClickRequest;
		
	}
}