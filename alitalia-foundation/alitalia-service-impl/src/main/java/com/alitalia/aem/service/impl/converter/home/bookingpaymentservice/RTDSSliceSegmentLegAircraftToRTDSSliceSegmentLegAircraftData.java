package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentLegAircraftData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionSliceSegmentLegAircraft;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentLegAircraftToRTDSSliceSegmentLegAircraftData.class)
public class RTDSSliceSegmentLegAircraftToRTDSSliceSegmentLegAircraftData implements
		Converter<ResultTicketingDetailSolutionSliceSegmentLegAircraft, ResultTicketingDetailSolutionSliceSegmentLegAircraftData> {

	@Override
	public ResultTicketingDetailSolutionSliceSegmentLegAircraftData convert(ResultTicketingDetailSolutionSliceSegmentLegAircraft source) {
		ResultTicketingDetailSolutionSliceSegmentLegAircraftData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionSliceSegmentLegAircraftData();

			destination.setCodeField(source.getCodeField());
			destination.setNameField(source.getNameField());
		}

		return destination;
	}

}
