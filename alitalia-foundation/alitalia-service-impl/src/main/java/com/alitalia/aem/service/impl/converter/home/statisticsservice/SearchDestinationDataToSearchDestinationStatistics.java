package com.alitalia.aem.service.impl.converter.home.statisticsservice;

import java.util.Calendar;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.SearchDestinationData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd2.TimeType;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd3.ObjectFactory;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd3.RouteType;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd3.SearchDestination;


@Component(immediate=true, metatype=false)
@Service(value=SearchDestinationDataToSearchDestinationStatistics.class)
public class SearchDestinationDataToSearchDestinationStatistics implements Converter<SearchDestinationData, SearchDestination> {

	@Reference
	private BookingSearchAirportDataToAirportStatistics airportConverter;
	
	@Override
	public SearchDestination convert(SearchDestinationData source) {
		ObjectFactory objectFactory =  new ObjectFactory();
		SearchDestination destination = objectFactory.createSearchDestination();

		Calendar departureDate = source.getDepartureDate();
		if (departureDate != null)
			destination.setDepartureDate(XsdConvertUtils.toXMLGregorianCalendar(departureDate));

		destination.setFrom(objectFactory.createASearchDestinationFrom(airportConverter.convert(source.getFromAirport())));
		destination.setSelectedOffer(source.getSelectedOffer());
		destination.setTo(objectFactory.createSearchDestinationTo(airportConverter.convert(source.getToAirport())));
		destination.setTimeType(TimeType.fromValue(source.getTimeType().value()));
		destination.setType(RouteType.fromValue(source.getRouteType().value()));

		return destination;
	}

}
