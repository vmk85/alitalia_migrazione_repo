/**
 * 
 */
package com.alitalia.aem.service.impl.converter.home.carnetstaticdataservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.CarnetStaticDataRequest;
import com.alitalia.aem.ws.carnet.staticdataservice.xsd1.GetStaticDataRequest;
import com.alitalia.aem.ws.carnet.staticdataservice.xsd1.ObjectFactory;


@Component(immediate=true, metatype=false)
@Service(value=CarnetStaticDataRequestToGetStaticDataRequest.class)
public class CarnetStaticDataRequestToGetStaticDataRequest implements Converter<CarnetStaticDataRequest, GetStaticDataRequest> {

	@Override
	public GetStaticDataRequest convert(CarnetStaticDataRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		GetStaticDataRequest destination = null;

		if (source != null) {
			
			destination = objectFactory.createGetStaticDataRequest();
			destination.setLanguageCode(objectFactory.createGetStaticDataRequestLanguageCode(source.getLanguageCode()));
			destination.setMarket(objectFactory.createGetStaticDataRequestMarket(source.getMarket()));
			
		}
		return destination;
	}
}
