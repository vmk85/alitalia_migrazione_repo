package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.SubscribeSACheckUserNameDuplicateResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.CheckEmailUsernameDuplicateResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.CheckUsernameDuplicateResponse;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

@Component(immediate = true, metatype = false)
@Service(value = CheckUserNameDuplicateResponseConverter.class)
public class CheckUserNameDuplicateResponseConverter implements Converter<CheckUsernameDuplicateResponse,SubscribeSACheckUserNameDuplicateResponse > {



	@Override
	public SubscribeSACheckUserNameDuplicateResponse convert( CheckUsernameDuplicateResponse source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");

		SubscribeSACheckUserNameDuplicateResponse destination = new SubscribeSACheckUserNameDuplicateResponse();

		if (source != null)
			destination.setFlagNoUserNameDuplicate(source.isFlagNoUsernameDuplicateOK());

		return destination;
	}
}