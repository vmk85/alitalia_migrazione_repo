package com.alitalia.aem.service.impl.converter.home.webagencyservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.AgencyRetrieveIataGroupsResponse;
import com.alitalia.aem.ws.webagency.xsd.ClAgencyData;

@Component(immediate = true, metatype = false)
@Service(value = ClAgencyDataToAgencyRetrieveIataGroupsResponse.class)
public class ClAgencyDataToAgencyRetrieveIataGroupsResponse implements Converter<ClAgencyData, AgencyRetrieveIataGroupsResponse> {

	@Override
	public AgencyRetrieveIataGroupsResponse convert(ClAgencyData source) {
		AgencyRetrieveIataGroupsResponse destination = new AgencyRetrieveIataGroupsResponse();
		destination.setCodiceAgenzia(source.getAGNCode());
		destination.setGroupsEnabled(source.isIsEnabled());
		destination.setIataEnrolled(source.isIsIataCode());
		destination.setValidResponse(true);
		return destination;
	}

}
