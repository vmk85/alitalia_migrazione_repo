package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.BalanceRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.CustomerRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;

@Component(immediate = true, metatype = false)
@Service(value = BalanceRequestToCustomerRequest.class)
public class BalanceRequestToCustomerRequest implements Converter<BalanceRequest, CustomerRequest> {

	@Reference
	private MMCustomerProfileDataToCustomerProfile mmCustomerProfileDataConverter;
	
	@Override
	public CustomerRequest convert(BalanceRequest source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		
		ObjectFactory objectFactory = new ObjectFactory();
		
		CustomerRequest destination = objectFactory.createCustomerRequest();
		CustomerProfile customerProfile = mmCustomerProfileDataConverter.convert(source.getCustomerProfile());
		
		JAXBElement<CustomerProfile> jaxbCustomerProfile = objectFactory.createCustomerRequestCustomer(customerProfile);
		destination.setCustomer(jaxbCustomerProfile);
				
		return destination;
	}
}