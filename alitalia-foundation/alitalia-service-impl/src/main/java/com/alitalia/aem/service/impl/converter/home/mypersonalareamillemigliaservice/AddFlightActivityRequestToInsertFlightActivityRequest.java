package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.AddFlightActivityRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.InsertFlightActivityRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;

@Component(immediate = true, metatype = false)
@Service(value = AddFlightActivityRequestToInsertFlightActivityRequest.class)
public class AddFlightActivityRequestToInsertFlightActivityRequest implements Converter<AddFlightActivityRequest, InsertFlightActivityRequest> {

	@Reference
	private MMCustomerProfileDataToCustomerProfile mmCustomerProfileDataConverter;
	
	@Override
	public InsertFlightActivityRequest convert(AddFlightActivityRequest source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		
		ObjectFactory objectFactory = new ObjectFactory();
		
		InsertFlightActivityRequest destination = objectFactory.createInsertFlightActivityRequest();
		CustomerProfile customerProfile = mmCustomerProfileDataConverter.convert(source.getCustomerProfile());
		
		JAXBElement<CustomerProfile> jaxbCustomerProfile = objectFactory.createCustomerRequestCustomer(customerProfile);
		JAXBElement<String> jaxbTicketNumber = objectFactory.createInsertFlightActivityRequestTicketNumber(source.getTicketNumber());
		destination.setCustomer(jaxbCustomerProfile);
		destination.setTicketNumber(jaxbTicketNumber);
				
		return destination;
	}
}