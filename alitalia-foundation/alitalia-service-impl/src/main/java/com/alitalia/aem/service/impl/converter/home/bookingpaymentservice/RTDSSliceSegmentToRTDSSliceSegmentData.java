package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentBookingInfoData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentLegData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentPricingFareData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionSliceSegment;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionSliceSegmentBookingInfo;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionSliceSegmentLeg;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionSliceSegmentPricingFare;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentToRTDSSliceSegmentData.class)
public class RTDSSliceSegmentToRTDSSliceSegmentData implements
		Converter<ResultTicketingDetailSolutionSliceSegment, ResultTicketingDetailSolutionSliceSegmentData> {

	@Reference
	private RTDSSliceSegmentBookingInfoToRTDSSliceSegmentBookingInfoData bookingInfoFieldConverter;

	@Reference
	private RTDSSliceSegmentConnectionToRTDSSliceSegmentConnectionData connectionFieldConverter;

	@Reference
	private RTDSSliceSegmentFlightToRTDSSliceSegmentFlightData flightFieldConverter;

	@Reference
	private RTDSSliceSegmentLegToRTDSSliceSegmentLegData legFieldConverter;

	@Reference
	private ResultTicketingDetailSolutionSliceSegmentPricingFareToResultTicketingDetailSolutionSliceSegmentPricingFareData pricingFieldConvert;

	@Override
	public ResultTicketingDetailSolutionSliceSegmentData convert(ResultTicketingDetailSolutionSliceSegment source) {
		ResultTicketingDetailSolutionSliceSegmentData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionSliceSegmentData();

			destination.setDepartureField(source.getDepartureField());
			destination.setDestinationField(source.getDestinationField());
			destination.setHashField(source.getHashField());
			destination.setOriginField(source.getOriginField());

			List<ResultTicketingDetailSolutionSliceSegmentBookingInfoData> bookingInfoField = null;
			if (source.getBookingInfoField() != null &&
					source.getBookingInfoField().getResultTicketingDetailSolutionSliceSegmentBookingInfo() != null &&
					!source.getBookingInfoField().getResultTicketingDetailSolutionSliceSegmentBookingInfo().isEmpty()) {
				bookingInfoField = new ArrayList<ResultTicketingDetailSolutionSliceSegmentBookingInfoData>();
				for(ResultTicketingDetailSolutionSliceSegmentBookingInfo sourceBookingInfoFieldElem : 
						source.getBookingInfoField().getResultTicketingDetailSolutionSliceSegmentBookingInfo())
					bookingInfoField.add(bookingInfoFieldConverter.convert(sourceBookingInfoFieldElem));
			}
			destination.setBookingInfoField(bookingInfoField);

			destination.setConnectionField(
					connectionFieldConverter.convert(source.getConnectionField()));

			destination.setFlightField(flightFieldConverter.convert(source.getFlightField()));

			List<ResultTicketingDetailSolutionSliceSegmentLegData> legField = null;
			if (source.getLegField() != null &&
					source.getLegField().getResultTicketingDetailSolutionSliceSegmentLeg() != null &&
					!source.getLegField().getResultTicketingDetailSolutionSliceSegmentLeg().isEmpty()) {
				legField = new ArrayList<ResultTicketingDetailSolutionSliceSegmentLegData>();
				for(ResultTicketingDetailSolutionSliceSegmentLeg sourceLegFieldElem : source.getLegField().getResultTicketingDetailSolutionSliceSegmentLeg())
					legField.add(legFieldConverter.convert(sourceLegFieldElem));
			}
			destination.setLegField(legField);

			List<ResultTicketingDetailSolutionSliceSegmentPricingFareData> pricingField = null;
			if (source.getPricingField() != null &&
					source.getPricingField().getResultTicketingDetailSolutionSliceSegmentPricingFare() != null &&
					!source.getPricingField().getResultTicketingDetailSolutionSliceSegmentPricingFare().isEmpty()) {
				pricingField = new ArrayList<ResultTicketingDetailSolutionSliceSegmentPricingFareData>();
				for(ResultTicketingDetailSolutionSliceSegmentPricingFare sourcePricingFieldElem : 
						source.getPricingField().getResultTicketingDetailSolutionSliceSegmentPricingFare())
					pricingField.add(pricingFieldConvert.convert(sourcePricingFieldElem));
			}
			destination.setPricingField(pricingField);
		}

		return destination;
	}

}
