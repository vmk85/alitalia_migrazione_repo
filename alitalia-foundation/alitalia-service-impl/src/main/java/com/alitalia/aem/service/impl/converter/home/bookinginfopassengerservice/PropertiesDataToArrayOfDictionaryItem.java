package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PropertiesData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ArrayOfDictionaryItem;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.DictionaryItem;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=PropertiesDataToArrayOfDictionaryItem.class)
public class PropertiesDataToArrayOfDictionaryItem implements Converter<PropertiesData, ArrayOfDictionaryItem> {

	@Reference
	private ResultBookingDetailsDataToResultBookingDetails resultBookingDetailsDataToResultBookingDetailConverter;

	@Reference
	private ResultTicketingDetailDataToResultTicketingDetail resultTicketingDetailDataConverter;

	@Override
	public ArrayOfDictionaryItem convert(PropertiesData source) {
		
		ArrayOfDictionaryItem destination = null;
		
		if (source != null) {
			
			ObjectFactory factory = new ObjectFactory();
			
			destination = factory.createArrayOfDictionaryItem();
	
			for (String key : source.keySet()) {
				DictionaryItem dictionaryItem = factory.createDictionaryItem();
				
				dictionaryItem.setKey(
						factory.createDictionaryItemKey(
								key));
				
				if (source.getElement(key) instanceof ResultBookingDetailsData) {
					dictionaryItem.setValue(
							factory.createDictionaryItemValue(
									resultBookingDetailsDataToResultBookingDetailConverter.convert(
											(ResultBookingDetailsData) source.getElement(key))));
				} else if (source.getElement(key) instanceof ResultTicketingDetailData) {
					dictionaryItem.setValue(
							factory.createDictionaryItemValue(
									resultTicketingDetailDataConverter.convert(
											(ResultTicketingDetailData) source.getElement(key))));
				} else {
					dictionaryItem.setValue(
							factory.createDictionaryItemValue(
									source.getElement(key)));
				}
				
				destination.getDictionaryItem().add(dictionaryItem);
			}
			
		}

		return destination;
	}

}
