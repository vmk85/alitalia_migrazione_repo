package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.InfoCarnetData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.InfoCarnet;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=InfoCarnetDataToInfoCarnet.class)
public class InfoCarnetDataToInfoCarnet implements Converter<InfoCarnetData, InfoCarnet> {

	@Override
	public InfoCarnet convert(InfoCarnetData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		InfoCarnet carnet = null; 
		if(source!=null){
			carnet = objectFactory.createInfoCarnet();
			carnet.setBuyerEmail(objectFactory.createInfoCarnetBuyerEmail(source.getBuyerEmail()));
			carnet.setBuyerFirstName(objectFactory.createInfoCarnetBuyerFirstName(source.getBuyerFirstName()));
			carnet.setBuyerLastName(objectFactory.createInfoCarnetBuyerLastName(source.getBuyerLastName()));
			carnet.setCarnetCode(objectFactory.createInfoCarnetCarnetCode(source.getCarnetCode()));
			carnet.setCarnetPassword(objectFactory.createInfoCarnetCarnetPassword(source.getCarnetPassword()));
			if (source.getExpiryDate() != null)
				carnet.setExpiryDate(XsdConvertUtils.toXMLGregorianCalendar(source.getExpiryDate()));
			carnet.setResidualRoutes(source.getResidualRoutes());
			carnet.setTotalFare(source.getTotalFare());
			carnet.setTotalRoutes(source.getTotalRoutes());
		}
		return carnet;
		
	}

}
