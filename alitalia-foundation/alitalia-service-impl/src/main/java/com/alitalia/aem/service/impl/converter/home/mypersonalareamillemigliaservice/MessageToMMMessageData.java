package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMMessageData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Message;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd3.ArrayOfanyType;

@Component(immediate = true, metatype = false)
@Service(value = MessageToMMMessageData.class)
public class MessageToMMMessageData implements Converter<ArrayOfanyType, List<MMMessageData>> {

	private static Logger logger = LoggerFactory.getLogger(MessageToMMMessageData.class);
	
	@Reference
	private MyPersonalareaMillemigliaServiceJAXBContextFactory jaxbContextFactory;
	
	@Override
	public List<MMMessageData> convert(ArrayOfanyType source) {
		
		if (source == null) {
			return null;
		}
		
		List<MMMessageData> mmMessages = new ArrayList<MMMessageData>();

		for (Object object: source.getAnyType()) {
			try {
				Unmarshaller unmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
				
				JAXBElement<Message> jaxbElement = (JAXBElement<Message>) unmarshaller.unmarshal((Node) object);
				Message message = jaxbElement.getValue();
				
				MMMessageData mmMessageData = new MMMessageData();
				mmMessageData.setAlreadyRead(message.isAlreadyRead());
				mmMessageData.setDescription(message.getDescription().getValue());
				mmMessageData.setMessageNumber(message.getMessageNumber());
				mmMessageData.setSequenceNumber(message.getSequenceNumber());
				mmMessageData.setText(message.getText().getValue());
				mmMessageData.setTitle(message.getTitle().getValue());

				if (message.getStartValidDate() != null)
					mmMessageData.setStartValidatDate(XsdConvertUtils.parseCalendar(message.getStartValidDate()));
				if (message.getEndValidDate() != null)
					mmMessageData.setEndValidatDate(XsdConvertUtils.parseCalendar(message.getEndValidDate()));
				if (message.getReadDate() != null)
					mmMessageData.setReadDate(XsdConvertUtils.parseCalendar(message.getReadDate()));
				
				mmMessages.add(mmMessageData);
			
			} catch (JAXBException e) {
				logger.error("Error while creating JAXBConext/Unmarshller for List<MMCreditCardData>: {}", e);
			}
		}
		
		return mmMessages;
	}
}