package com.alitalia.aem.service.impl.converter.home.checkinstaticdataservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.WebCheckinAirportNameRequest;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.GetAirportNameListRequest;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.Resource;



@Component(immediate=true, metatype=false)
@Service(value=WebCheckinAirportNameRequestToGetAirportNameListRequestConverter.class)
public class WebCheckinAirportNameRequestToGetAirportNameListRequestConverter implements Converter<WebCheckinAirportNameRequest, GetAirportNameListRequest> {

	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;
	
	@Override
	public GetAirportNameListRequest convert(WebCheckinAirportNameRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();		
		GetAirportNameListRequest destination = objectFactory.createGetAirportNameListRequest();
		
		destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
		destination.setX003CResourceX003EKBackingField(Resource.WEB);
		
		destination.setLanguageCode(objectFactory.createGetAirportNameListRequestLanguageCode(source.getLanguageCode()));

		return destination;
	}

}