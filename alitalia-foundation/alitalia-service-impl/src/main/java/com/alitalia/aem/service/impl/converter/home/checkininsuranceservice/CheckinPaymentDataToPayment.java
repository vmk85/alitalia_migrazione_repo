package com.alitalia.aem.service.impl.converter.home.checkininsuranceservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinPaymentData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd2.AncillaryService;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd2.Payment;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd2.SaleChannel;



@Component(immediate=true, metatype=false)
@Service(value=CheckinPaymentDataToPayment.class)
public class CheckinPaymentDataToPayment implements Converter<CheckinPaymentData, Payment> {
	
	@Reference
	private CheckinCreditCardDataToCreditCard checkinCreditCardDataConverter;
	
	@Reference
	private CheckinPaymentTransactionInfoDataToPaymentTransactionInfo checkinPaymentTransactionInfoDataConverter;
	
	@Reference
	private CheckinVerifiedByVisaDataToVerifiedByVisa checkinVerifiedByVisaDataConverter;


	@Override
	public Payment convert(CheckinPaymentData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		Payment destination = null;

		if (source != null) {
			destination = objectFactory.createPayment();
			if (source.getId() != null) {
				destination.setX003CIdX003EKBackingField(source.getId());
			}
			
			destination.setX003CAmountX003EKBackingField(source.getAmount());
			destination.setX003CApplicationSpecificOrderIDX003EKBackingField(source.getApplicationSpecificOrderID());
			destination.setX003CAuthUserFrequentFlyerCodeX003EKBackingField(source.getAuthUserFrequentFlyerCode());
			destination.setX003CCartIDX003EKBackingField(source.getCartID());
			
			destination.setX003CCreditCardDataX003EKBackingField(checkinCreditCardDataConverter.convert(source.getCreditCardData()));
			destination.setX003CCurrencyX003EKBackingField(source.getCurrency());
			destination.setX003CDescriptionX003EKBackingField(source.getDescription());
			destination.setX003CEmailX003EKBackingField(source.getEmail());
			destination.setX003CIDAuthEventX003EKBackingField(source.getIdAuthEvent());
			destination.setX003CMethodIdX003EKBackingField(source.getMethodId());
			destination.setX003COrderCodeX003EKBackingField(source.getOrderCode());
			if(source.getPaymentAttemptCount() != null){
				destination.setX003CPaymentAttemptCountX003EKBackingField(source.getPaymentAttemptCount());
			}
			destination.setX003CPersistenceIdX003EKBackingField(source.getPersistenceId());
			if(source.getSaleChannel() != null){
				destination.setX003CSaleChannelX003EKBackingField(SaleChannel.fromValue(source.getSaleChannel().value()));
			}
			if(source.getSoldGood() != null){
				destination.setX003CSoldGoodX003EKBackingField(AncillaryService.fromValue(source.getSoldGood().value()));
			}
			destination.setX003CTransactionDateX003EKBackingField(XsdConvertUtils.toXMLGregorianCalendar(source.getTransactionDate()));
			destination.setX003CTransactionInfoX003EKBackingField(checkinPaymentTransactionInfoDataConverter.convert(source.getTransactionInfo()));
			destination.setX003CVBVParamX003EKBackingField(source.getVbvParam());
			destination.setX003CVerifiedByVisaX003EKBackingField(checkinVerifiedByVisaDataConverter.convert(source.getVerifiedByVisa()));
			
		}
		return destination;
	}

}
