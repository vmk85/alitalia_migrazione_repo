package com.alitalia.aem.service.impl.converter.home.checkincomfortseatservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.ComfortSeatPurchaseCheckinResponse;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.ComfortSeatPurchaseResponse;


@Component(immediate=true, metatype=false)
@Service(value=ComfortSeatPurchaseResponseToComfortSeatPurchaseCheckinResponse.class)
public class ComfortSeatPurchaseResponseToComfortSeatPurchaseCheckinResponse implements Converter<ComfortSeatPurchaseResponse, ComfortSeatPurchaseCheckinResponse> {


	@Override
	public ComfortSeatPurchaseCheckinResponse convert(ComfortSeatPurchaseResponse source) {
		ComfortSeatPurchaseCheckinResponse destination = null;

		if (source != null) {
			destination = new ComfortSeatPurchaseCheckinResponse();
			
			destination.setAmount(source.getAmount());
			destination.setComfortSeatPaid(source.isComfortSeatPaid());
			if(source.getCurrency() != null){
				destination.setCurrency(source.getCurrency().getValue());
			}
			if(source.getSeat() != null){
				destination.setSeat(source.getSeat().getValue());
			}
		}

		return destination;
	}

}
