package com.alitalia.aem.service.impl.myalitaliarest.client;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyStore;
import java.util.Map;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.BaseRequest;
import com.alitalia.aem.service.impl.converter.homerest.staticdataservice.ResponseReaderService;
import com.alitalia.aem.ws.common.exception.WSClientException;
import com.alitalia.aem.ws.common.utils.WSClientConstants;
import com.google.gson.Gson;

@Service(value = AdyenRecurringPaymentServiceClient.class)
@Component(immediate = true, metatype = true) 
@Properties({  
@Property(name = "service.vendor", value = "Reply") })
public class AdyenRecurringPaymentServiceClient {
	
	private static final Logger logger = LoggerFactory.getLogger(AdyenRecurringPaymentServiceClient.class);
	
	@Property(description = "URL of the 'AdyenRecurringPaymentServiceClient Web Service REST' used to invoke the Web Service.")
	private static final String WS_ENDPOINT_PROPERTY = "ws.endpoint";

	@Property(description = "Flag used to invoke Web Service in HTTPS.")
	private static final String WS_HTTPS_MODE_PROPERTY = "ws.https_mode";

	@Property(description = "Flag used to load identity store.")
	private static final String WS_LOAD_IDENTITY_STORE_PROPERTY = "ws.load_identity_store";

	@Property(description = "Flag used to enable hostname verifier.")
	private static final String WS_HOSTNAME_VERIFIER_PROPERTY = "ws.hostname_verifier";

	@Property(description = "Property used to set identity store path.")
	private static final String WS_IDENTITY_STORE_PATH_PROPERTY = "ws.identity_store_path";

	@Property(description = "Property used to set identity store password.")
	private static final String WS_IDENTITY_STORE_PASSWORD_PROPERTY = "ws.identity_store_password";

	@Property(description = "Property used to set identity key password.")
	private static final String WS_IDENTITY_KEY_PASSWORD_PROPERTY = "ws.identity_key_password";

	@Property(description = "Property used to set trust store path.")
	private static final String WS_TRUST_STORE_PATH_PROPERTY = "ws.trust_store_path";

	@Property(description = "Property used to set trust store password.")
	private static final String WS_TRUST_STORE_PASSWORD_PROPERTY = "ws.trust_store_password";

	@Property(description = "Property used to set connection timout")
	private static final String WS_CONNECTION_TIMEOUT_PROPERTY = "ws.connection_timeout";

	@Property(description = "Property used to set request timout")
	private static final String WS_REQUEST_TIMEOUT_PROPERTY = "ws.request_timeout";

	@Property(description = "Property used to set request timout")
	private static final String WS_SSL_PROTOCOL = "ws.ssl_protocol";

	@Property(description = "Property used to print the request")
	private static final String WS_PRINT_REQUET = "ws.print_request";

	@Property(description = "Property used to print the response")
	private static final String WS_PRINT_RESPONSE = "ws.print_response";

	public String serviceEndpoint;
	private Integer connectionTimeout;
	private Integer requestTimeout;
	private String identityStorePath;
	private String identityStorePassword;
	private String identityKeyPassword;
	private String trustStorePath;
	private String trustStorePassword;
	private String sslProtocol;
	private boolean hostnameVerifier;
	private boolean loadIdentityStore;
	private boolean httpsMode;
	private boolean printRequest;
	private boolean printResponse;

	private boolean initRequired = true;


	private void init() throws WSClientException {
		FileInputStream fisTruststore = null;
		FileInputStream fisIdentitystore = null;
		try {
			logger.debug("MAPayment - Https mode [" + httpsMode + "]");

			if (httpsMode) {
				logger.debug("MAPayment - Loaded identity store path [" + identityStorePath + "], trust store path [" + trustStorePath + "], " 
						+ "hostname verifier [" + hostnameVerifier + "] and loading identity store flag [" + loadIdentityStore + "]");

				SSLContext sslContext = SSLContext.getInstance(sslProtocol);

				TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
				KeyStore trustKeystore = KeyStore.getInstance(KeyStore.getDefaultType());
				fisTruststore = new FileInputStream(trustStorePath);
				trustKeystore.load(fisTruststore, trustStorePassword.toCharArray());
				tmf.init(trustKeystore);

				if (loadIdentityStore) {
					KeyStore identityKeystore = KeyStore.getInstance(WSClientConstants.keystoreTypeDefault);
					fisIdentitystore = new FileInputStream(identityStorePath);
					identityKeystore.load(fisIdentitystore, identityStorePassword.toCharArray());

					KeyManagerFactory kmf = KeyManagerFactory.getInstance(WSClientConstants.keymanagerFactoryAlgorithmDefault);
					kmf.init(identityKeystore, identityKeyPassword.toCharArray());
					sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

					logger.debug("MAPayment - Loaded identity and trust store");
				} else {
					sslContext.init(null, tmf.getTrustManagers(), null);
					logger.debug("MAPayment - Loaded trust store only");
				}

				SSLContext.setDefault(sslContext);
			}

		} catch (java.net.MalformedURLException e) {
			logger.error("MAPayment - Error to initialize AdyenRecurringPaymentServiceClient, invalid endpoint = {}", this.serviceEndpoint);
			throw new WSClientException("Error to initialize AdyenRecurringPaymentServiceClient", e);
		} catch (Exception e) {
			logger.error("MAPayment - Error to initialize AdyenRecurringPaymentServiceClient: ", e);
			throw new WSClientException("Error to initialize AdyenRecurringPaymentServiceClient", e);
		} finally {
			if (fisIdentitystore != null) {
				try {
					fisIdentitystore.close();
				} catch (IOException e) {
					logger.error("MAPayment - Error to close input stream", e);
				}
			}

			if (fisTruststore != null) {
				try {
					fisTruststore.close();
				} catch (IOException e) {
					logger.error("MAPayment - Error to close input stream", e);
				}
			}
		}
	}

	@Activate
	protected void activate(final Map<String, Object> config) {
		String serviceEndpoint = null;
		String identityStorePath = null;
		String identityStorePassword = null;
		String identityKeyPassword = null;
		String trustStorePath = null;
		String trustStorePassword = null;
		Integer connectionTimeout = null;
		Integer requestTimeout = null;
		String sslProtocol = null;
		boolean httpsMode;
		boolean hostnameVerifier;
		boolean loadIdentityStore;
		boolean printRequest;
		boolean printResponse;

		try {			
			httpsMode = Boolean.valueOf(String.valueOf(config.get(WS_HTTPS_MODE_PROPERTY)));
			this.httpsMode = httpsMode;

			hostnameVerifier = Boolean.valueOf(String.valueOf(config.get(WS_HOSTNAME_VERIFIER_PROPERTY)));
			this.hostnameVerifier = hostnameVerifier;

			loadIdentityStore = Boolean.valueOf(String.valueOf(config.get(WS_LOAD_IDENTITY_STORE_PROPERTY)));
			this.loadIdentityStore = loadIdentityStore;

			identityStorePath = String.valueOf(config.get(WS_IDENTITY_STORE_PATH_PROPERTY));
			this.identityStorePath = identityStorePath;

			identityStorePassword = String.valueOf(config.get(WS_IDENTITY_STORE_PASSWORD_PROPERTY));
			this.identityStorePassword = identityStorePassword;

			identityKeyPassword = String.valueOf(config.get(WS_IDENTITY_KEY_PASSWORD_PROPERTY));
			this.identityKeyPassword = identityKeyPassword;

			trustStorePath = String.valueOf(config.get(WS_TRUST_STORE_PATH_PROPERTY));
			this.trustStorePath = trustStorePath;

			trustStorePassword = String.valueOf(config.get(WS_TRUST_STORE_PASSWORD_PROPERTY));
			this.trustStorePassword = trustStorePassword;

			connectionTimeout = Integer.valueOf(String.valueOf(config.get(WS_CONNECTION_TIMEOUT_PROPERTY)));
			this.connectionTimeout = connectionTimeout;	

			requestTimeout = Integer.valueOf(String.valueOf(config.get(WS_REQUEST_TIMEOUT_PROPERTY)));
			this.requestTimeout = requestTimeout;	

			sslProtocol = String.valueOf(config.get(WS_SSL_PROTOCOL));
			this.sslProtocol = sslProtocol;	

			printRequest = Boolean.valueOf(String.valueOf(config.get(WS_PRINT_REQUET)));
			this.printRequest = printRequest;	

			printResponse = Boolean.valueOf(String.valueOf(config.get(WS_PRINT_RESPONSE)));
			this.printResponse = printResponse;			
		} catch (Exception e) {
			logger.error("MAPayment - Error to read custom configuration to activate StaticDataServiceClient - default configuration used", e);
			setDefault();
		}

		try {			
			serviceEndpoint = String.valueOf(config.get(WS_ENDPOINT_PROPERTY));
			this.serviceEndpoint = serviceEndpoint;

			logger.info("MAPayment - Activated AdyenRecurringPaymentServiceClient: "
					+ "WS_ENDPOINT_PROPERTY=[" + this.serviceEndpoint + "], "
					+ "WS_HTTPS_MODE_PROPERTY=[" + this.httpsMode + "], "
					+ "WS_HOSTNAME_VERIFIER_PROPERTY=[" + this.hostnameVerifier +"], "
					+ "WS_LOAD_IDENTITY_STORE_PROPERTY=[" + this.loadIdentityStore + "], "
					+ "WS_IDENTITY_STORE_PATH_PROPERTY=[" + this.identityStorePath + "], "
					+ "WS_TRUST_STORE_PATH_PROPERTY=[" + this.trustStorePath + "], "
					+ "WS_CONNECTION_TIMEOUT_PROPERTY=[" + this.connectionTimeout + "], "
					+ "WS_REQUEST_TIMEOUT_PROPERTY=[" + this.requestTimeout + "], "
					+ "WS_SSL_PROTOCOL=[" + this.sslProtocol + "], "
					+ "WS_PRINT_RESPONSE=[" + this.printRequest + "], "
					+ "WS_PRINT_REQUET=[" + this.printResponse + "]");
		} catch (Exception e) {
			logger.error("MAPayment - Error to read WS_ENDPOINT_PROPERTY to activate AdyenRecurringPaymentServiceClient", e);
			throw new WSClientException("Error to initialize AdyenRecurringPaymentServiceClient", e);
		}

		try {
			init();
			this.initRequired = false;
		} catch (Exception e) {
			this.initRequired = true;
			logger.error("MAPayment - Error initializing AdyenRecurringPaymentServiceClient", e);
		}
	}

	@Modified
	protected void modified(final Map<String, Object> config) {
		String serviceEndpoint = null;
		boolean httpsMode;
		boolean hostnameVerifier;
		boolean loadIdentityStore;
		boolean printRequest;
		boolean printResponse;
		String identityStorePath = null;
		String identityStorePassword = null;
		String identityKeyPassword = null;
		String trustStorePath = null;
		String trustStorePassword = null;
		Integer connectionTimeout = null;
		Integer requestTimeout = null;
		String sslProtocol = null;

		try {			
			httpsMode = Boolean.valueOf(String.valueOf(config.get(WS_HTTPS_MODE_PROPERTY)));
			this.httpsMode = httpsMode;

			hostnameVerifier = Boolean.valueOf(String.valueOf(config.get(WS_HOSTNAME_VERIFIER_PROPERTY)));
			this.hostnameVerifier = hostnameVerifier;

			loadIdentityStore = Boolean.valueOf(String.valueOf(config.get(WS_LOAD_IDENTITY_STORE_PROPERTY)));
			this.loadIdentityStore = loadIdentityStore;

			identityStorePath = String.valueOf(config.get(WS_IDENTITY_STORE_PATH_PROPERTY));
			this.identityStorePath = identityStorePath;

			identityStorePassword = String.valueOf(config.get(WS_IDENTITY_STORE_PASSWORD_PROPERTY));
			this.identityStorePassword = identityStorePassword;

			identityKeyPassword = String.valueOf(config.get(WS_IDENTITY_KEY_PASSWORD_PROPERTY));
			this.identityKeyPassword = identityKeyPassword;

			trustStorePath = String.valueOf(config.get(WS_TRUST_STORE_PATH_PROPERTY));
			this.trustStorePath = trustStorePath;

			trustStorePassword = String.valueOf(config.get(WS_TRUST_STORE_PASSWORD_PROPERTY));
			this.trustStorePassword = trustStorePassword;

			connectionTimeout = Integer.valueOf(String.valueOf(config.get(WS_CONNECTION_TIMEOUT_PROPERTY)));
			this.connectionTimeout = connectionTimeout;	

			requestTimeout = Integer.valueOf(String.valueOf(config.get(WS_REQUEST_TIMEOUT_PROPERTY)));
			this.requestTimeout = requestTimeout;	

			sslProtocol = String.valueOf(config.get(WS_SSL_PROTOCOL));
			this.sslProtocol = sslProtocol;	

			printRequest = Boolean.valueOf(String.valueOf(config.get(WS_PRINT_REQUET)));
			this.printRequest = printRequest;	

			printResponse = Boolean.valueOf(String.valueOf(config.get(WS_PRINT_RESPONSE)));
			this.printResponse = printResponse;	
		} catch (Exception e) {
			logger.error("MAPayment - Error to read custom configuration to modify AdyenRecurringPaymentServiceClient - default configuration used", e);
			setDefault();
		}

		try {			
			serviceEndpoint = String.valueOf(config.get(WS_ENDPOINT_PROPERTY));
			this.serviceEndpoint = serviceEndpoint;

			logger.info("MAPayment - Modified AdyenRecurringPaymentServiceClient: "
					+ "WS_ENDPOINT_PROPERTY=[" + this.serviceEndpoint + "], "
					+ "WS_HTTPS_MODE_PROPERTY=[" + this.httpsMode + "], "
					+ "WS_HOSTNAME_VERIFIER_PROPERTY=[" + this.hostnameVerifier +"], "
					+ "WS_LOAD_IDENTITY_STORE_PROPERTY=[" + this.loadIdentityStore + "], "
					+ "WS_IDENTITY_STORE_PATH_PROPERTY=[" + this.identityStorePath + "], "
					+ "WS_TRUST_STORE_PATH_PROPERTY=[" + this.trustStorePath + "], "
					+ "WS_CONNECTION_TIMEOUT_PROPERTY=[" + this.connectionTimeout + "], "
					+ "WS_REQUEST_TIMEOUT_PROPERTY=[" + this.requestTimeout + "], "
					+ "WS_SSL_PROTOCOL=[" + this.sslProtocol + "], "
					+ "WS_PRINT_RESPONSE=[" + this.printRequest + "], "
					+ "WS_PRINT_REQUET=[" + this.printResponse + "]");
		} catch (Exception e) {
			logger.error("MAPayment - Error to read WS_ENDPOINT_PROPERTY to modify AdyenRecurringPaymentServiceClient", e);
			throw new WSClientException("Error to initialize AdyenRecurringPaymentServiceClient", e);
		}


		try {
			init();
			this.initRequired = false;
		} catch (Exception e) {
			this.initRequired = true;
			logger.error("MAPayment - Error initializing AdyenRecurringPaymentServiceClient", e);
		}
	}

	private void setDefault() {
		this.httpsMode = WSClientConstants.httpsModeDefault;
		this.hostnameVerifier = WSClientConstants.hostnameVerifierDefault;
		this.loadIdentityStore = WSClientConstants.loadIdentityStoreDefault;
		this.connectionTimeout = WSClientConstants.connectionTimeoutDefault;
		this.requestTimeout = WSClientConstants.requestTimeoutDefault;
		this.sslProtocol = WSClientConstants.sslProtocolDefault;
		this.printRequest = WSClientConstants.printRequest;
		this.printResponse = WSClientConstants.printResponse;
	}

	public Integer getConnectionTimeout() {
		return connectionTimeout;
	}

	public Integer getRequestTimeout() {
		return requestTimeout;
	}

	public String getIdentityStorePath() {
		return identityStorePath;
	}

	public String getIdentityStorePassword() {
		return identityStorePassword;
	}

	public String getTrustStorePath() {
		return trustStorePath;
	}

	public String getTrustStorePassword() {
		return trustStorePassword;
	}

	public boolean getHostnameVerifier() {
		return hostnameVerifier;
	}

	public boolean getLoadIdentityStore() {
		return loadIdentityStore;
	}

	public boolean getHttpsMode() {
		return httpsMode;
	}

	public String getSslProtocol() {
		return sslProtocol;
	}

	public String getIdentityKeyPassword() {
		return identityKeyPassword;
	}

	public boolean isPrintRequest() {
		return printRequest;
	}

	public boolean isPrintResponse() {
		return printResponse;
	}
	
//	public String getData(BaseRequest request, String relativePath){
//
//		String jsonString = "";
//		
//		try{
//			InputStream is = null;
//			OutputStream os = null;
//			URLConnection connection = null;
//			Gson gson = new Gson();		
//			String input = "";
//			input = gson.toJson(request);
//			logger.debug("Prima di aprire la connessione...");
//			String url = "http://172.31.251.138:9002/api/v0.1/checkin".concat(relativePath); //TODO : da cambiare il path radice (da prendere in configurazione)
//			connection = new URL(url).openConnection();
//			connection.setDoOutput(true);
//			connection.setRequestProperty("Accept-Charset", "application/json");
//			connection.setRequestProperty("Content-Type", "application/json");
//
//			os = connection.getOutputStream();
//			os.write(input.getBytes());
//			is = connection.getInputStream();
//
//			jsonString = ResponseReader.retrieveResponse(is);
//			System.out.println("jsonString è = " + jsonString);
//			logger.info("Connessione riuscita, la risposta del servizio è " + jsonString);
//		}
//		catch(Exception e){
//
//		}
//		
//		return jsonString;
//	}
	
	public String getData(BaseRequest request, String relativePath){

		String jsonString = "";
		
		FileInputStream fisTruststore = null;
		FileInputStream fisIdentitystore = null;
		
		InputStream is = null;
		OutputStream os = null;
		URLConnection urlConnection = null;
		Gson gson = new Gson();		
		String input = "";
		input = gson.toJson(request);
		
		HttpURLConnection httpUrlConnection = null;
		HttpsURLConnection httpsUrlConnection = null;
		
		String serviceEndPoint = this.serviceEndpoint;
		boolean httpsMode = this.httpsMode; 
		
		try {

			logger.debug("MAPayment - Https mode [" + httpsMode + "]");

			if (httpsMode) {
				logger.debug("MAPayment - Loaded identity store path [" + identityStorePath + "], trust store path [" + trustStorePath + "], "
						+ "hostname verifier [" + hostnameVerifier + "] and loading identity store flag [" + loadIdentityStore + "]");

				SSLContext sslContext = SSLContext.getInstance(sslProtocol);

				TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
				KeyStore trustKeystore = KeyStore.getInstance(KeyStore.getDefaultType());
				fisTruststore = new FileInputStream(trustStorePath);
				trustKeystore.load(fisTruststore, trustStorePassword.toCharArray());
				tmf.init(trustKeystore);

				if (loadIdentityStore) {
					KeyStore identityKeystore = KeyStore.getInstance(WSClientConstants.keystoreTypeDefault);
					fisIdentitystore = new FileInputStream(identityStorePath);
					identityKeystore.load(fisIdentitystore, identityStorePassword.toCharArray());

					KeyManagerFactory kmf = KeyManagerFactory.getInstance(WSClientConstants.keymanagerFactoryAlgorithmDefault);
					kmf.init(identityKeystore, identityKeyPassword.toCharArray());
					sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

					logger.debug("MAPayment - Loaded identity and trust store");
				} else {
					sslContext.init(null, tmf.getTrustManagers(), null);
					logger.debug("MAPayment - Loaded trust store only");
				}

				SSLContext.setDefault(sslContext);
				
				SSLSocketFactory sslSf = sslContext.getSocketFactory();
				
				// We prepare a URLConnection 
				String urlPath = serviceEndPoint.concat(relativePath);
				URL url = new URL(urlPath);
				long time_before = System.currentTimeMillis();
				urlConnection = url.openConnection();
				// Before actually opening the sockets, we affect the SSLSocketFactory
				httpsUrlConnection = (HttpsURLConnection) urlConnection;
				httpsUrlConnection.setSSLSocketFactory(sslSf);
				
				httpsUrlConnection.setDoOutput(true);
				httpsUrlConnection.setRequestProperty("Accept-Charset", "application/json");
				httpsUrlConnection.setRequestProperty("Content-Type", "application/json");
				
				logger.info("MAPayment - AdyenRecurringPaymentServiceClient request a {} : {}", url, input);
				os = httpsUrlConnection.getOutputStream();
				os.write(input.getBytes());
				is = httpsUrlConnection.getInputStream();
				
				jsonString = ResponseReaderService.retrieveResponse(is);
//				is.close();
				long time_after = System.currentTimeMillis();
				long execution_time = time_after - time_before;
				if (execution_time > 20000) {
					logger.info("MAPayment - ------ ALERT EXECUTION TIME ------ adyen recurring payment rest service execution time=" + execution_time);
				} else {
					logger.info("MAPayment - adyen recurring payment rest service execution time=" + execution_time);
				}
				logger.info("MAPayment - AdyenRecurringPaymentServiceClient response da {} : {}", url, jsonString);

			}else{
				String urlPath = serviceEndPoint.concat(relativePath);
				logger.info("MAPayment - UrlPath chiamata servizio : " + urlPath);
				
				URL url = new URL(urlPath);
				long time_before = System.currentTimeMillis();
				urlConnection = url.openConnection();
				httpUrlConnection = (HttpURLConnection) urlConnection;
				
				httpUrlConnection.setDoOutput(true);
				httpUrlConnection.setRequestProperty("Accept-Charset", "application/json");
				httpUrlConnection.setRequestProperty("Content-Type", "application/json");
	
				logger.info("MAPayment - AdyenRecurringPaymentServiceClient request a {} : {}", url, input);
				os = httpUrlConnection.getOutputStream();
				os.write(input.getBytes());
				is = httpUrlConnection.getInputStream();
	
				jsonString = ResponseReaderService.retrieveResponse(is);
//				is.close();
				long time_after = System.currentTimeMillis();
				long execution_time = time_after - time_before;
				if (execution_time > 20000) {
					logger.info("MAPayment - ------ ALERT EXECUTION TIME ------ adyen recurring payment rest service execution time=" + execution_time);
				} else {
					logger.info("MAPayment - adyen recurring payment rest service execution time=" + execution_time);
				}
				logger.info("MAPayment - AdyenRecurringPaymentServiceClient response da {} : {}", url, jsonString);
				
			}

		} catch (java.net.MalformedURLException e) {
			logger.error("MAPayment - Error to initialize AdyenRecurringPaymentServiceClient, invalid endpoint = {}", this.serviceEndpoint);
			throw new WSClientException("Error to initialize AdyenRecurringPaymentServiceClient", e);
		} catch (IOException e) { 
			if (httpsMode) {
				is = httpsUrlConnection.getErrorStream();
				jsonString = ResponseReaderService.retrieveResponse(is);
				logger.error("MAPayment - AdyenRecurringPaymentServiceClient ERRORE --- la risposta del servizio è " + jsonString);
			}else{
				is = httpUrlConnection.getErrorStream();
				jsonString = ResponseReaderService.retrieveResponse(is);
				logger.error("MAPayment - AdyenRecurringPaymentServiceClient ERRORE ---  la risposta del servizio è " + jsonString);
			}
			logger.error("MAPayment - Error to initialize AdyenRecurringPaymentServiceClient: ", e);
/*RDT TOLENTINO Rimuover Dopo Test - I servizi Adyen non rispondono sempre perchè non sono dietro Data Power*/		
/*RDT		Random rand = new Random();*/
/*RDT		if (relativePath.equals("/retrievalstoredpayment")) {*/
/*RDT			jsonString = "[ { \"creationDate\": \"2018-09-06T14:24:40Z\", \"recurringDetailList\": [ { \"additionalDataList\": [ { \"key\": \"cardBin\", \"cardBin\": \"222240\" } ], \"card\": { \"cvc\": null, \"expiryMonth\": \"10\", \"expiryYear\": \"2020\", \"holderName\": \"test memorizza"+String.format("%04d", rand.nextInt(10000))+"\", \"number\": \"0008\", \"type\": \"mc\" }, \"alias\": \"H101509493822804\", \"aliasType\": \"Default\", \"creationDate\": \"2018-09-18T09:00:27Z\", \"firstPspReference\": \"8835372612260081\", \"recurringDetailReference\": \"8415372612278685\", \"recurringList\": [ { \"contract\": \"ONECLICK\" } ], \"paymentMethodVariant\": \"mcstandardcredit\", \"variant\": \"mc\", \"address\": null, \"bank\": null, \"shopperName\": null }, { \"additionalDataList\": [ { \"key\": \"cardBin\", \"cardBin\": \"444433\" } ], \"card\": { \"cvc\": null, \"expiryMonth\": \"10\", \"expiryYear\": \"2020\", \"holderName\": \"prova memorizza"+String.format("%04d", rand.nextInt(10000))+"\", \"number\": \"1111\", \"type\": \"visa\" }, \"alias\": \"P106674586198742\", \"aliasType\": \"Default\", \"creationDate\": \"2018-09-18T10:48:27Z\", \"firstPspReference\": \"8515372677060233\", \"recurringDetailReference\": \"8315362438809277\", \"recurringList\": [ { \"contract\": \"ONECLICK\" }, { \"contract\": \"RECURRING\" } ], \"paymentMethodVariant\": \"visacorporatecredit\", \"variant\": \"visa\", \"address\": null, \"bank\": null, \"shopperName\": null } ], \"lastKnownShopperEmail\": \"gtolentino@alpenite.com\", \"shopperReference\": \"39fbe13b0ffd46ef9ee08d09ec577034\", \"conversationID\": \"987654321\" } ]";*/
/*RDT		} else {*/
			throw new WSClientException("Error to initialize AdyenRecurringPaymentServiceClient", e);
/*RDT		}*/
		} catch (Exception e) {
			logger.error("MAPayment - Error to initialize AdyenRecurringPaymentServiceClient: ", e);
			throw new WSClientException("Error to initialize AdyenRecurringPaymentServiceClient", e);
		} finally {
			if (is!=null) {
				try {
					is.close();
				} catch (IOException e) {
					logger.error("MAPayment - Error to close input stream", e);
				}
			}
			if (fisIdentitystore != null) {
				try {
					fisIdentitystore.close();
				} catch (IOException e) {
					logger.error("MAPayment - Error to close input stream", e);
				}
			}

			if (fisTruststore != null) {
				try {
					fisTruststore.close();
				} catch (IOException e) {
					logger.error("MAPayment - Error to close input stream", e);
				}
			}
		}

		return jsonString;
	}
	
	//Mock - Tolentino
//	public String getData(BaseRequest request, String relativePath){
//		String result = "";
//		switch (relativePath) {
//		case "/storingpayment":
//			/*
//			{
//			  "pspReference": "8535362442791929",
//			  "resultCode": "Authorised",
//			  "authCode": "64069",
//			  "conversationID": "987654321"
//			}
//			*/
//			logger.info("[getData - /storingpayment]");
//			result = "{ \"pspReference\": \"8535362442791929\", \"resultCode\": \"Authorised\", \"authCode\": \"64069\", \"conversationID\": \"987654321\" }";
//			break;
//
//		case "/retrievalstoredpayment":
//			/*
//			[
//			  {
//			    "creationDate": "2018-09-06T14:24:40Z",
//			    "recurringDetailList": [
//			      {
//			        "additionalDataList": [
//			          {
//			            "key": "cardBin",
//			            "cardBin": "444433"
//			          }
//			        ],
//			        "card": {
//			          "cvc": null,
//			          "expiryMonth": "8",
//			          "expiryYear": "2018",
//			          "holderName": "Test Uno",
//			          "number": "1111",
//			          "type": "visa"
//			        },
//			        "alias": "P106674586198742",
//			        "aliasType": "Default",
//			        "creationDate": "2018-09-06T14:24:40Z",
//			        "firstPspReference": "8835362438795089",
//			        "recurringDetailReference": "8315362438809277",
//			        "recurringList": [
//			          {
//			            "contract": "RECURRING"
//			          }
//			        ],
//			        "paymentMethodVariant": "visacorporatecredit",
//			        "variant": "visa",
//			        "address": {
//			          "city": "Roma",
//			          "country": "IT",
//			          "postalCode": "00100",
//			          "province": "RM",
//			          "street": "via roma 100"
//			        },
//			        "bank": null,
//			        "shopperName": null
//			      },
//			      {
//			        "additionalDataList": [
//			          {
//			            "key": "cardBin",
//			            "cardBin": "677179"
//			          }
//			        ],
//			        "card": {
//			          "cvc": null,
//			          "expiryMonth": "10",
//			          "expiryYear": "2020",
//			          "holderName": "Test Uno",
//			          "number": "0004",
//			          "type": "mc"
//			        },
//			        "alias": "P663519338012674",
//			        "aliasType": "Default",
//			        "creationDate": "2018-09-06T15:36:59Z",
//			        "firstPspReference": "8525362482191933",
//			        "recurringDetailReference": "8415362482191209",
//			        "recurringList": [
//			          {
//			            "contract": "RECURRING"
//			          }
//			        ],
//			        "paymentMethodVariant": "mc",
//			        "variant": "mc",
//			        "address": {
//			          "city": "Roma",
//			          "country": "IT",
//			          "postalCode": "00100",
//			          "province": "RM",
//			          "street": "via roma 100"
//			        },
//			        "bank": null,
//			        "shopperName": null
//			      },
//			      {
//			        "additionalDataList": [
//			          {
//			            "key": "cardBin",
//			            "cardBin": "370000"
//			          }
//			        ],
//			        "card": {
//			          "cvc": null,
//			          "expiryMonth": "10",
//			          "expiryYear": "2020",
//			          "holderName": "Test Uno",
//			          "number": "0002",
//			          "type": "amex"
//			        },
//			        "alias": "K961600404152739",
//			        "aliasType": "Default",
//			        "creationDate": "2018-09-10T09:57:43Z",
//			        "firstPspReference": "8535365734622231",
//			        "recurringDetailReference": "8315365734635718",
//			        "recurringList": [
//			          {
//			            "contract": "RECURRING"
//			          }
//			        ],
//			        "paymentMethodVariant": "amex",
//			        "variant": "amex",
//			        "address": {
//			          "city": "Roma",
//			          "country": "IT",
//			          "postalCode": "00100",
//			          "province": "RM",
//			          "street": "via roma 100"
//			        },
//			        "bank": null,
//			        "shopperName": null
//			      },
//			      {
//			        "additionalDataList": [
//			          {
//			            "key": "cardBin",
//			            "cardBin": "360066"
//			          }
//			        ],
//			        "card": {
//			          "cvc": null,
//			          "expiryMonth": "10",
//			          "expiryYear": "2020",
//			          "holderName": "Test Uno",
//			          "number": "3344",
//			          "type": "diners"
//			        },
//			        "alias": "A760956278894287",
//			        "aliasType": "Default",
//			        "creationDate": "2018-09-10T10:18:41Z",
//			        "firstPspReference": "8535365747210743",
//			        "recurringDetailReference": "8315365747216647",
//			        "recurringList": [
//			          {
//			            "contract": "RECURRING"
//			          }
//			        ],
//			        "paymentMethodVariant": "diners",
//			        "variant": "diners",
//			        "address": {
//			          "city": "Roma",
//			          "country": "IT",
//			          "postalCode": "00100",
//			          "province": "RM",
//			          "street": "via roma 100"
//			        },
//			        "bank": null,
//			        "shopperName": null
//			      },
//			      {
//			        "additionalDataList": [
//			          {
//			            "key": "cardBin",
//			            "cardBin": "400102"
//			          }
//			        ],
//			        "card": {
//			          "cvc": null,
//			          "expiryMonth": "10",
//			          "expiryYear": "2020",
//			          "holderName": "Test Uno",
//			          "number": "0009",
//			          "type": "visa"
//			        },
//			        "alias": "F826190531534372",
//			        "aliasType": "Default",
//			        "creationDate": "2018-09-10T10:23:41Z",
//			        "firstPspReference": "8825365750203634",
//			        "recurringDetailReference": "8315365750219587",
//			        "recurringList": [
//			          {
//			            "contract": "RECURRING"
//			          }
//			        ],
//			        "paymentMethodVariant": "electron",
//			        "variant": "visa",
//			        "address": {
//			          "city": "Roma",
//			          "country": "IT",
//			          "postalCode": "00100",
//			          "province": "RM",
//			          "street": "via roma 100"
//			        },
//			        "bank": null,
//			        "shopperName": null
//			      },
//			      {
//			        "additionalDataList": [
//			          {
//			            "key": "cardBin",
//			            "cardBin": "135410"
//			          }
//			        ],
//			        "card": {
//			          "cvc": null,
//			          "expiryMonth": "10",
//			          "expiryYear": "2020",
//			          "holderName": "Test Uno",
//			          "number": "4955",
//			          "type": "uatp"
//			        },
//			        "alias": "C725475035333744",
//			        "aliasType": "Default",
//			        "creationDate": "2018-09-10T12:30:54Z",
//			        "firstPspReference": "8825365826548111",
//			        "recurringDetailReference": "8315365826541691",
//			        "recurringList": [
//			          {
//			            "contract": "RECURRING"
//			          }
//			        ],
//			        "paymentMethodVariant": "uatp",
//			        "variant": "uatp",
//			        "address": {
//			          "city": "Roma",
//			          "country": "IT",
//			          "postalCode": "00100",
//			          "province": "RM",
//			          "street": "via roma 100"
//			        },
//			        "bank": null,
//			        "shopperName": null
//			      }
//			    ],
//			    "lastKnownShopperEmail": "test@test.com",
//			    "shopperReference": "39fbe13b0ffd46ef9ee08d09ec577034",
//			    "conversationID": "987654321"
//			  }
//			]
//			*/
//			logger.info("[getData - /retrievalstoredpayment]");
////			result = "[{ \"creationDate\": \"2018-09-06T14:24:40Z\", \"recurringDetailList\": [{ \"additionalDataList\": [{ \"key\": \"cardBin\", \"cardBin\": \"444433\" }], \"card\": { \"cvc\": null, \"expiryMonth\": \"8\", \"expiryYear\": \"2018\", \"holderName\": \"Test Uno\", \"number\": \"1111\", \"type\": \"visa\" }, \"alias\": \"P106674586198742\", \"aliasType\": \"Default\", \"creationDate\": \"2018-09-06T14:24:40Z\", \"firstPspReference\": \"8835362438795089\", \"recurringDetailReference\": \"8315362438809277\", \"recurringList\": [{ \"contract\": \"RECURRING\" }], \"paymentMethodVariant\": \"visacorporatecredit\", \"variant\": \"visa\", \"bank\": null, \"shopperName\": null }, { \"additionalDataList\": [{ \"key\": \"cardBin\", \"cardBin\": \"677179\" }], \"card\": { \"cvc\": null, \"expiryMonth\": \"10\", \"expiryYear\": \"2020\", \"holderName\": \"Test Uno\", \"number\": \"0004\", \"type\": \"mc\" }, \"alias\": \"P663519338012674\", \"aliasType\": \"Default\", \"creationDate\": \"2018-09-06T15:36:59Z\", \"firstPspReference\": \"8525362482191933\", \"recurringDetailReference\": \"8415362482191209\", \"recurringList\": [{ \"contract\": \"RECURRING\" }], \"paymentMethodVariant\": \"mc\", \"variant\": \"mc\", \"bank\": null, \"shopperName\": null } ], \"lastKnownShopperEmail\": \"test@test.com\", \"shopperReference\": \"39fbe13b0ffd46ef9ee08d09ec577034\", \"conversationID\": \"987654327\" }] ";
//			result = "[ { \"creationDate\": \"2018-09-06T14:24:40Z\", \"recurringDetailList\": [ { \"additionalDataList\": [ { \"key\": \"cardBin\", \"cardBin\": \"444433\" } ], \"card\": { \"cvc\": null, \"expiryMonth\": \"8\", \"expiryYear\": \"2018\", \"holderName\": \"Test Uno\", \"number\": \"1111\", \"type\": \"visa\" }, \"alias\": \"P106674586198742\", \"aliasType\": \"Default\", \"creationDate\": \"2018-09-06T14:24:40Z\", \"firstPspReference\": \"8835362438795089\", \"recurringDetailReference\": \"8315362438809277\", \"recurringList\": [ { \"contract\": \"RECURRING\" } ], \"paymentMethodVariant\": \"visacorporatecredit\", \"variant\": \"visa\", \"address\": { \"city\": \"Roma\", \"country\": \"IT\", \"postalCode\": \"00100\", \"province\": \"RM\", \"street\": \"via roma 100\" }, \"bank\": null, \"shopperName\": null }, { \"additionalDataList\": [ { \"key\": \"cardBin\", \"cardBin\": \"677179\" } ], \"card\": { \"cvc\": null, \"expiryMonth\": \"10\", \"expiryYear\": \"2020\", \"holderName\": \"Test Uno\", \"number\": \"0004\", \"type\": \"mc\" }, \"alias\": \"P663519338012674\", \"aliasType\": \"Default\", \"creationDate\": \"2018-09-06T15:36:59Z\", \"firstPspReference\": \"8525362482191933\", \"recurringDetailReference\": \"8415362482191209\", \"recurringList\": [ { \"contract\": \"RECURRING\" } ], \"paymentMethodVariant\": \"mc\", \"variant\": \"mc\", \"address\": { \"city\": \"Roma\", \"country\": \"IT\", \"postalCode\": \"00100\", \"province\": \"RM\", \"street\": \"via roma 100\" }, \"bank\": null, \"shopperName\": null }, { \"additionalDataList\": [ { \"key\": \"cardBin\", \"cardBin\": \"370000\" } ], \"card\": { \"cvc\": null, \"expiryMonth\": \"10\", \"expiryYear\": \"2020\", \"holderName\": \"Test Uno\", \"number\": \"0002\", \"type\": \"amex\" }, \"alias\": \"K961600404152739\", \"aliasType\": \"Default\", \"creationDate\": \"2018-09-10T09:57:43Z\", \"firstPspReference\": \"8535365734622231\", \"recurringDetailReference\": \"8315365734635718\", \"recurringList\": [ { \"contract\": \"RECURRING\" } ], \"paymentMethodVariant\": \"amex\", \"variant\": \"amex\", \"address\": { \"city\": \"Roma\", \"country\": \"IT\", \"postalCode\": \"00100\", \"province\": \"RM\", \"street\": \"via roma 100\" }, \"bank\": null, \"shopperName\": null }, { \"additionalDataList\": [ { \"key\": \"cardBin\", \"cardBin\": \"360066\" } ], \"card\": { \"cvc\": null, \"expiryMonth\": \"10\", \"expiryYear\": \"2020\", \"holderName\": \"Test Uno\", \"number\": \"3344\", \"type\": \"diners\" }, \"alias\": \"A760956278894287\", \"aliasType\": \"Default\", \"creationDate\": \"2018-09-10T10:18:41Z\", \"firstPspReference\": \"8535365747210743\", \"recurringDetailReference\": \"8315365747216647\", \"recurringList\": [ { \"contract\": \"RECURRING\" } ], \"paymentMethodVariant\": \"diners\", \"variant\": \"diners\", \"address\": { \"city\": \"Roma\", \"country\": \"IT\", \"postalCode\": \"00100\", \"province\": \"RM\", \"street\": \"via roma 100\" }, \"bank\": null, \"shopperName\": null }, { \"additionalDataList\": [ { \"key\": \"cardBin\", \"cardBin\": \"400102\" } ], \"card\": { \"cvc\": null, \"expiryMonth\": \"10\", \"expiryYear\": \"2020\", \"holderName\": \"Test Uno\", \"number\": \"0009\", \"type\": \"visa\" }, \"alias\": \"F826190531534372\", \"aliasType\": \"Default\", \"creationDate\": \"2018-09-10T10:23:41Z\", \"firstPspReference\": \"8825365750203634\", \"recurringDetailReference\": \"8315365750219587\", \"recurringList\": [ { \"contract\": \"RECURRING\" } ], \"paymentMethodVariant\": \"electron\", \"variant\": \"visa\", \"address\": { \"city\": \"Roma\", \"country\": \"IT\", \"postalCode\": \"00100\", \"province\": \"RM\", \"street\": \"via roma 100\" }, \"bank\": null, \"shopperName\": null }, { \"additionalDataList\": [ { \"key\": \"cardBin\", \"cardBin\": \"135410\" } ], \"card\": { \"cvc\": null, \"expiryMonth\": \"10\", \"expiryYear\": \"2020\", \"holderName\": \"Test Uno\", \"number\": \"4955\", \"type\": \"uatp\" }, \"alias\": \"C725475035333744\", \"aliasType\": \"Default\", \"creationDate\": \"2018-09-10T12:30:54Z\", \"firstPspReference\": \"8825365826548111\", \"recurringDetailReference\": \"8315365826541691\", \"recurringList\": [ { \"contract\": \"RECURRING\" } ], \"paymentMethodVariant\": \"uatp\", \"variant\": \"uatp\", \"address\": { \"city\": \"Roma\", \"country\": \"IT\", \"postalCode\": \"00100\", \"province\": \"RM\", \"street\": \"via roma 100\" }, \"bank\": null, \"shopperName\": null } ], \"lastKnownShopperEmail\": \"test@test.com\", \"shopperReference\": \"39fbe13b0ffd46ef9ee08d09ec577034\", \"conversationID\": \"987654321\" } ]";
//			break;
//
//		default:
//			break;
//		}
//		
//		return result;
//	}
	//Mock - Tolentino

}
