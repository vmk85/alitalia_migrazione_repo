package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.SocialAccountsRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.GetSocialAccountRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;

@Component(immediate = true, metatype = false)
@Service(value = SocialAccountsRequestToGetSocialAccountsRequest.class)
public class SocialAccountsRequestToGetSocialAccountsRequest implements Converter<SocialAccountsRequest, GetSocialAccountRequest> {

	@Reference
	private MMCustomerProfileDataToCustomerProfile mmCustomerProfileDataConverter;
	
	@Override
	public GetSocialAccountRequest convert(SocialAccountsRequest source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		
		ObjectFactory objectFactory = new ObjectFactory();
		
		GetSocialAccountRequest destination = objectFactory.createGetSocialAccountRequest();
		
		JAXBElement<String> jaxbPin = objectFactory.createGetSocialAccountRequestPin(source.getPin());
		JAXBElement<String> jaxbUserCode = objectFactory.createGetSocialAccountRequestUserCode(source.getUserCode()); 
		destination.setPin(jaxbPin);
		destination.setUserCode(jaxbUserCode);
				
		return destination;
	}
}