package com.alitalia.aem.service.impl.home;

import com.alitalia.aem.ws.booking.paymentservice.SabreDcPaymentServiceClient;
import com.day.cq.wcm.api.Page;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.enumerations.InitializePaymentErrorEnum;
import com.alitalia.aem.common.messages.home.AuthorizePaymentRequest;
import com.alitalia.aem.common.messages.home.AuthorizePaymentResponse;
import com.alitalia.aem.common.messages.home.CheckPaymentRequest;
import com.alitalia.aem.common.messages.home.CheckPaymentResponse;
import com.alitalia.aem.common.messages.home.InitializePaymentRequest;
import com.alitalia.aem.common.messages.home.InitializePaymentResponse;
import com.alitalia.aem.common.messages.home.RetrieveTicketRequest;
import com.alitalia.aem.common.messages.home.RetrieveTicketResponse;
import com.alitalia.aem.service.api.home.PaymentService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.bookingpaymentservice.AuthorizePaymentRequestConverter;
import com.alitalia.aem.service.impl.converter.home.bookingpaymentservice.AuthorizePaymentResponseConverter;
import com.alitalia.aem.service.impl.converter.home.bookingpaymentservice.CheckPaymentRequestConverter;
import com.alitalia.aem.service.impl.converter.home.bookingpaymentservice.CheckPaymentResponseConverter;
import com.alitalia.aem.service.impl.converter.home.bookingpaymentservice.InitializePaymentRequestConverter;
import com.alitalia.aem.service.impl.converter.home.bookingpaymentservice.InitializePaymentResponseConverter;
import com.alitalia.aem.service.impl.converter.home.bookingpaymentservice.RetrieveTicketRequestConverter;
import com.alitalia.aem.service.impl.converter.home.bookingpaymentservice.RetrieveTicketResponseConverter;
import com.alitalia.aem.ws.booking.paymentservice.PaymentServiceClient;
import com.alitalia.aem.ws.booking.paymentservice.wsdl.IPaymentServiceAuthorizePaymentServiceFaultFaultFaultMessage;
import com.alitalia.aem.ws.booking.paymentservice.wsdl.IPaymentServiceInitializePaymentServiceFaultFaultFaultMessage;
import com.alitalia.aem.ws.booking.paymentservice.xsd1.ErrorMessageCode;
import com.alitalia.aem.ws.booking.paymentservice.xsd5.ArrayOfKeyValueOfstringstring;
import com.alitalia.aem.ws.booking.paymentservice.xsd5.ArrayOfKeyValueOfstringstring.KeyValueOfstringstring;


@Service
@Component(immediate = true, metatype = false)
public class SimplePaymentService implements PaymentService {

	private static final Logger logger = LoggerFactory.getLogger(SimplePaymentService.class);

	@Reference
	private PaymentServiceClient paymentServiceClient;

	@Reference
	private InitializePaymentRequestConverter initializePaymentRequestConverter;

	@Reference
	private InitializePaymentResponseConverter initializePaymentResponseConverter;

	@Reference
	private AuthorizePaymentRequestConverter authorizePaymentRequestConverter;

	@Reference
	private AuthorizePaymentResponseConverter authorizePaymentResponseConverter;

	@Reference
	private CheckPaymentRequestConverter checkPaymentRequestConverter;

	@Reference
	private CheckPaymentResponseConverter checkPaymentResponseConverter;

	@Reference
	private RetrieveTicketRequestConverter retrieveTicketRequestConverter;

	@Reference
	private RetrieveTicketResponseConverter retrieveTicketResponseConverter;

	/** Migrazione 3.6 inizio*/

	@Reference
	private ResourceResolverFactory resolverFactory;

	@Reference
	private SabreDcPaymentServiceClient sabreDcPaymentServiceClient;

	/** Migrazione 3.6 fine*/

	@Override
	public InitializePaymentResponse initializePayment(InitializePaymentRequest request) {

		InitializePaymentResponse response = new InitializePaymentResponse();

		try {
			long time_before_req = System.currentTimeMillis();
			com.alitalia.aem.ws.booking.paymentservice.xsd1.InitializePaymentRequest request_ = initializePaymentRequestConverter.convert(request);
			long time_after_req = System.currentTimeMillis();

			/** Migrazione 3.6 inizio*/

			com.alitalia.aem.ws.booking.paymentservice.xsd1.InitializePaymentResponse response_ = null;

			if(newSabreEnable( request.getPrenotation().getPayment().getProcess().getMarketCode().toLowerCase())){

				response_ = sabreDcPaymentServiceClient.initializePayment(request_);

			}else{

				response_ = paymentServiceClient.initializePayment(request_);

			}

			/** Migrazione 3.6 fine*/

//			com.alitalia.aem.ws.booking.paymentservice.xsd1.InitializePaymentResponse response_ = paymentServiceClient.initializePayment(request_, request.getPrenotation().getPayment().getProcess().getMarketCode());
			long time_before_resp = System.currentTimeMillis();
			response = initializePaymentResponseConverter.convert(response_);
			long time_after_resp = System.currentTimeMillis();
			logger.info("InitializePayment: request converter execution time: {}. Response converter execution time: {}", (time_after_req - time_before_req), (time_after_resp - time_before_resp));
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}

			return response;
		} catch (IPaymentServiceInitializePaymentServiceFaultFaultFaultMessage e1){
			logger.error("Exception1 while executing method initializePayment(): {}", e1);

			if(e1.getFaultInfo() != null){
				ArrayOfKeyValueOfstringstring messageDictionaries = e1.getFaultInfo().getRemoteSystemMessageDictionary().getValue();
				if (messageDictionaries.getKeyValueOfstringstring() != null){
					for (KeyValueOfstringstring message : messageDictionaries.getKeyValueOfstringstring()){
						if (message.getValue().indexOf("\"errorCode") != -1){ //IN STATISTICHE: MSG ERRORE SABRE
							String errorMessage = message.getValue().substring(message.getValue().indexOf("\"errorCode"));
							response.setErrorStatisticMessage(errorMessage);
							if (message.getValue()!=null && message.getValue().contains("ERR.SSW.PURCHASE.INFANT_INVENTORY_UNAVAILABLE")){
								response.setErrorCode(InitializePaymentErrorEnum.INFANT_INVENTORY_UNAVAILABLE);
							}
							response.setTid(request.getTid());
							response.setSid(request.getSid());
							return response;
						}
					}
				}
				if(e1.getFaultInfo().getReasonErrorMessageCode() != null){
					response.setErrorStatisticMessage(e1.getFaultInfo().getReasonErrorMessageCode().value());
					if (e1.getFaultInfo().getReasonErrorMessageCode() == ErrorMessageCode.RISK_SCORE_PAYMENT_ERROR)
						response.setErrorCode(InitializePaymentErrorEnum.INSUFFICIENT_PLAFOND);
					else if (e1.getFaultInfo().getReasonErrorMessageCode() == ErrorMessageCode.ERROR_AZ_COMUNICATION) 
						response.setErrorCode(InitializePaymentErrorEnum.INVALID_MERCHANT_ID);
					else if (e1.getFaultInfo().getReasonErrorMessageCode() == ErrorMessageCode.ERROR_PAYMENT) 
						response.setErrorCode(InitializePaymentErrorEnum.BLOCKED_MERCHANT_ID);
					response.setTid(request.getTid());
					response.setSid(request.getSid());
					return response;
				}
			}
			throw new AlitaliaServiceException("Exception1 while executing method initializePayment(): Sid ["+request.getSid()+"]" , e1);
		}catch(Exception e){
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing initializePayment: Sid ["+request.getSid()+"]" , e);
		}
	}


	@Override
	public AuthorizePaymentResponse authorizePayment(AuthorizePaymentRequest request) {

		AuthorizePaymentResponse response = new AuthorizePaymentResponse();

		try {

			long time_before_req = System.currentTimeMillis();
			com.alitalia.aem.ws.booking.paymentservice.xsd1.AuthorizePaymentRequest request_ = authorizePaymentRequestConverter.convert(request);
			long time_after_req = System.currentTimeMillis();

			/** Migrazione 3.6 inizio*/

			com.alitalia.aem.ws.booking.paymentservice.xsd1.AuthorizePaymentResponse response_ = null;

			if(newSabreEnable( request.getPrenotation().getPayment().getProcess().getMarketCode().toLowerCase())){

				response_ = sabreDcPaymentServiceClient.authorizePayment(request_);
			}else{

				response_ = paymentServiceClient.authorizePayment(request_);
			}

			/** Migrazione 3.6 fine*/

//			com.alitalia.aem.ws.booking.paymentservice.xsd1.AuthorizePaymentResponse response_ = paymentServiceClient.authorizePayment(request_, request.getPrenotation().getPayment().getProcess().getMarketCode());
			long time_before_resp = System.currentTimeMillis();
			response = authorizePaymentResponseConverter.convert(response_);
			long time_after_resp = System.currentTimeMillis();
			logger.info("AuthorizePayment: request converter execution time: {}. Response converter execution time: {}", (time_after_req - time_before_req), (time_after_resp - time_before_resp));
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}",	response);
			}

			return response;
		} catch (IPaymentServiceAuthorizePaymentServiceFaultFaultFaultMessage e1){
			logger.error("Exception1 while executing method authorizePayment(): {}", e1);

			if(e1.getFaultInfo() != null){
				ArrayOfKeyValueOfstringstring messageDictionaries = e1.getFaultInfo().getRemoteSystemMessageDictionary().getValue();
				if (messageDictionaries.getKeyValueOfstringstring() != null){
					for (KeyValueOfstringstring message : messageDictionaries.getKeyValueOfstringstring()){
						if (message.getValue().indexOf("\"errorCode") != -1){ //IN STATISTICHE: MSG ERRORE SABRE
							String errorMessage = message.getValue().substring(message.getValue().indexOf("\"errorCode"));
							response.setErrorStatisticMessage(errorMessage);
							response.setTid(request.getTid());
							response.setSid(request.getSid());
							return response;
						}
					}
				}
				if (e1.getFaultInfo().getReasonErrorMessageCode() != null){ // IN STATISTICHE: MSG ERRORE BL
					response.setErrorStatisticMessage(e1.getFaultInfo().getReasonErrorMessageCode().value());
					response.setTid(request.getTid());
					response.setSid(request.getSid());
					return response;
				}
			}
			throw new AlitaliaServiceException("Exception1 while executing method authorizePayment(): Sid ["+request.getSid()+"]" , e1);
		} catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing authorizePayment: Sid ["+request.getSid()+"]" , e);
		}
	}


	@Override
	public CheckPaymentResponse checkPayment(CheckPaymentRequest request) {
		try{
			CheckPaymentResponse response = new CheckPaymentResponse();
			long time_before_req = System.currentTimeMillis();
			com.alitalia.aem.ws.booking.paymentservice.xsd1.CheckPaymentRequest request_ = checkPaymentRequestConverter.convert(request);
			long time_after_req = System.currentTimeMillis();

			/** Migrazione 3.6 inizio*/

			com.alitalia.aem.ws.booking.paymentservice.xsd1.CheckPaymentResponse response_ = null;

			if(newSabreEnable( request.getProcess().getMarketCode().toLowerCase())){

				response_ = sabreDcPaymentServiceClient.checkPayment(request_);

			}else{

				response_ = paymentServiceClient.checkPayment(request_);

			}

			/** Migrazione 3.6 fine*/

//			com.alitalia.aem.ws.booking.paymentservice.xsd1.CheckPaymentResponse response_ = paymentServiceClient.checkPayment(request_, request.getProcess().getMarketCode());
			long time_before_resp = System.currentTimeMillis();
			response = checkPaymentResponseConverter.convert(response_);
			long time_after_resp = System.currentTimeMillis();
			logger.info("CheckPayment: request converter execution time: {}. Response converter execution time: {}", (time_after_req - time_before_req), (time_after_resp - time_before_resp));
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}",	response);
			}

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing checkPayment: Sid ["+request.getSid()+"]" , e);
		}
	}


	@Override
	public RetrieveTicketResponse retrieveTicket(RetrieveTicketRequest request) {
		try{
			RetrieveTicketResponse response = new RetrieveTicketResponse();
			long time_before_req = System.currentTimeMillis();
			com.alitalia.aem.ws.booking.paymentservice.xsd1.RetrieveTicketRequest request_ = retrieveTicketRequestConverter.convert(request);
			long time_after_req = System.currentTimeMillis();

			/** Migrazione 3.6 inizio*/

			com.alitalia.aem.ws.booking.paymentservice.xsd1.RetrieveTicketResponse response_ = null;

			if(newSabreEnable(request.getProcess().getMarketCode().toLowerCase())){

				response_ = sabreDcPaymentServiceClient.retrieveTicket(request_);

			}else{

				response_ = paymentServiceClient.retrieveTicket(request_);

			}

			/** Migrazione 3.6 fine*/

//			com.alitalia.aem.ws.booking.paymentservice.xsd1.RetrieveTicketResponse response_ = paymentServiceClient.retrieveTicket(request_, request.getProcess().getMarketCode());
			long time_before_resp = System.currentTimeMillis();
			response = retrieveTicketResponseConverter.convert(response_);
			long time_after_resp = System.currentTimeMillis();
			logger.info("RetrieveTicket: request converter execution time: {}. Response converter execution time: {}", (time_after_req - time_before_req), (time_after_resp - time_before_resp));
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}",	response);
			}

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing retrieveTicket: Sid ["+request.getSid()+"]" , e);
		}
	}

	/** Migrazione 3.6 inizio*/

	private boolean newSabreEnable(String market) throws LoginException {

		ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
		Resource res = resourceResolver.getResource("/content/alitalia/alitalia-" + market);
		Page page = res.adaptTo(Page.class);
		ValueMap props = page.getProperties();
		String title = props.get("isNewService", "default value");
		logger.info("dato preso dalla pagina: " + title);
		return Boolean.parseBoolean(title);

	}

	/** Migrazione 3.6 fine*/

}