package com.alitalia.aem.service.impl.converter.home.staticdataservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveMealsRequest;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.StaticDataRequest;
import com.alitalia.aem.ws.booking.staticdataservice.xsd2.StaticDataType;


@Component(immediate = true, metatype = false)
@Service(value = RetrieveMealsRequestToStaticDataRequestConverter.class)
public class RetrieveMealsRequestToStaticDataRequestConverter implements Converter<RetrieveMealsRequest, StaticDataRequest> {
	
	@Override
	public StaticDataRequest convert(RetrieveMealsRequest source) {
		StaticDataRequest destination = new StaticDataRequest();
		
		String itemCache = source.getItemCache();
		String languageCode = source.getLanguageCode();
		String market = source.getMarket();
		
		ObjectFactory objectFactory = new ObjectFactory();
		destination.setLanguageCode(objectFactory.createStaticDataRequestLanguageCode(languageCode));
		destination.setMarket(objectFactory.createStaticDataRequestMarket(market));
		destination.setItemCache(objectFactory.createStaticDataRequestItemCache(itemCache));
		destination.setType(StaticDataType.MEAL);
		
			
		return destination;
	}
}