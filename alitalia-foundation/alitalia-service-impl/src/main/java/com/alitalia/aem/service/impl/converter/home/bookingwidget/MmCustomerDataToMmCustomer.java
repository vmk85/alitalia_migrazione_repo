package com.alitalia.aem.service.impl.converter.home.bookingwidget;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ContactData;
import com.alitalia.aem.common.data.home.FFCardData;
import com.alitalia.aem.common.data.home.MmCustomerData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.widgetservice.xsd2.GenderType;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.ArrayOfContact;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.ArrayOfFFCard;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.MmCustomer;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=MmCustomerDataToMmCustomer.class)
public class MmCustomerDataToMmCustomer 
		implements Converter<MmCustomerData, MmCustomer> {
	
	@Reference
	private FFCardDataToFFCard ffCardDataToFFCardConverter;
	
	@Reference
	private ContactDataToContact contactDataToContactConverter;
	
	@Reference
	private MealDataToMeal mealDataToMealConverter;
	
	@Reference
	private SeatTypeDataToSeatType seatTypeDataToSeatTypeConverter;
	
	@Override
	public MmCustomer convert(MmCustomerData source) {
		
		MmCustomer destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			destination = objectFactory.createMmCustomer();
			
			destination.setBirthDate(
					XsdConvertUtils.toXMLGregorianCalendar(
							source.getBirthDate()));
			
			ArrayOfFFCard cards = objectFactory.createArrayOfFFCard();
			if (source.getCards() != null) {
				for (FFCardData card : source.getCards()) {
					cards.getFFCard().add(
							ffCardDataToFFCardConverter.convert(
									card));
				}
			}
			destination.setCards(
					objectFactory.createMmCustomerCards(
							cards));
			
			destination.setCode(
					objectFactory.createMmCustomerCode(
							source.getCode()));
			
			ArrayOfContact contacts = objectFactory.createArrayOfContact();
			if (source.getCustomerPhones() != null) {
				for (ContactData contact : source.getCustomerPhones()) {
					contacts.getContact().add(
							contactDataToContactConverter.convert(
									contact));
				}
			}
			destination.setCustomerPhones(
					objectFactory.createMmCustomerCustomerPhones(
							contacts));
			
			destination.setEMail(
					objectFactory.createMmCustomerEMail(
							source.getEmail()));
			
			if (source.getGender() != null) {
				destination.setGender(
						GenderType.fromValue(source.getGender().value()));
			}
			
			destination.setIgnore(
					source.getIgnore());
			
			destination.setLastName(
					objectFactory.createMmCustomerLastName(
							source.getLastName()));
			
			destination.setMealPreference(
					objectFactory.createMmCustomerMealPreference(
							mealDataToMealConverter.convert(
								source.getMealPreference())));
			
			destination.setMilesBalance(
					source.getMilesBalance());
			
			destination.setMilesEarned(
					source.getMilesEarned());
			
			destination.setMilesQualify(
					source.getMilesQualify());
			
			destination.setName(
					objectFactory.createMmCustomerName(
							source.getName()));
			
			destination.setPin(
					objectFactory.createMmCustomerPin(
							source.getPin()));
			
			destination.setSeatPreference(
					objectFactory.createMmCustomerSeatPreference(
							seatTypeDataToSeatTypeConverter.convert(
								source.getSeatPreference())));
			
			destination.setTierCode(
					objectFactory.createMmCustomerTierCode(
							source.getTierCode()));
			
			destination.setTitle(
					objectFactory.createMmCustomerTitle(
							source.getTitle()));
		}
		
		return destination;
	}

}
