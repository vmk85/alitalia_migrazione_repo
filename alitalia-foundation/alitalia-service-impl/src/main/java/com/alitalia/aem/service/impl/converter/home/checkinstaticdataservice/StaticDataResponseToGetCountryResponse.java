package com.alitalia.aem.service.impl.converter.home.checkinstaticdataservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinCountryData;
import com.alitalia.aem.common.messages.home.GetCountryResponse;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.StaticDataResponseOfCountryUGS9ND5Y;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd2.Country;


@Component(immediate = true, metatype = false)
@Service(value = StaticDataResponseToGetCountryResponse.class)
public class StaticDataResponseToGetCountryResponse implements Converter<StaticDataResponseOfCountryUGS9ND5Y, GetCountryResponse> {

	@Reference
	private CountryToCheckinCountryData countryConverter;

	@Override
	public GetCountryResponse convert(StaticDataResponseOfCountryUGS9ND5Y source) {
		GetCountryResponse destination = new GetCountryResponse();

		List<CheckinCountryData> countriesData = new ArrayList<>();
		if (source.getEntities()!=null && source.getEntities().getValue()!=null &&
				source.getEntities().getValue().getCountry()!=null && source.getEntities().getValue().getCountry().size()>0){
			List<Country> countries = source.getEntities().getValue().getCountry();
			for (Country country : countries) {
				CheckinCountryData countryData = countryConverter.convert(country);
				countriesData.add(countryData);
			}
			destination.setCountries(countriesData);
		}

		return destination;			
	}
}