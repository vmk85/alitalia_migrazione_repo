package com.alitalia.aem.service.impl.converter.home.checkinemdservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinReasonForIssuance;
import com.alitalia.aem.ws.checkin.emdservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.checkin.emdservice.xsd2.ReasonForIssuanceType;



@Component(immediate=true, metatype=false)
@Service(value=CheckinReasonForIssuanceToReasonForIssuanceType.class)
public class CheckinReasonForIssuanceToReasonForIssuanceType implements Converter<CheckinReasonForIssuance, ReasonForIssuanceType> {

	@Override
	public ReasonForIssuanceType convert(CheckinReasonForIssuance source) {
		ObjectFactory objectFactory = new ObjectFactory();
		ReasonForIssuanceType destination = null;

		if (source != null) {
			destination = objectFactory.createReasonForIssuanceType();
			destination.setX003CReasonCodeX003EKBackingField(source.getReasonCode());
			destination.setX003CReasonDescriptionX003EKBackingField(source.getReasonDescription());
			destination.setX003CReasonSubCodeX003EKBackingField(source.getReasonSubCode());
		}
		return destination;
	}
}