package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.FlightDetailsData;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.FlightDetails;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=FlightDetailsDataToFlightDetails.class)
public class FlightDetailsDataToFlightDetails implements Converter<FlightDetailsData, FlightDetails> {

	@Override
	public FlightDetails convert(FlightDetailsData source) {
		ObjectFactory factory3 = new ObjectFactory();
		FlightDetails destination = factory3.createFlightDetails();

		destination.setAeromobile(factory3.createFlightDetailsAeromobile(source.getAeromobile()));
		destination.setArrivalTerminal(factory3.createFlightDetailsArrivalTerminal(source.getArrivalTerminal()));
		destination.setDepartureTerminal(factory3.createFlightDetailsDepartureTerminal(source.getDepartureTerminal()));
		destination.setDistance(factory3.createFlightDetailsDistance(source.getDistance()));
		destination.setOtherInformation(factory3.createFlightDetailsOtherInformation(source.getOtherInformation()));

		return destination;
	}

}
