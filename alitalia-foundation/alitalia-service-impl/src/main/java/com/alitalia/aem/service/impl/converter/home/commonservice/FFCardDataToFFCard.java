package com.alitalia.aem.service.impl.converter.home.commonservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.FFCardData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.commonservice.xsd2.FFCard;
import com.alitalia.aem.ws.booking.commonservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.booking.commonservice.xsd6.CardStaus;

@Component(immediate=true, metatype=false)
@Service(value=FFCardDataToFFCard.class)
public class FFCardDataToFFCard 
		implements Converter<FFCardData, FFCard> {
	
	
	@Override
	public FFCard convert(FFCardData source) {
		
		FFCard destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			destination = objectFactory.createFFCard();
			
			destination.setCode(
					objectFactory.createFFCardCode(
							source.getCode()));
			
			destination.setEndDate(
					XsdConvertUtils.toXMLGregorianCalendar(
							source.getEndDate()));
			
			destination.setOther(
					objectFactory.createFFCardOther(
							source.getOther()));
			
			destination.setStartDate(
					XsdConvertUtils.toXMLGregorianCalendar(
							source.getStartDate()));
			
			if (source.getStatus() != null) {
				destination.setStatus(
						CardStaus.fromValue(
								source.getStatus().value()));
			}

			destination.setText(
					objectFactory.createFFCardText(
							source.getText()));
			
			destination.setVIPCode(
					objectFactory.createFFCardVIPCode(
							source.getVipCode()));
			
		}
		
		return destination;
	}

}
