package com.alitalia.aem.service.impl.beanrest.staticdataservicerest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.CountriesRequest;
import com.alitalia.aem.common.messages.home.CountriesResponse;
import com.alitalia.aem.common.messages.home.CountryStatesRequest;
import com.alitalia.aem.common.messages.home.CountryStatesResponse;
import com.alitalia.aem.common.messages.home.GetDestinationsResponse;
import com.alitalia.aem.common.messages.home.GetFrequentFlyerRequest;
import com.alitalia.aem.common.messages.home.GetFrequentFlyerResponse;
import com.alitalia.aem.common.messages.home.RetrieveAirportDestinationRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.common.messages.home.RetrieveAvailabilityRequest;
import com.alitalia.aem.common.messages.home.RetrieveAvailabilityResponse;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.api.homerest.StaticDataServiceRest;
import com.alitalia.aem.service.impl.converter.homerest.beans.DataRequest;
import com.alitalia.aem.service.impl.converter.homerest.beans.GetAllAirports;
import com.alitalia.aem.service.impl.converter.homerest.datachange.RestDataChange;
import com.alitalia.aem.service.impl.converter.homerest.restresponseunmarshal.AirportDestinationResponseUnmarshal;
import com.alitalia.aem.service.impl.converter.homerest.restresponseunmarshal.AllAirportsResponseUnmarshal;
import com.alitalia.aem.service.impl.converter.homerest.restresponseunmarshal.AvailabilityResponseUnmarshal;
import com.alitalia.aem.service.impl.converter.homerest.restresponseunmarshal.CountriesUnmarshal;
import com.alitalia.aem.service.impl.converter.homerest.restresponseunmarshal.CountryStatesResponseUnmarshal;
import com.alitalia.aem.service.impl.converter.homerest.restresponseunmarshal.FrequentFlyerResponseUnmarshal;
import com.alitalia.aem.service.impl.converter.homerest.staticdataservice.StaticDataServiceClientRest;
import com.alitalia.aem.service.impl.home.SimpleStaticDataService;

@Service
@Component(immediate = true, metatype = false)
public class SimpleStaticDataServiceRest implements StaticDataServiceRest {
	
	private static final Logger logger = LoggerFactory.getLogger(SimpleStaticDataService.class);
	
	@Reference
	StaticDataServiceClientRest staticDataServiceClientRest;
	
	@Override
	public RetrieveAirportsResponse getAllAirports(RetrieveAirportsRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveAirports. The request is {}", request);
		}
		
		GetAllAirports responseToChange = new GetAllAirports();
		RestDataChange restDataChange = new RestDataChange();
		RetrieveAirportsResponse response = new RetrieveAirportsResponse();
		AllAirportsResponseUnmarshal allAirportsResponseUnmarshal = new AllAirportsResponseUnmarshal();
		DataRequest dataRequest = new DataRequest();
		dataRequest.setMarket(request.getMarket());
		dataRequest.setLanguage(request.getLanguageCode());
		dataRequest.setConversationID(request.getConversationID());
		
		try {
			String relativePath = "/getallairports";
			String clientResponse = staticDataServiceClientRest.getData(dataRequest, relativePath);
			responseToChange = allAirportsResponseUnmarshal.allAirportsUnmarshal(clientResponse);
			response = restDataChange.changeDataForResponse(responseToChange);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			logger.info("Chiamato getData di StaticDataServiceClientRest in getAllAirports e ottenuta response.");
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
			
			return response;
		}
		catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing retrieveAirports: Sid ["+request.getSid()+"]" , e);
		}

	}

	@Override
	public GetDestinationsResponse getAirportDestination(RetrieveAirportDestinationRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveAirportDestination. The request is {}", request);
		}
		
		GetDestinationsResponse response = new GetDestinationsResponse();
		AirportDestinationResponseUnmarshal airportDestinationResponseUnmarshal = new AirportDestinationResponseUnmarshal();
		
		try {
			String relativePath = "/getdestinationbyairport";
			String clientResponse = staticDataServiceClientRest.getData(request, relativePath);
			response = airportDestinationResponseUnmarshal.airportDestinationUnmarshal(clientResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			logger.info("Chiamato getData di StaticDataServiceClientRest in getAirportDestination e ottenuta response.");
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
			
			return response;
		}
		catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing retrieveAirportDestination: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public RetrieveAvailabilityResponse getAvailability(RetrieveAvailabilityRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method getAvailability. The request is {}", request);
		}
		
		RetrieveAvailabilityResponse response = new RetrieveAvailabilityResponse();
		AvailabilityResponseUnmarshal availabilityResponseUnmarshal = new AvailabilityResponseUnmarshal();
		
		try {
			String relativePath ="/getoedavailability";
			String clientResponse = staticDataServiceClientRest.getData(request, relativePath);
			response = availabilityResponseUnmarshal.availabilityUnmarshal(clientResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			logger.info("Chiamato getData di StaticDataServiceClientRest in getAvailability e ottenuta response.");
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
			
			return response;
		}
		catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing retrieveAvailabilityData: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public RetrieveAvailabilityResponse getAvailabilityRTN(RetrieveAvailabilityRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method getAvailabilityRTN. The request is {}", request);
		}

		RetrieveAvailabilityResponse response = new RetrieveAvailabilityResponse();
		AvailabilityResponseUnmarshal availabilityResponseUnmarshal = new AvailabilityResponseUnmarshal();

		try {
			String relativePath ="/getoedavailabilityrtn";
			String clientResponse = staticDataServiceClientRest.getData(request, relativePath);
			response = availabilityResponseUnmarshal.availabilityUnmarshal(clientResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			logger.info("Chiamato getData di StaticDataServiceClientRest in getAvailabilityRTN e ottenuta response.");
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}

			return response;
		}
		catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing getAvailabilityRTN: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public GetFrequentFlyerResponse getFrequentFlyers(GetFrequentFlyerRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method getFrequentFlyer. the request is {}", request);
		}
		GetFrequentFlyerResponse response = null;
		FrequentFlyerResponseUnmarshal frequentFlyerUnmarshal = new FrequentFlyerResponseUnmarshal();
		try {
			response = new GetFrequentFlyerResponse();
			String url = "/getfrequentflyer";
			String clientResponse = staticDataServiceClientRest.getData(request, url);

			//Chiedo i dati al CheckinServiceClient
			response = frequentFlyerUnmarshal.ffUnmarshal(clientResponse);
			
			return response;
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}

	@Override
	public CountryStatesResponse getCountryStates(CountryStatesRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method getCountryStates. The request is {}", request);
		}
		
		CountryStatesResponse response = new CountryStatesResponse();
		CountryStatesResponseUnmarshal countryStatesResponseUnmarshal = new CountryStatesResponseUnmarshal();
		
		try {
			String relativePath ="/getcountrystates";
			String clientResponse = staticDataServiceClientRest.getData(request, relativePath);
			response = countryStatesResponseUnmarshal.countryStatesUnmarshal(clientResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			logger.info("Chiamato getData di StaticDataServiceClientRest in getCountryStates e ottenuta response.");
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
			
			return response;
		}
		catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing getCountryStates in SimpleStaticDataServiceRest: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public CountriesResponse getCountries(CountriesRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method getCountries. The request is {}", request);
		}
		
		CountriesResponse response = new CountriesResponse();
		CountriesUnmarshal countriesUnmarshal = new CountriesUnmarshal();
		
		try {
			String relativePath ="/getcountries";
			String clientResponse = staticDataServiceClientRest.getData(request, relativePath);
			response = countriesUnmarshal.countriesUnmarshal(clientResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			logger.info("Chiamato getData di StaticDataServiceClientRest in getCountries e ottenuta response.");
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
			
			return response;
		}
		catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing getCountries in SimpleStaticDataServiceRest: Sid ["+request.getSid()+"]" , e);
		}
	}

	
}