package com.alitalia.aem.service.impl.converter.home.geospatialservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveGeoCitiesRequest;
import com.alitalia.aem.ws.geospatial.service.xsd1.GetCitiesRequest;
import com.alitalia.aem.ws.geospatial.service.xsd1.ObjectFactory;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveGeoCitiesRequestToGetCitiesRequest.class)
public class RetrieveGeoCitiesRequestToGetCitiesRequest implements Converter<RetrieveGeoCitiesRequest, GetCitiesRequest> {

	@Override
	public GetCitiesRequest convert(RetrieveGeoCitiesRequest source) {
		
		GetCitiesRequest request = new GetCitiesRequest();
		
		String languageCode = source.getLanguageCode();
		String marketCode = source.getMarketCode();
		String paxType = source.getPaxType();
		Integer down = source.getRngpryDwn();
		Integer up = source.getRngpryUp();
		String target = source.getTarget();
		
		ObjectFactory objectFactory = new ObjectFactory();
		
		// FIXME verificare il nuovo WSDL perchè sembrerebbe cambiata la struttura della request
		JAXBElement<String> jaxbLanguageCode = objectFactory.createGetOffersRequestLanguageCode(languageCode);
		JAXBElement<String> jaxbMarketCode = objectFactory.createGetOffersRequestMarketCode(marketCode);
		JAXBElement<String> jaxbPaxType = objectFactory.createGetOffersRequestPaxType(paxType);
//		JAXBElement<String> jaxbTarget = objectFactory.createGetOffersRequestTarget(target);
		
		request.setLanguageCode(jaxbLanguageCode);
		request.setMarketCode(jaxbMarketCode);
		request.setPaxType(jaxbPaxType);
//		request.setRngpryDwn(down);
//		request.setRngpryUp(up);
//		request.setTarget(jaxbTarget);
		
		return request;
	}
}