package com.alitalia.aem.service.impl.converter.home.staticdataservice;
import org.apache.felix.scr.annotations.Service;
import org.apache.felix.scr.annotations.Component;

import com.alitalia.aem.ws.booking.staticdataservice.xsd5.PhonePrefix;
import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PhonePrefixData;

@Component(immediate=true, metatype=false)
@Service(value=PhonePrefixToPhonePrefixData.class)
public class PhonePrefixToPhonePrefixData implements Converter<PhonePrefix, PhonePrefixData> {

	@Override
	public PhonePrefixData convert(PhonePrefix source) {
		PhonePrefixData prefixData = new PhonePrefixData();
		prefixData.setCode(source.getCode().getValue());
		prefixData.setDescription(source.getDescription().getValue());
		prefixData.setPrefix(source.getPrefix().getValue());
		return prefixData;
	}

}
