package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.BagsAndRuleRequest;
import com.alitalia.aem.common.messages.home.BagsAndRuleResponse;
import com.alitalia.aem.service.api.home.BookingBagsAndRulesService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.bookingbagsandrulesservice.BagsAndFareRuleResponseToBagsAndRuleResponse;
import com.alitalia.aem.service.impl.converter.home.bookingbagsandrulesservice.BagsAndRuleRequestToBagsAndFareRuleRequest;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.BagsAndRulesServiceClient;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd1.BagsAndFareRuleRequest;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd4.BagsAndFareRuleResponse;

@Service
@Component(immediate = true, metatype = false)
public class SimpleBookingBagsAndRulesService implements BookingBagsAndRulesService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleBookingBagsAndRulesService.class);

	@Reference
	private BagsAndRulesServiceClient bagsAndRulesServiceClient;

	@Reference
	private BagsAndRuleRequestToBagsAndFareRuleRequest bagsAndRulesRequestConverter;
	
	@Reference
	private BagsAndFareRuleResponseToBagsAndRuleResponse bagsAndRulesResponseConverter;

	@Override
	public BagsAndRuleResponse execute(BagsAndRuleRequest request) {
		
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method BagsAndRuleRequest-execute. The request is {}", request);
		}

		try {
			
			BagsAndRuleResponse response = new BagsAndRuleResponse();
			BagsAndFareRuleRequest bagsAndRulesRequest = bagsAndRulesRequestConverter.convert(request);
			BagsAndFareRuleResponse bagsAndFareRulesResponse = bagsAndRulesServiceClient.execute(bagsAndRulesRequest);
			response = bagsAndRulesResponseConverter.convert(bagsAndFareRulesResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			return response;
			
		} catch (Exception e) {
			logger.error("Exception while executing method execute() of BagsAndRulesService: {}", e);
			throw new AlitaliaServiceException("Exception executing BagsAndRuleRequest-execute: Sid ["+request.getSid()+"]" , e);
		}
	}
}