package com.alitalia.aem.service.impl.converter.home.staticdataservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.ws.booking.staticdataservice.xsd2.Country;

@Component(immediate=true, metatype=false)
@Service(value=CountryToCountryData.class)
public class CountryToCountryData implements Converter<Country, CountryData> {

	@Override
	public CountryData convert(Country source) {
		CountryData destination = new CountryData();
		if(source.getCode() !=null)
			destination.setCode(source.getCode().getValue());
		destination.setDescription(source.getDescription().getValue());
		destination.setStateCode(source.getStateCode().getValue());
		destination.setStateDescription(source.getStateDescription().getValue());
		return destination;
	}
}