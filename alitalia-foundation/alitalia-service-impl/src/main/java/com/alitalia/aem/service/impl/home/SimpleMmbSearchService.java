package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.GetSabrePNRRequest;
import com.alitalia.aem.common.messages.home.GetSabrePNRResponse;
import com.alitalia.aem.common.messages.home.MmbModifyPnrApisInfoRequest;
import com.alitalia.aem.common.messages.home.MmbModifyPnrApisInfoResponse;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationRequest;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationResponse;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrContactsRequest;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrContactsResponse;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrFrequentFlyerRequest;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrFrequentFlyerResponse;
import com.alitalia.aem.service.api.home.MmbSearchService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.mmbsearchservice.ApisDataResponseToMmbModifyPnrApisInfoResponse;
import com.alitalia.aem.service.impl.converter.home.mmbsearchservice.FrequentFlyResponseToUpdateMmbPnrFrequentFlyerResponse;
import com.alitalia.aem.service.impl.converter.home.mmbsearchservice.GetRoutesResponseRDBToGetSabrePNRResponseConverter;
import com.alitalia.aem.service.impl.converter.home.mmbsearchservice.GetRoutesResponseToRetriveMmbPnrInformationResponse;
import com.alitalia.aem.service.impl.converter.home.mmbsearchservice.GetSabrePNRRequestToGetRoutesRequestRDBConverter;
import com.alitalia.aem.service.impl.converter.home.mmbsearchservice.MmbModifyPnrApisInfoRequestToApisDataRequest;
import com.alitalia.aem.service.impl.converter.home.mmbsearchservice.PassengerListResponseToUpdateMmbPnrContactsResponse;
import com.alitalia.aem.service.impl.converter.home.mmbsearchservice.RetriveMmbPnrInformationRequestToGetRoutesRequestNew;
import com.alitalia.aem.service.impl.converter.home.mmbsearchservice.UpdateMmbPnrContactsRequestToPassengerListRequest;
import com.alitalia.aem.service.impl.converter.home.mmbsearchservice.UpdateMmbPnrFrequentFlyerRequestToFrequentFlyRequest;
import com.alitalia.aem.ws.mmb.searchservice.MmbSearchServiceClient;
import com.alitalia.aem.ws.mmb.searchservice.xsd1.ApisDataRequest;
import com.alitalia.aem.ws.mmb.searchservice.xsd1.ApisDataResponse;
import com.alitalia.aem.ws.mmb.searchservice.xsd1.FrequentFlyRequest;
import com.alitalia.aem.ws.mmb.searchservice.xsd1.FrequentFlyResponse;
import com.alitalia.aem.ws.mmb.searchservice.xsd1.GetRoutesRequestNew;
import com.alitalia.aem.ws.mmb.searchservice.xsd1.GetRoutesRequestRDB;
import com.alitalia.aem.ws.mmb.searchservice.xsd1.GetRoutesResponse;
import com.alitalia.aem.ws.mmb.searchservice.xsd1.GetRoutesResponseRDB;
import com.alitalia.aem.ws.mmb.searchservice.xsd1.PassengerListRequest;
import com.alitalia.aem.ws.mmb.searchservice.xsd1.PassengerListResponse;

@Component(immediate=true, metatype=false)
@Service
public class SimpleMmbSearchService implements MmbSearchService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleMmbSearchService.class);

	@Reference
	private MmbSearchServiceClient mmbSearchServiceClient;

	@Reference
	private RetriveMmbPnrInformationRequestToGetRoutesRequestNew retriveMmbPnrInformationRequestConverter;

	@Reference
	private GetRoutesResponseToRetriveMmbPnrInformationResponse getRoutesResponseConverter;

	@Reference
	private UpdateMmbPnrContactsRequestToPassengerListRequest updateMmbPnrContactsRequestConverter;

	@Reference
	private PassengerListResponseToUpdateMmbPnrContactsResponse passengerListResponseConverter;

	@Reference
	private MmbModifyPnrApisInfoRequestToApisDataRequest modifyMmbPnrApisInfoRequestConverter;

	@Reference
	private ApisDataResponseToMmbModifyPnrApisInfoResponse apisDataResponseConverter;

	@Reference
	private UpdateMmbPnrFrequentFlyerRequestToFrequentFlyRequest updateMmbPnrFrequentFlyerRequestConverter;

	@Reference
	private FrequentFlyResponseToUpdateMmbPnrFrequentFlyerResponse frequentFlyResponseConverter;
	
	@Reference
	private GetSabrePNRRequestToGetRoutesRequestRDBConverter getSabrePNRRequestConverter;
	
	@Reference
	private GetRoutesResponseRDBToGetSabrePNRResponseConverter getSabrePNRResponseConverter;

	@Override
	public RetriveMmbPnrInformationResponse retrivePnrInformation(RetriveMmbPnrInformationRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrivePnrInformation(). The request is {}", request);
		}
		

		try {
			RetriveMmbPnrInformationResponse response = new RetriveMmbPnrInformationResponse();
			GetRoutesRequestNew serviceRequest = retriveMmbPnrInformationRequestConverter.convert(request);
			GetRoutesResponse serviceResponse = mmbSearchServiceClient.getRouteListNew(serviceRequest);
			response = getRoutesResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully retrivePnrInformation(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method retrivePnrInformation().", e);
			throw new AlitaliaServiceException("Exception executing retrivePnrInformation: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public UpdateMmbPnrContactsResponse updatePnrContacts(UpdateMmbPnrContactsRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method updatePnrContacts(). The request is {}", request);
		}
		

		try {
			UpdateMmbPnrContactsResponse response = new UpdateMmbPnrContactsResponse();
			PassengerListRequest serviceRequest = updateMmbPnrContactsRequestConverter.convert(request);
			String pnr = request.getPnr();
			PassengerListResponse serviceResponse = mmbSearchServiceClient.modifyTelephonePNR(pnr, serviceRequest);
			response = passengerListResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully updatePnrContacts(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method updatePnrContacts().", e);
			throw new AlitaliaServiceException("Exception executing updatePnrContacts: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public MmbModifyPnrApisInfoResponse modifyPnrPassengerApisInfo(MmbModifyPnrApisInfoRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method modifyPnrPassengerApisInfo(). The request is {}", request);
		}
		

		try {
			MmbModifyPnrApisInfoResponse response = new MmbModifyPnrApisInfoResponse();
			ApisDataRequest serviceRequest = modifyMmbPnrApisInfoRequestConverter.convert(request);
			String pnr = request.getPnr();
			ApisDataResponse serviceResponse = mmbSearchServiceClient.modifyPassengerApisInfoPNR(pnr, serviceRequest);
			response = apisDataResponseConverter.convert(serviceResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully modifyPnrPassengerApisInfo(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method modifyPnrPassengerApisInfo().", e);
			throw new AlitaliaServiceException("Exception executing modifyPnrPassengerApisInfo: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public UpdateMmbPnrFrequentFlyerResponse updatePnrFrequentFlyer(UpdateMmbPnrFrequentFlyerRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method updatePnrFrequentFlyer(). The request is {}", request);
		}
		

		try {
			UpdateMmbPnrFrequentFlyerResponse response = new UpdateMmbPnrFrequentFlyerResponse();
			FrequentFlyRequest serviceRequest = updateMmbPnrFrequentFlyerRequestConverter.convert(request);
			String pnr = request.getPnr();
			FrequentFlyResponse serviceResponse = mmbSearchServiceClient.modifyFrequentFlyerPNR(pnr, serviceRequest);
			response = frequentFlyResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully updatePnrFrequentFlyer(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method updatePnrFrequentFlyer().", e);
			throw new AlitaliaServiceException("Exception executing updatePnrFrequentFlyer: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public GetSabrePNRResponse getSabrePNR(GetSabrePNRRequest request){
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method getSabrePNR(). The request is {}", request);
		}
		
		try {
			GetSabrePNRResponse response = new GetSabrePNRResponse();
			GetRoutesRequestRDB serviceRequest = getSabrePNRRequestConverter.convert(request);
			GetRoutesResponseRDB serviceResponse = mmbSearchServiceClient.getRouteListRDB(serviceRequest);
			response = getSabrePNRResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully getSabrePNR(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getSabrePNR().", e);
			throw new AlitaliaServiceException("Exception executing getSabrePNR: Sid ["+request.getSid()+"]" , e);
		}
	}

}
