package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareBasePriceData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionPricingFareBasePrice;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareBasePriceDataToRTDSPricingFareBasePrice.class)
public class RTDSPricingFareBasePriceDataToRTDSPricingFareBasePrice implements
		Converter<ResultTicketingDetailSolutionPricingFareBasePriceData, 
					ResultTicketingDetailSolutionPricingFareBasePrice> {

	@Override
	public ResultTicketingDetailSolutionPricingFareBasePrice convert(
			ResultTicketingDetailSolutionPricingFareBasePriceData source) {
		ResultTicketingDetailSolutionPricingFareBasePrice destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingFareBasePrice();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
