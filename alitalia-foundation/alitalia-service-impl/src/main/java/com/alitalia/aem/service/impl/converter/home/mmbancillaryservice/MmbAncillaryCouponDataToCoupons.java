package com.alitalia.aem.service.impl.converter.home.mmbancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryCouponData;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.Cabin;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.Coupons;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=MmbAncillaryCouponDataToCoupons.class)
public class MmbAncillaryCouponDataToCoupons implements Converter<MmbAncillaryCouponData, Coupons> {

	@Override
	public Coupons convert(MmbAncillaryCouponData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		Coupons destination = null;

		if (source != null) {
			destination = objectFactory.createCoupons();

			destination.setX003CBaggageAllowanceX003EKBackingField(source.getBaggageAllowance());

			CabinEnum sourceCabin = source.getCabin();
			if (sourceCabin != null)
				destination.setX003CCabinX003EKBackingField(Cabin.fromValue(sourceCabin.value()));

			destination.setX003CFareClassX003EKBackingField(source.getFareClass());
			destination.setX003CFlightIdX003EKBackingField(source.getFlightId());
			destination.setX003CFlightNumberX003EKBackingField(source.getFlightNumber());
			destination.setX003CInboundCouponNumberX003EKBackingField(source.getInboundCouponNumber());
			destination.setX003CIsChildX003EKBackingField(source.isChild());
			destination.setX003CIsInfantAccompanistX003EKBackingField(source.isInfantAccompanist());
			destination.setX003CIsInfantX003EKBackingField(source.isInfant());
			destination.setX003CIsSpecialPassengerX003EKBackingField(source.isSpecialPassenger());
			destination.setX003CNumberX003EKBackingField(source.getNumber());
			destination.setX003CPassengerNumberX003EKBackingField(source.getPassengerNumber());
		}

		return destination;
	}

}
