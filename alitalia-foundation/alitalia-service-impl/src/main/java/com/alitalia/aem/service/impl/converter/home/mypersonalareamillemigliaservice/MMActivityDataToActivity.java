package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMActivityData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Activity;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ActivityTypes;

@Component(immediate = true, metatype = false)
@Service(value = MMActivityDataToActivity.class)
public class MMActivityDataToActivity implements Converter<MMActivityData, Activity> {

	@Override
	public Activity convert(MMActivityData source) {
		Activity destination = null;

		if (source != null) {
			com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ObjectFactory objectFactory2 = new com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ObjectFactory();

			destination = objectFactory2.createActivity();

			destination.setAmount(source.getAmount());
			destination.setBasic(source.getBasic());
			destination.setBonus(source.getBonus());
			destination.setCustomerNumber(objectFactory2.createActivityCustomerNumber(source.getCustomerNumber()));
			destination.setDescription(objectFactory2.createActivityDescription(source.getDescription()));
			destination.setPointsTotal(source.getPointsTotal());
			destination.setQualified(source.getQualified());
			destination.setReferenceNumber(objectFactory2.createActivityReferenceNumber(source.getReferenceNumber()));
			destination.setRowID(objectFactory2.createActivityRowID(source.getRowId()));
			destination.setSequenceNumber(objectFactory2.createActivitySequenceNumber(source.getSequenceNumber()));

			if (source.getStatementDate() != null)
				destination.setStatementDate(XsdConvertUtils.toXMLGregorianCalendar(source.getStatementDate()));
			if (source.getActivityDate() != null)
				destination.setActivityDate(XsdConvertUtils.toXMLGregorianCalendar(source.getActivityDate()));
			if (source.getActivityType() != null)
				destination.setActivityType(ActivityTypes.fromValue(source.getActivityType().value()));
		}

		return destination;
	}

}
