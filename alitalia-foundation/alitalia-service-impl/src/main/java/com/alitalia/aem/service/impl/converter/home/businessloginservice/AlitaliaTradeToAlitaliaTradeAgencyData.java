package com.alitalia.aem.service.impl.converter.home.businessloginservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AlitaliaTradeAgencyData;
import com.alitalia.aem.common.data.home.enumerations.AlitaliaTradeUserType;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.b2bv2.xsd1.AlitaliaTrade;

@Component
@Service(value=AlitaliaTradeToAlitaliaTradeAgencyData.class)
public class AlitaliaTradeToAlitaliaTradeAgencyData implements Converter<AlitaliaTrade, AlitaliaTradeAgencyData> {

	@Override
	public AlitaliaTradeAgencyData convert(AlitaliaTrade source) {
		AlitaliaTradeAgencyData destination = new AlitaliaTradeAgencyData();

		destination.setAccountLocked(XsdConvertUtils.parseBoolean(source.getAccountSospeso()));
		String citta = source.getCitta();
		if (citta != null)
			destination.setCitta(citta.trim());

		destination.setCODFAMCON(source.getCODFAMCON());
		destination.setCodiceAccordo(source.getCodiceAccordo());
		destination.setCodiceAgenzia(source.getCodiceAgenzia());
		destination.setCodiceFiscale(source.getCodiceFiscale());
		destination.setCodiceSirax(source.getCodiceSirax());
		destination.setCondizioniAccettate(XsdConvertUtils.parseBoolean(source.getCondizioniAccettate()));
		destination.setDataValiditaAccordo(source.getDataValiditaAccordo());
		String email = source.getEmail();
		if (email != null)
			destination.setEmail(email.trim());
		email = source.getEmailAlitalia();
		if (email != null)
			destination.setEmailAlitalia(email.trim());
		
		String indirizzo = source.getIndirizzo();
		if (indirizzo != null)
			destination.setIndirizzo(indirizzo.trim());

		destination.setNuovoUtente(XsdConvertUtils.parseBoolean(source.getNuovoUtente()));
		destination.setPartitaIVA(source.getPartitaIVA());
		destination.setPassword(source.getPassword());
		destination.setPasswordExpired(XsdConvertUtils.parseBoolean(source.getPasswordScaduta()));
		
		String provincia = source.getProvincia();
		if (provincia != null)
			destination.setProvincia(provincia.trim());

		destination.setRagioneSociale(source.getRagioneSociale());

		String regione = source.getRegione();
		if (regione != null)
			destination.setRegione(regione.trim());
		destination.setRuolo(AlitaliaTradeUserType.fromValue(source.getRuolo().trim()));

		String stato = source.getStato();
		if (stato != null)
			destination.setStato(stato.trim());

		destination.setZip(source.getZip());

		return destination;
	}

}
