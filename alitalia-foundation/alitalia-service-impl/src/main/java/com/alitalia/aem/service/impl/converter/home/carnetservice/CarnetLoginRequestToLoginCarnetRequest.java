/**
 * 
 */
package com.alitalia.aem.service.impl.converter.home.carnetservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.CarnetLoginRequest;
import com.alitalia.aem.ws.carnet.carnetservice.xsd1.LoginCarnetRequest;
import com.alitalia.aem.ws.carnet.carnetservice.xsd1.ObjectFactory;



@Component(immediate=true, metatype=false)
@Service(value=CarnetLoginRequestToLoginCarnetRequest.class)
public class CarnetLoginRequestToLoginCarnetRequest implements Converter<CarnetLoginRequest, LoginCarnetRequest> {

	@Override
	public LoginCarnetRequest convert(CarnetLoginRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		LoginCarnetRequest destination = null;

		if (source != null) {
			
			destination = objectFactory.createLoginCarnetRequest();
			destination.setCarnetCode(objectFactory.createLoginCarnetRequestCarnetCode(source.getCarnetCode()));
			destination.setPassword(objectFactory.createLoginCarnetRequestPassword(source.getPassword()));

		}
		return destination;
	}
}
