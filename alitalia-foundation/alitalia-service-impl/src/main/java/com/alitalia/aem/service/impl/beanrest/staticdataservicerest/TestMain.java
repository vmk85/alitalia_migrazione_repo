package com.alitalia.aem.service.impl.beanrest.staticdataservicerest;

import com.alitalia.aem.common.messages.home.GetDestinationsResponse;
import com.alitalia.aem.common.messages.home.RetrieveAirportDestinationRequest;
import com.alitalia.aem.common.messages.home.RetrieveAvailabilityRequest;
import com.alitalia.aem.common.messages.home.RetrieveAvailabilityResponse;

public class TestMain {

	public static void main(String[] args) {
		
		System.out.println("Inizio metodo main");
		
		System.out.println("Prima chiamata: Airport Destination");
		
		RetrieveAirportDestinationRequest retAirRequest = new RetrieveAirportDestinationRequest();
		retAirRequest.setMarket("it");
		retAirRequest.setLanguage("it");
		retAirRequest.setIata("FCO");
		
		GetDestinationsResponse gDesResponse = new GetDestinationsResponse();
		
		//per test, aggiungere chiamata al client
		
		System.out.println("Response AirportDestination = " + gDesResponse.getDestinations());
		
		System.out.println("Seconda chiamata: Get Availability");
		
		RetrieveAvailabilityRequest retAvRequest = new RetrieveAvailabilityRequest();
		retAvRequest.setMarket("it");
		retAvRequest.setLanguage("it");
		retAvRequest.setOriginIATA("fco");
		retAvRequest.setDestinationIATA("lon");
		
		RetrieveAvailabilityResponse retAvResponse = new RetrieveAvailabilityResponse();
		
		//per test, aggiungere chiamata al client
		
		System.out.println("Response GetAvailability = " + retAvResponse.getAvailability());

		
		System.out.println("Fine metodo main");
	}

}
