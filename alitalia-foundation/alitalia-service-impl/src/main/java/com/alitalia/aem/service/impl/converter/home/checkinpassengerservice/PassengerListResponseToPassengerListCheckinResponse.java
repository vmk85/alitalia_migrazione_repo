package com.alitalia.aem.service.impl.converter.home.checkinpassengerservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.messages.home.PassengerListCheckinResponse;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.PassengerListResponse;
import com.alitalia.aem.ws.checkin.passengerservice.xsd2.ArrayOfPassenger;
import com.alitalia.aem.ws.checkin.passengerservice.xsd2.Passenger;

@Component(immediate=true, metatype=false)
@Service(value=PassengerListResponseToPassengerListCheckinResponse.class)
public class PassengerListResponseToPassengerListCheckinResponse implements
		Converter<PassengerListResponse, PassengerListCheckinResponse> {

	@Reference
	private PassengerToCheckinPassengerData passengerConverter;

	@Override
	public PassengerListCheckinResponse convert(PassengerListResponse source) {
		PassengerListCheckinResponse destination = null;

		if (source != null) {
			destination = new PassengerListCheckinResponse();
			
			destination.setIsListComplete(source.isIsListComplete());
			
			JAXBElement<ArrayOfPassenger> sourcePassengers = source.getPassengers();

			if(sourcePassengers != null &&
					sourcePassengers.getValue() != null &&
					sourcePassengers.getValue().getPassenger() != null &&
					!sourcePassengers.getValue().getPassenger().isEmpty()){
				List<CheckinPassengerData> passengersList = new ArrayList<CheckinPassengerData>();
				for(Passenger sourcePassenger : sourcePassengers.getValue().getPassenger()){
					passengersList.add(passengerConverter.convert(sourcePassenger));
				}
				destination.setPassengers(passengersList);
			}

			JAXBElement<String> sourcePnr = source.getPnr();
			if (sourcePnr != null)
				destination.setPnr(sourcePnr.getValue());
		}

		return destination;
	}

}
