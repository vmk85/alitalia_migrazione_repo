package com.alitalia.aem.service.impl.converter.home.mmbinsuranceservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.enumerations.ContactTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.CreditCardTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbPaymentGroupMaskEnum;
import com.alitalia.aem.common.data.home.mmb.MmbPaymentData;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd2.ContactType;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd2.CreditCardType;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd3.Payment;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd3.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=MmbPaymentDataToPayment.class)
public class MmbPaymentDataToPayment implements Converter<MmbPaymentData, Payment> {

	@Override
	public Payment convert(MmbPaymentData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		Payment destination = null;

		if (source != null) {
			destination = objectFactory.createPayment();

			destination.setX003CAddressX003EKBackingField(source.getAddress());
			destination.setX003CCAPX003EKBackingField(source.getCap());
			destination.setX003CCaptchaX003EKBackingField(source.getCaptcha());
			destination.setX003CCityX003EKBackingField(source.getCity());
			destination.setX003CContactPhoneNumberX003EKBackingField(source.getContactPhoneNumber());
			destination.setX003CContactPrefixX003EKBackingField(source.getContactPrefix());

			ContactTypeEnum sourceContactType = source.getContactType();
			if (sourceContactType != null)
				destination.setX003CContactTypeX003EKBackingField(ContactType.fromValue(sourceContactType.value()));
			else
				destination.setX003CContactTypeX003EKBackingField(ContactType.H);

			destination.setX003CCreditCardNumberX003EKBackingField(source.getCreditCardNumber());

			CreditCardTypeEnum sourceCreditCardType = source.getCreditCardType();
			if (sourceCreditCardType != null)
				destination.setX003CCreditCardTypeX003EKBackingField(CreditCardType.fromValue(sourceCreditCardType.value()));

			destination.setX003CCVCX003EKBackingField(source.getCvc());
			destination.setX003CEmailX003EKBackingField(source.getEmail());
			destination.setX003CExpirationMonthX003EKBackingField(source.getExpirationMonth());
			destination.setX003CExpirationYearX003EKBackingField(source.getExpirationYear());
			destination.setX003CLastNameX003EKBackingField(source.getLastName());
			destination.setX003CNameX003EKBackingField(source.getName());
			destination.setX003CSelectedCountryX003EKBackingField(source.getSelectedCountry());

			MmbPaymentGroupMaskEnum sourceSelectedPaymentGroup = source.getSelectedPaymentGroup();
			if (sourceSelectedPaymentGroup != null)
				destination.getX003CSelectedPaymentGroupX003EKBackingField().add(sourceSelectedPaymentGroup.value());
		}

		return destination;
	}

}
