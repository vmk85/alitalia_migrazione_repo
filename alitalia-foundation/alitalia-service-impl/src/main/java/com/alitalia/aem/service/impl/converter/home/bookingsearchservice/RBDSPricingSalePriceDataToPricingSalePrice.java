package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingSalePriceData;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ResultBookingDetailsSolutionPricingSalePrice;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingSalePriceDataToPricingSalePrice.class)
public class RBDSPricingSalePriceDataToPricingSalePrice implements
		Converter<ResultBookingDetailsSolutionPricingSalePriceData, ResultBookingDetailsSolutionPricingSalePrice> {

	@Override
	public ResultBookingDetailsSolutionPricingSalePrice convert(
			ResultBookingDetailsSolutionPricingSalePriceData source) {
		ResultBookingDetailsSolutionPricingSalePrice destination = null;
		if(source!=null){
			ObjectFactory objf = new ObjectFactory();
			destination = objf.createResultBookingDetailsSolutionPricingSalePrice();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}

}
