package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.home.CreditCardsListResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.GetCreditCardListResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;

@Component(immediate = true, metatype = false)
@Service(value = GetCreditCardListResponseToCreditCardsListResponse.class)
public class GetCreditCardListResponseToCreditCardsListResponse implements Converter<GetCreditCardListResponse, CreditCardsListResponse> {

	@Reference
	private CustomerProfileToMMCustomerProfileData customerProfileConverter;
	
	@Override
	public CreditCardsListResponse convert(GetCreditCardListResponse source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		CreditCardsListResponse destination = new CreditCardsListResponse(); 
		
		JAXBElement<CustomerProfile> jaxbElement = source.getCustomer();
		CustomerProfile customerProfile = jaxbElement.getValue();
		MMCustomerProfileData mmCustomerProfile = customerProfileConverter.convert(customerProfile);
		destination.setCustomerProfile(mmCustomerProfile);
		
		return destination;
	}
}