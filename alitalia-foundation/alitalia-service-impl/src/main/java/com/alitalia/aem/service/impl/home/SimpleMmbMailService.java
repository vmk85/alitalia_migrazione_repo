package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.MmbSendEmailRequest;
import com.alitalia.aem.common.messages.home.MmbSendEmailResponse;
import com.alitalia.aem.service.api.home.MmbMailService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.mmbmailservice.MmbSendEmailRequestToMailMessageRequest;
import com.alitalia.aem.ws.mmb.mailservice.MmbMailServiceClient;
import com.alitalia.aem.ws.mmb.mailservice.xsd1.MailMessageRequest;
import com.alitalia.aem.ws.mmb.mailservice.xsd1.MailMessageResponse;

@Component(immediate=true, metatype=false)
@Service
public class SimpleMmbMailService implements MmbMailService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleMmbMailService.class);

	@Reference
	private MmbMailServiceClient mmbMailServiceClient;

	@Reference
	private MmbSendEmailRequestToMailMessageRequest mmbSendEmailRequestConverter;

	@Override
	public MmbSendEmailResponse sendMail(MmbSendEmailRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method sendMail(). The request is {}", request);
		}
		

		try {
			MmbSendEmailResponse response = new MmbSendEmailResponse();
			MailMessageRequest serviceRequest = mmbSendEmailRequestConverter.convert(request);
			MailMessageResponse serviceResponse = mmbMailServiceClient.sendMail(serviceRequest);
			response.setMailSent(serviceResponse.isIsValid());

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully sendMail(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method sendMail().", e);
			throw new AlitaliaServiceException("Exception executing sendMail: Sid ["+request.getSid()+"]" , e);
		}
	}

}
