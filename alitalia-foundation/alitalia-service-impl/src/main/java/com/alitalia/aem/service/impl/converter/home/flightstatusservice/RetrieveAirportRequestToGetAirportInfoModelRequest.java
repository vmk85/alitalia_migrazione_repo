package com.alitalia.aem.service.impl.converter.home.flightstatusservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AirportModelData;
import com.alitalia.aem.common.messages.home.RetrieveAirportRequest;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetAirportInfoModelRequest;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.AirportModel;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.ObjectFactory;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveAirportRequestToGetAirportInfoModelRequest.class)
public class RetrieveAirportRequestToGetAirportInfoModelRequest implements Converter<RetrieveAirportRequest, GetAirportInfoModelRequest> {

	@Override
	public GetAirportInfoModelRequest convert(RetrieveAirportRequest source) {
		GetAirportInfoModelRequest request = new GetAirportInfoModelRequest();
		
		AirportModelData airportModelData = source.getAirport();
		String azTerminal = airportModelData.getAzTerminal();
		Integer checkTimeDomestic = airportModelData.getCheckInTimeDomestic();
		Integer checkIntercontinental = airportModelData.getCheckInTimeIntercontinental();
		Integer checkInternational = airportModelData.getCheckInTimeInternational();
		Double cityDistance = airportModelData.getCityDistance();
		Integer gmt = airportModelData.getGmt();
		String name = airportModelData.getName();
		
		ObjectFactory objectFactory = new ObjectFactory();
		AirportModel airportModel = new AirportModel();
		airportModel.setAZTerminal(objectFactory.createAirportModelAZTerminal(azTerminal));
		airportModel.setCheckInTimeDomestic(objectFactory.createAirportModelCheckInTimeDomestic(checkTimeDomestic));
		airportModel.setCheckInTimeIntercontinental(objectFactory.createAirportModelCheckInTimeIntercontinental(checkIntercontinental));
		airportModel.setCheckInTimeInternational(objectFactory.createAirportModelCheckInTimeInternational(checkInternational));
		airportModel.setCityDistance(objectFactory.createAirportModelCityDistance(cityDistance));
		airportModel.setGMT(objectFactory.createAirportModelGMT(gmt));
		airportModel.setName(objectFactory.createAirportModelName(name));
		
		request.setAirport(objectFactory.createAirportModel(airportModel));
		
		return request;
	}
}