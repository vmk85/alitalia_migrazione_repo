package com.alitalia.aem.service.impl.converter.home.geospatialservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.IPAddressData;
import com.alitalia.aem.common.messages.home.RetrieveGeoAirportsRequest;
import com.alitalia.aem.ws.geospatial.service.xsd1.GetNearAirportsRequest;
import com.alitalia.aem.ws.geospatial.service.xsd2.IPAddress;
import com.alitalia.aem.ws.geospatial.service.xsd1.ObjectFactory;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveGeoAirportsRequestToGetNearAirportsRequest.class)
public class RetrieveGeoAirportsRequestToGetNearAirportsRequest implements Converter<RetrieveGeoAirportsRequest, GetNearAirportsRequest> {

	@Reference
	private IPAddressDataToIPAddress ipAddressDataConverter;
	
	@Override
	public GetNearAirportsRequest convert(RetrieveGeoAirportsRequest source) {
		
		GetNearAirportsRequest request = new GetNearAirportsRequest();
		
		String cityCode = source.getCityCode();
		String languageCode = source.getLanguageCode();
		String marketCode = source.getMarketCode();
		IPAddressData ipAddressData = source.getIpAddress();
		
		IPAddress ipAddress = ipAddressDataConverter.convert(ipAddressData);
		
		ObjectFactory objectFactory = new ObjectFactory();
		request.setIPAddress(objectFactory.createGetNearAirportsRequestIPAddress(ipAddress));
		request.setCityCode(objectFactory.createGetNearAirportsRequestCityCode(cityCode));
		request.setLanguageCode(objectFactory.createGetNearAirportsRequestLanguageCode(languageCode));
		request.setMarketCode(objectFactory.createGetNearAirportsRequestMarketCode(marketCode));
		
		return request;
	}
}