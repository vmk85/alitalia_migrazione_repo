package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PropertiesData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsData;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ArrayOfDictionaryItem;
import com.alitalia.aem.ws.booking.searchservice.xsd3.DictionaryItem;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=PropertiesDataToArrayOfDictionaryItem.class)
public class PropertiesDataToArrayOfDictionaryItem implements Converter<PropertiesData, ArrayOfDictionaryItem> {

	@Reference
	private ResultBookingDetailsDataToResultBookingDetails resultBookingDetailsConverter;
	
	@Override
	public ArrayOfDictionaryItem convert(PropertiesData source) {
		
		ArrayOfDictionaryItem destination = null;
		
		if (source != null) {
			
			ObjectFactory factory = new ObjectFactory();
			
			destination = factory.createArrayOfDictionaryItem();
	
			for (String key : source.keySet()) {
				DictionaryItem dictionaryItem = factory.createDictionaryItem();
				
				dictionaryItem.setKey(
						factory.createDictionaryItemKey(
								key));
				
				
				if(source.getElement(key) instanceof ResultBookingDetailsData){
					dictionaryItem.setValue(factory.createDictionaryItemValue(resultBookingDetailsConverter.convert((ResultBookingDetailsData)source.getElement(key))));
				}else{
					dictionaryItem.setValue(
							factory.createDictionaryItemValue(
									source.getElement(key)));
				}
				destination.getDictionaryItem().add(dictionaryItem);
			}
			
		}

		return destination;
	}

}
