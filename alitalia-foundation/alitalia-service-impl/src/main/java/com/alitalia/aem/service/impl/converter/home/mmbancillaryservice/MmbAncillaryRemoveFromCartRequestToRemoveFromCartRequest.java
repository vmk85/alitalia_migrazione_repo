package com.alitalia.aem.service.impl.converter.home.mmbancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.MmbAncillaryRemoveFromCartRequest;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.RemoveFromCartRequest;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd3.ArrayOfint;

@Component(immediate=true, metatype=false)
@Service(value=MmbAncillaryRemoveFromCartRequestToRemoveFromCartRequest.class)
public class MmbAncillaryRemoveFromCartRequestToRemoveFromCartRequest implements
		Converter<MmbAncillaryRemoveFromCartRequest, RemoveFromCartRequest> {

	@Reference
	private MmbRequestBaseInfoDataToRequestBaseInfo mmbRequestBaseInfoDataConverter;

	@Override
	public RemoveFromCartRequest convert(MmbAncillaryRemoveFromCartRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		com.alitalia.aem.ws.mmb.ancillaryservice.xsd3.ObjectFactory objectFactory3 =
				new com.alitalia.aem.ws.mmb.ancillaryservice.xsd3.ObjectFactory();
		RemoveFromCartRequest destination = null;

		if (source != null) {
			destination = objectFactory.createRemoveFromCartRequest();

			destination.setCartId(source.getCartId());
			destination.setClient(source.getClient());
			destination.setMachineName(objectFactory.createAddToCartRequestMachineName(source.getMachineName()));
			destination.setSessionId(objectFactory.createAddToCartRequestSessionId(source.getSessionId()));
			destination.setX003CPNRX003EKBackingField(null);

			destination.setX003CInfoRequestX003EKBackingField(mmbRequestBaseInfoDataConverter.convert(source.getBaseInfo()));

			ArrayOfint ancillaryIds = objectFactory3.createArrayOfint();
			for(Integer sourceAncillaryId : source.getItemIdsToRemove())
				ancillaryIds.getInt().add(sourceAncillaryId);
			destination.setAncillaryIDs(objectFactory.createRemoveFromCartRequestAncillaryIDs(ancillaryIds));
		}

		return destination;
	}

}
