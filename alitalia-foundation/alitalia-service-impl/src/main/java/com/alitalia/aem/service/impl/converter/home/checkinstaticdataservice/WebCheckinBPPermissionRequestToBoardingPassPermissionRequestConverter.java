package com.alitalia.aem.service.impl.converter.home.checkinstaticdataservice;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.WebCheckinBPPermissionRequest;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.BoardingPassPermissionRequest;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.Resource;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd3.ArrayOfstring;



@Component(immediate=true, metatype=false)
@Service(value=WebCheckinBPPermissionRequestToBoardingPassPermissionRequestConverter.class)
public class WebCheckinBPPermissionRequestToBoardingPassPermissionRequestConverter implements Converter<WebCheckinBPPermissionRequest, BoardingPassPermissionRequest> {

	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;
	
	@Override
	public BoardingPassPermissionRequest convert(WebCheckinBPPermissionRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();		
		BoardingPassPermissionRequest destination = objectFactory.createBoardingPassPermissionRequest();
		
		destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
		destination.setX003CResourceX003EKBackingField(Resource.WEB);
		
		List<String> aptCodes = source.getAptCodes();
		
		com.alitalia.aem.ws.checkin.staticdataservice.xsd3.ObjectFactory objectFactory3 = new com.alitalia.aem.ws.checkin.staticdataservice.xsd3.ObjectFactory();
		ArrayOfstring airportCodes = objectFactory3.createArrayOfstring();
		if (aptCodes!=null && aptCodes.size()>0) {
			for (String s : aptCodes)
				airportCodes.getString().add(s);
		}
		destination.setAirportCodes(objectFactory.createBoardingPassPermissionRequestAirportCodes(airportCodes));

		return destination;
	}
}