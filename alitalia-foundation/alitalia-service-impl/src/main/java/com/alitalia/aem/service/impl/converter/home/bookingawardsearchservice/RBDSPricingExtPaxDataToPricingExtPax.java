package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingExtPaxData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetailsSolutionPricingExtPax;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingExtPaxDataToPricingExtPax.class)
public class RBDSPricingExtPaxDataToPricingExtPax implements Converter<ResultBookingDetailsSolutionPricingExtPaxData, ResultBookingDetailsSolutionPricingExtPax> {

	@Override
	public ResultBookingDetailsSolutionPricingExtPax convert(
			ResultBookingDetailsSolutionPricingExtPaxData source) {
		ResultBookingDetailsSolutionPricingExtPax destination = null;
		if(source!=null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionPricingExtPax();
			destination.setAdultsField(source.getAdultsField());
			destination.setAdultsFieldSpecified(source.isAdultsFieldSpecified());
			destination.setChildrenField(source.getChildrenField());
			destination.setChildrenFieldSpecified(source.isChildrenFieldSpecified());
			destination.setInfantsInLapField(source.getInfantsInLapField());
			destination.setInfantsInLapFieldSpecified(source.isInfantsInLapFieldSpecified());
			destination.setYouthField(source.getYouthField());
			destination.setYouthFieldSpecified(source.isYouthFieldSpecified());
		}
		return destination;
	}

}
