package com.alitalia.aem.service.impl.converter.home.newsservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveNewsCategoriesResponse;
import com.alitalia.aem.ws.news.newsservice.xsd2.GetCategoriesResponse;
import com.alitalia.aem.ws.news.newsservice.xsd3.ArrayOfstring;

@Component(immediate = true, metatype = false)
@Service(value = GetCategoriesResponseToRetrieveNewsCategoriesResponse.class)
public class GetCategoriesResponseToRetrieveNewsCategoriesResponse implements Converter<GetCategoriesResponse, RetrieveNewsCategoriesResponse> {

	@Override
	public RetrieveNewsCategoriesResponse convert(GetCategoriesResponse source) {
		
		RetrieveNewsCategoriesResponse response = new RetrieveNewsCategoriesResponse();
		
		JAXBElement<ArrayOfstring> jaxbCategories = source.getCategories();
		ArrayOfstring arrayOfString = jaxbCategories.getValue();
		
		response.setCategories(arrayOfString.getString());
		response.setCount(source.getCount());
		
		return response;
	}
}