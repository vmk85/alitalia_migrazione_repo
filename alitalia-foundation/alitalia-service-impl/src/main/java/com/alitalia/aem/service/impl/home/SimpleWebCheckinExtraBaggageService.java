package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.ExtraBaggageOrderCheckinRequest;
import com.alitalia.aem.common.messages.home.ExtraBaggageOrderCheckinResponse;
import com.alitalia.aem.common.messages.home.ExtraBaggagePolicyCheckinRequest;
import com.alitalia.aem.common.messages.home.ExtraBaggagePolicyCheckinResponse;
import com.alitalia.aem.service.api.home.WebCheckinExtraBaggageService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.checkinextrabaggageservice.ExtraBaggageOrderCheckinRequestToExtraBaggageOrderRequest;
import com.alitalia.aem.service.impl.converter.home.checkinextrabaggageservice.ExtraBaggageOrderResponseToExtraBaggageOrderCheckinResponse;
import com.alitalia.aem.service.impl.converter.home.checkinextrabaggageservice.ExtraBaggagePolicyCheckinRequestToExtraBaggagePolicyRequest;
import com.alitalia.aem.service.impl.converter.home.checkinextrabaggageservice.ExtraBaggagePolicyResponseToExtraBaggagePolicyCheckinResponse;
import com.alitalia.aem.ws.checkin.extrabaggageservice.CheckinExtraBaggageServiceClient;
import com.alitalia.aem.ws.checkin.extrabaggageservice.xsd1.ExtraBaggageOrderRequest;
import com.alitalia.aem.ws.checkin.extrabaggageservice.xsd1.ExtraBaggageOrderResponse;
import com.alitalia.aem.ws.checkin.extrabaggageservice.xsd1.ExtraBaggagePolicyRequest;
import com.alitalia.aem.ws.checkin.extrabaggageservice.xsd1.ExtraBaggagePolicyResponse;

@Component(immediate=true, metatype=false)
@Service
public class SimpleWebCheckinExtraBaggageService implements WebCheckinExtraBaggageService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleWebCheckinExtraBaggageService.class);

	@Reference
	private CheckinExtraBaggageServiceClient checkinExtraBaggageServiceClient;

	@Reference
	private ExtraBaggageOrderCheckinRequestToExtraBaggageOrderRequest extraBaggageOrderCheckinRequestConverter;
	
	@Reference
	private ExtraBaggageOrderResponseToExtraBaggageOrderCheckinResponse extraBaggageOrderResponseConverter;
	
	@Reference
	private ExtraBaggagePolicyCheckinRequestToExtraBaggagePolicyRequest extraBaggagePolicyCheckinRequestConverter;
	
	@Reference
	private ExtraBaggagePolicyResponseToExtraBaggagePolicyCheckinResponse extraBaggagePolicyResponseConverter;

	
	@Override
	public ExtraBaggageOrderCheckinResponse getExtraBaggageOrder(ExtraBaggageOrderCheckinRequest request) {
		
			logger.debug("Executing method getExtraBaggageOrder(). The request is {}", request);	

		try {
			ExtraBaggageOrderCheckinResponse response = new ExtraBaggageOrderCheckinResponse();
			ExtraBaggageOrderRequest serviceRequest = extraBaggageOrderCheckinRequestConverter.convert(request);
			ExtraBaggageOrderResponse serviceResponse = checkinExtraBaggageServiceClient.getExtraBaggageOrder(serviceRequest);
			response = extraBaggageOrderResponseConverter.convert(serviceResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			logger.debug("Executed method successfully getExtraBaggageOrder(). The response is {}", response);
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getExtraBaggageOrder().", e);
			throw new AlitaliaServiceException("Exception executing getExtraBaggageOrder: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public ExtraBaggagePolicyCheckinResponse getBaggagePolicy(ExtraBaggagePolicyCheckinRequest request) {
		
			logger.debug("Executing method getBaggagePolicy(). The request is {}", request);	

		try {
			ExtraBaggagePolicyCheckinResponse response = new ExtraBaggagePolicyCheckinResponse();
			ExtraBaggagePolicyRequest serviceRequest = extraBaggagePolicyCheckinRequestConverter.convert(request);
			ExtraBaggagePolicyResponse serviceResponse = checkinExtraBaggageServiceClient.getBaggagePolicy(serviceRequest);
			response = extraBaggagePolicyResponseConverter.convert(serviceResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			logger.debug("Executed method successfully getBaggagePolicy(). The response is {}", response);
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getBaggagePolicy().", e);
			throw new AlitaliaServiceException("Exception executing getBaggagePolicy: Sid ["+request.getSid()+"]" , e);
		}
	}
	
}
