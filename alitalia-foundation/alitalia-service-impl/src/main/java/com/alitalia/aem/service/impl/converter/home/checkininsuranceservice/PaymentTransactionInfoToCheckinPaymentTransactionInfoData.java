package com.alitalia.aem.service.impl.converter.home.checkininsuranceservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinPaymentTransactionInfoData;
import com.alitalia.aem.common.data.home.enumerations.CheckinPaymentTransactionInfoResponseTypeEnum;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd2.PaymentTransactionInfo;



@Component(immediate=true, metatype=false)
@Service(value=PaymentTransactionInfoToCheckinPaymentTransactionInfoData.class)
public class PaymentTransactionInfoToCheckinPaymentTransactionInfoData implements Converter<PaymentTransactionInfo, CheckinPaymentTransactionInfoData> {
	

	@Override
	public CheckinPaymentTransactionInfoData convert(PaymentTransactionInfo source) {
		CheckinPaymentTransactionInfoData destination = null;

		if (source != null) {
			destination = new CheckinPaymentTransactionInfoData();
			
			destination.setId(source.getX003CIdX003EKBackingField());
			destination.setAcceptHeader(source.getX003CAcceptHeaderX003EKBackingField());
			destination.setAuthorizationID(source.getX003CAuthorizationIDX003EKBackingField());
			destination.setErrorCode(source.getX003CErrorCodeX003EKBackingField());
			destination.setErrorDescription(source.getX003CErrorDescriptionX003EKBackingField());
			destination.setMarketCode(source.getX003CMarketCodeX003EKBackingField());
			destination.setOpaqueParameters(source.getX003COpaqueParametersX003EKBackingField());
			destination.setOrderCode(source.getX003COrderCodeX003EKBackingField());
			destination.setOrderDescription(source.getX003COrderDescriptionX003EKBackingField());
			destination.setRequestData(source.getX003CRequestDataX003EKBackingField());
			destination.setSessionId(source.getX003CSessionIdX003EKBackingField());
			destination.setSiteCode(source.getX003CSiteCodeX003EKBackingField());
			destination.setThreeDSecureIssueURL(source.getX003CThreeDSecureIssueURLX003EKBackingField());
			if(source.getX003CTypeX003EKBackingField() != null){
				destination.setType(CheckinPaymentTransactionInfoResponseTypeEnum.fromValue(source.getX003CTypeX003EKBackingField().value()));
			}
			destination.setUserAgentHeader(source.getX003CUserAgentHeaderX003EKBackingField());
			destination.setUserHostAddress(source.getX003CUserHostAddressX003EKBackingField());
			destination.setVbvPaRequest(source.getX003CVbVPaRequestX003EKBackingField());
			destination.setVbvPaResponse(source.getX003CVbVPaResponseX003EKBackingField());
			
		}
		return destination;
	}

}
