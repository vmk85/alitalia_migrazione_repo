package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenalty;


@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyToRBDSPBISPFRSPenaltiesCancellationAndRefundPenaltyData.class)
public class RBDSPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyToRBDSPBISPFRSPenaltiesCancellationAndRefundPenaltyData
		implements Converter<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenalty, ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData> {

	@Override
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData convert(
			ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenalty source) {
		ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData destination = null;
		
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData();
			destination.setApplicationField(source.getApplicationField());
			destination.setMaxPriceField(source.getMaxPriceField());
			destination.setMinPriceField(source.getMinPriceField());
		}
		
		return destination;
		
	}
	
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData convert(
			com.alitalia.aem.ws.bookingaward.searchservice.xsd13.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenalty source) {
		ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData destination = null;
		
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesCancellationAndRefundPenaltyData();
			destination.setApplicationField(source.getApplicationField());
			destination.setMaxPriceField(source.getMaxPriceField());
			destination.setMinPriceField(source.getMinPriceField());
		}
		
		return destination;
		
	}

}
