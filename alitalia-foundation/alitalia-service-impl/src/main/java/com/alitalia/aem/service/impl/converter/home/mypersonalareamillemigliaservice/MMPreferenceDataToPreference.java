package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMPreferenceData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Preference;

@Component(immediate = true, metatype = false)
@Service(value = MMPreferenceDataToPreference.class)
public class MMPreferenceDataToPreference implements Converter<MMPreferenceData, Preference> {

	@Override
	public Preference convert(MMPreferenceData source) {
		Preference destination = null;

		if (source != null) {
			com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ObjectFactory objectFactory2 = new com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ObjectFactory();

			destination = objectFactory2.createPreference();
			destination.setAdditionalText(objectFactory2.createPreferenceAdditionalText(source.getAdditionalText()));
			destination.setDescription(objectFactory2.createPreferenceDescription(source.getDescription()));
			destination.setIDCategory(objectFactory2.createPreferenceIDCategory(source.getIdCategory()));
			destination.setSequenceNumber(objectFactory2.createPreferenceSequenceNumber(source.getSequenceNumber()));
			destination.setShortNameCategory(objectFactory2.createPreferenceShortNameCategory(source
					.getShortNameCategory()));
			destination.setShortNamePreference(objectFactory2.createPreferenceShortNamePreference(source
					.getShortNamePreference()));

			if (source.getStartDate() != null)
				destination.setStartDate(XsdConvertUtils.toXMLGregorianCalendar(source.getStartDate()));
			if (source.getEndDate() != null)
				destination.setEndDate(XsdConvertUtils.toXMLGregorianCalendar(source.getEndDate()));
		}

		return destination;
	}

}
