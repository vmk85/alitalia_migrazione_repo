package com.alitalia.aem.service.impl.converter.home.bookingwidget;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PassengerApisInfoData;
import com.alitalia.aem.common.data.home.PassengerApisSecureFlightInfoData;
import com.alitalia.aem.common.data.home.PassengerBaseInfoData;
import com.alitalia.aem.common.data.home.PassengerSecureFlightInfoData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.ApisInfo;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.ApisSecureFlightInfo;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.InfoBase;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.ObjectFactory;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.SecureFlightInfo;
import com.alitalia.aem.ws.booking.widgetservice.xsd2.GenderType;

@Component(immediate = true, metatype = false)
@Service(value = PassengerInfoBaseDataToInfoBase.class)
public class PassengerInfoBaseDataToInfoBase implements Converter<PassengerBaseInfoData, InfoBase> {

	@Override
	public InfoBase convert(PassengerBaseInfoData source) {
		InfoBase destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();

			if (source instanceof PassengerApisSecureFlightInfoData) {
				
				destination = objectFactory.createApisSecureFlightInfo();
				
				((ApisSecureFlightInfo) destination).setPassportNumber(
						objectFactory.createApisSecureFlightInfoPassportNumber(
								((PassengerApisSecureFlightInfoData) source).getPassportNumber()));
				
				((ApisSecureFlightInfo) destination).setNationality(
						objectFactory.createApisSecureFlightInfoNationality(
								((PassengerApisSecureFlightInfoData) source).getNationality()));
				
				((ApisSecureFlightInfo) destination).setSecondName(
						objectFactory.createApisSecureFlightInfoSecondName(
								((PassengerApisSecureFlightInfoData) source).getSecondName()));
				
			} else if (source instanceof PassengerApisInfoData) {
				
				destination = objectFactory.createApisInfo();
				
				((ApisInfo) destination).setPassportNumber(
						objectFactory.createApisInfoPassportNumber(
								((PassengerApisInfoData) source).getPassportNumber()));
				
				((ApisInfo) destination).setNationality(
						objectFactory.createApisInfoNationality(
								((PassengerApisInfoData) source).getNationality()));
			
				
			} else if (source instanceof PassengerSecureFlightInfoData) {
				
				destination = objectFactory.createSecureFlightInfo();
				
				((SecureFlightInfo) destination).setSecondName(
						objectFactory.createSecureFlightInfoSecondName(
								((PassengerSecureFlightInfoData) source).getSecondName()));
				
			} else {
				
				destination = objectFactory.createInfoBase();
				
			}

			destination.setBirthDate(XsdConvertUtils.toXMLGregorianCalendar(source.getBirthDate()));

			if (source.getGender() != null) {
				destination.setGender(GenderType.fromValue(source.getGender().value()));
			}

		}

		return destination;
	}

}
