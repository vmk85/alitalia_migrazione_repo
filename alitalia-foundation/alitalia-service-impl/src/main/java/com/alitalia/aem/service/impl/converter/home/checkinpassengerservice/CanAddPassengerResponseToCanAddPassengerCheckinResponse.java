package com.alitalia.aem.service.impl.converter.home.checkinpassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.enumerations.CanAddPassengerReasonEnum;
import com.alitalia.aem.common.messages.home.CanAddPassengerCheckinResponse;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.CanAddPassengerReason;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.CanAddPassengerResponse;

@Component(immediate=true, metatype=false)
@Service(value=CanAddPassengerResponseToCanAddPassengerCheckinResponse.class)
public class CanAddPassengerResponseToCanAddPassengerCheckinResponse implements
		Converter<CanAddPassengerResponse, CanAddPassengerCheckinResponse> {

	@Override
	public CanAddPassengerCheckinResponse convert(CanAddPassengerResponse source) {
		CanAddPassengerCheckinResponse destination = null;

		if (source != null) {
			destination = new CanAddPassengerCheckinResponse();

			CanAddPassengerReason sourceCanAddPassengerReason = source.getCanAddPassenger();
			if(sourceCanAddPassengerReason != null)
				destination.setCanAddPassenger(CanAddPassengerReasonEnum.fromValue(sourceCanAddPassengerReason.value()));

		}

		return destination;
	}

}
