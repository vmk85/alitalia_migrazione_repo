package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.BoardingPassCheckinRequest;
import com.alitalia.aem.common.messages.home.BoardingPassCheckinResponse;
import com.alitalia.aem.common.messages.home.SendBoardingPassCheckinRequest;
import com.alitalia.aem.common.messages.home.SendBoardingPassCheckinResponse;
import com.alitalia.aem.common.messages.home.SendReminderCheckinRequest;
import com.alitalia.aem.common.messages.home.SendReminderCheckinResponse;
import com.alitalia.aem.service.api.home.WebCheckinBoardingPassService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.checkinboardingpassservice.BoardingPassCheckinRequestToBoardingPassRequest;
import com.alitalia.aem.service.impl.converter.home.checkinboardingpassservice.BoardingPassResponseToBoardingPassCheckinResponse;
import com.alitalia.aem.service.impl.converter.home.checkinboardingpassservice.SendBoardingPassCheckinRequestToSendBoardingPassRequest;
import com.alitalia.aem.service.impl.converter.home.checkinboardingpassservice.SendBoardingPassResponseToSendBoardingPassCheckinResponse;
import com.alitalia.aem.service.impl.converter.home.checkinboardingpassservice.SendReminderCheckinRequestToSendReminderRequest;
import com.alitalia.aem.service.impl.converter.home.checkinboardingpassservice.SendReminderResponseToSendReminderCheckinResponse;
import com.alitalia.aem.ws.checkin.boardingpassservice.CheckinBoardingPassServiceClient;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd1.BoardingPassRequest;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd1.BoardingPassResponse;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd1.SendBoardingPassRequest;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd1.SendReminderRequest;

@Component(immediate=true, metatype=false)
@Service
public class SimpleWebCheckinBoardingPassService implements WebCheckinBoardingPassService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleWebCheckinBoardingPassService.class);

	@Reference
	private CheckinBoardingPassServiceClient checkinBoardingPassServiceClient;

	@Reference
	private BoardingPassCheckinRequestToBoardingPassRequest boardingPassCheckinRequestConverter;

	@Reference
	private BoardingPassResponseToBoardingPassCheckinResponse boardingPassResponseConverter;

	@Reference
	private SendBoardingPassCheckinRequestToSendBoardingPassRequest sendBoardingPassCheckinRequestConverter;

	@Reference
	private SendBoardingPassResponseToSendBoardingPassCheckinResponse sendBoardingPassResponseConverter;

	@Reference
	private SendReminderCheckinRequestToSendReminderRequest sendReminderCheckinRequestConverter;

	@Reference
	private SendReminderResponseToSendReminderCheckinResponse sendReminderResponseConverter;
	
	@Override
	public BoardingPassCheckinResponse getBoardingPass(BoardingPassCheckinRequest request) {

		logger.debug("Executing method getBoardingPass(). The request is {}", request);	

		try {
			BoardingPassCheckinResponse response = new BoardingPassCheckinResponse();
			BoardingPassRequest serviceRequest = boardingPassCheckinRequestConverter.convert(request);
			BoardingPassResponse serviceResponse = checkinBoardingPassServiceClient.getBoardingPass(serviceRequest);
			response = boardingPassResponseConverter.convert(serviceResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			logger.debug("Executed method successfully getBoardingPass(). The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getBoardingPass().", e);
			throw new AlitaliaServiceException("Exception executing getBoardingPass: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public SendBoardingPassCheckinResponse sendBoardingPass(SendBoardingPassCheckinRequest request) {

		logger.debug("Executing method sendBoardingPass(). The request is {}", request);	

		try {
			SendBoardingPassCheckinResponse response = new SendBoardingPassCheckinResponse();
			SendBoardingPassRequest serviceRequest = sendBoardingPassCheckinRequestConverter.convert(request);
			Boolean serviceResponse = checkinBoardingPassServiceClient.sendBoardingPass(serviceRequest);
			response = sendBoardingPassResponseConverter.convert(serviceResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			logger.debug("Executed method successfully sendBoardingPass(). The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method sendBoardingPass().", e);
			throw new AlitaliaServiceException("Exception executing sendBoardingPass: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public SendReminderCheckinResponse sendReminder(SendReminderCheckinRequest request) {

		logger.debug("Executing method sendReminder(). The request is {}", request);	

		try {
			SendReminderCheckinResponse response = new SendReminderCheckinResponse();
			SendReminderRequest serviceRequest = sendReminderCheckinRequestConverter.convert(request);
			Boolean serviceResponse = checkinBoardingPassServiceClient.sendReminder(serviceRequest);
			response = sendReminderResponseConverter.convert(serviceResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			logger.debug("Executed method successfully sendReminder(). The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method sendReminder().", e);
			throw new AlitaliaServiceException("Exception executing sendReminder: Sid ["+request.getSid()+"]" , e);
		}
	}

}
