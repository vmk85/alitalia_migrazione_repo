package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.BaggageAllowanceData;
import com.alitalia.aem.ws.booking.searchservice.xsd3.BaggageAllowance;

@Component(immediate=true, metatype=false)
@Service(value=BaggageAllowanceToBaggageAllowanceData.class)
public class BaggageAllowanceToBaggageAllowanceData implements Converter<BaggageAllowance, BaggageAllowanceData> {

	@Override
	public BaggageAllowanceData convert(BaggageAllowance source) {
		BaggageAllowanceData destination = new BaggageAllowanceData();
		
		if (source != null){

			destination.setCode(source.getCode().getValue());
			destination.setCount(source.getCount());

		}
		
		return destination;
	}

}
