package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentLegData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentLegScheduledServicesData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ArrayOfresultTicketingDetailSolutionSliceSegmentLegScheduledServices;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionSliceSegmentLeg;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentLegDataToRTSSliceSegmentLeg.class)
public class RTDSSliceSegmentLegDataToRTSSliceSegmentLeg implements
		Converter<ResultTicketingDetailSolutionSliceSegmentLegData, 
					ResultTicketingDetailSolutionSliceSegmentLeg> {

	@Reference
	private RTDSSliceSegmentLegAircraftDataToRTDSSliceSegmentLegAircraft rtdsAircraftFieldConverter;

	@Reference
	private RTDSSliceSegmentLegConnectionDataToRTDSliceSegmentLegConnection rtdsConnectionFieldConverter;

	@Reference
	private RTDSSliceSegmentLegDistanceDataToRTDSliceSegmentLegDistance rtdsDistanceFieldConverter;

	@Reference
	private RTDSSliceSegmentLegExtDataToRTDSSliceSegmentLegExt rtdsExtFieldConverter;

	@Reference
	private RTDSSliceSegmentLegOperationalDisclosureDataToRTDSSliceSegmentLegOperationalDisclosure rtdsOperationalDisclosureFieldConverter;

	@Reference
	private RTDSSliceSegmentLegOperationalFlightDataToRTDSSliceSegmentLegOperationalFlight rtdsOperationalFlightFieldConverter;

	@Reference
	private RTDSSliceSegmentLegScheduledServicesDataToRTDSSliceSegmentLegScheduledServices rtdsScheduledServicesFieldConverter;

	@Override
	public ResultTicketingDetailSolutionSliceSegmentLeg convert(
			ResultTicketingDetailSolutionSliceSegmentLegData source) {
		ResultTicketingDetailSolutionSliceSegmentLeg destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionSliceSegmentLeg();

			destination.setAircraftField(rtdsAircraftFieldConverter.convert(source.getAircraftField()));

			destination.setArrivalField(source.getArrivalField());

			destination.setConnectionField(rtdsConnectionFieldConverter.convert(source.getConnectionField()));

			destination.setDepartureField(source.getDepartureField());
			destination.setDestinationField(source.getDestinationField());
			destination.setDestinationTerminalField(source.getDestinationTerminalField());

			destination.setDistanceField(rtdsDistanceFieldConverter.convert(source.getDistanceField()));

			destination.setDurationField(source.getDurationField());
			destination.setDurationFieldSpecified(source.isDurationFieldSpecified());

			destination.setExtField(rtdsExtFieldConverter.convert(source.getExtField()));

			destination.setOperationalDisclosureField(
					rtdsOperationalDisclosureFieldConverter.convert(source.getOperationalDisclosureField()));

			destination.setOperationalFlightField(
					rtdsOperationalFlightFieldConverter.convert(source.getOperationalFlightField()));

			destination.setOriginField(source.getOriginField());
			destination.setOriginTerminalField(source.getOriginTerminalField());

			ArrayOfresultTicketingDetailSolutionSliceSegmentLegScheduledServices scheduledServicesField = null;
			if (source.getScheduledServicesField() != null &&
					!source.getScheduledServicesField().isEmpty()) {
				scheduledServicesField = objectFactory.createArrayOfresultTicketingDetailSolutionSliceSegmentLegScheduledServices();
				for(ResultTicketingDetailSolutionSliceSegmentLegScheduledServicesData sourceScheduledServicesFieldElem : 
						source.getScheduledServicesField())
					scheduledServicesField.getResultTicketingDetailSolutionSliceSegmentLegScheduledServices().add(
							rtdsScheduledServicesFieldConverter.convert(sourceScheduledServicesFieldElem));
			}
			destination.setScheduledServicesField(scheduledServicesField);
		}

		return destination;
	}

}
