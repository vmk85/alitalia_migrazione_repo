package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.data.home.AirportDetailsData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.Airport;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.AreaValue;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ObjectFactory;

@Component(immediate = true, metatype = false)
@Service(value = AirportDataToAirport.class)
public class AirportDataToAirport implements Converter<AirportData, Airport> {

	@Reference
	private AirportDetailsDataToAirportDetails airportDetailsDataConverter;

	@Override
	public Airport convert(AirportData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		Airport airport = objectFactory.createAirport();

		airport.setArea(AreaValue.fromValue(source.getArea().value()));
		airport.setAZDeserves(source.getAzDeserves());
		airport.setCity(objectFactory.createAirportCity(source.getCity()));
		airport.setCityCode(objectFactory.createAirportCityCode(source.getCityCode()));
		airport.setCityUrl(objectFactory.createAirportCityUrl(source.getCityUrl()));
		airport.setCode(objectFactory.createAirportCode(source.getCode()));
		airport.setCountry(objectFactory.createAirportCountry(source.getCountry()));
		airport.setCountryCode(objectFactory.createAirportCountryCode(source.getCountryCode()));
		airport.setDescription(objectFactory.createAirportDescription(source.getDescription()));
		airport.setEligibility(source.getEligibility());
		airport.setEtktAvlblFrom(objectFactory.createAirportEtktAvlblFrom(XsdConvertUtils.toXMLGregorianCalendar(source.getEtktAvlblFrom())));
		airport.setHiddenInFirstDeparture(source.getHiddenInFirstDeparture());
		airport.setIdMsg(source.getIdMsg());
		airport.setName(objectFactory.createAirportName(source.getName()));
		airport.setState(objectFactory.createAirportState(source.getState()));
		airport.setStateCode(objectFactory.createAirportStateCode(source.getStateCode()));
		airport.setUnavlblFrom(objectFactory.createAirportUnavlblFrom(XsdConvertUtils.toXMLGregorianCalendar(source.getUnavlblFrom())));
		airport.setUnavlblUntil(objectFactory.createAirportUnavlblUntil(XsdConvertUtils.toXMLGregorianCalendar(source.getUnavlblUntil())));

		AirportDetailsData sourceDetails = source.getDetails();
		if (sourceDetails != null)
			airport.setDetails(objectFactory.createAirportDetails(airportDetailsDataConverter.convert(sourceDetails)));
		else
			airport.setDetails(objectFactory.createAirportDetails(null));
		
		return airport;
	}
}
