package com.alitalia.aem.service.impl.converter.home.businessloginservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.AgencySubscriptionRequest;
import com.alitalia.aem.ws.salesagentsubstrade.xsd.SalesAgentSubscriptionInput;
import com.alitalia.aem.ws.salesagentsubstrade.xsd.ObjectFactory;

@Component
@Service(value=AgencySubscriptionRequestToSalesAgentSubscriptionInput.class)
public class AgencySubscriptionRequestToSalesAgentSubscriptionInput implements Converter<AgencySubscriptionRequest, SalesAgentSubscriptionInput> {

	@Override
	public SalesAgentSubscriptionInput convert(AgencySubscriptionRequest source) {
		
		ObjectFactory factory = new ObjectFactory();
		SalesAgentSubscriptionInput destination = factory.createSalesAgentSubscriptionInput();

		destination.setADDRESS(source.getIndirizzo());
		destination.setCITY(source.getCitta());
		destination.setCODICEFISCALE(source.getCodiceFiscale());
		destination.setCOMPANYNAME(source.getNomeCompagnia());
		destination.setEMAILOPERATORE(source.getEmailOperatore());
		destination.setEMAILTITOLARE(source.getEmailTitolare());
		destination.setFAXNUMBER(source.getNumeroFax());
		destination.setPIVA(source.getPartitaIva());
		destination.setPOSTALCODE(source.getCap());
		destination.setPROVINCIA(source.getProvincia());
		destination.setSTATE(source.getStato());
		destination.setTELEPHONENUMBER(source.getNumeroTelefono());

		return destination;
	}

}
