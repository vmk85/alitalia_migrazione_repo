//2
package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetailsSolutionPricingFare;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingFareToPricingFareData.class)
public class RBDSPricingFareToPricingFareData implements Converter<ResultBookingDetailsSolutionPricingFare, ResultBookingDetailsSolutionPricingFareData> {

	@Override
	public ResultBookingDetailsSolutionPricingFareData convert(
			ResultBookingDetailsSolutionPricingFare source) {
		ResultBookingDetailsSolutionPricingFareData destination = null;
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingFareData();
			destination.setCarrierField(source.getCarrierField());
			destination.setDestinationCityField(source.getDestinationCityField());
			destination.setExtendedFareCodeField(source.getExtendedFareCodeField());
			destination.setOriginCityField(source.getOriginCityField());
			
			List<String> ptcField = new ArrayList<String>();
			if(source.getPtcField()!=null)
			for(String s : source.getPtcField().getString())
				ptcField.add(s);
			destination.setPtcField(ptcField);
			
			destination.setTagField(source.getTagField());
			destination.setTicketDesignatorField(source.getTicketDesignatorField());		}
		return destination;
	}
	
	public ResultBookingDetailsSolutionPricingFareData convert(
			com.alitalia.aem.ws.bookingaward.searchservice.xsd13.ResultBookingDetailsSolutionPricingFare source) {
		ResultBookingDetailsSolutionPricingFareData destination = null;
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingFareData();
			destination.setCarrierField(source.getCarrierField());
			destination.setDestinationCityField(source.getDestinationCityField());
			destination.setExtendedFareCodeField(source.getExtendedFareCodeField());
			destination.setOriginCityField(source.getOriginCityField());
			
			List<String> ptcField = new ArrayList<String>();
			if(source.getPtcField()!=null)
			for(String s : source.getPtcField().getString())
				ptcField.add(s);
			destination.setPtcField(ptcField);
			
			destination.setTagField(source.getTagField());
			destination.setTicketDesignatorField(source.getTicketDesignatorField());		}
		return destination;
	}

}
