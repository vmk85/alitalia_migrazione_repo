package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.GenericServiceResponse;
import com.alitalia.aem.common.messages.home.PaymentCheckinRequest;
import com.alitalia.aem.common.messages.home.PaymentCheckinResponse;
import com.alitalia.aem.common.messages.home.SendReceiptCheckinRequest;
import com.alitalia.aem.service.api.home.WebCheckinPaymentService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.checkinpaymentservice.PaymentCheckinRequestToPaymentRequest;
import com.alitalia.aem.service.impl.converter.home.checkinpaymentservice.PaymentResponseToPaymentCheckinResponse;
import com.alitalia.aem.service.impl.converter.home.checkinpaymentservice.SendReceiptCheckinRequestToSendReceiptRequest;
import com.alitalia.aem.service.impl.converter.home.checkinpaymentservice.SendReceiptResponseToGenericServiceResponse;
import com.alitalia.aem.ws.checkin.paymentservice.CheckinPaymentServiceClient;
import com.alitalia.aem.ws.checkin.paymentservice.xsd1.PaymentRequest;
import com.alitalia.aem.ws.checkin.paymentservice.xsd1.PaymentResponse;
import com.alitalia.aem.ws.checkin.paymentservice.xsd1.SendReceiptRequest;

@Component(immediate=true, metatype=false)
@Service
public class SimpleWebCheckinPaymentService implements WebCheckinPaymentService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleWebCheckinPaymentService.class);

	@Reference
	private CheckinPaymentServiceClient checkinPaymentServiceClient;

	@Reference
	private PaymentCheckinRequestToPaymentRequest paymentCheckinRequestConverter;

	@Reference
	private PaymentResponseToPaymentCheckinResponse paymentResponseConverter;
	
	@Reference
	private SendReceiptCheckinRequestToSendReceiptRequest sendReceiptCheckinRequestConverter;
	
	@Reference
	private SendReceiptResponseToGenericServiceResponse sendReceiptResponseConverter;
	
	
	@Override
	public PaymentCheckinResponse executePayment(PaymentCheckinRequest request) {

		logger.debug("Executing method executePayment(). The request is {}", request);	

		try {
			PaymentCheckinResponse response = new PaymentCheckinResponse();
			PaymentRequest serviceRequest = paymentCheckinRequestConverter.convert(request);
			PaymentResponse serviceResponse = checkinPaymentServiceClient.executePayment(serviceRequest);
			response = paymentResponseConverter.convert(serviceResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			logger.debug("Executed method successfully executePayment(). The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method executePayment().", e);
			throw new AlitaliaServiceException("Exception executing executePayment: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public GenericServiceResponse sendEmailReceipt(SendReceiptCheckinRequest request) {

		logger.debug("Executing method sendEmailReceipt(). The request is {}", request);	

		try {
			GenericServiceResponse response = new GenericServiceResponse();
			SendReceiptRequest serviceRequest = sendReceiptCheckinRequestConverter.convert(request);
			Boolean serviceResponse = checkinPaymentServiceClient.sendEmailReceipt(serviceRequest);
			response = sendReceiptResponseConverter.convert(serviceResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			logger.debug("Executed method successfully sendEmailReceipt(). The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method sendEmailReceipt().", e);
			throw new AlitaliaServiceException("Exception executing sendEmailReceipt: Sid ["+request.getSid()+"]" , e);
		}
	}

}
