package com.alitalia.aem.service.impl.converter.home.newsletterservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RegisterUserToAgencyNewsletterRequest;
import com.alitalia.aem.ws.news.newsletterservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.news.newsletterservice.xsd1.RegisterUserRequest;
import com.alitalia.aem.ws.news.newsletterservice.xsd2.ArrayOfNLUser;

@Component(immediate = true, metatype = false)
@Service(value = RegisterUserToAgencyNewsletterRequestToRegisterUserRequest.class)
public class RegisterUserToAgencyNewsletterRequestToRegisterUserRequest implements Converter<RegisterUserToAgencyNewsletterRequest, RegisterUserRequest> {

	@Reference
	private ListOfNLUserDataToArrayOfNLUser listOfNLUserConverter;
	
	@Override
	public RegisterUserRequest convert(RegisterUserToAgencyNewsletterRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		
		RegisterUserRequest request = objectFactory.createRegisterUserRequest();
		
		JAXBElement<String> jaxbAgencyId = objectFactory.createRegisterUserRequestAgencyId(source.getAgencyId());
		ArrayOfNLUser users = listOfNLUserConverter.convert(source.getUsers());
		
		request.setAgencyId(jaxbAgencyId);
		request.setUsers(objectFactory.createRegisterUserRequestUsers(users));
		
		return request;
	}
}