package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.UserInfoBaseData;
import com.alitalia.aem.common.data.home.UserInfoCompleteData;
import com.alitalia.aem.common.data.home.UserInfoFullData;
import com.alitalia.aem.common.data.home.UserInfoStandardData;
import com.alitalia.aem.common.data.home.enumerations.GenderTypeEnum;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.AUserInfoBase;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.UserInfoComplete;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.UserInfoFull;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.UserInfoStandard;

@Component(immediate = true, metatype = false)
@Service(value = AUserInfoToUserInfoBaseData.class)
public class AUserInfoToUserInfoBaseData implements Converter<AUserInfoBase, UserInfoBaseData> {

	@Override
	public UserInfoBaseData convert(AUserInfoBase source) {
		UserInfoBaseData destination = null;

		if (source != null) {
			if (source instanceof UserInfoFull) {
				UserInfoFull typedSource = (UserInfoFull) source;
				UserInfoFullData typedDestination = new UserInfoFullData();
				destination = typedDestination;

				if (typedSource.getAdress() != null) {
					typedDestination.setAdress(typedSource.getAdress().getValue());
				}

				typedDestination.setBirthDate(XsdConvertUtils.parseCalendar(typedSource.getBirthDate()));

				if (typedSource.getCAP() != null) {
					typedDestination.setCap(typedSource.getCAP().getValue());
				}

				if (typedSource.getCity() != null) {
					typedDestination.setCity(typedSource.getCity().getValue());
				}

				if (typedSource.getCountry() != null) {
					typedDestination.setCountry(typedSource.getCountry().getValue());
				}

				if (typedSource.getGender() != null) {
					typedDestination.setGender(GenderTypeEnum.fromValue(typedSource.getGender().value()));
				}

				if (typedSource.getState() != null) {
					typedDestination.setState(typedSource.getState().getValue());
				}

				if (typedSource.getHouseNumber() != null) {
					typedDestination.setHouseNumber(typedSource.getHouseNumber().getValue());
				}

				if (typedSource.getTitle() != null) {
					typedDestination.setTitle(typedSource.getTitle().getValue());
				}

				if (typedSource.getVatNumber() != null) {
					typedDestination.setVatNumber(typedSource.getVatNumber().getValue());
				}

			} else if (source instanceof UserInfoComplete) {
				UserInfoComplete typedSource = (UserInfoComplete) source;
				UserInfoCompleteData typedDestination = new UserInfoCompleteData();
				destination = typedDestination;

				if (typedSource.getAdress() != null) {
					typedDestination.setAdress(typedSource.getAdress().getValue());
				}

				if (typedSource.getCAP() != null) {
					typedDestination.setCap(typedSource.getCAP().getValue());
				}

				if (typedSource.getCity() != null) {
					typedDestination.setCity(typedSource.getCity().getValue());
				}

				if (typedSource.getCountry() != null) {
					typedDestination.setCountry(typedSource.getCountry().getValue());
				}

				if (typedSource.getState() != null) {
					typedDestination.setState(typedSource.getState().getValue());
				}

			} else if (source instanceof UserInfoStandard) {
				UserInfoStandardData typedDestination = new UserInfoStandardData();
				destination = typedDestination;
			} else {
				destination = new UserInfoBaseData();
			}

			if (source.getLastname() != null) {
				destination.setLastname(source.getLastname().getValue());
			}

			if (source.getName() != null) {
				destination.setName(source.getName().getValue());
			}
		}

		return destination;
	}

}
