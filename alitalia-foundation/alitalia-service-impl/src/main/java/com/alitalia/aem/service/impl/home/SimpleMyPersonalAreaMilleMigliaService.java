package com.alitalia.aem.service.impl.home;

import com.alitalia.aem.common.messages.home.*;
import com.alitalia.aem.common.messages.home.GetDatiSicurezzaProfiloOLDRequest;
import com.alitalia.aem.common.messages.home.GetDatiSicurezzaProfiloOLDResponse;
import com.alitalia.aem.common.messages.home.SetDatiSicurezzaProfiloRequest;
import com.alitalia.aem.common.messages.home.SetDatiSicurezzaProfiloResponse;
import com.alitalia.aem.common.messages.home.SetSocialAccountRequest;
import com.alitalia.aem.common.messages.home.SetSocialAccountResponse;
import com.alitalia.aem.service.impl.home.*;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.wsdl.*;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.*;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SubscribeSARequest;
//import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd5.CustomerProfile;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.AddFlightActivityRequestToInsertFlightActivityRequest;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.AddressesRequestToGetAddressesRequest;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.BalanceRequestToCustomerRequest;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.CreditCardsListRequestToGetCardListRequest;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.CustomerResponseToSubscribeResponse;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.DeletePanRequestToDeletePanSIARequest;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.DeletePanSIAResponseToDeletePanResponse;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.GetAddressesResponseToAddressesResponse;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.GetBalanceResponseToBalanceResponse;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.GetCreditCardListResponseToCreditCardsListResponse;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.GetMMUserFromSocialResponseToUserFromSocialResponse;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.GetMessagesResponseToMessagesResponse;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.GetProfileResponseToProfileResponse;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.GetSocialAccountResponseToSocialAccountsResponse;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.GetStatementByDateResponseToStatementByDateResponse;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.InsertFlightActivityResponseToAddFlightActivityResponse;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.LoginResponseToMilleMigliaLoginResponse;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.MailingListRegistrationResponseToMailinglistRegistrationResponse;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.MailinglistRegistrationRequestToMailingListRegistrationRequest;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.MessagesRequestToGetMessagesRequest;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.MilleMigliaLoginRequestToLoginRequest;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.NotifyLoginResponseToNotifySocialLoginResponse;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.NotifySocialLoginRequestToNotifyLoginRequest;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.PaybackAccrualRequestConverter;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.PaybackAccrualResponseConverter;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.ProfileRequestToGetProfileRequest;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.RemoveSocialAccountResponseToRemoveSocialAccountsResponse;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.RemoveSocialAccountsRequestToRemoveSocialAccountRequest;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.RetrievePINResponseToRetrievePinResponse;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.RetrievePinRequestToRetrievePINRequest;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.SendEmailRequestToSendMailRequest;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.SendMailResponseToSendEmailResponse;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.SendNewMailingListRegistrationMailRequestToSendMailRequest;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.SetSocialAccountRequestToSetSocialAccountRequest;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.SetSocialAccountResponseToSetSocialAccountResponse;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.SocialAccountsRequestToGetSocialAccountsRequest;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.StatementByDateRequestToGetStatementByDateRequest;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.SubscribeRequestToCustomerRequest;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.UpdateNickNameResponseToUpdateNicknameResponse;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.UpdateNicknameRequestToUpdateNickNameRequest;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.UpdatePINResponseToUpdatePinResponse;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.UpdatePinRequestToUpdatePINRequest;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.UpdateProfileResponseToUpdateUserProfileResponse;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.UpdateUserProfileRequestToUpdateProfileRequest;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.UserFromSocialRequestToGetMMUserFromSocialRequest;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.*;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.MilleMigliaServiceClient;

import javax.xml.bind.JAXBElement;

@Service
@Component(immediate = true, metatype = false)
public class SimpleMyPersonalAreaMilleMigliaService implements MyPersonalAreaMilleMigliaService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleMyPersonalAreaMilleMigliaService.class);

	@Reference
	private MilleMigliaServiceClient milleMigliaServiceClient;

	@Reference
	private ProfileRequestToGetProfileRequest profileRequestToGetProfileRequestConverter;

	@Reference
	private GetProfileResponseToProfileResponse getProfileResponseToProfileResponseConverter;

	@Reference
	private MilleMigliaLoginRequestToLoginRequest loginRequestConverter;

	@Reference
	private MilleMigliaLoginRequestToLoginSARequest loginSARequestConverter;

	@Reference
	private LoginResponseToMilleMigliaLoginSAResponse loginSAResponseConverter;

	@Reference
	private LoginResponseToMilleMigliaLoginResponse loginResponseConverter;

	@Reference
	private LoginResponseToMilleMigliaLoginResponse loginFaultConverter;	

	@Reference
	private UserFromSocialRequestToGetMMUserFromSocialRequest mmCodeRequestConverter;

	@Reference
	private GetMMUserFromSocialResponseToUserFromSocialResponse mmCodeResponseConverter;

	@Reference
	private SetSocialAccountRequestToSetSocialAccountRequest setSocialAccountRequestConverter;

	@Reference
	private SetSocialAccountResponseToSetSocialAccountResponse setSocialAccountResponseConverter;

	@Reference
	private NotifySocialLoginRequestToNotifyLoginRequest notifySocialLoginRequestConverter;

	@Reference
	private NotifyLoginResponseToNotifySocialLoginResponse notifySocialLoginResponseConverter;

	@Reference
	private AddressesRequestToGetAddressesRequest addressesRequestToGetAddressesRequestConverter;

	@Reference
	private GetAddressesResponseToAddressesResponse getAddressesResponseToAddressesResponseConverter;

	@Reference
	private UpdateUserProfileRequestToUpdateProfileRequest updateUserProfileRequestToUpdateProfileRequestConverter;

	@Reference
	private UpdateProfileResponseToUpdateUserProfileResponse updateProfileResponseToUpdateUserProfileResponseConverter;

	@Reference
	private MessagesRequestToGetMessagesRequest messagesRequestToGetMessagesRequestConverter;

	@Reference
	private GetMessagesResponseToMessagesResponse getMessagesResponseToMessagesResponseConverter;
	
	@Reference
	private BalanceRequestToCustomerRequest balanceRequestToCustomerRequestConverter;

	@Reference
	private GetBalanceResponseToBalanceResponse getBalanceResponseToBalanceResponseConverter;
	
	@Reference
	private StatementByDateRequestToGetStatementByDateRequest statementByDateRequestToGetStatementByDateRequestConverter;

	@Reference
	private GetStatementByDateResponseToStatementByDateResponse getStatementByDateResponseToStatementByDateResponseConverter;
	
	@Reference
	private AddFlightActivityRequestToInsertFlightActivityRequest addFlightActivityRequestToInsertFlightActivityRequestConverter;

	@Reference
	private InsertFlightActivityResponseToAddFlightActivityResponse insertFlightActivityResponseToAddFlightActivityResponseConverter;
	
	@Reference
	private UpdatePinRequestToUpdatePINRequest updatePinRequestConverter;

	@Reference
	private UpdatePINResponseToUpdatePinResponse updatePINResponseConverter;
	
	@Reference
	private SocialAccountsRequestToGetSocialAccountsRequest socialAccountsRequestConverter;

	@Reference
	private GetSocialAccountResponseToSocialAccountsResponse socialAccountsResponseConverter;
	
	@Reference
	private SubscribeRequestToCustomerRequest subscribeRequestConverter;

	@Reference
	private CustomerResponseToSubscribeResponse subscribeResponseConverter;
	
	@Reference
	private SendEmailRequestToSendMailRequest sendEmailRequestConverter;
	
	@Reference
	private SendNewMailingListRegistrationMailRequestToSendMailRequest sendNewMailingListRegistrationMailConverter;

	@Reference
	private SendMailResponseToSendEmailResponse sendMailResponseConverter;
	
	@Reference
	private UpdateNicknameRequestToUpdateNickNameRequest updateNicknameRequestConverter;

	@Reference
	private UpdateNickNameResponseToUpdateNicknameResponse updateNickNameResponseConverter;
	
	@Reference
	private RetrievePinRequestToRetrievePINRequest retrievePinRequestConverter;

	@Reference
	private RetrievePINResponseToRetrievePinResponse retrievePINResponseConverter;
	
	@Reference
	private MailinglistRegistrationRequestToMailingListRegistrationRequest mailinglistRegistrationRequestConverter;

	@Reference
	private MailingListRegistrationResponseToMailinglistRegistrationResponse mailingListRegistrationResponseConverter;
	
	@Reference
	private CreditCardsListRequestToGetCardListRequest creditCardsListRequestConverter;

	@Reference
	private GetCreditCardListResponseToCreditCardsListResponse getCreditCardListResponseConverter;
	
	@Reference
	private DeletePanRequestToDeletePanSIARequest deletePanRequestConverter;

	@Reference
	private DeletePanSIAResponseToDeletePanResponse deletePanSIAResponseConverter;
	
	@Reference
	private RemoveSocialAccountsRequestToRemoveSocialAccountRequest removeSocialAccountsRequestConverter;

	@Reference
	private RemoveSocialAccountResponseToRemoveSocialAccountsResponse removeSocialAccountResponseConverter;
	
	@Reference
	private PaybackAccrualRequestConverter paybackAccrualRequestConverter;
	
	@Reference
	private PaybackAccrualResponseConverter paybackAccrualResponseConverter;

    @Reference
    private CheckEmailUserNameDuplicateRequestConverter checkEmailUserNameDuplicateRequestConverter;

    @Reference
    private CheckEmailUserNameDuplicateResponseConverter checkEmailUserNameDuplicateResponseConverter;

    @Reference
    private CheckUserNameDuplicateRequestConverter checkUserNameDuplicateRequestConverter;

    @Reference
    private CheckUserNameDuplicateResponseConverter checkUserNameDuplicateResponseConverter;

	@Reference
	private SubscribeSARequestToCustomerRequest subscribeSARequestConverter;

	@Reference
	private SubscribeSAResponseToCustomerResponse subscribeSAResponseConverter;


	@Reference
	private SetDatiSicurezzaProfiloRequesToWsSetDatiSicurezzaProfiloRequest setDatiSicurezzaProfiloRequestConverterd;

	@Reference
	private SetDatiSicurezzaProfiloResponseConverter setDatiSicurezzaProfiloResponseConverter;


	@Reference
	private GetDatiSicurezzaProfiloRequesToWsConverter getDatiSicurezzaProfiloRequesToWsConverter;

	@Reference
	private GetDatiSicurezzaProfiloResponseToLocalConverter getDatiSicurezzaProfiloResponseToLocalConverter;


	@Reference
	private FindProfileRequestConverter findProfileRequestConverter;

	@Reference
	private FindProfileResponseConverter findProfileResponseConverter;

	@Reference
	private GetDatiSicurezzaProfiloOLDRequestConverter getDatiSicurezzaProfiloOLDRequestConverter;

	@Reference
	private GetDatiSicurezzaProfiloOLDResponseConverter getDatiSicurezzaProfiloOLDResponseConverter;



	@Override
	public MilleMigliaLoginResponse login(MilleMigliaLoginRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method login. The request is {}", request);

		MilleMigliaLoginResponse response = new MilleMigliaLoginResponse();

		try {

			LoginRequest loginRequest = loginRequestConverter.convert(request);
			LoginResponse loginResponse = milleMigliaServiceClient.login(loginRequest);
			response = loginResponseConverter.convert(loginResponse);

			response.setLoginSuccessful(Boolean.TRUE);
			if(logger.isDebugEnabled())
			logger.debug("Executed method login successfully. The response is {}", response);
		} catch (IMilleMigliaServiceLoginServiceExceptionFaultFaultMessage e) {
			logger.error("IMilleMigliaServiceLoginServiceExceptionFaultFaultMessage while executing method login.", e);
			response.setLoginSuccessful(Boolean.FALSE);
			if (e.getFaultInfo() != null) {
				// TODO [consumer/login] apply some logic to decode the error kind
				if (e.getFaultInfo().getErrorCode() != null) {
					response.setErrorCode(e.getFaultInfo().getErrorCode().value());
				}
				if (e.getFaultInfo().getMessage() != null) {
					response.setErrorDescription(e.getFaultInfo().getMessage().getValue());
				}
			}

		} catch (Exception e) {
			response.setLoginSuccessful(Boolean.FALSE);
			logger.error("Exception while executing method login.", e);

		} finally {
			response.setTid(response.getTid());
			response.setSid(response.getSid());
		}

		return response;
	}

	@Override
	public MilleMigliaLoginSAResponse loginSA(MilleMigliaLoginSARequest request) {
		if(logger.isDebugEnabled())
			logger.debug("Executing method loginSA. The request is {}", request);

		MilleMigliaLoginSAResponse response = new MilleMigliaLoginSAResponse();

		try {

			LoginSARequest loginSARequest = loginSARequestConverter.convert(request);
			LoginSAResponse loginSAResponse = milleMigliaServiceClient.loginSA(loginSARequest);

			response = loginSAResponseConverter.convert(loginSAResponse);


			response.setLoginSuccessful(true);

			if(logger.isDebugEnabled())
				logger.debug("Executed method loginSA successfully. The response is {}", response);
		} catch (IMilleMigliaServiceLoginSAServiceExceptionFaultFaultMessage e) {
			logger.error("IMilleMigliaServiceLoginSAServiceExceptionFaultFaultMessage while executing method loginSA.", e);
//			response.setLoginSuccessful(Boolean.FALSE);
			if (e.getFaultInfo() != null) {
				// TODO [consumer/login] apply some logic to decode the error kind
				if (e.getFaultInfo().getErrorCode() != null) {
					response.setErrorCode(e.getFaultInfo().getErrorCode().value());
				}
				if (e.getFaultInfo().getMessage() != null) {
					response.setErrorDescription(e.getFaultInfo().getMessage().getValue());
				}
			}

		} catch (Exception e) {
//			response.setLoginSuccessful(Boolean.FALSE);
			logger.error("Exception while executing method loginSA.", e);

		} finally {
			response.setTid(response.getTid());
			response.setSid(response.getSid());
		}

		return response;
	}

	@Override
	public UserFromSocialResponse getMMCode(UserFromSocialRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method getMMCode. The request is {}", request);

		UserFromSocialResponse response = new UserFromSocialResponse();

		try {
			GetMMUserFromSocialRequest mmCodeRequest = mmCodeRequestConverter.convert(request);
			GetMMUserFromSocialResponse mmCodeResponse = milleMigliaServiceClient.getMMCode(mmCodeRequest);
			response = mmCodeResponseConverter.convert(mmCodeResponse);

			response.setSuccessful(Boolean.TRUE);
			if(logger.isDebugEnabled())
			logger.debug("Executed method getMMCode. The response is {}", response);
		} catch (Exception e) {
			// TODO [consumer/login] assuming that this exception means "no MM user associated", is this correct?
			response.setSuccessful(Boolean.TRUE);
			if(logger.isDebugEnabled())
			logger.debug("No MM user associated.");
			//response.setSuccessful(Boolean.FALSE);
		} finally {
			response.setTid(request.getTid());
			response.setSid(request.getSid());
		}
		return response;
	}

	@Override
	public SetSocialAccountResponse setSocialAccount(SetSocialAccountRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method setSocialAccount. The request is {}", request);

		SetSocialAccountResponse response = new SetSocialAccountResponse();

		try {
			com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SetSocialAccountRequest setSocialAccountRequest =
					setSocialAccountRequestConverter.convert(request);
			com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SetSocialAccountResponse setSocialAccountResponse = 
					milleMigliaServiceClient.setSocialAccount(setSocialAccountRequest);
			response = setSocialAccountResponseConverter.convert(setSocialAccountResponse);
			if(logger.isDebugEnabled())
			logger.debug("Executed method setSocialAccount. The response is {}", response);
		} catch (Exception e) {
			logger.error("Exception while executing method setSocialAccount.", e);
			response.setSucceeded(Boolean.FALSE);
		} finally {
			response.setTid(request.getTid());
			response.setSid(request.getSid());
		}
		return response;
	}

	@Override
	public NotifySocialLoginResponse notifyLogin(NotifySocialLoginRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method notifyLogin. The request is {}", request);

		NotifySocialLoginResponse response = new NotifySocialLoginResponse();

		try {
			NotifyLoginRequest notifyLoginRequest = notifySocialLoginRequestConverter.convert(request);
			NotifyLoginResponse notifyLoginResponse = milleMigliaServiceClient.notifyLogin(notifyLoginRequest);
			response = notifySocialLoginResponseConverter.convert(notifyLoginResponse);
			if(logger.isDebugEnabled())
			logger.debug("Executed method notifyLogin. The response is {}", response);
		} catch (Exception e) {
			logger.error("Exception while executing method notifyLogin.", e);
			response.setResult(Boolean.FALSE);
		} finally{
			response.setTid(request.getTid());
			response.setSid(request.getSid());
		}
		return response;
	}

	@Override
	public ProfileResponse getProfile(ProfileRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method getProfile. The request is {}", request);

		try {
			ProfileResponse response = new ProfileResponse();
			response.setTid(request.getTid());
			response.setSid(request.getSid());

			GetProfileRequest getProfileRequest = profileRequestToGetProfileRequestConverter.convert(request);
			GetProfileResponse getProfileResponse = milleMigliaServiceClient.getProfile(getProfileRequest);
			response = getProfileResponseToProfileResponseConverter.convert(getProfileResponse);
			if(logger.isDebugEnabled())
			logger.debug("Executed method getProfile successfully. The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getProfile.", e);
			throw new AlitaliaServiceException("Exception executing getProfile: Sid [" + request.getSid() + "]" , e);
		}
	}

	@Override
	public AddressesResponse getAddresses(AddressesRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method getAddresses. The request is {}", request);

		try {
			AddressesResponse response = new AddressesResponse();
			response.setTid(request.getTid());
			response.setSid(request.getSid());

			GetAddressesRequest getAddressesRequest = addressesRequestToGetAddressesRequestConverter.convert(request);
			GetAddressesResponse getAddressesResponse = milleMigliaServiceClient.getAddresses(getAddressesRequest);
			response = getAddressesResponseToAddressesResponseConverter.convert(getAddressesResponse);
			if(logger.isDebugEnabled())
			logger.debug("Executed method getAddresses successfully. The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getAddresses.", e);
			throw new AlitaliaServiceException("Exception executing getAddresses: Sid [" + request.getSid() + "]" , e);
		}
	}

	@Override
	public UpdateUserProfileResponse updateProfile(UpdateUserProfileRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method updateProfile. The request is {}", request);

		try {
			UpdateUserProfileResponse response = new UpdateUserProfileResponse();
			response.setTid(request.getTid());
			response.setSid(request.getSid());

			UpdateProfileRequest updateProfileRequest = updateUserProfileRequestToUpdateProfileRequestConverter.convert(request);
			UpdateProfileResponse updateProfileResponse = milleMigliaServiceClient.updateProfile(updateProfileRequest);
			response = updateProfileResponseToUpdateUserProfileResponseConverter.convert(updateProfileResponse);
			if(logger.isDebugEnabled())
			logger.debug("Executed method updateProfile successfully. The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method updateProfile.", e);
			throw new AlitaliaServiceException("Exception executing updateProfile: Sid [" + request.getSid() + "]" , e);
		}
	}

	@Override
	public MessagesResponse getMessages(MessagesRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method getMessages(). The request is {}", request);

		MessagesResponse response = new MessagesResponse();
		response.setTid(request.getTid());
		response.setSid(request.getSid());

		try {

			GetMessagesRequest getMessagesRequest = messagesRequestToGetMessagesRequestConverter.convert(request);
			GetMessagesResponse getMessagesResponse = milleMigliaServiceClient.getMessages(getMessagesRequest);
			response = getMessagesResponseToMessagesResponseConverter.convert(getMessagesResponse);
			if(logger.isDebugEnabled())
			logger.debug("Method getMessages() successfully executed. The response is {}", response);
		} catch (IMilleMigliaServiceGetMessagesServiceFaultFaultFaultMessage e) {
			logger.error("Exception while executing method getMessages().", e);
			response.setReturnCode(-1);
			response.setErrorMessage(e.getMessage());
		} catch (Exception e) {
			logger.error("Exception while executing method getMessages().", e);
			throw new AlitaliaServiceException("Exception executing getMessages: Sid [" + request.getSid() + "]" , e);
		}
		return response;
	}

	@Override
	public BalanceResponse getBalance(BalanceRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method getBalance. The request is {}", request);

		try {
			BalanceResponse response = new BalanceResponse();
			response.setTid(request.getTid());
			response.setSid(request.getSid());

			CustomerRequest balanceRequest = balanceRequestToCustomerRequestConverter.convert(request);
			GetBalanceResponse getBalanceResponse = milleMigliaServiceClient.getBalance(balanceRequest);
			response = getBalanceResponseToBalanceResponseConverter.convert(getBalanceResponse);
			if(logger.isDebugEnabled())
			logger.debug("Executed method getBalance successfully. The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getBalance.", e);
			throw new AlitaliaServiceException("Exception executing getBalance: Sid [" + request.getSid() + "]" , e);
		}
	}
	
	@Override
	public StatementByDateResponse getStatementByDate(StatementByDateRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method getStatementByDate. The request is {}", request);

		try {
			StatementByDateResponse response = new StatementByDateResponse();
			response.setTid(request.getTid());
			response.setSid(request.getSid());

			GetStatementByDateRequest getStatementByDateRequest = statementByDateRequestToGetStatementByDateRequestConverter.convert(request);
			GetStatementByDateResponse getStatementByDateResponse = milleMigliaServiceClient.getStatementByDate(getStatementByDateRequest);
			response = getStatementByDateResponseToStatementByDateResponseConverter.convert(getStatementByDateResponse);
			if(logger.isDebugEnabled())
			logger.debug("Executed method getStatementByDate successfully. The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getStatementByDate.", e);
			throw new AlitaliaServiceException("Exception executing getStatementByDate: Sid [" + request.getSid() + "]" , e);
		}
	}
	
	@Override
	public AddFlightActivityResponse addFlightActivity(AddFlightActivityRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method addFlightActivity(). The request is {}", request);

		AddFlightActivityResponse response = new AddFlightActivityResponse();
		response.setTid(request.getTid());
		response.setSid(request.getSid());

		try {

			InsertFlightActivityRequest insertFlightActivityRequest = addFlightActivityRequestToInsertFlightActivityRequestConverter.convert(request);
			InsertFlightActivityResponse insertFlightActivityResponse = milleMigliaServiceClient.insertFlightActivity(insertFlightActivityRequest);
			response = insertFlightActivityResponseToAddFlightActivityResponseConverter.convert(insertFlightActivityResponse);
			if(logger.isDebugEnabled())
			logger.debug("Method addFlightActivity() successfully executed. The response is {}", response);
		} catch (IMilleMigliaServiceInsertFlightActivityServiceExceptionFaultFaultMessage e) {
			logger.error("Exception while executing method addFlightActivity().", e);
			if(e.getFaultInfo() != null && e.getFaultInfo().getMessage() != null){
				try{
					response.setReturnCode(Integer.parseInt(e.getFaultInfo().getMessage().getValue()));
				}catch (NumberFormatException e1) {
					logger.error("Method addFlightActivity() Exception while executing parsing String to Integer");
					response.setReturnCode(-1);
				}
			}else{
				response.setReturnCode(-1);
			}
			response.setErrorMessage(e.getFaultInfo().getErrorCode().value());
		} catch (Exception e) {
			logger.error("Exception while executing method addFlightActivity().", e);
			throw new AlitaliaServiceException("Exception executing addFlightActivity: Sid [" + request.getSid() + "]" , e);
		}
		return response;
	}

	@Override
	public UpdatePinResponse updatePin(UpdatePinRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method updatePin. The request is {}", request);

		try {
			UpdatePinResponse response = new UpdatePinResponse();
			response.setTid(request.getTid());
			response.setSid(request.getSid());

			UpdatePINRequest updatePinRequest = updatePinRequestConverter.convert(request);
			UpdatePINResponse updatePinResponse = milleMigliaServiceClient.updatePIN(updatePinRequest);
			response = updatePINResponseConverter.convert(updatePinResponse);
			if(logger.isDebugEnabled())
			logger.debug("Executed method updatePin successfully. The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method updatePin.", e);
			throw new AlitaliaServiceException("Exception executing updatePin: Sid [" + request.getSid() + "]" , e);
		}
	}

	@Override
	public SocialAccountsResponse getSocialAccounts(SocialAccountsRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method getSocialAccounts. The request is {}", request);

		try {
			SocialAccountsResponse response = new SocialAccountsResponse();
			response.setTid(request.getTid());
			response.setSid(request.getSid());

			GetSocialAccountRequest getSocialAccountRequest = socialAccountsRequestConverter.convert(request);
			GetSocialAccountResponse getSocialAccountResponse = milleMigliaServiceClient.getSocialAccounts(getSocialAccountRequest);
			response = socialAccountsResponseConverter.convert(getSocialAccountResponse);
			if(logger.isDebugEnabled())
			logger.debug("Executed method getSocialAccounts successfully. The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getSocialAccounts.", e);
			throw new AlitaliaServiceException("Exception executing getSocialAccounts: Sid [" + request.getSid() + "]" , e);
		}
	}



	@Override
	public UpdateNicknameResponse updateNickname(UpdateNicknameRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method updateNickname. The request is {}", request);

		try {
			UpdateNicknameResponse response = new UpdateNicknameResponse();
			response.setTid(request.getTid());
			response.setSid(request.getSid());

			UpdateNickNameRequest updateNicknameRequest = updateNicknameRequestConverter.convert(request);
			UpdateNickNameResponse updateNicknameResponse = milleMigliaServiceClient.updateNickName(updateNicknameRequest);
			response = updateNickNameResponseConverter.convert(updateNicknameResponse);
			if(logger.isDebugEnabled())
			logger.debug("Executed method updateNickname successfully. The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method updateNickname.", e);
			throw new AlitaliaServiceException("Exception executing updateNickname: Sid [" + request.getSid() + "]" , e);
		}
	}

	@Override
	public RetrievePinResponse retrievePinByCustomerNumber(RetrievePinRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method retrievePinByCustomerNumber. The request is {}", request);

		RetrievePinResponse response = new RetrievePinResponse();

		try {

			RetrievePINRequest retrievePINRequest = retrievePinRequestConverter.convert(request);
			RetrievePINResponse retrievePINResponse = milleMigliaServiceClient.retrievePinByCustomerNumber(retrievePINRequest);
			response = retrievePINResponseConverter.convert(retrievePINResponse);
			if(logger.isDebugEnabled())
			logger.debug("Executed method retrievePinByCustomerNumber. The response is {}", response);
		} catch (IMilleMigliaServiceRetrievePinByCustomerNumberServiceFaultFaultFaultMessage e1){
			logger.error("Exception while executing method retrievePinByCustomerNumber.", e1);
			response.setRetrieved(false);
			response.setReturnCode(-1);
			response.setErrorMessage(getErrorMessageFromServiceFaultInfo(e1.getFaultInfo()));
		} catch (Exception e) {
			logger.error("Exception while executing method retrievePinByCustomerNumber.", e);
			response.setRetrieved(false);
			response.setReturnCode(-1);
			response.setErrorMessage(e.getMessage());
		} finally{
			response.setTid(request.getTid());
			response.setSid(request.getSid());
		}
		return response;
	}

	@Override
	public RetrievePinResponse retrievePinByNickname(RetrievePinRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method retrievePinByNickname(). The request is {}", request);

		RetrievePinResponse response = new RetrievePinResponse();

		try {

			RetrievePINRequest retrievePINRequest = retrievePinRequestConverter.convert(request);
			RetrievePINResponse retrievePINResponse = milleMigliaServiceClient.retrievePinByNickName(retrievePINRequest);
			response = retrievePINResponseConverter.convert(retrievePINResponse);
			if(logger.isDebugEnabled())
			logger.debug("Method retrievePinByNickname() successfully executed. The response is {}", response);
		} catch (IMilleMigliaServiceRetrievePinByNickNameServiceFaultFaultFaultMessage e) {
			response.setRetrieved(false);
			response.setReturnCode(-1);
			response.setErrorMessage(getErrorMessageFromServiceFaultInfo(e.getFaultInfo()));
			logger.error("Exception while executing method retrievePinByNickname().", e.getFaultInfo());
		} catch (Exception e) {
			logger.error("Generic Exception while executing method retrievePinByNickname().", e);
			response.setRetrieved(false);
			response.setReturnCode(-1);
			response.setErrorMessage(e.getMessage());
		} finally {
			response.setTid(request.getTid());
			response.setSid(request.getSid());
		}
		return response;
	}
	
	@Override
	public MillemigliaSendEmailResponse sendNewRegistrationEmail(MillemigliaSendEmailRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method sendNewRegistrationEmail. The request is {}", request);

		try {
			MillemigliaSendEmailResponse response = new MillemigliaSendEmailResponse();
			response.setTid(request.getTid());
			response.setSid(request.getSid());

			SendMailRequest sendEmailRequest = sendEmailRequestConverter.convert(request);
			SendMailResponse sendEmailResponse = milleMigliaServiceClient.sendNewRegistrationMail(sendEmailRequest);
			response = sendMailResponseConverter.convert(sendEmailResponse);
			if(logger.isDebugEnabled())
			logger.debug("Executed method sendNewRegistrationEmail successfully. The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method sendNewRegistrationEmail.", e);
			throw new AlitaliaServiceException("Exception executing sendNewRegistrationEmail: Sid [" + request.getSid() + "]" , e);
		}
	}

	@Override
	public MillemigliaSendEmailResponse sendNewMailingListRegistrationMail(SendNewMailingListRegistrationMailRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method sendNewMailingListRegistrationMail. The request is {}", request);

		try {
			MillemigliaSendEmailResponse response = new MillemigliaSendEmailResponse();
			response.setTid(request.getTid());
			response.setSid(request.getSid());

			SendMailRequest sendNewMailingListRegistrationMailRequest = 
					sendNewMailingListRegistrationMailConverter.convert(request);
			SendMailResponse sendEmailResponse = 
					milleMigliaServiceClient.sendNewMailingListRegistrationMail(sendNewMailingListRegistrationMailRequest);
			response = sendMailResponseConverter.convert(sendEmailResponse);
			if(logger.isDebugEnabled())
			logger.debug("Executed method sendNewMailingListRegistrationMail successfully. The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method sendNewMailingListRegistrationMail.", e);
			throw new AlitaliaServiceException("Exception executing sendNewMailingListRegistrationMail: Sid [" + request.getSid() + "]" , e);
		}
	}

	@Override
	public MailinglistRegistrationResponse registerToMailinglist(MailinglistRegistrationRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method registerToMailinglist. The request is {}", request);

		MailinglistRegistrationResponse response = new MailinglistRegistrationResponse();
		response.setTid(request.getTid());
		response.setSid(request.getSid());

		try {

			MailingListRegistrationRequest mailingListRequest = mailinglistRegistrationRequestConverter.convert(request);
			MailingListRegistrationResponse mailingListResponse = milleMigliaServiceClient.mailingListRegistration(mailingListRequest);
			response = mailingListRegistrationResponseConverter.convert(mailingListResponse);
			if(logger.isDebugEnabled())
			logger.debug("Method registerToMailinglist executed successfully. The response is {}", response);
		} catch(IMilleMigliaServiceMailingListRegistrationServiceExceptionFaultFaultMessage exc){
			logger.error("Exception while executing method registerToMailinglist.", exc);
			response.setRegistrationIsSuccessful(false);
//			response.setErrorMessage(exc.getMessage());
			response.setErrorMessage(getErrorMessageFromServiceException(exc.getFaultInfo()));
			
		} catch (Exception e) {
			logger.error("Exception while executing method registerToMailinglist.", e);
			throw new AlitaliaServiceException("Exception executing registerToMailinglist: Sid [" + request.getSid() + "]" , e);
		}
		return response;
	}

	@Override
	public CreditCardsListResponse getCreditCardsList(CreditCardsListRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method getCreditCardsList. The request is {}", request);

		try {
			CreditCardsListResponse response = new CreditCardsListResponse();
			response.setTid(request.getTid());
			response.setSid(request.getSid());

			GetCreditCardListRequest getCreditCardsListRequest = creditCardsListRequestConverter.convert(request);
			GetCreditCardListResponse mailingListResponse = milleMigliaServiceClient.getCreditCardList(getCreditCardsListRequest);
			response = getCreditCardListResponseConverter.convert(mailingListResponse);
			if(logger.isDebugEnabled())
			logger.debug("Method getCreditCardsList executed successfully. The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getCreditCardsList.", e);
			throw new AlitaliaServiceException("Exception executing getCreditCardsList: Sid [" + request.getSid() + "]" , e);
		}
	}

	@Override
	public DeletePanResponse deletePan(DeletePanRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method deletePan. The request is {}", request);

		try {
			DeletePanResponse response = new DeletePanResponse();
			response.setTid(request.getTid());
			response.setSid(request.getSid());

			DeletePanSIARequest deletePanSIARequest = deletePanRequestConverter.convert(request);
			DeletePanSIAResponse deletePanSIAResponse = milleMigliaServiceClient.deletePanSIA(deletePanSIARequest);
			response = deletePanSIAResponseConverter.convert(deletePanSIAResponse);
			if(logger.isDebugEnabled())
			logger.debug("Method deletePan executed successfully. The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method deletePan.", e);
			throw new AlitaliaServiceException("Exception executing deletePan: Sid [" + request.getSid() + "]" , e);
		}
	}

	@Override
	public RemoveSocialAccountsResponse removeUidGigya(RemoveSocialAccountsRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method removeUidGigya. The request is {}", request);

		try {
			RemoveSocialAccountsResponse response = new RemoveSocialAccountsResponse();
			response.setTid(request.getTid());
			response.setSid(request.getSid());

			RemoveSocialAccountRequest removeSocialAccountRequest = removeSocialAccountsRequestConverter.convert(request);
			RemoveSocialAccountResponse removeSocialAccountResponse = milleMigliaServiceClient.removeUidGigya(removeSocialAccountRequest);
			response = removeSocialAccountResponseConverter.convert(removeSocialAccountResponse);
			if(logger.isDebugEnabled())
			logger.debug("Method removeUidGigya executed successfully. The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing removeUidGigya: Sid [" + request.getSid() + "]" , e);
		}
	}	
	
	@Override
	public MMPaybackAccrualResponse paybackAccrual(MMPaybackAccrualRequest request) {
		logger.debug("Executing method paybackAccrual. The request is {}", request);

		try {
			MMPaybackAccrualResponse response = new MMPaybackAccrualResponse();

			PaybackAccrualRequest paybackAccrualRequest = paybackAccrualRequestConverter.convert(request);
			PaybackAccrualResponse paybackAccrualResponse = milleMigliaServiceClient.paybackAccrual(paybackAccrualRequest);
			response = paybackAccrualResponseConverter.convert(paybackAccrualResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			logger.debug("Method paybackAccrual executed successfully. The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method paybackAccrual.", e);
			throw new AlitaliaServiceException("Exception executing paybackAccrual: Sid [" + request.getSid() + "]" , e);
		}
	}

	@Override
	public SubscribeResponse subscribeByCustomer(SubscribeRequest request) {
		if(logger.isDebugEnabled())
			logger.debug("Executing method subscribeByCustomer(). The request is {}", request);

		SubscribeResponse response = new SubscribeResponse();
		response.setTid(request.getTid());
		response.setSid(request.getSid());

		try {

			CustomerRequest subscribeByCustomerRequest = subscribeRequestConverter.convert(request);
			SubscribeByCustomerResponse subscribeByCustomerResponse = milleMigliaServiceClient.subscribeByCustomer(subscribeByCustomerRequest);
			response = subscribeResponseConverter.convert(subscribeByCustomerResponse);
			if(logger.isDebugEnabled())
				logger.debug("Method subscribeByCustomer() successfully executed. The response is {}", response);
		} catch (IMilleMigliaServiceSubscribeByCustomerServiceExceptionFaultFaultMessage e) {
			logger.error("Exception while executing method subscribeByCustomer().", e);
			response.setReturnCode(-1);
			response.setErrorMessage(e.getMessage());
		} catch (Exception e) {
			logger.error("Exception while executing method subscribeByCustomer().", e);
			throw new AlitaliaServiceException("Exception executing subscribeByCustomer: Sid [" + request.getSid() + "]" , e);
		}
		return response;
	}

    @Override
    public com.alitalia.aem.common.messages.home.SubscribeSAResponse subscribeSA(com.alitalia.aem.common.messages.home.SubscribeSARequest request) {
		if(logger.isDebugEnabled())
			logger.debug("Executing method subscribeSA. The request is {}", request);

		com.alitalia.aem.common.messages.home.SubscribeSAResponse response = new com.alitalia.aem.common.messages.home.SubscribeSAResponse();
		response.setTid(request.getTid());
		response.setSid(request.getSid());

		try {
			com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SubscribeSARequest req=subscribeSARequestConverter.convert(request);
			com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SubscribeSAResponse customerSAResponse = milleMigliaServiceClient.subscribeSA(req);
			response = subscribeSAResponseConverter.convert(customerSAResponse);
			if(logger.isDebugEnabled())
				logger.debug("Method subscribeSA() successfully executed. The response is {}", response);
		}
		catch (IMilleMigliaServiceSubscribeSAServiceExceptionFaultFaultMessage e) {
			logger.error("Exception while executing method subscribeSA().", e);
			response.setReturnCode(-1);
			response.setErrorMessage(e.getMessage());
		} catch (Exception e) {
			logger.error("Exception while executing method subscribeSA.", e);
			throw new AlitaliaServiceException("Exception executing subscribeSA: Sid [" + request.getSid() + "]" , e);
		} finally {
			response.setTid(response.getTid());
			response.setSid(response.getSid());
		}

		return response;
    }

    @Override
    public SubscribeSACheckEmailUserNameDuplicateResponse CheckEmailUserNameDuplicate(SubscribeSACheckEmailUserNameDuplicateRequest request) {
        if(logger.isDebugEnabled())
            logger.debug("Executing method CheckEmailUserNameDuplicate. The request is {}", request);

        SubscribeSACheckEmailUserNameDuplicateResponse response = new SubscribeSACheckEmailUserNameDuplicateResponse();

        try {

            CheckEmailUsernameDuplicateRequest checkEmailUserNameDuplicateRequestParameter = checkEmailUserNameDuplicateRequestConverter.convert(request);
            CheckEmailUsernameDuplicateResponse checkEmailUserNameDuplicateResponseOutput = milleMigliaServiceClient.CheckEmailUserNameDuplicate(checkEmailUserNameDuplicateRequestParameter);



            if (checkEmailUserNameDuplicateResponseOutput != null)
                response.setFlagNoEmailUserNameDuplicate(checkEmailUserNameDuplicateResponseOutput.isFlagNoEmailUsernameDuplicateOK());


            //TODO inserire eventuali implementazioni specifiche
            // response.setLoginSuccessful(Boolean.TRUE);

            if(logger.isDebugEnabled())
                logger.debug("Executed method CheckUserNameDuplicate successfully. The response is {}", response);
        } catch (IMilleMigliaServiceCheckEmailUsernameDuplicateServiceFaultFaultFaultMessage e) {
            logger.error("IMilleMigliaServiceCheckUserNameDuplicateServiceFaultFaultFaultMessage while executing method CheckEmailUserNameDuplicate.", e);

            //TODO inserire eventuali implementazioni specifiche
            //response.setLoginSuccessful(Boolean.FALSE);

            if (e.getFaultInfo() != null) {
                // TODO [CheckEmailUserNameDuplicate] apply some logic to decode the error kind

                response.setErrorMessage(getErrorMessageFromServiceFaultInfo(e.getFaultInfo()));

//				if (e.getFaultInfo().getErrorCode() != null) {
//					response.setErrorCode(e.getFaultInfo().getErrorCode().value());
//				}
//				if (e.getFaultInfo().getMessage() != null) {
//					response.setErrorDescription(e.getFaultInfo().getMessage().getValue());
//				}
            }

        } catch (Exception e) {
            //TODO inserire eventuali implementazioni specifiche per catch error
            // response.setLoginSuccessful(Boolean.FALSE);
            logger.error("Exception while executing method CheckEmailUserNameDuplicate.", e);

        } finally {
            response.setTid(response.getTid());
            response.setSid(response.getSid());
        }

        return response;
    }

    @Override
    public SubscribeSACheckUserNameDuplicateResponse CheckUserNameDuplicate(SubscribeSACheckUserNameDuplicateRequest request) {

	    if(logger.isDebugEnabled())
			logger.debug("Executing method CheckEmailUserNameDuplicate. The request is {}", request);

        SubscribeSACheckUserNameDuplicateResponse response = new SubscribeSACheckUserNameDuplicateResponse();

		try {

			CheckUsernameDuplicateRequest checkUserNameDuplicateRequestParameter = checkUserNameDuplicateRequestConverter.convert(request);
			CheckUsernameDuplicateResponse checkUserNameDuplicateResponseOutput = milleMigliaServiceClient.CheckUserNameDuplicate(checkUserNameDuplicateRequestParameter);
			response = checkUserNameDuplicateResponseConverter.convert(checkUserNameDuplicateResponseOutput);


			//TODO inserire eventuali implementazioni specifiche
			// response.setLoginSuccessful(Boolean.TRUE);

			if(logger.isDebugEnabled())
				logger.debug("Executed method CheckUserNameDuplicate successfully. The response is {}", response);
		} catch (IMilleMigliaServiceCheckUsernameDuplicateServiceFaultFaultFaultMessage e) {
			logger.error("IMilleMigliaServiceCheckUserNameDuplicateServiceFaultFaultFaultMessage while executing method CheckEmailUserNameDuplicate.", e);

            //TODO inserire eventuali implementazioni specifiche
            //response.setLoginSuccessful(Boolean.FALSE);

			if (e.getFaultInfo() != null) {
				// TODO [CheckEmailUserNameDuplicate] apply some logic to decode the error kind

                response.setErrorMessage(getErrorMessageFromServiceFaultInfo(e.getFaultInfo()));

//				if (e.getFaultInfo().getErrorCode() != null) {
//					response.setErrorCode(e.getFaultInfo().getErrorCode().value());
//				}
//				if (e.getFaultInfo().getMessage() != null) {
//					response.setErrorDescription(e.getFaultInfo().getMessage().getValue());
//				}
			}

		} catch (Exception e) {
			//TODO inserire eventuali implementazioni specifiche per catch error
            // response.setLoginSuccessful(Boolean.FALSE);
			logger.error("Exception while executing method CheckEmailUserNameDuplicate.", e);

		} finally {
			response.setTid(response.getTid());
			response.setSid(response.getSid());
		}

		return response;
    }

    @Override
    public SubscribeSASendMailResponse SubscribeSASendMail(SubscribeSASendMailRequest request) {
        //TODO implementare
	    return null;
    }


	private String getErrorMessageFromServiceFaultInfo(ServiceFault serviceFault) {
		return serviceFault.getRemoteSystemMessageDictionary().getValue().getKeyValueOfstringstring().get(0).getValue();
	}

	private String getErrorMessageFromServiceException(ServiceException serviceException) {
		return serviceException.getMessage().getValue();
	}

	/////// aiutoooooooooooooooooooo /////
//	@Override
//	public checkEmailUserNameDuplicateResponse CheckEmailUserNameDuplicate(CheckEmailUserNameDuplicateRequest request) {
//		if(logger.isDebugEnabled())
//			logger.debug("Executing method CheckEmailUserNameDuplicate. The request is {}", request);
//
//		CheckEmailUserNameDuplicateResponse response = new CheckEmailUserNameDuplicateResponse();
//
//		try {
//
//			CheckEmailUserNameDuplicateRequest CheckEmailUserNameDuplicateRequestParameter = CheckEmailUserNameDuplicateRequestConverter.convert(request);
//			CheckEmailUserNameDuplicateResponse CheckEmailUserNameDuplicateResponseOutput = milleMigliaServiceClient.CheckEmailUserNameDuplicate(CheckEmailUserNameDuplicateRequestParameter);
//			response = CheckEmailUserNameDuplicateResponseConverter.convert(CheckEmailUserNameDuplicateResponseOutput);
//
//
//			//TODO inserire eventuali implementazioni specifiche
//			// response.setLoginSuccessful(Boolean.TRUE);
//
//			if(logger.isDebugEnabled())
//				logger.debug("Executed method CheckEmailUserNameDuplicate successfully. The response is {}", response);
//		} catch (IMilleMigliaServiceCheckEmailUserNameDuplicateServiceFaultFaultFaultMessage e) {
//			logger.error("IMilleMigliaServiceCheckEmailUserNameDuplicateServiceExceptionFaultFaultMessage while executing method CheckEmailUserNameDuplicate.", e);
//
////TODO inserire eventuali implementazioni specifiche
////response.setLoginSuccessful(Boolean.FALSE);
//
//			if (e.getFaultInfo() != null) {
//				// TODO [CheckEmailUserNameDuplicate] apply some logic to decode the error kind
//				if (e.getFaultInfo().getErrorCode() != null) {
//					response.setErrorCode(e.getFaultInfo().getErrorCode().value());
//				}
//				if (e.getFaultInfo().getMessage() != null) {
//					response.setErrorDescription(e.getFaultInfo().getMessage().getValue());
//				}
//			}
//
//		} catch (Exception e) {
//			//TODO inserire eventuali implementazioni specifiche per catch error
////response.setLoginSuccessful(Boolean.FALSE);
//			logger.error("Exception while executing method CheckEmailUserNameDuplicate.", e);
//
//		} finally {
//			response.setTid(response.getTid());
//			response.setSid(response.getSid());
//		}
//
//		return response;
//	}
//	@Override
//	public CheckUserNameDuplicateResponse CheckUserNameDuplicate(CheckUserNameDuplicateRequest request) {
//		if(logger.isDebugEnabled())
//			logger.debug("Executing method CheckUserNameDuplicate. The request is {}", request);
//
//		CheckUserNameDuplicateResponse response = new CheckUserNameDuplicateResponse();
//
//		try {
//
//			CheckUserNameDuplicateRequest CheckUserNameDuplicateRequestParameter = CheckUserNameDuplicateRequestConverter.convert(request);
//			CheckUserNameDuplicateResponse CheckUserNameDuplicateResponseOutput = milleMigliaServiceClient.CheckUserNameDuplicate(CheckUserNameDuplicateRequestParameter);
//			response = CheckUserNameDuplicateResponseConverter.convert(CheckUserNameDuplicateResponseOutput);
//
////TODO inserire eventuali implementazioni specifiche
////response.setLoginSuccessful(Boolean.TRUE);
//
//
//			if(logger.isDebugEnabled())
//				logger.debug("Executed method CheckUserNameDuplicate successfully. The response is {}", response);
//		} catch (IMilleMigliaServiceCheckUserNameDuplicateServiceExceptionFaultFaultMessage e) {
//			logger.error("IMilleMigliaServiceCheckUserNameDuplicateServiceExceptionFaultFaultMessage while executing method CheckUserNameDuplicate.", e);
//
////TODO inserire eventuali implementazioni specifiche
////response.setLoginSuccessful(Boolean.FALSE);
//
//			if (e.getFaultInfo() != null) {
//				// TODO [CheckUserNameDuplicate] apply some logic to decode the error kind
//				if (e.getFaultInfo().getErrorCode() != null) {
//					response.setErrorCode(e.getFaultInfo().getErrorCode().value());
//				}
//				if (e.getFaultInfo().getMessage() != null) {
//					response.setErrorDescription(e.getFaultInfo().getMessage().getValue());
//				}
//			}
//
//		} catch (Exception e) {
//			//TODO inserire eventuali implementazioni specifiche per catch error
////response.setLoginSuccessful(Boolean.FALSE);
//			logger.error("Exception while executing method CheckUserNameDuplicate.", e);
//
//		} finally {
//			response.setTid(response.getTid());
//			response.setSid(response.getSid());
//		}
//
//		return response;
//	}
//	@Override
//	public FindProfileResponse FindProfile(FindProfileRequest request) {
//		if(logger.isDebugEnabled())
//			logger.debug("Executing method FindProfile. The request is {}", request);
//
//		FindProfileResponse response = new FindProfileResponse();
//
//		try {
//
//			FindProfileRequest FindProfileRequestParameter = FindProfileRequestConverter.convert(request);
//			FindProfileResponse FindProfileResponseOutput = milleMigliaServiceClient.FindProfile(FindProfileRequestParameter);
//			response = FindProfileResponseConverter.convert(FindProfileResponseOutput);
//
////TODO inserire eventuali implementazioni specifiche
////response.setLoginSuccessful(Boolean.TRUE);
//
//
//			if(logger.isDebugEnabled())
//				logger.debug("Executed method FindProfile successfully. The response is {}", response);
//		} catch (IMilleMigliaServiceFindProfileServiceExceptionFaultFaultMessage e) {
//			logger.error("IMilleMigliaServiceFindProfileServiceExceptionFaultFaultMessage while executing method FindProfile.", e);
//
////TODO inserire eventuali implementazioni specifiche
////response.setLoginSuccessful(Boolean.FALSE);
//
//			if (e.getFaultInfo() != null) {
//				// TODO [FindProfile] apply some logic to decode the error kind
//				if (e.getFaultInfo().getErrorCode() != null) {
//					response.setErrorCode(e.getFaultInfo().getErrorCode().value());
//				}
//				if (e.getFaultInfo().getMessage() != null) {
//					response.setErrorDescription(e.getFaultInfo().getMessage().getValue());
//				}
//			}
//
//		} catch (Exception e) {
//			//TODO inserire eventuali implementazioni specifiche per catch error
////response.setLoginSuccessful(Boolean.FALSE);
//			logger.error("Exception while executing method FindProfile.", e);
//
//		} finally {
//			response.setTid(response.getTid());
//			response.setSid(response.getSid());
//		}
//
//		return response;
//	}
//	@Override
//	public GetDatiSicurezzaProfiloResponse GetDatiSicurezzaProfilo(GetDatiSicurezzaProfiloRequest request) {
//		if(logger.isDebugEnabled())
//			logger.debug("Executing method GetDatiSicurezzaProfilo. The request is {}", request);
//
//		GetDatiSicurezzaProfiloResponse response = new GetDatiSicurezzaProfiloResponse();
//
//		try {
//
//			GetDatiSicurezzaProfiloRequest GetDatiSicurezzaProfiloRequestParameter = GetDatiSicurezzaProfiloRequestConverter.convert(request);
//			GetDatiSicurezzaProfiloResponse GetDatiSicurezzaProfiloResponseOutput = milleMigliaServiceClient.GetDatiSicurezzaProfilo(GetDatiSicurezzaProfiloRequestParameter);
//			response = GetDatiSicurezzaProfiloResponseConverter.convert(GetDatiSicurezzaProfiloResponseOutput);
//
////TODO inserire eventuali implementazioni specifiche
////response.setLoginSuccessful(Boolean.TRUE);
//
//
//			if(logger.isDebugEnabled())
//				logger.debug("Executed method GetDatiSicurezzaProfilo successfully. The response is {}", response);
//		} catch (IMilleMigliaServiceGetDatiSicurezzaProfiloServiceExceptionFaultFaultMessage e) {
//			logger.error("IMilleMigliaServiceGetDatiSicurezzaProfiloServiceExceptionFaultFaultMessage while executing method GetDatiSicurezzaProfilo.", e);
//
////TODO inserire eventuali implementazioni specifiche
////response.setLoginSuccessful(Boolean.FALSE);
//
//			if (e.getFaultInfo() != null) {
//				// TODO [GetDatiSicurezzaProfilo] apply some logic to decode the error kind
//				if (e.getFaultInfo().getErrorCode() != null) {
//					response.setErrorCode(e.getFaultInfo().getErrorCode().value());
//				}
//				if (e.getFaultInfo().getMessage() != null) {
//					response.setErrorDescription(e.getFaultInfo().getMessage().getValue());
//				}
//			}
//
//		} catch (Exception e) {
//			//TODO inserire eventuali implementazioni specifiche per catch error
////response.setLoginSuccessful(Boolean.FALSE);
//			logger.error("Exception while executing method GetDatiSicurezzaProfilo.", e);
//
//		} finally {
//			response.setTid(response.getTid());
//			response.setSid(response.getSid());
//		}
//
//		return response;
//	}
	@Override
	public GetDatiSicurezzaProfiloOLDResponse GetDatiSicurezzaProfiloOLD(GetDatiSicurezzaProfiloOLDRequest request) {
		if(logger.isDebugEnabled())
			logger.debug("Executing method GetDatiSicurezzaProfiloOLD. The request is {}", request);

		GetDatiSicurezzaProfiloOLDResponse response = new GetDatiSicurezzaProfiloOLDResponse();


		try {

			com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.GetDatiSicurezzaProfiloOLDRequest GetDatiSicurezzaProfiloOLDRequestParameter = getDatiSicurezzaProfiloOLDRequestConverter.convert(request);
			com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.GetDatiSicurezzaProfiloOLDResponse GetDatiSicurezzaProfiloOLDResponseOutput = milleMigliaServiceClient.GetDatiSicurezzaProfiloOLD(GetDatiSicurezzaProfiloOLDRequestParameter);
			response = getDatiSicurezzaProfiloOLDResponseConverter.convert(GetDatiSicurezzaProfiloOLDResponseOutput);


//TODO inserire eventuali implementazioni specifiche
//response.setLoginSuccessful(Boolean.TRUE);


			if(logger.isDebugEnabled())
				logger.debug("Executed method GetDatiSicurezzaProfiloOLD successfully. The response is {}", response);
		} catch (Exception e) {
			logger.error("IMilleMigliaServiceGetDatiSicurezzaProfiloOLDServiceExceptionFaultFaultMessage while executing method GetDatiSicurezzaProfiloOLD.", e);

//TODO inserire eventuali implementazioni specifiche
//response.setLoginSuccessful(Boolean.FALSE);

//			if (e.getFaultInfo() != null) {
				// TODO [GetDatiSicurezzaProfiloOLD] apply some logic to decode the error kind
//				if (e.getFaultInfo().getErrorCode() != null) {
////					response.setErrorCode(e.getFaultInfo().getErrorCode().value());
//				}
//				if (e.getFaultInfo().getMessage() != null) {
////					response.setErrorDescription(e.getFaultInfo().getMessage().getValue());
//				}
			}

//		} catch (Exception e) {
//			//TODO inserire eventuali implementazioni specifiche per catch error
////response.setLoginSuccessful(Boolean.FALSE);
//			logger.error("Exception while executing method GetDatiSicurezzaProfiloOLD.", e);
//
//		} finally {
//			response.setTid(response.getTid());
//			response.setSid(response.getSid());
//		}

		return response;
	}
//	@Override
//	public SetDatiProfiloResponse SetDatiProfilo(SetDatiProfiloRequest request) {
//		if(logger.isDebugEnabled())
//			logger.debug("Executing method SetDatiProfilo. The request is {}", request);
//
//		SetDatiProfiloResponse response = new SetDatiProfiloResponse();
//
//		try {
//
//			SetDatiProfiloRequest SetDatiProfiloRequestParameter = SetDatiProfiloRequestConverter.convert(request);
//			SetDatiProfiloResponse SetDatiProfiloResponseOutput = milleMigliaServiceClient.SetDatiProfilo(SetDatiProfiloRequestParameter);
//			response = SetDatiProfiloResponseConverter.convert(SetDatiProfiloResponseOutput);
//
////TODO inserire eventuali implementazioni specifiche
////response.setLoginSuccessful(Boolean.TRUE);
//
//
//			if(logger.isDebugEnabled())
//				logger.debug("Executed method SetDatiProfilo successfully. The response is {}", response);
//		} catch (IMilleMigliaServiceSetDatiProfiloServiceExceptionFaultFaultMessage e) {
//			logger.error("IMilleMigliaServiceSetDatiProfiloServiceExceptionFaultFaultMessage while executing method SetDatiProfilo.", e);
//
////TODO inserire eventuali implementazioni specifiche
////response.setLoginSuccessful(Boolean.FALSE);
//
//			if (e.getFaultInfo() != null) {
//				// TODO [SetDatiProfilo] apply some logic to decode the error kind
//				if (e.getFaultInfo().getErrorCode() != null) {
//					response.setErrorCode(e.getFaultInfo().getErrorCode().value());
//				}
//				if (e.getFaultInfo().getMessage() != null) {
//					response.setErrorDescription(e.getFaultInfo().getMessage().getValue());
//				}
//			}
//
//		} catch (Exception e) {
//			//TODO inserire eventuali implementazioni specifiche per catch error
////response.setLoginSuccessful(Boolean.FALSE);
//			logger.error("Exception while executing method SetDatiProfilo.", e);
//
//		} finally {
//			response.setTid(response.getTid());
//			response.setSid(response.getSid());
//		}
//
//		return response;
//	}
//	@Override
//	public SetDatiSicurezzaProfiloResponse SetDatiSicurezzaProfilo(SetDatiSicurezzaProfiloRequest request) {
//		if(logger.isDebugEnabled())
//			logger.debug("Executing method SetDatiSicurezzaProfilo. The request is {}", request);
//
//		SetDatiSicurezzaProfiloResponse response = new SetDatiSicurezzaProfiloResponse();
//
//		try {
//
//			SetDatiSicurezzaProfiloRequest SetDatiSicurezzaProfiloRequestParameter = SetDatiSicurezzaProfiloRequestConverter.convert(request);
//			SetDatiSicurezzaProfiloResponse SetDatiSicurezzaProfiloResponseOutput = milleMigliaServiceClient.SetDatiSicurezzaProfilo(SetDatiSicurezzaProfiloRequestParameter);
//			response = SetDatiSicurezzaProfiloResponseConverter.convert(SetDatiSicurezzaProfiloResponseOutput);
//
////TODO inserire eventuali implementazioni specifiche
////response.setLoginSuccessful(Boolean.TRUE);
//
//
//			if(logger.isDebugEnabled())
//				logger.debug("Executed method SetDatiSicurezzaProfilo successfully. The response is {}", response);
//		} catch (IMilleMigliaServiceSetDatiSicurezzaProfiloServiceExceptionFaultFaultMessage e) {
//			logger.error("IMilleMigliaServiceSetDatiSicurezzaProfiloServiceExceptionFaultFaultMessage while executing method SetDatiSicurezzaProfilo.", e);
//
////TODO inserire eventuali implementazioni specifiche
////response.setLoginSuccessful(Boolean.FALSE);
//
//			if (e.getFaultInfo() != null) {
//				// TODO [SetDatiSicurezzaProfilo] apply some logic to decode the error kind
//				if (e.getFaultInfo().getErrorCode() != null) {
//					response.setErrorCode(e.getFaultInfo().getErrorCode().value());
//				}
//				if (e.getFaultInfo().getMessage() != null) {
//					response.setErrorDescription(e.getFaultInfo().getMessage().getValue());
//				}
//			}
//
//		} catch (Exception e) {
//			//TODO inserire eventuali implementazioni specifiche per catch error
////response.setLoginSuccessful(Boolean.FALSE);
//			logger.error("Exception while executing method SetDatiSicurezzaProfilo.", e);
//
//		} finally {
//			response.setTid(response.getTid());
//			response.setSid(response.getSid());
//		}
//
//		return response;
//	}
//	@Override
//	public SetDatiSicurezzaProfiloOLDResponse SetDatiSicurezzaProfiloOLD(SetDatiSicurezzaProfiloOLDRequest request) {
//		if(logger.isDebugEnabled())
//			logger.debug("Executing method SetDatiSicurezzaProfiloOLD. The request is {}", request);
//
//		SetDatiSicurezzaProfiloOLDResponse response = new SetDatiSicurezzaProfiloOLDResponse();
//
//		try {
//
//			SetDatiSicurezzaProfiloOLDRequest SetDatiSicurezzaProfiloOLDRequestParameter = SetDatiSicurezzaProfiloOLDRequestConverter.convert(request);
//			SetDatiSicurezzaProfiloOLDResponse SetDatiSicurezzaProfiloOLDResponseOutput = milleMigliaServiceClient.SetDatiSicurezzaProfiloOLD(SetDatiSicurezzaProfiloOLDRequestParameter);
//			response = SetDatiSicurezzaProfiloOLDResponseConverter.convert(SetDatiSicurezzaProfiloOLDResponseOutput);
//
////TODO inserire eventuali implementazioni specifiche
////response.setLoginSuccessful(Boolean.TRUE);
//
//
//			if(logger.isDebugEnabled())
//				logger.debug("Executed method SetDatiSicurezzaProfiloOLD successfully. The response is {}", response);
//		} catch (IMilleMigliaServiceSetDatiSicurezzaProfiloOLDServiceExceptionFaultFaultMessage e) {
//			logger.error("IMilleMigliaServiceSetDatiSicurezzaProfiloOLDServiceExceptionFaultFaultMessage while executing method SetDatiSicurezzaProfiloOLD.", e);
//
////TODO inserire eventuali implementazioni specifiche
////response.setLoginSuccessful(Boolean.FALSE);
//
//			if (e.getFaultInfo() != null) {
//				// TODO [SetDatiSicurezzaProfiloOLD] apply some logic to decode the error kind
//				if (e.getFaultInfo().getErrorCode() != null) {
//					response.setErrorCode(e.getFaultInfo().getErrorCode().value());
//				}
//				if (e.getFaultInfo().getMessage() != null) {
//					response.setErrorDescription(e.getFaultInfo().getMessage().getValue());
//				}
//			}
//
//		} catch (Exception e) {
//			//TODO inserire eventuali implementazioni specifiche per catch error
////response.setLoginSuccessful(Boolean.FALSE);
//			logger.error("Exception while executing method SetDatiSicurezzaProfiloOLD.", e);
//
//		} finally {
//			response.setTid(response.getTid());
//			response.setSid(response.getSid());
//		}
//
//		return response;
//	}
//	@Override
//	public UpdateSmsAuthorizationResponse UpdateSmsAuthorization(UpdateSmsAuthorizationRequest request) {
//		if(logger.isDebugEnabled())
//			logger.debug("Executing method UpdateSmsAuthorization. The request is {}", request);
//
//		UpdateSmsAuthorizationResponse response = new UpdateSmsAuthorizationResponse();
//
//		try {
//
//			UpdateSmsAuthorizationRequest UpdateSmsAuthorizationRequestParameter = UpdateSmsAuthorizationRequestConverter.convert(request);
//			UpdateSmsAuthorizationResponse UpdateSmsAuthorizationResponseOutput = milleMigliaServiceClient.UpdateSmsAuthorization(UpdateSmsAuthorizationRequestParameter);
//			response = UpdateSmsAuthorizationResponseConverter.convert(UpdateSmsAuthorizationResponseOutput);
//
////TODO inserire eventuali implementazioni specifiche
////response.setLoginSuccessful(Boolean.TRUE);
//
//
//			if(logger.isDebugEnabled())
//				logger.debug("Executed method UpdateSmsAuthorization successfully. The response is {}", response);
//		} catch (IMilleMigliaServiceUpdateSmsAuthorizationServiceExceptionFaultFaultMessage e) {
//			logger.error("IMilleMigliaServiceUpdateSmsAuthorizationServiceExceptionFaultFaultMessage while executing method UpdateSmsAuthorization.", e);
//
////TODO inserire eventuali implementazioni specifiche
////response.setLoginSuccessful(Boolean.FALSE);
//
//			if (e.getFaultInfo() != null) {
//				// TODO [UpdateSmsAuthorization] apply some logic to decode the error kind
//				if (e.getFaultInfo().getErrorCode() != null) {
//					response.setErrorCode(e.getFaultInfo().getErrorCode().value());
//				}
//				if (e.getFaultInfo().getMessage() != null) {
//					response.setErrorDescription(e.getFaultInfo().getMessage().getValue());
//				}
//			}
//
//		} catch (Exception e) {
//			//TODO inserire eventuali implementazioni specifiche per catch error
////response.setLoginSuccessful(Boolean.FALSE);
//			logger.error("Exception while executing method UpdateSmsAuthorization.", e);
//
//		} finally {
//			response.setTid(response.getTid());
//			response.setSid(response.getSid());
//		}
//
//		return response;
//	}

	///////////////////////////////////////

	@Override
	public SetDatiSicurezzaProfiloResponse SetDatiSicurezzaProfilo(SetDatiSicurezzaProfiloRequest request) {
		if(logger.isDebugEnabled())
			logger.debug("Executing method SetDatiSicurezzaProfilo. The request is {}", request);

		SetDatiSicurezzaProfiloResponse response = new SetDatiSicurezzaProfiloResponse();

		try {

//			com.alitalia.aem.common.messages.home.SetDatiSicurezzaProfiloRequest;
//			com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SetDatiSicurezzaProfiloRequest;


			com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SetDatiSicurezzaProfiloRequest
					SetDatiSicurezzaProfiloRequestParameter = setDatiSicurezzaProfiloRequestConverterd.convert(request);

			com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SetDatiSicurezzaProfiloResponse SetDatiSicurezzaProfiloResponseOutput
					= milleMigliaServiceClient.SetDatiSicurezzaProfilo(SetDatiSicurezzaProfiloRequestParameter);

			response = setDatiSicurezzaProfiloResponseConverter.convert(SetDatiSicurezzaProfiloResponseOutput);


//TODO inserire eventuali implementazioni specifiche
			//response.setflagIDProfiloSicurezzaSaved(Boolean.TRUE);

			if(logger.isDebugEnabled())
				logger.debug("Executed method SetDatiSicurezzaProfilo successfully. The response is {}", response);
//		} catch (IMilleMigliaServiceSetDatiSicurezzaProfiloServiceFaultFaultFaultMessage e) {
//			logger.error("IMilleMigliaServiceSetDatiSicurezzaProfiloServiceFaultFaultFaultMessage while executing method SetDatiSicurezzaProfilo.", e);
//
////TODO inserire eventuali implementazioni specifiche
//			response.setflagIDProfiloSicurezzaSaved(Boolean.FALSE);
//
//			logger.error("Exception while executing method subscribeByCustomer().", e);
//			response.setReturnCode(-1);
//			response.setErrorMessage(e.getMessage());
////
////			if (e.getFaultInfo() != null) {
////				// TODO [SetDatiSicurezzaProfilo] apply some logic to decode the error kind
////				if (e.getFaultInfo().getErrorCode() != null) {
////					response.setErrorCode(e.getFaultInfo().getErrorCode().value());
////				}
////				if (e.getFaultInfo().getMessage() != null) {
////					response.setErrorDescription(e.getFaultInfo().getMessage().getValue());
////				}
////			}

		} catch (Exception e) {
			//TODO inserire eventuali implementazioni specifiche per catch error
			response.setflagIDProfiloSicurezzaSaved(Boolean.FALSE);
			logger.error("Exception while executing method SetDatiSicurezzaProfilo.", e);

		} finally {
			response.setTid(response.getTid());
			response.setSid(response.getSid());
		}

		return response;
	}


	@Override
	public GetDatiSicurezzaProfiloResponseLocal GetDatiSicurezzaProfilo(GetDatiSicurezzaProfiloRequestLocal request) {
		if(logger.isDebugEnabled())
			logger.debug("Executing method GetDatiSicurezzaProfilo. The request is {}", request);

		GetDatiSicurezzaProfiloResponseLocal response = new GetDatiSicurezzaProfiloResponseLocal();
		response.setTid(request.getTid());
		response.setSid(request.getSid());

		try {

			GetDatiSicurezzaProfiloRequest getDatiSicurezzaProfiloRequestParameter = getDatiSicurezzaProfiloRequesToWsConverter.convert(request);
			GetDatiSicurezzaProfiloResponse  getDatiSicurezzaProfiloResponseOutput = milleMigliaServiceClient.GetDatiSicurezzaProfilo(getDatiSicurezzaProfiloRequestParameter);
			response = getDatiSicurezzaProfiloResponseToLocalConverter.convert(getDatiSicurezzaProfiloResponseOutput);


//TODO inserire eventuali implementazioni specifiche
			//response.setflagIDProfiloSicurezzaSaved(Boolean.TRUE);

			if(logger.isDebugEnabled())
				logger.debug("Executed method SetDatiSicurezzaProfilo successfully. The response is {}", response);
//		} catch (IMilleMigliaServiceSetDatiSicurezzaProfiloServiceFaultFaultFaultMessage e) {
//			logger.error("IMilleMigliaServiceSetDatiSicurezzaProfiloServiceFaultFaultFaultMessage while executing method SetDatiSicurezzaProfilo.", e);
//
////TODO inserire eventuali implementazioni specifiche
//			response.setflagIDProfiloSicurezzaSaved(Boolean.FALSE);
//
//			logger.error("Exception while executing method subscribeByCustomer().", e);
//			response.setReturnCode(-1);
//			response.setErrorMessage(e.getMessage());
////
////			if (e.getFaultInfo() != null) {
////				// TODO [SetDatiSicurezzaProfilo] apply some logic to decode the error kind
////				if (e.getFaultInfo().getErrorCode() != null) {
////					response.setErrorCode(e.getFaultInfo().getErrorCode().value());
////				}
////				if (e.getFaultInfo().getMessage() != null) {
////					response.setErrorDescription(e.getFaultInfo().getMessage().getValue());
////				}
////			}

		} catch (Exception e) {
			//TODO inserire eventuali implementazioni specifiche per catch error
			logger.error("Exception while executing method SetDatiSicurezzaProfilo.", e);

		} finally {

		}

		return response;
	}



	public FindProfileResponseLocal FindProfile(FindProfileRequestLocal request) {
		if(logger.isDebugEnabled())
			logger.debug("Executing method FindProfile. The request is {}", request);

		FindProfileResponseLocal response = new FindProfileResponseLocal();

		try {

			FindProfileRequest FindProfileRequestParameter = findProfileRequestConverter.convert(request);
			FindProfileResponse FindProfileResponseOutput = milleMigliaServiceClient.FindProfile(FindProfileRequestParameter);
			response = findProfileResponseConverter.convert(FindProfileResponseOutput);

//TODO inserire eventuali implementazioni specifiche
//response.setLoginSuccessful(Boolean.TRUE);


			if(logger.isDebugEnabled())
				logger.debug("Executed method FindProfile successfully. The response is {}", response);
		} catch (IMilleMigliaServiceFindProfileServiceFaultFaultFaultMessage e) {
			logger.error("IMilleMigliaServiceFindProfileServiceExceptionFaultFaultMessage while executing method FindProfile.", e);

			//TODO inserire eventuali implementazioni specifiche
			//response.setLoginSuccessful(Boolean.FALSE);
//			if (e.getFaultInfo() != null) {
//				// TODO [FindProfile] apply some logic to decode the error kind
//				if (e.getFaultInfo().getErrorCode() != null) {
//					response.setErrorCode(e.getFaultInfo().getErrorCode().value());
//				}
//				if (e.getFaultInfo().getMessage() != null) {
//					response.setErrorDescription(e.getFaultInfo().getMessage().getValue());
//				}
//			}

		} catch (Exception e) {
			//TODO inserire eventuali implementazioni specifiche per catch error
//response.setLoginSuccessful(Boolean.FALSE);
			logger.error("Exception while executing method FindProfile.", e);

		} finally {
			response.setTid(response.getTid());
			response.setSid(response.getSid());
		}

		return response;
	}
}