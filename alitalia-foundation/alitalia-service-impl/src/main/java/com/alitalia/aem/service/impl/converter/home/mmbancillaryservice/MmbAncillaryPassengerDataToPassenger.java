package com.alitalia.aem.service.impl.converter.home.mmbancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryEticketData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryPassengerData;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.ArrayOfETicket;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.FrequentFlyer;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.Passenger;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=MmbAncillaryPassengerDataToPassenger.class)
public class MmbAncillaryPassengerDataToPassenger implements Converter<MmbAncillaryPassengerData, Passenger> {

	@Reference
	private MmbAncillaryETicketDataToETicket mmbAncillaryETicketDataConverter;

	@Reference
	private MmbAncillaryFrequentFlyerDataToFrequentFlyer mmbAncillaryFrequentFlyerDataConverter;
	
	@Override
	public Passenger convert(MmbAncillaryPassengerData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		Passenger destination = null;

		if (source != null) {
			destination = objectFactory.createPassenger();

			destination.setX003CIdX003EKBackingField(source.getId());
			destination.setX003CLastNameX003EKBackingField(source.getLastName());
			destination.setX003CNameX003EKBackingField(source.getName());
			destination.setX003CReferenceNumberX003EKBackingField(source.getReferenceNumber());

			ArrayOfETicket tickets = objectFactory.createArrayOfETicket();
			for (MmbAncillaryEticketData sourceETicket : source.getEtickets()) {
				tickets.getETicket().add(mmbAncillaryETicketDataConverter.convert(sourceETicket));
			}
			destination.setX003CETicketsX003EKBackingField(tickets);

			destination.setX003CFrequentFlyerInfoX003EKBackingField(
					mmbAncillaryFrequentFlyerDataConverter.convert(source.getFrequentFlyerInfo()));
		}

		return destination;
	}

}
