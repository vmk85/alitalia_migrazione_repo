package com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.Country;

@Component(immediate=true, metatype=false)
@Service(value=CountryToCountryData.class)
public class CountryToCountryData implements Converter<Country, CountryData> {

	@Override
	public CountryData convert(Country source) {
		CountryData destination = null;

		if (source != null) {
			destination = new CountryData();

			if (source.getX003CCodeX003EKBackingField() != null)
				destination.setCode(source.getX003CCodeX003EKBackingField());
			else
				destination.setCode(source.getX003CCountryCodeX003EKBackingField());

			if(source.getX003CDescriptionX003EKBackingField() != null)
				destination.setDescription(source.getX003CDescriptionX003EKBackingField());
			else
				destination.setDescription(source.getX003CCountryNameX003EKBackingField());

			destination.setStateCode(source.getX003CStateCodeX003EKBackingField());
			destination.setStateDescription(source.getX003CStateDescriptionX003EKBackingField());
		}

		return destination;
	}
}