package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.TicketInfoData;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.TicketInfo;

@Component(immediate = true, metatype = false)
@Service(value = TicketInfoToTicketInfoData.class)
public class TicketInfoToTicketInfoData implements Converter<TicketInfo, TicketInfoData> {

	@Override
	public TicketInfoData convert(TicketInfo source) {
		TicketInfoData destination = new TicketInfoData();

		JAXBElement<String> segmentId = source.getSegmentId();
		if (segmentId != null)
			destination.setSegmentId(segmentId.getValue());

		JAXBElement<String> ticketNumber = source.getTicketNumber();
		if (ticketNumber != null)
			destination.setTicketNumber(ticketNumber.getValue());

		return destination;
	}

}
