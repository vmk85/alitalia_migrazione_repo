package com.alitalia.aem.service.impl.converter.home.commonservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.AuthenticateMilleMigliaUserResponse;
import com.alitalia.aem.ws.booking.commonservice.xsd1.AuthenticateMmUserResponse;

@Component(immediate = true, metatype = false)
@Service(value = AuthenticateMmUserResponseToAuthenticateMilleMigliaUserResponse.class)
public class AuthenticateMmUserResponseToAuthenticateMilleMigliaUserResponse 
		implements Converter<AuthenticateMmUserResponse, AuthenticateMilleMigliaUserResponse> {

	@Reference
	private MmCustomerToMmCustomerData mmCustomerConverter;
	
	@Override
	public AuthenticateMilleMigliaUserResponse convert(AuthenticateMmUserResponse source) {
		
		AuthenticateMilleMigliaUserResponse destination = null;
		
		if (source != null) {
			
			destination = new AuthenticateMilleMigliaUserResponse();
			
			if (source.getMmUser() != null) {
				destination.setCustomer(
						mmCustomerConverter.convert(
							source.getMmUser().getValue()));
			}
			
			destination.setErrorCode(
					source.getErrorCode());
			
		}
		
		return destination;
	}
}