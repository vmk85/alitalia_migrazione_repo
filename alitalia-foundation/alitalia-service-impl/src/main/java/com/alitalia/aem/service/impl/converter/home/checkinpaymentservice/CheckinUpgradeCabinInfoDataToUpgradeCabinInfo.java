package com.alitalia.aem.service.impl.converter.home.checkinpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinUpgradeCabinInfoData;
import com.alitalia.aem.ws.checkin.paymentservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.checkin.paymentservice.xsd2.UpgradeCabinInfo;


@Component(immediate=true, metatype=false)
@Service(value=CheckinUpgradeCabinInfoDataToUpgradeCabinInfo.class)
public class CheckinUpgradeCabinInfoDataToUpgradeCabinInfo implements Converter<CheckinUpgradeCabinInfoData, UpgradeCabinInfo> {
	
	@Reference
	private CheckinUpgradeCabinOrderDataToUpgradeCabinOrder checkinUpgradeCabinOrderDataConverter;
	
	@Reference
	private CheckinPassengerDataToPassenger checkinPassengerDataConverter;
	
	@Reference
	private CheckinPaymentDataToPayment checkinPaymentDataConverter;
	
	@Reference
	private CheckinPaymentReceiptDataToPaymentReceipt checkinPaymentReceiptDataConverter;

	@Override
	public UpgradeCabinInfo convert(CheckinUpgradeCabinInfoData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		UpgradeCabinInfo destination = null;

		if (source != null) {
			destination = objectFactory.createUpgradeCabinInfo();
			
			destination.setX003COrderX003EKBackingField(checkinUpgradeCabinOrderDataConverter.convert(source.getOrder()));
			destination.setX003CPassengerX003EKBackingField(checkinPassengerDataConverter.convert(source.getPassenger()));
			destination.setX003CPaymentInfoX003EKBackingField(checkinPaymentDataConverter.convert(source.getPaymentInfo()));
			destination.setX003CReceiptInfoX003EKBackingField(checkinPaymentReceiptDataConverter.convert(source.getReceiptInfo()));
			
		}

		return destination;
	}

}
