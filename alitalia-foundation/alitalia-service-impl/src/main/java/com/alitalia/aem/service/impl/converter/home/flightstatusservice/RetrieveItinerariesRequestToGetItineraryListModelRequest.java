package com.alitalia.aem.service.impl.converter.home.flightstatusservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.FlightItinerayData;
import com.alitalia.aem.common.messages.home.RetrieveItinerariesRequest;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetItineraryListModelRequest;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.Flight;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveItinerariesRequestToGetItineraryListModelRequest.class)
public class RetrieveItinerariesRequestToGetItineraryListModelRequest implements Converter<RetrieveItinerariesRequest, GetItineraryListModelRequest> {

	@Reference
	private FlightItinerayDataToFlightItinerary flightItineraryConverter;
	
	@Override
	public GetItineraryListModelRequest convert(RetrieveItinerariesRequest source) {
		GetItineraryListModelRequest request = new GetItineraryListModelRequest();
		
		FlightItinerayData flightItinerayData = source.getFlight();
		Flight flight = flightItineraryConverter.convert(flightItinerayData);
		
		ObjectFactory objectFactory = new ObjectFactory();
		JAXBElement<Flight> jaxbFlight = objectFactory.createGetItineraryListModelRequestFlight(flight);
		request.setFlight(jaxbFlight);
		
		return request;
	}
}