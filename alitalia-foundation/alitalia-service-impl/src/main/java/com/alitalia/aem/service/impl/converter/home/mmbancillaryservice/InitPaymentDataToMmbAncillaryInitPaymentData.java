package com.alitalia.aem.service.impl.converter.home.mmbancillaryservice;

import java.util.HashMap;
import java.util.Map;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryInitPaymentData;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.InitPaymentData;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd5.KeyValuePairOfstringstring;

@Component(immediate=true, metatype=false)
@Service(value=InitPaymentDataToMmbAncillaryInitPaymentData.class)
public class InitPaymentDataToMmbAncillaryInitPaymentData implements
		Converter<InitPaymentData, MmbAncillaryInitPaymentData> {

	@Override
	public MmbAncillaryInitPaymentData convert(InitPaymentData source) {
		MmbAncillaryInitPaymentData destination = null;

		if (source != null) {
			destination = new MmbAncillaryInitPaymentData();

			destination.setRequestData(source.getX003CRequestDataX003EKBackingField());
			destination.setRequestMethod(source.getX003CRequestMethodX003EKBackingField());
			destination.setSessionID(source.getX003CSessionIDX003EKBackingField());
			destination.setTransactionID(source.getX003CTransactionIDX003EKBackingField());
			destination.setUrl(source.getX003CUrlX003EKBackingField());

			if (source.getX003CHtmlFormX003EKBackingField() != null &&
					source.getX003CHtmlFormX003EKBackingField().getKeyValuePairOfstringstring() != null &&
					!source.getX003CHtmlFormX003EKBackingField().getKeyValuePairOfstringstring().isEmpty()) {
				Map<String, String> htmlForm = new HashMap<String, String>();
				for( KeyValuePairOfstringstring sourceHtmlFormElem: source.getX003CHtmlFormX003EKBackingField().getKeyValuePairOfstringstring()) {
					htmlForm.put(sourceHtmlFormElem.getKey(), sourceHtmlFormElem.getValue());
				}
				destination.setHtmlForm(htmlForm);
			}
		}

		return destination;
	}

}
