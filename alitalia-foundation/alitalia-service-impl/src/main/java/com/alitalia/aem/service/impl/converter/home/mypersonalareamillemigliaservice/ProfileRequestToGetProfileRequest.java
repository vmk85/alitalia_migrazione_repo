package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.home.ProfileRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.GetProfileRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;

@Component(immediate = true, metatype = false)
@Service(value = ProfileRequestToGetProfileRequest.class)
public class ProfileRequestToGetProfileRequest implements Converter<ProfileRequest, GetProfileRequest> {

	@Reference
	private MMCustomerProfileDataToCustomerProfile customerProfileDataToCustomerProfileConverter;
	
	@Override
	public GetProfileRequest convert(ProfileRequest source) {
		com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory objectFactoryRequest = new com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory();
		
		GetProfileRequest request = objectFactoryRequest.createGetProfileRequest();
		
		MMCustomerProfileData customerProfileData = source.getCustomerProfile();
		CustomerProfile customerProfile = customerProfileDataToCustomerProfileConverter.convert(customerProfileData);
		
		JAXBElement<CustomerProfile> jaxbCustomerProfile = objectFactoryRequest.createCustomerRequestCustomer(customerProfile);
		request.setCustomer(jaxbCustomerProfile);
		
		return request;
	}
}