package com.alitalia.aem.service.impl.converter.home.mmbticketingservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.MmbPreferencesData;
import com.alitalia.aem.common.data.home.mmb.MmbSeatPreferenceData;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd3.ArrayOfanyType;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd4.ObjectFactory;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd4.Preferences;

@Component(immediate=true, metatype=false)
@Service(value=MmbPreferencesDataToPreferences.class)
public class MmbPreferencesDataToPreferences implements Converter<MmbPreferencesData, Preferences> {

	@Reference
	private MealDataToMeal mealDataConverter;

	@Reference
	private SeatTypeDataToSeatType seatTypeDataConverter;

	@Reference
	private MmbSeatPreferenceDataToSeatPreference mmbSeatPreferenceDataConverter;

	@Override
	public Preferences convert(MmbPreferencesData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		com.alitalia.aem.ws.mmb.ticketingservice.xsd3.ObjectFactory objectFactory3 =
				new com.alitalia.aem.ws.mmb.ticketingservice.xsd3.ObjectFactory();
		Preferences destination = null;

		if (source != null) {
			destination = objectFactory.createPreferences();

			destination.setMealType(objectFactory.createPreferencesMealType(
					mealDataConverter.convert(source.getMealType())));

			if (source.getSeatPreferences() != null &&
					!source.getSeatPreferences().isEmpty()) {
				ArrayOfanyType seatPreferencesList = objectFactory3.createArrayOfanyType();
				for (MmbSeatPreferenceData seatPreferenceData : source.getSeatPreferences()) {
					seatPreferencesList.getAnyType().add(mmbSeatPreferenceDataConverter.convert(seatPreferenceData));
				}
				destination.setSeatPreferences(objectFactory.createPreferencesSeatPreferences(seatPreferencesList));
			}
			destination.setSeatType(objectFactory.createPreferencesSeatType(
					seatTypeDataConverter.convert(source.getSeatType())));
		}

		return destination;
	}

}
