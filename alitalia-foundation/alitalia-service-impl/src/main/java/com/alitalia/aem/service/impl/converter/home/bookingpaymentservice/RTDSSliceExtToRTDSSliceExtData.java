package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceExtData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionSliceExt;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceExtToRTDSSliceExtData.class)
public class RTDSSliceExtToRTDSSliceExtData implements
		Converter<ResultTicketingDetailSolutionSliceExt, ResultTicketingDetailSolutionSliceExtData> {

	@Reference
	private RTDSSliceExtWarningToRTDSSliceExtWarningData warningFieldConverter;

	@Override
	public ResultTicketingDetailSolutionSliceExtData convert(ResultTicketingDetailSolutionSliceExt source) {
		ResultTicketingDetailSolutionSliceExtData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionSliceExtData();

			destination.setWarningField(
					warningFieldConverter.convert(source.getWarningField()));
		}

		return destination;
	}

}
