package com.alitalia.aem.service.impl.converter.home.mmbinsuranceservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.InsuranceResponse;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd1.InsuranceInfoResponse;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd2.InsurancePolicy;

@Component(immediate=true, metatype=false)
@Service(value=InsuranceInfoResponseToInsuranceResponse.class)
public class InsuranceInfoResponseToInsuranceResponse implements Converter<InsuranceInfoResponse, InsuranceResponse> {

	@Reference
	private InsurancePolicyToInsurancePolicyData insurancePolicyConverter;

	@Override
	public InsuranceResponse convert(InsuranceInfoResponse source) {
		InsuranceResponse destination = new InsuranceResponse();

		JAXBElement<InsurancePolicy> insurancePolicy = source.getInsurancePolicy();
		if (insurancePolicy != null && insurancePolicy.getValue() != null)
			destination.setPolicy(insurancePolicyConverter.convert(insurancePolicy.getValue()));
		else
			destination.setPolicy(null);
		
		return destination;
	}

}
