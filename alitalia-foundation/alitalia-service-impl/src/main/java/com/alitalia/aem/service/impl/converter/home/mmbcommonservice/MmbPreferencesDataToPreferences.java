package com.alitalia.aem.service.impl.converter.home.mmbcommonservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.MmbPreferencesData;
import com.alitalia.aem.common.data.home.mmb.MmbSeatPreferenceData;
import com.alitalia.aem.ws.mmb.commonservice.xsd2.ArrayOfanyType;
import com.alitalia.aem.ws.mmb.commonservice.xsd4.ObjectFactory;
import com.alitalia.aem.ws.mmb.commonservice.xsd4.Preferences;

@Component(immediate=true, metatype=false)
@Service(value=MmbPreferencesDataToPreferences.class)
public class MmbPreferencesDataToPreferences implements Converter<MmbPreferencesData, Preferences> {

	@Reference
	private MealDataToMeal mealDataConverter;

	@Reference
	private SeatTypeDataToSeatType seatTypeDataConverter;

	@Reference
	private MmbSeatPreferenceDataToSeatPreference mmbSeatPreferenceDataConverter;

	@Override
	public Preferences convert(MmbPreferencesData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		com.alitalia.aem.ws.mmb.commonservice.xsd2.ObjectFactory objectFactory2 =
				new com.alitalia.aem.ws.mmb.commonservice.xsd2.ObjectFactory();
		Preferences destination = null;

		if (source != null) {
			destination = objectFactory.createPreferences();

			destination.setMealType(objectFactory.createPreferencesMealType(
					mealDataConverter.convert(source.getMealType())));

			if (source.getSeatPreferences() != null &&
					!source.getSeatPreferences().isEmpty()) 
			{
				ArrayOfanyType seatPreferencesList = objectFactory2.createArrayOfanyType();
				for (MmbSeatPreferenceData seatPreferenceData : source.getSeatPreferences()) {
					seatPreferencesList.getAnyType().add(
							mmbSeatPreferenceDataConverter.convert(seatPreferenceData));
				}
				destination.setSeatPreferences(objectFactory.createPreferencesSeatPreferences(seatPreferencesList));
			}

			destination.setSeatType(objectFactory.createPreferencesSeatType(
					seatTypeDataConverter.convert(source.getSeatType())));
		}

		return destination;
	}

}
