package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentDepartureData;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ResultBookingDetailsSolutionPricingBookingInfoSegmentDeparture;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingBookingInfoSegmentDepartureToPricingBookingInfoSegmentDepartureDataConverter.class)
public class RBDSPricingBookingInfoSegmentDepartureToPricingBookingInfoSegmentDepartureDataConverter
		implements Converter<ResultBookingDetailsSolutionPricingBookingInfoSegmentDeparture, ResultBookingDetailsSolutionPricingBookingInfoSegmentDepartureData> {

	@Override
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentDepartureData convert(
			ResultBookingDetailsSolutionPricingBookingInfoSegmentDeparture source) {
		ResultBookingDetailsSolutionPricingBookingInfoSegmentDepartureData destination = null;
		
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingBookingInfoSegmentDepartureData();
			destination.setDateField(source.getDateField());
			destination.setTimeField(source.getTimeField());
		}
		
		return destination;
		
	}
	
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentDepartureData convert(
			com.alitalia.aem.ws.booking.searchservice.xsd14.ResultBookingDetailsSolutionPricingBookingInfoSegmentDeparture source) {
		ResultBookingDetailsSolutionPricingBookingInfoSegmentDepartureData destination = null;
		
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingBookingInfoSegmentDepartureData();
			destination.setDateField(source.getDateField());
			destination.setTimeField(source.getTimeField());
		}
		
		return destination;
		
	}
}
