package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMActivityData;
import com.alitalia.aem.common.data.home.MMAddressData;
import com.alitalia.aem.common.data.home.MMContractData;
import com.alitalia.aem.common.data.home.MMCreditCardData;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.data.home.MMFlightData;
import com.alitalia.aem.common.data.home.MMLifestyleData;
import com.alitalia.aem.common.data.home.MMMessageData;
import com.alitalia.aem.common.data.home.MMPreferenceData;
import com.alitalia.aem.common.data.home.MMProgramData;
import com.alitalia.aem.common.data.home.MMTelephoneData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Activity;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Address;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.AddressTypes;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Contract;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CreditCard;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Flight;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.GenderTypes;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Lifestyle;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.MaritalStatusTypes;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Message;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Preference;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Program;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.RegistrationTypes;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.SystemChannelTypes;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Telephone;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.TierCodeTypes;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.TitleTypes;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.WorkPositionTypes;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd3.ArrayOfanyType;

@Component(immediate = true, metatype = false)
@Service(value = MMCustomerProfileDataToCustomerProfile.class)
public class MMCustomerProfileDataToCustomerProfile implements Converter<MMCustomerProfileData, CustomerProfile> {

	@Reference
	private MMActivityDataToActivity activityDataToActivityConverter;

	@Reference
	private MMProgramDataToProgram programDataToProgramConverter;

	@Reference
	private MMTelephoneDataToTelephone telephoneDataToTelephoneConverter;

	@Reference
	private MMPreferenceDataToPreference preferenceDataToPreferenceConverter;

	@Reference
	private MMMessageDataToMessage messageDataToMessageConverter;

	@Reference
	private MMLifestyleDataToLifestyle lifestyleDataToLifestyleConverter;

	@Reference
	private MMFlightDataToFlight flightDataToFlightConverter;

	@Reference
	private MMAddressDataToAddress addressDataToAddressConverter;

	@Reference
	private MMCreditCardDataToCreditCard creditCardDataToCreditCardConverter;

	@Reference
	private MMContractDataToContract contractDataToContractConverter;

	@Override
	public CustomerProfile convert(MMCustomerProfileData source) {
		CustomerProfile destination = null;
		
		if (source != null) {
			com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ObjectFactory objectFactory2 = new com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ObjectFactory();
			com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd3.ObjectFactory objectFactory3 = new com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd3.ObjectFactory();

			destination = objectFactory2.createCustomerProfile();
			destination.setContratcType(objectFactory2.createCustomerProfileContratcType(source.getContratcType()));
			destination.setEmail(objectFactory2.createCustomerProfileEmail(source.getEmail()));
			destination.setFiscalCode(objectFactory2.createCustomerProfileFiscalCode(source.getFiscalCode()));
			destination.setLanguage(objectFactory2.createCustomerProfileLanguage(source.getLanguage()));
			destination.setMailingType(objectFactory2.createCustomerProfileMailingType(source.getMailingType()));
			destination.setMarket(objectFactory2.createCustomerProfileMarket(source.getMarket()));
			destination.setDefaultAddressCountry(objectFactory2.createCustomerProfileDefaultAddressCountry(source
					.getDefaultAddressCountry()));
			destination.setDefaultAddressMunicipalityName(objectFactory2
					.createCustomerProfileDefaultAddressMunicipalityName(source.getDefaultAddressMunicipalityName()));
			destination.setDefaultAddressPostalCode(objectFactory2.createCustomerProfileDefaultAddressPostalCode(source
					.getDefaultAddressPostalCode()));
			destination.setDefaultAddressStateCode(objectFactory2.createCustomerProfileDefaultAddressStateCode(source
					.getDefaultAddressStateCode()));
			destination.setDefaultAddressStreetFreeText(objectFactory2
					.createCustomerProfileDefaultAddressStreetFreeText(source.getDefaultAddressStreetFreeText()));
			destination.setCustomerName(objectFactory2.createCustomerProfileCustomerName(source.getCustomerName()));
			destination.setCustomerNickName(objectFactory2.createCustomerProfileCustomerNickName(source
					.getCustomerNickName()));
			destination
					.setCustomerNumber(objectFactory2.createCustomerProfileCustomerNumber(source.getCustomerNumber()));
			destination.setCustomerPinCode(objectFactory2.createCustomerProfileCustomerPinCode(source
					.getCustomerPinCode()));
			destination.setCustomerSurname(objectFactory2.createCustomerProfileCustomerSurname(source
					.getCustomerSurname()));



			destination.setContractAgree(source.getContractAgree());
			destination.setPointsEarnedPartial(source.getPointsEarnedPartial());
			destination.setPointsEarnedTotal(source.getPointsEarnedTotal());
			destination.setPointsEarnedTotalQualified(source.getPointsEarnedTotalQualified());
			destination.setPointsRemainingPartial(source.getPointsRemainingPartial());
			destination.setPointsRemainingTotal(source.getPointsRemainingTotal());
			destination.setPointsSpentPartial(source.getPointsSpentPartial());
			destination.setPointsSpentTotal(source.getPointsSpentTotal());
			destination.setProfiling(source.getProfiling());
			destination.setQualifyingSegment(source.getQualifyingSegment());
			destination.setSmsAuthorization(source.getSmsAuthorization());

			destination.setUserIdRequest(objectFactory2.createCustomerProfileUserIdRequest(source.getUserIdRequest()));
			destination
					.setWebPageRequest(objectFactory2.createCustomerProfileWebPageRequest(source.getWebPageRequest()));
			destination.setStatusCode(objectFactory2.createCustomerProfileStatusCode(source.getStatusCode()));
			destination.setProfessionalName(objectFactory2.createCustomerProfileProfessionalName(source
					.getProfessionalName()));

			if (source.getPassportDate() != null)
				destination.setPassportDate(XsdConvertUtils.toXMLGregorianCalendar(source.getPassportDate()));
			if (source.getBirthDate() != null)
				destination.setBirthDate(XsdConvertUtils.toXMLGregorianCalendar(source.getBirthDate()));
			if (source.getTierDate() != null)
				destination.setTierDate(XsdConvertUtils.toXMLGregorianCalendar(source.getTierDate()));

			destination.setGender(source.getGender() != null ? GenderTypes.fromValue(source.getGender().value()) : null);
			destination.setSystemChannel(source.getSystemChannel() != null ? SystemChannelTypes.fromValue(source.getSystemChannel().value()) : null);
			destination.setTierCode(source.getTierCode() != null ? TierCodeTypes.fromValue(source.getTierCode().value()) : null);
			destination.setRegistrationRequired(source.getRegistrationRequired() != null ? RegistrationTypes.fromValue(source.getRegistrationRequired().value()) : null);
			destination.setMaritalStatus(source.getMaritalStatus() != null ? MaritalStatusTypes.fromValue(source.getMaritalStatus().value()) : null);
			destination.setCustomerTitle(source.getCustomerTitle() != null ? TitleTypes.fromValue(source.getCustomerTitle().value()) : null);
			destination.setCustomerWorkPosition(source.getCustomerWorkPosition() != null ? WorkPositionTypes.fromValue(source.getCustomerWorkPosition().value()) : null);
			destination.setDefaultAddressType(source.getDefaultAddressType() != null ? AddressTypes.fromValue(source.getDefaultAddressType().value()) : null);

			List<MMActivityData> sourceActivityData = source.getActivities();
			ArrayOfanyType activitiesList = objectFactory3.createArrayOfanyType();
			if (sourceActivityData != null) {
				for (MMActivityData mmActivityData : sourceActivityData) {
					Activity activity = activityDataToActivityConverter.convert(mmActivityData);
					activitiesList.getAnyType().add(activity);
				}
			}
			destination.setActivities(objectFactory2.createCustomerProfileActivities(activitiesList));

			List<MMProgramData> sourceProgramData = source.getPrograms();
			ArrayOfanyType programsList = objectFactory3.createArrayOfanyType();
			if (sourceProgramData != null) {
				for (MMProgramData mmProgramData : sourceProgramData) {
					Program program = programDataToProgramConverter.convert(mmProgramData);
					programsList.getAnyType().add(program);
				}
			}
			destination.setPrograms(objectFactory2.createCustomerProfilePrograms(programsList));

			List<MMTelephoneData> sourceTelephoneData = source.getTelephones();
			ArrayOfanyType telephonesList = objectFactory3.createArrayOfanyType();
			if (sourceTelephoneData != null) {
				for (MMTelephoneData mmTelephoneData : sourceTelephoneData) {
					if(mmTelephoneData != null){
						Telephone telephone = telephoneDataToTelephoneConverter.convert(mmTelephoneData);
						telephonesList.getAnyType().add(telephone);
					}
				}
			}
			destination.setTelephones(objectFactory2.createCustomerProfileTelephones(telephonesList));

			List<MMPreferenceData> sourcePreferenceData = source.getPreferences();
			ArrayOfanyType preferencesList = objectFactory3.createArrayOfanyType();
			if (sourcePreferenceData != null) {
				for (MMPreferenceData mmPreferenceData : sourcePreferenceData) {
					Preference preference = preferenceDataToPreferenceConverter.convert(mmPreferenceData);
					preferencesList.getAnyType().add(preference);
				}
			}
			destination.setPreferences(objectFactory2.createCustomerProfilePreferences(preferencesList));

			List<MMMessageData> sourceMessageData = source.getMessages();
			ArrayOfanyType messagesList = objectFactory3.createArrayOfanyType();
			if (sourceMessageData != null) {
				for (MMMessageData mmMessageData : sourceMessageData) {
					Message message = messageDataToMessageConverter.convert(mmMessageData);
					messagesList.getAnyType().add(message);
				}
			}
			destination.setMessages(objectFactory2.createCustomerProfileMessages(messagesList));

			List<MMLifestyleData> sourceLifestyleData = source.getLifestyles();
			ArrayOfanyType lifestylesList = objectFactory3.createArrayOfanyType();
			if (sourceLifestyleData != null) {
				for (MMLifestyleData mmLifestyleData : sourceLifestyleData) {
					Lifestyle lifestyle = lifestyleDataToLifestyleConverter.convert(mmLifestyleData);
					lifestylesList.getAnyType().add(lifestyle);
				}
			}
			destination.setLifestyles(objectFactory2.createCustomerProfileLifestyles(lifestylesList));

			List<MMFlightData> sourceFlightData = source.getFlight();
			ArrayOfanyType flightsList = objectFactory3.createArrayOfanyType();
			if (sourceFlightData != null) {
				for (MMFlightData mmFlightData : sourceFlightData) {
					Flight flight = flightDataToFlightConverter.convert(mmFlightData);
					flightsList.getAnyType().add(flight);
				}
			}
			destination.setFlights(objectFactory2.createCustomerProfileFlights(flightsList));

			List<MMAddressData> sourceAddressData = source.getAddresses();
			ArrayOfanyType addressesList = objectFactory3.createArrayOfanyType();
			if (sourceAddressData != null) {
				for (MMAddressData mmAddressData : sourceAddressData) {
					Address address = addressDataToAddressConverter.convert(mmAddressData);
					addressesList.getAnyType().add(address);
				}
			}
			destination.setAddresses(objectFactory2.createCustomerProfileAddresses(addressesList));

			List<MMCreditCardData> sourceCreditCardData = source.getCreditCards();
			ArrayOfanyType creditCardsList = objectFactory3.createArrayOfanyType();
			if (sourceCreditCardData != null) {
				for (MMCreditCardData mmCreditCardData : sourceCreditCardData) {
					CreditCard creditCard = creditCardDataToCreditCardConverter.convert(mmCreditCardData);
					creditCardsList.getAnyType().add(creditCard);
				}
			}
			destination.setCreditCards(objectFactory2.createCustomerProfileCreditCards(creditCardsList));

			List<MMContractData> sourceContractData = source.getContracts();
			ArrayOfanyType contractsList = objectFactory3.createArrayOfanyType();
			if (sourceContractData != null) {
				for (MMContractData mmContractData : sourceContractData) {
					Contract contract = contractDataToContractConverter.convert(mmContractData);
					contractsList.getAnyType().add(contract);
				}
			}
			destination.setContracts(objectFactory2.createCustomerProfileContracts(contractsList));

		}

		return destination;
	}

}
