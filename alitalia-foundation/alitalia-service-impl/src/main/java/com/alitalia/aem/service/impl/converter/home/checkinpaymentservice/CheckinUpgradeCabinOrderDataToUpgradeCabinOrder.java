package com.alitalia.aem.service.impl.converter.home.checkinpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinUpgradeCabinOrderData;
import com.alitalia.aem.ws.checkin.paymentservice.xsd2.CompartimentalClass;
import com.alitalia.aem.ws.checkin.paymentservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.checkin.paymentservice.xsd2.UpgradeCabinOrder;


@Component(immediate=true, metatype=false)
@Service(value=CheckinUpgradeCabinOrderDataToUpgradeCabinOrder.class)
public class CheckinUpgradeCabinOrderDataToUpgradeCabinOrder implements Converter<CheckinUpgradeCabinOrderData, UpgradeCabinOrder> {

	@Reference
	private CheckinFlightDataToFlight checkinFlightDataConverter;

	@Override
	public UpgradeCabinOrder convert(CheckinUpgradeCabinOrderData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		UpgradeCabinOrder destination = null;

		if (source != null) {
			destination = objectFactory.createUpgradeCabinOrder();

			destination.setX003CIdX003EKBackingField(source.getId());
			
			if(source.getCabin() != null){
				destination.setX003CCabinX003EKBackingField(CompartimentalClass.fromValue(source.getCabin().value()));
			}
			destination.setX003CFlightX003EKBackingField(checkinFlightDataConverter.convert(source.getFlight()));
			destination.setX003CSeatX003EKBackingField(source.getSeat());
			destination.setX003CTicketNumberX003EKBackingField(source.getTicketNumber());
			
		}

		return destination;
	}

}
