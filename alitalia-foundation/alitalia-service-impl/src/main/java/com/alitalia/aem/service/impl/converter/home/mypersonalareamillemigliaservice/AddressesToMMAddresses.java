package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMAddressData;
import com.alitalia.aem.common.data.home.enumerations.MMAddressTypeEnum;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Address;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd3.ArrayOfanyType;

@Component(immediate = true, metatype = false)
@Service(value = AddressesToMMAddresses.class)
public class AddressesToMMAddresses implements Converter<ArrayOfanyType, List<MMAddressData>> {

	private static Logger logger = LoggerFactory.getLogger(AddressesToMMAddresses.class);
	
	@Reference
	private MyPersonalareaMillemigliaServiceJAXBContextFactory jaxbContextFactory;
	
	@Override
	public List<MMAddressData> convert(ArrayOfanyType source) {
		
		if (source == null) {
			return null;
		}
		
		List<MMAddressData> mmAddresses = new ArrayList<MMAddressData>();

		for (Object object: source.getAnyType()) {
			try {
				Unmarshaller unmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
				
				JAXBElement<Address> jaxbElement = (JAXBElement<Address>) unmarshaller.unmarshal((Node) object);
				Address address = jaxbElement.getValue();
				
				MMAddressData mmAddressData = new MMAddressData();
				mmAddressData.setAddressType(MMAddressTypeEnum.fromValue(address.getAddressType().value()));
				if(address.getCompanyName() != null)
					mmAddressData.setCompanyName(address.getCompanyName().getValue());
				mmAddressData.setCountry(address.getCountry().getValue());
				mmAddressData.setInvalidIndicator(address.getInvalidIndicator().getValue());
				mmAddressData.setIstruction(address.getIstruction().getValue());
				mmAddressData.setMailingIndicator(address.getMailingIndicator().getValue());
				mmAddressData.setMunicipalityName(address.getMunicipalityName().getValue());
				mmAddressData.setPostalCode(address.getPostalCode().getValue());
				mmAddressData.setStateCode(address.getStateCode().getValue());
				mmAddressData.setStreetFreeText(address.getStreetFreeText().getValue());

				if (address.getStartDate() != null)
					mmAddressData.setStartDate(XsdConvertUtils.parseCalendar(address.getStartDate()));
				if (address.getEndDate() != null)
					mmAddressData.setEndDate(XsdConvertUtils.parseCalendar(address.getEndDate()));
				
				mmAddresses.add(mmAddressData);
			
			} catch (JAXBException e) {
				logger.error("Error while creating JAXBConext/Unmarshller for List<MMAddressData>: {}", e);
			}
		}
		
		return mmAddresses;
	}
}