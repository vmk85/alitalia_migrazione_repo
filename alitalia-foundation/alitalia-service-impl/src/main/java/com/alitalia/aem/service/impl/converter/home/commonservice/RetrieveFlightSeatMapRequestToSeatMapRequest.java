package com.alitalia.aem.service.impl.converter.home.commonservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveFlightSeatMapRequest;
import com.alitalia.aem.ws.booking.commonservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.booking.commonservice.xsd1.SeatMapRequest;
import com.alitalia.aem.ws.booking.commonservice.xsd3.ArrayOfstring;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveFlightSeatMapRequestToSeatMapRequest.class)
public class RetrieveFlightSeatMapRequestToSeatMapRequest 
		implements Converter<RetrieveFlightSeatMapRequest, SeatMapRequest> {
	
	@Reference
	private DirectFlightDataToDirectFlight directFlightDataToDirectFlight;
	
	@Override
	public SeatMapRequest convert(RetrieveFlightSeatMapRequest source) {
		
		SeatMapRequest destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			com.alitalia.aem.ws.booking.commonservice.xsd3.ObjectFactory objectFactory3 = 
					new com.alitalia.aem.ws.booking.commonservice.xsd3.ObjectFactory();
			
			destination = new SeatMapRequest();
			
			ArrayOfstring compartimentalClass = objectFactory3.createArrayOfstring();
			if (source.getCompartimentalClass() != null) {
				for (String compartimentalClassItem : source.getCompartimentalClass()) {
					compartimentalClass.getString().add(compartimentalClassItem);
				}
			}
			destination.setCompartimentalClass(
					objectFactory.createSeatMapRequestCompartimentalClass(
							compartimentalClass));
			
			destination.setFlightInfo(
					objectFactory.createSeatMapRequestFlightInfo(
							directFlightDataToDirectFlight.convert(
									source.getFlightInfo())));
			
			destination.setCookie(
					objectFactory.createSeatMapRequestCookie(
							source.getCookie()));
			
			destination.setExecution(
					objectFactory.createSeatMapRequestExecution(
							source.getExecution()));
			
			destination.setSabreGateWayAuthToken(
					objectFactory.createSeatMapRequestSabreGateWayAuthToken(
							source.getSabreGateWayAuthToken()));
		
		}
		
		return destination;
		
	}
	
}