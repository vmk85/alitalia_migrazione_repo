package com.alitalia.aem.service.impl.converter.home.mmbinsuranceservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.data.home.mmb.MmbAirportData;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd2.AreaValue;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd3.Airport;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd3.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=MmbAirportDataToAirport.class)
public class MmbAirportDataToAirport implements Converter<MmbAirportData, Airport> {

	@Override
	public Airport convert(MmbAirportData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		Airport destination = null;

		if (source != null) {
			destination = objectFactory.createAirport();

			destination.setX003CAirportNameX003EKBackingField(source.getName());

			AreaValueEnum sourceArea = source.getArea();
			if (sourceArea != null)
				destination.setX003CAreaX003EKBackingField(AreaValue.fromValue(sourceArea.value()));

			destination.setX003CCityNameX003EKBackingField(source.getCity());
			destination.setX003CCityX003EKBackingField(source.getCityCode());
			destination.setX003CCodeX003EKBackingField(source.getCode());
			destination.setX003CCountryCodeX003EKBackingField(source.getCountryCode());
			destination.setX003CIdX003EKBackingField(source.getId());
			destination.setX003CNameX003EKBackingField(source.getCountry());
		}

		return destination;
	}

}
