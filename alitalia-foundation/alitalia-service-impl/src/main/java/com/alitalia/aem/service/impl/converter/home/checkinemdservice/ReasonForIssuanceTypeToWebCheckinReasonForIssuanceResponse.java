package com.alitalia.aem.service.impl.converter.home.checkinemdservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.WebCheckinReasonForIssuanceResponse;
import com.alitalia.aem.ws.checkin.emdservice.xsd2.ReasonForIssuanceType;


@Component(immediate=true, metatype=false)
@Service(value=ReasonForIssuanceTypeToWebCheckinReasonForIssuanceResponse.class)
public class ReasonForIssuanceTypeToWebCheckinReasonForIssuanceResponse implements Converter<ReasonForIssuanceType, WebCheckinReasonForIssuanceResponse> {

	@Reference
	private ReasonForIssuanceTypeToCheckinReasonForIssuance reasonForIssuanceConverter;
	
	@Override
	public WebCheckinReasonForIssuanceResponse convert(ReasonForIssuanceType source) {
		WebCheckinReasonForIssuanceResponse destination = null;

		if (source != null) {
			destination = new WebCheckinReasonForIssuanceResponse();
			destination.setCheckinReasonForIssuance(reasonForIssuanceConverter.convert(source));
		}

		return destination;
	}

}
