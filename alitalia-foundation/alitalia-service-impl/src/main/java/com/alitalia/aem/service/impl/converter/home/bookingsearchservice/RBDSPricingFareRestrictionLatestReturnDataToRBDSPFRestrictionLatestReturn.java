package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareRestrictionLatestReturnData;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ResultBookingDetailsSolutionPricingFareRestrictionLatestReturn;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingFareRestrictionLatestReturnDataToRBDSPFRestrictionLatestReturn.class)
public class RBDSPricingFareRestrictionLatestReturnDataToRBDSPFRestrictionLatestReturn
		implements Converter<ResultBookingDetailsSolutionPricingFareRestrictionLatestReturnData, ResultBookingDetailsSolutionPricingFareRestrictionLatestReturn> {

	@Override
	public ResultBookingDetailsSolutionPricingFareRestrictionLatestReturn convert(
			ResultBookingDetailsSolutionPricingFareRestrictionLatestReturnData source) {
		ResultBookingDetailsSolutionPricingFareRestrictionLatestReturn destination = null;
		
		if(source!=null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionPricingFareRestrictionLatestReturn();
			destination.setDateField(source.getDateField());
			destination.setTimeField(source.getTimeField());
		}
		
		return destination;
	}

}
