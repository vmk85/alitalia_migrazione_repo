package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMCreditCardData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CardTypes;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CreditCard;

@Component(immediate = true, metatype = false)
@Service(value = MMCreditCardDataToCreditCard.class)
public class MMCreditCardDataToCreditCard implements Converter<MMCreditCardData, CreditCard> {

	@Override
	public CreditCard convert(MMCreditCardData source) {
		CreditCard destination = null;

		if (source != null) {
			com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ObjectFactory objectFactory2 = new com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ObjectFactory();

			destination = objectFactory2.createCreditCard();
			destination.setCardType(CardTypes.fromValue(source.getCardType().value()));
			destination.setCircuitCode(objectFactory2.createCreditCardCircuitCode(source.getCircuitCode()));
			destination.setNumber(objectFactory2.createCreditCardNumber(source.getNumber()));
			destination.setToken(objectFactory2.createCreditCardToken(source.getToken()));

			if (source.getExpireDate() != null)
				destination.setExpireDate(XsdConvertUtils.toXMLGregorianCalendar(source.getExpireDate()));
		}

		return destination;
	}

}
