package com.alitalia.aem.service.impl.converter.home.mmbticketingservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.CodeDescriptionData;
import com.alitalia.aem.common.data.home.CrossSellingBaseData;
import com.alitalia.aem.common.data.home.CrossSellingsData;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd4.ACrossSellingDetail;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd4.CodeDescription;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd4.CrossSelling;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd4.CrossSellingBase;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd4.CrossSellings;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd4.HotelCrossSelling;

@Component(immediate = true, metatype = false)
@Service(value = CrossSellingsToCrossSellingsData.class)
public class CrossSellingsToCrossSellingsData implements Converter<CrossSellings, CrossSellingsData> {

	@Reference
	private CodeDescriptionToCodeDescriptionData codeDescriptionToCodeDescriptionDataConverter;
	
	@Reference
	private CrossSellingBaseToCrossSellingBaseData crossSellingBaseToCrossSellingBaseDataConverter;

	@Reference
	private ACrossSellingDetailToCrossSellingDetailData aCrossSellingDetailToCrossSellingDetailDataConverter;

	@Reference
	private CrossSellingToCrossSellingData crossSellingToCrossSellingDataConverter;

	@Reference
	private HotelCrossSellingToHotelCrossSellingData hotelCrossSellingToHotelCrossSellingDataConverter;
	
	@Reference
	private MmbTicketingServiceJAXBContextFactory jaxbContextFactory;

	@Override
	public CrossSellingsData convert(CrossSellings source) {
		CrossSellingsData destination = null;

		if (source != null) {
			destination = new CrossSellingsData();

			List<CodeDescriptionData> crossSellingInformationDatas = new ArrayList<CodeDescriptionData>();
			if (source.getCrossSellingInformations() != null && 
					source.getCrossSellingInformations().getValue() != null) {
				for (Object object : source.getCrossSellingInformations().getValue().getAnyType()) {
					crossSellingInformationDatas.add(codeDescriptionToCodeDescriptionDataConverter.convert((CodeDescription)object));
				}
			}
			destination.setCrossSellingInformations(crossSellingInformationDatas);
			
			List<CrossSellingBaseData> items = null;
			if (source.getItems() != null && 
					source.getItems().getValue() != null &&
					source.getItems().getValue().getAnyType() != null &&
					!source.getItems().getValue().getAnyType().isEmpty()) {
				List<Object> sourceItems = source.getItems().getValue().getAnyType();
				items = new ArrayList<CrossSellingBaseData>();
				for (Object object : sourceItems) {
					if (object instanceof CrossSellingBase)
						items.add(crossSellingBaseToCrossSellingBaseDataConverter.convert((CrossSellingBase)object));
					else if (object instanceof ACrossSellingDetail)
						items.add(aCrossSellingDetailToCrossSellingDetailDataConverter.convert((ACrossSellingDetail) object));
					else if (object instanceof CrossSelling)
						items.add(crossSellingToCrossSellingDataConverter.convert((CrossSelling) object));
					else if (object instanceof HotelCrossSelling)
						items.add(hotelCrossSellingToHotelCrossSellingDataConverter.convert((HotelCrossSelling) object));
					else {
						try {
							XPath xpath = XPathFactory.newInstance().newXPath();
							XPathExpression expr = xpath.compile("/*[local-name()='anyType']/@*[local-name()='type']");
			
							Unmarshaller crossSellingBaseUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			
							Unmarshaller aCrossSellingDetailUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			
							Unmarshaller crossSellingUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			
							Unmarshaller hotelCrossSellingUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			
							String crossSellingInstanceType = (String) expr.evaluate((Node) object, XPathConstants.STRING);
			
							if ("CrossSellingBase".equals(crossSellingInstanceType)) {
								@SuppressWarnings("unchecked")
								CrossSellingBase castedSource = (CrossSellingBase) 
										((JAXBElement<CrossSellingBase>) crossSellingBaseUnmarshaller.unmarshal((Node) object)).getValue();
								items.add(crossSellingBaseToCrossSellingBaseDataConverter.convert(castedSource));
							
							} else if ("ACrossSellingDetail".equals(crossSellingInstanceType)) {
								@SuppressWarnings("unchecked")
								ACrossSellingDetail castedSource = (ACrossSellingDetail) 
										((JAXBElement<ACrossSellingDetail>) aCrossSellingDetailUnmarshaller.unmarshal((Node) object)).getValue();
								items.add(aCrossSellingDetailToCrossSellingDetailDataConverter.convert(castedSource));
							
							} else if ("CrossSelling".equals(crossSellingInstanceType)) {
								@SuppressWarnings("unchecked")
								CrossSelling castedSource = (CrossSelling) 
										((JAXBElement<CrossSelling>) crossSellingUnmarshaller.unmarshal((Node) object)).getValue();
								items.add(crossSellingToCrossSellingDataConverter.convert(castedSource));
							
							} else if ("HotelCrossSelling".equals(crossSellingInstanceType)) {
								@SuppressWarnings("unchecked")
								HotelCrossSelling castedSource = (HotelCrossSelling) 
										((JAXBElement<HotelCrossSelling>) hotelCrossSellingUnmarshaller.unmarshal((Node) object)).getValue();
								items.add(hotelCrossSellingToHotelCrossSellingDataConverter.convert(castedSource));
							
							}
			
						} catch (JAXBException e) {
							throw new AlitaliaServiceException("Invalid class Items collection, cannot convert");
						} catch (XPathExpressionException e) {
							throw new AlitaliaServiceException("Invalid class Items collection, cannot convert");
						}
					}
				}
			}
			destination.setItems(items);
		}

		return destination;
	}

}
