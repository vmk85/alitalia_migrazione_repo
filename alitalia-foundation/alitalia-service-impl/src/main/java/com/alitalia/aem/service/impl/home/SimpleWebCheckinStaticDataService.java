package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.BaseRequest;
import com.alitalia.aem.common.messages.home.GetCountryRequest;
import com.alitalia.aem.common.messages.home.GetCountryResponse;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixResponse;
import com.alitalia.aem.common.messages.home.WebCheckinAirportNameRequest;
import com.alitalia.aem.common.messages.home.WebCheckinAirportNameResponse;
import com.alitalia.aem.common.messages.home.WebCheckinBPPermissionRequest;
import com.alitalia.aem.common.messages.home.WebCheckinBPPermissionResponse;
import com.alitalia.aem.common.messages.home.WebCheckinFrequentFlyerResponse;
import com.alitalia.aem.common.messages.home.WebCheckinStatesRequest;
import com.alitalia.aem.common.messages.home.WebCheckinStatesResponse;
import com.alitalia.aem.service.api.home.WebCheckinStaticDataService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.checkinstaticdataservice.BoardingPassPermissionResponseToWebCheckinBPPermissionResponseConverter;
import com.alitalia.aem.service.impl.converter.home.checkinstaticdataservice.GetCountryRequestToGetCountryListRequestConverter;
import com.alitalia.aem.service.impl.converter.home.checkinstaticdataservice.StaticDataResponseToGetCountryResponse;
import com.alitalia.aem.service.impl.converter.home.checkinstaticdataservice.StaticDataResponseToRetrievePhonePrefixResponseConverter;
import com.alitalia.aem.service.impl.converter.home.checkinstaticdataservice.StaticDataResponseToWebCheckinAirportNameResponse;
import com.alitalia.aem.service.impl.converter.home.checkinstaticdataservice.StaticDataResponseToWebCheckinFrequentFlyerResponse;
import com.alitalia.aem.service.impl.converter.home.checkinstaticdataservice.StaticDataResponseToWebCheckinStatesResponse;
import com.alitalia.aem.service.impl.converter.home.checkinstaticdataservice.WebCheckinAirportNameRequestToGetAirportNameListRequestConverter;
import com.alitalia.aem.service.impl.converter.home.checkinstaticdataservice.WebCheckinBPPermissionRequestToBoardingPassPermissionRequestConverter;
import com.alitalia.aem.service.impl.converter.home.checkinstaticdataservice.WebCheckinStatesRequestToGetStateListRequestConverter;
import com.alitalia.aem.ws.checkin.staticdataservice.CheckinStaticDataServiceClient;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.BoardingPassPermissionRequest;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.BoardingPassPermissionResponse;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.GetAirportNameListRequest;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.GetCountryListRequest;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.GetStateListRequest;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.StaticDataResponseOfAirportNameUGS9ND5Y;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.StaticDataResponseOfAmericanStatesUGS9ND5Y;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.StaticDataResponseOfCountryUGS9ND5Y;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.StaticDataResponseOfFrequentFlyerCarrierUGS9ND5Y;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.StaticDataResponseOfInternationalPhonePrefixUGS9ND5Y;

@Component(immediate=true, metatype=false)
@Service
public class SimpleWebCheckinStaticDataService implements WebCheckinStaticDataService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleWebCheckinStaticDataService.class);

	@Reference
	private CheckinStaticDataServiceClient checkinStaticDataClient;
	
	@Reference
	private GetCountryRequestToGetCountryListRequestConverter countryRequestConverter;

	@Reference
	private StaticDataResponseToGetCountryResponse countryResponseConverter;
	
	@Reference
	private StaticDataResponseToRetrievePhonePrefixResponseConverter prefixResponseConverter;
	
	@Reference
	private WebCheckinBPPermissionRequestToBoardingPassPermissionRequestConverter bpPermissionRequestConverter;
	
	@Reference
	private BoardingPassPermissionResponseToWebCheckinBPPermissionResponseConverter bpPermissionResponseConverter;
	
	@Reference
	private WebCheckinStatesRequestToGetStateListRequestConverter getStatesRequestConverter;
	
	@Reference
	private WebCheckinAirportNameRequestToGetAirportNameListRequestConverter getAirportNameRequestConverter;
	
	@Reference
	private StaticDataResponseToWebCheckinFrequentFlyerResponse getFrequentFlyerResponseConverter;
	
	@Reference
	private StaticDataResponseToWebCheckinStatesResponse getStatesResponseConverter;
	
	@Reference
	private StaticDataResponseToWebCheckinAirportNameResponse getAirportNameResponseConverter;
	
	@Override
	public GetCountryResponse getCountries(GetCountryRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method getCountries(). The request is {}", request);
		}

		try {
			GetCountryResponse response = new GetCountryResponse();
			GetCountryListRequest  serviceRequest = countryRequestConverter.convert(request);
			StaticDataResponseOfCountryUGS9ND5Y serviceResponse = checkinStaticDataClient.getCountriesList(serviceRequest);
			response = countryResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully getCountries(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getCountries().", e);
			throw new AlitaliaServiceException("Exception executing getCountries: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public RetrievePhonePrefixResponse getPrefixes(GetCountryRequest request) {
		logger.debug("Executing method getPrefixes(). The request is {}", request);

		try {
			RetrievePhonePrefixResponse response = new RetrievePhonePrefixResponse();
			GetCountryListRequest  serviceRequest = countryRequestConverter.convert(request);
			StaticDataResponseOfInternationalPhonePrefixUGS9ND5Y serviceResponse = checkinStaticDataClient.getInternationalPhonePrefixesList(serviceRequest);
			response = prefixResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully getPrefixes(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getPrefixes().", e);
			throw new AlitaliaServiceException("Exception executing getPrefixes: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public WebCheckinBPPermissionResponse getBoardingPassPermissions(WebCheckinBPPermissionRequest request) {
		logger.debug("Executing method getBoardingPassPermissions(). The request is {}", request);

		try {
			WebCheckinBPPermissionResponse response = new WebCheckinBPPermissionResponse();
			BoardingPassPermissionRequest  serviceRequest = bpPermissionRequestConverter.convert(request);
			BoardingPassPermissionResponse serviceResponse = checkinStaticDataClient.getBoardingPassPermissions(serviceRequest);
			response = bpPermissionResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully getBoardingPassPermissions(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getBoardingPassPermissions().", e);
			throw new AlitaliaServiceException("Exception executing getBoardingPassPermissions: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public WebCheckinFrequentFlyerResponse getFrequentFlyers(GetCountryRequest request) {
		logger.debug("Executing method getFrequentFlyers(). The request is {}", request);

		try {
			WebCheckinFrequentFlyerResponse response = new WebCheckinFrequentFlyerResponse();
			GetCountryListRequest serviceRequest = countryRequestConverter.convert(request);
			StaticDataResponseOfFrequentFlyerCarrierUGS9ND5Y serviceResponse = checkinStaticDataClient.getFrequentFlyersList(serviceRequest);
			response = getFrequentFlyerResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully getFrequentFlyers(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getFrequentFlyers().", e);
			throw new AlitaliaServiceException("Exception executing getFrequentFlyers: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public WebCheckinStatesResponse getAmericanStates(BaseRequest request) {
		logger.debug("Executing method getAmericanStates(). The request is {}", request);

		try {
			WebCheckinStatesResponse response = new WebCheckinStatesResponse();
			StaticDataResponseOfAmericanStatesUGS9ND5Y serviceResponse = checkinStaticDataClient.getAmericanStatesList();
			response = getStatesResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully getAmericanStates(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getAmericanStates().", e);
			throw new AlitaliaServiceException("Exception executing getAmericanStates: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public WebCheckinStatesResponse getStates(WebCheckinStatesRequest request) {
		logger.debug("Executing method getStates(). The request is {}", request);

		try {
			WebCheckinStatesResponse response = new WebCheckinStatesResponse();
			GetStateListRequest serviceRequest = getStatesRequestConverter.convert(request);
			StaticDataResponseOfAmericanStatesUGS9ND5Y serviceResponse = checkinStaticDataClient.getStatesList(serviceRequest);
			response = getStatesResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully getStates(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getStates().", e);
			throw new AlitaliaServiceException("Exception executing getStates: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public WebCheckinAirportNameResponse getAirportNames(WebCheckinAirportNameRequest request) {
		logger.debug("Executing method getAirportNames(). The request is {}", request);

		try {
			WebCheckinAirportNameResponse response = new WebCheckinAirportNameResponse();
			GetAirportNameListRequest serviceRequest = getAirportNameRequestConverter.convert(request);
			StaticDataResponseOfAirportNameUGS9ND5Y serviceResponse = checkinStaticDataClient.getAirportNamesList(serviceRequest);
			response = getAirportNameResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully getAirportNames(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getAirportNames().", e);
			throw new AlitaliaServiceException("Exception executing getAirportNames: Sid ["+request.getSid()+"]" , e);
		}
	}
}