package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentFlightData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetailsSolutionPricingBookingInfoSegmentFlight;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingBookingInfoSegmentFlightConverterDataToPricingBookingInfoSegmentFlightConverter.class)
public class RBDSPricingBookingInfoSegmentFlightConverterDataToPricingBookingInfoSegmentFlightConverter
		implements Converter<ResultBookingDetailsSolutionPricingBookingInfoSegmentFlightData, ResultBookingDetailsSolutionPricingBookingInfoSegmentFlight> {

	@Override
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentFlight convert(
			ResultBookingDetailsSolutionPricingBookingInfoSegmentFlightData source) {
		
		ResultBookingDetailsSolutionPricingBookingInfoSegmentFlight destination = null;
		
		if(source != null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionPricingBookingInfoSegmentFlight();
			destination.setCarrierField(source.getCarrierField());
			destination.setNumberField(source.getNumberField());
			destination.setNumberFieldSpecified(source.isNumberFieldSpecified());
		}
		
		return destination;
		
	}

	
}
