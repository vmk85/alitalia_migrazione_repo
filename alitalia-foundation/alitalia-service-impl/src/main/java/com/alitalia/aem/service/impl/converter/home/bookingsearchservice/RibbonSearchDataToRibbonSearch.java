package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import java.util.Calendar;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PassengerNumbersData;
import com.alitalia.aem.common.data.home.RibbonSearchData;
import com.alitalia.aem.common.data.home.SearchDestinationData;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.CabinTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.ResidencyTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.SearchTypeEnum;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.searchservice.xsd2.RibbonSearch;
import com.alitalia.aem.ws.booking.searchservice.xsd3.PassengerNumbers;
import com.alitalia.aem.ws.booking.searchservice.xsd3.SearchDestination;
import com.alitalia.aem.ws.booking.searchservice.xsd4.Cabin;
import com.alitalia.aem.ws.booking.searchservice.xsd4.CabinType;
import com.alitalia.aem.ws.booking.searchservice.xsd4.ResidencyType;
import com.alitalia.aem.ws.booking.searchservice.xsd4.SearchType;
import com.alitalia.aem.ws.booking.searchservice.xsd5.ArrayOfanyType;

@Component(immediate=true, metatype=false)
@Service(value=RibbonSearchDataToRibbonSearch.class)
public class RibbonSearchDataToRibbonSearch implements Converter<RibbonSearchData, RibbonSearch> {

	@Reference
	private SearchDestinationDataToSearchDestination destinationConverter;

	@Reference
	private PassengerNumbersDataToPassengerNumbers psnNumbersConverter;

	@Override
	public RibbonSearch convert(RibbonSearchData source) {
		com.alitalia.aem.ws.booking.searchservice.xsd2.ObjectFactory factory2 = new com.alitalia.aem.ws.booking.searchservice.xsd2.ObjectFactory();
		com.alitalia.aem.ws.booking.searchservice.xsd3.ObjectFactory factory3 = new com.alitalia.aem.ws.booking.searchservice.xsd3.ObjectFactory();
		com.alitalia.aem.ws.booking.searchservice.xsd5.ObjectFactory factory5 = new com.alitalia.aem.ws.booking.searchservice.xsd5.ObjectFactory();

		ArrayOfanyType destinations = factory5.createArrayOfanyType();
		for (SearchDestinationData sdData : source.getDestinations()) {
			SearchDestination searchDestination = destinationConverter.convert(sdData);
			destinations.getAnyType().add(searchDestination);
		}

		ArrayOfanyType passengerNumbers = factory5.createArrayOfanyType();
		for (PassengerNumbersData psnNumberData: source.getPassengerNumbers()) {
			PassengerNumbers numbers = psnNumbersConverter.convert(psnNumberData);
			passengerNumbers.getAnyType().add(numbers);
		}

		RibbonSearch destination = factory2.createRibbonSearch();

		destination.setProperties(factory3.createABoomBoxGenericInfoProperties(null));
		destination.setFarmId(factory3.createABoomBoxInfoFarmId(source.getFarmId()));
		destination.setId(factory3.createABoomBoxInfoId(source.getId()));
		destination.setSessionId(factory3.createABoomBoxInfoSessionId(source.getSolutionId()));
		destination.setSolutionSet(factory3.createABoomBoxInfoSolutionSet(source.getSolutionSet()));
		destination.setCUG(factory3.createASearchCUG(source.getCug()));
		destination.setDestinations(factory3.createASearchDestinations(destinations));
		destination.setInnerSearch(factory3.createASearchInnerSearch(null));
		destination.setMarket(factory3.createASearchMarket(source.getMarket()));
		destination.setOnlyDirectFlight(source.isOnlyDirectFlight());
		destination.setPassengerNumbers(factory3.createASearchPassengerNumbers(passengerNumbers));

		CabinEnum searchCabin = source.getSearchCabin();
		if (searchCabin != null)
			destination.setSearchCabin(Cabin.fromValue(searchCabin.value()));

		CabinTypeEnum searchCabinType = source.getSearchCabinType();
		if (searchCabinType != null)
			destination.setSearchCabinType(CabinType.fromValue(searchCabinType.value()));

		SearchTypeEnum type = source.getType();
		if (type != null)
			destination.setType(SearchType.fromValue(type.value()));

		ResidencyTypeEnum sourceResidency = source.getResidency();
		if (sourceResidency != null)
			destination.setResidency(ResidencyType.fromValue(sourceResidency.value()));
		
		destination.setIsBandoPeriod(source.isBandoPeriod());

		Calendar startOutboundDate = source.getStartOutboundDate();
		if (startOutboundDate != null)
			destination.setOutboundTabDate(XsdConvertUtils.toXMLGregorianCalendar(startOutboundDate));

		Calendar startReturnDate = source.getStartReturnDate();
		if (startReturnDate != null)
			destination.setInboundTabDate(XsdConvertUtils.toXMLGregorianCalendar(startReturnDate));

		return destination;
	}

}
