package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PaymentProviderBancaIntesaData;
import com.alitalia.aem.common.data.home.PaymentProviderBancoPostaData;
import com.alitalia.aem.common.data.home.PaymentProviderCreditCardData;
import com.alitalia.aem.common.data.home.PaymentProviderData;
import com.alitalia.aem.common.data.home.PaymentProviderFindomesticData;
import com.alitalia.aem.common.data.home.PaymentProviderGlobalCollectData;
import com.alitalia.aem.common.data.home.PaymentProviderInstalmentsBrazilData;
import com.alitalia.aem.common.data.home.PaymentProviderLottomaticaData;
import com.alitalia.aem.common.data.home.PaymentProviderMasterPassData;
import com.alitalia.aem.common.data.home.PaymentProviderPayAtTOData;
import com.alitalia.aem.common.data.home.PaymentProviderPayLaterData;
import com.alitalia.aem.common.data.home.PaymentProviderPayPalData;
import com.alitalia.aem.common.data.home.PaymentProviderPosteIDData;
import com.alitalia.aem.common.data.home.PaymentProviderSTSData;
import com.alitalia.aem.common.data.home.PaymentProviderUnicreditData;
import com.alitalia.aem.common.data.home.PaymentProviderZeroPaymentData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.APaymentProviderOfanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.CreditCardOfanyTypeanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.CreditCardType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.FindomesticOfanyTypeanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.GlobalCollectOfanyTypeanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.GlobalCollectPaymentType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.LottomaticaOfanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.MasterPassOfanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.PayAtTOOfanyTypeanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.PayLaterOfanyTypeanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.PayPalOfanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.STSOfanyTypeanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd5.ArrayOfstring;

@Component(immediate=true, metatype=false)
@Service(value=PaymentProviderDataToAPaymentProviderOfAnyType.class)
public class PaymentProviderDataToAPaymentProviderOfAnyType 
		implements Converter<PaymentProviderData, APaymentProviderOfanyType> {
	
	@Reference
	private PaymentComunicationBaseDataToAComunicationBase paymentComunicationBaseDataToAComunicationBase;
	
	@Reference
	private UserInfoBaseToAUserInfo userInfoBaseToAUserInfoConverter;
	
	@Reference
	private ContactDataToContact contactDataToContactConverter;
	
	private static final Logger logger = LoggerFactory.getLogger(PaymentProviderDataToAPaymentProviderOfAnyType.class);
	
	@Override
	public APaymentProviderOfanyType convert(PaymentProviderData source) {
		
		APaymentProviderOfanyType destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			com.alitalia.aem.ws.booking.paymentservice.xsd5.ObjectFactory xsd5ObjectFactory = 
					new com.alitalia.aem.ws.booking.paymentservice.xsd5.ObjectFactory();
			
			if (source instanceof PaymentProviderBancaIntesaData) {
				destination = objectFactory.createBancaIntesaOfanyType();
				
			} else if (source instanceof PaymentProviderUnicreditData){
				destination = objectFactory.createUnicreditOfanyType();
				
			} else if (source instanceof PaymentProviderBancoPostaData) {
				destination = objectFactory.createBancoPostaOfanyType();
				
			} else if (source instanceof PaymentProviderZeroPaymentData) {
				destination = objectFactory.createZeroPaymentProviderOfanyTypeanyType();
				
			} else if (source instanceof PaymentProviderPosteIDData) {
				destination = objectFactory.createPosteIDOfanyType();
				
			} else if (source instanceof PaymentProviderCreditCardData) {
				PaymentProviderCreditCardData typedSource = (PaymentProviderCreditCardData) source;
				CreditCardOfanyTypeanyType typedDestination = objectFactory.createCreditCardOfanyTypeanyType();
				
				typedDestination.setCreditCardNumber(
						objectFactory.createCreditCardOfanyTypeanyTypeCreditCardNumber(
								typedSource.getCreditCardNumber()));
				
				typedDestination.setCVV(
						objectFactory.createCreditCardOfanyTypeanyTypeCVV(
								typedSource.getCvv()));
				
				typedDestination.setExpiryMonth(
						typedSource.getExpiryMonth());
				
				typedDestination.setExpiryYear(
						typedSource.getExpiryYear());
				
				typedDestination.setIs3DSecure(typedSource.getIs3DSecure());
				
				typedDestination.setToken(
						objectFactory.createCreditCardOfanyTypeanyTypeToken(
								typedSource.getToken()));
				
				if (typedSource.getType() != null) {
					typedDestination.setType(
							CreditCardType.fromValue(typedSource.getType().value()));
				}
				
				typedDestination.setUseOneClick(typedSource.getUseOneClick());
				
				if (typedSource.getUserInfo() != null) {
					typedDestination.setUserInfo(
							objectFactory.createCreditCardOfanyTypeanyTypeUserInfo(
								userInfoBaseToAUserInfoConverter.convert(
										typedSource.getUserInfo())));
				}
				destination = typedDestination;
				
			} else if (source instanceof PaymentProviderFindomesticData) {
				PaymentProviderFindomesticData typedSource = (PaymentProviderFindomesticData) source;
				FindomesticOfanyTypeanyType typedDestination = objectFactory.createFindomesticOfanyTypeanyType();
				
				if (typedSource.getUserInfo() != null) {
					typedDestination.setUserInfo(
							objectFactory.createFindomesticOfanyTypeanyTypeUserInfo(
								userInfoBaseToAUserInfoConverter.convert(
										typedSource.getUserInfo())));
				}
				destination = typedDestination;
				
			} else if (source instanceof PaymentProviderGlobalCollectData) {
				logger.info("PaymentProviderData instanceof PaymentProviderGlobalCollectData");
				PaymentProviderGlobalCollectData typedSource = (PaymentProviderGlobalCollectData) source;
				GlobalCollectOfanyTypeanyType typedDestination = objectFactory.createGlobalCollectOfanyTypeanyType();

				if (typedSource.getUserInfo() != null) {
					typedDestination.setUserInfo(
							objectFactory.createGlobalCollectOfanyTypeanyTypeUserInfo(
								userInfoBaseToAUserInfoConverter.convert(
										typedSource.getUserInfo())));
				}
				
				if (typedSource.getType() != null) {
					typedDestination.setType(
							GlobalCollectPaymentType.fromValue(typedSource.getType().value()));
				}
				
				typedDestination.setInstallementsNumber(
						typedSource.getInstallementsNumber());
				
				destination = typedDestination;
				
			} else if (source instanceof PaymentProviderInstalmentsBrazilData) {
				PaymentProviderInstalmentsBrazilData typedSource = (PaymentProviderInstalmentsBrazilData) source;
				
				GlobalCollectOfanyTypeanyType typedDestination = objectFactory.createGlobalCollectOfanyTypeanyType();
				
				if (typedSource.getType() != null) {
					typedDestination.setType(
							GlobalCollectPaymentType.fromValue(typedSource.getType().value()));
				}
				typedDestination.setInstallementsNumber(typedSource.getInstallementsNumber()!=0 ? typedSource.getInstallementsNumber() : 0);
				
				destination = typedDestination;
			} else if (source instanceof PaymentProviderLottomaticaData) {
				LottomaticaOfanyType typedDestination = objectFactory.createLottomaticaOfanyType();
				destination = typedDestination;
				
			} else if (source instanceof PaymentProviderMasterPassData) {
				MasterPassOfanyType typedDestination = objectFactory.createMasterPassOfanyType();
				destination = typedDestination;	
				
			} else if (source instanceof PaymentProviderPayAtTOData) {
				PaymentProviderPayAtTOData typedSource = (PaymentProviderPayAtTOData) source;
				PayAtTOOfanyTypeanyType typedDestination = objectFactory.createPayAtTOOfanyTypeanyType();
				
				ArrayOfstring emailForMail = xsd5ObjectFactory.createArrayOfstring();
				if (typedSource.getEmailForMail() != null) {
					for (String email : typedSource.getEmailForMail()) {
						emailForMail.getString().add(email);
					}
				}
				typedDestination.setEmailForMail(
						objectFactory.createPayLaterOfanyTypeanyTypeEmailForMail(
								emailForMail));
				
				typedDestination.setEmailForPnr(
						objectFactory.createPayLaterOfanyTypeanyTypeEmailForPnr(
								typedSource.getEmailForPnr()));
				
				if (typedSource.getOtherContact() != null) {
					typedDestination.setOtherContact(
							objectFactory.createPayLaterOfanyTypeanyTypeOtherContact(
								contactDataToContactConverter.convert(
										typedSource.getOtherContact())));
				}
				
				destination = typedDestination;
			} else if (source instanceof PaymentProviderPayLaterData) {
				PaymentProviderPayLaterData typedSource = (PaymentProviderPayLaterData) source;
				PayLaterOfanyTypeanyType typedDestination = objectFactory.createPayLaterOfanyTypeanyType();
				
				ArrayOfstring emailForMail = xsd5ObjectFactory.createArrayOfstring();
				if (typedSource.getEmailForMail() != null) {
					for (String email : typedSource.getEmailForMail()) {
						emailForMail.getString().add(email);
					}
				}
				typedDestination.setEmailForMail(
						objectFactory.createPayLaterOfanyTypeanyTypeEmailForMail(
								emailForMail));
				
				typedDestination.setEmailForPnr(
						objectFactory.createPayLaterOfanyTypeanyTypeEmailForPnr(
								typedSource.getEmailForPnr()));
				
				if (typedSource.getOtherContact() != null) {
					typedDestination.setOtherContact(
							objectFactory.createPayLaterOfanyTypeanyTypeOtherContact(
								contactDataToContactConverter.convert(
										typedSource.getOtherContact())));
				}
				destination = typedDestination;
				
			} else if (source instanceof PaymentProviderPayPalData) {
				PayPalOfanyType typedDestination = objectFactory.createPayPalOfanyType();
				destination = typedDestination;	
				
			} else if (source instanceof PaymentProviderSTSData) {
				PaymentProviderSTSData typedSource = (PaymentProviderSTSData) source;
				STSOfanyTypeanyType typedDestination = objectFactory.createSTSOfanyTypeanyType();
				
				typedDestination.setClientInfoRef(
						objectFactory.createSTSOfanyTypeanyTypeClientInfoRef(
								typedSource.getClientInfoRef()));
				
				typedDestination.setClientIP(
						objectFactory.createSTSOfanyTypeanyTypeClientIP(
								typedSource.getClientIP()));
				
				typedDestination.setClientUserAccount(
						objectFactory.createSTSOfanyTypeanyTypeClientUserAccount(
								typedSource.getClientUserAccount()));
				
				typedDestination.setFailurePageUrl(
						objectFactory.createSTSOfanyTypeanyTypeFailurePageUrl(
								typedSource.getFailurePageUrl()));
				
				typedDestination.setMerchantShopRef(
						objectFactory.createSTSOfanyTypeanyTypeMerchantShopRef(
								typedSource.getMerchantShopRef()));
				
				typedDestination.setStsClientID(
						objectFactory.createSTSOfanyTypeanyTypeStsClientID(
								typedSource.getStsClientID()));
				
				typedDestination.setStsMerchantID(
						objectFactory.createSTSOfanyTypeanyTypeStsMerchantID(
								typedSource.getStsMerchantID()));
				
				typedDestination.setStsPayType(
						objectFactory.createSTSOfanyTypeanyTypeStsPayType(
								typedSource.getStsPayType()));
				
				typedDestination.setSuccessPageUrl(
						objectFactory.createSTSOfanyTypeanyTypeSuccessPageUrl(
								typedSource.getSuccessPageUrl()));
				
				if (typedSource.getStsAdvanceDate() != null)
					typedDestination.setStsAdvanceDate(XsdConvertUtils.toXMLGregorianCalendar(typedSource.getStsAdvanceDate()));
				
				if (typedSource.getStsBalanceDate() != null)
					typedDestination.setStsBalanceDate(XsdConvertUtils.toXMLGregorianCalendar(typedSource.getStsBalanceDate()));
				
				if (typedSource.getStsDepartureDate() != null)
					typedDestination.setStsDepartureDate(XsdConvertUtils.toXMLGregorianCalendar(typedSource.getStsDepartureDate()));
				
				destination = typedDestination;
				
			} else {
				destination = objectFactory.createAPaymentProviderOfanyType();
				
			}
			
			if (source.getComunication() != null) {
				destination.setComunication(
						objectFactory.createAPaymentProviderOfanyTypeComunication(
								paymentComunicationBaseDataToAComunicationBase.convert(
										source.getComunication())));
			}
		
		}
				
		return destination;
	}

}
