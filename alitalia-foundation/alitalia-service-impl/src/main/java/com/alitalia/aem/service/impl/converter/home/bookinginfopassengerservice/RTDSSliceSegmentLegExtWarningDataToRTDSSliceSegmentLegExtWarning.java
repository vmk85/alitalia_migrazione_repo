package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentLegExtWarningData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionSliceSegmentLegExtWarning;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentLegExtWarningDataToRTDSSliceSegmentLegExtWarning.class)
public class RTDSSliceSegmentLegExtWarningDataToRTDSSliceSegmentLegExtWarning implements
		Converter<ResultTicketingDetailSolutionSliceSegmentLegExtWarningData, 
					ResultTicketingDetailSolutionSliceSegmentLegExtWarning> {

	@Override
	public ResultTicketingDetailSolutionSliceSegmentLegExtWarning convert(
			ResultTicketingDetailSolutionSliceSegmentLegExtWarningData source) {
		ResultTicketingDetailSolutionSliceSegmentLegExtWarning destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionSliceSegmentLegExtWarning();

			destination.setOvernightField(source.isOvernightField());
			destination.setOvernightFieldSpecified(source.isOvernightFieldSpecified());
		}

		return destination;
	}

}
