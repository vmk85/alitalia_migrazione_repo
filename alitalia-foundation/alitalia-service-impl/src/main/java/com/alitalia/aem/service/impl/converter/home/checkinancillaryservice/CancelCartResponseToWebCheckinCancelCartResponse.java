package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.MmbAncillaryErrorData;
import com.alitalia.aem.common.messages.home.WebCheckinCancelCartResponse;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.CancelCartResponse;


@Component(immediate=true, metatype=false)
@Service(value=CancelCartResponseToWebCheckinCancelCartResponse.class)
public class CancelCartResponseToWebCheckinCancelCartResponse implements
		Converter<CancelCartResponse, WebCheckinCancelCartResponse> {

	@Reference
	private ErrorToMmbAncillaryErrorData errorConverter;

	@Override
	public WebCheckinCancelCartResponse convert(CancelCartResponse source) {
		WebCheckinCancelCartResponse destination = null;

		if (source != null) {
			destination = new WebCheckinCancelCartResponse();

			JAXBElement<String> sourceMachineName = source.getMachineName();
			if (sourceMachineName != null)
				destination.setMachineName(sourceMachineName.getValue());

			if (source.getErrors() != null &&
					source.getErrors().getValue() != null &&
					source.getErrors().getValue().getAnyType() != null &&
					!source.getErrors().getValue().getAnyType().isEmpty()) {
				List<Object> sourceErrors = source.getErrors().getValue().getAnyType();
				List<MmbAncillaryErrorData> errors = new ArrayList<MmbAncillaryErrorData>();
				for (Object sourceError : sourceErrors) {
					errors.add(errorConverter.convert(sourceError));
				}
				destination.setErrors(errors);
			}
		}

		return destination;
	}

}
