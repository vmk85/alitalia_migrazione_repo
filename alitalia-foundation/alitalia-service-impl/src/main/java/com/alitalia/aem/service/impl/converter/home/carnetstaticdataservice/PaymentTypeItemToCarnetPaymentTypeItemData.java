package com.alitalia.aem.service.impl.converter.home.carnetstaticdataservice;

import java.util.ArrayList;
import java.util.List;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.DictionaryItemData;
import com.alitalia.aem.common.data.home.carnet.CarnetPaymentTypeItemData;
import com.alitalia.aem.ws.carnet.staticdataservice.xsd2.DictionaryItem;
import com.alitalia.aem.ws.carnet.staticdataservice.xsd2.PaymentTypeItem;


@Component(immediate=true, metatype=false)
@Service(value=PaymentTypeItemToCarnetPaymentTypeItemData.class)
public class PaymentTypeItemToCarnetPaymentTypeItemData implements Converter<PaymentTypeItem, CarnetPaymentTypeItemData> {

	@Override
	public CarnetPaymentTypeItemData convert(PaymentTypeItem source) {
		CarnetPaymentTypeItemData destination = null;

		if (source != null) {
			destination = new CarnetPaymentTypeItemData();
			if (source.getCode() != null)
				destination.setCode(source.getCode().getValue());
			if (source.getDescription() != null)
				destination.setDescription(source.getDescription().getValue());
			if (source.getOtherInfo() != null && source.getOtherInfo().getValue() != null
				&& source.getOtherInfo().getValue().getDictionaryItem()!=null 
				&& source.getOtherInfo().getValue().getDictionaryItem().size()>0) {
				List<DictionaryItemData> otherInfo = new ArrayList<>();
				for(DictionaryItem dictionaryItem : source.getOtherInfo().getValue().getDictionaryItem()){
					DictionaryItemData dictionaryItemData = new DictionaryItemData();
					dictionaryItemData.setKey(dictionaryItem.getKey().getValue());
					dictionaryItemData.setValue(dictionaryItem.getValue().getValue());
					otherInfo.add(dictionaryItemData);
				}
				destination.setOtherInfo(otherInfo);
			}
		}
		return destination;
	}
}