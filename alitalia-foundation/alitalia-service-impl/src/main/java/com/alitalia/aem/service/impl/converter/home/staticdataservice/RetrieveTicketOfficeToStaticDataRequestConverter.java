package com.alitalia.aem.service.impl.converter.home.staticdataservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveTicketOfficeRequest;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.StaticDataRequest;
import com.alitalia.aem.ws.booking.staticdataservice.xsd2.StaticDataType;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveTicketOfficeToStaticDataRequestConverter.class)
public class RetrieveTicketOfficeToStaticDataRequestConverter implements Converter<RetrieveTicketOfficeRequest, StaticDataRequest>{
	
	@Override
	public StaticDataRequest convert(RetrieveTicketOfficeRequest request){
		StaticDataRequest staticDataRequest = new StaticDataRequest();
		ObjectFactory obj = new ObjectFactory();
		staticDataRequest.setLanguageCode(obj.createStaticDataRequestLanguageCode(request.getLanguageCode()));
		staticDataRequest.setItemCache(obj.createStaticDataRequestItemCache(request.getItemCache()));
		staticDataRequest.setMarket(obj.createStaticDataRequestMarket(request.getMarket()));
		staticDataRequest.setType(StaticDataType.TICKET_OFFICE);
		return staticDataRequest;
	}
}
