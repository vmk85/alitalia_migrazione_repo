package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.MMPaybackAccrualResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.PaybackAccrualResponse;

@Component(immediate = true, metatype = false)
@Service(value = PaybackAccrualResponseConverter.class)
public class PaybackAccrualResponseConverter implements Converter<PaybackAccrualResponse, MMPaybackAccrualResponse> {

	@Override
	public MMPaybackAccrualResponse convert(PaybackAccrualResponse source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		
		MMPaybackAccrualResponse destination = new MMPaybackAccrualResponse();
		
		if (source != null)
			destination.setPaybackResponse(source.getPaybackResponse());
		
		return destination;
	}
}