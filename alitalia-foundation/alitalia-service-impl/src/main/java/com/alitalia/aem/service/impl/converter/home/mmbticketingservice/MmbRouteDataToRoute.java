package com.alitalia.aem.service.impl.converter.home.mmbticketingservice;

import java.util.Calendar;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.enumerations.MmbApisTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbChannelsEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbMealAllowedEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbRouteStatusEnum;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.MmbRouteData;
import com.alitalia.aem.common.data.home.mmb.MmbTaxData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd2.ApisType;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd2.ArrayOfFlight;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd2.ArrayOfTax;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd2.Channels;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd2.MealAllowed;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd2.Route;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd2.RouteStatus;

@Component(immediate=true, metatype=false)
@Service(value=MmbRouteDataToRoute.class)
public class MmbRouteDataToRoute implements Converter<MmbRouteData, Route> {

	@Reference
	private MmbPassengerDataToPassenger mmbPassengerDataConverter;

	@Reference
	private MmbDeepLinkDataToDeepLink mmbDeepLinkDataConverter;

	@Reference
	private MmbFlightDataToFlight mmbFlightDataConverter;

	@Reference
	private MmbTaxDataToTax mmbTaxDataConverter;

	@Override
	public Route convert(MmbRouteData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		Route destination = null;

		if (source != null) {
			destination = objectFactory.createRoute();

			MmbApisTypeEnum sourceApisTypeRequired = source.getApisTypeRequired();
			if (sourceApisTypeRequired != null)
				destination.setX003CApisTypeRequiredX003EKBackingField(ApisType.fromValue(sourceApisTypeRequired.value()));

			destination.setX003CCartFastTrackBoxX003EKBackingField(source.isCartFastTrackBox());
			destination.setX003CCartVipLoungeBoxX003EKBackingField(source.isCartVipLoungeBox());

			MmbChannelsEnum sourceChannel = source.getChannel();
			if (sourceChannel != null)
				destination.setX003CChannelX003EKBackingField(Channels.fromValue(sourceChannel.value()));

			destination.setX003CCheckinEnabledX003EKBackingField(source.isCheckinEnabled());
			destination.setX003CConfirmedX003EKBackingField(source.isConfirmed());
			destination.setX003CCreateDateTimeX003EKBackingField(source.getCreateDateTime());
			destination.setX003CIdX003EKBackingField(source.getId());
			destination.setX003CMealAvailableX003EKBackingField(source.isMealAvailable());

			MmbMealAllowedEnum sourceMealAllowed = source.getMealAllowed();
			if (sourceMealAllowed != null)
				destination.setX003CMealAllowedX003EKBackingField(MealAllowed.fromValue(sourceMealAllowed.value()));

			destination.setX003CPnrX003EKBackingField(source.getPnr());
			destination.setX003CSelectedX003EKBackingField(source.isSelected());
			destination.setX003CUndoEnabledX003EKBackingField(source.isUndoEnabled());
			destination.setX003CVisibleApisX003EKBackingField(source.isVisibleApis());

			MmbRouteStatusEnum sourceStatus = source.getStatus();
			if (sourceStatus != null)
				destination.setX003CStatusX003EKBackingField(RouteStatus.fromValue(sourceStatus.value()));

			Calendar sourceDateTimePaymentLimit = source.getDateTimePaymentLimit();
			if (sourceDateTimePaymentLimit != null)
				destination.setX003CDateTimePaymentLimitX003EKBackingField(
						XsdConvertUtils.toXMLGregorianCalendar(sourceDateTimePaymentLimit));

			destination.setX003CApplicantPassengerX003EKBackingField(
					mmbPassengerDataConverter.convert(source.getApplicantPassenger()));

			destination.setX003CDeepLinkX003EKBackingField(mmbDeepLinkDataConverter.convert(source.getDeepLink()));

			List<MmbFlightData> sourceFlights = source.getFlights();
			if (sourceFlights != null && !sourceFlights.isEmpty()) {
				ArrayOfFlight flights = objectFactory.createArrayOfFlight();
				for(MmbFlightData sourceFlight : sourceFlights) {
					flights.getFlight().add(mmbFlightDataConverter.convert(sourceFlight));
				}
				destination.setX003CFlightsX003EKBackingField(flights);
			}

			List<MmbTaxData> sourceTaxes = source.getTaxes();
			if (sourceTaxes != null && !sourceTaxes.isEmpty()) {
				ArrayOfTax taxes = objectFactory.createArrayOfTax();
				for (MmbTaxData sourceTax : sourceTaxes) {
					taxes.getTax().add(mmbTaxDataConverter.convert(sourceTax));
				}
				destination.setX003CTaxesX003EKBackingField(taxes);
			}
		}

		return destination;
	}

}
