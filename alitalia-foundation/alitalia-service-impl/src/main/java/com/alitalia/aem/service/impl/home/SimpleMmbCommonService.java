package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.RetrieveMmbFlightSeatMapRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbFlightSeatMapResponse;
import com.alitalia.aem.service.api.home.MmbCommonService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.mmbcommonservice.RetrieveMmbFlightSeatMapRequestToSeatMapRequest;
import com.alitalia.aem.service.impl.converter.home.mmbcommonservice.SeatMapResponseToRetrieveMmbFlightSeatMapResponse;
import com.alitalia.aem.ws.mmb.commonservice.MmbCommonServiceClient;
import com.alitalia.aem.ws.mmb.commonservice.xsd1.SeatMapRequest;
import com.alitalia.aem.ws.mmb.commonservice.xsd1.SeatMapResponse;

@Service
@Component(immediate=true, metatype=false)
public class SimpleMmbCommonService implements MmbCommonService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleMmbCommonService.class);

	@Reference
	private MmbCommonServiceClient serviceClient;

	@Reference
	private RetrieveMmbFlightSeatMapRequestToSeatMapRequest retrieveMmbFlightSeatMapRequestConverter;
	
	@Reference
	private SeatMapResponseToRetrieveMmbFlightSeatMapResponse seatMapResponseConverter;

	@Override
	public RetrieveMmbFlightSeatMapResponse getMmbFlightSeatsMap(RetrieveMmbFlightSeatMapRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method getMmbFlightSeatsMap(). The request is {}", request);
		}
		

		try {
			RetrieveMmbFlightSeatMapResponse response = new RetrieveMmbFlightSeatMapResponse();
			SeatMapRequest serviceRequest = retrieveMmbFlightSeatMapRequestConverter.convert(request);
			SeatMapResponse serviceResponse = serviceClient.getFligthSeatMap(serviceRequest);
			response = seatMapResponseConverter.convert(serviceResponse);
			
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully method getMmbFlightSeatsMap(). The response is {}", response);
			}
			
			return response;
		} catch(Exception e) {
			logger.error("Exception while executing method method getMmbFlightSeatsMap().", e);
			throw new AlitaliaServiceException("Exception executing getMmbFlightSeatsMap: Sid ["+request.getSid()+"]" , e);
		}
	}
}
