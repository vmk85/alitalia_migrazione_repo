package com.alitalia.aem.service.impl.converter.home.bookingwidget;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.CashAndMilesData;
import com.alitalia.aem.common.messages.home.LoadWidgetCashMilesResponse;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.widgetservice.xsd1.LoadCashAndMilesResponse;
import com.alitalia.aem.ws.booking.widgetservice.xsd1.LoadWidgetResponse;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.CashAndMiles;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.MmCustomer;

@Component(immediate=true, metatype=false)
@Service(value=LoadWidgetResponseToLoadWidgetCashMilesResponse.class)
public class LoadWidgetResponseToLoadWidgetCashMilesResponse implements
		Converter<LoadWidgetResponse, LoadWidgetCashMilesResponse> {

	@Reference
	private MmCustomerToMmCustomerData mmCustomerConverter;

	@Override
	public LoadWidgetCashMilesResponse convert(LoadWidgetResponse source) {
		LoadWidgetCashMilesResponse destination = null;
		
		if (source instanceof LoadCashAndMilesResponse &&
				source.getWidget() != null && 
				source.getWidget().getValue() != null &&
						source.getWidget().getValue() instanceof CashAndMiles) {

			CashAndMiles sourceCasted = (CashAndMiles) source.getWidget().getValue();

			CashAndMilesData cashMiles = new CashAndMilesData();
			cashMiles.setDiscountAmount(sourceCasted.getDiscountAmount());

			MmCustomer mmCustomer = sourceCasted.getMMCustomer().getValue();
			if (mmCustomer != null)
				cashMiles.setMmCustomer(mmCustomerConverter.convert(mmCustomer));
			else
				cashMiles.setMmCustomer(null);

			cashMiles.setPaymentDate(XsdConvertUtils.parseCalendar(sourceCasted.getPaymentDate()));
			cashMiles.setUsedMileage(sourceCasted.getUsedMileage());

			destination = new LoadWidgetCashMilesResponse();
			destination.setCashMiles(cashMiles);
		}

		return destination;
	}

}
