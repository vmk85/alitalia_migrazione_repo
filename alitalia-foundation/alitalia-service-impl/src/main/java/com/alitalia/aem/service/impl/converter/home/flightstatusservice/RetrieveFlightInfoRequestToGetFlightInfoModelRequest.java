package com.alitalia.aem.service.impl.converter.home.flightstatusservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.FlightInfoData;
import com.alitalia.aem.common.messages.home.RetrieveFlightInfoRequest;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetFlightInfoModelRequest;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.FlightInfoModel;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveFlightInfoRequestToGetFlightInfoModelRequest.class)
public class RetrieveFlightInfoRequestToGetFlightInfoModelRequest implements Converter<RetrieveFlightInfoRequest, GetFlightInfoModelRequest> {

	@Reference
	private FlightInfoDataToFlightInfoModel flightInfoDataConverter;
	
	@Override
	public GetFlightInfoModelRequest convert(RetrieveFlightInfoRequest source) {
		GetFlightInfoModelRequest request = new GetFlightInfoModelRequest();
		
		FlightInfoData flightInfoData = source.getFlightInfoData();
		FlightInfoModel flightInfoModel = flightInfoDataConverter.convert(flightInfoData);
		ObjectFactory objectFactory = new ObjectFactory();
		request.setFlightInfo(objectFactory.createGetFlightInfoModelRequestFlightInfo(flightInfoModel));
		
		return request;
	}
}