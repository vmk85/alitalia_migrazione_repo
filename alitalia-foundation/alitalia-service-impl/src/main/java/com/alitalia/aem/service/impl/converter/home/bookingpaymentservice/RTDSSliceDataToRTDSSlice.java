package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ArrayOfresultTicketingDetailSolutionSliceSegment;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionSlice;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceDataToRTDSSlice.class)
public class RTDSSliceDataToRTDSSlice implements
		Converter<ResultTicketingDetailSolutionSliceData, ResultTicketingDetailSolutionSlice> {

	@Reference
	private RTDSSliceExtDataToRTDSSliceExt rtdsExtFieldConverter;

	@Reference
	private RTDSSliceSegmentDataToRTDSSliceSegment rtdsSegmentFieldConverter;

	@Override
	public ResultTicketingDetailSolutionSlice convert(ResultTicketingDetailSolutionSliceData source) {
		ResultTicketingDetailSolutionSlice destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionSlice();

			destination.setDurationField(source.getDurationField());
			destination.setDurationFieldSpecified(source.isDurationFieldSpecified());

			destination.setExtField(rtdsExtFieldConverter.convert(source.getExtField()));

			ArrayOfresultTicketingDetailSolutionSliceSegment segmentField = null;
			if (source.getSegmentField() != null &&
					!source.getSegmentField().isEmpty()) {
				segmentField = objectFactory.createArrayOfresultTicketingDetailSolutionSliceSegment();
				for(ResultTicketingDetailSolutionSliceSegmentData sourceSegmentFieldElem : source.getSegmentField())
					segmentField.getResultTicketingDetailSolutionSliceSegment().add(
							rtdsSegmentFieldConverter.convert(sourceSegmentFieldElem));
			}
			destination.setSegmentField(segmentField);

			destination.setStopCountField(source.getStopCountField());
		}

		return destination;
	}

}
