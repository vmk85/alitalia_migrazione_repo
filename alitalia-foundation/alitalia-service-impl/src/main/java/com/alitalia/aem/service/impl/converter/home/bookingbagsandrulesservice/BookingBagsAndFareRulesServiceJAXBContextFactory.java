package com.alitalia.aem.service.impl.converter.home.bookingbagsandrulesservice;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



@Component(immediate=true, metatype=false)
@Service(value=BookingBagsAndFareRulesServiceJAXBContextFactory.class)
public class BookingBagsAndFareRulesServiceJAXBContextFactory {

	private static final Logger logger = LoggerFactory.getLogger(BookingBagsAndFareRulesServiceJAXBContextFactory.class);

	private JAXBContext jaxbContext = null;

	public synchronized JAXBContext getJaxbContext() throws JAXBException {

		if (jaxbContext == null) {
			jaxbContext = JAXBContext.newInstance(
								com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetails.class,
								com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetails.class,
								com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd4.Brand.class
								);
		}

		return jaxbContext;
	}

	@Activate
	private void activate() {
		try {
			jaxbContext = JAXBContext.newInstance(
					com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetails.class,
					com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetails.class,
					com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd4.Brand.class
					);
		} catch (JAXBException e) {
			logger.warn("BookingSearchServiceJAXBContextFactory - cannot instantiate JAXBContext: {}", e.getMessage());
		}
	}
}
