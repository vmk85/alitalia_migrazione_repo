package com.alitalia.aem.service.impl.converter.home.mmbsearchservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.enumerations.MmbApisTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbChannelsEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbMealAllowedEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbRouteStatusEnum;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.MmbRouteData;
import com.alitalia.aem.common.data.home.mmb.MmbTaxData;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.ApisType;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.ArrayOfFlight;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.ArrayOfTax;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.Channels;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.Flight;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.MealAllowed;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.Route;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.RouteStatus;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.Tax;

@Component(immediate=true, metatype=false)
@Service(value=RouteToMmbRouteData.class)
public class RouteToMmbRouteData implements Converter<Route, MmbRouteData> {

	@Reference
	private PassengerToMmbPassengerData passengerConverter;

	@Reference
	private FlightToMmbFlightData flightConverter;	

	@Reference
	private DeepLinkToMmbDeepLinkData deepLinkConverter;

	@Reference
	private TaxToMmbTaxData taxConverter;

	@Override
	public MmbRouteData convert(Route source) {
		MmbRouteData destination = null;

		if (source != null) {
			destination = new MmbRouteData();

			ApisType sourceApisTypeRequired = source.getX003CApisTypeRequiredX003EKBackingField();
			if (sourceApisTypeRequired != null)
				destination.setApisTypeRequired(MmbApisTypeEnum.fromValue(sourceApisTypeRequired.value()));

			destination.setCartFastTrackBox(source.isX003CCartFastTrackBoxX003EKBackingField());
			destination.setCartVipLoungeBox(source.isX003CCartVipLoungeBoxX003EKBackingField());

			Channels sourceChannel = source.getX003CChannelX003EKBackingField();
			if (sourceChannel != null)
				destination.setChannel(MmbChannelsEnum.fromValue(sourceChannel.value()));

			destination.setCheckinEnabled(source.isX003CCheckinEnabledX003EKBackingField());
			destination.setConfirmed(source.isX003CConfirmedX003EKBackingField());
			destination.setCreateDateTime(source.getX003CCreateDateTimeX003EKBackingField());
			
			XMLGregorianCalendar sourceDateTimePaymentLimit = source.getX003CDateTimePaymentLimitX003EKBackingField();
			if (sourceDateTimePaymentLimit != null)
				destination.setDateTimePaymentLimit(sourceDateTimePaymentLimit.toGregorianCalendar());

			destination.setId(source.getX003CIdX003EKBackingField());
			destination.setMealAvailable(source.isX003CMealAvailableX003EKBackingField());

			MealAllowed sourceMealAllowed = source.getX003CMealAllowedX003EKBackingField();
			if (sourceMealAllowed != null)
				destination.setMealAllowed(MmbMealAllowedEnum.fromValue(sourceMealAllowed.value()));

			destination.setPnr(source.getX003CPnrX003EKBackingField());
			destination.setSelected(source.isX003CSelectedX003EKBackingField());
			destination.setUndoEnabled(source.isX003CUndoEnabledX003EKBackingField());
			destination.setVisibleApis(source.isX003CVisibleApisX003EKBackingField());

			RouteStatus sourceStatus = source.getX003CStatusX003EKBackingField();
			if (sourceStatus != null)
				destination.setStatus(MmbRouteStatusEnum.fromValue(sourceStatus.value()));

			destination.setApplicantPassenger(passengerConverter.convert(source.getX003CApplicantPassengerX003EKBackingField()));

			destination.setDeepLink(deepLinkConverter.convert(source.getX003CDeepLinkX003EKBackingField()));

			ArrayOfFlight sourceFlights = source.getX003CFlightsX003EKBackingField();
			if (sourceFlights != null &&
					sourceFlights.getFlight() != null &&
					!sourceFlights.getFlight().isEmpty()) {
				List<MmbFlightData> flights = new ArrayList<MmbFlightData>();
				for(Flight sourceFlight : sourceFlights.getFlight()) {
					flights.add(flightConverter.convert(sourceFlight));
				}
				destination.setFlights(flights);
			}

			ArrayOfTax sourceTaxes = source.getX003CTaxesX003EKBackingField();
			if (sourceTaxes != null &&
					sourceTaxes.getTax()!= null &&
					!sourceTaxes.getTax().isEmpty()) {
				List<MmbTaxData> taxes = new ArrayList<MmbTaxData>();
				for (Tax sourceTax : sourceTaxes.getTax()) {
					taxes.add(taxConverter.convert(sourceTax));
				}
				destination.setTaxes(taxes);
			}
		}

		return destination;
	}

}
