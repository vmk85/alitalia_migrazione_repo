package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.enumerations.TipoProfiloEnum;
import com.alitalia.aem.common.messages.home.GetDatiSicurezzaProfiloResponseLocal;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.GetDatiSicurezzaProfiloResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.TipoProfilo;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

@Component(immediate = true, metatype = false)
@Service(value = GetDatiSicurezzaProfiloResponseToLocalConverter.class)
public class GetDatiSicurezzaProfiloResponseToLocalConverter implements Converter<GetDatiSicurezzaProfiloResponse,GetDatiSicurezzaProfiloResponseLocal > {

	@Override
	public GetDatiSicurezzaProfiloResponseLocal convert( GetDatiSicurezzaProfiloResponse source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");

		GetDatiSicurezzaProfiloResponseLocal destination = new GetDatiSicurezzaProfiloResponseLocal();

		if (source != null) {

			destination.setAccessFailedCount(source.getAccessFailedCount().getValue());

			destination.setCellulare(source.getCertifiedPhoneAccount().getValue());
			destination.setPrefissoNazioneCellulare(source.getCertifiedCountryNumber().getValue());

			destination.setEmailAccount(source.getCertifiedEmailAccount().getValue());
			destination.setFlagIDProfiloExists(source.isFlagIDProfiloExists());
			destination.setFlagRememberPassword(source.isFlagRememberPassword());
			destination.setFlagVerificaCellulareOK(source.isPhoneCertificateFlag());
			destination.setFlagVerificaEmailAccountOK(source.isEmailCertificateFlag());
			destination.setFlagVerificaPasswordOK(source.isFlagVerificaPassword());
			destination.setFlagVerificaRispostaOK(source.isFlagVerificaRisposta());
			destination.setFlagVerificaUserNameOK(source.isFlagVerificaUsername());
			destination.setLockedout(source.isLockedout());
			destination.setPassword(source.getPassword().getValue());
			destination.setSecureAnswer(source.getSecureAnswer().getValue());
			destination.setSecureQuestionID(source.getSecureQuestionID().intValue());
			//destination.setSecureQuestionCode(source.getSecureQuestionCode().getValue());
			destination.setUsername(source.getUsername().getValue());


			TipoProfilo sourceTipoProfilo = source.getTipoProfilo();
			if (sourceTipoProfilo != null)
				destination.setTipoProfilo(TipoProfiloEnum.fromValue(sourceTipoProfilo.value()));

		}
		return destination;
	}
}