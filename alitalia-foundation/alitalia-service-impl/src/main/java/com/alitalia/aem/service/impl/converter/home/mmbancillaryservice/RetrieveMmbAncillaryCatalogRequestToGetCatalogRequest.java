package com.alitalia.aem.service.impl.converter.home.mmbancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryFlightData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryPassengerData;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCatalogRequest;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.GetCatalogRequest;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.ArrayOfFlight;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.ArrayOfPassenger;

@Component(immediate=true, metatype=false)
@Service(value=RetrieveMmbAncillaryCatalogRequestToGetCatalogRequest.class)
public class RetrieveMmbAncillaryCatalogRequestToGetCatalogRequest implements Converter<RetrieveMmbAncillaryCatalogRequest, GetCatalogRequest> {

	@Reference
	private MmbRequestBaseInfoDataToRequestBaseInfo mmbRequestBaseInfoDataConverter;

	@Reference
	private MmbAncillaryFlightDataToFlight mmbAncillaryFlightDataConverter;

	@Reference
	private MmbAncillaryPassengerDataToPassenger mmbAncillaryPassengerDataConverter;

	@Override
	public GetCatalogRequest convert(RetrieveMmbAncillaryCatalogRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.ObjectFactory objectFactory2 =
				new com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.ObjectFactory();
		GetCatalogRequest destination = null;

		if (source != null) {
			destination = objectFactory.createGetCatalogRequest();

			destination.setClient(source.getClient());
			destination.setX003CInfoRequestX003EKBackingField(
					mmbRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			destination.setX003CPNRX003EKBackingField(source.getPnr());

			destination.setLanguage(objectFactory.createGetCatalogRequestLanguage(source.getLanguage()));
			destination.setMachineName(objectFactory.createGetCatalogRequestMachineName(source.getMachineName()));
			destination.setMarket(objectFactory.createGetCatalogRequestMarket(source.getMarket()));
			destination.setSessionId(objectFactory.createGetCatalogRequestSessionId(source.getSessionId()));

			ArrayOfFlight flights = objectFactory2.createArrayOfFlight();
			for (MmbAncillaryFlightData sourceAncillaryFlightData : source.getFlights()) {
				flights.getFlight().add(mmbAncillaryFlightDataConverter.convert(sourceAncillaryFlightData));
			}
			destination.setFlights(objectFactory.createGetCatalogRequestFlights(flights));

			ArrayOfPassenger passengers = objectFactory2.createArrayOfPassenger();
			for (MmbAncillaryPassengerData sourceAncillaryPassengerData : source.getPassengers()) {
				passengers.getPassenger().add(mmbAncillaryPassengerDataConverter.convert(sourceAncillaryPassengerData));
			}
			destination.setPassengers(objectFactory.createGetCatalogRequestPassengers(passengers));
		}

		return destination;
	}

}
