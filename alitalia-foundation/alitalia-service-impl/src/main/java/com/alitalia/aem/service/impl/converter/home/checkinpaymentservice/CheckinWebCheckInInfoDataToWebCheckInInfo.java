package com.alitalia.aem.service.impl.converter.home.checkinpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinWebCheckInInfoData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.checkin.paymentservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.checkin.paymentservice.xsd2.WebCheckInInfo;


@Component(immediate=true, metatype=false)
@Service(value=CheckinWebCheckInInfoDataToWebCheckInInfo.class)
public class CheckinWebCheckInInfoDataToWebCheckInInfo implements Converter<CheckinWebCheckInInfoData, WebCheckInInfo> {

	@Override
	public WebCheckInInfo convert(CheckinWebCheckInInfoData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		WebCheckInInfo destination = null;

		if (source != null) {
			destination = objectFactory.createWebCheckInInfo();

			destination.setX003CIdX003EKBackingField(source.getId());
			destination.setX003CAptArrivalCodeX003EKBackingField(source.getAptArrivalCode());
			destination.setX003CAptDepartureCodeX003EKBackingField(source.getAptDepartureCode());
			destination.setX003CArrivalDateX003EKBackingField(XsdConvertUtils.toXMLGregorianCalendar(source.getArrivalDate()));
			destination.setX003CArrivalTimeX003EKBackingField(XsdConvertUtils.toXMLGregorianCalendar(source.getArrivalTime()));
			if(source.getCouponId() != null){
				destination.setX003CCouponIdX003EKBackingField(source.getCouponId());
			}
			destination.setX003CDepartureDateX003EKBackingField(XsdConvertUtils.toXMLGregorianCalendar(source.getDepartureDate()));
			destination.setX003CDepartureTimeX003EKBackingField(XsdConvertUtils.toXMLGregorianCalendar(source.getDepartureTime()));
			destination.setX003CSelectedPassengerEticketX003EKBackingField(source.getSelectedPassengerEticket());
		}

		return destination;
	}

}
