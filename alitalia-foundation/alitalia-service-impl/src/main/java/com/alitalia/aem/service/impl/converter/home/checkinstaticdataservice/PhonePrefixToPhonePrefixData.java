package com.alitalia.aem.service.impl.converter.home.checkinstaticdataservice;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PhonePrefixData;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd2.InternationalPhonePrefix;

@Component(immediate=true, metatype=false)
@Service(value=PhonePrefixToPhonePrefixData.class)
public class PhonePrefixToPhonePrefixData implements Converter<InternationalPhonePrefix, PhonePrefixData> {

	@Override
	public PhonePrefixData convert(InternationalPhonePrefix source) {
		
		PhonePrefixData prefixData = new PhonePrefixData();
		prefixData.setCode(String.valueOf(source.getX003CCountryIdX003EKBackingField()));
		prefixData.setDescription(source.getX003CCountryNameX003EKBackingField());
		prefixData.setPrefix(source.getX003CPrefixX003EKBackingField());
		return prefixData;
		
	}
}
