package com.alitalia.aem.service.impl.converter.home.newsletterservice;

import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.NLUserData;
import com.alitalia.aem.ws.news.newsletterservice.xsd2.ArrayOfNLUser;
import com.alitalia.aem.ws.news.newsletterservice.xsd2.NLUser;
import com.alitalia.aem.ws.news.newsletterservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.news.newsletterservice.xsd3.InsertStatus;

@Component(immediate = true, metatype = false)
@Service(value = ListOfNLUserDataToArrayOfNLUser.class)
public class ListOfNLUserDataToArrayOfNLUser implements Converter<List<NLUserData>, ArrayOfNLUser> {

	@Override
	public ArrayOfNLUser convert(List<NLUserData> source) {
		ObjectFactory objectFactory = new ObjectFactory();

		ArrayOfNLUser users = objectFactory.createArrayOfNLUser();

		if(source != null){
			for(NLUserData userData: source){

				NLUser user = objectFactory.createNLUser();

				JAXBElement<String> userEmail = objectFactory.createNLUserEmail(userData.getEmail());
				JAXBElement<String> lastName = objectFactory.createNLUserLastname(userData.getLastname());
				JAXBElement<String> name = objectFactory.createNLUserName(userData.getName());

				user.setEmail(userEmail);
				user.setLastname(lastName);
				user.setName(name);
				user.setRole(userData.getRole());
				
				if(userData.getStatus() != null){
					user.setSatus(InsertStatus.fromValue(userData.getStatus().value()));
				}

				users.getNLUser().add(user);
			}
		}

		return users;
	}
}