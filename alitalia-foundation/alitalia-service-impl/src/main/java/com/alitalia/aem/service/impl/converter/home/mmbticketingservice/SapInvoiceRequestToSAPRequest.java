package com.alitalia.aem.service.impl.converter.home.mmbticketingservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.TicketData;
import com.alitalia.aem.common.messages.home.SapInvoiceRequest;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd4.ArrayOfticket;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd6.ObjectFactory;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd6.SAPRequest;

@Component(immediate=true, metatype= false)
@Service(value=SapInvoiceRequestToSAPRequest.class)
public class SapInvoiceRequestToSAPRequest implements Converter<SapInvoiceRequest, SAPRequest> {

	@Reference
	private TicketDataToTicket ticketDataConverter;

	@Override
	public SAPRequest convert(SapInvoiceRequest source) {
		ObjectFactory objectFactory6 = new ObjectFactory();
		com.alitalia.aem.ws.mmb.ticketingservice.xsd4.ObjectFactory objectFactory4 =
				new com.alitalia.aem.ws.mmb.ticketingservice.xsd4.ObjectFactory();

		SAPRequest destination = objectFactory6.createSAPRequest();

		destination.setCap(objectFactory6.createSAPRequestCap(source.getCap()));
		destination.setCfInt(objectFactory6.createSAPRequestCfInt(source.getPartitaIva()));
		destination.setCfPax(objectFactory6.createSAPRequestCfPax(
				source.getCodiceFiscale() == null ? "" : source.getCodiceFiscale()));
		destination.setEmailInt(objectFactory6.createSAPRequestEmailInt(source.getEmailIntestatario()));

		String enteEmittente = source.getEnteEmittente();
		if (enteEmittente != null)
			destination.setEnteEmit(objectFactory6.createSAPRequestEnteEmit(enteEmittente));
		else
			destination.setEnteEmit(objectFactory6.createSAPRequestEnteEmit(""));

		String enteRichiedente = source.getEnteRichiedente();
		if (enteRichiedente != null)
			destination.setEnteRich(objectFactory6.createSAPRequestEnteRich(enteRichiedente));
		else
			destination.setEnteRich(objectFactory6.createSAPRequestEnteRich(""));

		destination.setIndSped(objectFactory6.createSAPRequestIndSped(source.getIndirizzoSpedizione()));
		destination.setIntFattura(objectFactory6.createSAPRequestIntFattura(source.getIntestatarioFattura()));
		destination.setLocSped(objectFactory6.createSAPRequestLocSped(source.getLocalitaSpedizione()));
		destination.setNome(objectFactory6.createSAPRequestNome(source.getNome()));
		destination.setPaese(objectFactory6.createSAPRequestPaese(source.getPaese()));
		
		destination.setPivaInt(objectFactory6.createSAPRequestPivaInt(source.getPartitaIva()));

		destination.setProv(objectFactory6.createSAPRequestProv(source.getProvincia()));

		String societa = source.getSocieta();
		if (societa != null)
			destination.setSocieta(objectFactory6.createSAPRequestSocieta(societa));
		else
			destination.setSocieta(objectFactory6.createSAPRequestSocieta(""));

		ArrayOfticket tickets = null;
		if (source.getBiglietti() != null && !source.getBiglietti().isEmpty()) {
			tickets = objectFactory4.createArrayOfticket();
			for (TicketData sourceTicketData : source.getBiglietti()) {
				tickets.getTicket().add(ticketDataConverter.convert(sourceTicketData));
			}
		}
		destination.setTickets(objectFactory6.createSAPRequestTickets(tickets));

		String tipoRichiesta = source.getTipoRichiesta();
		if (tipoRichiesta != null)
			destination.setTipoRichiesta(objectFactory6.createSAPRequestTipoRichiesta(tipoRichiesta));
		else
			destination.setTipoRichiesta(objectFactory6.createSAPRequestTipoRichiesta(""));

		return destination;
	}

}
