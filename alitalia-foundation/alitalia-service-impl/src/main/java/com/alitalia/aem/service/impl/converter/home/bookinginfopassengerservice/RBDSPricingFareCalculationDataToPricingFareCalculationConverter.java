package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareCalculationData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ArrayOfresultBookingDetailsSolutionPricingFareCalculationBoxedTax;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ResultBookingDetailsSolutionPricingFareCalculation;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd5.ArrayOfstring;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingFareCalculationDataToPricingFareCalculationConverter.class)
public class RBDSPricingFareCalculationDataToPricingFareCalculationConverter
		implements Converter<ResultBookingDetailsSolutionPricingFareCalculationData, ResultBookingDetailsSolutionPricingFareCalculation> {

	@Reference
	private RBDSPricingFareCalculationBoxedTaxDataToPricingFareCalculationBoxedTax pricingFareCalculationBoxedTaxConverter;
	
	@Override
	public ResultBookingDetailsSolutionPricingFareCalculation convert(
			ResultBookingDetailsSolutionPricingFareCalculationData source) {
		ResultBookingDetailsSolutionPricingFareCalculation destination = null;
		if(source != null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionPricingFareCalculation();
			
			
			ArrayOfresultBookingDetailsSolutionPricingFareCalculationBoxedTax boxedTaxField = objfact.createArrayOfresultBookingDetailsSolutionPricingFareCalculationBoxedTax();
			
			if(source.getBoxedTaxField()!=null)
			for(ResultBookingDetailsSolutionPricingFareCalculationBoxedTaxData s : source.getBoxedTaxField()){
				boxedTaxField.getResultBookingDetailsSolutionPricingFareCalculationBoxedTax().add(pricingFareCalculationBoxedTaxConverter.convert(s));
			}
			destination.setBoxedTaxField(boxedTaxField);
			
			com.alitalia.aem.ws.booking.infopassengerservice.xsd5.ObjectFactory obj = new com.alitalia.aem.ws.booking.infopassengerservice.xsd5.ObjectFactory();
			ArrayOfstring lineField = obj.createArrayOfstring();
			
			if(source.getLineField()!=null)
			for(String s : source.getLineField())
				lineField.getString().add(s);
			destination.setLineField(lineField);
		}
		return destination;
	}

}
