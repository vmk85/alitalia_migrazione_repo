package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSaleTaxTotalData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailSolutionSaleTaxTotal;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSaleTaxTotalToRTDSSaleTaxTotalData.class)
public class RTDSSaleTaxTotalToRTDSSaleTaxTotalData implements
		Converter<ResultTicketingDetailSolutionSaleTaxTotal, ResultTicketingDetailSolutionSaleTaxTotalData> {

	@Override
	public ResultTicketingDetailSolutionSaleTaxTotalData convert(ResultTicketingDetailSolutionSaleTaxTotal source) {
		ResultTicketingDetailSolutionSaleTaxTotalData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionSaleTaxTotalData();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
