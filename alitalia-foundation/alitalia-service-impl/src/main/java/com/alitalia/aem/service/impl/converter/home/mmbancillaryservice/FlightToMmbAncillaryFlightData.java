package com.alitalia.aem.service.impl.converter.home.mmbancillaryservice;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.enumerations.MmbFlightTypeEnum;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryFlightData;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.Flight;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.FlightType;

@Component(immediate=true, metatype=false)
@Service(value=FlightToMmbAncillaryFlightData.class)
public class FlightToMmbAncillaryFlightData implements Converter<Flight, MmbAncillaryFlightData> {

	@Override
	public MmbAncillaryFlightData convert(Flight source) {
		MmbAncillaryFlightData destination = null;

		if (source != null) {
			destination = new MmbAncillaryFlightData();

			destination.setCarrier(source.getX003CCarrierX003EKBackingField());

			XMLGregorianCalendar sourceDepartureDate = source.getX003CDepartureDateX003EKBackingField();
			if (sourceDepartureDate != null)
				destination.setDepartureDate(sourceDepartureDate.toGregorianCalendar());

			destination.setDestination(source.getX003CDestinationX003EKBackingField());

			FlightType sourceFlightType = source.getX003CFlightTypeX003EKBackingField();
			if (sourceFlightType != null)
				destination.setFlightType(MmbFlightTypeEnum.fromValue(sourceFlightType.value()));

			destination.setId(source.getX003CIdX003EKBackingField());
			destination.setNumber(source.getX003CNumberX003EKBackingField());
			destination.setOperationalFlightCarrier(source.getX003COperationalFlightCarrierX003EKBackingField());
			destination.setOperationalFlightNumber(source.getX003COperationalFlightNumberX003EKBackingField());
			destination.setOrigin(source.getX003COriginX003EKBackingField());
			destination.setReferenceNumber(source.getX003CReferenceNumberX003EKBackingField());
		}

		return destination;
	}

}
