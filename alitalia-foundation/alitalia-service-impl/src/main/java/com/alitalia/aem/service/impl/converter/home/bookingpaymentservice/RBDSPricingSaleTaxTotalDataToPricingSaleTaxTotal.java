package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingSaleTaxTotalData;
import com.alitalia.aem.ws.booking.paymentservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd11.ResultBookingDetailsSolutionPricingSaleTaxTotal;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingSaleTaxTotalDataToPricingSaleTaxTotal.class)
public class RBDSPricingSaleTaxTotalDataToPricingSaleTaxTotal implements
		Converter<ResultBookingDetailsSolutionPricingSaleTaxTotalData, ResultBookingDetailsSolutionPricingSaleTaxTotal> {

	@Override
	public ResultBookingDetailsSolutionPricingSaleTaxTotal convert(
			ResultBookingDetailsSolutionPricingSaleTaxTotalData source) {
		ResultBookingDetailsSolutionPricingSaleTaxTotal destination = null;
		if(source != null){
			ObjectFactory objf = new ObjectFactory();
			destination = objf.createResultBookingDetailsSolutionPricingSaleTaxTotal();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}

}
