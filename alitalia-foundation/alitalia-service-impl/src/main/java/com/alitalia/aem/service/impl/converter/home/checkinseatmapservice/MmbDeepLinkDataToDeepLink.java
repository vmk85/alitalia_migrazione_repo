package com.alitalia.aem.service.impl.converter.home.checkinseatmapservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.MmbDeepLinkData;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd2.DeepLink;


@Component(immediate=true, metatype=false)
@Service(value=MmbDeepLinkDataToDeepLink.class)
public class MmbDeepLinkDataToDeepLink implements Converter<MmbDeepLinkData, DeepLink> {

	@Override
	public DeepLink convert(MmbDeepLinkData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		DeepLink destination = null;

		if (source != null) {
			destination = objectFactory.createDeepLink();
			destination.setX003CCodeX003EKBackingField(source.getCode());
			destination.setX003CDescriptionX003EKBackingField(source.getDescription());
			destination.setX003CIdX003EKBackingField(source.getId());
			destination.setX003CUrlX003EKBackingField(source.getUrl());
		}
		return destination;
	}

}
