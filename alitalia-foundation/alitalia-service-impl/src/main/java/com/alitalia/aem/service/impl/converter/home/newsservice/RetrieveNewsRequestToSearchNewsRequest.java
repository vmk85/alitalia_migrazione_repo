package com.alitalia.aem.service.impl.converter.home.newsservice;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveNewsRequest;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.news.newsservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.news.newsservice.xsd2.SearchNewsRequest;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveNewsRequestToSearchNewsRequest.class)
public class RetrieveNewsRequestToSearchNewsRequest implements Converter<RetrieveNewsRequest, SearchNewsRequest> {

	@Override
	public SearchNewsRequest convert(RetrieveNewsRequest source) {
		
		SearchNewsRequest request = new SearchNewsRequest();
		
		ObjectFactory objectFactory = new ObjectFactory();
		
		String category = source.getCategory();
		XMLGregorianCalendar from = XsdConvertUtils.toXMLGregorianCalendar(source.getDateFrom());
		XMLGregorianCalendar to = XsdConvertUtils.toXMLGregorianCalendar(source.getDateTo());
		boolean isPubTest = source.isPubTest();
		String title = source.getTitle();
		
		request.setCategory(objectFactory.createSearchNewsRequestCategory(category));
		request.setDateFrom(objectFactory.createSearchNewsRequestDateFrom(from));
		request.setDateTo(objectFactory.createSearchNewsRequestDateTo(to));
		request.setPubTest(isPubTest);
		request.setTitle(objectFactory.createSearchNewsRequestTitle(title));
		
		return request;
	}
}