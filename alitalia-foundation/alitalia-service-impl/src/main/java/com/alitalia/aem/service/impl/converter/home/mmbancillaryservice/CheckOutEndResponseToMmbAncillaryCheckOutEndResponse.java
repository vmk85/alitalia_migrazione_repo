package com.alitalia.aem.service.impl.converter.home.mmbancillaryservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.MmbAncillaryErrorData;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutEndResponse;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.CheckOutEndResponse;

@Component(immediate=true, metatype=false)
@Service(value=CheckOutEndResponseToMmbAncillaryCheckOutEndResponse.class)
public class CheckOutEndResponseToMmbAncillaryCheckOutEndResponse implements Converter<CheckOutEndResponse, MmbAncillaryCheckOutEndResponse> {

	@Reference
	private ErrorToMmbAncillaryErrorData errorConverter;

	@Override
	public MmbAncillaryCheckOutEndResponse convert(CheckOutEndResponse source) {
		MmbAncillaryCheckOutEndResponse destination = null;

		if (source != null) {
			destination = new MmbAncillaryCheckOutEndResponse();

			JAXBElement<String> sourceMachineName = source.getMachineName();
			if (sourceMachineName != null)
				destination.setMachine(sourceMachineName.getValue());

			if (source.getErrors() != null &&
					source.getErrors().getValue() != null &&
					source.getErrors().getValue().getAnyType() != null &&
					!source.getErrors().getValue().getAnyType().isEmpty()) {
				List<Object> sourceErrors = source.getErrors().getValue().getAnyType();
				List<MmbAncillaryErrorData> errors = new ArrayList<MmbAncillaryErrorData>();
				for (Object sourceError : sourceErrors) {
					errors.add(errorConverter.convert(sourceError));
				}
				destination.setErrors(errors);
			}
		}

		return destination;
	}

}
