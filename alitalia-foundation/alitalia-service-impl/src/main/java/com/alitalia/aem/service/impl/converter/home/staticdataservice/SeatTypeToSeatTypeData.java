package com.alitalia.aem.service.impl.converter.home.staticdataservice;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.SeatTypeData;
import com.alitalia.aem.ws.booking.staticdataservice.xsd5.SeatType;

@Component(immediate=true, metatype=false)
@Service(value=SeatTypeToSeatTypeData.class)
public class SeatTypeToSeatTypeData implements Converter<SeatType, SeatTypeData> {

	@Override
	public SeatTypeData convert(SeatType source) {
		SeatTypeData seatTypeData = new SeatTypeData();
		seatTypeData.setCode(source.getCode().getValue());
		seatTypeData.setDescription(source.getDescription().getValue());
		return seatTypeData;
	}

}
