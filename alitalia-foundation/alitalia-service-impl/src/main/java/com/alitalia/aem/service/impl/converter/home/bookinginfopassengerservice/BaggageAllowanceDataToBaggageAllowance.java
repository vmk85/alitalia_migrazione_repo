package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.BaggageAllowanceData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.BaggageAllowance;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=BaggageAllowanceDataToBaggageAllowance.class)
public class BaggageAllowanceDataToBaggageAllowance implements Converter<BaggageAllowanceData, BaggageAllowance> {

	@Override
	public BaggageAllowance convert(BaggageAllowanceData source) {
		ObjectFactory factory = new ObjectFactory();
		BaggageAllowance destination = factory.createBaggageAllowance();

		destination.setCode(factory.createBaggageAllowanceCode(source.getCode()));
		destination.setCount(source.getCount());

		return destination;
	}

}
