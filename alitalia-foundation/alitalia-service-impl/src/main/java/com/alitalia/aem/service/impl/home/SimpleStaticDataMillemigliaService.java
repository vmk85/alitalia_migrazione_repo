package com.alitalia.aem.service.impl.home;

import com.alitalia.aem.common.messages.home.RetrieveSecureQuestionListRequest;
import com.alitalia.aem.common.messages.home.RetrieveSecureQuestionListResponse;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.SecureQuestionStaticDataResponseToRetrieveSecureQuestionListResponseConverter;
import com.alitalia.aem.ws.mypersonalarea.staticdataservice.xsd1.SecureQuestionStaticDataResponse;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.RetrieveStateListRequest;
import com.alitalia.aem.common.messages.home.RetrieveStateListResponse;
import com.alitalia.aem.service.api.home.StaticDataMillemigliaService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.RetrieveStateListRequestToStaticDataRequest;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.StateStaticDataResponseToRetrieveStateListResponseConverter;
import com.alitalia.aem.ws.mypersonalarea.staticdataservice.StaticDataServiceMillemigliaClient;
import com.alitalia.aem.ws.mypersonalarea.staticdataservice.wsdl.IStaticDataServiceGetStateListServiceFaultFaultFaultMessage;
import com.alitalia.aem.ws.mypersonalarea.staticdataservice.xsd1.StateStaticDataResponse;
import com.alitalia.aem.ws.mypersonalarea.staticdataservice.xsd1.StaticDataRequest;
import com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice.RetrieveSecureQuestionListRequestToStaticDataRequest;

@Service
@Component(immediate=true, metatype=false)
public class SimpleStaticDataMillemigliaService implements StaticDataMillemigliaService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleStaticDataMillemigliaService.class);
	
	@Reference
	private StaticDataServiceMillemigliaClient staticDataServiceClient;
	
	@Reference
	private RetrieveStateListRequestToStaticDataRequest retrieveStateListRequestConverter;

	@Reference
	private RetrieveSecureQuestionListRequestToStaticDataRequest retrieveSecureQuestionListRequestConverter;
	
	@Reference
	private StateStaticDataResponseToRetrieveStateListResponseConverter stateStaticDataResponseConverter;

	@Reference
	private SecureQuestionStaticDataResponseToRetrieveSecureQuestionListResponseConverter secureQuestionStaticDataResponseConverter;


	@Override
	public RetrieveStateListResponse retrieveStatesList(RetrieveStateListRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("Executing method retrieveStatesList. The request is {}", request);
		
		try {
			
			RetrieveStateListResponse response = new RetrieveStateListResponse();
			StaticDataRequest staticDataRequest = retrieveStateListRequestConverter.convert(request);
			StateStaticDataResponse staticDataResponse = staticDataServiceClient.getStateList(staticDataRequest);
			response = stateStaticDataResponseConverter.convert(staticDataResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if(logger.isDebugEnabled())
			logger.debug("Executed method. The response is {}", response);
			return response;
			
		} catch (Exception e) {
			logger.error("Error while executin the method.", e);
			throw new AlitaliaServiceException("Exception executing retrieveStatesList: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public RetrieveSecureQuestionListResponse retrieveSecureQuestionList(RetrieveSecureQuestionListRequest request) {
		if(logger.isDebugEnabled())
			logger.debug("Executing method retrieveSecureQuestionList. The request is {}", request);

		try {

			RetrieveSecureQuestionListResponse response = new RetrieveSecureQuestionListResponse();
			//StaticDataRequest staticDataRequest = retrieveStateListRequestConverter.convert(request);
			StaticDataRequest staticDataRequest = retrieveSecureQuestionListRequestConverter.convert(request);

			SecureQuestionStaticDataResponse staticDataResponse = staticDataServiceClient.getSecureQuestionList(staticDataRequest);
			response = secureQuestionStaticDataResponseConverter.convert(staticDataResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if(logger.isDebugEnabled())
				logger.debug("Executed method. The response is {}", response);
			return response;

		} catch (Exception e) {
			logger.error("Error while executin the method.", e);
			throw new AlitaliaServiceException("Exception executing retrieveSecureQuestionList: Sid ["+request.getSid()+"]" , e);
		}
	}
}