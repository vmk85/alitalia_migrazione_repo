package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetailsSolutionPricingFare;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd6.ArrayOfstring;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingFareDataToPricingFare.class)
public class RBDSPricingFareDataToPricingFare implements Converter<ResultBookingDetailsSolutionPricingFareData, ResultBookingDetailsSolutionPricingFare> {

	@Override
	public ResultBookingDetailsSolutionPricingFare convert(
			ResultBookingDetailsSolutionPricingFareData source) {
		ResultBookingDetailsSolutionPricingFare destination = null;
		if(source!=null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionPricingFare();
			destination.setCarrierField(source.getCarrierField());
			destination.setDestinationCityField(source.getDestinationCityField());
			destination.setExtendedFareCodeField(source.getExtendedFareCodeField());
			destination.setOriginCityField(source.getOriginCityField());
			
			com.alitalia.aem.ws.bookingaward.searchservice.xsd6.ObjectFactory obj = new com.alitalia.aem.ws.bookingaward.searchservice.xsd6.ObjectFactory();
			ArrayOfstring ptcField = obj.createArrayOfstring();
			if(source.getPtcField()!=null)
			for(String s : source.getPtcField())
				ptcField.getString().add(s);
			destination.setPtcField(ptcField);
			
			destination.setTagField(source.getTagField());
			destination.setTicketDesignatorField(source.getTicketDesignatorField());		}
		return destination;
	}

}
