package com.alitalia.aem.service.impl.converter.home.businessloginservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.EmailMessage;
import com.alitalia.aem.common.data.home.enumerations.MailPriorityEnum;
import com.alitalia.aem.ws.sendmail.xsd.MailPriority;
import com.alitalia.aem.ws.sendmail.xsd.MessaggioEmail;
import com.alitalia.aem.ws.sendmail.xsd.ObjectFactory;

/**
 * @author R.Capitini
 *
 */
@Component
@Service(value=EmailMessageToMessaggioEmail.class)
public class EmailMessageToMessaggioEmail implements Converter<EmailMessage, MessaggioEmail> {

	@Override
	public MessaggioEmail convert(EmailMessage source) {
		ObjectFactory factory = new ObjectFactory();
		MessaggioEmail destination = factory.createMessaggioEmail();

		destination.setBcc(source.getBcc());
		destination.setCc(source.getCc());
		destination.setFrom(source.getFrom());
		destination.setIsBodyHtml(source.isBodyHtml());
	    destination.setMessageText(source.getMessageText());
	    
	    if (source.getDeliveryNotification() != null) {
	    	destination.getDeliveryNotification().add(source.getDeliveryNotification().value());
	    }
	    
		MailPriorityEnum priority = source.getPriority();
		if (priority != null)
			destination.setPriority(MailPriority.fromValue(priority.value()));
		else
			destination.setPriority(null);

		destination.setSubject(source.getSubject());
		destination.setTo(source.getTo());
		destination.setUNCAllegato(source.getUNCAllegato());

		return destination;
	}

}
