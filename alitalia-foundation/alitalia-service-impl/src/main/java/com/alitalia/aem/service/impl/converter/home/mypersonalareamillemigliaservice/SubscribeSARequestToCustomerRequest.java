package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import com.alitalia.aem.common.converter.Converter;

import com.alitalia.aem.common.messages.home.SubscribeSARequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.CustomerRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.CustomerSARequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.GetCreditCardListRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;
//import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfileSA;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import javax.xml.bind.JAXBElement;

@Component(immediate = true, metatype = false)
@Service(value = SubscribeSARequestToCustomerRequest.class)
public class SubscribeSARequestToCustomerRequest implements Converter<
		com.alitalia.aem.common.messages.home.SubscribeSARequest,
		com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SubscribeSARequest> {

	@Reference
	private MMCustomerProfileDataToCustomerProfileSA mmCustomerProfileDataConverter;
	
	@Override
	public com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SubscribeSARequest convert(com.alitalia.aem.common.messages.home.SubscribeSARequest source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		
		ObjectFactory objectFactory = new ObjectFactory();
//
//        ////// verificare qui /////////////////////
//
//		com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SubscribeSARequest destination = objectFactory.createSubscribeSARequest();
//
//		CustomerProfileSA customerProfile = mmCustomerProfileDataConverter.convert(source.getCustomerProfile());
//
//		destination.setCustomerSA(
//				objectFactory.createCustomerSARequestCustomerSA(
//						customerProfile
//				)
//		);


		com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SubscribeSARequest destination = objectFactory.createSubscribeSARequest();
		CustomerProfileSA customerProfile = mmCustomerProfileDataConverter.convert(source.getCustomerProfile());

		JAXBElement<CustomerProfileSA> jaxbCustomerProfile = objectFactory.createCustomerSARequestCustomerSA(customerProfile);
		destination.setCustomerSA(jaxbCustomerProfile);

		return destination;
	}
}