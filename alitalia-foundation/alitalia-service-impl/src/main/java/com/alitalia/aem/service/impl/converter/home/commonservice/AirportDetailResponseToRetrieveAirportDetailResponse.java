package com.alitalia.aem.service.impl.converter.home.commonservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveAirportDetailsResponse;
import com.alitalia.aem.ws.booking.commonservice.xsd1.AirportDetailResponse;

@Component(immediate = true, metatype = false)
@Service(value = AirportDetailResponseToRetrieveAirportDetailResponse.class)
public class AirportDetailResponseToRetrieveAirportDetailResponse 
		implements Converter<AirportDetailResponse, RetrieveAirportDetailsResponse> {
	
	@Reference
	private AirportDetailsToAirportDetailsData airportDetailsDataConverter;
	
	@Override
	public RetrieveAirportDetailsResponse convert(AirportDetailResponse source) {
		
		RetrieveAirportDetailsResponse destination = null;
		
		if (source != null) {
		
			destination = new RetrieveAirportDetailsResponse();
			
			if (source.getDetails() != null) {
				destination.setAirportDetails(
						airportDetailsDataConverter.convert(
								source.getDetails().getValue()));
			}
			
		}
		
		return destination;
	}
	
}
