package com.alitalia.aem.service.impl.converter.home.carnetservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.CarnetRecoveryCodeResponse;
import com.alitalia.aem.ws.carnet.carnetservice.xsd1.RecoveryCarnetCodeResponse;

@Component(immediate=true, metatype=false)
@Service(value=RecoveryCarnetCodeResponseToCarnetRecoveryCodeResponse.class)
public class RecoveryCarnetCodeResponseToCarnetRecoveryCodeResponse implements Converter<RecoveryCarnetCodeResponse, CarnetRecoveryCodeResponse> {

	@Override
	public CarnetRecoveryCodeResponse convert(RecoveryCarnetCodeResponse source) {
		CarnetRecoveryCodeResponse destination = null;

		if (source != null) {
			destination = new CarnetRecoveryCodeResponse();
		}

		return destination;
	}

}
