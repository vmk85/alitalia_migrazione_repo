package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryCouponData;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryEticketData;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.Coupons;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ETicket;


@Component(immediate=true, metatype=false)
@Service(value=ETicketToCheckinAncillaryEticketData.class)
public class ETicketToCheckinAncillaryEticketData implements Converter<ETicket, CheckinAncillaryEticketData> {

	@Reference
	private CouponsToCheckinAncillaryCouponData couponConverter;

	@Override
	public CheckinAncillaryEticketData convert(ETicket source) {
		CheckinAncillaryEticketData destination = null;

		if (source != null) {
			destination = new CheckinAncillaryEticketData();

			destination.setNumber(source.getX003CNumberX003EKBackingField());
			destination.setPnr(source.getX003CPnrX003EKBackingField());

			List<CheckinAncillaryCouponData> coupons = null;
			if (source.getX003CCouponsX003EKBackingField() != null &&
					source.getX003CCouponsX003EKBackingField().getCoupons()!= null &&
					!source.getX003CCouponsX003EKBackingField().getCoupons().isEmpty()) {
				coupons = new ArrayList<CheckinAncillaryCouponData>();
				List<Coupons> sourceCouponsList = source.getX003CCouponsX003EKBackingField().getCoupons();
				for (Coupons sourceCoupons : sourceCouponsList)
					coupons.add(couponConverter.convert(sourceCoupons));
				
			}
			destination.setCoupons(coupons);
		}

		return destination;
	}

}
