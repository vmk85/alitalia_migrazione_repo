package com.alitalia.aem.service.impl.converter.homerest.restresponseunmarshal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.countries.Countries;
import com.alitalia.aem.common.messages.home.CountriesResponse;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class CountriesUnmarshal {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public CountriesResponse countriesUnmarshal (String jsonString) {
		CountriesResponse response = new CountriesResponse();
		Countries countries = new Countries();
		Gson gson = new Gson();
		
		try {
			countries = gson.fromJson(jsonString, Countries.class);
			response.setCountries(countries);
		}
		catch (JsonSyntaxException e) {
			e.printStackTrace();
			logger.debug("JsonSyntaxException in countriesUnmarshal");
		}
		
		return response;
	}

}
