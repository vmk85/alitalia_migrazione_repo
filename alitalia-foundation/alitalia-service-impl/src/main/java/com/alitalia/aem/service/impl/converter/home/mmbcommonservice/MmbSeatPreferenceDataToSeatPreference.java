package com.alitalia.aem.service.impl.converter.home.mmbcommonservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbUpdateSSREnum;
import com.alitalia.aem.common.data.home.mmb.MmbSeatPreferenceData;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.Cabin;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.UpdateSSR;
import com.alitalia.aem.ws.mmb.commonservice.xsd4.SeatPreference;
import com.alitalia.aem.ws.mmb.commonservice.xsd4.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=MmbSeatPreferenceDataToSeatPreference.class)
public class MmbSeatPreferenceDataToSeatPreference implements Converter<MmbSeatPreferenceData, SeatPreference> {

	@Override
	public SeatPreference convert(MmbSeatPreferenceData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		SeatPreference destination = null;

		if (source != null) {
			destination = objectFactory.createSeatPreference();

			CabinEnum sourceCabinSeat = source.getCabinSeat();
			if (sourceCabinSeat != null)
				destination.setCabinSeat(Cabin.fromValue(sourceCabinSeat.value()));

			destination.setFlightCarrier(objectFactory.createSeatPreferenceFlightCarrier(source.getFlightCarrier()));
			destination.setFlightNumber(objectFactory.createSeatPreferenceFlightNumber(source.getFlightNumber()));
			destination.setIsComfort(source.isComfort());
			destination.setIsPending(source.isPending());
			destination.setNumber(objectFactory.createSeatPreferenceNumber(source.getNumber()));
			destination.setOldNumber(objectFactory.createSeatPreferenceOldNumber(source.getOldNumber()));
			destination.setOldRow(objectFactory.createSeatPreferenceOldRow(source.getOldRow()));
			destination.setRow(objectFactory.createSeatPreferenceRow(source.getRow()));
			destination.setTravelerRefNumber(objectFactory.createSeatPreferenceTravelerRefNumber(source.getTravelerRefNumber()));

			MmbUpdateSSREnum sourceUpdate = source.getUpdate();
			if (sourceUpdate != null)
				destination.setUpdate(UpdateSSR.fromValue(sourceUpdate.value()));

			destination.setUpdateFlightRefNumberRPH(
					objectFactory.createSeatPreferenceUpdateFlightRefNumberRPH(source.getUpdateFlightRefNumberRPH()));
			destination.setUpdating(source.isUpdating());
		}

		return destination;
	}

}
