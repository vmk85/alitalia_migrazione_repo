package com.alitalia.aem.service.impl.converter.home.checkinservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.messages.home.WebCheckinResponse;
import com.alitalia.aem.ws.checkin.checkinservice.xsd1.CheckInResponse;
import com.alitalia.aem.ws.checkin.checkinservice.xsd2.ArrayOfPassenger;
import com.alitalia.aem.ws.checkin.checkinservice.xsd2.Passenger;
import com.alitalia.aem.ws.checkin.checkinservice.xsd4.ArrayOfstring;



@Component(immediate=true, metatype=false)
@Service(value=CheckinResponseToWebCheckinResponse.class)
public class CheckinResponseToWebCheckinResponse implements
		Converter<CheckInResponse, WebCheckinResponse> {
	
	@Reference
	private PassengerToCheckinPassengerData checkinPassengerConverter;

	@Override
	public WebCheckinResponse convert(CheckInResponse source) {
		WebCheckinResponse destination = null;

		if (source != null) {
			destination = new WebCheckinResponse();			
			
			List<String> messagesList = new ArrayList<String>();
			JAXBElement<ArrayOfstring> messages = source.getMessages();
			if (messages != null && messages.getValue() != null && messages.getValue().getString()!=null
					&& messages.getValue().getString().size()>0){
				List<String> mess = messages.getValue().getString();
				for (String s : mess){
					messagesList.add(s);
				}
			}
			destination.setMessages(messagesList);
			
			JAXBElement<ArrayOfPassenger> sourcePassengers = source.getPassengers();
			if (sourcePassengers != null && 
					sourcePassengers.getValue() != null &&
					sourcePassengers.getValue().getPassenger() != null &&
					!sourcePassengers.getValue().getPassenger().isEmpty()) {
				List<CheckinPassengerData> passengersList = new ArrayList<CheckinPassengerData>();
				for(Passenger sourcePassenger : sourcePassengers.getValue().getPassenger()) {
					passengersList.add(checkinPassengerConverter.convert(sourcePassenger));
				}
				destination.setPassengers(passengersList);
			}
		}
		return destination;
	}

}
