package com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.MmbAirportCheckinData;
import com.alitalia.aem.common.messages.home.RetrieveMmbAirportCheckinResponse;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.AirportCheckin;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.ArrayOfAirportCheckin;

@Component(immediate = true, metatype = false)
@Service(value = ArrayOfAirportCheckinToRetrieveMmbAirportCheckinResponseConverter.class)
public class ArrayOfAirportCheckinToRetrieveMmbAirportCheckinResponseConverter implements Converter<ArrayOfAirportCheckin, RetrieveMmbAirportCheckinResponse> {

	private static final Logger logger = LoggerFactory.getLogger(ArrayOfAirportCheckinToRetrieveMmbAirportCheckinResponseConverter.class);

	@Reference
	private AirportCheckinToMmbAirportCheckinData airportCheckinConverter;
	
	@Reference
	private BookingMMBStaticDataServiceJAXBContextFactory jaxbContextFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public RetrieveMmbAirportCheckinResponse convert(ArrayOfAirportCheckin source) {
		RetrieveMmbAirportCheckinResponse destination = new RetrieveMmbAirportCheckinResponse();
		try {
			List<MmbAirportCheckinData> airportCheckinListData = new ArrayList<>();
			
			Unmarshaller phonePrefixUnmashaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			JAXBElement<AirportCheckin> unmarshalledObj = null;

			List<AirportCheckin> countries = source.getAirportCheckin();
			for (Object airportCheckin : countries) {
				AirportCheckin airportCheckinSource = null;
				if (airportCheckin instanceof AirportCheckin) {
					airportCheckinSource = (AirportCheckin) airportCheckin;
				} else {
					unmarshalledObj = (JAXBElement<AirportCheckin>) phonePrefixUnmashaller.unmarshal((Node) airportCheckin);
					airportCheckinSource = unmarshalledObj.getValue();
				}
				MmbAirportCheckinData mmbAirportCheckinData = airportCheckinConverter.convert(airportCheckinSource);
				airportCheckinListData.add(mmbAirportCheckinData);
			}
			
			destination.setAirportCheckinList(airportCheckinListData);
			
		} catch (JAXBException e) {
			logger.error("Error instantiating JAXBContext/Unmarshaller: {}", e);
		} finally {
			logger.info("Conversion executed successfully");
		}
		return destination;			
	}
}