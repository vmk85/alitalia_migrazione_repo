package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareSurchargePriceData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingFareSurchargePrice;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareSurchargePriceToRTDSPricingFareSurchargePriceData.class)
public class RTDSPricingFareSurchargePriceToRTDSPricingFareSurchargePriceData implements
		Converter<ResultTicketingDetailSolutionPricingFareSurchargePrice, 
					ResultTicketingDetailSolutionPricingFareSurchargePriceData> {

	@Override
	public ResultTicketingDetailSolutionPricingFareSurchargePriceData convert(
			ResultTicketingDetailSolutionPricingFareSurchargePrice source) {
		ResultTicketingDetailSolutionPricingFareSurchargePriceData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingFareSurchargePriceData();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
