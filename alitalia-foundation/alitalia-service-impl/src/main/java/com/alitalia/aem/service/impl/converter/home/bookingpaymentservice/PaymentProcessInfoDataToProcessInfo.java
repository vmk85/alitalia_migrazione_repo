package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.FraudNetParamData;
import com.alitalia.aem.common.data.home.PaymentProcessInfoData;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.FraudNetParam;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.PaymentStep;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ProcessInfo;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ResultType;
import com.alitalia.aem.ws.booking.paymentservice.xsd5.ArrayOfanyType;

@Component(immediate=true, metatype=false)
@Service(value=PaymentProcessInfoDataToProcessInfo.class)
public class PaymentProcessInfoDataToProcessInfo 
		implements Converter<PaymentProcessInfoData, ProcessInfo> {
	
	@Override
	public ProcessInfo convert(PaymentProcessInfoData source) {
		
		ProcessInfo destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createProcessInfo();
			
			com.alitalia.aem.ws.booking.paymentservice.xsd5.ObjectFactory objectFactory5 = new com.alitalia.aem.ws.booking.paymentservice.xsd5.ObjectFactory();
			ArrayOfanyType arrayOfAnyType = objectFactory5.createArrayOfanyType();
			if (source.getFraudNetParams() != null){
				for (FraudNetParamData paramData : source.getFraudNetParams()){
					FraudNetParam param = objectFactory.createFraudNetParam();
					param.setName(objectFactory.createFraudNetParamName(paramData.getName()));
					param.setValue(objectFactory.createFraudNetParamValue(paramData.getValue()));
					arrayOfAnyType.getAnyType().add(param);
				}
			}
			destination.setFraudNetParam(objectFactory.createFraudNetParamProcessInfoElement(arrayOfAnyType));		
			
			destination.setMac(objectFactory.createProcessInfoMac(source.getMac()));
			
			destination.setMarketCode(
					objectFactory.createProcessInfoMarketCode(
							source.getMarketCode()));
			
			destination.setPaRes(
					objectFactory.createProcessInfoPaRes(
							source.getPaRes()));
			
			destination.setPaymentAttemps(
					source.getPaymentAttemps());
			
			destination.setPaymentRef(
					objectFactory.createProcessInfoPaymentRef(
							source.getPaymentRef()));
			
			destination.setPointOfSale(
					objectFactory.createProcessInfoPointOfSale(
							source.getPointOfSale()));
			
			destination.setRedirectUrl(
					objectFactory.createProcessInfoRedirectUrl(
							source.getRedirectUrl()));
			
			if (source.getResult() != null) {
				destination.setResult(
						ResultType.fromValue(source.getResult().value()));
			}
			
			destination.setSessionId(
					objectFactory.createProcessInfoSessionId(
							source.getSessionId()));
			
			destination.setShopId(
					objectFactory.createProcessInfoShopId(
							source.getShopId()));
			
			destination.setSiteCode(
					objectFactory.createProcessInfoSiteCode(
							source.getSiteCode()));
			
			if (source.getStep() != null) {
				destination.setStep(
						PaymentStep.fromValue(source.getStep().value()));
			}
			
			destination.setTokenId(
					objectFactory.createProcessInfoTokenId(
							source.getTokenId()));
			
			destination.setTransactionId(
					objectFactory.createProcessInfoTransactionId(
							source.getTransactionId()));
			
			ResultType value = null;
			if(source.getResult()!=null)
				value = ResultType.fromValue(source.getResult().value());
					
			destination.setResult(value);
			
			destination.setResultCode(objectFactory.createProcessInfoResultCode(source.getResultCode()));
			
			destination.setResponseCode(objectFactory.createProcessInfoResponseCode(source.getResponseCode()));
			
			destination.setMerchantId(objectFactory.createProcessInfoMerchantId(source.getMerchantId()));
			
			destination.setSupplierId(objectFactory.createProcessInfoSupplierId(source.getSupplierId()));
		
			destination.setMac(objectFactory.createProcessInfoMac(source.getMac()));
		}
		
		return destination;
	}

}
