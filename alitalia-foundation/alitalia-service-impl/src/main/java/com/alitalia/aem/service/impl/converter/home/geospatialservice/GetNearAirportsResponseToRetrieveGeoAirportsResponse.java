package com.alitalia.aem.service.impl.converter.home.geospatialservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.CodeDescriptionData;
import com.alitalia.aem.common.messages.home.RetrieveGeoAirportsResponse;
import com.alitalia.aem.ws.geospatial.service.xsd1.GetNearAirportsResponse;
import com.alitalia.aem.ws.geospatial.service.xsd6.ArrayOfCodeDescription;
import com.alitalia.aem.ws.geospatial.service.xsd6.CodeDescription;

@Component(immediate = true, metatype = false)
@Service(value = GetNearAirportsResponseToRetrieveGeoAirportsResponse.class)
public class GetNearAirportsResponseToRetrieveGeoAirportsResponse implements Converter<GetNearAirportsResponse, RetrieveGeoAirportsResponse> {

	@Override
	public RetrieveGeoAirportsResponse convert(GetNearAirportsResponse source) {
		
		RetrieveGeoAirportsResponse response = new RetrieveGeoAirportsResponse();
		
		JAXBElement<ArrayOfCodeDescription> jaxbArrayOfCodeDescription = source.getAirports();
		ArrayOfCodeDescription arrayOfCodeDescription = jaxbArrayOfCodeDescription.getValue();
		List<CodeDescription> codesDescriptions = arrayOfCodeDescription.getCodeDescription();
		
		List<CodeDescriptionData> codesDescriptionData = new ArrayList<>();
		for(CodeDescription codeDescription: codesDescriptions){
			CodeDescriptionData codeDescriptionData = new CodeDescriptionData();
			codeDescriptionData.setCode(codeDescription.getCode().getValue());
			codeDescriptionData.setDescription(codeDescription.getDescription().getValue());
			
			codesDescriptionData.add(codeDescriptionData);
		}
		
		response.setCodesDescriptions(codesDescriptionData);
		
		return response;
	}
}