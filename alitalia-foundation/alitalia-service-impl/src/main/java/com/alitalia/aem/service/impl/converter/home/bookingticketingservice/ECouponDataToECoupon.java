package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ECouponData;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ArrayOfPassengerType;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ECoupon;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ObjectFactory;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.PassengerType;
import com.alitalia.aem.ws.booking.ticketservice.xsd8.DiscountType;
import com.alitalia.aem.ws.booking.ticketservice.xsd8.FareType;

@Component(immediate = true, metatype = false)
@Service(value = ECouponDataToECoupon.class)
public class ECouponDataToECoupon implements Converter<ECouponData, ECoupon> {

	@Override
	public ECoupon convert(ECouponData source) {
		ECoupon destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();

			destination = objectFactory.createECoupon();
			destination.setAmount(source.getAmount());
			destination.setCode(objectFactory.createECouponCode(source.getCode()));
			destination.setErrorCode(source.getErrorCode());
			destination.setErrorDescription(objectFactory.createECouponErrorDescription(source.getErrorDescription()));
			destination.setFamilyEcoupon(objectFactory.createECouponFamilyEcoupon(source.getFamilyEcoupon()));

			if (source.getFare() != null) {
				destination.setFare(FareType.fromValue(source.getFare().value()));
			}

			destination.setIsSingleUse(source.isSingleUse());
			destination.setIsValid(source.isValid());

			ArrayOfPassengerType paxTypes = objectFactory.createArrayOfPassengerType();
			if (source.getPaxTypes() != null) {
				for (PassengerTypeEnum paxType : source.getPaxTypes()) {
					paxTypes.getPassengerType().add(PassengerType.fromValue(paxType.value()));
				}
			}
			destination.setPaxTypes(objectFactory.createECouponPaxTypes(paxTypes));

			if (source.getType() != null) {
				destination.setType(DiscountType.fromValue(source.getType().value()));
			}
		}

		return destination;
	}

}
