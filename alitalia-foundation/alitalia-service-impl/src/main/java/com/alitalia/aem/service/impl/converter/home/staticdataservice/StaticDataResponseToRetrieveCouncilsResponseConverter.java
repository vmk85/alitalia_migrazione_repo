package com.alitalia.aem.service.impl.converter.home.staticdataservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MunicipalityData;
import com.alitalia.aem.common.messages.home.RetrieveCouncilsResponse;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.StaticDataResponse;
import com.alitalia.aem.ws.booking.staticdataservice.xsd2.Municipality;

@Component(immediate = true, metatype = false)
@Service(value = StaticDataResponseToRetrieveCouncilsResponseConverter.class)
public class StaticDataResponseToRetrieveCouncilsResponseConverter implements Converter<StaticDataResponse, RetrieveCouncilsResponse> {

	private static final Logger logger = LoggerFactory.getLogger(StaticDataResponseToRetrieveCouncilsResponseConverter.class);

	@Reference
	private MunicipalityToMunicipalityData municipalityConverter;

	@Reference
	private StaticDataServiceJAXBContextFactory jaxbContextFactory;

	@SuppressWarnings("unchecked")
	@Override
	public RetrieveCouncilsResponse convert(StaticDataResponse source) {
		RetrieveCouncilsResponse destination = new RetrieveCouncilsResponse();
		try {
			List<MunicipalityData> councilsData = new ArrayList<>();
			
			Unmarshaller municipalityUnmashaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			JAXBElement<Municipality> unmarshalledObj = null;

			List<Object> municipalities = source.getEntities().getValue().getAnyType();
			for (Object municipality : municipalities) {
				Municipality municipalitySource = null;
				if (municipality instanceof Municipality) {
					municipalitySource = (Municipality) municipality;
				} else {
					unmarshalledObj = (JAXBElement<Municipality>) municipalityUnmashaller.unmarshal((Node) municipality);
					municipalitySource = unmarshalledObj.getValue();
				}
				MunicipalityData municipalityData = municipalityConverter.convert(municipalitySource);
				councilsData.add(municipalityData);
			}
			
			destination.setCouncils(councilsData);
			
		} catch (JAXBException e) {
			logger.error("Error instantiating JAXBContext/Unmarshaller: {}", e);
		} finally {
			logger.info("Conversion executed successfully");
		}
		return destination;			
	}
}