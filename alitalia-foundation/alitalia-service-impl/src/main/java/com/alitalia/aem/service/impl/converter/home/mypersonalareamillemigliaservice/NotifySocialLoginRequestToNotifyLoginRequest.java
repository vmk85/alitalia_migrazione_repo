package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.NotifySocialLoginRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.NotifyLoginRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;

@Component(immediate = true, metatype = false)
@Service(value = NotifySocialLoginRequestToNotifyLoginRequest.class)
public class NotifySocialLoginRequestToNotifyLoginRequest 
		implements Converter<NotifySocialLoginRequest, NotifyLoginRequest> {

	@Override
	public NotifyLoginRequest convert(NotifySocialLoginRequest source) {
		NotifyLoginRequest destination = null;

		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			destination = objectFactory.createNotifyLoginRequest();
			
			destination.setSiteUID(
					objectFactory.createNotifyLoginRequestSiteUID(
							source.getSiteUID()));
			
			destination.setNewUser(
					source.getNewUser());
			
		}

		return destination;
	}

}
