package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMProgramData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Program;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd3.ArrayOfanyType;

@Component(immediate = true, metatype = false)
@Service(value = ProgramsToMMProgramsData.class)
public class ProgramsToMMProgramsData implements Converter<ArrayOfanyType, List<MMProgramData>> {

	private static Logger logger = LoggerFactory.getLogger(ProgramsToMMProgramsData.class);
	
	@Reference
	private MyPersonalareaMillemigliaServiceJAXBContextFactory jaxbContextFactory;
	
	@Override
	public List<MMProgramData> convert(ArrayOfanyType source) {
		
		if (source == null) {
			return null;
		}
		
		List<MMProgramData> mmPrograms = new ArrayList<MMProgramData>();

		for (Object object: source.getAnyType()) {
			try {
				Unmarshaller unmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
				
				JAXBElement<Program> jaxbElement = (JAXBElement<Program>) unmarshaller.unmarshal((Node) object);
				Program program = jaxbElement.getValue();
				
				MMProgramData mmProgramData = new MMProgramData();
				if (program.getStartDate() != null)
					mmProgramData.setStartDate(XsdConvertUtils.parseCalendar(program.getEndDate()));
				if (program.getEndDate() != null)
					mmProgramData.setEndDate(XsdConvertUtils.parseCalendar(program.getEndDate()));
				mmProgramData.setProgramCode(program.getProgramCode().getValue());
				
				mmPrograms.add(mmProgramData);
			
			} catch (JAXBException e) {
				logger.error("Error while creating JAXBConext/Unmarshller for List<MMProgramData>: {}", e);
			}
		}
		
		return mmPrograms;
	}
}