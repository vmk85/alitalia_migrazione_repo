package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionCurrencyConversionData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ArrayOfresultTicketingDetailSolutionCurrencyConversion;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ArrayOfresultTicketingDetailSolutionPricing;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ArrayOfresultTicketingDetailSolutionSlice;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolution;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=ResultTicketingDetailSolutionDataToResultTicketingDetailSolution.class)
public class ResultTicketingDetailSolutionDataToResultTicketingDetailSolution implements
		Converter<ResultTicketingDetailSolutionData, ResultTicketingDetailSolution> {

	@Reference
	private RTDSCurrencyConversionDataToRTDSCurrencyConversion rtdsCurrencyConversionData;

	@Reference
	private RTDSDisplayFareTotalDataToRTDSDisplayFareTotal rtdsDisplayFareTotalDataConverter;

	@Reference
	private RTDSDisplayTaxTotalDataToRTDSDisplayTaxTotal rtdsDisplayTaxTotalConverter;

	@Reference
	private RTDSDisplayTotalDataToRTDSDisplayTotal rtdsDisplayTotalDataConverter;

	@Reference
	private RTDSExtDataToRTDSExt rtdsExtDataConverter;

	@Reference
	private RTDSPricingDataToRTDSPricing rtdsPricingDataConverter;

	@Reference
	private RTDSSaleFareTotalDataToRTDSSaleFareTotal rtdsSaleFareTotalDataConverter;

	@Reference
	private RTDSSaleTaxTotalDataToRTDSSaleTaxTotal rtdsSaleTaxTotalDataConverter;

	@Reference
	private RTDSSaleTotalDataToRTDSSaleTotal rtdsSaleTotalDataConverter;

	@Reference
	private RTDSSliceDataToRTDSSlice rtdsSliceFieldConverter;

	@Override
	public ResultTicketingDetailSolution convert(ResultTicketingDetailSolutionData source) {
		ResultTicketingDetailSolution destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolution();

			ArrayOfresultTicketingDetailSolutionCurrencyConversion currencyConversionField = null;
			if (source.getCurrencyConversionField() != null &&
					!source.getCurrencyConversionField().isEmpty()) {
				currencyConversionField = objectFactory.createArrayOfresultTicketingDetailSolutionCurrencyConversion();
				for(ResultTicketingDetailSolutionCurrencyConversionData sourceCurrencyConversionFieldElem : source.getCurrencyConversionField())
					currencyConversionField.getResultTicketingDetailSolutionCurrencyConversion().add(
							rtdsCurrencyConversionData.convert(sourceCurrencyConversionFieldElem));
			}
			destination.setCurrencyConversionField(currencyConversionField);

			destination.setDisplayFareTotalField(
					rtdsDisplayFareTotalDataConverter.convert(
							source.getDisplayFareTotalField()));

			destination.setDisplayTaxTotalField(
					rtdsDisplayTaxTotalConverter.convert(
							source.getDisplayTaxTotalField()));

			destination.setDisplayTotalField(
					rtdsDisplayTotalDataConverter.convert(
							source.getDisplayTotalField()));

			destination.setExtField(rtdsExtDataConverter.convert(source.getExtField()));

			destination.setIdField(source.getIdField());

			ArrayOfresultTicketingDetailSolutionPricing pricingField = null;
			if (source.getPricingField() != null &&
					!source.getPricingField().isEmpty()) {
				pricingField = objectFactory.createArrayOfresultTicketingDetailSolutionPricing();
				for (ResultTicketingDetailSolutionPricingData sourcePricingFieldElem : source.getPricingField())
					pricingField.getResultTicketingDetailSolutionPricing().add(
							rtdsPricingDataConverter.convert(sourcePricingFieldElem));
			}
			destination.setPricingField(pricingField);

			destination.setSaleFareTotalField(rtdsSaleFareTotalDataConverter.convert(source.getSaleFareTotalField()));

			destination.setSaleTaxTotalField(rtdsSaleTaxTotalDataConverter.convert(source.getSaleTaxTotalField()));

			destination.setSaleTotalField(rtdsSaleTotalDataConverter.convert(source.getSaleTotalField()));

			ArrayOfresultTicketingDetailSolutionSlice sliceField = null;
			if (source.getSliceField() != null &&
					!source.getSliceField().isEmpty()) {
				sliceField = objectFactory.createArrayOfresultTicketingDetailSolutionSlice();
				for(ResultTicketingDetailSolutionSliceData sourceSliceFieldElem : source.getSliceField())
					sliceField.getResultTicketingDetailSolutionSlice().add(
							rtdsSliceFieldConverter.convert(sourceSliceFieldElem));
			}
			destination.setSliceField(sliceField);
		}

		return destination;
	}

}
