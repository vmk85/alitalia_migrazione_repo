package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFare;


@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingBookingInfoSegmentPricingFareToPricingBookingInfoSegmentPricingFareData.class)
public class RBDSPricingBookingInfoSegmentPricingFareToPricingBookingInfoSegmentPricingFareData
		implements Converter<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFare, ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData> {

	@Reference
	private RBDSPricingBookingInfoSegmentPricingFareRuleSummaryPenaltiesToRBDSBInfoSegmentPricingData convertPenalties;
	
	@Override
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData convert(
			ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFare source) {
		ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData destination = null;
		
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData();
			ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryData ruleSummaryField = new ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareRuleSummaryData();
				ruleSummaryField.setPenaltiesField(convertPenalties.convert(source.getRuleSummaryField().getPenaltiesField()));
			destination.setRuleSummaryField(ruleSummaryField);
		}
		return destination;
	}

}
