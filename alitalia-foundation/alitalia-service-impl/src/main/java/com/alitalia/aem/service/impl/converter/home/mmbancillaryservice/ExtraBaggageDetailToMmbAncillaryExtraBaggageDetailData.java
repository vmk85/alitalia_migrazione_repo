package com.alitalia.aem.service.impl.converter.home.mmbancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryExtraBaggageDetailData;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.ExtraBaggageDetail;

@Component(immediate=true, metatype=false)
@Service(value=ExtraBaggageDetailToMmbAncillaryExtraBaggageDetailData.class)
public class ExtraBaggageDetailToMmbAncillaryExtraBaggageDetailData implements
		Converter<ExtraBaggageDetail, MmbAncillaryExtraBaggageDetailData> {

	@Override
	public MmbAncillaryExtraBaggageDetailData convert(ExtraBaggageDetail source) {
		MmbAncillaryExtraBaggageDetailData destination = null;

		if (source != null) {
			destination = new MmbAncillaryExtraBaggageDetailData();

			destination.setBaggageType(source.getX003CBaggageTypeX003EKBackingField());
		}

		return destination;
	}

}
