package com.alitalia.aem.service.impl.converter.home.mmbticketingservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.TicketData;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd4.ObjectFactory;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd4.Ticket;

@Component(immediate=true, metatype=false)
@Service(value=TicketDataToTicket.class)
public class TicketDataToTicket implements Converter<TicketData, Ticket> {

	@Override
	public Ticket convert(TicketData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		Ticket destination = objectFactory.createTicket();

		destination.setDataVolo(objectFactory.createTicketDataVolo(source.getDataVolo()));
		destination.setNTkt(objectFactory.createTicketNTkt(source.getNumeroBiglietto()));
		destination.setPnr(objectFactory.createTicketPnr(source.getPnr()));
		destination.setTratta(objectFactory.createTicketTratta(source.getTratta()));

		/*
		 * I seguenti campi sono necessari ma sempre vuoti, non sono stati nemmeno
		 * riportati nel DTO 
		 */
		destination.setZar(objectFactory.createTicketZar(""));
		destination.setZnTktSost(objectFactory.createTicketZnTktSost(""));

		return destination;
	}

}
