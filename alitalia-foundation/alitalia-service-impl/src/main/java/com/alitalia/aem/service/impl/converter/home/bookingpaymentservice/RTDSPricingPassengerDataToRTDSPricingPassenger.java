package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingPassengerData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingPassenger;
import com.alitalia.aem.ws.booking.paymentservice.xsd5.ArrayOfstring;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingPassengerDataToRTDSPricingPassenger.class)
public class RTDSPricingPassengerDataToRTDSPricingPassenger implements
		Converter<ResultTicketingDetailSolutionPricingPassengerData, 
					ResultTicketingDetailSolutionPricingPassenger> {

	@Override
	public ResultTicketingDetailSolutionPricingPassenger convert(
			ResultTicketingDetailSolutionPricingPassengerData source) {
		ResultTicketingDetailSolutionPricingPassenger destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			com.alitalia.aem.ws.booking.paymentservice.xsd5.ObjectFactory objectFactory5 =
					new com.alitalia.aem.ws.booking.paymentservice.xsd5.ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingPassenger();

			destination.setIdField(source.getIdField());

			ArrayOfstring ptcField = null;
			if (source.getPtcField() != null &&
					!source.getPtcField().isEmpty()) {
				ptcField = objectFactory5.createArrayOfstring();
				for(String sourcePtcFieldElem : source.getPtcField())
					ptcField.getString().add(sourcePtcFieldElem);
			}
			destination.setPtcField(ptcField);
		}

		return destination;
	}

}
