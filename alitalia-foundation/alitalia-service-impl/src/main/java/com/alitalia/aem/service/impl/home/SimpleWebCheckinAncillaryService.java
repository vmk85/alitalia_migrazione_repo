package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutInitRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutInitResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutStatusRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutStatusResponse;
import com.alitalia.aem.common.messages.home.WebCheckinAddToCartRequest;
import com.alitalia.aem.common.messages.home.WebCheckinAncillaryCartResponse;
import com.alitalia.aem.common.messages.home.WebCheckinAncillaryResponse;
import com.alitalia.aem.common.messages.home.WebCheckinCancelCartRequest;
import com.alitalia.aem.common.messages.home.WebCheckinCancelCartResponse;
import com.alitalia.aem.common.messages.home.WebCheckinGetCartRequest;
import com.alitalia.aem.common.messages.home.WebCheckinGetCatalogRequest;
import com.alitalia.aem.common.messages.home.WebCheckinRemoveFromCartRequest;
import com.alitalia.aem.common.messages.home.WebCheckinUpdateCartRequest;
import com.alitalia.aem.service.api.home.WebCheckinAncillaryService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.checkinancillaryservice.AddToCartResponseToWebCheckinAncillaryResponse;
import com.alitalia.aem.service.impl.converter.home.checkinancillaryservice.CancelCartResponseToWebCheckinCancelCartResponse;
import com.alitalia.aem.service.impl.converter.home.checkinancillaryservice.CheckOutInitResponseToMmbAncillaryCheckOutInitResponse;
import com.alitalia.aem.service.impl.converter.home.checkinancillaryservice.GetCartResponseToWebCheckinAncillaryCartResponse;
import com.alitalia.aem.service.impl.converter.home.checkinancillaryservice.GetCatalogResponseToWebCheckinAncillaryCartResponse;
import com.alitalia.aem.service.impl.converter.home.checkinancillaryservice.GetCheckOutStatusResponseToMmbAncillaryCheckOutStatusResponse;
import com.alitalia.aem.service.impl.converter.home.checkinancillaryservice.MmbAncillaryCheckOutInitRequestToCheckOutInitRequest;
import com.alitalia.aem.service.impl.converter.home.checkinancillaryservice.MmbAncillaryCheckOutStatusRequestToGetCheckOutStatusRequest;
import com.alitalia.aem.service.impl.converter.home.checkinancillaryservice.RemoveFromCartResponseToWebCheckinAncillaryResponse;
import com.alitalia.aem.service.impl.converter.home.checkinancillaryservice.UpdateCartResponseToWebCheckinAncillaryResponse;
import com.alitalia.aem.service.impl.converter.home.checkinancillaryservice.WebCheckinAddToCartRequestToAddToCartRequest;
import com.alitalia.aem.service.impl.converter.home.checkinancillaryservice.WebCheckinCancelCartRequestToCancelCartRequest;
import com.alitalia.aem.service.impl.converter.home.checkinancillaryservice.WebCheckinGetCartRequestToGetCartRequest;
import com.alitalia.aem.service.impl.converter.home.checkinancillaryservice.WebCheckinGetCatalogRequestToGetCatalogRequest;
import com.alitalia.aem.service.impl.converter.home.checkinancillaryservice.WebCheckinRemoveFromCartRequestToRemoveFromCartRequest;
import com.alitalia.aem.service.impl.converter.home.checkinancillaryservice.WebCheckinUpdateCartRequestToUpdateCartRequest;
import com.alitalia.aem.ws.checkin.ancillaryservice.CheckinAncillaryServiceClient;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.AddToCartRequest;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.AddToCartResponse;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.CancelCartRequest;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.CancelCartResponse;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.CheckOutInitRequest;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.CheckOutInitResponse;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.GetCartRequest;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.GetCartResponse;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.GetCatalogRequest;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.GetCatalogResponse;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.GetCheckOutStatusRequest;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.GetCheckOutStatusResponse;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.RemoveFromCartRequest;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.RemoveFromCartResponse;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.UpdateCartRequest;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.UpdateCartResponse;


@Component(immediate=true, metatype=false)
@Service
public class SimpleWebCheckinAncillaryService implements WebCheckinAncillaryService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleWebCheckinAncillaryService.class);

	@Reference
	private CheckinAncillaryServiceClient checkinAncillaryClient;
	
	@Reference
	private WebCheckinAddToCartRequestToAddToCartRequest addToCartAncillaryRequestConverter;
	
	@Reference
	private WebCheckinGetCartRequestToGetCartRequest getCartAncillaryRequestConverter;
	
	@Reference
	private WebCheckinUpdateCartRequestToUpdateCartRequest updateCartAncillaryRequestConverter;
	
	@Reference
	private WebCheckinRemoveFromCartRequestToRemoveFromCartRequest removeFromCartAncillaryRequestConverter;
	
	@Reference
	private WebCheckinGetCatalogRequestToGetCatalogRequest getCatalogAncillaryRequestConverter;
	
	@Reference
	private AddToCartResponseToWebCheckinAncillaryResponse addToCartAncillaryResponseConverter;
	
	@Reference
	private UpdateCartResponseToWebCheckinAncillaryResponse updateCartAncillaryResponseConverter;
	
	@Reference
	private RemoveFromCartResponseToWebCheckinAncillaryResponse removeFromCartAncillaryResponseConverter;
	
	@Reference
	private GetCatalogResponseToWebCheckinAncillaryCartResponse getCatalogAncillaryResponseConverter;
	
	@Reference
	private GetCartResponseToWebCheckinAncillaryCartResponse getCartAncillaryResponseConverter;
	
	@Reference
	private MmbAncillaryCheckOutInitRequestToCheckOutInitRequest mmbAncillaryCheckOutInitRequestConverter;

	@Reference
	private CheckOutInitResponseToMmbAncillaryCheckOutInitResponse checkOutInitResponseConverter;

	@Reference
	private MmbAncillaryCheckOutStatusRequestToGetCheckOutStatusRequest mmbAncillaryCheckOutStatusRequestConverter;

	@Reference
	private GetCheckOutStatusResponseToMmbAncillaryCheckOutStatusResponse getCheckOutStatusResponseConverter;
	
	@Reference
	private WebCheckinCancelCartRequestToCancelCartRequest cancelCartRequestConverter;
	
	@Reference
	private CancelCartResponseToWebCheckinCancelCartResponse cancelCartResponseConverter;
	
	@Override
	public WebCheckinAncillaryCartResponse getCatalog(WebCheckinGetCatalogRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method getCatalog(). The request is {}", request);
		}

		try {
			WebCheckinAncillaryCartResponse response = new WebCheckinAncillaryCartResponse();
			GetCatalogRequest serviceRequest = getCatalogAncillaryRequestConverter.convert(request);
			GetCatalogResponse serviceResponse = checkinAncillaryClient.getCatalog(serviceRequest);
			response = getCatalogAncillaryResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully getCatalog(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getCatalog().", e);
			throw new AlitaliaServiceException("Exception executing getCatalog: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public WebCheckinAncillaryResponse addToCart(WebCheckinAddToCartRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method addToCart(). The request is {}", request);
		}

		try {
			WebCheckinAncillaryResponse response = new WebCheckinAncillaryResponse();
			AddToCartRequest serviceRequest = addToCartAncillaryRequestConverter.convert(request);
			AddToCartResponse serviceResponse = checkinAncillaryClient.addToCart(serviceRequest);
			response = addToCartAncillaryResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully addToCart(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method addToCart().", e);
			throw new AlitaliaServiceException("Exception executing addToCart: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public WebCheckinAncillaryResponse updateCart(WebCheckinUpdateCartRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method updateCart(). The request is {}", request);
		}

		try {
			WebCheckinAncillaryResponse response = new WebCheckinAncillaryResponse();
			UpdateCartRequest serviceRequest = updateCartAncillaryRequestConverter.convert(request);
			UpdateCartResponse serviceResponse = checkinAncillaryClient.updateCart(serviceRequest);
			response = updateCartAncillaryResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully updateCart(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method updateCart().", e);
			throw new AlitaliaServiceException("Exception executing updateCart: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public WebCheckinAncillaryResponse removeFromCart(WebCheckinRemoveFromCartRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method removeFromCart(). The request is {}", request);
		}

		try {
			WebCheckinAncillaryResponse response = new WebCheckinAncillaryResponse();
			RemoveFromCartRequest serviceRequest = removeFromCartAncillaryRequestConverter.convert(request);
			RemoveFromCartResponse serviceResponse = checkinAncillaryClient.removeFromCart(serviceRequest);
			response = removeFromCartAncillaryResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully removeFromCart(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method removeFromCart().", e);
			throw new AlitaliaServiceException("Exception executing removeFromCart: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public WebCheckinAncillaryCartResponse getCart(WebCheckinGetCartRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method getCart(). The request is {}", request);
		}

		try {
			WebCheckinAncillaryCartResponse response = new WebCheckinAncillaryCartResponse();
			GetCartRequest serviceRequest = getCartAncillaryRequestConverter.convert(request);
			GetCartResponse serviceResponse = checkinAncillaryClient.getCart(serviceRequest);
			response = getCartAncillaryResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully getCart(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getCart().", e);
			throw new AlitaliaServiceException("Exception executing getCart: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public MmbAncillaryCheckOutInitResponse checkOutInit(MmbAncillaryCheckOutInitRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method checkOutInit(). The request is {}", request);
		}
		

		try {
			MmbAncillaryCheckOutInitResponse response = new MmbAncillaryCheckOutInitResponse();
			CheckOutInitRequest serviceRequest = mmbAncillaryCheckOutInitRequestConverter.convert(request);
			CheckOutInitResponse serviceResponse = checkinAncillaryClient.checkOutInit(serviceRequest);
			response = checkOutInitResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully checkOutInit(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method checkOutInit().", e);
			throw new AlitaliaServiceException("Exception executing checkOutInit: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public MmbAncillaryCheckOutStatusResponse getCheckOutStatus(MmbAncillaryCheckOutStatusRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method getCheckOutStatus(). The request is {}", request);
		}
		
		try {
			MmbAncillaryCheckOutStatusResponse response = new MmbAncillaryCheckOutStatusResponse();
			GetCheckOutStatusRequest serviceRequest = mmbAncillaryCheckOutStatusRequestConverter.convert(request);
			GetCheckOutStatusResponse serviceResponse = checkinAncillaryClient.getCheckOutStatus(serviceRequest);
			response = getCheckOutStatusResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully getCheckOutStatus(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getCheckOutStatus().", e);
			throw new AlitaliaServiceException("Exception executing getCheckOutStatus: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public WebCheckinCancelCartResponse cancelCart(WebCheckinCancelCartRequest request){
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method cancelCart(). The request is {}", request);
		}

		try {
			WebCheckinCancelCartResponse response = new WebCheckinCancelCartResponse();
			CancelCartRequest serviceRequest = cancelCartRequestConverter.convert(request);
			CancelCartResponse serviceResponse = checkinAncillaryClient.cancelCart(serviceRequest);
			response = cancelCartResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully cancelCart(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method cancelCart().", e);
			throw new AlitaliaServiceException("Exception executing cancelCart: Sid ["+request.getSid()+"]" , e);
		}
	}
}