package com.alitalia.aem.service.impl.converter.home.checkininsuranceservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinCreditCardHolderData;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd2.CreditCardHolderData;



@Component(immediate=true, metatype=false)
@Service(value=CheckinCreditCardHolderDataToCreditCardHolderData.class)
public class CheckinCreditCardHolderDataToCreditCardHolderData implements Converter<CheckinCreditCardHolderData, CreditCardHolderData> {


	@Override
	public CreditCardHolderData convert(CheckinCreditCardHolderData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		CreditCardHolderData destination = null;

		if (source != null) {
			destination = objectFactory.createCreditCardHolderData();
			if(source.getId() != null){
				destination.setX003CIdX003EKBackingField(source.getId());
			}
			
			destination.setX003CAddressX003EKBackingField(source.getAddress());
			destination.setX003CCityX003EKBackingField(source.getCity());
			destination.setX003CCountryX003EKBackingField(source.getCountry());
			destination.setX003CFirstNameX003EKBackingField(source.getFirstName());
			if(source.getIsActualBuyer() != null){
				destination.setX003CIsActualBuyerX003EKBackingField(source.getIsActualBuyer());
			}
			destination.setX003CLastNameX003EKBackingField(source.getLastName());
			destination.setX003CNameX003EKBackingField(source.getName());
			destination.setX003CStateX003EKBackingField(source.getState());
			destination.setX003CZipX003EKBackingField(source.getZip());
			
		}

		return destination;
	}

}
