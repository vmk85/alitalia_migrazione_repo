package com.alitalia.aem.service.impl.converter.home.bookingbagsandrulesservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionRefundPriceData;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetailsSolutionRefundPrice;

@Component(immediate=true, metatype=false)
@Service(value=RBDSRefundPriceToRefundPriceData.class)
public class RBDSRefundPriceToRefundPriceData implements Converter<ResultBookingDetailsSolutionRefundPrice, ResultBookingDetailsSolutionRefundPriceData> {

	@Override
	public ResultBookingDetailsSolutionRefundPriceData convert(ResultBookingDetailsSolutionRefundPrice source) {
		ResultBookingDetailsSolutionRefundPriceData destination = null;
		if(source!=null){
			destination = new ResultBookingDetailsSolutionRefundPriceData();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}
	
	public ResultBookingDetailsSolutionRefundPriceData convert(com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetailsSolutionRefundPrice source) {
		ResultBookingDetailsSolutionRefundPriceData destination = null;
		if(source!=null){
			destination = new ResultBookingDetailsSolutionRefundPriceData();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}
	
}
