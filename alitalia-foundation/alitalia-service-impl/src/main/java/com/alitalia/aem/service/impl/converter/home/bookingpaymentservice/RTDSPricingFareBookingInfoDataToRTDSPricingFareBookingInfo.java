package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareBookingInfoData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingFareBookingInfo;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareBookingInfoDataToRTDSPricingFareBookingInfo.class)
public class RTDSPricingFareBookingInfoDataToRTDSPricingFareBookingInfo implements
		Converter<ResultTicketingDetailSolutionPricingFareBookingInfoData, 
					ResultTicketingDetailSolutionPricingFareBookingInfo> {

	@Reference
	private RTDSPricingFareBookingInfoSegmentDataToRTDSPricingFareBookingInfoSegment rtdsSegmentFieldConverter;

	@Override
	public ResultTicketingDetailSolutionPricingFareBookingInfo convert(
			ResultTicketingDetailSolutionPricingFareBookingInfoData source) {
		ResultTicketingDetailSolutionPricingFareBookingInfo destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingFareBookingInfo();

			destination.setBookingCodeField(source.getBookingCodeField());

			destination.setSegmentField(rtdsSegmentFieldConverter.convert(source.getSegmentField()));
		}

		return destination;
	}

}
