package com.alitalia.aem.service.impl.converter.home.mmbancillaryservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.MmbAncillaryErrorData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.common.messages.home.MmbAncillaryAddToCartResponse;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.AddToCartResponse;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.Ancillaries;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.ArrayOfAncillaries;

@Component(immediate=true, metatype=false)
@Service(value=AddToCartResponseToMmbAncillaryAddToCartResponse.class)
public class AddToCartResponseToMmbAncillaryAddToCartResponse implements Converter<AddToCartResponse, MmbAncillaryAddToCartResponse> {

	@Reference
	private AncillariesToMmbAncillaryData ancillariesConverter;

	@Reference
	private ErrorToMmbAncillaryErrorData errorConverter;

	@Override
	public MmbAncillaryAddToCartResponse convert(AddToCartResponse source) {
		MmbAncillaryAddToCartResponse destination = null;

		if (source != null) {
			destination = new MmbAncillaryAddToCartResponse();

			JAXBElement<String> sourceMachineName = source.getMachineName();
			if (sourceMachineName != null)
				destination.setMachine(sourceMachineName.getValue());

			List<MmbAncillaryData> ancillaries = null;
			if (source.getAncillaries() != null &&
					source.getAncillaries().getValue() != null &&
					source.getAncillaries().getValue().getAncillaries() != null &&
					!source.getAncillaries().getValue().getAncillaries().isEmpty()) {
				ancillaries = new ArrayList<MmbAncillaryData>();
				ArrayOfAncillaries sourceAncillaries = source.getAncillaries().getValue();
				for(Ancillaries sourceAncillary : sourceAncillaries.getAncillaries()) {
					ancillaries.add(ancillariesConverter.convert(sourceAncillary));
				}
			}
			destination.setAncillaries(ancillaries);

			if (source.getErrors() != null &&
					source.getErrors().getValue() != null &&
					source.getErrors().getValue().getAnyType() != null &&
					!source.getErrors().getValue().getAnyType().isEmpty()) {
				List<Object> sourceErrors = source.getErrors().getValue().getAnyType();
				List<MmbAncillaryErrorData> errors = new ArrayList<MmbAncillaryErrorData>();
				for (Object sourceError : sourceErrors) {
					errors.add(errorConverter.convert(sourceError));
				}
				destination.setErrors(errors);
			}
		}

		return destination;
	}

}
