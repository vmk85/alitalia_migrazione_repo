package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.home.UpdateUserProfileResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.UpdateProfileResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;

@Component(immediate = true, metatype = false)
@Service(value = UpdateProfileResponseToUpdateUserProfileResponse.class)
public class UpdateProfileResponseToUpdateUserProfileResponse implements Converter<UpdateProfileResponse, UpdateUserProfileResponse> {

	@Reference
	private CustomerProfileToMMCustomerProfileData customerProfileConverter;
	
	@Override
	public UpdateUserProfileResponse convert(UpdateProfileResponse source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		UpdateUserProfileResponse destination = new UpdateUserProfileResponse(); 
		
		JAXBElement<CustomerProfile> customerProfile = source.getCustomer();
		if(customerProfile != null && customerProfile.getValue() != null){
			MMCustomerProfileData mmCustomerProfile = customerProfileConverter.convert(customerProfile.getValue());
			destination.setCustomerProfile(mmCustomerProfile);
		}
		
		List<String> updateErrors = source.getCustomerUpdateErrors();
		destination.setCustomerUpdateErrors(updateErrors);
		
		return destination;
	}
}