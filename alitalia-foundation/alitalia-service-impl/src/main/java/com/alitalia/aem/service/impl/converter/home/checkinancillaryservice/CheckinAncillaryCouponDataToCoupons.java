package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryCouponData;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.Coupons;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd3.ArrayOfCompartimentalClass;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd3.CompartimentalClass;


@Component(immediate=true, metatype=false)
@Service(value=CheckinAncillaryCouponDataToCoupons.class)
public class CheckinAncillaryCouponDataToCoupons implements Converter<CheckinAncillaryCouponData, Coupons> {

	@Override
	public Coupons convert(CheckinAncillaryCouponData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		Coupons destination = null;

		if (source != null) {
			destination = objectFactory.createCoupons();

			destination.setX003CBaggageAllowanceX003EKBackingField(source.getBaggageAllowance());
			destination.setX003CFareClassX003EKBackingField(source.getFareClass());
			destination.setX003CFlightIdX003EKBackingField(source.getFlightId());
			destination.setX003CFlightNumberX003EKBackingField(source.getFlightNumber());
			destination.setX003CInboundCouponNumberX003EKBackingField(source.getInboundCouponNumber());
			if (source.getIsChild()!=null)
				destination.setX003CIsChildX003EKBackingField(source.getIsChild());
			if (source.getIsInfantAccompanist()!=null)
				destination.setX003CIsInfantAccompanistX003EKBackingField(source.getIsInfantAccompanist());
			if (source.getIsInfant()!=null)
				destination.setX003CIsInfantX003EKBackingField(source.getIsInfant());
			if (source.getIsSpecialPassenger()!=null)
				destination.setX003CIsSpecialPassengerX003EKBackingField(source.getIsSpecialPassenger());
			destination.setX003CNumberX003EKBackingField(source.getNumber());
			destination.setX003CPassengerNumberX003EKBackingField(source.getPassengerNumber());
			if (source.getIsInterline()!=null)
				destination.setX003CIsInterlineX003EKBackingField(source.getIsInterline());
			if (source.getIsSeatAssignDisabled()!=null)
				destination.setX003CIsSeatAssignDisabledX003EKBackingField(source.getIsSeatAssignDisabled());
			if (source.getIsSeatAssignPayment()!=null)
				destination.setX003CIsSeatAssignPaymentX003EKBackingField(source.getIsSeatAssignPayment());
			destination.setX003CSeatNumberX003EKBackingField(source.getSeatNumber());
			
			if (source.getUpgradeCompartimentalClasses()!=null){
				com.alitalia.aem.ws.checkin.ancillaryservice.xsd3.ObjectFactory objectFactory3 = 
						new com.alitalia.aem.ws.checkin.ancillaryservice.xsd3.ObjectFactory();
				ArrayOfCompartimentalClass compClasses = objectFactory3.createArrayOfCompartimentalClass();
				for (MmbCompartimentalClassEnum compClass : source.getUpgradeCompartimentalClasses()){
					compClasses.getCompartimentalClass().add(CompartimentalClass.fromValue(compClass.value()));
				}
				destination.setX003CUpgradeCompartimentalClassesX003EKBackingField(compClasses);
			}		
		}

		return destination;
	}

}
