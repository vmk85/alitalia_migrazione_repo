package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.messages.home.InfoPassengerCheckFrequentFlyersCodeResponse;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd1.CheckFrequentFlyersCodeResponse;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.AdultPassenger;

@Component(immediate=true, metatype=false)
@Service(value=CheckFrequentFlyersCodeResponseConverter.class)
public class CheckFrequentFlyersCodeResponseConverter 
		implements Converter<CheckFrequentFlyersCodeResponse, InfoPassengerCheckFrequentFlyersCodeResponse> {

	@Reference
	private AdultPassengerToAdultPassengerData adultPassengerToAdultPassengerDataConverter;
	
	@Override
	public InfoPassengerCheckFrequentFlyersCodeResponse convert(CheckFrequentFlyersCodeResponse source) {
		
		InfoPassengerCheckFrequentFlyersCodeResponse destination = null;
		
		if (source != null) {
	
			destination = new InfoPassengerCheckFrequentFlyersCodeResponse();
			
			if (source.getAdultPassengersWithWrongFrequentFlyer() != null && 
					source.getAdultPassengersWithWrongFrequentFlyer().getValue() != null &&
					source.getAdultPassengersWithWrongFrequentFlyer().getValue().getAdultPassenger() != null) {
				List<AdultPassengerData> adultPassengersWithWrongFrequentFlyer = new ArrayList<>();
				for (AdultPassenger adultPassenger : 
						source.getAdultPassengersWithWrongFrequentFlyer().getValue().getAdultPassenger()) {
					adultPassengersWithWrongFrequentFlyer.add(
							adultPassengerToAdultPassengerDataConverter.convert(
									adultPassenger));
				}
				destination.setAdultPassengersWithWrongFrequentFlyer(
						adultPassengersWithWrongFrequentFlyer);
			}
			
		}
		
		return destination;
	}

}
