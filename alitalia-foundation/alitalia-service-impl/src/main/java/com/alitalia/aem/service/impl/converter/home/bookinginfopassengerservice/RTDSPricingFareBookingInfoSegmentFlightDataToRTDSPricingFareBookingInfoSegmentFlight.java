package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareBookingInfoSegmentFlightData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionPricingFareBookingInfoSegmentFlight;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareBookingInfoSegmentFlightDataToRTDSPricingFareBookingInfoSegmentFlight.class)
public class RTDSPricingFareBookingInfoSegmentFlightDataToRTDSPricingFareBookingInfoSegmentFlight implements
		Converter<ResultTicketingDetailSolutionPricingFareBookingInfoSegmentFlightData, 
					ResultTicketingDetailSolutionPricingFareBookingInfoSegmentFlight> {

	@Override
	public ResultTicketingDetailSolutionPricingFareBookingInfoSegmentFlight convert(
			ResultTicketingDetailSolutionPricingFareBookingInfoSegmentFlightData source) {
		ResultTicketingDetailSolutionPricingFareBookingInfoSegmentFlight destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingFareBookingInfoSegmentFlight();

			destination.setCarrierField(source.getCarrierField());
			destination.setNumberField(source.getNumberField());
			destination.setNumberFieldSpecified(source.isNumberFieldSpecified());
		}

		return destination;
	}

}
