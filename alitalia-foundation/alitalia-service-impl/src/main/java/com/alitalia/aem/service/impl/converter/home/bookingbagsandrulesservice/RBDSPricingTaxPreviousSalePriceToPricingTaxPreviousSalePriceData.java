package com.alitalia.aem.service.impl.converter.home.bookingbagsandrulesservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingTaxPreviousSalePriceData;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd11.ResultBookingDetailsSolutionPricingTaxPreviousSalePrice;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingTaxPreviousSalePriceToPricingTaxPreviousSalePriceData.class)
public class RBDSPricingTaxPreviousSalePriceToPricingTaxPreviousSalePriceData
		implements Converter<ResultBookingDetailsSolutionPricingTaxPreviousSalePrice, ResultBookingDetailsSolutionPricingTaxPreviousSalePriceData> {

	@Override
	public ResultBookingDetailsSolutionPricingTaxPreviousSalePriceData convert(
			ResultBookingDetailsSolutionPricingTaxPreviousSalePrice source) {
		ResultBookingDetailsSolutionPricingTaxPreviousSalePriceData destination = null;
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingTaxPreviousSalePriceData();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}

	public ResultBookingDetailsSolutionPricingTaxPreviousSalePriceData convert(
			com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd13.ResultBookingDetailsSolutionPricingTaxPreviousSalePrice source) {
		ResultBookingDetailsSolutionPricingTaxPreviousSalePriceData destination = null;
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingTaxPreviousSalePriceData();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}
}
