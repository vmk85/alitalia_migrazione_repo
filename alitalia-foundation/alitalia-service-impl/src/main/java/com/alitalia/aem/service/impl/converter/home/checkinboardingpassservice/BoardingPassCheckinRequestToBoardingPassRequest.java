package com.alitalia.aem.service.impl.converter.home.checkinboardingpassservice;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.messages.home.BoardingPassCheckinRequest;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd1.BoardingPassRequest;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd1.Resource;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd2.ArrayOfPassenger;


@Component(immediate=true, metatype=false)
@Service(value=BoardingPassCheckinRequestToBoardingPassRequest.class)
public class BoardingPassCheckinRequestToBoardingPassRequest implements Converter<BoardingPassCheckinRequest, BoardingPassRequest> {

	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;
	
	@Reference
	private CheckinPassengerDataToPassenger checkinPassengerDataConverter;
	
	@Reference
	private CheckinRouteDataToRoute checkinRouteDataConverter;

	@Override
	public BoardingPassRequest convert(BoardingPassCheckinRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		BoardingPassRequest destination = null;

		if (source != null) {
			destination = objectFactory.createBoardingPassRequest();
			
			destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			destination.setX003CResourceX003EKBackingField(Resource.WEB);
			
			List<CheckinPassengerData> sourcePassengerList = source.getPassengers();
			if(sourcePassengerList !=null){
				ArrayOfPassenger passengersList = new com.alitalia.aem.ws.checkin.boardingpassservice.xsd2.ObjectFactory().createArrayOfPassenger();
				for(CheckinPassengerData sourcePassengerData : sourcePassengerList){
					passengersList.getPassenger().add(checkinPassengerDataConverter.convert(sourcePassengerData));
				}
				destination.setPassengers(objectFactory.createBoardingPassRequestPassengers(passengersList));
			}			

			destination.setSelectedRoute(objectFactory.createBoardingPassRequestSelectedRoute(checkinRouteDataConverter.convert(source.getSelectedRoute())));
		}

		return destination;
	}


}
