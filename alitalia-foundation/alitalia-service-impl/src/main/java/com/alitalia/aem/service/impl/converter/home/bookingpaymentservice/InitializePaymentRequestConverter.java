package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.ws.booking.paymentservice.xsd1.InitializePaymentRequest;
import com.alitalia.aem.ws.booking.paymentservice.xsd1.ObjectFactory;

@Component(immediate = true, metatype = false)
@Service(value = InitializePaymentRequestConverter.class)
public class InitializePaymentRequestConverter implements Converter<com.alitalia.aem.common.messages.home.InitializePaymentRequest, InitializePaymentRequest >{

	@Reference
	RoutesDataToRoutes routesConverter;

	@Reference
	InfoCarnetDataToInfoCarnet infoCarnetConverter;

	@Override
	public InitializePaymentRequest convert(com.alitalia.aem.common.messages.home.InitializePaymentRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		InitializePaymentRequest destination = objectFactory.createInitializePaymentRequest();
		destination.setUserAgent(objectFactory.createInitializePaymentRequestUserAgent(source.getUserAgent()));
		destination.setPrenotation(objectFactory.createInitializePaymentRequestPrenotation(routesConverter.convert(source.getPrenotation())));
		destination.setIPAddress(objectFactory.createInitializePaymentRequestIPAddress(source.getIpAddress()));
		destination.setCarnetInfo(objectFactory.createInitializePaymentRequestCarnetInfo(infoCarnetConverter.convert(source.getCarnetInfo())));
		destination.setCookie(objectFactory.createInitializePaymentRequestCookie(source.getCookie()));
		destination.setExecution(objectFactory.createInitializePaymentRequestExecution(source.getExecution()));
		destination.setSabreGateWayAuthToken(objectFactory.createInitializePaymentRequestSabreGateWayAuthToken(source.getSabreGateWayAuthToken()));
		
		return destination;
	}
}
