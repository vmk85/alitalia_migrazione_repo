package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMLifestyleData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Lifestyle;

@Component(immediate = true, metatype = false)
@Service(value = MMLifestyleDataToLifestyle.class)
public class MMLifestyleDataToLifestyle implements Converter<MMLifestyleData, Lifestyle> {

	@Override
	public Lifestyle convert(MMLifestyleData source) {
		Lifestyle destination = null;

		if (source != null) {
			com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ObjectFactory objectFactory2 = new com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ObjectFactory();

			destination = objectFactory2.createLifestyle();
			destination.setCategory(objectFactory2.createLifestyleCategory(source.getCategory()));
			destination.setDescription(objectFactory2.createLifestyleDescription(source.getDescription()));
			destination.setLifeStyle(objectFactory2.createLifestyleLifeStyle(source.getLifestyle()));

			if (source.getCreateDate() != null)
				destination.setCreateDate(XsdConvertUtils.toXMLGregorianCalendar(source.getCreateDate()));
		}

		return destination;
	}

}
