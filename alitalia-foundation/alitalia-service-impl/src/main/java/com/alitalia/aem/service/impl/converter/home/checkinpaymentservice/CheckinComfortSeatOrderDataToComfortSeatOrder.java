package com.alitalia.aem.service.impl.converter.home.checkinpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinComfortSeatOrderData;
import com.alitalia.aem.ws.checkin.paymentservice.xsd2.ComfortSeatOrder;
import com.alitalia.aem.ws.checkin.paymentservice.xsd2.ObjectFactory;


@Component(immediate=true, metatype=false)
@Service(value=CheckinComfortSeatOrderDataToComfortSeatOrder.class)
public class CheckinComfortSeatOrderDataToComfortSeatOrder implements Converter<CheckinComfortSeatOrderData, ComfortSeatOrder> {

	@Reference
	private CheckinFlightDataToFlight checkinFlightDataConverter;

	@Override
	public ComfortSeatOrder convert(CheckinComfortSeatOrderData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		ComfortSeatOrder destination = null;

		if (source != null) {
			destination = objectFactory.createComfortSeatOrder();

			destination.setX003CIdX003EKBackingField(source.getId());
			destination.setX003CFlightX003EKBackingField(checkinFlightDataConverter.convert(source.getFlight()));
			destination.setX003CSeatX003EKBackingField(source.getSeat());
			destination.setX003CTicketNumberX003EKBackingField(source.getTicketNumber());
			
		}

		return destination;
	}

}
