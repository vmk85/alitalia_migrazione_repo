package com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.MmbFrequentFlyerCarrierData;
import com.alitalia.aem.common.messages.home.RetrieveMmbFrequentFlyerCarriersResponse;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.ArrayOfFrequentFlyerCarrier;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd2.FrequentFlyerCarrier;

@Component(immediate = true, metatype = false)
@Service(value = ArrayOfFrequentFlyerCarrierToRetrieveMmbFrequentFlyerCarriersResponseConverter.class)
public class ArrayOfFrequentFlyerCarrierToRetrieveMmbFrequentFlyerCarriersResponseConverter 
	implements Converter<ArrayOfFrequentFlyerCarrier, RetrieveMmbFrequentFlyerCarriersResponse> {

	private static final Logger logger = LoggerFactory.getLogger(ArrayOfFrequentFlyerCarrierToRetrieveMmbFrequentFlyerCarriersResponseConverter.class);

	@Reference
	private FrequentFlyerCarrierToMmbFrequentFlyerCarrierData frequentFlyerCarrierConverter;
	
	@Reference
	private BookingMMBStaticDataServiceJAXBContextFactory jaxbContextFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public RetrieveMmbFrequentFlyerCarriersResponse convert(ArrayOfFrequentFlyerCarrier source) {
		RetrieveMmbFrequentFlyerCarriersResponse destination = new RetrieveMmbFrequentFlyerCarriersResponse();
		try {
			List<MmbFrequentFlyerCarrierData> frequentFlyerCarriersData = new ArrayList<>();
			
			Unmarshaller frequentFlyerCarrierUnmashaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			JAXBElement<FrequentFlyerCarrier> unmarshalledObj = null;

			List<FrequentFlyerCarrier> countries = source.getFrequentFlyerCarrier();
			for (Object frequentFlyerCarrier : countries) {
				FrequentFlyerCarrier frequentFlyerCarrierSource = null;
				if (frequentFlyerCarrier instanceof FrequentFlyerCarrier) {
					frequentFlyerCarrierSource = (FrequentFlyerCarrier) frequentFlyerCarrier;
				} else {
					unmarshalledObj = (JAXBElement<FrequentFlyerCarrier>) frequentFlyerCarrierUnmashaller.unmarshal((Node) frequentFlyerCarrier);
					frequentFlyerCarrierSource = unmarshalledObj.getValue();
				}
				MmbFrequentFlyerCarrierData phonePrefixData = frequentFlyerCarrierConverter.convert(frequentFlyerCarrierSource);
				frequentFlyerCarriersData.add(phonePrefixData);
			}
			
			destination.setFrequentFlyers(frequentFlyerCarriersData);
			
		} catch (JAXBException e) {
			logger.error("Error instantiating JAXBContext/Unmarshaller: {}", e);
		} finally {
			logger.info("Conversion executed successfully");
		}
		return destination;			
	}
}