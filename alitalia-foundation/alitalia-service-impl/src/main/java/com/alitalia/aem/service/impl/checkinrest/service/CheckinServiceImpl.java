package com.alitalia.aem.service.impl.checkinrest.service;

import com.alitalia.aem.common.data.checkinrest.model.clearancillariescart.response.ClearAncillariesCart;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PnrListInfoByAddPassenger;
import com.alitalia.aem.common.messages.checkinrest.*;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.response.AncillaryOffers;
import com.alitalia.aem.common.data.checkinrest.model.boardingpass.BoardingPass;
import com.alitalia.aem.common.data.checkinrest.model.boardingpass.BoardingPassInfo;
import com.alitalia.aem.common.data.checkinrest.model.boardingpassprintable.BoardingPassPrintable;
import com.alitalia.aem.common.data.checkinrest.model.checkinselectedpassenger.response.Outcome;
import com.alitalia.aem.common.data.checkinrest.model.enhancedseatmap.response.EnhancedSeatMapResp;
import com.alitalia.aem.common.data.checkinrest.model.fasttrack.response.FastTrack;
import com.alitalia.aem.common.data.checkinrest.model.flightdetail.FlightDetail;
import com.alitalia.aem.common.data.checkinrest.model.lounge.response.Lounge;
import com.alitalia.aem.common.data.checkinrest.model.passenger.Passenger;
import com.alitalia.aem.common.data.checkinrest.model.passengerdata.PassengerData;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PnrListInfoByFrequentFlyerSearch;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PnrListInfoByPnrSearch;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PnrListInfoByTicketSearch;
import com.alitalia.aem.common.data.checkinrest.model.reprintbp.FreeTextInfoList;
import com.alitalia.aem.common.data.checkinrest.model.setbaggage.response.Baggage;
import com.alitalia.aem.common.data.checkinrest.model.tripsearch.Trip;
import com.alitalia.aem.common.data.checkinrest.model.updatepassenger.response.UpdatePassengerResp;
import com.alitalia.aem.common.data.checkinrest.model.updatepassengerdetails.response.UpdatePassengerDetailsResp;
import com.alitalia.aem.common.data.checkinrest.model.updatesecurityinfo.response.UpdateSecurityInfoResp;
import com.alitalia.aem.common.messages.checkinrest.CheckinChangeSeatsRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinChangeSeatsResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinClearAncillarySessionEndRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinClearAncillarySessionEndResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinCreateBoardingPassRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinCreateBoardingPassResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinEnhancedSeatMapRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinEnhancedSeatMapResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinFlightDetailRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinFlightDetailResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinFrequentFlyerSearchRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinFrequentFlyerSearchResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetBoardingPassInfoRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetBoardingPassInfoResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetPassengerDataRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetPassengerDataResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetSelectedAncillaryOffersRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetSelectedAncillaryOffersResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinInitPaymentRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinInitPaymentResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinIsBoardingPassPrintableRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinIsBoardingPassPrintableResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinPassengerRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinPassengerResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinPnrSearchRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinPnrSearchResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinReprintBPRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinReprintBPResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinSelectedPassengerRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinSelectedPassengerResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinSetBaggageRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinSetBaggageResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinSetFastTrackRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinSetFastTrackResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinSetLoungeRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinSetLoungeResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinTicketSearchRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinTicketSearchResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinTripSearchRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinTripSearchResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinUpdatePassengerDetailsRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinUpdatePassengerDetailsResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinUpdatePassengerRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinUpdatePassengerResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinUpdateSecurityInfoRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinUpdateSecurityInfoResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinOffloadResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinSetSmsDataResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinSetSmsDataRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetSmsDataResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetSmsDataRequest;
import com.alitalia.aem.service.api.home.ICheckinService;
import com.alitalia.aem.service.impl.checkinrest.client.CheckinServiceClient;
import com.alitalia.aem.service.impl.checkinrest.utils.CheckinMessageUnmarshal;

@Service
@Component(immediate = true, metatype = false)
public class CheckinServiceImpl implements ICheckinService{

	private static final Logger logger = LoggerFactory.getLogger(CheckinServiceImpl.class);
	
	@Reference
	private CheckinServiceClient checkinServiceClient;
	
	@Override
	public CheckinTripSearchResponse retrieveCheckinTrip(CheckinTripSearchRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveCheckinTrip. The request is {}", request);
		}
		CheckinTripSearchResponse response = null;
		try {
			response = new CheckinTripSearchResponse();
			String url = "/tripsearch";
			Trip responseTrip = new Trip();
			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinServiceClient.getData(request, url);
			responseTrip = CheckinMessageUnmarshal.checkinTripSearchUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.setTripData(responseTrip);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}

	@Override
	public CheckinPnrSearchResponse retrieveCheckinPnr(CheckinPnrSearchRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveCheckInPnr. the request is {}", request);
		}
		CheckinPnrSearchResponse response = null;
		try {
			response = new CheckinPnrSearchResponse();
			String url = "/searchbypnr";
			PnrListInfoByPnrSearch responsePnrInfo = new PnrListInfoByPnrSearch();
			
			//Chiedo i dati al CheckinServiceClient
//			logger.info("[CheckinServiceImpl] Prima di invocare il checkinServiceClient.");
			String responseClient = checkinServiceClient.getData(request,  url);
			responsePnrInfo = CheckinMessageUnmarshal.checkinPnrSearchUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.setPnrData(responsePnrInfo);
		}
		catch (Exception e) {
			logger.error("[CheckinServiceImpl] - Errore durante la retrieveCheckinPnr: " + e.getMessage());
		}
		return response;
	}
	
	@Override
	public CheckinGetPassengerDataResponse retrievePassengerData(CheckinGetPassengerDataRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrievePassengerData. the request is {}", request);
		}
		CheckinGetPassengerDataResponse response = null;
		try {
			response = new CheckinGetPassengerDataResponse();
			String url = "/getpassengerdata";
			
			PassengerData passengerData = new PassengerData();
			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinServiceClient.getData(request,  url);
			passengerData = CheckinMessageUnmarshal.checkinGetPassengerDataUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.setPassengerData(passengerData);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}

	@Override
	public CheckinTicketSearchResponse retrieveCheckinTicket(CheckinTicketSearchRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveCheckInTicket. the request is {}", request);
		}
		CheckinTicketSearchResponse response = null;
		try {
			response = new CheckinTicketSearchResponse();
			String url = "/searchbyticket";
			PnrListInfoByTicketSearch responsePnrInfo = new PnrListInfoByTicketSearch();
			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinServiceClient.getData(request,  url);
			responsePnrInfo = CheckinMessageUnmarshal.checkinTicketSearchUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.setPnrData(responsePnrInfo);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}

	@Override
	public CheckinFrequentFlyerSearchResponse retrieveCheckinFrequentFlyer(CheckinFrequentFlyerSearchRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveCheckInFrequentFlyer. the request is {}", request);
		}
		CheckinFrequentFlyerSearchResponse response = null;
		try {
			response = new CheckinFrequentFlyerSearchResponse();
			String url = "/searchbyfrequentflyer";
			PnrListInfoByFrequentFlyerSearch responsePnrInfo = new PnrListInfoByFrequentFlyerSearch();
			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinServiceClient.getData(request,  url);
			responsePnrInfo = CheckinMessageUnmarshal.checkinFrequentFlyerSearchUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.setPnrData(responsePnrInfo);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}

	@Override
	public CheckinSelectedPassengerResponse checkinSelectedPassenger(CheckinSelectedPassengerRequest request) {
		if (logger.isDebugEnabled()) {
//			logger.debug("Executing method checkinSelectedPassenger. the request is {}", request);
		}
		CheckinSelectedPassengerResponse response = null;
		try {
			response = new CheckinSelectedPassengerResponse();
			String url = "/checkinselectedpassenger";
			Outcome responseOutcome = new Outcome();
			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinServiceClient.getData(request,  url);
			responseOutcome = CheckinMessageUnmarshal.checkinSelectedPassengerUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.setOutcome(responseOutcome);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}
	
	@Override
	public CheckinFlightDetailResponse retrieveFlightDetail(CheckinFlightDetailRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveFlightDetail. the request is {}", request);
		}
		CheckinFlightDetailResponse response = null;
		try {
			response = new CheckinFlightDetailResponse();
			String url = "/flightdetail";
			
			FlightDetail flightDetail = new FlightDetail();
			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinServiceClient.getData(request,  url);
			flightDetail = CheckinMessageUnmarshal.checkinFlightDetailUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.setFlightDetail(flightDetail);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}

	@Override
	public CheckinPassengerResponse retrieveCheckinPassenger(CheckinPassengerRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveCheckinPassenger. the request is {}", request);
		}
		CheckinPassengerResponse response = null;
		try {
			response = new CheckinPassengerResponse();
			String url = "/checkinpassenger";
			
			Passenger passenger = new Passenger();
			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinServiceClient.getData(request,  url);
			passenger = CheckinMessageUnmarshal.checkinPassengerUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.setPassenger(passenger);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}

	@Override
	public CheckinReprintBPResponse retrieveCheckinNewBP(CheckinReprintBPRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveCheckinNewBP. the request is {}", request);
		}
		CheckinReprintBPResponse response = null;
		try {
			response = new CheckinReprintBPResponse();
			String url = "/reprintbp";
			
			FreeTextInfoList newBP = new FreeTextInfoList();
			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinServiceClient.getData(request,  url);
			newBP = CheckinMessageUnmarshal.checkinnewBPUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.setBpReprintedData(newBP);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}

	@Override
	public CheckinIsBoardingPassPrintableResponse retrieveBoardingPassPrintable(
			CheckinIsBoardingPassPrintableRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveBoardingPassPrintable. the request is {}", request);
		}
		CheckinIsBoardingPassPrintableResponse response = null;
		try {
			response = new CheckinIsBoardingPassPrintableResponse();
			String url = "/isboardingpassprintable";
			
			BoardingPassPrintable isPrintable = new BoardingPassPrintable();
			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinServiceClient.getData(request,  url);
			isPrintable = CheckinMessageUnmarshal.checkinBoardingPassPrintableUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.setIsPrintable(isPrintable);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}

	@Override
	public CheckinCreateBoardingPassResponse retrieveBoardingPass(CheckinCreateBoardingPassRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveBoardingPass. the request is {}", request);
		}
		CheckinCreateBoardingPassResponse response = null;
		try {
			response = new CheckinCreateBoardingPassResponse();
			String url = "/createboardingpass";
			
			BoardingPass boardingPass = new BoardingPass();
			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinServiceClient.getData(request,url);
			boardingPass = CheckinMessageUnmarshal.checkinBoardingPassUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.setBoardingPass(boardingPass);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}

	@Override
	public CheckinSetBaggageResponse setBaggage(CheckinSetBaggageRequest request) {
//		if (logger.isDebugEnabled()) {
//			logger.debug("Executing method setBaggage. the request is {}", request);
//		}
		CheckinSetBaggageResponse response = null;
		try {
			response = new CheckinSetBaggageResponse();
			String url = "/setbaggage";
			
			Baggage baggage = new Baggage();
			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinServiceClient.getData(request,url);
			baggage = CheckinMessageUnmarshal.checkinSetBaggageUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.setBaggage(baggage);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}

	@Override
	public CheckinGetSelectedAncillaryOffersResponse retrieveSelectedAncillaryOffers(
			CheckinGetSelectedAncillaryOffersRequest request) {
//		if (logger.isDebugEnabled()) {
//			logger.debug("Executing method retrieveSelectedAncillaryOffers. the request is {}", request);
//		}
		CheckinGetSelectedAncillaryOffersResponse response = null;
		try {
			response = new CheckinGetSelectedAncillaryOffersResponse();
			String url = "/getselectedancillaryoffers";
			AncillaryOffers ancillaryOffers = new AncillaryOffers();
			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinServiceClient.getData(request,  url);
			ancillaryOffers = CheckinMessageUnmarshal.checkinGetSelectedAncillayOffersUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.setAncillaryOffers(ancillaryOffers);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}

	@Override
	public CheckinEnhancedSeatMapResponse enhancedSeatMap(CheckinEnhancedSeatMapRequest request) {
//		if (logger.isDebugEnabled()) {
//			logger.debug("Executing method enhancedSeatMap. the request is {}", request);
//		}
		CheckinEnhancedSeatMapResponse response = null;
		try {
			response = new CheckinEnhancedSeatMapResponse();
			String url = "/enhancedseatmap";
		
			EnhancedSeatMapResp enhancedSeatMapResp = new EnhancedSeatMapResp();
			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinServiceClient.getData(request,  url);
			enhancedSeatMapResp = CheckinMessageUnmarshal.CheckinEnhancedSeatMapUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.set_enhancedseatmapResp(enhancedSeatMapResp);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}

	@Override
	public CheckinChangeSeatsResponse changeSeats(CheckinChangeSeatsRequest request) {
//		if (logger.isDebugEnabled()) {
//			logger.debug("Executing method changeSeats. the request is {}", request);
//		}
		CheckinChangeSeatsResponse response = null;
		try {
			response = new CheckinChangeSeatsResponse();
			String url = "/changeseats";
			
			//ChangeSeatsResp changeSeatsResp = new ChangeSeatsResp();
			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinServiceClient.getData(request,  url);
			response = CheckinMessageUnmarshal.CheckinChangeSeatsUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			//response.set_changeseatsResp(changeSeatsResp);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}

	@Override
	public CheckinUpdateSecurityInfoResponse updateSecurityInfo(CheckinUpdateSecurityInfoRequest request) {
//		if (logger.isDebugEnabled()) {
//			logger.debug("Executing method updateSecurityInfo. the request is {}", request);
//		}
		CheckinUpdateSecurityInfoResponse response = null;
		try {
			response = new CheckinUpdateSecurityInfoResponse();
			String url = "/updatesecurityinfo";
			
			UpdateSecurityInfoResp updateSecurityInfoResp = new UpdateSecurityInfoResp();
			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinServiceClient.getData(request,  url);
			updateSecurityInfoResp = CheckinMessageUnmarshal.CheckinUpdateSecurityInfoUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.set_updatesecurityinfoResp(updateSecurityInfoResp);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}
	
	@Override
	public CheckinSetFastTrackResponse setFastTrack(
			CheckinSetFastTrackRequest request) {
//		if (logger.isDebugEnabled()) {
//			logger.debug("Executing method setFastTrack. the request is {}", request);
//		}
		CheckinSetFastTrackResponse response = null;
		try {
			response = new CheckinSetFastTrackResponse();
			String url = "/setfasttrack";
			
			FastTrack fastTrack = new FastTrack();
			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinServiceClient.getData(request,  url);
			fastTrack = CheckinMessageUnmarshal.checkinSetFastTrackUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.setFastTrack(fastTrack);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}

	@Override
	public CheckinSetLoungeResponse setLounge(CheckinSetLoungeRequest request) {
//		if (logger.isDebugEnabled()) {
//			logger.debug("Executing method setLounge. the request is {}", request);
//		}
		CheckinSetLoungeResponse response = null;
		try {
			response = new CheckinSetLoungeResponse();
			String url = "/setlounge";
			
			Lounge lounge = new Lounge();
			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinServiceClient.getData(request,  url);
			lounge = CheckinMessageUnmarshal.checkinSetLoungeUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.setLounge(lounge);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}


	@Override
	public CheckinUpdatePassengerDetailsResponse updatePassengerDetails(CheckinUpdatePassengerDetailsRequest request) {
//		if (logger.isDebugEnabled()) {
//			logger.debug("Executing method updatePassengerDetails. the request is {}", request);
//		}
		CheckinUpdatePassengerDetailsResponse response = null;
		try {
			response = new CheckinUpdatePassengerDetailsResponse();
			String url = "/updatepassengerdetails";
			
			UpdatePassengerDetailsResp updatePassengerDetailsResp = new UpdatePassengerDetailsResp();
			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinServiceClient.getData(request,  url);
			updatePassengerDetailsResp = CheckinMessageUnmarshal.CheckinUpdatePassengerDetailsUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.set_updatepassengerdetailsResp(updatePassengerDetailsResp);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}
	
	@Override
	public CheckinUpdatePassengerResponse updatePassenger(CheckinUpdatePassengerRequest request) {
//		if (logger.isDebugEnabled()) {
//			logger.debug("Executing method updatePassengerDetails. the request is {}", request);
//		}
		CheckinUpdatePassengerResponse response = null;
		try {
			response = new CheckinUpdatePassengerResponse();
			String url = "/updatepassenger";
			
			UpdatePassengerResp updatePassengerResp = new UpdatePassengerResp();
			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinServiceClient.getData(request,  url);
			updatePassengerResp = CheckinMessageUnmarshal.CheckinUpdatePassengerUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.set_updatepassengerResp(updatePassengerResp);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}
	
	@Override
	public CheckinGetBoardingPassInfoResponse getBoardingPassInfo(CheckinGetBoardingPassInfoRequest request) {
//		if (logger.isDebugEnabled()) {
//			logger.debug("Executing method getBoardingPassInfo. the request is {}", request);
//		}
		CheckinGetBoardingPassInfoResponse response = null;
		try {
			response = new CheckinGetBoardingPassInfoResponse();
			String url = "/getboardinigpassinfo";
			
			BoardingPassInfo boardingPassInfo = new BoardingPassInfo();
			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinServiceClient.getData(request,  url);
			boardingPassInfo = CheckinMessageUnmarshal.checkinGetBoardingPassInfoUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.setBoardingPassInfo(boardingPassInfo);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}

	@Override
	public CheckinClearAncillarySessionEndResponse clearAcillayEndSession(
			CheckinClearAncillarySessionEndRequest request) {
//		if (logger.isDebugEnabled()) {
//			logger.debug("Executing method clearAcillayEndSession. the request is {}", request);
//		}
		CheckinClearAncillarySessionEndResponse response = null;
		try {
			response = new CheckinClearAncillarySessionEndResponse();
			String url = "/clearancillariessessionend";

			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinServiceClient.getData(request,  url);
			response = CheckinMessageUnmarshal.ClearAncillaySessionEndUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}


	@Override
	public CheckinClearAncillariesCartResponse clearAncillariesCart(
			CheckinClearAncillariesCartRequest request) {
//		if (logger.isDebugEnabled()) {
//			logger.debug("Executing method CheckinClearAncillariesCartResponse. the request is {}", request);
//		}
		CheckinClearAncillariesCartResponse response = null;
		try {
			response = new CheckinClearAncillariesCartResponse();
			String url = "/clearancillariescart";

			ClearAncillariesCart resp = new ClearAncillariesCart();

			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinServiceClient.getData(request,  url);
			resp = CheckinMessageUnmarshal.clearAncillaiesCartUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.setClearAncillariesCart(resp);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}

	@Override
	public CheckinAddPassengerResponse retrieveCheckinAddPassenger(CheckinAddPassengerRequest request) {
//		if (logger.isDebugEnabled()) {
//			logger.debug("Executing method retrieveAddPassenger. the request is {}", request);
//		}
		CheckinAddPassengerResponse response = null;
		try {
			response = new CheckinAddPassengerResponse();
			String url = "/addpassenger";
			PnrListInfoByAddPassenger responsePnrInfo = new PnrListInfoByAddPassenger();
			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinServiceClient.getData(request,  url);
			responsePnrInfo = CheckinMessageUnmarshal.checkinAddPassengerUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.setPnrData(responsePnrInfo);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}
	@Override
	public CheckinOffloadResponse offload(CheckinOffloadRequest request) {
//				if (logger.isDebugEnabled()) {
//						logger.debug("Executing method offload. the request is {}", request);
//					}
				CheckinOffloadResponse response = null;
				try {
						response = new CheckinOffloadResponse();
						String url = "/offload";

								String responseClient = checkinServiceClient.getData(request, url);
						response = CheckinMessageUnmarshal.OffloadUnmarshal(responseClient);
						response.setTid(request.getTid());
						response.setSid(request.getSid());
					} catch (Exception e) {
						logger.error(e.getMessage());
					}
				return response;
			}

	@Override
	public CheckinOffloadFlightsResponse offloadFlights(CheckinOffloadFlightsRequest request) {
//		if (logger.isDebugEnabled()) {
//			logger.debug("Executing method offload. the request is {}", request);
//		}
		CheckinOffloadFlightsResponse response = null;
		try {
			response = new CheckinOffloadFlightsResponse();
			String url = "/offloadflights";

			String responseClient = checkinServiceClient.getData(request, url);
			response = CheckinMessageUnmarshal.OffloadFlightsUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}

	@Override
	public CheckinGetBoardingPassInfoByNameResponse getBoardingPassInfoByName(CheckinGetBoardingPassInfoByNameRequest request) {
//		if (logger.isDebugEnabled()) {
//			logger.debug("Executing method getBoardingPassInfo. the request is {}", request);
//		}
		CheckinGetBoardingPassInfoByNameResponse response = null;
		try {
			response = new CheckinGetBoardingPassInfoByNameResponse();
			String url = "/getboardinigpassinfoByName";

			BoardingPassInfo boardingPassInfo = new BoardingPassInfo();
			//Chiedo i dati al CheckinServiceClient
			String responseClient = checkinServiceClient.getData(request,  url);
			boardingPassInfo = CheckinMessageUnmarshal.checkinGetBoardingPassInfoUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.setBoardingPassInfo(boardingPassInfo);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}

	@Override
	public CheckinSetSmsDataResponse setSmsData(CheckinSetSmsDataRequest request) {
//		if (logger.isDebugEnabled()) {
//			logger.debug("Executing method offload. the request is {}", request);
//		}
		CheckinSetSmsDataResponse response = null;
		try {
			response = new CheckinSetSmsDataResponse();
			String url = "/setsmsdata";

			String responseClient = checkinServiceClient.getData(request, url);
			response = CheckinMessageUnmarshal.setSmsDataUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}

	@Override
	public CheckinGetSmsDataResponse getSmsData(CheckinGetSmsDataRequest request) {
//		if (logger.isDebugEnabled()) {
//			logger.debug("Executing method offload. the request is {}", request);
//		}
		CheckinGetSmsDataResponse response = null;
		try {
			response = new CheckinGetSmsDataResponse();
			String url = "/getsmsdata";

			String responseClient = checkinServiceClient.getData(request, url);
			response = CheckinMessageUnmarshal.getSmsDataUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}
}

