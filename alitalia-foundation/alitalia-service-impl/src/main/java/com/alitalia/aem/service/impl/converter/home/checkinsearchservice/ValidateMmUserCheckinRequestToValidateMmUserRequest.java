/**
 * 
 */
package com.alitalia.aem.service.impl.converter.home.checkinsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.ValidateMmUserCheckinRequest;
import com.alitalia.aem.ws.checkin.searchservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.searchservice.xsd1.Resource;
import com.alitalia.aem.ws.checkin.searchservice.xsd1.ValidateMmUserRequest;


@Component(immediate=true, metatype=false)
@Service(value=ValidateMmUserCheckinRequestToValidateMmUserRequest.class)
public class ValidateMmUserCheckinRequestToValidateMmUserRequest implements Converter<ValidateMmUserCheckinRequest, ValidateMmUserRequest> {

	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;

	@Override
	public ValidateMmUserRequest convert(ValidateMmUserCheckinRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		ValidateMmUserRequest destination = null;

		if (source != null) {
			
			destination = objectFactory.createValidateMmUserRequest();
			destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			destination.setX003CResourceX003EKBackingField(Resource.WEB);
			destination.setMMCode(objectFactory.createValidateMmUserRequestMMCode(source.getMmCode()));
			destination.setMMPin(objectFactory.createValidateMmUserRequestMMPin(source.getMmPin()));
			
		}

		return destination;
	}

}
