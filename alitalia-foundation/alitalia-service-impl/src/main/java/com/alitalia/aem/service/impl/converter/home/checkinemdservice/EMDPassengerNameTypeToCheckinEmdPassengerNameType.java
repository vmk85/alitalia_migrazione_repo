package com.alitalia.aem.service.impl.converter.home.checkinemdservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinEmdPassengerNameType;
import com.alitalia.aem.ws.checkin.emdservice.xsd2.EMDPassengerNameType;


@Component(immediate=true, metatype=false)
@Service(value=EMDPassengerNameTypeToCheckinEmdPassengerNameType.class)
public class EMDPassengerNameTypeToCheckinEmdPassengerNameType implements
		Converter<EMDPassengerNameType, CheckinEmdPassengerNameType> {


	@Override
	public CheckinEmdPassengerNameType convert(EMDPassengerNameType source) {
		CheckinEmdPassengerNameType destination = null;

		if (source != null) {
			
			destination = new CheckinEmdPassengerNameType();	
			destination.setSurname(source.getX003CSurnameX003EKBackingField());
			List<String> givenName = new ArrayList<>();
			
			if (source.getX003CGivenNameX003EKBackingField()!=null &&
					source.getX003CGivenNameX003EKBackingField().getString()!=null &&
					source.getX003CGivenNameX003EKBackingField().getString().size()>0){
				for (String s : source.getX003CGivenNameX003EKBackingField().getString())
					givenName.add(s);
			}
			destination.setGivenName(givenName);
		}
		return destination;
	}
}