package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PassengerBaseInfoData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.searchservice.xsd3.InfoBase;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ObjectFactory;
import com.alitalia.aem.ws.booking.searchservice.xsd4.GenderType;

@Component(immediate=true, metatype=false)
@Service(value=InfoBaseDataToInfoBase.class)
public class InfoBaseDataToInfoBase 
		implements Converter<PassengerBaseInfoData, InfoBase> {
	
	@Override
	public InfoBase convert(PassengerBaseInfoData source) {
		
		InfoBase destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			destination = objectFactory.createInfoBase();
			
			destination.setBirthDate(
					XsdConvertUtils.toXMLGregorianCalendar(
							source.getBirthDate()));
			
			destination.setGender(
					GenderType.fromValue(
							source.getGender().value()));
			
		}
		
		return destination;
	}

}
