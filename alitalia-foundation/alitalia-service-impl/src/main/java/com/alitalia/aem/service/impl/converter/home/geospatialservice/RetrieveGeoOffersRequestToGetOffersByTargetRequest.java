package com.alitalia.aem.service.impl.converter.home.geospatialservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveGeoOffersRequest;
import com.alitalia.aem.ws.geospatial.service.xsd1.GetOffersByTargetRequest;
import com.alitalia.aem.ws.geospatial.service.xsd1.ObjectFactory;
import com.alitalia.aem.ws.geospatial.service.xsd8.CodAreaEnum;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveGeoOffersRequestToGetOffersByTargetRequest.class)
public class RetrieveGeoOffersRequestToGetOffersByTargetRequest implements Converter<RetrieveGeoOffersRequest, GetOffersByTargetRequest> {
	
	@Override
	public GetOffersByTargetRequest convert(RetrieveGeoOffersRequest source) {
		
		GetOffersByTargetRequest request = new GetOffersByTargetRequest();
		
		String departureAirpordCode = source.getDepartureAirportCode();
		String arrivalAirpordCode = source.getArrivalAirportCode();
		String marketCode = source.getMarketCode();
		String paxType = source.getPaxType();
		String target = source.getTarget();
		Integer rngpryDwn = source.getRngpryDwn();
		Integer rngpryUp = source.getRngpryUp();
		
		ObjectFactory objectFactory = new ObjectFactory();
		
		JAXBElement<String> jaxbDepartureAirport = objectFactory.createGetOffersByTargetRequestDepartureAirportCode(departureAirpordCode);
		JAXBElement<String> jaxbMarketCode = objectFactory.createGetOffersByTargetRequestMarketCode(marketCode);
		JAXBElement<String> jaxbPaxType = objectFactory.createGetOffersByTargetRequestPaxType(paxType);
		JAXBElement<String> jaxbArrivalAirport = objectFactory.createGetOffersByTargetRequestArrivalAirportCode(arrivalAirpordCode);

		JAXBElement<String> jaxbTarget = objectFactory.createGetOffersByTargetRequestTarget(target);
		JAXBElement<Integer> jaxbRngpryDwn = objectFactory.createGetOffersByTargetRequestRngpryDwn(rngpryDwn);
		JAXBElement<Integer> jaxbRngpryUp = objectFactory.createGetOffersByTargetRequestRngpryUp(rngpryUp);
		
		request.setDepartureAirportCode(jaxbDepartureAirport);
		request.setArrivalAirportCode(jaxbArrivalAirport);
		request.setMarketCode(jaxbMarketCode);
		request.setPaxType(jaxbPaxType);
		request.setTarget(jaxbTarget);
		request.setRngpryDwn(jaxbRngpryDwn);
		request.setRngpryUp(jaxbRngpryUp);
		
		if (source.getMonthDailyPrices() != null)
			request.setMonthDailyPrices(source.getMonthDailyPrices());
			
		if (source.getAreaValue() != null)
			request.setCodArea(CodAreaEnum.fromValue(source.getAreaValue().value()));
		
		return request;
	}
}