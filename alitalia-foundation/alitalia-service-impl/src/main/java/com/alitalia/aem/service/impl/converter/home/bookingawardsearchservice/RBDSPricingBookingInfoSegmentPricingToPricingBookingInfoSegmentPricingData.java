//2
package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatchData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricing;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatch;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFare;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingBookingInfoSegmentPricingToPricingBookingInfoSegmentPricingData.class)
public class RBDSPricingBookingInfoSegmentPricingToPricingBookingInfoSegmentPricingData
		implements Converter<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricing, ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData> {

	@Reference
	private RBDSPricingBookingInfoSegmentPricingBaggageMatchToPricingBookingInfoSegmentPricingBaggageMatchData rbdsPricingBookingInfoSegmentPricingBaggageMatchConverter;
	
	@Reference
	private RBDSPricingBookingInfoSegmentPricingFareToPricingBookingInfoSegmentPricingFareData rbdsPricingBookingInfoSegmentPricingFareConverter;
	
	@Override
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData convert(
			ResultBookingDetailsSolutionPricingBookingInfoSegmentPricing source) {
		ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData destination = null;
		
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData();
			
			List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatchData> arraydest_1 = new ArrayList<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatchData>();
			if(source.getExtField()!=null)
			for(ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatch s : source.getExtField().getResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatch()){
				arraydest_1.add(rbdsPricingBookingInfoSegmentPricingBaggageMatchConverter.convert(s));
			}
			destination.setExtField(arraydest_1);
			
			List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData> arraydest_2 = new ArrayList<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData>();
			if(source.getFareField()!=null)
			for(ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFare s : source.getFareField().getResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFare()){
				arraydest_2.add(rbdsPricingBookingInfoSegmentPricingFareConverter.convert(s));
			}
			destination.setFareField(arraydest_2);			
		}
		return destination;
	}
	
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData convert(
			com.alitalia.aem.ws.bookingaward.searchservice.xsd13.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricing source) {
		ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData destination = null;
		
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingData();
			
			List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatchData> arraydest_1 = new ArrayList<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatchData>();
			if(source.getExtField()!=null)
			for(com.alitalia.aem.ws.bookingaward.searchservice.xsd13.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatch s : source.getExtField().getResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatch()){
				arraydest_1.add(rbdsPricingBookingInfoSegmentPricingBaggageMatchConverter.convert(s));
			}
			destination.setExtField(arraydest_1);
			
			List<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData> arraydest_2 = new ArrayList<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFareData>();
			if(source.getFareField()!=null)
			for(com.alitalia.aem.ws.bookingaward.searchservice.xsd13.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFare s : source.getFareField().getResultBookingDetailsSolutionPricingBookingInfoSegmentPricingFare()){
				arraydest_2.add(rbdsPricingBookingInfoSegmentPricingFareConverter.convert(s));
			}
			destination.setFareField(arraydest_2);			
		}
		return destination;
	}

}
