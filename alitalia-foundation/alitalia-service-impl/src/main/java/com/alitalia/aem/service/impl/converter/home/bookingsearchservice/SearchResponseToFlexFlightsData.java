package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.FlexibleDatesMatrixData;
import com.alitalia.aem.ws.booking.searchservice.xsd1.SearchResponse;
import com.alitalia.aem.ws.booking.searchservice.xsd2.FlexibleDatesMatrix;
import com.alitalia.aem.ws.booking.searchservice.xsd2.ModelResponse;

@Component(immediate=true, metatype=false)
@Service(value=SearchResponseToFlexFlightsData.class)
public class SearchResponseToFlexFlightsData implements Converter<SearchResponse, List<FlexibleDatesMatrixData>> {

	private static final Logger logger = LoggerFactory.getLogger(SearchResponseToFlexFlightsData.class);
	
	@Reference
	private FlexibleDatesMatrixToFlexibleDatesMatrixData flexibleDatesMatrixConverter;

	@Override
	public List<FlexibleDatesMatrixData> convert(SearchResponse source) {
		List<FlexibleDatesMatrixData> destination;

		logger.debug("Converting SearchResponse to FlexibleDatesMatrixData List.");
		ModelResponse result = (ModelResponse) source.getResult().getValue();
		FlexibleDatesMatrix flexibleDatesMatrix = (FlexibleDatesMatrix) result.getModel().getValue();
		destination = flexibleDatesMatrixConverter.convert(flexibleDatesMatrix);

		return destination;
	}

}
