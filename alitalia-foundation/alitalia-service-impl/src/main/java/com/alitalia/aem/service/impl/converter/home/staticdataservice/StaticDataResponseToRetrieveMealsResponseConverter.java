package com.alitalia.aem.service.impl.converter.home.staticdataservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MealData;
import com.alitalia.aem.common.messages.home.RetrieveMealsResponse;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.StaticDataResponse;
import com.alitalia.aem.ws.booking.staticdataservice.xsd5.Meal;

@Component(immediate=true, metatype=false)
@Service(value=StaticDataResponseToRetrieveMealsResponseConverter.class)
public class StaticDataResponseToRetrieveMealsResponseConverter implements Converter<StaticDataResponse, RetrieveMealsResponse> {

	private static final Logger logger = LoggerFactory.getLogger(StaticDataResponseToRetrieveMealsResponseConverter.class);

	@Reference
	private MealToMealData mealConverter;

	@Reference
	private StaticDataServiceJAXBContextFactory jaxbContextFactory;

	@SuppressWarnings("unchecked")
	@Override
	public RetrieveMealsResponse convert(StaticDataResponse source) {
		RetrieveMealsResponse response = new RetrieveMealsResponse();
		try {
			List<MealData> mealsData = new ArrayList<>();
			
			Unmarshaller mealUnmashaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			JAXBElement<Meal> unmarshalledObj = null;

			List<Object> meals = source.getEntities().getValue().getAnyType();
			for(Object meal: meals){
				unmarshalledObj = (JAXBElement<Meal>) mealUnmashaller.unmarshal((Node) meal);
				Meal castedMeal = unmarshalledObj.getValue();
				MealData mealData = mealConverter.convert(castedMeal);
				mealsData.add(mealData);
			}
			
			response.setMeals(mealsData);
			
		} catch (JAXBException e) {
			logger.error("Error instantiating JAXBContext/Unmarshaller: {}", e);
		} finally {
			logger.info("Conversion executed successfully");
		}
		return response;			
		
	}
}