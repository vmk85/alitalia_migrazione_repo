package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMTelephoneData;
import com.alitalia.aem.common.data.home.enumerations.MMPhoneTypeEnum;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Telephone;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd3.ArrayOfanyType;

@Component(immediate = true, metatype = false)
@Service(value = TelephonesToMMTelephonesData.class)
public class TelephonesToMMTelephonesData implements Converter<ArrayOfanyType, List<MMTelephoneData>> {

	private static Logger logger = LoggerFactory.getLogger(TelephonesToMMTelephonesData.class);
	
	@Reference
	private MyPersonalareaMillemigliaServiceJAXBContextFactory jaxbContextFactory;
	
	@Override
	public List<MMTelephoneData> convert(ArrayOfanyType source) {
		
		if (source == null) {
			return null;
		}
		
		List<MMTelephoneData> mmTelephones = new ArrayList<MMTelephoneData>();

		for (Object object: source.getAnyType()) {
			try {
				Unmarshaller unmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
				
				JAXBElement<Telephone> jaxbElement = (JAXBElement<Telephone>) unmarshaller.unmarshal((Node) object);
				Telephone telephone = jaxbElement.getValue();
				
				MMTelephoneData mmProgramData = new MMTelephoneData();
				mmProgramData.setCountryNumber(telephone.getCountryNumber().getValue());
				mmProgramData.setEmail(telephone.getEmail().getValue());
				mmProgramData.setInvalidIndicator(telephone.getInvalidIndicator().getValue());
				mmProgramData.setIstruction(telephone.getIstruction().getValue());
				mmProgramData.setNumber(telephone.getNumber().getValue());
				mmProgramData.setOrigCustomerId(telephone.getOrigCustomerID().getValue());
				mmProgramData.setPhoneZone(telephone.getPhoneZone().getValue());
				mmProgramData.setSequenceNumber(telephone.getSequenceNumber().getValue());
				mmProgramData.setTelephoneType(MMPhoneTypeEnum.fromValue(telephone.getTelephoneType().value()));

				if (telephone.getEffectiveDate() != null)
					mmProgramData.setEffectiveDate(XsdConvertUtils.parseCalendar(telephone.getEffectiveDate()));
				
				mmTelephones.add(mmProgramData);
			
			} catch (JAXBException e) {
				logger.error("Error while creating JAXBConext/Unmarshller for List<MMTelephoneData>: {}", e);
			}
		}
		
		return mmTelephones;
	}
}