package com.alitalia.aem.service.impl.converter.home.statisticsservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RegisterStatisticsResponse;
import com.alitalia.aem.ws.tradestatistics.statisticsservice.xsd1.StatisticsResponse;

@Component(immediate = true, metatype = false)
@Service(value = TradeStatisticsResponseToRegisterStatisticsResponse.class)
public class TradeStatisticsResponseToRegisterStatisticsResponse implements Converter<StatisticsResponse, RegisterStatisticsResponse> {

	@Override
	public RegisterStatisticsResponse convert(StatisticsResponse source) {
		
		return new RegisterStatisticsResponse();
	
	}
}