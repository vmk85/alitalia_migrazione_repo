package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingFareRestrictionLatestReturnData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ResultBookingDetailsSolutionPricingFareRestrictionLatestReturn;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingFareRestrictionLatestReturnToRBDSPFRestrictionLatestReturnData.class)
public class RBDSPricingFareRestrictionLatestReturnToRBDSPFRestrictionLatestReturnData
		implements Converter<ResultBookingDetailsSolutionPricingFareRestrictionLatestReturn, ResultBookingDetailsSolutionPricingFareRestrictionLatestReturnData> {

	@Override
	public ResultBookingDetailsSolutionPricingFareRestrictionLatestReturnData convert(
			ResultBookingDetailsSolutionPricingFareRestrictionLatestReturn source) {
		ResultBookingDetailsSolutionPricingFareRestrictionLatestReturnData destination = null;
		
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingFareRestrictionLatestReturnData();
			destination.setDateField(source.getDateField());
			destination.setTimeField(source.getTimeField());
		}
		
		return destination;
	}

}
