package com.alitalia.aem.service.impl.converter.home.commonservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AirportDetailsData;
import com.alitalia.aem.ws.booking.commonservice.xsd2.AirportDetails;

@Component(immediate=true, metatype=false)
@Service(value=AirportDetailsToAirportDetailsData.class)
public class AirportDetailsToAirportDetailsData 
	implements Converter<AirportDetails, AirportDetailsData> {

	@Override
	public AirportDetailsData convert(AirportDetails source) {
		
		AirportDetailsData destination = null;
		
		if (source != null) {
			
			destination = new AirportDetailsData();
	
			if (source.getAZTerminal() != null) {
				destination.setAzTerminal(
						source.getAZTerminal().getValue());
			}
			
			destination.setCheckInTimeDomestic(
					source.getCheckInTimeDomestic());
			
			destination.setCheckInTimeIntercontinental(
					source.getCheckInTimeIntercontinental());
			
			destination.setCheckInTimeInternational(
					source.getCheckInTimeInternational());
			
			destination.setCityDistance(
					source.getCityDistance());
			
			destination.setCityTransferTime(
					source.getCityTransferTime());
			
			destination.setFusoOrarioGMT(
					source.getGMT());
			
			destination.setNotesCode(
					source.getNotesCode());
			
		}

		return destination;
	}

}
