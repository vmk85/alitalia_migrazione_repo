package com.alitalia.aem.service.impl.home;

import com.alitalia.aem.ws.booking.ancillaryservice.SabreDcAncillaryServiceClient;
import com.day.cq.wcm.api.Page;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.InsuranceRequest;
import com.alitalia.aem.common.messages.home.InsuranceResponse;
import com.alitalia.aem.common.messages.home.PayInsuranceRequest;
import com.alitalia.aem.common.messages.home.PayInsuranceResponse;
import com.alitalia.aem.service.api.home.AncillaryService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.bookingancillaryservice.GetInsuranceResponseToInsuranceResponse;
import com.alitalia.aem.service.impl.converter.home.bookingancillaryservice.InsuranceRequestToGetInsuranceRequest;
import com.alitalia.aem.ws.booking.ancillaryservice.AncillaryServiceClient;
import com.alitalia.aem.ws.booking.ancillaryservice.xsd1.GetInsuranceRequest;
import com.alitalia.aem.ws.booking.ancillaryservice.xsd1.GetInsuranceResponse;

@Service
@Component(immediate=true, metatype=false)
public class SimpleAncillaryService implements AncillaryService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleAncillaryService.class);

	
	@Reference
	private AncillaryServiceClient ancillaryClient;

	@Reference
	private InsuranceRequestToGetInsuranceRequest insuranceRequestconverter;

	@Reference
	private GetInsuranceResponseToInsuranceResponse getInsuranceResponseconverter;

	/** Migrazione 3.6 inizio*/

	@Reference
	private ResourceResolverFactory resolverFactory;

	@Reference
	private SabreDcAncillaryServiceClient sabreDcAncillaryServiceClient;

	/** Migrazione 3.6 fine*/

	@Override
	public InsuranceResponse getInsurance(InsuranceRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method getInsurance(). The request is {}", request);
		}
		


		try {
			
			InsuranceResponse response = new InsuranceResponse();
			GetInsuranceRequest getInsuranceRequest = insuranceRequestconverter.convert(request);

			/** Migrazione 3.6 inizio*/

			GetInsuranceResponse getInsuranceResponse = null;
			if (newSabreEnable(getInsuranceRequest.getCountryCode().getValue().toLowerCase())){
				getInsuranceResponse = sabreDcAncillaryServiceClient.getInsurance(getInsuranceRequest);
			} else {
				getInsuranceResponse = ancillaryClient.getInsurance(getInsuranceRequest);
			}

			/** Migrazione 3.6 fine*/

//			GetInsuranceResponse getInsuranceResponse = ancillaryClient.getInsurance(getInsuranceRequest);
			response = getInsuranceResponseconverter.convert(getInsuranceResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			return response;
			
		} catch (Exception e) {
			logger.error("Exception while executing method getInsurance().", e);
			throw new AlitaliaServiceException("Exception executing getInsurance: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public PayInsuranceResponse payInsurance(PayInsuranceRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method payInsurance. The request is {}", request);
		}
		

		try {
			
			PayInsuranceResponse response = new PayInsuranceResponse();
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			return response;
			
		} catch (Exception e) {
			logger.error("Exception while executing method payInsurance.", e);
			throw new AlitaliaServiceException("Exception executing payInsurance: Sid ["+request.getSid()+"]" , e);
		}
	}

	/** Migrazione 3.6 inizio*/

	private boolean newSabreEnable(String market) throws LoginException {

		ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
		Resource res = resourceResolver.getResource("/content/alitalia/alitalia-" + market);
		Page page = res.adaptTo(Page.class);
		ValueMap props = page.getProperties();
		String title = props.get("isNewService", "default value");
		logger.info("dato preso dalla pagina: " + title);
		return Boolean.parseBoolean(title);

	}

	/** Migrazione 3.6 fine*/

}