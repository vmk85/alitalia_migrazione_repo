package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.CrossSellingsResponse;
import com.alitalia.aem.common.messages.home.MmbCrossSellingsRequest;
import com.alitalia.aem.common.messages.home.SapInvoiceRequest;
import com.alitalia.aem.common.messages.home.SapInvoiceResponse;
import com.alitalia.aem.service.api.home.MmbTicketService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.mmbticketingservice.GetCrossSellingsResponseToCrossSellingsResponse;
import com.alitalia.aem.service.impl.converter.home.mmbticketingservice.MmbCrossSellingRequestToGetCrossSellingRequest;
import com.alitalia.aem.service.impl.converter.home.mmbticketingservice.SapInvoiceRequestToSAPRequest;
import com.alitalia.aem.ws.mmb.ticketingservice.MmbTicketingServiceClient;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd1.GetCrossSellingsRequest;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd6.GetCrossSellingsResponse;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd6.SAPRequest;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd6.SAPResponse;

@Component(immediate = true, metatype = false)
@Service
public class SimpleMmbTicketService implements MmbTicketService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleMmbTicketService.class);

	@Reference
	private MmbTicketingServiceClient mmbTicketingServiceClient;

	@Reference
	private MmbCrossSellingRequestToGetCrossSellingRequest mmbCrossSellingRequestToGetCrossSellingRequestConverter;

	@Reference
	private GetCrossSellingsResponseToCrossSellingsResponse getCrossSellingsResponseToCrossSellingsResponseConverter;

	@Reference
	private SapInvoiceRequestToSAPRequest sapRequestConverter;

	@Override
	public SapInvoiceResponse callSAPService(SapInvoiceRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method callSAPService. The request is {}", request);
		}
		

		try {
			SapInvoiceResponse response = new SapInvoiceResponse();
			SAPRequest sapRequest = sapRequestConverter.convert(request);
			SAPResponse callSAPServiceResponse = mmbTicketingServiceClient.callSAPService(sapRequest);
			/*
			 * La SAPResponse è banale, effettuo la conversione direttamente qui
			 */
			response.setResult(callSAPServiceResponse.getResult().getValue());
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method callSAPService(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method callSAPService(): {}", e);
			throw new AlitaliaServiceException("Exception executing callSAPService: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public CrossSellingsResponse getCrossSellings(MmbCrossSellingsRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method getCrossSellings. The request is {}", request);
		}
		

		try {
			CrossSellingsResponse response = new CrossSellingsResponse();

			GetCrossSellingsRequest serviceRequest = mmbCrossSellingRequestToGetCrossSellingRequestConverter.convert(request);
			GetCrossSellingsResponse serviceResponse = mmbTicketingServiceClient.getCrossSellings(serviceRequest);
			response = getCrossSellingsResponseToCrossSellingsResponseConverter.convert(serviceResponse);

			response.setTid(request.getTid());
			response.setSid(request.getSid());
			return response;
			
		} catch (Exception e) {
			logger.error("Exception while executing method getCrossSellings(): {}", e);
			throw new AlitaliaServiceException("Exception executing getCrossSellings: Sid ["+request.getSid()+"]" , e);
		}
	}

}
