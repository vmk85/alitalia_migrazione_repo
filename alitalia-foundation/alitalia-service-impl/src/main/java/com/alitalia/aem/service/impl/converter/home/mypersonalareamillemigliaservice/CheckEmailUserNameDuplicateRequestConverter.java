package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.*;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.CheckEmailUsernameDuplicateRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd5.FirstLastNameVerificationDataType;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd5.MemberAliasAuthenticationType;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd5.MemberIdentificationType;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd5.TicketNumberType;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

@Component(immediate = true, metatype = false)
@Service(value = CheckEmailUserNameDuplicateRequestConverter.class)
public class CheckEmailUserNameDuplicateRequestConverter implements Converter<SubscribeSACheckEmailUserNameDuplicateRequest, CheckEmailUsernameDuplicateRequest> {
	


	@Override
	public CheckEmailUsernameDuplicateRequest convert(SubscribeSACheckEmailUserNameDuplicateRequest source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");

		CheckEmailUsernameDuplicateRequest destination = new CheckEmailUsernameDuplicateRequest();
		ObjectFactory objectFactory=new ObjectFactory();

		destination.setIDProfilo(objectFactory.createCheckEmailUsernameDuplicateRequestIDProfilo(source.getProfileID()) );
		destination.setEmailUsername(objectFactory.createCheckEmailUsernameDuplicateRequestEmailUsername(source.getEmailUserName()) );

		return destination;
	}
}