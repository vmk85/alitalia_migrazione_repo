package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatchData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatch;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingBookingInfoSegmentPricingBaggageMatchDataToPricingBookingInfoSegmentPricingBaggageMatch.class)
public class RBDSPricingBookingInfoSegmentPricingBaggageMatchDataToPricingBookingInfoSegmentPricingBaggageMatch
		implements Converter<ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatchData, ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatch> {

	@Override
	public ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatch convert(
			ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatchData source) {
		ResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatch destination = null;
		
		if(source != null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionPricingBookingInfoSegmentPricingBaggageMatch();
			destination.setCommercialNameField(source.getCommercialNameField());
			destination.setCountField(source.getCountField());
			destination.setCountFieldSpecified(source.isCountFieldSpecified());
			destination.setSubcodeField(source.getSubcodeField());
		}
		return destination;
	}

}
