package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentLegOperationalDisclosureData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionSliceSegmentLegOperationalDisclosure;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentLegOperationalDisclosureDataToRTDSSliceSegmentLegOperationalDisclosure.class)
public class RTDSSliceSegmentLegOperationalDisclosureDataToRTDSSliceSegmentLegOperationalDisclosure implements
		Converter<ResultTicketingDetailSolutionSliceSegmentLegOperationalDisclosureData, 
					ResultTicketingDetailSolutionSliceSegmentLegOperationalDisclosure> {

	@Override
	public ResultTicketingDetailSolutionSliceSegmentLegOperationalDisclosure convert(
			ResultTicketingDetailSolutionSliceSegmentLegOperationalDisclosureData source) {
		ResultTicketingDetailSolutionSliceSegmentLegOperationalDisclosure destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionSliceSegmentLegOperationalDisclosure();

			destination.setCarrierField(source.getCarrierField());
			destination.setDisclosureTextField(source.getDisclosureTextField());
		}

		return destination;
	}

}
