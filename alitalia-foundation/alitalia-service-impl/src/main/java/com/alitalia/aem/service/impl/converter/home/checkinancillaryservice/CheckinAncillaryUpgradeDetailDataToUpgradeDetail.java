package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryUpgradeDetailData;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.UpgradeDetail;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd3.CompartimentalClass;


@Component(immediate=true, metatype=false)
@Service(value=CheckinAncillaryUpgradeDetailDataToUpgradeDetail.class)
public class CheckinAncillaryUpgradeDetailDataToUpgradeDetail implements Converter<CheckinAncillaryUpgradeDetailData, UpgradeDetail> {

	@Override
	public UpgradeDetail convert(CheckinAncillaryUpgradeDetailData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		UpgradeDetail destination = null;

		if (source != null) {
			destination = objectFactory.createUpgradeDetail();

			if (source.getCompartimentalClass() != null)
				destination.setX003CCompartimentalClassX003EKBackingField(CompartimentalClass.fromValue(source.getCompartimentalClass().value()));
			destination.setX003CSeatX003EKBackingField(source.getSeat());
		}

		return destination;
	}

}
