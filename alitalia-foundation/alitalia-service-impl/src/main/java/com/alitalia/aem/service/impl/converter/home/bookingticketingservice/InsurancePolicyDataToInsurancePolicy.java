package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.InsurancePolicyData;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.InsurancePolicy;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ObjectFactory;

@Component(immediate = true, metatype = false)
@Service(value = InsurancePolicyDataToInsurancePolicy.class)
public class InsurancePolicyDataToInsurancePolicy implements Converter<InsurancePolicyData, InsurancePolicy> {

	@Override
	public InsurancePolicy convert(InsurancePolicyData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		InsurancePolicy destination = objectFactory.createInsurancePolicy();
		
		if (source!=null){
			destination.setBuy(source.getBuy());
			destination.setCardType(objectFactory.createInsurancePolicyCardType(source.getCardType()));
			destination.setCurrency(objectFactory.createInsurancePolicyCurrency(source.getCurrency()));
			destination.setDiscount(source.getDiscount());
			destination.setDiscountedPrice(source.getDiscountedPrice());
			destination.setErrorDescription(objectFactory.createInsurancePolicyErrorDescription(source.getCurrency()));
			destination.setPolicyNumber(objectFactory.createInsurancePolicyPolicyNumber(source.getPolicyNumber()));
			destination.setTotalInsuranceCost(source.getTotalInsuranceCost());
			destination.setTypeOfTrip(objectFactory.createInsurancePolicyTypeOfTrip(source.getTypeOfTrip()));
		}
		return destination;
	}

}
