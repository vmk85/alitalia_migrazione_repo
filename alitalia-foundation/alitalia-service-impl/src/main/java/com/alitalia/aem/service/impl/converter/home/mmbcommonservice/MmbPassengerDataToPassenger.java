package com.alitalia.aem.service.impl.converter.home.mmbcommonservice;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.enumerations.ContactTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAQQTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbLinkTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbPassengerNoteEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbPassengerStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbUpdateSSREnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.common.data.home.mmb.MmbApisInfoData;
import com.alitalia.aem.common.data.home.mmb.MmbBaggageOrderData;
import com.alitalia.aem.common.data.home.mmb.MmbCouponData;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.MmbFrequentFlyerCarrierData;
import com.alitalia.aem.common.data.home.mmb.MmbPassengerData;
import com.alitalia.aem.common.data.home.mmb.MmbPreferencesData;
import com.alitalia.aem.common.data.home.mmb.MmbTicketData;
import com.alitalia.aem.ws.mmb.commonservice.xsd2.ArrayOfanyType;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.AQQType;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.ArrayOfCoupon;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.ArrayOfFlight;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.CompartimentalClass;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.LinkType;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.Passenger;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.ObjectFactory;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.PassengerNote;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.PassengerStatus;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.PassengerType;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.UpdateSSR;
import com.alitalia.aem.ws.mmb.commonservice.xsd4.ContactType;
import com.alitalia.aem.ws.mmb.commonservice.xsd4.Preferences;

@Component(immediate=true, metatype=false)
@Service(value=MmbPassengerDataToPassenger.class)
public class MmbPassengerDataToPassenger implements Converter<MmbPassengerData, Passenger> {

	@Reference
	private MmbApisInfoDataToApisData mmbApisInfoDataConverter;

	@Reference
	private MmbFrequentFlyerCarrierDataToFrequentFlyerCarrier mmbFrequentFlyerCarrierDataConverter;

	@Reference
	private MmbFrequentFlyerTypeDataToFrequentFlyerType mmbFrequentFlyerTypeDataConverter;

	@Reference
	private MmbBaggageOrderDataToBaggageOrder mmbBaggageOrderDataConverter;

	@Reference
	private MmbCouponDataToCoupon mmbCouponDataConverter;

	@Reference
	private MmbPreferencesDataToPreferences mmbPreferencesDataConverter;

	@Reference
	private MmbFlightDataToFlight mmbFlightDataConverter;

	@Override
	public Passenger convert(MmbPassengerData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		com.alitalia.aem.ws.mmb.commonservice.xsd2.ObjectFactory objectFactory2 = 
				new com.alitalia.aem.ws.mmb.commonservice.xsd2.ObjectFactory();
		Passenger destination = null;

		if (source != null) {
			destination = objectFactory.createPassenger();

			MmbAQQTypeEnum sourceAuthPermssion = source.getAuthorityPermission();
			if (sourceAuthPermssion != null)
				destination.setX003CAuthorityPermissionX003EKBackingField(
						AQQType.fromValue(sourceAuthPermssion.value()));

			destination.setX003CBaggageAllowanceQuantityX003EKBackingField(source.getBaggageAllowanceQuantity());
			destination.setX003CBaggageAllowanceX003EKBackingField(source.getBaggageAllowance());
			destination.setX003CBoardingPassRetrieveEmailX003EKBackingField(source.getBoardingPassRetrieveEmail());
			destination.setX003CBoardingPassRetrieveSmsX003EKBackingField(source.getBoardingPassRetrieveSms());
			destination.setX003CBoardingPassViaEmailX003EKBackingField(source.isBoardingPassViaEmail());
			destination.setX003CBoardingPassViaSmsX003EKBackingField(source.isBoardingPassViaSms());
			destination.setX003CComfortSeatFreeX003EKBackingField(source.isComfortSeatFree());

			ContactTypeEnum sourceContactType = source.getContactType();
			if (sourceContactType != null)
				destination.setX003CContactTypeX003EKBackingField(ContactType.fromValue(sourceContactType.value()));

			destination.setX003CCorporateX003EKBackingField(source.isCorporate());
			destination.setX003CEmail1X003EKBackingField(source.getEmail1());
			destination.setX003CEmail2X003EKBackingField(source.getEmail2());
			destination.setX003CEticketClassX003EKBackingField(source.getEticketClass());
			destination.setX003CEticketX003EKBackingField(source.getEticket());
			destination.setX003CFrequentFlyerCodeX003EKBackingField(source.getFrequentFlyerCode());
			destination.setX003CFrequentFlyerCustomerValueX003EKBackingField(source.getFrequentFlyerCustomerValue());
			destination.setX003CGenderX003EKBackingField(source.getGender());
			destination.setX003CIdX003EKBackingField(source.getId());
			destination.setX003CIsSpecialFareX003EKBackingField(source.isSpecialFare());
			destination.setX003CLastNameX003EKBackingField(source.getLastName());

			MmbLinkTypeEnum sourceLinkType = source.getLinkType();
			if (sourceLinkType != null)
				destination.setX003CLinkTypeX003EKBackingField(LinkType.fromValue(sourceLinkType.value()));

			destination.setX003CNameX003EKBackingField(source.getName());

			MmbPassengerNoteEnum sourceNote = source.getNote();
			if (sourceNote != null)
				destination.setX003CNoteX003EKBackingField(PassengerNote.fromValue(sourceNote.value()));

			destination.setX003CPaymentRetrieveEmailX003EKBackingField(source.getPaymentRetrieveEmail());
			destination.setX003CPaymentViaEmailX003EKBackingField(source.isPaymentViaEmail());
			destination.setX003CPnrX003EKBackingField(source.getPnr());
			destination.setX003CRouteIdX003EKBackingField(source.getRouteId());

			MmbCompartimentalClassEnum sourceSeatClass = source.getSeatClass();
			if (sourceSeatClass != null)
				destination.setX003CSeatClassX003EKBackingField(
						CompartimentalClass.fromValue(sourceSeatClass.value()));

			destination.setX003CSeatX003EKBackingField(source.getSeat());
			destination.setX003CSelectedX003EKBackingField(source.isSelected());
			destination.setX003CStatusNoteX003EKBackingField(source.getStatusNote());

			MmbPassengerStatusEnum sourceStatus = source.getStatus();
			if (sourceStatus != null)
				destination.setX003CStatusX003EKBackingField(
						PassengerStatus.fromValue(sourceStatus.value()));

			destination.setX003CTelephone1X003EKBackingField(source.getTelephone1());
			destination.setX003CTelephone2X003EKBackingField(source.getTelephone2());
			destination.setX003CTravelerRefNumberX003EKBackingField(source.getTravelerRefNumber());

			PassengerTypeEnum sourceType = source.getType();
			if (sourceType != null)
				destination.setX003CTypeX003EKBackingField(PassengerType.fromValue(sourceType.value()));

			destination.setX003CUpdateFlightRefNumberRPHX003EKBackingField(source.getUpdateFlightRefNumberRPH());

			MmbUpdateSSREnum sourceUpdate = source.getUpdate();
			if (sourceUpdate != null)
				destination.setX003CUpdateX003EKBackingField(UpdateSSR.fromValue(sourceUpdate.value()));

			destination.setX003CApisDataToUpdateX003EKBackingField(
					mmbApisInfoDataConverter.convert(source.getApisDataToUpdate()));

			destination.setX003CApisDataX003EKBackingField(
					mmbApisInfoDataConverter.convert(source.getApisData()));

			List<MmbCouponData> sourceCouponsList = source.getCoupons();
			if (sourceCouponsList != null) {
				ArrayOfCoupon couponsList = objectFactory.createArrayOfCoupon();
				for (MmbCouponData couponData : sourceCouponsList) {
					couponsList.getCoupon().add(mmbCouponDataConverter.convert(couponData));
				}
				destination.setX003CCouponsX003EKBackingField(couponsList);
			}

			List<MmbFlightData> sourceFlightsList = source.getFlights();
			if (sourceFlightsList != null) {
				ArrayOfFlight flightsList = objectFactory.createArrayOfFlight();
				for (MmbFlightData flightData : sourceFlightsList) {
					flightsList.getFlight().add(mmbFlightDataConverter.convert(flightData));
				}
				destination.setX003CFlightsX003EKBackingField(flightsList);
			}

			destination.setX003CFrequentFlyerCarrierX003EKBackingField(
					mmbFrequentFlyerCarrierDataConverter.convert(source.getFrequentFlyerCarrier()));

			destination.setX003CFrequentFlyerTypeX003EKBackingField(
					mmbFrequentFlyerTypeDataConverter.convert(source.getFrequentFlyerType()));

			destination.setX003COrderX003EKBackingField(
					mmbBaggageOrderDataConverter.convert(source.getBaggageOrder()));

			destination.setX003CPreferencesX003EKBackingField(
					mmbPreferencesDataConverter.convert(source.getPreferences()));

			destination.setX003CTicketsX003EKBackingField(null);

}
		return destination;
	}

}
