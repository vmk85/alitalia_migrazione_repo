package com.alitalia.aem.service.impl.converter.home.checkinservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.WebCheckinUndoCheckinResponse;
import com.alitalia.aem.ws.checkin.checkinservice.xsd1.UndoCheckInResponse;
import com.alitalia.aem.ws.checkin.checkinservice.xsd4.ArrayOfstring;



@Component(immediate=true, metatype=false)
@Service(value=UndoCheckinResponseToWebCheckinUndoCheckinResponse.class)
public class UndoCheckinResponseToWebCheckinUndoCheckinResponse implements
		Converter<UndoCheckInResponse, WebCheckinUndoCheckinResponse> {

	@Override
	public WebCheckinUndoCheckinResponse convert(UndoCheckInResponse source) {
		WebCheckinUndoCheckinResponse destination = null;

		if (source != null) {
			destination = new WebCheckinUndoCheckinResponse();			
			
			List<String> messagesList = new ArrayList<String>();
			JAXBElement<ArrayOfstring> messages = source.getMessages();
			if (messages != null && messages.getValue() != null && messages.getValue().getString()!=null
					&& messages.getValue().getString().size()>0){
				List<String> mess = messages.getValue().getString();
				for (String s : mess){
					messagesList.add(s);
				}
			}
			destination.setMessages(messagesList);
		}
		return destination;
	}
}