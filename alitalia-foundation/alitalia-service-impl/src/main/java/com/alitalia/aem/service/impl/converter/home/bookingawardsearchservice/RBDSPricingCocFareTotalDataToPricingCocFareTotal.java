package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingCocFareTotalData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetailsSolutionPricingCocFareTotal;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingCocFareTotalDataToPricingCocFareTotal.class)
public class RBDSPricingCocFareTotalDataToPricingCocFareTotal implements
		Converter<ResultBookingDetailsSolutionPricingCocFareTotalData, ResultBookingDetailsSolutionPricingCocFareTotal> {

	@Override
	public ResultBookingDetailsSolutionPricingCocFareTotal convert(
			ResultBookingDetailsSolutionPricingCocFareTotalData source) {
		ResultBookingDetailsSolutionPricingCocFareTotal destination = null;
		
		if(source != null){
			ObjectFactory objfact = new ObjectFactory();
			destination = objfact.createResultBookingDetailsSolutionPricingCocFareTotal();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}

}
