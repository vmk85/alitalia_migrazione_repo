package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.ChangeSeatCheckinRequest;
import com.alitalia.aem.common.messages.home.ChangeSeatCheckinResponse;
import com.alitalia.aem.common.messages.home.GetSeatDisponibilityCheckinRequest;
import com.alitalia.aem.common.messages.home.GetSeatDisponibilityCheckinResponse;
import com.alitalia.aem.common.messages.home.GetSeatMapCheckinRequest;
import com.alitalia.aem.common.messages.home.GetSeatMapCheckinResponse;
import com.alitalia.aem.service.api.home.WebCheckinSeatMapService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.checkinseatmapservice.ChangeSeatCheckinRequestToChangeSeatRequest;
import com.alitalia.aem.service.impl.converter.home.checkinseatmapservice.ChangeSeatResponseToChangeSeatCheckinResponse;
import com.alitalia.aem.service.impl.converter.home.checkinseatmapservice.GetSeatDisponibilityCheckinRequestToGetSeatDisponibilityRequest;
import com.alitalia.aem.service.impl.converter.home.checkinseatmapservice.GetSeatDisponibilityResponseToGetSeatDisponibilityCheckinResponse;
import com.alitalia.aem.service.impl.converter.home.checkinseatmapservice.GetSeatMapCheckinRequestToGetSeatMapRequest;
import com.alitalia.aem.service.impl.converter.home.checkinseatmapservice.GetSeatMapResponseToGetSeatMapCheckinResponse;
import com.alitalia.aem.ws.checkin.seatmapservice.CheckinSeatMapServiceClient;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd1.ChangeSeatRequest;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd1.ChangeSeatResponse;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd1.GetSeatDisponibilityRequest;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd1.GetSeatDisponibilityResponse;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd1.GetSeatMapRequest;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd1.GetSeatMapResponse;

@Component(immediate=true, metatype=false)
@Service
public class SimpleWebCheckinSeatMapService implements WebCheckinSeatMapService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleWebCheckinSeatMapService.class);

	@Reference
	private CheckinSeatMapServiceClient checkinSeatMapServiceClient;

	@Reference
	private GetSeatMapCheckinRequestToGetSeatMapRequest  getSeatMapCheckinRequestConverter;

	@Reference
	private GetSeatMapResponseToGetSeatMapCheckinResponse getSeatMapResponseConverter;
	
	@Reference
	private GetSeatDisponibilityCheckinRequestToGetSeatDisponibilityRequest  getSeatDisponibilityCheckinRequestConverter;

	@Reference
	private GetSeatDisponibilityResponseToGetSeatDisponibilityCheckinResponse getSeatDisponibilityResponseConverter;
	
	@Reference
	private ChangeSeatCheckinRequestToChangeSeatRequest changeSeatCheckinRequestConverter;

	@Reference
	private ChangeSeatResponseToChangeSeatCheckinResponse changeSeatResponseConverter;
	
	@Override
	public GetSeatMapCheckinResponse getSeatMap(GetSeatMapCheckinRequest request) {

		logger.debug("Executing method getSeatMap(). The request is {}", request);	

		try {
			GetSeatMapCheckinResponse response = new GetSeatMapCheckinResponse();
			GetSeatMapRequest serviceRequest = getSeatMapCheckinRequestConverter.convert(request);
			GetSeatMapResponse serviceResponse = checkinSeatMapServiceClient.getSeatMap(serviceRequest);
			response = getSeatMapResponseConverter.convert(serviceResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			logger.debug("Executed method successfully getSeatMap(). The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getSeatMap().", e);
			throw new AlitaliaServiceException("Exception executing getSeatMap: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public GetSeatDisponibilityCheckinResponse getSeatDisponibility(GetSeatDisponibilityCheckinRequest request) {

		logger.debug("Executing method getSeatDisponibility(). The request is {}", request);	

		try {
			GetSeatDisponibilityCheckinResponse response = new GetSeatDisponibilityCheckinResponse();
			GetSeatDisponibilityRequest serviceRequest = getSeatDisponibilityCheckinRequestConverter.convert(request);
			GetSeatDisponibilityResponse serviceResponse = checkinSeatMapServiceClient.getSeatDisponibility(serviceRequest);
			response = getSeatDisponibilityResponseConverter.convert(serviceResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			logger.debug("Executed method successfully getSeatDisponibility(). The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getSeatDisponibility().", e);
			throw new AlitaliaServiceException("Exception executing getSeatDisponibility: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public ChangeSeatCheckinResponse changeSeat(ChangeSeatCheckinRequest request) {

		logger.debug("Executing method changeSeat(). The request is {}", request);	

		try {
			ChangeSeatCheckinResponse response = new ChangeSeatCheckinResponse();
			ChangeSeatRequest serviceRequest = changeSeatCheckinRequestConverter.convert(request);
			ChangeSeatResponse serviceResponse = checkinSeatMapServiceClient.changeSeat(serviceRequest);
			response = changeSeatResponseConverter.convert(serviceResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			logger.debug("Executed method successfully changeSeat(). The response is {}", response);

			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method changeSeat().", e);
			throw new AlitaliaServiceException("Exception executing changeSeat: Sid ["+request.getSid()+"]" , e);
		}
	}
}
