package com.alitalia.aem.service.impl.converter.home.mmbsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrFrequentFlyerResponse;
import com.alitalia.aem.ws.mmb.searchservice.xsd1.FrequentFlyResponse;

@Component(immediate=true, metatype=false)
@Service(value=FrequentFlyResponseToUpdateMmbPnrFrequentFlyerResponse.class)
public class FrequentFlyResponseToUpdateMmbPnrFrequentFlyerResponse implements Converter<FrequentFlyResponse, UpdateMmbPnrFrequentFlyerResponse> {

	@Override
	public UpdateMmbPnrFrequentFlyerResponse convert(FrequentFlyResponse source) {
		UpdateMmbPnrFrequentFlyerResponse destination = null;

		if (source != null) {
			destination = new UpdateMmbPnrFrequentFlyerResponse();

			destination.setSuccessful(source.isIsUpdated());
		}

		return destination;
	}

}
