package com.alitalia.aem.service.impl.converter.home.checkinpassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.MmbFrequentFlyerCarrierData;
import com.alitalia.aem.ws.checkin.passengerservice.xsd2.FrequentFlyerCarrier;

@Component(immediate=true, metatype=false)
@Service(value=FrequentFlyerCarrierToMmbFrequentFlyerCarrierData.class)
public class FrequentFlyerCarrierToMmbFrequentFlyerCarrierData implements Converter<FrequentFlyerCarrier, MmbFrequentFlyerCarrierData> {

	@Override
	public MmbFrequentFlyerCarrierData convert(FrequentFlyerCarrier source) {
		MmbFrequentFlyerCarrierData destination = null;

		if (source != null) {
			destination = new MmbFrequentFlyerCarrierData();

			destination.setCode(source.getX003CCodeX003EKBackingField());
			destination.setDescription(source.getX003CDescriptionX003EKBackingField());
			destination.setId(source.getX003CIdX003EKBackingField());
			destination.setLenght(source.getX003CLenghtX003EKBackingField());
			destination.setPreFillChar(source.getX003CPreFillCharX003EKBackingField());
			destination.setRegularExpressionValidation(source.getX003CRegularExpressionValidationX003EKBackingField());
		}

		return destination;
	}

}
