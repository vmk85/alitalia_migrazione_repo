package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentLegOperationalDisclosureData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionSliceSegmentLegOperationalDisclosure;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentLegOperationalDisclosureToRTDSSliceSegmentLegOperationalDisclosureData.class)
public class RTDSSliceSegmentLegOperationalDisclosureToRTDSSliceSegmentLegOperationalDisclosureData implements
		Converter<ResultTicketingDetailSolutionSliceSegmentLegOperationalDisclosure, ResultTicketingDetailSolutionSliceSegmentLegOperationalDisclosureData> {

	@Override
	public ResultTicketingDetailSolutionSliceSegmentLegOperationalDisclosureData convert(
			ResultTicketingDetailSolutionSliceSegmentLegOperationalDisclosure source) {
		ResultTicketingDetailSolutionSliceSegmentLegOperationalDisclosureData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionSliceSegmentLegOperationalDisclosureData();

			destination.setCarrierField(source.getCarrierField());
			destination.setDisclosureTextField(source.getDisclosureTextField());
		}

		return destination;
	}

}
