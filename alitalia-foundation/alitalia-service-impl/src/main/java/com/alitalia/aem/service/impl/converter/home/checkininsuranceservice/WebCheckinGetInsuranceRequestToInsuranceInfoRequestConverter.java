package com.alitalia.aem.service.impl.converter.home.checkininsuranceservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.WebCheckinGetInsuranceRequest;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd1.InsuranceInfoRequest;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd2.LegType;




@Component(immediate=true, metatype=false)
@Service(value=WebCheckinGetInsuranceRequestToInsuranceInfoRequestConverter.class)
public class WebCheckinGetInsuranceRequestToInsuranceInfoRequestConverter implements Converter<WebCheckinGetInsuranceRequest, InsuranceInfoRequest> {
	
	@Override
	public InsuranceInfoRequest convert(WebCheckinGetInsuranceRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();		
		InsuranceInfoRequest destination = objectFactory.createInsuranceInfoRequest();
		
		if (source.getArea()!=null)
			destination.setArea(LegType.fromValue(source.getArea().value()));
		destination.setArrivalAirport(objectFactory.createInsuranceInfoRequestArrivalAirport(source.getArrivalAirport()));
		if (source.getArrivalDate() != null)
			destination.setArrivalDate(XsdConvertUtils.toXMLGregorianCalendar(source.getArrivalDate()));
		destination.setCountryCode(objectFactory.createInsuranceInfoRequestCountryCode(source.getCountryCode()));
		destination.setDepartureAirport(objectFactory.createInsuranceInfoRequestDepartureAirport(source.getDepartureAirport()));
		if (source.getDepartureDate() != null)
			destination.setDepartureDate(XsdConvertUtils.toXMLGregorianCalendar(source.getDepartureDate()));
		destination.setHasReturn(source.getHasReturn());
		destination.setLanguageCode(objectFactory.createInsuranceInfoRequestLanguageCode(source.getLanguageCode()));
		destination.setPassengersCount(source.getPassengersCount());
		destination.setPnr(objectFactory.createInsuranceInfoRequestPnr(source.getPnr()));
		destination.setFlgUsaCa(source.getFlgUsaCa());

		return destination;
	}
}