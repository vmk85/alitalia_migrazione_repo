package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingSalePriceData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingSalePrice;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingSalePriceDataToRTDSPricingSalePrice.class)
public class RTDSPricingSalePriceDataToRTDSPricingSalePrice implements
		Converter<ResultTicketingDetailSolutionPricingSalePriceData, 
					ResultTicketingDetailSolutionPricingSalePrice> {

	@Override
	public ResultTicketingDetailSolutionPricingSalePrice convert(
			ResultTicketingDetailSolutionPricingSalePriceData source) {
		ResultTicketingDetailSolutionPricingSalePrice destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingSalePrice();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
