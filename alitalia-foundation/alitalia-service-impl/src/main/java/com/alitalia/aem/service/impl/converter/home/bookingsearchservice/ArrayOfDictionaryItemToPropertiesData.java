package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PropertiesData;
import com.alitalia.aem.ws.booking.searchservice.xsd14.ResultBookingDetails;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ArrayOfDictionaryItem;
import com.alitalia.aem.ws.booking.searchservice.xsd3.DictionaryItem;

@Component(immediate=true, metatype=false)
@Service(value=ArrayOfDictionaryItemToPropertiesData.class)
public class ArrayOfDictionaryItemToPropertiesData implements Converter<ArrayOfDictionaryItem, PropertiesData> {

	private static final Logger logger = LoggerFactory.getLogger(ArrayOfDictionaryItemToPropertiesData.class);
	private XPath xpathXsd13RBD = null;
	private XPathExpression xpathExprXsd13RBD = null;
	private XPath xpathXsd12RBD = null;
	private XPathExpression xpathExprXsd12RBD = null;

	@Reference
	private ResultBookingDetailsToResultBookingDetailsData resultBookingDetailsConverter;

	@Reference
	private BookingSearchServiceJAXBContextFactory jaxbContextFactory;
	
	@Override
	public PropertiesData convert(ArrayOfDictionaryItem source) {

		PropertiesData destination = null;
		
		if (source != null) {
			
			destination = new PropertiesData();
			
			for (DictionaryItem item : source.getDictionaryItem()) {
				if (item.getKey() != null && item.getValue() != null) {
					if(item.getValue().getValue() instanceof ResultBookingDetails){
						destination.put(
								item.getKey().getValue(),
								resultBookingDetailsConverter.convert((ResultBookingDetails) item.getValue().getValue()));
					} else if(item.getValue().getValue() instanceof com.alitalia.aem.ws.booking.searchservice.xsd12.ResultBookingDetails){
						destination.put(
								item.getKey().getValue(),
								resultBookingDetailsConverter.convert((com.alitalia.aem.ws.booking.searchservice.xsd12.ResultBookingDetails) item.getValue().getValue()));
					} else {
						String xsd12ResultBookingDetailsInstanceType = "";
						String xsd13ResultBookingDetailsInstanceType = "";
						try {
							if (xpathXsd12RBD == null)
								xpathXsd12RBD = XPathFactory.newInstance().newXPath();

							if (xpathExprXsd12RBD == null)
								xpathExprXsd12RBD = xpathXsd12RBD.compile("/*[local-name()='Value'][namespace::*[.=\"http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.Booking.Model.BoomBox.BookingSearch\"] and contains(@*[local-name()='type'],\":resultBookingDetails\")]/@*[local-name()='type']");

							xsd12ResultBookingDetailsInstanceType = (String) xpathExprXsd12RBD.evaluate((Node) item.getValue().getValue(), XPathConstants.STRING);
						} catch (XPathExpressionException e) {
							logger.error("Error extracting type attribute value: {}", e);
							xsd12ResultBookingDetailsInstanceType = "";
						}
						try {
							if (xpathXsd13RBD == null)
								xpathXsd13RBD = XPathFactory.newInstance().newXPath();

							if (xpathExprXsd13RBD == null)
								xpathExprXsd13RBD = xpathXsd13RBD.compile("/*[local-name()='Value'][namespace::*[.=\"http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.Booking.Model.BoomBox.MetaSearch\"] and contains(@*[local-name()='type'],\":resultBookingDetails\")]/@*[local-name()='type']");

							xsd13ResultBookingDetailsInstanceType = (String) xpathExprXsd13RBD.evaluate((Node) item.getValue().getValue(), XPathConstants.STRING);
						} catch (XPathExpressionException e) {
							logger.error("Error extracting type attribute value: {}", e);
							xsd13ResultBookingDetailsInstanceType = "";
						}

						if (xsd12ResultBookingDetailsInstanceType != null
								&& !"".equals(xsd12ResultBookingDetailsInstanceType)) {
							try {
								Unmarshaller xsd12ResultBookingDetailsUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
								@SuppressWarnings("unchecked" )
								ResultBookingDetails castedSource = (ResultBookingDetails)
										((JAXBElement<ResultBookingDetails>) xsd12ResultBookingDetailsUnmarshaller.unmarshal((Node) item.getValue().getValue())).getValue();
								destination.put(
										item.getKey().getValue(),
										resultBookingDetailsConverter.convert(castedSource));
							} catch (JAXBException e) {
								logger.error("Error creating JAXBContext/Unmarshaller for a xsd12.ResultBookingDetailsInstanceType class: {}", e);
							}
						} else if (xsd13ResultBookingDetailsInstanceType != null
								&& !"".equals(xsd13ResultBookingDetailsInstanceType)) {
							try {
								Unmarshaller xsd12ResultBookingDetailsUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
								@SuppressWarnings("unchecked")
								com.alitalia.aem.ws.booking.searchservice.xsd12.ResultBookingDetails castedSource = 
										(com.alitalia.aem.ws.booking.searchservice.xsd12.ResultBookingDetails)
										((JAXBElement<com.alitalia.aem.ws.booking.searchservice.xsd12.ResultBookingDetails>) xsd12ResultBookingDetailsUnmarshaller.unmarshal((Node) item.getValue().getValue())).getValue();
								destination.put(
										item.getKey().getValue(),
										resultBookingDetailsConverter.convert(castedSource));
							} catch (JAXBException e) {
								logger.error("Error creating JAXBContext/Unmarshaller for a xsd13.ResultBookingDetailsInstanceType class: {}", e);
							}
						} else {
							destination.put(
								item.getKey().getValue(),
								item.getValue().getValue());
						}
					}
				}
			}
			
		}
		return destination;
	}

}
