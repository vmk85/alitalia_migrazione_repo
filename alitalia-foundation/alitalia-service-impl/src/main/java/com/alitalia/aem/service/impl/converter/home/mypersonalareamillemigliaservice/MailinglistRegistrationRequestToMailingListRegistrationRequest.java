package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.MailinglistRegistrationRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.MailingListRegistrationRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd5.MailingListActionTypes;

@Component(immediate = true, metatype = false)
@Service(value = MailinglistRegistrationRequestToMailingListRegistrationRequest.class)
public class MailinglistRegistrationRequestToMailingListRegistrationRequest implements Converter<MailinglistRegistrationRequest, MailingListRegistrationRequest> {

	@Reference
	private MMCustomerProfileDataToCustomerProfile mmCustomerProfileDataConverter;
	
	@Override
	public MailingListRegistrationRequest convert(MailinglistRegistrationRequest source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		
		ObjectFactory objectFactory = new ObjectFactory();
		
		MailingListRegistrationRequest destination = objectFactory.createMailingListRegistrationRequest();
		CustomerProfile customerProfile = mmCustomerProfileDataConverter.convert(source.getCustomerProfile());
		
		JAXBElement<CustomerProfile> jaxbCustomerProfile = objectFactory.createCustomerRequestCustomer(customerProfile);
		destination.setCustomer(jaxbCustomerProfile);
		destination.setAction(MailingListActionTypes.fromValue(source.getAction().value()));
		
		return destination;
	}
}