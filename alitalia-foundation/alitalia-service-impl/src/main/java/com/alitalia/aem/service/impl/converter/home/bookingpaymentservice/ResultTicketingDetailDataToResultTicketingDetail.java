package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ArrayOfresultTicketingDetailSolution;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetail;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=ResultTicketingDetailDataToResultTicketingDetail.class)
public class ResultTicketingDetailDataToResultTicketingDetail implements
		Converter<ResultTicketingDetailData, ResultTicketingDetail> {

	@Reference
	private ResultTicketingDetailPagesDataToResultTicketingDetailPages resultTicketingDetailPagesDataConverter;

	@Reference
	private ResultTicketingDetailSolutionDataToResultTicketingDetailSolution resultTicketingDetailSolutionDataConverter;

	@Override
	public ResultTicketingDetail convert(ResultTicketingDetailData source) {
		ResultTicketingDetail destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetail();

			destination.setPagesField(
					resultTicketingDetailPagesDataConverter.convert(source.getPagesField()));

			ArrayOfresultTicketingDetailSolution solutionField = null;
			if (source.getSolutionField() != null &&
					!source.getSolutionField().isEmpty()) {
				solutionField = objectFactory.createArrayOfresultTicketingDetailSolution();
				for(ResultTicketingDetailSolutionData sourceSolutionFieldElem : source.getSolutionField())
					solutionField.getResultTicketingDetailSolution().add(
							resultTicketingDetailSolutionDataConverter.convert(sourceSolutionFieldElem));
			}
			destination.setSolutionField(solutionField);
		}

		return destination;
	}

}
