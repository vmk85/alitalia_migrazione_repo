package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.SecureQuestionData;
import com.alitalia.aem.common.data.home.StateData;
import com.alitalia.aem.common.messages.home.RetrieveSecureQuestionListResponse;
import com.alitalia.aem.common.messages.home.RetrieveStateListResponse;
import com.alitalia.aem.ws.mypersonalarea.staticdataservice.xsd1.SecureQuestionStaticDataResponse;
import com.alitalia.aem.ws.mypersonalarea.staticdataservice.xsd1.StateStaticDataResponse;
import com.alitalia.aem.ws.mypersonalarea.staticdataservice.xsd2.ArrayOfSecureQuestion;
import com.alitalia.aem.ws.mypersonalarea.staticdataservice.xsd2.ArrayOfState;
import com.alitalia.aem.ws.mypersonalarea.staticdataservice.xsd2.SecureQuestion;
import com.alitalia.aem.ws.mypersonalarea.staticdataservice.xsd2.State;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import javax.xml.bind.JAXBElement;
import java.util.ArrayList;
import java.util.List;

@Component(immediate = true, metatype = false)
@Service(value = SecureQuestionStaticDataResponseToRetrieveSecureQuestionListResponseConverter.class)
public class SecureQuestionStaticDataResponseToRetrieveSecureQuestionListResponseConverter implements Converter<SecureQuestionStaticDataResponse, RetrieveSecureQuestionListResponse> {

	@Override
	public RetrieveSecureQuestionListResponse convert(SecureQuestionStaticDataResponse source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");

		RetrieveSecureQuestionListResponse destination = new RetrieveSecureQuestionListResponse();

		JAXBElement<ArrayOfSecureQuestion> jaxbArrayOfSecureQuestion = source.getEntities();
		ArrayOfSecureQuestion arrayOfState = jaxbArrayOfSecureQuestion.getValue();
		List<SecureQuestion> secureQuestions = arrayOfState.getSecureQuestion();
		
		List<SecureQuestionData> secureQuestionDataList = new ArrayList<>();
		for(SecureQuestion secureQuestion: secureQuestions){
			SecureQuestionData secureQuestionData = new SecureQuestionData();

			secureQuestionData.setSecureQuestionID(secureQuestion.getSecureQuestionID());
			secureQuestionData.setSecureQuestionCode(secureQuestion.getSecureQuestionCode().getValue());
			secureQuestionData.setSecureQuestionDescription(secureQuestion.getSecureQuestionDescription().getValue());
			
			secureQuestionDataList.add(secureQuestionData);
		}
		
		destination.setSecureQuestions(secureQuestionDataList);
		
		return destination;
	}


}