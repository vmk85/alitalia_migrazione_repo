package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ApplicantPassengerData;
import com.alitalia.aem.common.data.home.ContactData;
import com.alitalia.aem.common.data.home.TicketInfoData;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ARTaxInfoTypes;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ApplicantPassenger;
import com.alitalia.aem.ws.booking.searchservice.xsd3.ObjectFactory;
import com.alitalia.aem.ws.booking.searchservice.xsd3.PassengerType;
import com.alitalia.aem.ws.booking.searchservice.xsd5.ArrayOfanyType;

@Component(immediate=true, metatype=false)
@Service(value=ApplicantPassengerDataToApplicantPassenger.class)
public class ApplicantPassengerDataToApplicantPassenger 
		implements Converter<ApplicantPassengerData, ApplicantPassenger> {
	
	@Reference
	private PassengerInfoBaseDataToInfoBase infoBaseDataToInfoBaseConverter;
	
	@Reference
	private PreferencesDataToPreferences preferencesDataToPreferencesConverter;
	
	@Reference
	private FrequentFlyerTypeDataToFrequentFlyerType frequentFlyerTypeDataToFrequentFlyerTypeConverter;
	
	@Reference
	private TicketInfoDataToTicketInfo ticketInfoDataToTicketInfoConverter;
	
	@Reference
	private ContactDataToContact contactDataToContactConverter;
	
	@Override
	public ApplicantPassenger convert(ApplicantPassengerData source) {
		
		ApplicantPassenger destination = null;
		
		if (source != null) {
			
			ObjectFactory factory3 = new ObjectFactory();
			com.alitalia.aem.ws.booking.searchservice.xsd5.ObjectFactory factory5 = 
					new com.alitalia.aem.ws.booking.searchservice.xsd5.ObjectFactory();
			
			destination = factory3.createApplicantPassenger();
			
			// ApplicantPassenger data
			
			if (source.getArTaxInfoType() != null) {
				destination.setARTaxInfoType(ARTaxInfoTypes.fromValue(source.getArTaxInfoType().value()));
			}
			
			destination.setBlueBizCode(
					factory3.createApplicantPassengerBlueBizCode(
							source.getBlueBizCode()));
			
			List<ContactData> sourceContacts = source.getContact();
			ArrayOfanyType contacts = factory5.createArrayOfanyType();
			if (sourceContacts != null) {
				for (ContactData contact : sourceContacts) {
					contacts.getAnyType().add(
							contactDataToContactConverter.convert(contact));
				}
			}
			destination.setContact(factory3.createApplicantPassengerContact(contacts));

			destination.setCountry(factory3.createApplicantPassengerCountry(source.getCountry()));
			
			destination.setCouponPrice(source.getCouponPrice());
			
			destination.setCUIT(factory3.createApplicantPassengerCUIT(source.getCuit()));
			
			destination.setEmail(factory3.createApplicantPassengerEmail(source.getEmail()));
			
			destination.setExtraCharge(source.getExtraCharge());
			
			destination.setFee(source.getFee());
			destination.setCCFee(source.getCcFee());
			
			// AdultPassenger data
			
			destination.setFrequentFlyerCode(factory3.createAdultPassengerFrequentFlyerCode(source.getFrequentFlyerCode()));
			
			destination.setFrequentFlyerTier(factory3.createAdultPassengerFrequentFlyerTier(source.getFrequentFlyerTier()));
			
			destination.setFrequentFlyerType(factory3.createAdultPassengerFrequentFlyerType(
							frequentFlyerTypeDataToFrequentFlyerTypeConverter.convert(source.getFrequentFlyerType())));
			
			// ARegularPassenger data
			
			destination.setPreferences(factory3.createARegularPassengerPreferences(
							preferencesDataToPreferencesConverter.convert(source.getPreferences())));
			
			// APassengerBase data
			
			destination.setLastName(factory3.createAPassengerBaseLastName(source.getLastName()));
			
			destination.setName(factory3.createAPassengerBaseName(source.getName()));
			
			destination.setCouponPrice(source.getCouponPrice());
			
			destination.setExtraCharge(source.getExtraCharge());
			
			destination.setFee(source.getFee());
			
			destination.setCCFee(source.getCcFee());
			
			destination.setGrossFare(source.getGrossFare());
			
			destination.setNetFare(source.getNetFare());
			
			destination.setInfo(factory3.createAPassengerBaseInfo(
							infoBaseDataToInfoBaseConverter.convert(source.getInfo())));
			
			List<TicketInfoData> sourceTickets = source.getTickets();
			if (sourceTickets != null && !source.getTickets().isEmpty()) {
				ArrayOfanyType tickets = factory5.createArrayOfanyType();
				for (TicketInfoData ticketInfoData : sourceTickets) {
					tickets.getAnyType().add(ticketInfoDataToTicketInfoConverter.convert(ticketInfoData));
				}
				destination.setTickets(factory3.createAPassengerBaseTickets(tickets));
			}
			else
				destination.setTickets(factory3.createAPassengerBaseTickets(null));

			destination.setType(PassengerType.fromValue(source.getType().value()));
			destination.setSkyBonusCode(factory3.createApplicantPassengerSkyBonusCode(source.getSkyBonusCode()));
			destination.setSubscribeToNewsletter(factory3.createApplicantPassengerSubscribeToNewsletter(source.isSubscribeToNewsletter()));
		}
		
		return destination;
	}

}
