package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSaleFareTotalData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionSaleFareTotal;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSaleFareTotalToRTDSSaleFareTotalData.class)
public class RTDSSaleFareTotalToRTDSSaleFareTotalData implements
		Converter<ResultTicketingDetailSolutionSaleFareTotal, ResultTicketingDetailSolutionSaleFareTotalData> {

	@Override
	public ResultTicketingDetailSolutionSaleFareTotalData convert(ResultTicketingDetailSolutionSaleFareTotal source) {
		ResultTicketingDetailSolutionSaleFareTotalData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionSaleFareTotalData();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
