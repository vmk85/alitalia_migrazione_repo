package com.alitalia.aem.service.impl.converter.home.bookingwidget;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.FrequentFlyerTypeData;
import com.alitalia.aem.ws.booking.widgetservice.xsd6.ObjectFactory;
import com.alitalia.aem.ws.booking.widgetservice.xsd6.FrequentFlyerType;

@Component(immediate=true, metatype=false)
@Service(value=FrequentFlyerTypeDataToFrequentFlyerType.class)
public class FrequentFlyerTypeDataToFrequentFlyerType 
		implements Converter<FrequentFlyerTypeData, FrequentFlyerType> {
	
	@Override
	public FrequentFlyerType convert(FrequentFlyerTypeData source) {
		
		FrequentFlyerType destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			
			destination = objectFactory.createFrequentFlyerType();
			
			destination.setX003CCodeX003EKBackingField(source.getCode());
			
			destination.setX003CDescriptionX003EKBackingField(source.getDescription());
			
			destination.setX003CLenghtX003EKBackingField(source.getLenght());
			
			destination.setX003CPreFillCharX003EKBackingField(source.getPreFillChar());
			
			destination.setX003CRegularExpressionValidationX003EKBackingField(
					source.getRegularExpressionValidation());
			
		}
		
		return destination;
	}

}
