package com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice;
import org.apache.felix.scr.annotations.Service;
import org.apache.felix.scr.annotations.Component;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PhonePrefixData;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd3.PhonePrefix;

@Component(immediate=true, metatype=false)
@Service(value=PhonePrefixToPhonePrefixData.class)
public class PhonePrefixToPhonePrefixData implements Converter<PhonePrefix, PhonePrefixData> {

	@Override
	public PhonePrefixData convert(PhonePrefix source) {
		PhonePrefixData prefixData = new PhonePrefixData();
		prefixData.setCode(source.getCode().getValue());
		prefixData.setDescription(source.getDescription().getValue());
		prefixData.setPrefix(source.getPrefix().getValue());
		return prefixData;
	}

}
