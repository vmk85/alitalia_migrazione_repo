package com.alitalia.aem.service.impl.converter.home.commonservice;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.booking.commonservice.xsd2.BaggageAllowance;
import com.alitalia.aem.ws.booking.commonservice.xsd2.Brand;


@Component(immediate=true, metatype=false)
@Service(value=BookingCommonServiceJAXBContextFactory.class)
public class BookingCommonServiceJAXBContextFactory {

	private static final Logger logger = LoggerFactory.getLogger(BookingCommonServiceJAXBContextFactory.class);

	private JAXBContext jaxbContext = null;

	public synchronized JAXBContext getJaxbContext() throws JAXBException {

		if (jaxbContext == null) {
			jaxbContext = JAXBContext.newInstance(BaggageAllowance.class,
					Brand.class);
		}

		return jaxbContext;
	}

	@Activate
	private void activate() {
		try {
			jaxbContext = JAXBContext.newInstance(BaggageAllowance.class,
					Brand.class);
		} catch (JAXBException e) {
			logger.warn("BookingSearchServiceJAXBContextFactory - cannot instantiate JAXBContext: {}", e.getMessage());
		}
	}
}

