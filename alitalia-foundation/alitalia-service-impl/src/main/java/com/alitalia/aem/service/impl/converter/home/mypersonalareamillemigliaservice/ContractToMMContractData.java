package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMContractData;
import com.alitalia.aem.common.data.home.enumerations.MMContractStatusEnum;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Contract;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd3.ArrayOfanyType;

@Component(immediate = true, metatype = false)
@Service(value = ContractToMMContractData.class)
public class ContractToMMContractData implements Converter<ArrayOfanyType, List<MMContractData>> {

	private static Logger logger = LoggerFactory.getLogger(ContractToMMContractData.class);
	
	@Reference
	private MyPersonalareaMillemigliaServiceJAXBContextFactory jaxbContextFactory;
	
	@Override
	public List<MMContractData> convert(ArrayOfanyType source) {
		
		if (source == null) {
			return null;
		}
		
		List<MMContractData> mmContracts = new ArrayList<MMContractData>();

		for (Object object: source.getAnyType()) {
			try {
				Unmarshaller unmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
				
				JAXBElement<Contract> jaxbElement = (JAXBElement<Contract>) unmarshaller.unmarshal((Node) object);
				Contract contract = jaxbElement.getValue();
				
				MMContractData mmContractData = new MMContractData();
				mmContractData.setName(contract.getName().getValue());
				mmContractData.setStatus(MMContractStatusEnum.fromValue(contract.getStatus().value()));
				
				mmContracts.add(mmContractData);
			
			} catch (JAXBException e) {
				logger.error("Error while creating JAXBConext/Unmarshller for List<MMContractData>: {}", e);
			}
		}
		
		return mmContracts;
	}
}