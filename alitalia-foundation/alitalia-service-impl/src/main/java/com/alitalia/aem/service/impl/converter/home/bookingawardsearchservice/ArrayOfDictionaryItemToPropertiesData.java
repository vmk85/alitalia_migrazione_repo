package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PropertiesData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetail;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd12.ResultBookingDetails;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.ArrayOfDictionaryItem;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd3.DictionaryItem;

@Component(immediate=true, metatype=false)
@Service(value=ArrayOfDictionaryItemToPropertiesData.class)
public class ArrayOfDictionaryItemToPropertiesData implements Converter<ArrayOfDictionaryItem, PropertiesData> {

	private static final Logger logger = LoggerFactory.getLogger(ArrayOfDictionaryItemToPropertiesData.class);
	private XPath xpathRBD12 = null;
	private XPathExpression xpathExprRBD12 = null;
	private XPath xpathRBD13 = null;
	private XPathExpression xpathExprRBD13 = null;
	private XPath xpathTD = null;
	private XPathExpression xpathExprTD = null;

	@Reference
	private ResultBookingDetailsToResultBookingDetailsData resultBookingDetailsConverter;

	@Reference
	private ResultTicketingDetailToResultTicketingDetailData resultTicketingDetailConverter;

	@Reference
	private BookingAwardSearchServiceJAXBContextFactory jaxbContextFactory;

	@Override
	public PropertiesData convert(ArrayOfDictionaryItem source) {

		PropertiesData destination = null;
		
		if (source != null) {
			
			destination = new PropertiesData();
			
			for (DictionaryItem item : source.getDictionaryItem()) {
				if (item.getKey() != null && item.getValue() != null) {
					if(item.getValue().getValue() instanceof ResultBookingDetails){
						destination.put(
								item.getKey().getValue(),
								resultBookingDetailsConverter.convert((ResultBookingDetails) item.getValue().getValue()));
					} else if(item.getValue().getValue() instanceof com.alitalia.aem.ws.bookingaward.searchservice.xsd13.ResultBookingDetails){
						destination.put(
								item.getKey().getValue(),
								resultBookingDetailsConverter.convert((com.alitalia.aem.ws.bookingaward.searchservice.xsd13.ResultBookingDetails) item.getValue().getValue()));
					} else if(item.getValue().getValue() instanceof ResultTicketingDetail){
						destination.put(
								item.getKey().getValue(),
								resultTicketingDetailConverter.convert((ResultTicketingDetail) item.getValue().getValue()));
					} else {
						String xsd12ResultBookingDetailsInstanceType = "";
						String xsd13ResultBookingDetailsInstanceType = "";
						String resultTicketingDetailInstanceType = "";
						try {
							if (xpathRBD12 == null)
								xpathRBD12 = XPathFactory.newInstance().newXPath();

							if (xpathExprRBD12 == null)
								xpathExprRBD12 = xpathRBD12.compile("/*[local-name()='Value'][namespace::*[.=\"http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.Booking.Model.BoomBox.BookingSearch\"] and contains(@*[local-name()='type'],\":resultBookingDetails\")]/@*[local-name()='type']");

							xsd12ResultBookingDetailsInstanceType = (String) xpathExprRBD12.evaluate((Node) item.getValue().getValue(), XPathConstants.STRING);
						} catch (XPathExpressionException e) {
							logger.error("Error extracting type attribute value: {}", e);
							xsd12ResultBookingDetailsInstanceType = "";
						}
						try {
							if (xpathRBD13 == null)
								xpathRBD13 = XPathFactory.newInstance().newXPath();

							if (xpathExprRBD13 == null)
								xpathExprRBD13 = xpathRBD13.compile("/*[local-name()='Value'][namespace::*[.=\"http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.Booking.Model.BoomBox.MetaSearch\"] and contains(@*[local-name()='type'],\":resultBookingDetails\")]/@*[local-name()='type']");

							xsd13ResultBookingDetailsInstanceType = (String) xpathExprRBD13.evaluate((Node) item.getValue().getValue(), XPathConstants.STRING);
						} catch (XPathExpressionException e) {
							logger.error("Error extracting type attribute value: {}", e);
							xsd13ResultBookingDetailsInstanceType = "";
						}
						try {
							if (xpathTD == null)
								xpathTD = XPathFactory.newInstance().newXPath();

							if (xpathExprTD == null)
								xpathExprTD = xpathTD.compile("/*[local-name()='Value'][namespace::*[.=\"http://schemas.datacontract.org/2004/07/Alitalia.Portal.WebFrontend.LCWP.Booking.Model.BoomBox.AwardSearch\"] and contains(@*[local-name()='type'],\":resultTicketingDetails\")]/@*[local-name()='type']");

							resultTicketingDetailInstanceType = (String) xpathExprTD.evaluate((Node) item.getValue().getValue(), XPathConstants.STRING);
						} catch (XPathExpressionException e) {
							logger.error("Error extracting type attribute value: {}", e);
							resultTicketingDetailInstanceType = "";
						}

						if (xsd12ResultBookingDetailsInstanceType != null
								&& !"".equals(xsd12ResultBookingDetailsInstanceType)) {
							try {
								Unmarshaller xsd12ResultBookingDetailsUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
								@SuppressWarnings("unchecked" )
								ResultBookingDetails castedSource = (ResultBookingDetails)
										((JAXBElement<ResultBookingDetails>) xsd12ResultBookingDetailsUnmarshaller.unmarshal((Node) item.getValue().getValue())).getValue();
								destination.put(
										item.getKey().getValue(),
										resultBookingDetailsConverter.convert(castedSource));
							} catch (JAXBException e) {
								logger.error("Error creating JAXBContext/Unmarshaller for a xsd12.ResultBookingDetails class: {}", e);
							}
						} else if (xsd13ResultBookingDetailsInstanceType != null
								&& !"".equals(xsd13ResultBookingDetailsInstanceType)) {
							try {
								Unmarshaller xsd13ResultBookingDetailsUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
								@SuppressWarnings("unchecked")
								com.alitalia.aem.ws.bookingaward.searchservice.xsd13.ResultBookingDetails castedSource = 
										(com.alitalia.aem.ws.bookingaward.searchservice.xsd13.ResultBookingDetails)
										((JAXBElement<com.alitalia.aem.ws.bookingaward.searchservice.xsd13.ResultBookingDetails>) xsd13ResultBookingDetailsUnmarshaller.unmarshal((Node) item.getValue().getValue())).getValue();
								destination.put(
										item.getKey().getValue(),
										resultBookingDetailsConverter.convert(castedSource));
							} catch (JAXBException e) {
								logger.error("Error creating JAXBContext/Unmarshaller for a xsd13.ResultBookingDetails class: {}", e);
							}
						} else if (resultTicketingDetailInstanceType != null
								&& !"".equals(resultTicketingDetailInstanceType)) {
							try {
								Unmarshaller resultTicketingDetailUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
								@SuppressWarnings("unchecked")
								ResultTicketingDetail castedSource = 
										(ResultTicketingDetail) ((JAXBElement<ResultTicketingDetail>) resultTicketingDetailUnmarshaller.unmarshal((Node) item.getValue().getValue())).getValue();
								destination.put(
										item.getKey().getValue(),
										resultTicketingDetailConverter.convert(castedSource));
							} catch (JAXBException e) {
								logger.error("Error creating JAXBContext/Unmarshaller for a ResultTicketingDetail class: {}", e);
							}
						} else {
							destination.put(
								item.getKey().getValue(),
								item.getValue().getValue());
						}
					}
				}
			}
			
		}
		return destination;
	}

}
