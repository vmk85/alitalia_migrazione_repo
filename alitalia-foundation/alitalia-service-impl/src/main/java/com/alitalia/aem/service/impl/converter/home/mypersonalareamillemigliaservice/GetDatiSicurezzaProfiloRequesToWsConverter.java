package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;
import com.alitalia.aem.common.messages.home.GetDatiSicurezzaProfiloRequestLocal;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.GetDatiSicurezzaProfiloRequest;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

@Component(immediate = true, metatype = false)
@Service(value = GetDatiSicurezzaProfiloRequesToWsConverter.class)
public class GetDatiSicurezzaProfiloRequesToWsConverter implements Converter<GetDatiSicurezzaProfiloRequestLocal, GetDatiSicurezzaProfiloRequest> {

	@Reference
	private MMCustomerProfileDataToCustomerProfile mmCustomerProfileDataConverter;

	@Override
	public GetDatiSicurezzaProfiloRequest convert(GetDatiSicurezzaProfiloRequestLocal source) {
		GetDatiSicurezzaProfiloRequest destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = new GetDatiSicurezzaProfiloRequest();

			destination.setIDProfilo(
					objectFactory.createGetDatiSicurezzaProfiloRequestIDProfilo(
							source.getIdProfilo()));
		}

		return destination;
	}
}