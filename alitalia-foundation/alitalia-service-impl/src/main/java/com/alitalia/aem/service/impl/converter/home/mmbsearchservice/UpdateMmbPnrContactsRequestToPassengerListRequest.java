package com.alitalia.aem.service.impl.converter.home.mmbsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.enumerations.MmbUpdateSSREnum;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrContactsRequest;
import com.alitalia.aem.common.messages.home.enumeration.MmbUpdateReasonType;
import com.alitalia.aem.ws.mmb.searchservice.xsd1.PassengerListRequest;
import com.alitalia.aem.ws.mmb.searchservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mmb.searchservice.xsd1.SearchReason;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.UpdateSSR;

@Component(immediate=true, metatype=false)
@Service(value=UpdateMmbPnrContactsRequestToPassengerListRequest.class)
public class UpdateMmbPnrContactsRequestToPassengerListRequest implements
		Converter<UpdateMmbPnrContactsRequest, PassengerListRequest> {

	@Reference
	private MmbRouteDataToRoute mmbRouteDataConverter;

	@Reference
	private MmbRequestBaseInfoDataToRequestBaseInfo mmbRequestBaseInfoDataConverter;

	@Override
	public PassengerListRequest convert(UpdateMmbPnrContactsRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		PassengerListRequest destination = null;

		if (source != null) {
			destination = objectFactory.createPassengerListRequest();

			destination.setLastName(objectFactory.createPassengerListRequestLastName(source.getLastName()));
			destination.setMail(objectFactory.createPassengerListRequestMail(source.getMail()));
			destination.setName(objectFactory.createPassengerListRequestName(source.getName()));
			destination.setOldMail(objectFactory.createPassengerListRequestOldMail(source.getOldMail()));
			destination.setOldTelephone(objectFactory.createPassengerListRequestOldTelephone(source.getOldTelephone()));
			destination.setOldTelephone2(objectFactory.createPassengerListRequestOldTelephone2(source.getOldTelephone2()));
			destination.setPnr(objectFactory.createPassengerListRequestPnr(source.getPnr()));

			MmbUpdateReasonType sourceReason = source.getReason();
			if (sourceReason != null)
				destination.setReason(SearchReason.fromValue(sourceReason.value()));

			destination.setSelectedRoute(objectFactory.createPassengerListRequestSelectedRoute(
					mmbRouteDataConverter.convert(source.getSelectedRoute())));
			destination.setTelephone(objectFactory.createPassengerListRequestTelephone(source.getTelephone()));
			destination.setTelephone2(objectFactory.createPassengerListRequestTelephone2(source.getTelephone2()));

			MmbUpdateSSREnum updateMailAction = source.getUpdateMailAction();
			if (updateMailAction != null)
				destination.setUpdateMail(UpdateSSR.fromValue(updateMailAction.value()));

			MmbUpdateSSREnum updateTelephoneAction = source.getUpdateTelephoneAction();
			if (updateTelephoneAction != null)
				destination.setUpdateTelephone(UpdateSSR.fromValue(updateTelephoneAction.value()));

			MmbUpdateSSREnum updateTelephone2Action = source.getUpdateTelephone2Action();
			if (updateTelephone2Action != null)
				destination.setUpdateTelephone2(UpdateSSR.fromValue(updateTelephone2Action.value()));

			destination.setX003CPNRX003EKBackingField(source.getPnr());
			destination.setX003CInfoRequestX003EKBackingField(mmbRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
		}

		return destination;
	}

}
