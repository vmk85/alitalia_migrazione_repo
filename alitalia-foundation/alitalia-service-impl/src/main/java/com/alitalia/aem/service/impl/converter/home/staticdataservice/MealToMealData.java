package com.alitalia.aem.service.impl.converter.home.staticdataservice;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.ws.booking.staticdataservice.xsd5.Meal;
import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MealData;

@Component(immediate=true, metatype=false)
@Service(value=MealToMealData.class)
public class MealToMealData implements Converter<Meal, MealData> {

	@Override
	public MealData convert(Meal source) {
		MealData mealData = new MealData();
		mealData.setCode(source.getCode().getValue());
		mealData.setDescription(source.getDescription().getValue());
		return mealData;
	}

}
