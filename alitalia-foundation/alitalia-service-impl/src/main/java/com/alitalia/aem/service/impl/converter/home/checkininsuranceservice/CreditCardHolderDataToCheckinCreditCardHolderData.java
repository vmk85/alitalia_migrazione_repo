package com.alitalia.aem.service.impl.converter.home.checkininsuranceservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinCreditCardHolderData;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd2.CreditCardHolderData;



@Component(immediate=true, metatype=false)
@Service(value=CreditCardHolderDataToCheckinCreditCardHolderData.class)
public class CreditCardHolderDataToCheckinCreditCardHolderData implements Converter<CreditCardHolderData, CheckinCreditCardHolderData> {


	@Override
	public CheckinCreditCardHolderData convert(CreditCardHolderData source) {
		CheckinCreditCardHolderData destination = null;

		if (source != null) {
			destination = new CheckinCreditCardHolderData();
			destination.setId(source.getX003CIdX003EKBackingField());
			destination.setAddress(source.getX003CAddressX003EKBackingField());
			destination.setCity(source.getX003CCityX003EKBackingField());
			destination.setCountry(source.getX003CCountryX003EKBackingField());
			destination.setFirstName(source.getX003CFirstNameX003EKBackingField());
			destination.setIsActualBuyer(source.isX003CIsActualBuyerX003EKBackingField());
			destination.setLastName(source.getX003CLastNameX003EKBackingField());
			destination.setName(source.getX003CNameX003EKBackingField());
			destination.setState(source.getX003CNameX003EKBackingField());
			destination.setZip(source.getX003CZipX003EKBackingField());
		}

		return destination;
	}

}
