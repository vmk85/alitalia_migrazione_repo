package com.alitalia.aem.service.impl.converter.home.mmbinsuranceservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.messages.home.MmbInsuranceInfoRequest;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd1.InsuranceInfoRequest;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd2.AreaValue;
import com.alitalia.aem.ws.mmb.insuranceservice.xsd2.RouteType;

@Component(immediate=true, metatype=false)
@Service(value=MmbInsuranceInfoRequestToInsuranceInfoRequest.class)
public class MmbInsuranceInfoRequestToInsuranceInfoRequest implements
		Converter<MmbInsuranceInfoRequest, InsuranceInfoRequest> {

	@Override
	public InsuranceInfoRequest convert(MmbInsuranceInfoRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		InsuranceInfoRequest destination = null;

		if (source != null) {
			destination = objectFactory.createInsuranceInfoRequest();

			AreaValueEnum sourceArea = source.getArea();
			if (sourceArea != null)
				destination.setArea(AreaValue.fromValue(sourceArea.value()));

			destination.setArrivalAirport(objectFactory.createInsuranceInfoRequestArrivalAirport(source.getArrivalAirport()));
			destination.setArrivalDate(XsdConvertUtils.toXMLGregorianCalendar(source.getArrivalDate()));
			destination.setCountryCode(objectFactory.createInsuranceInfoRequestCountryCode(source.getCountryCode()));
			destination.setDepartureAirport(objectFactory.createInsuranceInfoRequestDepartureAirport(source.getDepartureAirport()));
			destination.setDepartureDate(XsdConvertUtils.toXMLGregorianCalendar(source.getDepartureDate()));
			destination.setFlgUsaCa(source.isFlgUsaCa());
			destination.setLanguageCode(objectFactory.createInsuranceInfoRequestLanguageCode(source.getLanguageCode()));
			destination.setPassengersCount(source.getPassengersCount());
			destination.setPnr(objectFactory.createInsuranceInfoRequestPnr(source.getPnr()));

			RouteTypeEnum sourceRouteType = source.getRouteType();
			if (sourceRouteType != null)
				destination.setRouteType(RouteType.fromValue(sourceRouteType.value()));
		}

		return destination;
	}

}
