package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.GetDatiSicurezzaProfiloOLDRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

@Component(immediate = true, metatype = false)
@Service(value = GetDatiSicurezzaProfiloOLDRequestConverter.class)
public class GetDatiSicurezzaProfiloOLDRequestConverter implements Converter< com.alitalia.aem.common.messages.home.GetDatiSicurezzaProfiloOLDRequest,com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.GetDatiSicurezzaProfiloOLDRequest > {

    @Override
    public  GetDatiSicurezzaProfiloOLDRequest convert(com.alitalia.aem.common.messages.home.GetDatiSicurezzaProfiloOLDRequest source) {
        if(source == null) throw new IllegalArgumentException("Request is null.");
        ObjectFactory objectFactory=new ObjectFactory();

        com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.GetDatiSicurezzaProfiloOLDRequest destination = new com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.GetDatiSicurezzaProfiloOLDRequest();

        if (source != null)
            destination.setIDProfilo(objectFactory.createCheckCredenzialiOLDResponseIDProfilo(source.getIdProfilo()));

        return destination;
    }
}
