package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.MilleMigliaLoginResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.LoginResponse;

@Component(immediate = true, metatype = false)
@Service(value = LoginResponseToMilleMigliaLoginResponse.class)
public class LoginResponseToMilleMigliaLoginResponse 
		implements Converter<LoginResponse, MilleMigliaLoginResponse> {

	@Reference
	private CustomerProfileToMMCustomerProfileData customerProfileConverter;
	
	@Override
	public MilleMigliaLoginResponse convert(LoginResponse source) {
		MilleMigliaLoginResponse destination = null;

		if (source != null) {
			
			destination = new MilleMigliaLoginResponse();
			
			if (source.getCustomer() != null) {
				destination.setCustomerProfileData(
						customerProfileConverter.convert(
								source.getCustomer().getValue()));	
			}
			if (source.getSSOToken() != null) {
				destination.setSsoToken(source.getSSOToken().getValue());
			}
		}

		return destination;
	}

}
