package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.home.ProfileResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.GetProfileResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;

@Component(immediate = true, metatype = false)
@Service(value = GetProfileResponseToProfileResponse.class)
public class GetProfileResponseToProfileResponse implements Converter<GetProfileResponse, ProfileResponse> {

	@Reference
	private CustomerProfileToMMCustomerProfileData customerProfileToCustomerProfileDataConverter;
	
	@Override
	public ProfileResponse convert(GetProfileResponse source) {
		ProfileResponse response = new ProfileResponse();

		CustomerProfile customerProfile = source.getCustomer().getValue();
		MMCustomerProfileData mmCustomerProfileData = customerProfileToCustomerProfileDataConverter.convert(customerProfile);
		response.setCustomerProfile(mmCustomerProfileData);
		
		return response;
	}
}	