package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryEticketData;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryPassengerData;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ETicket;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.Passenger;

@Component(immediate=true, metatype=false)
@Service(value=PassengerToCheckinAncillaryPassengerData.class)
public class PassengerToCheckinAncillaryPassengerData implements Converter<Passenger, CheckinAncillaryPassengerData> {

	@Reference
	private FrequentFlyerToMmbAncillaryFrequentFlyerData frequentFlyerConverter;

	@Reference
	private ETicketToCheckinAncillaryEticketData eticketConverter;

	@Override
	public CheckinAncillaryPassengerData convert(Passenger source) {
		CheckinAncillaryPassengerData destination = null;

		if (source != null) {
			destination = new CheckinAncillaryPassengerData();

			destination.setId(source.getX003CIdX003EKBackingField());
			destination.setLastName(source.getX003CLastNameX003EKBackingField());
			destination.setName(source.getX003CNameX003EKBackingField());
			destination.setReferenceNumber(source.getX003CReferenceNumberX003EKBackingField());

			destination.setFrequentFlyerInfo(
					frequentFlyerConverter.convert(source.getX003CFrequentFlyerInfoX003EKBackingField()));

			List<CheckinAncillaryEticketData> etickets = null;
			if (source.getX003CETicketsX003EKBackingField() != null &&
					source.getX003CETicketsX003EKBackingField().getETicket() != null &&
					!source.getX003CETicketsX003EKBackingField().getETicket().isEmpty()) {
				etickets = new ArrayList<CheckinAncillaryEticketData>();
				List<ETicket> sourceEticketsList = source.getX003CETicketsX003EKBackingField().getETicket();
				for(ETicket sourceEticket : sourceEticketsList)
					etickets.add(eticketConverter.convert(sourceEticket));
				
			}
			destination.setEtickets(etickets);
		}

		return destination;
	}

}
