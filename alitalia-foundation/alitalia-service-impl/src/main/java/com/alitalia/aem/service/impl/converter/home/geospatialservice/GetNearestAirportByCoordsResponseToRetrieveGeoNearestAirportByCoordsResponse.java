package com.alitalia.aem.service.impl.converter.home.geospatialservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveGeoNearestAirportByCoordsResponse;
import com.alitalia.aem.ws.geospatial.service.xsd6.GetNearestAirportByCoordsResponse;

@Component(immediate = true, metatype = false)
@Service(value = GetNearestAirportByCoordsResponseToRetrieveGeoNearestAirportByCoordsResponse.class)
public class GetNearestAirportByCoordsResponseToRetrieveGeoNearestAirportByCoordsResponse 
		implements Converter<GetNearestAirportByCoordsResponse, RetrieveGeoNearestAirportByCoordsResponse> {

	@Reference
	private IPAddressDataToIPAddress ipAddressDataConverter;
	
	@Override
	public RetrieveGeoNearestAirportByCoordsResponse convert(GetNearestAirportByCoordsResponse source) {
		
		RetrieveGeoNearestAirportByCoordsResponse destination = null;
		
		if (source != null) {
			
			destination = new RetrieveGeoNearestAirportByCoordsResponse();
			
			if (source.getAirportCode() != null) {
				destination.setAirportCode(
						source.getAirportCode().getValue());
			}
			
			if (source.getAirportDesc() != null) {
				destination.setAirportDesc(
						source.getAirportDesc().getValue());
			}
			
			if (source.getAirportDistanceKM() != null) {
				destination.setAirportDistanceKM(
						source.getAirportDistanceKM().getValue());
			}
			
			if (source.getAirportLatitude() != null) {
				destination.setAirportLatitude(
						source.getAirportLatitude().getValue());
			}
			
			if (source.getAirportLongitude() != null) {
				destination.setAirportLongitude(
						source.getAirportLongitude().getValue());
			}
			
			if (source.getCityCode() != null) {
				destination.setCityCode(
						source.getCityCode().getValue());
			}
			
			if (source.getCityDesc() != null) {
				destination.setCityDesc(
						source.getCityDesc().getValue());
			}
			
			if (source.getCountryCode() != null) {
				destination.setCountryCode(
						source.getCountryCode().getValue());
			}
			
			if (source.getReturnCode() != null) {
				destination.setReturnCode(
						source.getReturnCode().getValue());
			}
			
			if (source.getReturnDescription() != null) {
				destination.setReturnDescription(
						source.getReturnDescription().getValue());
			}
			
		}
		
		return destination;
	}
	
}