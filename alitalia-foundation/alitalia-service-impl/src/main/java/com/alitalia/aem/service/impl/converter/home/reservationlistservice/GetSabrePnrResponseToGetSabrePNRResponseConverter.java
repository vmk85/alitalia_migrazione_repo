package com.alitalia.aem.service.impl.converter.home.reservationlistservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.GetSabrePNRResponse;
import com.alitalia.aem.ws.reservationlist.reservationlistservice.xsd1.GetSabrePnrResponse;

@Component(immediate=true, metatype=false)
@Service(value=GetSabrePnrResponseToGetSabrePNRResponseConverter.class)
public class GetSabrePnrResponseToGetSabrePNRResponseConverter implements Converter<GetSabrePnrResponse, GetSabrePNRResponse> {

	@Override
	public GetSabrePNRResponse convert(GetSabrePnrResponse source) {
		GetSabrePNRResponse destination = null;

		if (source != null) {
			destination = new GetSabrePNRResponse();

			destination.setPnr(source.getPnr().getValue());
			destination.setPnrSabre(source.getPnrSabre().getValue());
		}

		return destination;
	}

}
