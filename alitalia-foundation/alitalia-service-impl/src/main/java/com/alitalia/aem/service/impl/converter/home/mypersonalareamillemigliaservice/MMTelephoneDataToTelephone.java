package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMTelephoneData;
import com.alitalia.aem.common.data.home.enumerations.MMPhoneTypeEnum;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.PhoneTypes;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Telephone;

@Component(immediate = true, metatype = false)
@Service(value = MMTelephoneDataToTelephone.class)
public class MMTelephoneDataToTelephone implements Converter<MMTelephoneData, Telephone> {

	@Override
	public Telephone convert(MMTelephoneData source) {
		Telephone destination = null;

		if (source != null) {
			com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ObjectFactory objectFactory2 = new com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ObjectFactory();

			destination = objectFactory2.createTelephone();
			destination.setCountryNumber(objectFactory2.createTelephoneCountryNumber(source.getCountryNumber()));
			destination.setEmail(objectFactory2.createTelephoneEmail(source.getEmail()));
			destination.setInvalidIndicator(objectFactory2.createTelephoneInvalidIndicator(source.getInvalidIndicator()));
			destination.setIstruction(objectFactory2.createTelephoneIstruction(source.getIstruction()));
			destination.setNumber(objectFactory2.createTelephoneNumber(source.getNumber()));
			destination.setOrigCustomerID(objectFactory2.createTelephoneOrigCustomerID(source.getOrigCustomerId()));
			destination.setPhoneZone(objectFactory2.createTelephonePhoneZone(source.getPhoneZone()));
			destination.setSequenceNumber(objectFactory2.createTelephoneSequenceNumber(source.getSequenceNumber()));
			MMPhoneTypeEnum sourceTelephoneType = source.getTelephoneType();
			if (sourceTelephoneType != null)
				destination.setTelephoneType(PhoneTypes.fromValue(sourceTelephoneType.value()));

			if (source.getEffectiveDate() != null)
				destination.setEffectiveDate(XsdConvertUtils.toXMLGregorianCalendar(source.getEffectiveDate()));
			
		}

		return destination;
	}

}
