package com.alitalia.aem.service.impl.converter.home.flightstatusservice;

import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.FlightDetailsModelData;
import com.alitalia.aem.common.data.home.FlySegmentData;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.ArrayOfFlySegment;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.FlySegment;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.ObjectFactory;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.FlightDetailsModel;

@Component(immediate = true, metatype = false)
@Service(value = FlightItinerayDetailsModelDataToFlightDetailsModel.class)
public class FlightItinerayDetailsModelDataToFlightDetailsModel implements Converter<FlightDetailsModelData, FlightDetailsModel> {

	@Override
	public FlightDetailsModel convert(FlightDetailsModelData source) {
		FlightDetailsModel flightDetails = new FlightDetailsModel();

		ObjectFactory objectFactory = new ObjectFactory();

		String aircraft = source.getAirCraft();
		String flightNumber = source.getFlightNumber();
		String vector = source.getVector();

		if(aircraft != null){
			flightDetails.setAirCraft(objectFactory.createFlightDetailsModelAirCraft(aircraft));
		}

		if(flightNumber != null){
			flightDetails.setFlightNumber(objectFactory.createFlightDetailsModelFlightNumber(flightNumber));
		}
		if(vector != null){
			flightDetails.setVector(objectFactory.createFlightDetailsModelVector(vector));
		}

		flightDetails.setFlightDate(flightDetails.getFlightDate());
		
		List<FlySegmentData> flySegmentDatas = source.getFlySegments();
		ArrayOfFlySegment flySegments = objectFactory.createArrayOfFlySegment();
		List<FlySegment> flySegmentList = flySegments.getFlySegment();
		for(FlySegmentData flySegmentData: flySegmentDatas){
			FlySegment flySegment = new FlySegment();
			flySegment.setBoardPoint(objectFactory.createFlySegmentBoardPoint(flySegmentData.getBoardPoint()));
			flySegment.setBoardPointDepTime(objectFactory.createFlySegmentBoardPointDepTime(flySegmentData.getBoardPointDepTime()));
			flySegment.setDaysFromStart(flySegmentData.getDaysFromStart());
			flySegment.setMilesFromStart(flySegmentData.getMilesFromStart());
			flySegment.setOffPoint(objectFactory.createFlySegmentOffPoint(flySegmentData.getOffPoint()));
			flySegment.setOffPointArrTime(objectFactory.createFlySegmentTimeFromStart(flySegmentData.getTimeFromStart()));
			flySegmentList.add(flySegment);
		}

		JAXBElement<ArrayOfFlySegment>  jaxbFlySegments = objectFactory.createFlightDetailsModelFlySegment(flySegments);
		flightDetails.setFlySegment(jaxbFlySegments);

		return flightDetails;
	}
}