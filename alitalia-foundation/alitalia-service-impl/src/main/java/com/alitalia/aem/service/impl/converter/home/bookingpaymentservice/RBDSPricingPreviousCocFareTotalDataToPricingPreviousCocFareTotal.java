package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingPreviousCocFareTotalData;
import com.alitalia.aem.ws.booking.paymentservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd11.ResultBookingDetailsSolutionPricingPreviousCocFareTotal;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingPreviousCocFareTotalDataToPricingPreviousCocFareTotal.class)
public class RBDSPricingPreviousCocFareTotalDataToPricingPreviousCocFareTotal
		implements Converter<ResultBookingDetailsSolutionPricingPreviousCocFareTotalData, ResultBookingDetailsSolutionPricingPreviousCocFareTotal> {
	
	@Override
	public ResultBookingDetailsSolutionPricingPreviousCocFareTotal convert(
			ResultBookingDetailsSolutionPricingPreviousCocFareTotalData source) {
		ResultBookingDetailsSolutionPricingPreviousCocFareTotal destination = null;
		if(source!=null){
			ObjectFactory objf = new ObjectFactory();
			destination = objf.createResultBookingDetailsSolutionPricingPreviousCocFareTotal();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}

}
