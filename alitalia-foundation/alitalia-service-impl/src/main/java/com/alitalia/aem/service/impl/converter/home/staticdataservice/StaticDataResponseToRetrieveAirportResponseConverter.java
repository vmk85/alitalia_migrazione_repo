package com.alitalia.aem.service.impl.converter.home.staticdataservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.StaticDataResponse;
import com.alitalia.aem.ws.booking.staticdataservice.xsd5.Airport;

@Component(immediate=true, metatype=false)
@Service(value=StaticDataResponseToRetrieveAirportResponseConverter.class)
public class StaticDataResponseToRetrieveAirportResponseConverter implements Converter<StaticDataResponse, RetrieveAirportsResponse> {

	private static final Logger logger = LoggerFactory.getLogger(StaticDataResponseToRetrieveAirportResponseConverter.class);

	@Reference
	private AirportToAirportData airportCoverter;

	@Reference
	private StaticDataServiceJAXBContextFactory jaxbContextFactory;

	@SuppressWarnings("unchecked")
	@Override
	public RetrieveAirportsResponse convert(StaticDataResponse source) {
		RetrieveAirportsResponse destination = new RetrieveAirportsResponse();
		try {
			List<AirportData> airportsData = new ArrayList<>();
			
			Unmarshaller airportUnmashaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			JAXBElement<Airport> unmarshalledObj = null;

			List<Object> airports = source.getEntities().getValue().getAnyType();
			for(Object airport: airports){
				unmarshalledObj = (JAXBElement<Airport>) airportUnmashaller.unmarshal((Node) airport);
				Airport castedAirport = unmarshalledObj.getValue();
				AirportData airportData = airportCoverter.convert(castedAirport);
				airportsData.add(airportData);
			}
			
			destination.setAirports(airportsData);
			
		} catch (JAXBException e) {
			logger.error("Error instantiating JAXBContext/Unmarshaller: {}", e);
		} finally {
			logger.info("Conversion executed successfully");
		}
		return destination;			
	}
}