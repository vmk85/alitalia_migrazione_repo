package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.home.BalanceResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.GetBalanceResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;

@Component(immediate = true, metatype = false)
@Service(value = GetBalanceResponseToBalanceResponse.class)
public class GetBalanceResponseToBalanceResponse implements Converter<GetBalanceResponse, BalanceResponse> {

	@Reference
	private CustomerProfileToMMCustomerProfileData customerProfileConverter;
	
	@Override
	public BalanceResponse convert(GetBalanceResponse source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		BalanceResponse destination = new BalanceResponse(); 
		
		JAXBElement<CustomerProfile> jaxbElement = source.getCustomer();
		CustomerProfile customerProfile = jaxbElement.getValue();
		MMCustomerProfileData mmCustomerProfile = customerProfileConverter.convert(customerProfile);
		destination.setCustomerProfile(mmCustomerProfile);
		
		return destination;
	}
}