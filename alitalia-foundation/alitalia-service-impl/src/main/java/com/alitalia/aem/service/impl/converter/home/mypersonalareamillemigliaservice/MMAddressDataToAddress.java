package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMAddressData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Address;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.AddressTypes;

@Component(immediate = true, metatype = false)
@Service(value = MMAddressDataToAddress.class)
public class MMAddressDataToAddress implements Converter<MMAddressData, Address> {

	@Override
	public Address convert(MMAddressData source) {
		Address destination = null;

		if (source != null) {
			com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ObjectFactory objectFactory2 = new com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ObjectFactory();

			destination = objectFactory2.createAddress();
			destination.setAddressType(AddressTypes.fromValue(source.getAddressType().value()));
			destination.setCompanyName(objectFactory2.createAddressCompanyName(source.getCompanyName()));
			destination.setCountry(objectFactory2.createAddressCountry(source.getCountry()));
			destination.setInvalidIndicator(objectFactory2.createAddressInvalidIndicator(source.getInvalidIndicator()));
			destination.setIstruction(objectFactory2.createAddressIstruction(source.getIstruction()));
			destination.setMailingIndicator(objectFactory2.createAddressMailingIndicator(source.getMailingIndicator()));
			destination.setMunicipalityName(objectFactory2.createAddressMunicipalityName(source.getMunicipalityName()));
			destination.setPostalCode(objectFactory2.createAddressPostalCode(source.getPostalCode()));
			destination.setStateCode(objectFactory2.createAddressStateCode(source.getStateCode()));
			destination.setStreetFreeText(objectFactory2.createAddressStreetFreeText(source.getStreetFreeText()));

			if (source.getStartDate() != null)
				destination.setStartDate(XsdConvertUtils.toXMLGregorianCalendar(source.getStartDate()));
			if (source.getEndDate() != null)
				destination.setEndDate(XsdConvertUtils.toXMLGregorianCalendar(source.getEndDate()));
		}

		return destination;
	}

}
