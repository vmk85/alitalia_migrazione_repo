package com.alitalia.aem.service.impl.converter.home.carnetstaticdataservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.ws.carnet.staticdataservice.xsd2.Country;



@Component(immediate=true, metatype=false)
@Service(value=CountryToCountryData.class)
public class CountryToCountryData implements Converter<Country, CountryData> {

	@Override
	public CountryData convert(Country source) {
		CountryData destination = null;

		if (source != null) {
			destination = new CountryData();
			if (source.getCode() != null)
				destination.setCode(source.getCode().getValue());
			if (source.getDescription() != null)
				destination.setDescription(source.getDescription().getValue());
			if (source.getStateCode() != null)
				destination.setStateCode(source.getStateCode().getValue());
			if (source.getStateDescription() != null)
				destination.setStateDescription(source.getStateDescription().getValue());
		}

		return destination;
	}
}