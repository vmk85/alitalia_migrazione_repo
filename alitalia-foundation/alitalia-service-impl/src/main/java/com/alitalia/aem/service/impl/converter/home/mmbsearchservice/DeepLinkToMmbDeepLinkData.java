package com.alitalia.aem.service.impl.converter.home.mmbsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.MmbDeepLinkData;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.DeepLink;

@Component(immediate=true, metatype=false)
@Service(value=DeepLinkToMmbDeepLinkData.class)
public class DeepLinkToMmbDeepLinkData implements Converter<DeepLink, MmbDeepLinkData> {

	@Override
	public MmbDeepLinkData convert(DeepLink source) {
		MmbDeepLinkData destination = null;

		if (source != null) {
			destination = new MmbDeepLinkData();

			destination.setCode(source.getX003CCodeX003EKBackingField());
			destination.setDescription(source.getX003CDescriptionX003EKBackingField());
			destination.setId(source.getX003CIdX003EKBackingField());
			destination.setUrl(source.getX003CUrlX003EKBackingField());
		}

		return destination;
	}

}
