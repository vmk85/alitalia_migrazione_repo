package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentConnectionExtWarningData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionSliceSegmentConnectionExtWarning;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentConnectionExtWarningDataToRTDSSliceSegmentConnectionExtWarning.class)
public class RTDSSliceSegmentConnectionExtWarningDataToRTDSSliceSegmentConnectionExtWarning implements
		Converter<ResultTicketingDetailSolutionSliceSegmentConnectionExtWarningData, 
					ResultTicketingDetailSolutionSliceSegmentConnectionExtWarning> {

	@Override
	public ResultTicketingDetailSolutionSliceSegmentConnectionExtWarning convert(
			ResultTicketingDetailSolutionSliceSegmentConnectionExtWarningData source) {
		ResultTicketingDetailSolutionSliceSegmentConnectionExtWarning destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionSliceSegmentConnectionExtWarning();

			destination.setLongLayoverField(source.isLongLayoverField());
			destination.setLongLayoverFieldSpecified(source.isLongLayoverFieldSpecified());
			destination.setRiskyConnectionField(source.isRiskyConnectionField());
			destination.setRiskyConnectionFieldSpecified(source.isRiskyConnectionFieldSpecified());
			destination.setTightConnectionField(source.isTightConnectionField());
			destination.setTightConnectionFieldSpecified(source.isTightConnectionFieldSpecified());
		}

		return destination;
	}

}
