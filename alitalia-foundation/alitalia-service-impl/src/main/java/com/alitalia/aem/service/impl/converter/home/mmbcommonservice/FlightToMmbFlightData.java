package com.alitalia.aem.service.impl.converter.home.mmbcommonservice;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferenceCardinality;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbApisTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbFlightStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbLegTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.MmbPassengerData;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.ApisType;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.ArrayOfPassenger;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.Cabin;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.CompartimentalClass;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.Flight;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.FlightStatus;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.LegType;
import com.alitalia.aem.ws.mmb.commonservice.xsd3.Passenger;
import com.alitalia.aem.ws.mmb.commonservice.xsd4.RouteType;

@Component(immediate=true, metatype=false)
@Service(value=FlightToMmbFlightData.class)
public class FlightToMmbFlightData implements Converter<Flight, MmbFlightData> {

	@Reference
	private DeepLinkToMmbDeepLinkData deepLinkConverter;

	@Reference
	private AirportToMmbAirportData airportConverter;

	// E' stata inserita la cardianlita' opzionale perche' c'e' un ciclo
	// di riferimenti fra Passenger, Flight e Coupon che altrimenti impedisce
	// che salgano i service component
	@Reference(policy=ReferencePolicy.DYNAMIC, cardinality=ReferenceCardinality.OPTIONAL_UNARY)
	private volatile PassengerToMmbPassengerData passengerConverter;

	@Override
	public MmbFlightData convert(Flight source) {
		MmbFlightData destination = null;

		if (source != null) {
			destination = new MmbFlightData();

			ApisType sourceApisTypeReq = source.getX003CApisTypeRequiredX003EKBackingField();
			if (sourceApisTypeReq != null)
				destination.setApisTypeRequired(MmbApisTypeEnum.fromValue(sourceApisTypeReq.value()));

			Calendar arrivalDateTime = source.getX003CArrivalDateX003EKBackingField().toGregorianCalendar();
			Calendar arrivalTime = source.getX003CArrivalTimeX003EKBackingField().toGregorianCalendar();
			arrivalDateTime.set(Calendar.HOUR_OF_DAY, arrivalTime.get(Calendar.HOUR_OF_DAY));
			arrivalDateTime.set(Calendar.MINUTE, arrivalTime.get(Calendar.MINUTE));
			arrivalDateTime.set(Calendar.SECOND, arrivalTime.get(Calendar.SECOND));
			destination.setArrivalDateTime(arrivalDateTime);

			destination.setBaggageAllow(source.getX003CBaggageAllowX003EKBackingField());

			Cabin sourceCabin = source.getX003CCabinX003EKBackingField();
			if (sourceCabin != null)
				destination.setCabin(CabinEnum.fromValue(sourceCabin.value()));

			destination.setCarrier(source.getX003CCarrierX003EKBackingField());
			destination.setComfortSeatFare(source.getX003CComfortSeatFareX003EKBackingField());
			destination.setComfortSeatPaid(source.isX003CComfortSeatPaidX003EKBackingField());

			Calendar departureDateTime = source.getX003CDepartureDateX003EKBackingField().toGregorianCalendar();
			Calendar departureTime = source.getX003CDepartureTimeX003EKBackingField().toGregorianCalendar();
			departureDateTime.set(Calendar.HOUR_OF_DAY, departureTime.get(Calendar.HOUR_OF_DAY));
			departureDateTime.set(Calendar.MINUTE, departureTime.get(Calendar.MINUTE));
			departureDateTime.set(Calendar.SECOND, departureTime.get(Calendar.SECOND));
			destination.setDepartureDateTime(departureDateTime);

			destination.setEnabledSeatMap(source.isX003CEnabledSeatMapX003EKBackingField());
			destination.setEticket(source.getX003CEticketX003EKBackingField());
			destination.setEticketClass(source.getX003CEticketClassX003EKBackingField());
			destination.setExtraBaggage(source.getX003CExtraBaggageX003EKBackingField());
			destination.setFareClass(source.getX003CFareClassX003EKBackingField());
			destination.setFirstFlight(source.isX003CFirstFlightX003EKBackingField());
			destination.setFlightNumber(source.getX003CFlightNumberX003EKBackingField());
			destination.setFromCity(source.getX003CFromCityX003EKBackingField());
			destination.setFromTerminal(source.getX003CFromTerminalX003EKBackingField());
			destination.setHasComfortSeat(source.isX003CHasComfortSeatX003EKBackingField());
			destination.setId(source.getX003CIdX003EKBackingField());
			destination.setIndex(source.getX003CIndexX003EKBackingField());

			LegType sourceLegType = source.getX003CLegTypeX003EKBackingField();
			if (sourceLegType != null)
				destination.setLegType(MmbLegTypeEnum.fromValue(sourceLegType.value()));

			destination.setMiniFare(source.isX003CIsMiniFareX003EKBackingField());
			destination.setMultitratta(source.getX003CMultitrattaX003EKBackingField());
			destination.setOperatingCarrier(source.getX003COperatingCarrierX003EKBackingField());
			destination.setOperatingFlightNumber(source.getX003COperatingFlightNumberX003EKBackingField());
			destination.setPnr(source.getX003CPnrX003EKBackingField());
			destination.setRouteId(source.getX003CRouteIdX003EKBackingField());
			destination.setRph(source.getX003CRPHX003EKBackingField());
			CompartimentalClass sourceSeatClass = source.getX003CSeatClassX003EKBackingField();
			if (sourceSeatClass != null)
				destination.setSeatClass(MmbCompartimentalClassEnum.fromValue(sourceSeatClass.value()));

			FlightStatus sourceFlightStatus = source.getX003CStatusX003EKBackingField();
			if (sourceFlightStatus != null)
				destination.setStatus(MmbFlightStatusEnum.fromValue(sourceFlightStatus.value()));

			destination.setToCity(source.getX003CToCityX003EKBackingField());
			destination.setToTerminal(source.getX003CToTerminalX003EKBackingField());

			RouteType sourceType = source.getX003CTypeX003EKBackingField();
			if (sourceType != null)
				destination.setType(RouteTypeEnum.fromValue(sourceType.value()));

			destination.setDeepLinkCode(
					deepLinkConverter.convert(source.getX003CDeepLinkCodeX003EKBackingField()));

			destination.setFrom(airportConverter.convert(source.getX003CFromX003EKBackingField()));

			ArrayOfPassenger sourcePassengers = source.getX003CPassengersX003EKBackingField();
			if (sourcePassengers != null &&
					sourcePassengers.getPassenger() != null &&
					!sourcePassengers.getPassenger().isEmpty()) {
				List<MmbPassengerData> passengersList = new ArrayList<MmbPassengerData>();
				for(Passenger passenger : sourcePassengers.getPassenger()) {
					passengersList.add(passengerConverter.convert(passenger));
				}
				destination.setPassengers(passengersList);
			}

			destination.setTo(airportConverter.convert(source.getX003CToX003EKBackingField()));
		}

		return destination;
	}

}
