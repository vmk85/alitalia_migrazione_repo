package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.AgencyInsertConsentRequest;
import com.alitalia.aem.common.messages.home.AgencyInsertConsentResponse;
import com.alitalia.aem.common.messages.home.AgencyLoginRequest;
import com.alitalia.aem.common.messages.home.AgencyLoginResponse;
import com.alitalia.aem.common.messages.home.AgencyPasswordChangeRequest;
import com.alitalia.aem.common.messages.home.AgencyPasswordChangeResponse;
import com.alitalia.aem.common.messages.home.AgencyRetrieveDataRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrieveDataResponse;
import com.alitalia.aem.common.messages.home.AgencyRetrievePasswordByIDRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrievePasswordByVATRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrievePasswordResponse;
import com.alitalia.aem.common.messages.home.AgencySendMailRequest;
import com.alitalia.aem.common.messages.home.AgencySendMailResponse;
import com.alitalia.aem.common.messages.home.AgencySubscriptionRequest;
import com.alitalia.aem.common.messages.home.AgencySubscriptionResponse;
import com.alitalia.aem.common.messages.home.AgencyUnsubscribeNewsLetterIataRequest;
import com.alitalia.aem.common.messages.home.AgencyUnsubscribeNewsLetterIataResponse;
import com.alitalia.aem.common.messages.home.AgencyUpdateDataRequest;
import com.alitalia.aem.common.messages.home.AgencyUpdateDataResponse;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.service.api.home.BusinessLoginService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.api.home.exc.businessloginservice.RetrievePasswordByIDException;
import com.alitalia.aem.service.api.home.exc.businessloginservice.RetrievePasswordByVATException;
import com.alitalia.aem.service.impl.converter.home.businessloginservice.AgencySubscriptionRequestToSalesAgentSubscriptionInput;
import com.alitalia.aem.service.impl.converter.home.businessloginservice.EmailMessageToMessaggioEmail;
import com.alitalia.aem.service.impl.converter.home.businessloginservice.GetAnagraficaAgenziaToAgencyRetrieveDataResponse;
import com.alitalia.aem.service.impl.converter.home.businessloginservice.LoginB2BResultToAgencyLoginResponse;
import com.alitalia.aem.service.impl.converter.home.businessloginservice.RetrievePasswordResultToAgencyRetrievePasswordResponse;
import com.alitalia.aem.service.impl.converter.home.businessloginservice.SalesAgentSubscriptionOutputToAgencySubscriptionResponse;
import com.alitalia.aem.ws.b2bv2.BusinessToBusinessServiceClient;
import com.alitalia.aem.ws.b2bv2.xsd.ChangePassword;
import com.alitalia.aem.ws.b2bv2.xsd.ChangePasswordResponse;
import com.alitalia.aem.ws.b2bv2.xsd.GetAnagraficaAgenzia;
import com.alitalia.aem.ws.b2bv2.xsd.GetAnagraficaAgenziaResponse;
import com.alitalia.aem.ws.b2bv2.xsd.GetAnagraficaAgenziaResponse.GetAnagraficaAgenziaResult;
import com.alitalia.aem.ws.b2bv2.xsd.InsAgyEst;
import com.alitalia.aem.ws.b2bv2.xsd.InsAgyEstResponse;
import com.alitalia.aem.ws.b2bv2.xsd.InsertConsensiAgenzia;
import com.alitalia.aem.ws.b2bv2.xsd.InsertConsensiAgenziaResponse;
import com.alitalia.aem.ws.b2bv2.xsd.InsertUserNonIataV2;
import com.alitalia.aem.ws.b2bv2.xsd.InsertUserNonIataV2Response;
import com.alitalia.aem.ws.b2bv2.xsd.LoginB2B;
import com.alitalia.aem.ws.b2bv2.xsd.LoginB2BResponse;
import com.alitalia.aem.ws.b2bv2.xsd.LoginB2BResponse.LoginB2BResult;
import com.alitalia.aem.ws.b2bv2.xsd.RetrievePassword;
import com.alitalia.aem.ws.b2bv2.xsd.RetrievePasswordByPartitaIva;
import com.alitalia.aem.ws.b2bv2.xsd.RetrievePasswordByPartitaIvaResponse;
import com.alitalia.aem.ws.b2bv2.xsd.RetrievePasswordByPartitaIvaResponse.RetrievePasswordByPartitaIvaResult;
import com.alitalia.aem.ws.b2bv2.xsd.RetrievePasswordResponse;
import com.alitalia.aem.ws.b2bv2.xsd.RetrievePasswordResponse.RetrievePasswordResult;
import com.alitalia.aem.ws.common.utils.WSClientUtility;
import com.alitalia.aem.ws.salesagentsubstrade.IntfB2TTradeSalesAgentSubscriptionWSServiceServiceagentClient;
import com.alitalia.aem.ws.salesagentsubstrade.xsd.SalesAgentSubscriptionInput;
import com.alitalia.aem.ws.salesagentsubstrade.xsd.SalesAgentSubscriptionOutput;
import com.alitalia.aem.ws.sendmail.SendMailServiceClient;
import com.alitalia.aem.ws.sendmail.xsd.MessaggioEmail;
import com.alitalia.aem.ws.sendmail.xsd.Send;
import com.alitalia.aem.ws.sendmail.xsd.SendResponse;

@Service
@Component(immediate=true, metatype=false)
public class SimpleBusinessLoginService implements BusinessLoginService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleBusinessLoginService.class);

	@Reference
	private BusinessToBusinessServiceClient businessLoginClient;

	@Reference
	private IntfB2TTradeSalesAgentSubscriptionWSServiceServiceagentClient webAgencySubscriptionClient;

	@Reference
	private LoginB2BResultToAgencyLoginResponse loginB2BResultConverter;

	@Reference
	private SendMailServiceClient sendMailClient;

	@Reference
	private RetrievePasswordResultToAgencyRetrievePasswordResponse retrievePasswordResultConverter;

	@Reference
	private GetAnagraficaAgenziaToAgencyRetrieveDataResponse getAnagraficaAgenziaConverter;

	@Reference
	private AgencySubscriptionRequestToSalesAgentSubscriptionInput agencySubscriptionRequestConverter;

	@Reference
	private SalesAgentSubscriptionOutputToAgencySubscriptionResponse agencySubscriptionResponseConverter;

	@Reference
	private EmailMessageToMessaggioEmail emailMessageConverter;

	@Override
	public AgencyLoginResponse agencyLogin(AgencyLoginRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method agencyLogin. The request is {}", request);
		}

		LoginB2BResult businessLoginResponse = null;
		String idRuolo = request.getRuolo().value();
		String codiceAgenzia = request.getCodiceAgenzia();
		String password = request.getPassword();

		try {

			AgencyLoginResponse response = new AgencyLoginResponse();
			boolean operationSuccessful = false;
			boolean isFirstLogin = false;
			boolean isPasswordExpired = false;
			boolean isAccountLocked = false;
			boolean isApprovalRequired = false;
			response = new AgencyLoginResponse(	request.getTid(), request.getSid(), null, operationSuccessful,
					isFirstLogin, isPasswordExpired, isAccountLocked, isApprovalRequired);

			businessLoginResponse = businessLoginClient.loginB2B(idRuolo, codiceAgenzia, password);
			if (businessLoginResponse != null)
				response = loginB2BResultConverter.convert(businessLoginResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method agencyLogin.", e);
			LoginB2B loginB2B = new LoginB2B();
			loginB2B.setCodiceAgenzia(codiceAgenzia);
			loginB2B.setIDRuolo(idRuolo);
			loginB2B.setPassword(password);
			WSClientUtility.printXMLMessage(loginB2B, "agencyLogin",  true);
			if (businessLoginResponse != null){
				LoginB2BResponse loginB2BResponse = new LoginB2BResponse();
				loginB2BResponse.setLoginB2BResult(businessLoginResponse);
				WSClientUtility.printXMLMessage(loginB2BResponse, "agencyLogin", false);
			}
			throw new AlitaliaServiceException("Exception executing agencyLogin: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public AgencyPasswordChangeResponse changePassword(AgencyPasswordChangeRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method changePassword. The request is {}", request);
		}

		Boolean operationSuccessful = null;
		String idRuolo = request.getRuolo().value();
		String codiceAgenzia = request.getCodiceAgenzia();
		String oldPassword = request.getOldPassword();
		String newPassword = request.getNewPassword();

		try {

			AgencyPasswordChangeResponse response = new AgencyPasswordChangeResponse(request.getTid(), request.getSid(), operationSuccessful);
			
			operationSuccessful = businessLoginClient.changePassword(idRuolo, codiceAgenzia, oldPassword, newPassword);
			response.setOperationSuccessful(operationSuccessful);
			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method.", e);
			ChangePassword changePwd = new ChangePassword();
			changePwd.setCodiceAgenzia(codiceAgenzia);
			changePwd.setIDRuolo(idRuolo);
			changePwd.setOldPassword(oldPassword);
			changePwd.setNewPassword(newPassword);
			WSClientUtility.printXMLMessage(changePwd, "changePassword",  true);
			if (operationSuccessful != null){
				ChangePasswordResponse changePwdResponse = new ChangePasswordResponse();
				changePwdResponse.setChangePasswordResult(operationSuccessful);
				WSClientUtility.printXMLMessage(changePwdResponse, "changePassword", false);
			}
			throw new AlitaliaServiceException("Exception executing changePassword: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public AgencyRetrievePasswordResponse retrievePasswordByID(AgencyRetrievePasswordByIDRequest request) {

		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrievePasswordByID. The request is {}", request);
		}

		RetrievePasswordResult retrievePasswordResponse = null;
		String idRuolo = request.getRuolo().value();
		String codiceAgenzia = request.getCodiceAgenzia();

		try {

			AgencyRetrievePasswordResponse response = new AgencyRetrievePasswordResponse(request.getTid(), request.getSid(), null, false, false, false);
			retrievePasswordResponse = businessLoginClient.retrievePassword(idRuolo, codiceAgenzia);
			if (retrievePasswordResponse != null)
				response = retrievePasswordResultConverter.convert(retrievePasswordResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method retrievePasswordByID.", e);
			RetrievePassword retrievePwd = new RetrievePassword();
			retrievePwd.setCodiceAgenzia(codiceAgenzia);
			retrievePwd.setIDRuolo(idRuolo);
			WSClientUtility.printXMLMessage(retrievePwd, "retrievePasswordByID",  true);
			if (retrievePasswordResponse != null){
				RetrievePasswordResponse retrievePwdResponse = new RetrievePasswordResponse();
				retrievePwdResponse.setRetrievePasswordResult(retrievePasswordResponse);
				WSClientUtility.printXMLMessage(retrievePwdResponse, "retrievePasswordByID", false);
			}
			throw new RetrievePasswordByIDException("Exception executing retrievePasswordByID: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public AgencyRetrievePasswordResponse retrievePasswordByVAT(AgencyRetrievePasswordByVATRequest request) {

		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrievePasswordByVAT. The request is {}", request);
		}
		
		String idRuolo = request.getRuolo().value();
		String partitaIva = request.getPartitaIva();
		RetrievePasswordByPartitaIvaResult retrievePasswordResponse = null;


		try {

			AgencyRetrievePasswordResponse response = new AgencyRetrievePasswordResponse(request.getTid(), request.getSid(), null, false, false, false);
			
			retrievePasswordResponse = businessLoginClient.retrievePasswordByPartitaIva(idRuolo, partitaIva);
			if (retrievePasswordResponse != null)
				response = retrievePasswordResultConverter.convert(retrievePasswordResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method retrievePasswordByVAT.", e);
			RetrievePasswordByPartitaIva retrievePwd = new RetrievePasswordByPartitaIva();
			retrievePwd.setPartitaIva(partitaIva);
			retrievePwd.setIDRuolo(idRuolo);
			WSClientUtility.printXMLMessage(retrievePwd, "retrievePasswordByVAT",  true);
			if (retrievePasswordResponse != null){
				RetrievePasswordByPartitaIvaResponse retrievePwdResponse = new RetrievePasswordByPartitaIvaResponse();
				retrievePwdResponse.setRetrievePasswordByPartitaIvaResult(retrievePasswordResponse);
				WSClientUtility.printXMLMessage(retrievePwdResponse, "retrievePasswordByVAT", false);
			}
			throw new RetrievePasswordByVATException("Exception executing retrievePasswordByVAT: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public AgencyRetrieveDataResponse retrieveAgencyData(AgencyRetrieveDataRequest request) {

		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveAgencyData. The request is {}", request);
		}

		GetAnagraficaAgenziaResult result = null;
		String idAgenzia = request.getIdAgenzia();

		try {
			AgencyRetrieveDataResponse response = new AgencyRetrieveDataResponse(request.getTid(), request.getSid());
			result = businessLoginClient.retrieveAnagraficaAgenzia(idAgenzia);
			if (result != null)
				response = getAnagraficaAgenziaConverter.convert(result);

			response.setTid(request.getTid());
			response.setSid(request.getSid());
			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method retrieveAgencyData.", e);
			GetAnagraficaAgenzia anagraficaAgenziaRequest = new GetAnagraficaAgenzia();
			anagraficaAgenziaRequest.setIdAgenzia(idAgenzia);
			WSClientUtility.printXMLMessage(anagraficaAgenziaRequest, "retrieveAgencyData",  true);
			if (result != null){
				GetAnagraficaAgenziaResponse anagraficaAgenziaResponse = new GetAnagraficaAgenziaResponse();
				anagraficaAgenziaResponse.setGetAnagraficaAgenziaResult(result);
				WSClientUtility.printXMLMessage(anagraficaAgenziaResponse, "retrieveAgencyData", false);
			}
			throw new AlitaliaServiceException("Exception executing retrieveAgencyData: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public AgencyUpdateDataResponse updateAgencyData(AgencyUpdateDataRequest request) {

		if (logger.isDebugEnabled()) {
			logger.debug("Executing method updateAgencyData. The request is {}", request);

		}
		
		Boolean result = null;

		try {

			AgencyUpdateDataResponse response = new AgencyUpdateDataResponse(request.getTid(), request.getSid());
			result = businessLoginClient.insertUserNonIataV2(
					request.getCodiceAgenzia(),
					request.getRagioneSociale(),
					request.getPartitaIVA(),
					request.getCodiceFiscale(),
					request.getIndirizzo(),
					request.getCitta(),
					request.getProvincia(),
					request.getZip(),
					request.getRegione(),
					request.getStato(),
					request.getTelefono(),
					request.getFax(),
					request.getCodFamCon(),
					request.getEmailTitolare(),
					request.getEmailOperatore(),
					request.getCodiceAccordo(),
					request.getFlagSendMail(),
					request.getCodiceSirax(),
					XsdConvertUtils.printDateTime(request.getDataValiditaAccordo()));

			response.setIsSuccessful(Boolean.valueOf(result));
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method updateAgencyData.", e);
			InsertUserNonIataV2 insertUserNonIataV2 = new InsertUserNonIataV2();
			insertUserNonIataV2.setCodiceAgenzia(request.getCodiceAgenzia());
			insertUserNonIataV2.setRagioneSociale(request.getRagioneSociale());
			insertUserNonIataV2.setPartitaIVA(request.getPartitaIVA());
			insertUserNonIataV2.setCodiceFiscale(request.getCodiceFiscale());
			insertUserNonIataV2.setIndirizzo(request.getIndirizzo());
			insertUserNonIataV2.setCitta(request.getCitta());
			insertUserNonIataV2.setProvincia(request.getProvincia());
			insertUserNonIataV2.setZIP(request.getZip());
			insertUserNonIataV2.setRegione(request.getRegione());
			insertUserNonIataV2.setStato(request.getStato());
			insertUserNonIataV2.setTelefono(request.getTelefono());
			insertUserNonIataV2.setFax(request.getFax());
			insertUserNonIataV2.setCODFAMCON(request.getCodFamCon());
			insertUserNonIataV2.setEmailTitolare(request.getEmailTitolare());
			insertUserNonIataV2.setEmailOperatore(request.getEmailOperatore());
			insertUserNonIataV2.setCodiceAccordo(request.getCodiceAccordo());
			insertUserNonIataV2.setFlagSendMail(request.getFlagSendMail());
			insertUserNonIataV2.setCodiceSirax(request.getCodiceSirax());
			insertUserNonIataV2.setDataValiditaAccordo(XsdConvertUtils.printDateTime(request.getDataValiditaAccordo()));
			WSClientUtility.printXMLMessage(insertUserNonIataV2, "insertUserNonIataV2",  true);
			if (result != null){
				InsertUserNonIataV2Response insertUserNonIataV2Response = new InsertUserNonIataV2Response();
				insertUserNonIataV2Response.setInsertUserNonIataV2Result(result);
				WSClientUtility.printXMLMessage(insertUserNonIataV2Response, "insertUserNonIataV2", false);
			}
			throw new AlitaliaServiceException("Exception executing updateAgencyData: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public AgencySubscriptionResponse subscribeAgency(AgencySubscriptionRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method subscribeAgency. The request is {}", request);
		}
		
		SalesAgentSubscriptionInput salesAgentSubscriptionRequest = null;
		SalesAgentSubscriptionOutput salesAgentSubscriptionResponse = null;


		try {

			AgencySubscriptionResponse response = new AgencySubscriptionResponse();
			salesAgentSubscriptionRequest = agencySubscriptionRequestConverter.convert(request);
			salesAgentSubscriptionResponse = webAgencySubscriptionClient.b2TTradeSalesAgentSubscriptionWSOp(salesAgentSubscriptionRequest);
			response = agencySubscriptionResponseConverter.convert(salesAgentSubscriptionResponse);

			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}

			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method subscribeAgency.", e);
			if (salesAgentSubscriptionRequest!=null){
				WSClientUtility.printXMLMessage(salesAgentSubscriptionRequest, "subscribeAgency",  true);
			}
			if (salesAgentSubscriptionResponse != null){
				WSClientUtility.printXMLMessage(salesAgentSubscriptionResponse, "subscribeAgency", false);
			}
			throw new AlitaliaServiceException("Exception executing subscribeAgency: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public AgencySendMailResponse sendMail(AgencySendMailRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method sendMail. The request is {}", request);
		}

		Boolean esito = null;
		MessaggioEmail email = null;

		try {

			AgencySendMailResponse response = new AgencySendMailResponse();
			email = emailMessageConverter.convert(request.getEmailMessage());
			esito = sendMailClient.sendMail(email);
			response.setSendMailSuccessful(esito);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed sendMail successfully. The response is {}", response);
			}

			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method sendMail {}", e);
			if (email!=null){
				Send send = new Send();
				send.setMsgEmail(email);
				WSClientUtility.printXMLMessage(send, "sendMail",  true);
			}
			if (esito!=null){
				SendResponse sendResponse = new SendResponse();
				sendResponse.setSendResult(esito);
				WSClientUtility.printXMLMessage(sendResponse, "sendMail", false);
			}
			throw new AlitaliaServiceException("Exception executing sendMail: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public AgencySendMailResponse sendMailWithAttachment(AgencySendMailRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method sendMail. The request is {}", request);
		}

		Boolean esito = null;
		MessaggioEmail email = null;

		try {

			AgencySendMailResponse response = new AgencySendMailResponse();
			email = emailMessageConverter.convert(request.getEmailMessage());

			com.alitalia.aem.ws.sendmail.xsd.SendMailWithAttachXml.Attachments attachments = new com.alitalia.aem.ws.sendmail.xsd.SendMailWithAttachXml.Attachments();
			Object att = email.getUNCAllegato();
			attachments.setAny(att);

			com.alitalia.aem.ws.sendmail.xsd.SendMailWithAttachXml x = new com.alitalia.aem.ws.sendmail.xsd.SendMailWithAttachXml();
			x.setMsgEmail(email);
			x.setAttachments(attachments);


			esito = sendMailClient.sendMailWithAttachment(email, attachments);
			response.setSendMailSuccessful(esito);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed sendMail successfully. The response is {}", response);
			}

			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method sendMail {}", e);
			if (email!=null){
				Send send = new Send();
				send.setMsgEmail(email);
				WSClientUtility.printXMLMessage(send, "sendMail",  true);
			}
			if (esito!=null){
				SendResponse sendResponse = new SendResponse();
				sendResponse.setSendResult(esito);
				WSClientUtility.printXMLMessage(sendResponse, "sendMail", false);
			}
			throw new AlitaliaServiceException("Exception executing sendMail: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public AgencyInsertConsentResponse insertConsent(AgencyInsertConsentRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method insertConsent. The request is {}", request);
		}

		String idAgenzia = request.getIdAgenzia();
		boolean flagCondizioni = request.isFlagCondizioni();
		boolean flagDatiPersonali = request.isFlagDatiPersonali();
		Boolean result = null;

		try {

			AgencyInsertConsentResponse response = new AgencyInsertConsentResponse();
			

			result = businessLoginClient.insertConsensiAgenzia(idAgenzia, flagCondizioni, flagDatiPersonali);
			response.setResult(result);
			response.setTid(request.getTid());
			response.setSid(request.getSid());

			if (logger.isDebugEnabled()) {
				logger.debug("Executed insertConsent successfully. The response is {}", response);
			}

			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method insertConsent {}", e);
			InsertConsensiAgenzia insertConsensiAgenzia = new InsertConsensiAgenzia();
			insertConsensiAgenzia.setFlagCondizioni(flagCondizioni);
			insertConsensiAgenzia.setFlagDatiPersonali(flagDatiPersonali);
			insertConsensiAgenzia.setIdAgenzia(idAgenzia);
			WSClientUtility.printXMLMessage(insertConsensiAgenzia, "insertConsent",  true);
			if (result != null){
				InsertConsensiAgenziaResponse insertConsensiAgenziaResponse = new InsertConsensiAgenziaResponse();
				insertConsensiAgenziaResponse.setInsertConsensiAgenziaResult(result);
				WSClientUtility.printXMLMessage(insertConsensiAgenziaResponse, "insertConsent", false);
			}
			throw new AlitaliaServiceException("Exception executing insertConsent: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public AgencyUnsubscribeNewsLetterIataResponse unsubscribeNewsletterForeignAgency(AgencyUnsubscribeNewsLetterIataRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method unsubscribeNewsletterForeignAgency. The request is {}", request);
		}

		String idAgenzia = request.getIdAgenzia();
		Boolean result = null;
		
		logger.debug("Executing method unsubscribeNewsletterForeignAgency.", request);
		
		try {

			AgencyUnsubscribeNewsLetterIataResponse response = new AgencyUnsubscribeNewsLetterIataResponse();
			
			result = businessLoginClient.insAgyEst(idAgenzia);
			response.setResult(result);
			response.setTid(request.getTid());
			response.setSid(request.getSid());

			if (logger.isDebugEnabled()) {
				logger.debug("Executed unsubscribeNewsletterForeignAgency successfully. The response is {}", response);
			}

			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method unsubscribeNewsletterForeignAgency {}", e);
			InsAgyEst insAgyEst = new InsAgyEst();
			insAgyEst.setCodiceAgenzia(idAgenzia);
			WSClientUtility.printXMLMessage(insAgyEst, "unsubscribeNewsletterForeignAgency",  true);
			if (result != null){
				InsAgyEstResponse insAgyEstResponse = new InsAgyEstResponse();
				insAgyEstResponse.setInsAgyEstResult(result);
				WSClientUtility.printXMLMessage(insAgyEstResponse, "unsubscribeNewsletterForeignAgency", false);
			}
			throw new AlitaliaServiceException("Exception executing unsubscribeNewsletterForeignAgency: Sid ["+request.getSid()+"]" , e);
		}
	}
}