package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentLegExtData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionSliceSegmentLegExt;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentLegExtToRTDSSliceSegmentLegExtData.class)
public class RTDSSliceSegmentLegExtToRTDSSliceSegmentLegExtData implements
		Converter<ResultTicketingDetailSolutionSliceSegmentLegExt, ResultTicketingDetailSolutionSliceSegmentLegExtData> {

	@Reference
	private RTDSSliceSegmentLegExtWarningToRTDSSliceSegmentLegExtWarningData warningFieldConverter;

	@Override
	public ResultTicketingDetailSolutionSliceSegmentLegExtData convert(ResultTicketingDetailSolutionSliceSegmentLegExt source) {
		ResultTicketingDetailSolutionSliceSegmentLegExtData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionSliceSegmentLegExtData();

			destination.setWarningField(warningFieldConverter.convert(source.getWarningField()));
		}

		return destination;
	}

}
