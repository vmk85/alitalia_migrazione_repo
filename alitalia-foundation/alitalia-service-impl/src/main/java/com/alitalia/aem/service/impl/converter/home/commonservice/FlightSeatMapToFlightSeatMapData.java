package com.alitalia.aem.service.impl.converter.home.commonservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.FlightSeatMapData;
import com.alitalia.aem.common.data.home.SeatMapData;
import com.alitalia.aem.ws.booking.commonservice.xsd7.FlightSeatMap;
import com.alitalia.aem.ws.booking.commonservice.xsd7.SeatMap;

@Component(immediate=true, metatype=false)
@Service(value=FlightSeatMapToFlightSeatMapData.class)
public class FlightSeatMapToFlightSeatMapData 
		implements Converter<FlightSeatMap, FlightSeatMapData> {

	@Reference
	private DirectFlightToDirectFlightData directFlightDataConverter;
	
	@Reference
	private SeatMapToSeatMapData seatMapConverter;
	
	@Override
	public FlightSeatMapData convert(FlightSeatMap source) {
		
		FlightSeatMapData destination = null;
		
		if (source != null) {
		
			destination = new FlightSeatMapData();
			
			destination.setDirectFlight(
					directFlightDataConverter.convert(
							source.getX003CFlightX003EKBackingField()));
			
			List<SeatMapData> seatMaps = new ArrayList<SeatMapData>();
			if (source.getX003CSeatMapsX003EKBackingField() != null &&
					source.getX003CSeatMapsX003EKBackingField().getSeatMap() != null) {
				for (SeatMap seatMap : source.getX003CSeatMapsX003EKBackingField().getSeatMap()) {
					seatMaps.add(seatMapConverter.convert(seatMap));
				}
			}
			destination.setSeatMaps(seatMaps);
			
		}
		
		return destination;
		
	}

}
