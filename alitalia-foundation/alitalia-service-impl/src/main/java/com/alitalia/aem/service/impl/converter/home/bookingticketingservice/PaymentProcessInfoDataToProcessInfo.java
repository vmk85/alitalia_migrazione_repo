package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PaymentProcessInfoData;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ObjectFactory;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.PaymentStep;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ProcessInfo;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ResultType;

@Component(immediate = true, metatype = false)
@Service(value = PaymentProcessInfoDataToProcessInfo.class)
public class PaymentProcessInfoDataToProcessInfo implements Converter<PaymentProcessInfoData, ProcessInfo> {

	@Override
	public ProcessInfo convert(PaymentProcessInfoData source) {
		ProcessInfo destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();

			destination = objectFactory.createProcessInfo();
			destination.setMarketCode(objectFactory.createProcessInfoMarketCode(source.getMarketCode()));
			destination.setPaRes(objectFactory.createProcessInfoPaRes(source.getPaRes()));
			destination.setPaymentAttemps(source.getPaymentAttemps());
			destination.setPaymentAttemps(source.getPaymentAttemps());
			destination.setPointOfSale(objectFactory.createProcessInfoPointOfSale(source.getPointOfSale()));
			destination.setRedirectUrl(objectFactory.createProcessInfoRedirectUrl(source.getRedirectUrl()));

			if (source.getResult() != null) {
				destination.setResult(ResultType.fromValue(source.getResult().value()));
			}

			destination.setSessionId(objectFactory.createProcessInfoSessionId(source.getSessionId()));
			destination.setShopId(objectFactory.createProcessInfoShopId(source.getShopId()));
			destination.setSiteCode(objectFactory.createProcessInfoSiteCode(source.getSiteCode()));

			if (source.getStep() != null) {
				destination.setStep(PaymentStep.fromValue(source.getStep().value()));
			}

			destination.setTokenId(objectFactory.createProcessInfoTokenId(source.getTokenId()));
			destination.setTransactionId(objectFactory.createProcessInfoTransactionId(source.getTransactionId()));
		}

		return destination;
	}

}
