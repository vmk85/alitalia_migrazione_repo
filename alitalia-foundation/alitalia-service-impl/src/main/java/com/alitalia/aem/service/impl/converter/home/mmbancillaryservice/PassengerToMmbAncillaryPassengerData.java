package com.alitalia.aem.service.impl.converter.home.mmbancillaryservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryEticketData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryPassengerData;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.ETicket;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.Passenger;

@Component(immediate=true, metatype=false)
@Service(value=PassengerToMmbAncillaryPassengerData.class)
public class PassengerToMmbAncillaryPassengerData implements Converter<Passenger, MmbAncillaryPassengerData> {

	@Reference
	private FrequentFlyerToMmbAncillaryFrequentFlyerData frequentFlyerConverter;

	@Reference
	private ETicketToMmbAncillaryEticketData eticketConverter;

	@Override
	public MmbAncillaryPassengerData convert(Passenger source) {
		MmbAncillaryPassengerData destination = null;

		if (source != null) {
			destination = new MmbAncillaryPassengerData();

			destination.setId(source.getX003CIdX003EKBackingField());
			destination.setLastName(source.getX003CLastNameX003EKBackingField());
			destination.setName(source.getX003CNameX003EKBackingField());
			destination.setReferenceNumber(source.getX003CReferenceNumberX003EKBackingField());

			destination.setFrequentFlyerInfo(
					frequentFlyerConverter.convert(source.getX003CFrequentFlyerInfoX003EKBackingField()));

			List<MmbAncillaryEticketData> etickets = null;
			if (source.getX003CETicketsX003EKBackingField() != null &&
					source.getX003CETicketsX003EKBackingField().getETicket() != null &&
					!source.getX003CETicketsX003EKBackingField().getETicket().isEmpty()) {
				etickets = new ArrayList<MmbAncillaryEticketData>();
				List<ETicket> sourceEticketsList = source.getX003CETicketsX003EKBackingField().getETicket();
				for(ETicket sourceEticket : sourceEticketsList)
					etickets.add(eticketConverter.convert(sourceEticket));
				
			}
			destination.setEtickets(etickets);
		}

		return destination;
	}

}
