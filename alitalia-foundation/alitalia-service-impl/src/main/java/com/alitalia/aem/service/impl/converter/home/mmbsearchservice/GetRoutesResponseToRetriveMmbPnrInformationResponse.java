package com.alitalia.aem.service.impl.converter.home.mmbsearchservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.MmbPassengerData;
import com.alitalia.aem.common.data.home.mmb.MmbRouteData;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationResponse;
import com.alitalia.aem.ws.mmb.searchservice.xsd1.GetRoutesResponse;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.ArrayOfPassenger;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.ArrayOfRoute;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.Passenger;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.Route;

@Component(immediate=true, metatype=false)
@Service(value=GetRoutesResponseToRetriveMmbPnrInformationResponse.class)
public class GetRoutesResponseToRetriveMmbPnrInformationResponse implements
		Converter<GetRoutesResponse, RetriveMmbPnrInformationResponse> {

	@Reference
	private PassengerToMmbPassengerData passengerConverter;

	@Reference
	private RouteToMmbRouteData routeConverter;

	@Override
	public RetriveMmbPnrInformationResponse convert(GetRoutesResponse source) {
		RetriveMmbPnrInformationResponse destination = null;

		if (source != null) {
			destination = new RetriveMmbPnrInformationResponse();

			JAXBElement<String> sourceEticket = source.getEticket();
			if (sourceEticket != null)
				destination.seteTicket(sourceEticket.getValue());

			JAXBElement<String> sourceLastname = source.getLastname();
			if (sourceLastname != null)
				destination.setLastname(sourceLastname.getValue());

			JAXBElement<String> sourceName = source.getName();
			if (sourceName != null)
				destination.setName(sourceName.getValue());

			JAXBElement<String> sourcePnr = source.getPnr();
			if (sourcePnr != null)
				destination.setPnr(sourcePnr.getValue());

			JAXBElement<ArrayOfPassenger> sourcePassengers = source.getPassenger();
			if (sourcePassengers != null && 
					sourcePassengers.getValue() != null &&
					sourcePassengers.getValue().getPassenger() != null &&
					!sourcePassengers.getValue().getPassenger().isEmpty()) {
				List<MmbPassengerData> passengersList = new ArrayList<MmbPassengerData>();
				for(Passenger sourcePassenger : sourcePassengers.getValue().getPassenger()) {
					passengersList.add(passengerConverter.convert(sourcePassenger));
				}
				destination.setPassengersList(passengersList);
			}

			JAXBElement<ArrayOfRoute> sourceRoutesList = source.getRoutes();
			if (sourceRoutesList != null && 
					sourceRoutesList.getValue() != null &&
					sourceRoutesList.getValue().getRoute() != null &&
					!sourceRoutesList.getValue().getRoute().isEmpty()) {
				List<MmbRouteData> routesList = new ArrayList<MmbRouteData>();
				for (Route sourceRoute : sourceRoutesList.getValue().getRoute()) {
					routesList.add(routeConverter.convert(sourceRoute));
				}
				destination.setRoutesList(routesList);
			}
		}

		return destination;
	}

}
