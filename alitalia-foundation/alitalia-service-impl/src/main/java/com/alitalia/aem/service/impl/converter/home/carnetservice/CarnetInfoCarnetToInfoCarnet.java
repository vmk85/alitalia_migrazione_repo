package com.alitalia.aem.service.impl.converter.home.carnetservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.carnet.CarnetInfoCarnet;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.carnet.carnetservice.xsd5.InfoCarnet;
import com.alitalia.aem.ws.carnet.carnetservice.xsd5.ObjectFactory;


@Component(immediate=true, metatype=false)
@Service(value=CarnetInfoCarnetToInfoCarnet.class)
public class CarnetInfoCarnetToInfoCarnet implements Converter<CarnetInfoCarnet, InfoCarnet> {

	@Override
	public InfoCarnet convert(CarnetInfoCarnet source) {
		InfoCarnet destination = null;

		if (source != null) {

			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createInfoCarnet();
			destination.setBuyerEmail(objectFactory.createInfoCarnetBuyerEmail(source.getBuyerEmail()));
			destination.setBuyerLastname(objectFactory.createInfoCarnetBuyerLastname(source.getBuyerLastname()));
			destination.setBuyerName(objectFactory.createInfoCarnetBuyerName(source.getBuyerName()));
			destination.setCarnetCode(objectFactory.createInfoCarnetCarnetCode(source.getCarnetCode()));
			if (source.getCreationDate() != null)
				destination.setCreationDate(XsdConvertUtils.toXMLGregorianCalendar(source.getCreationDate()));
			destination.setECouponCode(objectFactory.createInfoCarnetECouponCode(source.geteCouponCode()));
			destination.setEMDCode(objectFactory.createInfoCarnetEMDCode(source.getEmdCode()));
			if (source.getExpiryDate() != null)
				destination.setExpiryDate(XsdConvertUtils.toXMLGregorianCalendar(source.getExpiryDate()));
			destination.setId(source.getId());
			destination.setPassword(objectFactory.createInfoCarnetPassword(source.getPassword()));
			destination.setPurchasedCarnetCode(objectFactory.createInfoCarnetPurchasedCarnetCode(source.getPurchasedCarnetCode()));
			destination.setResidualFare(source.getResidualFare());
			destination.setResidualRoutes(source.getResidualRoutes());
			destination.setTotalFare(source.getTotalFare());
			destination.setTotalRoutes(source.getTotalRoutes());
			destination.setUsedFare(source.getUsedFare());
			destination.setUsedRoutes(source.getUsedRoutes());
			
		}
		return destination;
	}
}