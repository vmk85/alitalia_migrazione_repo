package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;






import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareCalculationData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ArrayOfresultTicketingDetailSolutionPricingFareCalculationBoxedTax;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingFareCalculation;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd5.ArrayOfstring;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareCalculationDataToRTDSPricingFareCalculation.class)
public class RTDSPricingFareCalculationDataToRTDSPricingFareCalculation implements
		Converter<ResultTicketingDetailSolutionPricingFareCalculationData, 
					ResultTicketingDetailSolutionPricingFareCalculation> {

	@Reference
	private RTDSPricingFareCalculationBoxedTaxDataToRTDSPricingFareCalculationBoxedTax boxedTaxFieldConverter;

	@Override
	public ResultTicketingDetailSolutionPricingFareCalculation convert(
			ResultTicketingDetailSolutionPricingFareCalculationData source) {
		ResultTicketingDetailSolutionPricingFareCalculation destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingFareCalculation();

			ArrayOfresultTicketingDetailSolutionPricingFareCalculationBoxedTax boxedTaxField = null;
			if (source.getBoxedTaxField() != null &&
					!source.getBoxedTaxField().isEmpty()) {
				boxedTaxField = objectFactory.createArrayOfresultTicketingDetailSolutionPricingFareCalculationBoxedTax();
				for(ResultTicketingDetailSolutionPricingFareCalculationBoxedTaxData sourceBoxedTaxFieldElem : source.getBoxedTaxField())
					boxedTaxField.getResultTicketingDetailSolutionPricingFareCalculationBoxedTax().add(
							boxedTaxFieldConverter.convert(sourceBoxedTaxFieldElem));
			}
			destination.setBoxedTaxField(boxedTaxField);

			ArrayOfstring lineField = null;
			if (source.getLineField() != null &&
					!source.getLineField().isEmpty()) {
				com.alitalia.aem.ws.booking.paymentservice.xsd5.ObjectFactory objectFactory5 =
						new com.alitalia.aem.ws.booking.paymentservice.xsd5.ObjectFactory();
				lineField = objectFactory5.createArrayOfstring();
				for (String sourceLineFieldElem : source.getLineField())
					lineField.getString().add(sourceLineFieldElem);
			}
			destination.setLineField(lineField);
		}

		return destination;
	}

}
