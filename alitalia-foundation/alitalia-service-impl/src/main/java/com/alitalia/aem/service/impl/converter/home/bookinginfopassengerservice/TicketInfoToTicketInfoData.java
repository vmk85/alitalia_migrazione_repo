package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.TicketInfoData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.TicketInfo;

@Component(immediate=true, metatype=false)
@Service(value=TicketInfoToTicketInfoData.class)
public class TicketInfoToTicketInfoData 
		implements Converter<TicketInfo, TicketInfoData> {
	
	@Override
	public TicketInfoData convert(TicketInfo source) {
		
		TicketInfoData destination = null;
		
		if (source != null) {
			
			destination = new TicketInfoData();
			
			if (source.getSegmentId() != null) {
				destination.setSegmentId(
						source.getSegmentId().getValue());
			}
			
			if (source.getTicketNumber() != null) {
				destination.setTicketNumber(
						source.getTicketNumber().getValue());
			}
			
		}
		
		return destination;
	}

}
