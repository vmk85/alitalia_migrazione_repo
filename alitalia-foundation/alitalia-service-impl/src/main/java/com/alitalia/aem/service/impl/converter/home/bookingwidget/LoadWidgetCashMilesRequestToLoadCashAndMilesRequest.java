package com.alitalia.aem.service.impl.converter.home.bookingwidget;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.LoadWidgetCashMilesRequest;
import com.alitalia.aem.ws.booking.widgetservice.xsd1.LoadCashAndMilesRequest;
import com.alitalia.aem.ws.booking.widgetservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.booking.widgetservice.xsd2.WidgetType;

@Component(immediate=true, metatype=false)
@Service(value=LoadWidgetCashMilesRequestToLoadCashAndMilesRequest.class)
public class LoadWidgetCashMilesRequestToLoadCashAndMilesRequest implements
		Converter<LoadWidgetCashMilesRequest, LoadCashAndMilesRequest> {

	@Override
	public LoadCashAndMilesRequest convert(LoadWidgetCashMilesRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		LoadCashAndMilesRequest destination = objectFactory.createLoadCashAndMilesRequest();

		destination.setLanguageCode(objectFactory.createLoadCashAndMilesRequestLanguageCode(source.getLanguage()));
		destination.setMMCode(objectFactory.createLoadCashAndMilesRequestMMCode(source.getMmCode()));
		destination.setMMPin(objectFactory.createLoadCashAndMilesRequestMMPin(source.getMmPin()));
		destination.setSite(objectFactory.createLoadWidgetRequestSite(source.getSite()));
		destination.setType(WidgetType.CASH_AND_MILES);
		return destination;
	}

}
