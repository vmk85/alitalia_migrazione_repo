package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMCreditCardData;
import com.alitalia.aem.common.data.home.enumerations.MMCardTypeEnum;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CreditCard;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd3.ArrayOfanyType;

@Component(immediate = true, metatype = false)
@Service(value = CreditCardsToMMCreditCardsData.class)
public class CreditCardsToMMCreditCardsData implements Converter<ArrayOfanyType, List<MMCreditCardData>> {

	private static Logger logger = LoggerFactory.getLogger(CreditCardsToMMCreditCardsData.class);
	
	@Reference
	private MyPersonalareaMillemigliaServiceJAXBContextFactory jaxbContextFactory;
	
	@Override
	public List<MMCreditCardData> convert(ArrayOfanyType source) {
		
		if (source == null) {
			return null;
		}
		
		List<MMCreditCardData> mmCards = new ArrayList<MMCreditCardData>();

		for (Object object: source.getAnyType()) {
			try {
				Unmarshaller unmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
				
				JAXBElement<CreditCard> jaxbElement = (JAXBElement<CreditCard>) unmarshaller.unmarshal((Node) object);
				CreditCard creditCard = jaxbElement.getValue();
				
				MMCreditCardData mmCreditCardData = new MMCreditCardData();
				mmCreditCardData.setCardType(MMCardTypeEnum.fromValue(creditCard.getCardType().value()));
				mmCreditCardData.setCircuitCode(creditCard.getCircuitCode().getValue());
				mmCreditCardData.setNumber(creditCard.getNumber().getValue());
				mmCreditCardData.setToken(creditCard.getToken().getValue());
				
				if (creditCard.getExpireDate() != null)
					mmCreditCardData.setExpireDate(XsdConvertUtils.parseCalendar(creditCard.getExpireDate()));
				
				mmCards.add(mmCreditCardData);
			
			} catch (JAXBException e) {
				logger.error("Error while creating JAXBConext/Unmarshller for List<MMCreditCardData>: {}", e);
			}
		}
		
		return mmCards;
	}
}