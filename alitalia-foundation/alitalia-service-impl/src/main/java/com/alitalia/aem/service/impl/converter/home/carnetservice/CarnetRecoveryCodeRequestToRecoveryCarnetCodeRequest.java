/**
 * 
 */
package com.alitalia.aem.service.impl.converter.home.carnetservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.CarnetRecoveryCodeRequest;
import com.alitalia.aem.ws.carnet.carnetservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.carnet.carnetservice.xsd1.RecoveryCarnetCodeRequest;



@Component(immediate=true, metatype=false)
@Service(value=CarnetRecoveryCodeRequestToRecoveryCarnetCodeRequest.class)
public class CarnetRecoveryCodeRequestToRecoveryCarnetCodeRequest implements Converter<CarnetRecoveryCodeRequest, RecoveryCarnetCodeRequest> {

	@Override
	public RecoveryCarnetCodeRequest convert(CarnetRecoveryCodeRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		RecoveryCarnetCodeRequest destination = null;

		if (source != null) {
			
			destination = objectFactory.createRecoveryCarnetCodeRequest();
			destination.setCarnetCode(objectFactory.createRecoveryCarnetCodeRequestCarnetCode(source.getCarnetCode()));
			destination.setEmail(objectFactory.createRecoveryCarnetCodeRequestEmail(source.getEmail()));
			destination.setLanguageCode(objectFactory.createRecoveryCarnetCodeRequestLanguageCode(source.getLanguageCode()));
			destination.setMarketCode(objectFactory.createRecoveryCarnetCodeRequestMarketCode(source.getMarketCode()));

		}
		return destination;
	}
}
