package com.alitalia.aem.service.impl.crmdatarest.service;

import com.alitalia.aem.common.messages.crmdatarest.*;
import com.alitalia.aem.common.logging.CrmData.CrmServiceLog;
import com.google.gson.Gson;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.service.api.home.IServiceCRMData;

import com.alitalia.aem.service.impl.crmdatarest.client.CRMDataServiceClient;
import com.alitalia.aem.service.impl.crmdatarest.utils.CRMDataMessageUnmarshal;

@Service
@Component(immediate = true, metatype = false)
public class CRMDataServiceImpl implements IServiceCRMData {

    private static final Logger logger = LoggerFactory.getLogger(CRMDataServiceImpl.class);

    private Gson gson = new Gson();

    CrmServiceLog crmServiceLog = new CrmServiceLog();

    @Reference
    private CRMDataServiceClient crmdataserviceclient;

    @Override
    public SetCRMDataNewsLetterResponse setCRMDataNewsLetter(SetCRMDataNewsLetterRequest request) {
        crmServiceLog.info("Executing method setCRMDataNewsLetter. The request for [" +request.getEmail() + "], Canale [" + request.getCanale() + "], SID: [" + request.getSid() + "]");
        crmServiceLog.debug("Executing method setCRMDataNewsLetter. The request is [" + gson.toJson(request) + "]");
        SetCRMDataNewsLetterResponse response = null;
        try {
            response = new SetCRMDataNewsLetterResponse();
            String url = "/setazcrmdatainfonewsletter";
            String responseClient = crmdataserviceclient.getData(request, url);
            crmServiceLog.info("Response method setazcrmdatainfonewsletter: The response is [" + responseClient + "] for mail + " + request.getEmail());
            crmServiceLog.debug("Response method setazcrmdatainfonewsletter: The response is [" + responseClient + "]");
            response = CRMDataMessageUnmarshal.setCRMDataUnmarshal(responseClient);
            response.setTid(request.getTid());
            response.setSid(request.getSid());
        } catch (Exception e) {
            crmServiceLog.error(e.getMessage());
        }
        return response;
    }

    @Override
    public SetCrmDataInfoResponse setCRMDataInfo(SetCrmDataInfoRequest request) {
        crmServiceLog.info("Executing method setCRMDataInfo. The request for [" +request.getEmail() + "], Canale: " + request.getCanale() + "], SID: [" + request.getSid() + "]");
        crmServiceLog.debug("Executing method setCRMDataInfo MyAlitalia User Logged. The request is [" + gson.toJson(request) + "]");
        SetCrmDataInfoResponse response = null;
        try {
            response = new SetCrmDataInfoResponse();
            String url = "/setazcrmdatainfo";
            String responseClient = crmdataserviceclient.getData(request, url);
            response = CRMDataMessageUnmarshal.setCRMDataInfoUnmarshal(responseClient);
            crmServiceLog.info("Response method setazcrmdatainfo: The response is [" + responseClient + "] for mail + " + request.getEmail());
            crmServiceLog.debug("Response method setazcrmdatainfo: The response is [" + responseClient + "]");
            response.setTid(request.getTid());
            response.setSid(request.getSid());
        } catch (Exception e) {
            crmServiceLog.error(e.getMessage());
        }
        return response;
    }

    @Override
    public GetCrmDataInfoResponse getCRMDataInfo(GetCrmDataInfoRequest request) {
        crmServiceLog.info("Executing method getCRMDataInfo. The request for [" +request.getIdMyAlitalia() + "] , SID: [" + request.getSid() + "]");
        crmServiceLog.debug("Executing method getCRMDataInfo MyAlitalia User Logged. The request is [" + gson.toJson(request) + "]");
        GetCrmDataInfoResponse response = null;
        try {
            response = new GetCrmDataInfoResponse();
            String url = "/getazcrmdatainfo";
            String responseClient = crmdataserviceclient.getData(request, url);
            response = CRMDataMessageUnmarshal.getCRMDataInfoUnmarshal(responseClient);
            crmServiceLog.info("Response method getazcrmdatainfo: The response success for mail + " + request.getIdMyAlitalia());
            crmServiceLog.debug("Response method getazcrmdatainfo: The response is [" + responseClient + "]");
            response.setTid(request.getTid());
            response.setSid(request.getSid());
        } catch (Exception e) {
            crmServiceLog.error(e.getMessage());
        }
        return response;
    }
}

