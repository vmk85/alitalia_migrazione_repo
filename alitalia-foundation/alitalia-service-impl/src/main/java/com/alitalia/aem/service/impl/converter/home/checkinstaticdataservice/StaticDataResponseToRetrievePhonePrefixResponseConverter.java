package com.alitalia.aem.service.impl.converter.home.checkinstaticdataservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PhonePrefixData;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixResponse;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.StaticDataResponseOfInternationalPhonePrefixUGS9ND5Y;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd2.InternationalPhonePrefix;


@Component(immediate=true, metatype=false)
@Service(value=StaticDataResponseToRetrievePhonePrefixResponseConverter.class)
public class StaticDataResponseToRetrievePhonePrefixResponseConverter implements Converter<StaticDataResponseOfInternationalPhonePrefixUGS9ND5Y, RetrievePhonePrefixResponse> {

	@Reference
	private PhonePrefixToPhonePrefixData phonePrefixConverter;

	@Override
	public RetrievePhonePrefixResponse convert(StaticDataResponseOfInternationalPhonePrefixUGS9ND5Y source) {
		RetrievePhonePrefixResponse response = new RetrievePhonePrefixResponse();

		List<PhonePrefixData> phonePrefixsData = new ArrayList<>();
		List<InternationalPhonePrefix> phonePrefixs = source.getEntities().getValue().getInternationalPhonePrefix();
		for(InternationalPhonePrefix phonePrefix: phonePrefixs){
			PhonePrefixData phonePrefixData = phonePrefixConverter.convert(phonePrefix);
			phonePrefixsData.add(phonePrefixData);
		}
		response.setPhonePrefix(phonePrefixsData);

		return response;			

	}
}