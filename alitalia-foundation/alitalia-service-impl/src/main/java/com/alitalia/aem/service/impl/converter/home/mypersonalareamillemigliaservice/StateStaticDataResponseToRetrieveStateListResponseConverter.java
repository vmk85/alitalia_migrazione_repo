package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.StateData;
import com.alitalia.aem.common.messages.home.RetrieveStateListResponse;
import com.alitalia.aem.ws.mypersonalarea.staticdataservice.xsd1.StateStaticDataResponse;
import com.alitalia.aem.ws.mypersonalarea.staticdataservice.xsd2.ArrayOfState;
import com.alitalia.aem.ws.mypersonalarea.staticdataservice.xsd2.State;

@Component(immediate = true, metatype = false)
@Service(value = StateStaticDataResponseToRetrieveStateListResponseConverter.class)
public class StateStaticDataResponseToRetrieveStateListResponseConverter implements Converter<StateStaticDataResponse, RetrieveStateListResponse> {

	@Override
	public RetrieveStateListResponse convert(StateStaticDataResponse source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		
		RetrieveStateListResponse destination = new RetrieveStateListResponse();

		JAXBElement<ArrayOfState> jaxbArrayOfState = source.getEntities();
		ArrayOfState arrayOfState = jaxbArrayOfState.getValue();
		List<State> states = arrayOfState.getState();
		
		List<StateData> statesData = new ArrayList<>();
		for(State state: states){
			StateData stateData = new StateData();
			stateData.setStateCode(state.getStateCode().getValue());
			stateData.setStateDescription(state.getStateDescription().getValue());
			
			statesData.add(stateData);
		}
		
		destination.setStates(statesData);
		
		return destination;
	}
}