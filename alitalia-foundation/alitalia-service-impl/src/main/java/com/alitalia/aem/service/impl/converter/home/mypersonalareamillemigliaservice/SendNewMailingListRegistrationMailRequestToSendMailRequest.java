package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.SendNewMailingListRegistrationMailRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SendMailRequest;

@Component(immediate = true, metatype = false)
@Service(value = SendNewMailingListRegistrationMailRequestToSendMailRequest.class)
public class SendNewMailingListRegistrationMailRequestToSendMailRequest 
		implements Converter<SendNewMailingListRegistrationMailRequest, SendMailRequest> {
	
	@Reference
	private MMCustomerProfileDataToCustomerProfile mmCustomerProfileDataConverter;
	
	@Override
	public SendMailRequest convert(SendNewMailingListRegistrationMailRequest source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		
		ObjectFactory objectFactory = new ObjectFactory();
		
		SendMailRequest destination = objectFactory.createSendMailRequest();
		JAXBElement<String> jaxbEmailBody = objectFactory.createSendMailRequestMailBody(source.getMailBody());
		JAXBElement<String> jaxbEmailSubject = objectFactory.createSendMailRequestMailSubject(source.getMailSubject());
		destination.setMailBody(jaxbEmailBody);
		destination.setMailSubject(jaxbEmailSubject);
		
		destination.setCustomer(
				objectFactory.createCustomerRequestCustomer(
						mmCustomerProfileDataConverter.convert(source.getCustomer())));
		
		return destination;
	}
}