package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentLegScheduledServicesData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionSliceSegmentLegScheduledServices;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd5.ArrayOfstring;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentLegScheduledServicesDataToRTDSSliceSegmentLegScheduledServices.class)
public class RTDSSliceSegmentLegScheduledServicesDataToRTDSSliceSegmentLegScheduledServices implements
		Converter<ResultTicketingDetailSolutionSliceSegmentLegScheduledServicesData, 
					ResultTicketingDetailSolutionSliceSegmentLegScheduledServices> {

	@Override
	public ResultTicketingDetailSolutionSliceSegmentLegScheduledServices convert(
			ResultTicketingDetailSolutionSliceSegmentLegScheduledServicesData source) {
		ResultTicketingDetailSolutionSliceSegmentLegScheduledServices destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			com.alitalia.aem.ws.booking.infopassengerservice.xsd5.ObjectFactory objectFactory5 =
					new com.alitalia.aem.ws.booking.infopassengerservice.xsd5.ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionSliceSegmentLegScheduledServices();

			destination.setCabinField(source.getCabinField());

			ArrayOfstring mealField = null;
			if (source.getMealField() != null &&
					!source.getMealField().isEmpty()) {
				mealField = objectFactory5.createArrayOfstring();
				for(String sourceMealFieldElem : source.getMealField())
					mealField.getString().add(sourceMealFieldElem);
			}
			destination.setMealField(mealField);
		}

		return destination;
	}

}
