package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.home.MailinglistRegistrationResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.MailingListRegistrationResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;

@Component(immediate = true, metatype = false)
@Service(value = MailingListRegistrationResponseToMailinglistRegistrationResponse.class)
public class MailingListRegistrationResponseToMailinglistRegistrationResponse implements Converter<MailingListRegistrationResponse, MailinglistRegistrationResponse> {

	@Reference
	private CustomerProfileToMMCustomerProfileData customerProfileConverter;
	
	@Override
	public MailinglistRegistrationResponse convert(MailingListRegistrationResponse source) {
		if(source == null) throw new IllegalArgumentException("Response is null.");
		MailinglistRegistrationResponse destination = new MailinglistRegistrationResponse(); 
		
		JAXBElement<CustomerProfile> jaxbElement = source.getCustomer();
		//Commentato secondo le indicazioni di Cafiero
		//CustomerProfile customerProfile = jaxbElement.getValue();
		//MMCustomerProfileData mmCustomerProfile = customerProfileConverter.convert(customerProfile);
		
		//destination.setCustomerProfile(mmCustomerProfile);
		destination.setRegistrationIsSuccessful(true);
		
		return destination;
	}
}