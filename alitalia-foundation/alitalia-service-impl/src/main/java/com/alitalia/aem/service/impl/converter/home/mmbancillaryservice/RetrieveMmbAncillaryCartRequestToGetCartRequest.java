package com.alitalia.aem.service.impl.converter.home.mmbancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCartRequest;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.GetCartRequest;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd1.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=RetrieveMmbAncillaryCartRequestToGetCartRequest.class)
public class RetrieveMmbAncillaryCartRequestToGetCartRequest implements Converter<RetrieveMmbAncillaryCartRequest, GetCartRequest> {

	@Reference
	private MmbRequestBaseInfoDataToRequestBaseInfo mmbRequestBaseInfoDataConverter;

	@Override
	public GetCartRequest convert(RetrieveMmbAncillaryCartRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		GetCartRequest destination = null;

		if (source != null) {
			destination = objectFactory.createGetCartRequest();

			destination.setCartId(source.getCartId());
			destination.setClient(source.getClient());
			destination.setMachineName(objectFactory.createAddToCartRequestMachineName(source.getMachineName()));
			destination.setSessionId(objectFactory.createAddToCartRequestSessionId(source.getSessionId()));
			destination.setX003CPNRX003EKBackingField(source.getPnr());

			destination.setX003CInfoRequestX003EKBackingField(
					mmbRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
		}

		return destination;
	}

}
