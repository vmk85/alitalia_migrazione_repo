package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AwardBookingSearchData;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.common.messages.home.enumeration.SearchExecuteResponseType;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd1.SearchRequest;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd2.GateWayAwardSearch;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd2.SearchResponseType;

@Component(immediate=true, metatype=false)
@Service(value=SearchFlightSolutionRequestToGatewayAwardTaxSearchRequest.class)
public class SearchFlightSolutionRequestToGatewayAwardTaxSearchRequest
		implements Converter<SearchFlightSolutionRequest, SearchRequest> {

	@Reference
	private AwardBookingSearchDataToGateWayAwardSearch bookingSearchDataConverter;

	@Override
	public SearchRequest convert(SearchFlightSolutionRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		SearchRequest destination = null;

		if (source != null) {
			destination = objectFactory.createSearchRequest();

			AwardBookingSearchData awardBookingSearchData = (AwardBookingSearchData) source.getFilter();
			GateWayAwardSearch filter = bookingSearchDataConverter.convert(awardBookingSearchData);
			destination.setFilter(objectFactory.createSearchRequestFilter(filter));

			destination.setLanguageCode(objectFactory.createSearchRequestLanguageCode(source.getLanguageCode()));
			destination.setMarketCode(objectFactory.createSearchRequestMarketCode(source.getMarket()));
			SearchExecuteResponseType sourceResponseType = source.getResponseType();
			if (sourceResponseType != null)
				destination.setResponseType(SearchResponseType.fromValue(sourceResponseType.value()));
		}

		return destination;
	}

}
