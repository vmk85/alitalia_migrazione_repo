package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingTaxData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionPricingTax;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingTaxToRTDSPricingTaxData.class)
public class RTDSPricingTaxToRTDSPricingTaxData implements
		Converter<ResultTicketingDetailSolutionPricingTax, 
					ResultTicketingDetailSolutionPricingTaxData> {

	@Reference
	private RTDSPricingTaxDisplayPriceToRTDSPricingTaxDisplayPriceData displayPriceFieldConverter;

	@Reference
	private RTDSPricingTaxSalePriceToRTDPricingTaxSalePriceData salePriceFieldConverter;

	@Override
	public ResultTicketingDetailSolutionPricingTaxData convert(ResultTicketingDetailSolutionPricingTax source) {
		ResultTicketingDetailSolutionPricingTaxData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingTaxData();

			destination.setCodeField(source.getCodeField());
			destination.setIdField(source.getIdField());
			destination.setStatusField(source.getStatusField());

			destination.setDisplayPriceField(
					displayPriceFieldConverter.convert(source.getDisplayPriceField()));

			destination.setSalePriceField(salePriceFieldConverter.convert(source.getSalePriceField()));
		}

		return destination;
	}

}
