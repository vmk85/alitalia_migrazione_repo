package com.alitalia.aem.service.impl.converter.home.geospatialservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveGeoCountryResponse;
import com.alitalia.aem.ws.geospatial.service.xsd1.GetCountryByIPResponse;

@Component(immediate = true, metatype = false)
@Service(value = GetCountryByIPResponseToRetrieveGeoCountryResponse.class)
public class GetCountryByIPResponseToRetrieveGeoCountryResponse implements Converter<GetCountryByIPResponse, RetrieveGeoCountryResponse> {

	@Override
	public RetrieveGeoCountryResponse convert(GetCountryByIPResponse source) {
		
		RetrieveGeoCountryResponse response = new RetrieveGeoCountryResponse();
		
		String country = source.getCountry().getValue();
		response.setCountry(country);
		
		return response;
	}
}