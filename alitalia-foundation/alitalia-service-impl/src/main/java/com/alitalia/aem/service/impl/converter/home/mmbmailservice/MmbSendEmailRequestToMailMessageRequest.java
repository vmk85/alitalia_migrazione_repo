package com.alitalia.aem.service.impl.converter.home.mmbmailservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.MmbSendEmailRequest;
import com.alitalia.aem.ws.mmb.mailservice.xsd1.MailMessageRequest;
import com.alitalia.aem.ws.mmb.mailservice.xsd1.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=MmbSendEmailRequestToMailMessageRequest.class)
public class MmbSendEmailRequestToMailMessageRequest implements Converter<MmbSendEmailRequest, MailMessageRequest> {

	@Reference
	private MmbRequestBaseInfoDataToRequestBaseInfo mmbRequestBaseInfoDataConverter;

	@Override
	public MailMessageRequest convert(MmbSendEmailRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		MailMessageRequest destination = null;

		if (source != null) {
			destination = objectFactory.createMailMessageRequest();

			destination.setClient(source.getClient());
			destination.setFrom(objectFactory.createMailMessageRequestFrom(source.getSender()));
			destination.setIsBodyHtml(source.isHtmlBody());
			destination.setMessageText(objectFactory.createMailMessageRequestMessageText(source.getMailBody()));
			destination.setSubject(objectFactory.createMailMessageRequestSubject(source.getMailSubject()));
			destination.setTo(objectFactory.createMailMessageRequestTo(source.getRecipient()));
			destination.setX003CPNRX003EKBackingField(source.getPnr());

			destination.setX003CInfoRequestX003EKBackingField(
					mmbRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
		}

		return destination;
	}

}
