package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.AwardBookingSearchData;
import com.alitalia.aem.common.data.home.AwardSearchData;
import com.alitalia.aem.common.data.home.enumerations.GatewayTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.SearchTypeEnum;
import com.alitalia.aem.common.messages.home.AwardValueRequest;
import com.alitalia.aem.common.messages.home.AwardValueResponse;
import com.alitalia.aem.common.messages.home.SearchBookingSolutionResponse;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionResponse;
import com.alitalia.aem.service.api.home.BookingAwardSearchService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice.AwardValueRequestToAwardMilesPriceRequest;
import com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice.SearchFlightSolutionRequestToAwardBookingSearchRequest;
import com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice.SearchFlightSolutionRequestToAwardCalendarSearchRequest;
import com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice.SearchFlightSolutionRequestToAwardFlightSearchRequest;
import com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice.SearchFlightSolutionRequestToAwardSearchRequest;
import com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice.SearchFlightSolutionRequestToGatewayAwardSellupSearchRequest;
import com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice.SearchFlightSolutionRequestToGatewayAwardTaxSearchRequest;
import com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice.SearchResponseToAvailableFlightsData;
import com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice.SearchResponseToRoutesData;
import com.alitalia.aem.ws.bookingaward.searchservice.AwardSearchServiceClient;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd1.AwardMilesPriceRequest;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd1.AwardMilesPriceResponse;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd1.SearchRequest;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd1.SearchResponse;

@Service
@Component(immediate=true, metatype=false)
public class SimpleBookingAwardSearchService implements BookingAwardSearchService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleBookingAwardSearchService.class);
	
	@Reference
	private AwardSearchServiceClient awardSearchServiceClient;

	@Reference
	private SearchFlightSolutionRequestToAwardSearchRequest awardSearchConverter;

	@Reference
	private SearchFlightSolutionRequestToAwardCalendarSearchRequest awardCalendarSearchConverter;

	@Reference
	private SearchFlightSolutionRequestToAwardFlightSearchRequest awardFlightSearchConverter;

	@Reference
	private SearchFlightSolutionRequestToAwardBookingSearchRequest awardBookingSearchConverter;

	@Reference
	private SearchResponseToAvailableFlightsData searchResponseCoverter;

	@Reference
	private SearchFlightSolutionRequestToGatewayAwardSellupSearchRequest gatewaySellupSearchConverter;

	@Reference
	private SearchFlightSolutionRequestToGatewayAwardTaxSearchRequest gatewayTaxSearchConverter;

	@Reference
	private SearchResponseToRoutesData searchResponseToRoutesCoverter;

	@Reference
	private AwardValueRequestToAwardMilesPriceRequest awardValueRequestConverter;

	@Override
	public SearchFlightSolutionResponse executeAwardSearch(SearchFlightSolutionRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method executeAwardSearch. The request is {}", request);
		}
		
		if (! (request.getFilter() instanceof AwardSearchData) &&
				(((AwardSearchData) request.getFilter()).getType() != SearchTypeEnum.AWARD_SEARCH)) {
			logger.error("Filter data inside request is not from the correct class: " + request.getFilter().getClass());
			throw new IllegalArgumentException("Filter data inside request is not from the correct class");
		}

		try {
			SearchFlightSolutionResponse response = new SearchFlightSolutionResponse();
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			
			SearchRequest searchRequest = awardSearchConverter.convert(request);
			SearchResponse searchResponse = awardSearchServiceClient.execute(searchRequest);
			response.setAvailableFlights(searchResponseCoverter.convert(searchResponse));
			
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method executeAwardSearch(). The response is {}", response);
			}
			

			return response;
		}  catch (Exception e) {
			logger.error("Exception while executing method executeAwardSearch(): {}", e);
			throw new AlitaliaServiceException("Exception executing executeAwardSearch: Sid [" + request.getSid() + "]" , e);
		}
	}

	@Override
	public SearchFlightSolutionResponse executeAwardCalendarSearch(SearchFlightSolutionRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method executeAwardCalendarSearch. The request is {}", request);
		}
		
		if (! (request.getFilter() instanceof AwardSearchData) &&
				(((AwardSearchData) request.getFilter()).getType() != SearchTypeEnum.AWARD_CALENDAR_SEARCH)) {
			logger.error("Filter data inside request is not from the correct class: " + request.getFilter().getClass());
			throw new IllegalArgumentException("Filter data inside request is not from the correct class");
		}

		try {
			SearchFlightSolutionResponse response = new SearchFlightSolutionResponse();
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			
			SearchRequest searchRequest = awardCalendarSearchConverter.convert(request);
			SearchResponse searchResponse = awardSearchServiceClient.execute(searchRequest);
			response.setAvailableFlights(searchResponseCoverter.convert(searchResponse));
			
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method executeAwardCalendarSearch(). The response is {}", response);
			}
			
			return response;

		}  catch (Exception e) {
			logger.error("Exception while executing method executeAwardCalendarSearch(): {}", e);
			throw new AlitaliaServiceException("Exception executing executeAwardCalendarSearch: Sid [" + request.getSid() + "]" , e);
		}
	}

	@Override
	public SearchFlightSolutionResponse executeAwardFlightSearch(SearchFlightSolutionRequest request) {
		
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method executeAwardFlightSearch. The request is {}", request);
		}
		
		if (! (request.getFilter() instanceof AwardSearchData) &&
				(((AwardSearchData) request.getFilter()).getType() != SearchTypeEnum.AWARD_FLIGHT_SEARCH)) {
			logger.error("Filter data inside request is not from the correct class: " + request.getFilter().getClass());
			throw new IllegalArgumentException("Filter data inside request is not from the correct class");
		}

		try {
			SearchFlightSolutionResponse response = new SearchFlightSolutionResponse();
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			
			SearchRequest searchRequest = awardFlightSearchConverter.convert(request);
			SearchResponse searchResponse = awardSearchServiceClient.execute(searchRequest);
			response.setAvailableFlights(searchResponseCoverter.convert(searchResponse));
			
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method executeAwardFlightSearch(). The response is {}", response);
			}
			

			return response;
		}  catch (Exception e) {
			logger.error("Exception while executing method executeAwardFlightSearch(): {}", e);
			throw new AlitaliaServiceException("Exception executing executeAwardFlightSearch: Sid [" + request.getSid() + "]" , e);
		}
	}

	@Override
	public SearchBookingSolutionResponse executeAwardSellupSearch(SearchFlightSolutionRequest request) {
		
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method executeAwardSellupSearch. The request is {}", request);
		}
		
		if (! (request.getFilter() instanceof AwardBookingSearchData) &&
				(((AwardBookingSearchData) request.getFilter()).getType() != GatewayTypeEnum.AWARD_SELLUP)) {
			logger.error("Filter data inside request is not from the correct class: " + request.getFilter().getClass());
			throw new IllegalArgumentException("Filter data inside request is not from the correct class");
		}

		try {
			SearchBookingSolutionResponse response = new SearchBookingSolutionResponse();
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			
			SearchRequest searchRequest = awardBookingSearchConverter.convert(request);
			SearchResponse searchResponse = awardSearchServiceClient.execute(searchRequest);
			response.setRoutesData(searchResponseToRoutesCoverter.convert(searchResponse));
			
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method executeAwardSellupSearch(). The response is {}", response);
			}
			

			return response;

		}  catch (Exception e) {
			logger.error("Exception while executing method executeAwardSellupSearch(): {}", e);
			throw new AlitaliaServiceException("Exception executing executeAwardSellupSearch: Sid [" + request.getSid() + "]" , e);
		}
	}

	@Override
	public SearchFlightSolutionResponse executeAwardTaxSearch(SearchFlightSolutionRequest request) {
		
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method executeAwardFlightSearch. The request is {}", request);
		}
		
		if (! (request.getFilter() instanceof AwardBookingSearchData) &&
				(((AwardBookingSearchData) request.getFilter()).getType() != GatewayTypeEnum.AWARD_TAXES)) {
			logger.error("Filter data inside request is not from the correct class: " + request.getFilter().getClass());
			throw new IllegalArgumentException("Filter data inside request is not from the correct class");
		}

		try {
			
			SearchFlightSolutionResponse response = new SearchFlightSolutionResponse();
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			
			SearchRequest searchRequest = awardBookingSearchConverter.convert(request);
			SearchResponse searchResponse = awardSearchServiceClient.execute(searchRequest);
			response.setAvailableFlights(searchResponseCoverter.convert(searchResponse));
			
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method executeAwardTaxSearchSearch(). The response is {}", response);
			}
			

			return response;

		}  catch (Exception e) {
			logger.error("Exception while executing method executeAwardTaxSearchSearch(): {}", e);
			throw new AlitaliaServiceException("Exception executing executeAwardTaxSearchSearch: Sid [" + request.getSid() + "]" , e);
		}
	}

	@Override
	public AwardValueResponse retrieveAwardValue(AwardValueRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method executeAwardFlightSearch. The request is {}", request);
		}
		
		if (request == null) {
			throw new IllegalArgumentException("Request is null");
		}

		try {
			
			AwardValueResponse response = new AwardValueResponse();
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			
			AwardMilesPriceRequest priceRequest = awardValueRequestConverter.convert(request);
			AwardMilesPriceResponse priceResponse = awardSearchServiceClient.getAwardPointsAsPrice(priceRequest);
			response.setAwardValue(priceResponse.getAwardPrice());
			
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method executeAwardTaxSearchSearch(). The response is {}", response);
			}
			

			return response;

		}  catch (Exception e) {
			logger.error("Exception while executing method executeAwardTaxSearchSearch(): {}", e);
			throw new AlitaliaServiceException("Exception executing executeAwardTaxSearchSearch: Sid [" + request.getSid() + "]" , e);
		}
	}

}
