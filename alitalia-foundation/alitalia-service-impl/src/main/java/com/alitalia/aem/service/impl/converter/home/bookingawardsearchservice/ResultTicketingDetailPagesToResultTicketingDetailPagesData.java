package com.alitalia.aem.service.impl.converter.home.bookingawardsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailPagesData;
import com.alitalia.aem.ws.bookingaward.searchservice.xsd11.ResultTicketingDetailPages;

@Component(immediate=true, metatype=false)
@Service(value=ResultTicketingDetailPagesToResultTicketingDetailPagesData.class)
public class ResultTicketingDetailPagesToResultTicketingDetailPagesData
		implements Converter<ResultTicketingDetailPages, ResultTicketingDetailPagesData> {

	@Override
	public ResultTicketingDetailPagesData convert(ResultTicketingDetailPages source) {
		ResultTicketingDetailPagesData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailPagesData();

			destination.setCountField(source.getCountField());
			destination.setCurrentField(source.getCurrentField());
		}

		return destination;
	}

}
