package com.alitalia.aem.service.impl.converter.home.checkinseatmapservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinSeatMapData;
import com.alitalia.aem.common.messages.home.GetSeatMapCheckinResponse;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd1.GetSeatMapResponse;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd2.ArrayOfSeatMap;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd2.SeatMap;

@Component(immediate=true, metatype=false)
@Service(value=GetSeatMapResponseToGetSeatMapCheckinResponse.class)
public class GetSeatMapResponseToGetSeatMapCheckinResponse implements Converter<GetSeatMapResponse, GetSeatMapCheckinResponse> {
	

	@Reference
	private SeatMapToCheckinSeatMapData seatMapConverter;


	@Override
	public GetSeatMapCheckinResponse convert(GetSeatMapResponse source) {
		GetSeatMapCheckinResponse destination = null;

		if (source != null) {
			destination = new GetSeatMapCheckinResponse();
			
			JAXBElement<ArrayOfSeatMap> sourceSeatMap = source.getSeatMap();
			if(sourceSeatMap != null &&
					sourceSeatMap.getValue() != null &&
					sourceSeatMap.getValue().getSeatMap() != null &&
					!sourceSeatMap.getValue().getSeatMap().isEmpty()){
				List<CheckinSeatMapData> seatMapList = new ArrayList<CheckinSeatMapData>();
				for(SeatMap sourceSeatMapItem : sourceSeatMap.getValue().getSeatMap()){
					seatMapList.add(seatMapConverter.convert(sourceSeatMapItem));
				}
				destination.setSeatMap(seatMapList);
			}
		}

		return destination;
	}

}
