package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowanceData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowance;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingCarryOnBaggageAllowanceFreeBaggageAllowanceToRTDSPricingCarryOnBaggageAllowanceFreeBaggageAllowanceData.class)
public class RTDSPricingCarryOnBaggageAllowanceFreeBaggageAllowanceToRTDSPricingCarryOnBaggageAllowanceFreeBaggageAllowanceData implements
		Converter<ResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowance,
					ResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowanceData> {

	@Override
	public ResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowanceData convert(
			ResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowance source) {
		ResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowanceData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionPricingCarryOnBaggageAllowanceFreeBaggageAllowanceData();

			destination.setBagDescriptorField(null);

			destination.setKilosField(source.getKilosField());
			destination.setKilosFieldSpecified(source.isKilosFieldSpecified());
			destination.setKilosPerPieceField(source.getKilosPerPieceField());
			destination.setKilosPerPieceFieldSpecified(source.isKilosPerPieceFieldSpecified());
			destination.setPiecesField(source.getPiecesField());
			destination.setPiecesFieldSpecified(source.isPiecesFieldSpecified());
			destination.setPoundsField(source.getPoundsField());
			destination.setPoundsFieldSpecified(source.isPoundsFieldSpecified());
		}

		return destination;
	}

}
