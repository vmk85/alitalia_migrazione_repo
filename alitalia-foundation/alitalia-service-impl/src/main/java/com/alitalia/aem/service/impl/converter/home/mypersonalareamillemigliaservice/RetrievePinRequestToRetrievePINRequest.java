package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrievePinRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.RetrievePINRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;

@Component(immediate = true, metatype = false)
@Service(value = RetrievePinRequestToRetrievePINRequest.class)
public class RetrievePinRequestToRetrievePINRequest implements Converter<RetrievePinRequest, RetrievePINRequest> {

	@Reference
	private MMCustomerProfileDataToCustomerProfile mmCustomerProfileDataConverter;
	
	@Override
	public RetrievePINRequest convert(RetrievePinRequest source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		
		ObjectFactory objectFactory = new ObjectFactory();
		
		RetrievePINRequest destination = objectFactory.createRetrievePINRequest();
		CustomerProfile customerProfile = mmCustomerProfileDataConverter.convert(source.getCustomerProfile());
		
		JAXBElement<CustomerProfile> jaxbCustomerProfile = objectFactory.createCustomerRequestCustomer(customerProfile);
		JAXBElement<String> jaxbLastname = objectFactory.createRetrievePINRequestCustomerLastName(source.getCustomerLastName());
		JAXBElement<String> jaxbCustomerNickName = objectFactory.createRetrievePINRequestCustomerNickName(source.getCustomerNickName());
		JAXBElement<String> jaxbCustomerNumber = objectFactory.createRetrievePINRequestCustomerNumber(source.getCustomerNumber());
		JAXBElement<String> jaxbLanguageCode = objectFactory.createRetrievePINRequestLanguageCode(source.getLanguageCode());
		JAXBElement<String> jaxbMarketCode = objectFactory.createRetrievePINRequestMarketCode(source.getMarketCode());
		
		destination.setCustomerLastName(jaxbLastname);
		destination.setCustomerNickName(jaxbCustomerNickName);
		destination.setCustomerNumber(jaxbCustomerNumber);
		destination.setLanguageCode(jaxbLanguageCode);
		destination.setMarketCode(jaxbMarketCode);
		destination.setCustomer(jaxbCustomerProfile);
		
		return destination;
	}
}