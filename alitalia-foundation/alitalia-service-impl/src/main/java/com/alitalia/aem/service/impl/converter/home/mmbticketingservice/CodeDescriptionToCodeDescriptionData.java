package com.alitalia.aem.service.impl.converter.home.mmbticketingservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.CodeDescriptionData;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd4.CodeDescription;

@Component(immediate = true, metatype = false)
@Service(value = CodeDescriptionToCodeDescriptionData.class)
public class CodeDescriptionToCodeDescriptionData implements
		Converter<CodeDescription, CodeDescriptionData> {

	@Override
	public CodeDescriptionData convert(CodeDescription source) {
		CodeDescriptionData destination = null;
		if (source != null) {
			destination = new CodeDescriptionData();
			destination.setCode(source.getCode().getValue());
			destination.setDescription(source.getDescription().getValue());
		}

		return destination;
	}

}
