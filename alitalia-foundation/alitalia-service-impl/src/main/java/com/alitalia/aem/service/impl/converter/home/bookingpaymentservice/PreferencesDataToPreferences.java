package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PreferencesData;
import com.alitalia.aem.common.data.home.SeatPreferencesData;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.Preferences;
import com.alitalia.aem.ws.booking.paymentservice.xsd5.ArrayOfanyType;

@Component(immediate=true, metatype=false)
@Service(value=PreferencesDataToPreferences.class)
public class PreferencesDataToPreferences 
		implements Converter<PreferencesData, Preferences> {
	
	@Reference
	private SeatPreferencesDataToSeatPreference seatPreferencesDataToSeatPreferencesConverter;
	
	@Reference
	private MealDataToMeal mealDataToMealConverter;
	
	@Reference
	private SeatTypeDataToSeatType seatTypeDataToSeatTypeConverter;
	
	@Override
	public Preferences convert(PreferencesData source) {
		
		Preferences destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			com.alitalia.aem.ws.booking.paymentservice.xsd5.ObjectFactory xsd5ObjectFactory = 
					new com.alitalia.aem.ws.booking.paymentservice.xsd5.ObjectFactory();
			
			destination = objectFactory.createPreferences();
			
			ArrayOfanyType seatPreferences = xsd5ObjectFactory.createArrayOfanyType();
			if (source.getSeatPreferences() != null) {
				for (SeatPreferencesData seatPreferencesData : source.getSeatPreferences()) {
					seatPreferences.getAnyType().add(
							seatPreferencesDataToSeatPreferencesConverter.convert(
									seatPreferencesData));
				}
			}
			destination.setSeatPreferences(
					objectFactory.createPreferencesSeatPreferences(
							seatPreferences));
			
			destination.setMealType(
					objectFactory.createPreferencesMealType(
							mealDataToMealConverter.convert(
									source.getMealType())));
			
			destination.setSeatType(
					objectFactory.createPreferencesSeatType(
							seatTypeDataToSeatTypeConverter.convert(
									source.getSeatType())));
			
		}
		
		return destination;
	}

}
