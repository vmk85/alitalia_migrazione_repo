package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutInitRequest;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.CheckOutInitRequest;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.ObjectFactory;


@Component(immediate=true, metatype=false)
@Service(value=MmbAncillaryCheckOutInitRequestToCheckOutInitRequest.class)
public class MmbAncillaryCheckOutInitRequestToCheckOutInitRequest implements
		Converter<MmbAncillaryCheckOutInitRequest, CheckOutInitRequest> {

	@Reference
	private MmbAncillaryPaymentDataToPayment mmbAncillaryPaymentDataConverter;

	@Override
	public CheckOutInitRequest convert(MmbAncillaryCheckOutInitRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		CheckOutInitRequest destination = null;

		if (source != null) {
			destination = objectFactory.createCheckOutInitRequest();

			destination.setCartID(source.getCartId());
			destination.setMachineName(objectFactory.createAddToCartRequestMachineName(source.getMachineName()));
			destination.setSessionId(objectFactory.createAddToCartRequestSessionId(source.getSessionId()));
			destination.setClient(objectFactory.createCheckOutInitRequestClient(source.getClient()));

			destination.setPayment(objectFactory.createCheckOutInitRequestPayment(
					mmbAncillaryPaymentDataConverter.convert(source.getPayment())));
		}

		return destination;
	}

}
