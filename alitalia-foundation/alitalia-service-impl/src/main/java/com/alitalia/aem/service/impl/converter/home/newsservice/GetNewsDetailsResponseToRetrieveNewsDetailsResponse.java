package com.alitalia.aem.service.impl.converter.home.newsservice;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.DetailedNewsData;
import com.alitalia.aem.common.messages.home.RetrieveNewsDetailsResponse;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.news.newsservice.xsd2.GetNewsDetailsResponse;
import com.alitalia.aem.ws.news.newsservice.xsd4.DetailedNews;

@Component(immediate = true, metatype = false)
@Service(value = GetNewsDetailsResponseToRetrieveNewsDetailsResponse.class)
public class GetNewsDetailsResponseToRetrieveNewsDetailsResponse implements Converter<GetNewsDetailsResponse, RetrieveNewsDetailsResponse> {

	@Override
	public RetrieveNewsDetailsResponse convert(GetNewsDetailsResponse source) {
		
		RetrieveNewsDetailsResponse response = new RetrieveNewsDetailsResponse();
		
		JAXBElement<DetailedNews> jaxbDetailedNews = source.getNewsDetails();
		DetailedNews detailedNews = jaxbDetailedNews.getValue();
		
		DetailedNewsData detailedNewsData = new DetailedNewsData();
		detailedNewsData.setIsDeleted(detailedNews.isEliminato());

		XMLGregorianCalendar dataScad = detailedNews.getDataScad();
		if (dataScad != null)
			detailedNewsData.setExpiryDate(XsdConvertUtils.parseCalendar(dataScad));

		XMLGregorianCalendar dataUltMod = detailedNews.getDataUltMod();
		if (dataUltMod != null)
			detailedNewsData.setLastEditDate(XsdConvertUtils.parseCalendar(dataUltMod));

		detailedNewsData.setOwner(detailedNews.getProprietario().getValue());
		detailedNewsData.setIsPubLive(detailedNews.isPubLive());
		detailedNewsData.setIsPubTest(detailedNews.isPubTest());
		detailedNewsData.setText(detailedNews.getTesto().getValue());
		
		response.setDetailedNews(detailedNewsData);
		
		return response;
	}
}