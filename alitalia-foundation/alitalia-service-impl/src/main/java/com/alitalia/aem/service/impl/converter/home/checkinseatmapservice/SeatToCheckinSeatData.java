package com.alitalia.aem.service.impl.converter.home.checkinseatmapservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinSeatData;
import com.alitalia.aem.common.data.home.enumerations.CheckinAvailabilitySeatEnum;
import com.alitalia.aem.common.data.home.enumerations.CheckinTypeSeatEnum;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd2.Seat;


@Component(immediate=true, metatype=false)
@Service(value=SeatToCheckinSeatData.class)
public class SeatToCheckinSeatData implements Converter<Seat, CheckinSeatData> {
	

	@Override
	public CheckinSeatData convert(Seat source) {
		CheckinSeatData destination = null;

		if (source != null) {
			destination = new CheckinSeatData();

			if(source.getX003CAvailabilityX003EKBackingField() != null){
				destination.setAvailability(CheckinAvailabilitySeatEnum.fromValue(source.getX003CAvailabilityX003EKBackingField().value()));
			}
			destination.setIsComfortSeat(source.isX003CIsComfortSeatX003EKBackingField());
			destination.setIsUpgradeSeat(source.isX003CIsUpgradeSeatX003EKBackingField());
			if(source.getX003CTypeX003EKBackingField() != null){
				destination.setType(CheckinTypeSeatEnum.fromValue(source.getX003CTypeX003EKBackingField().value()));
			}
		}

		return destination;
	}

}
