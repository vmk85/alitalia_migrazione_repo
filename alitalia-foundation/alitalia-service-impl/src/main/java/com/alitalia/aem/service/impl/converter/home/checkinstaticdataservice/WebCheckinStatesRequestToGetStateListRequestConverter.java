package com.alitalia.aem.service.impl.converter.home.checkinstaticdataservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.WebCheckinStatesRequest;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.GetStateListRequest;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.Resource;



@Component(immediate=true, metatype=false)
@Service(value=WebCheckinStatesRequestToGetStateListRequestConverter.class)
public class WebCheckinStatesRequestToGetStateListRequestConverter implements Converter<WebCheckinStatesRequest, GetStateListRequest> {

	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;
	
	@Override
	public GetStateListRequest convert(WebCheckinStatesRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();		
		GetStateListRequest destination = objectFactory.createGetStateListRequest();
		
		destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
		destination.setX003CResourceX003EKBackingField(Resource.WEB);
		
		destination.setCountryCode(objectFactory.createGetStateListRequestCountryCode(source.getCountryCode()));

		return destination;
	}

}