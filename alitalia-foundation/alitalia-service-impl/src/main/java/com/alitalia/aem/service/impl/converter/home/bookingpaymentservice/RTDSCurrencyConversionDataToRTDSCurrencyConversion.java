package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionCurrencyConversionData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionCurrencyConversion;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=RTDSCurrencyConversionDataToRTDSCurrencyConversion.class)
public class RTDSCurrencyConversionDataToRTDSCurrencyConversion implements
		Converter<ResultTicketingDetailSolutionCurrencyConversionData,
					ResultTicketingDetailSolutionCurrencyConversion> {

	@Override
	public ResultTicketingDetailSolutionCurrencyConversion convert(
			ResultTicketingDetailSolutionCurrencyConversionData source) {
		ResultTicketingDetailSolutionCurrencyConversion destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionCurrencyConversion();

			destination.setApplicationField(source.getApplicationField());
			destination.setFromField(source.getFromField());
			destination.setRateField(source.getRateField());
			destination.setRateTableField(source.getRateTableField());
			destination.setToField(source.getToField());
		}

		return destination;
	}

}
