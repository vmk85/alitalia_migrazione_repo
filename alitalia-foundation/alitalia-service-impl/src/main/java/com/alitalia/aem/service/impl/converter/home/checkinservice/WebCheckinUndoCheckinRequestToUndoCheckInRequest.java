/**
 * 
 */
package com.alitalia.aem.service.impl.converter.home.checkinservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.WebCheckinUndoCheckinRequest;
import com.alitalia.aem.ws.checkin.checkinservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.checkinservice.xsd1.Resource;
import com.alitalia.aem.ws.checkin.checkinservice.xsd1.UndoCheckInRequest;

@Component(immediate=true, metatype=false)
@Service(value=WebCheckinUndoCheckinRequestToUndoCheckInRequest.class)
public class WebCheckinUndoCheckinRequestToUndoCheckInRequest implements Converter<WebCheckinUndoCheckinRequest, UndoCheckInRequest> {

	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;
	
	@Reference
	private CheckinRouteDataToRoute checkinRouteDataConverter;

	@Override
	public UndoCheckInRequest convert(WebCheckinUndoCheckinRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		UndoCheckInRequest destination = null;

		if (source != null) {
			
			destination = objectFactory.createUndoCheckInRequest();
			destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			destination.setX003CResourceX003EKBackingField(Resource.WEB);
			
			destination.setEticket(objectFactory.createUndoCheckInRequestEticket(source.getEticket()));
			destination.setName(objectFactory.createUndoCheckInRequestName(source.getName()));
			destination.setLastname(objectFactory.createUndoCheckInRequestLastname(source.getLastname()));
			
			destination.setRoute(objectFactory.createUndoCheckInRequestRoute(checkinRouteDataConverter.convert(source.getRoute())));

		}
		return destination;
	}
}
