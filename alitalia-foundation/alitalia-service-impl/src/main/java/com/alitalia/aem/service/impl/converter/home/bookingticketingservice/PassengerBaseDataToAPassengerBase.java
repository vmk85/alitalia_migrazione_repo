package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.data.home.ApplicantPassengerData;
import com.alitalia.aem.common.data.home.ChildPassengerData;
import com.alitalia.aem.common.data.home.InfantPassengerData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.APassengerBase;

@Component(immediate = true, metatype = false)
@Service(value = PassengerBaseDataToAPassengerBase.class)
public class PassengerBaseDataToAPassengerBase implements Converter<PassengerBaseData, APassengerBase> {

	@Reference
	private ApplicantPassengerDataToApplicantPassenger applicantPassengerDataConverter;

	@Reference
	private AdultPassengerDataToAdultPassenger adultPassengerDataConverter;

	@Reference
	private ChildPassengerDataToChildPassenger childPassengerDataConverter;

	@Reference
	private InfantPassengerDataToInfantPassenger infantPassengerDataConverter;

	@Override
	public APassengerBase convert(PassengerBaseData source) {
		APassengerBase destination = null;

		if (source != null) {
			if (source instanceof ApplicantPassengerData) {
				destination = applicantPassengerDataConverter.convert((ApplicantPassengerData) source);
			} else if (source instanceof AdultPassengerData) {
				destination = adultPassengerDataConverter.convert((AdultPassengerData) source);
			} else if (source instanceof ChildPassengerData) {
				destination = childPassengerDataConverter.convert((ChildPassengerData) source);
			} else if (source instanceof InfantPassengerData) {
				destination = infantPassengerDataConverter.convert((InfantPassengerData) source);
			}
		}

		return destination;
	}

}
