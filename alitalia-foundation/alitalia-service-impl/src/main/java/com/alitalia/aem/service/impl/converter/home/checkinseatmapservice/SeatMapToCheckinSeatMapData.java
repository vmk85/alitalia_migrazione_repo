package com.alitalia.aem.service.impl.converter.home.checkinseatmapservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinSeatData;
import com.alitalia.aem.common.data.home.checkin.CheckinSeatMapData;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd2.ArrayOfSeat;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd2.Seat;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd2.SeatMap;
import com.alitalia.aem.ws.checkin.seatmapservice.xsd4.ArrayOfstring;


@Component(immediate=true, metatype=false)
@Service(value=SeatMapToCheckinSeatMapData.class)
public class SeatMapToCheckinSeatMapData implements Converter<SeatMap, CheckinSeatMapData> {


	@Reference
	private SeatToCheckinSeatData seatConverter;

	@Override
	public CheckinSeatMapData convert(SeatMap source) {
		CheckinSeatMapData destination = null;

		if (source != null) {
			destination = new CheckinSeatMapData();

			List<String> columnsList = null;
			ArrayOfstring sourceColumnsList = source.getX003CColumnsX003EKBackingField();
			if (sourceColumnsList != null &&
					sourceColumnsList.getString() != null &&
					!sourceColumnsList.getString().isEmpty()) {
				columnsList = new ArrayList<String>();
				for (String sourceColumn : sourceColumnsList.getString()) {
					columnsList.add(sourceColumn);
				}
				destination.setColumns(columnsList);
			}
			destination.setHasExit(source.isX003CHasExitX003EKBackingField());
			destination.setIsExist(source.isX003CIsExistX003EKBackingField());
			destination.setNumber(source.getX003CNumberX003EKBackingField());
			
			
			List<CheckinSeatData> seatsList = null;
			ArrayOfSeat sourceSeatsList = source.getX003CSeatsX003EKBackingField();
			if (sourceSeatsList != null &&
					sourceSeatsList.getSeat() != null &&
					!sourceSeatsList.getSeat().isEmpty()) {
				seatsList = new ArrayList<CheckinSeatData>();
				for (Seat sourceSeat : sourceSeatsList.getSeat()) {
					seatsList.add(seatConverter.convert(sourceSeat));
				}
				destination.setSeats(seatsList);
			}			
			destination.setSecondLevel(source.isX003CSecondLevelX003EKBackingField());
		}

		return destination;
	}

}
