package com.alitalia.aem.service.impl.converter.home.checkinstaticdataservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.FrequentFlyerTypeData;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd2.FrequentFlyerCarrier;


@Component(immediate = true, metatype = false)
@Service(value = FrequentFlyerCarrierToFrequentFlyerTypeData.class)
public class FrequentFlyerCarrierToFrequentFlyerTypeData implements Converter<FrequentFlyerCarrier, FrequentFlyerTypeData> {

	@Override
	public FrequentFlyerTypeData convert(FrequentFlyerCarrier source) {
		FrequentFlyerTypeData destination = new FrequentFlyerTypeData();
		
		destination.setCode(source.getX003CCodeX003EKBackingField());
		destination.setDescription(source.getX003CDescriptionX003EKBackingField());
		destination.setLenght(source.getX003CLenghtX003EKBackingField());
		destination.setPreFillChar(source.getX003CPreFillCharX003EKBackingField());
		destination.setRegularExpressionValidation(source.getX003CRegularExpressionValidationX003EKBackingField());
		
		return destination;
	}
}