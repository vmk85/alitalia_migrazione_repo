package com.alitalia.aem.service.impl.converter.home.mmbancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryExtraBaggageDetailData;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.ExtraBaggageDetail;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=MmbAncillaryExtraBaggageDetailDataToExtraBaggageDetail.class)
public class MmbAncillaryExtraBaggageDetailDataToExtraBaggageDetail implements
		Converter<MmbAncillaryExtraBaggageDetailData, ExtraBaggageDetail> {

	@Override
	public ExtraBaggageDetail convert(MmbAncillaryExtraBaggageDetailData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		ExtraBaggageDetail destination = null;

		if (source != null) {
			destination = objectFactory.createExtraBaggageDetail();

			destination.setX003CBaggageTypeX003EKBackingField(source.getBaggageType());
		}

		return destination;
	}

}
