package com.alitalia.aem.service.impl.converter.home.staticdataservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.messages.home.RetrieveProvincesResponse;
import com.alitalia.aem.ws.booking.staticdataservice.xsd1.StaticDataResponse;
import com.alitalia.aem.ws.booking.staticdataservice.xsd2.Country;

@Component(immediate = true, metatype = false)
@Service(value = StaticDataResponseToRetrieveProvincesResponseConverter.class)
public class StaticDataResponseToRetrieveProvincesResponseConverter implements Converter<StaticDataResponse, RetrieveProvincesResponse> {

	private static final Logger logger = LoggerFactory.getLogger(StaticDataResponseToRetrieveProvincesResponseConverter.class);

	@Reference
	private CountryToCountryData countryConverter;
	
	@Reference
	private StaticDataServiceJAXBContextFactory jaxbContextFactory;

	@SuppressWarnings("unchecked")
	@Override
	public RetrieveProvincesResponse convert(StaticDataResponse source) {
		RetrieveProvincesResponse destination = new RetrieveProvincesResponse();
		try {
			List<CountryData> countriesData = new ArrayList<>();
			
			Unmarshaller countryUnmashaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			JAXBElement<Country> unmarshalledObj = null;

			List<Object> countries = source.getEntities().getValue().getAnyType();
			for (Object country : countries) {
				Country countrySource = null;
				if (country instanceof Country) {
					countrySource = (Country) country;
				} else {
					unmarshalledObj = (JAXBElement<Country>) countryUnmashaller.unmarshal((Node) country);
					countrySource = unmarshalledObj.getValue();
				}
				CountryData countryData = countryConverter.convert(countrySource);
				countriesData.add(countryData);
			}
			
			destination.setProvinces(countriesData);
			
		} catch (JAXBException e) {
			logger.error("Error instantiating JAXBContext/Unmarshaller: {}", e);
		} finally {
			logger.info("Conversion executed successfully");
		}
		return destination;			
	}
}