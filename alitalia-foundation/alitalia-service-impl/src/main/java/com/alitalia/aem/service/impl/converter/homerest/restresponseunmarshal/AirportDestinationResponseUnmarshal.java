package com.alitalia.aem.service.impl.converter.homerest.restresponseunmarshal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.GetDestinationsResponse;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class AirportDestinationResponseUnmarshal {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public GetDestinationsResponse airportDestinationUnmarshal(String jsonString) {
		
		GetDestinationsResponse response = new GetDestinationsResponse();
		Gson gson = new Gson();
		
		try {
			response = gson.fromJson(jsonString, GetDestinationsResponse.class);
		}
		catch (JsonSyntaxException e) {
			e.printStackTrace();
			logger.debug("JsonSyntaxException in airportDestinationUnmarshal");
		}
		
		return response;
	}


}
