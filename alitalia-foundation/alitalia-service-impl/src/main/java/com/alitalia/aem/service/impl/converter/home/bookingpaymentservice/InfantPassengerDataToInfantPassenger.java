package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.InfantPassengerData;
import com.alitalia.aem.common.data.home.TicketInfoData;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.InfantPassenger;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.PassengerType;
import com.alitalia.aem.ws.booking.paymentservice.xsd5.ArrayOfanyType;

@Component(immediate=true, metatype=false)
@Service(value=InfantPassengerDataToInfantPassenger.class)
public class InfantPassengerDataToInfantPassenger 
		implements Converter<InfantPassengerData, InfantPassenger> {
	
	@Reference
	private PassengerInfoBaseDataToInfoBase infoBaseDataToInfoBaseConverter;
	
	@Reference
	private PreferencesDataToPreferences preferencesDataToPreferencesConverter;
	
	@Reference
	private TicketInfoDataToTicketInfo ticketInfoDataToTicketInfoConverter;
	
	@Override
	public InfantPassenger convert(InfantPassengerData source) {
		
		InfantPassenger destination = null;
		
		if (source != null) {
			
			ObjectFactory objectFactory = new ObjectFactory();
			com.alitalia.aem.ws.booking.paymentservice.xsd5.ObjectFactory xsd5ObjectFactory = 
					new com.alitalia.aem.ws.booking.paymentservice.xsd5.ObjectFactory();
			
			destination = objectFactory.createInfantPassenger();
			
			// InfantPassenger data
			
			destination.setAdultReferent(
					source.getAdultRefent());
			
			// APassengerBase data
			
			if (source.getType() != null) {
				destination.setType(
						PassengerType.fromValue(source.getType().value()));
			}
			
			destination.setLastName(
					objectFactory.createAPassengerBaseLastName(
							source.getLastName()));
			
			destination.setName(
					objectFactory.createAPassengerBaseName(
							source.getName()));
			
			destination.setCouponPrice(
					source.getCouponPrice());
			
			destination.setExtraCharge(
					source.getExtraCharge());
			
			destination.setFee(
					source.getFee());
			
			destination.setGrossFare(
					source.getGrossFare());
			
			destination.setNetFare(
					source.getNetFare());
			
			destination.setInfo(
					objectFactory.createAPassengerBaseInfo(
							infoBaseDataToInfoBaseConverter.convert(
									source.getInfo())));
			
			ArrayOfanyType tickets = xsd5ObjectFactory.createArrayOfanyType();
			if (source.getTickets() != null && !source.getTickets().isEmpty()) {
				for (TicketInfoData ticketInfoData : source.getTickets()) {
					tickets.getAnyType().add(
							ticketInfoDataToTicketInfoConverter.convert(
								ticketInfoData));
				}
			}
			destination.setTickets(
					objectFactory.createAPassengerBaseTickets(
							tickets));
		
		}
		
		return destination;
	}

}
