package com.alitalia.aem.service.impl.converter.home.mmbticketingservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.CrossSellingBaseData;
import com.alitalia.aem.common.data.home.enumerations.CrossSellingTypeEnum;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd4.CrossSellingBase;

@Component(immediate = true, metatype = false)
@Service(value = CrossSellingBaseToCrossSellingBaseData.class)
public class CrossSellingBaseToCrossSellingBaseData implements
		Converter<CrossSellingBase, CrossSellingBaseData> {

	@Override
	public CrossSellingBaseData convert(CrossSellingBase source) {
		CrossSellingBaseData destination = null;
		if (source != null) {
			destination = new CrossSellingBaseData();
			destination.setDescription(source.getDescription().getValue());
			destination.setImageName(source.getImageName().getValue());
			destination.setName(source.getName().getValue());
			destination.setType(CrossSellingTypeEnum.fromValue(source.getType().value()));
			destination.setUrl(source.getUrl().getValue());
		}

		return destination;
	}

}
