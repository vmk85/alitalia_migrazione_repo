package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.CheckinInsuranceResponse;
import com.alitalia.aem.common.messages.home.WebCheckinGetInsuranceRequest;
import com.alitalia.aem.common.messages.home.WebCheckinPayInsuranceRequest;
import com.alitalia.aem.service.api.home.WebCheckinInsuranceService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.checkininsuranceservice.InsuranceInfoResponseToInsuranceResponseConverter;
import com.alitalia.aem.service.impl.converter.home.checkininsuranceservice.InsurancePaymentResponseToInsuranceResponseConverter;
import com.alitalia.aem.service.impl.converter.home.checkininsuranceservice.WebCheckinGetInsuranceRequestToInsuranceInfoRequestConverter;
import com.alitalia.aem.service.impl.converter.home.checkininsuranceservice.WebCheckinPayInsuranceRequestToInsurancePaymentRequestConverter;
import com.alitalia.aem.ws.checkin.insuranceservice.CheckinInsuranceServiceClient;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd1.InsuranceInfoRequest;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd1.InsuranceInfoResponse;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd1.InsurancePaymentRequest;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd1.InsurancePaymentResponse;


@Component(immediate=true, metatype=false)
@Service
public class SimpleWebCheckinInsuranceService implements WebCheckinInsuranceService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleWebCheckinInsuranceService.class);

	@Reference
	private CheckinInsuranceServiceClient checkinInsuranceClient;

	@Reference
	private WebCheckinGetInsuranceRequestToInsuranceInfoRequestConverter checkInInsuranceRequestConverter;
	
	@Reference
	private InsuranceInfoResponseToInsuranceResponseConverter checkInInsuranceResponseConverter;
	
	@Reference
	private InsurancePaymentResponseToInsuranceResponseConverter checkInPaymentInsuranceResponseConverter;
	
	@Reference
	private WebCheckinPayInsuranceRequestToInsurancePaymentRequestConverter checkinPayInsuranceRequestConverter;
	
	@Override
	public CheckinInsuranceResponse getInsuranceInfo(WebCheckinGetInsuranceRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method getInsuranceInfo(). The request is {}", request);
		}

		try {
			CheckinInsuranceResponse response = new CheckinInsuranceResponse();
			InsuranceInfoRequest serviceRequest = checkInInsuranceRequestConverter.convert(request);
			InsuranceInfoResponse serviceResponse = checkinInsuranceClient.getInsurance(serviceRequest);
			response = checkInInsuranceResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully getInsuranceInfo(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getInsuranceInfo().", e);
			throw new AlitaliaServiceException("Exception executing getInsuranceInfo: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public CheckinInsuranceResponse payInsurance(WebCheckinPayInsuranceRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method payInsurance(). The request is {}", request);
		}

		try {
			CheckinInsuranceResponse response = new CheckinInsuranceResponse();
			InsurancePaymentRequest serviceRequest = checkinPayInsuranceRequestConverter.convert(request);
			InsurancePaymentResponse serviceResponse = checkinInsuranceClient.payInsurance(serviceRequest);
			response = checkInPaymentInsuranceResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully payInsurance(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method payInsurance().", e);
			throw new AlitaliaServiceException("Exception executing payInsurance: Sid ["+request.getSid()+"]" , e);
		}
	}	
}