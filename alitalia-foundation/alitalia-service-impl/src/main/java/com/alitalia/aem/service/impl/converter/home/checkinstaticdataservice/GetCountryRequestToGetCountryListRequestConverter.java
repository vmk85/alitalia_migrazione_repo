package com.alitalia.aem.service.impl.converter.home.checkinstaticdataservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.GetCountryRequest;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.GetCountryListRequest;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.staticdataservice.xsd1.Resource;



@Component(immediate=true, metatype=false)
@Service(value=GetCountryRequestToGetCountryListRequestConverter.class)
public class GetCountryRequestToGetCountryListRequestConverter implements Converter<GetCountryRequest, GetCountryListRequest> {

	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;
	
	@Override
	public GetCountryListRequest convert(GetCountryRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();		
		GetCountryListRequest destination = objectFactory.createGetCountryListRequest();
		
		destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
		destination.setX003CResourceX003EKBackingField(Resource.WEB);
		
		String languageCode = source.getLanguageCode();
		String siteCode = source.getSiteCode();
		
		destination.setLanguageCode(objectFactory.createGetCountryListRequestLanguageCode(languageCode));
		destination.setSiteCode(objectFactory.createGetCountryListRequestSiteCode(siteCode));

		return destination;
	}

}