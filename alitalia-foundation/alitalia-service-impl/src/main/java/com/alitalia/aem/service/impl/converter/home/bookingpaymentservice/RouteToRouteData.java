
package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ConnectingFlight;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.DirectFlight;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.Route;

@Component(immediate=true, metatype=false)
@Service(value=RouteToRouteData.class)
public class RouteToRouteData implements Converter<Route, RouteData> {

	@Reference
	private DirectFlightToDirectFlightData directFlightConverter;

	@Reference
	private ConnectingFlightToConnectingFlightData connectingFlightConverter;

	@Override
	public RouteData convert(Route source) {
		RouteData destination = new RouteData();

		destination.setIndex(source.getIndex());
		destination.setType(RouteTypeEnum.fromValue(source.getType().value()));

		ArrayList<FlightData> flights = new ArrayList<FlightData>();
		List<Object> sourceFlights = source.getFlights().getValue().getAnyType();
		for (Object flight : sourceFlights) {
			if (flight instanceof DirectFlight)
			{
				DirectFlight castedDirectFlight = (DirectFlight) flight;
				DirectFlightData directFlightData = directFlightConverter.convert(castedDirectFlight);
				flights.add(directFlightData);
			}
			else 
			{
				ConnectingFlight castedConnectingFlight = (ConnectingFlight) flight;
				ConnectingFlightData connectingFlightData = connectingFlightConverter.convert(castedConnectingFlight);
				flights.add(connectingFlightData);
			}
		}
		destination.setFlights(flights);
		
		return destination;
	}

}
