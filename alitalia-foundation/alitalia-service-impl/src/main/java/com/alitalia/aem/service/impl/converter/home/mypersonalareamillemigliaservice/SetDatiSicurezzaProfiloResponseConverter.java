package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import com.alitalia.aem.common.converter.Converter;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

@Component(immediate = true, metatype = false)
@Service(value = SetDatiSicurezzaProfiloResponseConverter.class)
public class SetDatiSicurezzaProfiloResponseConverter implements Converter<com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SetDatiSicurezzaProfiloResponse,com.alitalia.aem.common.messages.home.SetDatiSicurezzaProfiloResponse > {



	@Override
	public com.alitalia.aem.common.messages.home.SetDatiSicurezzaProfiloResponse convert( com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SetDatiSicurezzaProfiloResponse source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");

		com.alitalia.aem.common.messages.home.SetDatiSicurezzaProfiloResponse destination = new com.alitalia.aem.common.messages.home.SetDatiSicurezzaProfiloResponse();

		if (source != null)
			destination.setflagIDProfiloSicurezzaSaved(source.isFlagIDProfiloSicurezzaSaved());

		return destination;
	}
}