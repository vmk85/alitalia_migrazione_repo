package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.MilleMigliaLoginResponse;
import com.alitalia.aem.common.messages.home.MilleMigliaLoginSAResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.LoginSAResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.LoginResponse;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

@Component(immediate = true, metatype = false)
@Service(value = LoginResponseToMilleMigliaLoginSAResponse.class)
public class LoginResponseToMilleMigliaLoginSAResponse
		implements Converter<LoginSAResponse, MilleMigliaLoginSAResponse> {

	@Reference
	private CustomerSAToMMCustomerSA customerSAToMMCustomerSA;
	
	@Override
	public MilleMigliaLoginSAResponse convert(LoginSAResponse source) {
		MilleMigliaLoginSAResponse destination = null;

		if (source != null) {

			destination = new MilleMigliaLoginSAResponse();

			if (source.getCustomerSA() != null) {

				destination.setCustomerSA(
						customerSAToMMCustomerSA.convert(
								source.getCustomerSA().getValue()));
			}
			if (source.getSSOToken() != null) {
				destination.setSSO_token(source.getSSOToken().getValue());
			}
		}

		return destination;
	}

}
