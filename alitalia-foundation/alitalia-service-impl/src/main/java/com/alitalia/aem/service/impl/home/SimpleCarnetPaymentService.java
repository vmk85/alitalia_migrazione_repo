package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.CarnetBuyCarnetRequest;
import com.alitalia.aem.common.messages.home.CarnetBuyCarnetResponse;
import com.alitalia.aem.common.messages.home.CarnetSendEmailRequest;
import com.alitalia.aem.common.messages.home.CarnetSendEmailResponse;
import com.alitalia.aem.service.api.home.CarnetPaymentService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.carnetpaymentservice.BuyCarnetResponseToCarnetBuyCarnetResponse;
import com.alitalia.aem.service.impl.converter.home.carnetpaymentservice.CarnetBuyCarnetRequestToBuyCarnetRequest;
import com.alitalia.aem.service.impl.converter.home.carnetpaymentservice.CarnetSendEmailRequestToSendEmailRequest;
import com.alitalia.aem.service.impl.converter.home.carnetpaymentservice.SendEmailResponseToCarnetSendEmailResponse;
import com.alitalia.aem.ws.carnet.paymentservice.CarnetPaymentServiceClient;
import com.alitalia.aem.ws.carnet.paymentservice.xsd1.BuyCarnetRequest;
import com.alitalia.aem.ws.carnet.paymentservice.xsd1.BuyCarnetResponse;
import com.alitalia.aem.ws.carnet.paymentservice.xsd1.SendEmailRequest;
import com.alitalia.aem.ws.carnet.paymentservice.xsd1.SendEmailResponse;

@Component(immediate=true, metatype=false)
@Service
public class SimpleCarnetPaymentService implements CarnetPaymentService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleCarnetPaymentService.class);

	@Reference
	private CarnetPaymentServiceClient carnetPaymentServiceClient;
	
	@Reference
	private CarnetBuyCarnetRequestToBuyCarnetRequest buyCarnetRequestConverter;
	
	@Reference
	private BuyCarnetResponseToCarnetBuyCarnetResponse buyCarnetResponseConverter;

	@Reference
	private CarnetSendEmailRequestToSendEmailRequest sendEmailRequestConverter;

	@Reference
	private SendEmailResponseToCarnetSendEmailResponse sendEmailResponseConverter;
	
	@Override
	public CarnetBuyCarnetResponse buyCarnet(CarnetBuyCarnetRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method buyCarnet(). The request is {}", request);
		}

		try {
			CarnetBuyCarnetResponse response = new CarnetBuyCarnetResponse();
			BuyCarnetRequest serviceRequest = buyCarnetRequestConverter.convert(request);
			BuyCarnetResponse serviceResponse = carnetPaymentServiceClient.buyCarnet(serviceRequest);
			response = buyCarnetResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully buyCarnet(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method buyCarnet().", e);
			throw new AlitaliaServiceException("Exception executing buyCarnet: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public CarnetSendEmailResponse sendEmail(CarnetSendEmailRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method sendEmail(). The request is {}", request);
		}

		try {
			CarnetSendEmailResponse response = new CarnetSendEmailResponse();
			SendEmailRequest serviceRequest = sendEmailRequestConverter.convert(request);
			SendEmailResponse serviceResponse = carnetPaymentServiceClient.sendEmail(serviceRequest);
			response = sendEmailResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully sendEmail(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method sendEmail().", e);
			throw new AlitaliaServiceException("Exception executing sendEmail: Sid ["+request.getSid()+"]" , e);
		}
	}
}