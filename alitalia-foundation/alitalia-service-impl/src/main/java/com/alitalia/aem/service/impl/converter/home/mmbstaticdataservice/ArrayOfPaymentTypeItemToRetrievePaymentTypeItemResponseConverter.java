package com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PaymentTypeItemData;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemResponse;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd3.ArrayOfPaymentTypeItem;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd3.PaymentTypeItem;

@Component(immediate = true, metatype = false)
@Service(value = ArrayOfPaymentTypeItemToRetrievePaymentTypeItemResponseConverter.class)
public class ArrayOfPaymentTypeItemToRetrievePaymentTypeItemResponseConverter implements Converter<ArrayOfPaymentTypeItem, RetrievePaymentTypeItemResponse> {

	private static final Logger logger = LoggerFactory.getLogger(ArrayOfPaymentTypeItemToRetrievePaymentTypeItemResponseConverter.class);

	@Reference
	private PaymentTypeItemToPaymentTypeItemData paymentTypeItemConverter;
	
	@Reference
	private BookingMMBStaticDataServiceJAXBContextFactory jaxbContextFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public RetrievePaymentTypeItemResponse convert(ArrayOfPaymentTypeItem source) {
		RetrievePaymentTypeItemResponse destination = new RetrievePaymentTypeItemResponse();
		try {
			List<PaymentTypeItemData> paymentTypeItemsData = new ArrayList<>();
			
			Unmarshaller paymentTypeItemUnmashaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			JAXBElement<PaymentTypeItem> unmarshalledObj = null;

			List<PaymentTypeItem> countries = source.getPaymentTypeItem();
			for (Object paymentTypeItemPrefix : countries) {
				PaymentTypeItem phonePrefixSource = null;
				if (paymentTypeItemPrefix instanceof PaymentTypeItem) {
					phonePrefixSource = (PaymentTypeItem) paymentTypeItemPrefix;
				} else {
					unmarshalledObj = (JAXBElement<PaymentTypeItem>) paymentTypeItemUnmashaller.unmarshal((Node) paymentTypeItemPrefix);
					phonePrefixSource = unmarshalledObj.getValue();
				}
				PaymentTypeItemData phonePrefixData = paymentTypeItemConverter.convert(phonePrefixSource);
				paymentTypeItemsData.add(phonePrefixData);
			}
			
			destination.setPaymentTypeItemsData(paymentTypeItemsData);
			
		} catch (JAXBException e) {
			logger.error("Error instantiating JAXBContext/Unmarshaller: {}", e);
		} finally {
			logger.info("Conversion executed successfully");
		}
		return destination;			
	}
}