package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.CarnetCountryStaticDataResponse;
import com.alitalia.aem.common.messages.home.CarnetPaymentTypeResponse;
import com.alitalia.aem.common.messages.home.CarnetStaticDataRequest;
import com.alitalia.aem.service.api.home.CarnetStaticDataService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.carnetstaticdataservice.CarnetStaticDataRequestToGetStaticDataRequest;
import com.alitalia.aem.service.impl.converter.home.carnetstaticdataservice.CountryStaticDataResponseToCarnetCountryStaticDataResponse;
import com.alitalia.aem.service.impl.converter.home.carnetstaticdataservice.PaymentTypeItemStaticDataResponseToCarnetPaymentTypeResponse;
import com.alitalia.aem.ws.carnet.staticdataservice.CarnetStaticDataServiceClient;
import com.alitalia.aem.ws.carnet.staticdataservice.xsd1.CountryStaticDataResponse;
import com.alitalia.aem.ws.carnet.staticdataservice.xsd1.GetStaticDataRequest;
import com.alitalia.aem.ws.carnet.staticdataservice.xsd1.PaymentTypeItemStaticDataResponse;

@Component(immediate=true, metatype=false)
@Service
public class SimpleCarnetStaticDataService implements CarnetStaticDataService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleCarnetStaticDataService.class);

	@Reference
	private CarnetStaticDataServiceClient carnetStaticDataServiceClient;
	
	@Reference
	private CarnetStaticDataRequestToGetStaticDataRequest carnetStaticDataRequestConverter;
	
	@Reference
	private CountryStaticDataResponseToCarnetCountryStaticDataResponse countryStaticDataResponseConverter;
	
	@Reference
	private PaymentTypeItemStaticDataResponseToCarnetPaymentTypeResponse paymentTypeItemStaticDataResponseConverter;
	
	@Override
	public CarnetPaymentTypeResponse getPaymentList(CarnetStaticDataRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method getPaymentList(). The request is {}", request);
		}

		try {
			CarnetPaymentTypeResponse response = new CarnetPaymentTypeResponse();
			GetStaticDataRequest serviceRequest = carnetStaticDataRequestConverter.convert(request);
			PaymentTypeItemStaticDataResponse serviceResponse = carnetStaticDataServiceClient.getPaymentList(serviceRequest);
			response = paymentTypeItemStaticDataResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully getPaymentList(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getPaymentList().", e);
			throw new AlitaliaServiceException("Exception executing getPaymentList: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public CarnetCountryStaticDataResponse getCreditCardCountryList(CarnetStaticDataRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method getCreditCardCountryList(). The request is {}", request);
		}

		try {
			CarnetCountryStaticDataResponse response = new CarnetCountryStaticDataResponse();
			GetStaticDataRequest serviceRequest = carnetStaticDataRequestConverter.convert(request);
			CountryStaticDataResponse serviceResponse = carnetStaticDataServiceClient.getCreditCardCountryList(serviceRequest);
			response = countryStaticDataResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully getCreditCardCountryList(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getCreditCardCountryList().", e);
			throw new AlitaliaServiceException("Exception executing getCreditCardCountryList: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public CarnetCountryStaticDataResponse getStateList(CarnetStaticDataRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method getStateList(). The request is {}", request);
		}

		try {
			CarnetCountryStaticDataResponse response = new CarnetCountryStaticDataResponse();
			GetStaticDataRequest serviceRequest = carnetStaticDataRequestConverter.convert(request);
			CountryStaticDataResponse serviceResponse = carnetStaticDataServiceClient.getStateList(serviceRequest);
			response = countryStaticDataResponseConverter.convert(serviceResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully getStateList(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method getStateList().", e);
			throw new AlitaliaServiceException("Exception executing getStateList: Sid ["+request.getSid()+"]" , e);
		}
	}
}