package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMMessageData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Message;

@Component(immediate = true, metatype = false)
@Service(value = MMMessageDataToMessage.class)
public class MMMessageDataToMessage implements Converter<MMMessageData, Message> {

	@Override
	public Message convert(MMMessageData source) {
		Message destination = null;

		if (source != null) {
			com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ObjectFactory objectFactory2 = new com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ObjectFactory();

			destination = objectFactory2.createMessage();
			destination.setAlreadyRead(source.getAlreadyRead());
			destination.setDescription(objectFactory2.createMessageDescription(source.getDescription()));
			destination.setMessageNumber(source.getMessageNumber());
			destination.setSequenceNumber(source.getSequenceNumber());
			destination.setText(objectFactory2.createMessageText(source.getText()));
			destination.setTitle(objectFactory2.createMessageTitle(source.getTitle()));

			if (source.getStartValidatDate() != null)
				destination.setStartValidDate(XsdConvertUtils.toXMLGregorianCalendar(source.getStartValidatDate()));
			if (source.getEndValidatDate() != null)
				destination.setEndValidDate(XsdConvertUtils.toXMLGregorianCalendar(source.getEndValidatDate()));
			if (source.getReadDate() != null)
				destination.setReadDate(XsdConvertUtils.toXMLGregorianCalendar(source.getReadDate()));
		}

		return destination;
	}

}
