package com.alitalia.aem.service.impl.converter.home.flightstatusservice;

import java.util.Calendar;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.DaysOfWeekData;
import com.alitalia.aem.common.data.home.FlightItinerayData;
import com.alitalia.aem.common.data.home.ItineraryModelData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.ArrayOfFlight;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.DaysOfWeek;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.Flight;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.ItineraryModel;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.ObjectFactory;

@Component(immediate = true, metatype = false)
@Service(value = ItineraryModelDataToItineraryModel.class)
public class ItineraryModelDataToItineraryModel implements Converter<ItineraryModelData, ItineraryModel> {

	@Reference
	FlightItinerayDetailsModelDataToFlightDetailsModel flightItinerayDetailsModelDataToFlightDetailsModel;
	
	@Override
	public ItineraryModel convert(ItineraryModelData source) {
		ItineraryModel itineraryModel = null;
		
		if(source!=null){
		
		
		ObjectFactory objectFactory = new ObjectFactory();
		
		itineraryModel = objectFactory.createItineraryModel();
		
		
		Calendar dateFrom = source.getDateFrom();
		if (dateFrom != null)
			itineraryModel.setDateFrom(XsdConvertUtils.toXMLGregorianCalendar(dateFrom));

		Calendar dateTo = source.getDateTo();
		if (dateTo != null)
			itineraryModel.setDateTo(XsdConvertUtils.toXMLGregorianCalendar(dateTo));
		
		DaysOfWeekData daysOfWeekData = source.getFrequency();
		DaysOfWeek daysOfWeek = objectFactory.createDaysOfWeek();
		
		daysOfWeek.setMonday(daysOfWeekData.getMonday());
		daysOfWeek.setTuesday(daysOfWeekData.getTuesday());
		daysOfWeek.setWednesday(daysOfWeekData.getWednesday());
		daysOfWeek.setThursday(daysOfWeekData.getThursday());
		daysOfWeek.setFriday(daysOfWeekData.getFriday());
		daysOfWeek.setSaturday(daysOfWeekData.getSaturday());
		daysOfWeek.setSunday(daysOfWeekData.getSunday());
		
		itineraryModel.setFrequency(objectFactory.createDaysOfWeek(daysOfWeek));
		itineraryModel.setKey(objectFactory.createItineraryModelKey(source.getKey()));
		itineraryModel.setNumberOfConnection(source.getNumberOfConnection());
		
		
		ArrayOfFlight arrayOfFlight = objectFactory.createArrayOfFlight();
		for(FlightItinerayData f : source.getFlights()){
			Flight flight = objectFactory.createFlight();
			flight.setArrival(objectFactory.createFlightArrival(f.getArrival()));
			flight.setArrivalDate(XsdConvertUtils.toXMLGregorianCalendar(f.getArrivalDate()));
			flight.setArrivalTime(f.getArrivalTime());
			flight.setDeparture(objectFactory.createFlightDeparture(f.getDeparture()));
			flight.setDepartureDate(XsdConvertUtils.toXMLGregorianCalendar(f.getDepartureDate()));
			flight.setDepartureTime(f.getDepartureTime());
			flight.setFlightDetails(objectFactory.createFlightDetailsModelElement(flightItinerayDetailsModelDataToFlightDetailsModel.convert(f.getFlightDetails())));
			flight.setFlightLength(f.getFlightLength());
			flight.setFlightNumber(objectFactory.createFlightFlightNumber(f.getFlightNumber()));
			flight.setVector(objectFactory.createFlightVector(f.getVector()));
			arrayOfFlight.getFlight().add(flight);
		}
		
		
		itineraryModel.setFlights(objectFactory.createItineraryModelFlights(arrayOfFlight));
		}
		return itineraryModel;
	}
}