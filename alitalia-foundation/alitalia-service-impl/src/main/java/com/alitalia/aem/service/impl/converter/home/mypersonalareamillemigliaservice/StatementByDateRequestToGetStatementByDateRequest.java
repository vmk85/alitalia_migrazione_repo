package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.StatementByDateRequest;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.GetStatementByDateRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd5.ActivityTypes;

@Component(immediate = true, metatype = false)
@Service(value = StatementByDateRequestToGetStatementByDateRequest.class)
public class StatementByDateRequestToGetStatementByDateRequest implements Converter<StatementByDateRequest, GetStatementByDateRequest> {

	@Reference
	private MMCustomerProfileDataToCustomerProfile mmCustomerProfileDataConverter;
	
	@Override
	public GetStatementByDateRequest convert(StatementByDateRequest source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		
		ObjectFactory objectFactory = new ObjectFactory();
		
		GetStatementByDateRequest destination = objectFactory.createGetStatementByDateRequest();
		CustomerProfile customerProfile = mmCustomerProfileDataConverter.convert(source.getCustomerProfile());
		
		JAXBElement<CustomerProfile> jaxbCustomerProfile = objectFactory.createCustomerRequestCustomer(customerProfile);
		destination.setCustomer(jaxbCustomerProfile);
		destination.setActivityType(ActivityTypes.fromValue(source.getActivityType().value()));
		
		if (source.getDateFrom() != null)
			destination.setDateFrom(XsdConvertUtils.toXMLGregorianCalendar(source.getDateFrom()));
		
		if (source.getDateTo() != null)
			destination.setDateTo(XsdConvertUtils.toXMLGregorianCalendar(source.getDateTo()));
		
		destination.setMaxResults(source.getMaxResults());
				
		return destination;
	}
}