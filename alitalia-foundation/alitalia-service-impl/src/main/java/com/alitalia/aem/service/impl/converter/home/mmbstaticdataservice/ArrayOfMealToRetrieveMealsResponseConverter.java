package com.alitalia.aem.service.impl.converter.home.mmbstaticdataservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MealData;
import com.alitalia.aem.common.messages.home.RetrieveMealsResponse;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd3.ArrayOfMeal;
import com.alitalia.aem.ws.mmb.staticdataservice.xsd3.Meal;

@Component(immediate = true, metatype = false)
@Service(value = ArrayOfMealToRetrieveMealsResponseConverter.class)
public class ArrayOfMealToRetrieveMealsResponseConverter implements Converter<ArrayOfMeal, RetrieveMealsResponse> {

	private static final Logger logger = LoggerFactory.getLogger(ArrayOfMealToRetrieveMealsResponseConverter.class);

	@Reference
	private MealToMealData mealConverter;
	
	@Reference
	private BookingMMBStaticDataServiceJAXBContextFactory jaxbContextFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public RetrieveMealsResponse convert(ArrayOfMeal source) {
		RetrieveMealsResponse destination = new RetrieveMealsResponse();
		try {
			List<MealData> mealsData = new ArrayList<>();
			
			Unmarshaller mealUnmashaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
			JAXBElement<Meal> unmarshalledObj = null;

			List<Meal> countries = source.getMeal();
			for (Object meal : countries) {
				Meal mealSource = null;
				if (meal instanceof Meal) {
					mealSource = (Meal) meal;
				} else {
					unmarshalledObj = (JAXBElement<Meal>) mealUnmashaller.unmarshal((Node) meal);
					mealSource = unmarshalledObj.getValue();
				}
				MealData mealData = mealConverter.convert(mealSource);
				mealsData.add(mealData);
			}
			
			destination.setMeals(mealsData);
			
		} catch (JAXBException e) {
			logger.error("Error instantiating JAXBContext/Unmarshaller: {}", e);
		} finally {
			logger.info("Conversion executed successfully");
		}
		return destination;			
	}
}