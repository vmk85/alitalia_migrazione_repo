//2
package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingRefundPriceData;
import com.alitalia.aem.ws.booking.paymentservice.xsd11.ResultBookingDetailsSolutionPricingRefundPrice;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingRefundPriceToPricingRefundPriceData.class)
public class RBDSPricingRefundPriceToPricingRefundPriceData implements Converter<ResultBookingDetailsSolutionPricingRefundPrice, ResultBookingDetailsSolutionPricingRefundPriceData> {

	@Override
	public ResultBookingDetailsSolutionPricingRefundPriceData convert(
			ResultBookingDetailsSolutionPricingRefundPrice source) {
		ResultBookingDetailsSolutionPricingRefundPriceData destination = null;
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingRefundPriceData();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}

}
