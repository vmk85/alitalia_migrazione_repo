package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.RetrievePnrInformationCheckinResponse;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationRequest;
import com.alitalia.aem.common.messages.home.ValidateMmUserCheckinRequest;
import com.alitalia.aem.common.messages.home.ValidateMmUserCheckinResponse;
import com.alitalia.aem.service.api.home.WebCheckinSearchService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.checkinsearchservice.GetRoutesResponseToRetrieveMmbPnrInformationResponse;
import com.alitalia.aem.service.impl.converter.home.checkinsearchservice.RetrieveMmbPnrInformationRequestToGetRoutesRequest;
import com.alitalia.aem.service.impl.converter.home.checkinsearchservice.ValidateMmUserCheckinRequestToValidateMmUserRequest;
import com.alitalia.aem.service.impl.converter.home.checkinsearchservice.ValidateMmUserResponseToValidateMmUserCheckinResponse;
import com.alitalia.aem.ws.checkin.searchservice.CheckinSearchServiceClient;
import com.alitalia.aem.ws.checkin.searchservice.xsd1.GetRoutesRequest;
import com.alitalia.aem.ws.checkin.searchservice.xsd1.GetRoutesResponse;
import com.alitalia.aem.ws.checkin.searchservice.xsd1.ValidateMmUserRequest;
import com.alitalia.aem.ws.checkin.searchservice.xsd1.ValidateMmUserResponse;

@Component(immediate=true, metatype=false)
@Service
public class SimpleWebCheckinSearchService implements WebCheckinSearchService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleWebCheckinSearchService.class);

	@Reference
	private CheckinSearchServiceClient checkinSearchServiceClient;

	@Reference
	private RetrieveMmbPnrInformationRequestToGetRoutesRequest retrieveMmbPnrInformationRequestConverter;

	@Reference
	private GetRoutesResponseToRetrieveMmbPnrInformationResponse getRoutesResponseConverter;
	
	@Reference
	private ValidateMmUserCheckinRequestToValidateMmUserRequest validateMmUserRequestConverter;
	
	@Reference
	private ValidateMmUserResponseToValidateMmUserCheckinResponse validateMmUserResponseConverter;

	@Override
	public RetrievePnrInformationCheckinResponse retrievePnrInformation(RetriveMmbPnrInformationRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrivePnrInformation(). The request is {}", request);
		}
		

		try {
			RetrievePnrInformationCheckinResponse response = new RetrievePnrInformationCheckinResponse();
			GetRoutesRequest serviceRequest = retrieveMmbPnrInformationRequestConverter.convert(request);
			GetRoutesResponse serviceResponse = checkinSearchServiceClient.getRouteList(serviceRequest);
			response = getRoutesResponseConverter.convert(serviceResponse);
			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully retrievePnrInformation(). The response is {}", response);
			}
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method retrivePnrInformation().", e);
			throw new AlitaliaServiceException("Exception executing retrivePnrInformation: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public ValidateMmUserCheckinResponse validateUser(ValidateMmUserCheckinRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method validateUser(). The request is {}", request);
		}

		try {
			ValidateMmUserCheckinResponse response = new ValidateMmUserCheckinResponse();
			ValidateMmUserRequest serviceRequest = validateMmUserRequestConverter.convert(request);
			ValidateMmUserResponse validateMmUserResponse = checkinSearchServiceClient.validateMmUser(serviceRequest);
			response = validateMmUserResponseConverter.convert(validateMmUserResponse);

			response.setSid(request.getSid());
			response.setTid(request.getTid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method successfully validateUser(). The response is {}", response);
			}
			
			return response;
		} catch (Exception e) {
			logger.error("Exception while executing method validateUser().", e);
			throw new AlitaliaServiceException("Exception executing validateUser: Sid ["+request.getSid()+"]" , e);
		}
	}
}
