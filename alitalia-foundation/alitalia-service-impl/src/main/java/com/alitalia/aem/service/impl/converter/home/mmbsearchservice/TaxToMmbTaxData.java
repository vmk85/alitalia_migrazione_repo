package com.alitalia.aem.service.impl.converter.home.mmbsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.MmbTaxData;
import com.alitalia.aem.ws.mmb.searchservice.xsd2.Tax;

@Component(immediate=true, metatype=false)
@Service(value=TaxToMmbTaxData.class)
public class TaxToMmbTaxData implements Converter<Tax, MmbTaxData> {

	@Override
	public MmbTaxData convert(Tax source) {
		MmbTaxData destination = null;

		if (source != null) {
			destination = new MmbTaxData();

			destination.setAmount(source.getX003CAmountX003EKBackingField());
			destination.setCode(source.getX003CCodeX003EKBackingField());
			destination.setCurrency(source.getX003CCurrencyX003EKBackingField());
			destination.setId(String.valueOf(source.getX003CIdX003EKBackingField()));
			destination.setName(source.getX003CNameX003EKBackingField());
			destination.setPassengerTypeQuantity(source.getX003CPassengerTypeQuantityX003EKBackingField());
			destination.setTotalAmount(source.getX003CTotalAmountX003EKBackingField());
		}

		return destination;
	}

}
