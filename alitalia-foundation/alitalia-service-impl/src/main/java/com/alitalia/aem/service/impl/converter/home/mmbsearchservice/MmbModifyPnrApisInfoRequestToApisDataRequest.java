package com.alitalia.aem.service.impl.converter.home.mmbsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.MmbModifyPnrApisInfoRequest;
import com.alitalia.aem.ws.mmb.searchservice.xsd1.ApisDataRequest;
import com.alitalia.aem.ws.mmb.searchservice.xsd1.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=MmbModifyPnrApisInfoRequestToApisDataRequest.class)
public class MmbModifyPnrApisInfoRequestToApisDataRequest implements Converter<MmbModifyPnrApisInfoRequest, ApisDataRequest> {

	@Reference
	private MmbPassengerDataToPassenger mmbPassengerDataConverter;

	@Reference
	private MmbRequestBaseInfoDataToRequestBaseInfo mmbRequestBaseInfoDataConverter;

	@Override
	public ApisDataRequest convert(MmbModifyPnrApisInfoRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		ApisDataRequest destination = null;

		if (source != null) {
			destination = objectFactory.createApisDataRequest();

			destination.setPnr(objectFactory.createApisDataRequestPnr(source.getPnr()));
			destination.setPassenger(objectFactory.createApisDataRequestPassenger(
					mmbPassengerDataConverter.convert(source.getPassenger())));

			destination.setX003CPNRX003EKBackingField(null);
			destination.setX003CInfoRequestX003EKBackingField(mmbRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
		}

		return destination;
	}

}
