package com.alitalia.aem.service.impl.converter.home.checkinboardingpassservice;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.SendReminderCheckinRequest;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd1.Resource;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd1.SendReminderRequest;
import com.alitalia.aem.ws.checkin.boardingpassservice.xsd4.ArrayOfstring;


@Component(immediate=true, metatype=false)
@Service(value=SendReminderCheckinRequestToSendReminderRequest.class)
public class SendReminderCheckinRequestToSendReminderRequest implements Converter<SendReminderCheckinRequest, SendReminderRequest> {

	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;
	

	@Override
	public SendReminderRequest convert(SendReminderCheckinRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		SendReminderRequest destination = null;

		if (source != null) {
			destination = objectFactory.createSendReminderRequest();
			
			destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			destination.setX003CResourceX003EKBackingField(Resource.WEB);
			
			destination.setAttachment(objectFactory.createSendBoardingPassRequestAttachment(source.getAttachment()));
			destination.setAttachmentName(objectFactory.createSendBoardingPassRequestAttachmentName(source.getAttachmentName()));
			destination.setEmailBody(objectFactory.createSendBoardingPassRequestEmailBody(source.getEmailBody()));
			destination.setEmailSubject(objectFactory.createSendBoardingPassRequestEmailSubject(source.getEmailSubject()));
			
			List<String> sourceReceiverMailsList = source.getReceiverMails();
			if(sourceReceiverMailsList != null){
				ArrayOfstring receiverMailsList = new com.alitalia.aem.ws.checkin.boardingpassservice.xsd4.ObjectFactory().createArrayOfstring();
				for(String sourceReceiverMailsData : sourceReceiverMailsList){
					receiverMailsList.getString().add(sourceReceiverMailsData);
				}
				destination.setReceiverMails(objectFactory.createSendReminderRequestReceiverMails(receiverMailsList));
			}

			
		}

		return destination;
	}


}
