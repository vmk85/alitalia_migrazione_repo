package com.alitalia.aem.service.impl.converter.home.mmbcommonservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.SeatData;
import com.alitalia.aem.common.data.home.enumerations.AvailabilitySeatEnum;
import com.alitalia.aem.common.data.home.enumerations.TypeSeatEnum;
import com.alitalia.aem.ws.mmb.commonservice.xsd6.Seat;

@Component(immediate=true, metatype=false)
@Service(value=SeatToSeatData.class)
public class SeatToSeatData implements Converter<Seat, SeatData> {
	
	@Override
	public SeatData convert(Seat source) {
		
		SeatData destination = null;
		
		if (source != null) {
		
			destination = new SeatData();
			
			if (source.getX003CAvailabilityX003EKBackingField() != null) {
				destination.setAvailability(
						AvailabilitySeatEnum.fromValue(
								source.getX003CAvailabilityX003EKBackingField().value()));
			}
			
			destination.setNumber(
					source.getX003CNumberX003EKBackingField());
			
			destination.setPassengerIndex(
					source.getX003CPassengerIndexX003EKBackingField());
			
			if (source.getX003CTypeX003EKBackingField() != null) {
				destination.setTypeSeat(
						TypeSeatEnum.fromValue(
								source.getX003CTypeX003EKBackingField().value()));
			}
			
		}
		
		return destination;
		
	}

}
