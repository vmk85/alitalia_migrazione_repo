package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.RetrieveAirportRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportResponse;
import com.alitalia.aem.common.messages.home.RetrieveFlightDetailsRequest;
import com.alitalia.aem.common.messages.home.RetrieveFlightDetailsResponse;
import com.alitalia.aem.common.messages.home.RetrieveFlightInfoRequest;
import com.alitalia.aem.common.messages.home.RetrieveFlightInfoResponse;
import com.alitalia.aem.common.messages.home.RetrieveFullItinerariesRequest;
import com.alitalia.aem.common.messages.home.RetrieveFullItinerariesResponse;
import com.alitalia.aem.common.messages.home.RetrieveItinerariesRequest;
import com.alitalia.aem.common.messages.home.RetrieveItinerariesResponse;
import com.alitalia.aem.service.api.home.FlightStatusService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.flightstatusservice.GetAirportInfoModelResponseToRetrieveAirportResponse;
import com.alitalia.aem.service.impl.converter.home.flightstatusservice.GetFlightDetailsModelResponseToRetrieveFlightDetailsResponse;
import com.alitalia.aem.service.impl.converter.home.flightstatusservice.GetFlightInfoModelResponseToRetrieveFlightInfoResponseConverter;
import com.alitalia.aem.service.impl.converter.home.flightstatusservice.GetItineraryListModelFullResponseToRetrieveFullItinerariesResponse;
import com.alitalia.aem.service.impl.converter.home.flightstatusservice.GetItineraryListModelResponseToRetrieveItinerariesResponse;
import com.alitalia.aem.service.impl.converter.home.flightstatusservice.RetrieveAirportRequestToGetAirportInfoModelRequest;
import com.alitalia.aem.service.impl.converter.home.flightstatusservice.RetrieveFlightDetailsRequestToGetFlightDetailsModelRequest;
import com.alitalia.aem.service.impl.converter.home.flightstatusservice.RetrieveFlightInfoRequestToGetFlightInfoModelRequest;
import com.alitalia.aem.service.impl.converter.home.flightstatusservice.RetrieveFullItinerariesRequestToGetItineraryListModelFullRequest;
import com.alitalia.aem.service.impl.converter.home.flightstatusservice.RetrieveItinerariesRequestToGetItineraryListModelRequest;
import com.alitalia.aem.ws.booking.flightstatusservice.FlightStatusServiceClient;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetAirportInfoModelRequest;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetAirportInfoModelResponse;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetFlightDetailsModelRequest;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetFlightDetailsModelResponse;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetFlightInfoModelRequest;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetFlightInfoModelResponse;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetIteneraryListModelFullRequest;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetIteneraryListModelFullResponse;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetItineraryListModelRequest;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd2.GetItineraryListModelResponse;

@Service
@Component(immediate = true, metatype = false)
public class SimpleFlightStatusService implements FlightStatusService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleFlightStatusService.class);

	@Reference
	private FlightStatusServiceClient flightStatusServiceClient;

	@Reference
	private RetrieveFlightInfoRequestToGetFlightInfoModelRequest retrieveFlightInfoRequestConverter;
	
	@Reference
	private GetFlightInfoModelResponseToRetrieveFlightInfoResponseConverter retrieveFlightInfoResponseConverter;

	@Reference
	private RetrieveFlightDetailsRequestToGetFlightDetailsModelRequest retrieveFlightDetailsRequestConverter;

	@Reference
	private GetFlightDetailsModelResponseToRetrieveFlightDetailsResponse retrieveFlightDetailsResponseConverter;
	
	@Reference
	private RetrieveItinerariesRequestToGetItineraryListModelRequest retrieveItinerariesRequestConverter;

	@Reference
	private GetItineraryListModelResponseToRetrieveItinerariesResponse retrieveItinerariesResponseConverter;
	
	@Reference
	private RetrieveFullItinerariesRequestToGetItineraryListModelFullRequest retrieveFullItinerariesRequestConverter;

	@Reference
	private GetItineraryListModelFullResponseToRetrieveFullItinerariesResponse retrieveFullItinerariesResponseConverter;

	@Reference
	private RetrieveAirportRequestToGetAirportInfoModelRequest retrieveAirportRequestConverter;

	@Reference
	private GetAirportInfoModelResponseToRetrieveAirportResponse retrieveAirportResponseConverter;

	
	@Override
	public RetrieveFlightInfoResponse retrieveFlightInfo(RetrieveFlightInfoRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveFlightInfo. The request is {}", request);
		}
		

		try {

			RetrieveFlightInfoResponse response = new RetrieveFlightInfoResponse();
			GetFlightInfoModelRequest flightInfoModelRequest = retrieveFlightInfoRequestConverter.convert(request);
			GetFlightInfoModelResponse flightInfoModelResponse = flightStatusServiceClient.getFlightInfoModel(flightInfoModelRequest);
			response = retrieveFlightInfoResponseConverter.convert(flightInfoModelResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method retrieveFlightInfo. The response is {}", response);
			}
		
			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method retrieveFlightInfo.", e);
			throw new AlitaliaServiceException("Exception executing retrieveFlightInfo: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public RetrieveFlightDetailsResponse retrieveFlightDetails(RetrieveFlightDetailsRequest request) {
		
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveFlightDetails. The request is {}", request);
		}
		

		try {

			RetrieveFlightDetailsResponse response = new RetrieveFlightDetailsResponse();
			GetFlightDetailsModelRequest flightDetailsModelRequest = retrieveFlightDetailsRequestConverter.convert(request);
			GetFlightDetailsModelResponse flightDetailsModelResponse = flightStatusServiceClient.getFlightDetailsModel(flightDetailsModelRequest);
			response = retrieveFlightDetailsResponseConverter.convert(flightDetailsModelResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method retrieveFlightDetails. The response is {}", response);
			}
			
			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method retrieveFlightDetails.", e);
			throw new AlitaliaServiceException("Exception executing retrieveFlightDetails: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public RetrieveItinerariesResponse retrieveItineraries(RetrieveItinerariesRequest request) {
		
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveItineraries. The request is {}", request);
		}
		

		try {

			RetrieveItinerariesResponse response = new RetrieveItinerariesResponse();
			GetItineraryListModelRequest getItineraryListModelRequest = retrieveItinerariesRequestConverter.convert(request);
			GetItineraryListModelResponse getItineraryListModelResponse = flightStatusServiceClient.getItineraryListModel(getItineraryListModelRequest);
			response = retrieveItinerariesResponseConverter.convert(getItineraryListModelResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method retrieveItineraries. The response is {}", response);
			}
			
			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method retrieveItineraries.", e);
			throw new AlitaliaServiceException("Exception executing retrieveItineraries: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public RetrieveFullItinerariesResponse retrieveFullItineraries(RetrieveFullItinerariesRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveFullItineraries. The request is {}", request);
		}
		

		try {

			RetrieveFullItinerariesResponse response = new RetrieveFullItinerariesResponse();
			GetIteneraryListModelFullRequest getItineraryListModelFullRequest = retrieveFullItinerariesRequestConverter.convert(request);
			GetIteneraryListModelFullResponse getItineraryListModelFullResponse = flightStatusServiceClient.getIteneraryListModelFull(getItineraryListModelFullRequest);
			response = retrieveFullItinerariesResponseConverter.convert(getItineraryListModelFullResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method retrieveFullItineraries. The response is {}", response);
			}
			
			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method retrieveFullItineraries.", e);
			throw new AlitaliaServiceException("Exception executing retrieveFullItineraries: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public RetrieveAirportResponse retrieveAirport(RetrieveAirportRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveAirport. The request is {}", request);
		}
		

		try {

			RetrieveAirportResponse response = new RetrieveAirportResponse();
			GetAirportInfoModelRequest getAirportInfoModelRequest = retrieveAirportRequestConverter.convert(request);
			GetAirportInfoModelResponse getAirportInfoModelResponse = flightStatusServiceClient.getAirportInfoModel(getAirportInfoModelRequest);
			response = retrieveAirportResponseConverter.convert(getAirportInfoModelResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method retrieveAirport. The response is {}", response);
			}
			
			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method retrieveAirport.", e);
			throw new AlitaliaServiceException("Exception executing retrieveAirport: Sid ["+request.getSid()+"]" , e);
		}
	}
}