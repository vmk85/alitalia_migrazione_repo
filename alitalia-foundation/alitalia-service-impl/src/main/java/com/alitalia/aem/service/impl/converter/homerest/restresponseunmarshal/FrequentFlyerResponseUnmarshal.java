package com.alitalia.aem.service.impl.converter.homerest.restresponseunmarshal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.frequentflyer.FrequentFlyers;
import com.alitalia.aem.common.messages.home.GetFrequentFlyerResponse;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class FrequentFlyerResponseUnmarshal {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public GetFrequentFlyerResponse ffUnmarshal (String jsonString) {
		
		GetFrequentFlyerResponse response = new GetFrequentFlyerResponse();
		FrequentFlyers ff = new FrequentFlyers();
		Gson gson = new Gson();
		
		try {
			ff = gson.fromJson(jsonString, FrequentFlyers.class);
			response.setFrequentFlyers(ff);
		}
		catch (JsonSyntaxException e) {
			e.printStackTrace();
			logger.debug("JsonSyntaxException in FrequentFlyerResponseUnmarshal");
		}
		
		return response;
	}
}
