package com.alitalia.aem.service.impl.converter.home.checkinextrabaggageservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.ExtraBaggageOrderCheckinRequest;
import com.alitalia.aem.ws.checkin.extrabaggageservice.xsd1.ExtraBaggageOrderRequest;
import com.alitalia.aem.ws.checkin.extrabaggageservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.extrabaggageservice.xsd1.Resource;


@Component(immediate=true, metatype=false)
@Service(value=ExtraBaggageOrderCheckinRequestToExtraBaggageOrderRequest.class)
public class ExtraBaggageOrderCheckinRequestToExtraBaggageOrderRequest implements Converter<ExtraBaggageOrderCheckinRequest, ExtraBaggageOrderRequest> {

	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;
	
	@Reference
	private CheckinFlightDataToFlight checkinFlightDataConverter;

	@Override
	public ExtraBaggageOrderRequest convert(ExtraBaggageOrderCheckinRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		ExtraBaggageOrderRequest destination = null;

		if (source != null) {
			destination = objectFactory.createExtraBaggageOrderRequest();
			
			destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			destination.setX003CResourceX003EKBackingField(Resource.WEB);
			
			destination.setEticket(objectFactory.createExtraBaggageOrderRequestEticket(source.getEticket()));
			destination.setFlight(objectFactory.createExtraBaggageOrderRequestFlight(checkinFlightDataConverter.convert(source.getFlight())));		
		}

		return destination;
	}


}
