/**
 * 
 */
package com.alitalia.aem.service.impl.converter.home.checkinservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.UndoCheckinPassengerData;
import com.alitalia.aem.common.messages.home.WebCheckinUndoManyCheckinRequest;
import com.alitalia.aem.ws.checkin.checkinservice.xsd1.ArrayOfUndoRequestPassenger;
import com.alitalia.aem.ws.checkin.checkinservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.checkinservice.xsd1.Resource;
import com.alitalia.aem.ws.checkin.checkinservice.xsd1.UndoManyCheckInRequest;

@Component(immediate=true, metatype=false)
@Service(value=WebCheckinUndoManyCheckinRequestToUndoManyCheckInRequest.class)
public class WebCheckinUndoManyCheckinRequestToUndoManyCheckInRequest implements Converter<WebCheckinUndoManyCheckinRequest, UndoManyCheckInRequest> {

	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;
	
	@Reference
	private CheckinRouteDataToRoute checkinRouteDataConverter;
	
	@Reference
	private UndoCheckinPassengerDataToUndoCheckinPassenger undoCheckinPassengerDataConverter;

	@Override
	public UndoManyCheckInRequest convert(WebCheckinUndoManyCheckinRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		UndoManyCheckInRequest destination = null;

		if (source != null) {
			
			destination = objectFactory.createUndoManyCheckInRequest();
			destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			destination.setX003CResourceX003EKBackingField(Resource.WEB);
			
			ArrayOfUndoRequestPassenger passengersList = objectFactory.createArrayOfUndoRequestPassenger();
			if (source.getPassengers()!=null){
				for (UndoCheckinPassengerData passengerData : source.getPassengers()) {
					passengersList.getUndoRequestPassenger().add(undoCheckinPassengerDataConverter.convert(passengerData));
				}
			}
			destination.setPassengers(objectFactory.createUndoManyCheckInRequestPassengers(passengersList));
			destination.setRoute(objectFactory.createUndoManyCheckInRequestRoute(checkinRouteDataConverter.convert(source.getRoute())));
		}
		return destination;
	}
}
