package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryMealDetailData;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.MealDetail;


@Component(immediate=true, metatype=false)
@Service(value=MmbAncillaryMealDetailDataToMealDetail.class)
public class MmbAncillaryMealDetailDataToMealDetail implements Converter<MmbAncillaryMealDetailData, MealDetail> {

	@Override
	public MealDetail convert(MmbAncillaryMealDetailData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		MealDetail destination = null;

		if (source != null) {
			destination = objectFactory.createMealDetail();

			destination.setX003CMealX003EKBackingField(source.getMeal());
		}

		return destination;
	}

}
