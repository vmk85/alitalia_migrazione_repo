package com.alitalia.aem.service.impl.converter.home.checkincomfortseatservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.messages.home.ChangeCabinCheckinResponse;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd1.ChangeCabinResponse;
import com.alitalia.aem.ws.checkin.comfortseatservice.xsd4.ArrayOfstring;


@Component(immediate=true, metatype=false)
@Service(value=ChangeCabinResponseToChangeCabinCheckinResponse.class)
public class ChangeCabinResponseToChangeCabinCheckinResponse implements Converter<ChangeCabinResponse, ChangeCabinCheckinResponse> {


	@Override
	public ChangeCabinCheckinResponse convert(ChangeCabinResponse source) {
		ChangeCabinCheckinResponse destination = null;

		if (source != null) {
			destination = new ChangeCabinCheckinResponse();

			JAXBElement<ArrayOfstring> sourceMessages = source.getMessages();
			if(sourceMessages != null &&
					sourceMessages.getValue() != null &&
					sourceMessages.getValue().getString() != null &&
					!sourceMessages.getValue().getString().isEmpty()){
				List<String> messagesList = new ArrayList<String>();
				for(String sourceMessage : sourceMessages.getValue().getString()){
					messagesList.add(sourceMessage);
				}
				destination.setMessages(messagesList);
			}
			if(source.getSeat() != null){
				destination.setSeat(source.getSeat().getValue());
			}
			if(source.getSeatClass() != null){
				destination.setSeatClass(MmbCompartimentalClassEnum.fromValue(source.getSeatClass().value()));
			}
			if(source.getSeatClassName() != null){
				destination.setSeatClassName(source.getSeatClassName().getValue());
			}
			destination.setSucceeded(source.isSucceeded());
		}

		return destination;
	}

}
