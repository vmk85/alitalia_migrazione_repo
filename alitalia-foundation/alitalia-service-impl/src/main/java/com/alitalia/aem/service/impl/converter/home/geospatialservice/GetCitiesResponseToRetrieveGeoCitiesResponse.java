package com.alitalia.aem.service.impl.converter.home.geospatialservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.CodeDescriptionData;
import com.alitalia.aem.common.messages.home.RetrieveGeoCitiesResponse;
import com.alitalia.aem.ws.geospatial.service.xsd1.GetCitiesResponse;
import com.alitalia.aem.ws.geospatial.service.xsd6.ArrayOfCodeDescription;
import com.alitalia.aem.ws.geospatial.service.xsd6.CodeDescription;

@Component(immediate = true, metatype = false)
@Service(value = GetCitiesResponseToRetrieveGeoCitiesResponse.class)
public class GetCitiesResponseToRetrieveGeoCitiesResponse implements Converter<GetCitiesResponse, RetrieveGeoCitiesResponse> {

	@Reference
	private GeoAirportToAirportData geoAirportConverter;
	
	@Override
	public RetrieveGeoCitiesResponse convert(GetCitiesResponse source) {
		
		RetrieveGeoCitiesResponse response = new RetrieveGeoCitiesResponse();
		
		JAXBElement<ArrayOfCodeDescription> jaxbCities = source.getCities();
		ArrayOfCodeDescription arrayOfAnyType = jaxbCities.getValue();
		List<CodeDescription> cities = arrayOfAnyType.getCodeDescription();
		
		List<CodeDescriptionData> codesDescriptionData = new ArrayList<>();
		for(CodeDescription codeDescription: cities){
			CodeDescriptionData codeDescriptionData = new CodeDescriptionData();
			codeDescriptionData.setCode(codeDescription.getCode().getValue());
			codeDescriptionData.setDescription(codeDescription.getDescription().getValue());
			
			codesDescriptionData.add(codeDescriptionData);
		}
		
		response.setCities(codesDescriptionData);
		
		return response;
	}
}