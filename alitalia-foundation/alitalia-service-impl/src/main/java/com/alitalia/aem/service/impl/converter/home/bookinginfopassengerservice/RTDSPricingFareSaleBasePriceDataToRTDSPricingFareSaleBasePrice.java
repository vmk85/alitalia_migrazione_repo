package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingFareSaleBasePriceData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ObjectFactory;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionPricingFareSaleBasePrice;

@Component(immediate=true, metatype=false)
@Service(value=RTDSPricingFareSaleBasePriceDataToRTDSPricingFareSaleBasePrice.class)
public class RTDSPricingFareSaleBasePriceDataToRTDSPricingFareSaleBasePrice implements
		Converter<ResultTicketingDetailSolutionPricingFareSaleBasePriceData, 
					ResultTicketingDetailSolutionPricingFareSaleBasePrice> {

	@Override
	public ResultTicketingDetailSolutionPricingFareSaleBasePrice convert(
			ResultTicketingDetailSolutionPricingFareSaleBasePriceData source) {
		ResultTicketingDetailSolutionPricingFareSaleBasePrice destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionPricingFareSaleBasePrice();

			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}

		return destination;
	}

}
