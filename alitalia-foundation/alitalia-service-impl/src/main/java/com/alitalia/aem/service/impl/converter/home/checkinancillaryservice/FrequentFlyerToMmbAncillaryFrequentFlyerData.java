package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryFrequentFlyerData;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.FrequentFlyer;


@Component(immediate=true, metatype=false)
@Service(value=FrequentFlyerToMmbAncillaryFrequentFlyerData.class)
public class FrequentFlyerToMmbAncillaryFrequentFlyerData implements
		Converter<FrequentFlyer, MmbAncillaryFrequentFlyerData> {

	@Override
	public MmbAncillaryFrequentFlyerData convert(FrequentFlyer source) {
		MmbAncillaryFrequentFlyerData destination = null;

		if (source != null) {
			destination = new MmbAncillaryFrequentFlyerData();

			destination.setCode(source.getX003CCodeX003EKBackingField());
			destination.setFfType(source.getX003CFFTypeX003EKBackingField());
			destination.setTier(source.getX003CTierX003EKBackingField());
		}

		return destination;
	}

}
