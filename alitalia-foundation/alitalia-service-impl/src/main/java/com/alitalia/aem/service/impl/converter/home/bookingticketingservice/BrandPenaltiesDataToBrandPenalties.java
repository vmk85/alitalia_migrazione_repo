package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.BrandPenaltiesData;
import com.alitalia.aem.common.data.home.BrandPenaltyData;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.BrandPenalties;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.ObjectFactory;

@Component(immediate = true, metatype = false)
@Service(value = BrandPenaltiesDataToBrandPenalties.class)
public class BrandPenaltiesDataToBrandPenalties implements Converter<BrandPenaltiesData, BrandPenalties> {

	@Reference
	private BrandPenaltyDataToBrandPenalty brandPenaltyDataConveter;

	@Override
	public BrandPenalties convert(BrandPenaltiesData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		BrandPenalties destination = objectFactory.createBrandPenalties();
		BrandPenaltyData penalty = null;

		penalty = source.getChangeAfterDepature();
		if (penalty != null) {
			destination.setChangeAfterDepature(objectFactory
					.createBrandPenaltiesChangeAfterDepature(brandPenaltyDataConveter.convert(penalty)));
		} else {
			destination.setChangeAfterDepature(null);
		}

		penalty = source.getChangeBeforeDepature();
		if (penalty != null) {
			destination.setChangeBeforeDepature(objectFactory
					.createBrandPenaltiesChangeBeforeDepature(brandPenaltyDataConveter.convert(penalty)));
		} else {
			destination.setChangeBeforeDepature(null);
		}

		penalty = source.getRefundAfterDepature();
		if (penalty != null) {
			destination.setRefundAfterDepature(objectFactory
					.createBrandPenaltiesRefundAfterDepature(brandPenaltyDataConveter.convert(penalty)));
		} else {
			destination.setRefundAfterDepature(null);
		}
		
		penalty = source.getRefundBeforeDepature();
		if (penalty != null) {
			destination.setRefundBeforeDepature(objectFactory
					.createBrandPenaltiesRefundBeforeDepature(brandPenaltyDataConveter.convert(penalty)));
		} else {
			destination.setRefundBeforeDepature(null);
		}
		
		return destination;
	}

}
