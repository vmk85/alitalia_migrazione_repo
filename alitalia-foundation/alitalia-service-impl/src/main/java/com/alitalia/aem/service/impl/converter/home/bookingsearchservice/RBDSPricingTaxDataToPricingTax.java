package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingTaxData;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ResultBookingDetailsSolutionPricingTax;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingTaxDataToPricingTax.class)
public class RBDSPricingTaxDataToPricingTax implements Converter<ResultBookingDetailsSolutionPricingTaxData, ResultBookingDetailsSolutionPricingTax> {

	@Reference
	RBDSPricingTaxPreviousSalePriceDataToPricingTaxPreviousSalePrice pricingTaxPreviousSalePriceConverter;
	
	@Reference
	RBDSPricingTaxSalePriceDataToPricingTaxSalePrice pricingTaxSalePriceConverter;

	@Override
	public ResultBookingDetailsSolutionPricingTax convert(
			ResultBookingDetailsSolutionPricingTaxData source) {
		ResultBookingDetailsSolutionPricingTax destination = null;
		if(source!=null){
			ObjectFactory objf = new ObjectFactory();
			destination = objf.createResultBookingDetailsSolutionPricingTax();
			destination.setCodeField(source.getCodeField());
			destination.setPreviousSalePriceField(pricingTaxPreviousSalePriceConverter.convert(source.getPreviousSalePriceField()));
			destination.setSalePriceField(pricingTaxSalePriceConverter.convert(source.getSalePriceField()));
		}
		return destination;
	}

}
