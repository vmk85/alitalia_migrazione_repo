package com.alitalia.aem.service.impl.converter.home.businessloginservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.AgencySubscriptionResponse;
import com.alitalia.aem.ws.salesagentsubstrade.xsd.SalesAgentSubscriptionOutput;

@Component
@Service(value=SalesAgentSubscriptionOutputToAgencySubscriptionResponse.class)
public class SalesAgentSubscriptionOutputToAgencySubscriptionResponse implements Converter<SalesAgentSubscriptionOutput, AgencySubscriptionResponse> {

	@Override
	public AgencySubscriptionResponse convert(SalesAgentSubscriptionOutput source) {
		
		AgencySubscriptionResponse destination = new AgencySubscriptionResponse();

		destination.setErrorCode(source.getERRORCODE());
		destination.setErrorMessage(source.getERRORMESSAGE());

		return destination;
	}
}