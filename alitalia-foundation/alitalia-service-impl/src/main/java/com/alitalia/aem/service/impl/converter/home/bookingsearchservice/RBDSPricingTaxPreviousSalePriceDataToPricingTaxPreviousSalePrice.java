package com.alitalia.aem.service.impl.converter.home.bookingsearchservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingTaxPreviousSalePriceData;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ResultBookingDetailsSolutionPricingTaxPreviousSalePrice;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingTaxPreviousSalePriceDataToPricingTaxPreviousSalePrice.class)
public class RBDSPricingTaxPreviousSalePriceDataToPricingTaxPreviousSalePrice
		implements Converter<ResultBookingDetailsSolutionPricingTaxPreviousSalePriceData, ResultBookingDetailsSolutionPricingTaxPreviousSalePrice> {

	@Override
	public ResultBookingDetailsSolutionPricingTaxPreviousSalePrice convert(
			ResultBookingDetailsSolutionPricingTaxPreviousSalePriceData source) {
		ResultBookingDetailsSolutionPricingTaxPreviousSalePrice destination = null;
		if(source != null){
			ObjectFactory objf = new ObjectFactory();
			destination = objf.createResultBookingDetailsSolutionPricingTaxPreviousSalePrice();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}

}
