package com.alitalia.aem.service.impl.converter.home.mmbancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillarySeatDetailData;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.SeatDetail;
import com.alitalia.aem.ws.mmb.ancillaryservice.xsd2.ObjectFactory;

@Component(immediate=true, metatype=false)
@Service(value=MmbAncillarySeatDetailDataToSeatDetail.class)
public class MmbAncillarySeatDetailDataToSeatDetail implements Converter<MmbAncillarySeatDetailData, SeatDetail> {

	@Override
	public SeatDetail convert(MmbAncillarySeatDetailData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		SeatDetail destination = null;

		if (source != null) {
			destination = objectFactory.createSeatDetail();

			destination.setX003CIsComfortX003EKBackingField(source.isComfort());
			destination.setX003CSeatX003EKBackingField(source.getSeat());
		}

		return destination;
	}

}
