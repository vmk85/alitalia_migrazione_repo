package com.alitalia.aem.service.impl.converter.home.checkinservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.UndoCheckinPassengerData;
import com.alitalia.aem.ws.checkin.checkinservice.xsd1.UndoRequestPassenger;
import com.alitalia.aem.ws.checkin.checkinservice.xsd4.ArrayOfstring;


@Component(immediate=true, metatype=false)
@Service(value=UndoRequestPassengerToUndoCheckinPassengerData.class)
public class UndoRequestPassengerToUndoCheckinPassengerData implements Converter<UndoRequestPassenger, UndoCheckinPassengerData> {

	@Override
	public UndoCheckinPassengerData convert(UndoRequestPassenger source) {
		UndoCheckinPassengerData destination = null;

		if (source != null) {
			destination = new UndoCheckinPassengerData();

			destination.setEticket(source.getEticket().getValue());
			destination.setLastname(source.getLastname().getValue());
			destination.setName(source.getName().getValue());
			
			JAXBElement<ArrayOfstring> messages = source.getMessages();

			List<String> messList = null;
			if (messages != null && messages.getValue() != null &&
				messages.getValue().getString() != null && !messages.getValue().getString().isEmpty()) {
				messList = new ArrayList<String>();
				for (String mess : messages.getValue().getString()) {
					messList.add(mess);
				}
				destination.setMessages(messList);
			}
		}
		return destination;
	}

}
