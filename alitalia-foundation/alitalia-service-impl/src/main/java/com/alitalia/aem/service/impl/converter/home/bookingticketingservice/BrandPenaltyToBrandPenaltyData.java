package com.alitalia.aem.service.impl.converter.home.bookingticketingservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.BrandPenaltyData;
import com.alitalia.aem.ws.booking.ticketservice.xsd5.BrandPenalty;

@Component(immediate = true, metatype = false)
@Service(value = BrandPenaltyToBrandPenaltyData.class)
public class BrandPenaltyToBrandPenaltyData implements Converter<BrandPenalty, BrandPenaltyData> {

	@Override
	public BrandPenaltyData convert(BrandPenalty source) {
		BrandPenaltyData destination = new BrandPenaltyData();

		destination.setMaxPrice(source.getMaxPrice().getValue());
		destination.setMaxPriceCurrency(source.getMaxPriceCurrency().getValue());
		destination.setMinPrice(source.getMinPrice().getValue());
		destination.setMinPriceCurrency(source.getMinPriceCurrency().getValue());
		destination.setPermitted(source.isPermitted());

		return destination;
	}

}
