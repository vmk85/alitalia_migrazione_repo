package com.alitalia.aem.service.impl.home;

import com.alitalia.aem.ws.booking.ticketservice.SabreDcTicketingServiceClient;
import com.day.cq.wcm.api.Page;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.CarnetSendNotificationEmailRequest;
import com.alitalia.aem.common.messages.home.CarnetSendNotificationEmailResponse;
import com.alitalia.aem.common.messages.home.CrossSellingsRequest;
import com.alitalia.aem.common.messages.home.CrossSellingsResponse;
import com.alitalia.aem.common.messages.home.SapInvoiceRequest;
import com.alitalia.aem.common.messages.home.SapInvoiceResponse;
import com.alitalia.aem.service.api.home.BookingTicketService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.bookingticketingservice.CarnetSendNotificationEmailRequestToSendCarnetNotificationEmailRequest;
import com.alitalia.aem.service.impl.converter.home.bookingticketingservice.CrossSellingRequestToGetCrossSellingRequest;
import com.alitalia.aem.service.impl.converter.home.bookingticketingservice.GetCrossSellingsResponseToCrossSellingsResponse;
import com.alitalia.aem.service.impl.converter.home.bookingticketingservice.SapInvoiceRequestToSAPRequest;
import com.alitalia.aem.ws.booking.ticketservice.TicketingServiceClient;
import com.alitalia.aem.ws.booking.ticketservice.xsd1.GetCrossSellingsRequest;
import com.alitalia.aem.ws.booking.ticketservice.xsd1.GetCrossSellingsResponse;
import com.alitalia.aem.ws.booking.ticketservice.xsd1.SAPRequest;
import com.alitalia.aem.ws.booking.ticketservice.xsd1.SAPResponse;
import com.alitalia.aem.ws.booking.ticketservice.xsd1.SendCarnetNotificationEmailRequest;
import com.alitalia.aem.ws.booking.ticketservice.xsd1.SendCarnetNotificationEmailResponse;

@Service
@Component(immediate = true, metatype = false)
public class SimpleBookingTicketService implements BookingTicketService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleBookingTicketService.class);

	@Reference
	private TicketingServiceClient ticketingServiceClient;

	@Reference
	private CrossSellingRequestToGetCrossSellingRequest crossSellingRequestToGetCrossSellingRequestConverter;

	@Reference
	private GetCrossSellingsResponseToCrossSellingsResponse getCrossSellingsResponseToCrossSellingsResponseConverter;

	@Reference
	private SapInvoiceRequestToSAPRequest sapRequestConverter;
	
	@Reference
	private CarnetSendNotificationEmailRequestToSendCarnetNotificationEmailRequest carnetSendNotificationEmailRequestConverter;

	/** Migrazione 3.6 inizio*/

	@Reference
	private ResourceResolverFactory resolverFactory;

	@Reference
	private SabreDcTicketingServiceClient sabreDcTicketingServiceClient;

	/** Migrazione 3.6 fine*/
	@Override
	public CrossSellingsResponse getCrossSellings(CrossSellingsRequest request) {
		
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method getCrossSellings. The request is {}", request);
		}
		

		try {
			
			CrossSellingsResponse response = new CrossSellingsResponse();
			GetCrossSellingsRequest getCrossSellingsRequest = crossSellingRequestToGetCrossSellingRequestConverter
					.convert(request);

			/** Migrazione 3.6 inizio*/

			GetCrossSellingsResponse getCrossSellingsResponse = null;
			if (newSabreEnable(getCrossSellingsRequest.getMarketCode().getValue().toLowerCase())){
				getCrossSellingsResponse = sabreDcTicketingServiceClient.getCrossSellings(getCrossSellingsRequest);
			} else {
				getCrossSellingsResponse = ticketingServiceClient.getCrossSellings(getCrossSellingsRequest);
			}

			/** Migrazione 3.6 fine*/

//			GetCrossSellingsResponse getCrossSellingsResponse = ticketingServiceClient
//					.getCrossSellings(getCrossSellingsRequest);
			response = getCrossSellingsResponseToCrossSellingsResponseConverter.convert(getCrossSellingsResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			return response;
			
		} catch (Exception e) {
			logger.error("Exception while executing method getCrossSellings(): {}", e);
			throw new AlitaliaServiceException("Exception executing getCrossSellings: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public SapInvoiceResponse callSAPService(SapInvoiceRequest request) {
		
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method callSAPService. The request is {}", request);
		}
		
		
		try {
			
			SapInvoiceResponse response = new SapInvoiceResponse();
			SAPRequest sapRequest = sapRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			SAPResponse callSAPServiceResponse = null;
			if (newSabreEnable("it")){
				callSAPServiceResponse = sabreDcTicketingServiceClient.callSAPService(sapRequest);
			} else {
				callSAPServiceResponse = ticketingServiceClient.callSAPService(sapRequest);
			}

			/** Migrazione 3.6 fine*/

//			SAPResponse callSAPServiceResponse = ticketingServiceClient.callSAPService(sapRequest);
			/*
			 * La SAPResponse è banale, effettuo la conversione direttamente qui
			 */
			response.setResult(callSAPServiceResponse.getResult().getValue());
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			return response;
			
		} catch (Exception e) {
			logger.error("Exception while executing method callSAPService(): {}", e);
			throw new AlitaliaServiceException("Exception executing callSAPService: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public CarnetSendNotificationEmailResponse sendCarnetNotificationEmail(CarnetSendNotificationEmailRequest request) {
		
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method sendCarnetNotificationEmail. The request is {}", request);
		}

		try {
			
			CarnetSendNotificationEmailResponse response = new CarnetSendNotificationEmailResponse();
			SendCarnetNotificationEmailRequest sendCarnetNotificationEmailRequest = carnetSendNotificationEmailRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			SendCarnetNotificationEmailResponse sendCarnetNotificationEmailResponse = null;
			if (newSabreEnable(sendCarnetNotificationEmailRequest.getMarketCode().getValue().toLowerCase())){
				sendCarnetNotificationEmailResponse = sabreDcTicketingServiceClient.sendCarnetNotificationEmail(sendCarnetNotificationEmailRequest);
			} else {
				sendCarnetNotificationEmailResponse = ticketingServiceClient.sendCarnetNotificationEmail(sendCarnetNotificationEmailRequest);
			}

			/** Migrazione 3.6 fine*/

//			SendCarnetNotificationEmailResponse sendCarnetNotificationEmailResponse = ticketingServiceClient.sendCarnetNotificationEmail(sendCarnetNotificationEmailRequest);
			if (sendCarnetNotificationEmailResponse != null && sendCarnetNotificationEmailResponse.isWasSend() != null)
				response.setHaveSendEmail(sendCarnetNotificationEmailResponse.isWasSend());
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			return response;
			
		} catch (Exception e) {
			logger.error("Exception while executing method sendCarnetNotificationEmail(): {}", e);
			throw new AlitaliaServiceException("Exception executing sendCarnetNotificationEmail: Sid ["+request.getSid()+"]" , e);
		}
	}


	/** Migrazione 3.6 inizio*/

	private boolean newSabreEnable(String market) throws LoginException {

		ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
		Resource res = resourceResolver.getResource("/content/alitalia/alitalia-" + market);
		Page page = res.adaptTo(Page.class);
		ValueMap props = page.getProperties();
		String title = props.get("isNewService", "default value");
		logger.info("dato preso dalla pagina: " + title);
		return Boolean.parseBoolean(title);

	}

	/** Migrazione 3.6 fine*/

}