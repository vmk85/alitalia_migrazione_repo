package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveSecureQuestionListRequest;
import com.alitalia.aem.common.messages.home.RetrieveStateListRequest;
import com.alitalia.aem.ws.mypersonalarea.staticdataservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mypersonalarea.staticdataservice.xsd1.StaticDataRequest;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveSecureQuestionListRequestToStaticDataRequest.class)
public class RetrieveSecureQuestionListRequestToStaticDataRequest implements Converter<RetrieveSecureQuestionListRequest, StaticDataRequest> {

	@Reference
	private MMCustomerProfileDataToCustomerProfile mmCustomerProfileDataConverter;
	
	@Override
	public StaticDataRequest convert(RetrieveSecureQuestionListRequest source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		
		ObjectFactory objectFactory = new ObjectFactory();
		
		StaticDataRequest destination = objectFactory.createStaticDataRequest();
		destination.setItemCache(objectFactory.createStaticDataRequestItemCache(source.getItemCache()));
		destination.setLanguageCode(objectFactory.createStaticDataRequestLanguageCode(source.getLanguageCode()));
		destination.setMarket(objectFactory.createStaticDataRequestMarket(source.getMarket()));
		
		return destination;
	}
}