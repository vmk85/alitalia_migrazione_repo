//2
package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingCocFareTotalData;
import com.alitalia.aem.ws.booking.paymentservice.xsd11.ResultBookingDetailsSolutionPricingCocFareTotal;

@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingCocFareTotalToPricingCocFareTotalData.class)
public class RBDSPricingCocFareTotalToPricingCocFareTotalData implements
		Converter<ResultBookingDetailsSolutionPricingCocFareTotal, ResultBookingDetailsSolutionPricingCocFareTotalData> {

	@Override
	public ResultBookingDetailsSolutionPricingCocFareTotalData convert(
			ResultBookingDetailsSolutionPricingCocFareTotal source) {
		ResultBookingDetailsSolutionPricingCocFareTotalData destination = null;
		
		if(source != null){
			destination = new ResultBookingDetailsSolutionPricingCocFareTotalData();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}

}
