package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMContractData;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Contract;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ContractStatus;

@Component(immediate = true, metatype = false)
@Service(value = MMContractDataToContract.class)
public class MMContractDataToContract implements Converter<MMContractData, Contract> {

	@Override
	public Contract convert(MMContractData source) {
		Contract destination = null;

		if (source != null) {
			com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ObjectFactory objectFactory2 = new com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ObjectFactory();

			destination = objectFactory2.createContract();
			destination.setName(objectFactory2.createContractName(source.getName()));
			destination.setStatus(ContractStatus.fromValue(source.getStatus().value()));
		}

		return destination;
	}

}
