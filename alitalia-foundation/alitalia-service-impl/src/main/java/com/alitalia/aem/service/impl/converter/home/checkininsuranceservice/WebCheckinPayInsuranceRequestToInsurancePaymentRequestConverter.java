package com.alitalia.aem.service.impl.converter.home.checkininsuranceservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.messages.home.WebCheckinPayInsuranceRequest;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd1.InsurancePaymentRequest;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd2.ArrayOfPassenger;
import com.alitalia.aem.ws.checkin.insuranceservice.xsd2.LegType;





@Component(immediate=true, metatype=false)
@Service(value=WebCheckinPayInsuranceRequestToInsurancePaymentRequestConverter.class)
public class WebCheckinPayInsuranceRequestToInsurancePaymentRequestConverter implements Converter<WebCheckinPayInsuranceRequest, InsurancePaymentRequest> {
	
	@Reference
	private InsurancePolicyDataToInsurancePolicy insurancePolicyDataConverter;
	
	@Reference
	private CheckinRouteDataToRoute checkinRouteConverter;
	
	@Reference
	private CheckinPassengerDataToPassenger checkinPassengerConverter;
	
	@Override
	public InsurancePaymentRequest convert(WebCheckinPayInsuranceRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();		
		InsurancePaymentRequest destination = objectFactory.createInsurancePaymentRequest();
		
		if (source.getArea()!=null)
			destination.setArea(LegType.fromValue(source.getArea().value()));
		destination.setHasReturn(source.getHasReturn());
		destination.setInsurancePolicy(objectFactory.createInsurancePaymentRequestInsurancePolicy(insurancePolicyDataConverter.convert(source.getInsurancePolicy())));
		
		destination.setRoute(objectFactory.createInsurancePaymentRequestRoute(checkinRouteConverter.convert(source.getSelectedRoute())));
		
		com.alitalia.aem.ws.checkin.insuranceservice.xsd2.ObjectFactory objectFactory2 = new com.alitalia.aem.ws.checkin.insuranceservice.xsd2.ObjectFactory();
		JAXBElement<ArrayOfPassenger> passengersList = objectFactory.createInsurancePaymentRequestPassengers(objectFactory2.createArrayOfPassenger());
		if (source.getPassengersList()!=null){
			for (CheckinPassengerData passengerData : source.getPassengersList()) {
				passengersList.getValue().getPassenger().add(checkinPassengerConverter.convert(passengerData));
			}
		}
		destination.setPassengers(passengersList);

		return destination;
	}
}