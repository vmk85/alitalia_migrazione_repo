package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryPassengerData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryFlightData;
import com.alitalia.aem.common.messages.home.WebCheckinGetCatalogRequest;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.GetCatalogRequest;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.Resource;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ArrayOfFlight;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ArrayOfPassenger;


@Component(immediate=true, metatype=false)
@Service(value=WebCheckinGetCatalogRequestToGetCatalogRequest.class)
public class WebCheckinGetCatalogRequestToGetCatalogRequest implements Converter<WebCheckinGetCatalogRequest, GetCatalogRequest> {

	@Reference
	private MmbRequestBaseInfoDataToRequestBaseInfo mmbRequestBaseInfoDataConverter;

	@Reference
	private MmbAncillaryFlightDataToFlight mmbAncillaryFlightDataConverter;

	@Reference
	private CheckinAncillaryPassengerDataToPassenger checkinAncillaryPassengerDataConverter;

	@Override
	public GetCatalogRequest convert(WebCheckinGetCatalogRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ObjectFactory objectFactory2 =
				new com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ObjectFactory();
		GetCatalogRequest destination = null;

		if (source != null) {
			destination = objectFactory.createGetCatalogRequest();

			destination.setClient(objectFactory.createGetCatalogRequestClient(source.getClient()));
			destination.setX003CInfoRequestX003EKBackingField(
					mmbRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			
			destination.setX003CResourceX003EKBackingField(Resource.WEB);

			destination.setLanguage(objectFactory.createGetCatalogRequestLanguage(source.getLanguage()));
			destination.setMachineName(objectFactory.createGetCatalogRequestMachineName(source.getMachineName()));
			destination.setMarket(objectFactory.createGetCatalogRequestMarket(source.getMarket()));
			destination.setSessionId(objectFactory.createGetCatalogRequestSessionId(source.getSessionId()));

			ArrayOfFlight flights = objectFactory2.createArrayOfFlight();
			for (MmbAncillaryFlightData sourceAncillaryFlightData : source.getFlights()) {
				flights.getFlight().add(mmbAncillaryFlightDataConverter.convert(sourceAncillaryFlightData));
			}
			destination.setFlights(objectFactory.createGetCatalogRequestFlights(flights));

			ArrayOfPassenger passengers = objectFactory2.createArrayOfPassenger();
			for (CheckinAncillaryPassengerData sourceAncillaryPassengerData : source.getPassengers()) {
				passengers.getPassenger().add(checkinAncillaryPassengerDataConverter.convert(sourceAncillaryPassengerData));
			}
			destination.setPassengers(objectFactory.createGetCatalogRequestPassengers(passengers));
		}

		return destination;
	}

}
