package com.alitalia.aem.service.impl.checkinrest.service;

import com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.response.AncillaryOffers;
import com.alitalia.aem.common.data.checkinrest.model.boardingpass.BoardingPass;
import com.alitalia.aem.common.data.checkinrest.model.boardingpass.BoardingPassInfo;
import com.alitalia.aem.common.data.checkinrest.model.boardingpassprintable.BoardingPassPrintable;
import com.alitalia.aem.common.data.checkinrest.model.checkinselectedpassenger.response.Outcome;
import com.alitalia.aem.common.data.checkinrest.model.clearancillariescart.response.ClearAncillariesCart;
import com.alitalia.aem.common.data.checkinrest.model.enhancedseatmap.response.EnhancedSeatMapResp;
import com.alitalia.aem.common.data.checkinrest.model.fasttrack.response.FastTrack;
import com.alitalia.aem.common.data.checkinrest.model.flightdetail.FlightDetail;
import com.alitalia.aem.common.data.checkinrest.model.lounge.response.Lounge;
import com.alitalia.aem.common.data.checkinrest.model.passenger.Passenger;
import com.alitalia.aem.common.data.checkinrest.model.passengerdata.PassengerData;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PnrListInfoByAddPassenger;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PnrListInfoByFrequentFlyerSearch;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PnrListInfoByPnrSearch;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PnrListInfoByTicketSearch;
import com.alitalia.aem.common.data.checkinrest.model.reprintbp.FreeTextInfoList;
import com.alitalia.aem.common.data.checkinrest.model.setbaggage.response.Baggage;
import com.alitalia.aem.common.data.checkinrest.model.tripsearch.Trip;
import com.alitalia.aem.common.data.checkinrest.model.updatepassenger.response.UpdatePassengerResp;
import com.alitalia.aem.common.data.checkinrest.model.updatepassengerdetails.response.UpdatePassengerDetailsResp;
import com.alitalia.aem.common.data.checkinrest.model.updatesecurityinfo.response.UpdateSecurityInfoResp;
import com.alitalia.aem.common.messages.checkinrest.*;
import com.alitalia.aem.service.api.home.ICheckinService;
import com.alitalia.aem.service.api.home.IMyFlightsService;
import com.alitalia.aem.service.impl.checkinrest.client.CheckinServiceClient;
import com.alitalia.aem.service.impl.checkinrest.client.MyFlightsServiceClient;
import com.alitalia.aem.service.impl.checkinrest.utils.CheckinMessageUnmarshal;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
@Component(immediate = true, metatype = false)
public class MyFlightsServiceImpl implements IMyFlightsService {

	private static final Logger logger = LoggerFactory.getLogger(MyFlightsServiceImpl.class);
	
	@Reference
	private MyFlightsServiceClient myFlightsServiceClient;
	
	@Override
	public CheckinPnrSearchResponse retrieveCheckinPnr(CheckinPnrSearchRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveCheckInPnr. the request is {}", request);
		}
		CheckinPnrSearchResponse response = null;
		try {
			response = new CheckinPnrSearchResponse();
			String url = "/searchbypnr";
			PnrListInfoByPnrSearch responsePnrInfo = new PnrListInfoByPnrSearch();
			
			//Chiedo i dati al CheckinServiceClient
//			logger.info("[CheckinServiceImpl] Prima di invocare il checkinServiceClient.");
			String responseClient = myFlightsServiceClient.getData(request,  url);
			responsePnrInfo = CheckinMessageUnmarshal.checkinPnrSearchUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			response.setPnrData(responsePnrInfo);
		}
		catch (Exception e) {
			logger.error("[CheckinServiceImpl] - Errore durante la retrieveCheckinPnr: " + e.getMessage());
		}
		return response;
	}

}

