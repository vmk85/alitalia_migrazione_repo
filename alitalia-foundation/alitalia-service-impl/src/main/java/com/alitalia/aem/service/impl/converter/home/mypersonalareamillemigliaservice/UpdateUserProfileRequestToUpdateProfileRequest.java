package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.UpdateUserProfileRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.NewInformation;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.UpdateProfileRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;

@Component(immediate = true, metatype = false)
@Service(value = UpdateUserProfileRequestToUpdateProfileRequest.class)
public class UpdateUserProfileRequestToUpdateProfileRequest implements Converter<UpdateUserProfileRequest, UpdateProfileRequest> {

	@Reference
	private MMCustomerProfileDataToCustomerProfile customerProfileDataConverter;
	
	@Reference
	private NewInformationDataToNewInformation newInformationDataConverter;
	
	@Override
	public UpdateProfileRequest convert(UpdateUserProfileRequest source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		
		ObjectFactory objectFactory = new ObjectFactory();
		
		UpdateProfileRequest destination = objectFactory.createUpdateProfileRequest();
		
		if(source.getCustomerProfile() != null){
			CustomerProfile customerProfile = customerProfileDataConverter.convert(source.getCustomerProfile());
			JAXBElement<CustomerProfile> jaxbCustomerProfile = objectFactory.createCustomerRequestCustomer(customerProfile);
			destination.setCustomer(jaxbCustomerProfile);
		}
		
		if(source.getNewInformation() != null){
			NewInformation newInformation = newInformationDataConverter.convert(source.getNewInformation());
			JAXBElement<NewInformation> jaxbNewInformation = objectFactory.createUpdateProfileRequestNewInformation(newInformation);
			destination.setNewInformation(jaxbNewInformation);
		}
				
		return destination;
	}
}