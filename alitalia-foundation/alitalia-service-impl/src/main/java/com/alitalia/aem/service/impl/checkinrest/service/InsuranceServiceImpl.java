package com.alitalia.aem.service.impl.checkinrest.service;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.checkinrest.DeleteInsuranceRequest;
import com.alitalia.aem.common.messages.checkinrest.DeleteInsuranceResponse;
import com.alitalia.aem.common.messages.checkinrest.GetInsuranceRequest;
import com.alitalia.aem.common.messages.checkinrest.GetInsuranceResponse;
import com.alitalia.aem.common.messages.checkinrest.SetInsuranceRequest;
import com.alitalia.aem.common.messages.checkinrest.SetInsuranceResponse;
import com.alitalia.aem.service.api.home.IServiceInsurance;
import com.alitalia.aem.service.impl.checkinrest.client.InsuranceServiceClient;
import com.alitalia.aem.service.impl.checkinrest.utils.InsuranceMessageUnmarshal;

@Service
@Component(immediate = true, metatype = false)
public class InsuranceServiceImpl implements IServiceInsurance {
	
	private static final Logger logger = LoggerFactory.getLogger(CheckinServiceImpl.class);

	@Reference
	private InsuranceServiceClient insuranceServiceClient;

	@Override
	public GetInsuranceResponse getInsurance(GetInsuranceRequest request) {
//		if (logger.isDebugEnabled()) {
//			logger.debug("Executing method getInsurance. The request is {}", request);
//		}
		GetInsuranceResponse response = null;
		try {
			response = new GetInsuranceResponse();
			String url = "/getinsurance";
			String responseClient = insuranceServiceClient.getData(request, url);
			response = InsuranceMessageUnmarshal.getInsuranceUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}

	@Override
	public SetInsuranceResponse setInsurance(SetInsuranceRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method setInsurance. The request is {}", request);
		}
		SetInsuranceResponse response = null;
		try {
			response = new SetInsuranceResponse();
			String url = "/setinsurance";
			String responseClient = insuranceServiceClient.getData(request, url);
			response = InsuranceMessageUnmarshal.setInsuranceUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}

	@Override
	public DeleteInsuranceResponse deleteInsurance(DeleteInsuranceRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method deleteInsurance. The request is {}", request);
		}
		DeleteInsuranceResponse response = null;
		try {
			response = new DeleteInsuranceResponse();
			String url = "/deleteinsurance";
			String responseClient = insuranceServiceClient.getData(request, url);
			response = InsuranceMessageUnmarshal.deleteInsuranceUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}
	
}
