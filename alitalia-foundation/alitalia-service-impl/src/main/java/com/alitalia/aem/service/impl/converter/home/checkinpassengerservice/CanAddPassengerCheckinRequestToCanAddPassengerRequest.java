package com.alitalia.aem.service.impl.converter.home.checkinpassengerservice;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.messages.home.CanAddPassengerCheckinRequest;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.CanAddPassengerRequest;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.Resource;
import com.alitalia.aem.ws.checkin.passengerservice.xsd2.ArrayOfPassenger;

@Component(immediate=true, metatype=false)
@Service(value=CanAddPassengerCheckinRequestToCanAddPassengerRequest.class)
public class CanAddPassengerCheckinRequestToCanAddPassengerRequest implements Converter<CanAddPassengerCheckinRequest, CanAddPassengerRequest> {

	@Reference
	private CheckinPassengerDataToPassenger checkinPassengerDataConverter;
	
	@Reference
	private CheckinRequestBaseInfoDataToRequestBaseInfo checkinRequestBaseInfoDataConverter;

	@Override
	public CanAddPassengerRequest convert(CanAddPassengerCheckinRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		CanAddPassengerRequest destination = null;

		if (source != null) {
			destination = objectFactory.createCanAddPassengerRequest();
			
			destination.setX003CInfoRequestX003EKBackingField(checkinRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			destination.setX003CResourceX003EKBackingField(Resource.WEB);
			
			destination.setPnr(objectFactory.createPassengerListRequestPnr(source.getPnr()));			
			
			List<CheckinPassengerData> sourcePassengerList = source.getPassengers();
			if(sourcePassengerList !=null){
				ArrayOfPassenger passengersList = new com.alitalia.aem.ws.checkin.passengerservice.xsd2.ObjectFactory().createArrayOfPassenger();
				for(CheckinPassengerData sourcePassengerData : sourcePassengerList){
					passengersList.getPassenger().add(checkinPassengerDataConverter.convert(sourcePassengerData));
				}
				destination.setPassengers(objectFactory.createCanAddPassengerRequestPassengers(passengersList));
			}			
		}

		return destination;
	}


}
