package com.alitalia.aem.service.impl.converter.home.mmbticketingservice;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.mmb.ticketingservice.xsd4.ACrossSellingDetail;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd4.CrossSelling;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd4.CrossSellingBase;
import com.alitalia.aem.ws.mmb.ticketingservice.xsd4.HotelCrossSelling;

@Component(immediate=true, metatype=false)
@Service(value=MmbTicketingServiceJAXBContextFactory.class)
public class MmbTicketingServiceJAXBContextFactory {

	private static final Logger logger = LoggerFactory.getLogger(MmbTicketingServiceJAXBContextFactory.class);

	private JAXBContext jaxbContext = null;

	public synchronized JAXBContext getJaxbContext() throws JAXBException {

		if (jaxbContext == null) {
			jaxbContext = JAXBContext.newInstance(CrossSellingBase.class,
													ACrossSellingDetail.class,
													CrossSelling.class,
													HotelCrossSelling.class
								);
		}

		return jaxbContext;
	}

	@Activate
	private void activate() {
		try {
			jaxbContext = JAXBContext.newInstance(CrossSellingBase.class,
													ACrossSellingDetail.class,
													CrossSelling.class,
													HotelCrossSelling.class
					);
		} catch (JAXBException e) {
			logger.warn("MmbTicketingServiceJAXBContextFactory - cannot instantiate JAXBContext: {}", e.getMessage());
		}
	}
}
