package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PaymentData;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.APaymentProviderOfanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.PaymentOfanyTypeanyType;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.ProcessInfo;

@Component(immediate=true, metatype=false)
@Service(value=PaymentToPaymentData.class)
public class PaymentToPaymentData 
		implements Converter<PaymentOfanyTypeanyType, PaymentData> {
	
	@Reference
	ProcessInfoToPaymentProcessInfoData processToPaymentProcessInfoDataConverter;
	
	@Reference
	APaymentProviderOfAnyTypeToPaymentProviderData aPaymentProviderOfAnyTypeToPaymentProviderDataConverter;
	
	@Override
	public PaymentData convert(PaymentOfanyTypeanyType source) {
		
		PaymentData destination = null;
		
		if (source != null) {
			
			destination = new PaymentData();
			
			destination.setEnabledNewsLetter(source.isEnabledNewsLetter());
			
			if (source.getCurrency() != null) {
				destination.setCurrency(
						source.getCurrency().getValue());
			}
			
			if (source.getDescription() != null) {
				destination.setDescription(
						source.getDescription().getValue());
			}
			
			if (source.getEmail() != null) {
				destination.setEmail(
						source.getEmail().getValue());
			}
			
			destination.setGrossAmount(
				source.getGrossAmount());
			
			if (source.getMailSubject() != null) {
				destination.setMailSubject(
						source.getMailSubject().getValue());
			}
			
			destination.setNetAmount(
					source.getNetAmount());
			
			destination.setPnr(
					source.getPNR().getValue());
			
			if (source.getProcess() != null) {
				destination.setProcess(
						processToPaymentProcessInfoDataConverter.convert(
								(ProcessInfo) source.getProcess().getValue()));
			}
			
			if (source.getProvider() != null) {
				destination.setProvider(
						aPaymentProviderOfAnyTypeToPaymentProviderDataConverter.convert(
								(APaymentProviderOfanyType) source.getProvider().getValue()));
			}
			
			if (source.getXsltMail() != null) {
				destination.setXsltMail(
						source.getXsltMail().getValue());
			}
			
			if(source.getType()!=null){
				destination.setType(PaymentTypeEnum.fromValue(source.getType().value()));
			}
		}
		
		return destination;
	}

}
