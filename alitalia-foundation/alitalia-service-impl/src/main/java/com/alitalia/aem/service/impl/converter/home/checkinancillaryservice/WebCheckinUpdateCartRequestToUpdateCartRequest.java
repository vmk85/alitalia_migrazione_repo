package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryCartItemData;
import com.alitalia.aem.common.messages.home.WebCheckinUpdateCartRequest;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.Resource;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd1.UpdateCartRequest;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ArrayOfUpdateCartItem;


@Component(immediate=true, metatype=false)
@Service(value=WebCheckinUpdateCartRequestToUpdateCartRequest.class)
public class WebCheckinUpdateCartRequestToUpdateCartRequest implements
		Converter<WebCheckinUpdateCartRequest, UpdateCartRequest> {

	@Reference
	private MmbRequestBaseInfoDataToRequestBaseInfo mmbRequestBaseInfoDataConverter;

	@Reference
	private MmbAncillaryCartItemDataToUpdateCartItem mmbAncillaryCartItemDataConverter;

	@Override
	public UpdateCartRequest convert(WebCheckinUpdateCartRequest source) {
		ObjectFactory objectFactory = new ObjectFactory();
		com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ObjectFactory objectFactory2 =
				new com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ObjectFactory();
		UpdateCartRequest destination = null;

		if (source != null) {
			destination = objectFactory.createUpdateCartRequest();

			destination.setCartId(source.getCartId());
			destination.setClient(objectFactory.createUpdateCartRequestClient(source.getClient()));
			destination.setMachineName(objectFactory.createAddToCartRequestMachineName(source.getMachineName()));
			destination.setSessionId(objectFactory.createAddToCartRequestSessionId(source.getSessionId()));

			destination.setX003CInfoRequestX003EKBackingField(mmbRequestBaseInfoDataConverter.convert(source.getBaseInfo()));
			destination.setX003CResourceX003EKBackingField(Resource.WEB);

			ArrayOfUpdateCartItem cartItems = objectFactory2.createArrayOfUpdateCartItem();
			for(MmbAncillaryCartItemData sourceItem: source.getItems()) {
				cartItems.getUpdateCartItem().add(mmbAncillaryCartItemDataConverter.convert(sourceItem));
			}
			destination.setItems(objectFactory.createUpdateCartRequestItems(cartItems));
		}

		return destination;
	}

}
