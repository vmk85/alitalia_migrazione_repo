package com.alitalia.aem.service.impl.home;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.LocateRequest;
import com.alitalia.aem.common.messages.home.LocateResponse;
import com.alitalia.aem.service.api.home.EgonService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.egonservice.IPLOUTToLocateResponse;
import com.alitalia.aem.service.impl.converter.home.egonservice.LocateRequestToIPLINPRequest;
import com.alitalia.aem.ws.egon.WsEgonClient;
import com.alitalia.aem.ws.egon.xsd.IPLINP;
import com.alitalia.aem.ws.egon.xsd.IPLOUT;

@Service
@Component(immediate=true, metatype=false)
public class SimpleEgonService implements EgonService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleEgonService.class);

	@Reference
	private WsEgonClient wsEgonClient;
	
	@Reference
	private LocateRequestToIPLINPRequest locateRequestConverter;
	
	@Reference
	private IPLOUTToLocateResponse iploutConverter;
	
	@Override
	public LocateResponse locate(LocateRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method locate. The request is {}", request);
		}
		

		try {
			
			LocateResponse response = new LocateResponse();
			IPLINP egonRequest = locateRequestConverter.convert(request);
			IPLOUT egonResponse = wsEgonClient.wpIpL(egonRequest);
			response = iploutConverter.convert(egonResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method locate. The response is {}", response);
			}
			
			return response;
			
		} catch (Exception e) {
			logger.error("Exception while executing method locate.", e);
			throw new AlitaliaServiceException("Exception executing locate: Sid ["+request.getSid()+"]" , e);
		}
	}
}