package com.alitalia.aem.service.impl.converter.home.geospatialservice;

import javax.xml.bind.JAXBElement;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveGeoCitiesRequest;
import com.alitalia.aem.ws.geospatial.service.xsd1.GetCitiesByTargetRequest;
import com.alitalia.aem.ws.geospatial.service.xsd1.ObjectFactory;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveGeoCitiesRequestToGetCitiesByTargetRequest.class)
public class RetrieveGeoCitiesRequestToGetCitiesByTargetRequest implements Converter<RetrieveGeoCitiesRequest, GetCitiesByTargetRequest> {

	@Override
	public GetCitiesByTargetRequest convert(RetrieveGeoCitiesRequest source) {
		
		GetCitiesByTargetRequest request = new GetCitiesByTargetRequest();
		
		String languageCode = source.getLanguageCode();
		String marketCode = source.getMarketCode();
		String paxType = source.getPaxType();
		Integer down = source.getRngpryDwn();
		Integer up = source.getRngpryUp();
		String target = source.getTarget();
		
		ObjectFactory objectFactory = new ObjectFactory();
		
		JAXBElement<String> jaxbLanguageCode = objectFactory.createGetCitiesByTargetRequestLanguageCode(languageCode);
		JAXBElement<String> jaxbMarketCode = objectFactory.createGetCitiesByTargetRequestMarketCode(marketCode);
		JAXBElement<String> jaxbPaxType = objectFactory.createGetCitiesByTargetRequestPaxType(paxType);
		JAXBElement<String> jaxbTarget = objectFactory.createGetCitiesByTargetRequestTarget(target);
		JAXBElement<Integer> jaxbRngpryDwn = objectFactory.createGetCitiesByTargetRequestRngpryDwn(down);
		JAXBElement<Integer> jaxbRngpryUp = objectFactory.createGetCitiesByTargetRequestRngpryUp(up);
		
		request.setLanguageCode(jaxbLanguageCode);
		request.setMarketCode(jaxbMarketCode);
		request.setPaxType(jaxbPaxType);
		request.setRngpryDwn(jaxbRngpryDwn);
		request.setRngpryUp(jaxbRngpryUp);
		request.setTarget(jaxbTarget);
		
		return request;
	}
}