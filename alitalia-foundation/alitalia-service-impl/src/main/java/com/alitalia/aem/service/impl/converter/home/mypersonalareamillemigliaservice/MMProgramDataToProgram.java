package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMProgramData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Program;

@Component(immediate = true, metatype = false)
@Service(value = MMProgramDataToProgram.class)
public class MMProgramDataToProgram implements Converter<MMProgramData, Program> {

	@Override
	public Program convert(MMProgramData source) {
		Program destination = null;

		if (source != null) {
			com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ObjectFactory objectFactory2 = new com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ObjectFactory();

			destination = objectFactory2.createProgram();
			if (source.getStartDate() != null)
				destination.setStartDate(XsdConvertUtils.toXMLGregorianCalendar(source.getEndDate()));
			if (source.getEndDate() != null)
				destination.setEndDate(XsdConvertUtils.toXMLGregorianCalendar(source.getEndDate()));
			destination.setProgramCode(objectFactory2.createProgramProgramCode(source.getProgramCode()));
		}

		return destination;
	}
}