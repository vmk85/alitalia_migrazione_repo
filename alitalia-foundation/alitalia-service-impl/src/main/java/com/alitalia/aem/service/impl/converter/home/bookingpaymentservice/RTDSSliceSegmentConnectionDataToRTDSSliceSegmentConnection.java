package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceSegmentConnectionData;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ObjectFactory;
import com.alitalia.aem.ws.booking.paymentservice.xsd12.ResultTicketingDetailSolutionSliceSegmentConnection;

@Component(immediate=true, metatype=false)
@Service(value=RTDSSliceSegmentConnectionDataToRTDSSliceSegmentConnection.class)
public class RTDSSliceSegmentConnectionDataToRTDSSliceSegmentConnection implements
		Converter<ResultTicketingDetailSolutionSliceSegmentConnectionData, 
					ResultTicketingDetailSolutionSliceSegmentConnection> {

	@Reference
	private RTDSSliceSegmentConnectionExtDataToRTDSSliceSegmentConnectionExt rtdsExtFieldConverter;

	@Override
	public ResultTicketingDetailSolutionSliceSegmentConnection convert(
			ResultTicketingDetailSolutionSliceSegmentConnectionData source) {
		ResultTicketingDetailSolutionSliceSegmentConnection destination = null;

		if (source != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			destination = objectFactory.createResultTicketingDetailSolutionSliceSegmentConnection();

			destination.setDurationField(source.getDurationField());

			destination.setExtField(rtdsExtFieldConverter.convert(source.getExtField()));
		}

		return destination;
	}

}
