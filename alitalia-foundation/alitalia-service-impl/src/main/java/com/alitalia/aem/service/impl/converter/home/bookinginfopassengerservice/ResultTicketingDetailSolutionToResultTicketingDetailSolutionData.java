package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionCurrencyConversionData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionPricingData;
import com.alitalia.aem.common.data.home.ResultTicketingDetailSolutionSliceData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolution;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionCurrencyConversion;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionPricing;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd11.ResultTicketingDetailSolutionSlice;

@Component(immediate=true, metatype=false)
@Service(value=ResultTicketingDetailSolutionToResultTicketingDetailSolutionData.class)
public class ResultTicketingDetailSolutionToResultTicketingDetailSolutionData
		implements Converter<ResultTicketingDetailSolution, ResultTicketingDetailSolutionData> {

	@Reference
	private RTDSCurrencyConversionToRTDSCurrencyConversionData currencyConversionConverter;

	@Reference
	private RTDSDisplayFareTotalToRTDSDisplayFareTotalData displayFareTotalConverter;

	@Reference
	private RTDSDisplayTaxTotalToRTDSDisplayTaxTotalData displayTaxTotalConverter;

	@Reference
	private RTDSDisplayTotalToRTDSDisplayTotalData displayTotalFieldConverter;

	@Reference
	private RTDSExtToRTDSExtData extFieldConverter;

	@Reference
	private RTDSPricingToRTDSPricingData pricingConverter;

	@Reference
	private RTDSSaleFareTotalToRTDSSaleFareTotalData saleFareTotalFieldConverter;

	@Reference
	private RTDSSaleTaxTotalToRTDSSaleTaxTotalData saleTaxTotalFieldConverter;

	@Reference
	private RTDSSaleTotalToRTDSSaleTotalData saleTotalFieldConverter;

	@Reference
	private RTDSSliceToRTDSSliceData sliceFieldConverter; 

	@Override
	public ResultTicketingDetailSolutionData convert(ResultTicketingDetailSolution source) {
		ResultTicketingDetailSolutionData destination = null;

		if (source != null) {
			destination = new ResultTicketingDetailSolutionData();

			destination.setIdField(source.getIdField());

			List<ResultTicketingDetailSolutionCurrencyConversionData> currencyConversionField = null;
			if (source.getCurrencyConversionField() != null &&
					source.getCurrencyConversionField().getResultTicketingDetailSolutionCurrencyConversion() != null &&
					!source.getCurrencyConversionField().getResultTicketingDetailSolutionCurrencyConversion().isEmpty()) {
				currencyConversionField = new ArrayList<ResultTicketingDetailSolutionCurrencyConversionData>();
				for (ResultTicketingDetailSolutionCurrencyConversion sourceRTDSCurrencyConversion : 
							source.getCurrencyConversionField().getResultTicketingDetailSolutionCurrencyConversion()) 
					currencyConversionField.add(
							currencyConversionConverter.convert(sourceRTDSCurrencyConversion));
			}
			destination.setCurrencyConversionField(currencyConversionField);

			destination.setDisplayFareTotalField(
					displayFareTotalConverter.convert(source.getDisplayFareTotalField()));

			destination.setDisplayTaxTotalField(
					displayTaxTotalConverter.convert(source.getDisplayTaxTotalField()));

			destination.setDisplayTotalField(
					displayTotalFieldConverter.convert(source.getDisplayTotalField()));

			destination.setExtField(extFieldConverter.convert(source.getExtField()));

			List<ResultTicketingDetailSolutionPricingData> pricingField = null;
			if (source.getPricingField() != null &&
					source.getPricingField().getResultTicketingDetailSolutionPricing() != null &&
					!source.getPricingField().getResultTicketingDetailSolutionPricing().isEmpty()) {
				pricingField = new ArrayList<ResultTicketingDetailSolutionPricingData>();
				for(ResultTicketingDetailSolutionPricing sourcePricingElem : source.getPricingField().getResultTicketingDetailSolutionPricing())
					pricingField.add(pricingConverter.convert(sourcePricingElem));
			}
			destination.setPricingField(pricingField);

			destination.setSaleFareTotalField(
					saleFareTotalFieldConverter.convert(source.getSaleFareTotalField()));

			destination.setSaleTaxTotalField(
					saleTaxTotalFieldConverter.convert(source.getSaleTaxTotalField()));

			destination.setSaleTotalField(
					saleTotalFieldConverter.convert(source.getSaleTotalField()));

			List<ResultTicketingDetailSolutionSliceData> sliceField = null;
			if (source.getSliceField() != null &&
					source.getSliceField().getResultTicketingDetailSolutionSlice() != null &&
					!source.getSliceField().getResultTicketingDetailSolutionSlice().isEmpty()) {
				sliceField = new ArrayList<ResultTicketingDetailSolutionSliceData>();
				for(ResultTicketingDetailSolutionSlice sourceSliceFieldElem : source.getSliceField().getResultTicketingDetailSolutionSlice())
					sliceField.add(sliceFieldConverter.convert(sourceSliceFieldElem));
			}
			destination.setSliceField(sliceField);
		}

		return destination;
	}

}
