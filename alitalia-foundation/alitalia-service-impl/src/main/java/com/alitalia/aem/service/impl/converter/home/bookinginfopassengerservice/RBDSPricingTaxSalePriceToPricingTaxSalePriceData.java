package com.alitalia.aem.service.impl.converter.home.bookinginfopassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingTaxSalePriceData;
import com.alitalia.aem.ws.booking.infopassengerservice.xsd12.ResultBookingDetailsSolutionPricingTaxSalePrice;
@Component(immediate=true, metatype=false)
@Service(value=RBDSPricingTaxSalePriceToPricingTaxSalePriceData.class)
public class RBDSPricingTaxSalePriceToPricingTaxSalePriceData implements
		Converter<ResultBookingDetailsSolutionPricingTaxSalePrice, ResultBookingDetailsSolutionPricingTaxSalePriceData> {

	@Override
	public ResultBookingDetailsSolutionPricingTaxSalePriceData convert(
			ResultBookingDetailsSolutionPricingTaxSalePrice source) {
		ResultBookingDetailsSolutionPricingTaxSalePriceData destination = null;
		if(source!=null){
			destination = new ResultBookingDetailsSolutionPricingTaxSalePriceData();
			destination.setAmountField(source.getAmountField());
			destination.setCurrencyField(source.getCurrencyField());
		}
		return destination;
	}

}
