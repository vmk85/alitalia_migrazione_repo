package com.alitalia.aem.service.impl.home;

import com.alitalia.aem.ws.booking.widgetservice.SabreDcWidgetServiceClient;
import com.alitalia.aem.ws.statistics.statisticsservice.xsd1.StatisticsResponse;
import com.day.cq.wcm.api.Page;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.LoadWidgetCashMilesRequest;
import com.alitalia.aem.common.messages.home.LoadWidgetCashMilesResponse;
import com.alitalia.aem.common.messages.home.LoadWidgetECouponRequest;
import com.alitalia.aem.common.messages.home.LoadWidgetECouponResponse;
import com.alitalia.aem.common.messages.home.LoadWidgetOneClickRequest;
import com.alitalia.aem.common.messages.home.LoadWidgetOneClickResponse;
import com.alitalia.aem.common.messages.home.UpdateWidgetCashMilesRequest;
import com.alitalia.aem.common.messages.home.UpdateWidgetCashMilesResponse;
import com.alitalia.aem.service.api.home.BookingWidgetService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.bookingwidget.LoadWidgetCashMilesRequestToLoadCashAndMilesRequest;
import com.alitalia.aem.service.impl.converter.home.bookingwidget.LoadWidgetECouponRequestToLoadECouponRequest;
import com.alitalia.aem.service.impl.converter.home.bookingwidget.LoadWidgetOneClickRequestToLoadOneClickRequest;
import com.alitalia.aem.service.impl.converter.home.bookingwidget.LoadWidgetResponseToLoadWidgetCashMilesResponse;
import com.alitalia.aem.service.impl.converter.home.bookingwidget.LoadWidgetResponseToLoadWidgetECouponResponse;
import com.alitalia.aem.service.impl.converter.home.bookingwidget.LoadWidgetResponseToLoadWidgetOneClickResponse;
import com.alitalia.aem.service.impl.converter.home.bookingwidget.UpdateWidgetCashMilesRequestToUpdateCashAndMilesRequest;
import com.alitalia.aem.service.impl.converter.home.bookingwidget.UpdateWidgetResponseToUpdateWidgetCashMilesResponse;
import com.alitalia.aem.ws.booking.widgetservice.WidgetServiceClient;
import com.alitalia.aem.ws.booking.widgetservice.xsd1.LoadCashAndMilesRequest;
import com.alitalia.aem.ws.booking.widgetservice.xsd1.LoadECouponRequest;
import com.alitalia.aem.ws.booking.widgetservice.xsd1.LoadOneClickRequest;
import com.alitalia.aem.ws.booking.widgetservice.xsd1.LoadOneClickResponse;
import com.alitalia.aem.ws.booking.widgetservice.xsd1.LoadWidgetResponse;
import com.alitalia.aem.ws.booking.widgetservice.xsd1.UpdateCashAndMilesRequest;
import com.alitalia.aem.ws.booking.widgetservice.xsd1.UpdateWidgetResponse;

@Service
@Component(immediate=true, metatype=false)
public class SimpleBookingWidgetService implements BookingWidgetService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleBookingWidgetService.class);
	
	@Reference
	private WidgetServiceClient widgetServiceClient;
	
	@Reference
	private LoadWidgetECouponRequestToLoadECouponRequest loadECouponRequestConverter;
	
	@Reference
	private LoadWidgetResponseToLoadWidgetECouponResponse loadECouponResponseConverter;
	
	@Reference
	private LoadWidgetOneClickRequestToLoadOneClickRequest loadOneClickRequestConverter;
	
	@Reference
	private LoadWidgetResponseToLoadWidgetOneClickResponse loadOneClickResponseConverter;
	
	@Reference
	private LoadWidgetCashMilesRequestToLoadCashAndMilesRequest loadCashMilesRequestConverter;
	
	@Reference
	private LoadWidgetResponseToLoadWidgetCashMilesResponse loadCashMilesResponseConverter;
	
	@Reference
	private UpdateWidgetCashMilesRequestToUpdateCashAndMilesRequest updateCashMilesRequestConverter;
	
	@Reference
	private UpdateWidgetResponseToUpdateWidgetCashMilesResponse updateCashMilesResponseConverter;

	/** Migrazione 3.6 inizio*/

	@Reference
	private ResourceResolverFactory resolverFactory;

	@Reference
	private SabreDcWidgetServiceClient sabreDcWidgetServiceClient;

	/** Migrazione 3.6 fine*/

	@Override
	public LoadWidgetECouponResponse loadWidgetECoupon(LoadWidgetECouponRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method loadWidgetECoupon(). The request is {}", request);
		}
		
		
		try {

			LoadWidgetECouponResponse response = new LoadWidgetECouponResponse();
			LoadECouponRequest loadECouponRequest = loadECouponRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			LoadWidgetResponse loadWidgetResponse = null;
			if (newSabreEnable(loadECouponRequest.getSite().getValue().toLowerCase())){
				loadWidgetResponse = sabreDcWidgetServiceClient.loadWidget(loadECouponRequest);
			} else {
				loadWidgetResponse = widgetServiceClient.loadWidget(loadECouponRequest);
			}

			/** Migrazione 3.6 fine*/

//			LoadWidgetResponse loadWidgetResponse = widgetServiceClient.loadWidget(loadECouponRequest);
			response = loadECouponResponseConverter.convert(loadWidgetResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			return response;
			
		} catch (Exception e) {
			logger.error("Exception while executing method loadWidgetECoupon(): {}", e);
			throw new AlitaliaServiceException("Exception executing loadWidgetECoupon: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public LoadWidgetCashMilesResponse loadWidgetCashMiles(LoadWidgetCashMilesRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method loadWidgetCashMiles(). The request is {}", request);
		}
		
		
		try {
			
			LoadWidgetCashMilesResponse response = new LoadWidgetCashMilesResponse();
			LoadCashAndMilesRequest loadCashMilesRequest = loadCashMilesRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			LoadWidgetResponse loadWidgetResponse = null;
			if (newSabreEnable(loadCashMilesRequest.getSite().getValue().toLowerCase())){
				loadWidgetResponse = sabreDcWidgetServiceClient.loadWidget(loadCashMilesRequest);
			} else {
				loadWidgetResponse = widgetServiceClient.loadWidget(loadCashMilesRequest);
			}

			/** Migrazione 3.6 fine*/

//			LoadWidgetResponse loadWidgetResponse = widgetServiceClient.loadWidget(loadCashMilesRequest);
			response = loadCashMilesResponseConverter.convert(loadWidgetResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			return response;
			
		} catch (Exception e) {
			logger.error("Exception while executing method loadWidgetCashMiles(): {}", e);
			throw new AlitaliaServiceException("Exception executing loadWidgetCashMiles: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public UpdateWidgetCashMilesResponse updateWidgetCashMiles(UpdateWidgetCashMilesRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method updateWidgetCashMiles(). The request is {}", request);
		}
		
		
		try {
			
			UpdateWidgetCashMilesResponse response = new UpdateWidgetCashMilesResponse();
			UpdateCashAndMilesRequest updateCashMilesRequest = updateCashMilesRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			UpdateWidgetResponse updateWidgetResponse = null;
			if (newSabreEnable(updateCashMilesRequest.getSite().getValue().toLowerCase())){
				updateWidgetResponse = sabreDcWidgetServiceClient.updateWidget(updateCashMilesRequest);
			} else {
				updateWidgetResponse = widgetServiceClient.updateWidget(updateCashMilesRequest);
			}

			/** Migrazione 3.6 fine*/

//			UpdateWidgetResponse updateWidgetResponse = widgetServiceClient.updateWidget(updateCashMilesRequest);
			response = updateCashMilesResponseConverter.convert(updateWidgetResponse);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			return response;
			
		} catch (Exception e) {
			logger.error("Exception while executing method updateWidgetCashMiles(): {}", e);
			throw new AlitaliaServiceException("Exception executing updateWidgetCashMiles: Sid ["+request.getSid()+"]" , e);
		}
	}
	
	@Override
	public LoadWidgetOneClickResponse loadWidgetOneClick(LoadWidgetOneClickRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method loadWidgetOneClick(). The request is {}", request);
		}


		try {

			LoadWidgetOneClickResponse response = new LoadWidgetOneClickResponse();
			LoadOneClickRequest loadOneClickRequest = loadOneClickRequestConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			LoadWidgetResponse loadWidgetResponse = null;
			if (newSabreEnable(loadOneClickRequest.getSite().getValue().toLowerCase())){
				loadWidgetResponse = sabreDcWidgetServiceClient.loadWidget(loadOneClickRequest);
			} else {
				loadWidgetResponse = widgetServiceClient.loadWidget(loadOneClickRequest);
			}

			/** Migrazione 3.6 fine*/

//			LoadWidgetResponse loadWidgetResponse = widgetServiceClient.loadWidget(loadOneClickRequest);
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method loadWidget(). The response is {}", loadWidgetResponse);
			}

			response = loadOneClickResponseConverter.convert((LoadOneClickResponse) loadWidgetResponse);
			if (logger.isDebugEnabled()) {
				logger.debug("Response Converted. The response is {}", response);
			}

			response.setTid(request.getTid());
			response.setSid(request.getSid());
			return response;

		} catch (Exception e) {
			logger.error("Exception while executing method loadWidgetOneClick(): {}", e);
			throw new AlitaliaServiceException("Exception executing loadWidgetOneClick: Sid [" + request.getSid() + "]", e);
		}
	}

	/** Migrazione 3.6 inizio*/

	private boolean newSabreEnable(String market) throws LoginException {

		ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
		Resource res = resourceResolver.getResource("/content/alitalia/alitalia-" + market);
		Page page = res.adaptTo(Page.class);
		ValueMap props = page.getProperties();
		String title = props.get("isNewService", "default value");
		logger.info("dato preso dalla pagina: " + title);
		return Boolean.parseBoolean(title);

	}

	/** Migrazione 3.6 fine*/


}