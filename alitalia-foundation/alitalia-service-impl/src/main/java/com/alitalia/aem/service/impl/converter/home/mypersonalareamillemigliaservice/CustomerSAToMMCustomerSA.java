package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.data.home.MMCustomerSA;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfileSA;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

@Component(immediate=true, metatype=false)
@Service(value=CustomerSAToMMCustomerSA.class)
public class CustomerSAToMMCustomerSA implements Converter<CustomerProfileSA, MMCustomerSA> {

    @Reference
    private CustomerProfileToMMCustomerProfileData customerProfileConverter;

    @Override
    public MMCustomerSA convert(CustomerProfileSA source) {
        if(source == null) throw new IllegalArgumentException("CustomerProfileSA is null.");

        MMCustomerSA destination = null;

        if (source != null) {
            destination = new MMCustomerSA();

            Integer accessFailureCount = source.getAccessFailureCount();
            String certifiedCountryNumber = source.getCertifiedCountryNumber() != null ? source
                    .getCertifiedCountryNumber().getValue() : "";
            String certifiedEmailAccount = source.getCertifiedEmailAccount() != null ? source
                    .getCertifiedEmailAccount().getValue() : "";
            String certifiedPhoneAccount = source.getCertifiedPhoneAccount() != null ? source
                    .getCertifiedPhoneAccount().getValue() : "";
            Boolean emailCertificateFlag = source.isEmailCertificateFlag();
            Boolean flagOldCustomer = source.isFlagOldCustomer();
            Boolean flagRememberPassword = source.isFlagRememberPassword();
            Boolean flagVerificaPassword = source.isFlagVerificaPassword();
            Boolean flagVerificaRisposta = source.isFlagVerificaRisposta();
            Boolean flagVerificaUsername = source.isFlagVerificaUsername();
            Boolean lockedout = source.isLockedout();
            String password = source.getPassword() != null ? source
                    .getPassword().getValue() : "";
            Boolean phoneCertificateFlag = source.isPhoneCertificateFlag();
            String secureAnswer = source.getSecureAnswer() != null ? source
                    .getSecureAnswer().getValue() : "";
            Integer secureQuestionID = source.getSecureQuestionID();
            String username = source.getUsername() != null ? source
                    .getUsername().getValue() : "";

            if (source.getCustomerProfile() != null) {
                MMCustomerProfileData mmCustomerProfileData = customerProfileConverter.convert(source.getCustomerProfile().getValue());
                destination.setMmCustomerProfileData(mmCustomerProfileData);
            }

            destination.setAccessFailureCount(accessFailureCount);
            destination.setCertifiedCountryNumber(certifiedCountryNumber);
            destination.setCertifiedEmailAccount(certifiedEmailAccount);
            destination.setCertifiedPhoneAccount(certifiedPhoneAccount);
            destination.setEmailCertificateFlag(emailCertificateFlag);
            destination.setFlagOldCustomer(flagOldCustomer);
            destination.setFlagRememberPassword(flagRememberPassword);
            destination.setFlagVerificaPassword(flagVerificaPassword);
            destination.setFlagVerificaRisposta(flagVerificaRisposta);
            destination.setFlagVerificaUsername(flagVerificaUsername);


            destination.setLockedout(lockedout);
            destination.setPassword(password);
            destination.setPhoneCertificateFlag(phoneCertificateFlag);

            destination.setSecureAnswer(secureAnswer);
            destination.setSecureQuestionID(secureQuestionID);
            destination.setUsername(username);

        }
        return destination;
    }
}
