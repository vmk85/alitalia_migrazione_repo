package com.alitalia.aem.service.impl.converter.homerest.datachange;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.service.impl.converter.homerest.beans.GetAllAirports;

public class RestDataChange {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public RetrieveAirportsResponse changeDataForResponse (GetAllAirports getAllAirports) {
		
		AirportData airports = null;
		RetrieveAirportsResponse retrieveAirportsResponse = new RetrieveAirportsResponse();
		List<AirportData> airportsList = new ArrayList<AirportData>();
		logger.info("prima del for loop di RestDataChange...");
		
		try {
			for (int i = 0; i < getAllAirports.getAirports().size(); i++) {
				
				airports = new AirportData();
				
				airports.setCity(getAllAirports.getAirports().get(i).getCity());
				airports.setCityUrl(getAllAirports.getAirports().get(i).getCityUrl());
				airports.setCityCode(getAllAirports.getAirports().get(i).getCityCode());
				airports.setCode(getAllAirports.getAirports().get(i).getCode());
				airports.setCountry(getAllAirports.getAirports().get(i).getCountry());
				airports.setCountryCode(getAllAirports.getAirports().get(i).getCountryCode());
				airports.setState(getAllAirports.getAirports().get(i).getState());
				airports.setStateCode(getAllAirports.getAirports().get(i).getStateCode());
				airports.setName(getAllAirports.getAirports().get(i).getName());
				airports.setEligibility(getAllAirports.getAirports().get(i).getCheckInEligibility());
			
				airportsList.add(airports);
			}
		}
		catch (IndexOutOfBoundsException e) {
			logger.info(e.getMessage());
		}
		
		retrieveAirportsResponse.setAirports(airportsList);
		retrieveAirportsResponse.setConversationID(getAllAirports.getConversationID());
		
		return retrieveAirportsResponse;
	}
}
