package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMPreferenceData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Preference;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd3.ArrayOfanyType;

@Component(immediate = true, metatype = false)
@Service(value = PreferencesToMMPreferencesData.class)
public class PreferencesToMMPreferencesData implements Converter<ArrayOfanyType, List<MMPreferenceData>> {

	private static Logger logger = LoggerFactory.getLogger(PreferencesToMMPreferencesData.class);
	
	@Reference
	private MyPersonalareaMillemigliaServiceJAXBContextFactory jaxbContextFactory;
	
	@Override
	public List<MMPreferenceData> convert(ArrayOfanyType source) {
		
		if (source == null) {
			return null;
		}
		
		List<MMPreferenceData> mmPreferences = new ArrayList<MMPreferenceData>();

		for (Object object: source.getAnyType()) {
			try {
				Unmarshaller unmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
				
				JAXBElement<Preference> jaxbElement = (JAXBElement<Preference>) unmarshaller.unmarshal((Node) object);
				Preference preference = jaxbElement.getValue();
				
				MMPreferenceData mmPreferenceData = new MMPreferenceData();
				mmPreferenceData.setAdditionalText(preference.getAdditionalText().getValue());
				mmPreferenceData.setDescription(preference.getDescription().getValue());
				mmPreferenceData.setIdCategory(preference.getIDCategory().getValue());
				mmPreferenceData.setSequenceNumber(preference.getSequenceNumber().getValue());
				mmPreferenceData.setShortNameCategory(preference.getShortNameCategory().getValue());
				mmPreferenceData.setShortNamePreference(preference.getShortNamePreference().getValue());

				if (preference.getStartDate() != null)
					mmPreferenceData.setStartDate(XsdConvertUtils.parseCalendar(preference.getStartDate()));
				if (preference.getEndDate() != null)
					mmPreferenceData.setEndDate(XsdConvertUtils.parseCalendar(preference.getEndDate()));
				
				mmPreferences.add(mmPreferenceData);
			
			} catch (JAXBException e) {
				logger.error("Error while creating JAXBConext/Unmarshller for List<MMPreferenceData>: {}", e);
			}
		}

		return mmPreferences;
	}
}