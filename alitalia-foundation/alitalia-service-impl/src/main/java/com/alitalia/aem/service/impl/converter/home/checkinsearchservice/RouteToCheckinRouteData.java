package com.alitalia.aem.service.impl.converter.home.checkinsearchservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.common.data.home.checkin.CheckinRouteData;
import com.alitalia.aem.common.data.home.enumerations.MmbApisTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbRouteStatusEnum;
import com.alitalia.aem.ws.checkin.searchservice.xsd2.ApisType;
import com.alitalia.aem.ws.checkin.searchservice.xsd2.ArrayOfFlight;
import com.alitalia.aem.ws.checkin.searchservice.xsd2.Flight;
import com.alitalia.aem.ws.checkin.searchservice.xsd2.Route;
import com.alitalia.aem.ws.checkin.searchservice.xsd2.RouteStatus;


@Component(immediate=true, metatype=false)
@Service(value=RouteToCheckinRouteData.class)
public class RouteToCheckinRouteData implements Converter<Route, CheckinRouteData> {

	@Reference
	private PassengerToCheckinPassengerData passengerConverter;

	@Reference
	private FlightToCheckinFlightData flightConverter;	

	@Reference
	private DeepLinkToMmbDeepLinkCheckinData deepLinkConverter;

	@Override
	public CheckinRouteData convert(Route source) {
		CheckinRouteData destination = null;

		if (source != null) {
			destination = new CheckinRouteData();

			destination.setId(source.getX003CIdX003EKBackingField());
			ApisType sourceApisTypeRequired = source.getX003CApisTypeRequiredX003EKBackingField();
			if (sourceApisTypeRequired != null)
				destination.setApisTypeRequired(MmbApisTypeEnum.fromValue(sourceApisTypeRequired.value()));

			destination.setCheckinEnabled(source.isX003CCheckinEnabledX003EKBackingField());
			
			destination.setDeepLink(deepLinkConverter.convert(source.getX003CDeepLinkX003EKBackingField()));
			
			ArrayOfFlight sourceFlights = source.getX003CFlightsX003EKBackingField();
			if (sourceFlights != null &&
					sourceFlights.getFlight() != null &&
					!sourceFlights.getFlight().isEmpty()) {
				List<CheckinFlightData> flights = new ArrayList<CheckinFlightData>();
				for(Flight sourceFlight : sourceFlights.getFlight()) {
					flights.add(flightConverter.convert(sourceFlight));
				}
				destination.setFlights(flights);
			}
			
			destination.setIdApis(source.isX003CIdIDApisX003EKBackingField());
			
			ArrayOfFlight interlineFlights = source.getX003CInterlineFlightsX003EKBackingField();
			if (interlineFlights != null &&
					interlineFlights.getFlight() != null &&
					!interlineFlights.getFlight().isEmpty()) {
				List<CheckinFlightData> flights = new ArrayList<CheckinFlightData>();
				for(Flight sourceFlight : interlineFlights.getFlight()) {
					flights.add(flightConverter.convert(sourceFlight));
				}
				destination.setInterlineFlights(flights);
			}

			destination.setPnr(source.getX003CPnrX003EKBackingField());
			destination.setSelected(source.isX003CSelectedX003EKBackingField());
			destination.setIsOutwardRoute(source.isX003CIsOutwardRouteX003EKBackingField());

			RouteStatus sourceStatus = source.getX003CStatusX003EKBackingField();
			if (sourceStatus != null)
				destination.setStatus(MmbRouteStatusEnum.fromValue(sourceStatus.value()));
			destination.setUndoEnabled(source.isX003CUndoEnabledX003EKBackingField());
			
		}

		return destination;
	}

}
