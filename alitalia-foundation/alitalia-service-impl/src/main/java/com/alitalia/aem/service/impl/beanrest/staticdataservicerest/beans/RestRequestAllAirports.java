package com.alitalia.aem.service.impl.beanrest.staticdataservicerest.beans;

public class RestRequestAllAirports {
	
	private String language;
	private String market;
	
	public String getMarket() {
		return market;
	}
	public void setMarket(String market) {
		this.market = market;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	
	

}
