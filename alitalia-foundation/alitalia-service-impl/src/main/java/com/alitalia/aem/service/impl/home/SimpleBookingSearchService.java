package com.alitalia.aem.service.impl.home;

import java.util.ArrayList;

import com.alitalia.aem.ws.booking.searchservice.SabreDcSearchServiceClient;
import com.day.cq.wcm.api.Page;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.AvailableFlightsData;
import com.alitalia.aem.common.data.home.BookingSearchData;
import com.alitalia.aem.common.data.home.BrandSearchData;
import com.alitalia.aem.common.data.home.CarnetSearchData;
import com.alitalia.aem.common.data.home.FlexSearchData;
import com.alitalia.aem.common.data.home.FullTextSearchData;
import com.alitalia.aem.common.data.home.MultiSliceSearchData;
import com.alitalia.aem.common.data.home.NextSliceSearchData;
import com.alitalia.aem.common.data.home.RefreshSearchData;
import com.alitalia.aem.common.data.home.RibbonSearchData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.common.data.home.enumerations.GatewayTypeEnum;
import com.alitalia.aem.common.messages.home.SearchBookingSolutionResponse;
import com.alitalia.aem.common.messages.home.SearchExtraChargeRequest;
import com.alitalia.aem.common.messages.home.SearchExtraChargeResponse;
import com.alitalia.aem.common.messages.home.SearchFlexibleDatesMatrixResponse;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionResponse;
import com.alitalia.aem.service.api.home.BookingSearchService;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.impl.converter.home.bookingsearchservice.ExtraChargeResponseToPassengerList;
import com.alitalia.aem.service.impl.converter.home.bookingsearchservice.SearchExtraChargeRequestToExtraChargeRequest;
import com.alitalia.aem.service.impl.converter.home.bookingsearchservice.SearchFlightSolutionRequestToBrandSearchRequest;
import com.alitalia.aem.service.impl.converter.home.bookingsearchservice.SearchFlightSolutionRequestToFlexSearchRequest;
import com.alitalia.aem.service.impl.converter.home.bookingsearchservice.SearchFlightSolutionRequestToFullTextRulesSearchRequest;
import com.alitalia.aem.service.impl.converter.home.bookingsearchservice.SearchFlightSolutionRequestToGatewayBookingSearchRequest;
import com.alitalia.aem.service.impl.converter.home.bookingsearchservice.SearchFlightSolutionRequestToJumpUpSearchRequest;
import com.alitalia.aem.service.impl.converter.home.bookingsearchservice.SearchFlightSolutionRequestToMetaSearchRequest;
import com.alitalia.aem.service.impl.converter.home.bookingsearchservice.SearchFlightSolutionRequestToMultiSliceSearchRequest;
import com.alitalia.aem.service.impl.converter.home.bookingsearchservice.SearchFlightSolutionRequestToNextSliceSearchRequest;
import com.alitalia.aem.service.impl.converter.home.bookingsearchservice.SearchFlightSolutionRequestToRefreshSearchRequest;
import com.alitalia.aem.service.impl.converter.home.bookingsearchservice.SearchFlightSolutionRequestToRibbonSearchRequest;
import com.alitalia.aem.service.impl.converter.home.bookingsearchservice.SearchResponseToAvailableFlightsData;
import com.alitalia.aem.service.impl.converter.home.bookingsearchservice.SearchResponseToFlexFlightsData;
import com.alitalia.aem.service.impl.converter.home.bookingsearchservice.SearchResponseToRoutesData;
import com.alitalia.aem.service.impl.converter.home.bookingsearchservice.SearchTaxResponseToAvailableFlightsData;
import com.alitalia.aem.ws.booking.searchservice.SearchServiceClient;
import com.alitalia.aem.ws.booking.searchservice.wsdl.ISearchServiceExecuteServiceFaultFaultFaultMessage;
import com.alitalia.aem.ws.booking.searchservice.xsd1.ExtraChargeRequest;
import com.alitalia.aem.ws.booking.searchservice.xsd1.ExtraChargeResponse;
import com.alitalia.aem.ws.booking.searchservice.xsd1.SearchRequest;
import com.alitalia.aem.ws.booking.searchservice.xsd1.SearchResponse;
import com.alitalia.aem.ws.booking.searchservice.xsd2.ModelResponse;
import com.alitalia.aem.ws.booking.searchservice.xsd5.ArrayOfKeyValueOfstringstring;
import com.alitalia.aem.ws.booking.searchservice.xsd5.ArrayOfKeyValueOfstringstring.KeyValueOfstringstring;

@Service
@Component(immediate=true, metatype=false)
public class SimpleBookingSearchService implements BookingSearchService {

	private static final Logger logger = LoggerFactory.getLogger(SimpleBookingSearchService.class);

	@Reference
	private SearchServiceClient searchServiceClient;

	@Reference
	private SearchFlightSolutionRequestToBrandSearchRequest brandSearchConverter;

	@Reference
	private SearchFlightSolutionRequestToFlexSearchRequest flexSearchConverter;

	@Reference
	private SearchFlightSolutionRequestToRibbonSearchRequest ribbonSearchConverter;

	@Reference
	private SearchFlightSolutionRequestToRefreshSearchRequest refreshSearchConverter;

	@Reference
	private SearchFlightSolutionRequestToJumpUpSearchRequest jumpUpSearchConverter;

	@Reference
	private SearchFlightSolutionRequestToGatewayBookingSearchRequest gatewayBookingSearchConverter;

	@Reference
	private SearchExtraChargeRequestToExtraChargeRequest extraChargeSearchConverter;

	@Reference
	private SearchFlightSolutionRequestToMultiSliceSearchRequest multiSliceSearchConverter;

	@Reference
	private SearchFlightSolutionRequestToNextSliceSearchRequest nextSliceSearchConverter;

	@Reference
	private SearchFlightSolutionRequestToMetaSearchRequest metaSearchConverter;

	@Reference
	private SearchResponseToAvailableFlightsData searchResponseCoverter;
	
	@Reference
	private SearchTaxResponseToAvailableFlightsData searchTaxResponseCoverter;

	@Reference
	private SearchResponseToFlexFlightsData searchResponseToFlexCoverter;

	@Reference
	private SearchResponseToRoutesData searchResponseToRoutesCoverter;

	@Reference
	private ExtraChargeResponseToPassengerList extraChargeResponseConverter;

	@Reference
	private SearchFlightSolutionRequestToFullTextRulesSearchRequest fullTextSearchConverter;


	/** Migrazione 3.6 inizio*/

	@Reference
	private ResourceResolverFactory resolverFactory;

	@Reference
	private SabreDcSearchServiceClient sabreDcSearchServiceClient;

	/** Migrazione 3.6 fine*/

	@Override
	public SearchFlightSolutionResponse executeBrandSearch(SearchFlightSolutionRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method executeBrandSearch. The request is {}", request);
		}
		
		if (! (request.getFilter() instanceof BrandSearchData) 
				&& ! (request.getFilter() instanceof CarnetSearchData)) {
			logger.error("Filter data inside request is not from the correct class: " + request.getFilter().getClass());
			throw new IllegalArgumentException("Filter data inside request is not from the correct class");
		}
		
		SearchFlightSolutionResponse response = new SearchFlightSolutionResponse();
		try {

			response.setTid(request.getTid());
			response.setSid(request.getSid());
			long time_before_req = System.currentTimeMillis();
			SearchRequest searchRequest = brandSearchConverter.convert(request);
			long time_after_req = System.currentTimeMillis();

			/** Migrazione 3.6 inizio*/

			SearchResponse searchResponse = null;
			if (newSabreEnable(searchRequest.getMarketCode().getValue().toLowerCase())){
				searchResponse = sabreDcSearchServiceClient.execute(searchRequest);
			} else {
				searchResponse = searchServiceClient.execute(searchRequest);
			}

			/** Migrazione 3.6 fine*/

//			SearchResponse searchResponse = searchServiceClient.execute(searchRequest);
			long time_before_resp = System.currentTimeMillis();
			response.setAvailableFlights(searchResponseCoverter.convert(searchResponse));
			ModelResponse modelResponse = (ModelResponse) searchResponse.getResult().getValue();
			response.setCookie(modelResponse.getCookie().getValue());
			response.setExecute(modelResponse.getExecution().getValue());
			response.setSabreGateWayAuthToken(modelResponse.getSabreGateWayAuthToken().getValue());
				
			long time_after_resp = System.currentTimeMillis();
			logger.info("BrandSearch: request converter execution time: {}. Response converter execution time: {}", (time_after_req - time_before_req), (time_after_resp - time_before_resp));
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method executeBrandSearch(). The response is {}", response);
			}
			
			return response;

		} catch (ISearchServiceExecuteServiceFaultFaultFaultMessage e1){
			
			ArrayOfKeyValueOfstringstring messageDictionaries = e1.getFaultInfo().getRemoteSystemMessageDictionary().getValue();
			
			if (messageDictionaries.getKeyValueOfstringstring() != null){
				for (KeyValueOfstringstring message : messageDictionaries.getKeyValueOfstringstring()){
					if (message.getValue()!=null && message.getValue().contains("ERR.SSW.NO_AVAILABILITY_FOUND")){
						AvailableFlightsData flightsData = new AvailableFlightsData();
					flightsData.setRoutes(new ArrayList<RouteData>());
						response.setAvailableFlights(flightsData);
						return response;
					}
				}
			} 
			logger.error("Exception1 while executing method executeBrandSearch(): {}", e1);
			throw new AlitaliaServiceException("Exception1 executing executeBrandSearch: Sid ["+request.getSid()+"]" , e1);
		}  catch (Exception e) {
			logger.error("Exception while executing method executeBrandSearch(): {}", e);
			throw new AlitaliaServiceException("Exception executing executeBrandSearch: Sid ["+request.getSid()+"]" , e);
		}

	}

	@Override
	public SearchFlightSolutionResponse executeRibbonSearch(SearchFlightSolutionRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method executeRibbonSearch. The request is {}", request);
		}
		
		if (! (request.getFilter() instanceof RibbonSearchData) ) {
			logger.error("Filter data inside request is not from the correct class: " + request.getFilter().getClass());
			throw new IllegalArgumentException("Filter data inside request is not from the correct class");
		}

		try {

			SearchFlightSolutionResponse response = new SearchFlightSolutionResponse();
			long time_before_req = System.currentTimeMillis();
			SearchRequest searchRequest = ribbonSearchConverter.convert(request);
			long time_after_req = System.currentTimeMillis();

			/** Migrazione 3.6 inizio*/

			SearchResponse searchResponse = null;
			if (newSabreEnable(searchRequest.getMarketCode().getValue().toLowerCase())){
				searchResponse = sabreDcSearchServiceClient.execute(searchRequest);
			} else {
				searchResponse = searchServiceClient.execute(searchRequest);
			}

			/** Migrazione 3.6 fine*/

//			SearchResponse searchResponse = searchServiceClient.execute(searchRequest);
			long time_before_resp = System.currentTimeMillis();
			response.setAvailableFlights(searchResponseCoverter.convert(searchResponse));
			long time_after_resp = System.currentTimeMillis();
			logger.info("RibbonSearch: request converter execution time: {}. Response converter execution time: {}", (time_after_req - time_before_req), (time_after_resp - time_before_resp));
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			ModelResponse modelResponse = (ModelResponse) searchResponse.getResult().getValue();
			response.setCookie(modelResponse.getCookie().getValue());
			response.setExecute(modelResponse.getExecution().getValue());
			response.setSabreGateWayAuthToken(modelResponse.getSabreGateWayAuthToken().getValue());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method executeRibbonSearch(). The response is {}", response);
			}
			
			return response;

		}  catch (Exception e) {
			logger.error("Exception while executing method executeRibbonSearch(): {}", e);
			throw new AlitaliaServiceException("Exception executing executeRibbonSearch: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public SearchFlightSolutionResponse executeRefreshSearch(SearchFlightSolutionRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method executeRefreshSearch. The request is {}", request);
		}
		
		if (! (request.getFilter() instanceof RefreshSearchData) ) {
			logger.error("Filter data inside request is not from the correct class: " + request.getFilter().getClass());
			throw new IllegalArgumentException("Filter data inside request is not from the correct class");
		}

		try {

			SearchFlightSolutionResponse response = new SearchFlightSolutionResponse();
			SearchRequest searchRequest = refreshSearchConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			SearchResponse searchResponse = null;
			if (newSabreEnable(searchRequest.getMarketCode().getValue().toLowerCase())){
				searchResponse = sabreDcSearchServiceClient.execute(searchRequest);
			} else {
				searchResponse = searchServiceClient.execute(searchRequest);
			}

			/** Migrazione 3.6 fine*/

//			SearchResponse searchResponse = searchServiceClient.execute(searchRequest);
			response.setAvailableFlights(searchResponseCoverter.convert(searchResponse));
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			ModelResponse modelResponse = (ModelResponse) searchResponse.getResult().getValue();
			response.setCookie(modelResponse.getCookie().getValue());
			response.setExecute(modelResponse.getExecution().getValue());
			response.setSabreGateWayAuthToken(modelResponse.getSabreGateWayAuthToken().getValue());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method executeRefreshSearch(). The response is {}", response);
			}
			
			return response;

		}  catch (Exception e) {
			logger.error("Exception while executing method executeRefreshSearch(): {}", e);
			throw new AlitaliaServiceException("Exception executing executeRefreshSearch: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public SearchFlightSolutionResponse executeYouthSearch(SearchFlightSolutionRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method executeRefreshSearch. The request is {}", request);
		}
		
		if (! (request.getFilter() instanceof BrandSearchData) ) {
			logger.error("Filter data inside request is not from the correct class: " + request.getFilter().getClass());
			throw new IllegalArgumentException("Filter data inside request is not from the correct class");
		}

		try {
			
			SearchFlightSolutionResponse response = new SearchFlightSolutionResponse();
			SearchRequest searchRequest = brandSearchConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			SearchResponse searchResponse = null;
			if (newSabreEnable(searchRequest.getMarketCode().getValue().toLowerCase())){
				searchResponse = sabreDcSearchServiceClient.execute(searchRequest);
			} else {
				searchResponse = searchServiceClient.execute(searchRequest);
			}

			/** Migrazione 3.6 fine*/

//			SearchResponse searchResponse = searchServiceClient.execute(searchRequest);
			response.setAvailableFlights(searchResponseCoverter.convert(searchResponse));
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			ModelResponse modelResponse = (ModelResponse) searchResponse.getResult().getValue();
			response.setCookie(modelResponse.getCookie().getValue());
			response.setExecute(modelResponse.getExecution().getValue());
			response.setSabreGateWayAuthToken(modelResponse.getSabreGateWayAuthToken().getValue());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method executeYouthSearch(). The response is {}", response);
			}
			
			return response;
			
		}  catch (Exception e) {
			logger.error("Exception while executing method executeYouthSearch(): {}", e);
			throw new AlitaliaServiceException("Exception executing executeYouthSearch: Sid ["+request.getSid()+"]" , e);
		}

	}

	/*@Override
	public SearchYouthSolutionResponse executeJumpUpSearch(SearchFlightSolutionRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method executeRefreshSearch. The request is {}", request);
		}
		
		if (! (request.getFilter() instanceof JumpUpSearchData) ) {
			logger.error("Filter data inside request is not from the correct class: " + request.getFilter().getClass());
			throw new IllegalArgumentException("Filter data inside request is not from the correct class");
		}

		try {
			
			SearchYouthSolutionResponse response = new SearchYouthSolutionResponse();
			SearchRequest searchRequest = jumpUpSearchConverter.convert(request);
			SearchResponse searchResponse = searchServiceClient.execute(searchRequest);
			response.setAvailableFlights(searchResponseCoverter.convert(searchResponse));
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			ModelResponse modelResponse = (ModelResponse) searchResponse.getResult().getValue();
			response.setCookie(modelResponse.getCookie().getValue());
			response.setExecute(modelResponse.getExecution().getValue());
			response.setSabreGateWayAuthToken(modelResponse.getSabreGateWayAuthToken().getValue());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method executeYouthSearch(). The response is {}", response);
			}
			
			return response;
			
		}  catch (Exception e) {
			logger.error("Exception while executing method executeYouthSearch(): {}", e);
			throw new AlitaliaServiceException("Exception executing executeYouthSearch: Sid ["+request.getSid()+"]" , e);
		}

	}*/

	@Override
	public SearchExtraChargeResponse executeExtraChargeSearch(SearchExtraChargeRequest request) {
		
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method executeExtraChargeSearch. The request is {}", request);
		}
		
		if (! (request.getFilter() instanceof BrandSearchData)
				&& ! (request.getFilter() instanceof MultiSliceSearchData) 
				&& ! (request.getFilter() instanceof CarnetSearchData) ) {
			logger.error("Filter data inside request is not from the correct class: " + request.getFilter().getClass());
			throw new IllegalArgumentException("Filter data inside request is not from the correct class");
		}

		try {

			SearchExtraChargeResponse response = new SearchExtraChargeResponse();
			long time_before_req = System.currentTimeMillis();
			ExtraChargeRequest searchRequest = extraChargeSearchConverter.convert(request);
			long time_after_req = System.currentTimeMillis();

			/** Migrazione 3.6 inizio*/

			ExtraChargeResponse searchResponse = null;
			if (newSabreEnable(searchRequest.getFilter().getValue().getMarket().getValue().toLowerCase())){
				searchResponse = sabreDcSearchServiceClient.extraCharge(searchRequest);
			} else {
				searchResponse = searchServiceClient.extraCharge(searchRequest);
			}

			/** Migrazione 3.6 fine*/

//			ExtraChargeResponse searchResponse = searchServiceClient.extraCharge(searchRequest);
			long time_before_resp = System.currentTimeMillis();
			response.setPassengerList(extraChargeResponseConverter.convert(searchResponse));
			long time_after_resp = System.currentTimeMillis();
			logger.info("ExtraCharge search: request converter execution time: {}. Response converter execution time: {}", (time_after_req - time_before_req), (time_after_resp - time_before_resp));
			logger.info("ExtraCharge Passenger List {}", (response.getPassengerList() != null ? "size: "+response.getPassengerList().size() : "null"));
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method executeExtraChargeSearch(). The response is {}", response);
			}
			
			return response;
			
		} catch (Exception e) {
			logger.error("Exception while executing method executeExtraChargeSearch(): {}", e);
			throw new AlitaliaServiceException("Exception executing executeYouthSearch: Sid ["+request.getSid()+"]" , e);
		} 
	}

	@Override
	public SearchBookingSolutionResponse executeBookingSellupSearch(SearchFlightSolutionRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method executeBookingSellupSearch. The request is {}", request);
		}
		
		if (! (request.getFilter() instanceof BookingSearchData) ) {
			logger.error("Filter data inside request is not from the correct class: " + request.getFilter().getClass());
			throw new IllegalArgumentException("Filter data inside request is not from the correct class");
		}

		if ( !((BookingSearchData) request.getFilter()).getType().equals(GatewayTypeEnum.BOOKING_SELLUP) ) {
			logger.error("Filter data inside request is not from the correct type: " + 
					((BookingSearchData) request.getFilter()).getType());
			throw new IllegalArgumentException("Filter data inside request is not from the correct type");
		}

		try {

			SearchBookingSolutionResponse response = new SearchBookingSolutionResponse();
			long time_before_req = System.currentTimeMillis();
			SearchRequest searchRequest = gatewayBookingSearchConverter.convert(request);
			long time_after_req = System.currentTimeMillis();

			/** Migrazione 3.6 inizio*/

			SearchResponse searchResponse = null;
			if (newSabreEnable(searchRequest.getMarketCode().getValue().toLowerCase())){
				searchResponse = sabreDcSearchServiceClient.execute(searchRequest);
			} else {
				searchResponse = searchServiceClient.execute(searchRequest);
			}

			/** Migrazione 3.6 fine*/

//			SearchResponse searchResponse = searchServiceClient.execute(searchRequest);

			long time_before_resp = System.currentTimeMillis();
			response.setRoutesData(searchResponseToRoutesCoverter.convert(searchResponse));
			response.setAvailableFlightsTaxes(searchTaxResponseCoverter.convert(searchResponse));
			long time_after_resp = System.currentTimeMillis();
			logger.info("BookingSellupSearch: request converter execution time: {}. Response converter execution time: {}", (time_after_req - time_before_req), (time_after_resp - time_before_resp));
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			ModelResponse modelResponse = (ModelResponse) searchResponse.getResult().getValue();
			response.setCookie(modelResponse.getCookie().getValue());
			response.setExecute(modelResponse.getExecution().getValue());
			response.setSabreGateWayAuthToken(modelResponse.getSabreGateWayAuthToken().getValue());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method executeBookingSellupSearch(). The response is {}", response);
			}
			
			return response;
			
		}  catch (Exception e) {
			logger.error("Exception while executing method executeBookingSellupSearch(): {}", e);
			throw new AlitaliaServiceException("Exception executing executeBookingSellupSearch: Sid ["+request.getSid()+"]" , e);
		}	
	}

	@Override
	public SearchFlightSolutionResponse executeBookingTaxSearchSearch(SearchFlightSolutionRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method executeBookingSellupSearch. The request is {}", request);
		}
		
		if (! (request.getFilter() instanceof BookingSearchData) ) {
			logger.error("Filter data inside request is not from the correct class: " + request.getFilter().getClass());
			throw new IllegalArgumentException("Filter data inside request is not from the correct class");
		}

		if ( !((BookingSearchData) request.getFilter()).getType().equals(GatewayTypeEnum.BOOKING_TAX_SEARCH) ) {
			logger.error("Filter data inside request is not from the correct type: " + 
					((BookingSearchData) request.getFilter()).getType());
			throw new IllegalArgumentException("Filter data inside request is not from the correct type");
		}

		try {
			
			SearchFlightSolutionResponse response = new SearchFlightSolutionResponse();
			long time_before_req = System.currentTimeMillis();
			SearchRequest searchRequest = gatewayBookingSearchConverter.convert(request);
			long time_after_req = System.currentTimeMillis();

			/** Migrazione 3.6 inizio*/

			SearchResponse searchResponse = null;
			if (newSabreEnable(searchRequest.getMarketCode().getValue().toLowerCase())){
				searchResponse = sabreDcSearchServiceClient.execute(searchRequest);
			} else {
				searchResponse = searchServiceClient.execute(searchRequest);
			}

			/** Migrazione 3.6 fine*/

//			SearchResponse searchResponse = searchServiceClient.execute(searchRequest);

			long time_before_resp = System.currentTimeMillis();
			response.setAvailableFlights(searchResponseCoverter.convert(searchResponse));
			long time_after_resp = System.currentTimeMillis();
			logger.info("RibbonSearch: request converter execution time: {}. Response converter execution time: {}", (time_after_req - time_before_req), (time_after_resp - time_before_resp));
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			ModelResponse modelResponse = (ModelResponse) searchResponse.getResult().getValue();
			response.setCookie(modelResponse.getCookie().getValue());
			response.setExecute(modelResponse.getExecution().getValue());
			response.setSabreGateWayAuthToken(modelResponse.getSabreGateWayAuthToken().getValue());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method executeBookingSellupSearch(). The response is {}", response);
			}
			
			return response;
			
		}  catch (Exception e) {
			logger.error("Exception while executing method executeBookingTaxSearchSearch(): {}", e);
			throw new AlitaliaServiceException("Exception executing executeBookingTaxSearchSearch: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public SearchFlightSolutionResponse executeMultiSliceSearch(SearchFlightSolutionRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method executeMultiSliceSearch. The request is {}", request);
		}
		
		if (! (request.getFilter() instanceof MultiSliceSearchData) ) {
			logger.error("Filter data inside request is not from the correct class: " + request.getFilter().getClass());
			throw new IllegalArgumentException("Filter data inside request is not from the correct class");
		}
		SearchFlightSolutionResponse response = new SearchFlightSolutionResponse();

		try {

			SearchRequest searchRequest = multiSliceSearchConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			SearchResponse searchResponse = null;
			if (newSabreEnable(searchRequest.getMarketCode().getValue().toLowerCase())){
				searchResponse = sabreDcSearchServiceClient.execute(searchRequest);
			} else {
				searchResponse = searchServiceClient.execute(searchRequest);
			}

			/** Migrazione 3.6 fine*/

//			SearchResponse searchResponse = searchServiceClient.execute(searchRequest);

			response.setAvailableFlights(searchResponseCoverter.convert(searchResponse));
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			ModelResponse modelResponse = (ModelResponse) searchResponse.getResult().getValue();
			response.setCookie(modelResponse.getCookie().getValue());
			response.setExecute(modelResponse.getExecution().getValue());
			response.setSabreGateWayAuthToken(modelResponse.getSabreGateWayAuthToken().getValue());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method executeMultiSliceSearch(). The response is {}", response);
			}
			
			return response;
			
		} catch (ISearchServiceExecuteServiceFaultFaultFaultMessage e1){
			
			ArrayOfKeyValueOfstringstring messageDictionaries = e1.getFaultInfo().getRemoteSystemMessageDictionary().getValue();
			
			if (messageDictionaries.getKeyValueOfstringstring() != null){
				for (KeyValueOfstringstring message : messageDictionaries.getKeyValueOfstringstring()){
					if (message.getValue()!=null && message.getValue().contains("ERR.SSW.NO_AVAILABILITY_FOUND")){
						AvailableFlightsData flightsData = new AvailableFlightsData();
						flightsData.setRoutes(new ArrayList<RouteData>());
						response.setAvailableFlights(flightsData);
						return response;
					}
				}
			} 
			logger.error("Exception1 while executing method executeMultiSliceSearch(): {}", e1);
			throw new AlitaliaServiceException("Exception1 executing executeMultiSliceSearch: Sid ["+request.getSid()+"]" , e1);
		}  catch (Exception e) {
			logger.error("Exception while executing method executeMultiSliceSearch(): {}", e);
			throw new AlitaliaServiceException("Exception executing executeMultiSliceSearch: Sid ["+request.getSid()+"]" , e);
		} 
	}

	@Override
	public SearchFlightSolutionResponse executeNextSliceSearch(SearchFlightSolutionRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method executeNextSliceSearch. The request is {}", request);
		}
		
		if (! (request.getFilter() instanceof NextSliceSearchData) ) {
			logger.error("Filter data inside request is not from the correct class: " + request.getFilter().getClass());
			throw new IllegalArgumentException("Filter data inside request is not from the correct class");
		}

		try {

			SearchFlightSolutionResponse response = new SearchFlightSolutionResponse();
			SearchRequest searchRequest = nextSliceSearchConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			SearchResponse searchResponse = null;
			if (newSabreEnable(searchRequest.getMarketCode().getValue().toLowerCase())){
				searchResponse = sabreDcSearchServiceClient.execute(searchRequest);
			} else {
				searchResponse = searchServiceClient.execute(searchRequest);
			}

			/** Migrazione 3.6 fine*/

//			SearchResponse searchResponse = searchServiceClient.execute(searchRequest);

			response.setAvailableFlights(searchResponseCoverter.convert(searchResponse));
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			ModelResponse modelResponse = (ModelResponse) searchResponse.getResult().getValue();
			response.setCookie(modelResponse.getCookie().getValue());
			response.setExecute(modelResponse.getExecution().getValue());
			response.setSabreGateWayAuthToken(modelResponse.getSabreGateWayAuthToken().getValue());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method executeNextSliceSearch(). The response is {}", response);
			}
			
			return response;

		}  catch (Exception e) {
			logger.error("Exception while executing method executeNextSliceSearch(): {}", e);
			throw new AlitaliaServiceException("Exception executing executeNextSliceSearch: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public SearchFlexibleDatesMatrixResponse executeFlexibleDatesSearch(SearchFlightSolutionRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method executeFlexibleDatesSearch. The request is {}", request);
		}
		
		if (! (request.getFilter() instanceof FlexSearchData) ) {
			logger.error("Filter data inside request is not from the correct class: " + request.getFilter().getClass());
			throw new IllegalArgumentException("Filter data inside request is not from the correct class");
		}

		try {
			
			SearchFlexibleDatesMatrixResponse response = new SearchFlexibleDatesMatrixResponse();
			SearchRequest searchRequest = flexSearchConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			SearchResponse searchResponse = null;
			if (newSabreEnable(searchRequest.getMarketCode().getValue().toLowerCase())){
				searchResponse = sabreDcSearchServiceClient.execute(searchRequest);
			} else {
				searchResponse = searchServiceClient.execute(searchRequest);
			}

			/** Migrazione 3.6 fine*/

//			SearchResponse searchResponse = searchServiceClient.execute(searchRequest);

			response.setFlexibleDatesMatrixDataList(searchResponseToFlexCoverter.convert(searchResponse));
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			ModelResponse modelResponse = (ModelResponse) searchResponse.getResult().getValue();
			response.setCookie(modelResponse.getCookie().getValue());
			response.setExecute(modelResponse.getExecution().getValue());
			response.setSabreGateWayAuthToken(modelResponse.getSabreGateWayAuthToken().getValue());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method executeFlexibleDatesSearch(). The response is {}", response);
			}
			
			return response;
			
		}  catch (Exception e) {
			logger.error("Exception while executing method executeFlexibleDatesSearch(): {}", e);
			throw new AlitaliaServiceException("Exception executing executeFlexibleDatesSearch: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public SearchFlightSolutionResponse executeMetaSearch(SearchFlightSolutionRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method executeMetaSearch. The request is {}", request);
		}
	
		if (! (request.getFilter() instanceof BrandSearchData) ) {
			logger.error("Filter data inside request is not from the correct class: " + request.getFilter().getClass());
			throw new IllegalArgumentException("Filter data inside request is not from the correct class");
		}
		
		try {

			SearchFlightSolutionResponse response = new SearchFlightSolutionResponse();
			SearchRequest searchRequest = metaSearchConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			SearchResponse searchResponse = null;
			if (newSabreEnable(searchRequest.getMarketCode().getValue().toLowerCase())){
				searchResponse = sabreDcSearchServiceClient.execute(searchRequest);
			} else {
				searchResponse = searchServiceClient.execute(searchRequest);
			}

			/** Migrazione 3.6 fine*/

//			SearchResponse searchResponse = searchServiceClient.execute(searchRequest);
			response.setAvailableFlights(searchResponseCoverter.convert(searchResponse));
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			ModelResponse modelResponse = (ModelResponse) searchResponse.getResult().getValue();
			response.setCookie(modelResponse.getCookie().getValue());
			response.setExecute(modelResponse.getExecution().getValue());
			response.setSabreGateWayAuthToken(modelResponse.getSabreGateWayAuthToken().getValue());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method executeMetaSearch(). The response is {}", response);
			}
			
			return response;
			
		}  catch (Exception e) {
			logger.error("Exception while executing method executeMetaSearch(): {}", e);
			throw new AlitaliaServiceException("Exception executing executeMetaSearch: Sid ["+request.getSid()+"]" , e);
		}
	}

	@Override
	public SearchFlightSolutionResponse executeFullTextRulesSearch(SearchFlightSolutionRequest request) {
		logger.info("Executing method executeFullTextRulesSearch. The request is {}", request);
		if (! (request.getFilter() instanceof FullTextSearchData) ) {
			logger.error("Filter data inside request is not from the correct class: " + request.getFilter().getClass());
			throw new IllegalArgumentException("Filter data inside request is not from the correct class");
		}

		try {

			SearchFlightSolutionResponse response = new SearchFlightSolutionResponse();
			SearchRequest searchRequest = fullTextSearchConverter.convert(request);

			/** Migrazione 3.6 inizio*/

			SearchResponse searchResponse = null;
			if (newSabreEnable(searchRequest.getMarketCode().getValue().toLowerCase())){
				searchResponse = sabreDcSearchServiceClient.execute(searchRequest);
			} else {
				searchResponse = searchServiceClient.execute(searchRequest);
			}

			/** Migrazione 3.6 fine*/

//			SearchResponse searchResponse = searchServiceClient.execute(searchRequest);

			response.setAvailableFlights(searchResponseCoverter.convert(searchResponse));
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			ModelResponse modelResponse = (ModelResponse) searchResponse.getResult().getValue();
			response.setCookie(modelResponse.getCookie().getValue());
			response.setExecute(modelResponse.getExecution().getValue());
			response.setSabreGateWayAuthToken(modelResponse.getSabreGateWayAuthToken().getValue());
			logger.info("Executed method executeRefreshSearch(). The response is {}", response);
			return response;

		}  catch (Exception e) {
			logger.error("Exception while executing method executeRefreshSearch(): {}", e);
			throw new AlitaliaServiceException("Exception executing executeRefreshSearch: Sid ["+request.getSid()+"]" , e);
		}
	}

	/** Migrazione 3.6 inizio*/

	private boolean newSabreEnable(String market) throws LoginException {

		ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
		Resource res = resourceResolver.getResource("/content/alitalia/alitalia-" + market);
		Page page = res.adaptTo(Page.class);
		ValueMap props = page.getProperties();
		String title = props.get("isNewService", "default value");
		logger.info("dato preso dalla pagina: " + title);
		return Boolean.parseBoolean(title);

	}

	/** Migrazione 3.6 fine*/
}