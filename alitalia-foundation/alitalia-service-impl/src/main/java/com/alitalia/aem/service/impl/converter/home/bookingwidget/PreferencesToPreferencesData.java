package com.alitalia.aem.service.impl.converter.home.bookingwidget;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PreferencesData;
import com.alitalia.aem.common.data.home.SeatPreferencesData;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.Meal;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.Preferences;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.SeatPreference;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.SeatType;

@Component(immediate=true, metatype=false)
@Service(value=PreferencesToPreferencesData.class)
public class PreferencesToPreferencesData 
		implements Converter<Preferences, PreferencesData> {
	
	@Reference
	private SeatPreferencesToSeatPreferenceData seatPreferencesToSeatPreferencesDataConverter;
	
	@Reference
	private MealToMealData mealToMealConverterData;
	
	@Reference
	private SeatTypeToSeatTypeData seatTypeToSeatTypeConverterData;
	
	@Override
	public PreferencesData convert(Preferences source) {
		
		PreferencesData destination = null;
		
		if (source != null) {
		
			destination = new PreferencesData();
			
			List<SeatPreferencesData> seatPreferences = new ArrayList<SeatPreferencesData>();
			if (source.getSeatPreferences() != null && source.getSeatPreferences().getValue() != null) {
				for (Object seatPreference : source.getSeatPreferences().getValue().getAnyType()) {
					seatPreferences.add(
							seatPreferencesToSeatPreferencesDataConverter.convert(
									(SeatPreference) seatPreference));
				}
			}
			destination.setSeatPreferences(seatPreferences);
			
			if (source.getMealType() != null) {
				destination.setMealType(
						mealToMealConverterData.convert(
								(Meal) source.getMealType().getValue()));
			}
			
			if (source.getSeatType() != null) {
				destination.setSeatType(
						seatTypeToSeatTypeConverterData.convert(
								(SeatType) source.getSeatType().getValue()));
			}
			
		}
		
		return destination;
	}

}
