package com.alitalia.aem.service.impl.converter.home.checkinancillaryservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillarySeatDetailData;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.ObjectFactory;
import com.alitalia.aem.ws.checkin.ancillaryservice.xsd2.SeatDetail;


@Component(immediate=true, metatype=false)
@Service(value=MmbAncillarySeatDetailDataToSeatDetail.class)
public class MmbAncillarySeatDetailDataToSeatDetail implements Converter<MmbAncillarySeatDetailData, SeatDetail> {

	@Override
	public SeatDetail convert(MmbAncillarySeatDetailData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		SeatDetail destination = null;

		if (source != null) {
			destination = objectFactory.createSeatDetail();
			if(source.isComfort() != null) {
				destination.setX003CIsComfortX003EKBackingField(source.isComfort());
			}
			if (source.isPayment()!=null) {
				destination.setX003CIsPaymentX003EKBackingField(source.isPayment());
			}
			destination.setX003CSeatX003EKBackingField(source.getSeat());
		}

		return destination;
	}

}
