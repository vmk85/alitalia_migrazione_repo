package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.MMFlightData;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.Flight;

@Component(immediate = true, metatype = false)
@Service(value = MMFlightDataToFlight.class)
public class MMFlightDataToFlight implements Converter<MMFlightData, Flight> {

	@Override
	public Flight convert(MMFlightData source) {
		Flight destination = null;

		if (source != null) {
			com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ObjectFactory objectFactory2 = new com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.ObjectFactory();

			destination = objectFactory2.createFlight();
			destination.setAirportArrival(objectFactory2.createFlightAirportArrival(source.getAirportArrival()));
			destination.setAirportDeparture(objectFactory2.createFlightAirportDeparture(source.getAirportDeparture()));
			destination.setFlightNumber(objectFactory2.createFlightFlightNumber(source.getFlightNumber()));
			destination.setResult(source.getResult());
			destination.setServiceClass(objectFactory2.createFlightServiceClass(source.getServiceClass()));
			destination.setTicketNumber(objectFactory2.createFlightTicketNumber(source.getTicketNumber()));
			destination.setVector(objectFactory2.createFlightVector(source.getVector()));

			if (source.getFlightDate() != null)
				destination.setFlightDate(XsdConvertUtils.toXMLGregorianCalendar(source.getFlightDate()));
		}

		return destination;
	}

}
