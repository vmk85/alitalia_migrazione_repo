package com.alitalia.aem.service.impl.converter.home.bookingbagsandrulesservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.BagsAndRuleRequest;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd1.BagsAndFareRuleRequest;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.booking.bagsandfarerulesservice.xsd2.ArrayOfstring;


@Component(immediate=true, metatype= false)
@Service(value=BagsAndRuleRequestToBagsAndFareRuleRequest.class)
public class BagsAndRuleRequestToBagsAndFareRuleRequest implements Converter<BagsAndRuleRequest, BagsAndFareRuleRequest> {

	@Override
	public BagsAndFareRuleRequest convert(BagsAndRuleRequest source) {
		
		ObjectFactory objectFactory1 = new ObjectFactory();
		BagsAndFareRuleRequest destination = objectFactory1.createBagsAndFareRuleRequest();

		destination.setCookie(objectFactory1.createBagsAndFareRuleRequestCookie(source.getCookie()));
		destination.setExecution(objectFactory1.createBagsAndFareRuleRequestExecution(source.getExecution()));
		ArrayOfstring solutionIds = new ArrayOfstring();
		if (source.getSolutionIds() != null){
			for (String s : source.getSolutionIds()){
				solutionIds.getString().add(s);
			}
		}
		destination.setSolutionIds(objectFactory1.createBagsAndFareRuleRequestSolutionIds(solutionIds));
		return destination;
	}

}
