package com.alitalia.aem.service.impl.converter.home.mypersonalareamillemigliaservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.RetrieveStateListRequest;
import com.alitalia.aem.ws.mypersonalarea.staticdataservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mypersonalarea.staticdataservice.xsd1.StaticDataRequest;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveStateListRequestToStaticDataRequest.class)
public class RetrieveStateListRequestToStaticDataRequest implements Converter<RetrieveStateListRequest, StaticDataRequest> {

	@Reference
	private MMCustomerProfileDataToCustomerProfile mmCustomerProfileDataConverter;
	
	@Override
	public StaticDataRequest convert(RetrieveStateListRequest source) {
		if(source == null) throw new IllegalArgumentException("Request is null.");
		
		ObjectFactory objectFactory = new ObjectFactory();
		
		StaticDataRequest destination = objectFactory.createStaticDataRequest();
		destination.setItemCache(objectFactory.createStaticDataRequestItemCache(source.getItemCache()));
		destination.setLanguageCode(objectFactory.createStaticDataRequestLanguageCode(source.getLanguageCode()));
		destination.setMarket(objectFactory.createStaticDataRequestMarket(source.getMarket()));
		
		return destination;
	}
}