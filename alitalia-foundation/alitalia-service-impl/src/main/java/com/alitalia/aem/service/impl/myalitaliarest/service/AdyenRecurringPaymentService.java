package com.alitalia.aem.service.impl.myalitaliarest.service;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.AdyenRetrievalStoredPaymentData;
import com.alitalia.aem.common.messages.home.AdyenDisableStoredPaymentRequest;
import com.alitalia.aem.common.messages.home.AdyenDisableStoredPaymentResponse;
import com.alitalia.aem.common.messages.home.AdyenRetrievalStoredPaymentRequest;
import com.alitalia.aem.common.messages.home.AdyenRetrievalStoredPaymentResponse;
import com.alitalia.aem.common.messages.home.AdyenStoringPaymentRequest;
import com.alitalia.aem.common.messages.home.AdyenStoringPaymentResponse;
import com.alitalia.aem.common.messages.home.AdyenUpdateStoredPaymentRequest;
import com.alitalia.aem.common.messages.home.AdyenUpdateStoredPaymentResponse;
import com.alitalia.aem.service.api.home.IAdyenRecurringPaymentService;
import com.alitalia.aem.service.impl.myalitaliarest.client.AdyenRecurringPaymentServiceClient;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Service
@Component(immediate = true, metatype = false)
public class AdyenRecurringPaymentService implements IAdyenRecurringPaymentService {
	
	private static final Logger logger = LoggerFactory.getLogger(AdyenRecurringPaymentService.class);
	
	@Reference
	private AdyenRecurringPaymentServiceClient adyenRecurringPaymentServiceClient;
	
	@Override
	public AdyenStoringPaymentResponse storingPayment(AdyenStoringPaymentRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("MAPayment - Executing method storingPayment. the request is {}", request);
		}
		AdyenStoringPaymentResponse response = null;
		try {
			response = new AdyenStoringPaymentResponse();
			String url = "/storingpayment";
			
			//Chiedo i dati al AdyenRecurringPaymentServiceClient
			String responseClient = adyenRecurringPaymentServiceClient.getData(request,  url);
			logger.debug("MAPayment - Executed service storingpayment, the response is {}", responseClient);
			response = storingPaymentUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
		}
		catch (Exception e) {
			logger.error("MAPayment - Error executing service storingpayment: "+e.getMessage());
		}
		return response;
	}

	@Override
	public AdyenRetrievalStoredPaymentResponse retrievalStoredPayment(AdyenRetrievalStoredPaymentRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("MAPayment - Executing method retrievalStoredPayment. the request is {}", request);
		}
		AdyenRetrievalStoredPaymentResponse response = null;
		try {
			response = new AdyenRetrievalStoredPaymentResponse();
			String url = "/retrievalstoredpayment";
			
			//Chiedo i dati al AdyenRecurringPaymentServiceClient
			String responseClient = adyenRecurringPaymentServiceClient.getData(request,  url);
			logger.debug("MAPayment - Executed service retrievalstoredpayment, the response is {}", responseClient);
			response.setAdyenRetrievalStoredPaymentList(retrievalStoredPaymentUnmarshal(responseClient));
			response.setTid(request.getTid());
			response.setSid(request.getSid());
		}
		catch (Exception e) {
			logger.error("MAPayment - Error executing service retrievalstoredpayment: "+e.getMessage());
		}
		return response;
	}

	@Override
	public AdyenUpdateStoredPaymentResponse updateStoredPayment(AdyenUpdateStoredPaymentRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("MAPayment - Executing method updateStoredPayment. the request is {}", request);
		}
		AdyenUpdateStoredPaymentResponse response = null;
		try {
			response = new AdyenUpdateStoredPaymentResponse();
			String url = "/updatestoredpayment";
			
			//Chiedo i dati al AdyenRecurringPaymentServiceClient
			String responseClient = adyenRecurringPaymentServiceClient.getData(request,  url);
			logger.debug("MAPayment - Executed service updatestoredpayment, the response is {}", responseClient);
			response = updateStoredPaymentUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
		}
		catch (Exception e) {
			logger.error("MAPayment - Error executing service updatestoredpayment: "+e.getMessage());
		}
		return response;
	}

	@Override
	public AdyenDisableStoredPaymentResponse disableStoredPayment(AdyenDisableStoredPaymentRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("MAPayment - Executing method disableStoredPayment. the request is {}", request);
		}
		AdyenDisableStoredPaymentResponse response = null;
		try {
			response = new AdyenDisableStoredPaymentResponse();
			String url = "/disablestoredpayment";
			
			//Chiedo i dati al AdyenRecurringPaymentServiceClient
			String responseClient = adyenRecurringPaymentServiceClient.getData(request,  url);
			logger.debug("MAPayment - Executed service disableStoredPayment, the response is {}", responseClient);
			response = disableStoredPaymentUnmarshal(responseClient);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
		}
		catch (Exception e) {
			logger.error("MAPayment - Error executing service disableStoredPayment: "+e.getMessage());
		}
		return response;
	}

	private List<AdyenRetrievalStoredPaymentData> retrievalStoredPaymentUnmarshal(String responseClient) {
		return new Gson().fromJson(responseClient, new TypeToken<List<AdyenRetrievalStoredPaymentData>>(){}.getType());
	}

	private AdyenStoringPaymentResponse storingPaymentUnmarshal(String responseClient) {
		return new Gson().fromJson(responseClient, AdyenStoringPaymentResponse.class);
	}

	private AdyenUpdateStoredPaymentResponse updateStoredPaymentUnmarshal(String responseClient) {
		return new Gson().fromJson(responseClient, AdyenUpdateStoredPaymentResponse.class);
	}

	private AdyenDisableStoredPaymentResponse disableStoredPaymentUnmarshal(String responseClient) {
		return new Gson().fromJson(responseClient, AdyenDisableStoredPaymentResponse.class);
	}

}
