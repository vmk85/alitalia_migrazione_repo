package com.alitalia.aem.service.impl.converter.home.checkinservice;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.messages.home.CheckinSendSmsResponse;
import com.alitalia.aem.ws.sendsms.xsd.SmsMsgReturn;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

@Component(immediate = true, metatype = false)
@Service(value = CheckinSendSmsDataConverResponse.class)
public class CheckinSendSmsDataConverResponse implements Converter<SmsMsgReturn, CheckinSendSmsResponse> {

    @Override
    public CheckinSendSmsResponse convert(SmsMsgReturn source)
    {
        CheckinSendSmsResponse response = new CheckinSendSmsResponse();
        if(source != null){
            response.setMsgId(source.getMsgId());
            response.setDescription(source.getDescription());
            response.setResponseCode(source.getResponseCode());
        }
        return response;
    }
}