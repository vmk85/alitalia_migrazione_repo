package com.alitalia.aem.service.impl.converter.home.bookingwidget;

import java.util.ArrayList;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.TicketInfoData;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.ws.booking.widgetservice.xsd3.ArrayOfanyType;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.APassengerBase;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.InfoBase;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.PassengerType;
import com.alitalia.aem.ws.booking.widgetservice.xsd5.TicketInfo;

@Component(immediate = true, metatype = false)
@Service(value = APassengerBaseToPassengerBaseData.class)
public class APassengerBaseToPassengerBaseData implements Converter<APassengerBase, PassengerBaseData> {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Reference
	private InfoBaseToPassengerBaseInfoData passengerBaseInfoConverter;

	@Reference
	private TicketInfoToTicketInfoData ticketInfoConverer;

	@Reference
	private BookingWidgetServiceJAXBContextFactory jaxbContextFactory;

	@Override
	public PassengerBaseData convert(APassengerBase source) {
		PassengerBaseData destination = new PassengerBaseData();

		destination.setCouponPrice(source.getCouponPrice());
		destination.setExtraCharge(source.getExtraCharge());
		destination.setFee(source.getFee());
		destination.setGrossFare(source.getGrossFare());
		destination.setNetFare(source.getNetFare());

		JAXBElement<String> lastName = source.getLastName();
		if (lastName != null)
			destination.setLastName(lastName.getValue());

		JAXBElement<String> name = source.getName();
		if (name != null)
			destination.setName(name.getValue());

		PassengerType type = source.getType();
		if (type != null)
			destination.setType(PassengerTypeEnum.fromValue(type.value()));

		JAXBElement<Object> info = source.getInfo();
		if (info != null && info.getValue() != null) {
			try {
				Unmarshaller infoBaseUnmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
				@SuppressWarnings("unchecked")
				JAXBElement<InfoBase> sourceInfoBaseJAXB = (JAXBElement<InfoBase>) infoBaseUnmarshaller.unmarshal((Node) info.getValue());
				InfoBase sourceInfoBase = (InfoBase) sourceInfoBaseJAXB.getValue();
				
				destination.setInfo(passengerBaseInfoConverter.convert(sourceInfoBase));
			} catch (JAXBException e) {
				logger.error("Error while creating JAXBConext/Unmarshller for InfoBase: {}", e);
			}
		}

		JAXBElement<ArrayOfanyType> sourceTickets = source.getTickets();
		if (sourceTickets != null && sourceTickets.getValue() != null) {
			ArrayList<TicketInfoData> tickets = new ArrayList<TicketInfoData>();
			ArrayOfanyType sourceTicketsList = sourceTickets.getValue();
			
			try {
				Unmarshaller unmarshaller = jaxbContextFactory.getJaxbContext().createUnmarshaller();
				for (Object unmashalledObject : sourceTicketsList.getAnyType()) {
					TicketInfo sourceTicket = (TicketInfo) unmarshaller.unmarshal((Node) unmashalledObject);
					TicketInfoData ticketInfoData = ticketInfoConverer.convert((TicketInfo) sourceTicket);
					tickets.add(ticketInfoData);
				}
			} catch (JAXBException e) {
				logger.error("Error while creating JAXBConext/Unmarshller for TicketInfo: {}", e);
			}
			if (!tickets.isEmpty())
				destination.setTickets(tickets);
		}

		return destination;
	}

}
