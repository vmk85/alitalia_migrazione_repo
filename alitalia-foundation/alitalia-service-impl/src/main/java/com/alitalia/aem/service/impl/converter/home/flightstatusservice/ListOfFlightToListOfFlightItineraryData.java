package com.alitalia.aem.service.impl.converter.home.flightstatusservice;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.datatype.Duration;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.FlightItinerayData;
import com.alitalia.aem.ws.booking.flightstatusservice.xsd3.Flight;

@Component(immediate = true, metatype = false)
@Service(value = ListOfFlightToListOfFlightItineraryData.class)
public class ListOfFlightToListOfFlightItineraryData implements Converter<List<Flight>, List<FlightItinerayData>> {

	@Reference
	private FlightItinerayDetailsModelDataToFlightDetailsModel flightItineraryDetailsModelConverter;

	@Override
	public List<FlightItinerayData> convert(List<Flight> flights) {
		List<FlightItinerayData> result = new ArrayList<FlightItinerayData>();

		for(Flight source: flights){
			FlightItinerayData flight = new FlightItinerayData();

			flight.setArrival(source.getArrival().getValue());
			flight.setArrivalDate(source.getArrivalDate().toGregorianCalendar());
			Duration arrivalTime = source.getArrivalTime();
			if(arrivalTime != null)
				flight.setArrivalTime(arrivalTime);

			flight.setDeparture(source.getDeparture().getValue());

			Calendar departureDate = source.getDepartureDate().toGregorianCalendar();
			if (departureDate != null)
				flight.setDepartureDate(departureDate);

			Duration departureTime = source.getDepartureTime();
			if(departureTime != null)
				flight.setDepartureTime(departureTime);

			//		FlightDetailsModelData flightDetailsData = flightItineraryDetailsModelConverter.convert(source.getFlightDetails());
			//		flight.setFlightDetails(flightDetailsData);
			flight.setFlightLength(source.getFlightLength());
			flight.setFlightNumber(source.getFlightNumber().getValue());
			flight.setVector(source.getVector().getValue());
			result.add(flight);
		}

		return result;
	}
}