package com.alitalia.aem.service.impl.converter.home.checkinpassengerservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.mmb.MmbRequestBaseInfoData;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.RequestBaseInfo;
import com.alitalia.aem.ws.checkin.passengerservice.xsd1.ObjectFactory;


@Component(immediate=true, metatype=false)
@Service(value=CheckinRequestBaseInfoDataToRequestBaseInfo.class)
public class CheckinRequestBaseInfoDataToRequestBaseInfo implements Converter<MmbRequestBaseInfoData, RequestBaseInfo> {

	@Override
	public RequestBaseInfo convert(MmbRequestBaseInfoData source) {
		ObjectFactory objectFactory = new ObjectFactory();
		RequestBaseInfo destination = null;

		if (source != null) {
			destination = objectFactory.createRequestBaseInfo();
			destination.setX003CClientIPX003EKBackingField(source.getClientIp());
			destination.setX003CSessionIDX003EKBackingField(source.getSessionId());
			destination.setX003CSiteCodeX003EKBackingField(source.getSiteCode());
		}

		return destination;
	}

}
