package com.alitalia.aem.service.impl.converter.home.bookingpaymentservice;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.InsurancePolicyData;
import com.alitalia.aem.ws.booking.paymentservice.xsd2.InsurancePolicy;

@Component(immediate=true, metatype=false)
@Service(value=InsurancePolicyToInsurancePolicyData.class)
public class InsurancePolicyToInsurancePolicyData implements Converter<InsurancePolicy, InsurancePolicyData> {
	

	@Override
	public InsurancePolicyData convert(InsurancePolicy source) {
		
		InsurancePolicyData destination = null;
		
		if (source != null) {
			
			destination = new InsurancePolicyData();
			
			destination.setBuy(
					source.isBuy());
			
			if (source.getCardType() != null) {
				destination.setCardType(
						source.getCardType().getValue());
			}
			
			if (source.getCurrency() != null) {
				destination.setCurrency(
						source.getCurrency().getValue());
			}
			
			destination.setDiscount(
					source.getDiscount());
			
			destination.setDiscountedPrice(
					source.getDiscountedPrice());
			
			if (source.getErrorDescription() != null) {
				destination.setErrorDescription(
								source.getCurrency().getValue());
			}
			
			if (source.getPolicyNumber() != null) {
				destination.setPolicyNumber(
						source.getPolicyNumber().getValue());
			}
			
			destination.setTotalInsuranceCost(
					source.getTotalInsuranceCost());
			
			if (source.getTypeOfTrip() != null) {
				destination.setTypeOfTrip(
						source.getTypeOfTrip().getValue());
			}
			
		}
		
		return destination;
	}

}
