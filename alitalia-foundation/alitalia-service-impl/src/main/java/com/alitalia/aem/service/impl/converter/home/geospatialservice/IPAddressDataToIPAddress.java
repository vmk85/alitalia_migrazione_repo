package com.alitalia.aem.service.impl.converter.home.geospatialservice;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.converter.Converter;
import com.alitalia.aem.common.data.home.IPAddressData;
import com.alitalia.aem.common.data.home.enumerations.AddressFamilyEnum;
import com.alitalia.aem.ws.geospatial.service.xsd2.IPAddress;
import com.alitalia.aem.ws.geospatial.service.xsd3.AddressFamily;
import com.alitalia.aem.ws.geospatial.service.xsd5.ArrayOfunsignedShort;

@Component(immediate = true, metatype = false)
@Service(value = IPAddressDataToIPAddress.class)
public class IPAddressDataToIPAddress implements Converter<IPAddressData, IPAddress> {

	@Override
	public IPAddress convert(IPAddressData source) {
		IPAddress ipAddress = null;

		if (source!=null) {
			ipAddress = new IPAddress();
			
			long mAddress = source.getmAddress();
			AddressFamilyEnum addressFamilyEnum = source.getmFamily();
			int mHashCode = source.getmHashCode();
			List<Integer> numbers = source.getmNumbers();
			long scopeId = source.getmScopeId();
			
			ipAddress.setMAddress(mAddress);
			ipAddress.setMFamily(AddressFamily.fromValue(addressFamilyEnum.value()));
			ipAddress.setMHashCode(mHashCode);
			
			ArrayOfunsignedShort arrayOfunsignedShort = new ArrayOfunsignedShort();
			List<Integer> unsignedNumbers = arrayOfunsignedShort.getUnsignedShort();
			for(int n : numbers){
				unsignedNumbers.add(n);
			}
			ipAddress.setMNumbers(arrayOfunsignedShort);
			ipAddress.setMScopeId(scopeId);
		}
		return ipAddress;
	}
}