package com.alitalia.aem.service.impl.beanrest.staticdataservicerest;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.service.impl.converter.homerest.beans.DataRequest;
import com.alitalia.aem.service.impl.converter.homerest.beans.GetAllAirports;
import com.alitalia.aem.service.impl.converter.homerest.datachange.RestDataChange;
import com.alitalia.aem.service.impl.converter.homerest.restresponseunmarshal.AllAirportsResponseUnmarshal;
import com.alitalia.aem.service.impl.converter.homerest.staticdataservice.ResponseReaderService;
import com.google.gson.Gson;

@Service(value = StaticDataServiceClientRest.class)
@Component(immediate = true, metatype = false) //@Component : Component is defined to generate metatype information, however no properties 
@Properties({ //or only private properties have been defined; in case no properties or only private properties are wanted, 
	@Property(name = "service.vendor", value = "Reply") }) //consider to use 'metatype=false' (org.apache.felix:maven-scr-plugin:1.25.0:scr:generate-scr-scrdescriptor:process-classes)
public class StaticDataServiceClientRest {
	
private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	

	public RetrieveAirportsResponse getAllAirportsData(RetrieveAirportsRequest request) {
//		if (this.initRequired) {
//			init();
//			this.initRequired = false;
//		}
		
		InputStream is = null;
		OutputStream os = null;
		URLConnection connection = null;
		Gson gson = new Gson();
		String url = "";
		String input = "";
		String jsonString = "";
		
		ResponseReaderService responseReaderService = new ResponseReaderService();
		AllAirportsResponseUnmarshal allAirportsResponseUnmarshal = new AllAirportsResponseUnmarshal();
		GetAllAirports responseToChange = new GetAllAirports();
		RestDataChange restDataChange = new RestDataChange();
		RetrieveAirportsResponse response = new RetrieveAirportsResponse();
		
		url = "http://azwtapb001.user.alitalia.local:9003/api/v0.1/staticdata/getallairports";
		
		DataRequest dataRequest = new DataRequest();
		
		dataRequest.setLanguage("1");
		dataRequest.setMarket("2");
		
		input = gson.toJson(dataRequest);
		
		try {
			connection = new URL(url).openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Accept-Charset", "application/json");
			connection.setRequestProperty("Content-Type", "application/json");
			
			os = connection.getOutputStream();
			os.write(input.getBytes());
			
			is = connection.getInputStream();
			
			jsonString = responseReaderService.retrieveResponse(is);
			responseToChange = allAirportsResponseUnmarshal.allAirportsUnmarshal(jsonString);
			response = restDataChange.changeDataForResponse(responseToChange);
			
		}
		catch (IOException e) {
			e.printStackTrace();
		}
//		finally {
//			try {
//				is.close();
//				os.close();
//			}
//			catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
		
		
		return response;
	}

}
