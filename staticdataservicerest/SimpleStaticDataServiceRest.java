package com.alitalia.aem.service.impl.beanrest.staticdataservicerest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.service.api.homerest.StaticDataServiceRest;
import com.alitalia.aem.service.impl.home.SimpleStaticDataService;

@Service
@Component(immediate = true, metatype = false)
public class SimpleStaticDataServiceRest implements StaticDataServiceRest {
	
	private static final Logger logger = LoggerFactory.getLogger(SimpleStaticDataService.class);
	
	@Reference
	private StaticDataServiceClientRest staticDataServiceClientRest;
	
	@Override
	public RetrieveAirportsResponse getAllAirports(RetrieveAirportsRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing method retrieveAirports. The request is {}", request);
		}
		
		try {
			RetrieveAirportsResponse response = new RetrieveAirportsResponse();
			response = staticDataServiceClientRest.getAllAirportsData(request);
			response.setTid(request.getTid());
			response.setSid(request.getSid());
			if (logger.isDebugEnabled()) {
				logger.debug("Executed method. The response is {}", response);
			}
			
			return response;
		}
		catch (Exception e) {
			logger.error("Exception while executing method.", e);
			throw new AlitaliaServiceException("Exception executing retrieveAirports: Sid ["+request.getSid()+"]" , e);
		}

	}

	@Override
	public RetrieveAirportsResponse getAirportDestination(RetrieveAirportsRequest request) {
		// TODO Auto-generated method stub
		return null;
	}
}