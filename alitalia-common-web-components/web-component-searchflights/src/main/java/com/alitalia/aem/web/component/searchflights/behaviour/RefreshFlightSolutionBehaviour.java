package com.alitalia.aem.web.component.searchflights.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionResponse;
import com.alitalia.aem.service.api.home.BookingSearchService;

@Component(immediate = true, metatype = false)
@Service(value = RefreshFlightSolutionBehaviour.class)
public class RefreshFlightSolutionBehaviour extends Behaviour<SearchFlightSolutionRequest, SearchFlightSolutionResponse> {

	@Reference
	private BookingSearchService bookingSearchService;

	@Override
	public SearchFlightSolutionResponse executeOrchestration(SearchFlightSolutionRequest request) {
		if(request == null)
			throw new IllegalArgumentException("Search Flight Solution request is null.");

		SearchFlightSolutionResponse response = bookingSearchService.executeRefreshSearch(request);

		return response;
	}

}
