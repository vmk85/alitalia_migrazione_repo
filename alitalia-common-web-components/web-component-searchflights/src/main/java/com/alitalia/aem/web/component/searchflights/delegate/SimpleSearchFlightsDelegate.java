package com.alitalia.aem.web.component.searchflights.delegate;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.component.AlitaliaWebComponentCommon;
import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.data.PassengerBase;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.common.messages.home.BagsAndRuleRequest;
import com.alitalia.aem.common.messages.home.BagsAndRuleResponse;
import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.common.messages.home.SearchBookingSolutionResponse;
import com.alitalia.aem.common.messages.home.SearchFlexibleDatesMatrixResponse;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionResponse;
import com.alitalia.aem.common.messages.home.SearchFlightsRequest;
import com.alitalia.aem.common.messages.home.SearchFlightsResponse;
import com.alitalia.aem.web.component.searchflights.behaviour.BookingSellupBehaviour;
import com.alitalia.aem.web.component.searchflights.behaviour.BookingTaxSearchBehaviour;
import com.alitalia.aem.web.component.searchflights.behaviour.ExecuteBagsAndRuleBehaviour;
import com.alitalia.aem.web.component.searchflights.behaviour.FullTextRulesSearchBehaviour;
import com.alitalia.aem.web.component.searchflights.behaviour.MetaSearchBehaviour;
import com.alitalia.aem.web.component.searchflights.behaviour.RefreshFlightSolutionBehaviour;
import com.alitalia.aem.web.component.searchflights.behaviour.RetrieveAirportsBehaviour;
import com.alitalia.aem.web.component.searchflights.behaviour.SearchFlexibleDatesMatrixBehaviour;
import com.alitalia.aem.web.component.searchflights.behaviour.SearchFlightSolutionBehaviour;
import com.alitalia.aem.web.component.searchflights.behaviour.SearchFlightsBehaviour;
import com.alitalia.aem.web.component.searchflights.behaviour.SearchFlightsSolutionCustomRibbonBehaviour;
import com.alitalia.aem.web.component.searchflights.behaviour.SearchMultiLegNextSolutionBehaviour;
import com.alitalia.aem.web.component.searchflights.behaviour.SearchMultiLegSolutionBehaviour;
import com.alitalia.aem.web.component.searchflights.behaviour.SearchYouthSolutionBehaviour;

@Component(immediate = true, metatype = true)
@Properties({
	@Property( name = SimpleSearchFlightsDelegate.MINIFARE_SOURCE_BRAND_CODE_PROPERTY, label = "Source brand code eligible to be mapped to mini-fare brand." ),
	@Property( name = SimpleSearchFlightsDelegate.MINIFARE_TARGET_BRAND_CODE_PROPERTY, label = "Target brand code to assign to mapped mini-fare brand." ),
	@Property( name = SimpleSearchFlightsDelegate.CONTINUITY_SARDINIA_AIRPORT_CODES_A_PROPERTY, label = "Airport codes enabled for Sardinia territorial continuity (first set), as a comma-separated list." ),
	@Property( name = SimpleSearchFlightsDelegate.CONTINUITY_SARDINIA_AIRPORT_CODES_B_PROPERTY, label = "Airport codes enabled for Sardinia territorial continuity (second set), as a comma-separated list." ),
	@Property( name = SimpleSearchFlightsDelegate.CONTINUITY_SICILY_AIRPORT_CODES_A_PROPERTY, label = "Airport codes enabled for Sicily territorial continuity (first set), as a comma-separated list." ),
	@Property( name = SimpleSearchFlightsDelegate.CONTINUITY_SICILY_AIRPORT_CODES_B_PROPERTY, label = "Airport codes enabled for Sicily territorial continuity (second set), as a comma-separated list." )
})
@Service(value=SearchFlightsDelegate.class)
public class SimpleSearchFlightsDelegate implements SearchFlightsDelegate {

	private static Logger logger = LoggerFactory.getLogger(SimpleSearchFlightsDelegate.class);

	public static final String MINIFARE_SOURCE_BRAND_CODE_PROPERTY = "minifare.source.brand.code";
	public static final String MINIFARE_TARGET_BRAND_CODE_PROPERTY = "minifare.target.brand.code";

	public static final String CONTINUITY_SARDINIA_AIRPORT_CODES_A_PROPERTY = "continuity.sardinia.airport.codes.a";
	public static final String CONTINUITY_SARDINIA_OLBIA_AIRPORT_CODES_A_PROPERTY = "continuity.sardinia.olbia.airport.codes.a";
	public static final String CONTINUITY_SARDINIA_AIRPORT_CODES_B_PROPERTY = "continuity.sardinia.airport.codes.b";
	public static final String CONTINUITY_SICILY_AIRPORT_CODES_A_PROPERTY = "continuity.sicily.airport.codes.a";
	public static final String CONTINUITY_SICILY_AIRPORT_CODES_B_PROPERTY = "continuity.sicily.airport.codes.b";
	private static final String BRAND_FAMILY = "FA";

	public static final String PROMO_BAGGAGE_BOOK_DATE_START = "promoBaggage.bookDate.start";
	public static final String PROMO_BAGGAGE_BOOK_DATE_END = "promoBaggage.bookDate.end";
	public static final String PROMO_BAGGAGE_DEPARTURE_DATE_START = "promoBaggage.depDate.start";
	public static final String PROMO_BAGGAGE_DEPARTURE_DATE_END = "promoBaggage.depDate.end";

	private ComponentContext componentContext;

	@Reference
	private AlitaliaWebComponentCommon alitaliaWebComponentCommon;

	@Reference
	private RetrieveAirportsBehaviour retrieveAirportsBehaviour;

	@Reference
	private SearchFlightsBehaviour searchFlightsBehaviour;

	@Reference
	private SearchFlightSolutionBehaviour searchFlightSolutionBehaviour;

	@Reference
	private SearchFlightsSolutionCustomRibbonBehaviour searchFlightSolutionCustomRibbonBehaviour;

	@Reference
	private RefreshFlightSolutionBehaviour refreshFlightSolutionBehaviour;

	@Reference
	private SearchYouthSolutionBehaviour searchYouthSolutionBehaviour;

	@Reference
	private BookingSellupBehaviour bookingSellupBehaviour;

	@Reference
	private SearchFlexibleDatesMatrixBehaviour flexibleDatesMatrixBehaviour;

	@Reference
	private MetaSearchBehaviour metaSearchBehaviour;

	@Reference
	private BookingTaxSearchBehaviour bookingTaxSearchBehaviour;

	@Reference
	private SearchMultiLegSolutionBehaviour searchMultiLegSolutionBehaviour;

	@Reference
	private SearchMultiLegNextSolutionBehaviour searchMultiLegNextSolutionBehaviour;

	@Reference
	private FullTextRulesSearchBehaviour fullTextRulesBehaviour;

	@Reference
	private ExecuteBagsAndRuleBehaviour executeBagsAndRulesBehaviour;

	/* SCR lifecycle */

	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}

	@Modified
	private void modified(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	/* service methods */

	@Override
	@Deprecated
	public RetrieveAirportsResponse retrieveAirports(RetrieveAirportsRequest request) {
		BehaviourExecutor<RetrieveAirportsBehaviour, RetrieveAirportsRequest, RetrieveAirportsResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(retrieveAirportsBehaviour, request);
	}

	@Override
	@Deprecated
	public SearchFlightsResponse searchFlights(SearchFlightsRequest request) {
		BehaviourExecutor<SearchFlightsBehaviour, SearchFlightsRequest, SearchFlightsResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(searchFlightsBehaviour, request);
	}

	/**
	 * 
	 * @author R.Capitini
	 * 
	 * Performs a search of the information needed to populate flights grid and ribbons.
	 * 
	 * @param request An object from SearchFlightSolutionRequest
	 * @see SearchFlightSolutionRequest
	 * @return A SearchFlightSolutionResponse containing the results of the following calls:
	 * - operation execute with BrandSearch input
	 * - operation execute with RibbonSearch input
	 * - operation extraCharge with BrandSearch input
	 * The results are stored inside availableFligths and extraChargePassengerList attributes
	 * For what concerns availableFlights, only routes and tabs attributes will be significant 
	 * @see SearchFlightSolutionResponse
	 */
	@Override
	public SearchFlightSolutionResponse searchInitialSolutions(SearchFlightSolutionRequest request) {
		BehaviourExecutor<SearchFlightSolutionBehaviour, SearchFlightSolutionRequest, SearchFlightSolutionResponse> executor = 
				new BehaviourExecutor<>();

		SearchFlightSolutionResponse response = executor.executeBehaviour(searchFlightSolutionBehaviour, request);
		if (response != null) {

			manageFakeBrands(response);

			if (request.getCug().equals("FAM")) {
				manageFamilyBrand(response);
			} else {
				//remove the brand EconomyBasic,
				//because it represents the family fare for (2 adults and 1 child/infant for ADT)
				removeBrand(response, "EconomyBasic");
			}

			manageExtraCharge(request,response);
			if (response.getAvailableFlights() != null) {
				response.setBusCarrier(searchForABus(response.getAvailableFlights().getRoutes()));
				if (response.getAvailableFlights().getRoutes() != null && 
						response.getAvailableFlights().getRoutes().size() != 0 && 
						response.getAvailableFlights().getRoutes().get(0).getFlights() == null){ // Routes != null e flights prima tratta == null
					response.getAvailableFlights().setRoutes(new ArrayList<RouteData>());
				}
			}
		}

		return response;
	}

	@Override
	@Deprecated
	public SearchFlightSolutionResponse searchInitialCustomRubbonSolutions(SearchFlightSolutionRequest request) {
		BehaviourExecutor<SearchFlightsSolutionCustomRibbonBehaviour, SearchFlightSolutionRequest, SearchFlightSolutionResponse> executor = 
				new BehaviourExecutor<>();

		SearchFlightSolutionResponse response = executor.executeBehaviour(searchFlightSolutionCustomRibbonBehaviour, request);

		if (response != null) {

			if (request.getCug().equals("FAM")) {
				manageFamilyBrand(response);
			} else {
				//remove the brand EconomyBasic,
				//because it represents the family fare for (2 adults and 1 child/infant for ADT)
				removeBrand(response, "EconomyBasic");
			}

			manageExtraCharge(request,response);
			if (response.getAvailableFlights() != null) {
				response.setBusCarrier(searchForABus(response.getAvailableFlights().getRoutes()));
			}
		}

		return response;
	}

	@Override
	@Deprecated
	public SearchFlightSolutionResponse searchRefreshSolutions(SearchFlightSolutionRequest request) {
		BehaviourExecutor<RefreshFlightSolutionBehaviour, SearchFlightSolutionRequest, SearchFlightSolutionResponse> executor = 
				new BehaviourExecutor<>();

		SearchFlightSolutionResponse response = executor.executeBehaviour(refreshFlightSolutionBehaviour, request);

		if (response != null) {

			if (request.getCug().equals("FAM")) {
				manageFamilyBrand(response);
			} else {
				//remove the brand EconomyBasic,
				//because it represents the family fare for (2 adults and 1 child/infant for ADT)
				removeBrand(response, "EconomyBasic");
			}
			if (response.getAvailableFlights() != null) {
				response.setBusCarrier(searchForABus(response.getAvailableFlights().getRoutes()));
			}
		}

		return response;

	}

	@Override
	@Deprecated
	public SearchFlightSolutionResponse searchYoungSolutions(SearchFlightSolutionRequest request) {
		BehaviourExecutor<SearchYouthSolutionBehaviour, SearchFlightSolutionRequest, SearchFlightSolutionResponse> executor =
				new BehaviourExecutor<>();
		SearchFlightSolutionResponse response = executor.executeBehaviour(searchYouthSolutionBehaviour, request);
		if (response != null) {
			manageExtraCharge(request,response);
			if (response.getAvailableFlights() != null) {
				response.setBusCarrier(searchForABus(response.getAvailableFlights().getRoutes()));
			}
		}
		return response;
	}

	@Override
	public SearchBookingSolutionResponse bookingSellup(SearchFlightSolutionRequest request) {
		BehaviourExecutor<BookingSellupBehaviour, SearchFlightSolutionRequest, SearchBookingSolutionResponse> executor =
				new BehaviourExecutor<>();
		SearchBookingSolutionResponse bookingSolutionResponse = executor.executeBehaviour(bookingSellupBehaviour, request);
		if (bookingSolutionResponse != null) {
			if (bookingSolutionResponse.getRoutesData() != null) {
				searchForABus(bookingSolutionResponse.getRoutesData().getRoutesList());
			}
		}
		return bookingSolutionResponse;
	}

	@Override
	@Deprecated
	public SearchFlightSolutionResponse bookingTaxSearch(SearchFlightSolutionRequest request) {
		BehaviourExecutor<BookingTaxSearchBehaviour, SearchFlightSolutionRequest, SearchFlightSolutionResponse> executor =
				new BehaviourExecutor<>();
		return executor.executeBehaviour(bookingTaxSearchBehaviour, request);
	}

	@Override
	public SearchFlightSolutionResponse searchMultipleLegsSolutions(SearchFlightSolutionRequest request) {
		BehaviourExecutor<SearchMultiLegSolutionBehaviour, SearchFlightSolutionRequest, SearchFlightSolutionResponse> executor = 
				new BehaviourExecutor<>();
		SearchFlightSolutionResponse response = executor.executeBehaviour(searchMultiLegSolutionBehaviour, request);
		if (response != null) {
			manageExtraCharge(request,response);
			if (response.getAvailableFlights() != null) {
				response.setBusCarrier(searchForABus(response.getAvailableFlights().getRoutes()));
			}
		}
		return response;
	}

	@Override
	@Deprecated
	public SearchFlightSolutionResponse searchMultipleLegsNextSolution(SearchFlightSolutionRequest request) {
		BehaviourExecutor<SearchMultiLegNextSolutionBehaviour, SearchFlightSolutionRequest, SearchFlightSolutionResponse> executor = 
				new BehaviourExecutor<>();

		SearchFlightSolutionResponse response = executor.executeBehaviour(searchMultiLegNextSolutionBehaviour, request);
		if (response != null) {
			if (response.getAvailableFlights() != null) {
				response.setBusCarrier(searchForABus(response.getAvailableFlights().getRoutes()));
			}
		}
		return response;
	}

	@Override
	public SearchFlexibleDatesMatrixResponse searchFlexibleDatesMatrixSolutions(
			SearchFlightSolutionRequest request) {
		BehaviourExecutor<SearchFlexibleDatesMatrixBehaviour, SearchFlightSolutionRequest, SearchFlexibleDatesMatrixResponse> executor = 
				new BehaviourExecutor<>();
		return executor.executeBehaviour(flexibleDatesMatrixBehaviour, request);
	}

	@Override
	public SearchFlightSolutionResponse metaSearch(SearchFlightSolutionRequest request) {
		BehaviourExecutor<MetaSearchBehaviour, SearchFlightSolutionRequest, SearchFlightSolutionResponse> executor = new BehaviourExecutor<>();
		SearchFlightSolutionResponse response = executor.executeBehaviour(metaSearchBehaviour, request);
		if (response != null) {
			if (response.getAvailableFlights() != null) {
				response.setBusCarrier(searchForABus(response.getAvailableFlights().getRoutes()));
			}
			if (response.getAvailableFlights().getRoutesList() != null && response.getAvailableFlights().getRoutesList().get(0).getPassengers()!=null){
				List<PassengerBase> passengerExtraChargeList = new ArrayList<>();
				for (PassengerBaseData passenger : response.getAvailableFlights().getRoutesList().get(0).getPassengers()){
					passengerExtraChargeList.add(passenger);
				}
				response.setExtraChargePassengerList(passengerExtraChargeList);
			}
		}

		return response;
	}

	@Override
	public SearchFlightSolutionResponse executeFullTextRulesSearch(SearchFlightSolutionRequest request) {
		BehaviourExecutor<FullTextRulesSearchBehaviour, SearchFlightSolutionRequest, SearchFlightSolutionResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(fullTextRulesBehaviour, request);
	}

	@Override
	@Deprecated
	public BagsAndRuleResponse retrieveBagsAndFareRules(BagsAndRuleRequest request) {
		BehaviourExecutor<ExecuteBagsAndRuleBehaviour, BagsAndRuleRequest, BagsAndRuleResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(executeBagsAndRulesBehaviour, request);
	}

	/**
	 * manage the brand family
	 */
	private void manageFamilyBrand(
			SearchFlightSolutionResponse response){

		ArrayList<RouteData> routes = response.getAvailableFlights().getRoutes();
		if (response.getAvailableFlights() != null && !routes.isEmpty()) {

			int cont = 0, indexBrand = -1;
			for (RouteData routeData : routes) {

				//search the position of the brand family
				indexBrand = getIndexOfBrand(routeData, BRAND_FAMILY);

				if (indexBrand > -1) {
					//check if all brand are available in the routes 
					if (checkAvailabilityForBrand(routeData, indexBrand)) {
						cont++;
					}
				}
			}
			if (cont == routes.size()) {
				filterResponseExtractingFamilySolutions(routes, indexBrand);
			} else {
				removeBrand(response,BRAND_FAMILY);
			}
		}

	}

	/**
	 * It filters the response for family search, in order to obtain one brandData with code "family"
	 * if the search returned solutions for family, otherwise it don't modify the response.  
	 * @param response
	 */
	private void filterResponseExtractingFamilySolutions(
			ArrayList<RouteData> routes, int indexBrand) {

		//extract the family brand.
		for (RouteData routeData : routes) {
			if (indexBrand > -1) {
				for ( FlightData flight : routeData.getFlights() ) {
					List<BrandData> brandListForFamily = new ArrayList<BrandData>();
					brandListForFamily.add(flight.getBrands().get(indexBrand));
					flight.setBrands(brandListForFamily);
				}
			}
		}
	}

	/**
	 * It removes the brand by brandCode
	 * @param response
	 * @param brandCode 
	 */
	private void removeBrand(
			SearchFlightSolutionResponse response, String brandCode) {
		if (response.getAvailableFlights() != null && !response.getAvailableFlights().getRoutes().isEmpty()) {
			for (RouteData routeData : response.getAvailableFlights().getRoutes()) {

				//search the position of the brand EconomyBasic
				int indexBrand = getIndexOfBrand(routeData, brandCode);
				//for each flight remove the brand
				if (indexBrand > -1) {
					for ( FlightData flight : routeData.getFlights() ) {
						List<BrandData> newbrandList = new ArrayList<BrandData>();
						flight.getBrands().remove(indexBrand);
						newbrandList.addAll(flight.getBrands());
						flight.setBrands(newbrandList);
					}
				}

			}
		}
	}

	/**
	 * It search brand in the routeData by the brandCode, and return the index
	 * @param routeData
	 * @param brandCode
	 * @return -1 if not found
	 */
	private int getIndexOfBrand(RouteData routeData, String brandCode){
		FlightData flightData = routeData.getFlights().get(0);
		int indexBrand = -1;
		boolean trovato = false;
		for (BrandData brandData : flightData.getBrands()) {
			indexBrand++;
			if (brandData.getCode().equalsIgnoreCase(brandCode) ) {
				trovato = true;
				break;
			}
		}
		if (!trovato) {
			return -1;
		}
		return indexBrand;
	}

	/**
	 * It checks the availability of the brand at index specified in indexBrand,
	 * for all flights in the routeData
	 * @param routeData
	 * @param indexBrand
	 * @param brandFamily
	 * @return false if all brands are not available
	 */
	private boolean checkAvailabilityForBrand(RouteData routeData, int indexBrand) {

		int cont = 0;
		for (FlightData flightData : routeData.getFlights()) {
			BrandData brandData = flightData.getBrands().get(indexBrand);
			if (!brandData.isEnabled() || brandData.getSeatsAvailable()<=0) {
				cont++;
			}
		}
		if (cont == routeData.getFlights().size()) {
			return false;
		}
		return true;

	}

	/**
	 * 
	 * @param request 
	 * @param response
	 */
	private void manageExtraCharge(SearchFlightSolutionRequest request, SearchFlightSolutionResponse response) {
		//		String from = request.getFromSearchElement();
		//		String to = request.getToSearchElement();
		//		
		//		if (isSliceSardiniaTerritorialContinuity(from, to)) 
		//				|| isSliceSicilyTerritorialContinuity(from, to)) {
		cleanExtraChargeFareForPassengers(response.getExtraChargePassengerList());
		//		}
	}

	/**
	 * For each passenger it set to zero the extra charge fare.
	 * @param response
	 */
	private void cleanExtraChargeFareForPassengers(List<PassengerBase> extraChargePassengerList) {
		for (PassengerBase passenger : extraChargePassengerList) {
			PassengerBaseData passengerBaseData = (PassengerBaseData) passenger;
			passengerBaseData.setExtraCharge(new BigDecimal(0));
		}

	}

	/**
	 * It obtains a list of directFligth for the current response
	 * @param response
	 */
	private boolean searchForABus(List<RouteData> routesList) {
		boolean isBusCarrier = false;

		if (!alitaliaWebComponentCommon.getBookingBusEnabled()) {
			return false;
		}

		if (routesList != null) {
			for (RouteData route : routesList) {
				for (FlightData flight : route.getFlights()) {
					List<FlightData> directFlightList = new ArrayList<FlightData>();
					if (flight instanceof ConnectingFlightData) {
						directFlightList = ((ConnectingFlightData) flight).getFlights();
					} else {
						directFlightList.add(flight);
					}
					if (manageDirectFlightDataForBus(directFlightList)) {
						isBusCarrier = true;
					}
				}
			}
		}
		return isBusCarrier;
	}

	/**
	 * It sets the bus attribute of a DirectFlightData
	 * according to its fligthNumber and the range provided by configuration 
	 * @param directFlightList
	 */
	private boolean manageDirectFlightDataForBus(List<FlightData> flightList) {
		boolean isBusCarrier = false;
		if (flightList != null) {
			for (FlightData flight : flightList) {
				if (flight instanceof DirectFlightData) {
					DirectFlightData directFlight = (DirectFlightData) flight;
					int fligthNumber = -1;
					try {
						fligthNumber = Integer.parseInt(directFlight.getFlightNumber());
					} catch (NumberFormatException e) {
						logger.error("Error converting fligthNumber in Integer: ", e);
					}
					boolean bus = alitaliaWebComponentCommon.isBusNumber(fligthNumber);
					if (bus) {
						isBusCarrier = true;
					}
					directFlight.setBus(bus);
				}
			}
		}
		return isBusCarrier;
	}

	/**
	 * It check if the Airport Codes are belonging to the territorial continuity for Sardinia
	 * @param from
	 * @param to
	 * @return
	 */
	public boolean isSliceSardiniaTerritorialContinuity(String from, String to) {
        Set<String> airportCodesA = createSetFromProperty(CONTINUITY_SARDINIA_AIRPORT_CODES_A_PROPERTY);
        Set<String> airportCodesB = createSetFromProperty(CONTINUITY_SARDINIA_AIRPORT_CODES_B_PROPERTY);
        return airportsBelongToSets(from, to, airportCodesA, airportCodesB);
    }

    public boolean isSliceSardiniaTerritorialContinuityOlbia(String from, String to) {
        Set<String> airportCodesA = createSetFromProperty(CONTINUITY_SARDINIA_OLBIA_AIRPORT_CODES_A_PROPERTY);
        Set<String> airportCodesB = createSetFromProperty(CONTINUITY_SARDINIA_AIRPORT_CODES_B_PROPERTY);
        return airportsBelongToSets(from, to, airportCodesA, airportCodesB);
    }

	/**
	 * It check if the Airport Codes are belonging to the territorial continuity for Sicily
	 * @param from
	 * @param to
	 * @return
	 */
	public boolean isSliceSicilyTerritorialContinuity(String from, String to) {
		Set<String> airportCodesA = createSetFromProperty(CONTINUITY_SICILY_AIRPORT_CODES_A_PROPERTY);
		Set<String> airportCodesB = createSetFromProperty(CONTINUITY_SICILY_AIRPORT_CODES_B_PROPERTY);
		return airportsBelongToSets(from, to, airportCodesA, airportCodesB);
	}

	/**
	 * Util method to check if the two ariport codes are in the sets
	 * @param from
	 * @param to
	 * @param airportCodesA
	 * @param airportCodesB
	 * @return
	 */
	private boolean airportsBelongToSets(String from, String to, Set<String> airportCodesA, Set<String> airportCodesB) {
		if ( airportCodesA.contains(from) ) {
			if ( airportCodesB.contains(to) ) {
				return true;
			}
		} else if ( airportCodesB.contains(from) ) {
			if ( airportCodesA.contains(to) ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @param propertyName
	 * @return
	 */
	private Set<String> createSetFromProperty(String propertyName) {
		String propertyValue = PropertiesUtil.toString(componentContext.getProperties().get(propertyName), "");
		String[] setElements = propertyValue.split(",");
		Set<String> set = new HashSet<String>();
		for (String setElement : setElements) {
			set.add(setElement);
		}
		return set;
	}

	/**
	 * Check if departureDate is between BaggagePromo startDepartureDate and endDepartureDate 
	 * and if now is between startBookingDate and endBookingDate. 
	 * @param departureDate
	 * @return true if date are compatible with promo period, false otherwise
	 */
	public boolean checkPromoBaggagePeriod(Calendar departureDate) {

		String startBook = PropertiesUtil.toString(componentContext.getProperties().get(PROMO_BAGGAGE_BOOK_DATE_START), "");
		String endBook = PropertiesUtil.toString(componentContext.getProperties().get(PROMO_BAGGAGE_BOOK_DATE_END), "");
		String startDep = PropertiesUtil.toString(componentContext.getProperties().get(PROMO_BAGGAGE_DEPARTURE_DATE_START), "");
		String endDep = PropertiesUtil.toString(componentContext.getProperties().get(PROMO_BAGGAGE_DEPARTURE_DATE_END), "");

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Calendar startBookDate = Calendar.getInstance();
		Calendar endBookDate = Calendar.getInstance();
		Calendar startDepDate = Calendar.getInstance();
		Calendar endDepDate = Calendar.getInstance();
		try {
			startBookDate.setTime(sdf.parse(startBook));
			endBookDate.setTime(sdf.parse(endBook));
			startDepDate.setTime(sdf.parse(startDep));
			endDepDate.setTime(sdf.parse(endDep));
		} catch (ParseException e) {
			logger.error("Error parsing date: ", e);
		}
		Calendar now = Calendar.getInstance();

		if( now.before(startBookDate) || now.after(endBookDate) || departureDate.before(startDepDate) || departureDate.after(endDepDate) ){
			return false;
		}

		return true;
	}

	private void manageFakeBrands(SearchFlightSolutionResponse response) {

		String brandToRemove = null;
		if (response.getAvailableFlights() != null && response.getAvailableFlights().getRoutes() != null && response.getAvailableFlights().getRoutes().size()>0 && areYSAndYLBrandsAvailable(response.getAvailableFlights().getRoutes().get(0).getFlights().get(0).getBrands())){ //Condizione per eliminazione di un brand
			if (isFakeBrand("YL", response.getAvailableFlights().getRoutes())){
				brandToRemove = "YL";
				logger.debug("YL To Remove");
			}else if (isFakeBrand("YS", response.getAvailableFlights().getRoutes())){
				logger.debug("YS To Remove");
				brandToRemove = "YS";
			}
		}
		if (brandToRemove != null){
			removeBrand(response, brandToRemove);
			logger.debug("EconomyBrandRemoved: ["+brandToRemove+"]");
		}
	}

	private boolean areYSAndYLBrandsAvailable(List<BrandData> brands) {
		boolean isYSAvailable = false;
		boolean isYLAvailable = false;
		for(BrandData brand : brands) {
			if("YS".equalsIgnoreCase(brand.getCode()))
				isYSAvailable = true;
			if("YL".equalsIgnoreCase(brand.getCode()))
				isYLAvailable = true;
		}
		return isYSAvailable && isYLAvailable;
	}

	private boolean isFakeBrand(String brand, List<RouteData> routes) {
		if (routes != null){
			for(RouteData route : routes) {
				if (route.getFlights() != null){
					for (FlightData flight : route.getFlights()){
						if (flight.getBrands() != null){
							for (BrandData b : flight.getBrands()){
								if (brand.equalsIgnoreCase(b.getCode()) && b.getSolutionId() != null){
									return false;
								}
							}
						}
						
					}
				}
			}
		}
		return true;
	}
}