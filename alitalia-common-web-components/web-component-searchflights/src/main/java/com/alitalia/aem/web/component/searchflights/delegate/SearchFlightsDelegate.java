package com.alitalia.aem.web.component.searchflights.delegate;

import java.util.Calendar;

import com.alitalia.aem.common.messages.home.BagsAndRuleRequest;
import com.alitalia.aem.common.messages.home.BagsAndRuleResponse;
import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.common.messages.home.SearchBookingSolutionResponse;
import com.alitalia.aem.common.messages.home.SearchFlexibleDatesMatrixResponse;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionResponse;
import com.alitalia.aem.common.messages.home.SearchFlightsRequest;
import com.alitalia.aem.common.messages.home.SearchFlightsResponse;

public interface SearchFlightsDelegate {

	@Deprecated
	RetrieveAirportsResponse retrieveAirports(RetrieveAirportsRequest request);
	@Deprecated
	SearchFlightsResponse searchFlights(SearchFlightsRequest request);
	SearchFlightSolutionResponse searchInitialSolutions(SearchFlightSolutionRequest request);
	@Deprecated
	SearchFlightSolutionResponse searchInitialCustomRubbonSolutions(SearchFlightSolutionRequest request);
	@Deprecated
	SearchFlightSolutionResponse searchRefreshSolutions(SearchFlightSolutionRequest request);
	@Deprecated
	SearchFlightSolutionResponse searchYoungSolutions(SearchFlightSolutionRequest request);
	SearchBookingSolutionResponse bookingSellup(SearchFlightSolutionRequest request);
	@Deprecated
	SearchFlightSolutionResponse bookingTaxSearch(SearchFlightSolutionRequest request);
	SearchFlightSolutionResponse searchMultipleLegsSolutions(SearchFlightSolutionRequest request);
	@Deprecated
	SearchFlightSolutionResponse searchMultipleLegsNextSolution(SearchFlightSolutionRequest request);
	boolean isSliceSardiniaTerritorialContinuity(String from, String to);
	boolean isSliceSardiniaTerritorialContinuityOlbia(String from, String to);
	boolean isSliceSicilyTerritorialContinuity(String from, String to);
	boolean checkPromoBaggagePeriod(Calendar departureDate);
	SearchFlexibleDatesMatrixResponse searchFlexibleDatesMatrixSolutions(SearchFlightSolutionRequest request);
	SearchFlightSolutionResponse metaSearch(SearchFlightSolutionRequest request);
	SearchFlightSolutionResponse executeFullTextRulesSearch(SearchFlightSolutionRequest request);
	@Deprecated
	BagsAndRuleResponse retrieveBagsAndFareRules(BagsAndRuleRequest request);
}
