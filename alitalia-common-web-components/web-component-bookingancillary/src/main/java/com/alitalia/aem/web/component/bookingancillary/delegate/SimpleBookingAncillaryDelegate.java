package com.alitalia.aem.web.component.bookingancillary.delegate;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.messages.home.InsuranceRequest;
import com.alitalia.aem.common.messages.home.InsuranceResponse;
import com.alitalia.aem.web.component.bookingancillary.behaviour.GetInsuranceBehaviour;

@Service
@Component(immediate=true, metatype=false)
public class SimpleBookingAncillaryDelegate implements BookingAncillaryDelegate {

	@Reference
	private GetInsuranceBehaviour getInsuranceBehaviour;

	@Override
	public InsuranceResponse getInsurance(InsuranceRequest request) {
		BehaviourExecutor<GetInsuranceBehaviour, InsuranceRequest, InsuranceResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(getInsuranceBehaviour, request);
	}

}
