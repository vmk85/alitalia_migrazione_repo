package com.alitalia.aem.web.component.common.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveAirportDetailsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportDetailsResponse;
import com.alitalia.aem.service.api.home.CommonService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveAirportDetailsBehaviour.class)
public class RetrieveAirportDetailsBehaviour extends Behaviour<RetrieveAirportDetailsRequest, RetrieveAirportDetailsResponse> {

	@Reference
	private CommonService commonService;
	
	@Override
	public RetrieveAirportDetailsResponse executeOrchestration(RetrieveAirportDetailsRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Airport Details request is null.");
		return commonService.retrieveAirportDetails(request);
	}
}