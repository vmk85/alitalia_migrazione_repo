package com.alitalia.aem.web.component.common.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.AuthenticateMilleMigliaUserRequest;
import com.alitalia.aem.common.messages.home.AuthenticateMilleMigliaUserResponse;
import com.alitalia.aem.service.api.home.CommonService;

@Component(immediate = true, metatype = false)
@Service(value = AuthenticateMilleMigliaUserBehaviour.class)
public class AuthenticateMilleMigliaUserBehaviour extends Behaviour<AuthenticateMilleMigliaUserRequest, AuthenticateMilleMigliaUserResponse> {

	@Reference
	private CommonService commonService;
	
	@Override
	public AuthenticateMilleMigliaUserResponse executeOrchestration(AuthenticateMilleMigliaUserRequest request) {
		if(request == null) throw new IllegalArgumentException("Authenticate MilleMiglia User request is null.");
		return commonService.authenticateMilleMigliaUser(request);
	}
}