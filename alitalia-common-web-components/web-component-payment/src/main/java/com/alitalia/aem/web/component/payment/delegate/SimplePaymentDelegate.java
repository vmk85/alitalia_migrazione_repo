package com.alitalia.aem.web.component.payment.delegate;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.messages.home.AuthorizePaymentRequest;
import com.alitalia.aem.common.messages.home.AuthorizePaymentResponse;
import com.alitalia.aem.common.messages.home.CheckPaymentRequest;
import com.alitalia.aem.common.messages.home.CheckPaymentResponse;
import com.alitalia.aem.common.messages.home.InitializePaymentRequest;
import com.alitalia.aem.common.messages.home.InitializePaymentResponse;
import com.alitalia.aem.common.messages.home.RetrieveTicketRequest;
import com.alitalia.aem.common.messages.home.RetrieveTicketResponse;
import com.alitalia.aem.web.component.payment.behaviour.AuthorizePaymentBehaviour;
import com.alitalia.aem.web.component.payment.behaviour.CheckPaymentBehaviour;
import com.alitalia.aem.web.component.payment.behaviour.InitializePaymentBehaviour;
import com.alitalia.aem.web.component.payment.behaviour.RetrieveTicketBehaviour;

@Component(immediate = true, metatype = false)
@Service(value = PaymentDelegate.class)
public class SimplePaymentDelegate implements PaymentDelegate {

	
	@Reference
	private InitializePaymentBehaviour initializePaymentBehaviour;
	
	@Reference
	private AuthorizePaymentBehaviour authorizedPaymentBehaviour;
	
	@Reference
	private CheckPaymentBehaviour checkPaymentBehaviour;
	
	@Reference
	private RetrieveTicketBehaviour retrieveTicketBehaviour;
	
	
	@Override
	public InitializePaymentResponse initializePayment(InitializePaymentRequest request) {
		BehaviourExecutor<InitializePaymentBehaviour, InitializePaymentRequest, InitializePaymentResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(initializePaymentBehaviour, request);
	}


	@Override
	public AuthorizePaymentResponse authorizePayment(AuthorizePaymentRequest request) {
		BehaviourExecutor<AuthorizePaymentBehaviour, AuthorizePaymentRequest, AuthorizePaymentResponse> executor = new BehaviourExecutor<AuthorizePaymentBehaviour, AuthorizePaymentRequest, AuthorizePaymentResponse>();
		return executor.executeBehaviour(authorizedPaymentBehaviour, request);
	}


	@Override
	public CheckPaymentResponse checkPayment(CheckPaymentRequest request) {
		BehaviourExecutor<CheckPaymentBehaviour, CheckPaymentRequest, CheckPaymentResponse> executor = new BehaviourExecutor<CheckPaymentBehaviour, CheckPaymentRequest, CheckPaymentResponse>();
		return executor.executeBehaviour(checkPaymentBehaviour, request);
	}


	@Override
	public RetrieveTicketResponse retrieveTicket(RetrieveTicketRequest request) {
		BehaviourExecutor<RetrieveTicketBehaviour, RetrieveTicketRequest, RetrieveTicketResponse> executor = new BehaviourExecutor<RetrieveTicketBehaviour, RetrieveTicketRequest, RetrieveTicketResponse>();	
		return executor.executeBehaviour(retrieveTicketBehaviour, request);
	}
	
	
	
	

	 
	
	
}