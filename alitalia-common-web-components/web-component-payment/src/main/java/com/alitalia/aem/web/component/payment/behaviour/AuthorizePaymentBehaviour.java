package com.alitalia.aem.web.component.payment.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.AuthorizePaymentRequest;
import com.alitalia.aem.common.messages.home.AuthorizePaymentResponse;
import com.alitalia.aem.service.api.home.PaymentService;

@Component(immediate = true, metatype = false)
@Service(value = AuthorizePaymentBehaviour.class)
public class AuthorizePaymentBehaviour extends Behaviour<AuthorizePaymentRequest, AuthorizePaymentResponse> {

	@Reference
	private PaymentService paymentService;
	
	@Override
	public AuthorizePaymentResponse executeOrchestration(AuthorizePaymentRequest request) {
		if(request == null) throw new IllegalArgumentException("AuthorizePayment request is null.");
		return paymentService.authorizePayment(request);
	}
	
	
}