package com.alitalia.aem.web.component.payment.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.InitializePaymentRequest;
import com.alitalia.aem.common.messages.home.InitializePaymentResponse;
import com.alitalia.aem.service.api.home.PaymentService;

@Component(immediate = true, metatype = false)
@Service(value = InitializePaymentBehaviour.class)
public class InitializePaymentBehaviour extends Behaviour<InitializePaymentRequest, InitializePaymentResponse> {

	private static final Logger logger = LoggerFactory.getLogger(InitializePaymentBehaviour.class);

	@Reference
	private PaymentService paymentService;

	@Override
	public InitializePaymentResponse executeOrchestration(InitializePaymentRequest request) {
		if(request == null) throw new IllegalArgumentException("InitializePayment request is null.");

		return paymentService.initializePayment(request);
	}

}