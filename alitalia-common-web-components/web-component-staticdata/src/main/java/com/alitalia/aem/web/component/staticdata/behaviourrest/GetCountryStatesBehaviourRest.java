package com.alitalia.aem.web.component.staticdata.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.CountryStatesRequest;
import com.alitalia.aem.common.messages.home.CountryStatesResponse;
import com.alitalia.aem.service.api.homerest.StaticDataServiceRest;

@Component(immediate = true, metatype = false)
@Service(value = GetCountryStatesBehaviourRest.class)
public class GetCountryStatesBehaviourRest extends Behaviour<CountryStatesRequest, CountryStatesResponse> {

	@Reference
	private StaticDataServiceRest staticDataServiceRest;
	
	@Override
	protected CountryStatesResponse executeOrchestration(CountryStatesRequest request) {
		if(request == null) throw new IllegalArgumentException("Get Country States request is null.");
		return staticDataServiceRest.getCountryStates(request);
	}

}
