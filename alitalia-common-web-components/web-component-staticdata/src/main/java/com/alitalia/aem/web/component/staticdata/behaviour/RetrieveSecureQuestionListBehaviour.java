package com.alitalia.aem.web.component.staticdata.behaviour;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveSecureQuestionListRequest;
import com.alitalia.aem.common.messages.home.RetrieveSecureQuestionListResponse;
import com.alitalia.aem.common.messages.home.RetrieveStateListRequest;
import com.alitalia.aem.common.messages.home.RetrieveStateListResponse;
import com.alitalia.aem.service.api.home.StaticDataMillemigliaService;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;



@Component(immediate = true, metatype = false)
@Service(value = RetrieveSecureQuestionListBehaviour.class)
public class RetrieveSecureQuestionListBehaviour extends Behaviour<RetrieveSecureQuestionListRequest, RetrieveSecureQuestionListResponse> {

	@Reference
	private StaticDataMillemigliaService staticDataMillemigliaService;
	
	@Override
	public RetrieveSecureQuestionListResponse executeOrchestration(RetrieveSecureQuestionListRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Secure Question List request is null.");
		return staticDataMillemigliaService.retrieveSecureQuestionList(request);
	}
}