package com.alitalia.aem.web.component.staticdata.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveAvailabilityRequest;
import com.alitalia.aem.common.messages.home.RetrieveAvailabilityResponse;
import com.alitalia.aem.service.api.homerest.StaticDataServiceRest;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveAvailabilityRTNBehaviourRest.class)
public class RetrieveAvailabilityRTNBehaviourRest extends Behaviour<RetrieveAvailabilityRequest, RetrieveAvailabilityResponse> {

	@Reference
	private StaticDataServiceRest staticDataServiceRest;

	@Override
	protected RetrieveAvailabilityResponse executeOrchestration(RetrieveAvailabilityRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Availability rtn request is null.");
		return staticDataServiceRest.getAvailabilityRTN(request);
	}

}
