package com.alitalia.aem.web.component.staticdata.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveMealsRequest;
import com.alitalia.aem.common.messages.home.RetrieveMealsResponse;
import com.alitalia.aem.service.api.home.StaticDataService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveMealsBehaviour.class)
public class RetrieveMealsBehaviour extends Behaviour<RetrieveMealsRequest, RetrieveMealsResponse> {

	@Reference
	private StaticDataService staticDataService;
	
	@Override
	public RetrieveMealsResponse executeOrchestration(RetrieveMealsRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Meals request is null.");
		return staticDataService.retrieveMeals(request);
	}
}