package com.alitalia.aem.web.component.staticdata.delegaterest;

import com.alitalia.aem.common.messages.home.CountriesRequest;
import com.alitalia.aem.common.messages.home.CountriesResponse;
import com.alitalia.aem.common.messages.home.CountryStatesRequest;
import com.alitalia.aem.common.messages.home.CountryStatesResponse;
import com.alitalia.aem.common.messages.home.GetDestinationsResponse;
import com.alitalia.aem.common.messages.home.GetFrequentFlyerRequest;
import com.alitalia.aem.common.messages.home.GetFrequentFlyerResponse;
import com.alitalia.aem.common.messages.home.RetrieveAirportDestinationRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.common.messages.home.RetrieveAvailabilityRequest;
import com.alitalia.aem.common.messages.home.RetrieveAvailabilityResponse;

public interface StaticDataDelegateRest {
	
	RetrieveAirportsResponse getAllAirports(RetrieveAirportsRequest request);
	GetDestinationsResponse getAirportDestination(RetrieveAirportDestinationRequest destinationRequest);
	RetrieveAvailabilityResponse getAvailabilty(RetrieveAvailabilityRequest request);
	RetrieveAvailabilityResponse getAvailabiltyRTN(RetrieveAvailabilityRequest request);
	GetFrequentFlyerResponse getFrequentFlyer(GetFrequentFlyerRequest request);
	CountryStatesResponse getCountryStates(CountryStatesRequest request);
	CountriesResponse getCountries(CountriesRequest request);
}
