package com.alitalia.aem.web.component.staticdata.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.service.api.homerest.StaticDataServiceRest;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveAirportsBehaviourRest.class)
public class RetrieveAirportsBehaviourRest  extends Behaviour<RetrieveAirportsRequest, RetrieveAirportsResponse> {
	
	@Reference
	private StaticDataServiceRest staticDataServiceRest;

	@Override
	protected RetrieveAirportsResponse executeOrchestration(RetrieveAirportsRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Airports request is null.");
		return staticDataServiceRest.getAllAirports(request);
	}

}
