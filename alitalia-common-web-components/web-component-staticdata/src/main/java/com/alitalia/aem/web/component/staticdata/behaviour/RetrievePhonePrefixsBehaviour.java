package com.alitalia.aem.web.component.staticdata.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixRequest;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixResponse;
import com.alitalia.aem.service.api.home.StaticDataService;

@Component(immediate = true, metatype = false)
@Service(value = RetrievePhonePrefixsBehaviour.class)
public class RetrievePhonePrefixsBehaviour extends Behaviour<RetrievePhonePrefixRequest, RetrievePhonePrefixResponse> {

	@Reference
	private StaticDataService staticDataService;
	
	@Override
	public RetrievePhonePrefixResponse executeOrchestration(RetrievePhonePrefixRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Phone prefixs request is null.");
		return staticDataService.retrievePhonePrefix(request);
	}
}