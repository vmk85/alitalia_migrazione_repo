package com.alitalia.aem.web.component.staticdata.delegaterest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.messages.home.CountriesRequest;
import com.alitalia.aem.common.messages.home.CountriesResponse;
import com.alitalia.aem.common.messages.home.CountryStatesRequest;
import com.alitalia.aem.common.messages.home.CountryStatesResponse;
import com.alitalia.aem.common.messages.home.GetDestinationsResponse;
import com.alitalia.aem.common.messages.home.GetFrequentFlyerRequest;
import com.alitalia.aem.common.messages.home.GetFrequentFlyerResponse;
import com.alitalia.aem.common.messages.home.RetrieveAirportDestinationRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.common.messages.home.RetrieveAvailabilityRequest;
import com.alitalia.aem.common.messages.home.RetrieveAvailabilityResponse;
import com.alitalia.aem.web.component.staticdata.behaviourrest.GetCountriesBehaviourRest;
import com.alitalia.aem.web.component.staticdata.behaviourrest.GetCountryStatesBehaviourRest;
import com.alitalia.aem.web.component.staticdata.behaviourrest.GetFrequentFlyerRestBehaviour;
import com.alitalia.aem.web.component.staticdata.behaviourrest.RetrieveAirportDestinationBehaviourRest;
import com.alitalia.aem.web.component.staticdata.behaviourrest.RetrieveAirportsBehaviourRest;
import com.alitalia.aem.web.component.staticdata.behaviourrest.RetrieveAvailabilityBehaviourRest;
import com.alitalia.aem.web.component.staticdata.behaviourrest.RetrieveAvailabilityRTNBehaviourRest;

@Component(immediate = true, metatype = false)
@Properties({
	@Property(name = SimpleStaticDataDelegateRest.CACHE_REFRESH_INTERVAL, label = "Refresh interval of the cache."),
	@Property(name = SimpleStaticDataDelegateRest.IS_CACHE_ENABLED, label = "Flag to enable cache."),
	@Property(name = SimpleStaticDataDelegateRest.OED_CACHE_REFRESH_INTERVAL, label = "Refresh interval of oed cache."),
	@Property(name = SimpleStaticDataDelegateRest.IS_OED_CACHE_ENABLED, label = "Flag to enable oed cache."),
	@Property(name = SimpleStaticDataDelegateRest.OED_REFRESH_HOUR, label = "Hour to create timestamp for oed refresh"),
	@Property(name = SimpleStaticDataDelegateRest.OED_REFRESH_MINUTES, label = "Minutes to create timestamp for oed refresh"),
	@Property(name = SimpleStaticDataDelegateRest.OED_REFRESH_SECONDS, label = "Seconds to create timestamp for oed refresh"),
	@Property(name = SimpleStaticDataDelegateRest.DESTINATION_CACHE_REFRESH_INTERVAL, label = "Refresh interval of destination cache."),
	@Property(name = SimpleStaticDataDelegateRest.IS_DESTINATION_CACHE_ENABLED, label = "Flag to enable destination cache."),
	@Property(name = SimpleStaticDataDelegateRest.DESTINATION_REFRESH_PARAM, label = "Parameter to check for destination refresh")
})
@Service(value = StaticDataDelegateRest.class)
public class SimpleStaticDataDelegateRest implements StaticDataDelegateRest{

	private static final Logger logger = LoggerFactory.getLogger(SimpleStaticDataDelegateRest.class);

	@Reference
	private RetrieveAirportsBehaviourRest retrieveAirportsBehaviourRest;

	@Reference 
	private RetrieveAirportDestinationBehaviourRest retrieveAirportDestinationBehaviourRest;

	@Reference
	private RetrieveAvailabilityBehaviourRest retrieveAvailabilityBehaviourRest;

    @Reference
    private RetrieveAvailabilityRTNBehaviourRest retrieveAvailabilityRTNBehaviourRest;

	@Reference
	private GetFrequentFlyerRestBehaviour getFrequentFlyerRestBehaviour;

	@Reference
	private GetCountryStatesBehaviourRest getCountryStatesBehaviourRest;

	@Reference
	private GetCountriesBehaviourRest getCountriesBehaviourRest;

	private long refreshInterval = 3600000L;
	private long oedRefreshInterval = 86400000L;
	private long destinationRefreshInterval = 28800000L;
	private boolean isCacheEnabled = false;
	private boolean isOedCacheEnabled = false;
	private boolean isDestinationCacheEnabled = false;

	private RetrieveAirportsResponse retrieveAirportsResponse;

	private ConcurrentHashMap<String, Object> cacheMapDestinationAirport = new ConcurrentHashMap<String, Object>();
	private ConcurrentHashMap<String, Object> cacheMapAvailabilityAirport = new ConcurrentHashMap<String, Object>();
    private ConcurrentHashMap<String, Object> cacheMapAvailabilityRTNAirport = new ConcurrentHashMap<String, Object>();
	
	private Object retrieveAirportsResponseLock = new Object();

	private long lastCleanAirports = System.currentTimeMillis();
	private long lastCleanAvailability = System.currentTimeMillis();
	private long lastCleanDestination = System.currentTimeMillis();

	public static final String CACHE_REFRESH_INTERVAL = "cache.refresh.interval";
	public static final String IS_CACHE_ENABLED = "cache.is_enabled";
	public static final String OED_CACHE_REFRESH_INTERVAL = "oed.cache.refresh.interval";
	public static final String IS_OED_CACHE_ENABLED = "oed.cache.is_enabled";
	public static final String DESTINATION_CACHE_REFRESH_INTERVAL = "destination.cache.refresh.interval";
	public static final String IS_DESTINATION_CACHE_ENABLED = "destination.cache.is_enabled";
	
	public static final String OED_REFRESH_HOUR = "oed.refresh.h";
	public static final String OED_REFRESH_MINUTES = "oed.refresh.m";
	public static final String OED_REFRESH_SECONDS = "oed.refresh.s";
	
	public static final String DESTINATION_REFRESH_PARAM = "destination.refresh.param";

	private int OedRefreshH = 0;
	private int OedRefreshM = 0;
	private int OedRefreshS = 0;
	
	private int DestinationRefreshParam = 0;
	
	private boolean OedImmediateRefresh = false;
	private boolean DestinationImmediateRefresh = false;
	
	@Override
	public RetrieveAirportsResponse getAllAirports(RetrieveAirportsRequest request) {
		BehaviourExecutor<RetrieveAirportsBehaviourRest, RetrieveAirportsRequest, RetrieveAirportsResponse> executor = new BehaviourExecutor<RetrieveAirportsBehaviourRest, RetrieveAirportsRequest, RetrieveAirportsResponse>();

		logger.debug("Executing delegate getAllAirports");

		synchronized(retrieveAirportsResponseLock) {

			if (isCacheEnabled) {
				if (isCacheToBeRefreshed(lastCleanAirports)) {
					retrieveAirportsResponse = null;
					lastCleanAirports = System.currentTimeMillis();
				}

				if (retrieveAirportsResponse == null ||retrieveAirportsResponse.getAirports() == null || retrieveAirportsResponse.getAirports().size()==0) {
					retrieveAirportsResponse = executor.executeBehaviour(retrieveAirportsBehaviourRest, request);
				}
				else {
					logger.info("retrieveAllAirports: data retrieved from cache");
				}

				return retrieveAirportsResponse;
			}
			else {
				logger.debug("Cache manager disabled - starting service call to get airports list");
				logger.info("Chiamando BehaviourRest...");
				return executor.executeBehaviour(retrieveAirportsBehaviourRest, request);
			}
		}
	}


	@Override
	public GetDestinationsResponse getAirportDestination(RetrieveAirportDestinationRequest destinationRequest) {
		BehaviourExecutor<RetrieveAirportDestinationBehaviourRest, RetrieveAirportDestinationRequest, GetDestinationsResponse> executor = new BehaviourExecutor<RetrieveAirportDestinationBehaviourRest, RetrieveAirportDestinationRequest, GetDestinationsResponse>();

		logger.debug("Executing delegate getAirportDestination");

		if (isDestinationCacheEnabled) {
			if (isDestinationCacheToBeRefreshed(lastCleanDestination)) {
				cacheMapDestinationAirport.clear();
        		lastCleanDestination = System.currentTimeMillis();
        	}
			
			String key = destinationRequest.getIata();
			if (key != null && !"".equals(key)) {

				GetDestinationsResponse getDestinationsResponse = (GetDestinationsResponse) cacheMapDestinationAirport.get(key);

				if (getDestinationsResponse == null) {
					logger.debug("Called cache manager to retrieve airport destinations - cache missing");
					getDestinationsResponse = (GetDestinationsResponse) cacheMapDestinationAirport.get(key);
					if (getDestinationsResponse == null) {
						getDestinationsResponse = executor.executeBehaviour(retrieveAirportDestinationBehaviourRest, destinationRequest);
						if (getDestinationsResponse != null && getDestinationsResponse.getDestinations() != null && getDestinationsResponse.getDestinations().size() > 0) {
							cacheMapDestinationAirport.put(key, getDestinationsResponse);
						}
						logger.debug("Called cache manager to retrieve airport destination - put destination list in cache");
					}
					else {
						logger.debug("Called cache manager to retrieve airport destination - put destination list in cache from another thread");
					}
					return getDestinationsResponse;
				}
				logger.info("called cache manager to retrieve airport destination - returned destination list from cache");
				return getDestinationsResponse;
			}
			else {
				logger.debug("Bad key found - starting service call to get destination list");
				return executor.executeBehaviour(retrieveAirportDestinationBehaviourRest, destinationRequest);
			}
		}
		else {
			logger.debug("Cache manager disabled - starting service call to get destination list");
			return executor.executeBehaviour(retrieveAirportDestinationBehaviourRest, destinationRequest);
		}
	}

	@Override
	public GetFrequentFlyerResponse getFrequentFlyer(GetFrequentFlyerRequest request) {
		GetFrequentFlyerResponse response = null;
		BehaviourExecutor<GetFrequentFlyerRestBehaviour, GetFrequentFlyerRequest, GetFrequentFlyerResponse> executor = new BehaviourExecutor<GetFrequentFlyerRestBehaviour, GetFrequentFlyerRequest, GetFrequentFlyerResponse>();
		try {
			logger.debug("Executing delegate getFrequentFlyers");
			response = executor.executeBehaviour(getFrequentFlyerRestBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[CheckinDelegateImpl - getFrequentFlyers] - Errore durante il recupero delle informazioni sui frequent flyers.", e);
		}
		return response;
	}

	@Override
	public CountryStatesResponse getCountryStates(CountryStatesRequest request) {
		BehaviourExecutor<GetCountryStatesBehaviourRest, CountryStatesRequest, CountryStatesResponse> executor = new BehaviourExecutor<GetCountryStatesBehaviourRest, CountryStatesRequest, CountryStatesResponse>();

		logger.info("Chiamando GetCountryStatesBehaviourRest...");
		return executor.executeBehaviour(getCountryStatesBehaviourRest, request);
	}


	@Override
	public CountriesResponse getCountries(CountriesRequest request) {
		BehaviourExecutor<GetCountriesBehaviourRest, CountriesRequest, CountriesResponse> executor = new BehaviourExecutor<GetCountriesBehaviourRest, CountriesRequest, CountriesResponse>();

		logger.info("Chiamando GetCountriesBehaviourRest...");
		return executor.executeBehaviour(getCountriesBehaviourRest, request);
	}


	//		synchronized(retrieveAirportDestinationResponseLock) {
	//			
	//			if (isCacheEnabled) {
	//				if (isCacheToBeRefreshed(lastCleanAirports)) {
	//					retrieveAirportsResponse = null;
	//					lastCleanAirports = System.currentTimeMillis();
	//				}
	//			
	//				if (getDestinationsResponse == null ||getDestinationsResponse.getDestinations() == null || getDestinationsResponse.getDestinations().size()==0) {
	//					getDestinationsResponse = executor.executeBehaviour(retrieveAirportDestinationBehaviourRest, destinationRequest);
	//				}
	//				else {
	//					logger.info("getDestinationsResponse: data retrieved from cache");
	//				}
	//				
	//			return getDestinationsResponse;
	//			}
	//			else {
	//				logger.debug("Cache manager disabled - starting service call to get airports list");
	//				logger.info("Chiamando BehaviourRest...");
	//					return executor.executeBehaviour(retrieveAirportDestinationBehaviourRest, destinationRequest);
	//			}
	//		}

	@Override
	public RetrieveAvailabilityResponse getAvailabilty(RetrieveAvailabilityRequest availabilityRequest) {
		BehaviourExecutor<RetrieveAvailabilityBehaviourRest, RetrieveAvailabilityRequest, RetrieveAvailabilityResponse> executor = new BehaviourExecutor<RetrieveAvailabilityBehaviourRest, RetrieveAvailabilityRequest, RetrieveAvailabilityResponse>();

		logger.debug("Executing delegate getAvailability");

		if (isOedCacheEnabled) {
			if (isOedCacheToBeRefreshed(lastCleanAvailability)) {
				cacheMapAvailabilityAirport.clear();
        		LocalDateTime lastCleanLDT= LocalDate.now().atTime(OedRefreshH, OedRefreshM, OedRefreshS);
        		lastCleanAvailability = PropertiesUtil.toLong(lastCleanLDT, System.currentTimeMillis());
        	}
			
			String keyO = availabilityRequest.getOriginIATA();
			String keyD = availabilityRequest.getDestinationIATA();
			if (keyO != null && !"".equals(keyO) && keyD != null && !"".equals(keyD)) {

				String key = keyO.concat(keyD);
				RetrieveAvailabilityResponse retrieveAvailabilityResponse = (RetrieveAvailabilityResponse) cacheMapAvailabilityAirport.get(key);

				if (retrieveAvailabilityResponse == null) {
					logger.debug("Called cache manager to retrieve airport availability - cache missing");
					retrieveAvailabilityResponse = (RetrieveAvailabilityResponse) cacheMapAvailabilityAirport.get(key);
					if (retrieveAvailabilityResponse == null) {
						retrieveAvailabilityResponse = executor.executeBehaviour(retrieveAvailabilityBehaviourRest, availabilityRequest);
						if (retrieveAvailabilityResponse != null && retrieveAvailabilityResponse.getAvailability() != null) {
							cacheMapAvailabilityAirport.put(key, retrieveAvailabilityResponse);
						}
						logger.debug("Called cache manager to retrieve airport availability - put availability list in cache");
					}
					else {
						logger.debug("Called cache manager to retrieve airport availability - put availability list in cache from another thread");
					}
					return retrieveAvailabilityResponse;
				}
				logger.info("called cache manager to retrieve airport availability - returned availability list from cache");
				return retrieveAvailabilityResponse;
			}
			else {
				logger.debug("Bad key found - starting service call to get availability list");
				return executor.executeBehaviour(retrieveAvailabilityBehaviourRest, availabilityRequest);
			}
		}
		else {
			logger.debug("Cache manager disabled - starting service call to get availability");
			logger.info("Chiamando BehaviourRest...");
			return executor.executeBehaviour(retrieveAvailabilityBehaviourRest, availabilityRequest);
		}
	}

    @Override
    public RetrieveAvailabilityResponse getAvailabiltyRTN(RetrieveAvailabilityRequest availabilityRTNRequest) {
        BehaviourExecutor<RetrieveAvailabilityRTNBehaviourRest, RetrieveAvailabilityRequest, RetrieveAvailabilityResponse> executor = new BehaviourExecutor<RetrieveAvailabilityRTNBehaviourRest, RetrieveAvailabilityRequest, RetrieveAvailabilityResponse>();

        logger.debug("Executing delegate getAvailabilityRTN");

        if (isOedCacheEnabled) {
        	if (isOedCacheToBeRefreshed(lastCleanAvailability)) {
        		cacheMapAvailabilityRTNAirport.clear();
        		LocalDateTime lastCleanLDT= LocalDate.now().atTime(OedRefreshH, OedRefreshM, OedRefreshS);
        		lastCleanAvailability = PropertiesUtil.toLong(lastCleanLDT, System.currentTimeMillis());
        	}
        	
            String keyO = availabilityRTNRequest.getOriginIATA();
            String keyD = availabilityRTNRequest.getDestinationIATA();
            if (keyO != null && !"".equals(keyO) && keyD != null && !"".equals(keyD)) {

                String key = keyO.concat(keyD);
                RetrieveAvailabilityResponse retrieveAvailabilityRTNResponse = (RetrieveAvailabilityResponse) cacheMapAvailabilityRTNAirport.get(key);

                if (retrieveAvailabilityRTNResponse == null) {
                    logger.debug("Called cache manager to retrieve airport availabilityRTN - cache missing");
                    retrieveAvailabilityRTNResponse = (RetrieveAvailabilityResponse) cacheMapAvailabilityRTNAirport.get(key);
                    if (retrieveAvailabilityRTNResponse == null) {
                        retrieveAvailabilityRTNResponse = executor.executeBehaviour(retrieveAvailabilityRTNBehaviourRest, availabilityRTNRequest);
                        if (retrieveAvailabilityRTNResponse != null && retrieveAvailabilityRTNResponse.getAvailability() != null && retrieveAvailabilityRTNResponse.getAvailability_ret() != null) {
                            cacheMapAvailabilityRTNAirport.put(key, retrieveAvailabilityRTNResponse);
                        }
                        logger.debug("Called cache manager to retrieve airport availabilityRTN - put availability list in cache");
                    }
                    else {
                        logger.debug("Called cache manager to retrieve airport availabilityRTN - put availability list in cache from another thread");
                    }
                    return retrieveAvailabilityRTNResponse;
                }
                logger.info("called cache manager to retrieve airport availabilityRTN - returned availability list from cache");
                return retrieveAvailabilityRTNResponse;
            }
            else {
                logger.debug("Bad key found - starting service call to get availabilityRTN list");
                return executor.executeBehaviour(retrieveAvailabilityRTNBehaviourRest, availabilityRTNRequest);
            }
        }
        else {
            logger.debug("Cache manager disabled - starting service call to get availabilityRTN");
            logger.info("Chiamando BehaviourRest...");
            return executor.executeBehaviour(retrieveAvailabilityRTNBehaviourRest, availabilityRTNRequest);
        }

    }

	private boolean isCacheToBeRefreshed(Long lastClean) {
		long currentTime = System.currentTimeMillis();
		logger.debug("SimpleStaticDataDelegate.checkIsCacheToBeRefreshed - currentTime is [" + currentTime + "], lastClean is [" + lastClean + "] and refreshInterval is [" + refreshInterval + "]");
		if (currentTime - lastClean > refreshInterval) {
			logger.debug("SimpleStaticDataDelegate.checkIsCacheToBeRefreshed - clear cache");
			//			lastClean = currentTime;
			return true;
		}
		return false;
	}
	
	private boolean isOedCacheToBeRefreshed(Long lastOedClean) {
		try {
			LocalDateTime oedRefresh = LocalDate.now().atTime(OedRefreshH, OedRefreshM, OedRefreshS);
			ZonedDateTime oedWTimeZone = oedRefresh.atZone(ZoneId.of("UTC+1"));
			long currentTimeForOed = oedWTimeZone.toInstant().getEpochSecond();
			logger.debug("SimpleStaticDataDelegate.checkIsOedCacheToBeRefreshed - currentTimeForOed is [" + currentTimeForOed + "], lastOedClean is [" + lastOedClean + "] and oedRefreshInterval is [" + oedRefreshInterval + "]");
			if (OedImmediateRefresh) {
				OedImmediateRefresh = false;
				return true;
			}
			else if (currentTimeForOed - lastOedClean > oedRefreshInterval) {
				logger.debug("SimpleStaticDataDelegate.checkIsOedCacheToBeRefreshed - clear cache");
				return true;
			}
		}
		catch (Exception e) {
			logger.error("Errore in metodo isOedCacheToBeRefreshed, l'eccezione è " + e);
		}
		return false;
	}
	
	private boolean isDestinationCacheToBeRefreshed(Long lastDestinationClean) {
		try {
			long currentTimeForDestination = System.currentTimeMillis();
			logger.debug("SimpleStaticDataDelegate.checkIsDestinationCacheToBeRefreshed - currentTimeForDestination is [" + currentTimeForDestination + "], lastDestinationClean is [" + lastDestinationClean + "] and destinationRefreshInterval is [" + destinationRefreshInterval + "]");
			if (DestinationImmediateRefresh) {
				DestinationImmediateRefresh = false;
				return true;
			}
			else if (currentTimeForDestination - lastDestinationClean > destinationRefreshInterval) {
				logger.debug("SimpleStaticDataDelegate.checkIsDestinationCacheToBeRefreshed - clear cache");
				return true;
			}
		}
		catch (Exception e) {
			logger.error("Errore in metodo isDestinationCacheToBeRefreshed, l'eccezione è " + e);
		}
		return false;
	}

	@Activate
	private void activate(ComponentContext componentContext) {
		try {					
			isCacheEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_CACHE_ENABLED), false);
			refreshInterval = PropertiesUtil.toLong(
					componentContext.getProperties().get(CACHE_REFRESH_INTERVAL), 3600000);
			isOedCacheEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_OED_CACHE_ENABLED), false);
			oedRefreshInterval = PropertiesUtil.toLong(
					componentContext.getProperties().get(OED_CACHE_REFRESH_INTERVAL), 86400000);
			isDestinationCacheEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_DESTINATION_CACHE_ENABLED), false);
			destinationRefreshInterval = PropertiesUtil.toLong(
					componentContext.getProperties().get(DESTINATION_CACHE_REFRESH_INTERVAL), 28800000);
			
			OedRefreshH = PropertiesUtil.toInteger(componentContext.getProperties().get(OED_REFRESH_HOUR), 3);
			OedRefreshM = PropertiesUtil.toInteger(componentContext.getProperties().get(OED_REFRESH_MINUTES), 0);
			OedRefreshS = PropertiesUtil.toInteger(componentContext.getProperties().get(OED_REFRESH_SECONDS), 0);
			DestinationRefreshParam = PropertiesUtil.toInteger(componentContext.getProperties().get(DESTINATION_REFRESH_PARAM), 0);
			
		} catch (Exception e) {
			logger.error("Error to modify SimpleStaticDataDelegate - Load default value", e);
			isCacheEnabled = false;	
			isOedCacheEnabled = false;
			isDestinationCacheEnabled = false;
		}

		logger.info("Activated SimpleStaticDataDelegate: "
				+ "IS_CACHE_ENABLED=[" + isCacheEnabled + "], "
				+ "CACHE_REFRESH_INTERVAL=[" + refreshInterval + "], "
				+ "IS_OED_CACHE_ENABLED=[" + isOedCacheEnabled + "], "
				+ "OED_CACHE_REFRESH_INTERVAL=[" + oedRefreshInterval + "], "
				+ "IS_DESTINATION_CACHE_ENABLED=[" + isDestinationCacheEnabled + "], "
				+ "DESTINATION_CACHE_REFRESH_INTERVAL=[" + destinationRefreshInterval + "], "
				+ "DestinationRefreshParam = [" + DestinationRefreshParam + "], "
				+ "OedRefreshH = [" + OedRefreshH + "], "
				+ "OedRefreshM = [" + OedRefreshM + "], "
				+ "OedRefreshS = [" + OedRefreshS + "]");
	}

	@Modified
	private void modified(ComponentContext componentContext) {
		//per controllare se i valori di ora, minuti e secondi sono stati modificati nelle configurazioni
		int checkH, checkM, checkS; 
		//per controllare se il valore del parametro di refresh per la cache destination è stato modificato in configurazione
		int destinationParam;
		
		try {
			isCacheEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_CACHE_ENABLED), false);
			refreshInterval = PropertiesUtil.toLong(
					componentContext.getProperties().get(CACHE_REFRESH_INTERVAL), 3600000);
			isOedCacheEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_OED_CACHE_ENABLED), false);
			oedRefreshInterval = PropertiesUtil.toLong(
					componentContext.getProperties().get(OED_CACHE_REFRESH_INTERVAL), 86400000);
			isDestinationCacheEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_DESTINATION_CACHE_ENABLED), false);
			destinationRefreshInterval = PropertiesUtil.toLong(
					componentContext.getProperties().get(DESTINATION_CACHE_REFRESH_INTERVAL), 28800000);
			
			destinationParam = PropertiesUtil.toInteger(componentContext.getProperties().get(DESTINATION_REFRESH_PARAM), 0);
			if (DestinationRefreshParam != destinationParam) {
				DestinationRefreshParam = destinationParam;
				DestinationImmediateRefresh = true;
			}
			
			checkH = PropertiesUtil.toInteger(componentContext.getProperties().get(OED_REFRESH_HOUR), 3);
			if (checkH != OedRefreshH) {
				OedRefreshH = checkH;
				OedImmediateRefresh = true;
			}
			
			checkM = PropertiesUtil.toInteger(componentContext.getProperties().get(OED_REFRESH_MINUTES), 0);
			if (checkM != OedRefreshM) {
				OedRefreshM = checkM;
				OedImmediateRefresh = true;
			}
			
			checkS = PropertiesUtil.toInteger(componentContext.getProperties().get(OED_REFRESH_SECONDS), 0);
			if (checkS != OedRefreshS) {
				OedRefreshS = checkS;
				OedImmediateRefresh = true;
			}
			
		} catch (Exception e) {
			logger.error("Error to modify SimpleStaticDataDelegate - Load default value", e);
			isCacheEnabled = false;
			isOedCacheEnabled = false;
			isDestinationCacheEnabled = false;
			OedImmediateRefresh = false;
			DestinationImmediateRefresh = false;
		}

		logger.info("Modified SimpleStaticDataDelegate: "
				+ "IS_CACHE_ENABLED=[" + isCacheEnabled + "], "
				+ "CACHE_REFRESH_INTERVAL=[" + refreshInterval + "], "
				+ "IS_OED_CACHE_ENABLED=[" + isOedCacheEnabled + "], "
				+ "OED_CACHE_REFRESH_INTERVAL=[" + oedRefreshInterval + "], "
				+ "IS_DESTINATION_CACHE_ENABLED=[" + isDestinationCacheEnabled + "], "
				+ "DESTINATION_CACHE_REFRESH_INTERVAL=[" + destinationRefreshInterval + "], "
				+ "DestinationRefreshParam = [" + DestinationRefreshParam + "], "
				+ "OedRefreshH = [" + OedRefreshH + "], "
				+ "OedRefreshM = [" + OedRefreshM + "], "
				+ "OedRefreshS = [" + OedRefreshS + "]");
	}


}
