package com.alitalia.aem.web.component.staticdata.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemRequest;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemResponse;
import com.alitalia.aem.service.api.home.StaticDataService;

@Component(immediate = true, metatype = false)
@Service(value = RetrievePaymentTypeItemsBehaviour.class)
public class RetrievePaymentTypeItemsBehaviour extends Behaviour<RetrievePaymentTypeItemRequest, RetrievePaymentTypeItemResponse> {

	@Reference
	private StaticDataService staticDataService;
	
	@Override
	public RetrievePaymentTypeItemResponse executeOrchestration(RetrievePaymentTypeItemRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Payment Type Items request is null.");
		return staticDataService.retrievePaymentTypeItem(request);
	}
}