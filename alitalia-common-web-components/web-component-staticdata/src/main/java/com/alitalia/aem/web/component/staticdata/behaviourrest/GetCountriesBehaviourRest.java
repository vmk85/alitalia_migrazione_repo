package com.alitalia.aem.web.component.staticdata.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.CountriesRequest;
import com.alitalia.aem.common.messages.home.CountriesResponse;
import com.alitalia.aem.service.api.homerest.StaticDataServiceRest;

@Component(immediate = true, metatype = false)
@Service(value = GetCountriesBehaviourRest.class)
public class GetCountriesBehaviourRest extends Behaviour<CountriesRequest, CountriesResponse> {

	@Reference
	private StaticDataServiceRest staticDataServiceRest;

	@Override
	protected CountriesResponse executeOrchestration(CountriesRequest request) {
		if(request == null) throw new IllegalArgumentException("Get Countries request is null.");
		return staticDataServiceRest.getCountries(request);
	}
	
}
