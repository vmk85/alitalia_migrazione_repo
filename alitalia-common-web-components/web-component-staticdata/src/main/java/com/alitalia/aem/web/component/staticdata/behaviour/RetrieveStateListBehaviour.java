package com.alitalia.aem.web.component.staticdata.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveStateListRequest;
import com.alitalia.aem.common.messages.home.RetrieveStateListResponse;
import com.alitalia.aem.service.api.home.StaticDataMillemigliaService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveStateListBehaviour.class)
public class RetrieveStateListBehaviour extends Behaviour<RetrieveStateListRequest, RetrieveStateListResponse> {

	@Reference
	private StaticDataMillemigliaService staticDataMillemigliaService;
	
	@Override
	public RetrieveStateListResponse executeOrchestration(RetrieveStateListRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve State List request is null.");
		return staticDataMillemigliaService.retrieveStatesList(request);
	}
}