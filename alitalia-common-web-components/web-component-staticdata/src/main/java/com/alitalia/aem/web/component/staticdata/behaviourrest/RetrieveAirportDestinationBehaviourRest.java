package com.alitalia.aem.web.component.staticdata.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.GetDestinationsResponse;
import com.alitalia.aem.common.messages.home.RetrieveAirportDestinationRequest;
import com.alitalia.aem.service.api.homerest.StaticDataServiceRest;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveAirportDestinationBehaviourRest.class)
public class RetrieveAirportDestinationBehaviourRest extends Behaviour<RetrieveAirportDestinationRequest, GetDestinationsResponse> {

	@Reference
	private StaticDataServiceRest staticDataServiceRest;
	
	@Override
	protected GetDestinationsResponse executeOrchestration(RetrieveAirportDestinationRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Airport Destination request is null.");
		return staticDataServiceRest.getAirportDestination(request);
	}

}
