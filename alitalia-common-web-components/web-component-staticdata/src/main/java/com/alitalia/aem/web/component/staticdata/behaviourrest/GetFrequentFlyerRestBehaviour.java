package com.alitalia.aem.web.component.staticdata.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.GetFrequentFlyerRequest;
import com.alitalia.aem.common.messages.home.GetFrequentFlyerResponse;
import com.alitalia.aem.service.api.homerest.StaticDataServiceRest;

@Component(immediate = true, metatype = false)
@Service(value = GetFrequentFlyerRestBehaviour.class)
public class GetFrequentFlyerRestBehaviour extends Behaviour<GetFrequentFlyerRequest, GetFrequentFlyerResponse>{

	@Reference
	private StaticDataServiceRest staticDataServiceRest;
	
	@Override
	protected GetFrequentFlyerResponse executeOrchestration(GetFrequentFlyerRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Airport Destination request is null.");
		return staticDataServiceRest.getFrequentFlyers(request);
	}

}
