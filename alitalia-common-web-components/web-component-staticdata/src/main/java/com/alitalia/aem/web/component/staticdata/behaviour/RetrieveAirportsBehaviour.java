package com.alitalia.aem.web.component.staticdata.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.service.api.home.StaticDataService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveAirportsBehaviour.class)
public class RetrieveAirportsBehaviour extends Behaviour<RetrieveAirportsRequest, RetrieveAirportsResponse> {

	@Reference
	private StaticDataService staticDataService;
	
	@Override
	public RetrieveAirportsResponse executeOrchestration(RetrieveAirportsRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Airports request is null.");
		return staticDataService.retrieveAirports(request);
	}
}