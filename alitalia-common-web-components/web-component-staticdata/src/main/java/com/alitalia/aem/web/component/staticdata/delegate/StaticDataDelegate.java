package com.alitalia.aem.web.component.staticdata.delegate;

import com.alitalia.aem.common.messages.home.*;

public interface StaticDataDelegate {

	RetrieveCountriesResponse retrieveCountries(RetrieveCountriesRequest request);
	RetrieveCreditCardCountriesResponse retrieveCreditCardCountries(RetrieveCreditCardCountriesRequest request);
	RetrieveFrequentFlyersResponse retrieveFrequentFlyersType(RetrieveFrequentFlayerRequest request);
	RetrieveMealsResponse retrieveMeals(RetrieveMealsRequest request);
	RetrievePaymentTypeItemResponse retrievePaymentTypeItems(RetrievePaymentTypeItemRequest request);
	RetrievePhonePrefixResponse retrievePhonePrefixs(RetrievePhonePrefixRequest request);
	RetrieveSeatTypeResponse retrievesSeatTypes(RetrieveSeatTypeRequest request);
	RetrieveTicketOfficeResponse retrieveTicketsOffice(RetrieveTicketOfficeRequest request);
	RetrieveAirportsResponse retrieveAirports(RetrieveAirportsRequest request);
	RetrieveCouncilsResponse retrieveCouncils(RetrieveCouncilsRequest request);
	RetrieveProvincesResponse retrieveProvinces(RetrieveProvincesRequest request);
	RetrieveStateListResponse retrieveStateList(RetrieveStateListRequest request);

	RetrieveSecureQuestionListResponse retrieveSecureQuestionList(RetrieveSecureQuestionListRequest request);

}