package com.alitalia.aem.web.component.statistics.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RegisterStatisticsRequest;
import com.alitalia.aem.common.messages.home.RegisterStatisticsResponse;
import com.alitalia.aem.service.api.home.RegisterStatisticsService;

@Component(immediate = true, metatype = false)
@Service(value = RegisterStatisticsBehaviour.class)
public class RegisterStatisticsBehaviour extends Behaviour<RegisterStatisticsRequest, RegisterStatisticsResponse> {

	@Reference
	private RegisterStatisticsService registerStatisticsService;
	
	@Override
	public RegisterStatisticsResponse executeOrchestration(RegisterStatisticsRequest request) {
		if(request == null) throw new IllegalArgumentException("Register Statistics request is null.");
		return registerStatisticsService.registerStatistics(request);
	}	
}