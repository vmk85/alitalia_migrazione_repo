package com.alitalia.aem.web.component.widget.delegate;

import com.alitalia.aem.common.data.home.MMCreditCardData;
import com.alitalia.aem.common.messages.home.LoadWidgetCashMilesRequest;
import com.alitalia.aem.common.messages.home.LoadWidgetCashMilesResponse;
import com.alitalia.aem.common.messages.home.LoadWidgetECouponRequest;
import com.alitalia.aem.common.messages.home.LoadWidgetECouponResponse;
import com.alitalia.aem.common.messages.home.LoadWidgetOneClickRequest;
import com.alitalia.aem.common.messages.home.UpdateWidgetCashMilesRequest;
import com.alitalia.aem.common.messages.home.UpdateWidgetCashMilesResponse;

public interface BookingWidgetDelegate {

	LoadWidgetECouponResponse loadWidgetECoupon(LoadWidgetECouponRequest request);
	LoadWidgetCashMilesResponse loadWidgetCashMiles(LoadWidgetCashMilesRequest request);
	UpdateWidgetCashMilesResponse updateWidgetCashMiles(UpdateWidgetCashMilesRequest request);
	MMCreditCardData loadWidgetOneClick(LoadWidgetOneClickRequest request);
}