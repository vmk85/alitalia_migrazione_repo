package com.alitalia.aem.web.component.widget;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.LoadWidgetCashMilesRequest;
import com.alitalia.aem.common.messages.home.LoadWidgetCashMilesResponse;
import com.alitalia.aem.service.api.home.BookingWidgetService;

@Component(immediate = true, metatype = false)
@Service(value = LoadWidgetCashMilesBehaviour.class)
public class LoadWidgetCashMilesBehaviour extends Behaviour<LoadWidgetCashMilesRequest, LoadWidgetCashMilesResponse> {

	@Reference
	private BookingWidgetService bookingWidgetService;
	
	@Override
	public LoadWidgetCashMilesResponse executeOrchestration(LoadWidgetCashMilesRequest request) {
		if(request == null) throw new IllegalArgumentException("Load Widget ECoupon request is null.");
		return bookingWidgetService.loadWidgetCashMiles(request);
	}
}