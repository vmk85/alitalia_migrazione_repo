package com.alitalia.aem.web.component.widget;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.UpdateWidgetCashMilesRequest;
import com.alitalia.aem.common.messages.home.UpdateWidgetCashMilesResponse;
import com.alitalia.aem.service.api.home.BookingWidgetService;

@Component(immediate = true, metatype = false)
@Service(value = UpdateWidgetCashMilesBehaviour.class)
public class UpdateWidgetCashMilesBehaviour extends Behaviour<UpdateWidgetCashMilesRequest, UpdateWidgetCashMilesResponse> {

	@Reference
	private BookingWidgetService bookingWidgetService;
	
	@Override
	public UpdateWidgetCashMilesResponse executeOrchestration(UpdateWidgetCashMilesRequest request) {
		if(request == null) throw new IllegalArgumentException("Load Widget ECoupon request is null.");
		return bookingWidgetService.updateWidgetCashMiles(request);
	}
}