package com.alitalia.aem.web.component.widget;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.LoadWidgetECouponRequest;
import com.alitalia.aem.common.messages.home.LoadWidgetECouponResponse;
import com.alitalia.aem.service.api.home.BookingWidgetService;

@Component(immediate = true, metatype = false)
@Service(value = LoadWidgetECouponBehaviour.class)
public class LoadWidgetECouponBehaviour extends Behaviour<LoadWidgetECouponRequest, LoadWidgetECouponResponse> {

	@Reference
	private BookingWidgetService bookingWidgetService;
	
	@Override
	public LoadWidgetECouponResponse executeOrchestration(LoadWidgetECouponRequest request) {
		if(request == null) throw new IllegalArgumentException("Load Widget ECoupon request is null.");
		return bookingWidgetService.loadWidgetECoupon(request);
	}
}