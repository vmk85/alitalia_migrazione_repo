package com.alitalia.aem.web.component.widget;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.LoadWidgetOneClickRequest;
import com.alitalia.aem.common.messages.home.LoadWidgetOneClickResponse;
import com.alitalia.aem.service.api.home.BookingWidgetService;

@Component(immediate = true, metatype = false)
@Service(value = LoadWidgetOneClickBehaviour.class)
public class LoadWidgetOneClickBehaviour extends Behaviour<LoadWidgetOneClickRequest, LoadWidgetOneClickResponse> {

	@Reference
	private BookingWidgetService bookingWidgetService;

	@Override
	public LoadWidgetOneClickResponse executeOrchestration(LoadWidgetOneClickRequest request) {
		if(request == null) throw new IllegalArgumentException("Load Widget OneClick request is null.");
		return bookingWidgetService.loadWidgetOneClick(request);
	}
}