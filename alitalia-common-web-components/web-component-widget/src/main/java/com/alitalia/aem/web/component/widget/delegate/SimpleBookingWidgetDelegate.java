package com.alitalia.aem.web.component.widget.delegate;



import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.data.home.MMCreditCardData;
import com.alitalia.aem.common.messages.home.LoadWidgetCashMilesRequest;
import com.alitalia.aem.common.messages.home.LoadWidgetCashMilesResponse;
import com.alitalia.aem.common.messages.home.LoadWidgetECouponRequest;
import com.alitalia.aem.common.messages.home.LoadWidgetECouponResponse;
import com.alitalia.aem.common.messages.home.LoadWidgetOneClickRequest;
import com.alitalia.aem.common.messages.home.LoadWidgetOneClickResponse;
import com.alitalia.aem.common.messages.home.UpdateWidgetCashMilesRequest;
import com.alitalia.aem.common.messages.home.UpdateWidgetCashMilesResponse;
import com.alitalia.aem.web.component.widget.LoadWidgetCashMilesBehaviour;
import com.alitalia.aem.web.component.widget.LoadWidgetECouponBehaviour;
import com.alitalia.aem.web.component.widget.LoadWidgetOneClickBehaviour;
import com.alitalia.aem.web.component.widget.UpdateWidgetCashMilesBehaviour;

@Component(immediate = true, metatype = false)
@Service(value = BookingWidgetDelegate.class)
public class SimpleBookingWidgetDelegate implements BookingWidgetDelegate {
	
	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Reference
	private LoadWidgetECouponBehaviour loadWidgetECouponBehaviour;
	
	@Reference
	private LoadWidgetCashMilesBehaviour loadWidgetCashMilesBehaviour;
	
	@Reference
	private UpdateWidgetCashMilesBehaviour updateWidgetCashMilesBehaviour;
	
	@Reference
	private LoadWidgetOneClickBehaviour loadWidgetOneClickBehaviour;
	
	@Override
	public LoadWidgetECouponResponse loadWidgetECoupon(LoadWidgetECouponRequest request) {
		BehaviourExecutor<LoadWidgetECouponBehaviour, LoadWidgetECouponRequest, LoadWidgetECouponResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(loadWidgetECouponBehaviour, request);
	}

	@Override
	public LoadWidgetCashMilesResponse loadWidgetCashMiles(LoadWidgetCashMilesRequest request) {
		BehaviourExecutor<LoadWidgetCashMilesBehaviour, LoadWidgetCashMilesRequest, LoadWidgetCashMilesResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(loadWidgetCashMilesBehaviour, request);
	}

	@Override
	public UpdateWidgetCashMilesResponse updateWidgetCashMiles(UpdateWidgetCashMilesRequest request) {
		BehaviourExecutor<UpdateWidgetCashMilesBehaviour, UpdateWidgetCashMilesRequest, UpdateWidgetCashMilesResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(updateWidgetCashMilesBehaviour, request);
	}
	
	@Override
	public MMCreditCardData loadWidgetOneClick(LoadWidgetOneClickRequest request) {
		MMCreditCardData creditCardData = null;
		
		BehaviourExecutor<LoadWidgetOneClickBehaviour, LoadWidgetOneClickRequest, LoadWidgetOneClickResponse> executor = new BehaviourExecutor<>();
		LoadWidgetOneClickResponse oneClickResponse = executor.executeBehaviour(loadWidgetOneClickBehaviour, request);
		
		logger.debug("Load Widget One Click called. Response: "+oneClickResponse);
		
		if (oneClickResponse != null && oneClickResponse.getCreditCards() != null && oneClickResponse.getCreditCards().size()>0){
			logger.debug("Entro primo if");
			for (MMCreditCardData creditCard : oneClickResponse.getCreditCards()){
				if (creditCard.getToken()!=null && !creditCard.getToken().equals("")){
					logger.debug("Entro secondo if");
					creditCardData = new MMCreditCardData(creditCard.getCircuitCode(), creditCard.getNumber(), creditCard.getToken(), creditCard.getExpireDate(), creditCard.getCardType());
					break;
				}
			}
		}
		creditCardData.setCookie(oneClickResponse.getCookie());
		creditCardData.setExecute(oneClickResponse.getExecute());
		return creditCardData;
	}
}