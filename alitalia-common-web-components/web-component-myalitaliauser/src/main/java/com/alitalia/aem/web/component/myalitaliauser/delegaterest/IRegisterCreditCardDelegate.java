package com.alitalia.aem.web.component.myalitaliauser.delegaterest;

import com.alitalia.aem.common.messages.home.RegisterCreditCardRequest;
import com.alitalia.aem.common.messages.home.RegisterCreditCardResponse;

public interface IRegisterCreditCardDelegate {

	RegisterCreditCardResponse registerCreditCard(RegisterCreditCardRequest request);
}