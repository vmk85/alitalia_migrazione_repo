package com.alitalia.aem.web.component.myalitaliauser.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.AdyenRetrievalStoredPaymentRequest;
import com.alitalia.aem.common.messages.home.AdyenRetrievalStoredPaymentResponse;
import com.alitalia.aem.service.api.home.IAdyenRecurringPaymentService;

@Component(immediate = true, metatype = false)
@Service(value = AdyenRetrievalStoredPaymentBehaviour.class)
public class AdyenRetrievalStoredPaymentBehaviour extends Behaviour<AdyenRetrievalStoredPaymentRequest, AdyenRetrievalStoredPaymentResponse> {

	@Reference
	private IAdyenRecurringPaymentService adyenRecurringPaymentService;
	
	@Override
	protected AdyenRetrievalStoredPaymentResponse executeOrchestration(AdyenRetrievalStoredPaymentRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Retrieve payment request is null.");
		return adyenRecurringPaymentService.retrievalStoredPayment(request);
	}

}
