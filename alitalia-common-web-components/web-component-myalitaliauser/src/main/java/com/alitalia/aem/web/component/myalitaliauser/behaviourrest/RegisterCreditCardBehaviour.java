package com.alitalia.aem.web.component.myalitaliauser.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RegisterCreditCardRequest;
import com.alitalia.aem.common.messages.home.RegisterCreditCardResponse;
import com.alitalia.aem.service.api.home.IMyAlitaliaManageCreditCardService;

@Component(immediate = true, metatype = false)
@Service(value = RegisterCreditCardBehaviour.class)
public class RegisterCreditCardBehaviour extends Behaviour<RegisterCreditCardRequest, RegisterCreditCardResponse> {

	@Reference
	private IMyAlitaliaManageCreditCardService myAlitaliaManageCreditCardService;
	
	@Override
	protected RegisterCreditCardResponse executeOrchestration(RegisterCreditCardRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Register credit card request is null.");
		return myAlitaliaManageCreditCardService.storeCreditCardData(request);
	}

}
