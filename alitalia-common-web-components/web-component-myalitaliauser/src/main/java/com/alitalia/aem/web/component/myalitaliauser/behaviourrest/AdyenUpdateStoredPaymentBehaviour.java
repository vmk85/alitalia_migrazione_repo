package com.alitalia.aem.web.component.myalitaliauser.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.AdyenUpdateStoredPaymentRequest;
import com.alitalia.aem.common.messages.home.AdyenUpdateStoredPaymentResponse;
import com.alitalia.aem.service.api.home.IAdyenRecurringPaymentService;

@Component(immediate = true, metatype = false)
@Service(value = AdyenUpdateStoredPaymentBehaviour.class)
public class AdyenUpdateStoredPaymentBehaviour extends Behaviour<AdyenUpdateStoredPaymentRequest, AdyenUpdateStoredPaymentResponse> {

	@Reference
	private IAdyenRecurringPaymentService adyenRecurringPaymentService;
	
	@Override
	protected AdyenUpdateStoredPaymentResponse executeOrchestration(AdyenUpdateStoredPaymentRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Update stored payment request is null.");
		return adyenRecurringPaymentService.updateStoredPayment(request);
	}

}
