package com.alitalia.aem.web.component.myalitaliauser.delegaterest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.messages.home.RegisterCreditCardRequest;
import com.alitalia.aem.common.messages.home.RegisterCreditCardResponse;
import com.alitalia.aem.web.component.myalitaliauser.behaviourrest.RegisterCreditCardBehaviour;

@Component(immediate = true, metatype = false)
@Service(value = IRegisterCreditCardDelegate.class)
public class RegisterCreditCardDelegateImpl implements IRegisterCreditCardDelegate {

	@Reference
	private RegisterCreditCardBehaviour registerCreditCardBehaviour;


	@Override
	public RegisterCreditCardResponse registerCreditCard(RegisterCreditCardRequest request) {
		BehaviourExecutor<RegisterCreditCardBehaviour, RegisterCreditCardRequest, RegisterCreditCardResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(registerCreditCardBehaviour, request);
	}
}