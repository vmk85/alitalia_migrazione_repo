package com.alitalia.aem.web.component.myalitaliauser.delegaterest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.messages.home.AdyenDisableStoredPaymentRequest;
import com.alitalia.aem.common.messages.home.AdyenDisableStoredPaymentResponse;
import com.alitalia.aem.common.messages.home.AdyenRetrievalStoredPaymentRequest;
import com.alitalia.aem.common.messages.home.AdyenRetrievalStoredPaymentResponse;
import com.alitalia.aem.common.messages.home.AdyenStoringPaymentRequest;
import com.alitalia.aem.common.messages.home.AdyenStoringPaymentResponse;
import com.alitalia.aem.common.messages.home.AdyenUpdateStoredPaymentRequest;
import com.alitalia.aem.common.messages.home.AdyenUpdateStoredPaymentResponse;
import com.alitalia.aem.web.component.myalitaliauser.behaviourrest.AdyenDisableStoredPaymentBehaviour;
import com.alitalia.aem.web.component.myalitaliauser.behaviourrest.AdyenRetrievalStoredPaymentBehaviour;
import com.alitalia.aem.web.component.myalitaliauser.behaviourrest.AdyenStoringPaymentBehaviour;
import com.alitalia.aem.web.component.myalitaliauser.behaviourrest.AdyenUpdateStoredPaymentBehaviour;

@Component(immediate = true, metatype = false)
@Service(value = IAdyenRecurringPaymentDelegate.class)
public class AdyenRecurringPaymentDelegateImpl implements IAdyenRecurringPaymentDelegate{
	
	@Reference
	private AdyenStoringPaymentBehaviour adyenStoringPaymentBehaviour;
	
	@Reference
	private AdyenRetrievalStoredPaymentBehaviour adyenRetrievalStoredPaymentBehaviour;
	
	@Reference
	private AdyenUpdateStoredPaymentBehaviour adyenUpdateStoredPaymentBehaviour;
	
	@Reference
	private AdyenDisableStoredPaymentBehaviour adyenDisableStoredPaymentBehaviour;
	
	private static final Logger logger = LoggerFactory.getLogger(AdyenRecurringPaymentDelegateImpl.class);
	
	@Override
	public AdyenStoringPaymentResponse storingPayment(AdyenStoringPaymentRequest request) {
		logger.debug("MAPayment - storingPayment");
		AdyenStoringPaymentResponse response = null;
		BehaviourExecutor<AdyenStoringPaymentBehaviour, AdyenStoringPaymentRequest, AdyenStoringPaymentResponse> executor = new BehaviourExecutor<AdyenStoringPaymentBehaviour, AdyenStoringPaymentRequest, AdyenStoringPaymentResponse>();
		try {
			logger.debug("MAPayment - storingPayment: Executing delegate storingPaymentBehaviour");
			response = executor.executeBehaviour(adyenStoringPaymentBehaviour, request);
		}
		catch (Exception e) {
			logger.error("MAPayment - storingPayment: Errore durante il servizio di Recurring Payment : storingPayment.", e);
		}
		return response;
	}
	
	@Override
	public AdyenRetrievalStoredPaymentResponse retrievalStoredPayment(AdyenRetrievalStoredPaymentRequest request) {
		logger.debug("MAPayment - retrievalStoredPayment");
		AdyenRetrievalStoredPaymentResponse response = null;
		BehaviourExecutor<AdyenRetrievalStoredPaymentBehaviour, AdyenRetrievalStoredPaymentRequest, AdyenRetrievalStoredPaymentResponse> executor = new BehaviourExecutor<AdyenRetrievalStoredPaymentBehaviour, AdyenRetrievalStoredPaymentRequest, AdyenRetrievalStoredPaymentResponse>();
		try {
			logger.debug("MAPayment - retrievalStoredPayment: Executing delegate retrievePaymentBehaviour");
			response = executor.executeBehaviour(adyenRetrievalStoredPaymentBehaviour, request);
		}
		catch (Exception e) {
			logger.error("MAPayment - retrievalStoredPayment: Errore durante il servizio di Recurring Payment : retrievePayment.", e);
		}
		return response;
	}

	@Override
	public AdyenUpdateStoredPaymentResponse updateStoredPayment(AdyenUpdateStoredPaymentRequest request) {
		logger.debug("MAPayment - updateStoredPayment");
		AdyenUpdateStoredPaymentResponse response = null;
		BehaviourExecutor<AdyenUpdateStoredPaymentBehaviour, AdyenUpdateStoredPaymentRequest, AdyenUpdateStoredPaymentResponse> executor = new BehaviourExecutor<AdyenUpdateStoredPaymentBehaviour, AdyenUpdateStoredPaymentRequest, AdyenUpdateStoredPaymentResponse>();
		try {
			logger.debug("MAPayment - updateStoredPayment: Executing delegate updateStoredPaymentBehaviour");
			response = executor.executeBehaviour(adyenUpdateStoredPaymentBehaviour, request);
		}
		catch (Exception e) {
			logger.error("MAPayment - updateStoredPayment: Errore durante il servizio di Recurring Payment : updateStoredPayment.", e);
		}
		return response;
	}

	@Override
	public AdyenDisableStoredPaymentResponse disableStoredPayment(AdyenDisableStoredPaymentRequest request) {
		logger.debug("MAPayment - disableStoredPayment");
		AdyenDisableStoredPaymentResponse response = null;
		BehaviourExecutor<AdyenDisableStoredPaymentBehaviour, AdyenDisableStoredPaymentRequest, AdyenDisableStoredPaymentResponse> executor = new BehaviourExecutor<AdyenDisableStoredPaymentBehaviour, AdyenDisableStoredPaymentRequest, AdyenDisableStoredPaymentResponse>();
		try {
			logger.debug("MAPayment - disableStoredPayment: Executing delegate disableStoredPaymentBehaviour");
			response = executor.executeBehaviour(adyenDisableStoredPaymentBehaviour, request);
		}
		catch (Exception e) {
			logger.error("MAPayment - disableStoredPayment: Errore durante il servizio di Recurring Payment : disableStoredPayment.", e);
		}
		return response;
	}
}
