package com.alitalia.aem.web.component.myalitaliauser.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.AdyenDisableStoredPaymentRequest;
import com.alitalia.aem.common.messages.home.AdyenDisableStoredPaymentResponse;
import com.alitalia.aem.service.api.home.IAdyenRecurringPaymentService;

@Component(immediate = true, metatype = false)
@Service(value = AdyenDisableStoredPaymentBehaviour.class)
public class AdyenDisableStoredPaymentBehaviour extends Behaviour<AdyenDisableStoredPaymentRequest, AdyenDisableStoredPaymentResponse> {

	@Reference
	private IAdyenRecurringPaymentService adyenRecurringPaymentService;
	
	@Override
	protected AdyenDisableStoredPaymentResponse executeOrchestration(AdyenDisableStoredPaymentRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Disable stored payment request is null.");
		return adyenRecurringPaymentService.disableStoredPayment(request);
	}

}
