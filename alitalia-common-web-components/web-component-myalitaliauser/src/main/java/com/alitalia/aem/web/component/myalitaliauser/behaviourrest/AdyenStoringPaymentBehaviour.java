package com.alitalia.aem.web.component.myalitaliauser.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.AdyenStoringPaymentRequest;
import com.alitalia.aem.common.messages.home.AdyenStoringPaymentResponse;
import com.alitalia.aem.service.api.home.IAdyenRecurringPaymentService;

@Component(immediate = true, metatype = false)
@Service(value = AdyenStoringPaymentBehaviour.class)
public class AdyenStoringPaymentBehaviour extends Behaviour<AdyenStoringPaymentRequest, AdyenStoringPaymentResponse> {

	@Reference
	private IAdyenRecurringPaymentService adyenRecurringPaymentService;
	
	@Override
	protected AdyenStoringPaymentResponse executeOrchestration(AdyenStoringPaymentRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Storing payment request is null.");
		return adyenRecurringPaymentService.storingPayment(request);
	}

}
