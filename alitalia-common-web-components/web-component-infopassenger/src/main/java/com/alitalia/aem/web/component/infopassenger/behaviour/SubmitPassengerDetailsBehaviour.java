package com.alitalia.aem.web.component.infopassenger.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.InfoPassengerSubmitPassengerRequest;
import com.alitalia.aem.common.messages.home.InfoPassengerSubmitPassengerResponse;
import com.alitalia.aem.service.api.home.BookingInfoPassengerService;

@Component(immediate = true, metatype = false)
@Service(value = SubmitPassengerDetailsBehaviour.class)
public class SubmitPassengerDetailsBehaviour extends Behaviour<InfoPassengerSubmitPassengerRequest, InfoPassengerSubmitPassengerResponse> {

	@Reference
	BookingInfoPassengerService bookingInfoPassengerService;
	
	@Override
	public InfoPassengerSubmitPassengerResponse executeOrchestration(InfoPassengerSubmitPassengerRequest request) {
		if (request == null)
			throw new IllegalArgumentException("InfoPassengerSubmitPassengerRequest is null.");
		return bookingInfoPassengerService.submitPassengerDetails(request);
	}

}
