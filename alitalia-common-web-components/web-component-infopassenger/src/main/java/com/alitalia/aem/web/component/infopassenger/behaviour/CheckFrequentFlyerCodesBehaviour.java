package com.alitalia.aem.web.component.infopassenger.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.InfoPassengerCheckFrequentFlyersCodeRequest;
import com.alitalia.aem.common.messages.home.InfoPassengerCheckFrequentFlyersCodeResponse;
import com.alitalia.aem.service.api.home.BookingInfoPassengerService;

@Component(immediate = true, metatype = false)
@Service(value = CheckFrequentFlyerCodesBehaviour.class)
public class CheckFrequentFlyerCodesBehaviour 
		extends Behaviour<InfoPassengerCheckFrequentFlyersCodeRequest, InfoPassengerCheckFrequentFlyersCodeResponse> {

	@Reference
	BookingInfoPassengerService bookingInfoPassengerService;
	
	@Override
	public InfoPassengerCheckFrequentFlyersCodeResponse executeOrchestration(InfoPassengerCheckFrequentFlyersCodeRequest request) {
		if (request == null)
			throw new IllegalArgumentException("InfoPassengerCheckFrequentFlyersCodeRequest is null.");
		return bookingInfoPassengerService.executeCheckFrequentFlyersCode(request);
	}

}
