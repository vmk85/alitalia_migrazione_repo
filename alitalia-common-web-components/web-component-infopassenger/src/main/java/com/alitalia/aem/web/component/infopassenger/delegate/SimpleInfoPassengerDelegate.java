package com.alitalia.aem.web.component.infopassenger.delegate;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.messages.home.InfoPassengerCheckFrequentFlyersCodeRequest;
import com.alitalia.aem.common.messages.home.InfoPassengerCheckFrequentFlyersCodeResponse;
import com.alitalia.aem.common.messages.home.InfoPassengerCreatePNRRequest;
import com.alitalia.aem.common.messages.home.InfoPassengerCreatePNRResponse;
import com.alitalia.aem.common.messages.home.InfoPassengerSubmitPassengerRequest;
import com.alitalia.aem.common.messages.home.InfoPassengerSubmitPassengerResponse;
import com.alitalia.aem.web.component.infopassenger.behaviour.CheckFrequentFlyerCodesBehaviour;
import com.alitalia.aem.web.component.infopassenger.behaviour.CreatePNRBehaviour;
import com.alitalia.aem.web.component.infopassenger.behaviour.SubmitPassengerDetailsBehaviour;

@Service
@Component(immediate = true, metatype = false)
public class SimpleInfoPassengerDelegate implements InfoPassengerDelegate {
	
	@Reference
	private CreatePNRBehaviour createPNRBehaviour;
	
	@Reference
	private CheckFrequentFlyerCodesBehaviour checkFrequentFlyerCodesBehaviour;
	
	@Reference
	private SubmitPassengerDetailsBehaviour submitPassengerDetailsBehaviour;
	
	@Override
	public InfoPassengerCreatePNRResponse createPNRRequest(InfoPassengerCreatePNRRequest request) {
		BehaviourExecutor<CreatePNRBehaviour, InfoPassengerCreatePNRRequest, InfoPassengerCreatePNRResponse> executor =
					new BehaviourExecutor<>();
		return executor.executeBehaviour(createPNRBehaviour, request);
	}
	
	@Override
	public InfoPassengerCheckFrequentFlyersCodeResponse checkFrequentFlyersCode(InfoPassengerCheckFrequentFlyersCodeRequest request) {
		BehaviourExecutor<CheckFrequentFlyerCodesBehaviour, InfoPassengerCheckFrequentFlyersCodeRequest, InfoPassengerCheckFrequentFlyersCodeResponse> executor =
				new BehaviourExecutor<>();
		return executor.executeBehaviour(checkFrequentFlyerCodesBehaviour, request);
	}
	
	@Override
	public InfoPassengerSubmitPassengerResponse submitPassengerDetails(InfoPassengerSubmitPassengerRequest request) {
		BehaviourExecutor<SubmitPassengerDetailsBehaviour, InfoPassengerSubmitPassengerRequest, InfoPassengerSubmitPassengerResponse> executor =
					new BehaviourExecutor<>();
		return executor.executeBehaviour(submitPassengerDetailsBehaviour, request);
	}
}