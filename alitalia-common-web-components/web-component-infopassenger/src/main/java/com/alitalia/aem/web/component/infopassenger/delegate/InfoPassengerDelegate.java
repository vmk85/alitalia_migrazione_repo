package com.alitalia.aem.web.component.infopassenger.delegate;

import com.alitalia.aem.common.messages.home.InfoPassengerCheckFrequentFlyersCodeRequest;
import com.alitalia.aem.common.messages.home.InfoPassengerCheckFrequentFlyersCodeResponse;
import com.alitalia.aem.common.messages.home.InfoPassengerCreatePNRRequest;
import com.alitalia.aem.common.messages.home.InfoPassengerCreatePNRResponse;
import com.alitalia.aem.common.messages.home.InfoPassengerSubmitPassengerRequest;
import com.alitalia.aem.common.messages.home.InfoPassengerSubmitPassengerResponse;

public interface InfoPassengerDelegate {

	InfoPassengerCreatePNRResponse createPNRRequest(InfoPassengerCreatePNRRequest request);
	
	InfoPassengerCheckFrequentFlyersCodeResponse checkFrequentFlyersCode(InfoPassengerCheckFrequentFlyersCodeRequest request);
	
	InfoPassengerSubmitPassengerResponse submitPassengerDetails(InfoPassengerSubmitPassengerRequest request);
	
}
