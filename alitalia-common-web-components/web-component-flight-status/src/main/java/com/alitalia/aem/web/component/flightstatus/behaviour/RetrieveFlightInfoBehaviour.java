package com.alitalia.aem.web.component.flightstatus.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveFlightInfoRequest;
import com.alitalia.aem.common.messages.home.RetrieveFlightInfoResponse;
import com.alitalia.aem.service.api.home.FlightStatusService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveFlightInfoBehaviour.class)
public class RetrieveFlightInfoBehaviour extends Behaviour<RetrieveFlightInfoRequest, RetrieveFlightInfoResponse> {

	@Reference
	private FlightStatusService flightStatusService;
	
	@Override
	public RetrieveFlightInfoResponse executeOrchestration(RetrieveFlightInfoRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Flight Info request is null.");
		return flightStatusService.retrieveFlightInfo(request);
	}	
}