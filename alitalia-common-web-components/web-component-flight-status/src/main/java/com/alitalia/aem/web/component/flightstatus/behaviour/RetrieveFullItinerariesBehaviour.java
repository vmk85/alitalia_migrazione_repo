package com.alitalia.aem.web.component.flightstatus.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveFullItinerariesRequest;
import com.alitalia.aem.common.messages.home.RetrieveFullItinerariesResponse;
import com.alitalia.aem.service.api.home.FlightStatusService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveFullItinerariesBehaviour.class)
public class RetrieveFullItinerariesBehaviour extends Behaviour<RetrieveFullItinerariesRequest, RetrieveFullItinerariesResponse> {

	@Reference
	private FlightStatusService flightStatusService;

	@Override
	public RetrieveFullItinerariesResponse executeOrchestration(RetrieveFullItinerariesRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Full Itineraries request is null.");
		return flightStatusService.retrieveFullItineraries(request);
	}
}