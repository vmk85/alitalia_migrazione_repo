package com.alitalia.aem.web.component.flightstatus.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveFlightDetailsRequest;
import com.alitalia.aem.common.messages.home.RetrieveFlightDetailsResponse;
import com.alitalia.aem.service.api.home.FlightStatusService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveFlightDetailsBehaviour.class)
public class RetrieveFlightDetailsBehaviour extends Behaviour<RetrieveFlightDetailsRequest, RetrieveFlightDetailsResponse> {

	@Reference
	private FlightStatusService flightStatusService;
	
	@Override
	public RetrieveFlightDetailsResponse executeOrchestration(RetrieveFlightDetailsRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Flight Details request is null.");
		return flightStatusService.retrieveFlightDetails(request);
	}	
}