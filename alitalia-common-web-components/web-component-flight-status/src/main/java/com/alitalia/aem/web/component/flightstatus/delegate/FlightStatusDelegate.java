package com.alitalia.aem.web.component.flightstatus.delegate;

import com.alitalia.aem.common.messages.home.RetrieveAirportRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportResponse;
import com.alitalia.aem.common.messages.home.RetrieveFlightDetailsRequest;
import com.alitalia.aem.common.messages.home.RetrieveFlightDetailsResponse;
import com.alitalia.aem.common.messages.home.RetrieveFlightInfoRequest;
import com.alitalia.aem.common.messages.home.RetrieveFlightInfoResponse;
import com.alitalia.aem.common.messages.home.RetrieveFullItinerariesRequest;
import com.alitalia.aem.common.messages.home.RetrieveFullItinerariesResponse;
import com.alitalia.aem.common.messages.home.RetrieveItinerariesRequest;
import com.alitalia.aem.common.messages.home.RetrieveItinerariesResponse;

public interface FlightStatusDelegate {
	
	RetrieveFlightDetailsResponse retrieveFlightDetails(RetrieveFlightDetailsRequest request);
	RetrieveFlightInfoResponse retrieveFlightInfo(RetrieveFlightInfoRequest request);
	RetrieveItinerariesResponse retrieveItineraries(RetrieveItinerariesRequest request);
	RetrieveFullItinerariesResponse retrieveFullItineraries(RetrieveFullItinerariesRequest request);
	RetrieveAirportResponse retrieveAirport(RetrieveAirportRequest request);
}