package com.alitalia.aem.web.component.geospatial.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveGeoOffersRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoOffersResponse;
import com.alitalia.aem.service.api.home.GeoSpatialService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveGeoOffersByTargetBehaviour.class)
public class RetrieveGeoOffersByTargetBehaviour extends Behaviour<RetrieveGeoOffersRequest, RetrieveGeoOffersResponse> {

	@Reference
	private GeoSpatialService geoSpatialService;
	
	@Override
	public RetrieveGeoOffersResponse executeOrchestration(RetrieveGeoOffersRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Geo Offers request is null.");
		return geoSpatialService.retrieveGeoOffersByTarget(request);
	}	
}