package com.alitalia.aem.web.component.geospatial.delegate;

//import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.apache.sling.commons.scheduler.ScheduleOptions;
import org.apache.sling.commons.scheduler.Scheduler;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.data.home.enumerations.OwRtEnum;
import com.alitalia.aem.common.messages.home.RetrieveGeoAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoAirportsResponse;
import com.alitalia.aem.common.messages.home.RetrieveGeoCitiesRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoCitiesResponse;
import com.alitalia.aem.common.messages.home.RetrieveGeoCountryRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoCountryResponse;
import com.alitalia.aem.common.messages.home.RetrieveGeoNearestAirportByCoordsRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoNearestAirportByCoordsResponse;
import com.alitalia.aem.common.messages.home.RetrieveGeoOffersRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoOffersResponse;
import com.alitalia.aem.web.component.geospatial.behaviour.RetrieveGeoAirportsBehaviour;
import com.alitalia.aem.web.component.geospatial.behaviour.RetrieveGeoCitiesBehaviour;
import com.alitalia.aem.web.component.geospatial.behaviour.RetrieveGeoCitiesByTargetBehaviour;
import com.alitalia.aem.web.component.geospatial.behaviour.RetrieveGeoCountryBehaviour;
import com.alitalia.aem.web.component.geospatial.behaviour.RetrieveGeoNearestAirportByCoordsBehaviour;
import com.alitalia.aem.web.component.geospatial.behaviour.RetrieveGeoOffersBehaviour;
import com.alitalia.aem.web.component.geospatial.behaviour.RetrieveGeoOffersByTargetBehaviour;

@Component(immediate = true, metatype = false)
@Properties({
	@Property( name = SimpleGeoSpatialDelegate.CACHE_CITY_REFRESH_INTERVAL, label = "Refresh interval of the city cache." ),
	@Property( name = SimpleGeoSpatialDelegate.IS_CACHE_CITY_ENABLED, label = "Flag to enable city cache." ),
	@Property( name = SimpleGeoSpatialDelegate.CACHE_AIRPORT_REFRESH_INTERVAL, label = "Refresh interval of the airport cache." ),
	@Property( name = SimpleGeoSpatialDelegate.IS_CACHE_AIRPORT_ENABLED, label = "Flag to enable airport cache." ),
	@Property( name = SimpleGeoSpatialDelegate.CACHE_OFFER_REFRESH_INTERVAL, label = "Refresh interval of the offer cache." ),
	@Property( name = SimpleGeoSpatialDelegate.IS_CACHE_OFFER_ENABLED, label = "Flag to enable offer cache." )
})
@Service(value = GeoSpatialDelegate.class)
public class SimpleGeoSpatialDelegate implements GeoSpatialDelegate {

	private static final Logger logger = LoggerFactory.getLogger(SimpleGeoSpatialDelegate.class);

	@Reference
	private RetrieveGeoOffersBehaviour retrieveGeoOffersBehaviour;

	@Reference
	private RetrieveGeoCitiesBehaviour retrieveGeoCitiesBehaviour;
	
	@Reference
	private RetrieveGeoOffersByTargetBehaviour retrieveGeoOffersByTargetBehaviour;

	@Reference
	private RetrieveGeoCitiesByTargetBehaviour retrieveGeoCitiesByTargetBehaviour;

	@Reference
	private RetrieveGeoAirportsBehaviour retrieveGeoAirportsBehaviour;

	@Reference
	private RetrieveGeoCountryBehaviour retrieveGeoCountryBehaviour;

	@Reference
	private RetrieveGeoNearestAirportByCoordsBehaviour retrieveGeoNearestAirportByCoordsBehaviour;

	@Reference
	private Scheduler scheduler;

	private ConcurrentHashMap<String, Object> cacheMapCity = new ConcurrentHashMap<String, Object>();
	private ConcurrentHashMap<String, Object> cacheMapCityByTarget = new ConcurrentHashMap<String, Object>();
	private ConcurrentHashMap<String, Object> cacheMapAirport = new ConcurrentHashMap<String, Object>();
	private ConcurrentHashMap<String, Object> cacheMapOffer = new ConcurrentHashMap<String, Object>();
	private ConcurrentHashMap<String, Object> cacheMapOfferByTarget = new ConcurrentHashMap<String, Object>();

	
	private boolean isCacheCityEnabled = false;
	private long refreshIntervalCity = 3600000L;

	private boolean isCacheAirportEnabled = false;
	private long refreshIntervalAirport = 3600000L;

	private boolean isCacheOfferEnabled = false;
	private long refreshIntervalOffer = 3600000L;	

	public static final String CACHE_CITY_REFRESH_INTERVAL = "cache.city.refresh.interval";
	public static final String IS_CACHE_CITY_ENABLED = "cache.city.is_enabled";

	public static final String CACHE_AIRPORT_REFRESH_INTERVAL = "cache.airport.refresh.interval";
	public static final String IS_CACHE_AIRPORT_ENABLED = "cache.airport.is_enabled";

	public static final String CACHE_OFFER_REFRESH_INTERVAL = "cache.offer.refresh.interval";
	public static final String IS_CACHE_OFFER_ENABLED = "cache.offer.is_enabled";

	@Override
	public RetrieveGeoOffersResponse retrieveGeoOffers(RetrieveGeoOffersRequest request) {
		BehaviourExecutor<RetrieveGeoOffersBehaviour, RetrieveGeoOffersRequest, RetrieveGeoOffersResponse> executor = new BehaviourExecutor<>();

		logger.debug("Executing delegate retrieveGeoOffers ...");
		
		if (isCacheOfferEnabled) {
			String separator = "_";
			String keyDepartureAirport = request.getDepartureAirportCode();
			String keyLang = request.getLanguageCode();
			String keyMarket = request.getMarketCode();
			String keyPaxType = request.getPaxType() != null ? request.getPaxType() : "";
			StringBuffer keyBuffer = new StringBuffer();
			keyBuffer.append(keyDepartureAirport).append(separator).append(keyLang).append(separator).append(keyMarket).append(separator).append(keyPaxType);
			String key = keyBuffer.toString();
			if (key != null && !"".equals(key)) {

				RetrieveGeoOffersResponse retrieveGeoOffersResponse  = (RetrieveGeoOffersResponse)cacheMapOffer.get(key);

				if(retrieveGeoOffersResponse == null) {
					logger.debug("Called cache manager to retrieve offers - cache miss");
					retrieveGeoOffersResponse = (RetrieveGeoOffersResponse) cacheMapOffer.get(key);
					if(retrieveGeoOffersResponse == null) {
						retrieveGeoOffersResponse = executor.executeBehaviour(retrieveGeoOffersBehaviour, request);
						if (retrieveGeoOffersResponse!=null && retrieveGeoOffersResponse.getOffers()!=null && retrieveGeoOffersResponse.getOffers().size()>0)
							cacheMapOffer.put(key, retrieveGeoOffersResponse);

						logger.debug("Called cache manager to retrieve offers - put offers list in cache");
					} else {
						logger.debug("Called cache manager to retrieve offers - put offers list in cache from an other thread");
					}
					return retrieveGeoOffersResponse;
				} 

				logger.info("Called cache manager to retrieve offers - returned offers list from cache");
				return retrieveGeoOffersResponse;
			} else {
				logger.debug("Bad key found - starting service call to get offers list");
				logger.debug("Executing delegate retrieveGeoOffers DONE");
				return executor.executeBehaviour(retrieveGeoOffersBehaviour, request);
			}
		} else {
			logger.debug("Cache manager disabled - starting service call to get offers list");
			logger.debug("Executing delegate retrieveGeoOffers DONE");
			return executor.executeBehaviour(retrieveGeoOffersBehaviour, request);
		}
	}

	@Override
	public RetrieveGeoCitiesResponse retrieveGeoCities(RetrieveGeoCitiesRequest request) {
		BehaviourExecutor<RetrieveGeoCitiesBehaviour, RetrieveGeoCitiesRequest, RetrieveGeoCitiesResponse> executor = new BehaviourExecutor<>();

		logger.debug("Executing delegate retrieveGeoCities ...");
		
		if (isCacheCityEnabled) {
			String keyLang = request.getLanguageCode();
			String keyMarket = request.getMarketCode();
			String keyPaxType = request.getPaxType();
			String separator = "_";
			StringBuffer keyBuffer = new StringBuffer();
			keyBuffer.append(keyLang).append(separator).append(keyMarket).append(separator).append(keyPaxType);

			RetrieveGeoCitiesResponse retrieveGeoCitiesResponse  = (RetrieveGeoCitiesResponse)cacheMapCity.get(keyBuffer.toString());

			if(retrieveGeoCitiesResponse == null) {
				logger.debug("Called cache manager to retrieve cities - cache miss");
				//Inserisco il valore estratto dalla mappa in una variabile per evitare problemi dovuti alla cancellazione della cache da parte di un altro thread
				//in seguito all'operazione di put in cache del thread corrente. Se non utilizzassi la variabile locale dovrei sincronizzare con il lock anche la
				//cancellazione della cache che ridurrebbe notevolmente le performance  
				retrieveGeoCitiesResponse = (RetrieveGeoCitiesResponse) cacheMapCity.get(keyBuffer.toString());
				if(retrieveGeoCitiesResponse == null) {
					retrieveGeoCitiesResponse = executor.executeBehaviour(retrieveGeoCitiesBehaviour, request);
					if (retrieveGeoCitiesResponse!=null && retrieveGeoCitiesResponse.getCities()!=null && retrieveGeoCitiesResponse.getCities().size()>0)
						cacheMapCity.put(keyBuffer.toString(), retrieveGeoCitiesResponse);

					logger.debug("Called cache manager to retrieve cities - put cities list in cache");
				} else {
					logger.debug("Called cache manager to retrieve cities - put cities list in cache from an other thread");
				}

				return retrieveGeoCitiesResponse;
			} 

			logger.info("Called cache manager to retrieve cities - returned cities list from cache");
			logger.debug("Executing delegate retrieveGeoCities DONE");
			return retrieveGeoCitiesResponse;
		} else {
			logger.debug("Cache manager disabled - starting service call to get cities list");
			logger.debug("Executing delegate retrieveGeoCities DONE");
			return executor.executeBehaviour(retrieveGeoCitiesBehaviour, request);
		}
	}		
	
	@Override
	public RetrieveGeoOffersResponse retrieveGeoOffersByTarget(RetrieveGeoOffersRequest request) {
		BehaviourExecutor<RetrieveGeoOffersByTargetBehaviour, RetrieveGeoOffersRequest, RetrieveGeoOffersResponse> executor = new BehaviourExecutor<>();

		logger.debug("Executing delegate retrieveGeoOffersByTarget ...");
		
		if (isCacheOfferEnabled) {
			String separator = "_";
			String keyDepartureAirport = request.getDepartureAirportCode();
			String keyMarket = request.getMarketCode();
			String keyTarget = request.getTarget();
			String areaValue = request.getAreaValue() != null ? request.getAreaValue().value() : "";
			String owrt = request.getOwRt() != null ? request.getOwRt().value() : "";
			String keyPaxType = request.getPaxType() != null ? request.getPaxType() : "";
			StringBuffer keyBuffer = new StringBuffer();
			keyBuffer.append(keyDepartureAirport).append(separator).append(keyMarket).append(separator).append(keyTarget).append(separator).append(areaValue).append(separator).append(owrt).append(separator).append(keyPaxType);
			String key = keyBuffer.toString();
			if (key != null && !"".equals(key)) {

				RetrieveGeoOffersResponse retrieveGeoOffersResponse  = (RetrieveGeoOffersResponse)cacheMapOfferByTarget.get(key);

				if(retrieveGeoOffersResponse == null) {
					logger.debug("Called cache manager to retrieve offers - cache miss");
					retrieveGeoOffersResponse = (RetrieveGeoOffersResponse) cacheMapOfferByTarget.get(key);
					if(retrieveGeoOffersResponse == null) {
						retrieveGeoOffersResponse = executor.executeBehaviour(retrieveGeoOffersByTargetBehaviour, request);
						if (retrieveGeoOffersResponse!=null && retrieveGeoOffersResponse.getOffers()!=null && retrieveGeoOffersResponse.getOffers().size()>0)
							cacheMapOfferByTarget.put(key, retrieveGeoOffersResponse);

						logger.debug("Called cache manager to retrieve offers - put offers list in cache");
					} else {
						logger.debug("Called cache manager to retrieve offers - put offers list in cache from an other thread");
					}
					return retrieveGeoOffersResponse;
				} 

				logger.info("Called cache manager to retrieve offers - returned offers list from cache");
				return retrieveGeoOffersResponse;
			} else {
				logger.debug("Bad key found - starting service call to get offers list");
				logger.debug("Executing delegate retrieveGeoOffers DONE");
				return executor.executeBehaviour(retrieveGeoOffersByTargetBehaviour, request);
			}
		} else {
			logger.debug("Cache manager disabled - starting service call to get offers list");
			logger.debug("Executing delegate retrieveGeoOffersByTarget DONE");
			return executor.executeBehaviour(retrieveGeoOffersByTargetBehaviour, request);
		}
	}

	@Override
	public RetrieveGeoCitiesResponse retrieveGeoCitiesByTarget(RetrieveGeoCitiesRequest request) {
		BehaviourExecutor<RetrieveGeoCitiesByTargetBehaviour, RetrieveGeoCitiesRequest, RetrieveGeoCitiesResponse> executor = new BehaviourExecutor<>();

		logger.debug("Executing delegate retrieveGeoCitiesByTarget ...");
		
		if (isCacheCityEnabled) {
			String keyLang = request.getLanguageCode();
			String keyMarket = request.getMarketCode();
			String keyTarget = request.getTarget();
			String keyPaxType = request.getPaxType();
			String separator = "_";
			StringBuffer keyBuffer = new StringBuffer();
			keyBuffer.append(keyLang).append(separator).append(keyMarket).append(separator).append(keyTarget).append(separator).append(keyPaxType);

			RetrieveGeoCitiesResponse retrieveGeoCitiesResponse  = (RetrieveGeoCitiesResponse)cacheMapCityByTarget.get(keyBuffer.toString());

			if(retrieveGeoCitiesResponse == null) {
				logger.debug("Called cache manager to retrieve cities - cache miss");
				//Inserisco il valore estratto dalla mappa in una variabile per evitare problemi dovuti alla cancellazione della cache da parte di un altro thread
				//in seguito all'operazione di put in cache del thread corrente. Se non utilizzassi la variabile locale dovrei sincronizzare con il lock anche la
				//cancellazione della cache che ridurrebbe notevolmente le performance  
				retrieveGeoCitiesResponse = (RetrieveGeoCitiesResponse) cacheMapCityByTarget.get(keyBuffer.toString());
				if(retrieveGeoCitiesResponse == null) {
					retrieveGeoCitiesResponse = executor.executeBehaviour(retrieveGeoCitiesByTargetBehaviour, request);
					if (retrieveGeoCitiesResponse!=null && retrieveGeoCitiesResponse.getCities()!=null && retrieveGeoCitiesResponse.getCities().size()>0)
						cacheMapCityByTarget.put(keyBuffer.toString(), retrieveGeoCitiesResponse);

					logger.debug("Called cache manager to retrieve cities - put cities list in cache");
				} else {
					logger.debug("Called cache manager to retrieve cities - put cities list in cache from an other thread");
				}

				return retrieveGeoCitiesResponse;
			} 

			logger.info("Called cache manager to retrieve cities - returned cities list from cache");
			logger.debug("Executing delegate retrieveGeoCitiesByTarget DONE");
			return retrieveGeoCitiesResponse;
		} else {
			logger.debug("Cache manager disabled - starting service call to get cities list");
			logger.debug("Executing delegate retrieveGeoCitiesByTarget DONE");
			return executor.executeBehaviour(retrieveGeoCitiesByTargetBehaviour, request);
		}
	}		

	@Override
	public RetrieveGeoAirportsResponse retrieveGeoAirports(RetrieveGeoAirportsRequest request) {
		BehaviourExecutor<RetrieveGeoAirportsBehaviour, RetrieveGeoAirportsRequest, RetrieveGeoAirportsResponse> executor = new BehaviourExecutor<>();

		logger.debug("Executing delegate retrieveGeoAirports ...");
		
		if (isCacheAirportEnabled) {
			String key = request.getCityCode();
			if (key != null && !"".equals(key)) {

				RetrieveGeoAirportsResponse retrieveGeoAirportsResponse  = (RetrieveGeoAirportsResponse)cacheMapAirport.get(key);

				if(retrieveGeoAirportsResponse == null) {
					logger.debug("Called cache manager to retrieve airports - cache miss");
					retrieveGeoAirportsResponse = (RetrieveGeoAirportsResponse) cacheMapAirport.get(key);
					if(retrieveGeoAirportsResponse == null) {
						retrieveGeoAirportsResponse = executor.executeBehaviour(retrieveGeoAirportsBehaviour, request);
						if (retrieveGeoAirportsResponse!=null && retrieveGeoAirportsResponse.getCodesDescriptions()!=null && retrieveGeoAirportsResponse.getCodesDescriptions().size()>0)
							cacheMapAirport.put(key, retrieveGeoAirportsResponse);

						logger.debug("Called cache manager to retrieve airports - put airports list in cache");
					} else {
						logger.debug("Called cache manager to retrieve airports - put airports list in cache from an other thread");
					}
					logger.debug("Executing delegate retrieveGeoAirports DONE");
					return retrieveGeoAirportsResponse;
				} 

				logger.info("Called cache manager to retrieve airports - returned airports list from cache");
				return retrieveGeoAirportsResponse;
			} else {
				logger.debug("Bad key found - starting service call to get airports list");
				logger.debug("Executing delegate retrieveGeoAirports DONE");
				return executor.executeBehaviour(retrieveGeoAirportsBehaviour, request);
			}
		} else {
			logger.debug("Cache manager disabled - starting service call to get airports list");
			logger.debug("Executing delegate retrieveGeoAirports DONE");
			return executor.executeBehaviour(retrieveGeoAirportsBehaviour, request);
		}
	}	

	@Override
	public RetrieveGeoCountryResponse retrieveGeoCountry(RetrieveGeoCountryRequest request) {
		BehaviourExecutor<RetrieveGeoCountryBehaviour, RetrieveGeoCountryRequest, RetrieveGeoCountryResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(retrieveGeoCountryBehaviour, request);
	}

	@Override
	public RetrieveGeoNearestAirportByCoordsResponse retrieveGeoNearestAirportByCoords(
			RetrieveGeoNearestAirportByCoordsRequest request) {
		BehaviourExecutor<RetrieveGeoNearestAirportByCoordsBehaviour, RetrieveGeoNearestAirportByCoordsRequest, RetrieveGeoNearestAirportByCoordsResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(retrieveGeoNearestAirportByCoordsBehaviour, request);
	}

	private void refreshAirportCache() {
		logger.debug("refreshAirportCache started");
		if (!cacheMapAirport.isEmpty()) {
			logger.debug("refreshAirportCache is not empty, doing refresh");
			BehaviourExecutor<RetrieveGeoAirportsBehaviour, RetrieveGeoAirportsRequest, RetrieveGeoAirportsResponse> executor = new BehaviourExecutor<>();
			RetrieveGeoAirportsResponse retrieveGeoAirportsResponse;
			for(String key : cacheMapAirport.keySet()) {
				logger.debug("Refreshing key entry: " + key);
				RetrieveGeoAirportsRequest request = new RetrieveGeoAirportsRequest();
				request.setCityCode(key);
				retrieveGeoAirportsResponse = executor.executeBehaviour(retrieveGeoAirportsBehaviour, request);
				if (cacheMapAirport != null) {
					cacheMapAirport.replace(key, retrieveGeoAirportsResponse);
				}
				else {
					cacheMapAirport.remove(key);
				}
			}
		}
		logger.debug("refreshAirportCache ended");
	}

	private void refreshCityCache() {
		logger.debug("refreshCityCache started");
		if (!cacheMapCity.isEmpty()) {
			logger.debug("cacheMapCity is not empty, doing refresh");
			BehaviourExecutor<RetrieveGeoCitiesBehaviour, RetrieveGeoCitiesRequest, RetrieveGeoCitiesResponse> executor = new BehaviourExecutor<>();
			RetrieveGeoCitiesResponse retrieveGeoCitiesResponse;
			for(String key : cacheMapCity.keySet()) {
				String separator = "_";
				String[] keyList = key.split(separator);
				String keyLang = keyList[0];
				String keyMarket = keyList[1];
				String keyPaxType = keyList[2];
				logger.debug("Refreshing key entry: " + key);
				RetrieveGeoCitiesRequest request = new RetrieveGeoCitiesRequest();
				request.setLanguageCode(keyLang);
				request.setMarketCode(keyMarket);
				request.setPaxType(keyPaxType);
				retrieveGeoCitiesResponse = executor.executeBehaviour(retrieveGeoCitiesBehaviour, request);
				if (retrieveGeoCitiesResponse != null) {
					cacheMapCity.replace(key, retrieveGeoCitiesResponse);
				}
				else {
					cacheMapCity.remove(key);
				}
			}
		}
		logger.debug("refreshCityCache ended");
	}
	
	private void refreshCityCacheByTarget() {
		logger.debug("refreshCityCacheByTarget started");
		if (!cacheMapCityByTarget.isEmpty()) {
			logger.debug("cacheMapCityByTarget is not empty, doing refresh");
			BehaviourExecutor<RetrieveGeoCitiesByTargetBehaviour, RetrieveGeoCitiesRequest, RetrieveGeoCitiesResponse> executor = new BehaviourExecutor<>();
			RetrieveGeoCitiesResponse retrieveGeoCitiesResponse;
			for(String key : cacheMapCityByTarget.keySet()) {
				String separator = "_";
				String[] keyList = key.split(separator);
				String keyLang = keyList[0];
				String keyMarket = keyList[1];
				String keyTarget = keyList[2];
				String keyPaxType = keyList[3];
				logger.debug("Refreshing key entry: " + key);
				RetrieveGeoCitiesRequest request = new RetrieveGeoCitiesRequest();
				request.setLanguageCode(keyLang);
				request.setMarketCode(keyMarket);
				request.setTarget(keyTarget);
				request.setPaxType(keyPaxType);
				retrieveGeoCitiesResponse = executor.executeBehaviour(retrieveGeoCitiesByTargetBehaviour, request);
				if (retrieveGeoCitiesResponse != null) {
					cacheMapCityByTarget.replace(key, retrieveGeoCitiesResponse);
				}
				else {
					cacheMapCityByTarget.remove(key);
				}
			}
		}
		logger.debug("refreshCityCacheByTarget ended");
	}

	private void refreshOfferCache() {
		logger.debug("refreshOfferCache started");
		if (!cacheMapOffer.isEmpty()) {
			logger.debug("cacheMapOffer is not empty, doing refresh");
			BehaviourExecutor<RetrieveGeoOffersBehaviour, RetrieveGeoOffersRequest, RetrieveGeoOffersResponse> executor = new BehaviourExecutor<>();
			RetrieveGeoOffersResponse retrieveGeoOffersResponse;
			for(String key : cacheMapOffer.keySet()) {
				String separator = "_";
				String[] keyList = key.split(separator);
				String keyAirport = keyList[0];
				String keyLang = keyList[1];
				String keyMarket = keyList[2];
				String keyPaxType = keyList[3];
				logger.debug("Refreshing key entry: " + key);
				RetrieveGeoOffersRequest request = new RetrieveGeoOffersRequest();
				request.setDepartureAirportCode(keyAirport);
				request.setLanguageCode(keyLang);
				request.setMarketCode(keyMarket);
				if (!"".equals(keyPaxType))
					request.setPaxType(keyPaxType);
				retrieveGeoOffersResponse = executor.executeBehaviour(retrieveGeoOffersBehaviour, request);
				if (retrieveGeoOffersResponse != null) {
					cacheMapOffer.replace(key, retrieveGeoOffersResponse);
				} else {
					cacheMapOffer.remove(key);
				}
			}
		}
		logger.debug("refreshOfferCache ended");
	}
	
	private void refreshOfferCacheByTarget() {
		logger.debug("refreshOfferCacheByTarget started");
		if (!cacheMapOfferByTarget.isEmpty()) {
			logger.debug("cacheMapOfferByTarget is not empty, doing refresh");
			BehaviourExecutor<RetrieveGeoOffersByTargetBehaviour, RetrieveGeoOffersRequest, RetrieveGeoOffersResponse> executor = new BehaviourExecutor<>();
			RetrieveGeoOffersResponse retrieveGeoOffersResponse;
			for(String key : cacheMapOfferByTarget.keySet()) {
				String separator = "_";
				String[] keyList = key.split(separator);
				String keyAirport = keyList[0];
				String keyMarket = keyList[1];
				String keyTarget = keyList[2];
				String areaValue = keyList[3];
				String owrt = keyList[4];
				String keyPaxType = keyList[5];
				logger.debug("Refreshing key entry: " + key);
				RetrieveGeoOffersRequest request = new RetrieveGeoOffersRequest();
				request.setDepartureAirportCode(keyAirport);
				request.setMarketCode(keyMarket);
				request.setTarget(keyTarget);
				if (!"".equals(areaValue))
					request.setAreaValue(AreaValueEnum.fromValue(areaValue));
				if (!"".equals(owrt))
					request.setOwRt(OwRtEnum.fromValue(owrt));
				if (!"".equals(keyPaxType))
					request.setPaxType(keyPaxType);
				retrieveGeoOffersResponse = executor.executeBehaviour(retrieveGeoOffersByTargetBehaviour, request);
				if (retrieveGeoOffersResponse != null) {
					cacheMapOfferByTarget.replace(key, retrieveGeoOffersResponse);
				} else {
					cacheMapOfferByTarget.remove(key);
				}
			}
		}
		logger.debug("refreshOfferCacheByTarget ended");
	}

	@Activate
	private void activate(ComponentContext componentContext) {
		try {		
			isCacheCityEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_CACHE_CITY_ENABLED), false);
			refreshIntervalCity = PropertiesUtil.toLong(
					componentContext.getProperties().get(CACHE_CITY_REFRESH_INTERVAL), 3600000);
			isCacheAirportEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_CACHE_AIRPORT_ENABLED), false);
			refreshIntervalAirport = PropertiesUtil.toLong(
					componentContext.getProperties().get(CACHE_AIRPORT_REFRESH_INTERVAL), 3600000);
			isCacheOfferEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_CACHE_OFFER_ENABLED), false);
			refreshIntervalOffer = PropertiesUtil.toLong(
					componentContext.getProperties().get(CACHE_OFFER_REFRESH_INTERVAL), 3600000);			
		} catch (Exception e) {
			logger.error("Error to modify SimpleGeoSpatialDelegate - Load default value", e);
			isCacheCityEnabled = false;		
			isCacheAirportEnabled = false;
			isCacheOfferEnabled = false;
		}

		if (isCacheCityEnabled) {
			ScheduleOptions scheduleOptions = scheduler.NOW(-1, refreshIntervalCity/1000);
			scheduleOptions.name("refreshCityCache");
	        final Runnable job = new Runnable() {
	            public void run() {
	            	refreshCityCache();
	            	refreshCityCacheByTarget();
	            }
	        };
			try {
				this.scheduler.schedule(job, scheduleOptions);
				logger.debug("Scheduled job refreshCityCache");
			} catch (Exception e) {
				logger.warn("Couldn't scheduled job refreshCityCache, cache disabled");
				isCacheCityEnabled = false;
			}
		}

		if (isCacheAirportEnabled) {
			ScheduleOptions scheduleOptions = scheduler.NOW(-1, refreshIntervalAirport/1000);
			scheduleOptions.name("refreshAirportCache");
	        final Runnable job = new Runnable() {
	            public void run() {
	            	refreshAirportCache();
	            }
	        };
			try {
				this.scheduler.schedule(job, scheduleOptions);
				logger.debug("Scheduled job refreshAirportCache");
			} catch (Exception e) {
				logger.warn("Couldn't scheduled job refreshAirportCache, cache disabled");
				isCacheAirportEnabled = false;
			}
		}

		if (isCacheOfferEnabled) {
			ScheduleOptions scheduleOptions = scheduler.NOW(-1, refreshIntervalOffer/1000);
			scheduleOptions.name("refreshOfferCache");
	        final Runnable job = new Runnable() {
	            public void run() {
	            	refreshOfferCache();
	            	refreshOfferCacheByTarget();
	            }
	        };
			try {
				this.scheduler.schedule(job, scheduleOptions);
				logger.debug("Scheduled job refreshOfferCache");
			} catch (Exception e) {
				logger.warn("Couldn't scheduled job refreshOfferCache, cache disabled");
				isCacheOfferEnabled = false;
			}
		}

		logger.info("Activated SimpleGeoSpatialDelegate: "
				+ "IS_CACHE_CITY_ENABLED=[" + isCacheCityEnabled + "], "
				+ "CACHE_CITY_REFRESH_INTERVAL=[" + refreshIntervalCity + "], "
				+ "IS_CACHE_AIRPORT_ENABLED=[" + isCacheAirportEnabled + "], "
				+ "CACHE_AIRPORT_REFRESH_INTERVAL=[" + refreshIntervalAirport + "], "
				+ "IS_CACHE_OFFER_ENABLED=[" + isCacheOfferEnabled + "], "
				+ "CACHE_OFFER_REFRESH_INTERVAL=[" + refreshIntervalOffer + "], ");
	}

	@Modified
	private void modified(ComponentContext componentContext) {
		try {
			isCacheCityEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_CACHE_CITY_ENABLED), false);
			refreshIntervalCity = PropertiesUtil.toLong(
					componentContext.getProperties().get(CACHE_CITY_REFRESH_INTERVAL), 3600000);
			isCacheAirportEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_CACHE_AIRPORT_ENABLED), false);
			refreshIntervalAirport = PropertiesUtil.toLong(
					componentContext.getProperties().get(CACHE_AIRPORT_REFRESH_INTERVAL), 3600000);
			isCacheOfferEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_CACHE_OFFER_ENABLED), false);
			refreshIntervalOffer = PropertiesUtil.toLong(
					componentContext.getProperties().get(CACHE_OFFER_REFRESH_INTERVAL), 3600000);
		} catch (Exception e) {
			logger.error("Error to modify SimpleGeoSpatialDelegate - Load default value", e);
			isCacheCityEnabled = false;		
			isCacheAirportEnabled = false;	
			isCacheOfferEnabled = false;
		}

		if (isCacheCityEnabled) {
			ScheduleOptions scheduleOptions = scheduler.NOW(-1, refreshIntervalCity/1000);
			scheduleOptions.name("refreshCityCache");
	        final Runnable job = new Runnable() {
	            public void run() {
	            	refreshCityCache();
	            	refreshCityCacheByTarget();
	            }
	        };
			try {
				this.scheduler.schedule(job, scheduleOptions);
				logger.debug("Scheduled job refreshCityCache");
			} catch (Exception e) {
				logger.warn("Couldn't scheduled job refreshCityCache, cache disabled");
				isCacheCityEnabled = false;
			}
		}
		else
		{
			this.scheduler.unschedule("refreshCityCache");
			cacheMapCity.clear();
			cacheMapCityByTarget.clear();
		}

		if (isCacheAirportEnabled) {
			ScheduleOptions scheduleOptions = scheduler.NOW(-1, refreshIntervalAirport/1000);
			scheduleOptions.name("refreshAirportCache");
	        final Runnable job = new Runnable() {
	            public void run() {
	            	refreshAirportCache();
	            }
	        };
			try {
				this.scheduler.schedule(job, scheduleOptions);
				logger.debug("Scheduled job refreshAirportCache");
			} catch (Exception e) {
				logger.warn("Couldn't scheduled job refreshAirportCache, cache disabled");
				isCacheAirportEnabled = false;
			}
		}
		else
		{
			this.scheduler.unschedule("refreshAirportCache");
			cacheMapAirport.clear();
		}

		if (isCacheOfferEnabled) {
			ScheduleOptions scheduleOptions = scheduler.NOW(-1, refreshIntervalOffer/1000);
			scheduleOptions.name("refreshOfferCache");
	        final Runnable job = new Runnable() {
	            public void run() {
	            	refreshOfferCache();
	            	refreshOfferCacheByTarget();
	            }
	        };
			try {
				this.scheduler.schedule(job, scheduleOptions);
				logger.debug("Scheduled job refreshOfferCache");
			} catch (Exception e) {
				logger.warn("Couldn't scheduled job refreshOfferCache, cache disabled");
				isCacheOfferEnabled = false;
			}
		}
		else
		{
			this.scheduler.unschedule("refreshOfferCache");
			cacheMapOffer.clear();
			cacheMapOfferByTarget.clear();
		}

		logger.info("Modified SimpleGeoSpatialDelegate: "
				+ "IS_CACHE_CITY_ENABLED=[" + isCacheCityEnabled + "], "
				+ "CACHE_CITY_REFRESH_INTERVAL=[" + refreshIntervalCity + "], "
				+ "IS_CACHE_AIRPORT_ENABLED=[" + isCacheAirportEnabled + "], "
				+ "CACHE_AIRPORT_REFRESH_INTERVAL=[" + refreshIntervalAirport + "], "
				+ "IS_CACHE_OFFER_ENABLED=[" + isCacheOfferEnabled + "], "
				+ "CACHE_OFFER_REFRESH_INTERVAL=[" + refreshIntervalOffer + "], ");
	}
	
}