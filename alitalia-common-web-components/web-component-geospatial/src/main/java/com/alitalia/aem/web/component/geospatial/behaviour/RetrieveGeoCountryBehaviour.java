package com.alitalia.aem.web.component.geospatial.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveGeoCountryRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoCountryResponse;
import com.alitalia.aem.service.api.home.GeoSpatialService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveGeoCountryBehaviour.class)
public class RetrieveGeoCountryBehaviour extends Behaviour<RetrieveGeoCountryRequest, RetrieveGeoCountryResponse> {

	@Reference
	private GeoSpatialService geoSpatialService;
	
	@Override
	public RetrieveGeoCountryResponse executeOrchestration(RetrieveGeoCountryRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Geo Country request is null.");
		return geoSpatialService.retrieveGeoCountry(request);
	}	
}