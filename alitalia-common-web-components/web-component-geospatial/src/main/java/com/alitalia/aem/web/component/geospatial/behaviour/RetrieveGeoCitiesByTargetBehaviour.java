package com.alitalia.aem.web.component.geospatial.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveGeoCitiesRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoCitiesResponse;
import com.alitalia.aem.service.api.home.GeoSpatialService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveGeoCitiesByTargetBehaviour.class)
public class RetrieveGeoCitiesByTargetBehaviour extends Behaviour<RetrieveGeoCitiesRequest, RetrieveGeoCitiesResponse> {

	@Reference
	private GeoSpatialService geoSpatialService;
	
	@Override
	public RetrieveGeoCitiesResponse executeOrchestration(RetrieveGeoCitiesRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Geo Cities request is null.");
		return geoSpatialService.retrieveGeoCitiesByTarget(request);
	}	
}