package com.alitalia.aem.web.component.bookingticket.delegate;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.messages.home.CarnetSendNotificationEmailRequest;
import com.alitalia.aem.common.messages.home.CarnetSendNotificationEmailResponse;
import com.alitalia.aem.common.messages.home.CrossSellingsRequest;
import com.alitalia.aem.common.messages.home.CrossSellingsResponse;
import com.alitalia.aem.web.component.bookingticket.behaviour.CarnetSendNotificationEmailBehaviour;
import com.alitalia.aem.web.component.bookingticket.behaviour.CrossSellingsBehaviour;

@Component(immediate = true, metatype = false)
@Service(value = BookingTicketDelegate.class)
public class SimpleBookingTicketDelegate implements BookingTicketDelegate {

	@Reference
	private CrossSellingsBehaviour crossSellingsBehaviour;
	
	@Reference
	private CarnetSendNotificationEmailBehaviour carnetSendNotificationEmailBehaviour;
	
	@Override
	public CrossSellingsResponse getCrossSellings(CrossSellingsRequest request) {
		BehaviourExecutor<CrossSellingsBehaviour, CrossSellingsRequest, CrossSellingsResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(crossSellingsBehaviour, request);
	}
	
	@Override
	public CarnetSendNotificationEmailResponse sendCarnetNotificationEmail(CarnetSendNotificationEmailRequest request) {
		BehaviourExecutor<CarnetSendNotificationEmailBehaviour, CarnetSendNotificationEmailRequest, CarnetSendNotificationEmailResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(carnetSendNotificationEmailBehaviour, request);
	}
}