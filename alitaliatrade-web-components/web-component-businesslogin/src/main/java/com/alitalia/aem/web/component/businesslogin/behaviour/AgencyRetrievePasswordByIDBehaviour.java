package com.alitalia.aem.web.component.businesslogin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.AgencyRetrievePasswordByIDRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrievePasswordResponse;
import com.alitalia.aem.service.api.home.BusinessLoginService;

@Component(immediate = true, metatype = false)
@Service(value = AgencyRetrievePasswordByIDBehaviour.class)
public class AgencyRetrievePasswordByIDBehaviour extends
				Behaviour<AgencyRetrievePasswordByIDRequest, AgencyRetrievePasswordResponse> {

	@Reference
	BusinessLoginService businessLoginService;

	@Override
	public AgencyRetrievePasswordResponse executeOrchestration(AgencyRetrievePasswordByIDRequest request) {
		if(request == null) throw new IllegalArgumentException("Agency Retrive Password request is null.");
		return businessLoginService.retrievePasswordByID(request);
	}

}
