package com.alitalia.aem.web.component.businesslogin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.AgencyRetrieveDataRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrieveDataResponse;
import com.alitalia.aem.service.api.home.BusinessLoginService;

@Component(immediate = true, metatype = false)
@Service(value = AgencyRetrieveDataBehaviour.class)
public class AgencyRetrieveDataBehaviour extends Behaviour<AgencyRetrieveDataRequest, AgencyRetrieveDataResponse> {

	@Reference
	private BusinessLoginService businessLoginService;

	@Override
	public AgencyRetrieveDataResponse executeOrchestration(AgencyRetrieveDataRequest request) {
		if(request == null)
			throw new IllegalArgumentException("Agency retrieve data request is null.");

		AgencyRetrieveDataResponse response = businessLoginService.retrieveAgencyData(request);
		
		return response;
	}

}
