package com.alitalia.aem.web.component.businesslogin.delegate;

import com.alitalia.aem.common.messages.home.AgencyInsertConsentRequest;
import com.alitalia.aem.common.messages.home.AgencyInsertConsentResponse;
import com.alitalia.aem.common.messages.home.AgencyLoginRequest;
import com.alitalia.aem.common.messages.home.AgencyLoginResponse;
import com.alitalia.aem.common.messages.home.AgencyPasswordChangeRequest;
import com.alitalia.aem.common.messages.home.AgencyPasswordChangeResponse;
import com.alitalia.aem.common.messages.home.AgencyRetrieveDataRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrieveDataResponse;
import com.alitalia.aem.common.messages.home.AgencyRetrievePasswordByIDRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrievePasswordByVATRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrievePasswordRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrievePasswordResponse;
import com.alitalia.aem.common.messages.home.AgencySubscriptionRequest;
import com.alitalia.aem.common.messages.home.AgencySubscriptionResponse;
import com.alitalia.aem.common.messages.home.AgencyUnsubscribeNewsLetterIataRequest;
import com.alitalia.aem.common.messages.home.AgencyUnsubscribeNewsLetterIataResponse;
import com.alitalia.aem.common.messages.home.AgencyUpdateDataRequest;
import com.alitalia.aem.common.messages.home.AgencyUpdateDataResponse;

public interface BusinessLoginDelegate {

	AgencyLoginResponse agencyLogin(AgencyLoginRequest request);
	AgencyPasswordChangeResponse changePassword(AgencyPasswordChangeRequest request);
	
	/**
	 * Effettua l'operazione di recupero password vericando la lunghezza dell'identificativo
	 * passato ed effettuando la chiamata all'operazione relativa.
	 * Se l'identificativo è lungo 11 caratteri si assume che sia una Partita IVA e viene
	 * chiamata l'operation per il recupero ByVAT, altrimenti ByID.
	 * In caso di esito positivo viene inviata la password via email all'indirizzo dell'utente
	 * richiedente
	 * @param request
	 * @return AgencyRetrievePasswordResponse
	 */
	AgencyRetrievePasswordResponse retrievePassword(AgencyRetrievePasswordRequest request);
	
	/**
	 * @deprecated
	 * Effettua l'operazione di recupero password tramite l'ID dell'agenzia. 
	 * In caso di esito positivo viene inviata la password via email all'indirizzo dell'utente
	 * richiedente
	 * @param request
	 * @return AgencyRetrievePasswordResponse
	 */
	AgencyRetrievePasswordResponse retrievePasswordByID(AgencyRetrievePasswordByIDRequest request);

	/**
	 * @deprecated
	 * Effettua l'operazione di recupero password tramite la Partita IVA dell'agenzia. 
	 * In caso di esito positivo viene inviata la password via email all'indirizzo dell'utente
	 * richiedente
	 * @param request
	 * @return
	 */
	AgencyRetrievePasswordResponse retrievePasswordByVAT(AgencyRetrievePasswordByVATRequest request);

	AgencyRetrieveDataResponse retrieveAgencyData(AgencyRetrieveDataRequest request);
	AgencyUpdateDataResponse updateAgencyData(AgencyUpdateDataRequest request);
	AgencySubscriptionResponse subscribeAgency(AgencySubscriptionRequest request);
	AgencyInsertConsentResponse insertConsent(AgencyInsertConsentRequest request);
	AgencyUnsubscribeNewsLetterIataResponse unsubscribeNewsLetterIata(AgencyUnsubscribeNewsLetterIataRequest request);
}