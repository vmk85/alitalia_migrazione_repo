package com.alitalia.aem.web.component.businesslogin.delegate;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.messages.home.AgencyInsertConsentRequest;
import com.alitalia.aem.common.messages.home.AgencyInsertConsentResponse;
import com.alitalia.aem.common.messages.home.AgencyLoginRequest;
import com.alitalia.aem.common.messages.home.AgencyLoginResponse;
import com.alitalia.aem.common.messages.home.AgencyPasswordChangeRequest;
import com.alitalia.aem.common.messages.home.AgencyPasswordChangeResponse;
import com.alitalia.aem.common.messages.home.AgencyRetrieveDataRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrieveDataResponse;
import com.alitalia.aem.common.messages.home.AgencyRetrievePasswordByIDRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrievePasswordByVATRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrievePasswordRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrievePasswordResponse;
import com.alitalia.aem.common.messages.home.AgencySubscriptionRequest;
import com.alitalia.aem.common.messages.home.AgencySubscriptionResponse;
import com.alitalia.aem.common.messages.home.AgencyUnsubscribeNewsLetterIataRequest;
import com.alitalia.aem.common.messages.home.AgencyUnsubscribeNewsLetterIataResponse;
import com.alitalia.aem.common.messages.home.AgencyUpdateDataRequest;
import com.alitalia.aem.common.messages.home.AgencyUpdateDataResponse;
import com.alitalia.aem.web.component.businesslogin.behaviour.AgencyChangePasswordBehaviour;
import com.alitalia.aem.web.component.businesslogin.behaviour.AgencyInsertConsentBehaviour;
import com.alitalia.aem.web.component.businesslogin.behaviour.AgencyLoginBehaviour;
import com.alitalia.aem.web.component.businesslogin.behaviour.AgencyRetrieveDataBehaviour;
import com.alitalia.aem.web.component.businesslogin.behaviour.AgencyRetrievePasswordBehaviour;
import com.alitalia.aem.web.component.businesslogin.behaviour.AgencyRetrievePasswordByIDBehaviour;
import com.alitalia.aem.web.component.businesslogin.behaviour.AgencyRetrievePasswordByVATBehaviour;
import com.alitalia.aem.web.component.businesslogin.behaviour.AgencySubscriptionBehaviour;
import com.alitalia.aem.web.component.businesslogin.behaviour.AgencyUnsubscribeNewsLetterIataBehaviour;
import com.alitalia.aem.web.component.businesslogin.behaviour.AgencyUpdateDataBehaviour;

@Service
@Component(immediate = true, metatype = false)
public class SimpleBusinessLoginDelegate implements BusinessLoginDelegate {
	
	private static final String DEFINED_CHARACHTER_SUBSTITUTE = ".";

	@Reference
	private AgencyLoginBehaviour agencyLoginBehaviour;

	@Reference
	private AgencyChangePasswordBehaviour agencyChangePasswordBehaviour;

	@Reference
	private AgencyRetrievePasswordByIDBehaviour retrievePasswordByIDBehaviour;

	@Reference
	private AgencyRetrievePasswordByVATBehaviour retrievePasswordByVATBehaviour;
	
	@Reference
	private AgencyRetrievePasswordBehaviour retrievePasswordBehaviour;
	
	@Reference
	private AgencyRetrieveDataBehaviour agencyRetrieveDataBehavior;
	
	@Reference
	private AgencyUpdateDataBehaviour agencyUpdateDataBehavior;
	
	@Reference
	private AgencySubscriptionBehaviour agencySubscriptionBehavior;
	
	@Reference
	private AgencyInsertConsentBehaviour agencyInsertConsentBehavior;
	
	@Reference
	private AgencyUnsubscribeNewsLetterIataBehaviour agencyUnsubscribeNewsLetterIataBehaviour;
	
	@Override
	public AgencyLoginResponse agencyLogin(AgencyLoginRequest request) {
		BehaviourExecutor<AgencyLoginBehaviour, AgencyLoginRequest, AgencyLoginResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(agencyLoginBehaviour, request);
	}

	@Override
	public AgencyPasswordChangeResponse changePassword(AgencyPasswordChangeRequest request) {
		BehaviourExecutor<AgencyChangePasswordBehaviour, AgencyPasswordChangeRequest, AgencyPasswordChangeResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(agencyChangePasswordBehaviour, request);
	}

	@Override
	public AgencyRetrievePasswordResponse retrievePassword(AgencyRetrievePasswordRequest request) {
		BehaviourExecutor<AgencyRetrievePasswordBehaviour, AgencyRetrievePasswordRequest, AgencyRetrievePasswordResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(retrievePasswordBehaviour, request);
	}

	@Override
	@Deprecated
	public AgencyRetrievePasswordResponse retrievePasswordByID(AgencyRetrievePasswordByIDRequest request) {
		BehaviourExecutor<AgencyRetrievePasswordByIDBehaviour, AgencyRetrievePasswordByIDRequest, AgencyRetrievePasswordResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(retrievePasswordByIDBehaviour, request);
	}

	@Override
	@Deprecated
	public AgencyRetrievePasswordResponse retrievePasswordByVAT(AgencyRetrievePasswordByVATRequest request) {
		BehaviourExecutor<AgencyRetrievePasswordByVATBehaviour, AgencyRetrievePasswordByVATRequest, AgencyRetrievePasswordResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(retrievePasswordByVATBehaviour, request);
	}
	
	@Override
	public AgencyRetrieveDataResponse retrieveAgencyData(AgencyRetrieveDataRequest request) {
		BehaviourExecutor<AgencyRetrieveDataBehaviour, AgencyRetrieveDataRequest, AgencyRetrieveDataResponse> executor = new BehaviourExecutor<>();
		AgencyRetrieveDataResponse response = executor.executeBehaviour(agencyRetrieveDataBehavior, request);
		//Implementing strategy for not required fax 
		String convertedFax = convertDotToEmptyFax(response.getFax());
		response.setFax(convertedFax);
		return response;
	}
	
	@Override
	public AgencyUpdateDataResponse updateAgencyData(AgencyUpdateDataRequest request) {
		BehaviourExecutor<AgencyUpdateDataBehaviour, AgencyUpdateDataRequest, AgencyUpdateDataResponse> executor = new BehaviourExecutor<>();
		//Implementing the not requiring fax strategy
		String convertedFax = convertEmptyFaxToDot(request.getFax());
		request.setFax(convertedFax);
		return executor.executeBehaviour(agencyUpdateDataBehavior, request);
	}

	@Override
	public AgencySubscriptionResponse subscribeAgency(AgencySubscriptionRequest request) {
		BehaviourExecutor<AgencySubscriptionBehaviour, AgencySubscriptionRequest, AgencySubscriptionResponse> executor = new BehaviourExecutor<>();
		//Implementing strategy for not required fax 
		String convertedFax = convertEmptyFaxToDot(request.getNumeroFax());
		request.setNumeroFax(convertedFax);
		return executor.executeBehaviour(agencySubscriptionBehavior, request);
	}

	@Override
	public AgencyInsertConsentResponse insertConsent(AgencyInsertConsentRequest request) {
		BehaviourExecutor<AgencyInsertConsentBehaviour, AgencyInsertConsentRequest, AgencyInsertConsentResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(agencyInsertConsentBehavior, request);
	}
	
	@Override
	public AgencyUnsubscribeNewsLetterIataResponse unsubscribeNewsLetterIata(AgencyUnsubscribeNewsLetterIataRequest request) {		
		BehaviourExecutor<AgencyUnsubscribeNewsLetterIataBehaviour, AgencyUnsubscribeNewsLetterIataRequest, AgencyUnsubscribeNewsLetterIataResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(agencyUnsubscribeNewsLetterIataBehaviour, request);
	}
	
	/**
	 * This method is used to convert an empty fax in a dot,
	 * because the field fax is required by Alitalia service.
	 * The user introduced a new requirement that specify the fax as 
	 * not required. According to Alitalia User, we convert an empty fax in a dot.
	 * Alitalia will store the dot in their DB. Therfore a dot will expected 
	 * by retriving the agency information 
	 * @param fax
	 * @return a dot if the param fax is empty or null
	 */
	private String convertEmptyFaxToDot(String fax) {
		if (fax==null) {
			return DEFINED_CHARACHTER_SUBSTITUTE;
		}
		if (("").equals(fax)) {
			return DEFINED_CHARACHTER_SUBSTITUTE;
		}
		return fax;

	}
	
	/**
	 * This method is used to convert a dot in an empty fax,
	 * because the field fax is required by Alitalia service.
	 * The user introduced a new requirement that specify the fax as 
	 * not required. According to Alitalia User, that srore a dot in their DB,
	 * we convert a dot in an empty fax.
	 * @param fax
	 * @return empty fax if the param fax is a dot
	 */
	private String convertDotToEmptyFax(String fax) {
		fax = fax.trim();
		if (fax.equals(DEFINED_CHARACHTER_SUBSTITUTE)) {
			return "";
		}
		return fax;

	}
}