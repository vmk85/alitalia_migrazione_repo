package com.alitalia.aem.web.component.businesslogin.behaviour;

import java.util.Map;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.data.home.EmailMessage;
import com.alitalia.aem.common.data.home.enumerations.AlitaliaTradeUserType;
import com.alitalia.aem.common.data.home.enumerations.DeliveryNotificationOptionsEnum;
import com.alitalia.aem.common.data.home.enumerations.MailPriorityEnum;
import com.alitalia.aem.common.messages.home.AgencyRetrievePasswordByIDRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrievePasswordByVATRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrievePasswordRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrievePasswordResponse;
import com.alitalia.aem.common.messages.home.AgencySendMailRequest;
import com.alitalia.aem.common.messages.home.AgencySendMailResponse;
import com.alitalia.aem.service.api.home.BusinessLoginService;
import com.alitalia.aem.service.api.home.exc.businessloginservice.RetrievePasswordByIDException;
import com.alitalia.aem.service.api.home.exc.businessloginservice.RetrievePasswordByVATException;

@Component(immediate = true, metatype = false)
@Service(value = AgencyRetrievePasswordBehaviour.class)
public class AgencyRetrievePasswordBehaviour extends Behaviour<AgencyRetrievePasswordRequest, AgencyRetrievePasswordResponse> {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private String passwordEmailSender;
	private String passwordEmailSubject;
	private String passwordEmailTextTemplate;
	private String passwordPlaceholder;
	private Boolean htmlText;

	@Property(description="Email sender for forgotten password.")
	private static final String EMAIL_SENDER_PROPERTY = "email.sender.property";
	@Property(description="Email subject for forgotten password.")
	private static final String EMAIL_SUBJECT_PROPERTY = "email.subject.property";
	@Property(description="Email text template for forgotten password.")
	private static final String EMAIL_TEXT_PROPERTY = "email.text.property";
	@Property(description="Password placeholder in text template for forgotten password.")
	private static final String EMAIL_PASSWORD_PLACEHOLDER_PROPERTY = "email.password.placeholder.property";
	@Property(description="Flag to specify if email text for forgotten password is in HTML format.")
	private static final String EMAIL_HTML_FLAG_PROPERTY = "email.html.flag.property";
	
	@Reference
	BusinessLoginService businessLoginService;

	public AgencyRetrievePasswordBehaviour() {
		super();
	}

	@Override
	public AgencyRetrievePasswordResponse executeOrchestration(AgencyRetrievePasswordRequest request) {
		if(request == null) throw new IllegalArgumentException("Agency Retrive Password request is null.");

		AgencyRetrievePasswordResponse response = null;
		AlitaliaTradeUserType ruolo = request.getRuolo();
		String sid = request.getSid();
		String tid = request.getTid();
		String identificativo = request.getIdentificativo();

		if ((identificativo != null) && (!"".equals(identificativo)) && identificativo.length() < 11) {
			AgencyRetrievePasswordByIDRequest passwordByIDRequest = 
					new AgencyRetrievePasswordByIDRequest(tid, sid, identificativo, ruolo);
			response = businessLoginService.retrievePasswordByID(passwordByIDRequest);
			if (!response.isRetrieveSuccessful()) {
				throw new RetrievePasswordByIDException("RetrievePasswordByID not success: Sid ["+request.getSid()+"]");
			}
		} else {
			AgencyRetrievePasswordByVATRequest passwordByVATRequest = 
					new AgencyRetrievePasswordByVATRequest(tid, sid, identificativo, ruolo);
			response = businessLoginService.retrievePasswordByVAT(passwordByVATRequest);
			if (!response.isRetrieveSuccessful()) {
				throw new RetrievePasswordByVATException("RetrievePasswordByVAT not success: Sid ["+request.getSid()+"]");
			}
		}

		if (response.isRetrieveSuccessful()) {
			EmailMessage emailMessage = new EmailMessage();
			AgencySendMailRequest sendMailRequest = new AgencySendMailRequest();
			String password = response.getAgencyData().getPassword();
			String passwordEmailRecipient = response.getAgencyData().getEmail();
			String passwordEmailText = this.passwordEmailTextTemplate.replace(this.passwordPlaceholder, password);

			emailMessage.setFrom(this.passwordEmailSender);
			emailMessage.setTo(passwordEmailRecipient);
			emailMessage.setSubject(this.passwordEmailSubject);
			emailMessage.setDeliveryNotification(DeliveryNotificationOptionsEnum.NONE);
			emailMessage.setPriority(MailPriorityEnum.NORMAL);
			emailMessage.setIsBodyHtml(this.htmlText);
			emailMessage.setMessageText(passwordEmailText);

			sendMailRequest.setTid(request.getTid());
			sendMailRequest.setSid(request.getSid());
			sendMailRequest.setEmailMessage(emailMessage);
			AgencySendMailResponse sendMailResponse = businessLoginService.sendMail(sendMailRequest);
			if (!sendMailResponse.isSendMailSuccessful()) {
				response.setAgencyData(null);
				response.setAccountLocked(false);
				response.setPasswordExpired(false);
				response.setRetrieveSuccessful(false);
			}
		}

		return response;
	}

	@Activate
	protected void activate(final Map<String, Object> config) {
		logger.info("Activate EMAIL_HTML_FLAG_PROPERTY = {}", config.get(EMAIL_HTML_FLAG_PROPERTY));
		this.htmlText = Boolean.valueOf(String.valueOf(config.get(EMAIL_HTML_FLAG_PROPERTY)));

		logger.info("Activate EMAIL_PASSWORD_PLACEHOLDER_PROPERTY = {}", config.get(EMAIL_PASSWORD_PLACEHOLDER_PROPERTY));
		this.passwordPlaceholder = String.valueOf(config.get(EMAIL_PASSWORD_PLACEHOLDER_PROPERTY));

		logger.info("Activate EMAIL_SENDER_PROPERTY = {}", config.get(EMAIL_SENDER_PROPERTY));
		this.passwordEmailSender = String.valueOf(config.get(EMAIL_SENDER_PROPERTY));

		logger.info("Activate EMAIL_SUBJECT_PROPERTY = {}", config.get(EMAIL_SUBJECT_PROPERTY));
		this.passwordEmailSubject = String.valueOf(config.get(EMAIL_SUBJECT_PROPERTY));

		logger.info("Activate EMAIL_TEXT_PROPERTY = {}", config.get(EMAIL_TEXT_PROPERTY));
		this.passwordEmailTextTemplate = String.valueOf(config.get(EMAIL_TEXT_PROPERTY));
	}
	
	@Modified
	protected void modified(final Map<String, Object> config) {
		logger.info("Modified EMAIL_HTML_FLAG_PROPERTY = {}", config.get(EMAIL_HTML_FLAG_PROPERTY));
		this.htmlText = Boolean.valueOf(String.valueOf(config.get(EMAIL_HTML_FLAG_PROPERTY)));

		logger.info("Modified EMAIL_PASSWORD_PLACEHOLDER_PROPERTY = {}", config.get(EMAIL_PASSWORD_PLACEHOLDER_PROPERTY));
		this.passwordPlaceholder = String.valueOf(config.get(EMAIL_PASSWORD_PLACEHOLDER_PROPERTY));

		logger.info("Modified EMAIL_SENDER_PROPERTY = {}", config.get(EMAIL_SENDER_PROPERTY));
		this.passwordEmailSender = String.valueOf(config.get(EMAIL_SENDER_PROPERTY));

		logger.info("Modified EMAIL_SUBJECT_PROPERTY = {}", config.get(EMAIL_SUBJECT_PROPERTY));
		this.passwordEmailSubject = String.valueOf(config.get(EMAIL_SUBJECT_PROPERTY));

		logger.info("Modified EMAIL_TEXT_PROPERTY = {}", config.get(EMAIL_TEXT_PROPERTY));
		this.passwordEmailTextTemplate = String.valueOf(config.get(EMAIL_TEXT_PROPERTY));
	}

}
