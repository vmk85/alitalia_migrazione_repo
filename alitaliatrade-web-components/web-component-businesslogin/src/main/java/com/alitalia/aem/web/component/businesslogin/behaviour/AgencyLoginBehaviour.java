package com.alitalia.aem.web.component.businesslogin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.AgencyLoginRequest;
import com.alitalia.aem.common.messages.home.AgencyLoginResponse;
import com.alitalia.aem.common.messages.home.AgencyRetrieveIataGroupsRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrieveIataGroupsResponse;
import com.alitalia.aem.service.api.home.BusinessLoginService;
import com.alitalia.aem.service.api.home.WebAgencyService;
import com.alitalia.aem.ws.common.exception.WSClientException;

@Component(immediate = true, metatype = false)
@Service(value = AgencyLoginBehaviour.class)
public class AgencyLoginBehaviour extends Behaviour<AgencyLoginRequest, AgencyLoginResponse> {

	@Reference
	private BusinessLoginService businessLoginService;

	@Reference
	private WebAgencyService webAgencyService;

	@Override
	public AgencyLoginResponse executeOrchestration(AgencyLoginRequest request) {
		if(request == null)
			throw new IllegalArgumentException("Agency Login request is null.");

		AgencyLoginResponse loginResponse = businessLoginService.agencyLogin(request);

		if (loginResponse.isLoginSuccessful()) {
			
			boolean isIata;
			boolean isGroupEnabled;
			
			String codFamCon = loginResponse.getAgencyData().getCODFAMCON();
			if ("I4".equals(codFamCon)) {
				isIata = true;
			} else if ("N4".equals(codFamCon)) {
				isIata = false;
			} else {
				throw new WSClientException("Unrecognized CODFAMCON in agency data: " + codFamCon);
			}
			
			if (isIata) {
				
				AgencyRetrieveIataGroupsRequest retrieveInfoIataRequest = new AgencyRetrieveIataGroupsRequest(
						request.getTid(), request.getSid(), request.getCodiceAgenzia());
				AgencyRetrieveIataGroupsResponse retrieveInfoIataResponse = 
						webAgencyService.retrieveIataAgencyInfo(retrieveInfoIataRequest);
				if (!retrieveInfoIataResponse.isValidResponse()) {
					throw new WSClientException("Invalid response found retrieving agency IATA info.");
				}
				isGroupEnabled = retrieveInfoIataResponse.isGroupsEnabled();
			
			} else {
				
				AgencyRetrieveIataGroupsRequest retrieveInfoNonIataRequest = new AgencyRetrieveIataGroupsRequest(
						request.getTid(), request.getSid(), request.getCodiceAgenzia());
				AgencyRetrieveIataGroupsResponse retrieveInfoNonIataResponse = 
						webAgencyService.retrieveNonIataAgencyInfo(retrieveInfoNonIataRequest);
				if (!retrieveInfoNonIataResponse.isValidResponse()) {
					throw new WSClientException("Invalid response found retrieving agency non-IATA info.");
				}
				isGroupEnabled = retrieveInfoNonIataResponse.isGroupsEnabled();
				
			}
			
			loginResponse.getAgencyData().setIataAgency(isIata);
			loginResponse.getAgencyData().setGroupEnabled(isGroupEnabled);			
		}
		
		return loginResponse;
	}

}
