package com.alitalia.aem.web.component.businesslogin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.AgencyPasswordChangeRequest;
import com.alitalia.aem.common.messages.home.AgencyPasswordChangeResponse;
import com.alitalia.aem.service.api.home.BusinessLoginService;

@Component(immediate = true, metatype = false)
@Service(value = AgencyChangePasswordBehaviour.class)
public class AgencyChangePasswordBehaviour extends Behaviour<AgencyPasswordChangeRequest, AgencyPasswordChangeResponse> {

	@Reference
	BusinessLoginService businessLoginService;

	@Override
	public AgencyPasswordChangeResponse executeOrchestration(AgencyPasswordChangeRequest request) {
		if(request == null) throw new IllegalArgumentException("Agency Login request is null.");
		return businessLoginService.changePassword(request);
	}

}
