package com.alitalia.aem.web.component.businesslogin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.AgencySubscriptionRequest;
import com.alitalia.aem.common.messages.home.AgencySubscriptionResponse;
import com.alitalia.aem.service.api.home.BusinessLoginService;

@Component(immediate = true, metatype = false)
@Service(value = AgencySubscriptionBehaviour.class)
public class AgencySubscriptionBehaviour extends Behaviour<AgencySubscriptionRequest, AgencySubscriptionResponse> {

	@Reference
	BusinessLoginService businessLoginService;

	@Override
	public AgencySubscriptionResponse executeOrchestration(AgencySubscriptionRequest request) {
		if(request == null) throw new IllegalArgumentException("Agency Subscription request is null.");
		return businessLoginService.subscribeAgency(request);
	}
}