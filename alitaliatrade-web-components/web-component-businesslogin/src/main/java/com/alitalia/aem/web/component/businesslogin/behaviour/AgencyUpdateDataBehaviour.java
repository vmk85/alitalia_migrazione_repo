package com.alitalia.aem.web.component.businesslogin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.AgencyUpdateDataRequest;
import com.alitalia.aem.common.messages.home.AgencyUpdateDataResponse;
import com.alitalia.aem.service.api.home.BusinessLoginService;

@Component(immediate = true, metatype = false)
@Service(value = AgencyUpdateDataBehaviour.class)
public class AgencyUpdateDataBehaviour extends Behaviour<AgencyUpdateDataRequest, AgencyUpdateDataResponse> {

	@Reference
	private BusinessLoginService businessLoginService;

	@Override
	public AgencyUpdateDataResponse executeOrchestration(AgencyUpdateDataRequest request) {
		if(request == null)
			throw new IllegalArgumentException("Agency update data request is null.");

		AgencyUpdateDataResponse response = businessLoginService.updateAgencyData(request);
		
		return response;
	}

}
