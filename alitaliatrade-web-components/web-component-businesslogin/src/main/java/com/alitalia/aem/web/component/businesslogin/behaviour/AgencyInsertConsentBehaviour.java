package com.alitalia.aem.web.component.businesslogin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.AgencyInsertConsentRequest;
import com.alitalia.aem.common.messages.home.AgencyInsertConsentResponse;
import com.alitalia.aem.service.api.home.BusinessLoginService;

@Component(immediate = true, metatype = false)
@Service(value = AgencyInsertConsentBehaviour.class)
public class AgencyInsertConsentBehaviour extends Behaviour<AgencyInsertConsentRequest, AgencyInsertConsentResponse> {

	@Reference
	private BusinessLoginService businessLoginService;

	@Override
	public AgencyInsertConsentResponse executeOrchestration(AgencyInsertConsentRequest request) {
		if(request == null) throw new IllegalArgumentException("Agency insert consent request is null.");
		return businessLoginService.insertConsent(request);
	}
}