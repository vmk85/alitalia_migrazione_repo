package com.alitalia.aem.web.component.businesslogin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.AgencyUnsubscribeNewsLetterIataRequest;
import com.alitalia.aem.common.messages.home.AgencyUnsubscribeNewsLetterIataResponse;
import com.alitalia.aem.service.api.home.BusinessLoginService;

@Component(immediate = true, metatype = false)
@Service(value = AgencyUnsubscribeNewsLetterIataBehaviour.class)
public class AgencyUnsubscribeNewsLetterIataBehaviour extends Behaviour<AgencyUnsubscribeNewsLetterIataRequest, AgencyUnsubscribeNewsLetterIataResponse> {

	@Reference
	private BusinessLoginService businessLoginService;

	@Override
	public AgencyUnsubscribeNewsLetterIataResponse executeOrchestration(AgencyUnsubscribeNewsLetterIataRequest request) {
		if(request == null) throw new IllegalArgumentException("AgencyUnsubscribeNewsLetterIataRequest request is null.");
		return businessLoginService.unsubscribeNewsletterForeignAgency(request);
	}
}