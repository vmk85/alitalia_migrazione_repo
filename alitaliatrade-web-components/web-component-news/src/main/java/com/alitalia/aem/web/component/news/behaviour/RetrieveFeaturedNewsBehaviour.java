package com.alitalia.aem.web.component.news.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveFeaturedNewsRequest;
import com.alitalia.aem.common.messages.home.RetrieveFeaturedNewsResponse;
import com.alitalia.aem.service.api.home.RetrieveNewsService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveFeaturedNewsBehaviour.class)
public class RetrieveFeaturedNewsBehaviour extends Behaviour<RetrieveFeaturedNewsRequest, RetrieveFeaturedNewsResponse> {

	@Reference
	private RetrieveNewsService retrieveNewsService;
	
	@Override
	public RetrieveFeaturedNewsResponse executeOrchestration(RetrieveFeaturedNewsRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve Featured News request is null.");
		return retrieveNewsService.searchFeaturedNews(request);
	}	
}