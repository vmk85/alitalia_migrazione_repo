package com.alitalia.aem.web.component.news.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveNewsRequest;
import com.alitalia.aem.common.messages.home.RetrieveNewsResponse;
import com.alitalia.aem.service.api.home.RetrieveNewsService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveNewsBehaviour.class)
public class RetrieveNewsBehaviour extends Behaviour<RetrieveNewsRequest, RetrieveNewsResponse> {

	@Reference
	private RetrieveNewsService retrieveNewsService;
	
	@Override
	public RetrieveNewsResponse executeOrchestration(RetrieveNewsRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve News request is null.");
		return retrieveNewsService.searchNews(request);
	}	
}