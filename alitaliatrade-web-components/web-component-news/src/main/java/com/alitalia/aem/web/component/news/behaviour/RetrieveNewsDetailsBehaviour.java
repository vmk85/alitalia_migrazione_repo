package com.alitalia.aem.web.component.news.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveNewsDetailsRequest;
import com.alitalia.aem.common.messages.home.RetrieveNewsDetailsResponse;
import com.alitalia.aem.service.api.home.RetrieveNewsService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveNewsDetailsBehaviour.class)
public class RetrieveNewsDetailsBehaviour extends Behaviour<RetrieveNewsDetailsRequest, RetrieveNewsDetailsResponse> {

	@Reference
	private RetrieveNewsService retrieveNewsService;
	
	@Override
	public RetrieveNewsDetailsResponse executeOrchestration(RetrieveNewsDetailsRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve News Details request is null.");
		return retrieveNewsService.searchNewsDetails(request);
	}
}