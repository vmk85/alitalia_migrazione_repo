package com.alitalia.aem.web.component.news.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveNewsCategoriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveNewsCategoriesResponse;
import com.alitalia.aem.service.api.home.RetrieveNewsService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveNewsCategoriesBehaviour.class)
public class RetrieveNewsCategoriesBehaviour extends Behaviour<RetrieveNewsCategoriesRequest, RetrieveNewsCategoriesResponse> {

	@Reference
	private RetrieveNewsService retrieveNewsService;
	
	@Override
	public RetrieveNewsCategoriesResponse executeOrchestration(RetrieveNewsCategoriesRequest request) {
		if(request == null) throw new IllegalArgumentException("Retrieve News Categories request is null.");
		return retrieveNewsService.searchNewsCategories(request);
	}
}