package com.alitalia.aem.web.component.news.delegate;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.messages.home.RetrieveFeaturedNewsRequest;
import com.alitalia.aem.common.messages.home.RetrieveFeaturedNewsResponse;
import com.alitalia.aem.common.messages.home.RetrieveNewsCategoriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveNewsCategoriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveNewsDetailsRequest;
import com.alitalia.aem.common.messages.home.RetrieveNewsDetailsResponse;
import com.alitalia.aem.common.messages.home.RetrieveNewsRequest;
import com.alitalia.aem.common.messages.home.RetrieveNewsResponse;
import com.alitalia.aem.web.component.news.behaviour.RetrieveFeaturedNewsBehaviour;
import com.alitalia.aem.web.component.news.behaviour.RetrieveNewsBehaviour;
import com.alitalia.aem.web.component.news.behaviour.RetrieveNewsCategoriesBehaviour;
import com.alitalia.aem.web.component.news.behaviour.RetrieveNewsDetailsBehaviour;

@Component(immediate = true, metatype = true)
@Properties({
	@Property( name = SimpleSearchNewsDelegate.FLAG_NEWS_ENVIRONMENT, label = "It is used to selecte the type of news. It must be false in production; it must be true in stage", boolValue = true)
})
@Service(value = SearchNewsDelegate.class)
public class SimpleSearchNewsDelegate implements SearchNewsDelegate {
	
	public static final String FLAG_NEWS_ENVIRONMENT = "environement.test.news";
	private boolean isPubTest;
	
	@Reference
	private RetrieveNewsBehaviour retrieveNewsBehaviour;
	
	@Reference
	private RetrieveFeaturedNewsBehaviour retrieveFeaturedNewsBehaviour;
	
	@Reference
	private RetrieveNewsDetailsBehaviour retrieveNewsDetailsBehaviour;
	
	@Reference
	private RetrieveNewsCategoriesBehaviour retrieveNewsCategoriesBehaviour;

	
	/* SCR lifecycle */
	
	@Activate
	private void activate(ComponentContext componentContext) {
		setProperties(componentContext);
	}
	
	@Modified
	private void modified(ComponentContext componentContext) {
		setProperties(componentContext);
	}
	
	@Override
	public RetrieveNewsResponse searchNews(RetrieveNewsRequest request) {
		BehaviourExecutor<RetrieveNewsBehaviour, RetrieveNewsRequest, RetrieveNewsResponse> executor = new BehaviourExecutor<>();
		request.setPubTest(isPubTest);
		return executor.executeBehaviour(retrieveNewsBehaviour, request);
	}

	@Override
	public RetrieveFeaturedNewsResponse searchFeaturedNews(RetrieveFeaturedNewsRequest request) {
		BehaviourExecutor<RetrieveFeaturedNewsBehaviour, RetrieveFeaturedNewsRequest, RetrieveFeaturedNewsResponse> executor = new BehaviourExecutor<>();
		request.setPubTest(isPubTest);
		return executor.executeBehaviour(retrieveFeaturedNewsBehaviour, request);
	}

	@Override
	public RetrieveNewsDetailsResponse searchNewsDetails(RetrieveNewsDetailsRequest request) {
		BehaviourExecutor<RetrieveNewsDetailsBehaviour, RetrieveNewsDetailsRequest, RetrieveNewsDetailsResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(retrieveNewsDetailsBehaviour, request);
	}

	@Override
	public RetrieveNewsCategoriesResponse searchNewsCategories(RetrieveNewsCategoriesRequest request) {
		BehaviourExecutor<RetrieveNewsCategoriesBehaviour, RetrieveNewsCategoriesRequest, RetrieveNewsCategoriesResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(retrieveNewsCategoriesBehaviour, request);
	}
	
	
	private void setProperties(ComponentContext ctx) {
		isPubTest = PropertiesUtil.toBoolean(ctx.getProperties().get(FLAG_NEWS_ENVIRONMENT), true);
	}
}