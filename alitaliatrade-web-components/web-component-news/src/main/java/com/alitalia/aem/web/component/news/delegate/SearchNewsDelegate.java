package com.alitalia.aem.web.component.news.delegate;

import com.alitalia.aem.common.messages.home.RetrieveFeaturedNewsRequest;
import com.alitalia.aem.common.messages.home.RetrieveFeaturedNewsResponse;
import com.alitalia.aem.common.messages.home.RetrieveNewsCategoriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveNewsCategoriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveNewsDetailsRequest;
import com.alitalia.aem.common.messages.home.RetrieveNewsDetailsResponse;
import com.alitalia.aem.common.messages.home.RetrieveNewsRequest;
import com.alitalia.aem.common.messages.home.RetrieveNewsResponse;

public interface SearchNewsDelegate {

	RetrieveNewsResponse searchNews(RetrieveNewsRequest request);
	RetrieveFeaturedNewsResponse searchFeaturedNews(RetrieveFeaturedNewsRequest request);
	RetrieveNewsDetailsResponse searchNewsDetails(RetrieveNewsDetailsRequest request);
	RetrieveNewsCategoriesResponse searchNewsCategories(RetrieveNewsCategoriesRequest request);
}