package com.alitalia.aem.web.component.newsletter.delegate;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.messages.home.RegisterUserToAgencyNewsletterRequest;
import com.alitalia.aem.common.messages.home.RegisterUserToAgencyNewsletterResponse;
import com.alitalia.aem.web.component.newsletter.behaviour.RegisterUserToAgencyNewsletterBehaviour;

@Component(immediate = true, metatype = false)
@Service(value = NewsletterDelegate.class)
public class SimpleNewsletterDelegate implements NewsletterDelegate {

	@Reference
	private RegisterUserToAgencyNewsletterBehaviour registerUserBehaviour;
	
	@Override
	public RegisterUserToAgencyNewsletterResponse registerUserToAgencyNewsletter(RegisterUserToAgencyNewsletterRequest request) {
		BehaviourExecutor<RegisterUserToAgencyNewsletterBehaviour, RegisterUserToAgencyNewsletterRequest, RegisterUserToAgencyNewsletterResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(registerUserBehaviour, request);
	}
}