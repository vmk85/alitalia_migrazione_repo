package com.alitalia.aem.web.component.newsletter.delegate;

import com.alitalia.aem.common.messages.home.RegisterUserToAgencyNewsletterRequest;
import com.alitalia.aem.common.messages.home.RegisterUserToAgencyNewsletterResponse;

public interface NewsletterDelegate {

	RegisterUserToAgencyNewsletterResponse registerUserToAgencyNewsletter(RegisterUserToAgencyNewsletterRequest request);
}