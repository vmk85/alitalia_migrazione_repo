package com.alitalia.aem.web.component.newsletter.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RegisterUserToAgencyNewsletterRequest;
import com.alitalia.aem.common.messages.home.RegisterUserToAgencyNewsletterResponse;
import com.alitalia.aem.service.api.home.NewsletterService;

@Component(immediate = true, metatype = false)
@Service(value = RegisterUserToAgencyNewsletterBehaviour.class)
public class RegisterUserToAgencyNewsletterBehaviour extends Behaviour<RegisterUserToAgencyNewsletterRequest, RegisterUserToAgencyNewsletterResponse> {

	@Reference
	private NewsletterService newsletterService;
	
	@Override
	public RegisterUserToAgencyNewsletterResponse executeOrchestration(RegisterUserToAgencyNewsletterRequest request) {
		if(request == null) throw new IllegalArgumentException("Register User to Agency Newsletter request is null.");
		return newsletterService.registerUserToAgencyNewsletter(request);
	}	
}