package com.alitalia.aem.web.component.tradestatistics.delegate;

import com.alitalia.aem.common.messages.home.RegisterStatisticsResponse;
import com.alitalia.aem.common.messages.home.TradeRegisterStatisticRequest;

public interface RegisterTradeStatisticsDelegate {

	RegisterStatisticsResponse registerStatistics(TradeRegisterStatisticRequest request);
}