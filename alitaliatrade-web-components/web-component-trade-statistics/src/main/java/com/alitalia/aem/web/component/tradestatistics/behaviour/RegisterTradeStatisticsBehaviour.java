package com.alitalia.aem.web.component.tradestatistics.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RegisterStatisticsResponse;
import com.alitalia.aem.common.messages.home.TradeRegisterStatisticRequest;
import com.alitalia.aem.service.api.home.RegisterTradeStatisticsService;

@Component(immediate = true, metatype = false)
@Service(value = RegisterTradeStatisticsBehaviour.class)
public class RegisterTradeStatisticsBehaviour extends Behaviour<TradeRegisterStatisticRequest, RegisterStatisticsResponse> {

	@Reference
	private RegisterTradeStatisticsService registerTradeStatisticsService;
	
	@Override
	public RegisterStatisticsResponse executeOrchestration(TradeRegisterStatisticRequest request) {
		if(request == null) throw new IllegalArgumentException("Register Trade Statistics request is null.");
		return registerTradeStatisticsService.registerStatistics(request);
	}
}