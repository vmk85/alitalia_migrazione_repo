package com.alitalia.aem.web.component.tradestatistics.delegate;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.messages.home.RegisterStatisticsResponse;
import com.alitalia.aem.common.messages.home.TradeRegisterStatisticRequest;
import com.alitalia.aem.web.component.tradestatistics.behaviour.RegisterTradeStatisticsBehaviour;

@Component(immediate = true, metatype = false)
@Service(value = RegisterTradeStatisticsDelegate.class)
public class SimpleRegisterStatisticsDelegate implements RegisterTradeStatisticsDelegate {

	@Reference
	private RegisterTradeStatisticsBehaviour registerStatisticsBehaviour;
	
	@Override
	public RegisterStatisticsResponse registerStatistics(TradeRegisterStatisticRequest request) {
		BehaviourExecutor<RegisterTradeStatisticsBehaviour, TradeRegisterStatisticRequest, RegisterStatisticsResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(registerStatisticsBehaviour, request);
	}
}